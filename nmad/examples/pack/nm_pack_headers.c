/*
 * NewMadeleine
 * Copyright (C) 2006-2022 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


#include <stdlib.h>
#include <stdio.h>
#include "../common/nm_examples_helper.h"
#include <nm_pack_interface.h>

#define SIZE  (64 * 1024)
#define NHEADERS 40

int main(int argc, char**argv)
{
  nm_examples_init(&argc, argv);

  char*buf = malloc(SIZE + 1);

  if(is_server)
    {
      nm_pack_cnx_t cnx;
      nm_begin_packing(p_session, p_gate, 0, &cnx);
      int i;
      int h[NHEADERS];
      for(i = 0; i < NHEADERS; i++)
        {
          /* note: we use an array here and don't reuse the same local variable
             for each pack, since data must remain untouched until nm_end_packing().
           */
          h[i] = i;
          nm_pack_express(&cnx, &h[i], sizeof(h[i]));
        }
      nm_pack(&cnx, buf, SIZE);
      nm_end_packing(&cnx);
    }
  else
    {
      nm_pack_cnx_t cnx;
      nm_begin_unpacking(p_session, p_gate, 0, &cnx);
      int i;
      for(i = 0; i < NHEADERS; i++)
        {
          int h = -1;
          nm_unpack_express(&cnx, &h, sizeof(h));
          printf("received h[%d] = %d\n", i, h);
          if(h != i)
            {
              NM_WARN("corrupted header: expected = %d; received = %d\n", i, h);
            }
        }
      nm_unpack(&cnx, buf, SIZE);
      nm_end_unpacking(&cnx);
    }
  free(buf);
  nm_examples_exit();
  return 0;
}

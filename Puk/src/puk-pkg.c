/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Provides "pkg" module driver
 * @ingroup Puk
 */

#include "Puk-internals.h"
#include "Module.h"

PADICO_MODULE_HOOK(Puk);


struct puk_unit_pkg_s
{
  puk_mod_t sub_mod;
  puk_mod_t super_mod;
};
PUK_AUTO_TYPE(puk_unit_pkg);

static padico_rc_t puk_unit_pkg_load  (puk_unit_t unit);
static padico_rc_t puk_unit_pkg_start (puk_unit_t unit, puk_job_t job);
static padico_rc_t puk_unit_pkg_unload(puk_unit_t unit);


static const struct padico_loader_s puk_pkg_driver =
{
  .name   = "pkg",
  .load   = &puk_unit_pkg_load,
  .start  = &puk_unit_pkg_start,
  .unload = &puk_unit_pkg_unload,
};

void padico_puk_pkg_init(void)
{
  puk_component_declare("PadicoLoader-pkg", puk_component_provides("PadicoLoader", "loader", &puk_pkg_driver));
}


/* *** driver: "pkg" *************************************** */

static padico_rc_t puk_unit_pkg_load(puk_unit_t unit)
{
  puk_unit_pkg_t pkg;
  padico_rc_t rc = NULL;

  padico_trace("PKG LOAD **%s**\n", unit->name);
  if(!unit->driver_specific)
    {
      pkg = puk_unit_pkg_new();
      pkg->super_mod = unit->mod;
      pkg->sub_mod = puk_mod_getbyname(unit->name);
      if(pkg->sub_mod)
        {
          rc = padico_rc_error("PKG load error: submodule %s already loaded", unit->name);
          return rc;
        }
      rc = padico_puk_mod_open(&pkg->sub_mod, unit->name);
      if(padico_rc_iserror(rc))
        {
          return rc;
        }
      puk_mod_set_ancestor(pkg->sub_mod, unit->mod);
      unit->driver_specific = (void*)pkg;
    }
  else
    {
      pkg = (puk_unit_pkg_t)unit->driver_specific;
    }
  rc = padico_puk_mod_load(pkg->sub_mod);
  return rc;
}

static padico_rc_t puk_unit_pkg_start(puk_unit_t unit, puk_job_t job)
{
  puk_unit_pkg_t pkg = (puk_unit_pkg_t)unit->driver_specific;
  padico_rc_t rc = NULL;

  padico_trace("Puk: PKG RUN **%s**\n", unit->name);
  /* TODO: **warning** this works because a pkg is allowed to
   *       have **only one** runnable unit.
   *       the sub_mod is started with the same job.
   */
  rc = padico_puk_mod_start(pkg->sub_mod, job);
  return rc;
}

static padico_rc_t puk_unit_pkg_unload(puk_unit_t unit)
{
  puk_unit_pkg_t pkg = (puk_unit_pkg_t)unit->driver_specific;
  padico_rc_t rc = NULL;

  padico_trace("PKG UNLOAD **%s**\n", unit->name);
  rc = padico_puk_mod_unload(pkg->sub_mod);
  pkg->sub_mod = NULL;
  puk_unit_pkg_delete(pkg);
  unit->driver_specific = NULL;
  return rc;
}

void puk_unit_pkg_ctor(puk_unit_pkg_t pkg)
{
  pkg->sub_mod = NULL;
  pkg->super_mod = NULL;
}

void puk_unit_pkg_dtor(puk_unit_pkg_t pkg)
{
  if(pkg->sub_mod)
    padico_fatal("PKG internal error -- submods not unloaded\n");
}

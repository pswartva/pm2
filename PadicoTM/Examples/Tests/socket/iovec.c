#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <strings.h>
#include <string.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <time.h>
#include <sys/uio.h>
#include "helper.h"

#define MSGLEN 10

int main(int argc, char *argv[])
{
  usage(argc, argv);
  int port = atoi(argv[3]);
  int sock_fd = create_socket();
  char buf0[MSGLEN];
  char buf1[MSGLEN];
  char buf2[MSGLEN];
  int i;

  struct iovec iov[3];
  int iovcnt = 3;

  if(strncmp("server",argv[1],6)==0)
    {
      bind_and_listen(sock_fd, port);
      struct sockaddr_in cli_addr;
      int clilen = sizeof(cli_addr);
      int clifd = accept(sock_fd,  (struct sockaddr *) &cli_addr,&clilen);
      if (clifd < 0)
        print_error("accept()");

      memset(buf0, 'a', MSGLEN); buf0[MSGLEN-1]='\0';
      memset(buf1, 'b', MSGLEN); buf1[MSGLEN-1]='\0';
      memset(buf2, 'c', MSGLEN); buf2[MSGLEN-1]='\0';

      iov[0].iov_base = buf0;
      iov[0].iov_len = strlen(iov[0].iov_base) + 1;
      iov[1].iov_base = buf1;
      iov[1].iov_len = strlen(iov[1].iov_base) + 1;
      iov[2].iov_base = buf2;
      iov[2].iov_len = strlen(iov[2].iov_base) + 1;

      if (writev(clifd, iov, iovcnt) < 0)
        print_error("writev()");
      printf("Server: I send    %s %s %s\n", iov[0].iov_base, iov[1].iov_base, iov[2].iov_base);

      buf0[3] = '\0';
      iov[0].iov_len = strlen(iov[0].iov_base) + 1;

      if (writev(clifd, iov, iovcnt) < 0)
        print_error("writev()");
      printf("Server: I send    %s %s %s\n", iov[0].iov_base, iov[1].iov_base, iov[2].iov_base);

      close2(clifd);

   }

  else if(strncmp("client",argv[1],6)==0)
    {
      connect2(sock_fd, port, argv[2]);

      iov[0].iov_base = buf0;
      iov[0].iov_len = MSGLEN;
      iov[1].iov_base = buf1;
      iov[1].iov_len = MSGLEN;
      iov[2].iov_base = buf2;
      iov[2].iov_len = MSGLEN;
      if(readv(sock_fd, iov, iovcnt) < 0)
        print_error("readv()");
      printf("Client: I receive %s %s %s\n", iov[0].iov_base, iov[1].iov_base, iov[2].iov_base);

      iov[0].iov_len = 4;
      if(readv(sock_fd, iov, iovcnt) < 0)
        print_error("readv()");
      printf("Client: I receive %s %s %s\n", iov[0].iov_base, iov[1].iov_base, iov[2].iov_base);

    }
  close2(sock_fd);

  return 0;
}

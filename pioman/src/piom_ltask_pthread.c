/*
 * Pioman: a Generic I/O Manager
 * Copyright (C) 2001-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include "piom_private.h"
#include <unistd.h>
#include <sched.h>

#include <Padico/Module.h>

PADICO_MODULE_HOOK(pioman);

#define PIOM_PTHREAD_MAX_THREADS 64

#ifdef PIOMAN_PTHREAD
static struct
{
  /** number of spare LWPs running */
  int lwps_num;
  /** number of available LWPs */
  int lwps_avail;
  /** signal new tasks to LWPs */
  sem_t lwps_ready;
  /** ltasks queue to feed LWPs */
  struct piom_ltask_lfqueue_s lwps_queue;
  /** array of all created threads */
  pthread_t all_threads[PIOM_PTHREAD_MAX_THREADS];
  /** size of above array */
  int n_threads;
  /** number of bound polling threads */
  volatile int bound_threads_num;
  /** array of logical ids for bound polling threads */
  int bound_threads_indexes[PIOM_PTHREAD_MAX_THREADS];
  /** hierarchy level where to bind polling threads */
  int bound_threads_level;
  /** signal bound thread level info is available */
  sem_t bound_threads_ready;
  /** thread local information */
  pthread_key_t threadstate_key;
} __piom_pthread =
  {
    .lwps_num  = 0,
    .n_threads = 0,
    .bound_threads_num = 0,
    .bound_threads_level = 0
  };

#endif /* PIOMAN_PTHREAD */


#ifdef PIOMAN_PTHREAD
static void __piom_pthread_setname(const char*name)
{
#if defined(__GLIBC__) && defined(__GLIBC_MINOR__) && ((__GLIBC__ == 2 && __GLIBC_MINOR__ >= 12) || (__GLIBC__ > 2))
  pthread_setname_np(pthread_self(), name);
#else
#warning pthread_setname_np not available
#endif
}
static pthread_t*__piom_pthread_create(const pthread_attr_t*attr, void*(*start_routine)(void*), void *arg)
{
  pthread_t*thread = &__piom_pthread.all_threads[__piom_pthread.n_threads];
  __piom_pthread.n_threads++;
  assert(__piom_pthread.n_threads < PIOM_PTHREAD_MAX_THREADS);
  int err = pthread_create(thread, attr, start_routine, arg);
  if(err)
    {
      PIOM_FATAL("cannot create threads- error %d (%s)\n", err, strerror(err));
    }
  return thread;
}

static void*__piom_ltask_timer_worker(void*_dummy __attribute__((unused)))
{
  const long timeslice_nsec = piom_parameters.timer_period * 1000;
  int policy = SCHED_OTHER;
  const int max_prio = sched_get_priority_max(policy);
  struct sched_param old_param;
  pthread_getschedparam(pthread_self(), &policy, &old_param);
  const int old_prio = old_param.sched_priority;
  __piom_pthread_setname("_pioman_timer");
  int rc = pthread_setschedprio(pthread_self(), max_prio);
  const int prio_enable = ((rc == 0) && (max_prio != old_prio));
  if(rc != 0)
    {
      PIOM_WARN("timer thread could not get priority %d (err=%d, %s).\n", max_prio, rc, strerror(rc));
    }
  piom_ltask_queue_t*global_queue = piom_topo_get_queue(piom_topo_current_obj());
  while(global_queue->state != PIOM_LTASK_QUEUE_STATE_STOPPED)
    {
      struct timespec t = { .tv_sec = 0, .tv_nsec = timeslice_nsec };
      clock_nanosleep(CLOCK_MONOTONIC, 0, &t, NULL);
      piom_trace_local_event(PIOM_TRACE_EVENT_TIMER_POLL, NULL);
      if(prio_enable)
        pthread_setschedprio(pthread_self(), old_prio);
      piom_ltask_schedule(PIOM_POLL_POINT_TIMER);
      sched_yield();
      if(prio_enable)
        pthread_setschedprio(pthread_self(), max_prio);
    }
  return NULL;
}

static void*__piom_ltask_idle_worker(void*_dummy)
{
  piom_ltask_queue_t*queue = _dummy;
  int policy = SCHED_OTHER;
  const int min_prio = sched_get_priority_min(policy);
  struct sched_param old_param;
  pthread_getschedparam(pthread_self(), &policy, &old_param);
  const int old_prio = old_param.sched_priority;
  __piom_pthread_setname("_pioman_idle");
  int rc = pthread_setschedprio(pthread_self(), min_prio);
  const int prio_enable = ((rc == 0) && (min_prio != old_prio));
#ifdef SCHED_IDLE
  rc = pthread_setschedparam(pthread_self(), SCHED_IDLE, &old_param);
#else /* SCHED_IDLE */
  rc = -1;
#endif /* SCHED_IDLE */
  const int sched_idle_enable = (rc == 0);
  const int granularity0 = piom_parameters.idle_granularity;
  const int coef = (piom_parameters.idle_coef > 0) ? piom_parameters.idle_coef : 1;
  if(rc != 0)
    {
      PIOM_WARN("idle thread could not get priority %d (err=%d, %s).\n", min_prio, rc, strerror(rc));
    }
  usleep(1000); /* give an opportunity to Linux scheduler to move this thread away */
  while(queue->state != PIOM_LTASK_QUEUE_STATE_STOPPED)
    {
      puk_tick_t s1, s2;
      const int poll_level = piom_ltask_poll_level_get();
      const int granularity = (poll_level && !piom_ltask_dlfq_empty(&queue->ltask_queue)) ?
        granularity0 : coef * (granularity0 + 1);
      PUK_GET_TICK(s1);
      piom_trace_local_event(PIOM_TRACE_EVENT_IDLE_POLL, NULL);
      if(prio_enable)
        pthread_setschedprio(pthread_self(), old_prio);
      if(sched_idle_enable)
        pthread_setschedparam(pthread_self(), SCHED_OTHER, &old_param);

      piom_ltask_schedule(PIOM_POLL_POINT_IDLE);

      if(sched_idle_enable)
        pthread_setschedparam(pthread_self(), SCHED_IDLE, &old_param);
      if(prio_enable)
        pthread_setschedprio(pthread_self(), min_prio);
      if(granularity > 0 && granularity < 55)
        {
          double d = 0.0;
          do
            {
              pthread_yield();
#if 0
#ifdef PIOMAN_X86INTRIN
              /* TODO: we should check for 'contant_tsc' cpuid flag before using tsc */
              uint64_t tsc0 = __rdtsc();
              uint64_t tsc1 = 0;
              do
                {
                  tsc1 = __rdtsc();
                }
              while(tsc1 - tsc0 < 100 * granularity);
#endif /* PIOMAN_X86INTRIN */
#endif
              const int poll_level2 = piom_ltask_poll_level_get();
              if(poll_level2 > poll_level)
                break;
              PUK_GET_TICK(s2);
              d = PUK_TIMING_DELAY(s1, s2);
            }
          while(d < granularity);
        }
      else if(granularity > 0)
        {
          struct timespec t = { .tv_sec = 0, .tv_nsec = granularity * 1000 };
          clock_nanosleep(CLOCK_MONOTONIC, 0, &t, NULL);
        }
      else
        {
          sched_yield();
        }
    }
  return NULL;
}

static void*__piom_ltask_bound_worker(void*_dummy __attribute__((unused)))
{
  __piom_pthread_setname("_pioman_bound");

  piom_ltask_queue_t*global_queue = piom_topo_get_queue(piom_topo_current_obj());
  while(global_queue->state != PIOM_LTASK_QUEUE_STATE_STOPPED)
    {
      piom_ltask_schedule(PIOM_POLL_POINT_FORCED);
      if(PUK_VG_RUNNING_ON_VALGRIND)
        {
          sched_yield();
        }
    }
  return NULL;
}

static void*__piom_ltask_bound_creator_worker(void*_dummy __attribute__((unused)))
{
  __piom_pthread_setname("_pioman_bound_creator");
#if defined(PIOMAN_TOPOLOGY_HWLOC)
  hwloc_topology_t topo = piom_topo_hwloc_get();
  hwloc_obj_t o = NULL;
  int i;
  unsigned next_logical_id;

  sem_wait(&__piom_pthread.bound_threads_ready);
  assert(__piom_pthread.bound_threads_num);

  const hwloc_obj_type_t level = __piom_pthread.bound_threads_level;
  for(i = 0; i < __piom_pthread.bound_threads_num; i ++)
    {
      next_logical_id = __piom_pthread.bound_threads_indexes[i];
      o = hwloc_get_obj_by_type(topo, level, next_logical_id);
      if(o == NULL)
        PIOM_FATAL("Logical id %d not found at level.", next_logical_id);
      if(o->logical_index == next_logical_id)
        {
          char s_obj[128];
          hwloc_obj_type_snprintf(s_obj, sizeof(s_obj), o, 0);
          PIOM_DISP("bound worker #%d on %s #%d.\n", i, s_obj, o->logical_index);
          pthread_attr_t attr;
          pthread_attr_init(&attr);

          pthread_t*bound_thread = __piom_pthread_create(&attr, &__piom_ltask_bound_worker, NULL);
          if(puk_opt_parse_bool(getenv("PIOM_DEDICATED_NOBIND")))
            {
              PIOM_WARN("pioman dedicated thread not bound due to PIOM_DEDICATED_NOBIND=1 set by user.\n");
            }
          else
            {
              int rc = hwloc_set_thread_cpubind(topo, *bound_thread, o->cpuset, HWLOC_CPUBIND_THREAD);
              if(rc != 0)
                {
                  PIOM_WARN("hwloc_set_thread_cpubind failed; rc = %d; errno = %d (%s).\n", rc, errno, strerror(errno));
                }
            }
        }
    }
#endif /* PIOMAN_TOPOLOGY_* */
  return NULL;
}

static void*__piom_ltask_lwp_worker(void*_dummy __attribute__((unused)))
{
  __piom_pthread_setname("_pioman_lwp");
  for(;;)
    {
      sem_wait(&__piom_pthread.lwps_ready);
      struct piom_ltask*task = piom_ltask_lfqueue_dequeue(&__piom_pthread.lwps_queue);
      piom_ltask_blocking_invoke(task);
      __sync_fetch_and_add(&__piom_pthread.lwps_avail, 1);
    }
  return NULL;
}

struct piom_ltask_threadstate_s*piom_ltask_pthread_getthreadstate(void)
{
  struct piom_ltask_threadstate_s*p_threadstate = pthread_getspecific(__piom_pthread.threadstate_key);
  if(p_threadstate == NULL)
    {
      p_threadstate = padico_malloc(sizeof(struct piom_ltask_threadstate_s));
      pthread_setspecific(__piom_pthread.threadstate_key, p_threadstate);
      p_threadstate->scheduling_queue = NULL;
    }
  return p_threadstate;
}

static void __piom_pthread_state_destructor(void*_p_threadstate)
{
  struct piom_pthread_threadstate_s*p_threadstate = _p_threadstate;
  assert(p_threadstate != NULL);
  padico_free(p_threadstate);
}
void piom_ltask_pthread_init(void)
{
  pthread_key_create(&__piom_pthread.threadstate_key, &__piom_pthread_state_destructor);
  /* ** timer-based polling */
  if(piom_parameters.timer_period > 0)
    {
      PIOM_DISP("starting timer worker (period = %d usec.)\n", piom_parameters.timer_period);
      __piom_pthread_create(NULL, &__piom_ltask_timer_worker, NULL);
    }
  /* ** idle polling */
  if(piom_parameters.idle_granularity >= 0)
    {
#if defined(PIOMAN_TOPOLOGY_NONE)
      PIOM_WARN("no hwloc, using global queue. Running in degraded mode.\n");
      piom_ltask_queue_t*queue = piom_topo_get_queue(piom_topo_full);
      pthread_t*idle_thread = __piom_pthread_create(NULL, &__piom_ltask_idle_worker, queue);
      pthread_setschedprio(*idle_thread, sched_get_priority_min(SCHED_OTHER));
#elif defined(PIOMAN_TOPOLOGY_HWLOC)
      hwloc_topology_t topo = piom_topo_hwloc_get();
      const hwloc_obj_type_t level = piom_parameters.binding_level;
      hwloc_obj_t o = NULL;
      int i = 0;
      do
        {
          o = hwloc_get_obj_by_type(topo, level, i);
          if(o == NULL)
            break;
          if( ((piom_parameters.idle_distrib == PIOM_BIND_DISTRIB_FIRST) && (o->logical_index == 0)) ||
              ((piom_parameters.idle_distrib == PIOM_BIND_DISTRIB_ODD)   && (o->logical_index % 2 == 1)) ||
              ((piom_parameters.idle_distrib == PIOM_BIND_DISTRIB_EVEN)  && (o->logical_index % 2 == 0)) ||
              ( piom_parameters.idle_distrib == PIOM_BIND_DISTRIB_ALL)
              )
            {
              char s_obj[128];
              hwloc_obj_type_snprintf(s_obj, sizeof(s_obj), o, 0);
              PIOM_DISP("idle worker #%d on %s #%d (granularity = %d usec.)\n", i, s_obj, o->logical_index, piom_parameters.idle_granularity);
              piom_ltask_queue_t*queue = piom_topo_get_queue(o);
              pthread_attr_t attr;
              pthread_attr_init(&attr);
#ifdef SCHED_IDLE
              if(!piom_parameters.mckernel)
                {
                  pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);
                  pthread_attr_setschedpolicy(&attr, SCHED_IDLE);
                }
              else
                {
                  PIOM_WARN("idle thread not set as SCHED_IDLE on mckernel.\n");
                }
#endif /* SCHED_IDLE */
              pthread_t*idle_thread = __piom_pthread_create(&attr, &__piom_ltask_idle_worker, queue);
              int rc = hwloc_set_thread_cpubind(topo, *idle_thread, o->cpuset, HWLOC_CPUBIND_THREAD);
              if(rc != 0)
                {
                  PIOM_WARN("hwloc_set_thread_cpubind failed; rc = %d; errno = %d (%s).\n", rc, errno, strerror(errno));
                }
            }
          i++;
        }
      while(o != NULL);
#endif /* PIOMAN_TOPOLOGY_* */
    }
  /* ** bound polling */
  if(piom_parameters.dedicated)
    {
#if defined(PIOMAN_TOPOLOGY_NONE)
      PIOM_FATAL("Bound thread polling requested but no way to bind it!\n");
#elif defined(PIOMAN_TOPOLOGY_HWLOC)
      sem_init(&__piom_pthread.bound_threads_ready, 0, 0);
      if(!piom_parameters.dedicated_wait)
        {
          hwloc_topology_t topo = piom_topo_hwloc_get();
          const hwloc_obj_type_t level = piom_parameters.dedicated_level;
          hwloc_obj_t o = NULL;
          unsigned last_index = hwloc_get_nbobjs_by_type(topo, level) - 1;
          int i = 0;
          int ithreads = 0;
          do
            {
              o = hwloc_get_obj_by_type(topo, level, i);
              if(o == NULL)
                break;
              if( ((piom_parameters.dedicated_distrib == PIOM_BIND_DISTRIB_LAST)  && (o->logical_index == last_index)) ||
                  ((piom_parameters.dedicated_distrib == PIOM_BIND_DISTRIB_FIRST) && (o->logical_index == 0)) ||
                  ((piom_parameters.dedicated_distrib == PIOM_BIND_DISTRIB_ODD)   && (o->logical_index % 2 == 1)) ||
                  ((piom_parameters.dedicated_distrib == PIOM_BIND_DISTRIB_EVEN)  && (o->logical_index % 2 == 0)) ||
                  ( piom_parameters.dedicated_distrib == PIOM_BIND_DISTRIB_ALL)
                  )
                {
                  __piom_pthread.bound_threads_indexes[ithreads] = o->logical_index;
                  ithreads++;
                }
              i++;
            }
          while(o != NULL);
          __piom_pthread.bound_threads_level = level;
          __piom_pthread.bound_threads_num = ithreads;
          sem_post(&__piom_pthread.bound_threads_ready);
        }
      else
        {
          if(__piom_pthread.bound_threads_num > 0)
            {
              /* piom_ltask_set_bound_thread_indexes() was called before init */
              sem_post(&__piom_pthread.bound_threads_ready);
            }
        }
      __piom_pthread_create(NULL, &__piom_ltask_bound_creator_worker, NULL);
#endif /* PIOMAN_TOPOLOGY_* */
    }
  /* ** spare LWPs for blocking calls */
  __piom_pthread.lwps_avail = 0;
  __piom_pthread.lwps_num = piom_parameters.spare_lwp;
  if(piom_parameters.spare_lwp)
    {
      PIOM_DISP("starting %d spare LWPs.\n", piom_parameters.spare_lwp);
      sem_init(&__piom_pthread.lwps_ready, 0, 0);
      piom_ltask_lfqueue_init(&__piom_pthread.lwps_queue);
      int i;
      for(i = 0; i < __piom_pthread.lwps_num; i++)
        {
          pthread_t*tid = __piom_pthread_create(NULL, &__piom_ltask_lwp_worker, NULL);
          if(tid == NULL)
            {
              PIOM_FATAL("cannot create spare LWP #%d\n", i);
            }
          __piom_pthread.lwps_avail++;
        }
    }

}

/* Indexes are logical for this function */
int piom_ltask_set_bound_thread_indexes(int level, int*indexes, int size)
{
#if defined(PIOMAN_TOPOLOGY_HWLOC) // threads are not bound if hwloc is not used
  assert(size <= PIOM_PTHREAD_MAX_THREADS);
  memcpy(__piom_pthread.bound_threads_indexes, indexes, size * sizeof(int));
  __piom_pthread.bound_threads_level = level;
  __piom_pthread.bound_threads_num = size;
  if(piom_parameters.dedicated_wait)
    {
      /* signal only if dedicated_wait is explicitely set; semaphore may be uninitialized otherwise */
      sem_post(&__piom_pthread.bound_threads_ready);
    }
#endif
  return 0;
}

/* Indexes are physical for this function */
int piom_ltask_set_bound_thread_os_indexes(int level, int*indexes, int size)
{
#if defined(PIOMAN_TOPOLOGY_HWLOC) // threads are not bound if hwloc is not used
  assert(size <= PIOM_PTHREAD_MAX_THREADS);

  int logical_indexes[PIOM_PTHREAD_MAX_THREADS];
  hwloc_topology_t topo = piom_topo_hwloc_get();
  int i = 0;

  for(i = 0; i < size; i++)
    {
      logical_indexes[i] = hwloc_get_pu_obj_by_os_index(topo, indexes[i])->logical_index;
    }

  return piom_ltask_set_bound_thread_indexes(level, logical_indexes, size);
#else
  return 0;
#endif
}

static int piom_pthread_lwp_exit(void*_arg __attribute__((unused)))
{
  piom_ltask_postinvoke(); /* pretend the task is over */
  pthread_exit(NULL); /* exit LWP worker from task */
}

void piom_ltask_pthread_exit(void)
{
  /* idle & timer threads exit is triggered by ltask queues going to state STOPPED */
  /* ask for LWPs exit */
  int i;
  struct piom_ltask ltask[PIOM_PTHREAD_MAX_THREADS];
  for(i = 0; i < __piom_pthread.lwps_num; i++)
    {
      piom_ltask_create(&ltask[i], &piom_pthread_lwp_exit, NULL, PIOM_LTASK_OPTION_ONESHOT);
      piom_ltask_set_blocking(&ltask[i], &piom_pthread_lwp_exit, 0);
      piom_ltask_submit_in_lwp(&ltask[i]);
    }

  for(i = 0; i < __piom_pthread.n_threads; i++)
    {
      pthread_join(__piom_pthread.all_threads[i], NULL);
    }
  pthread_key_delete(__piom_pthread.threadstate_key);
}

int piom_ltask_submit_in_lwp(struct piom_ltask*task)
{
  assert(task->blocking_func);
  if(__sync_fetch_and_sub(&__piom_pthread.lwps_avail, 1) > 0)
    {
      piom_ltask_lfqueue_enqueue(&__piom_pthread.lwps_queue, task);
      sem_post(&__piom_pthread.lwps_ready);
      return 0;
    }
  else
    {
      /* rollback */
      __sync_fetch_and_add(&__piom_pthread.lwps_avail, 1);
    }
  return -1;
}

#endif /* PIOMAN_PTHREAD */

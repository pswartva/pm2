/*
 * MadMPI benchmark
 * Copyright (C) 2015-2020 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include "mpi_bench_generic.h"

static int rank = -1;
static int size = -1;
static void*root_buf = NULL;

static int compute = 500;

static const struct mpi_bench_param_bounds_s param_bounds =
  {
    .min  = MIN_COMPUTE_COLL,
    .max  = MAX_COMPUTE_COLL,
    .mult = MULT_COMPUTE_COLL,
    .incr = 1
  };

static const struct mpi_bench_param_bounds_s*mpi_bench_colloverlap_igather_getparams(void)
{
  return &param_bounds;
}

static void mpi_bench_colloverlap_igather_setparam(int param)
{
  compute = param;
}

static void mpi_bench_colloverlap_igather_init(void*buf, size_t len)
{
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  mpi_bench_check_size(size * len);
  if(rank == 0)
    {
      root_buf = malloc(size * len);
    }
}

static void mpi_bench_colloverlap_igather_finalize(void)
{
  if(root_buf != NULL)
    {
      free(root_buf);
      root_buf = NULL;
    }
}

static void mpi_bench_colloverlap_igather_func(void*buf, size_t len)
{
  MPI_Request req;
  MPI_Igather(buf, len, MPI_BYTE, root_buf, len, MPI_BYTE, 0, MPI_COMM_WORLD, &req);
  mpi_bench_do_compute(compute);
  MPI_Wait(&req, MPI_STATUS_IGNORE);
}

const struct mpi_bench_s mpi_bench_colloverlap_igather =
  {
    .label      = "mpi_bench_colloverlap_igather",
    .name       = "MPI igather overlap",
    .rtt        = MPI_BENCH_RTT_COLLECTIVE,
    .collective = 1,
    .init       = &mpi_bench_colloverlap_igather_init,
    .finalize   = &mpi_bench_colloverlap_igather_finalize,
    .server     = &mpi_bench_colloverlap_igather_func,
    .client     = &mpi_bench_colloverlap_igather_func,
    .setparam   = &mpi_bench_colloverlap_igather_setparam,
    .getparams  = &mpi_bench_colloverlap_igather_getparams
  };

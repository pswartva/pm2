#! /bin/sh

NS_CONFIG=$1
peer=$2

if [ ! -r $NS_CONFIG ]; then
    echo "# ns config file *${NS_CONFIG}* not found."
    exit 1
fi

padico-launch -c -v -DNS_BASIC_CONF=${NS_CONFIG} -DPADICO_BOOTSTRAP_MODE=client -DPADICO_BOOTSTRAP_HOST=${peer} -DPADICO_BOOTSTRAP_SYNC=2 -DBENCH_PEER=${peer} bench-VIO 


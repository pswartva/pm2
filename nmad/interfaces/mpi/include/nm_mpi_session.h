/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#ifndef NM_MPI_SESSION_H
#define NM_MPI_SESSION_H

/**
 * @ingroup mpi_interface
 * @file
 * declarations for MPI collectives
 * @{
 */

/** @name Functions: Sessions
 * @note 100% of the MPI-4 session API is supported. However, the
 * semantics is not correct yet. We may create sessions only if
 * MPI_Init() has been called previously.
 * @{ */

int MPI_Session_create_errhandler(MPI_Session_errhandler_function*session_errhandler_fn,
                                  MPI_Errhandler*errhandler);

int MPI_Session_set_errhandler(MPI_Session session, MPI_Errhandler errhandler);
int MPI_Session_get_errhandler(MPI_Session session, MPI_Errhandler*errhandler);
int MPI_Session_call_errhandler(MPI_Session session, int errorcode);


int MPI_Session_init(MPI_Info info, MPI_Errhandler errhandler, MPI_Session*session);
int MPI_Session_finalize(MPI_Session*session);

int MPI_Session_get_num_psets(MPI_Session session, MPI_Info info, int*npset_names);
int MPI_Session_get_nth_pset(MPI_Session session, MPI_Info info, int n,
                             int*pset_len, char*pset_name);
int MPI_Session_get_info(MPI_Session session, MPI_Info*info_used);
int MPI_Session_get_pset_info(MPI_Session session, const char*pset_name, MPI_Info*info);

int MPI_Group_from_session_pset(MPI_Session session, const char*pset_name, MPI_Group*newgroup);

/** @}*/
/** @}*/

#endif /* NM_MPI_SESSION_H */

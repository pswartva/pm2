/*
 * NewMadeleine
 * Copyright (C) 2006 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <values.h>

#include "helper.h"

#define MIN_DEFAULT     1
#define MAX_DEFAULT     (512 * 512 * 1024)
#define MULT_DEFAULT    2
#define INCR_DEFAULT    0
#define LOOPS_DEFAULT   1000000

//sort algorithm
void fusion(double* tableau, int debut1, int fin1, int fin2)
{

   int debut2 = fin1+1;
   int compteur1 = debut1;
   int compteur2 = debut2;
   int i;

   double* tableau2 = malloc((fin1-debut1+1)*sizeof(double));

   // copy of the element of the beginning of the array
   for(i=debut1; i<=fin1; i++)
      tableau2[i-debut1] = tableau[i];

   // fusion of the two arrays
   for(i=debut1; i<=fin2; i++)
   {
      if(compteur1==debut2) // every element of the first array used
         break; // every element ranked
      else if(compteur2==(fin2+1)) //every element of the second array used
      { // copy of the element of the first under-array at the end of the array
         tableau[i] = tableau2[compteur1-debut1];
         compteur1++;
      }
      else if(tableau2[compteur1-debut1]<tableau[compteur2])
      { // adding of 1 element of the first under-array
         tableau[i] = tableau2[compteur1-debut1];
         compteur1++; // going the next item of the first under-array
      }
      else
      { // copy of the element at the end of the array
         tableau[i] = tableau[compteur2];
         compteur2++; // going the next item of the second under-array
      }
   }
   free(tableau2);
}

void triFusionAux(double* tableau, int debut, int fin)
{
   if(debut!=fin) // stop condition
   {
      int milieu = (debut+fin)/2;
      triFusionAux(tableau, debut, milieu); // sort part1
      triFusionAux(tableau, milieu+1, fin); // sort part2
      fusion(tableau, debut, milieu, fin); // fusion of the two parts
   }
}

void triFusion(double* tableau, int longueur)
{
   if(longueur>0)
      triFusionAux(tableau, 0, longueur-1);
}


//end sort algorithm

//begin median

double median(double* tab, int length){
  if(length%2==0){
    return (tab[length/2-1]+tab[length/2])/2;
  }
  else{
    return tab[length/2];
  }
}

//end median

static inline uint32_t _next(uint32_t len, double multiplier, uint32_t increment)
{
  if(len == 0)
    len = 1;
  return len * multiplier + increment;
}

static inline uint32_t _iterations(int iterations, uint32_t len)
{
  const uint64_t max_data = 512 * 512 * 1024;
  if(len <= 0)
    len = 1;
  uint64_t data_size = ((uint64_t)iterations * (uint64_t)len);
  if(data_size  > max_data)
    {
      iterations = (max_data / (uint64_t)len);
      if(iterations < 2)
        iterations = 2;
    }
  return iterations;
}


static void usage_ping(void) {
  fprintf(stderr, "-S start_len - starting length [%d]\n", MIN_DEFAULT);
  fprintf(stderr, "-E end_len - ending length [%d]\n", MAX_DEFAULT);
  fprintf(stderr, "-I incr - length increment [%d]\n", INCR_DEFAULT);
  fprintf(stderr, "-M mult - length multiplier [%d]\n", MULT_DEFAULT);
  fprintf(stderr, "\tNext(0)      = 1+increment\n");
  fprintf(stderr, "\tNext(length) = length*multiplier+increment\n");
  fprintf(stderr, "-N iterations - iterations per length [%d]\n", LOOPS_DEFAULT);
}

static void fill_buffer(char *buffer, int len) {
  int i = 0;

  for (i = 0; i < len; i++) {
    buffer[i] = 'a'+(i%26);
  }
}

static void clear_buffer(char *buffer, int len) {
  memset(buffer, 0, len);
}

int main(int argc, char         **argv)
{
  uint32_t       start_len      = MIN_DEFAULT;
  uint32_t       end_len        = MAX_DEFAULT;
  double         multiplier     = MULT_DEFAULT;
  uint32_t         increment      = INCR_DEFAULT;
  int              iterations     = LOOPS_DEFAULT;
  int              i;

  nm_examples_init_topo(&argc, argv, NM_EXAMPLES_TOPO_PAIRS);

  if (argc > 1 && !strcmp(argv[1], "--help")) {
    usage_ping();
    nm_examples_exit();
    exit(0);
  }

  for(i=1 ; i<argc ; i+=2) {
    if (!strcmp(argv[i], "-S")) {
      start_len = atoi(argv[i+1]);
    }
    else if (!strcmp(argv[i], "-E")) {
      end_len = atoi(argv[i+1]);
    }
    else if (!strcmp(argv[i], "-I")) {
      increment = atoi(argv[i+1]);
    }
    else if (!strcmp(argv[i], "-M")) {
      multiplier = atof(argv[i+1]);
    }
    else if (!strcmp(argv[i], "-N")) {
      iterations = atoi(argv[i+1]);
    }
    else {
      fprintf(stderr, "Illegal argument %s\n", argv[i]);
      usage_ping();
      nm_examples_exit();
      exit(0);
    }
  }

  if(is_server)
    {
      /* server
       */
      uint32_t   len;
      for(len = start_len; len <= end_len; len = _next(len, multiplier, increment))
        {
          int k;
          char* buf = malloc(len);
          clear_buffer(buf, len);
          iterations = _iterations(iterations, len);
          for(k = 0; k < 2; k++)
            {
              nm_sr_request_t request;

              nm_sr_irecv(p_session, p_gate, 0, buf, len, &request);
              nm_sr_rwait(p_session, &request);

              nm_sr_isend(p_session, p_gate, 0, buf, len, &request);
              nm_sr_swait(p_session, &request);
            }
          for(k = 0; k < iterations; k++)
            {
              nm_sr_request_t request;

              nm_sr_irecv(p_session, p_gate, 0, buf, len, &request);
              nm_sr_rwait(p_session, &request);

              nm_sr_isend(p_session, p_gate, 0, buf, len, &request);
              nm_sr_swait(p_session, &request);
            }
          free(buf);
        }
    }
  else
    {
      /* client
       */
      tbx_tick_t t1, t2;
      printf("# sr_bench begin\n");
      printf("# size |  latency min | latency med | latency max |  min 10^6 B/s | min MB/s | med 10^6 B/s | med MB/s | max 10^6 B/s | max MB/s |\n");
      uint32_t   len;
      int j=0;
      for(len = start_len; len <= end_len; len = _next(len, multiplier, increment))
        {
          i=0;
          char* buf = malloc(len);
          iterations = _iterations(iterations, len);
          double times[iterations];
          int k;
          for(k = 0; k < 2; k++)
            {
              nm_sr_request_t request;
              nm_sr_isend(p_session, p_gate, 0, buf, len, &request);
              nm_sr_swait(p_session, &request);
              nm_sr_irecv(p_session, p_gate, 0, buf, len, &request);
              nm_sr_rwait(p_session, &request);
            }
          for(k = 0; k < iterations; k++)
            {
              nm_sr_request_t request;
              TBX_GET_TICK(t1);
              nm_sr_isend(p_session, p_gate, 0, buf, len, &request);
              nm_sr_swait(p_session, &request);
              nm_sr_irecv(p_session, p_gate, 0, buf, len, &request);
              nm_sr_rwait(p_session, &request);
              TBX_GET_TICK(t2);
              const double delay = TBX_TIMING_DELAY(t1, t2);
              const double t = delay / 2;
              times[i]=t;
              i++;
            }
          triFusion(times,iterations);
          double bw_million_byte_min = len / times[iterations/4];
          double bw_mbyte_min       = bw_million_byte_min / 1.048576;
          double bw_million_byte_med = len / median(times,iterations);
          double bw_mbyte_med       = bw_million_byte_med / 1.048576;
          double bw_million_byte_max = len / times[iterations*3/4];
          double bw_mbyte_max       = bw_million_byte_max / 1.048576;
#ifdef MARCEL
          printf("%9d\t%9.3lf\t%9.3f\t%9.3f\t%9.3lf\t%9.3f\t%9.3f\t%9.3lf\t%9.3f\t%9.3f\tmarcel\n",
                 len, times[iterations/4],median(times,iterations),times[iterations*3/4], bw_million_byte_min, bw_mbyte_min, bw_million_byte_med, bw_mbyte_med, bw_million_byte_max, bw_mbyte_max);
#else
          printf("%9d\t%9.3lf\t%9.3f\t%9.3f\t%9.3lf\t%9.3f\t%9.3f\t%9.3lf\t%9.3f\t%9.3f\n",
                 len, times[iterations/4],median(times,iterations),times[iterations*3/4], bw_million_byte_min, bw_mbyte_min, bw_million_byte_med, bw_mbyte_med, bw_million_byte_max, bw_mbyte_max);
#endif
          fflush(NULL);
          free(buf);
          j++;
        }
      printf("// %d\n",j);
      printf("# sr_bench end\n");
    }

  nm_examples_exit();
  exit(0);
}

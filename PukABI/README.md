#
README for PukABI
=================

This document decribes PukABI installation and configuration.

for any question, mailto: Alexandre.Denis@inria.fr

for information on what PukABI is, see http://pm2.gforge.inria.fr/PadicoTM/

Quick Start
-----------

A quick cheat sheet for the impatient:

    ./autogen.sh
    mkdir build
    cd build
    ../configure --prefix=/prefix
    make
    make install

Requirements
------------
  - autoconf and autoheader (v 2.50 or later)
  - pkg-config
  - gcc 3.2 or higher
  - GNU ld and a ELF-based platform

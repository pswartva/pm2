/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief File-specific part for vnodes.
 * @ingroup VIO
 */

#include "VLink-internals.h"
#include "VIO-internals.h"
#include <Padico/Module.h>

#include <stdio.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/mman.h>


/** @defgroup VFile_SysIO component: VFile_SysIO- plain file component
 * @ingroup PadicoComponent
 */

static int   vfile_open(padico_vnode_t vnode, const char *path, int oflag, mode_t mode);
static off_t vfile_lseek(padico_vnode_t vnode, off_t offset, int whence);
static int   vfile_ftruncate(padico_vnode_t vnode, off_t length);
static int   vfile___fxstat(int version, padico_vnode_t vnode, struct stat *buf);
static void* vfile_mmap(void *addr, size_t len, int prot, int flags, padico_vnode_t vnode, off_t off);
static int   vfile_fsync(padico_vnode_t vnode);

/** driver for the 'VLink' facet of the VFile_SysIO component
 * @ingroup VFile_SysIO
 */
static const struct padico_vlink_driver_s vfile_vlink_driver =
  {
    .create       = NULL,
    .bind         = NULL,
    .listen       = NULL,
    .connect      = NULL,
    .fullshutdown = NULL,
    .close        = vnode_hfd_close,
    .send_post    = vnode_hfd_write_post,
    .recv_post    = vnode_hfd_read_post,
    .dumpstat     = NULL
  };
/** driver for the 'VFile' facet of the VFile/SysIO component
 * @ingroup VFile_SysIO
 */
static const struct padico_vfile_driver_s vfile_driver =
  {
    .open      = &vfile_open,
    .lseek     = &vfile_lseek,
    .ftruncate = &vfile_ftruncate,
    .fxstat    = &vfile___fxstat,
    .mmap      = &vfile_mmap,
    .fcntl     = &vnode_hfd_fcntl,
    .fsync     = &vfile_fsync
  };

PADICO_MODULE_COMPONENT(VFile_SysIO,
  puk_component_declare("VFile_SysIO",
                        puk_component_provides("PadicoComponent", "component", &padico_vnode_component_driver),
                        puk_component_provides("VLink", "vlink", &vfile_vlink_driver),
                        puk_component_provides("VFile", "vfile", &vfile_driver)));

/* ********************************************************* */

static int vfile_open(padico_vnode_t vnode, const char *path, int oflag, mode_t mode)
{
  int fd, err = -1;
  padico_out(50, "vnode=%p; path=%s\n", vnode, path);
  if(oflag & O_CREAT)
    {
      fd  = PUK_ABI_FSYS_WRAP(open)(path, oflag, mode);
      err = __puk_abi_wrap__errno;
    }
  else
    {
      fd  = PUK_ABI_FSYS_WRAP(open)(path, oflag);
      err = __puk_abi_wrap__errno;
    }
  padico_out(50, "vnode=%p; fd=%d\n", vnode, fd);
  if(fd == -1)
    {
      return -err;
    }
  else
    {
      vnode_hfd_create(vnode, fd, mode);
      vnode->status.opened = 1;
      padico_out(50, "vnode=%p; exit\n", vnode);
      return 0;
    }
}

static off_t vfile_lseek(padico_vnode_t vnode, off_t offset, int whence)
{
  vnode_check(vnode, hfd);
  off_t rc = PUK_ABI_FSYS_WRAP(lseek)(vnode->hfd.fd, offset, whence);
  int err  = __puk_abi_wrap__errno;
  vnode_set_error(vnode, err);
  return rc;
}

static int vfile_ftruncate(padico_vnode_t vnode, off_t length)
{
  vnode_check(vnode, hfd);
  int rc  = PUK_ABI_FSYS_WRAP(ftruncate)(vnode->hfd.fd, length);
  int err = __puk_abi_wrap__errno;
  vnode_set_error(vnode, err);
  return rc;
}

static int vfile___fxstat(int version, padico_vnode_t vnode, struct stat *buf)
{
  vnode_check(vnode, hfd);
  int rc  = PUK_ABI_FSYS_WRAP(__fxstat)(version, vnode->hfd.fd, buf);
  int err = __puk_abi_wrap__errno;
  vnode_set_error(vnode, err);
  return rc;
}

static void*vfile_mmap(void *addr, size_t len, int prot, int flags, padico_vnode_t vnode, off_t off)
{
  vnode_check(vnode, hfd);
  padico_out(20, "addr=%p len=%ld hfd=%d\n", addr, puk_ulong(len), vnode->hfd.fd);
  void*rc = PUK_ABI_FSYS_WRAP(mmap)(addr, len, prot, flags, vnode->hfd.fd, off);
  int err = __puk_abi_wrap__errno;
  if(rc == MAP_FAILED)
    {
      vnode_set_error(vnode, err);
    }
  return rc;
}

static int vfile_fsync(padico_vnode_t vnode)
{
  int rc  = PUK_ABI_FSYS_WRAP(fsync)(vnode->hfd.fd);
  int err = __puk_abi_wrap__errno;
  vnode_set_error(vnode, err);
  return rc;
}

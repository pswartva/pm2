
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include <Padico/Puk.h>

#define N 200

int main(int argc, char**argv)
{
  struct puk_rbtree_s tree;
  puk_rbtree_init(&tree);

  int r;
  for(r = 0; r < 100; r++)
    {
      srandom(r);

      int i;
      for(i = 0; i < N; i++)
        {
          int d = random() % N;
          while(puk_rbtree_lookup(&tree, d) != NULL)
            {
              d = random() % N;
            }
          printf("# i = %d; add data %d; height = %d\n", i, d, puk_rbtree_height(&tree));
          puk_rbtree_insert(&tree, d);
          puk_rbtree_traversal(&tree);
          assert(puk_rbtree_lookup(&tree, d) != NULL);

          puk_rbtree_show(&tree);
        }

      for(i = 0; i < N; i++)
        {
          int d = random() % N;
          while(puk_rbtree_lookup(&tree, d) == NULL)
            {
              d = random() % N;
            }
          struct puk_rbtree_node_s*node = puk_rbtree_lookup(&tree, d);
          printf("# node for data = %d: %p\n", d, node);
          puk_rbtree_delete(&tree, node);
          printf("# i = %d; delete data %d; height = %d\n", i, d, puk_rbtree_height(&tree));
          puk_rbtree_traversal(&tree);
          assert(puk_rbtree_lookup(&tree, d) == NULL);
        }
    }

  return 0;
}

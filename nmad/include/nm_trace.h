/*
 * NewMadeleine
 * Copyright (C) 2014-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#ifndef NM_TRACE_H
#define NM_TRACE_H

/** @ingroup nmad_private
 * @file
 * Definitions for traces
 */

typedef int nm_trace_event_t;

/* ** global events */

#define NM_TRACE_EVENT_CONNECT                 ((nm_trace_event_t)0x0100)
#define NM_TRACE_EVENT_DISCONNECT              ((nm_trace_event_t)0x0101)

/* ** core-level pack events */

/** current number of pending packs */
#define NM_TRACE_EVENT_VAR_N_PACKS             ((nm_trace_event_t)0x0200)
/** current number of pending unpacks */
#define NM_TRACE_EVENT_VAR_N_UNPACKS           ((nm_trace_event_t)0x0201)

#define NM_TRACE_EVENT_CORE_PACK_SUBMIT        ((nm_trace_event_t)0x0211) /**< pack submitted to nmad, still in submission list */
#define NM_TRACE_EVENT_CORE_PACK_FLUSH         ((nm_trace_event_t)0x0212) /**< pack flushed out of submission list */
#define NM_TRACE_EVENT_CORE_PACK_RDV           ((nm_trace_event_t)0x0213) /**< send rdv for this pack */
#define NM_TRACE_EVENT_CORE_PACK_IN_PW         ((nm_trace_event_t)0x0214) /**< first chunk of data packed in a pw, ready to send */
#define NM_TRACE_EVENT_CORE_PACK_PW_POSTED     ((nm_trace_event_t)0x0215) /**< pw containing first chunk of data has been posted */
#define NM_TRACE_EVENT_CORE_PACK_COMPLETED     ((nm_trace_event_t)0x0216)

#define NM_TRACE_EVENT_CORE_UNPACK_SUBMIT      ((nm_trace_event_t)0x0221)
#define NM_TRACE_EVENT_CORE_UNPACK_RDV         ((nm_trace_event_t)0x0222) /**< a rdv has been received for this unpack */
#define NM_TRACE_EVENT_CORE_UNPACK_RTR         ((nm_trace_event_t)0x0223) /**< a rtr has been sent for this unpack */
#define NM_TRACE_EVENT_CORE_UNPACK_MATCH_FIRST ((nm_trace_event_t)0x0224) /**< first fragment matches */
#define NM_TRACE_EVENT_CORE_UNPACK_MATCH_LAST  ((nm_trace_event_t)0x0225) /**< last fragment matches */
#define NM_TRACE_EVENT_CORE_UNPACK_COMPLETED   ((nm_trace_event_t)0x0226)

/* ** packet-wrapper progression- scope: driver */

#define NM_TRACE_EVENT_PW_SEND_LARGE_PENDING   ((nm_trace_event_t)0x0401) /**< large pw allocated, not ready to send yet */
#define NM_TRACE_EVENT_PW_POST_SEND            ((nm_trace_event_t)0x0402)
#define NM_TRACE_EVENT_PW_POLL_SEND            ((nm_trace_event_t)0x0403)
#define NM_TRACE_EVENT_PW_COMPLETE_SEND        ((nm_trace_event_t)0x0404)
#define NM_TRACE_EVENT_PW_RECV_LARGE           ((nm_trace_event_t)0x0405) /**< large pw allocated for recv, waiting for data */
#define NM_TRACE_EVENT_PW_POST_RECV            ((nm_trace_event_t)0x0406)
#define NM_TRACE_EVENT_PW_POLL_RECV            ((nm_trace_event_t)0x0407)
#define NM_TRACE_EVENT_PW_COMPLETE_RECV        ((nm_trace_event_t)0x0408)

/* ** Core task states */
#define NM_TRACE_STATE_CORE_NONE               ((nm_trace_event_t)0x0800)
#define NM_TRACE_STATE_CORE_STRATEGY           ((nm_trace_event_t)0x0801)
#define NM_TRACE_STATE_CORE_POLLING            ((nm_trace_event_t)0x0802)
#define NM_TRACE_STATE_CORE_TASK_FLUSH         ((nm_trace_event_t)0x0803)
#define NM_TRACE_STATE_CORE_DISPATCHING        ((nm_trace_event_t)0x0804)
#define NM_TRACE_STATE_CORE_UNPACK_NEXT        ((nm_trace_event_t)0x0805)
#define NM_TRACE_STATE_CORE_COMPLETED_PW       ((nm_trace_event_t)0x0806)
#define NM_TRACE_STATE_CORE_PACK_SUBMIT        ((nm_trace_event_t)0x0807)
#define NM_TRACE_STATE_CORE_HANDLER            ((nm_trace_event_t)0x0808)

/* ** filters */

#define NM_TRACE_FILTER_GLOBAL    0x0100
#define NM_TRACE_FILTER_PACK      0x0200
#define NM_TRACE_FILTER_DRIVER    0x0400
#define NM_TRACE_FILTER_CORE      0x0800
#define NM_TRACE_FILTER_LINK      0x1000
#define NM_TRACE_FILTER_ALL       0xFF00


#ifdef NMAD_TRACE

__PUK_SYM_INTERNAL
void nm_trace_init(nm_core_t p_core);

__PUK_SYM_INTERNAL
void nm_trace_exit(nm_core_t p_core);

void nm_trace_var(nm_trace_event_t event, int value, struct nm_gate_s*p_gate, struct nm_drv_s*p_drv);
void nm_trace_state(nm_trace_event_t event, struct nm_gate_s*p_gate, struct nm_drv_s*p_drv);
void nm_trace_event(nm_trace_event_t event, void*value, struct nm_gate_s*p_gate, struct nm_drv_s*p_drv,
                    nm_core_tag_t core_tag, nm_seq_t seq, nm_len_t len);

#else /* NMAD_TRACE */

#define nm_trace_event(EVENT, VALUE, GATE, DRV, TAG, SEQ, LEN)
#define nm_trace_var(EVENT, VALUE, GATE, DRV)
#define nm_trace_state(EVENT, GATE, DRV)

#endif /* NMAD_TRACE */

#endif /* NM_TRACE_H */

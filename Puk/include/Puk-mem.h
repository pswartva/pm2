/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 ** @brief Memory management.
 * @ingroup Puk
 */

#include <stddef.h>
#include <malloc.h>

/* ** valgrind integration ********************************* */

#if defined(PUK_HAVE_VALGRIND_H)
#include <valgrind/valgrind.h>
#include <valgrind/memcheck.h>
#define PUK_VG_RUNNING_ON_VALGRIND               RUNNING_ON_VALGRIND
#else /* PUK_HAVE_VALGRIND_H */
#define PUK_VG_RUNNING_ON_VALGRIND               0
#endif /* PUK_HAVE_VALGRIND_H */

#if defined(PUK_DEBUG) && defined(PUK_HAVE_VALGRIND_H)
#define PUK_VG_CREATE_MEMPOOL(pool, rz, zeroed)  VALGRIND_CREATE_MEMPOOL(pool, rz, zeroed)
#define PUK_VG_DESTROY_MEMPOOL(pool)             VALGRIND_DESTROY_MEMPOOL(pool)
#define PUK_VG_MEMPOOL_ALLOC(pool, addr, size)   VALGRIND_MEMPOOL_ALLOC(pool, addr, size)
#define PUK_VG_MEMPOOL_FREE(pool, addr)          VALGRIND_MEMPOOL_FREE(pool, addr)
#define PUK_VG_MAKE_MEM_NOACCESS(addr, len)      VALGRIND_MAKE_MEM_NOACCESS(addr, len)
#define PUK_VG_STACK_REGISTER(start, end)        VALGRIND_STACK_REGISTER(start, end)
#define PUK_VG_STACK_DEREGISTER(id)              VALGRIND_STACK_DEREGISTER(id)
#else /* PUK_DEBUG && HAVE_VALGRIND_H */
#define PUK_VG_NOOP ((void)0)
#define PUK_VG_CREATE_MEMPOOL(pool, rz, zeroed)  PUK_VG_NOOP
#define PUK_VG_DESTROY_MEMPOOL(pool)             PUK_VG_NOOP
#define PUK_VG_MEMPOOL_ALLOC(pool, addr, size)   PUK_VG_NOOP
#define PUK_VG_MEMPOOL_FREE(pool, addr)          PUK_VG_NOOP
#define PUK_VG_MAKE_MEM_NOACCESS(addr, len)      PUK_VG_NOOP
#define PUK_VG_STACK_REGISTER(start, end)        PUK_VG_NOOP
#define PUK_VG_STACK_DEREGISTER(id)              PUK_VG_NOOP
#endif /* PUK_DEBUG && HAVE_VALGRIND_H */

/* ** ASAN ************************************************* */

#ifdef __has_feature
#if __has_feature(address_sanitizer)
/* ASAN with clang */
#define PUK_ASAN
#endif /* __has_feature(address_sanitizer) */
#if __has_feature(memory_sanitizer)
/* MSAN with clang */
#define PUK_MSAN
#endif /* __has_feature(memory_sanitizer) */
#endif /* __has_feature */

#ifdef __SANITIZE_ADDRESS__
/* ASAN with gcc */
#define PUK_ASAN
#endif /* __SANITIZE_ADDRESS__ */

/* ** auto types with ctor/dtor **************************** */

#define PUK_AUTO_TYPE(ENAME) \
  PUK_AUTO_TYPE_DECLARE(ENAME) \
  PUK_AUTO_TYPE_CREATE(ENAME)

#define PUK_AUTO_TYPE_DECLARE(ENAME) \
  typedef struct ENAME##_s * ENAME##_t;

#define PUK_AUTO_TYPE_CREATE(ENAME) \
  void ENAME##_ctor(ENAME##_t); \
  void ENAME##_dtor(ENAME##_t); \
  /** @internal */ \
  static inline ENAME##_t ENAME##_new(void) \
  { ENAME##_t obj = (ENAME##_t)padico_malloc(sizeof(struct ENAME##_s)); \
    ENAME##_ctor(obj); return obj; }                                    \
  /** @internal */                                                      \
  static inline void ENAME##_delete(ENAME##_t obj)                      \
  { ENAME##_dtor(obj); padico_free(obj); }


/* ** memory profiling ************************************* */

/** get a pointer to the global memory profiling */
struct puk_profile_mem_s*puk_profile_mem_get_global(void);

/** declare memory profiling as pvars */
void puk_profile_mem_set_pvars(struct puk_profile_mem_s*profile, const char*name);

/** get the total amount of memory allocated through Puk malloc wrappers */
ssize_t puk_profile_mem_get_total(void);

ssize_t puk_profile_mem_get_malloc(void);

ssize_t puk_profile_mem_get_free(void);

void puk_profile_mem_entry_dump(void);

void puk_profile_mem_entry_add(void*ptr, const char*mod, const char*func, const char*file, int line);
void puk_profile_mem_entry_remove(void*ptr, const char*mod, const char*func, const char*file, int line);

void puk_profile_mem_add(struct puk_profile_mem_s*profile, void*ptr, ssize_t size);

void puk_profile_mem_sub(struct puk_profile_mem_s*profile, void*ptr);


/* ** malloc frontends ************************************* */

static inline void*padico_malloc_func(size_t size, struct puk_profile_mem_s*profile, const char*mod, const char*func, const char*file, int line)
{
  void*ptr = malloc(size);
  if(ptr == NULL)
    {
      padico_fatal("memory allocation failed.\n");
    }
  puk_profile_mem_add(puk_profile_mem_get_global(), ptr, size);
  puk_profile_mem_add(profile, ptr, size);
  puk_profile_mem_entry_add(ptr, mod, func, file, line);
  return ptr;
}

#define padico_malloc(SIZE) (padico_malloc_func(SIZE, puk_mod_binary_get_profile(), padico_module_self_name(), __func__, __FILE__, __LINE__))

static inline void*padico_realloc_func(void*ptr, size_t size, struct puk_profile_mem_s*profile, const char*mod, const char*func, const char*file, int line)
{
  if(ptr != NULL)
    {
      puk_profile_mem_sub(puk_profile_mem_get_global(), ptr);
      puk_profile_mem_sub(profile, ptr);
      puk_profile_mem_entry_remove(ptr, mod, func, file, line);
    }
  ptr = realloc(ptr, size);
  if(ptr == NULL)
    {
      padico_fatal("memory allocation failed.\n");
    }
  puk_profile_mem_add(puk_profile_mem_get_global(), ptr, size);
  puk_profile_mem_add(profile, ptr, size);
  puk_profile_mem_entry_add(ptr, mod, func, file, line);
  return ptr;
}
#define padico_realloc(PTR, SIZE) (padico_realloc_func(PTR, SIZE, puk_mod_binary_get_profile(), padico_module_self_name(), __func__, __FILE__, __LINE__))

static inline void*padico_calloc_func(size_t n, size_t size, struct puk_profile_mem_s*profile, const char*mod,  const char*func, const char*file, int line)
{
  void*ptr = calloc(n, size);
  if(ptr == NULL)
    {
      padico_fatal("memory allocation failed.\n");
    }
  puk_profile_mem_add(puk_profile_mem_get_global(), ptr, n * size);
  puk_profile_mem_add(profile, ptr, n * size);
  puk_profile_mem_entry_add(ptr, mod, func, file, line);
  return ptr;
}
#define padico_calloc(N, SIZE) (padico_calloc_func(N, SIZE, puk_mod_binary_get_profile(), padico_module_self_name(), __func__, __FILE__, __LINE__))

static inline int padico_memalign_func(void**ptr, size_t alignment, size_t size, struct puk_profile_mem_s*profile, const char*mod,  const char*func, const char*file, int line)
{
  int rc = posix_memalign(ptr, alignment, size);
  puk_profile_mem_add(puk_profile_mem_get_global(), *ptr, size);
  puk_profile_mem_add(profile, *ptr, size);
  puk_profile_mem_entry_add(*ptr, mod, func, file, line);
  return rc;
}
#define padico_memalign(PTR, ALIGNMENT, SIZE) (padico_memalign_func(PTR, ALIGNMENT, SIZE, puk_mod_binary_get_profile(), padico_module_self_name(), __func__, __FILE__, __LINE__))

static inline char*padico_strdup_func(const char*s, struct puk_profile_mem_s*profile, const char*mod, const char*func, const char*file, int line)
{
  char*ptr = strdup(s);
  if(ptr == NULL)
    {
      padico_fatal("memory allocation failed.\n");
    }
  const ssize_t size = strlen(s);
  puk_profile_mem_add(puk_profile_mem_get_global(), ptr, size);
  puk_profile_mem_add(profile, ptr, size);
  puk_profile_mem_entry_add(ptr, mod, func, file, line);
  return ptr;
}

#define padico_strdup(S) (padico_strdup_func(S, puk_mod_binary_get_profile(), padico_module_self_name(), __func__, __FILE__, __LINE__))


static inline void padico_free_func(void*ptr, struct puk_profile_mem_s*profile, const char*mod, const char*func, const char*file, int line)
{
  puk_profile_mem_sub(puk_profile_mem_get_global(), ptr);
  puk_profile_mem_sub(profile, ptr);
  puk_profile_mem_entry_remove(ptr, mod, func, file, line);
  free(ptr);
}

#define padico_free(PTR) (padico_free_func(PTR, puk_mod_binary_get_profile(), padico_module_self_name(), __func__, __FILE__, __LINE__))

/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file XML parser and serializer for topology objects
 *  @ingroup Topology
 */

#include <Padico/Puk.h>
#include <Padico/Module.h>

#include "Topology.h"
#include "Topology-internals.h"

#include <netinet/in.h>
#include <arpa/inet.h>

PADICO_MODULE_HOOK(Topology)


/* ********************************************************* */
/* *** Topology serialization ****************************** */


static padico_string_t topo_hwaddr_serialize(padico_topo_hwaddr_t hwaddr)
{
  size_t hex_addr_len = hwaddr->len;
  char* hex_addr = puk_hex_encode(hwaddr->_hwaddr_storage, &hex_addr_len, NULL);
  padico_string_t s = padico_string_new();
  padico_string_catf(s, "  <Topology:hwaddr name=\"%s\" index=\"%d\">", hwaddr->name, hwaddr->index);
  padico_string_cat(s, hex_addr);
  padico_string_cat(s, "</Topology:hwaddr>\n");
  padico_free(hex_addr);
  return s;
}

static padico_string_t topo_hostname_serialize(topo_hostname_vect_t names)
{
  padico_string_t s = padico_string_new();
  topo_hostname_vect_itor_t p;
  puk_vect_foreach(p, topo_hostname, names)
    {
      padico_string_catf(s,"  <Topology:hostname resolvable=\"%d\">%s</Topology:hostname>\n",
                         (*p)->resolvable, (*p)->name);
    }
  return s;
}

static padico_string_t topo_binding_serialize(const struct topo_binding_s*b)
{
  padico_string_t s = padico_string_new();
  assert(b != NULL);
  padico_string_catf(s, "  <Topology:binding network=\"%s\">\n", b->network->network_id);
  topo_property_vect_itor_t p;
  puk_vect_foreach(p, topo_property, (struct topo_property_vect_s*)&b->props)
    {
      switch((*p)->kind)
        {
        case TOPO_PROPERTY_ADDR:
          {
            const struct topo_address_s*addr = &(*p)->content.ADDR.addr;
            size_t addr_size = b->network->addr_size;
            char*s_addr = puk_hex_encode(addr->_addr_storage, &addr_size, NULL);
            padico_string_catf(s, "    <Topology:property kind=\"addr\" family=\"%d\">%s</Topology:property>\n", addr->family, s_addr);
            padico_free(s_addr);
          }
          break;
        case TOPO_PROPERTY_HWADDR:
          {
            padico_string_catf(s, "    <Topology:property kind=\"hwaddr\" ifname=\"%s\" index=\"%d\"/>\n",
                               (*p)->content.HWADDR.ifname,
                               ((*p)->content.HWADDR.hwaddr != NULL) ? (*p)->content.HWADDR.hwaddr->index : -1);
          }
          break;
        case TOPO_PROPERTY_GW_ADDR:
          {
            const struct topo_address_s*gw_addr = &(*p)->content.GW_ADDR.addr;
            size_t addr_size = b->network->addr_size;
            char*s_addr = puk_hex_encode(gw_addr->_addr_storage, &addr_size, NULL);
            padico_string_catf(s, "    <Topology:property kind=\"gwaddr\" family=\"%d\">%s</Topology:property>\n", gw_addr->family, s_addr);
            padico_free(s_addr);
          }
          break;
        case TOPO_PROPERTY_GW_HWADDR:
          {
          }
          break;
        case TOPO_PROPERTY_HOSTNAME:
          {
            padico_string_catf(s, "    <Topology:property kind=\"hostname\" name=\"%s\"/>\n", (*p)->content.HOSTNAME.name);
          }
          break;
        case TOPO_PROPERTY_FILTER:
          {
            const padico_topo_filter_t filter = &(*p)->content.FILTER.filter;
            const char*proto_name = topo_filter_protocol_getname(filter->protocol);
            padico_string_catf(s, "    <Topology:property kind=\"filter\" policy=\"%s\" protocol=\"%s\" port=\"%d\"/>\n",
                               topo_filter_policy_getname(filter->policy), proto_name, filter->port);
          }
          break;
        case TOPO_PROPERTY_ROUTE:
          {
            const padico_topo_network_t dest = (*p)->content.ROUTE.dest;
            const struct topo_address_s*gw_addr =  &(*p)->content.ROUTE.gw;
            size_t addr_size = gw_addr->size;
            char*s_addr = puk_hex_encode(gw_addr->_addr_storage, &addr_size, NULL);
            padico_string_catf(s, "    <Topology:property kind=\"route\" network=\"%s\" gwfamily=\"%d\" gwaddr=\"%s\"/>\n",
                               padico_topo_network_getname(dest), gw_addr->family, s_addr);
          }
          break;
        case TOPO_PROPERTY_PERF:
          {
            const struct topo_perf_s perf = (*p)->content.PERF.perf;
            padico_string_catf(s, "    <Topology:property kind=\"perf\" lat=\"%d\" bw=\"%d\"/>\n",
                               perf.lat, perf.bw);
          }
          break;
        case TOPO_PROPERTY_INDEX:
          {
            padico_string_catf(s, "    <Topology:property kind=\"index\" index=\"%lld\"/>\n",
                               (long long)(*p)->content.INDEX.index);
          }
        default:
          break;
        }
    }

  padico_string_catf(s, "  </Topology:binding>\n");
  return s;
}

static padico_string_t na_bindings_serialize(padico_topo_binding_vect_t bindings)
{
  padico_string_t s = padico_string_new();
  padico_topo_binding_vect_itor_t i;
  assert(bindings != NULL);
  puk_vect_foreach(i, padico_topo_binding, bindings)
    {
      const struct topo_binding_s*b = *i;
      padico_string_t s1 = topo_binding_serialize(b);
      padico_string_cat(s, padico_string_get(s1));
      padico_string_delete(s1);
    }
  return s;
}

padico_string_t padico_topo_node_serialize(padico_topo_node_t node)
{
  padico_string_t s = padico_string_new();
  padico_string_t s_bindings = na_bindings_serialize(&node->bindings);
  padico_string_catf(s, "<Topology:node name=\"%s\" uuid=\"%s\" host=\"%s\" bootid=\"%s\">\n"
                     "%s"
                     "</Topology:node>\n",
                     node->canonical_nodename,
                     &node->uuid,
                     node->host->hostname,
                     node->boot_id,
                     padico_string_get(s_bindings));
  padico_string_delete(s_bindings);
  return s;
}

padico_string_t padico_topo_host_serialize(padico_topo_host_t host)
{
  padico_string_t s = padico_string_new();
  padico_string_t s_bindings = na_bindings_serialize(&host->bindings);
  padico_string_t s_names = topo_hostname_serialize(&host->hnames);
  padico_string_catf(s, "<Topology:host fqdn=\"%s\">\n", host->hostname);
  topo_hwaddr_vect_itor_t h;
  puk_vect_foreach(h, topo_hwaddr, &host->hwaddrs)
    {
      padico_string_t s_hwaddr = topo_hwaddr_serialize(*h);
      padico_string_cat(s, padico_string_get(s_hwaddr));
      padico_string_delete(s_hwaddr);
    }
  padico_string_catf(s, "%s", padico_string_get(s_names));
  padico_string_catf(s, "%s", padico_string_get(s_bindings));
  topo_property_vect_itor_t p;
  puk_vect_foreach(p, topo_property, (struct topo_property_vect_s*)&host->props)
    {
      if((*p)->kind == TOPO_PROPERTY_SERVICE)
        {
          const padico_topo_service_t service = &(*p)->content.SERVICE.service;
          padico_string_catf(s, "  <Topology:property kind=\"service\" port=\"%d\" name=\"%s\"/>\n",
                             service->port, service->name);
        }
      if((*p)->kind == TOPO_PROPERTY_MISC)
        {
          padico_string_catf(s, "  <Topology:property kind=\"misc\" label=\"%s\" value=\"%s\"/>\n",
                             (*p)->content.MISC.label, (*p)->content.MISC.value);
        }
    }
  padico_string_catf(s, "</Topology:host>\n");
  padico_string_delete(s_bindings);
  padico_string_delete(s_names);
  return s;
}

padico_string_t padico_topo_network_serialize(padico_topo_network_t network)
{
  padico_string_t s = padico_string_new();
  padico_string_t uplink = network->uplink ? topo_binding_serialize(network->uplink) : padico_string_new();
  padico_string_catf(s, "<Topology:network id=\"%s\" addr_size=\"%d\" scope=\"%s\" family=\"%d\">\n"
                     "%s",
                     network->network_id, network->addr_size,
                     (network->scope == PADICO_TOPO_SCOPE_NODE)?"node":"host",
                     network->family,
                     padico_string_get(uplink));
  if(network->perf.lat != 0 || network->perf.bw != 0)
    {
      padico_string_catf(s, "  <Topology:performance lat=\"%d\" bw=\"%d\"/>\n", network->perf.lat, network->perf.bw);
    }
  padico_string_catf(s, "</Topology:network>\n");

  padico_string_delete(uplink);
  return s;
}

padico_string_t padico_na_topology_serialize(padico_topo_binding_scope_t scope)
{
  const int dump_hosts = !!(scope & PADICO_TOPO_SCOPE_HOST);
  const int dump_nodes = !!(scope & PADICO_TOPO_SCOPE_NODE);
  padico_string_t s = padico_string_new();
  char*name;
  puk_hashtable_enumerator_t e;
  /* dump networks */
  padico_topo_network_vect_itor_t n;
  puk_vect_foreach(n, padico_topo_network, topology_get()->networks)
    {
      if( (((*n)->scope == PADICO_TOPO_SCOPE_HOST) && dump_hosts) ||
          (((*n)->scope == PADICO_TOPO_SCOPE_NODE) && dump_nodes))
        {
          padico_string_t s2 = padico_topo_network_serialize(*n);
          padico_string_cat(s, padico_string_get(s2));
          padico_string_delete(s2);
        }
    }
  /* dump hosts */
  if(dump_hosts)
    {
      e = puk_hashtable_enumerator_new(topology_get()->hostsbyname);
      padico_topo_host_t host;
      puk_hashtable_enumerator_next2(e, &name, &host);
      while(host)
        {
          if(strcmp(name, host->hostname) == 0)
            {
              padico_string_t s2 = padico_topo_host_serialize(host);
              padico_string_cat(s, padico_string_get(s2));
              padico_string_delete(s2);
            }
          puk_hashtable_enumerator_next2(e, &name, &host);
        }
      puk_hashtable_enumerator_delete(e);
    }
  /* dump nodes */
  if(dump_nodes)
    {
      e = puk_hashtable_enumerator_new(topology_get()->node_table);
      padico_topo_node_t node;
      puk_hashtable_enumerator_next2(e, NULL, &node);
      while(node)
        {
          padico_string_t s2 = padico_topo_node_serialize(node);
          padico_string_cat(s, padico_string_get(s2));
          padico_string_delete(s2);
          puk_hashtable_enumerator_next2(e, NULL, &node);
        }
      puk_hashtable_enumerator_delete(e);
    }
  return s;
}

/* ********************************************************* */
/* *** Topology parsing ************************************ */

static void topo_node_start_handler(puk_parse_entity_t e)
{
  const char*name       = puk_parse_getattr(e, "name");
  const char*boot_id    = puk_parse_getattr(e, "bootid");
  const char*hostname   = puk_parse_getattr(e, "host");
  const char*uuid       = puk_parse_getattr(e, "uuid");
  const char*byhost     = puk_parse_getattr(e, "byhost");
  padico_topo_node_t node = NULL;

  if(name != NULL)
    {
      node = padico_topo_getnodebyname(name);
    }
  if(node == NULL && uuid != NULL)
    {
      node = padico_topo_getnodebyuuid((padico_topo_uuid_t)uuid);
      if(name != NULL && node != NULL)
        {
          if(strcmp(name, node->canonical_nodename) != 0)
            {
              padico_fatal("uuid = %s colision; old node = %s; new node = %s\n",
                           uuid, name, node->canonical_nodename);
            }
        }
    }

  if(byhost == NULL && node == NULL)
    {
      if(hostname != NULL && uuid != NULL && boot_id != NULL)
        {
          padico_topo_host_t host = padico_topo_host_resolvebyname(hostname);
          node = topo_node_create(name, host, uuid, boot_id);
        }
      else
        {
          puk_parse_set_rc(e, padico_rc_error("Topology: unknown node.\n"));
          padico_fatal("cannot parse node description- name=%s; host=%s; uuid=%s; boot_id=%s\n",
                       name?:"<none>", hostname?:"<none>", uuid?:"<none>", boot_id?:"<none>");
        }
    }
  else if(node == NULL)
    {
      padico_topo_host_t host = padico_topo_host_resolvebyname(byhost);
      padico_topo_node_vect_t candidates = padico_topo_host_getnodes(host);
      if(padico_topo_node_vect_size(candidates) <= 0)
        padico_fatal("No valid candidate for host: %s\n", byhost);
      if(padico_topo_node_vect_size(candidates) > 1)
        padico_warning("Taking first of %i nodes candidates\n", padico_topo_node_vect_size(candidates));
      node = padico_topo_node_vect_at(candidates, 0);
    }
  puk_parse_set_content(e, node);
}
static void topo_node_end_handler(puk_parse_entity_t e)
{
  padico_topo_node_t node = puk_parse_get_content(e);
  puk_parse_entity_vect_t entities = puk_parse_get_contained(e);
  puk_parse_entity_vect_itor_t i;
  puk_vect_foreach(i, puk_parse_entity, entities)
    {
      if(puk_parse_is(*i, "Topology:binding"))
        {
          padico_topo_binding_t binding = puk_parse_get_content(*i);
          if(binding != NULL)
            {
              topo_binding_bind_node(&binding, node);
            }
        }
      else if(puk_parse_is(*i, "Topology:property"))
        {
          struct topo_property_s*prop = puk_parse_get_content(*i);
          padico_free(prop);
        }
    }
}

static void topo_host_start_handler(puk_parse_entity_t e)
{
  const char*fqdn = puk_parse_getattr(e, "fqdn");
  padico_topo_host_t host = topo_host_lookup(fqdn);
  if(!host)
    {
      host = topo_host_lookup(fqdn);
      if(!host)
        {
          host = padico_topo_host_create(fqdn);
        }
    }
  puk_parse_set_content(e, host);
}
static void topo_host_end_handler(puk_parse_entity_t e)
{
  padico_topo_host_t host = puk_parse_get_content(e);
  puk_parse_entity_vect_t entities = puk_parse_get_contained(e);
  puk_parse_entity_vect_itor_t i;
  puk_vect_foreach(i, puk_parse_entity, entities)
    {
      if(puk_parse_is(*i, "Topology:hwaddr"))
        {
          padico_topo_hwaddr_t hwaddr = puk_parse_get_content(*i);
          padico_topo_hwaddr_add(host, &hwaddr);
        }
      else if(puk_parse_is(*i, "Topology:binding"))
        {
          padico_topo_binding_t binding = puk_parse_get_content(*i);
          if(binding != NULL)
            topo_binding_bind_host(&binding, host);
        }
      else if(puk_parse_is(*i, "Topology:hostname"))
        {
          struct topo_hostname_s*hname = puk_parse_get_content(*i);
          padico_topo_host_add_name(host, hname->name, hname->resolvable);
          padico_free(hname->name);
          padico_free(hname);
        }
      else if(puk_parse_is(*i, "Topology:property"))
        {
          struct topo_property_s*prop = puk_parse_get_content(*i);
          if(prop != NULL)
            {
              if(prop->kind == TOPO_PROPERTY_SERVICE)
                {
                  padico_topo_host_add_service(host, prop->content.SERVICE.service.name,
                                               prop->content.SERVICE.service.port);
                  padico_free(prop);
                }
              else if(prop->kind == TOPO_PROPERTY_MISC)
                {
                  const char*label = prop->content.MISC.label;
                  const char*value = prop->content.MISC.value;
                  const char*found = padico_topo_host_getproperty(host, label);
                  if(!found || (found && (strcmp(found, value) != 0)))
                    {
                      topo_property_vect_push_back(&host->props, prop);
                    }
                  else
                    {
                      topo_property_destroy(prop);
                    }
                }
              else
                {
                  /* ignore unknown property */
                  padico_free(prop);
                }
            }
        }
    }
}

static void topo_property_end_handler(puk_parse_entity_t e)
{
  const char*s_kind = puk_parse_getattr(e, "kind");
  if(strcmp(s_kind, "addr") == 0)
    {
      const char*s_family    = puk_parse_getattr(e, "family");
      const int family = s_family ? atoi(s_family) : AF_UNSPEC;
      const char*s_addr = puk_parse_get_text(e);
      size_t addr_size = strlen(s_addr);
      struct topo_property_s*prop = padico_malloc(sizeof(struct topo_property_s) + addr_size);
      prop->kind = TOPO_PROPERTY_ADDR;
      prop->content.ADDR.addr.family = family;
      if((family == AF_INET) && (strchr(s_addr, '.') != NULL))
        {
          struct in_addr inaddr;
          inet_aton(s_addr, &inaddr);
          memcpy(prop->content.ADDR.addr._addr_storage, &inaddr, sizeof(struct in_addr));
          prop->content.ADDR.addr.size = sizeof(struct in_addr);
        }
      else
        {
          void*addr = puk_hex_decode(s_addr, &addr_size, NULL);
          memcpy(prop->content.ADDR.addr._addr_storage, addr, addr_size);
          padico_free(addr);
          prop->content.ADDR.addr.size = addr_size;
        }
      puk_parse_set_content(e, prop);
    }
  else if(strcmp(s_kind, "hwaddr") == 0)
    {
      const char*ifname = puk_parse_getattr(e, "ifname");
      struct topo_property_s*prop = padico_malloc(sizeof(struct topo_property_s));
      prop->kind = TOPO_PROPERTY_HWADDR;
      prop->content.HWADDR.hwaddr = NULL;
      strncpy(prop->content.HWADDR.ifname, ifname, TOPO_HWADDR_NAMSIZ);
      prop->content.HWADDR.ifname[TOPO_HWADDR_NAMSIZ - 1] = 0;
      puk_parse_set_content(e, prop);
    }
  else if(strcmp(s_kind, "service") == 0)
    {
      const char*s_name = puk_parse_getattr(e, "name");
      const char*s_port = puk_parse_getattr(e, "port");
      uint16_t service_port = (uint16_t)atoi(s_port);
      size_t name_len = strlen(s_name);
      struct topo_property_s*prop = padico_malloc(sizeof(struct topo_property_s) + name_len + 1);
      prop->kind = TOPO_PROPERTY_SERVICE;
      prop->content.SERVICE.service.port = service_port;
      memcpy(&prop->content.SERVICE.service.name[0], s_name, name_len);
      prop->content.SERVICE.service.name[name_len] = 0;
      puk_parse_set_content(e, prop);
    }
  else if(strcmp(s_kind, "filter") == 0)
    {
      const char*s_port = puk_parse_getattr(e, "port");
      const char*s_policy = puk_parse_getattr(e, "policy");
      const char*s_protocol = puk_parse_getattr(e, "protocol");
      const int protocol = topo_filter_protocol_getnumber(s_protocol);
      const uint16_t port = (uint16_t)atoi(s_port);
      const padico_topo_filter_policy_t policy = topo_filter_policy_getbyname(s_policy);
      if(policy == _PADICO_TOPO_FILTER_NONE)
        padico_fatal("unrecognized policy *%s*.\n", s_policy);
      struct topo_property_s*prop = padico_malloc(sizeof(struct topo_property_s));
      prop->kind = TOPO_PROPERTY_FILTER;
      prop->content.FILTER.filter.port = port;
      prop->content.FILTER.filter.protocol = protocol;
      prop->content.FILTER.filter.policy = policy;
      puk_parse_set_content(e, prop);
    }
  else if(strcmp(s_kind, "gwaddr") == 0)
    {
      const char*s_family = puk_parse_getattr(e, "family");
      const int family = s_family ? atoi(s_family) : AF_UNSPEC;
      const char*s_addr = puk_parse_get_text(e);
      size_t addr_size = strlen(s_addr);
      void*addr = puk_hex_decode(s_addr, &addr_size, NULL);
      struct topo_property_s*prop = padico_malloc(sizeof(struct topo_property_s) + addr_size);
      prop->kind = TOPO_PROPERTY_GW_ADDR;
      prop->content.GW_ADDR.addr.family = family;
      prop->content.GW_ADDR.addr.size = addr_size;
      memcpy(prop->content.GW_ADDR.addr._addr_storage, addr, addr_size);
      padico_free(addr);
      puk_parse_set_content(e, prop);
    }
  else if(strcmp(s_kind, "hostname") == 0)
    {
      const char*s_name = puk_parse_getattr(e, "name");
      struct topo_property_s*prop = padico_malloc(sizeof(struct topo_property_s));
      prop->kind = TOPO_PROPERTY_HOSTNAME;
      prop->content.HOSTNAME.name = padico_strdup(s_name);
      puk_parse_set_content(e, prop);
    }
  else if(strcmp(s_kind, "route") == 0)
    {
      const char*s_network = puk_parse_getattr(e, "network");
      const char*s_family = puk_parse_getattr(e, "gwfamily");
      const char*s_addr = puk_parse_getattr(e, "gwaddr");
      size_t addr_size = strlen(s_addr);
      void*addr = puk_hex_decode(s_addr, &addr_size, NULL);
      const int family = s_family ? atoi(s_family) : AF_UNSPEC;
      struct topo_property_s*prop = padico_malloc(sizeof(struct topo_property_s) + addr_size);
      prop->kind = TOPO_PROPERTY_ROUTE;
      padico_topo_network_t network = padico_topo_network_getbyid(s_network);
      if(network)
        {
          prop->content.ROUTE.dest = network;
        }
      prop->content.ROUTE.gw.family = family;
      prop->content.ROUTE.gw.size = addr_size;
      memcpy(prop->content.ROUTE.gw._addr_storage, addr, addr_size);
      puk_parse_set_content(e, prop);
    }
  else if(strcmp(s_kind, "misc") == 0)
    {
      const char*s_label = puk_parse_getattr(e, "label");
      const char*s_value = puk_parse_getattr(e, "value");
      struct topo_property_s*prop = padico_malloc(sizeof(struct topo_property_s));
      prop->kind = TOPO_PROPERTY_MISC;
      prop->content.MISC.label = padico_strdup(s_label);
      prop->content.MISC.value = padico_strdup(s_value);
      puk_parse_set_content(e, prop);
    }
  else if(strcmp(s_kind, "perf") == 0)
    {
      const char*s_lat = puk_parse_getattr(e, "lat");
      const char*s_bw = puk_parse_getattr(e, "bw");
      struct topo_property_s*prop = padico_malloc(sizeof(struct topo_property_s));
      prop->kind = TOPO_PROPERTY_PERF;
      prop->content.PERF.perf.lat = atoi(s_lat);
      prop->content.PERF.perf.bw = atoi(s_bw);
      puk_parse_set_content(e, prop);
    }
  else if(strcmp(s_kind, "index") == 0)
    {
      const char*s_index = puk_parse_getattr(e, "index");
      struct topo_property_s*prop = padico_malloc(sizeof(struct topo_property_s));
      prop->kind = TOPO_PROPERTY_INDEX;
      prop->content.INDEX.index = atoll(s_index);
      puk_parse_set_content(e, prop);
    }
  else
    {
      /* TODO */
    }
}

static void topo_binding_end_handler(puk_parse_entity_t e)
{
  const char*s_net  = puk_parse_getattr(e, "network");
  padico_topo_network_t network = padico_topo_network_getbyid(s_net);
  padico_topo_binding_t binding = NULL;
  if(network)
    {
      binding = topo_binding_create(network);
      puk_parse_entity_vect_t entities = puk_parse_get_contained(e);
      puk_parse_entity_vect_itor_t i;
      puk_vect_foreach(i, puk_parse_entity, entities)
        {
          if(puk_parse_is(*i, "Topology:property"))
            {
              struct topo_property_s*prop = puk_parse_get_content(*i);
              if(prop != NULL)
                {
                  topo_property_vect_push_back(&binding->props, prop);
                }
            }
        }
    }
  else
    {
      const char loopback[] = "Loopback";
      if(strncmp(s_net, loopback, strlen(loopback)) != 0)
        {
          padico_print("parsing binding to unknown network (%s)\n", s_net);
        }
      puk_parse_entity_vect_t entities = puk_parse_get_contained(e);
      puk_parse_entity_vect_itor_t i;
      puk_vect_foreach(i, puk_parse_entity, entities)
        {
          if(puk_parse_is(*i, "Topology:property"))
            {
              /* discard properties on unknown networks*/
              struct topo_property_s*prop = puk_parse_get_content(*i);
              topo_property_destroy(prop);
            }
        }
    }
  puk_parse_set_content(e, (void*)binding);
}

static void topo_network_end_handler(puk_parse_entity_t e)
{
  const char*id          = puk_parse_getattr(e, "id");
  const char*s_addr_size = puk_parse_getattr(e, "addr_size");
  const char*s_scope     = puk_parse_getattr(e, "scope");
  const char*s_family    = puk_parse_getattr(e, "family");
  padico_topo_network_t network = padico_topo_network_getbyid(id);
  if(!network)
    {
      const padico_topo_binding_scope_t scope =
        (strcmp(s_scope, "node") == 0)?PADICO_TOPO_SCOPE_NODE:PADICO_TOPO_SCOPE_HOST;
      const size_t addr_size = atoi(s_addr_size);
      const int family = s_family ? atoi(s_family) : AF_UNSPEC;
      network = padico_topo_network_create(id, scope, addr_size, family);
    }
      puk_parse_entity_vect_t entities = puk_parse_get_contained(e);
      puk_parse_entity_vect_itor_t i;
      puk_vect_foreach(i, puk_parse_entity, entities)
        {
          if(puk_parse_is(*i, "Topology:binding"))
            {
              padico_topo_binding_t uplink = puk_parse_get_content(*i);
              uplink->entity.network = network;
              uplink->entity.kind = TOPO_ENTITY_NETWORK;
              if(network->uplink == NULL)
                {
                  network->uplink = uplink;
                }
              else
                {
                  topo_binding_merge(network->uplink, uplink);
                }
            }
          else if(puk_parse_is(*i, "Topology:performance"))
            {
              struct topo_perf_s*perf = puk_parse_get_content(*i);
              network->perf.lat = perf->lat;
              network->perf.bw = perf->bw;
              padico_free(perf);
            }
          else if(puk_parse_is(*i, "Topology:property"))
            {
              struct topo_property_s*prop = puk_parse_get_content(*i);
              padico_free(prop);
            }
        }
  puk_parse_set_content(e, network);
}

static void topo_hwaddr_end_handler(puk_parse_entity_t e)
{
  const char*name = puk_parse_getattr(e, "name");
  const char*s_index = puk_parse_getattr(e, "index");
  const char*hex_addr = puk_parse_get_text(e);
  size_t addr_len = strlen(hex_addr);
  char*addr = puk_hex_decode(hex_addr, &addr_len, NULL);
  const int index = s_index?atoi(s_index):0;
  padico_topo_hwaddr_t hwaddr = padico_topo_hwaddr_create(name, index,
#ifdef ARPHRD_VOID
                                                   ARPHRD_VOID, /* TODO */
#else /* ARPHRD_VOID */
#warning ARPHRD_VOID not defined- using default ARPHRD_ETHER
                                                   ARPHRD_ETHER,
#endif /* ARPHRD_VOID */
                                                   addr_len, addr);
  padico_free(addr);
  puk_parse_set_content(e, (void*)hwaddr);
}

static void topo_hostname_end_handler(puk_parse_entity_t e)
{
  char*text = puk_parse_get_text(e);
  int resolvable = atoi(puk_parse_getattr(e, "resolvable")?:"1");
  struct topo_hostname_s*hname = padico_malloc(sizeof(struct topo_hostname_s));
  hname->name = padico_strdup(text);
  hname->resolvable = resolvable;
  puk_parse_set_content(e, hname);
}

static void topo_performance_end_handler(puk_parse_entity_t e)
{
  struct topo_perf_s*perf = padico_malloc(sizeof(struct topo_perf_s));
  const char*s_lat = puk_parse_getattr(e, "lat");
  const char*s_bw = puk_parse_getattr(e, "bw");
  perf->lat = atoi(s_lat);
  perf->bw = atoi(s_bw);
  puk_parse_set_content(e, perf);
}

static void topo_lstopo_end_handler(puk_parse_entity_t e)
{
  const char*s_scope = puk_parse_getattr(e, "scope");
  padico_topo_binding_scope_t scope = PADICO_TOPO_SCOPE_ALL;
  if(s_scope != NULL && strcmp(s_scope, "host") == 0)
    scope = PADICO_TOPO_SCOPE_HOST;
  else if(s_scope != NULL && strcmp(s_scope, "node") == 0)
    scope = PADICO_TOPO_SCOPE_NODE;
  padico_rc_t rc = padico_rc_msg("<Topology:topology>\n");
  padico_string_t topo = padico_na_topology_serialize(scope);
  rc = padico_rc_add(rc, padico_rc_msg(padico_string_get(topo)));
  rc = padico_rc_add(rc, padico_rc_msg("</Topology:topology>"));
  padico_string_delete(topo);
  puk_parse_set_rc(e, rc);
}

/* ********************************************************* */

__PUK_SYM_INTERNAL
void topology_parser_init(void)
{
  puk_xml_add_action((struct puk_tag_action_s)
                     {
                       .xml_tag        = "Topology:topology",
                       .start_handler  = NULL,
                       .end_handler    = NULL,
                       .required_level = PUK_TRUST_CONTROL,
                       .help           = "for internal use by Topology"
                      });
  puk_xml_add_action((struct puk_tag_action_s)
                     {
                       .xml_tag        = "Topology:node",
                       .start_handler  = &topo_node_start_handler,
                       .end_handler    = &topo_node_end_handler,
                       .required_level = PUK_TRUST_CONTROL,
                       .help           = "for internal use by Topology"
                      });
  puk_xml_add_action((struct puk_tag_action_s)
                     {
                       .xml_tag        = "Topology:host",
                       .start_handler  = &topo_host_start_handler,
                       .end_handler    = &topo_host_end_handler,
                       .required_level = PUK_TRUST_CONTROL,
                       .help           = "for internal use by Topology"
                      });
  puk_xml_add_action((struct puk_tag_action_s)
                     {
                       .xml_tag        = "Topology:network",
                       .start_handler  = NULL,
                       .end_handler    = &topo_network_end_handler,
                       .required_level = PUK_TRUST_CONTROL,
                       .help           = "for internal use by Topology"
                      });
  puk_xml_add_action((struct puk_tag_action_s)
                     {
                       .xml_tag        = "Topology:binding",
                       .start_handler  = NULL,
                       .end_handler    = &topo_binding_end_handler,
                       .required_level = PUK_TRUST_CONTROL,
                       .help           = "for internal use by Topology"
                      });
  puk_xml_add_action((struct puk_tag_action_s)
                     {
                       .xml_tag        = "Topology:property",
                       .start_handler  = NULL,
                       .end_handler    = &topo_property_end_handler,
                       .required_level = PUK_TRUST_CONTROL,
                       .help           = "for internal use by Topology"
                      });
  puk_xml_add_action((struct puk_tag_action_s)
                     {
                       .xml_tag        = "Topology:hwaddr",
                       .start_handler  = NULL,
                       .end_handler    = &topo_hwaddr_end_handler,
                       .required_level = PUK_TRUST_CONTROL,
                       .help           = "for internal use by Topology"
                      });
  puk_xml_add_action((struct puk_tag_action_s)
                     {
                       .xml_tag        = "Topology:hostname",
                       .start_handler  = NULL,
                       .end_handler    = &topo_hostname_end_handler,
                       .required_level = PUK_TRUST_CONTROL,
                       .help           = "for internal use by Topology"
                      });
  puk_xml_add_action((struct puk_tag_action_s)
                     {
                       .xml_tag        = "Topology:performance",
                       .start_handler  = NULL,
                       .end_handler    = &topo_performance_end_handler,
                       .required_level = PUK_TRUST_CONTROL,
                       .help           = "for internal use by Topology"
                      });
  puk_xml_add_action((struct puk_tag_action_s)
                     {
                       .xml_tag        = "lstopo",
                       .start_handler  = NULL,
                       .end_handler    = &topo_lstopo_end_handler,
                       .required_level = PUK_TRUST_OUTSIDE,
                       .help           = "list known topology"
                      });
}

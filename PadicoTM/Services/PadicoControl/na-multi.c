/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Padico NetAccess "multi" module driver- SPMDize a module on a Padico group.
 * @ingroup NetAccess
 */

#include <Padico/Puk.h>
#include <Padico/Module.h>
#include <Padico/PadicoTM.h>
#include <Padico/Topology.h>

#include "PadicoControl-internals.h"

PADICO_MODULE_HOOK(PadicoControl);

static void na_multi_notify(void*key);

static struct
{
  puk_component_t multi_loader, multi_proxy_loader;
} na_multi = { .multi_loader = NULL, .multi_proxy_loader = NULL };

/* *** Globals ********************************************* */

struct na_unit_multi_s
{
  padico_group_t     group;   /**< group the "multi" is running on */
  puk_mod_t          sub_mod; /**< module contained in this unit */
  padico_string_t    run_cmd;
  padico_group_req_t run_req;
  puk_job_t          compound_job;
};
PUK_AUTO_TYPE(na_unit_multi);

static padico_rc_t na_unit_multi_load  (puk_unit_t unit);
static padico_rc_t na_unit_multi_start (puk_unit_t unit, puk_job_t job);
static padico_rc_t na_unit_multi_unload(puk_unit_t unit);
static struct padico_loader_s na_multi_driver =
{
  .name   = "multi",
  .load   = &na_unit_multi_load,
  .start  = &na_unit_multi_start,
  .unload = &na_unit_multi_unload,
};

struct na_unit_multi_proxy_s
{
  padico_group_t group;
  puk_mod_t   sub_mod;
};
PUK_AUTO_TYPE(na_unit_multi_proxy);

static padico_rc_t na_unit_multi_proxy_load  (puk_unit_t unit);
static padico_rc_t na_unit_multi_proxy_start (puk_unit_t unit, puk_job_t job);
static padico_rc_t na_unit_multi_proxy_unload(puk_unit_t unit);
static struct padico_loader_s na_multi_proxy_driver =
{
  .name   = "multi-proxy",
  .load   = &na_unit_multi_proxy_load,
  .start  = &na_unit_multi_proxy_start,
  .unload = &na_unit_multi_proxy_unload,
};

void padico_control_multi_init(void)
{
  na_multi.multi_loader =
    puk_component_declare("PadicoLoader-multi",
                          puk_component_provides("PadicoLoader", "loader", &na_multi_driver));
  na_multi.multi_proxy_loader =
    puk_component_declare("PadicoLoader-multi-proxy",
                          puk_component_provides("PadicoLoader", "loader", &na_multi_proxy_driver));
}

void padico_control_multi_finalize(void)
{
  puk_component_destroy(na_multi.multi_loader);
  puk_component_destroy(na_multi.multi_proxy_loader);
}

/* *** na_unit_multi auto-type ***************************** */


void na_unit_multi_ctor(na_unit_multi_t um)
{
  um->sub_mod = NULL;
}
void na_unit_multi_dtor(na_unit_multi_t um)
{
  um->sub_mod = NULL;
}
void na_unit_multi_proxy_ctor(na_unit_multi_proxy_t um)
{
  um->sub_mod = NULL;
}
void na_unit_multi_proxy_dtor(na_unit_multi_proxy_t um)
{
  um->sub_mod = NULL;
}

/* *** Driver: multi *************************************** */

static padico_rc_t na_unit_multi_load(puk_unit_t unit)
{
  padico_rc_t rc = NULL;
  padico_out(50, "unit=%s; mod=%s- loading...\n", unit->name, puk_mod_getname(unit->mod));
  unit->driver_specific = NULL;
  const char*group_id = puk_mod_getattr(unit->mod, "GroupID");
  if(group_id == NULL)
    {
      rc = padico_rc_error("PadicoControl: [multi] GroupID undefined for mod %s", puk_mod_getname(unit->mod));
      return rc;
    }
  padico_group_t group = padico_group_lookup(group_id);
  if(group == NULL)
    {
      rc = padico_rc_error("PadicoControl: [multi] unknown group **%s**", group_id);
      return rc;
    }
  padico_string_t cmd = padico_string_new();
  padico_string_catf(cmd, "<defmod name=\"PROXY-%s\" driver=\"multi-proxy\">\n", unit->name);
  padico_string_catf(cmd, "  <attr label=\"GroupID\">%s</attr>\n", group_id);
  padico_string_catf(cmd, "  <unit>%s</unit>\n", unit->name);
  padico_string_catf(cmd, "</defmod>\n");
  padico_group_req_t req = padico_group_send_to_all(group, padico_string_get(cmd));
  rc = padico_group_wait_all(group, req);
  if(padico_rc_iserror(rc))
    {
      return rc;
    }

  /* load module */
  padico_string_printf(cmd, "<load>PROXY-%s</load>\n", unit->name);
  struct na_unit_multi_s*unit_multi = na_unit_multi_new();
  unit->driver_specific = unit_multi;
  puk_mod_t sub_mod;
  rc = padico_puk_mod_resolve(&sub_mod, unit->name);
  if(padico_rc_iserror(rc))
    {
      na_unit_multi_delete(unit_multi);
      unit->driver_specific = NULL;
      return rc;
    }
  unit_multi->sub_mod = sub_mod;
  unit_multi->group = group;
  puk_mod_set_ancestor(sub_mod, unit->mod);
  padico_out(70, "unit=%s- sending requests...\n", unit->name);
  req = padico_group_send_to_all(group, padico_string_get(cmd));
  padico_out(70, "unit=%s- loading local module...\n", unit->name);
  rc = padico_puk_mod_load(sub_mod);
  padico_out(70, "unit=%s- module loaded on local node\n", unit->name);
  rc = padico_group_wait_all(group, req);
  padico_out(70, "unit=%s- loaded on all nodes.\n", unit->name);
  padico_string_delete(cmd);
  if(padico_rc_iserror(rc))
    {
      na_unit_multi_delete(unit->driver_specific);
      unit->driver_specific = NULL;
    }
  return rc;
}

static padico_rc_t na_unit_multi_start(puk_unit_t unit, puk_job_t job)
{
  padico_rc_t rc = NULL;
  struct na_unit_multi_s*unit_multi = (struct na_unit_multi_s*)unit->driver_specific;
  padico_group_t group = unit_multi?(unit_multi->group):NULL;
  puk_job_t local_job = padico_malloc(sizeof(struct puk_job_s));
  int i;

  padico_out(50, "unit=%s; mod=%s- starting...\n", unit->name, puk_mod_getname(unit->mod));
  if(unit_multi == NULL)
    {
      rc = padico_rc_error("multi driver: cannot run unit **%s** (not properly initialized).",
                           unit->name);
      return rc;
    }
  local_job->argc = job->argc;
  local_job->argv = job->argv;
  local_job->rc = NULL;
  local_job->notify = &na_multi_notify;
  local_job->notify_key = unit;

  unit_multi->run_cmd = padico_string_new();
  unit_multi->compound_job = job;

  padico_string_printf(unit_multi->run_cmd, "<run>PROXY-%s", unit->name);
  for(i = 1; i < job->argc; i++) /* skip argv[0] (== module name) */
    {
      assert(job->argv[i] != NULL);
      padico_string_cat(unit_multi->run_cmd, "<arg>");
      padico_string_cat(unit_multi->run_cmd, job->argv[i]);
      padico_string_cat(unit_multi->run_cmd, "</arg>");
    }
  padico_string_cat(unit_multi->run_cmd, "</run>\n");
  padico_out(70, "unit=%s- sending requests...\n", unit->name);
  unit_multi->run_req = padico_group_send_to_all(group, padico_string_get(unit_multi->run_cmd));
  padico_out(70, "unit=%s- starting local module...\n", unit->name);
  rc = padico_puk_mod_start(unit_multi->sub_mod, local_job);
  padico_string_delete(unit_multi->run_cmd);
  return rc;
}

static padico_rc_t na_unit_multi_unload(puk_unit_t unit)
{
  padico_rc_t rc = NULL;
  padico_string_t cmd = padico_string_new();
  padico_out(50, "unit=%s; mod=%s- unloading...\n", unit->name, puk_mod_getname(unit->mod));
  if(unit->driver_specific == NULL)
    {
      rc = padico_rc_error("Unit was not properly initialized. Unload failed.\n");
    }
  else
    {
      padico_group_req_t req;
      padico_group_t group = ((struct na_unit_multi_s*)(unit->driver_specific))->group;
      padico_string_printf(cmd, "<unload>PROXY-%s</unload>\n", unit->name);
      req = padico_group_send_to_all(group, padico_string_get(cmd));

      puk_mod_t mod = puk_mod_getbyname(unit->name);
      if(mod == NULL)
        {
          padico_warning("unit %s already unloaded.\n", unit->name);
        }
      else
        {
          rc = padico_puk_mod_unload(((struct na_unit_multi_s*)(unit->driver_specific))->sub_mod);
          padico_out(50, "unit=%s; mod=%s- unloaded on local node; waiting other nodes.\n",
                     unit->name, puk_mod_getname(unit->mod));
        }
      rc = padico_group_wait_all(group, req);
      padico_string_delete(cmd);
      na_unit_multi_delete(unit->driver_specific);
      unit->driver_specific = NULL;
      padico_out(50, "unit=%s; mod=%s- done.\n", unit->name, puk_mod_getname(unit->mod));
    }
  return rc;
}


/* *** Driver: multi-proxy ********************************* */

static padico_rc_t na_unit_multi_proxy_load(puk_unit_t unit)
{
  padico_rc_t rc = NULL;
  puk_mod_t sub_mod;
  padico_out(50, "unit=%s; mod=%s- loading...\n", unit->name, puk_mod_getname(unit->mod));
  rc = padico_puk_mod_resolve(&sub_mod, unit->name);
  if(padico_rc_iserror(rc))
    {
      return rc;
    }
  unit->driver_specific = na_unit_multi_proxy_new();
  ((struct na_unit_multi_proxy_s*)(unit->driver_specific))->sub_mod = sub_mod;
  puk_mod_set_ancestor(sub_mod, unit->mod);
  rc = padico_puk_mod_load(sub_mod);
  padico_out(50, "unit=%s; mod=%s- done.\n", unit->name, puk_mod_getname(unit->mod));
  return rc;
}

static padico_rc_t na_unit_multi_proxy_start(puk_unit_t unit, puk_job_t proxy_job)
{
  padico_rc_t rc = NULL;
  puk_mod_t mod = ((struct na_unit_multi_proxy_s*)unit->driver_specific)->sub_mod;
  puk_job_t local_job = padico_malloc(sizeof(struct puk_job_s));
  /*  local_job->argc = proxy_job->argc;
      local_job->argv = proxy_job->argv;
      local_job->rc = NULL;
      local_job->notify = &na_unit_multi_proxy_notify;
      local_job->notify_key = unit;
  */
  memcpy(local_job, proxy_job, sizeof(struct puk_job_s));

  rc = padico_puk_mod_start(mod, local_job);
  return rc;
}

static padico_rc_t na_unit_multi_proxy_unload(puk_unit_t unit)
{
  puk_mod_t mod = puk_mod_getbyname(unit->name);
  padico_rc_t rc = NULL;
  if(mod == NULL)
    {
      padico_warning("unit %s already unloaded.\n", unit->name);
    }
  else
    {
      mod = ((struct na_unit_multi_proxy_s*)unit->driver_specific)->sub_mod;
      padico_out(50, "unit=%s- unloading...\n", unit->name);
      rc = padico_puk_mod_unload(mod);
    }
  na_unit_multi_proxy_delete(unit->driver_specific);
  unit->driver_specific = NULL;
  padico_out(50, "unit=%s- done.\n", unit->name);
  return rc;
}


/* ********************************************************* */



/* called when the local job of a multi job terminates
 */
static void na_multi_notify(void*key)
{
  puk_unit_t unit = (puk_unit_t)key;
  struct na_unit_multi_s*unit_multi = (struct na_unit_multi_s*)unit->driver_specific;
  padico_group_t group = unit_multi->group;

  padico_out(40, "unit=%s- local job finished on leader; waiting completion on other nodes.\n", unit->name);
  unit_multi->compound_job->rc = padico_group_wait_all(group, unit_multi->run_req);
  padico_out(40, "unit=%s- job finished on all nodes- notifying compound job.\n", unit->name);
  puk_job_notify(unit_multi->compound_job);
  padico_out(40, "unit=%s- multi job finished [mod=%s]\n", unit->name, puk_mod_getname(unit->mod));
}

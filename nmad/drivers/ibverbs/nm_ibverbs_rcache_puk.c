/*
 * NewMadeleine
 * Copyright (C) 2006-2023 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include "nm_ibverbs.h"
#include "nm_ibverbs_rcache.h"

#include <Padico/Module.h>

PADICO_MODULE_BUILTIN(nm_ibverbs_rcache_puk, NULL, NULL, NULL);

static void nm_ibverbs_puk_mem_init(struct nm_ibverbs_context_s*p_ibverbs_context);
static void nm_ibverbs_puk_mem_reg(struct nm_ibverbs_context_s*p_ibverbs_context, const void*ptr, size_t len,
                                   struct ibv_mr**pp_mr, void**pp_handle);
static void nm_ibverbs_puk_mem_unreg(struct nm_ibverbs_context_s*p_ibverbs_context, const void*ptr, size_t len,
                                     struct ibv_mr*p_mr, void*p_handle);

const struct nm_ibverbs_reg_driver_s nm_ibverbs_reg_driver_puk =
  {
    .init      = &nm_ibverbs_puk_mem_init,
    .mem_reg   = &nm_ibverbs_puk_mem_reg,
    .mem_unreg = &nm_ibverbs_puk_mem_unreg
  };


/* ** cache ************************************************ */

#ifdef MINI_PUKABI

#include <malloc.h>

/** a registered memory entry */
struct puk_mem_reg_s
{
  void*context;
  const void*ptr;
  size_t len;
  void*key;
  int refcount;
};
/** hook to register memory- returned value is used as key */
typedef void*(*puk_mem_register_t)  (void*context, const void*ptr, size_t len);
/** hook to unregister memory */
typedef void (*puk_mem_unregister_t)(void*context, const void*ptr, void*key);
/** sets handlers to register memory */
void puk_mem_set_handlers(puk_mem_register_t reg, puk_mem_unregister_t unreg);
/** asks for memory registration- does nothing if registration has been cached */
const struct puk_mem_reg_s*puk_mem_reg(void*context, const void*ptr, size_t len);
/** marks registration entry as 'unused' */
void puk_mem_unreg(const struct puk_mem_reg_s*reg);

static int puk_mem_cache_evict(void);


static void(*puk_mem_invalidate_hook)(void*ptr, size_t size) = NULL;

void puk_abi_mem_invalidate(void*ptr, int size)
{
  if(puk_mem_invalidate_hook)
    (*puk_mem_invalidate_hook)(ptr, size);
}


#define PUK_MEM_CACHE_SIZE 64

static struct
{
  struct puk_mem_reg_s cache[PUK_MEM_CACHE_SIZE];
  puk_mem_register_t   reg;
  puk_mem_unregister_t unreg;
} puk_mem =
  {
    .reg   = NULL,
    .unreg = NULL,
    .cache = { { .refcount = 0, .ptr = NULL, .len = 0 } }
  };


static void puk_mem_cache_flush(struct puk_mem_reg_s*r)
{
  assert(r->refcount == 0);
  (*puk_mem.unreg)(r->context, r->ptr, r->key);
  r->context = NULL;
  r->ptr = NULL;
  r->len = 0;
  r->key = 0;
  r->refcount = 0;
}

static int puk_mem_cache_evict(void)
{
  static unsigned int victim = 0;
  const int victim_first = victim;
  victim = (victim + 1) % PUK_MEM_CACHE_SIZE;
  struct puk_mem_reg_s*r = &puk_mem.cache[victim];
  while(((r->refcount > 0) || (r->ptr == NULL)) && victim != victim_first)
    {
      victim = (victim + 1) % PUK_MEM_CACHE_SIZE;
      r = &puk_mem.cache[victim];
    }
  if(r->refcount > 0)
    {
      return 0;
    }
  if(r->ptr != NULL)
    {
      puk_mem_cache_flush(r);
      return 1;
    }
  return 0;
}

static struct puk_mem_reg_s*puk_mem_slot_lookup(void)
{
  int i;
 retry:
  for(i = 0; i < PUK_MEM_CACHE_SIZE; i++)
    {
      struct puk_mem_reg_s*r = &puk_mem.cache[i];
      if(r->ptr == NULL && r->len == 0)
        {
          return r;
        }
    }
  int evicted = puk_mem_cache_evict();
  if(evicted)
    {
      goto retry;
    }
  return NULL;
}

extern void puk_mem_dump(void)
{
  int i;
  for(i = 0; i < PUK_MEM_CACHE_SIZE; i++)
    {
      struct puk_mem_reg_s*r = &puk_mem.cache[i];
      if(r->ptr != NULL)
        {
          puk_mem_cache_flush(r);
        }
    }
}

/** Invalidate memory registration before it is freed.
 */
extern void puk_mem_invalidate(void*ptr, size_t size)
{
  if(ptr)
    {
      int i;
      for(i = 0; i < PUK_MEM_CACHE_SIZE; i++)
        {
          struct puk_mem_reg_s*r = &puk_mem.cache[i];
          const void*rbegin = r->ptr;
          const void*rend   = r->ptr + r->len;
          const void*ibegin = ptr;
          const void*iend   = ptr + size;
          if( (r->ptr != NULL) &&
              ( ((ibegin >= rbegin) && (ibegin < rend)) ||
                ((iend >= rbegin) && (iend < rend)) ||
                ((ibegin >= rbegin) && (iend < rend)) ||
                ((rbegin >= ibegin) && (rend < iend)) ) )
            {
              if(r->refcount > 0)
                {
                  NM_FATAL("trying to invalidate registered memory still in use.\n");
                }
              puk_mem_cache_flush(r);
            }
        }
    }
}

/* ********************************************************* */

static inline void puk_spinlock_acquire(void)
{ }
static inline void puk_spinlock_release(void)
{ }

void puk_mem_set_handlers(puk_mem_register_t reg, puk_mem_unregister_t unreg)
{
  mallopt(M_TRIM_THRESHOLD, -1);
  puk_spinlock_acquire();
  puk_mem.reg = reg;
  puk_mem.unreg = unreg;
  puk_mem_invalidate_hook = &puk_mem_invalidate;
  puk_spinlock_release();
}
void puk_mem_unreg(const struct puk_mem_reg_s*_r)
{
  struct puk_mem_reg_s*r = (struct puk_mem_reg_s*)_r;
  puk_spinlock_acquire();
  r->refcount--;
  if(r->refcount < 0)
    {
      NM_FATAL("unbalanced registration detected.\n");
    }
  puk_spinlock_release();
}
const struct puk_mem_reg_s*puk_mem_reg(void*context, const void*ptr, size_t len)
{
  assert(ptr != NULL);
  puk_spinlock_acquire();
  int i;
  for(i = 0; i < PUK_MEM_CACHE_SIZE; i++)
    {
      struct puk_mem_reg_s*r = &puk_mem.cache[i];
      if(context == r->context && ptr >= r->ptr && (ptr + len <= r->ptr + r->len))
        {
          r->refcount++;
          puk_spinlock_release();
          return r;
        }
    }
  struct puk_mem_reg_s*r = puk_mem_slot_lookup();

  if(r)
    {
      r->context  = context;
      r->ptr      = ptr;
      r->len      = len;
      r->refcount = 1;
      r->key      = (*puk_mem.reg)(context, ptr, len);
    }
  else
    {
      padico_fatal("rcache buffer full.\n");
    }
  puk_spinlock_release();
  return r;
}

/* ********************************************************* */

static void (*minipukabi_old_free_hook)(void*ptr, const void*caller) = NULL;
static void minipukabi_free_hook(void*ptr, const void*caller);

static void minipukabi_install_hooks(void)
{
  minipukabi_old_free_hook = __free_hook;
  __free_hook = minipukabi_free_hook;
}
static void minipukabi_remove_hooks(void)
{
  __free_hook = minipukabi_free_hook;
}
static void minipukabi_free_hook(void*ptr, const void*caller)
{
  puk_abi_mem_invalidate(ptr, 1);
  minipukabi_remove_hooks();
  padico_free(ptr);
  minipukabi_install_hooks();
}

#endif /* PUKABI */



/* ********************************************************* */
/* ** puk mem */


static void*nm_ibverbs_puk_mem_reg_handler(void*context, const void*ptr, size_t len)
{
  struct nm_ibverbs_context_s*p_ibverbs_context = context;
  struct ibv_mr*mr = NULL;
 retry:
  mr = nm_ibverbs_reg_mr(p_ibverbs_context, (void*)ptr, len, NULL);
  if(mr == NULL)
    {
#ifdef MINI_PUKABI
      if(errno == ENOMEM)
        {
          int evicted = puk_mem_cache_evict();
          if(evicted)
            {
              NM_WARN("retrying memory registration.\n");
              goto retry;
            }
        }
#endif /* MINI_PUKABI */
      NM_FATAL("error %d (%s) while registering memory- ptr = %p; len = %d.\n",
               errno, strerror(errno), ptr, (int)len);
    }
  return mr;
}
static void nm_ibverbs_puk_mem_unreg_handler(void*context, const void*ptr, void*key)
{
  struct ibv_mr*mr = key;
  int rc = ibv_dereg_mr(mr);
  if(rc != 0)
    {
      NM_FATAL("error %d (%s) while deregistering memory.\n", rc, strerror(rc));
    }
}

static void nm_ibverbs_puk_mem_init(struct nm_ibverbs_context_s*p_ibverbs_context)
{
  padico_out(puk_verbose_notice, "nm_ibverbs_rcache_puk- init\n");
  /*  minipukabi_install_hooks(); */
  puk_mem_set_handlers(&nm_ibverbs_puk_mem_reg_handler, &nm_ibverbs_puk_mem_unreg_handler);
}

static void nm_ibverbs_puk_mem_reg(struct nm_ibverbs_context_s*p_ibverbs_context, const void*ptr, size_t len,
                                   struct ibv_mr**pp_mr, void**pp_handle)
{
  const struct puk_mem_reg_s*p_reg = puk_mem_reg(p_ibverbs_context, ptr, len);
  *pp_handle = (void*)p_reg;
  *pp_mr = p_reg->key;
}

static void nm_ibverbs_puk_mem_unreg(struct nm_ibverbs_context_s*p_ibverbs_context, const void*ptr, size_t len,
                                     struct ibv_mr*p_mr, void*p_handle)
{
  struct puk_mem_reg_s*p_reg = p_handle;
  puk_mem_unreg(p_reg);
}

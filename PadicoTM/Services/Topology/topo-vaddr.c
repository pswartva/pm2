/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Virtual address serialization
 * @ingroup Topology
 */

#include <Padico/Puk.h>
#include <Padico/Module.h>
#include "Topology-internals.h"

#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/un.h>


PADICO_MODULE_HOOK(Topology);


/* *** Address serialization ******************************* */

padico_string_t padico_vaddr_marshall(const struct padico_vaddr_s*vaddr)
{
  padico_string_t s = padico_string_new();
  switch(vaddr->addr->sa_family)
    {
    case AF_INET:
      padico_string_catf(s, "family=AF_INET address=%s:%d proto=%d\n",
                         inet_ntoa(((struct sockaddr_in*)vaddr->addr)->sin_addr),
                         ntohs(((struct sockaddr_in*)vaddr->addr)->sin_port),
                         vaddr->proto);
      break;
    case AF_INET6:
      {
        char addr6[INET6_ADDRSTRLEN];
        inet_ntop(AF_INET6, &((struct sockaddr_in6*)vaddr->addr)->sin6_addr, addr6, INET6_ADDRSTRLEN);
        padico_string_catf(s, "family=AF_INET6 address=%s/%d proto=%d\n",
                           addr6,
                           ntohs(((struct sockaddr_in6*)vaddr->addr)->sin6_port),
                           vaddr->proto);
      }
      break;
    case AF_UNIX:
      padico_string_catf(s, "family=AF_UNIX path=%s\n",
                         ((struct sockaddr_un*)vaddr->addr)->sun_path);
      break;
    case AF_PADICO:
      {
        padico_address_kind_t k = AF_PADICO_MAKE(((struct sockaddr_pa*)vaddr->addr)->spa_magic);
        const unsigned char*bytes = ((struct sockaddr_pa*)vaddr->addr)->spa_payload;
        int i;
        padico_string_catf(s, "family=AF_PADICO kind=%c%c%c%c bytes=#",
                           k.bytes[0], k.bytes[1], k.bytes[2], k.bytes[3]);
        for(i = 0; i < AF_PADICO_PAYLOAD; i++)
          {
            padico_string_catf(s, "%2X ", (int)bytes[i]);
          }
      }
      padico_string_catf(s, " #\n");
      break;
    default:
      padico_warning("cannot marshall address of unknown family %d\n",
                     vaddr->addr->sa_family);
      padico_string_catf(s, "family=unknown[%d]\n", vaddr->addr->sa_family);
    }
  return s;
}

padico_vaddr_t padico_vaddr_unmarshall(const char*s)
{
  padico_vaddr_t vaddr = padico_malloc(sizeof(struct padico_vaddr_s));
  char family[32];
  sscanf(s, "family=%s \n", family);
  if(strcmp(family, "AF_PADICO") == 0)
    {
      char k[4];
      struct sockaddr_pa*addrpa = padico_malloc(sizeof(struct sockaddr_pa));
      sscanf(s, "family=%*s kind=%4c\n", k);
      addrpa->spa_family = AF_PADICO;
      addrpa->spa_magic  = AF_PADICO_MAGIC(k[0], k[1], k[2], k[3]);
      {
        unsigned char*dest = addrpa->spa_payload;
        const char*source = strchr(s, '#') + 1;
        int i;
        for(i = 0; i < AF_PADICO_PAYLOAD; i++)
          {
            unsigned int c;
            sscanf(source, "%X ", &c);
            *dest = c;
            source += 3;
            dest++;
          }
      }
      vaddr->addr    = (struct sockaddr*)addrpa;
      vaddr->addrlen = sizeof(struct sockaddr_pa);
      vaddr->proto   = -1; /* 'proto' non-significant in PADICO address family */
    }
  else if(strcmp(family, "AF_INET") == 0)
    {
      int port;
      char inaddr[16]; /* 4 x 3 digits + 3 dots + null */
      int proto;
      struct sockaddr_in*addrin = padico_malloc(sizeof(struct sockaddr_in));
      sscanf(s, "family=%*s address=%[^:]:%d proto=%d\n",
             inaddr, &port, &proto);
      addrin->sin_family      = AF_INET;
      addrin->sin_port        = htons(port);
      addrin->sin_addr.s_addr = inet_addr(inaddr);
      vaddr->addr    = (struct sockaddr*)addrin;
      vaddr->addrlen = sizeof(struct sockaddr_in);
      vaddr->proto   = proto;
    }
  else if(strcmp(family, "AF_INET6") == 0)
    {
      int port;
      char in6addr[INET6_ADDRSTRLEN]; /* 8 x 4 digits + 7 dots + null */
      int proto;
      struct sockaddr_in6*addrin6 = padico_malloc(sizeof(struct sockaddr_in6));
      sscanf(s, "family=%*s address=%[^/]/%d proto=%d\n",
             in6addr, &port, &proto);
      addrin6->sin6_family   = AF_INET6;
      addrin6->sin6_port     = htons(port);
      addrin6->sin6_flowinfo = 0;
      addrin6->sin6_scope_id = 0;
      inet_pton(AF_INET6, in6addr, &addrin6->sin6_addr);
      vaddr->addr    = (struct sockaddr*)addrin6;
      vaddr->addrlen = sizeof(struct sockaddr_in);
      vaddr->proto   = proto;
    }
  else if(strcmp(family, "AF_UNIX") == 0)
    {
      struct sockaddr_un*addrun = padico_malloc(sizeof(struct sockaddr_un));
      addrun->sun_family = AF_UNIX;
      sscanf(s, "family=%*s path=%s\n", addrun->sun_path);
      vaddr->addr    = (struct sockaddr*)addrun;
      vaddr->addrlen = sizeof(struct sockaddr_un);
      vaddr->proto   = -1; /* 'proto' non-significant in UNIX address family */
    }
  else
    {
      padico_free(vaddr);
      vaddr = NULL;
      padico_warning("cannot unmarshal vaddr: %s\n", s);
    }
  return vaddr;
}

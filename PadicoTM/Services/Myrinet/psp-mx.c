/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file psp-mx.c
 */

#include <Padico/Puk.h>
#include <Padico/Module.h>
#include <Padico/Topology.h>
#include <Padico/PSP.h>
#include <Padico/PM2.h>
#include <Padico/AddrDB.h>
#include <myriexpress.h>

/* needed for mx__get_mapper_state */
#include <mx_internals/mx__driver_interface.h>

#ifdef PIOMAN
#include <pioman.h>
#endif  /* PIOMAN */

static int psp_mx_module_init(void);
static void psp_mx_module_finalize(void);

PADICO_MODULE_DECLARE(PSP_MX, psp_mx_module_init, NULL, psp_mx_module_finalize);


/** @defgroup PSP_MX component: PSP_MX- Direct messaging over Myrinet Express
 * @ingroup PadicoComponent */

/* ********************************************************* */
/* *** PadicoSimplePackets for MXs */

static void*psp_mx_instantiate(puk_instance_t ai, puk_context_t context);
static void psp_mx_destroy(void*_status);
static void psp_mx_init(void*_status, uint32_t tag, const char*label, size_t*h_size,
                        padico_psp_handler_t handler, void*key);
static void psp_mx_connect(void*_status, padico_topo_node_t remote_node);
static padico_psp_connection_t psp_mx_new_message(void*_status, padico_topo_node_t node, void**_sbuf);
static void psp_mx_pack(void*_status, padico_psp_connection_t _conn,
                         const char*bytes, size_t size);
static void psp_mx_end_message(void*_status, padico_psp_connection_t _conn);

/** instanciation facet for PSP-IBV
 * @ingroup PSP-IBV
 */
static const struct puk_component_driver_s psp_mx_component_driver =
  {
    .instantiate = &psp_mx_instantiate,
    .destroy     = &psp_mx_destroy
  };

/** 'SimplePackets' facet for PSP-IBV
* @ingroup PSP-IBV
*/
static const struct padico_psp_driver_s psp_mx_driver =
  {
    .init        = &psp_mx_init,
    .connect     = &psp_mx_connect,
    .new_message = &psp_mx_new_message,
    .pack        = &psp_mx_pack,
    .end_message = &psp_mx_end_message
  };

struct psp_mx_status_s
{
  padico_psp_slot_t slot;
  uint32_t tag;
  size_t h_size;
  mx_endpoint_addr_t addr;
  padico_topo_node_t peer;
  struct psp_mx_msghdr_s*sbuf;
  uint32_t self_id, peer_id;
  int hsent;
};

PUK_VECT_TYPE(psp_mx_conn, struct psp_mx_status_s*)

static struct
{
  mx_endpoint_t endpoint;
  mx_request_t req;
  marcel_t worker;
  volatile int power_on; /**< set to 0 to ask the worker to stop */
#ifdef PIOMAN
  struct piom_ltask ltask;
#endif
  struct padico_psp_directory_s slots;         /**< PSP demultiplexing */
  struct psp_mx_conn_vect_s conns; /**< known connections, indexed by peer_id */
  uint32_t next_id;
  uint64_t nic_id;
  padico_topo_network_t network;
  struct
  {
    const uint32_t board_num;
    const uint32_t endpoint_id;
    const uint32_t filter;
    const int timeout;
  } param;
} psp_mx = {
  .power_on = 0,
  .next_id = 1,
  .param.board_num   = 0, /* MX_ANY_NIC */
  .param.endpoint_id = 1, /* MX_ANY_ENDPOINT */
  .param.filter      = 0x12341234,
  .param.timeout     = 10
};

static void*psp_mx_worker(void*dummy);
#ifdef PIOMAN
static int psp_mx_fastpoll(void*_dummy);

static inline void psp_mx_piom_lock(void)
{
  piom_ltask_mask(&psp_mx.ltask);
}
static inline void psp_mx_piom_unlock(void)
{
  piom_ltask_unmask(&psp_mx.ltask);
}
#else
static inline void psp_mx_piom_lock(void)
{
}
static inline void psp_mx_piom_unlock(void)
{
}
#endif


#define PSP_MX_OP_NEWID  0x01
#define PSP_MX_OP_NEWMSG 0x02
#define PSP_MX_OP_HDR    0x04
#define PSP_MX_OP_PACK   0x08

#define PSP_MX_MASK_ID  0x00000000FFFFFF00
#define PSP_MX_MASK_OP  0x00000000000000FF
#define PSP_MX_MASK_TAG 0xFFFFFFFF00000000

#define PSP_MX_MAX_HDR 256

struct psp_mx_msghdr_s
{
  uint64_t id;
  uint32_t tag;
  char data[PSP_MX_MAX_HDR];
};

static inline uint64_t psp_mx_match_raw(uint64_t id, uint64_t tag, uint64_t op)
{
  return ( (op          & 0x00000000000000FF ) |
           ((id << 8  ) & 0x00000000FFFFFF00 ) |
           ((tag << 32) & 0xFFFFFFFF00000000 ) );
}
static inline uint64_t psp_mx_match(struct psp_mx_status_s*status, uint64_t op)
{
  const uint64_t id = status->peer_id;
  const uint64_t tag = (uint64_t)status->tag;
  return psp_mx_match_raw(id, tag, op);
}

/* ********************************************************* */
/* ** toolbox */

static inline void psp_mx_checkrc(int rc, const char*op)
{
  if(rc != MX_SUCCESS)
    {
      padico_fatal("error %d in %s: %s\n", rc, op, mx_strerror(rc));
    }
}

static inline void psp_mx_send(struct psp_mx_status_s*status, const void*bytes, size_t size, uint64_t match)
{
  mx_request_t req;
  mx_status_t stat;
  uint32_t res;
  mx_segment_t seg =
    {
      .segment_ptr = (void*)bytes,
      .segment_length = size
    };
  psp_mx_piom_lock();
  mx_isend(psp_mx.endpoint, &seg, 1, status->addr, match, NULL, &req);
  psp_mx_piom_unlock();
  /*  mx_wait(psp_mx.endpoint, &req, MX_INFINITE, &stat, &res); */
  int rc;
  do {
    psp_mx_piom_lock();
    rc = mx_test(psp_mx.endpoint, &req, &stat, &res);
    psp_mx_piom_unlock();
    if(!res)
      {
#ifndef PIOMAN
        marcel_yield();
#endif
      }
  } while (rc == MX_SUCCESS && !res);
}

static inline void psp_mx_recv(void*bytes, size_t size, uint64_t mask, uint64_t match)
{
  mx_request_t req;
  mx_status_t stat;
  uint32_t res;
  mx_segment_t seg =
    {
      .segment_ptr = bytes,
      .segment_length = size
    };
  psp_mx_piom_lock();
  mx_irecv(psp_mx.endpoint, &seg, 1, match, mask, NULL, &req);
  psp_mx_piom_unlock();
  /*
    mx_wait(psp_mx.endpoint, &req, MX_INFINITE, &stat, &res);
  */
  int rc;
  do {
    psp_mx_piom_lock();
    rc = mx_test(psp_mx.endpoint, &req, &stat, &res);
    psp_mx_piom_unlock();
    if(!res)
      {
#ifndef PIOMAN
        marcel_yield();
#endif
      }
  } while (rc == MX_SUCCESS && !res);
}

/* ********************************************************* */

static int psp_mx_module_init(void)
{
  padico_psp_directory_init(&psp_mx.slots);

  //  padico_setenv("MX_RCACHE", "0");
  padico_setenv("MX_MONOTHREAD", "1");

  mx_set_error_handler(MX_ERRORS_RETURN);
  int rc = mx_init();
  psp_mx_checkrc(rc, "init");

  rc = mx_open_endpoint(psp_mx.param.board_num, psp_mx.param.endpoint_id, psp_mx.param.filter,
                        NULL, 0, &psp_mx.endpoint);
  psp_mx_checkrc(rc, "open_endpoint");

  /* get the NIC id */
  rc = mx_board_number_to_nic_id(psp_mx.param.board_num, &psp_mx.nic_id);
  psp_mx_checkrc(rc, "board_number_to_nic_id");

  /* get the mapper MAC */
  mx_endpt_handle_t handle;
  rc = mx_open_board(psp_mx.param.board_num, &handle);
  psp_mx_checkrc(rc, "open_board");
  mx_mapper_state_t mapper = { .board_number = psp_mx.param.board_num, .iport = 0 };
  rc = mx__get_mapper_state(handle, &mapper);
  psp_mx_checkrc(rc, "get_mapper_state");

  /* decalre the MX network */
  padico_string_t s_net = padico_string_new();
  padico_string_printf(s_net, "MX-[%02X:%02X:%02X:%02X:%02X:%02X]",
                       mapper.mapper_mac[0], mapper.mapper_mac[1],
                       mapper.mapper_mac[2], mapper.mapper_mac[3],
                       mapper.mapper_mac[4], mapper.mapper_mac[5]);
  psp_mx.network = padico_topo_network_create(padico_string_get(s_net), PADICO_TOPO_SCOPE_HOST, sizeof(uint64_t), AF_UNSPEC);
  padico_topo_host_bind(padico_topo_getlocalhost(), psp_mx.network, &psp_mx.nic_id);

  padico_print("NIC id = %llX; net = %s\n", (unsigned long long)psp_mx.nic_id, padico_string_get(s_net));
#if 0 /* PIOMAN */
  piom_ltask_create(&psp_mx.ltask, &psp_mx_fastpoll, NULL, PIOM_LTASK_OPTION_REPEAT);
  piom_ltask_submit(&psp_mx.ltask);
#endif
  /* start polling */
  psp_mx.power_on = 1;
#ifndef PIOMAN
  marcel_create(&psp_mx.worker, NULL, &psp_mx_worker, NULL);

  puk_component_declare("PSP_MX",
                        puk_component_provides("PadicoComponent", "component", &psp_mx_component_driver),
                        puk_component_provides("PadicoSimplePackets", "psp", &psp_mx_driver));
#endif

  return 0;
}

static void psp_mx_module_finalize(void)
{
  psp_mx.power_on = 0;
  marcel_join(psp_mx.worker, NULL);
}

/* ********************************************************* */

static void*psp_mx_instantiate(puk_instance_t ai, puk_context_t context)
{
  struct psp_mx_status_s*status = padico_malloc(sizeof(struct psp_mx_status_s));
  status->slot = NULL;
  status->peer = NULL;
  return status;
}

static void psp_mx_destroy(void*_status)
{
  struct psp_mx_status_s*status = _status;
  padico_free(status);
}

/* ********************************************************* */

static void psp_mx_init(void*_status, uint32_t tag, const char*label, size_t*h_size,
                        padico_psp_handler_t handler, void*key)
{
  struct psp_mx_status_s*status = _status;
  *h_size += 32;
  if(status->slot != NULL)
    {
      padico_warning("init already done- tag=%x (%x); hsize=%d (%d)\n",
                     status->tag, tag, (int)status->h_size, (int)*h_size);
      return;
    }
  assert(*h_size <= PSP_MX_MAX_HDR);
  status->tag = tag;
  status->h_size = *h_size;
  status->sbuf = padico_malloc(sizeof(struct psp_mx_msghdr_s));
  status->self_id = psp_mx.next_id++;
  status->peer_id = 0;
  status->slot = padico_psp_slot_insert(&psp_mx.slots, handler, key, tag, status->h_size);
}

static void psp_mx_connect(void*_status, padico_topo_node_t remote_node)
{
  struct psp_mx_status_s*status = _status;
  uint64_t peer_nic;

  if(status->peer)
    {
      assert(status->peer == remote_node);
      padico_warning("already connected!\n");
      return;
    }

  padico_addrdb_publish(remote_node, "PSP_MX",
                        &psp_mx.param.board_num, sizeof(psp_mx.param.board_num),
                        &psp_mx.nic_id, sizeof(psp_mx.nic_id));
  padico_req_t req = padico_tm_req_new(NULL, NULL);
  padico_addrdb_get(remote_node, "PSP_MX",
                    &psp_mx.param.board_num, sizeof(psp_mx.param.board_num),
                    &peer_nic, sizeof(peer_nic), req);
  padico_tm_req_wait(req);
  padico_print("connecting to NIC ID %llX\n", (unsigned long long)peer_nic);

  int rc = mx_connect(psp_mx.endpoint, peer_nic,
                      psp_mx.param.endpoint_id, psp_mx.param.filter, psp_mx.param.timeout, &status->addr);
  psp_mx_checkrc(rc, "mx_connect");
  status->peer = remote_node;

  psp_mx_send(status, &status->self_id, sizeof(status->self_id), PSP_MX_OP_NEWID);
  psp_mx_recv(&status->peer_id, sizeof(status->peer_id), PSP_MX_MASK_OP, PSP_MX_OP_NEWID);
  psp_mx_conn_vect_put(&psp_mx.conns, status, status->peer_id);
}

static padico_psp_connection_t psp_mx_new_message(void*_status, padico_topo_node_t node, void**_sbuf)
{
  struct psp_mx_status_s*status = _status;
  status->hsent = 0;
  *_sbuf = &status->sbuf->data[0];
  return status;
}

static void psp_mx_hsend(struct psp_mx_status_s*status)
{
  if(!status->hsent)
    {
      status->sbuf->tag = status->tag;
      status->sbuf->id  = status->peer_id;
      psp_mx_send(status, status->sbuf, sizeof(struct psp_mx_msghdr_s) - (PSP_MX_MAX_HDR - status->h_size), psp_mx_match(status, PSP_MX_OP_NEWMSG));
      status->hsent = 1;
    }
}

static void psp_mx_pack(void*_status, padico_psp_connection_t _conn,
                        const char*bytes, size_t size)
{
  struct psp_mx_status_s*status = _status;
  psp_mx_hsend(status);
  psp_mx_send(status, bytes, size, psp_mx_match(status, PSP_MX_OP_PACK));
}

static void psp_mx_end_message(void*_status, padico_psp_connection_t _conn)
{
  struct psp_mx_status_s*status = _status;
  psp_mx_hsend(status);
}

static void psp_mx_pump(void*token, void*bytes, size_t size)
{
  struct psp_mx_msghdr_s*h = token;
  psp_mx_recv(bytes, size, PSP_MX_MASK_OP | PSP_MX_MASK_TAG | PSP_MX_MASK_ID,
              psp_mx_match_raw(h->id, h->tag, PSP_MX_OP_PACK));
}

#ifdef PIOMAN
static int psp_mx_fastpoll(void*_dummy)
{
  mx_status_t stat;
  uint32_t res;
  int rc = mx_test(psp_mx.endpoint, &psp_mx.req, &stat, &res);
  const int ready = (rc == MX_SUCCESS && res);
  if(ready)
    {
      piom_ltask_completed(&psp_mx.ltask);
    }
  return 0;
}
#endif

static void*psp_mx_worker(void*dummy)
{
  while(psp_mx.power_on)
    {
      struct psp_mx_msghdr_s h;
      mx_segment_t seg =
        {
          .segment_ptr = &h,
          .segment_length = sizeof(h)
        };
      psp_mx_piom_lock();
      mx_irecv(psp_mx.endpoint, &seg, 1, PSP_MX_OP_NEWMSG, PSP_MX_MASK_OP, NULL, &psp_mx.req);
      psp_mx_piom_unlock();
      /* mx_wait(psp_mx.endpoint, &req, MX_INFINITE, &stat, &res); */
#ifdef PIOMAN
      piom_ltask_wait(&psp_mx.ltask);
#else /* PIOMAN */
      int rc;
      mx_status_t stat;
      uint32_t res;
      do {
        rc = mx_test(psp_mx.endpoint, &psp_mx.req, &stat, &res);
        if(!res)
          marcel_yield();
        if(!psp_mx.power_on)
          return NULL;
      } while (rc == MX_SUCCESS && !res);
#endif /* PIOMAN */
      struct psp_mx_status_s*status = psp_mx_conn_vect_at(&psp_mx.conns, h.id);
      padico_psp_slot_t slot = padico_psp_slot_lookup(&psp_mx.slots, h.tag);
      (slot->handler)(&h.data[0], status->peer, slot->key, &psp_mx_pump, &h);
#ifdef PIOMAN
      piom_ltask_create(&psp_mx.ltask, &psp_mx_fastpoll, NULL, PIOM_LTASK_OPTION_REPEAT);
  piom_ltask_submit(&psp_mx.ltask);
#endif /* PIOMAN */
    }
  return NULL;
}

/*
 * NewMadeleine
 * Copyright (C) 2006-2023 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

static void allgather_check(int*rrbuf, int numtasks, int rank)
{
  int i;
  int success = 1;
  for(i = 0 ; i < numtasks ; i+=2)
    {
      if(rrbuf[i] != (i/2)*2  || rrbuf[i+1] != (i/2)+1)
        {
          success=0;
        }
    }
  if (success)
    {
      fprintf(stdout, "Success\n");
    }
  else
    {
      fprintf(stdout, "[%d] Error!!! Received: ", rank);
      for(i = 0 ; i < numtasks * 2 ; i++)
        {
          fprintf(stdout, "%d [ %d ] ", rrbuf[i], (i % 2 == 0) ? (i / 2) * 2 : (i / 2) + 1 );
        }
      fprintf(stdout, "\n");
    }
 }

int main(int argc, char**argv)
{
  int numtasks, rank;

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &numtasks);

  int *rrbuf = malloc(numtasks * 2 * sizeof(int));
  int sendarray[2];
  sendarray[0] = rank*2;
  sendarray[1] = rank+1;

  MPI_Request req;
  MPI_Iallgather(sendarray, 2, MPI_INT, rrbuf, 2, MPI_INT, MPI_COMM_WORLD, &req);
  MPI_Wait(&req, MPI_STATUS_IGNORE);
  allgather_check(rrbuf, numtasks, rank);

  memset(rrbuf, 0, numtasks * 2 * sizeof(int));
  rrbuf[0 + 2 * rank] = sendarray[0];
  rrbuf[1 + 2 * rank] = sendarray[1];
  MPI_Iallgather(MPI_IN_PLACE, 0, MPI_DATATYPE_NULL, rrbuf, 2, MPI_INT, MPI_COMM_WORLD, &req);
  MPI_Wait(&req, MPI_STATUS_IGNORE);
  allgather_check(rrbuf, numtasks, rank);

  free(rrbuf);
  fflush(stdout);
  MPI_Finalize();
  return 0;
}

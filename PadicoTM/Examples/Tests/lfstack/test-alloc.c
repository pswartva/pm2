#include <Padico/Puk.h>

PUK_LFSTACK_TYPE(test, void*_dummy; );

PUK_ALLOCATOR_TYPE(testalloc, void*);

int main(int argc, char**argv)
{
  test_lfstack_t stack;

  test_lfstack_init(&stack);

  int i;
  struct test_lfstack_cell_s*c = NULL;
  for(i = 0; i < 1000; i++)
    {
      c = padico_malloc(sizeof(struct test_lfstack_cell_s));
      test_lfstack_push(&stack, c);
    }
  for(i = 0; i < 1000; i++)
    {
      c = test_lfstack_pop(&stack);
      fprintf(stderr, "%p\n", c);
      free(c);
    }
  c = test_lfstack_pop(&stack);
  fprintf(stderr, "%p\n", c);
  test_lfstack_destroy(&stack);

  testalloc_allocator_t allocator = testalloc_allocator_new(16);
  for(i = 0; i < 1000; i++)
    {
      void*p = testalloc_malloc(allocator);
      testalloc_free(allocator, p);
    }
  testalloc_allocator_delete(allocator);

  return 0;
}

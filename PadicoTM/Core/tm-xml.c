/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief PadicoTM XML commands
 * @ingroup TaskManager
 */

#include <stdio.h>
#include <string.h>
#include <Padico/Puk.h>
#include <Padico/Module.h>

#include "TaskManager-internals.h"

PADICO_MODULE_HOOK(PadicoTM);

/* ********************************************************* */

static int padico_return_code = 0;
void padico_tm_update_rc(int rc)
{
  padico_return_code=rc;
}

/* ********************************************************* */

static void load_end_handler(puk_parse_entity_t e)
{
  const char*mod = puk_parse_get_text(e);
  padico_rc_t rc = NULL;
  if(mod)
    {
      rc = padico_tm_mod_action(PADICO_TM_MOD_LOAD, mod);
    }
  else
    {
      rc = padico_rc_error("NULL module name");
    }
  padico_out(60, "after tm_mod_action\n");
  puk_parse_set_rc(e, rc);
}
static const struct puk_tag_action_s load_action =
{
  .xml_tag        = "load",
  .start_handler  = NULL,
  .end_handler    = &load_end_handler,
  .required_level = PUK_TRUST_CONTROL
};

/* ********************************************************* */

static void run_end_handler(puk_parse_entity_t e)
{
  /* XML <arg> contains argv[1..argc-1], *not* argv[0].
   * We build a real argv (including argv[0]) here.
   */
  const char*mod = puk_parse_get_text(e);
  padico_rc_t rc;
  puk_parse_entity_vect_t c = puk_parse_get_contained(e);
  const int argc = puk_parse_entity_vect_size(c) + 1;
  char**argv = padico_malloc((argc + 1) * sizeof(char*));
  puk_parse_entity_vect_itor_t i;
  int j = 1;
  argv[0] = padico_strdup(mod);
  padico_out(50, "argc = %d, argv[0] = %s\n", argc, argv[0]);
  puk_vect_foreach(i, puk_parse_entity, c)
    {
      const char*arg = puk_parse_get_content(*i);
      padico_out(50, "argv[%d] = %s\n", j, arg);
      argv[j] = padico_strdup(arg);
      j++;
    }
  assert(j == argc);
  argv[argc] = NULL;
  if(mod)
    {
      rc = padico_tm_mod_action_args(PADICO_TM_MOD_RUN, mod, argc, argv);
    }
  else
    {
      rc = padico_rc_error("Cannot run: module name is NULL.");
    }
  puk_parse_set_rc(e, rc);
  for(j = 0; j < argc; j++)
    {
      padico_free(argv[j]);
    }
  padico_free(argv);
}
static const struct puk_tag_action_s run_action =
{
  .xml_tag        = "run",
  .start_handler  = NULL,
  .end_handler    = &run_end_handler,
  .required_level = PUK_TRUST_CONTROL
};

/* ********************************************************* */

static void arg_end_handler(puk_parse_entity_t e)
{
  puk_parse_set_content(e, padico_strdup(puk_parse_get_text(e)));
}
static const struct puk_tag_action_s arg_action =
{
  .xml_tag        = "arg",
  .start_handler  = NULL,
  .end_handler    = &arg_end_handler,
  .required_level = PUK_TRUST_CONTROL
};

/* ********************************************************* */

static void unload_end_handler(puk_parse_entity_t e)
{
  padico_rc_t rc = NULL;
  if(puk_parse_get_text(e) != NULL)
    {
      rc = padico_tm_mod_action(PADICO_TM_MOD_UNLOAD, puk_parse_get_text(e)); 
    }
  else
    {
      rc = padico_rc_error("NULL module name");
    }
  puk_parse_set_rc(e, rc);
}
static const struct puk_tag_action_s unload_action =
{
  .xml_tag        = "unload",
  .start_handler  = NULL,
  .end_handler    = &unload_end_handler,
  .required_level = PUK_TRUST_CONTROL
};

/* ********************************************************* */

static void kill_end_handler(puk_parse_entity_t e)
{
  const char*s_rc = puk_parse_getattr(e, "rc");
  int rc = (s_rc != NULL) ? atoi(s_rc) : padico_return_code;
  padico_print("kill- rc = %d.\n", rc);
  PUK_ABI_PROC_WRAP(exit)(rc); 
  /* padico_tm_exit(); */
}
static const struct puk_tag_action_s kill_action =
{
  .xml_tag        = "kill",
  .start_handler  = NULL,
  .end_handler    = &kill_end_handler,
  .required_level = PUK_TRUST_CONTROL
};

/* ********************************************************* */

static void threadtop_end_handler(puk_parse_entity_t e __attribute__((unused)))
{
  padico_tm_thread_top();
}
static const struct puk_tag_action_s threadtop_action =
{
  .xml_tag        = "marcel:top",
  .start_handler  = NULL,
  .end_handler    = &threadtop_end_handler,
  .required_level = PUK_TRUST_CONTROL
};

/* ********************************************************* */

static void setenv_start_handler(puk_parse_entity_t e)
{
  const char*label = puk_parse_getattr(e, "label");
  const char*value = puk_parse_getattr(e, "value");
  padico_setenv(label, value);
}

static const struct puk_tag_action_s setenv_action =
{
  .xml_tag        = "setenv",
  .start_handler  = &setenv_start_handler,
  .end_handler    = NULL,
  .required_level = PUK_TRUST_CONTROL
};

/* ********************************************************* */

static void getenv_start_handler(puk_parse_entity_t e)
{
  const char*label = puk_parse_getattr(e, "label");
  char*value = NULL;
  if(label != NULL)
    {
      value = (char*)padico_getenv(label);
    }
  puk_parse_set_rc(e, padico_rc_msg("%s", value));
}

static const struct puk_tag_action_s getenv_action =
{
  .xml_tag        = "getenv",
  .start_handler  = &getenv_start_handler,
  .end_handler    = NULL,
  .required_level = PUK_TRUST_CONTROL
};

/* ********************************************************* */

void padico_tm_xml_init(void)
{
  puk_xml_add_action(load_action);
  puk_xml_add_action(run_action);
  puk_xml_add_action(arg_action);
  puk_xml_add_action(unload_action);
  puk_xml_add_action(kill_action);
  puk_xml_add_action(setenv_action);
  puk_xml_add_action(getenv_action);
  puk_xml_add_action(threadtop_action);
}

void padico_tm_xml_finalize(void)
{
  puk_xml_delete_action("load");
  puk_xml_delete_action("run");
  puk_xml_delete_action("arg");
  puk_xml_delete_action("unload");
  puk_xml_delete_action("kill");
  puk_xml_delete_action("setenv");
  puk_xml_delete_action("getenv");
  puk_xml_delete_action("marcel:top");
}

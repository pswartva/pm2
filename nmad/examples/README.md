@page examplereadme README for examples

NewMadeleine examples
=====================

This directory contains examples, tests, and benchmarks.
Examples are organized as follows:

- `sendrecv/`    examples based on the classical sendrecv interface
- `launcher/`    examples of the 'launcher' interface used to open and close nmad sessions
- `rpc/`         examples using the RPC interface
- `mpi/`         MPI examples, when using MadMPI
- `pack/`        examples for the legacy 'pack' interface
- `piom/`        pioman-based examples, for progression and multithreading
- `lowlevel/`    various tests for low-level interfaces, not for end-users
- `benchmarks/`  a benchmark suite for various nmad features
- `common/`      common header shared across examples

Examples are automatically built and installed by the installation tool.

/*
 * NewMadeleine
 * Copyright (C) 2014-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include "nm_mpi_private.h"
#include "nm_mpi_private_syms.h"
#include <assert.h>
#include <errno.h>

#include <Padico/Module.h>
PADICO_MODULE_HOOK(MadMPI);

/* ********************************************************* */

NM_MPI_ALIAS(MPI_Dims_create,           mpi_dims_create);
NM_MPI_ALIAS(MPI_Cart_create,           mpi_cart_create);
NM_MPI_ALIAS(MPI_Cart_coords,           mpi_cart_coords)
NM_MPI_ALIAS(MPI_Cart_rank,             mpi_cart_rank);
NM_MPI_ALIAS(MPI_Cart_shift,            mpi_cart_shift);
NM_MPI_ALIAS(MPI_Cart_get,              mpi_cart_get);
NM_MPI_ALIAS(MPI_Cart_sub,              mpi_cart_sub);
NM_MPI_ALIAS(MPI_Topo_test,             mpi_topo_test);
NM_MPI_ALIAS(MPI_Cartdim_get,           mpi_cartdim_get);

/* ********************************************************* */

/* ** Cartesian topology *********************************** */

/** reverse compare function, to get list sorted in descending order */
static int nm_mpi_int_rcompare(const void*_a, const void*_b)
{
  const int*a = _a;
  const int*b = _b;
  if(*a == *b)
    return 0;
  else if(*a < *b)
    return 1;
  else
    return -1;
}

int mpi_dims_create(int nnodes, int ndims, int*dims)
{
  /* check parameters consistency */
  if(ndims <= 0)
    return NM_MPI_ERROR_WORLD(MPI_ERR_DIMS);
  int p = 1; /**< product of constrained dimensions */
  int d = 0; /**< number of free dimensions */
  int i;
  for(i = 0; i < ndims; i++)
    {
      if(dims[i] < 0)
        return NM_MPI_ERROR_WORLD(MPI_ERR_DIMS);
      else if(dims[i] > 0)
        p *= dims[i];
      else
        d++;
    }
  if(nnodes % p != 0)
    {
      return NM_MPI_ERROR_WORLD(MPI_ERR_DIMS);
    }
  /* init vector for free dimensions */
  const int r0 = nnodes / p; /**< product of free dimensions */
  int*r = padico_malloc(sizeof(int) * d);
  for(i = 0; i < d; i++)
    {
      r[i] = 1;
    }
  /* balance prime factors with greedy algorithm */
  int f = r0;
  int k;
  for(k = 2; k <= r0; k++)
    {
      while(f % k == 0)
        {
          /* find dimension with smallest factor */
          int v = r[0], m = 0;
          int j;
          for(j = 0; j < d; j++)
            {
              if(r[j] < v)
                {
                  v = r[j];
                  m = j;
                }
            }
          r[m] *= k;
          f /= k;
        }
    }
  assert(f == 1);
  qsort(r, d, sizeof(int), &nm_mpi_int_rcompare);
  /* copy dims to user buffer */
  int n = 0;
  for(i = 0; i < ndims; i++)
    {
      if(dims[i] == 0)
        {
          assert(n < d);
          dims[i] = r[n];
          n++;
        }
    }
  return MPI_SUCCESS;
}

int mpi_cart_create(MPI_Comm comm_old, int ndims, const int*dims, const int*periods, int reorder, MPI_Comm*newcomm)
{
  nm_mpi_communicator_t*p_old_comm = nm_mpi_communicator_get(comm_old);
  struct nm_mpi_cart_topology_s cart;
  if(ndims < 0)
    {
      return NM_MPI_ERROR_COMM(p_old_comm, MPI_ERR_DIMS);
    }
  cart.ndims = ndims;
  cart.dims = padico_malloc(ndims * sizeof(int));
  cart.periods = padico_malloc(ndims * sizeof(int));
  cart.size = 1;
  int d;
  for(d = 0; d < ndims; d++)
    {
      if(dims[d] <= 0)
        {
          return NM_MPI_ERROR_COMM(p_old_comm, MPI_ERR_DIMS);
        }
      cart.dims[d] = dims[d];
      cart.periods[d] = periods[d];
      cart.size *= dims[d];
    }
  if(cart.size > nm_comm_size(p_old_comm->p_nm_comm))
    return NM_MPI_ERROR_COMM(p_old_comm, MPI_ERR_TOPOLOGY);
  nm_group_t cart_group = nm_gate_vect_new();
  int i;
  for(i = 0; i < nm_comm_size(p_old_comm->p_nm_comm); i++)
    {
      nm_gate_vect_push_back(cart_group, nm_comm_get_gate(p_old_comm->p_nm_comm, i));
    }
  nm_mpi_communicator_t*p_new_comm = nm_mpi_communicator_alloc(nm_comm_create(p_old_comm->p_nm_comm, cart_group), p_old_comm->p_errhandler, NM_MPI_COMMUNICATOR_INTRA);
  p_new_comm->cart_topology = cart;
  p_new_comm->topo = NM_MPI_TOPOLOGY_CART;
  *newcomm = p_new_comm->id;
  nm_group_free(cart_group);
  return MPI_SUCCESS;
}

static void nm_mpi_cart_coords(const struct nm_mpi_cart_topology_s*p_cart, int rank, int ndims, int*coords)
{
  assert(p_cart->ndims > 0);
  int d;
  int nnodes = p_cart->size;
  for(d = 0; d < p_cart->ndims; d++)
    {
      nnodes    = nnodes / p_cart->dims[d];
      coords[d] = rank / nnodes;
      rank      = rank % nnodes;
    }
}

int mpi_cart_coords(MPI_Comm comm, int rank, int ndims, int*coords)
{
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  if(p_comm->topo != NM_MPI_TOPOLOGY_CART)
    {
      NM_MPI_WARNING("communicator %d doesn't have cartesian topology.\n", comm);
      return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_COMM);
    }
  struct nm_mpi_cart_topology_s*p_cart = &p_comm->cart_topology;
  if(ndims < p_cart->ndims)
    return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_DIMS);
  nm_mpi_cart_coords(p_cart, rank, ndims, coords);
  return MPI_SUCCESS;
}

int mpi_cart_rank(MPI_Comm comm, const int*coords, int*rank)
{
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  if(p_comm->topo != NM_MPI_TOPOLOGY_CART)
    {
      NM_MPI_WARNING("communicator %d doesn't have cartesian topology.\n", comm);
      return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_COMM);
    }
  struct nm_mpi_cart_topology_s*p_cart = &p_comm->cart_topology;
  const int ndims = p_cart->ndims;
  *rank = 0;
  int multiplier = 1;
  int i;
  for(i = ndims - 1; i >= 0; i--)
    {
      int coord = coords[i];
      if(p_cart->periods[i])
        {
          if(coord >= p_cart->dims[i])
            {
              coord = coord % p_cart->dims[i];
            }
          else if(coord < 0)
            {
              coord = coord % p_cart->dims[i];
              if(coord)
                coord = p_cart->dims[i] + coord;
            }
        }
      else
        if(coord >= p_cart->dims[i] || coord < 0)
          {
            *rank = MPI_PROC_NULL;
            return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_RANK);
          }
      *rank += multiplier * coord;
      multiplier *= p_cart->dims[i];
    }
  return MPI_SUCCESS;
}

int mpi_cart_shift(MPI_Comm comm, int direction, int displ, int*source, int*dest)
{
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  if(p_comm->topo != NM_MPI_TOPOLOGY_CART)
    {
      NM_MPI_WARNING("communicator %d doesn't have cartesian topology.\n", comm);
      return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_COMM);
    }
  struct nm_mpi_cart_topology_s*p_cart = &p_comm->cart_topology;
  const int rank = nm_comm_rank(p_comm->p_nm_comm);
  if(direction < 0 || direction >= p_cart->ndims)
    return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_ARG);
  if(displ == 0)
    return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_ARG);
  int*coords = padico_malloc(p_cart->ndims * sizeof(int));
  int err = mpi_cart_coords(comm, rank, p_cart->ndims, coords);
  if(err != MPI_SUCCESS)
    return NM_MPI_ERROR_COMM(p_comm, err);

  /* And now, compute the shitfs.
   * The periodic case is managed in mpi_cart_rank().
   * We have to check both boundaries (ie < 0 and >= dims[direction]), because
   * displ can be negative. */
  coords[direction] += displ;
  if(!p_cart->periods[direction] && (coords[direction] < 0 || coords[direction] >= p_cart->dims[direction]))
    {
      *dest = MPI_PROC_NULL;
    }
  else
    {
      mpi_cart_rank(comm, coords, dest);
    }
  coords[direction] -= 2 * displ;
  if(!p_cart->periods[direction] && (coords[direction] < 0 || coords[direction] >= p_cart->dims[direction]))
    {
      *source = MPI_PROC_NULL;
    }
  else
    {
      mpi_cart_rank(comm, coords, source);
    }

  padico_free(coords);
  return MPI_SUCCESS;
}

int mpi_cart_get(MPI_Comm comm, int maxdims, int*dims, int*periods, int*coords)
{
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  if(p_comm->topo != NM_MPI_TOPOLOGY_CART)
    {
      NM_MPI_WARNING("communicator %d doesn't have cartesian topology.\n", comm);
      return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_COMM);
    }
  const struct nm_mpi_cart_topology_s*p_cart = &p_comm->cart_topology;
  if(maxdims < p_cart->ndims)
    return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_DIMS);
  int i;
  for(i = 0; i < p_cart->ndims; i++)
    {
      dims[i] = p_cart->dims[i];
      periods[i] = p_cart->periods[i];
    }
  const int rank = nm_comm_rank(p_comm->p_nm_comm);
  mpi_cart_coords(comm, rank, maxdims, coords);
  return MPI_SUCCESS;
}

int mpi_cart_sub(MPI_Comm comm, const int*remain_dims, MPI_Comm*newcomm)
{
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  if(p_comm->topo != NM_MPI_TOPOLOGY_CART)
    {
      NM_MPI_WARNING("communicator %d doesn't have cartesian topology.\n", comm);
      return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_COMM);
    }
  const struct nm_mpi_cart_topology_s*p_cart = &p_comm->cart_topology;
  assert(p_cart->ndims > 0);
  int*coords = padico_malloc(sizeof(int) * p_cart->ndims);
  nm_mpi_cart_coords(p_cart, nm_comm_rank(p_comm->p_nm_comm), p_cart->ndims, coords);
  int color = 0, key = 0;
  int rdims = 0;
  int multiplier = 1;
  int i;
  for(i = p_cart->ndims - 1; i >= 0; i--)
    {
      const int c = coords[i];
      if(remain_dims[i])
        {
          /* this dimension remains */
          rdims++;
          key += c * multiplier;
        }
      else
        {
          /* this dimension is suppressed */
          color += c * multiplier;
        }
      multiplier *= p_cart->dims[i];
    }
  int rc = mpi_comm_split(comm, color, key, newcomm);
  if(rc != MPI_SUCCESS)
    {
      return rc;
    }
  int n = 0;
  struct nm_mpi_cart_topology_s cart;
  cart.ndims   = rdims;
  cart.dims    = padico_malloc(rdims * sizeof(int));
  cart.periods = padico_malloc(rdims * sizeof(int));
  cart.size    = 1;
  for(i = 0; i < p_cart->ndims; i++)
    {
      if(remain_dims[i])
        {
          cart.dims[n]    = p_cart->dims[i];
          cart.periods[n] = p_cart->periods[n];
          cart.size *= cart.dims[n];
          n++;
        }
    }
  nm_mpi_communicator_t*p_newcomm = nm_mpi_communicator_get(*newcomm);
  p_newcomm->cart_topology = cart;
  p_newcomm->topo = NM_MPI_TOPOLOGY_CART;
  return MPI_SUCCESS;
}

int mpi_topo_test(MPI_Comm comm, int*topo_type)
{
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  if(p_comm == NULL)
    return NM_MPI_ERROR_WORLD(MPI_ERR_COMM);
  *topo_type = p_comm->topo;
  return MPI_SUCCESS;
}

int mpi_cartdim_get(MPI_Comm comm, int*ndims)
{
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  if(p_comm == NULL)
    return NM_MPI_ERROR_WORLD(MPI_ERR_COMM);
  if(p_comm->topo != NM_MPI_TOPOLOGY_CART)
    {
      NM_MPI_WARNING("communicator %d doesn't have cartesian topology.\n", comm);
      return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_COMM);
    }
  const struct nm_mpi_cart_topology_s*p_cart = &p_comm->cart_topology;
  *ndims = p_cart->ndims;
  return MPI_SUCCESS;
}

/*
 * Pioman: a Generic I/O Manager
 * Copyright (C) 2001-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <assert.h>
#include <signal.h>
#include <time.h>
#include <sched.h>

#include "piom_private.h"

#include <Padico/Module.h>

PADICO_MODULE_HOOK(pioman);

//#define UNLOCK_QUEUES


/** block of static state for pioman ltask */
static struct
{
  int initialized;                 /**< refcounter on piom_ltask */
  int poll_level;                  /**< 0: low; 1: high (pending req needs progression) */
  piom_ltask_queue_t**all_queues;  /**< all ltask queues, for global polling */
  int n_queues;                    /**< size of array 'all_queues' */
} __piom_ltask =
  {
    .initialized = 0,
    .n_queues    = 0,
    .poll_level  = 0,
    .all_queues  = NULL
  };

/* ********************************************************* */

void piom_ltask_check_wait(void)
{
#ifdef PIOMAN_DEBUG
  struct piom_ltask_threadstate_s*p_threadstate = piom_ltask_getthreadstate();
  if((p_threadstate != NULL) && (p_threadstate->scheduling_queue != NULL))
    {
      fprintf(stderr, "pioman: deadlock detected- cannot wait while dispatching ltask.\n");
      abort();
    }
#endif /* PIOMAN_DEBUG */
}


/* Initialize a queue */
void piom_ltask_queue_init(piom_ltask_queue_t*queue, piom_topo_obj_t binding)
{
  queue->state = PIOM_LTASK_QUEUE_STATE_NONE;
  piom_ltask_dlfq_init(&queue->ltask_queue, 64);
  piom_ltask_lfqueue_init(&queue->submit_queue);
  piom_spin_init(&queue->mask);
  queue->binding = binding;
  __piom_ltask.all_queues = padico_realloc(__piom_ltask.all_queues, sizeof(struct piom_ltask_queue*)*(__piom_ltask.n_queues + 1));
  __piom_ltask.all_queues[__piom_ltask.n_queues] = queue;
#if defined(PIOMAN_TOPOLOGY_HWLOC)
  char s_binding[128];
  hwloc_obj_type_snprintf(s_binding, sizeof(s_binding), queue->binding, 0);
  PIOM_DISP("queue #%d on %s #%d\n", __piom_ltask.n_queues, s_binding, queue->binding->logical_index);
#endif /* PIOMAN_TOPOLOGY_HWLOC */
  __piom_ltask.n_queues++;
  queue->state = PIOM_LTASK_QUEUE_STATE_RUNNING;
}


/* Try to schedule a task from a given queue
 * @param full schedule all tasks from if 1, a single task if 0
 */
static int piom_ltask_queue_schedule(piom_ltask_queue_t*queue, int full)
{
  if(!piom_spin_trylock(&queue->mask))
    return -1;
  piom_spin_assert_locked(&queue->mask);
  const int hint1 = piom_ltask_dlfq_load(&queue->ltask_queue);
  const int hint2 = piom_ltask_lfqueue_load(&queue->submit_queue);
  const int hint = ((hint1 > 0)?hint1:1) + ((hint2 > 0)?hint2:1);
  const int count = (full != 0) ? hint : 1;
  int success = 0;
  int submit_queue_empty = 0;
  int i;
  for(i = 0; ((i < count) || !submit_queue_empty) && !success; i++)
    {
      int again = 0;
      struct piom_ltask*task = piom_ltask_lfqueue_dequeue_single_reader(&queue->submit_queue);
      if(task == NULL)
        {
          submit_queue_empty = 1;
          task = piom_ltask_dlfq_dequeue_single_reader(&queue->ltask_queue);
        }
      else
        {
          /* reset flag if it changed while running previous task */
          submit_queue_empty = 0;
        }
      piom_trace_remote_var(queue->binding, PIOM_TRACE_VAR_LTASKS,
                            (PIOM_MAX_LTASK + queue->ltask_queue._head - queue->ltask_queue._tail) % PIOM_MAX_LTASK);
      if(task == NULL)
        {
          if(queue->state == PIOM_LTASK_QUEUE_STATE_STOPPING)
            {
              /* no more task to run, set the queue as stopped */
              queue->state = PIOM_LTASK_QUEUE_STATE_STOPPED;
              return 0; /* purposely return while holding the lock */
            }
        }
      else
        {
          piom_trace_local_state(PIOM_TRACE_STATE_INIT);
          const int prestate = piom_ltask_state_set(task, PIOM_LTASK_STATE_SCHEDULED);
          if(prestate & PIOM_LTASK_STATE_READY)
            {
              /* wait is pending: poll */
              piom_trace_local_state(PIOM_TRACE_STATE_POLL);
              piom_ltask_preinvoke(queue);
              const int options = task->options;
#ifdef UNLOCK_QUEUES
              piom_spin_unlock(&queue->mask); /* unlock queue to schedule- will be re-acquired later */
#endif /* UNLOCK_QUEUES */
              (*task->func_ptr)(task->data_ptr);
              if(options & PIOM_LTASK_OPTION_DESTROY)
                {
                  /* ltask was destroyed by handler */
                  task = NULL;
                  piom_ltask_postinvoke();
                  success = 1;
                }
              else if((options & PIOM_LTASK_OPTION_REPEAT) && !(task->state & PIOM_LTASK_STATE_SUCCESS))
                {
                  piom_ltask_postinvoke();
                  /* If another thread is currently stopping the queue don't repost the task */
                  piom_ltask_state_unset(task, PIOM_LTASK_STATE_SCHEDULED);
                  if(queue->state == PIOM_LTASK_QUEUE_STATE_RUNNING)
                    {
                      again = 1;
                      assert(task->state != PIOM_LTASK_STATE_NONE);
                      assert(!(task->state & PIOM_LTASK_STATE_DESTROYED));
                      if(options & PIOM_LTASK_OPTION_BLOCKING)
                        {
                          int do_block = (task->blocking_delay == 0);
                          if(task->blocking_delay > 0)
                            {
                              struct timespec t;
                              clock_gettime(CLOCK_MONOTONIC, &t);
                              const long d_usec = (t.tv_sec - task->origin.tv_sec) * 1000000 +
                                (t.tv_nsec - task->origin.tv_nsec) / 1000;
                              if(d_usec > task->blocking_delay)
                                {
                                  do_block = 1;
                                }
                            }
                          if(do_block)
                            {
                              if(piom_ltask_submit_in_lwp(task) == 0)
                                again = 0;
                            }
                        }
                    }
                  else
                    {
                      PIOM_WARN("task %p is left uncompleted\n", task);
                    }
                }
              else
                {
                  task->state = PIOM_LTASK_STATE_TERMINATED;
                  piom_ltask_completed(task);
                  if((task->options & PIOM_LTASK_OPTION_NOWAIT) && (task->destructor))
                    {
                      (*task->destructor)(task);
                    }
                  task = NULL;
                  piom_ltask_postinvoke();
                  success = 1;
                }
#ifdef UNLOCK_QUEUES
              const int relock = piom_spin_trylock(&queue->mask);
              if(relock == 0)
                {
                  /* didn't get the lock again- enqueue as new, and abort loop */
                  if(again)
                    {
                      piom_ltask_lfqueue_enqueue(&queue->submit_queue, task);
                    }
                  return 0;
                }
#endif /* UNLOCK_QUEUES */
            }
          else if((prestate & PIOM_LTASK_STATE_SUCCESS) ||
                  (prestate & PIOM_LTASK_STATE_CANCELLED))
            {
              success = 1;
              piom_ltask_state_unset(task, PIOM_LTASK_STATE_SCHEDULED);
              piom_ltask_state_set(task, PIOM_LTASK_STATE_TERMINATED);
            }
          else if(!(prestate & PIOM_LTASK_STATE_READY))
            {
              /* task not ready; likely to be masked */
              piom_ltask_state_unset(task, PIOM_LTASK_STATE_SCHEDULED);
              if(prestate & PIOM_LTASK_STATE_MASKED)
                {
                  again = 1;
                }
            }
          else
            {
              PIOM_FATAL("wrong state 0x%4x for scheduled ltask.\n", prestate);
            }
          piom_trace_local_state(PIOM_TRACE_STATE_NONE);
          if(success)
            {
              piom_trace_local_event(PIOM_TRACE_EVENT_SUCCESS, task);
            }
          if(again)
            {
              int rc = -1;
              while(rc != 0)
                {
                  assert(task->state != PIOM_LTASK_STATE_NONE);
                  assert(!(task->state & PIOM_LTASK_STATE_DESTROYED));
                  rc = piom_ltask_dlfq_enqueue_single_writer(&queue->ltask_queue, task);
                  if(rc != 0)
                    {
                      const int size = piom_ltask_dlfq_size(&queue->ltask_queue);
                      const int load = piom_ltask_dlfq_load(&queue->ltask_queue);
                      if(size > 100)
                        {
                          PIOM_WARN("ltask polling queue full (%d / %d -> %d)\n", load, size, 2 * size);
                        }
                      piom_ltask_dlfq_resize(&queue->ltask_queue, 2 * size);
                      assert(piom_ltask_dlfq_load(&queue->ltask_queue) == load);
                    }
                }
            }
        }
    }
  piom_spin_unlock(&queue->mask);
  return 0;
}


void piom_ltask_queue_exit(piom_ltask_queue_t*queue)
{
  assert(queue->state != PIOM_LTASK_QUEUE_STATE_STOPPED);
  queue->state = PIOM_LTASK_QUEUE_STATE_STOPPING;
  /* empty the list of tasks */
  while(queue->state != PIOM_LTASK_QUEUE_STATE_STOPPED)
    {
      piom_ltask_queue_schedule(queue, 0);
    }
}

void piom_ltask_blocking_invoke(struct piom_ltask*task)
{
  const int options = task->options;
  assert(task != NULL);
  piom_ltask_state_set(task, PIOM_LTASK_STATE_BLOCKED | PIOM_LTASK_STATE_SCHEDULED);
  piom_ltask_preinvoke(PIOM_LTASK_QUEUE_INVALID);
  (*task->blocking_func)(task->data_ptr);
  if(!(options & PIOM_LTASK_OPTION_DESTROY))
    {
      piom_ltask_state_unset(task, PIOM_LTASK_STATE_BLOCKED | PIOM_LTASK_STATE_SCHEDULED);
      if((options & PIOM_LTASK_OPTION_REPEAT) && !(task->state & PIOM_LTASK_STATE_SUCCESS))
        {
          PIOM_WARN("task %p not completed after blocking call! Re-submitting.\n", task);

          abort();

          piom_ltask_queue_t*queue = task->queue;
          int rc = -1;
          do
            {
              rc = piom_ltask_lfqueue_enqueue(&queue->submit_queue, task);
            }
          while(rc != 0);
        }
      else
        {
          task->state = PIOM_LTASK_STATE_TERMINATED;
          piom_ltask_completed(task);
          if((task->options & PIOM_LTASK_OPTION_NOWAIT) && (task->destructor))
            {
              (*task->destructor)(task);
            }
        }
    }
  piom_ltask_postinvoke();
}

void piom_ltasks_init(void)
{
  if(__piom_ltask.initialized)
    return;

  /* ** create queues */
  piom_topo_init();

  /* ** Start polling */
  piom_ltask_backend_init();

  __piom_ltask.initialized++;
}

void piom_ltasks_exit(void)
{
  __piom_ltask.initialized--;
  assert(__piom_ltask.initialized >= 0);
  if(__piom_ltask.initialized == 0)
    {
      int i;
      for(i = 0; i < __piom_ltask.n_queues; i++)
        {
          piom_ltask_queue_exit(__piom_ltask.all_queues[i]);
        }
#ifdef PIOMAN_TRACE
      piom_trace_flush();
#endif /* PIOMAN_TRACE */
      piom_ltask_backend_exit();
      piom_topo_exit();
    }

}

int piom_ltask_test_activity(void)
{
  return (__piom_ltask.initialized != 0);
}

void piom_ltask_poll_level_set(int level)
{
  /* no need for atomic here- polling level is a hint */
  __piom_ltask.poll_level = level;
}

int piom_ltask_poll_level_get(void)
{
  return __piom_ltask.poll_level;
}

void piom_ltask_submit(struct piom_ltask*task)
{
  assert(task != NULL);
  assert(task->state == PIOM_LTASK_STATE_NONE || (task->state & PIOM_LTASK_STATE_TERMINATED) );
  task->state = PIOM_LTASK_STATE_NONE;
  piom_ltask_queue_t*queue = piom_topo_get_queue(task->binding);
  assert(queue != NULL);
  piom_trace_remote_event(queue->binding, PIOM_TRACE_EVENT_SUBMIT, task);
  if(queue->state == PIOM_LTASK_QUEUE_STATE_STOPPING)
    {
      PIOM_WARN("submitting a task (%p) to a queue (%p) that is being stopped\n", task, queue);
    }
  if(queue->state == PIOM_LTASK_QUEUE_STATE_STOPPED)
    {
      PIOM_FATAL("submitting a task (%p) to a queue (%p) that is already stopped.\n", task, queue);
    }
  task->state = PIOM_LTASK_STATE_READY;
  task->queue = queue;
  /* wait until a task is removed from the list */
  int loop = 0;
  int rc = -1;
  do
    {
#if defined(PIOMAN_MULTITHREAD)
      rc = piom_ltask_lfqueue_enqueue(&queue->submit_queue, task);
#else
      rc = piom_ltask_lfqueue_enqueue_single_writer(&queue->submit_queue, task);
#endif
      if(rc != 0)
        {
          if(queue->state == PIOM_LTASK_QUEUE_STATE_STOPPED)
            {
              break;
            }
          loop++;
          if(loop > 2)
            sched_yield();
          if(loop >= 10 && (loop % 10) == 0)
            {
              PIOM_WARN("ltask submit queue full; queue = %16p; loop = %d; submit queue = %d; task queue = %d / %d; state = 0x%x\n",
                        queue, loop,
                        piom_ltask_lfqueue_load(&queue->submit_queue),
                        piom_ltask_dlfq_load(&queue->ltask_queue),
                        piom_ltask_dlfq_size(&queue->ltask_queue),
                        queue->state);
            }
          int rc2 = piom_ltask_queue_schedule(queue, 1);
          if(loop > 10 && rc2 < 0)
            {
              piom_spin_lock(&queue->mask);
              piom_spin_unlock(&queue->mask);
            }
        }
    }
  while(rc != 0);
  piom_trace_remote_var(queue->binding, PIOM_TRACE_VAR_LTASKS, (PIOM_MAX_LTASK + queue->ltask_queue._head - queue->ltask_queue._tail) % PIOM_MAX_LTASK);
}

void piom_ltask_schedule(int point)
{
  if(__piom_ltask.initialized)
    {
      if(point == PIOM_POLL_POINT_BUSY)
        {
          /* busy wait- poll all queues, higher frequency for local queue */
          piom_ltask_queue_t*local_queue = piom_topo_get_queue(piom_topo_current_obj());
          int i;
          for(i = 0; i < __piom_ltask.n_queues; i++)
            {
              piom_ltask_queue_t*queue = __piom_ltask.all_queues[i];
              assert(queue != NULL);
              if(queue == local_queue)
                {
                  piom_ltask_queue_schedule(queue, 1);
                }
              else
                {
                  piom_ltask_queue_schedule(queue, 0);
                }
            }
        }
      else if(point == PIOM_POLL_POINT_TIMER || point == PIOM_POLL_POINT_FORCED)
        {
          /* timer poll & forced- poll all tasks from all queues */
          int i;
          for(i = 0; i < __piom_ltask.n_queues; i++)
            {
              piom_ltask_queue_t*queue = __piom_ltask.all_queues[i];
              assert(queue != NULL);
              piom_ltask_queue_schedule(queue, 1);
            }
        }
      else if(point == PIOM_POLL_POINT_SINGLE)
        {
          piom_ltask_queue_t*queue = piom_topo_get_queue(piom_topo_current_obj());
          assert(queue != NULL);
          piom_ltask_queue_schedule(queue, 0);
        }
      else
        {
          /* other points (idle, hook)- poll local queue */
          piom_ltask_queue_t*queue = piom_topo_get_queue(piom_topo_current_obj());
          assert(queue != NULL);
          piom_ltask_queue_schedule(queue, 1);
        }
    }
}

void piom_ltask_assert_noschedule(void)
{
#ifdef PIOMAN_DEBUG
  int i;
  for(i = 0; i < __piom_ltask.n_queues; i++)
    {
      piom_ltask_queue_t*queue = __piom_ltask.all_queues[i];
      assert(queue != NULL);
      piom_spin_assert_notlocked(&queue->mask);
    }
#endif /* PIOMAN_DEBUG */
}

void piom_ltask_wait_success(struct piom_ltask *task)
{
  assert(task->state != PIOM_LTASK_STATE_NONE);
  assert(!(task->options & PIOM_LTASK_OPTION_DESTROY));
  assert(!(task->options & PIOM_LTASK_OPTION_NOWAIT));
  piom_cond_wait(&task->done, PIOM_LTASK_STATE_SUCCESS);
}

void piom_ltask_wait(struct piom_ltask *task)
{
  piom_ltask_wait_success(task);
  while(!piom_ltask_state_test(task, PIOM_LTASK_STATE_TERMINATED))
    {
      piom_ltask_schedule(PIOM_POLL_POINT_BUSY);
    }
}

void piom_ltask_set_blocking(struct piom_ltask*task, piom_ltask_func_t func, int delay_usec)
{
  task->blocking_func = func;
  task->blocking_delay = delay_usec;
  task->options |= PIOM_LTASK_OPTION_BLOCKING;
  if(delay_usec > 0)
    clock_gettime(CLOCK_MONOTONIC, &task->origin);
}

void piom_ltask_mask(struct piom_ltask*ltask)
{
  piom_ltask_state_set(ltask, PIOM_LTASK_STATE_MASKED);
  piom_ltask_state_unset(ltask, PIOM_LTASK_STATE_READY);
  while(piom_ltask_state_test(ltask, PIOM_LTASK_STATE_SCHEDULED))
    {
      sched_yield();
    }
}

void piom_ltask_unmask(struct piom_ltask*ltask)
{
  assert(piom_ltask_state_test(ltask, PIOM_LTASK_STATE_READY) == 0);
  piom_ltask_state_set(ltask, PIOM_LTASK_STATE_READY);
  piom_ltask_state_unset(ltask, PIOM_LTASK_STATE_MASKED);
}

int piom_ltask_cancel_request(struct piom_ltask*ltask)
{
  piom_ltask_assert_noschedule();
  if(ltask->state == PIOM_LTASK_STATE_NONE)
    return 1;
  piom_ltask_state_set(ltask, PIOM_LTASK_STATE_CANCELLED);
  piom_ltask_state_unset(ltask, PIOM_LTASK_STATE_READY);
  while(piom_ltask_state_test(ltask, PIOM_LTASK_STATE_SCHEDULED))
    {
      sched_yield();
    }
  return !!(ltask->state & PIOM_LTASK_STATE_TERMINATED);
}

void piom_ltask_cancel_wait(struct piom_ltask*ltask)
{
  assert((ltask->state & PIOM_LTASK_STATE_CANCELLED) || (ltask->state & PIOM_LTASK_STATE_TERMINATED));
  struct piom_ltask_queue*queue = ltask->queue;
  if(queue)
    {
      while(!piom_ltask_state_test(ltask, PIOM_LTASK_STATE_TERMINATED))
        {
          piom_ltask_queue_schedule(queue, 0);
        }
    }
  ltask->state = PIOM_LTASK_STATE_TERMINATED;
}

void piom_ltask_cancel(struct piom_ltask*ltask)
{
  piom_ltask_cancel_request(ltask);
  piom_ltask_cancel_wait(ltask);
}

void piom_ltask_cancel_immediate(struct piom_ltask*ltask)
{
  /* ensure task is not scheduled */
  piom_ltask_mask(ltask);
  if(ltask->state & PIOM_LTASK_STATE_TERMINATED)
    {
      return;
    }
  assert(!(ltask->options & PIOM_LTASK_OPTION_DESTROY));
  struct piom_ltask_threadstate_s*p_threadstate = piom_ltask_getthreadstate();
  struct piom_ltask_queue*queue = ltask->queue;
  const int lock = (queue != p_threadstate->scheduling_queue);
  if(lock)
    {
      /* lock the queue only if ltask is not queued in the queue of
       * the current thread; don't try to lock the queue or it
       * will deadlock */
      piom_spin_lock(&queue->mask);
    }
  /* at this point, the ltask is not running, and its queue is
   * either locked or local; no ltask scheduling will happen here,
   * we can mangle with queues */

  int found = 0;
  /* try main ltask queue */
  const int s1 = piom_ltask_dlfq_load(&queue->ltask_queue);
  int i;
  for(i = 0; i < s1; i++)
    {
      struct piom_ltask*t = piom_ltask_dlfq_dequeue(&queue->ltask_queue);
      if(t == ltask)
        {
          found = 1;
          goto out;
        }
      else if(t != NULL)
        {
          piom_ltask_dlfq_enqueue(&queue->ltask_queue, t);
        }
    }

  /* try submit_queue */
  const int s2 = piom_ltask_lfqueue_load(&queue->submit_queue);
  for(i = 0; i < s2; i++)
    {
      /* in case concurrent threads submit ltasks, they are _after_ ltasks
       * already there when capturing s2 */
      struct piom_ltask*t = piom_ltask_lfqueue_dequeue(&queue->submit_queue);
      if(t == ltask)
        {
          found = 1;
          goto out;
        }
      else if(t != NULL)
        {
          piom_ltask_lfqueue_enqueue(&queue->submit_queue, t);
        }
    }
 out:
  if((!found) && !(ltask->state & PIOM_LTASK_STATE_TERMINATED))
    {
      PIOM_FATAL("cannot find ltask = %p in ltask queue", ltask);
    }
  if(lock)
    {
      piom_spin_unlock(&queue->mask);
    }
}

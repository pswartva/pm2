
#include <Padico/Puk.h>

#include <stdlib.h>
#include <stdio.h>
#include <time.h>

PUK_PRIO_TYPE(testp,
              int dummy;);

int main(int argc, char**argv)
{
  struct testp_prio_list_s prio_list;
  testp_prio_list_init(&prio_list);

  srand(time(NULL));
  int i;
  for(i = 0; i < 1000; i++)
    {
      struct testp_prio_cell_s*c = malloc(sizeof(struct testp_prio_cell_s));
      c->dummy = i;
      int k = rand();
      testp_prio_list_insert(&prio_list, k, c);
    }
  for(i = 0; i < 1001; i++)
    {
      struct testp_prio_cell_s*c = testp_prio_list_pop(&prio_list);
      if(c)
        fprintf(stderr, "# key = %d; i = %d\n", testp_prio_cell_get_prio(c), c->dummy);
      else
        fprintf(stderr, "# NULL\n");
    }
  return 0;
}

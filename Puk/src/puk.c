/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Puk main functions
 * @ingroup Puk
 * @note Puk is not a true PadicoTM module. It is declared as
 * a module for compatibility with padico_module_self()
 */

#include <stdlib.h>
#include <setjmp.h>
#include <time.h>
#include <execinfo.h>
#include <fnmatch.h>

#include "Module.h"
#include "Puk-internals.h"

PADICO_MODULE_HOOK(Puk); /* only a hook here.
                          * For some obscure reasons, the real
                          * declaration is in puk-mod.c
                          */

PADICO_MODULE_ATTR(profile, "PUK_PROFILE", "pattern of memory profiling vars to display; default is '' (empty); set to '*' to display all entries", string, "");

PADICO_MODULE_ATTR(profile_mem, "PUK_PROFILE_MEM", "detailed memory profiling; may miss some early allocations, before the attribute is processed", bool, 0);

PADICO_MODULE_ATTR(display_prof, "PUK_DISPLAY_PROFILE", "display all profiling variables at startup; default: false", bool, 0);

/******************************************************/

static struct
{
  void (*lock_acquire)(void);
  void (*lock_release)(void);
  int lock_level;
#ifdef PUK_ENABLE_PROFILE
  struct puk_profile_mem_s profile; /**< global memory profiling */
#endif /* PUK_ENABLE_PROFILE */
  struct puk_profile_var_vect_s profile_vars; /**< set of profiling variables */
  char  hostname[256];
  int initialized, finalized;
  puk_path_vect_t path; /**< list of roots to consider when resolving xml files, modules, traces, etc. */
} puk = { .lock_acquire = NULL, .lock_release = NULL, .lock_level = 0,
#ifdef PUK_ENABLE_PROFILE
          .profile = PUK_PROFILE_MEM_STATIC_INITIALIZER,
#endif /* PUK_ENABLE_PROFILE */
          .profile_vars = PUK_VECT_STATIC_INITIALIZER,
          .path = NULL,
          .initialized = 0,
          .finalized = 0
        };

#ifdef PUK_ENABLE_PROFILE

#define PUK_PROFILE_ENTRY_MEM_MAX 50000

static struct puk_profile_mem_entry_s
{
  const char*mod;
  const char*func;
  const char*file;
  int line;
  void*ptr;
} puk_profile_mem_entries[PUK_PROFILE_ENTRY_MEM_MAX];
static int puk_profile_mem_next = 0;

#endif /* PUK_ENABLE_PROFILE */

void puk_profile_mem_entry_add(void*ptr, const char*mod, const char*func, const char*file, int line)
{
#ifdef PUK_ENABLE_PROFILE
  if(!padico_module_attr_profile_mem_getvalue())
    return;
  if(puk_profile_mem_next == PUK_PROFILE_ENTRY_MEM_MAX)
    {
      puk_profile_mem_next++;
      padico_warning("memory profiling buffer full.\n");
    }
  if(puk_profile_mem_next >= PUK_PROFILE_ENTRY_MEM_MAX)
    return;
  struct puk_profile_mem_entry_s*entry = &puk_profile_mem_entries[puk_profile_mem_next];
  puk_profile_mem_next++;
  entry->mod  = mod;
  entry->func = func;
  entry->file = file;
  entry->line = line;
  entry->ptr  = ptr;
#endif /* PUK_ENABLE_PROFILE */
}

void puk_profile_mem_entry_remove(void*ptr, const char*mod, const char*func, const char*file, int line)
{
#ifdef PUK_ENABLE_PROFILE
  if(!padico_module_attr_profile_mem_getvalue())
    return;
  int i;
  for(i = 0; i < puk_profile_mem_next; i++)
    {
      if(puk_profile_mem_entries[i].ptr == ptr)
        {
          puk_profile_mem_entries[i].ptr = NULL;
          return;
        }
    }
  if(puk_profile_mem_next < PUK_PROFILE_ENTRY_MEM_MAX)
    {
      padico_warning("free()- ptr not found; ptr = %p; mod = %s; func = %s; file = %s:%d\n",
                     ptr, mod, func, file, line);
    }
#endif /* PUK_ENABLE_PROFILE */
}

extern void puk_profile_mem_entry_dump(void)
{
#ifdef PUK_ENABLE_PROFILE
  if(!padico_module_attr_profile_mem_getvalue())
    return;
  int i;
  for(i = 0; i < puk_profile_mem_next; i++)
    {
      struct puk_profile_mem_entry_s*entry = &puk_profile_mem_entries[i];
      if(entry->ptr != NULL)
        {
          fprintf(stderr, "# i = %d; ptr = %p; mod = %s; func = %s; file = %s:%d\n",
                  i,entry->ptr, entry->mod, entry->func, entry->file, entry->line);
        }
    }
#endif /* PUK_ENABLE_PROFILE */
}

/******************************************************/

void puk_lock_set_handlers(void(*_acquire)(void), void(*_release)(void))
{
  puk.lock_acquire = _acquire;
  puk.lock_release = _release;
}

void __puk_lock_acquire(const char*func __attribute__((unused)))
{
  if((!puk.initialized) && (!puk.finalized))
    {
      padico_fatal("called before init.\n");
    }
  if(puk.finalized)
    {
      padico_trace("called after finalize().\n");
    }
  padico_out(80, "%s locking...\n", func);
  if(puk.lock_acquire)
    (*puk.lock_acquire)();
  assert(puk.lock_level >= 0);
  puk.lock_level++;
  padico_out(80, "%s locked - level=%d\n", func, puk.lock_level);
}
void __puk_lock_release(const char*func __attribute__((unused)))
{
  padico_out(80, "%s unlocking- level=%d\n", func, puk.lock_level - 1);
  assert(puk.lock_level > 0);
  puk.lock_level--;
  if(puk.lock_release)
    (*puk.lock_release)();
}

/** backoff function for lock-free structures
 * @note not as inline in header, since nanosleep is not pure c99
 */
void puk_lfbackoff(int r)
{
  const int offset_retry = 20; /* number of retries without delay */
  const int max_delay_usec = 2; /* maximum delay in cas of waiting */
  if(r < offset_retry)
    return;
  int delay_nsec = (r < 32) ? (1 << (r - offset_retry)) * 10 : (max_delay_usec * 1000);
  if(delay_nsec > max_delay_usec * 1000)
    delay_nsec = max_delay_usec * 1000;
  struct timespec t = { .tv_sec = 0, .tv_nsec = delay_nsec };
  nanosleep(&t, NULL);
}

void puk_profile_var_add(struct puk_profile_var_s profile_var)
{
  struct puk_profile_var_vect_s*p_vars = puk_profile_get_vars();
  struct puk_profile_var_s*p_profile_var = padico_malloc(sizeof(struct puk_profile_var_s));
  *p_profile_var = profile_var;
  puk_profile_var_vect_push_back(p_vars, p_profile_var);
}

struct puk_profile_var_vect_s*puk_profile_get_vars(void)
{
  return &puk.profile_vars;
}

void puk_profile_dump(void)
{
  if(padico_module_attr_display_prof_getvalue())
    {
      padico_out(puk_verbose_always, "dumping profiling vars description\n");
      puk_profile_var_vect_itor_t i;
      puk_vect_foreach(i, puk_profile_var, puk_profile_get_vars())
        {
          struct puk_profile_var_s*p_pvar = *i;
          padico_out(puk_verbose_always, "%s = \"%s\" (%s)\n",
                     p_pvar->label, p_pvar->desc, puk_profile_type_name(p_pvar->type));
        }
    }
  if(!puk_profile_var_vect_empty(&puk.profile_vars))
    {
      const char*puk_profile = padico_module_attr_profile_getvalue();
      if((puk_profile == NULL) || (strcmp(puk_profile, "") == 0))
        {
          padico_out(puk_verbose_warning, "profiling is enabled but PUK_PROFILE=''; set PUK_PROFILE='*' to display all profiling counters, or set a filter; set PUK_DISPLAY_PROFILE=yes to display the description of all profiling counters.\n");
        }
      else
        {
          padico_out(puk_verbose_always, "dumping profiling vars with PUK_PROFILE=%s ____________\n", puk_profile);
          puk_profile_var_vect_itor_t i;
          puk_vect_foreach(i, puk_profile_var, puk_profile_get_vars())
            {
              struct puk_profile_var_s*p_pvar = *i;
              if(fnmatch(puk_profile, p_pvar->label, 0) == 0)
                {
                  switch(p_pvar->type)
                    {
                    case puk_profile_type_unsigned_long:
                      padico_out(puk_verbose_always, "%s = %lu\n", p_pvar->label, *p_pvar->value.as_unsigned_long);
                      break;
                    case puk_profile_type_unsigned_long_long:
                      padico_out(puk_verbose_always, "%s = %llu\n", p_pvar->label, *p_pvar->value.as_unsigned_long_long);
                      break;
                    case puk_profile_type_double:
                      padico_out(puk_verbose_always, "%s = %f\n", p_pvar->label, *p_pvar->value.as_double);
                      break;
                    }
                }
            }
        }
    }
  puk_profile_mem_entry_dump();
}


const char*puk_profile_type_name(enum puk_profile_type_e t)
{
  switch(t)
    {
    case puk_profile_type_unsigned_long:
      return "unsigned long";
      break;
    case puk_profile_type_unsigned_long_long:
      return "unsigned long long";
      break;
    case puk_profile_type_double:
      return "double";
      break;
    }
  return NULL;
}

ssize_t puk_profile_mem_get_total(void)
{
#ifdef PUK_ENABLE_PROFILE
  return puk.profile.total_mem;
#else /* PUK_ENABLE_PROFILE */
  padico_warning("cannot get total size of allocated memory without --enable-profile.\n");
  return -1;
#endif /* PUK_ENABLE_PROFILE */
}

ssize_t puk_profile_mem_get_malloc(void)
{
#ifdef PUK_ENABLE_PROFILE
  return puk.profile.num_malloc;
#else /* PUK_ENABLE_PROFILE */
  padico_warning("cannot get total number of malloc without --enable-profile.\n");
  return -1;
#endif /* PUK_ENABLE_PROFILE */
}

ssize_t puk_profile_mem_get_free(void)
{
#ifdef PUK_ENABLE_PROFILE
  return puk.profile.num_free;
#else /* PUK_ENABLE_PROFILE */
  padico_warning("cannot get total number of free without --enable-profile.\n");
  return -1;
#endif /* PUK_ENABLE_PROFILE */
}

struct puk_profile_mem_s*puk_profile_mem_get_global(void)
{
#ifdef PUK_ENABLE_PROFILE
  return &puk.profile;
#else
  return NULL;
#endif
}

void puk_profile_mem_set_pvars(struct puk_profile_mem_s*profile, const char*name)
{
#ifdef PUK_ENABLE_PROFILE
  assert(profile != NULL);
  padico_string_t pvar_name = padico_string_new();
  padico_string_printf(pvar_name, "%s.total_mem", name);
  puk_profile_var_def(unsigned_long_long, aggregate, &profile->total_mem,
                      "puk", padico_string_get(pvar_name), "total memory allocated through Puk wrappers for this module");
  padico_string_printf(pvar_name, "%s.max_mem", name);
  puk_profile_var_def(unsigned_long_long, highwatermark, &profile->max_mem,
                      "puk", padico_string_get(pvar_name), "maximum memory allocated through Puk wrappers for this module");
  padico_string_printf(pvar_name, "%s.num_malloc", name);
  puk_profile_var_def(unsigned_long_long, counter, &profile->num_malloc,
                      "puk",  padico_string_get(pvar_name), "total number of malloc() for this module");
  padico_string_printf(pvar_name, "%s.num_free", name);
  puk_profile_var_def(unsigned_long_long, counter, &profile->num_free,
                      "puk",  padico_string_get(pvar_name), "total number of free() for this module");
  padico_string_delete(pvar_name);
#endif /* PUK_ENABLE_PROFILE */
}

void puk_profile_mem_add(struct puk_profile_mem_s*profile, void*ptr, ssize_t size)
{
#ifdef PUK_ENABLE_PROFILE
  const ssize_t rsize = malloc_usable_size(ptr);
  assert(rsize >= size);
  if(profile != NULL)
    {
      __sync_fetch_and_add(&profile->total_mem, rsize + 2 * sizeof(void*)); /* add size of headers in glibc */
      __sync_fetch_and_add(&profile->num_malloc, 1);
      if(profile->total_mem > profile->max_mem)
        {
          profile->max_mem = profile->total_mem;
        }
    }
  else
    {
      padico_warning("puk_profile_mem_add()- module not defined for memory profiling\n");
      void*buffer[10];
      int nptrs = backtrace(buffer, 10);
      backtrace_symbols_fd(buffer, nptrs, STDERR_FILENO);
      fflush(stderr);
    }
#endif /* PUK_ENABLE_PROFILE */
}

void puk_profile_mem_sub(struct puk_profile_mem_s*profile, void*ptr)
{
#ifdef PUK_ENABLE_PROFILE
  const ssize_t oldsize = malloc_usable_size(ptr);
  assert(oldsize >= 0);
  if(profile != NULL)
    {
      __sync_fetch_and_sub(&profile->total_mem, oldsize + 2 * sizeof(void*));
      __sync_fetch_and_add(&profile->num_free, 1);
    }
  else
    {
      padico_warning("puk_profile_mem_sub()- module not defined for memory profiling\n");
      void*buffer[10];
      int nptrs = backtrace(buffer, 10);
      backtrace_symbols_fd(buffer, nptrs, STDERR_FILENO);
      fflush(stderr);
    }
#endif /* PUK_ENABLE_PROFILE */
}

/******************************************************/

int padico_puk_initialized(void)
{
  return puk.initialized;
}

int padico_puk_finalized(void)
{
  return puk.finalized;
}

void padico_puk_init(void)
{
  if(puk.initialized)
    return; /* Puk already initialized */
  if(puk.finalized)
    padico_fatal("trying to initialize Puk after shutdown.\n");
  if(puk_verbose_level() >= puk_verbose_info)
    {
      /* welcome banner */
      fprintf(stderr, "# Welcome to " PUK_BUILD_STRING " %s\n", puk_getlibfile());
    }
  if(gethostname(puk.hostname, 256))
    padico_fatal("cannot get gethostname()\n");
  puk.path = puk_path_vect_new();

  const char*symprefix = puk_getsymprefix();
  if(symprefix != NULL)
    {
      fprintf(stderr, "# Puk: symbol prefix = %s; running in sim mode.\n", symprefix);
      const char*simroot = getenv("PUK_MULT_ROOT");
      if(simroot == NULL)
        {
          fprintf(stderr, "# cannot find environment variable PUK_MULT_ROOT\n");
          abort();
        }
      padico_string_t root = padico_string_new();
      padico_string_printf(root, "%s/%s", simroot, symprefix);
      puk_path_vect_push_back(puk.path, padico_strdup(padico_string_get(root)));
      fprintf(stderr, "# Puk: set root = %s\n", padico_string_get(root));
      padico_string_delete(root);
      strcat(puk.hostname, "-");
      strcat(puk.hostname, symprefix);
    }
  else
    {
      puk_path_vect_push_back(puk.path, padico_strdup(PUK_ROOT));
    }
#ifdef PUK_ENABLE_PROFILE
  puk_profile_var_def(unsigned_long_long, aggregate, &puk.profile.total_mem,
                      "puk", "puk.total_mem", "total memory allocated through Puk wrappers");
  puk_profile_var_def(unsigned_long_long, counter, &puk.profile.num_malloc,
                      "puk", "puk.num_malloc", "total number of malloc()");
  puk_profile_var_def(unsigned_long_long, counter, &puk.profile.num_free,
                      "puk", "puk.num_free", "total number of free()");
#endif /* PUK_ENABLE_PROFILE */
  puk.initialized = 1;

  /* low-level init */
  puk_timing_stamp();           /* force lazy init */
  padico_puk_xml_init();        /* XML parser */
  padico_puk_debug_init();      /* debugging tools */
  padico_puk_components_init(); /* component framework */
  padico_puk_builtin_init();    /* driver: 'builtin' */
  padico_puk_mod_init();        /* module management */
  padico_puk_pkg_init();        /* driver: 'pkg' */
  padico_puk_monitor_init();    /* monitoring functions ('lsmod' and the likes) */
  padico_puk_binary_init();     /* driver: 'binary' */
  padico_puk_opt_init();
  padico_out(puk_verbose_info, "timing source = %s\n", PUK_TIMING_METHOD);
}

void padico_puk_add_path(const char*path)
{
  puk_path_vect_itor_t i;
  puk_lock_acquire();
  puk_vect_foreach(i, puk_path, puk.path)
    {
      if(strcmp(*i, path) == 0)
        goto already;
    }
  puk_path_vect_push_back(puk.path, padico_strdup(path));
 already:
  puk_lock_release();
}

puk_path_vect_t puk_path_get(void)
{
  return puk.path;
}

void padico_puk_start(void)
{
  puk_mod_t startup_mod = NULL;
  padico_rc_t rc = NULL;
  /* load the startup module */
  const char* startup_name = "PadicoTM";
  padico_print("loading startup module '%s'.\n", startup_name);
  rc = padico_puk_mod_resolve(&startup_mod, startup_name);
  if(padico_rc_iserror(rc))
    {
      padico_rc_show(rc);
      padico_fatal("startup module '%s' not found.\n", startup_name);
    }
  rc = padico_puk_mod_load(startup_mod);
  if(padico_rc_iserror(rc))
    {
      padico_rc_show(rc);
      padico_fatal("error while loading startup module.\n");
    }
  /* The startup mod is supposed to take the control of the process.
   * We never reach here.
   */
  padico_fatal("invalid startup module. It should never return from init!\n");
}

void padico_puk_shutdown(void)
{
  if(puk.initialized && !puk.finalized)
    {
      puk.finalized = 1;
      padico_print("shutdown.\n");
      padico_puk_mod_finalize();
      padico_puk_builtin_finalize();
      padico_puk_binary_finalize();
      padico_puk_components_finalize();
      padico_puk_debug_finalize();
      padico_puk_xml_finalize();
      puk_path_vect_itor_t i;
      puk_vect_foreach(i, puk_path, puk.path)
        {
          const char*p = *i;
          padico_free((void*)p);
        }
      puk_path_vect_delete(puk.path);
      puk_profile_var_vect_itor_t v;
      puk_vect_foreach(v, puk_profile_var, &puk.profile_vars)
        {
          struct puk_profile_var_s*p = *v;
          padico_free(p->label);
          padico_free(p);
        }
      puk_profile_var_vect_destroy(&puk.profile_vars);
      puk.initialized = 0;
    }
}

void puk_notimplemented(void)
{
  padico_fatal("Function not implemented\n");
}

int puk_mckernel(void)
{
  static int is_mckernel = -1;
  if(is_mckernel == -1)
    {
      struct stat proc_mckernel;
      int rc = stat("/proc/mckernel", &proc_mckernel);
      is_mckernel = (rc == 0);
      if(is_mckernel)
        fprintf(stderr, "# Puk: running on mckernel.\n");
#ifndef MCKERNEL
      if(is_mckernel)
        padico_fatal("running on mckernel but build without mckernel support.\n");
#else
      if(!is_mckernel)
        padico_warning("code build with mckernel support, not running on mckernel.\n");
#endif
    }
  return is_mckernel;
}

/******************************************************/


void padico_setenv(const char *var, const char *val)
{
#ifdef HAVE_SETENV
  setenv(var, val, 1);
#else
  int l;
  char *p;
  l = strlen(var)+strlen(val)+2;
  p = (char*) padico_malloc(l);
  strcpy(p, var);
  strcat(p, "=");
  strcat(p, val);
  putenv(p);
#endif
}

const char*padico_getenv(const char*label)
{
  const char*value = getenv(label);
  return value;
}

void padico_cleanenv(void)
{
  const char*padico_root = PUK_ROOT;
  padico_string_t new_lib = padico_string_new();
  padico_string_t padico_lib = padico_string_new();
  padico_string_catf(padico_lib, "%s/lib", padico_root);
  char*saveptr = NULL;
  char*lib = getenv("LD_LIBRARY_PATH");
  char*token = strtok_r(lib, ":", &saveptr);
  while(token != NULL)
    {
      if(strstr(token, padico_string_get(padico_lib)) == NULL)
        {
          padico_string_catf(new_lib, "%s:", token);
        }
      token = strtok_r(NULL, ":", &saveptr);
    }
  padico_setenv("LD_LIBRARY_PATH", padico_string_get(new_lib));
  padico_string_delete(new_lib);
  padico_string_delete(padico_lib);
}

const char*padico_hostname(void)
{
  return puk.hostname;
}

const char*padico_root(void)
{
  return PUK_ROOT;
}

/******************************************************/

const char*puk_getsymprefix(void)
{
  static Dl_info info;
  static const char*p = NULL;
  if(p != NULL)
    return p;
  int rc = dladdr(&padico_puk_init, &info);
  if(rc == 0)
    {
      padico_warning("dladdr() failed; err = %d (%s).\n", errno, strerror(errno));
    }
  char*s = strstr(info.dli_sname, "padico_puk_init");
  if(s == NULL)
    {
      padico_warning("cannot find symbol prefix in %s.\n", info.dli_sname);
      return NULL;
    }
  else if(s == info.dli_sname)
    {
      /* no prefix */
      return NULL;
    }
  else
    {
      s--; /* we want prefix without the added underscore */
      assert(*s == '_');
      *s = '\0';
      p = info.dli_sname;
      return p;
    }
  return NULL;
}

int puk_mult_getnum(void)
{
  const char*p = puk_getsymprefix();
  if(p == NULL)
    {
      padico_warning("cannot get sim number; not in simulation mode.\n");
      return -1;
    }
  else
    {
      int num = -1;
      sscanf(p, "sim%d", &num);
      if(num == -1)
        {
          padico_fatal("cannot parse symprefix = %s.\n", p);
        }
      return num;
    }
}

const char*puk_getlibfile(void)
{
  static Dl_info info;
  int rc = dladdr(&padico_puk_init, &info);
  if(rc == 0)
    {
      padico_warning("dladdr() failed; err = %d (%s).\n", errno, strerror(errno));
    }
  return info.dli_fname; /* return pointer in static var */
}

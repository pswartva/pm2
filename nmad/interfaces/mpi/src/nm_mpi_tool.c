/*
 * NewMadeleine
 * Copyright (C) 2014-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include "nm_mpi_private.h"
#include "nm_mpi_private_syms.h"
#include <assert.h>
#include <errno.h>

#include <Padico/Module.h>
PADICO_MODULE_HOOK(MadMPI);

/* ********************************************************* */

NM_MPI_ALIAS(MPI_T_init_thread,         mpi_t_init_thread);
NM_MPI_ALIAS(MPI_T_finalize,            mpi_t_finalize);
NM_MPI_ALIAS(MPI_T_enum_get_info,       mpi_t_enum_get_info);
NM_MPI_ALIAS(MPI_T_enum_get_item,       mpi_t_enum_get_item);

NM_MPI_ALIAS(MPI_T_cvar_get_num,        mpi_t_cvar_get_num);
NM_MPI_ALIAS(MPI_T_cvar_get_info,       mpi_t_cvar_get_info);
NM_MPI_ALIAS(MPI_T_cvar_get_index,      mpi_t_cvar_get_index);
NM_MPI_ALIAS(MPI_T_cvar_handle_alloc,   mpi_t_cvar_handle_alloc);
NM_MPI_ALIAS(MPI_T_cvar_handle_free,    mpi_t_cvar_handle_free);
NM_MPI_ALIAS(MPI_T_cvar_read,           mpi_t_cvar_read);
NM_MPI_ALIAS(MPI_T_cvar_write,          mpi_t_cvar_write);

NM_MPI_ALIAS(MPI_T_pvar_get_num,        mpi_t_pvar_get_num);
NM_MPI_ALIAS(MPI_T_pvar_get_info,       mpi_t_pvar_get_info);
NM_MPI_ALIAS(MPI_T_pvar_get_index,      mpi_t_pvar_get_index);
NM_MPI_ALIAS(MPI_T_pvar_session_create, mpi_t_pvar_session_create);
NM_MPI_ALIAS(MPI_T_pvar_session_free,   mpi_t_pvar_session_free);
NM_MPI_ALIAS(MPI_T_pvar_handle_alloc,   mpi_t_pvar_handle_alloc);
NM_MPI_ALIAS(MPI_T_pvar_handle_free,    mpi_t_pvar_handle_free);
NM_MPI_ALIAS(MPI_T_pvar_start,          mpi_t_pvar_start);
NM_MPI_ALIAS(MPI_T_pvar_stop,           mpi_t_pvar_stop);
NM_MPI_ALIAS(MPI_T_pvar_read,           mpi_t_pvar_read);
NM_MPI_ALIAS(MPI_T_pvar_write,          mpi_t_pvar_write);
NM_MPI_ALIAS(MPI_T_pvar_reset,          mpi_t_pvar_reset);
NM_MPI_ALIAS(MPI_T_pvar_readreset,      mpi_t_pvar_readreset);

NM_MPI_ALIAS(MPI_T_category_get_num,        mpi_t_category_get_num);
NM_MPI_ALIAS(MPI_T_category_get_info,       mpi_t_category_get_info);
NM_MPI_ALIAS(MPI_T_category_get_num_events, mpi_t_category_get_num_events);
NM_MPI_ALIAS(MPI_T_category_get_index,      mpi_t_category_get_index);
NM_MPI_ALIAS(MPI_T_category_get_cvars,      mpi_t_category_get_cvars);
NM_MPI_ALIAS(MPI_T_category_get_pvars,      mpi_t_category_get_pvars);
NM_MPI_ALIAS(MPI_T_category_get_events,     mpi_t_category_get_events);
NM_MPI_ALIAS(MPI_T_category_get_categories, mpi_t_category_get_categories);
NM_MPI_ALIAS(MPI_T_category_changed,        mpi_t_category_changed);

/* ********************************************************* */

/** a cvar handle */
struct nm_mpi_t_cvar_s
{
  int id;
  struct puk_opt_s*p_opt;
};

NM_MPI_HANDLE_TYPE(t_cvar, struct nm_mpi_t_cvar_s, 1, 16);

/** a pvar session */
struct nm_mpi_pvar_session_s
{
  int id;
  /* empty, since we only have global pvars for now */
};

NM_MPI_HANDLE_TYPE(pvar_session, struct nm_mpi_pvar_session_s, 1, 16);

/** a pvar handle */
struct nm_mpi_t_pvar_s
{
  int id;
  struct nm_mpi_pvar_session_s*p_pvar_session;
  struct puk_profile_var_s*p_prof;
};

NM_MPI_HANDLE_TYPE(t_pvar, struct nm_mpi_t_pvar_s, 1, 16);

/** hashtable type for categories; key = name; data = index */
PUK_HASHTABLE_TYPE(nm_mpi_category, char*, int,
                   &puk_hash_string_default_hash, &puk_hash_string_default_eq, NULL);

/** vector of category to keep them in order (order cannot change between calls) */
PUK_VECT_TYPE(nm_mpi_category, const char*);

static struct nm_mpi_t_s
{
  int refcount;
  struct nm_mpi_handle_t_cvar_s cvars;
  struct nm_mpi_handle_pvar_session_s pvar_sessions;
  struct nm_mpi_handle_t_pvar_s pvars;
  int categories_update;     /**< number of updates of categories */
  int categories_pvars_num;  /**< number of pvars last time categories were updated */
  nm_mpi_category_hashtable_t categories_index;
  nm_mpi_category_vect_t categories;
} nm_mpi_tool =
  {
    .refcount = 0,
    .cvars = NM_MPI_HANDLE_NULL,
    .pvars = NM_MPI_HANDLE_NULL,
    .categories_update = 0,
    .categories_pvars_num = -1, /* make sure we trigger an update the first time */
    .categories_index = NULL,
    .categories = NULL
  };

/* ********************************************************* */

int mpi_t_init_thread(int required, int*provided)
{
  int thread_level = nm_mpi_thread_level_get(required);
  nm_core_set_thread_level(thread_level);
  *provided = thread_level;
  const int count = nm_atomic_inc(&nm_mpi_tool.refcount);
  if(count == 0)
    {
      /* init */
      nm_mpi_handle_t_cvar_init(&nm_mpi_tool.cvars);
      nm_mpi_handle_t_pvar_init(&nm_mpi_tool.pvars);
      nm_mpi_handle_pvar_session_init(&nm_mpi_tool.pvar_sessions);
    }
  return MPI_SUCCESS;
}

int mpi_t_finalize(void)
{
  const int count = nm_atomic_dec(&nm_mpi_tool.refcount);
  if(count == 0)
    {
      /* exit */
      nm_mpi_handle_t_cvar_finalize(&nm_mpi_tool.cvars, NULL);
      nm_mpi_handle_t_pvar_finalize(&nm_mpi_tool.pvars, NULL);
    }
  return MPI_SUCCESS;
}

int mpi_t_enum_get_info(MPI_T_enum enumtype, int*num, char*name, int*name_len)
{
  return MPI_T_ERR_INVALID_INDEX; /* always invalid, since we don't create enums */
}

int mpi_t_enum_get_item(MPI_T_enum enumtype, int index, int*value, char*name, int*name_len)
{
  return MPI_T_ERR_INVALID_INDEX; /* always invalid, since we don't create enums */
}

/* ** cvars ************************************************ */

int mpi_t_cvar_get_num(int*num_cvar)
{
  int num = puk_opt_get_num();
  *num_cvar = num;
  return MPI_SUCCESS;
}

int mpi_t_cvar_get_info(int cvar_index, char*name, int*name_len,
                        int*verbosity, MPI_Datatype*datatype, MPI_T_enum*enumtype,
                        char*desc, int*desc_len, int*bind, int*scope)
{
  if((cvar_index < 0) || (cvar_index >= puk_opt_get_num()))
    return MPI_T_ERR_INVALID;
  const struct puk_opt_s*p_opt = puk_opt_get_by_index(cvar_index);
  if(p_opt == NULL)
    return MPI_T_ERR_INVALID;
  if(name_len != NULL)
    {
      if(name != NULL)
        {
          strncpy(name, p_opt->label, *name_len);
          name[*name_len - 1] = '\0';
        }
      int l = strlen(p_opt->label) + 1;
      if((name == NULL) || (*name_len == 0) || (l < *name_len))
        *name_len = l;
    }
  if(desc_len != NULL)
    {
      if(desc != NULL)
        {
          strncpy(desc, p_opt->desc, *desc_len);
          desc[*desc_len - 1] = '\0';
        }
      int l = strlen(p_opt->desc) + 1;
      if((desc == NULL) || (*desc_len == 0) || (l < *desc_len))
        *desc_len = l;
    }
  if(verbosity != NULL)
    *verbosity = MPI_T_VERBOSITY_USER_ALL;
  if(bind != NULL)
    *bind = MPI_T_BIND_NO_OBJECT;
  if(scope != NULL)
    *scope = MPI_T_SCOPE_ALL; /* for now, assume scope all */
  if(datatype != NULL)
    {
      switch(p_opt->type)
        {
        case puk_opt_type_bool:
          *datatype = MPI_INT; /* MPI_BOOL not allowed by spec for MPI_T cvars type */
          break;
        case puk_opt_type_int:
          *datatype = MPI_INT;
          break;
        case puk_opt_type_unsigned:
          *datatype = MPI_UNSIGNED;
          break;
        case puk_opt_type_string:
          *datatype = MPI_CHAR;
          break;
        }
    }
  if(enumtype != NULL)
    *enumtype = MPI_T_ENUM_NULL;
  return MPI_SUCCESS;
}

int mpi_t_cvar_get_index(const char*name, int*cvar_index)
{
  const struct puk_opt_s*p_opt = puk_opt_get_by_name(name);
  if(p_opt != NULL)
    {
      int index = puk_opt_get_index(p_opt);
      if(index != -1)
        {
          *cvar_index = index;
          return MPI_SUCCESS;
        }
    }
  return MPI_T_ERR_INVALID_INDEX;
}

int mpi_t_cvar_handle_alloc(int cvar_index, void*obj_handle, MPI_T_cvar_handle*handle, int*count)
{
  struct nm_mpi_t_cvar_s*p_cvar = nm_mpi_handle_t_cvar_alloc(&nm_mpi_tool.cvars);
  p_cvar->p_opt = puk_opt_get_by_index(cvar_index);
  *handle = p_cvar->id;
  *count = 1; /* TODO- fix count for strings */
  return MPI_SUCCESS;
}

int mpi_t_cvar_handle_free(MPI_T_cvar_handle*handle)
{
  struct nm_mpi_t_cvar_s*p_cvar = nm_mpi_handle_t_cvar_get(&nm_mpi_tool.cvars, *handle);
  if(p_cvar == NULL)
    return MPI_T_ERR_INVALID_HANDLE;
  nm_mpi_handle_t_cvar_free(&nm_mpi_tool.cvars, p_cvar);
  *handle = MPI_T_CVAR_HANDLE_NULL;
  return MPI_SUCCESS;
}

int mpi_t_cvar_read(MPI_T_cvar_handle handle, void*buf)
{
  const struct nm_mpi_t_cvar_s*p_cvar = nm_mpi_handle_t_cvar_get(&nm_mpi_tool.cvars, handle);
  if(p_cvar == NULL)
    return MPI_T_ERR_INVALID_HANDLE;
  switch(p_cvar->p_opt->type)
    {
    case puk_opt_type_bool:
      {
        int*ibuf = buf;
        *ibuf = puk_opt_getvalue_bool(p_cvar->p_opt);
      }
      break;
    case puk_opt_type_int:
      {
        int*ibuf = buf;
        *ibuf = puk_opt_getvalue_int(p_cvar->p_opt);
      }
      break;
    case puk_opt_type_unsigned:
      {
        unsigned*ibuf = buf;
        *ibuf = puk_opt_getvalue_unsigned(p_cvar->p_opt);
      }
      break;
    case puk_opt_type_string:
      {
        char*ibuf = buf;
        ibuf[0] = *puk_opt_getvalue_string(p_cvar->p_opt);
        ibuf[1] = '\0';
      }
      break;
    }
  return MPI_SUCCESS;
}

int mpi_t_cvar_write(MPI_T_cvar_handle handle, const void*buf)
{
  const struct nm_mpi_t_cvar_s*p_cvar = nm_mpi_handle_t_cvar_get(&nm_mpi_tool.cvars, handle);
  if(p_cvar == NULL)
    return MPI_T_ERR_INVALID_HANDLE;
  switch(p_cvar->p_opt->type)
    {
    case puk_opt_type_bool:
      {
        const int*ibuf = buf;
        puk_opt_setvalue_bool(p_cvar->p_opt, *ibuf);
      }
      break;
    case puk_opt_type_int:
      {
        const int*ibuf = buf;
        puk_opt_setvalue_int(p_cvar->p_opt, *ibuf);
      }
      break;
    case puk_opt_type_unsigned:
      {
        const unsigned*ibuf = buf;
        puk_opt_setvalue_unsigned(p_cvar->p_opt, *ibuf);
      }
      break;
    case puk_opt_type_string:
      {
        const char*ibuf = buf;
        puk_opt_setvalue_string(p_cvar->p_opt, ibuf);
      }
      break;
    }
  return MPI_SUCCESS;
}


/* ** pvars ************************************************ */

int mpi_t_pvar_get_num(int*num_pvar)
{
  *num_pvar = puk_profile_var_vect_size(puk_profile_get_vars());
  return MPI_SUCCESS;
}

int mpi_t_pvar_get_info(int pvar_index, char*name, int*name_len,
                        int*verbosity, int*var_class, MPI_Datatype*datatype,
                        MPI_T_enum*enumtype, char*desc, int*desc_len, int*bind,
                        int*readonly, int*continuous, int*atomic)
{
  if((pvar_index < 0) || (pvar_index >= puk_profile_var_vect_size(puk_profile_get_vars())))
    {
      return MPI_T_ERR_INVALID;
    }
  struct puk_profile_var_s*p_profile_var = puk_profile_var_vect_at(puk_profile_get_vars(), pvar_index);
  if(name_len != NULL)
    {
      if(name != NULL)
        {
          strncpy(name, p_profile_var->label, *name_len);
          name[*name_len - 1] = '\0';
        }
      int l = strlen(p_profile_var->label) + 1;
      if((name == NULL) || (*name_len == 0) || (l < *name_len))
        *name_len = l;
    }
  if(desc_len != NULL)
    {
      if(desc != NULL)
        {
          strncpy(desc, p_profile_var->desc, *desc_len);
          desc[*desc_len - 1] = '\0';
        }
      int l = strlen(p_profile_var->desc) + 1;
      if((desc == NULL) || (*desc_len == 0) || (l < *desc_len))
        *desc_len = l;
    }
  if(verbosity != NULL)
    *verbosity = MPI_T_VERBOSITY_USER_ALL;
  if(var_class != NULL)
    *var_class = p_profile_var->var_class;
  if(datatype != NULL)
    {
      switch(p_profile_var->type)
        {
        case puk_profile_type_unsigned_long:
          *datatype = MPI_UNSIGNED_LONG;
          break;
        case puk_profile_type_unsigned_long_long:
          *datatype = MPI_UNSIGNED_LONG_LONG;
          break;
        case puk_profile_type_double:
          *datatype = MPI_DOUBLE;
          break;
        }
    }
  if(enumtype != NULL)
    *enumtype = MPI_T_ENUM_NULL;
  if(bind != NULL)
    *bind = MPI_T_BIND_NO_OBJECT;
  if(readonly != NULL)
    *readonly = 1;   /**< all variables are readonly */
  if(continuous != NULL)
    *continuous = 1; /**< all variables are continuous */
  if(atomic != NULL)
    *atomic = 0;     /**< no reset */
  return MPI_SUCCESS;
}

int mpi_t_pvar_get_index(const char*name, int var_class, int*p_var_index)
{
  puk_profile_var_vect_itor_t i;
  puk_vect_foreach(i, puk_profile_var, puk_profile_get_vars())
    {
      if(((*i)->var_class == var_class) && (strcmp(name, (*i)->label) == 0))
        {
          *p_var_index = puk_profile_var_vect_rank(puk_profile_get_vars(), i);
          return MPI_SUCCESS;
        }
    }
  return MPI_T_ERR_INVALID_INDEX;
}

int mpi_t_pvar_session_create(MPI_T_pvar_session*pvar_session)
{
  struct nm_mpi_pvar_session_s*p_pvar_session = nm_mpi_handle_pvar_session_alloc(&nm_mpi_tool.pvar_sessions);
  *pvar_session = p_pvar_session->id;
  return MPI_SUCCESS;
}

int mpi_t_pvar_session_free(MPI_T_pvar_session*pvar_session)
{
  struct nm_mpi_pvar_session_s*p_pvar_session = nm_mpi_handle_pvar_session_get(&nm_mpi_tool.pvar_sessions, *pvar_session);
  if(p_pvar_session == NULL)
    return MPI_T_ERR_INVALID_SESSION;
  nm_mpi_handle_pvar_session_free(&nm_mpi_tool.pvar_sessions, p_pvar_session);
  *pvar_session = MPI_T_PVAR_SESSION_NULL;
  return MPI_SUCCESS;
}

int mpi_t_pvar_handle_alloc(MPI_T_pvar_session pvar_session, int pvar_index,
                            void*obj_handle, MPI_T_pvar_handle*handle, int*count)
{
  struct nm_mpi_pvar_session_s*p_pvar_session = nm_mpi_handle_pvar_session_get(&nm_mpi_tool.pvar_sessions, pvar_session);
  if(p_pvar_session == NULL)
    return MPI_T_ERR_INVALID_SESSION;
  if((pvar_index < 0) || (pvar_index >= puk_profile_var_vect_size(puk_profile_get_vars())))
    return MPI_T_ERR_INVALID_HANDLE;
  struct nm_mpi_t_pvar_s*p_pvar = nm_mpi_handle_t_pvar_alloc(&nm_mpi_tool.pvars);
  p_pvar->p_prof = puk_profile_var_vect_at(puk_profile_get_vars(), pvar_index);
  *handle = p_pvar->id;
  *count = 1;
  return MPI_SUCCESS;
}

int mpi_t_pvar_handle_free(MPI_T_pvar_session pe_session, MPI_T_pvar_handle*handle)
{
  struct nm_mpi_t_pvar_s*p_pvar = nm_mpi_handle_t_pvar_get(&nm_mpi_tool.pvars, *handle);
  if(p_pvar == NULL)
    return MPI_T_ERR_INVALID_HANDLE;
  nm_mpi_handle_t_pvar_free(&nm_mpi_tool.pvars, p_pvar);
  *handle = MPI_T_PVAR_HANDLE_NULL;
  return MPI_SUCCESS;
}

int mpi_t_pvar_start(MPI_T_pvar_session pe_session, MPI_T_pvar_handle handle)
{
  return MPI_T_ERR_PVAR_NO_STARTSTOP; /* all variables are continuous */
}

int mpi_t_pvar_stop(MPI_T_pvar_session pe_session, MPI_T_pvar_handle handle)
{
  return MPI_T_ERR_PVAR_NO_STARTSTOP; /* all variables are continuous */
}

int mpi_t_pvar_read(MPI_T_pvar_session pe_session, MPI_T_pvar_handle handle, void*buf)
{
  const struct nm_mpi_t_pvar_s*p_pvar = nm_mpi_handle_t_pvar_get(&nm_mpi_tool.pvars, handle);
  if(p_pvar == NULL)
    return MPI_T_ERR_INVALID_HANDLE;
  switch(p_pvar->p_prof->type)
    {
    case puk_profile_type_unsigned_long:
      {
        unsigned long*lbuf = buf;
        *lbuf = *p_pvar->p_prof->value.as_unsigned_long;
      }
      break;
    case puk_profile_type_unsigned_long_long:
      {
        long long*llbuf = buf;
        *llbuf = *p_pvar->p_prof->value.as_unsigned_long_long;
      }
      break;
    case puk_profile_type_double:
      {
        double*dbuf = buf;
        *dbuf = *p_pvar->p_prof->value.as_double;
      }
      break;
    }
  return MPI_SUCCESS;
}

int mpi_t_pvar_write(MPI_T_pvar_session pe_session,
                     MPI_T_pvar_handle handle, const void*buf)
{
  return MPI_T_ERR_PVAR_NO_WRITE;
}

int mpi_t_pvar_reset(MPI_T_pvar_session pe_session, MPI_T_pvar_handle handle)
{
  return MPI_T_ERR_PVAR_NO_ATOMIC ;
}

int mpi_t_pvar_readreset(MPI_T_pvar_session pe_session, MPI_T_pvar_handle handle, void*buf)
{
  return MPI_T_ERR_PVAR_NO_ATOMIC ;
}

/* ** categories ******************************************* */

static void nm_mpi_category_update(void)
{
  if(nm_mpi_tool.categories_pvars_num == puk_profile_var_vect_size(puk_profile_get_vars()))
    return;
  if(nm_mpi_tool.categories == NULL)
    {
      assert(nm_mpi_tool.categories_index == NULL);
      nm_mpi_tool.categories = nm_mpi_category_vect_new();
      nm_mpi_tool.categories_index = nm_mpi_category_hashtable_new();
    }
  puk_profile_var_vect_itor_t i;
  puk_vect_foreach(i, puk_profile_var, puk_profile_get_vars())
    {
      char*cat = (*i)->category;
      if(!nm_mpi_category_hashtable_probe(nm_mpi_tool.categories_index, cat))
        {
          nm_mpi_category_vect_itor_t n = nm_mpi_category_vect_push_back(nm_mpi_tool.categories, cat);
          const int idx = nm_mpi_category_vect_rank(nm_mpi_tool.categories, n);
          nm_mpi_category_hashtable_insert(nm_mpi_tool.categories_index, cat, idx);
        }
    }
  nm_mpi_tool.categories_update++;
}

int mpi_t_category_get_num(int*num_cat)
{
  struct nm_core*p_core = nm_core_get_singleton();
  if(p_core == NULL)
    return MPI_T_ERR_NOT_ACCESSIBLE;
  nm_mpi_category_update();
  return nm_mpi_category_vect_size(nm_mpi_tool.categories);
  *num_cat = 0;
  return MPI_SUCCESS;
}

int mpi_t_category_get_info(int cat_index, char*name, int*name_len,
                            char*desc, int*desc_len, int*num_cvars, int*num_pvars,
                            int*num_categories)
{
  struct nm_core*p_core = nm_core_get_singleton();
  if(p_core == NULL)
    return MPI_T_ERR_NOT_ACCESSIBLE;
  nm_mpi_category_update();
  if(cat_index < 0 || cat_index >= nm_mpi_category_vect_size(nm_mpi_tool.categories))
    return MPI_T_ERR_INVALID_INDEX;
  const char*c = nm_mpi_category_vect_at(nm_mpi_tool.categories, cat_index);
  if(name_len != NULL)
    {
      if(name != NULL)
        {
          strncpy(name, c, *name_len);
          name[*name_len - 1] = '\0';
        }
      int l = strlen(c) + 1;
      if((name == NULL) || (*name_len == 0) || (l < *name_len))
        *name_len = l;
    }
  if(desc_len != NULL)
    {
      if(desc != NULL)
        {
          strncpy(desc, c, *desc_len);
          desc[*desc_len - 1] = '\0';
        }
      int l = strlen(c) + 1;
      if((desc == NULL) || (*desc_len == 0) || (l < *desc_len))
        *desc_len = l;
    }
  if(num_pvars != NULL)
    {
      int count = 0;
      puk_profile_var_vect_itor_t i;
      puk_vect_foreach(i, puk_profile_var, puk_profile_get_vars())
        {
          if(strcmp(c, (*i)->category) == 0)
            count++;
        }
      *num_pvars = count;
    }
  if(num_cvars != NULL)
    num_cvars = 0;
  if(num_categories != NULL)
    num_categories = 0;
  return MPI_SUCCESS;
}

int mpi_t_category_get_num_events(int cat_index, int*num_events)
{
  /* events not supported yet */
  return MPI_T_ERR_NOT_SUPPORTED;
}

int mpi_t_category_get_index(const char*name, int*cat_index)
{
  struct nm_core*p_core = nm_core_get_singleton();
  if(p_core == NULL)
    return MPI_T_ERR_NOT_ACCESSIBLE;
  nm_mpi_category_update();
  if(!nm_mpi_category_hashtable_probe(nm_mpi_tool.categories_index, name))
    return MPI_T_ERR_INVALID_NAME;
  int index = nm_mpi_category_hashtable_lookup(nm_mpi_tool.categories_index, name);
  if(cat_index != NULL)
    *cat_index = index;
  return MPI_SUCCESS;
}

int mpi_t_category_get_cvars(int cat_index, int len, int indices[])
{
  /* no cvars in categories yet */
  return MPI_T_ERR_NOT_SUPPORTED;
}

int mpi_t_category_get_pvars(int cat_index, int len, int indices[])
{
  nm_mpi_category_update();
  if(cat_index < 0 || cat_index >= nm_mpi_category_vect_size(nm_mpi_tool.categories))
    return MPI_T_ERR_INVALID_INDEX;
  const char*c = nm_mpi_category_vect_at(nm_mpi_tool.categories, cat_index);
  int count = 0;
  puk_profile_var_vect_itor_t i;
  puk_vect_foreach(i, puk_profile_var, puk_profile_get_vars())
    {
      if(count >= len)
        break;
      if(strcmp(c, (*i)->category) == 0)
        {
          const int index = puk_profile_var_vect_rank(puk_profile_get_vars(), i);
          indices[count] = index;
          count++;
        }
    }
  return MPI_SUCCESS;
}

int mpi_t_category_get_events(int cat_index, int len, int indices[])
{
  /* events not supported yet */
  return MPI_T_ERR_NOT_SUPPORTED;
}

int mpi_t_category_get_categories(int cat_index, int len, int indices[])
{
  /* no categories in categories yet */
  return MPI_T_ERR_NOT_SUPPORTED;
}

int mpi_t_category_changed(int*update_number)
{
  nm_mpi_category_update();
  *update_number = nm_mpi_tool.categories_update;
  return MPI_SUCCESS;
}

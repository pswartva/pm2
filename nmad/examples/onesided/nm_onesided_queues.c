/*
 * NewMadeleine
 * Copyright (C) 2006-2022 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

/** @file
 * Test for remote queues
 */

#include <nm_onesided_interface.h>
#include "../common/nm_examples_helper.h"
#include <stdio.h>

static const char*p_msg = "hello world";

int main(int argc, char**argv)
{
  const nm_tag_t tag = 0x01;
  nm_examples_init(&argc, argv);

  /* intialize the onesided interface */
  nm_onesided_t p_onesided;
  nm_onesided_init(p_session, &p_onesided);

  if(is_server)
    {
      nm_onesided_queue_id_t q = nm_onesided_queue_create(p_onesided);
      nm_sr_send(p_session, p_gate, tag, &q, sizeof(q));

      char*p_ptr = NULL;
      while(p_ptr == NULL)
        {
          p_ptr = nm_onesided_queue_dequeue(p_onesided, q);
        }
      printf("received message: %s\n", p_ptr);
      padico_free(p_ptr);
    }
  else
    {
      nm_onesided_queue_id_t q = -1;
      nm_sr_recv(p_session, p_gate, tag, &q, sizeof(q));
      printf("received queue id = %d\n", q);

      nm_onesided_request_t req;
      nm_onesided_queue_ienqueue(p_onesided, p_gate, q,
                                 p_msg, strlen(p_msg) + 1, &req);
      nm_onesided_req_wait(&req);
    }

  /* stop the onesided interface */
  nm_onesided_finalize(p_onesided);
  nm_examples_exit();
}

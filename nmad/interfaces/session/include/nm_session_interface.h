/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#ifndef NM_SESSION_INTERFACE_H
#define NM_SESSION_INTERFACE_H

#include <nm_public.h>
#include <nm_core_interface.h>
#include <Padico/Puk.h>

/** @defgroup session_interface Session interface
 * This is the session interface, the nmad interface used to build sessions.
 * @{
 */

typedef struct nm_session_s*nm_session_t;


/** Open a new session, assuming processes are already connected.
 */
int nm_session_open(nm_session_t*pp_session, const char*label);

/** Disconnect and destroy a session.
 */
int nm_session_close(nm_session_t p_session);

/** Lookup a session by hashcode
 */
nm_session_t nm_session_lookup(nm_session_hash_t hashcode);

/** Get the name of the given session.
 */
const char*nm_session_get_name(nm_session_t p_session);


/** @} */

#endif /* NM_SESSION_INTERFACE_H */

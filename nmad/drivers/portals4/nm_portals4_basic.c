/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>

#include <portals4.h>

#include "nm_portals4_common.h"
#include <nm_minidriver.h>
#include <nm_private.h>
#include <Padico/Module.h>

static void*nm_portals4_basic_instantiate(puk_instance_t instance, puk_context_t context);
static void nm_portals4_basic_destroy(void*status);

static const struct puk_component_driver_s nm_portals4_basic_component =
  {
    .instantiate = &nm_portals4_basic_instantiate,
    .destroy     = &nm_portals4_basic_destroy
  };

static void nm_portals4_basic_getprops(puk_context_t context, struct nm_minidriver_properties_s*props);
static void nm_portals4_basic_init(puk_context_t context, const void**drv_url, size_t*url_size);
static void nm_portals4_basic_close(puk_context_t context);
static void nm_portals4_basic_connect(void*_status, const void*remote_url, size_t url_size);
static void nm_portals4_basic_disconnect(void*_status);
static int  nm_portals4_basic_send_iov_post(void*_status, const struct iovec*v, int n);
static int  nm_portals4_basic_send_poll(void*_status);
static int  nm_portals4_basic_recv_iov_post(void*_status,  struct iovec*v, int n);
static int  nm_portals4_basic_recv_poll_one(void*_status);
static int  nm_portals4_basic_recv_cancel(void*_status);

static const struct nm_minidriver_iface_s nm_portals4_basic_minidriver =
  {
    .getprops         = &nm_portals4_basic_getprops,
    .init             = &nm_portals4_basic_init,
    .close            = &nm_portals4_basic_close,
    .connect          = &nm_portals4_basic_connect,
    .disconnect       = &nm_portals4_basic_disconnect,
    .send_iov_post    = &nm_portals4_basic_send_iov_post,
    .send_data_post   = NULL,
    .send_buf_get     = NULL,
    .send_buf_post    = NULL,
    .send_poll        = &nm_portals4_basic_send_poll,
    .recv_iov_post    = &nm_portals4_basic_recv_iov_post,
    .recv_data_post   = NULL,
    .recv_buf_poll    = NULL,
    .recv_buf_release = NULL,
    .recv_poll_one    = &nm_portals4_basic_recv_poll_one,
    .recv_cancel      = &nm_portals4_basic_recv_cancel
  };

/* ********************************************************* */

PADICO_MODULE_COMPONENT(Minidriver_portals4_basic,
  puk_component_declare("Minidriver_portals4_basic",
                        puk_component_provides("PadicoComponent", "component", &nm_portals4_basic_component),
                        puk_component_provides("NewMad_minidriver", "minidriver", &nm_portals4_basic_minidriver)));

/* ********************************************************* */

/** 'portals4' driver per-context data. */
struct nm_portals4_basic_context_s
{
  ptl_ni_limits_t ni_limits;
  ptl_handle_ni_t ni_handle;        /**< handle to the network interface */
  struct nm_portals4_addr_s local;  /**< local address */
  ptl_handle_eq_t global_eq_handle; /**< global event queue for recv */
};

/** 'portals4' per-instance status. */
struct nm_portals4_basic_s
{
  struct nm_portals4_basic_context_s*p_portals4_context;
  struct nm_portals4_addr_s peer;
  struct
  {
    nm_len_t length;            /**< length of message beeing sent */
    ptl_handle_eq_t eq_handle;  /**< event queue for send events */
    ptl_handle_md_t md_handle;  /**< memory descriptor used for sending */
  } send;
  struct
  {
    ptl_handle_me_t me_handle;  /**< matching entry for receive */
    int done;
  } recv;
};

/* ********************************************************* */

static void*nm_portals4_basic_instantiate(puk_instance_t instance, puk_context_t context)
{
  struct nm_portals4_basic_s*p_status = padico_malloc(sizeof(struct nm_portals4_basic_s));
  struct nm_portals4_basic_context_s*p_portals4_context = puk_context_get_status(context);
  p_status->p_portals4_context = p_portals4_context;
  return p_status;
}

static void nm_portals4_basic_destroy(void*_status)
{
  struct nm_portals4_basic_s*p_status = _status;
  padico_free(p_status);
}

/* ********************************************************* */

static void nm_portals4_basic_getprops(puk_context_t context, struct nm_minidriver_properties_s*props)
{
  struct nm_portals4_basic_context_s*p_portals4_context = padico_malloc(sizeof(struct nm_portals4_basic_context_s));
  puk_context_set_status(context, p_portals4_context);
  int rc = PtlInit();
  nm_portals4_check_error(rc, "failed to initialise portals4 library");

  rc = PtlNIInit(PTL_IFACE_DEFAULT, PTL_NI_MATCHING | PTL_NI_PHYSICAL, PTL_PID_ANY, NULL,
                 &p_portals4_context->ni_limits, &p_portals4_context->ni_handle);
  nm_portals4_check_error(rc, "failed to initialise portals4 network interface");

  fprintf(stderr, "# portals4 init- max_unexpected_headers = %d\n", p_portals4_context->ni_limits.max_unexpected_headers);
  PtlGetPhysId(p_portals4_context->ni_handle, &p_portals4_context->local.process_id);

  props->profile.latency = 2000;
  props->profile.bandwidth = 10000;
  props->capabilities.supports_iovec = 0;
  props->capabilities.max_msg_size = p_portals4_context->ni_limits.max_msg_size;
  props->capabilities.trk_rdv = 1; /* needs rdv for flow control */
  props->nickname = "portals4-basic";
}

static void nm_portals4_basic_init(puk_context_t context, const void**drv_url, size_t*url_size)
{
  struct nm_portals4_basic_context_s*p_portals4_context = puk_context_get_status(context);
  int rc = PtlEQAlloc(p_portals4_context->ni_handle, 32, &p_portals4_context->global_eq_handle);
  nm_portals4_check_error(rc, "failed to allocate event queue");

  rc = PtlPTAlloc(p_portals4_context->ni_handle, PTL_PT_FLOWCTRL,
                  p_portals4_context->global_eq_handle,
                  PTL_PT_ANY, &p_portals4_context->local.pt_index);
  nm_portals4_check_error(rc, "failed to allocate portal table");

  *drv_url = &p_portals4_context->local;
  *url_size = sizeof(p_portals4_context->local);
}

static void nm_portals4_basic_close(puk_context_t context)
{
  struct nm_portals4_basic_context_s*p_portals4_context = puk_context_get_status(context);
  PtlNIFini(p_portals4_context->ni_handle);
  PtlFini();
  puk_context_set_status(context, NULL);
  padico_free(p_portals4_context);
}

static void nm_portals4_basic_connect(void*_status, const void*remote_url, size_t url_size)
{
  struct nm_portals4_basic_s*p_status = _status;
  struct nm_portals4_basic_context_s*p_portals4_context = p_status->p_portals4_context;
  assert(url_size == sizeof(struct nm_portals4_addr_s));
  const struct nm_portals4_addr_s*p_peer = remote_url;
  p_status->peer = *p_peer;
  int rc = PtlEQAlloc(p_portals4_context->ni_handle, 32, &p_status->send.eq_handle);
  nm_portals4_check_error(rc, "failed to allocate event queue");
}

static void nm_portals4_basic_disconnect(void*_status)
{
  struct nm_portals4_basic_s*p_status = _status;
  PtlEQFree(p_status->send.eq_handle);
}

static int nm_portals4_basic_send_iov_post(void*_status, const struct iovec*v, int n)
{
  struct nm_portals4_basic_s*p_status = _status;
  const ptl_md_t md =
    {
      .start     = v[0].iov_base,
      .length    = v[0].iov_len,
      .options   = 0,
      .eq_handle = p_status->send.eq_handle,
      .ct_handle = PTL_CT_NONE
    };
  int rc = PtlMDBind(p_status->p_portals4_context->ni_handle, &md, &p_status->send.md_handle);
  nm_portals4_check_error(rc, "failed to bind memory descriptor");
  p_status->send.length = v[0].iov_len;

  rc = PtlPut(p_status->send.md_handle,
              0 /* local_offset */, p_status->send.length,
              PTL_ACK_REQ /* ack_req */,
              p_status->peer.process_id,
              p_status->peer.pt_index,
              1 /* match_bits */,
              0 /* remote_offset */,
              NULL /* user_ptr */,
              0 /* hdr_data */);
  nm_portals4_check_error(rc, "failed to send data");
  return NM_ESUCCESS;
}

static int nm_portals4_basic_send_poll(void*_status)
{
  struct nm_portals4_basic_s*p_status = _status;

  ptl_event_t event;
  int rc = PtlEQGet(p_status->send.eq_handle, &event);
  if(rc == PTL_OK)
    {
      if(event.ni_fail_type == PTL_NI_PT_DISABLED)
        {
          NM_WARN("send poll- NI failed with PTL_NI_PT_DISABLED; it shouldn't happen; re-trying send.\n");
          puk_usleep(1000);

          /* flow control has disabled PT on remote side; try again */
          rc = PtlPut(p_status->send.md_handle,
                      0 /* local_offset */, p_status->send.length,
                      PTL_ACK_REQ /* ack_req */,
                      p_status->peer.process_id,
                      p_status->peer.pt_index,
                      1 /* match_bits */,
                      0 /* remote_offset */,
                      NULL /* user_ptr */,
                      0 /* hdr_data */);
          nm_portals4_check_error(rc, "failed to send data");
        }
      else if(event.ni_fail_type == PTL_NI_OK)
        {
          if(event.type == PTL_EVENT_ACK) /* PTL_EVENT_SEND */
            {
              int rc = PtlMDRelease(p_status->send.md_handle);
              nm_portals4_check_error(rc, "failed to release memory descriptor");
              return NM_ESUCCESS;
            }
        }
      else
        {
          NM_WARN("event NI fail type = %d (%s)\n",
                  event.ni_fail_type, nm_portals4_nifail_strerror(event.ni_fail_type));
        }
      return -NM_EAGAIN;
    }
  else if(rc == PTL_EQ_EMPTY)
    {
      return -NM_EAGAIN;
    }
  else if(rc == PTL_EQ_DROPPED)
    {
      /* TODO- manage dropped events */
      NM_FATAL("detected dropped event\n");
    }
  else
    {
      nm_portals4_check_error(rc, "error while polling for events");
      return -NM_EUNKNOWN;
    }
}

static int nm_portals4_basic_recv_iov_post(void*_status, struct iovec*v, int n)
{
  struct nm_portals4_basic_s*p_status = _status;
  p_status->recv.done = 0;
  ptl_me_t me;
  me.start  = v[0].iov_base;
  me.length = v[0].iov_len;
  me.uid    = PTL_UID_ANY;
  me.match_id = p_status->peer.process_id;
  me.match_bits  = 1;
  me.ignore_bits = 0;
  me.min_free = 0;
  me.options  = PTL_ME_OP_PUT | PTL_ME_USE_ONCE;
  me.ct_handle = PTL_CT_NONE;
  int rc = PtlMEAppend(p_status->p_portals4_context->ni_handle, p_status->p_portals4_context->local.pt_index,
                       &me, PTL_PRIORITY_LIST, p_status /* user_ptr */, &p_status->recv.me_handle);
  nm_portals4_check_error(rc, "error while posting matching request");

  return NM_ESUCCESS;
}

static void nm_portals4_basic_recv_poll_all(struct nm_portals4_basic_context_s*p_portals4_context)
{
  ptl_event_t event;
  int rc = PtlEQGet(p_portals4_context->global_eq_handle, &event);
  if(rc == PTL_OK)
    {
      if(event.ni_fail_type == PTL_NI_PT_DISABLED)
        {
          NM_WARN("recv poll- NI failed with PTL_NI_PT_DISABLED\n");
        }
      else if(event.ni_fail_type != PTL_NI_OK)
        {
          NM_WARN("event NI fail type = %d (%s)\n",
                  event.ni_fail_type, nm_portals4_nifail_strerror(event.ni_fail_type));
        }
      if(event.type == PTL_EVENT_PUT)
        {
          struct nm_portals4_basic_s*p_status = event.user_ptr;
          p_status->recv.done = 1;
        }
      else if(event.type == PTL_EVENT_PT_DISABLED)
        {
          NM_WARN("recv poll- got event PTL_EVENT_PT_DISABLED; it shouldn't happen; re-enabling PT\n");
          PtlPTEnable(p_portals4_context->ni_handle, p_portals4_context->local.pt_index);
        }
    }
  else if(rc == PTL_EQ_EMPTY)
    {
      /* no event; do nothing */
    }
  else if(rc == PTL_EQ_DROPPED)
    {
      /* TODO- manage dropped events */
      NM_FATAL("detected dropped event\n");
    }
  else
    {
      nm_portals4_check_error(rc, "error while polling for events");
    }
}

static int nm_portals4_basic_recv_poll_one(void*_status)
{
  struct nm_portals4_basic_s*p_status = _status;
  nm_portals4_basic_recv_poll_all(p_status->p_portals4_context);
  if(p_status->recv.done)
    return NM_ESUCCESS;
  else
    return -NM_EAGAIN;
}


static int nm_portals4_basic_recv_cancel(void*_status)
{
  return -NM_ENOTIMPL;
}

/*
 * NewMadeleine
 * Copyright (C) 2012-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

/** @file
 * @brief FORTRAN interface implementation
 */

#include "nm_mpi_private.h"
#include "nm_mpi_private_syms.h"
#include <stdint.h>
#include <assert.h>
#include <errno.h>

#include <Padico/Module.h>
PADICO_MODULE_HOOK(MadMPI);


#if !defined(NMAD_FORTRAN_TARGET_NONE)

/* ** FORTRAN argc/argv support */

#define MAX_ARG_LEN 128
static char**global_argv = NULL;

/** Function defined in libmadmpif. See nm_mpi_initf.f90 */
extern void nm_mpi_initf_(void);

/** Helper macros for status manipulation */
#define NM_MPI_F_STATUS_DECLARE(_status) MPI_Status *_status, _status2;

#define NM_MPI_F_STATUS_SET(_status, status) {          \
    if(status == MPI_F_STATUS_IGNORE) {                 \
      _status = MPI_STATUS_IGNORE;                      \
    }                                                   \
    else {                                              \
      _status = &_status2;                              \
    }                                                   \
  }

#define NM_MPI_F_STATUS_RETURN(_status, status) {       \
    if(_status != MPI_STATUS_IGNORE) {                  \
      mpi_status_c2f(_status, status);                  \
    }                                                   \
  }                                                     \

#define NM_MPI_F2C_BOTTOM(addr) {               \
    if(addr == NM_MPI_F_MPI_BOTTOM) {           \
      addr = MPI_BOTTOM;                        \
    }                                           \
  }

#if defined(NMAD_FORTRAN_TARGET_GFORTRAN)

/* GFortran iargc/getargc bindings
 */
void _gfortran_getarg_i4(int32_t*num, char*value, int val_len) __attribute__ ((weak));
void _gfortran_getarg_i8(int64_t*num, char*value, int val_len) __attribute__ ((weak));
int32_t _gfortran_iargc(void) __attribute__ ((weak));

/** Get argc/argv when initialisation done by Fortran code.
 */
static void nm_fortran_init(int*argc, char***argv)
{
  int i;
  *argc = 1 + _gfortran_iargc();
  *argv = padico_malloc((1 + *argc) * sizeof(char *));
  for(i = 0; i < *argc; i++)
    {
      int j;
      (*argv)[i] = padico_malloc(MAX_ARG_LEN + 1);
      if(sizeof(char*) == 4)
        {
          int32_t ii = i;
          _gfortran_getarg_i4(&ii, (*argv)[i], MAX_ARG_LEN);
        }
      else
        {
          int64_t ii = i;
          _gfortran_getarg_i8(&ii, (*argv)[i], MAX_ARG_LEN);
        }
      j = MAX_ARG_LEN;
      while(j > 1 && ((*argv)[i])[j - 1] == ' ')
        {
          j--;
        }
      ((*argv)[i])[j] = '\0';
    }
  (*argv)[*argc] = NULL;
  global_argv = *argv;
}

#elif defined(NMAD_FORTRAN_TARGET_IFORT)

/* Ifort iargc/getargc bindings
 */
extern int iargc_() __attribute__ ((weak));
extern void getarg_(int*, char*, int) __attribute__ ((weak));

static void nm_fortran_init(int *argc, char ***argv)
{
  int i;
  *argc = 1 + iargc_();
  *argv = padico_malloc((1 + *argc) * sizeof(char *));
  for(i = 0; i < *argc; i++)
    {
      int j;
      (*argv)[i] = padico_malloc(MAX_ARG_LEN + 1);
      getarg_((int32_t *) & i, (*argv)[i], MAX_ARG_LEN);

      j = MAX_ARG_LEN;
      while(j > 1 && ((*argv)[i])[j - 1] == ' ')
        {
          j--;
        }
      ((*argv)[i])[j] = '\0';
    }
  global_argv = *argv;
}

#elif defined(NMAD_FORTRAN_TARGET_CRAY)

static void nm_fortran_init(int*argc, char***argv)
{
  /* fake argc/argv */
  static char*fake_argv = { "(fake)", NULL };
  *argc = 1;
  *argv = &fake_argv;
}

#else
#  error unknown FORTRAN TARGET for Mad MPI
#endif /* NMAD_FORTRAN_TARGET_* */

static void nm_fortran_finalize(void)
{
  if(global_argv)
    {
      char**argv;
      for(argv = global_argv; *argv != NULL; argv++)
        {
          padico_free(*argv);
        }
      padico_free(global_argv);
      global_argv = NULL;
    }
}

/** Fortran version for MPI_INIT */
void mpi_init_(MPI_Fint*ierr)
{
  int argc = -1;
  char**argv = NULL;
  nm_fortran_init(&argc, &argv);
  nm_mpi_initf_();
  *ierr = mpi_init(&argc, &argv);
}

/** Fortran version for MPI_INIT_THREAD */
void mpi_init_thread_(MPI_Fint*required, MPI_Fint*provided, MPI_Fint*ierr)
{
  int argc = -1;
  char**argv = NULL;
  nm_fortran_init(&argc, &argv);
  int c_provided = 0;
  *ierr = mpi_init_thread(&argc, &argv, *required, &c_provided);
  *provided = c_provided;
}


/* ********************************************************* */
/* ** f2c & c2f: C function to interface with FORTRAN  */


NM_MPI_ALIAS(MPI_Status_c2f, mpi_status_c2f);
NM_MPI_ALIAS(MPI_Status_f2c, mpi_status_f2c);
NM_MPI_ALIAS(MPI_Comm_c2f, mpi_comm_c2f);
NM_MPI_ALIAS(MPI_Comm_f2c, mpi_comm_f2c);
NM_MPI_ALIAS(MPI_Type_c2f, mpi_type_c2f);
NM_MPI_ALIAS(MPI_Type_f2c, mpi_type_f2c);
NM_MPI_ALIAS(MPI_Group_c2f, mpi_group_c2f);
NM_MPI_ALIAS(MPI_Group_f2c, mpi_group_f2c);
NM_MPI_ALIAS(MPI_Info_c2f, mpi_info_c2f);
NM_MPI_ALIAS(MPI_Info_f2c, mpi_info_f2c);
NM_MPI_ALIAS(MPI_Request_c2f, mpi_request_c2f);
NM_MPI_ALIAS(MPI_Request_f2c, mpi_request_f2c);
NM_MPI_ALIAS(MPI_Op_c2f, mpi_op_c2f);
NM_MPI_ALIAS(MPI_Op_f2c, mpi_op_f2c);
NM_MPI_ALIAS(MPI_Win_c2f, mpi_win_c2f);
NM_MPI_ALIAS(MPI_Win_f2c, mpi_win_f2c);
NM_MPI_ALIAS(MPI_Errhandler_f2c, mpi_errhandler_f2c);
NM_MPI_ALIAS(MPI_Errhandler_c2f, mpi_errhandler_c2f);
NM_MPI_ALIAS(MPI_File_f2c, mpi_file_f2c);
NM_MPI_ALIAS(MPI_File_c2f, mpi_file_c2f);

int mpi_status_f2c(MPI_Fint*f_status, MPI_Status*c_status)
{
  if(f_status == MPI_F_STATUS_IGNORE ||
     f_status == MPI_F_STATUSES_IGNORE ||
     c_status == NULL)
    {
      return NM_MPI_ERROR_WORLD(MPI_ERR_ARG);
    }
  c_status->MPI_SOURCE = f_status[0];
  c_status->MPI_TAG    = f_status[1];
  c_status->MPI_ERROR  = f_status[2];
  c_status->size       = f_status[3];
  c_status->cancelled  = f_status[4];
  return MPI_SUCCESS;
}

int mpi_status_c2f(MPI_Status*c_status, MPI_Fint*f_status)
{
  if(c_status == MPI_STATUS_IGNORE ||
     c_status == MPI_STATUSES_IGNORE ||
     c_status == NULL)
    {
      return NM_MPI_ERROR_WORLD(MPI_ERR_ARG);
    }

  f_status[0] = c_status->MPI_SOURCE;
  f_status[1] = c_status->MPI_TAG;
  f_status[2] = c_status->MPI_ERROR;
  f_status[3] = c_status->size;
  f_status[4] = c_status->cancelled;
  return MPI_SUCCESS;
}

MPI_Comm mpi_comm_f2c(MPI_Fint comm)
{
  return comm;
}
MPI_Fint mpi_comm_c2f(MPI_Comm comm)
{
  return comm;
}
MPI_Datatype mpi_type_f2c(MPI_Fint datatype)
{
  return datatype;
}
MPI_Fint mpi_type_c2f(MPI_Datatype datatype)
{
  return datatype;
}
MPI_Group mpi_group_f2c(MPI_Fint group)
{
  return group;
}
MPI_Fint mpi_group_c2f(MPI_Group group)
{
  return group;
}
MPI_Info mpi_info_f2c(MPI_Fint info)
{
  return info;
}
MPI_Fint mpi_info_c2f(MPI_Info info)
{
  return info;
}
MPI_Request mpi_request_f2c(MPI_Fint request)
{
  return request;
}
MPI_Fint mpi_request_c2f(MPI_Request request)
{
  return request;
}
MPI_Op mpi_op_f2c(MPI_Fint op)
{
  return op;
}
MPI_Fint mpi_op_c2f(MPI_Op op)
{
  return op;
}
MPI_Win mpi_win_f2c(MPI_Fint win)
{
  return win;
}
MPI_Fint mpi_win_c2f(MPI_Op win)
{
  return win;
}
MPI_Errhandler mpi_errhandler_f2c(MPI_Fint errhandler)
{
  return errhandler;
}
MPI_Fint mpi_errhandler_c2f(MPI_Errhandler errhandler)
{
  return errhandler;
}
MPI_File mpi_file_f2c(MPI_Fint file)
{
  return file;
}
MPI_Fint mpi_file_c2f(MPI_File file)
{
  return file;
}


/* ** some f2c & c2f internal helper */

static void nm_mpif_array_c2f(int*c_array, MPI_Fint f_array[], int size)
{
  int i;
  for(i = 0; i < size; i++)
    {
      f_array[i] = c_array[i];
    }
}

static int*nm_mpif_array_f2c(MPI_Fint f_array[], int size)
{
  int*c_array = padico_malloc(sizeof(int) * size);
  int i;
  for(i = 0; i < size; i++)
    {
      c_array[i] = f_array[i];
    }
  return c_array;
}

static void nm_mpif_string_c2f(char*c_str, char*f_str, int len)
{
  int i;
  int c_len = strlen(c_str);
  for(i = 0; i < c_len; i++)
    f_str[i] = c_str[i];
  for(i = c_len; i < len; i++)
    f_str[i] = ' ';
}

static char*nm_mpif_string_f2c(char*f_str, int len)
{
  int i;
  char*c_str;
  char*pf = f_str + len - 1;
  while(*pf == ' ' && pf > f_str)
    pf--;
  pf++;
  int c_len = pf - f_str;//len without null char
  c_str = padico_malloc(sizeof(char) * (c_len + 1));
  for(i = 0; i < c_len; i++)
    c_str[i] = f_str[i];
  c_str[i] = 0;
  return c_str;
}

static char*nm_mpif_info_string_f2c(char*f_str, int len)
{
  int i;
  char*c_str;
  char*pf = f_str + len - 1;
  char*pf_b = f_str;
  while(*pf == ' ' && pf > f_str)
    pf--;
  pf++;
  while(*pf_b == ' ' && pf_b < pf)
    pf_b++;

  int c_len = pf - pf_b;//len without null char
  c_str = padico_malloc(sizeof(char) * (c_len + 1));
  for(i = 0; i < c_len; i++)
    c_str[i] = pf_b[i];
  c_str[i] = 0;
  return c_str;
}

/* ********************************************************* */
/* ** FORTRAN version of MPI symbols
 */

/** Fortran version for MPI_QUERY_THREAD */
void mpi_query_thread_(MPI_Fint*provided, MPI_Fint*ierr)
{
  int c_provided;
  *ierr = mpi_query_thread(&c_provided);
  *provided = c_provided;
}

/** Fortran version for MPI_INITIALIZED */
void mpi_initialized_(MPI_Fint*flag, MPI_Fint*ierr)
{
  int c_flag;
  *ierr = mpi_initialized(&c_flag);
  *flag = c_flag;
}

/** Fortran version for MPI_FINALIZE */
void mpi_finalize_(void)
{
#ifndef NMAD_FORTRAN_TARGET_NONE
  nm_fortran_finalize();
#endif
  mpi_finalize();
}

/** Fortran version for MPI_FINALIZED */
void mpi_finalized_(MPI_Fint*flag, MPI_Fint*ierr)
{
  int c_flag;
  *ierr = mpi_finalized(&c_flag);
  *flag = c_flag;
}

/** Fortran version for MPI_ABORT */
void mpi_abort_(MPI_Fint*comm, MPI_Fint*errorcode)
{
  mpi_abort(*comm, *errorcode);
}

/** Fortran version for MPI_COMM_SIZE */
void mpi_comm_size_(MPI_Fint*comm, MPI_Fint*size, MPI_Fint*ierr)
{
  int c_size;
  *ierr = mpi_comm_size(*comm, &c_size);
  *size = c_size;
}

/** Fortran version for MPI_COMM_RANK */
void mpi_comm_rank_(MPI_Fint*comm, MPI_Fint*rank, MPI_Fint*ierr)
{
  int c_rank;
  *ierr = mpi_comm_rank(*comm, &c_rank);
  *rank = c_rank;
}

/** Fortran version for MPI_KEYVAL_CREATE */
void mpi_keyval_create_(nm_mpi_copy_subroutine_t*copy_fn, nm_mpi_delete_subroutine_t*delete_fn, MPI_Fint*keyval, void*extra_state, MPI_Fint*ierr)
{
  int c_keyval;
  *ierr = nm_mpi_comm_create_keyval_fort(copy_fn, delete_fn, &c_keyval, extra_state);
  *keyval = c_keyval;
}

void mpi_dup_fn_(MPI_Fint*oldcomm, MPI_Fint*keyval, MPI_Fint*extra_state, MPI_Fint*attribute_val_in, MPI_Fint*attribute_val_out, MPI_Fint*flag, MPI_Fint*ierr)
{
  *flag = 1;
  *attribute_val_out = *attribute_val_in;
  *ierr = MPI_SUCCESS;
}

void mpi_null_copy_fn_(MPI_Fint*oldcomm, MPI_Fint*keyval, MPI_Fint*extra_state, MPI_Fint*attribute_val_in, MPI_Fint*attribute_val_out, MPI_Fint*flag, MPI_Fint*ierr)
{
  *flag = 0;
  *ierr = MPI_SUCCESS;
}

void mpi_null_delete_fn_(MPI_Fint*comm, MPI_Fint*comm_keyval, MPI_Fint*attribute_val, MPI_Fint*extra_state, MPI_Fint*ierr)
{
  *ierr = MPI_SUCCESS;
}

/** Fortran version for MPI_KEYVAL_FREE */
void mpi_keyval_free_(MPI_Fint*keyval, MPI_Fint*ierr)
{
  int c_keyval = *keyval;
  *ierr = mpi_keyval_free(&c_keyval);
  *keyval = c_keyval;
}

/** Fortran version for MPI_ATTR_GET */
void mpi_attr_get_(MPI_Fint*comm, MPI_Fint*keyval, MPI_Fint*attr_value, MPI_Fint*flag, MPI_Fint*ierr)
{
  int c_flag = *flag;
  void*c_attr_value;
  *ierr = mpi_attr_get(*comm, *keyval, &c_attr_value, &c_flag);
  *attr_value = (MPI_Fint)c_attr_value;
  *flag = c_flag;
}

void mpi_attr_put_(MPI_Fint*comm, MPI_Fint*keyval, MPI_Fint*attr_value, MPI_Fint*ierr)
{
  *ierr = mpi_attr_put(*comm, *keyval, (void*)(*attr_value));
}

/** Fortran version for MPI_ATTR_DELETE */
void mpi_attr_delete_(MPI_Fint*comm, MPI_Fint*keyval, MPI_Fint*ierr)
{
  *ierr = mpi_attr_delete(*comm, *keyval);
}

/** Fortran version for MPI_COMM_CREATE_KEYVAL */
void mpi_comm_create_keyval_(nm_mpi_copy_subroutine_t*comm_copy_attr_fn,
                             nm_mpi_delete_subroutine_t*comm_delete_attr_fn,
                             MPI_Fint*comm_keyval, MPI_Aint*extra_state, MPI_Fint*ierr)
{
  int c_comm_keyval;
  *ierr = nm_mpi_comm_create_keyval_fort(comm_copy_attr_fn, comm_delete_attr_fn, &c_comm_keyval, extra_state);
  *comm_keyval = c_comm_keyval;
}

void mpi_comm_dup_fn_(MPI_Fint*oldcomm, MPI_Fint*comm_keyval, MPI_Aint*extra_state, MPI_Aint*attribute_val_in, MPI_Aint*attribute_val_out, MPI_Fint*flag, MPI_Fint*ierr)
{
  *flag = 1;
  *attribute_val_out = *attribute_val_in;
  *ierr = MPI_SUCCESS;
}

void mpi_comm_null_copy_fn_(MPI_Fint*oldcomm, MPI_Fint*comm_keyval, MPI_Aint*extra_state, MPI_Aint*attribute_val_in, MPI_Aint*attribute_val_out, MPI_Fint*flag, MPI_Fint*ierr)
{
  *flag = 0;
  *ierr = MPI_SUCCESS;
}

void mpi_comm_null_delete_fn_(MPI_Fint*comm, MPI_Fint*comm_keyval, MPI_Aint*attribute_val, MPI_Aint*extra_state, MPI_Fint*ierr)
{
  *ierr = MPI_SUCCESS;
}

/** Fortran version for MPI_COMM_FREE_KEYVAL */
void mpi_comm_free_keyval_(MPI_Fint*comm_keyval, MPI_Fint*ierr)
{
  int c_comm_keyval = *comm_keyval;
  *ierr = mpi_comm_free_keyval(&c_comm_keyval);
  *comm_keyval = c_comm_keyval;
}

/** Fortran version for MPI_COMM_GET_ATTR */
void mpi_comm_get_attr_(MPI_Fint*comm, MPI_Fint*keyval, MPI_Aint*attr_value, MPI_Fint*flag, MPI_Fint*ierr)
{
  int c_flag = *flag;
  void*c_attr_value;
  *ierr = mpi_comm_get_attr(*comm, *keyval, &c_attr_value, &c_flag);
  *attr_value = (MPI_Aint)c_attr_value;
  *flag = c_flag;
}

/** Fortran version for MPI_COMM_SET_ATTR */
void mpi_comm_set_attr_(MPI_Fint*comm, MPI_Fint*keyval, MPI_Aint*attr_value, MPI_Fint*ierr)
{
  *ierr = mpi_comm_set_attr(*comm, *keyval, (void*)(*attr_value));
}

/** Fortran version for MPI_COMM_DELETE_ATTR */
void mpi_comm_delete_attr_(MPI_Fint*comm, MPI_Fint*comm_keyval, MPI_Fint*ierr)
{
  *ierr = mpi_comm_delete_attr(*comm, *comm_keyval);
}

/** Fortran version for MPI_COMM_SET_NAME */
void mpi_comm_set_name_(MPI_Fint*comm, char*comm_name, MPI_Fint*ierr, int comm_name_len)
{
  char*c_comm_name = nm_mpif_string_f2c(comm_name, comm_name_len);
  *ierr = mpi_comm_set_name(*comm, c_comm_name);
  padico_free(c_comm_name);
}

/** Fortran version for MPI_COMM_GET_NAME */
void mpi_comm_get_name_(MPI_Fint*comm, char*comm_name, MPI_Fint*resultlen, MPI_Fint*ierr, int comm_name_len)
{
  int c_resultlen;
  char*c_comm_name = padico_malloc(sizeof(char) * (comm_name_len + 1));
  *ierr = mpi_comm_get_name(*comm, c_comm_name, &c_resultlen);
  *resultlen = c_resultlen;
  nm_mpif_string_c2f(c_comm_name, comm_name, comm_name_len);
  padico_free(c_comm_name);
}

/** Fortran version for MPI_GET_PROCESSOR_NAME */
void mpi_get_processor_name_(char*name, MPI_Fint*resultlen, MPI_Fint*ierr, int name_len)
{
  int c_resultlen;
  char*c_name = padico_malloc(sizeof(char) * (name_len + 1));
  mpi_get_processor_name(c_name, &c_resultlen);
  *resultlen = c_resultlen;
  nm_mpif_string_c2f(c_name, name, name_len);
  padico_free(c_name);
}

/** Fortran version for MPI_SEND */
void mpi_send_(void*buffer, MPI_Fint*count, MPI_Fint*datatype, MPI_Fint*dest, MPI_Fint*tag, MPI_Fint*comm, MPI_Fint*ierr)
{
  NM_MPI_F2C_BOTTOM(buffer);
  *ierr = mpi_send(buffer, *count, *datatype, *dest, *tag, *comm);
}

/** Fortran version for MPI_ISEND */
void mpi_isend_(void*buffer, MPI_Fint*count, MPI_Fint*datatype, MPI_Fint*dest, MPI_Fint*tag, MPI_Fint*comm, MPI_Fint*request, MPI_Fint*ierr)
{
  MPI_Request c_request;
  NM_MPI_F2C_BOTTOM(buffer);
  *ierr = mpi_isend(buffer, *count, *datatype, *dest, *tag, *comm, &c_request);
  *request = c_request;
}

/** Fortran version for MPI_ISSEND */
void mpi_issend_(void*buffer, MPI_Fint*count, MPI_Fint*datatype, MPI_Fint*dest, MPI_Fint*tag, MPI_Fint*comm, MPI_Fint*request, MPI_Fint*ierr)
{
  MPI_Request c_request;
  NM_MPI_F2C_BOTTOM(buffer);
  *ierr = mpi_issend(buffer, *count, *datatype, *dest, *tag, *comm, &c_request);
  *request = c_request;
}

/** Fortran version for MPI_IRSEND */
void mpi_irsend_(void*buffer, MPI_Fint*count, MPI_Fint*datatype, MPI_Fint*dest, MPI_Fint*tag, MPI_Fint*comm, MPI_Fint*request, MPI_Fint*ierr)
{
  MPI_Request c_request;
  NM_MPI_F2C_BOTTOM(buffer);
  *ierr = mpi_irsend(buffer, *count, *datatype, *dest, *tag, *comm, &c_request);
  *request = c_request;
}

/** Fortran version for MPI_RSEND */
void mpi_rsend_(void*buffer, MPI_Fint*count, MPI_Fint*datatype, MPI_Fint*dest, MPI_Fint*tag, MPI_Fint*comm, MPI_Fint*ierr)
{
  NM_MPI_F2C_BOTTOM(buffer);
  *ierr = mpi_rsend(buffer, *count, *datatype, *dest, *tag, *comm);
}

/** Fortran version for MPI_SSEND */
void mpi_ssend_(void*buffer, MPI_Fint*count, MPI_Fint*datatype, MPI_Fint*dest, MPI_Fint*tag, MPI_Fint*comm, MPI_Fint*ierr)
{
  NM_MPI_F2C_BOTTOM(buffer);
  *ierr = mpi_ssend(buffer, *count, *datatype, *dest, *tag, *comm);
}

/** Fortran version for MPI_RECV */
void mpi_recv_(void*buffer, MPI_Fint*count, MPI_Fint*datatype, MPI_Fint*source, MPI_Fint*tag, MPI_Fint*comm, MPI_Fint*status, MPI_Fint*ierr)
{
  NM_MPI_F_STATUS_DECLARE(_status);
  NM_MPI_F_STATUS_SET(_status, status);
  NM_MPI_F2C_BOTTOM(buffer);
  *ierr = mpi_recv(buffer, *count, *datatype, *source, *tag, *comm, _status);
  NM_MPI_F_STATUS_RETURN(_status, status);
}

/** Fortran version for MPI_IRECV */
void mpi_irecv_(void*buffer, MPI_Fint*count, MPI_Fint*datatype, MPI_Fint*source, MPI_Fint*tag, MPI_Fint*comm, MPI_Fint*request, MPI_Fint*ierr)
{
  MPI_Request c_request;
  NM_MPI_F2C_BOTTOM(buffer);
  *ierr = mpi_irecv(buffer, *count, *datatype, *source, *tag, *comm, &c_request);
  *request = c_request;
}

/** Fortran version for MPI_SENDRECV */
void mpi_sendrecv_(void*sendbuf, MPI_Fint*sendcount, MPI_Fint*sendtype, MPI_Fint*dest, MPI_Fint*sendtag,
                   void*recvbuf, MPI_Fint*recvcount, MPI_Fint*recvtype, MPI_Fint*source, MPI_Fint*recvtag,
                   MPI_Fint*comm, MPI_Fint*status, MPI_Fint*ierr)
{
  NM_MPI_F_STATUS_DECLARE(_status);
  NM_MPI_F_STATUS_SET(_status, status);
  NM_MPI_F2C_BOTTOM(sendbuf);
  NM_MPI_F2C_BOTTOM(recvbuf);
  *ierr = mpi_sendrecv(sendbuf, *sendcount, *sendtype, *dest, *sendtag,
                       recvbuf, *recvcount, *recvtype, *source, *recvtag, *comm,
                       _status);
  NM_MPI_F_STATUS_RETURN(_status, status);
}

/** Fortran version for MPI_SENDRECV_REPLACE*/
void mpi_sendrecv_replace_(void*buf, MPI_Fint*count, MPI_Fint*datatype, MPI_Fint*dest, MPI_Fint*sendtag, MPI_Fint*source, MPI_Fint*recvtag, MPI_Fint*comm, MPI_Fint*status, MPI_Fint*ierr)
{
  NM_MPI_F_STATUS_DECLARE(_status);
  NM_MPI_F_STATUS_SET(_status, status);
  NM_MPI_F2C_BOTTOM(buf);
  *ierr = mpi_sendrecv_replace(buf, *count, *datatype, *dest, *sendtag, *source, *recvtag, *comm, _status);
  NM_MPI_F_STATUS_RETURN(_status, status);
}

/** Fortran version for MPI_PACK */
void mpi_pack_(void*inbuf, MPI_Fint*incount, MPI_Fint*datatype, void*outbuf, MPI_Fint*outcount, MPI_Fint*position, MPI_Fint*comm, MPI_Fint*ierr)
{
  int c_position = *position;
  NM_MPI_F2C_BOTTOM(inbuf);
  *ierr = mpi_pack(inbuf, *incount, *datatype, outbuf, *outcount, &c_position, *comm);
  *position = c_position;
}

/** Fortran version for MPI_UNPACK */
void mpi_unpack_(void*inbuf, MPI_Fint*insize, MPI_Fint*position, void*outbuf, MPI_Fint*outcount, MPI_Fint*datatype, MPI_Fint*comm, MPI_Fint*ierr)
{
  int c_position = *position;
  NM_MPI_F2C_BOTTOM(outbuf);
  *ierr = mpi_unpack(inbuf, *insize, &c_position, outbuf, *outcount, *datatype, *comm);
  *position = c_position;
}

/** Fortran version for MPI_PACK_SIZE */
void mpi_pack_size_(MPI_Fint*incount, MPI_Fint*datatype, MPI_Fint*comm, MPI_Fint*size, MPI_Fint*ierr)
{
  int c_size = *size;
  *ierr = mpi_pack_size(*incount, *datatype, *comm, &c_size);
  *size = c_size;
}

/** Fortran version for MPI_TYPE_SET_NAME */
void mpi_type_set_name_(MPI_Fint*datatype, char*type_name, MPI_Fint*ierr, int type_name_len)
{
  char*c_type_name = nm_mpif_string_f2c(type_name, type_name_len);
  *ierr = mpi_type_set_name(*datatype, c_type_name);
  padico_free(c_type_name);
}

/** Fortran version for MPI_TYPE_GET_NAME */
void mpi_type_get_name_(MPI_Fint*datatype, char*type_name, MPI_Fint*resultlen, MPI_Fint*ierr, int type_name_len)
{
  int c_resultlen;
  char*c_type_name = padico_malloc(sizeof(char) * (type_name_len + 1));
  *ierr = mpi_type_get_name(*datatype, c_type_name, &c_resultlen);
  *resultlen = c_resultlen;
  nm_mpif_string_c2f(c_type_name, type_name, type_name_len);
  padico_free(c_type_name);
}

/** Fortran version for MPI_TYPE_CREATE_KEYVAL */
void mpi_type_create_keyval_(nm_mpi_copy_subroutine_t*type_copy_attr_fn,
                             nm_mpi_delete_subroutine_t*type_delete_attr_fn,
                             MPI_Fint*type_keyval, void*extra_state, MPI_Fint*ierr)
{
  int c_type_keyval;
  *ierr = nm_mpi_type_create_keyval_fort(type_copy_attr_fn, type_delete_attr_fn, &c_type_keyval, extra_state);
  *type_keyval = c_type_keyval;
}

void mpi_type_dup_fn_(MPI_Fint*oldtype, MPI_Fint*type_keyval, MPI_Aint*extra_state, MPI_Aint*attribute_val_in, MPI_Aint*attribute_val_out, MPI_Fint*flag, MPI_Fint*ierr)
{
  *flag = 1;
  *attribute_val_out = *attribute_val_in;
  *ierr = MPI_SUCCESS;
}

void mpi_type_null_copy_fn_(MPI_Fint*oldtype, MPI_Fint*type_keyval, MPI_Aint*extra_state, MPI_Aint*attribute_val_in, MPI_Aint*attribute_val_out, MPI_Fint*flag, MPI_Fint*ierr)
{
  *flag = 0;
  *ierr = MPI_SUCCESS;
}

void mpi_type_null_delete_fn_(MPI_Fint*datatype, MPI_Fint*type_keyval, MPI_Aint*attribute_val, MPI_Aint*extra_state, MPI_Fint*ierr)
{
  *ierr = MPI_SUCCESS;
}

/** Fortran version for MPI_TYPE_FREE_KEYVAL */
void mpi_type_free_keyval_(MPI_Fint*keyval, MPI_Fint*ierr)
{
  int c_keyval = *keyval;
  *ierr = mpi_type_free_keyval(&c_keyval);
  *keyval = c_keyval;
}

/** Fortran version for MPI_TYPE_DELETE_ATTR */
void mpi_type_delete_attr_(MPI_Fint*datatype, MPI_Fint*keyval, MPI_Fint*ierr)
{
  *ierr = mpi_type_delete_attr(*datatype, *keyval);
}

/** Fortran version for MPI_TYPE_SET_ATTR */
void mpi_type_set_attr_(MPI_Fint*datatype, MPI_Fint*type_keyval, MPI_Aint*attribute_val,  MPI_Fint*ierr)
{
  *ierr = mpi_type_set_attr(*datatype, *type_keyval, (void*)(*attribute_val));
}

/** Fortran version for MPI_TYPE_GET_ATTR */
void mpi_type_get_attr_(MPI_Fint*datatype, MPI_Fint*type_keyval, MPI_Aint*attribute_val, MPI_Fint*flag, MPI_Fint*ierr)
{
  int c_flag = *flag;
  void*c_attribute_val;
  *ierr = mpi_type_get_attr(*datatype, *type_keyval, &c_attribute_val, &c_flag);
  *attribute_val = (MPI_Aint)c_attribute_val;
  *flag = c_flag;
}

/** Fortran stub for MPI_PACK_EXTERNAL (not implemented) */
void mpi_pack_external_(char*datarep, void*inbuf, MPI_Fint*incount, MPI_Fint*datatype, void*outbuf, MPI_Fint*outsize, MPI_Fint*position, MPI_Fint*ierr)
{
  *ierr = MPI_ERR_UNKNOWN;
}

/** Fortran stub for MPI_UNPACK_EXTERNAL (not implemented) */
void mpi_unpack_external_(char*datarep, void*inbuf, MPI_Fint*incount, MPI_Fint*datatype, void*outbuf, MPI_Fint*outsize, MPI_Fint*position, MPI_Fint*ierr)
{
  *ierr = MPI_ERR_UNKNOWN;
}

/** Fortran stub for MPI_PACK_EXTERNAL_SIZE (not implemented) */
void mpi_pack_external_size_(char*datarep, MPI_Fint*incount, MPI_Fint*datatype, MPI_Fint*size, MPI_Fint*ierr)
{
  *ierr = MPI_ERR_UNKNOWN;
}

/** Fortran stub for MPI_BSEND (not implemented) */
void mpi_bsend_(void*buf, MPI_Fint*count, MPI_Fint*datatype, MPI_Fint*dest, MPI_Fint*tag, MPI_Fint*comm, MPI_Fint*ierr)
{
  *ierr = MPI_ERR_UNKNOWN;
}

/** Fortran stub for MPI_BSEND_INIT (not implemented) */
void mpi_bsend_init_(void*buf, MPI_Fint*count, MPI_Fint*datatype, MPI_Fint*dest, MPI_Fint*tag, MPI_Fint*comm, MPI_Fint*request, MPI_Fint*ierr)
{
  *ierr = MPI_ERR_UNKNOWN;
}

/** Fortran stub for MPI_BUFFER_ATTACH (not implemented) */
void mpi_buffer_attach_(void*buffer, MPI_Fint*size, MPI_Fint*ierr)
{
  *ierr = MPI_ERR_UNKNOWN;
}

/** Fortran stub for MPI_BUFFER_DETACH (not implemented) */
void mpi_buffer_detach_(void*buffer, MPI_Fint*size, MPI_Fint*ierr)
{
  *ierr = MPI_ERR_UNKNOWN;
}

/** Fortran stub for MPI_IBSEND (not implemented) */
void mpi_ibsend_(void*buf, MPI_Fint*count, MPI_Fint*datatype, MPI_Fint*dest, MPI_Fint*tag, MPI_Fint*comm, MPI_Fint*request, MPI_Fint*ierr)
{
  *ierr = MPI_ERR_UNKNOWN;
}

/** Fortran version for MPI_WAIT */
void mpi_wait_(MPI_Fint*request, MPI_Fint*status, MPI_Fint*ierr)
{
  NM_MPI_F_STATUS_DECLARE(_status);
  MPI_Request c_request = *request;
  NM_MPI_F_STATUS_SET(_status, status);
  *ierr = mpi_wait(&c_request, _status);
  NM_MPI_F_STATUS_RETURN(_status, status);
  *request = c_request;
}

/** Fortran version for MPI_WAITSOME */
void mpi_waitsome_(MPI_Fint*incount, MPI_Fint*array_of_requests, MPI_Fint*outcount, MPI_Fint*array_of_indices, MPI_Fint array_of_statuses[][MPI_STATUS_SIZE], MPI_Fint*ierr)
{
  MPI_Status*_statuses;
  int*c_requests = nm_mpif_array_f2c(array_of_requests, *incount);
  int*c_indices = nm_mpif_array_f2c(array_of_indices, *incount);
  int c_outcount;
  if((MPI_Fint*)array_of_statuses != MPI_F_STATUSES_IGNORE)
    {
      _statuses = padico_malloc(*incount * sizeof(MPI_Status));
    }
  else
    {
      _statuses = MPI_STATUSES_IGNORE;
    }
  *ierr = mpi_waitsome(*incount, c_requests, &c_outcount, c_indices, _statuses);
  if((MPI_Fint*)array_of_statuses != MPI_F_STATUSES_IGNORE)
    {
      for (int i = 0; i < c_outcount; i++)
        {
          mpi_status_c2f(&_statuses[i], array_of_statuses[i]);
        }
    }
  nm_mpif_array_c2f(c_requests, array_of_requests, c_outcount);
  for (int i = 0; i < c_outcount; i++)
    {
      c_indices[i] += 1;
    }
  nm_mpif_array_c2f(c_indices, array_of_indices, c_outcount);
  *outcount = c_outcount;
  padico_free(c_requests);
  padico_free(c_indices);
  if((MPI_Fint*)array_of_statuses != MPI_F_STATUSES_IGNORE)
    {
      padico_free(_statuses);
    }
}

/** Fortran version for MPI_WAIT_ALL */
void mpi_waitall_(MPI_Fint*count, MPI_Fint*array_of_requests, MPI_Fint array_of_statuses[][MPI_STATUS_SIZE], MPI_Fint*ierr)
{
  int i;
  MPI_Status*_statuses;
  int*c_requests = nm_mpif_array_f2c(array_of_requests, *count);
  if((MPI_Fint*)array_of_statuses != MPI_F_STATUSES_IGNORE)
    {
      _statuses = padico_malloc(*count * sizeof(MPI_Status));
    }
  else
    {
      _statuses = MPI_STATUSES_IGNORE;
    }
  *ierr = mpi_waitall(*count, c_requests, _statuses);
  if((MPI_Fint*)array_of_statuses != MPI_F_STATUSES_IGNORE)
    {
      for (i = 0; i < *count; i++)
        {
          mpi_status_c2f(&_statuses[i], array_of_statuses[i]);
        }
    }
  nm_mpif_array_c2f(c_requests, array_of_requests, *count);
  padico_free(c_requests);
  if((MPI_Fint*)array_of_statuses != MPI_F_STATUSES_IGNORE)
    {
      padico_free(_statuses);
    }
}

/** Fortran version for MPI_WAIT_ANY */
void mpi_waitany_(MPI_Fint*count, MPI_Fint array_of_requests[], MPI_Fint*rqindex, MPI_Fint*status, MPI_Fint*ierr)
{
  NM_MPI_F_STATUS_DECLARE(_status);
  int c_rqindex;
  int*c_requests = nm_mpif_array_f2c(array_of_requests, *count);
  NM_MPI_F_STATUS_SET(_status, status);
  *ierr = mpi_waitany(*count, c_requests, &c_rqindex, _status);
  NM_MPI_F_STATUS_RETURN(_status, status);
  nm_mpif_array_c2f(c_requests, array_of_requests, *count);
  if(c_rqindex != MPI_UNDEFINED)
    c_rqindex += 1;
  *rqindex = c_rqindex;
  padico_free(c_requests);
}

/** Fortran version for MPI_TEST */
void mpi_test_(MPI_Fint*request, MPI_Fint*flag, MPI_Fint*status, MPI_Fint*ierr)
{
  NM_MPI_F_STATUS_DECLARE(c_status);
  MPI_Request c_request = *request;
  int c_flag;
  NM_MPI_F_STATUS_SET(c_status, status);
  *ierr = mpi_test(&c_request, &c_flag, c_status);
  *request = c_request;
  *flag = c_flag;
  if(*ierr != MPI_SUCCESS)
    return;
  NM_MPI_F_STATUS_RETURN(c_status, status);
}

/** Fortran version for MPI_TESTANY */
void mpi_testany_(MPI_Fint*count, MPI_Fint array_of_requests[], MPI_Fint*rqindex, MPI_Fint*flag, MPI_Fint*status, MPI_Fint*ierr)
{
  NM_MPI_F_STATUS_DECLARE(_status);
  int c_flag, c_rqindex;
  NM_MPI_F_STATUS_SET(_status, status);
  int*c_requests = nm_mpif_array_f2c(array_of_requests, *count);
  *ierr = mpi_testany(*count, c_requests, &c_rqindex, &c_flag, _status);
  NM_MPI_F_STATUS_RETURN(_status, status);
  *flag = c_flag;
  nm_mpif_array_c2f(c_requests, array_of_requests, *count);
  if(c_rqindex != MPI_UNDEFINED)
    c_rqindex += 1;
  *rqindex = c_rqindex;
  padico_free(c_requests);
}

/** Fortran version for MPI_TESTALL */
void mpi_testall_(MPI_Fint*count, MPI_Fint*array_of_requests, MPI_Fint*flag, MPI_Fint array_of_statuses[][MPI_STATUS_SIZE], MPI_Fint*ierr)
{
  int c_flag;
  MPI_Status*_statuses;
  int*c_requests = nm_mpif_array_f2c(array_of_requests, *count);
  if((MPI_Fint*)array_of_statuses != MPI_F_STATUSES_IGNORE)
    {
      _statuses = padico_malloc(*count * sizeof(MPI_Status));
    }
  else
    {
      _statuses = MPI_STATUSES_IGNORE;
    }
  *ierr = mpi_testall(*count, c_requests, &c_flag, _statuses);
  if((MPI_Fint*)array_of_statuses != MPI_F_STATUSES_IGNORE)
    {
      for (int i = 0; i < *count; i++)
        {
          mpi_status_c2f(&_statuses[i], array_of_statuses[i]);
        }
    }
  nm_mpif_array_c2f(c_requests, array_of_requests, *count);
  *flag = c_flag;
  padico_free(c_requests);
  if((MPI_Fint*)array_of_statuses != MPI_F_STATUSES_IGNORE)
    {
      padico_free(_statuses);
    }
}

/** Fortran version for MPI_TESTSOME */
void mpi_testsome_(MPI_Fint*count, MPI_Fint*array_of_requests, MPI_Fint*outcount, MPI_Fint*array_of_indices, MPI_Fint array_of_statuses[][MPI_STATUS_SIZE], MPI_Fint*ierr)
{
  MPI_Status*_statuses;
  int*c_requests = nm_mpif_array_f2c(array_of_requests, *count);
  int*c_indices = nm_mpif_array_f2c(array_of_indices, *count);
  int c_outcount;
  if((MPI_Fint*)array_of_statuses != MPI_F_STATUSES_IGNORE)
    {
      _statuses = padico_malloc(*count * sizeof(MPI_Status));
    }
  else
    {
      _statuses = MPI_STATUSES_IGNORE;
    }
  *ierr = mpi_testsome(*count, c_requests, &c_outcount, c_indices, _statuses);
  if((MPI_Fint*)array_of_statuses != MPI_F_STATUSES_IGNORE)
    {
      for (int i = 0; i < c_outcount; i++)
        {
          mpi_status_c2f(&_statuses[i], array_of_statuses[i]);
        }
    }
  nm_mpif_array_c2f(c_requests, array_of_requests, c_outcount);
  for (int i = 0; i < c_outcount; i++)
    {
      c_indices[i] += 1;
    }
  nm_mpif_array_c2f(c_indices, array_of_indices, c_outcount);
  *outcount = c_outcount;
  padico_free(c_indices);
  padico_free(c_requests);
  if((MPI_Fint*)array_of_statuses != MPI_F_STATUSES_IGNORE)
    {
      padico_free(_statuses);
    }
}

/** Fortran version for MPI_IPROBE */
void mpi_iprobe_(MPI_Fint*source, MPI_Fint*tag, MPI_Fint*comm, MPI_Fint*flag, MPI_Fint*status, MPI_Fint*ierr)
{
  NM_MPI_F_STATUS_DECLARE(_status);
  int c_flag;
  NM_MPI_F_STATUS_SET(_status, status);
  *ierr = mpi_iprobe(*source, *tag, *comm, &c_flag, _status);
  *flag = c_flag;
  NM_MPI_F_STATUS_RETURN(_status, status);
}

/** Fortran version for MPI_PROBE */
void mpi_probe_(MPI_Fint*source, MPI_Fint*tag, MPI_Fint*comm, MPI_Fint*status,MPI_Fint*ierr)
{
  NM_MPI_F_STATUS_DECLARE(_status);
  NM_MPI_F_STATUS_SET(_status, status);
  *ierr = mpi_probe(*source, *tag, *comm, _status);
  NM_MPI_F_STATUS_RETURN(_status, status);
}

/** Fortran version for MPI_CANCEL */
void mpi_cancel_(MPI_Fint*request, MPI_Fint*ierr)
{
  MPI_Request c_request = *request;
  *ierr = mpi_cancel(&c_request);
}

/** Fortran version for MPI_TEST_CANCELLED */
void mpi_test_cancelled_(MPI_Fint*status, MPI_Fint*flag, MPI_Fint*ierr)
{
  MPI_Status _status;
  int c_flag;
  mpi_status_f2c(status, &_status);
  *ierr = mpi_test_cancelled(&_status, &c_flag);
  *flag = c_flag;
}

/** Fortran version for MPI_REQUEST_FREE */
void mpi_request_free_(MPI_Fint*request, MPI_Fint*ierr)
{
  MPI_Request c_request = *request;
  *ierr = mpi_request_free(&c_request);
  *request = c_request;
}

/** Fortran version for MPI_GET_COUNT */
void mpi_get_count_(MPI_Fint*status, MPI_Fint*datatype, MPI_Fint*count, MPI_Fint*ierr)
{
  MPI_Status _status;
  int c_count;
  mpi_status_f2c(status, &_status);
  *ierr = mpi_get_count(&_status, *datatype, &c_count);
  *count = c_count;
}

/** Fortran version for MPI_GET_ELEMENTS_X */
void mpi_get_elements_x_(MPI_Fint*status, MPI_Fint*datatype, MPI_Count*count, MPI_Fint*ierr)
{
  MPI_Status _status;
  MPI_Count c_count;
  mpi_status_f2c(status, &_status);
  *ierr = mpi_get_elements_x(&_status, *datatype, &c_count);
  *count = c_count;
}

/** Fortran version for MPI_GET_ELEMENTS */
void mpi_get_elements_(MPI_Fint*status, MPI_Fint*datatype, MPI_Fint*count, MPI_Fint*ierr)
{
  MPI_Status _status;
  int c_count;
  mpi_status_f2c(status, &_status);
  *ierr = mpi_get_elements(&_status, *datatype, &c_count);
  *count = c_count;
}

/** Fortran version for MPI_SEND_INIT */
void mpi_send_init_(void*buf, MPI_Fint*count, MPI_Fint*datatype, MPI_Fint*dest, MPI_Fint*tag, MPI_Fint*comm, MPI_Fint*request, MPI_Fint*ierr)
{
  MPI_Request c_request = *request;
  NM_MPI_F2C_BOTTOM(buf);
  *ierr = mpi_send_init(buf, *count, *datatype, *dest, *tag, *comm, &c_request);
  *request = c_request;
}

/** Fortran version for MPI_RSEND_INIT */
void mpi_rsend_init_(void*buf, MPI_Fint*count, MPI_Fint*datatype, MPI_Fint*dest, MPI_Fint*tag, MPI_Fint*comm, MPI_Fint*request, MPI_Fint*ierr)
{
  MPI_Request c_request = *request;
  NM_MPI_F2C_BOTTOM(buf);
  *ierr = mpi_rsend_init(buf, *count, *datatype, *dest, *tag, *comm, &c_request);
  *request = c_request;
}

/** Fortran version for MPI_SSEND_INIT */
void mpi_ssend_init_(void*buf, MPI_Fint*count, MPI_Fint*datatype, MPI_Fint*dest, MPI_Fint*tag, MPI_Fint*comm, MPI_Fint*request, MPI_Fint*ierr)
{
  MPI_Request c_request = *request;
  NM_MPI_F2C_BOTTOM(buf);
  *ierr = mpi_ssend_init(buf, *count, *datatype, *dest, *tag, *comm, &c_request);
  *request = c_request;
}

/** Fortran version for MPI_RECV_INIT */
void mpi_recv_init_(void*buf, MPI_Fint*count, MPI_Fint*datatype, MPI_Fint*source, MPI_Fint*tag, MPI_Fint*comm, MPI_Fint*request, MPI_Fint*ierr)
{
  MPI_Request c_request = *request;
  NM_MPI_F2C_BOTTOM(buf);
  *ierr = mpi_recv_init(buf, *count, *datatype, *source, *tag, *comm, &c_request);
  *request = c_request;
}

/** Fortran version for MPI_IMPROBE */
void mpi_improbe_(MPI_Fint*source, MPI_Fint*tag, MPI_Fint*comm, MPI_Fint*flag, MPI_Fint*message, MPI_Fint*status, MPI_Fint*ierr)
{
  NM_MPI_F_STATUS_DECLARE(_status);
  int c_flag, c_message;
  NM_MPI_F_STATUS_SET(_status, status);
  *ierr = mpi_improbe(*source, *tag, *comm, &c_flag, &c_message, _status);
  *flag = c_flag;
  *message = c_message;
  NM_MPI_F_STATUS_RETURN(_status, status);
}

/** Fortran version for MPI_MPROBE */
void mpi_mprobe_(MPI_Fint*source, MPI_Fint*tag, MPI_Fint*comm, MPI_Fint*message, MPI_Fint*status, MPI_Fint*ierr)
{
  NM_MPI_F_STATUS_DECLARE(_status);
  int c_message;
  NM_MPI_F_STATUS_SET(_status, status);
  *ierr = mpi_mprobe(*source, *tag, *comm, &c_message, _status);
  *message = c_message;
  NM_MPI_F_STATUS_RETURN(_status, status);
}

/** Fortran version for MPI_IMRECV */
void mpi_imrecv_(void*buf, MPI_Fint*count, MPI_Fint*datatype, MPI_Fint*message, MPI_Fint*request, MPI_Fint*ierr)
{
  MPI_Request c_request = *request;
  NM_MPI_F2C_BOTTOM(buf);
  *ierr = mpi_imrecv(buf, *count, *datatype, message, &c_request);
  *request = c_request;
}

/** Fortran version for MPI_MRECV */
void mpi_mrecv_(void*buf, MPI_Fint*count, MPI_Fint*datatype, MPI_Fint*message, MPI_Fint*status, MPI_Fint*ierr)
{
  NM_MPI_F_STATUS_DECLARE(_status);
  NM_MPI_F_STATUS_SET(_status, status);
  NM_MPI_F2C_BOTTOM(buf);
  *ierr = mpi_mrecv(buf, *count, *datatype, message, _status);
  NM_MPI_F_STATUS_RETURN(_status, status);
}

/** Fortran version for MPI_START */
void mpi_start_(MPI_Fint*request, MPI_Fint*ierr)
{
  MPI_Request c_request = *request;
  *ierr = mpi_start(&c_request);
}

/** Fortran version for MPI_START_ALL */
void mpi_startall_(MPI_Fint*count, MPI_Fint array_of_requests[], MPI_Fint*ierr)
{
  int*c_requests = nm_mpif_array_f2c(array_of_requests, *count);
  *ierr = mpi_startall(*count, c_requests);
  padico_free(c_requests);
}

/** Fortran version for MPI_BARRIER */
void mpi_barrier_(MPI_Fint*comm, MPI_Fint*ierr)
{
  *ierr = mpi_barrier(*comm);
}

/** Fortran version for MPI_IBARRIER */
void mpi_ibarrier_(MPI_Fint*comm, MPI_Request*request, MPI_Fint*ierr)
{
  MPI_Request c_request;
  *ierr = mpi_ibarrier(*comm, &c_request);
  *request = c_request;
}

/** Fortran version for MPI_BCAST */
void mpi_bcast_(void*buffer, MPI_Fint*count, MPI_Fint*datatype, MPI_Fint*root, MPI_Fint*comm, MPI_Fint*ierr)
{
  NM_MPI_F2C_BOTTOM(buffer);
  *ierr = mpi_bcast(buffer, *count, *datatype, *root, *comm);
}

/** Fortran version for MPI_IBCAST */
void mpi_ibcast_(void*buffer, MPI_Fint*count, MPI_Fint*datatype, MPI_Fint*root, MPI_Fint*comm, MPI_Fint*request, MPI_Fint*ierr)
{
  MPI_Request c_request;
  NM_MPI_F2C_BOTTOM(buffer);
  *ierr = mpi_ibcast(buffer, *count, *datatype, *root, *comm, &c_request);
  *request = c_request;
}

/** Fortran version for MPI_GATHER */
void mpi_gather_(void*sendbuf, MPI_Fint*sendcount, MPI_Fint*sendtype,
                 void*recvbuf, MPI_Fint*recvcount, MPI_Fint*recvtype,
                 MPI_Fint*root, MPI_Fint*comm, MPI_Fint*ierr)
{
  NM_MPI_F2C_BOTTOM(sendbuf);
  NM_MPI_F2C_BOTTOM(recvbuf);
  if(sendbuf == NM_MPI_F_MPI_IN_PLACE)
    {
      sendbuf = MPI_IN_PLACE;
    }
  *ierr = mpi_gather(sendbuf, *sendcount, *sendtype, recvbuf, *recvcount, *recvtype, *root, *comm);
}

/** Fortran version for MPI_IGATHER */
void mpi_igather_(void*sendbuf, MPI_Fint*sendcount, MPI_Fint*sendtype,
                  void*recvbuf, MPI_Fint*recvcount, MPI_Fint*recvtype,
                  MPI_Fint*root, MPI_Fint*comm, MPI_Fint*request, MPI_Fint*ierr)
{
  MPI_Request c_request;
  NM_MPI_F2C_BOTTOM(sendbuf);
  NM_MPI_F2C_BOTTOM(recvbuf);
  if(sendbuf == NM_MPI_F_MPI_IN_PLACE)
    {
      sendbuf = MPI_IN_PLACE;
    }
  *ierr = mpi_igather(sendbuf, *sendcount, *sendtype, recvbuf, *recvcount, *recvtype, *root, *comm, &c_request);
  *request = c_request;
}

/** Fortran version for MPI_GATHERV */
void mpi_gatherv_(void*sendbuf, MPI_Fint*sendcount, MPI_Fint*sendtype,
                  void*recvbuf, MPI_Fint recvcounts[], MPI_Fint displs[], MPI_Fint*recvtype,
                  MPI_Fint*root, MPI_Fint*comm, MPI_Fint*ierr)
{
  int comm_size;
  mpi_comm_size(*comm, &comm_size);
  int*c_recvcounts = nm_mpif_array_f2c(recvcounts, comm_size);
  int*c_displs = nm_mpif_array_f2c(displs, comm_size);
  NM_MPI_F2C_BOTTOM(sendbuf);
  NM_MPI_F2C_BOTTOM(recvbuf);
  if(sendbuf == NM_MPI_F_MPI_IN_PLACE)
    {
      sendbuf = MPI_IN_PLACE;
    }
  *ierr = mpi_gatherv(sendbuf, *sendcount, *sendtype,
                      recvbuf, c_recvcounts, c_displs, *recvtype, *root, *comm);
  padico_free(c_recvcounts);
  padico_free(c_displs);
}

/** Fortran version for MPI_ALLGATHER */
void mpi_allgather_(void*sendbuf, MPI_Fint*sendcount, MPI_Fint*sendtype,
                    void*recvbuf, MPI_Fint*recvcount, MPI_Fint*recvtype,
                    MPI_Fint*comm, MPI_Fint*ierr)
{
  NM_MPI_F2C_BOTTOM(sendbuf);
  NM_MPI_F2C_BOTTOM(recvbuf);
  if(sendbuf == NM_MPI_F_MPI_IN_PLACE)
    {
      sendbuf = MPI_IN_PLACE;
    }
  *ierr = mpi_allgather(sendbuf, *sendcount, *sendtype, recvbuf, *recvcount,
                        *recvtype, *comm);
}

/** Fortran version for MPI_IALLGATHER */
void mpi_iallgather_(void*sendbuf, MPI_Fint*sendcount, MPI_Fint*sendtype,
                     void*recvbuf, MPI_Fint*recvcount, MPI_Fint*recvtype,
                     MPI_Fint*comm, MPI_Fint*request, MPI_Fint*ierr)
{
  MPI_Request c_request;
  NM_MPI_F2C_BOTTOM(sendbuf);
  NM_MPI_F2C_BOTTOM(recvbuf);
  if(sendbuf == NM_MPI_F_MPI_IN_PLACE)
    {
      sendbuf = MPI_IN_PLACE;
    }
  *ierr = mpi_iallgather(sendbuf, *sendcount, *sendtype, recvbuf, *recvcount, *recvtype, *comm, &c_request);
  *request = c_request;
}

/** Fortran version for MPI_ALLGATHERV */
void mpi_allgatherv_(void*sendbuf, MPI_Fint*sendcount, MPI_Fint*sendtype,
                     void *recvbuf, MPI_Fint*recvcounts, MPI_Fint*displs, MPI_Fint*recvtype,
                     MPI_Fint*comm, MPI_Fint*ierr)
{
  int comm_size;
  mpi_comm_size(*comm, &comm_size);
  int*c_recvcounts = nm_mpif_array_f2c(recvcounts, comm_size);
  int*c_displs = nm_mpif_array_f2c(displs, comm_size);
  NM_MPI_F2C_BOTTOM(sendbuf);
  NM_MPI_F2C_BOTTOM(recvbuf);
  if(sendbuf == NM_MPI_F_MPI_IN_PLACE)
    {
      sendbuf = MPI_IN_PLACE;
    }
  *ierr = mpi_allgatherv(sendbuf, *sendcount, *sendtype,
                         recvbuf, c_recvcounts, c_displs, *recvtype, *comm);
  padico_free(c_recvcounts);
  padico_free(c_displs);
}

/** Fortran version for MPI_SCATTER */
void mpi_scatter_(void*sendbuf, MPI_Fint*sendcount, MPI_Fint*sendtype,
                  void*recvbuf, MPI_Fint*recvcount, MPI_Fint*recvtype,
                  MPI_Fint*root, MPI_Fint*comm, MPI_Fint*ierr)
{
  NM_MPI_F2C_BOTTOM(sendbuf);
  NM_MPI_F2C_BOTTOM(recvbuf);
  if(recvbuf == NM_MPI_F_MPI_IN_PLACE)
    {
      recvbuf = MPI_IN_PLACE;
    }
  *ierr = mpi_scatter(sendbuf, *sendcount, *sendtype, recvbuf, *recvcount,
                      *recvtype, *root, *comm);
}

/** Fortran version of MPI_Scatterv*/
void mpi_scatterv_(void*sendbuf, MPI_Fint*sendcounts, MPI_Fint*displs, MPI_Fint*sendtype,
                   void*recvbuf, MPI_Fint*recvcount, MPI_Fint*recvtype,
                   MPI_Fint*root, MPI_Fint*comm, MPI_Fint*ierr)
{
  int comm_size;
  mpi_comm_size(*comm, &comm_size);
  int*c_sendcounts = nm_mpif_array_f2c(sendcounts, comm_size);
  int*c_displs = nm_mpif_array_f2c(displs, comm_size);
  NM_MPI_F2C_BOTTOM(sendbuf);
  NM_MPI_F2C_BOTTOM(recvbuf);
  if(recvbuf == NM_MPI_F_MPI_IN_PLACE)
    {
      recvbuf = MPI_IN_PLACE;
    }
  *ierr = mpi_scatterv(sendbuf, c_sendcounts, c_displs, *sendtype,
                       recvbuf, *recvcount, *recvtype,
                       *root, *comm);
  padico_free(c_sendcounts);
  padico_free(c_displs);
}

/** Fortran version for MPI_ALLTOALL */
void mpi_alltoall_(void*sendbuf, MPI_Fint*sendcount, MPI_Fint*sendtype,
                   void*recvbuf, MPI_Fint*recvcount, MPI_Fint*recvtype,
                   MPI_Fint*comm, MPI_Fint*ierr)
{
  NM_MPI_F2C_BOTTOM(sendbuf);
  NM_MPI_F2C_BOTTOM(recvbuf);
  if(sendbuf == NM_MPI_F_MPI_IN_PLACE)
    {
      sendbuf = MPI_IN_PLACE;
    }
  *ierr = mpi_alltoall(sendbuf, *sendcount, *sendtype, recvbuf, *recvcount,
                       *recvtype, *comm);
}

/** Fortran version for MPI_IALLTOALL */
void mpi_ialltoall_(void*sendbuf, MPI_Fint*sendcount, MPI_Fint*sendtype,
                    void*recvbuf, MPI_Fint*recvcount, MPI_Fint*recvtype,
                    MPI_Fint*comm, MPI_Fint*request, MPI_Fint*ierr)
{
  MPI_Request c_request;
  NM_MPI_F2C_BOTTOM(sendbuf);
  NM_MPI_F2C_BOTTOM(recvbuf);
  if(sendbuf == NM_MPI_F_MPI_IN_PLACE)
    {
      sendbuf = MPI_IN_PLACE;
    }
  *ierr = mpi_ialltoall(sendbuf, *sendcount, *sendtype, recvbuf, *recvcount,
                        *recvtype, *comm, &c_request);
  *request = c_request;
}

/** Fortran version for MPI_ALLTOALLV */
void mpi_alltoallv_(void*sendbuf, MPI_Fint*sendcounts, MPI_Fint*sdispls, MPI_Fint*sendtype,
                    void*recvbuf, MPI_Fint*recvcounts, MPI_Fint*rdispls, MPI_Fint*recvtype,
                    MPI_Fint*comm, MPI_Fint*ierr)
{
  int comm_size;
  mpi_comm_size(*comm, &comm_size);
  int*c_sdispls = nm_mpif_array_f2c(sdispls, comm_size);
  int*c_sendcounts = nm_mpif_array_f2c(sendcounts, comm_size);
  int*c_rdispls = nm_mpif_array_f2c(rdispls, comm_size);
  int*c_recvcounts = nm_mpif_array_f2c(recvcounts, comm_size);
  NM_MPI_F2C_BOTTOM(sendbuf);
  NM_MPI_F2C_BOTTOM(recvbuf);
  if(sendbuf == NM_MPI_F_MPI_IN_PLACE)
    {
      sendbuf = MPI_IN_PLACE;
    }
  *ierr = mpi_alltoallv(sendbuf, c_sendcounts, c_sdispls, *sendtype, recvbuf,
                        c_recvcounts, c_rdispls, *recvtype, *comm);
  padico_free(c_sdispls);
  padico_free(c_rdispls);
  padico_free(c_sendcounts);
  padico_free(c_recvcounts);
}

/** Fortran version for MPI_OP_CREATE */
void mpi_op_create_(MPI_User_function*function, MPI_Fint*commute, MPI_Fint*op, MPI_Fint*ierr)
{
  MPI_Op c_op;
  *ierr = mpi_op_create(function, *commute, &c_op);
  *op = c_op;
}

/** Fortran version for MPI_OP_FREE */
void mpi_op_free_(MPI_Fint*op, MPI_Fint*ierr)
{
  MPI_Op c_op = *op;
  *ierr = mpi_op_free(&c_op);
  *op = c_op;
}

/** Fortran version for MPI_REDUCE */
void mpi_reduce_(void*sendbuf, void*recvbuf, MPI_Fint*count, MPI_Fint*datatype, MPI_Fint*op,
                 MPI_Fint*root, MPI_Fint*comm, MPI_Fint*ierr)
{
  NM_MPI_F2C_BOTTOM(sendbuf);
  NM_MPI_F2C_BOTTOM(recvbuf);
  if(sendbuf == NM_MPI_F_MPI_IN_PLACE)
    {
      sendbuf = MPI_IN_PLACE;
    }
  *ierr = mpi_reduce(sendbuf, recvbuf, *count, *datatype, *op, *root, *comm);
}

/** Fortran version for MPI_REDUCE_LOCAL */
void mpi_reduce_local_(void*sendbuf, void*recvbuf, MPI_Fint*count, MPI_Fint*datatype, MPI_Fint*op, MPI_Fint*ierr)
{
  NM_MPI_F2C_BOTTOM(sendbuf);
  NM_MPI_F2C_BOTTOM(recvbuf);
  *ierr = mpi_reduce_local(sendbuf, recvbuf, *count, *datatype, *op);
}

/** Fortran version for MPI_IREDUCE */
void mpi_ireduce_(void*sendbuf, void*recvbuf, MPI_Fint*count, MPI_Fint*datatype, MPI_Fint*op,
                 MPI_Fint*root, MPI_Fint*comm, MPI_Fint*request, MPI_Fint*ierr)
{
  MPI_Request c_request;
  NM_MPI_F2C_BOTTOM(sendbuf);
  NM_MPI_F2C_BOTTOM(recvbuf);
  if(sendbuf == NM_MPI_F_MPI_IN_PLACE)
    {
      sendbuf = MPI_IN_PLACE;
    }
  *ierr = mpi_ireduce(sendbuf, recvbuf, *count, *datatype, *op, *root, *comm, &c_request);
  *request = c_request;
}

/** Fortran version for MPI_SCAN */
void mpi_scan_(void*sendbuf, void*recvbuf, MPI_Fint*count, MPI_Fint*datatype, MPI_Fint*op, MPI_Fint*comm, MPI_Fint*ierr)
{
  NM_MPI_F2C_BOTTOM(sendbuf);
  NM_MPI_F2C_BOTTOM(recvbuf);
  if(sendbuf == NM_MPI_F_MPI_IN_PLACE)
    {
      sendbuf = MPI_IN_PLACE;
    }
  *ierr = mpi_scan(sendbuf, recvbuf, *count, *datatype, *op, *comm);
}

/** Fortran version for MPI_ALLREDUCE */
void mpi_allreduce_(void*sendbuf, void*recvbuf, MPI_Fint*count, MPI_Fint*datatype, MPI_Fint*op,
                    MPI_Fint*comm, MPI_Fint*ierr)
{
  NM_MPI_F2C_BOTTOM(sendbuf);
  NM_MPI_F2C_BOTTOM(recvbuf);
  if(sendbuf == NM_MPI_F_MPI_IN_PLACE)
    {
      sendbuf = MPI_IN_PLACE;
    }
  *ierr = mpi_allreduce(sendbuf, recvbuf, *count, *datatype, *op, *comm);
}

/** Fortran version for MPI_IALLREDUCE */
void mpi_iallreduce_(void*sendbuf, void*recvbuf, MPI_Fint*count, MPI_Fint*datatype, MPI_Fint*op,
                     MPI_Fint*comm, MPI_Fint*request, MPI_Fint*ierr)
{
  MPI_Request c_request;
  NM_MPI_F2C_BOTTOM(sendbuf);
  NM_MPI_F2C_BOTTOM(recvbuf);
  if(sendbuf == NM_MPI_F_MPI_IN_PLACE)
    {
      sendbuf = MPI_IN_PLACE;
    }
  *ierr = mpi_iallreduce(sendbuf, recvbuf, *count, *datatype, *op, *comm, &c_request);
  *request = c_request;
}

/** Fortran version for MPI_REDUCE_SCATTER */
void mpi_reduce_scatter_(void*sendbuf, void*recvbuf, MPI_Fint*recvcounts, MPI_Fint*datatype, MPI_Fint*op,
                         MPI_Fint*comm, MPI_Fint*ierr)
{
  int comm_size;
  mpi_comm_size(*comm, &comm_size);
  int*c_recvcounts = nm_mpif_array_f2c(recvcounts, comm_size);
  NM_MPI_F2C_BOTTOM(sendbuf);
  NM_MPI_F2C_BOTTOM(recvbuf);
  if(sendbuf == NM_MPI_F_MPI_IN_PLACE)
    {
      sendbuf = MPI_IN_PLACE;
    }
  *ierr = mpi_reduce_scatter(sendbuf, recvbuf, c_recvcounts, *datatype, *op, *comm);
  padico_free(c_recvcounts);
}

/** Fortran version for MPI_WTIME */
double mpi_wtime_(void)
{
  return mpi_wtime();
}

/** Fortran version for MPI_WTICK */
double mpi_wtick_(void)
{
  return mpi_wtick();
}

/** Fortran version for MPI_ERROR_CLASS */
void mpi_error_class_(MPI_Fint*errorcode, MPI_Fint*errorclass, MPI_Fint*ierr)
{
  int c_errorclass;
  *ierr = mpi_error_class(*errorcode, &c_errorclass);
  *errorclass = c_errorclass;
}

/** Fortran version for  MPI_ERROR_STRING */
void mpi_error_string_(MPI_Fint*errorcode, char*string, MPI_Fint*resultlen, MPI_Fint*ierr, int string_len)
{
  int c_resultlen = *resultlen;
  char*c_string = padico_malloc(sizeof(char) * (string_len + 1));
  *ierr = mpi_error_string(*errorcode, c_string, &c_resultlen);
  *resultlen = c_resultlen;
  nm_mpif_string_c2f(c_string, string, string_len);
  padico_free(c_string);
}

/** Fortran version for MPI_COMM_SET_ERRHANDLER */
void mpi_comm_set_errhandler_(MPI_Fint*comm, MPI_Fint*errhandler, MPI_Fint*ierr)
{
  *ierr = mpi_comm_set_errhandler(*comm, *errhandler);
}

/** Fortran version for MPI_COMM_GET_ERRHANDLER */
void mpi_comm_get_errhandler_(MPI_Fint*comm, MPI_Fint*errhandler, MPI_Fint*ierr)
{
  MPI_Errhandler c_errhandler;
  *ierr = mpi_comm_get_errhandler(*comm, &c_errhandler);
  *errhandler = c_errhandler;
}

/** Fortran version for MPI_COMM_CALL_ERRHANDLER */
void mpi_comm_call_errhandler_(MPI_Fint*comm, MPI_Fint*errorcode, MPI_Fint*ierr)
{
  *ierr = mpi_comm_call_errhandler(*comm, *errorcode);
}

/** Fortran version for  MPI_ERRHANDLER_SET */
void mpi_errhandler_set_(MPI_Fint*comm, MPI_Fint*errhandler, MPI_Fint*ierr)
{
  *ierr = mpi_errhandler_set(*comm, *errhandler);
}

/** Fortran version for MPI_ERRHANDLER_FREE */
void mpi_errhandler_free_(MPI_Fint*errhandler, MPI_Fint*ierr)
{
  MPI_Errhandler c_errhandler = *errhandler;
  *ierr = mpi_errhandler_free(&c_errhandler);
  *errhandler = c_errhandler;
}

/** Fortran version for MPI_GET_VERSION */
void mpi_get_version_(MPI_Fint*version, MPI_Fint*subversion, MPI_Fint*ierr)
{
  int c_version, c_subversion;
  *ierr = mpi_get_version(&c_version, &c_subversion);
  *version = c_version;
  *subversion = c_subversion;
}

/** Fortran version for MPI_GET_LIBRARY_VERSION */
void mpi_get_library_version_(char*version, MPI_Fint*resultlen, MPI_Fint*ierr, int version_len)
{
  int c_resultlen;
  char*c_version = padico_malloc(sizeof(char) * (version_len + 1));
  *ierr = mpi_get_library_version(c_version, &c_resultlen);
  *resultlen = c_resultlen;
  nm_mpif_string_c2f(c_version, version, version_len);
  padico_free(c_version);
}

/** Fortran version for MPI_GET_ADDRESS */
void mpi_get_address_(void*location, MPI_Aint*address, MPI_Fint*ierr)
{
  MPI_Aint _address;
  NM_MPI_F2C_BOTTOM(location);
  *ierr = mpi_get_address(location, &_address);
  *address = _address;
}

/** Fortran version for MPI_ADDRESS */
void mpi_address_(void*location, MPI_Aint*address, MPI_Fint*ierr)
{
  MPI_Aint _address;
  *ierr = mpi_address(location, &_address);
  *address = _address;
}

/** Fortran version for MPI_INFO_CREATE */
void mpi_info_create_(MPI_Fint*info, MPI_Fint*ierr)
{
  MPI_Info c_info;
  *ierr = mpi_info_create(&c_info);
  *info = c_info;
}

/** Fortran version for MPI_INFO_FREE */
void mpi_info_free_(MPI_Fint*info, MPI_Fint*ierr)
{
  MPI_Info c_info = *info;
  *ierr = mpi_info_free(&c_info);
  *info = c_info;
}

/** Fortran version for MPI_INFO_DUP */
void mpi_info_dup_(MPI_Fint*info, MPI_Info*newinfo, MPI_Fint*ierr)
{
  MPI_Info c_newinfo;
  *ierr = mpi_info_dup(*info, &c_newinfo);
  *newinfo = c_newinfo;
}

/** Fortran version for MPI_INFO_SET */
void mpi_info_set_(MPI_Fint*info, char*key, char*value, MPI_Fint*ierr, int key_len, int value_len)
{
  char*c_key = nm_mpif_info_string_f2c(key, key_len);
  char*c_value = nm_mpif_info_string_f2c(value, value_len);
  *ierr = mpi_info_set(*info, c_key, c_value);
  padico_free(c_key);
  padico_free(c_value);
}

/** Fortran version for MPI_INFO_GET */
void mpi_info_get_(MPI_Fint*info, char*key, MPI_Fint*valuelen, char*value, MPI_Fint*flag, MPI_Fint*ierr, int key_len, int value_len)
{
  int c_flag;
  char*c_key = nm_mpif_string_f2c(key, key_len);
  char*c_value = padico_malloc(sizeof(char) * (value_len + 1));
  *ierr = mpi_info_get(*info, c_key, *valuelen, c_value, &c_flag);
  *flag = c_flag;
  if(c_flag && *ierr == MPI_SUCCESS)
    {
      nm_mpif_string_c2f(c_value, value, value_len);
    }
  padico_free(c_key);
  padico_free(c_value);
}

/** Fortran version for MPI_INFO_DELETE */
void mpi_info_delete_(MPI_Fint*info, char*key, MPI_Fint*ierr, int key_len)
{
  char*c_key = nm_mpif_string_f2c(key, key_len);
  *ierr = mpi_info_delete(*info, c_key);
  padico_free(c_key);
}

/** Fortran version for MPI_INFO_GET_NKEYS */
void mpi_info_get_nkeys_(MPI_Fint*info, MPI_Fint*nkeys, MPI_Fint*ierr)
{
  int c_nkeys;
  *ierr = mpi_info_get_nkeys(*info, &c_nkeys);
  *nkeys = c_nkeys;
}

/** Fortran version for MPI_INFO_GET_NTHKEY */
void mpi_info_get_nthkey_(MPI_Fint*info, MPI_Fint*n, char*key, MPI_Fint*ierr, int key_len)
{
  char*c_key = padico_malloc(sizeof(char) * (key_len + 1));
  *ierr = mpi_info_get_nthkey(*info, *n, c_key);
  nm_mpif_string_c2f(c_key, key, key_len);
  padico_free(c_key);
}

/** Fortran version for MPI_INFO_GET_VALUELEN */
void mpi_info_get_valuelen_(MPI_Fint*info, char*key, MPI_Fint*valuelen, MPI_Fint*flag, MPI_Fint*ierr, int key_len)
{
  int c_valuelen, c_flag;
  char*c_key = nm_mpif_string_f2c(key, key_len);
  *ierr = mpi_info_get_valuelen(*info, c_key, &c_valuelen, &c_flag);
  *valuelen = c_valuelen;
  *flag = c_flag;
  padico_free(c_key);
}

/** Fortran version for MPI_PCONTROL */
void mpi_pcontrol_(MPI_Fint*level)
{
  int c_level = *level;
  mpi_pcontrol(c_level);
}

/** Fortran version for MPI_TYPE_SIZE */
void mpi_type_size_(MPI_Fint*datatype, MPI_Fint*size, MPI_Fint*ierr)
{
  int c_size;
  *ierr = mpi_type_size(*datatype, &c_size);
  *size = c_size;
}

/** Fortran version for MPI_TYPE_SIZE_X */
void mpi_type_size_x_(MPI_Fint*datatype, MPI_Count*size, MPI_Fint*ierr)
{
  MPI_Count c_size;
  *ierr = mpi_type_size_x(*datatype, &c_size);
  *size = c_size;
}

/** Fortran version for MPI_TYPE_GET_EXTENT */
void mpi_type_get_extent_(MPI_Fint*datatype, MPI_Aint*lb, MPI_Aint*extent, MPI_Fint*ierr)
{
  MPI_Aint c_lb, c_extent;
  *ierr = mpi_type_get_extent(*datatype, &c_lb, &c_extent);
  *lb = c_lb;
  *extent = c_extent;
}

/** Fortran version for MPI_TYPE_GET_EXTENT_X_ */
void mpi_type_get_extent_x_(MPI_Fint*datatype, MPI_Count*lb, MPI_Count*extent, MPI_Fint*ierr)
{
  MPI_Count c_lb, c_extent;
  *ierr = mpi_type_get_extent_x(*datatype, &c_lb, & c_extent);
  *lb = c_lb;
  *extent = c_extent;
}

/** Fortran version for MPI_TYPE_EXTENT */
void mpi_type_extent_(MPI_Fint*datatype, MPI_Aint*extent, MPI_Fint*ierr)
{
  MPI_Aint c_extent;
  *ierr = mpi_type_extent(*datatype, &c_extent);
  *extent = c_extent;
}

/** Fortran version for MPI_TYPE_GET_TRUE_EXTENT */
void mpi_type_get_true_extent_(MPI_Fint*datatype, MPI_Aint*true_lb, MPI_Aint*true_extent, MPI_Fint*ierr)
{
  MPI_Aint c_true_lb, c_true_extent;
  *ierr = mpi_type_get_true_extent(*datatype, &c_true_lb, & c_true_extent);
  *true_lb     = c_true_lb;
  *true_extent = c_true_extent;
}

/** Fortran version for MPI_TYPE_GET_TRUE_EXTENT_x */
void mpi_type_get_true_extent_x_(MPI_Fint*datatype, MPI_Count*true_lb, MPI_Count*true_extent, MPI_Fint*ierr)
{
  MPI_Count c_true_lb, c_true_extent;
  *ierr = mpi_type_get_true_extent_x(*datatype, &c_true_lb, &c_true_extent);
  *true_lb     = c_true_lb;
  *true_extent = c_true_extent;
}

/** Fortran version for MPI_TYPE_LB */
void mpi_type_lb_(MPI_Fint*datatype, MPI_Aint*lb, MPI_Fint*ierr)
{
  MPI_Aint c_lb;
  *ierr = mpi_type_lb(*datatype, &c_lb);
  *lb = c_lb;
}

/** Fortran version for MPI_TYPE_DUP */
void mpi_type_dup_(MPI_Fint*oldtype, MPI_Fint*newtype, MPI_Fint*ierr)
{
  MPI_Datatype c_newtype;
  *ierr = mpi_type_dup(*oldtype, &c_newtype);
  *newtype = c_newtype;
}

/** Fortran version for MPI_TYPE_CREATE_RESIZED */
void mpi_type_create_resized_(MPI_Fint*oldtype, MPI_Aint*lb, MPI_Aint*extent, MPI_Fint*newtype, MPI_Fint*ierr)
{
  MPI_Datatype c_newtype;
  *ierr = mpi_type_create_resized(*oldtype, *lb, *extent, &c_newtype);
  *newtype = c_newtype;
}

/** Fortran version for MPI_TYPE_COMMIT */
void mpi_type_commit_(MPI_Fint*datatype, MPI_Fint*ierr)
{
  MPI_Datatype c_datatype = *datatype;
  *ierr = mpi_type_commit(&c_datatype);
  *datatype = c_datatype;
}

/** Fortran version for MPI_TYPE_FREE */
void mpi_type_free_(MPI_Fint*datatype, MPI_Fint*ierr)
{
  MPI_Datatype c_datatype = *datatype;
  *ierr = mpi_type_free(&c_datatype);
  *datatype = c_datatype;
}

/** Fortran version for MPI_TYPE_CONTIGUOUS */
void mpi_type_contiguous_(MPI_Fint*count, MPI_Fint*oldtype, MPI_Fint*newtype, MPI_Fint*ierr)
{
  MPI_Datatype c_newtype;
  *ierr = mpi_type_contiguous(*count, *oldtype, &c_newtype);
  *newtype = c_newtype;
}

/** Fortran version for MPI_TYPE_VECTOR */
void mpi_type_vector_(MPI_Fint*count, MPI_Fint*blocklength, MPI_Fint*stride, MPI_Fint*oldtype, MPI_Fint*newtype, MPI_Fint*ierr)
{
  MPI_Datatype c_newtype;
  *ierr = mpi_type_vector(*count, *blocklength, *stride, *oldtype, &c_newtype);
  *newtype = c_newtype;
}

/** Fortran version for MPI_TYPE_HVECTOR */
void mpi_type_hvector_(MPI_Fint*count, MPI_Fint*blocklength, MPI_Aint*stride, MPI_Fint*oldtype, MPI_Fint*newtype, MPI_Fint*ierr)
{
  MPI_Datatype c_newtype;
  *ierr = mpi_type_hvector(*count, *blocklength, *stride, *oldtype, &c_newtype);
  *newtype = c_newtype;
}

/** Fortran version for MPI_TYPE_CREATE_HVECTOR */
void mpi_type_create_hvector_(MPI_Fint*count, MPI_Fint*blocklength, MPI_Fint*stride, MPI_Fint*oldtype, MPI_Fint*newtype, MPI_Fint*ierr)
{
  MPI_Datatype c_newtype;
  *ierr = mpi_type_hvector(*count, *blocklength, *stride, *oldtype, &c_newtype);
  *newtype = c_newtype;
}

/** Fortran version for MPI_TYPE_INDEXED */
void mpi_type_indexed_(MPI_Fint*count, MPI_Fint*array_of_blocklengths, MPI_Fint*array_of_displacements,
                       MPI_Fint*oldtype, MPI_Fint*newtype, MPI_Fint*ierr)
{
  MPI_Datatype c_newtype;
  int*c_array_of_blocklengths = nm_mpif_array_f2c(array_of_blocklengths, *count);
  int*c_array_of_displacements = nm_mpif_array_f2c(array_of_displacements, *count);
  *ierr = mpi_type_indexed(*count, c_array_of_blocklengths, c_array_of_displacements, *oldtype, &c_newtype);
  padico_free(c_array_of_blocklengths);
  padico_free(c_array_of_displacements);
  *newtype = c_newtype;
}

/** Fortran version for MPI_TYPE_HINDEXED */
void mpi_type_hindexed_(MPI_Fint*count, MPI_Fint*array_of_blocklengths, MPI_Fint*array_of_displacements,
                        MPI_Fint*oldtype, MPI_Fint*newtype, MPI_Fint*ierr)
{
  MPI_Datatype c_newtype;
  MPI_Aint*c_array_of_displacements = padico_malloc(*count * sizeof(MPI_Aint));
  int i;
  for(i = 0; i < *count; i++)
    {
      c_array_of_displacements[i] = array_of_displacements[i];
    }
  int*c_array_of_blocklengths = nm_mpif_array_f2c(array_of_blocklengths, *count);
  *ierr = mpi_type_hindexed(*count, c_array_of_blocklengths, c_array_of_displacements,
                            *oldtype, &c_newtype);
  *newtype = c_newtype;
  padico_free(c_array_of_displacements);
  padico_free(c_array_of_blocklengths);
}

/** Fortran version for MPI_TYPE_CREATE_HINDEXED */
void mpi_type_create_hindexed_(MPI_Fint*count, MPI_Fint*array_of_blocklengths, MPI_Aint*array_of_displacements,
                               MPI_Fint*oldtype, MPI_Fint*newtype, MPI_Fint*ierr)
{
  MPI_Datatype c_newtype;
  MPI_Aint*c_array_of_displacements = padico_malloc(*count * sizeof(MPI_Aint));
  int i;
  for(i = 0; i < *count; i++)
    {
      c_array_of_displacements[i] = array_of_displacements[i];
    }
  int*c_array_of_blocklengths = nm_mpif_array_f2c(array_of_blocklengths, *count);
  *ierr = mpi_type_create_hindexed(*count, c_array_of_blocklengths, c_array_of_displacements,
                                   *oldtype, &c_newtype);
  *newtype = c_newtype;
  padico_free(c_array_of_displacements);
  padico_free(c_array_of_blocklengths);
}

/** Fortran version for MPI_TYPE_CREATE_INDEXED_BLOCK */
void mpi_type_create_indexed_block_(MPI_Fint*count, MPI_Fint*blocklength, MPI_Fint*array_of_displacements,
                                    MPI_Fint*oldtype, MPI_Fint*newtype, MPI_Fint*ierr)
{
  MPI_Datatype c_newtype;
  int*c_array_of_displacements = nm_mpif_array_f2c(array_of_displacements, *count);
  *ierr = mpi_type_create_indexed_block(*count, *blocklength, c_array_of_displacements, *oldtype, &c_newtype);
  *newtype = c_newtype;
  padico_free(c_array_of_displacements);
}

/** Fortran version for MPI_TYPE_CREATE_HINDEXED_BLOCK */
void mpi_type_create_hindexed_block_(MPI_Fint*count, MPI_Fint*blocklength, MPI_Aint*array_of_displacements,
                                     MPI_Fint*oldtype, MPI_Fint*newtype, MPI_Fint*ierr)
{
  MPI_Datatype c_newtype;
  MPI_Aint*c_array_of_displacements = padico_malloc(*count * sizeof(MPI_Aint));
  int i;
  for(i = 0; i < *count; i++)
    {
      c_array_of_displacements[i] = array_of_displacements[i];
    }
  *ierr = mpi_type_create_hindexed_block(*count, *blocklength, c_array_of_displacements,
                                         *oldtype, &c_newtype);
  *newtype = c_newtype;
  padico_free(c_array_of_displacements);
}

/** Fortran version for MPI_TYPE_STRUCT */
void mpi_type_struct_(MPI_Fint*count, MPI_Fint*array_of_blocklengths, MPI_Aint*array_of_displacements,
                      MPI_Fint*array_of_types, MPI_Fint*newtype, MPI_Fint*ierr)
{
  MPI_Datatype c_newtype;
  MPI_Aint*c_array_of_displacements = padico_malloc(*count * sizeof(MPI_Aint));
  int i;
  for(i = 0; i < *count; i++)
    {
      c_array_of_displacements[i] = array_of_displacements[i];
    }
  int*c_array_of_blocklengths = nm_mpif_array_f2c(array_of_blocklengths, *count);
  int*c_array_of_types = nm_mpif_array_f2c(array_of_types, *count);
  *ierr = mpi_type_struct(*count, c_array_of_blocklengths, c_array_of_displacements,
                          c_array_of_types, &c_newtype);
  *newtype = c_newtype;
  padico_free(c_array_of_displacements);
  padico_free(c_array_of_blocklengths);
  padico_free(c_array_of_types);
}

/** Fortran version for MPI_TYPE_CREATE_STRUCT */
void mpi_type_create_struct_(MPI_Fint*count, MPI_Fint*array_of_blocklengths, MPI_Aint*array_of_displacements,
                             MPI_Fint*array_of_types, MPI_Fint*newtype, MPI_Fint*ierr)
{
  mpi_type_struct_(count, array_of_blocklengths, array_of_displacements, array_of_types, newtype, ierr);
}

/** Fortran version for MPI_TYPE_GET_ENVELOPE */
void mpi_type_get_envelope_(MPI_Fint*datatype, MPI_Fint*num_integers, MPI_Fint*num_addresses, MPI_Fint*num_datatypes, MPI_Fint*combiner, MPI_Fint*ierr)
{
  int c_num_integers, c_num_addresses, c_num_datatypes, c_combiner;
  *ierr = mpi_type_get_envelope(*datatype, &c_num_integers, &c_num_addresses, &c_num_datatypes, &c_combiner);
  *num_integers  = c_num_integers;
  *num_addresses = c_num_addresses;
  *num_datatypes = c_num_datatypes;
  *combiner      = c_combiner;
}

/** Fortran version for MPI_TYPE_GET_CONTENTS */
void mpi_type_get_contents_(MPI_Fint*datatype, MPI_Fint*max_integers, MPI_Fint*max_addresses, MPI_Fint*max_datatypes,
                            MPI_Fint*array_of_integers, MPI_Aint*array_of_addresses, MPI_Fint*array_of_datatypes, MPI_Fint*ierr)
{
  int*c_array_of_integers       = padico_malloc(*max_integers * sizeof(int));
  int*c_array_of_datatypes      = padico_malloc(*max_datatypes * sizeof(int));
  MPI_Aint*c_array_of_addresses = padico_malloc(*max_addresses * sizeof(MPI_Aint));
  int i;
  *ierr = mpi_type_get_contents(*datatype, *max_integers, *max_addresses, *max_datatypes, c_array_of_integers, c_array_of_addresses, c_array_of_datatypes);
  nm_mpif_array_c2f(c_array_of_integers, array_of_integers, *max_integers);
  nm_mpif_array_c2f(c_array_of_datatypes, array_of_datatypes, *max_datatypes);
  for(i = 0; i < *max_addresses; i++)
    {
      array_of_addresses[i] = c_array_of_addresses[i];
    }
  padico_free(c_array_of_integers);
  padico_free(c_array_of_datatypes);
  padico_free(c_array_of_addresses);
}

/** Fortran version for MPI_TYPE_CREATE_SUBARRAY */
void mpi_type_create_subarray_(MPI_Fint*ndims, MPI_Fint*array_of_sizes, MPI_Fint*array_of_subsizes,
                               MPI_Fint*array_of_starts, MPI_Fint*order, MPI_Fint*oldtype, MPI_Fint*newtype, MPI_Fint*ierr)
{
  MPI_Datatype c_newtype;
  int*c_array_of_sizes    = nm_mpif_array_f2c(array_of_sizes, *ndims);
  int*c_array_of_subsizes = nm_mpif_array_f2c(array_of_subsizes, *ndims);
  int*c_array_of_starts   = nm_mpif_array_f2c(array_of_starts, *ndims);
  *ierr = mpi_type_create_subarray(*ndims, c_array_of_sizes, c_array_of_subsizes, c_array_of_starts, *order, *oldtype, &c_newtype);
  *newtype = c_newtype;
  padico_free(c_array_of_sizes);
  padico_free(c_array_of_subsizes);
  padico_free(c_array_of_starts);
}

/** Fortran version for MPI_TYPE_CREATE_DARRAY */
void mpi_type_create_darray_(MPI_Fint*size, MPI_Fint*rank, MPI_Fint*ndims,
                             MPI_Fint*array_of_gsizes, MPI_Fint*array_of_distribs,
                             MPI_Fint*array_of_dargs, MPI_Fint*array_of_psizes,
                             MPI_Fint*order, MPI_Fint*oldtype, MPI_Fint*newtype, MPI_Fint*ierr)
{
  MPI_Datatype c_newtype;
  int*c_array_of_gsizes   = nm_mpif_array_f2c(array_of_gsizes, *ndims);
  int*c_array_of_distribs = nm_mpif_array_f2c(array_of_distribs, *ndims);
  int*c_array_of_dargs    = nm_mpif_array_f2c(array_of_dargs, *ndims);
  int*c_array_of_psizes   = nm_mpif_array_f2c(array_of_psizes, *ndims);
  *ierr = mpi_type_create_darray(*size, *rank, *ndims, c_array_of_gsizes, c_array_of_distribs, c_array_of_dargs, c_array_of_psizes, *order, *oldtype, &c_newtype);
  *newtype = c_newtype;
  padico_free(c_array_of_gsizes);
  padico_free(c_array_of_distribs);
  padico_free(c_array_of_dargs);
  padico_free(c_array_of_psizes);
}

/** Fortran version for MPI_COMM_TEST_INTER */
void mpi_comm_test_inter_(MPI_Fint*comm, MPI_Fint*flag, MPI_Fint*ierr)
{
  int c_flag;
  *ierr = mpi_comm_test_inter(*comm, &c_flag);
  *flag = c_flag;
}

/** Fortran version for MPI_COMM_COMPARE */
void mpi_comm_compare_(MPI_Fint*comm1, MPI_Fint*comm2, MPI_Fint*result, MPI_Fint*ierr)
{
  int c_result;
  *ierr = mpi_comm_compare(*comm1, *comm2, &c_result);
  *result = c_result;
}

/** Fortran version for MPI_COMM_GROUP */
void mpi_comm_group_(MPI_Fint*comm, MPI_Fint*group, MPI_Fint*ierr)
{
  MPI_Group c_group;
  *ierr = mpi_comm_group(*comm, &c_group);
  *group = c_group;
}

/** Fortran version for MPI_COMM_CREATE */
void mpi_comm_create_(MPI_Fint*comm, MPI_Fint*group, MPI_Fint*newcomm, MPI_Fint*ierr)
{
  MPI_Comm c_newcomm;
  int err = mpi_comm_create(*comm, *group, &c_newcomm);
  *newcomm = c_newcomm;
  *ierr = err;
}

/** Fortran version for MPI_COMM_CREATE_GROUP */
void mpi_comm_create_group_(MPI_Fint*comm, MPI_Fint*group, MPI_Fint*tag, MPI_Fint*newcomm, MPI_Fint*ierr)
{
  MPI_Comm c_newcomm;
  *ierr = mpi_comm_create_group(*comm, *group, *tag, &c_newcomm);
  *newcomm = c_newcomm;
}

/** Fortran version for MPI_COMM_SPLIT */
void mpi_comm_split_(MPI_Fint*comm, MPI_Fint*color, MPI_Fint*key, MPI_Fint*newcomm, MPI_Fint*ierr)
{
  MPI_Comm c_newcomm;
  *ierr = mpi_comm_split(*comm, *color, *key, &c_newcomm);
  *newcomm = c_newcomm;
}

/** Fortran version for MPI_COMM_SPLIT_TYPE */
void mpi_comm_split_type_(MPI_Fint*oldcomm, MPI_Fint*split_type, MPI_Fint*key, MPI_Fint*info, MPI_Fint*newcomm, MPI_Fint*ierr)
{
  MPI_Comm c_newcomm;
  *ierr = mpi_comm_split_type(*oldcomm, *split_type, *key, *info, &c_newcomm);
  *newcomm = c_newcomm;
}

/** Fortran version for MPI_COMM_DUP */
void mpi_comm_dup_(MPI_Fint*comm, MPI_Fint*newcomm, MPI_Fint*ierr)
{
  MPI_Comm c_newcomm;
  *ierr = mpi_comm_dup(*comm, &c_newcomm);
  *newcomm = c_newcomm;
}

/** Fortran version for MPI_COMM_FREE */
void mpi_comm_free_(MPI_Fint*comm, MPI_Fint*ierr)
{
  MPI_Comm c_comm = *comm;
  *ierr = mpi_comm_free(&c_comm);
  *comm = c_comm;
}

/** Fortran version for MPI_COMM_GET_PARENT */
void mpi_comm_get_parent_(MPI_Fint*parent, MPI_Fint*ierr)
{
  MPI_Comm c_parent;
  *ierr = mpi_comm_get_parent(&c_parent);
  *parent = c_parent;
}

/** Fortran version for MPI_COMM_REMOTE_SIZE */
void mpi_comm_remote_size_(MPI_Fint*comm, MPI_Fint*size, MPI_Fint*ierr)
{
  int c_size;
  *ierr = mpi_comm_remote_size(*comm, &c_size);
  *size = c_size;
}

/** Fortran version for MPI_COMM_REMOTE_GROUP */
void mpi_comm_remote_group_(MPI_Fint*comm, MPI_Fint*group, MPI_Fint*ierr)
{
  MPI_Group c_group;
  *ierr = mpi_comm_remote_group(*comm, &c_group);
  *group = c_group;
}

/** Fortran version for MPI_INTERCOMM_CREATE */
void mpi_intercomm_create_(MPI_Fint*local_comm, MPI_Fint*local_leader, MPI_Fint*parent_comm, MPI_Fint*remote_leader, MPI_Fint*tag, MPI_Fint*newintercomm, MPI_Fint*ierr)
{
  MPI_Comm c_newintercomm;
  *ierr = mpi_intercomm_create(*local_comm, *local_leader, *parent_comm, *remote_leader, *tag, &c_newintercomm);
  *newintercomm = c_newintercomm;
}

/** Fortran version for MPI_INTERCOMM_MERGE */
void mpi_intercomm_merge_(MPI_Fint*intercomm, MPI_Fint*high, MPI_Fint*newcomm, MPI_Fint*ierr)
{
  MPI_Comm c_newcomm;
  *ierr = mpi_intercomm_merge(*intercomm, *high, &c_newcomm);
  *newcomm = c_newcomm;
}

void mpi_dims_create_(MPI_Fint*nnodes, MPI_Fint*ndims, MPI_Fint*dims, MPI_Fint*ierr)
{
  int*c_dims = nm_mpif_array_f2c(dims, *ndims);
  *ierr = mpi_dims_create(*nnodes, *ndims, c_dims);
  nm_mpif_array_c2f(c_dims, dims, *ndims);
  padico_free(c_dims);
}

void mpi_cart_create_(MPI_Fint*_comm_old, MPI_Fint*ndims, MPI_Fint*dims, MPI_Fint*periods, MPI_Fint*reorder, MPI_Fint*_comm_cart, MPI_Fint*ierr)
{
  int*c_dims = nm_mpif_array_f2c(dims, *ndims);
  int*c_periods = nm_mpif_array_f2c(periods, *ndims);
  MPI_Comm comm_old = *_comm_old;
  MPI_Comm comm_cart = MPI_COMM_NULL;
  *ierr = mpi_cart_create(comm_old, *ndims, c_dims, c_periods, *reorder, &comm_cart);
  *_comm_cart = comm_cart;
  padico_free(c_dims);
  padico_free(c_periods);
}

void mpi_cart_coords_(MPI_Fint*_comm, MPI_Fint*rank, MPI_Fint*ndims, MPI_Fint*coords, MPI_Fint*ierr)
{
  int*c_coords = padico_malloc(sizeof(int) * *ndims);
  MPI_Comm comm = *_comm;
  *ierr = mpi_cart_coords(comm, *rank, *ndims, c_coords);
  nm_mpif_array_c2f(c_coords, coords, *ndims);
  padico_free(c_coords);
}

void mpi_cart_rank_(MPI_Fint*comm, MPI_Fint*coords, MPI_Fint*rank, MPI_Fint*ierr)
{
  int c_rank = *rank;
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(*comm);
  struct nm_mpi_cart_topology_s*p_cart = &p_comm->cart_topology;
  const int ndims = p_cart->ndims;
  int*c_coords = nm_mpif_array_f2c(coords, ndims);
  *ierr = mpi_cart_rank(*comm, c_coords, &c_rank);
  *rank = c_rank;
  padico_free(c_coords);
}

void mpi_cart_shift_(MPI_Fint*comm, MPI_Fint*direction, MPI_Fint*displ, MPI_Fint*source, MPI_Fint*dest, MPI_Fint*ierr)
{
  int c_source = *source;
  int c_dest = *dest;
  *ierr = mpi_cart_shift(*comm, *direction, *displ, &c_source, &c_dest);
  *source = c_source;
  *dest = c_dest;
}

/** Fortran version for MPI_CART_GET */
void mpi_cart_get_(MPI_Fint*comm, MPI_Fint*maxdims, MPI_Fint*dims, MPI_Fint*periods, MPI_Fint*coords, MPI_Fint*ierr)
{
  int*c_dims    = padico_malloc(sizeof(int) * *maxdims);
  int*c_periods = padico_malloc(sizeof(int) * *maxdims);
  int*c_coords  = padico_malloc(sizeof(int) * *maxdims);
  *ierr = mpi_cart_get(*comm, *maxdims, c_dims, c_periods, c_coords);
  nm_mpif_array_c2f(c_dims, dims, *maxdims);
  nm_mpif_array_c2f(c_periods, periods, *maxdims);
  nm_mpif_array_c2f(c_coords, coords, *maxdims);
  padico_free(c_dims);
  padico_free(c_periods);
  padico_free(c_coords);
}

void mpi_cart_sub_(MPI_Fint*comm, MPI_Fint*remaindims, MPI_Fint*newcomm, MPI_Fint*ierr)
{
  MPI_Comm c_newcomm = *newcomm;
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(*comm);
  struct nm_mpi_cart_topology_s*p_cart = &p_comm->cart_topology;
  const int ndims = p_cart->ndims;
  int*c_remaindims = nm_mpif_array_f2c(remaindims, ndims);
  *ierr = mpi_cart_sub(*comm, c_remaindims, &c_newcomm);
  *newcomm = c_newcomm;
  padico_free(c_remaindims);
}

/** Fortran version for MPI_TOPO_TEST */
void mpi_topo_test_(MPI_Fint*comm, MPI_Fint*topo_type, MPI_Fint*ierr)
{
  int c_topo_type;
  *ierr = mpi_topo_test(*comm, &c_topo_type);
  *topo_type = c_topo_type;
}

/** Fortran version for MPI_CARTDIM_GET */
void mpi_cartdim_get_(MPI_Fint*comm, MPI_Fint*ndims, MPI_Fint*ierr)
{
  int c_ndims;
  *ierr = mpi_cartdim_get(*comm, &c_ndims);
  *ndims = c_ndims;
}

/** Fortran version for MPI_GROUP_SIZE */
void mpi_group_size_(MPI_Fint*group, MPI_Fint*size, MPI_Fint*ierr)
{
  int c_size;
  *ierr = mpi_group_size(*group, &c_size);
  *size = c_size;
}

/** Fortran version for MPI_GROUP_RANK */
void mpi_group_rank_(MPI_Fint*group, MPI_Fint*rank, MPI_Fint*ierr)
{
  int c_rank;
  *ierr = mpi_group_rank(*group, &c_rank);
  *rank = c_rank;
}

/** Fortran version for MPI_GROUP_UNION */
void mpi_group_union_(MPI_Fint*group1, MPI_Fint*group2, MPI_Fint*newgroup, MPI_Fint*ierr)
{
  MPI_Group c_newgroup;
  *ierr = mpi_group_union(*group1, *group2, &c_newgroup);
  *newgroup = c_newgroup;
}

/** Fortran version for MPI_GROUP_INTERSECTION */
void mpi_group_intersection_(MPI_Fint*group1, MPI_Fint*group2, MPI_Fint*newgroup, MPI_Fint*ierr)
{
  MPI_Group c_newgroup;
  *ierr = mpi_group_intersection(*group1, *group2, &c_newgroup);
  *newgroup = c_newgroup;
}

/** Fortran version for MPI_GROUP_DIFFERENCE */
void mpi_group_difference_(MPI_Fint*group1, MPI_Fint*group2, MPI_Fint*newgroup, MPI_Fint*ierr)
{
  MPI_Group c_newgroup;
  *ierr = mpi_group_difference(*group1, *group2, &c_newgroup);
  *newgroup = c_newgroup;
}

/** Fortran version for MPI_GROUP_COMPARE */
void mpi_group_compare_(MPI_Fint*group1, MPI_Fint*group2, MPI_Fint*result, MPI_Fint*ierr)
{
  int c_result;
  *ierr = mpi_group_compare(*group1, *group2, &c_result);
  *result = c_result;
}

/** Fortran version for MPI_Group_incl */
void mpi_group_incl_(MPI_Fint*group, MPI_Fint*n, MPI_Fint*ranks, MPI_Fint*newgroup, MPI_Fint*ierr)
{
  int*c_ranks = nm_mpif_array_f2c(ranks, *n);
  MPI_Group _newgroup;
  int err = mpi_group_incl(*group, *n, c_ranks, &_newgroup);
  *newgroup = _newgroup;
  *ierr = err;
  padico_free(c_ranks);
}

/** Fortran version for MPI_GROUP_RANGE_INCL */
void mpi_group_range_incl_(MPI_Fint*group, MPI_Fint*n, MPI_Fint ranges[][3], MPI_Fint*newgroup, MPI_Fint*ierr)
{
  MPI_Group c_newgroup;
  int i, j;
  int (*c_ranges)[3] = padico_malloc(sizeof(int[*n][3]));
  for(i = 0; i < *n; i++)
    {
      for(j = 0; j < 3; j++)
        {
          c_ranges[i][j] = ranges[i][j];
        }
    }
  *ierr = mpi_group_range_incl(*group, *n, c_ranges, &c_newgroup);
  *newgroup = c_newgroup;
  padico_free(c_ranges);
}

/** Fortran version for MPI_GROUP_EXCL */
void mpi_group_excl_(MPI_Fint*group, MPI_Fint*n, MPI_Fint*ranks, MPI_Fint*newgroup, MPI_Fint*ierr)
{
  MPI_Group c_newgroup;
  int*c_ranks = nm_mpif_array_f2c(ranks, *n);
  *ierr = mpi_group_excl(*group, *n, c_ranks, &c_newgroup);
  *newgroup = c_newgroup;
  padico_free(c_ranks);
}

/** Fortran version for MPI_GROUP_RANGE_EXCL */
void mpi_group_range_excl_(MPI_Fint*group, MPI_Fint*n, MPI_Fint ranges[][3], MPI_Fint*newgroup, MPI_Fint*ierr)
{
  MPI_Group c_newgroup;
  int i, j;
  int (*c_ranges)[3] = padico_malloc(sizeof(int[*n][3]));
  for(i = 0; i < *n; i++)
    {
      for(j = 0; j < 3; j++)
        {
          c_ranges[i][j] = ranges[i][j];
        }
    }
  *ierr = mpi_group_range_excl(*group, *n, c_ranges, &c_newgroup);
  *newgroup = c_newgroup;
  padico_free(c_ranges);
}

/** Fortran version for MPI_GROUP_FREE */
void mpi_group_free_(MPI_Fint*group, MPI_Fint*ierr)
{
  MPI_Group c_group = *group;
  *ierr = mpi_group_free(&c_group);
  *group = c_group;
}

/** Fortran version for MPI_GROUP_TRANSLATE_RANKS */
void mpi_group_translate_ranks_(MPI_Fint*group1, MPI_Fint*n, MPI_Fint*ranks1, MPI_Fint*group2, MPI_Fint*ranks2, MPI_Fint*ierr)
{
  int*c_ranks1 = nm_mpif_array_f2c(ranks1, *n);
  int*c_ranks2 = nm_mpif_array_f2c(ranks2, *n);
  *ierr = mpi_group_translate_ranks(*group1, *n, c_ranks1, *group2, c_ranks2);
  nm_mpif_array_c2f(c_ranks2, ranks2, *n);
  padico_free(c_ranks1);
  padico_free(c_ranks2);
}

/** Fortran version for MPI_WIN_CREATE */
void mpi_win_create_(void*base, MPI_Aint*size, MPI_Fint*disp_unit, MPI_Fint*info,
                     MPI_Fint*comm, MPI_Fint*win, MPI_Fint*ierr)
{
  MPI_Win c_win;
  *ierr = mpi_win_create(base, *size, *disp_unit, *info, *comm, &c_win);
  *win = c_win;
}

/** Fortran version for MPI_WIN_ALLOCATE */
void mpi_win_allocate_(MPI_Aint*size, MPI_Fint*disp_unit, MPI_Fint*info,
                       MPI_Fint*comm, void*baseptr, MPI_Fint*win, MPI_Fint*ierr)
{
  MPI_Win c_win;
  *ierr = mpi_win_allocate(*size, *disp_unit, *info, *comm, baseptr, &c_win);
  *win = c_win;
}

/** Fortran version for MPI_WIN_ALLOCATE_SHARED */
void mpi_win_allocate_shared_(MPI_Aint*size, MPI_Fint*disp_unit, MPI_Fint*info,
                              MPI_Fint*comm, void*baseptr, MPI_Fint*win, MPI_Fint*ierr)
{
  MPI_Win c_win;
  *ierr = mpi_win_allocate_shared(*size, *disp_unit, *info, *comm, baseptr, &c_win);
  *win = c_win;
}

/** Fortran version for MPI_WIN_SHARED_QUERY */
void mpi_win_shared_query_(MPI_Fint*win, MPI_Fint*rank, MPI_Aint*size,
                           MPI_Fint*disp_unit, void*baseptr, MPI_Fint*ierr)
{
  int c_disp_unit;
  *ierr = mpi_win_shared_query(*win, *rank, size, &c_disp_unit, baseptr);
  *disp_unit = c_disp_unit;
}

/** Fortran version for MPI_WIN_CREATE_DYNAMIC */
void mpi_win_create_dynamic_(MPI_Fint*info, MPI_Fint*comm, MPI_Fint*win, MPI_Fint*ierr)
{
  MPI_Win c_win;
  *ierr = mpi_win_create_dynamic(*info, *comm, &c_win);
  *win = c_win;
}

/** Fortran version for MPI_WIN_ATTACH */
void mpi_win_attach_(MPI_Fint*win, void*base, MPI_Aint*size, MPI_Fint*ierr)
{
  *ierr = mpi_win_attach(*win, base, *size);
}

/** Fortran version for MPI_WIN_DETACH */
void mpi_win_detach_(MPI_Fint*win, void*base, MPI_Fint*ierr)
{
  *ierr = mpi_win_detach(*win, base);
}

/** Fortran version for MPI_WIN_FREE */
void mpi_win_free_(MPI_Fint*win, MPI_Fint*ierr)
{
  MPI_Win c_win;
  *ierr = mpi_win_free(&c_win);
  *win = c_win;
}

/** Fortran version for MPI_WIN_GET_GROUP */
void mpi_win_get_group_(MPI_Fint*win, MPI_Fint*group, MPI_Fint*ierr)
{
  MPI_Group c_group;
  *ierr = mpi_win_get_group(*win, &c_group);
  *group = c_group;
}

/** Fortran version for MPI_WIN_SET_INFO */
void mpi_win_set_info_(MPI_Fint*win, MPI_Fint*info, MPI_Fint*ierr)
{
  *ierr = mpi_win_set_info(*win, *info);
}

/** Fortran version for MPI_WIN_GET_INFO */
void mpi_win_get_info_(MPI_Fint*win, MPI_Fint*info_used, MPI_Fint*ierr)
{
  MPI_Info c_info_used;
  *ierr = mpi_win_get_info(*win, &c_info_used);
  *info_used = c_info_used;
}

/** Fortran version for MPI_WIN_CREATE_KEYVAL */
void mpi_win_create_keyval_(nm_mpi_copy_subroutine_t*win_copy_attr_fn,
                            nm_mpi_delete_subroutine_t*win_delete_attr_fn,
                            MPI_Fint*win_keyval, void*extra_state, MPI_Fint*ierr)
{
  int c_win_keyval;
  *ierr = nm_mpi_win_create_keyval_fort(win_copy_attr_fn, win_delete_attr_fn, &c_win_keyval, extra_state);
  *win_keyval = c_win_keyval;
}

void mpi_win_dup_fn_(MPI_Fint*oldwin, MPI_Fint*win_keyval, MPI_Aint*extra_state, MPI_Aint*attribute_val_in, MPI_Aint*attribute_val_out, MPI_Fint*flag, MPI_Fint*ierr)
{
  *flag = 1;
  *attribute_val_out = *attribute_val_in;
  *ierr = MPI_SUCCESS;
}

void mpi_win_null_copy_fn_(MPI_Fint*oldwin, MPI_Fint*win_keyval, MPI_Aint*extra_state, MPI_Aint*attribute_val_in, MPI_Aint*attribute_val_out, MPI_Fint*flag, MPI_Fint*ierr)
{
  *flag = 0;
  *ierr = MPI_SUCCESS;
}

void mpi_win_null_delete_fn_(MPI_Fint*win, MPI_Fint*win_keyval, MPI_Aint*attribute_val, MPI_Aint*extra_state, MPI_Fint*ierr)
{
  *ierr = MPI_SUCCESS;
}

/** Fortran version for MPI_WIN_FREE_KEYVAL */
void mpi_win_free_keyval_(MPI_Fint*keyval, MPI_Fint*ierr)
{
  int c_keyval = *keyval;
  *ierr = mpi_win_free_keyval(&c_keyval);
  *keyval = c_keyval;
}

/** Fortran version for MPI_WIN_DELETE_ATTR */
void mpi_win_delete_attr_(MPI_Fint*win, MPI_Fint*keyval, MPI_Fint*ierr)
{
  *ierr = mpi_win_delete_attr(*win, *keyval);
}

/** Fortran version for MPI_WIN_SET_ATTR */
void mpi_win_set_attr_(MPI_Fint*win, MPI_Fint*win_keyval, MPI_Aint*attribute_val, MPI_Fint*ierr)
{
  *ierr = mpi_win_set_attr(*win, *win_keyval, (void*)(*attribute_val));
}

/** Fortran version for MPI_WIN_GET_ATTR */
void mpi_win_get_attr_(MPI_Fint*win, MPI_Fint*win_keyval, MPI_Aint*attribute_val, MPI_Fint*flag, MPI_Fint*ierr)
{
  int c_flag;
  void*c_attribute_val;
  *ierr = mpi_win_get_attr(*win, *win_keyval, &c_attribute_val, &c_flag);
  *attribute_val = (MPI_Aint)c_attribute_val;
  *flag = c_flag;
}

/** Fortran version for MPI_WIN_SET_NAME */
void mpi_win_set_name_(MPI_Fint*win, char*win_name, MPI_Fint*ierr, int win_name_len)
{
  char*c_win_name = nm_mpif_string_f2c(win_name, win_name_len);
  *ierr = mpi_win_set_name(*win, c_win_name);
  padico_free(c_win_name);
}

/** Fortran version for MPI_WIN_GET_NAME */
void mpi_win_get_name_(MPI_Fint*win, char*win_name, MPI_Fint*resultlen, MPI_Fint*ierr, int win_name_len)
{
  int c_resultlen;
  char*c_win_name = padico_malloc(sizeof(char) * (win_name_len + 1));
  *ierr = mpi_win_get_name(*win, c_win_name, &c_resultlen);
  *resultlen = c_resultlen;
  nm_mpif_string_c2f(c_win_name, win_name, win_name_len);
  padico_free(c_win_name);
}


/** Fortran version for MPI_WIN_SET_ERRHANDLER */
void mpi_win_set_errhandler_(MPI_Fint*win, MPI_Fint*errhandler, MPI_Fint*ierr)
{
  *ierr = mpi_win_set_errhandler(*win, *errhandler);
}

/** Fortran version for MPI_WIN_GET_ERRHANDLER */
void mpi_win_get_errhandler_(MPI_Fint*win, MPI_Fint*errhandler, MPI_Fint*ierr)
{
  MPI_Errhandler c_errhandler;
  *ierr = mpi_win_get_errhandler(*win, &c_errhandler);
  *errhandler = c_errhandler;
}

/** Fortran version for MPI_WIN_CALL_ERRHANDLER */
void mpi_win_call_errhandler_(MPI_Fint*win, MPI_Fint*errorcode, MPI_Fint*ierr)
{
  *ierr = mpi_win_call_errhandler(*win, *errorcode);
}

/** Fortran version for MPI_WIN_FENCE */
void mpi_win_fence_(MPI_Fint*assert, MPI_Fint*win, MPI_Fint*ierr)
{
  *ierr = mpi_win_fence(*assert, *win);
}

/** Fortran version for MPI_WIN_START */
void mpi_win_start_(MPI_Fint*group, MPI_Fint*assert, MPI_Fint*win, MPI_Fint*ierr)
{
  *ierr = mpi_win_start(*group, *assert, *win);
}

/** Fortran version for MPI_WIN_COMPLETE */
void mpi_win_complete_(MPI_Fint*win, MPI_Fint*ierr)
{
  *ierr = mpi_win_complete(*win);
}

/** Fortran version for MPI_WIN_POST */
void mpi_win_post_(MPI_Fint*group, MPI_Fint*assert, MPI_Fint*win, MPI_Fint*ierr)
{
  *ierr = mpi_win_post(*group, *assert, *win);
}

/** Fortran version for MPI_WIN_WAIT */
void mpi_win_wait_(MPI_Fint*win, MPI_Fint*ierr)
{
  *ierr = mpi_win_wait(*win);
}

/** Fortran version for MPI_WIN_TEST */
void mpi_win_test_(MPI_Fint*win, MPI_Fint*flag, MPI_Fint*ierr)
{
  int c_flag;
  *ierr = mpi_win_test(*win, &c_flag);
  *flag = c_flag;
}

/** Fortran version for MPI_WIN_LOCK */
void mpi_win_lock_(MPI_Fint*lock_type, MPI_Fint*rank, MPI_Fint*assert, MPI_Fint*win, MPI_Fint*ierr)
{
  *ierr = mpi_win_lock(*lock_type, *rank, *assert, *win);
}

/** Fortran version for MPI_WIN_LOCK_ALL */
void mpi_win_lock_all_(MPI_Fint*assert, MPI_Fint*win, MPI_Fint*ierr)
{
  *ierr = mpi_win_lock_all(*assert, *win);
}

/** Fortran version for MPI_WIN_UNLOCK */
void mpi_win_unlock_(MPI_Fint*rank, MPI_Fint*win, MPI_Fint*ierr)
{
  *ierr = mpi_win_unlock(*rank, *win);
}

/** Fortran version for MPI_WIN_UNLOCK_ALL */
void mpi_win_unlock_all_(MPI_Fint*win, MPI_Fint*ierr)
{
  *ierr = mpi_win_unlock_all(*win);
}

/** Fortran version for MPI_WIN_FLUSH */
void mpi_win_flush_(MPI_Fint*rank, MPI_Fint*win, MPI_Fint*ierr)
{
  *ierr = mpi_win_flush(*rank, *win);
}

/** Fortran version for MPI_WIN_FLUSH_ALL */
void mpi_win_flush_all_(MPI_Fint*win, MPI_Fint*ierr)
{
  *ierr = mpi_win_flush_all(*win);
}

/** Fortran version for MPI_WIN_FLUSH_LOCAL */
void mpi_win_flush_local_(MPI_Fint*rank, MPI_Fint*win, MPI_Fint*ierr)
{
  *ierr = mpi_win_flush_local(*rank, *win);
}

/** Fortran version for MPI_WIN_FLUSH_LOCAL_ALL */
void mpi_win_flush_local_all_(MPI_Fint*win, MPI_Fint*ierr)
{
  *ierr = mpi_win_flush_local_all(*win);
}

/** Fortran version for MPI_WIN_SYNC */
void mpi_win_sync_(MPI_Fint*win, MPI_Fint*ierr)
{
  *ierr = mpi_win_sync(*win);
}

/** Fortran version for MPI_FILE_OPEN */
void mpi_file_open_(MPI_Fint*comm, char*filename, MPI_Fint*amode, MPI_Fint*info, MPI_Fint*fh, MPI_Fint*ierr, int filename_len)
{
  MPI_File c_fh;
  char*c_filename = nm_mpif_string_f2c(filename, filename_len);
  *ierr = mpi_file_open(*comm, c_filename, *amode, *info, &c_fh);
  *fh = c_fh;
  padico_free(c_filename);
}

/** Fortran version for MPI_FILE_CLOSE */
void mpi_file_close_(MPI_Fint*fh, MPI_Fint*ierr)
{
  MPI_File c_fh = *fh;
  *ierr = mpi_file_close(&c_fh);
  *fh = c_fh;
}

/** Fortran version for MPI_FILE_DELETE */
void mpi_file_delete_(char*filename, MPI_Fint*info, MPI_Fint*ierr, int filename_len)
{
  char*c_filename = nm_mpif_string_f2c(filename, filename_len);
  *ierr = mpi_file_delete(c_filename, *info);
  padico_free(c_filename);
}

/** Fortran version for MPI_FILE_SYNC */
void mpi_file_sync_(MPI_Fint*fh, MPI_Fint*ierr)
{
  *ierr = mpi_file_sync(*fh);
}

/** Fortran version for MPI_FILE_SET_ATOMICITY */
void mpi_file_set_atomicity_(MPI_Fint*fh, MPI_Fint*flag, MPI_Fint*ierr)
{
  *ierr = mpi_file_set_atomicity(*fh, *flag);
}

/** Fortran version for MPI_FILE_GET_ATOMICITY */
void mpi_file_get_atomicity_(MPI_Fint*fh, MPI_Fint*flag, MPI_Fint*ierr)
{
  int c_flag;
  *ierr = mpi_file_get_atomicity(*fh, &c_flag);
  *flag = c_flag;
}

/** Fortran version for MPI_FILE_SET_INFO */
void mpi_file_set_info_(MPI_Fint*fh, MPI_Fint*info, MPI_Fint*ierr)
{
  *ierr = mpi_file_set_info(*fh, *info);
}

/** Fortran version for MPI_FILE_GET_INFO */
void mpi_file_get_info_(MPI_Fint*fh, MPI_Fint*info_used, MPI_Fint*ierr)
{
  MPI_Info c_info_used;
  *ierr = mpi_file_get_info(*fh, &c_info_used);
  *info_used = c_info_used;
}

/** Fortran version for MPI_FILE_SEEK */
void mpi_file_seek_(MPI_Fint*fh, MPI_Offset*offset, MPI_Fint*whence, MPI_Fint*ierr)
{
  *ierr = mpi_file_seek(*fh, *offset, *whence);
}

/** Fortran version for MPI_FILE_GET_POSITION */
void mpi_file_get_position_(MPI_Fint*fh, MPI_Offset*offset, MPI_Fint*ierr)
{
  *ierr = mpi_file_get_position(*fh, offset);
}

/** Fortran version for MPI_FILE_GET_BYTE_OFFSET */
void mpi_file_get_byte_offset_(MPI_Fint*fh, MPI_Offset*offset, MPI_Offset*disp, MPI_Fint*ierr)
{
  *ierr = mpi_file_get_byte_offset(*fh, *offset, disp);
}

/** Fortran version for MPI_FILE_PREALLOCATE */
void mpi_file_preallocate_(MPI_Fint*fh, MPI_Offset*size, MPI_Fint*ierr)
{
  *ierr = mpi_file_preallocate(*fh, *size);
}

/** Fortran version for MPI_FILE_SET_SIZE */
void mpi_file_set_size_(MPI_Fint*fh, MPI_Offset*size, MPI_Fint*ierr)
{
  *ierr = mpi_file_set_size(*fh, *size);
}

/** Fortran version for MPI_FILE_GET_SIZE */
void mpi_file_get_size_(MPI_Fint*fh, MPI_Offset*size, MPI_Fint*ierr)
{
  *ierr = mpi_file_get_size(*fh, size);
}

/** Fortran version for MPI_FILE_GET_AMODE */
void mpi_file_get_amode_(MPI_Fint*fh, MPI_Fint*amode, MPI_Fint*ierr)
{
  int c_amode;
  *ierr = mpi_file_get_amode(*fh, &c_amode);
  *amode = c_amode;
}

/** Fortran version for MPI_FILE_GET_GROUP */
void mpi_file_get_group_(MPI_Fint*fh, MPI_Fint*group, MPI_Fint*ierr)
{
  MPI_Group c_group;
  *ierr = mpi_file_get_group(*fh, &c_group);
  *group = c_group;
}

/** Fortran version for MPI_FILE_GET_TYPE_EXTENT */
void mpi_file_get_type_extent_(MPI_Fint*fh, MPI_Fint*datatype, MPI_Aint*extent, MPI_Fint*ierr)
{
  *ierr = mpi_file_get_type_extent(*fh, *datatype, extent);
}

/** Fortran version for MPI_FILE_SET_VIEW */
void mpi_file_set_view_(MPI_Fint*fh, MPI_Offset*disp, MPI_Fint*etype, MPI_Fint*filetype, char*datarep, MPI_Fint*info, MPI_Fint*ierr, int datarep_len)
{
  char*c_datarep = nm_mpif_string_f2c(datarep, datarep_len);
  *ierr = mpi_file_set_view(*fh, *disp, *etype, *filetype, c_datarep, *info);
  padico_free(c_datarep);
}

/** Fortran version for MPI_FILE_GET_VIEW */
void mpi_file_get_view_(MPI_Fint*fh, MPI_Offset*disp, MPI_Fint*etype, MPI_Fint*filetype, char*datarep, MPI_Fint*ierr, int datarep_len)
{
  MPI_Datatype c_etype, c_filetype;
  char*c_datarep = padico_malloc(sizeof(char) * (datarep_len + 1));
  *ierr = mpi_file_get_view(*fh, disp, &c_etype, &c_filetype, c_datarep);
  nm_mpif_string_c2f(c_datarep, datarep, datarep_len);
  *etype = c_etype;
  *filetype = c_filetype;
  padico_free(c_datarep);
}

/** Fortran version for MPI_FILE_READ */
void mpi_file_read_(MPI_Fint*fh, void*buf, MPI_Fint*count, MPI_Fint*datatype, MPI_Fint*status, MPI_Fint*ierr)
{
  MPI_Status _status;
  NM_MPI_F2C_BOTTOM(buf);
  *ierr = mpi_file_read(*fh, buf, *count, *datatype, &_status);
  mpi_status_c2f(&_status, status);
}

/** Fortran version for MPI_FILE_WRITE */
void mpi_file_write_(MPI_Fint*fh, void*buf, MPI_Fint*count, MPI_Fint*datatype, MPI_Fint*status, MPI_Fint*ierr)
{
  MPI_Status _status;
  NM_MPI_F2C_BOTTOM(buf);
  *ierr = mpi_file_write(*fh, buf, *count, *datatype, &_status);
  mpi_status_c2f(&_status, status);
}

/** Fortran version for MPI_FILE_READ_AT */
void mpi_file_read_at_(MPI_Fint*fh, MPI_Offset*offset, void*buf, MPI_Fint*count, MPI_Fint*datatype, MPI_Fint*status, MPI_Fint*ierr)
{
  MPI_Status _status;
  *ierr = mpi_file_read_at(*fh, *offset, buf, *count, *datatype, &_status);
  mpi_status_c2f(&_status, status);
}

/** Fortran version for MPI_FILE_WRITE_AT */
void mpi_file_write_at_(MPI_Fint*fh, MPI_Offset*offset, void*buf, MPI_Fint*count, MPI_Fint*datatype, MPI_Fint*status, MPI_Fint*ierr)
{
  MPI_Status _status;
  NM_MPI_F2C_BOTTOM(buf);
  *ierr = mpi_file_write_at(*fh, *offset, buf, *count, *datatype, &_status);
  mpi_status_c2f(&_status, status);
}

/** Fortran version for MPI_FILE_READ_ALL */
void mpi_file_read_all_(MPI_Fint*fh, void*buf, MPI_Fint*count, MPI_Fint*datatype, MPI_Fint*status, MPI_Fint*ierr)
{
  MPI_Status _status;
  NM_MPI_F2C_BOTTOM(buf);
  *ierr = mpi_file_read_all(*fh, buf, *count, *datatype, &_status);
  mpi_status_c2f(&_status, status);
}

/** Fortran version for MPI_FILE_WRITE_ALL */
void mpi_file_write_all_(MPI_Fint*fh, void*buf, MPI_Fint*count, MPI_Fint*datatype, MPI_Fint*status, MPI_Fint*ierr)
{
  MPI_Status _status;
  NM_MPI_F2C_BOTTOM(buf);
  *ierr = mpi_file_write_all(*fh, buf, *count, *datatype, &_status);
  mpi_status_c2f(&_status, status);
}

/** Fortran version for MPI_FILE_READ_AT_ALL */
void mpi_file_read_at_all_(MPI_Fint*fh, MPI_Offset*offset, void*buf, MPI_Fint*count, MPI_Fint*datatype, MPI_Fint*status, MPI_Fint*ierr)
{
  MPI_Status _status;
  NM_MPI_F2C_BOTTOM(buf);
  *ierr = mpi_file_read_at_all(*fh, *offset, buf, *count, *datatype, &_status);
  mpi_status_c2f(&_status, status);
}

/** Fortran version for MPI_FILE_WRITE_AT_ALL */
void mpi_file_write_at_all_(MPI_Fint*fh, MPI_Offset*offset, void*buf, MPI_Fint*count, MPI_Fint*datatype, MPI_Fint*status, MPI_Fint*ierr)
{
  MPI_Status _status;
  NM_MPI_F2C_BOTTOM(buf);
  *ierr = mpi_file_write_at_all(*fh, *offset, buf, *count, *datatype, &_status);
  mpi_status_c2f(&_status, status);
}

/** Fortran version for MPI_FILE_READ_ALL_BEGIN */
void mpi_file_read_all_begin_(MPI_Fint*fh, void*buf, MPI_Fint*count, MPI_Fint*datatype, MPI_Fint*ierr)
{
  NM_MPI_F2C_BOTTOM(buf);
  *ierr = mpi_file_read_all_begin(*fh, buf, *count, *datatype);
}

/** Fortran version for MPI_FILE_READ_ALL_END */
void mpi_file_read_all_end_(MPI_Fint*fh, void*buf, MPI_Fint*status, MPI_Fint*ierr)
{
  MPI_Status _status;
  *ierr = mpi_file_read_all_end(*fh, buf, &_status);
  mpi_status_c2f(&_status, status);
}

/** Fortran version for MPI_FILE_WRITE_ALL_BEGIN */
void mpi_file_write_all_begin_(MPI_Fint*fh, void*buf, MPI_Fint*count, MPI_Fint*datatype, MPI_Fint*ierr)
{
  NM_MPI_F2C_BOTTOM(buf);
  *ierr = mpi_file_write_all_begin(*fh, buf, *count, *datatype);
}

/** Fortran version for MPI_FILE_WRITE_ALL_END */
void mpi_file_write_all_end_(MPI_Fint*fh, void*buf, MPI_Fint*status, MPI_Fint*ierr)
{
  MPI_Status _status;
  *ierr = mpi_file_write_all_end(*fh, buf, &_status);
  mpi_status_c2f(&_status, status);
}

/** Fortran version for MPI_FILE_READ_AT_ALL_BEGIN */
void mpi_file_read_at_all_begin_(MPI_Fint*fh, MPI_Offset*offset, void*buf, MPI_Fint*count, MPI_Fint*datatype, MPI_Fint*ierr)
{
  NM_MPI_F2C_BOTTOM(buf);
  *ierr = mpi_file_read_at_all_begin(*fh, *offset, buf, *count, *datatype);
}

/** Fortran version for MPI_FILE_READ_AT_ALL_END */
void mpi_file_read_at_all_end_(MPI_Fint*fh, void*buf, MPI_Fint*status, MPI_Fint*ierr)
{
  MPI_Status _status;
  *ierr = mpi_file_read_at_all_end(*fh, buf, &_status);
  mpi_status_c2f(&_status, status);
}

/** Fortran version for MPI_FILE_WRITE_AT_ALL_BEGIN */
void mpi_file_write_at_all_begin_(MPI_Fint*fh, MPI_Offset*offset, void*buf, MPI_Fint*count, MPI_Fint*datatype, MPI_Fint*ierr)
{
  NM_MPI_F2C_BOTTOM(buf);
  *ierr = mpi_file_write_at_all_begin(*fh, *offset, buf, *count, *datatype);
}

/** Fortran version for MPI_FILE_WRITE_AT_ALL_END */
void mpi_file_write_at_all_end_(MPI_Fint*fh, void*buf, MPI_Fint*status, MPI_Fint*ierr)
{
  MPI_Status _status;
  *ierr = mpi_file_write_at_all_end(*fh, buf, &_status);
  mpi_status_c2f(&_status, status);
}

/** Fortran version for MPI_FILE_SET_ERRHANDLER */
void mpi_file_set_errhandler_(MPI_Fint*fh, MPI_Fint*errhandler, MPI_Fint*ierr)
{
  *ierr = mpi_file_set_errhandler(*fh, *errhandler);
}

/** Fortran version for MPI_FILE_GET_ERRHANDLER */
void mpi_file_get_errhandler_(MPI_Fint*fh, MPI_Fint*errhandler, MPI_Fint*ierr)
{
  MPI_Errhandler c_errhandler;
  *ierr = mpi_file_get_errhandler(*fh, &c_errhandler);
  *errhandler = c_errhandler;
}

/** Fortran version for MPI_FILE_CALL_ERRHANDLER */
void mpi_file_call_errhandler_(MPI_Fint*fh, MPI_Fint*errorcode, MPI_Fint*ierr)
{
  *ierr=mpi_file_call_errhandler(*fh, *errorcode);
}

/** Fortran version for MPI_FILE_GET_POSITION_SHARED */
void mpi_file_get_position_shared_(MPI_Fint*fh, MPI_Offset*offset, MPI_Fint*ierr)
{
  *ierr = mpi_file_get_position_shared(*fh, offset);
}

/** Fortran version for MPI_FILE_SEEK_SHARED */
void mpi_file_seek_shared_(MPI_Fint*fh, MPI_Offset*offset, MPI_Fint*whence, MPI_Fint*ierr)
{
  *ierr = mpi_file_seek_shared(*fh, *offset, *whence);
}

/** Fortran version for MPI_FILE_READ_SHARED */
void mpi_file_read_shared_(MPI_Fint*fh, void*buf, MPI_Fint*count, MPI_Fint*datatype, MPI_Fint*status, MPI_Fint*ierr)
{
  MPI_Status _status;
  NM_MPI_F2C_BOTTOM(buf);
  *ierr = mpi_file_read_shared(*fh, buf, *count, *datatype, &_status);
  mpi_status_c2f(&_status, status);
}

/** Fortran version for MPI_FILE_WRITE_SHARED */
void mpi_file_write_shared_(MPI_Fint*fh, void*buf, MPI_Fint*count, MPI_Fint*datatype, MPI_Fint*status, MPI_Fint*ierr)
{
  MPI_Status _status;
  NM_MPI_F2C_BOTTOM(buf);
  *ierr = mpi_file_write_shared(*fh, buf, *count, *datatype, &_status);
  mpi_status_c2f(&_status, status);
}

/** Fortran version for MPI_FILE_IREAD_SHARED */
void mpi_file_iread_shared_(MPI_Fint*fh, void*buf, MPI_Fint*count, MPI_Fint*datatype, MPI_Fint*request, MPI_Fint*ierr)
{
  MPI_Request c_request;
  NM_MPI_F2C_BOTTOM(buf);
  *ierr = mpi_file_iread_shared(*fh, buf, *count, *datatype, &c_request);
  *request = c_request;
}

/** Fortran version for MPI_FILE_IWRITE_SHARED */
void mpi_file_iwrite_shared_(MPI_Fint*fh, void*buf, MPI_Fint*count, MPI_Fint*datatype, MPI_Fint*request, MPI_Fint*ierr)
{
  MPI_Request c_request;
  NM_MPI_F2C_BOTTOM(buf);
  *ierr = mpi_file_iwrite_shared(*fh, buf, *count, *datatype, &c_request);
  *request = c_request;
}

/** Fortran version for MPI_FILE_READ_ORDERED */
void mpi_file_read_ordered_(MPI_Fint*fh, void*buf, MPI_Fint*count, MPI_Fint*datatype, MPI_Fint*status, MPI_Fint*ierr)
{
  MPI_Status _status;
  NM_MPI_F2C_BOTTOM(buf);
  *ierr = mpi_file_read_ordered(*fh, buf, *count, *datatype, &_status);
  mpi_status_c2f(&_status, status);
}

/** Fortran version for MPI_FILE_WRITE_ORDERED */
void mpi_file_write_ordered_(MPI_Fint*fh, void*buf, MPI_Fint*count, MPI_Fint*datatype, MPI_Fint*status, MPI_Fint*ierr)
{
  MPI_Status _status;
  NM_MPI_F2C_BOTTOM(buf);
  *ierr = mpi_file_write_ordered(*fh, buf, *count, *datatype, &_status);
  mpi_status_c2f(&_status, status);
}

/** Fortran version for MPI_FILE_WRITE_ORDERED_BEGIN */
void mpi_file_write_ordered_begin_(MPI_Fint*fh, void*buf, MPI_Fint*count, MPI_Fint*datatype, MPI_Fint*ierr)
{
  NM_MPI_F2C_BOTTOM(buf);
  *ierr = mpi_file_write_ordered_begin(*fh, buf, *count, *datatype);
}

/** Fortran version for MPI_FILE_WRITE_ORDERED_END */
void mpi_file_write_ordered_end_(MPI_Fint*fh, void*buf, MPI_Fint*status, MPI_Fint*ierr)
{
  MPI_Status _status;
  *ierr = mpi_file_write_ordered_end(*fh, buf, &_status);
  mpi_status_c2f(&_status, status);
}

/** Fortran version for MPI_FILE_READ_ORDERED_BEGIN */
void mpi_file_read_ordered_begin_(MPI_Fint*fh, void*buf, MPI_Fint*count, MPI_Fint*datatype, MPI_Fint*ierr)
{
  NM_MPI_F2C_BOTTOM(buf);
  *ierr = mpi_file_read_ordered_begin(*fh, buf, *count, *datatype);
}

/** Fortran version for MPI_FILE_READ_ORDERED_END */
void mpi_file_read_ordered_end_(MPI_Fint*fh, void*buf, MPI_Fint*status, MPI_Fint*ierr)
{
  MPI_Status _status;
  *ierr = mpi_file_read_ordered_end(*fh, buf, &_status);
  mpi_status_c2f(&_status, status);
}

/** Fortran version for MPI_FILE_IWRITE */
void mpi_file_iwrite_(MPI_Fint*fh, void*buf, MPI_Fint*count, MPI_Fint*datatype, MPI_Fint*request, MPI_Fint*ierr)
{
  MPI_Request c_request;
  NM_MPI_F2C_BOTTOM(buf);
  *ierr = mpi_file_iwrite(*fh, buf, *count, *datatype, &c_request);
  *request = c_request;
}

/** Fortran version for MPI_FILE_IREAD */
void mpi_file_iread_(MPI_Fint*fh, void*buf, MPI_Fint*count, MPI_Fint*datatype, MPI_Fint*request, MPI_Fint*ierr)
{
  MPI_Request c_request;
  NM_MPI_F2C_BOTTOM(buf);
  *ierr = mpi_file_iread(*fh, buf, *count, *datatype, &c_request);
  *request = c_request;
}

/** Fortran version for MPI_FILE_IWRITE_AT */
void mpi_file_iwrite_at_(MPI_Fint*fh, MPI_Offset*offset, void*buf,
                         MPI_Fint*count, MPI_Fint*datatype, MPI_Fint*request, MPI_Fint*ierr)
{
  MPI_Request c_request;
  NM_MPI_F2C_BOTTOM(buf);
  *ierr = mpi_file_iwrite_at(*fh, *offset, buf, *count, *datatype, &c_request);
  *request = c_request;
}

/** Fortran version for MPI_FILE_IREAD_AT */
void mpi_file_iread_at_(MPI_Fint*fh, MPI_Offset*offset, void*buf, MPI_Fint*count,
                        MPI_Fint*datatype, MPI_Fint*request, MPI_Fint*ierr)
{
  MPI_Request c_request;
  NM_MPI_F2C_BOTTOM(buf);
  *ierr = mpi_file_iread_at(*fh, *offset, buf, *count, *datatype, &c_request);
  *request = c_request;
}

void mpi_put_(void*origin_addr, MPI_Fint*origin_count, MPI_Fint*origin_datatype,
              MPI_Fint*target_rank, MPI_Aint*target_disp, MPI_Fint*target_count, MPI_Fint*target_datatype,
              MPI_Fint*win, MPI_Fint*ierr)
{
  NM_MPI_F2C_BOTTOM(origin_addr);
  *ierr = mpi_put(origin_addr, *origin_count, *origin_datatype,
                  *target_rank, *target_disp, *target_count, *target_datatype,
                  *win);
}

void mpi_get_(void*origin_addr, MPI_Fint*origin_count, MPI_Fint*origin_datatype,
              MPI_Fint*target_rank, MPI_Aint*target_disp, MPI_Fint*target_count, MPI_Fint*target_datatype,
              MPI_Fint*win, MPI_Fint*ierr)
{
  NM_MPI_F2C_BOTTOM(origin_addr);
  *ierr = mpi_get(origin_addr, *origin_count, *origin_datatype,
                  *target_rank, *target_disp, *target_count, *target_datatype,
                  *win);
}

void mpi_accumulate_(void*origin_addr, MPI_Fint*origin_count, MPI_Fint*origin_datatype,
                     MPI_Fint*target_rank, MPI_Aint*target_disp, MPI_Fint*target_count,
                     MPI_Fint*target_datatype, MPI_Fint*op, MPI_Fint*win, MPI_Fint*ierr)
{
  NM_MPI_F2C_BOTTOM(origin_addr);
  *ierr = mpi_accumulate(origin_addr, *origin_count, *origin_datatype,
                         *target_rank, *target_disp, *target_count,
                         *target_datatype, *op, *win);
}

void mpi_get_accumulate_(void*origin_addr, MPI_Fint*origin_count, MPI_Fint*origin_datatype,
                         void*result_addr, MPI_Fint*result_count, MPI_Fint*result_datatype,
                         MPI_Fint*target_rank, MPI_Aint*target_disp, MPI_Fint*target_count,
                         MPI_Fint*target_datatype, MPI_Fint*op, MPI_Fint*win, MPI_Fint*ierr)
{
  NM_MPI_F2C_BOTTOM(origin_addr);
  NM_MPI_F2C_BOTTOM(result_addr);
  *ierr = mpi_get_accumulate(origin_addr, *origin_count, *origin_datatype,
                             result_addr, *result_count, *result_datatype,
                             *target_rank, *target_disp, *target_count,
                             *target_datatype, *op, *win);
}

void mpi_fetch_and_op_(void*origin_addr, void*result_addr,
                       MPI_Fint*datatype, MPI_Fint*target_rank, MPI_Aint*target_disp,
                       MPI_Fint*op, MPI_Fint*win, MPI_Fint*ierr)
{
  NM_MPI_F2C_BOTTOM(origin_addr);
  NM_MPI_F2C_BOTTOM(result_addr);
  *ierr = mpi_fetch_and_op(origin_addr, result_addr,
                           *datatype, *target_rank, *target_disp,
                           *op, *win);
}

void mpi_compare_and_swap_(void*origin_addr, void*compare_addr,
                           void*result_addr, MPI_Fint*datatype, MPI_Aint*target_rank,
                           MPI_Fint*target_disp, MPI_Fint*win, MPI_Fint*ierr)
{
  NM_MPI_F2C_BOTTOM(origin_addr);
  NM_MPI_F2C_BOTTOM(compare_addr);
  NM_MPI_F2C_BOTTOM(result_addr);
  *ierr = mpi_compare_and_swap(origin_addr, compare_addr,
                               result_addr, *datatype, *target_rank,
                               *target_disp, *win);
}

void mpi_rput_(void*origin_addr, MPI_Fint*origin_count, MPI_Fint*origin_datatype,
               MPI_Fint*target_rank, MPI_Aint*target_disp, MPI_Fint*target_count, MPI_Fint*target_datatype,
               MPI_Fint*win, MPI_Fint*request, MPI_Fint*ierr)
{
  MPI_Request c_request;
  NM_MPI_F2C_BOTTOM(origin_addr);
  *ierr = mpi_rput(origin_addr, *origin_count, *origin_datatype,
                   *target_rank, *target_disp, *target_count, *target_datatype,
                   *win, &c_request);
  *request = c_request;
}

void mpi_rget_(void*origin_addr, MPI_Fint*origin_count,
               MPI_Fint*origin_datatype, MPI_Fint*target_rank,
               MPI_Aint*target_disp, MPI_Fint*target_count,
               MPI_Fint*target_datatype, MPI_Fint*win,
               MPI_Fint*request, MPI_Fint*ierr)
{
  MPI_Request c_request;
  NM_MPI_F2C_BOTTOM(origin_addr);
  *ierr = mpi_rget(origin_addr, *origin_count,
                   *origin_datatype, *target_rank,
                   *target_disp, *target_count,
                   *target_datatype, *win,
                   &c_request);
  *request = c_request;
}

void mpi_raccumulate_(void*origin_addr, MPI_Fint*origin_count,
                      MPI_Fint*origin_datatype, MPI_Fint*target_rank,
                      MPI_Aint*target_disp, MPI_Fint*target_count,
                      MPI_Fint*target_datatype, MPI_Fint*op, MPI_Fint*win,
                      MPI_Fint*request, MPI_Fint*ierr)
{
  MPI_Request c_request;
  NM_MPI_F2C_BOTTOM(origin_addr);
  *ierr = mpi_raccumulate(origin_addr, *origin_count,
                          *origin_datatype, *target_rank,
                          *target_disp, *target_count,
                          *target_datatype, *op, *win,
                          &c_request);
  *request = c_request;
}

void mpi_rget_accumulate_(void*origin_addr, MPI_Fint*origin_count,
                          MPI_Fint*origin_datatype, void*result_addr,
                          MPI_Fint*result_count, MPI_Fint*result_datatype,
                          MPI_Fint*target_rank, MPI_Aint*target_disp, MPI_Fint*target_count,
                          MPI_Fint*target_datatype, MPI_Fint*op, MPI_Fint*win,
                          MPI_Fint*request, MPI_Fint*ierr)
{
  MPI_Request c_request;
  NM_MPI_F2C_BOTTOM(origin_addr);
  NM_MPI_F2C_BOTTOM(result_addr);
  *ierr = mpi_rget_accumulate(origin_addr, *origin_count,
                              *origin_datatype, result_addr,
                              *result_count, *result_datatype,
                              *target_rank, *target_disp, *target_count,
                              *target_datatype, *op, *win,
                              &c_request);
  *request = c_request;
}
#endif /* NMAD_FORTRAN_TARGET_NONE */

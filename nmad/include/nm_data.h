/*
 * NewMadeleine
 * Copyright (C) 2015-2023 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULA R PURPOSE.  See the GNU
 * General Public License for more details.
 */

/** @ingroup nm_public
 * @file
 * High-level data manipulation through iterators
 */

#ifndef NM_DATA_H
#define NM_DATA_H

#include <assert.h>
#include <string.h>

#include <Padico/Puk.h>

/** @ingroup nm_public
 * @defgroup nm_data Internal data representation
 *
 * nmad internal data representation is an **iterator**, *ie* the
 * user essentially has to write a ::nm_data_traversal_t function
 * which applies a function to every contiguous chunk of the data type.
 * The iterator may use an internal state of size up to #_NM_DATA_CONTENT_SIZE.
 * The types must then be declared using macro #NM_DATA_TYPE which associate the
 * iterator function with the type for its internal state, the given name, and
 * generates some typed accessor functions.
 *
 * The main patterns to access data are:
 *   * **traversal**: apply a given function to every chunk of data. The API is
 *     function nm_data_traversal_apply(), using a user-supplied ::nm_data_apply_t.
 *   * **generator**: iterator (init/next/destroy) that returns a pointer to every
 *     chunk of data. The API is nm_data_generator_init(), nm_data_generator_next(),
 *     nm_data_generator_destroy().
 *   * **slicer**: copy blocks of data with user-defined bounds for blocks, not
 *     necessary following in-memory data layout. The API is: nm_data_slicer_init(),
 *     nm_data_slicer_forward(), nm_data_slicer_copy_from(),
 *     nm_data_slicer_copy_to(), nm_data_slicer_destroy().
 *
 * For a user-defined data type descriptor, user must define a traversal
 * function ::nm_data_traversal_t, and optionnaly an optimized generator
 * with ::nm_data_generator_init_t, ::nm_data_generator_next_t,
 * ::nm_data_generator_destroy_t.
 *
 * There is a collection of @ref nm_data_builtin for common cases.
 * Manipulation of such data representation is usually done through
 * @ref nm_data_helper in higher layers, and @ref nm_data_slicer in drivers.
 *
 * @example nm_sr_custom_data.c
 */

/** @ingroup nm_data
 * @defgroup nm_data_descriptor Data descriptors
 * Core types to define data descriptors
 * @{
 */


/* ** Data descriptor ************************************** */

/** maximum size of content descriptor for nm_data */
#define _NM_DATA_CONTENT_SIZE 64
/** maximum content size for generators */
#define _NM_DATA_GENERATOR_SIZE 64

#ifndef __ia64__
#ifdef _FORTIFY_SOURCE
#define NM_DATA_USE_UCONTEXT_SLICER
#else
/** whether to use coroutine-based generators */
#define NM_DATA_USE_COROUTINE_GENERATOR
/** whether to use coroutine-based slicers */
#define NM_DATA_USE_COROUTINE_SLICER
#endif
#else
/* setjmp/longjmp is not properly supported on Itanium.
 * It works ok to unwind on the same stack but not for stack jumping.
 * (register stack is not saved/restored)
 */
#define NM_DATA_USE_UCONTEXT_GENERATOR
#endif

/* forward declaration so that operators declared inside struct nm_data_s may
 * take struct nm_data_s* as parameters
 */
struct nm_data_s;

/** function to apply to each data chunk upon traversal */
typedef void (*nm_data_apply_t)(void*ptr, nm_len_t len, void*_context);

/** funtion to traverse data with app layout, i.e. ='map' functional operation
 * @param _content content description of data type
 * @param apply function to apply to all chunks
 * @param _context context pointer given to apply function
 */
typedef void (*nm_data_traversal_t)(const void*_content, const nm_data_apply_t apply, void*_context);

/** state of a data generator */
struct nm_data_generator_s
{
  const struct nm_data_s*p_data;
  char _bytes[_NM_DATA_GENERATOR_SIZE]; /** block of bytes for the type dependent function */
};

/** initializes a generator (i.e. semi-coroutine) for the given data type */
typedef void (*nm_data_generator_init_t)(const struct nm_data_s*p_data, void*_generator);
/** chunk of data returned by generators */
struct nm_data_chunk_s
{
  void*ptr;     /**< pointer to chunk */
  nm_len_t len; /**< length of chunk */
};
#define NM_DATA_CHUNK_NULL ((struct nm_data_chunk_s){ .ptr = NULL, .len = 0 })
static inline int nm_data_chunk_isnull(const struct nm_data_chunk_s*p_chunk)
{
  return (p_chunk->ptr == NULL);
}

/** returns next data chunk for the given generator.
 * _content and _generator must be consistent accross calls
 * no error checking is done
 */
typedef struct nm_data_chunk_s (*nm_data_generator_next_t)(const struct nm_data_s*p_data, void*_generator);

/** destroys resources allocated by generator */
typedef void (*nm_data_generator_destroy_t)(const struct nm_data_s*p_data, void*_generator);

/** @internal used internally by #NM_DATA_TYPE in ::nm_data_s */
void                   nm_data_generator_coroutine_init(const struct nm_data_s*p_data, void*_generator);
/** @internal used internally by #NM_DATA_TYPE in ::nm_data_s */
struct nm_data_chunk_s nm_data_generator_coroutine_next(const struct nm_data_s*p_data, void*_generator);
/** @internal used internally by #NM_DATA_TYPE in ::nm_data_s */
void                   nm_data_generator_coroutine_destroy(const struct nm_data_s*p_data, void*_generator);

/** @internal used internally by #NM_DATA_TYPE in ::nm_data_s */
void                   nm_data_generator_generic_init(const struct nm_data_s*p_data, void*_generator);
/** @internal used internally by #NM_DATA_TYPE in ::nm_data_s */
struct nm_data_chunk_s nm_data_generator_generic_next(const struct nm_data_s*p_data, void*_generator);

#ifdef NM_DATA_USE_COROUTINE_GENERATOR
#define nm_data_generator_default_init    &nm_data_generator_coroutine_init
#define nm_data_generator_default_next    &nm_data_generator_coroutine_next
#define nm_data_generator_default_destroy &nm_data_generator_coroutine_destroy
#else /* NM_DATA_USE_COROUTINE_GENERATOR */
#define nm_data_generator_default_init    &nm_data_generator_generic_init
#define nm_data_generator_default_next    &nm_data_generator_generic_next
#define nm_data_generator_default_destroy NULL
#endif /* NM_DATA_USE_COROUTINE_GENERATOR */

/** block of static properties for a given data descriptor */
struct nm_data_properties_s
{
  nm_len_t blocks; /**< number of blocks; -1 if properties are not initialized */
  nm_len_t size;   /**< total size in bytes (accumulator) */
  int is_contig;   /**< is data contiguous */
};

/** function to compute data properties */
typedef void (*nm_data_properties_compute_t)(struct nm_data_s*p_data);

/** set of operations available on data type.
 */
struct nm_data_ops_s
{
  nm_data_traversal_t          p_traversal; /**< operation to apply a given function to all chunks of data (required) */
  nm_data_generator_init_t     p_generator_init;    /**< initializes a new generator (chunks enumerator) (optionnal) */
  nm_data_generator_next_t     p_generator_next;    /**< get next chunk from generator (optionnal) */
  nm_data_generator_destroy_t  p_generator_destroy; /**< destroy a generator after use (optionnal, may be NULL even if p_generator_init != NULL) */
  nm_data_properties_compute_t p_properties_compute; /**< optimized function to compute data properties (optionnal)*/
};

/** @internal compute data properties; not for enduser, exported for use by inline function
 */
void nm_data_default_properties_compute(struct nm_data_s*p_data);

/** a data descriptor, used to pack/unpack data from app layout to/from contiguous buffers */
struct nm_data_s
{
  struct nm_data_ops_s ops;              /**< collection of iterators */
  struct nm_data_properties_s props;     /**< cache for properties */
  char _content[_NM_DATA_CONTENT_SIZE];  /**< placeholder for type-dependant content */
};
/** macro to generate typed functions to init/access data fields.
 * @param ENAME is the base name used to build symbols (must be a valid C token)
 * @param CONTENT_TYPE is the iterator internal state
 * @param OPS is a collection of operations of type ::nm_data_ops_s for the given type. At
 * least nm_data_ops_s::p_traversal field must be non-NULL; other fields may be NULL and will
 * be filled with default values.
 */
#define NM_DATA_TYPE(ENAME, CONTENT_TYPE, OPS)                          \
  static inline void nm_data_##ENAME##_set(struct nm_data_s*p_data, CONTENT_TYPE value) \
  {                                                                     \
    p_data->ops = *(OPS);                                               \
    assert(p_data->ops.p_traversal != NULL);                            \
    if(p_data->ops.p_properties_compute == NULL)                        \
      {                                                                 \
        p_data->ops.p_properties_compute = nm_data_default_properties_compute; \
      }                                                                 \
    assert(sizeof(CONTENT_TYPE) <= _NM_DATA_CONTENT_SIZE);              \
    CONTENT_TYPE*p_content = (CONTENT_TYPE*)&p_data->_content[0];       \
    *p_content = value;                                                 \
    p_data->props.blocks = -1;                                          \
    (*p_data->ops.p_properties_compute)(p_data);                        \
    if(p_data->ops.p_generator_init == NULL)                            \
      {                                                                 \
        if(p_data->props.is_contig)                                     \
          {                                                             \
            /* use default generator for contig types- no need to create contexts since we know there is a single chunk */ \
            p_data->ops.p_generator_init    = &nm_data_generator_generic_init; \
            p_data->ops.p_generator_next    = &nm_data_generator_generic_next; \
            p_data->ops.p_generator_destroy = NULL;                     \
          }                                                             \
        else                                                            \
          {                                                             \
            p_data->ops.p_generator_init    = nm_data_generator_default_init; \
            p_data->ops.p_generator_next    = nm_data_generator_default_next; \
            p_data->ops.p_generator_destroy = nm_data_generator_default_destroy; \
          }                                                             \
      }                                                                 \
  }                                                                     \
  static inline CONTENT_TYPE*nm_data_##ENAME##_content(const struct nm_data_s*p_data) \
  {                                                                     \
    return (CONTENT_TYPE*)p_data->_content;                             \
  }

/* ** datav data (dynamic vector of nm_data)
 */

/** initial size of an nm_datav */
#define NM_DATAV_INIT_SIZE 4

/** encapsulate a dynamic vector of nm_data */
struct nm_datav_s
{
  struct nm_data_s*p_data; /**< vector of nm_data; either dynamically allocated, or points to data[0] */
  struct nm_data_s data[NM_DATAV_INIT_SIZE];  /**< vector of data */
  int n_data;             /**< number of entries actually used in the above array */
  int allocated;          /**< allocated number of entries in p_data */
  int commited;
};

/** initialize a datav */
static inline void nm_datav_init(struct nm_datav_s*p_datav);
/** destroys a datav */
static inline void nm_datav_destroy(struct nm_datav_s*p_datav);
/** add a chunk of data to datav; given p_data content is copied. */
static inline void nm_datav_add_chunk_data(struct nm_datav_s*p_datav, const struct nm_data_s*p_data);
/** add a chunk of contiguous data to a datav */
static inline void nm_datav_add_chunk(struct nm_datav_s*p_datav, const void*ptr, nm_len_t len);
/** get the size (number of bytes) of data contained in the datav */
static inline nm_len_t nm_datav_size(struct nm_datav_s*p_datav);
/** 'uncommit' a datav: explicitely declare that nm_data pointing to this datav has been destroyed.
 * Use with care! */
static inline void nm_datav_uncommit(struct nm_datav_s*p_datav);

/** @} */

/* ** Built-in data types ********************************** */

/** @ingroup nm_data
 * @defgroup nm_data_builtin Built-in data types
 *
 * A collection of pre-defined data iterators for common data types
 * such as:
 *   * null data
 *   * contiguous data
 *   * iovecs
 *   * vector of nm_data
 *   * chunk extractor
 */

/** @ingroup nm_data_builtin
 * @{
 */

/** data descriptor for 'null' data
 */
struct nm_data_null_s
{
  int dummy; /**< unused, to avoid non-portable empty structure */
};
extern const struct nm_data_ops_s nm_data_ops_null;
NM_DATA_TYPE(null, struct nm_data_null_s, &nm_data_ops_null);
static inline void nm_data_null_build(struct nm_data_s*p_data)
{
  struct nm_data_null_s n = { 0 };
  nm_data_null_set(p_data, n);
}
static inline int nm_data_isnull(struct nm_data_s*p_data)
{
  return (p_data->ops.p_traversal == nm_data_ops_null.p_traversal);
}

/** data descriptor for contiguous data
 */
struct nm_data_contiguous_s
{
  void*ptr;     /**< base pointer for block */
  nm_len_t len; /**< data length */
};
extern const struct nm_data_ops_s nm_data_ops_contiguous;
NM_DATA_TYPE(contiguous, struct nm_data_contiguous_s, &nm_data_ops_contiguous);

static inline void nm_data_contiguous_build(struct nm_data_s*p_data, void*ptr, nm_len_t len)
{
  struct nm_data_contiguous_s dc;
  dc.ptr = ptr;
  dc.len = len;
  nm_data_contiguous_set(p_data, dc);
}

/** data descriptor for iov data
 */
struct nm_data_iov_s
{
  const struct iovec*v;
  int n;
};
extern const struct nm_data_ops_s nm_data_ops_iov;
NM_DATA_TYPE(iov, struct nm_data_iov_s, &nm_data_ops_iov);

static inline void nm_data_iov_build(struct nm_data_s*p_data, const struct iovec*v, int n)
{
  struct nm_data_iov_s di;
  di.v = v;
  di.n = n;
  nm_data_iov_set(p_data, di);
}

/** data descriptor for datav in a nm_data
 */
struct nm_data_datav_s
{
  struct nm_datav_s*p_datav;
};
extern const struct nm_data_ops_s nm_data_ops_datav;
NM_DATA_TYPE(datav, struct nm_data_datav_s, &nm_data_ops_datav);

/** frontend to build a nm_data from a datav */
static inline void nm_data_datav_build(struct nm_data_s*p_datav_data, struct nm_datav_s*p_datav)
{
  p_datav->commited = 1;
  struct nm_data_datav_s dv;
  dv.p_datav = p_datav;
  nm_data_datav_set(p_datav_data, dv);
}

struct nm_data_excerpt_s
{
  nm_len_t chunk_offset;
  nm_len_t chunk_len;
  struct nm_data_s*p_data;
};
extern const struct nm_data_ops_s nm_data_ops_excerpt;
NM_DATA_TYPE(excerpt, struct nm_data_excerpt_s, &nm_data_ops_excerpt);

/** build a data descriptor as an excerpt of another data.
 * @note p_inner_data is not copied; it must stay allocated as long as the excerpt */
static inline void nm_data_excerpt_build(struct nm_data_s*p_data, struct nm_data_s*p_inner_data,
                                         nm_len_t chunk_offset, nm_len_t chunk_len)
{
  struct nm_data_excerpt_s de;
  de.chunk_offset = chunk_offset;
  de.chunk_len = chunk_len;
  de.p_data = p_inner_data;
  nm_data_excerpt_set(p_data, de);
}

/** @}
 */

/* ** Helper functions ************************************* */

/** @ingroup nm_data
 * @defgroup nm_data_helper Data helper functions
 *
 * A collection of helper function to help manipulating nm_data
 */

/** @ingroup nm_data_helper
 * @{
 */

/** helper function to apply iterator to data
 */
static inline void nm_data_traversal_apply(const struct nm_data_s*p_data, nm_data_apply_t apply, void*_context)
{
  assert(p_data->ops.p_traversal != NULL);
  (*p_data->ops.p_traversal)((void*)p_data->_content, apply, _context);
}

/* ** helper functions for generator */

/** build a new generator for the given data type */
static inline void nm_data_generator_init(const struct nm_data_s*p_data, struct nm_data_generator_s*p_generator)
{
  assert(p_data->ops.p_generator_init != NULL);
  p_generator->p_data = p_data;
  (*p_data->ops.p_generator_init)(p_data, &p_generator->_bytes);
}

/** get the next chunk of data */
static inline struct nm_data_chunk_s nm_data_generator_next(struct nm_data_generator_s*p_generator)
{
  assert(p_generator->p_data->ops.p_generator_next != NULL);
  const struct nm_data_chunk_s chunk = (*p_generator->p_data->ops.p_generator_next)(p_generator->p_data, &p_generator->_bytes);
  return chunk;
}

/** destroy the generator after use */
static inline void nm_data_generator_destroy(struct nm_data_generator_s*p_generator)
{
  if(p_generator->p_data->ops.p_generator_destroy)
    (*p_generator->p_data->ops.p_generator_destroy)(p_generator->p_data, &p_generator->_bytes);
}

void nm_data_chunk_extractor_traversal(const struct nm_data_s*p_data, nm_len_t chunk_offset, nm_len_t chunk_len,
                                       nm_data_apply_t apply, void*_context);

void nm_data_aggregator_traversal(const struct nm_data_s*p_data, nm_data_apply_t apply, void*_context);

/** returns the properties block for the data */
static inline const struct nm_data_properties_s*nm_data_properties_get(const struct nm_data_s*p_data)
{
  return &p_data->props;
}

/** returns the amount of data contained in the descriptor */
static inline nm_len_t nm_data_size(const struct nm_data_s*p_data)
{
  const struct nm_data_properties_s*p_props = nm_data_properties_get((struct nm_data_s*)p_data);
  return p_props->size;
}

/** find base pointer for a data known to be contiguous */
void*nm_data_baseptr_get(const struct nm_data_s*p_data);

/** find base pointer for a data chunk known to be contiguous */
void*nm_data_chunk_baseptr_get(const struct nm_data_s*p_data, nm_len_t chunk_offset, nm_len_t chunk_len);

/** compute properties of the given chunk inside the data */
void nm_data_chunk_properties_compute(const struct nm_data_s*p_data, nm_len_t chunk_offset, nm_len_t chunk_len,
                                      struct nm_data_properties_s*p_props);

/** checksum data  */
uint32_t nm_data_checksum(const struct nm_data_s*p_data);

/** copy chunk of data from user layout to contiguous buffer */
void nm_data_copy_from(const struct nm_data_s*p_data, nm_len_t offset, nm_len_t len, void*destbuf);

/** copy chunk of data from contiguous buffer to user layout */
void nm_data_copy_to(const struct nm_data_s*p_data, nm_len_t offset, nm_len_t len, const void*srcbuf);

/** copy from nm_data to another nm_data */
void nm_data_copy(struct nm_data_s*p_dest, struct nm_data_s*p_from);

/** @} */

/* ** Data slicer ****************************************** */

/** @ingroup nm_data
 * @defgroup nm_data_slicer Data slicing primitives.
 *
 * Data slicer is a collection of primitives to get a series
 * of blocks with bounds controled by the caller from a
 * given nm_data iterator. Two flavors are available,
 * automatically selected dependaing on the CPU:
 *   * coroutine-based slicer, using setjmp/longjmp, used
 *     by default when available.
 *   * generator-based slicer, using the generator primitives,
 *     used in other cases.
 */

/** @ingroup nm_data_slicer
 * @{
 */

/** various kinds of slicer implementations */
typedef enum
  {
    NM_DATA_SLICER_NONE = 0,
    NM_DATA_SLICER_GENERATOR, /**< generator-based slicer, optimal for types with a dedicated generator; slow otherwise */
    NM_DATA_SLICER_COROUTINE, /**< coroutine longjmp-based slicer, optimal for types without generator, when stack jumping is possible with longjmp */
    NM_DATA_SLICER_UCONTEXT   /**< ucontext-based coroutine slicer, when stack jumping is not available through longjmp */
  } nm_data_slicer_kind_t;

/** internal state of a data slicer. Opaque structure for the user. */
typedef struct nm_data_slicer_s
{
  nm_data_slicer_kind_t kind;
  union
  {
    struct nm_data_slicer_generator_s*p_generator;
    struct nm_data_slicer_coroutine_s*p_coroutine;
    struct nm_data_slicer_ucontext_s*p_uslicer;
  };
} nm_data_slicer_t;

typedef enum nm_data_slicer_op_e
  {
    NM_SLICER_OP_NONE = 0,   /**< no operation selected */
    NM_SLICER_OP_FORWARD,    /**< fast forward in slicer */
    NM_SLICER_OP_COPY_FROM,  /**< copy from iterator to user buffer */
    NM_SLICER_OP_COPY_TO     /**< copy to iterator, from user buffer */
  } nm_data_slicer_op_t;

/** a slicer attached to no data */
#define NM_DATA_SLICER_NULL ((struct nm_data_slicer_s){ .kind = NM_DATA_SLICER_NONE })

/** tests whether a slicer is null */
static inline int nm_data_slicer_isnull(const nm_data_slicer_t*p_slicer)
{
  return (p_slicer->kind == NM_DATA_SLICER_NONE);
}

void nm_data_slicer_coroutine_init(nm_data_slicer_t*p_slicer, const struct nm_data_s*p_data);
void nm_data_slicer_coroutine_op(nm_data_slicer_t*p_slicer, void*ptr, nm_len_t len, nm_data_slicer_op_t op);
void nm_data_slicer_coroutine_destroy(nm_data_slicer_t*p_slicer);

void nm_data_slicer_generator_init(nm_data_slicer_t*p_slicer, const struct nm_data_s*p_data);
void nm_data_slicer_generator_op(nm_data_slicer_t*p_slicer, void*ptr, nm_len_t slice_len, nm_data_slicer_op_t op);
void nm_data_slicer_generator_destroy(nm_data_slicer_t*p_slicer);

void nm_data_slicer_ucontext_init(nm_data_slicer_t*p_slicer, const struct nm_data_s*p_data);
void nm_data_slicer_ucontext_op(nm_data_slicer_t*p_slicer, void*ptr, nm_len_t len, nm_data_slicer_op_t op);
void nm_data_slicer_ucontext_destroy(nm_data_slicer_t*p_slicer);

void nm_data_slicer_init(nm_data_slicer_t*p_slicer, const struct nm_data_s*p_data);
void nm_data_slicer_op(nm_data_slicer_t*p_slicer, void*ptr, nm_len_t len, nm_data_slicer_op_t op);
void nm_data_slicer_copy_from(nm_data_slicer_t*p_slicer, void*dest_ptr, nm_len_t slice_len);
void nm_data_slicer_copy_to(nm_data_slicer_t*p_slicer, const void*src_ptr, nm_len_t slice_len);
void nm_data_slicer_forward(nm_data_slicer_t*p_slicer, nm_len_t offset);
void nm_data_slicer_destroy(nm_data_slicer_t*p_slicer);


/** @}
 */

/* ********************************************************* */

/* inline functions */

static inline void nm_datav_init(struct nm_datav_s*p_datav)
{
  p_datav->p_data = &p_datav->data[0];
  p_datav->n_data = 0;
  p_datav->allocated = 0;
  p_datav->commited = 0;
}

static inline void nm_datav_destroy(struct nm_datav_s*p_datav)
{
  assert(p_datav->p_data != NULL);
  if(p_datav->p_data != &p_datav->data[0])
   {
     padico_free(p_datav->p_data);
     p_datav->p_data = NULL;
   }
 }

static inline void nm_datav_add_chunk_data(struct nm_datav_s*p_datav, const struct nm_data_s*p_data)
{
  assert(!p_datav->commited); /* cannot modify datav once it is used as a nm_data */
  if(p_datav->n_data == NM_DATAV_INIT_SIZE)
    {
      assert(p_datav->p_data == &p_datav->data[0]);
      p_datav->allocated = NM_DATAV_INIT_SIZE * 2;
      p_datav->p_data = (struct nm_data_s*)padico_malloc(p_datav->allocated * sizeof(struct nm_data_s));
      memcpy(p_datav->p_data, &p_datav->data[0], p_datav->n_data * sizeof(struct nm_data_s));
    }
  else if((p_datav->n_data > NM_DATAV_INIT_SIZE) &&
          (p_datav->n_data > p_datav->allocated - 1))
    {
      assert(p_datav->p_data != &p_datav->data[0]);
      p_datav->allocated *= 2;
      p_datav->p_data = (struct nm_data_s*)padico_realloc(p_datav->p_data, p_datav->allocated * sizeof(struct nm_data_s));
    }
  p_datav->p_data[p_datav->n_data] = *p_data;
  p_datav->n_data++;
}

static inline void nm_datav_add_chunk(struct nm_datav_s*p_datav, const void*ptr, nm_len_t len)
{
  struct nm_data_s data;
  nm_data_contiguous_build(&data, (void*)ptr, len);
  nm_datav_add_chunk_data(p_datav, &data);
}

static inline nm_len_t nm_datav_size(struct nm_datav_s*p_datav)
{
  nm_len_t size = 0;
  int i;
  for(i = 0; i < p_datav->n_data; i++)
    {
      size += nm_data_size(&p_datav->p_data[i]);
    }
  return size;
}

static inline void nm_datav_uncommit(struct nm_datav_s*p_datav)
{
  assert(p_datav->commited);
  p_datav->commited = 0;
}

#endif /* NM_DATA_H */

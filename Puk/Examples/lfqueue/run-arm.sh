#! /bin/sh

qs="atomic nblfq looqueue scq2 wcq simqueue koganpetrank crturn lcrq msqueue crdoublelink wfqueue ccqueue dsmqueue hqueue fcqueue"

export CXX="g++ -std=c++17"

TIMEOUT=240
MAXTHREADS=64

HERE=$PWD

for q in ${qs}; do
    echo
    echo "# queue: ${q}"
    echo

    nmroot=${HERE}/aarch64-${q}
    
    ( cd ../../../scripts ; ./pm2-build-packages --prefix=${nmroot}-mini --purge ./madmpi-mini.conf -- --enable-extqueues --disable-wfqueue --enable-lfqueue=${q} )

    rm -r ${nmroot}-mini/build
    
    ${nmroot}-mini/bin/padico-launch -n 2 --timeout ${TIMEOUT} ${nmroot}-mini/bin/nm_bench_sendrecv -N 10000 | timeout ${TIMEOUT} tee ${HERE}/${q}-sendrecv.dat
    
    ( cd ../../../scripts ; ./pm2-build-packages --prefix=${nmroot}-piom --purge ./madmpi.conf -- --enable-extqueues --disable-wfqueue --enable-lfqueue=${q} )

    rm -r ${nmroot}-piom/build

    ${nmroot}-piom/bin/padico-launch -n 2 --timeout ${TIMEOUT} ${nmroot}-piom/bin/nm_piom_pingpong -S 1 -E 1 -T ${MAXTHREADS} -N 1000 | timeout ${TIMEOUT} tee ${HERE}/${q}-piom.dat

done


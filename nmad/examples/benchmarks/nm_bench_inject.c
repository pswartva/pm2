/*
 * NewMadeleine
 * Copyright (C) 2016-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include "nm_bench_generic.h"
#include <nm_sendrecv_interface.h>
#include <nm_core_interface.h>
#include <nm_rpc_interface.h>

static const nm_tag_t data_tag = 0x02;

static const nm_tag_t rpc_tag = 0x042;

struct inject_header_s
{
  nm_len_t len;
  nm_tag_t tag;
  nm_seq_t seq;
  nm_session_hash_t session;
};

struct inject_pkt_s
{
  struct inject_header_s header;
  nm_rpc_token_t p_rpc_token;
  struct nm_req_s*p_req;
};


static struct
{
  nm_rpc_service_t p_service;
} inject = { . p_service = NULL };

static void inject_notify_req(struct nm_req_s*p_req, const struct nm_data_s*p_data0,
                              nm_len_t chunk_offset, nm_len_t chunk_len, void*p_ref)
{
  struct inject_pkt_s*p_pkt = p_ref;
  p_pkt->p_req = p_req;
  assert(chunk_offset == 0);
  assert(chunk_len == nm_data_size(p_data0));
  assert(chunk_len == p_pkt->header.len);
  nm_rpc_irecv_body_data(p_pkt->p_rpc_token, (struct nm_data_s*)p_data0);
  nm_rpc_token_complete(p_pkt->p_rpc_token);
}

static void inject_rpc_handler(nm_rpc_token_t p_token)
{
  struct nm_core*p_core = nm_rpc_service_get_ref(nm_rpc_get_service(p_token));
  struct inject_pkt_s*p_pkt = malloc(sizeof(struct inject_pkt_s));
  nm_rpc_token_set_ref(p_token, p_pkt);
  p_pkt->header.len = NM_LEN_UNDEFINED;
  p_pkt->p_rpc_token = p_token;
  p_pkt->p_req = NULL;
  nm_rpc_recv_header(p_token, &p_pkt->header, sizeof(p_pkt->header));
  nm_rpc_token_delay(p_token);
  nm_core_tag_t core_tag = nm_core_tag_build(p_pkt->header.session, p_pkt->header.tag);
  nm_core_inject_chunk(p_core, nm_bench_common.p_gate, core_tag, p_pkt->header.seq,
                       0 /* chunk_offset */, p_pkt->header.len, 1 /* last chunk */,
                       &inject_notify_req, p_pkt);
}

static void inject_rpc_finalizer(nm_rpc_token_t p_token)
{
  struct nm_core*p_core = nm_rpc_service_get_ref(nm_rpc_get_service(p_token));
  struct inject_pkt_s*p_pkt = nm_rpc_token_get_ref(p_token);
  nm_core_inject_complete_finalize(p_core, p_pkt->p_req, 0, p_pkt->header.len);
  free(p_pkt);
}


/* ********************************************************* */

static void inject_send(void*buf, nm_len_t len)
{
  nm_core_tag_t core_tag = nm_core_tag_build(nm_bench_common.p_session->hash_code, data_tag);
  const nm_seq_t seq = nm_core_send_seq_get(nm_core_get_singleton(), nm_bench_common.p_gate, core_tag);
  struct inject_header_s header =
    {
     .len     = len,
     .tag     = data_tag,
     .seq     = seq,
     .session = nm_bench_common.p_session->hash_code
    };
  nm_rpc_req_t p_req = nm_rpc_req_init(inject.p_service, nm_bench_common.p_gate, rpc_tag);
  nm_rpc_req_pack_header(p_req, &header, sizeof(header));
  nm_rpc_req_pack_body(p_req, buf, len);
  nm_rpc_req_isend(p_req);
  nm_rpc_req_wait(p_req);
}

static void inject_recv(void*buf, nm_len_t len)
{
  nm_sr_recv(nm_bench_common.p_session, nm_bench_common.p_gate, data_tag, buf, len);
}

/* ********************************************************* */

static void inject_init(void*buf, nm_len_t len)
{
  if(inject.p_service == NULL)
    {
      inject.p_service = nm_rpc_register(nm_bench_common.p_session, rpc_tag, NM_TAG_MASK_FULL,
                                         &inject_rpc_handler, &inject_rpc_finalizer, nm_core_get_singleton());
    }
}

static void inject_server(void*buf, nm_len_t len)
{
  inject_recv(buf, len);
  inject_send(buf, len);
}

static void inject_client(void*buf, nm_len_t len)
{
  inject_send(buf, len);
  inject_recv(buf, len);
}

const struct nm_bench_s nm_bench =
  {
    .name = "rpc + injection",
    .init   = &inject_init,
    .server = &inject_server,
    .client = &inject_client
  };

/*
 * Pioman: a Generic I/O Manager
 * Copyright (C) 2008-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include "piom_private.h"

#include <Padico/Module.h>

PADICO_MODULE_HOOK(pioman);

#ifdef PIOMAN_SIMGRID

static struct
{
  sg_actor_t polling_actor;               /**< the actor use for timer-based polling */
  struct piom_ltask_lfqueue_s lwps_queue; /**< ltasks queue to feed LWPs */
} piom_simgrid =
  {
    .polling_actor = NULL
  };


static void piom_simgrid_polling_worker(int argc, char**argv)
{
  const double timeslice_usec = piom_parameters.timer_period;
  piom_ltask_queue_t*global_queue = piom_topo_get_queue(piom_topo_current_obj());
  while(global_queue->state != PIOM_LTASK_QUEUE_STATE_STOPPED)
    {
      sg_actor_sleep_for(0.000001 * timeslice_usec);
      piom_trace_local_event(PIOM_TRACE_EVENT_TIMER_POLL, NULL);
      piom_ltask_schedule(PIOM_POLL_POINT_TIMER);
    }
}

static void piom_simgrid_lwp_worker(int argc, char**argv)
{
  /* feed LWP workers through a queue to avoid passing pointers through argc/argv */
  struct piom_ltask*task = piom_ltask_lfqueue_dequeue(&piom_simgrid.lwps_queue);
  assert(task != NULL);
  piom_ltask_blocking_invoke(task);
}

int piom_ltask_submit_in_lwp(struct piom_ltask*task)
{
  assert(task->blocking_func);
  piom_ltask_lfqueue_enqueue(&piom_simgrid.lwps_queue, task);
  piom_simgrid.polling_actor = sg_actor_create("piom_lwp", sg_host_self(),
                                               &piom_simgrid_lwp_worker, 0, NULL);
  return 0;
}

void piom_ltask_simgrid_init(void)
{
  piom_ltask_lfqueue_init(&piom_simgrid.lwps_queue);
  piom_simgrid.polling_actor = sg_actor_create("piom_timer", sg_host_self(),
                                               &piom_simgrid_polling_worker, 0, NULL);
}

void piom_ltask_simgrid_exit(void)
{
  sg_actor_join(piom_simgrid.polling_actor, -1);
}

struct piom_ltask_threadstate_s*piom_ltask_simgrid_getthreadstate(void)
{
  struct piom_ltask_threadstate_s*p_threadstate = sg_actor_get_data(sg_actor_self());
   if(p_threadstate == NULL)
    {
      p_threadstate = padico_malloc(sizeof(struct piom_ltask_threadstate_s));
      sg_actor_set_data(sg_actor_self(), p_threadstate);
      p_threadstate->scheduling_queue = NULL;
    }
  return p_threadstate;
}


#endif /* PIOMAN_SIMGRID */

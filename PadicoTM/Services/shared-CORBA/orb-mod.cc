/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief PadicoTM-enabled CORBA implementation
 * @ingroup CORBA
 */

/** @defgroup CORBA Service: CORBA -- PadicoTM-enabled shared CORBA implementation
 * @author Alexandre Denis
 * @note recognized attributes:
 *   NameService
 *   ORBgiopMaxMsgSize.
 */

/** @example Tutorial/03-HelloCORBA/Hello-server.cc
 * @example Tutorial/03-HelloCORBA/Hello-client.cc
 */

#include <Padico/Puk.h>
#include <Padico/PadicoTM.h>
#include <Padico/Module.h>
#include <Padico/PM2.h>

#include "./CORBA.h"
/* This really is "CORBA.h" from the *current* directory,
 * not from any include/ directory
 */

#ifndef MYCORBA_SHAREABLE
#error "shared-CORBA: trying to build shared-CORBA with a non-shareable ORB (MYCORBA_SHAREABLE not set)."
#endif

#ifndef MYCORBA_PADICOTM
#error "shared-CORBA: trying to build shared-CORBA with a non PadicoTM-enabled ORB."
#endif

#ifdef MYCORBA_OMNIORB_4_0_PADICO
#include <omniORB4/internal/orbParameters.h>
#endif

static int padico_corbamod_init(void);
static void padico_corbamod_finalize(void);

PADICO_MODULE_DECLARE(shared_CORBA, padico_corbamod_init, NULL, padico_corbamod_finalize);

CORBA::ORB_ptr Padico::the_orb = NULL;


static void* padico_corba_background_fun(void*)
{
  padico_trace("CORBA: padico_corba_background_fun\n");

  padico_tm_thread_givename("orb->run()");

  try {
    Padico::the_orb->run();
  }
  catch(...)
    {
      padico_fatal("orb->run() exception\n");
    }
  return NULL;
}

static int padico_corbamod_init(void)
{
  int argc;
  char**argv;
  padico_rttask_t rttask;
  const char*name_service = padico_getattr("NameService");
  char*msg_size = (char*) padico_getattr("ORBgiopMaxMsgSize");
  char*endpoint = (char*) padico_getattr("ORBendPoint");
  padico_string_t name_ref = padico_string_new();
  int i;

  argc=1;
  padico_out(30, "init 1 (argc=%d)\n", argc);
  if(name_service) argc+=2;
  if(msg_size) argc+=2;
  if(endpoint) argc+=2;
  argv = (char**)padico_malloc((argc+1)*sizeof(char*));

  argv[0] = (char*) padico_module_self_name();
  i=1;
  if(name_service)
    {
      padico_trace("ORB: NameService=**%s**\n", name_service);
      padico_string_printf(name_ref, "NameService=%s", name_service);
      argv[i] = "-ORBInitRef";
      argv[i+1] = padico_string_get(name_ref);
      i += 2;
    }
  if(msg_size)
    {
      argv[i] = "-ORBgiopMaxMsgSize";
      argv[i+1] = msg_size;
      i += 2;
    }
      padico_trace("ORB: env end point=**%s**\n", getenv("ORBendPoint"));
  if(endpoint)
    {
      padico_trace("ORB: end point=**%s**\n", endpoint);
      argv[i] = "-ORBendPoint";
      argv[i+1] = endpoint;
      i += 2;
    }
  argv[i] = NULL;

  padico_out(30, "init 2 (argc=%d)\n", argc);

  try
    {
      padico_print("myCORBA = %s\n", MYCORBA_STRING);
#ifdef MYCORBA_OMNIORB_4_0_PADICO
      omniORB::traceInvocations = 0;
      omniORB::traceLevel = 0;
      padico_print("tuning parameters for %s\n", MYCORBA_STRING);
      omni::orbParameters::scanGranularity  = 0;  // in seconds
      omni::orbParameters::outConScanPeriod = 10;  // in seconds
      omni::orbParameters::inConScanPeriod  = 10;  // in seconds
      omni::orbParameters::giopMaxMsgSize = 64*1024*1024; /* 64 MB is a better default value */
      omni::orbParameters::maxServerThreadPerConnection = 1;
      omni::orbParameters::supportCurrent = 0;
      omni::orbParameters::strictIIOP = 0;
      // just for tests...
      /*
        omni::orbParameters::maxGIOPVersion.major = 1;
        omni::orbParameters::maxGIOPVersion.minor = 0;
      */
#endif
      CORBA::ORB_ptr orb = CORBA::ORB_init(argc, argv, MYCORBA_ORBID);
      padico_out(20, "ORB_init() ok.\n");
      Padico::the_orb = orb;
    }
  catch(...)
    {
      padico_warning("ORB_init exception\n");
      return -1;
    }


  rttask = padico_rttask_new();
  rttask->background_fun = padico_corba_background_fun;
  rttask->kind = PADICO_RTTASK_BGTHREAD;
  padico_tm_rttask_submit(rttask);
  return 0;
}

void padico_corbamod_finalize(void)
{
  padico_trace("CORBA: orb shuting down...\n");
  Padico::the_orb->shutdown(0);
  padico_trace("CORBA: orb shutdown ok\n");
  padico_trace("CORBA: orb destroying...\n");
  Padico::the_orb->destroy();
  padico_trace("CORBA: orb destroyed\n");
}

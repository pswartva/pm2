#!/bin/bash

masque2cidr ()
{
    ipbin=""
    for n in $(echo $1 | tr "." " ")
    do
	q=1
	b=""
	if [ "x0" = "x$n" ]
	then
	    b="0"
	fi
	while [ $n -ne 0 ]
	do
	    b="$(($n%2))$b"
	    n=$(($n/2))
	done 
	ipbin="$ipbin$(printf "%8s" $b | tr " " "0")"
    done
    echo $ipbin | cut -d "0" -f 1  | tr -d [:space:] | wc -c
}

networks=""
hwaddrs=""
bindings=""
hostnames=""
echo "<Topology:topology xmlns:Topology=\"http://runtime.bordeaux.inria.fr/PadicoTM/Topology\">"

interfaces=$(/sbin/ifconfig | cut -d " " -f 1 | sed '/^$/d')
for if in ${interfaces}
do
    if [ "x$if" != "xlo" ]
    then
	    
	ifinfo=$(/sbin/ifconfig $if | grep -E "inet|Link" | awk '{ if ( $2 == "Link" ) {print  $1"\n"$5} else { if( $1 == "inet" )  { print $2"\n"$4}}}')
	hwaddr=$(echo ${ifinfo} | cut -d " " -f 2 | tr -d ":" | tr '[:lower:]' '[:upper:]')
	addr=$(echo ${ifinfo} | cut -d " " -f 3 | cut -d ":" -f 2)
	masque=$(echo ${ifinfo} | cut -d " " -f 4 | cut -d ":" -f 2)
	hexaddr=$(printf '%2x%2x%2x%2x'  $(echo ${addr} | tr "." " ") | tr " " "0" | tr '[:lower:]' '[:upper:]')
	n1=$(($(echo ${addr} | cut -d "." -f 1) & $(echo ${masque} | cut -d "." -f 1)))
	n2=$(($(echo ${addr} | cut -d "." -f 2) & $(echo ${masque} | cut -d "." -f 2)))
	n3=$(($(echo ${addr} | cut -d "." -f 3) & $(echo ${masque} | cut -d "." -f 3)))
	n4=$(($(echo ${addr} | cut -d "." -f 4) & $(echo ${masque} | cut -d "." -f 4)))
	network=$(printf '%d.%d.%d.%d' ${n1} ${n2} ${n3} ${n4})
	cidr=$(masque2cidr ${masque})
	hostname=$(host ${addr} | grep "domain name pointer" | cut -d " " -f 5 | sed 's/.$//') 
	networkname=""
	if [ "$(echo ${network} | cut -d "." -f 1,2)" = "192.168" ]  ||
	    [ "$(echo ${network} | cut -d "." -f 1)" = "10" ] || 
	    [ "$(echo ${network} | cut -d "." -f 1)" = "172" -a $(($(echo ${network} | cut -d "." -f 2) & 16)) -eq 16 ]
	then
	    networkname="-[${network}/${cidr}]"
	fi

	hwaddrs="${hwaddrs}
    <Topology:hwaddr name=\"${if}\" index=\"0\">${hwaddr}</Topology:hwaddr>"
	networks="${networks}
  <Topology:network id=\"inet${networkname}\" addr_size=\"4\" scope=\"host\" family=\"2\">
  </Topology:network>"
	bindings="${bindings}
    <Topology:binding network=\"inet${networkname}\">
      <Topology:property kind=\"addr\" family=\"2\">${hexaddr}</Topology:property>
      <Topology:property kind=\"hwaddr\" ifname=\"${if}\"/>"
	if [ "x${hostname}" != "x" ]
	then
	    bindings="${bindings}
      <Topology:property kind=\"hostname\" name=\"${hostname}\"/>"
	    hostnames="${hostnames}
    <Topology:hostname>${hostname}</Topology:hostname>"
	fi
	bindings="${bindings}
    </Topology:binding>"
    fi
done
echo "${networks}" | sed '/^$/d'
echo "  <Topology:host fqdn=\"$(hostname -f)\">"
hostnames="${hostnames}
    <Topology:hostname>$(hostname -f)</Topology:hostname>"
for n in $(hostname -a)
do
    hostnames="${hostnames}
    <Topology:hostname>${n}</Topology:hostname>"
done
echo "${hostnames}" | uniq | sed '/^$/d'
echo "${hwaddrs}" | sed '/^$/d'
echo "${bindings}" | sed '/^$/d'
echo "    <Topology:property kind=\"misc\" label=\"login\" value=\"$(logname)\"/>"
if [ $(ps -e | grep sshd | wc -l) -ge 1 ] && [ $(netstat -N --listen --tcp | grep ssh | wc -l) -ge 1 ]
then
    echo "    <Topology:property kind=\"service\" port=\"22\" name=\"ssh\"/>"
fi
echo "  </Topology:host>"
echo "</Topology:topology>"
/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Memory monitor: dynamically check for process memory usage
 * @ingroup MemMonitor
 */

/** @defgroup MemMonitor
 * @author Alexandre Denis
 */

#include <sys/time.h>
#include <sys/resource.h>

#include <Padico/Puk.h>
#include <Padico/PadicoTM.h>
#include <Padico/Module.h>

#define PADICO_MEM_MONITOR_PERIOD 3 /* in seconds */
#define PADICO_MEM_MONITOR_GETRUSAGE 1

static int padico_mem_monitor_init(void);
static void padico_mem_monitor_finalize(void);

PADICO_MODULE_DECLARE(MemMonitor, padico_mem_monitor_init, NULL, padico_mem_monitor_finalize);

static struct
{
  int period;
  padico_tm_bgthread_pool_t pool;
  volatile int interrupt;
} padico_mem_monitor =
  {
    .period = PADICO_MEM_MONITOR_PERIOD,
    .interrupt = 0
  };

static void*padico_mem_monitor_worker(void*_arg)
{
  while(!padico_mem_monitor.interrupt)
    {
      struct rusage usage;
      getrusage(RUSAGE_SELF, &usage);
      fprintf(stderr, "# %s: MemMonitor: maxrssKB = %ld\n", padico_hostname(), usage.ru_maxrss);
#ifdef PADICO_MEM_MONITOR_GETRUSAGE
      fprintf(stderr, "# %s: MemMonitor: getrusage: utime %g; stime %g\n",
              padico_hostname(),
              (double)(1000000.0 * usage.ru_utime.tv_sec + 1.0 * usage.ru_utime.tv_usec),
              (double)(1000000.0 * usage.ru_stime.tv_sec + 1.0 * usage.ru_stime.tv_usec));
#endif /* PADICO_MEM_MONITOR_GETRUSAGE */
#ifdef PUK_ENABLE_PROFILE
      const ssize_t mem_total  = puk_profile_mem_get_total();
      const ssize_t num_malloc = puk_profile_mem_get_malloc();
      const ssize_t num_free   = puk_profile_mem_get_free();
      fprintf(stderr, "# %s: MemMonitor: puk_profile_total_bytes = %zd\n", padico_hostname(), mem_total);
      fprintf(stderr, "# %s: MemMonitor: puk_profile_malloc = %zd\n", padico_hostname(), num_malloc);
      fprintf(stderr, "# %s: MemMonitor: puk_profile_free = %zd\n", padico_hostname(), num_free);
      {
        padico_string_t s = padico_string_new();
        padico_string_catf(s, "# %s: MemMonitor: puk_mods ", padico_hostname());
        padico_modID_vect_t modIDs = puk_mod_getmodIDs();
        padico_modID_vect_itor_t i;
        puk_vect_foreach(i, padico_modID, modIDs)
          {
            puk_mod_t m = puk_mod_getbyname(*i);
            struct puk_profile_mem_s*profile = puk_profile_mem_get_mod(m);
            if(profile)
              padico_string_catf(s, "%s = %zd; ", *i, profile->total_mem);
          }
        padico_string_catf(s, "\n");
        fprintf(stderr, padico_string_get(s));
        padico_string_delete(s);
      }
#endif /* PUK_ENABLE_PROFILE */
      puk_sleep(padico_mem_monitor.period);
    }
  return NULL;
}

static int padico_mem_monitor_init(void)
{
  const char*s_period = getenv("PADICO_MEM_MONITOR_PERIOD");
  if(s_period != NULL)
    {
      padico_mem_monitor.period = atoi(s_period);
      if(padico_mem_monitor.period <= 0)
        {
          padico_fatal("invalid period = %d\n", padico_mem_monitor.period);
        }
    }
  padico_out(puk_verbose_notice, "loading background memory monitor; period = %d seconds.\n",
             padico_mem_monitor.period);
  padico_mem_monitor.pool = padico_tm_bgthread_pool_create("MemMonitor");
  padico_tm_bgthread_start(padico_mem_monitor.pool, &padico_mem_monitor_worker, NULL, "MemMonitor");
  return 0;
}

static void padico_mem_monitor_finalize(void)
{
  padico_mem_monitor.interrupt = 1;
  padico_tm_bgthread_pool_wait(padico_mem_monitor.pool);
}

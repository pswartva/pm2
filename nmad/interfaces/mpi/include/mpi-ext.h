/*
 * NewMadeleine
 * Copyright (C) 2006-2023 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


#ifndef MPI_EXT_H
#define MPI_EXT_H

#define MPIX_CUDA_AWARE_SUPPORT 1

/**< returns 1 if there is CUDA aware support, 0 otherwise. */
int MPIX_Query_cuda_support(void);

#endif /* MPI_EXT_H */

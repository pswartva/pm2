#include <stdio.h>
#include <netdb.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>

static char h[128];

int main(int argc, char**argv)
{
  int rc;
  int err;
  printf("ABI-Test: Hello world!\n");

  printf("ABI-Test: hostname...\n");
  gethostname(h, 128);
  printf("ABI-Test: hostname: %s\n", h);

  printf("ABI-Test: fcntl...\n");
  rc = fcntl(2, F_GETFD, 0x01);
  printf("ABI-Test: fcntl()- rc = %d\n", rc);

  printf("ABI-Test: gethostbyname...\n");
  struct hostent*e = gethostbyname(h);
  printf("ABI-Test: gethostbyname: %s\n", e->h_name);

  printf("ABI-Test: strtod [ok]...\n");
  static char test_int[] = "125456";
  errno = 0;
  rc = strtod(test_int, NULL);
  err = errno;
  printf("ABI-Test: strtod: %d [%d (%s)]\n", rc, err, strerror(err));

  printf("ABI-Test: strtod  [error]...\n");
  static char test_int2[] = "ZZZWWX";
  errno = 0;
  rc = strtod(test_int2, NULL);
  err = errno;
  printf("ABI-Test: strtod: %d [%d (%s)]\n", rc, err, strerror(err));

  printf("ABI-Test: open  [error]...\n");
  static char test_open[] = "/does_not_exist";
  rc = open(test_open, O_RDONLY);
  err = errno;
  printf("ABI-Test: open: %d [%d (%s)]\n", rc, err, strerror(err));
  
  return 0;
}


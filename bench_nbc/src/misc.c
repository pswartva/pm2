/*
 * Bench NBC
 * Copyright (C) 2019-2020 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <time.h>

#include "misc.h"
#include "bench_constants.h"

#ifdef GLOBAL_BUFFER
/* definition in case of global buffers */
void*data = NULL, *data2 = NULL;
double*a = NULL, *b = NULL, *res = NULL;
#endif /* GLOBAL_BUFFER */


int get_nb_rep(double data_size, double matrix_size,int base){
  /*int n = base;

    if(data_size >= 8000000){
    n/=5;
    }
    else if(data_size >= 400000){
    n/=2;
    }

    if(matrix_size >= 225){
    n/=5;
    }
    else if(matrix_size >= 125){
    n/=2;
    }*/
  double n = 0, n1=0,n2=0;
  if(data_size > 0)
    n1 = ((double)base/DATA_THRESHOLD)*(DATA_THRESHOLD - data_size);
  if(matrix_size > 0)
    n2 = ((double)base/COMPUTE_THRESHOLD)*(COMPUTE_THRESHOLD - matrix_size);
  if(n1 < 0)
    n1=0;
  if(n2 < 0)
    n2=0;
  n = n1 + n2;
  if(n < base/20)
    n=base/20;
  if(n > base)
    n=base;
  int nb =(int)round(n);
  //fprintf(stderr,"NB_REP:%d\n",(int)round(n));
  return nb;
}

bench_time_t*estimate_comp_time(int size)
{
#ifndef GLOBAL_BUFFER
  double *a,*b,*res;
  a=malloc(size*size*sizeof(double));
  b=malloc(size*size*sizeof(double));
  res=calloc(size*size,sizeof(double));
#endif
  int nb_rep = get_nb_rep(0,size,NB_REP);
  double* purecalc_time = malloc(sizeof(double)*nb_rep);
  struct timespec t,t1;
  int i;
  for(i=0; i < nb_rep; i++){

    clock_gettime(CLOCK_MONOTONIC,&t);
#ifdef GLOBAL_BUFFER
    matrix_compute(size);
#else
    matrix_compute(a,b,res,size);
#endif
    clock_gettime(CLOCK_MONOTONIC,&t1);
    purecalc_time[i] = ((double)t1.tv_sec*1000000.0 + (double) t1.tv_nsec /1000.0) - ((double)t.tv_sec*1000000.0 + (double) t.tv_nsec /1000.0);
  }
  bench_time_t*times = malloc(sizeof(bench_time_t));//,b,c;
  times->median = malloc(sizeof(double));
  times->var = malloc(sizeof(double));
  times->max = malloc(sizeof(double));
  times->min = malloc(sizeof(double));
  times->median[0] = median(purecalc_time,nb_rep);
  times->var[0] = variance(purecalc_time,nb_rep);
  times->max[0] = max(purecalc_time,nb_rep);
  times->min[0] = min(purecalc_time,nb_rep);
#ifndef GLOBAL_BUFFER
  free(a);
  free(b);
  free(res);
#endif
  free(purecalc_time);
  return times;
}
uint64_t get_time()
{
  uint64_t tsc;
  __asm__ __volatile__("rdtscp; "         // serializing read of tsc
      "shl $32,%%rdx; "// shift higher 32 bits stored in rdx up
      "or %%rdx,%%rax"// and or onto rax
      : "=a"(tsc)// output to tsc variable
      :
      : "%rcx", "%rdx");// rcx and rdx are clobbered
  return tsc;

}

void get_time_r(uint64_t *tsc)
{
  *tsc = 0;
  __asm__ __volatile__("rdtscp; "         // serializing read of tsc
      "shl $32,%%rdx; "// shift higher 32 bits stored in rdx up
      "or %%rdx,%%rax"// and or onto rax
      : "=a"(*tsc)// output to tsc variable
      :
      : "%rcx", "%rdx");// rcx and rdx are clobbered
  //fprintf(stderr,"tsc:%llu\n",tsc);

}

double get_proc_freq()
{
  uint64_t s1, s2;
  struct timespec ts = { 0, 10000000 /* 10 ms */ }, t1,t2;
  clock_gettime(CLOCK_MONOTONIC,&t1);
  s1 = get_time();
  nanosleep(&ts, NULL);
  s2 = get_time();
  clock_gettime(CLOCK_MONOTONIC,&t2);
  double t = (t2.tv_sec * 1e6 + t2.tv_nsec / 1e3)-(t1.tv_sec * 1e6 + t1.tv_nsec / 1e3);
  //fprintf(stderr,"slept:%f\n",t);
  //return (ts.tv_sec * 1e6 + ts.tv_nsec / 1e3) / (double)(s2 - s1);
  return t / (double)(s2 - s1);
}

int compare_doubles(const void* av,const void* bv)
{
  const double *a = av;
  const double *b = bv;
  if(*a < *b)
    return -1;
  if(*a == *b)
    return 0;
  return 1;
}

double mean(double * data, int size)
{
  //calcul de la moyenne
  int i;
  double moy; //moyenne
  moy=0;
  for (i=0 ; i< size ; i++) moy += data[i];
  moy /= size;
  return moy;
}

double median(double* data, int size)
{
  qsort(data,size,sizeof(double),compare_doubles);
  return data[size/2];
}

double variance(double* data, int size)
{
int i;
double var=0,moy=0;
  for (i=0 ; i< size ; i++) var += data[i]*data[i];
  var /= size;
  moy = mean(data,size);
  var -= moy*moy;

  return var;
}

double max(double* tab,int nb)
{
  int i;
  double s = 0;
  for(i = 0; i < nb; i++){
    if(tab[i] > s)
      s = tab[i];
  }
  return s;
}

double min(double* tab,int nb)
{
  int i;
  double s = DBL_MAX;
  for(i = 0; i < nb; i++){
    if(tab[i] < s)
      s = tab[i];
  }
  return s;
}

double sum(double* tab, int nb)
{
  int i;
  double s = 0;

  for(i = 0; i < nb; i++){
    s += tab[i];
  }
  return s;
}

double racine3(double x){

  return pow(x,1.0/3.0);

}

/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Public API for Virtual I/O.
 * @ingroup VIO
 */

/** @defgroup VIOAPI API: VIO
 * @ingroup VIO
 * @note Behaves exactly like stdio functions
 */

/** @example Tests/VIO/vio-test.c
 */

#ifndef PADICO_VIO_H
#define PADICO_VIO_H

#include <Padico/Puk.h>
#include <Padico/PadicoTM.h>

#include <unistd.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <sys/un.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <netdb.h>
#include <fcntl.h>
#include <errno.h>

#ifdef HAVE_POLL_H
#include <poll.h>
#endif

PUK_MOD_AUTO_DEP(VIO, PADICO_LAST_MODULE_NAME);
#undef PADICO_LAST_MODULE_NAME
#define PADICO_LAST_MODULE_NAME VIO

#ifdef __cplusplus
extern "C" {
#endif

  /** @addtogroup VIOAPI
   * @{
   */
  /** @hideinitializer */
#define padico_vio_errno padico_vio_get_errno()
  /** @internal */
  extern int padico_vio_get_errno(void);
  /** @internal */
  extern int*padico_vio_errno_location(void);

  /* *** generic VIO front-ends ****************************** */
  ssize_t padico_vio_read(int fd, void *buf, size_t count);
  ssize_t padico_vio_write(int fd, const void *buf, size_t count);
  ssize_t padico_vio_readv(int fildes, const struct iovec *iov, int iovcnt);
  ssize_t padico_vio_writev(int fildes, const struct iovec *iov, int iovcnt);
  int     padico_vio_close(int fd);
  int     padico_vio_select(int nfds, fd_set*readfds, fd_set*writefds, fd_set*errorfds,
                            struct timeval *timeout);
  int     padico_vio_fcntl(int fd, int cmd, void* arg);
#ifdef HAVE_POLL_H
  int     padico_vio_poll(struct pollfd fds[], unsigned int nfds, int timeout);
#endif

  /* *** pipe-specific *************************************** */
  int     padico_vio_pipe(int fildes[2]);

  /* *** file-specific front-ends **************************** */
  int     padico_vio_open(const char*path, int oflag, ...);
  int     padico_vio_creat(const char*path, mode_t mode);
  off_t   padico_vio_lseek(int fd, off_t offset, int whence);
  int     padico_vio_ftruncate(int fd, off_t length);
#ifdef HAVE___FXSTAT
  int     padico_vio___fxstat(int version, int fd, struct stat*buf);
#ifdef __USE_LARGEFILE64
  int     padico_vio___fxstat64(int version, int fd, struct stat64*buf);
#endif
#else
  int     padico_vio_fstat(int fd, struct stat *buf);
#endif
  void*   padico_vio_mmap(void *addr, size_t len, int prot, int flags, int fd, off_t off);
  int     padico_vio_fsync(int fd);

#ifdef __USE_LARGEFILE64
  int     padico_vio_open64(const char*path, int oflag, ...);
  int     padico_vio_creat64(const char*path, mode_t mode);
  off64_t padico_vio_lseek64(int fd, off64_t offset, int whence);
  int     padico_vio_ftruncate64(int fd, off64_t length);
  int     padico_vio_fstat64(int fd, struct stat64 *buf);
  void*   padico_vio_mmap64(void *addr, size_t len, int prot, int flags, int fd, off64_t off);
#endif

  /* *** socket-specific front-ends ************************** */
  ssize_t padico_vio_send(int s, const void*msg, size_t len, int flags);
  ssize_t padico_vio_recv(int s, void*msg, size_t len, int flags);
  ssize_t padico_vio_sendto(int s, const void*buf, size_t len, int flags,
                            const struct sockaddr *to, int  tolen);
  ssize_t padico_vio_recvfrom(int s, void *buf, size_t len, int flags,
                              struct sockaddr *from, socklen_t*fromlen);
  ssize_t padico_vio_recvfrom_madio(int s, void *buf, size_t len, int flags,
                                    struct sockaddr *from, int *fromlen);
  ssize_t padico_vio_sendmsg(int s, const struct msghdr *msg, int flags);
  ssize_t padico_vio_recvmsg(int s, struct msghdr *msg, int flags);

  /* *** socket connection management ************************ */
  int     padico_vio_socket(int domain, int type, int protocol);
  int     padico_vio_bind(int s, const struct sockaddr*name, socklen_t namelen);
  int     padico_vio_listen(int s, int n);
  int     padico_vio_accept(int s, struct sockaddr*raddr, socklen_t*namelen);
  int     padico_vio_connect(int s, const struct sockaddr*name, socklen_t namelen);
  int     padico_vio_shutdown(int s, int how);

  /* *** socket infos & options ****************************** */
  int     padico_vio_setsockopt(int s, int level, int optname, const void*optval, socklen_t optlen);
  int     padico_vio_getsockopt(int s, int level, int optname, void *optval, socklen_t *optlen);
  int     padico_vio_getsockname(int s, struct sockaddr*name, socklen_t*namelen);
  int     padico_vio_getpeername(int s, struct sockaddr*name, socklen_t*namelen);

  /* *** Linux 'inotify' API ********************************* */
#ifdef HAVE_INOTIFY_H
  int     padico_vio_inotify_init(void);
  int     padico_vio_inotify_add_watch(int fd, const char*pathname, uint32_t mask);
  int     padico_vio_inotify_rm_watch(int fd, uint32_t wd);
#endif

  /* *** partial support ************************************* */
  int     padico_vio_ulimit(int cmd, ...); /** @warning only cmd=4 (GDESLIM) is supported */
  int     padico_vio_ioctl(int fd, int request, unsigned long arg);

  /* *** PadicoTM-VIO specific extensions ******************** */
  int     padico_vio_interrupt(int fd);
  padico_rc_t padico_vio_dump_vfd(int vfd);
  puk_component_t padico_vio_component(int fd);

  /** @} */

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* PADICO_VIO_H */

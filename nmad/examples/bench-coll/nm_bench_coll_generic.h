/*
 * NewMadeleine
 * Copyright (C) 2019 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


#include <nm_public.h>
#include <nm_coll_interface.h>
#include <nm_sync_clocks_interface.h>

#ifndef NM_BENCH_COLL_GENERIC_H
#define NM_BENCH_COLL_GENERIC_H

struct nm_bench_coll_common_s
{
  nm_comm_t p_comm;
  nm_sync_clocks_t p_clocks;
};

struct nm_bench_coll_s
{
  const char*label;
  const char*name;
  void (*init)(struct nm_bench_coll_common_s*p_common, void*buf, nm_len_t len);
  void (*run)(struct nm_bench_coll_common_s*p_common, void*buf, nm_len_t len);
  void (*finalize)(struct nm_bench_coll_common_s*p_common);
};


#endif /* NM_BENCH_COLL_GENERIC_H */


/*
 * PM2: Parallel Multithreaded Machine
 * Copyright (C) 2001 "the PM2 team" (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <tbx.h>
#include <values.h>

static sem_t sem1, sem2;

static double time_min = DBL_MAX;
static double time_max = 0.0;
static double time_total = 0.0;

static void* f(void* arg)
{
  int n = (int) (intptr_t) arg;
  tbx_tick_t t1, t2;
  
  sem_post(&sem1);
  TBX_GET_TICK(t1);
  while (--n) 
    {
      sem_wait(&sem2);
      sem_post(&sem1);
    }
  TBX_GET_TICK(t2);

  const double delay = TBX_TIMING_DELAY(t1, t2) / (2 * (double) (intptr_t) arg);
  printf("cond time =  %fus\n", delay);
  if(delay > time_max)
    time_max = delay;
  if(delay < time_min)
    time_min = delay;
  time_total += delay;
  return NULL;
}

static void bench_cond(unsigned nb)
{
  pthread_t pid;
  void* status;
  register int n = nb;
  
  if (!nb)
    return;
  
  n++;
  
  sem_init(&sem1, 0, 0);
  sem_init(&sem2, 0, 0);
  pthread_create(&pid, NULL, f, (void*) (intptr_t) n);
  
  sem_wait(&sem1);
  while (--n) 
    {
      sem_post(&sem2);
      sem_wait(&sem1);
    }
  
  pthread_join(pid, &status);
  sem_destroy(&sem1);
  sem_destroy(&sem2);
}

int main(int argc, char *argv[])
{
  const int essais = 10000;
  int i = essais;
  unsigned nb = (argc == 2) ? atoi(argv[1]) : 1;
  
  while(i--)
    {
      bench_cond(nb);
    }
  
  fprintf(stderr, "  min = %g; max = %g; avg = %g\n", time_min, time_max, time_total / (double)essais);

  return 0;
}

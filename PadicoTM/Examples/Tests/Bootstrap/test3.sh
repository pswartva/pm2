#!/bin/bash

#in this test we use ssh for the bootstrap channel 
source helper.sh

SSH_MODE="SSH-Netcat"

ssh 2>&1 | grep "\-W" > /dev/null
if [ "x$?" = "x0" ]; then
    SSH_MODE="${SSH_MODE} SSH2"
fi


for m in ${SSH_MODE}; do 
    cat > controlssh.xml <<EOF
<?xml version="1.0"?>
<NS:rule-set 
   xmlns:NS="http://runtime.bordeaux.inria.fr/PadicoTM/NetSelector" 
   xmlns:puk="http://runtime.bordeaux.inria.fr/PadicoTM/Puk"
   >

  <puk:composite id="control-by-ssh">
    <puk:component id="0" name="SocketFactory-${m}">
      <puk:attr label="ssh_host">${PADICO_HOST1}</puk:attr>
      <puk:attr label="ssh_port">$(( ${PADICO_PORT} + 500 ))</puk:attr>
    </puk:component>
    <puk:component id="1" name="Control_SysIO">
      <puk:uses iface="SocketFactory" port="sf" provider-id="0"/>
    </puk:component>
    <puk:entry-point provider-id="1" iface="PadicoBootstrap" port="bootstrap"/>
    <puk:entry-point provider-id="1" iface="PadicoControl"   port="control"/>
    <puk:entry-point provider-id="1" iface="PadicoComponent" port="component"/>
  </puk:composite>

  <NS:rule>
    <puk:composite ref="control-by-ssh"/>
    <NS:target kind="multipartite">
      <NS:host name="${PADICO_HOST2}"/>
      <NS:host name="${PADICO_HOST1}"/>
    </NS:target>
  </NS:rule>
</NS:rule-set>

EOF

    echo "TEST: BOOTSTRAP ${m}"
    padico-launch --timeout 20 -l=.  -v -DPADICO_BOOTSTRAP_LISTEN=${PADICO_PORT} --host ${PADICO_HOST1} sleep 5 &> rdvnode &
    pid=$!
    padico-launch --timeout 20 -v -DPADICO_BOOTSTRAP_PORT=${PADICO_PORT} -DPADICO_BOOTSTRAP_HOST=${PADICO_HOST1} --host ${PADICO_HOST2} -DNS_BASIC_CONF=$(pwd)/controlssh.xml true &> node1
    rc1=$?
    wait ${pid}
    rc2=$?
    if [ "x${rc1}" != "x0" -o "x${rc2}" != "x0" ]; then
	failed "return code ${rc1} ${rc2}"
    else
	session_nodes=$(grep "new node" *.log | wc -l )
	if [ "x${session_nodes}" != "x2" ]; then
	    failed
	else
	    success
	fi
    fi
    clean
    rm controlssh.xml rdvnode node1 *.log
done    


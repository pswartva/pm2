/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <assert.h>

#include <nm_minidriver.h>
#include <nm_private.h>
#include <Padico/Module.h>

#include <simgrid/mailbox.h>
#include <simgrid/comm.h>
#include <simgrid/link.h>
#include <simgrid/actor.h>

/** cost of polling */
#define NM_SG_SMALL_TIME_POLL 0.00001

#define NM_SG_SMALL_MSGSIZE (64 * 1024) /* 64 kB */

#define NM_SG_SMALL_CANCEL ((void*)(0x01))

static void*nm_sg_small_instantiate(puk_instance_t instance, puk_context_t context);
static void nm_sg_small_destroy(void*status);

static const struct puk_component_driver_s nm_sg_small_component =
  {
    .instantiate = &nm_sg_small_instantiate,
    .destroy     = &nm_sg_small_destroy
  };

static void nm_sg_small_getprops(puk_context_t context, struct nm_minidriver_properties_s*props);
static void nm_sg_small_init(puk_context_t context, const void**drv_url, size_t*url_size);
static void nm_sg_small_close(puk_context_t context);
static void nm_sg_small_connect(void*_status, const void*remote_url, size_t url_size);
static void nm_sg_small_disconnect(void*_status);
static int  nm_sg_small_send_buf_get(void*_status, void**p_buffer, nm_len_t*p_len);
static int  nm_sg_small_send_buf_post(void*_status, nm_len_t len);
static int  nm_sg_small_send_poll(void*_status);
static int  nm_sg_small_send_wait(void*_status);
static int  nm_sg_small_recv_buf_poll(void*_status, void**p_buffer, nm_len_t*p_len);
static int  nm_sg_small_recv_buf_release(void*_status);
static int  nm_sg_small_recv_probe_any(puk_context_t p_context, void**_status);
static int  nm_sg_small_recv_wait_any(puk_context_t p_context, void**_status);
static int  nm_sg_small_recv_cancel_any(puk_context_t p_context);



static const struct nm_minidriver_iface_s nm_sg_small_minidriver =
  {
    .getprops         = &nm_sg_small_getprops,
    .init             = &nm_sg_small_init,
    .close            = &nm_sg_small_close,
    .connect          = &nm_sg_small_connect,
    .disconnect       = &nm_sg_small_disconnect,
    .send_iov_post    = NULL,
    .send_data_post   = NULL,
    .send_buf_get     = &nm_sg_small_send_buf_get,
    .send_buf_post    = &nm_sg_small_send_buf_post,
    .send_poll        = &nm_sg_small_send_poll,
    .send_wait        = &nm_sg_small_send_wait,
    .recv_iov_post    = NULL,
    .recv_data_post   = NULL,
    .recv_poll_one    = NULL,
    .recv_cancel      = NULL,
    .recv_buf_poll    = &nm_sg_small_recv_buf_poll,
    .recv_buf_release = &nm_sg_small_recv_buf_release,
    .recv_probe_any   = &nm_sg_small_recv_probe_any,
    .recv_wait_any    = &nm_sg_small_recv_wait_any ,
    .recv_cancel_any  = &nm_sg_small_recv_cancel_any
  };

/* ********************************************************* */

PADICO_MODULE_COMPONENT(Minidriver_simgrid_small,
  puk_component_declare("Minidriver_simgrid_small",
                        puk_component_provides("PadicoComponent", "component", &nm_sg_small_component),
                        puk_component_provides("NewMad_minidriver", "minidriver", &nm_sg_small_minidriver)));

/* ********************************************************* */

struct nm_sg_small_s;

PUK_HASHTABLE_TYPE(nm_sg_small_status, const char*, struct nm_sg_small_s*,
                   &puk_hash_string_default_hash, &puk_hash_string_default_eq, NULL);

struct nm_sg_small_pkt_s
{
  const char*p_remote_url; /**< url of the source node */
  void*p_buffer;           /**< content of packet */
};

/** 'simgrid' driver per-context data. */
struct nm_sg_small_context_s
{
  char*url;                 /**< server url */
  sg_mailbox_t mailbox;
  sg_comm_t comm;
  struct nm_sg_small_pkt_s*p_incoming_pkt;
  struct nm_sg_small_status_hashtable_s status_table; /**< statuses hashed by url */
};

/** 'simgrid' per-instance status. */
struct nm_sg_small_s
{
  struct nm_sg_small_context_s*p_simgrid_context;
  sg_mailbox_t dest_mailbox;
  char*p_remote_url;
  struct
  {
    sg_comm_t comm;
    void*p_buffer;
  } send;
  struct
  {
    void*p_buffer;
  } recv;
};

static void nm_sg_small_recv_reload(struct nm_sg_small_context_s*p_simgrid_context);

/* ********************************************************* */

static void*nm_sg_small_instantiate(puk_instance_t instance, puk_context_t context)
{
  struct nm_sg_small_s*p_status = padico_malloc(sizeof(struct nm_sg_small_s));
  struct nm_sg_small_context_s*p_simgrid_context = puk_context_get_status(context);
  p_status->p_simgrid_context = p_simgrid_context;
  p_status->send.p_buffer = NULL;
  p_status->recv.p_buffer = NULL;
  p_status->p_remote_url = NULL;
  p_status->dest_mailbox = NULL;
  return p_status;
}

static void nm_sg_small_destroy(void*_status)
{
  struct nm_sg_small_s*p_status = _status;
  nm_sg_small_status_hashtable_remove(&p_status->p_simgrid_context->status_table, p_status->p_remote_url);
  if(p_status->p_remote_url)
    padico_free(p_status->p_remote_url);
  padico_free(p_status);
}

/* ********************************************************* */

static void nm_sg_small_getprops(puk_context_t context, struct nm_minidriver_properties_s*props)
{
  props->profile.latency = 500; /* TODO- sg_link_get_latency() */
  props->profile.bandwidth = 8000; /* TODO- sg_link_get_bandiwdth() */
  props->capabilities.supports_buf_send = 1;
  props->capabilities.supports_buf_recv = 1;
  props->capabilities.supports_data = 0;
  props->capabilities.supports_iovec = 0;
  props->capabilities.supports_recv_any = 1;
  props->capabilities.supports_wait_any = 1;
  props->capabilities.prefers_wait_any = 1;
  props->capabilities.prefers_send_wait = 1;
  props->capabilities.max_msg_size = NM_SG_SMALL_MSGSIZE;
  props->nickname = "simgrid_small";
}

static void nm_sg_small_init(puk_context_t context, const void**drv_url, size_t*url_size)
{
  struct nm_sg_small_context_s*p_simgrid_context = padico_malloc(sizeof(struct nm_sg_small_context_s));
  sg_actor_t actor = sg_actor_self();
  const char*actor_name = sg_actor_get_name(actor);
  padico_string_t s = padico_string_new();
  padico_string_printf(s, "%s_%p", actor_name, context);
  p_simgrid_context->url = padico_strdup(padico_string_get(s));
  padico_string_printf(s, "to:%s", p_simgrid_context->url);
  p_simgrid_context->mailbox = sg_mailbox_by_name(padico_string_get(s));
  /* sg_mailbox_set_receiver(sg_mailbox_get_name(p_simgrid_context->mailbox)); */
  padico_string_delete(s);
  p_simgrid_context->comm = NULL;
  p_simgrid_context->p_incoming_pkt = NULL;
  nm_sg_small_status_hashtable_init(&p_simgrid_context->status_table);
  puk_context_set_status(context, p_simgrid_context);
  *drv_url = p_simgrid_context->url;
  *url_size = strlen(p_simgrid_context->url) + 1;

  nm_sg_small_recv_reload(p_simgrid_context);
}

static void nm_sg_small_close(puk_context_t context)
{
  struct nm_sg_small_context_s*p_simgrid_context = puk_context_get_status(context);
  puk_context_set_status(context, NULL);
  padico_free(p_simgrid_context->url);
  padico_free(p_simgrid_context);
}

static void nm_sg_small_lazy_connect(struct nm_sg_small_s*p_status)
{
  if(p_status->dest_mailbox == NULL)
    {
      padico_string_t s = padico_string_new();
      struct nm_sg_small_context_s*p_simgrid_context = p_status->p_simgrid_context;
      padico_string_printf(s, "to:%s", p_status->p_remote_url);
      p_status->dest_mailbox = sg_mailbox_by_name(padico_string_get(s));
      padico_string_delete(s);
      padico_out(puk_verbose_info, "local mailbox = %s (%p); dest mailbox = %s (%p)\n",
                 sg_mailbox_get_name(p_simgrid_context->mailbox), p_simgrid_context->mailbox,
                 sg_mailbox_get_name(p_status->dest_mailbox), p_status->dest_mailbox);
    }
}

static void nm_sg_small_connect(void*_status, const void*remote_url, size_t url_size)
{
  struct nm_sg_small_s*p_status = _status;
  struct nm_sg_small_context_s*p_simgrid_context = p_status->p_simgrid_context;
  p_status->p_remote_url = padico_strdup(remote_url);
  nm_sg_small_status_hashtable_insert(&p_simgrid_context->status_table, p_status->p_remote_url, p_status);
}

static void nm_sg_small_disconnect(void*_status)
{
  struct nm_sg_small_s*p_status = _status;
}

static int nm_sg_small_send_buf_get(void*_status, void**p_buffer, nm_len_t*p_len)
{
  struct nm_sg_small_s*p_status = _status;
  nm_sg_small_lazy_connect(p_status);
  assert(p_status->send.p_buffer == NULL);
  p_status->send.p_buffer = padico_malloc(NM_SG_SMALL_MSGSIZE);
  *p_buffer = p_status->send.p_buffer;
  *p_len = NM_SG_SMALL_MSGSIZE;
  return NM_ESUCCESS;
}

static int nm_sg_small_send_buf_post(void*_status, nm_len_t len)
{
  struct nm_sg_small_s*p_status = _status;
  NM_TRACEF("dest mailbox = %s (%p); ptr = %p; size = %lu\n",
            sg_mailbox_get_name(p_status->dest_mailbox), p_status->dest_mailbox,
            p_status->send.p_buffer, len);
  struct nm_sg_small_pkt_s*p_pkt = padico_malloc(sizeof(struct nm_sg_small_pkt_s));
  p_pkt->p_buffer = p_status->send.p_buffer;
  p_pkt->p_remote_url = p_status->p_simgrid_context->url;
  p_status->send.p_buffer = NULL;
  assert(p_pkt != NULL);
  p_status->send.comm = sg_mailbox_put_async(p_status->dest_mailbox, p_pkt, len);
  return NM_ESUCCESS;
}

static int nm_sg_small_send_poll(void*_status)
{
  struct nm_sg_small_s*p_status = _status;
  if(sg_comm_test(p_status->send.comm))
    {
      p_status->send.comm = NULL;
      return NM_ESUCCESS;
    }
  else
    {
      sg_actor_sleep_for(NM_SG_SMALL_TIME_POLL);
      return -NM_EAGAIN;
    }
}

static int nm_sg_small_send_wait(void*_status)
{
  struct nm_sg_small_s*p_status = _status;
  if(sg_comm_wait(p_status->send.comm) == SG_OK)
    {
      p_status->send.comm = NULL;
      return NM_ESUCCESS;
    }
  else
    {
      sg_actor_sleep_for(NM_SG_SMALL_TIME_POLL);
      return -NM_EAGAIN;
    }
}


static void nm_sg_small_recv_reload(struct nm_sg_small_context_s*p_simgrid_context)
{
  assert(p_simgrid_context->comm == NULL);
  assert(p_simgrid_context->p_incoming_pkt == NULL);
  p_simgrid_context->comm = sg_mailbox_get_async(p_simgrid_context->mailbox,
                                                 (void**)&p_simgrid_context->p_incoming_pkt);
  assert(p_simgrid_context->comm != NULL);
}

static int nm_sg_small_recv_buf_poll(void*_status, void**p_buffer, nm_len_t*p_len)
{
  struct nm_sg_small_s*p_status = _status;
  if(p_status->recv.p_buffer != NULL)
    {
      *p_buffer = p_status->recv.p_buffer;
      *p_len = NM_SG_SMALL_MSGSIZE; /* TODO- */
      return NM_ESUCCESS;
    }
  else
    {
      NM_WARN("buffer not ready after probe/wait_any.\n");
      sg_actor_sleep_for(NM_SG_SMALL_TIME_POLL);
      return -NM_EAGAIN;
    }
}

static int nm_sg_small_recv_buf_release(void*_status)
{
  struct nm_sg_small_s*p_status = _status;
  padico_free(p_status->recv.p_buffer);
  p_status->recv.p_buffer = NULL;
  nm_sg_small_recv_reload(p_status->p_simgrid_context);
  return NM_ESUCCESS;
}

static int nm_sg_small_recv_probe_any(puk_context_t p_context, void**_status)
{
  struct nm_sg_small_context_s*p_simgrid_context = puk_context_get_status(p_context);
  assert(p_simgrid_context->comm != NULL);
  if(sg_comm_test(p_simgrid_context->comm))
    {
      assert(p_simgrid_context->p_incoming_pkt != NULL);
      p_simgrid_context->comm = NULL;
      if(p_simgrid_context->p_incoming_pkt == NM_SG_SMALL_CANCEL)
        {
          p_simgrid_context->p_incoming_pkt = NULL;
          *_status = NULL;
          return -NM_ECANCELED;
        }
      else
        {
          struct nm_sg_small_s*p_status = nm_sg_small_status_hashtable_lookup(&p_simgrid_context->status_table, p_simgrid_context->p_incoming_pkt->p_remote_url);
          assert(p_status->recv.p_buffer == NULL);
          p_status->recv.p_buffer = p_simgrid_context->p_incoming_pkt->p_buffer;
          padico_free(p_simgrid_context->p_incoming_pkt);
          p_simgrid_context->p_incoming_pkt = NULL;
          *_status = p_status;
          return NM_ESUCCESS;
        }
    }
  else
    {
      *_status = NULL;
      sg_actor_sleep_for(NM_SG_SMALL_TIME_POLL);
      return -NM_EAGAIN;
    }
}

static int nm_sg_small_recv_wait_any(puk_context_t p_context, void**_status)
{
  struct nm_sg_small_context_s*p_simgrid_context = puk_context_get_status(p_context);
  assert(p_simgrid_context->comm != NULL);

  if(sg_comm_wait(p_simgrid_context->comm) == SG_OK)
    {
      assert(p_simgrid_context->p_incoming_pkt != NULL);
      p_simgrid_context->comm = NULL;
      if(p_simgrid_context->p_incoming_pkt == NM_SG_SMALL_CANCEL)
        {
          p_simgrid_context->p_incoming_pkt = NULL;
          *_status = NULL;
          return -NM_ECANCELED;
        }
      else
        {
          struct nm_sg_small_s*p_status = nm_sg_small_status_hashtable_lookup(&p_simgrid_context->status_table, p_simgrid_context->p_incoming_pkt->p_remote_url);
          assert(p_status->recv.p_buffer == NULL);
          p_status->recv.p_buffer = p_simgrid_context->p_incoming_pkt->p_buffer;
          padico_free(p_simgrid_context->p_incoming_pkt);
          p_simgrid_context->p_incoming_pkt = NULL;
          *_status = p_status;
          return NM_ESUCCESS;
        }
    }
  else
    {
      *_status = NULL;
      sg_actor_sleep_for(NM_SG_SMALL_TIME_POLL);
      return -NM_EAGAIN;
    }
}

static int nm_sg_small_recv_cancel_any(puk_context_t p_context)
{
  struct nm_sg_small_context_s*p_simgrid_context = puk_context_get_status(p_context);
  sg_mailbox_put(p_simgrid_context->mailbox, NM_SG_SMALL_CANCEL, 0);
  return NM_ESUCCESS;
}

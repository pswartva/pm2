/*
 * NewMadeleine
 * Copyright (C) 2014-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

/** @file Shared-memory driver with 'minidriver' interface.
 */

#include <Padico/Puk.h>
#include <Padico/Module.h>
#include <Padico/Shm.h>
#include <nm_minidriver.h>
#include <nm_private.h>

/* ********************************************************* */

static void*nm_minidriver_largeshm_instantiate(puk_instance_t instance, puk_context_t context);
static void nm_minidriver_largeshm_destroy(void*);

static const struct puk_component_driver_s nm_minidriver_largeshm_component =
  {
    .instantiate = &nm_minidriver_largeshm_instantiate,
    .destroy = &nm_minidriver_largeshm_destroy
  };

static void nm_minidriver_largeshm_getprops(puk_context_t context, struct nm_minidriver_properties_s*props);
static void nm_minidriver_largeshm_init(puk_context_t context, const void**drv_url, size_t*url_size);
static void nm_minidriver_largeshm_close(puk_context_t context);
static void nm_minidriver_largeshm_connect(void*_status, const void*remote_url, size_t url_size);
static void nm_minidriver_largeshm_connect_async(void*_status, const void*remote_url, size_t url_size);
static int  nm_minidriver_largeshm_send_data_post(void*_status, const struct nm_data_s*p_data, nm_len_t chunk_offset, nm_len_t chunk_len);
static int  nm_minidriver_largeshm_send_poll(void*_status);
static int  nm_minidriver_largeshm_recv_data_post(void*_status, const struct nm_data_s*p_data, nm_len_t chunk_offset, nm_len_t chunk_len);
static int  nm_minidriver_largeshm_recv_poll_one(void*_status);
static int  nm_minidriver_largeshm_recv_cancel(void*_status);
static int  nm_minidriver_largeshm_get_rdv_data(void*_status, void*p_ptr, nm_len_t size);
static int  nm_minidriver_largeshm_set_rdv_data(void*_status, void*p_ptr, nm_len_t size);

static const struct nm_minidriver_iface_s nm_minidriver_largeshm_minidriver =
  {
    .getprops        = &nm_minidriver_largeshm_getprops,
    .init            = &nm_minidriver_largeshm_init,
    .close           = &nm_minidriver_largeshm_close,
    .connect         = &nm_minidriver_largeshm_connect,
    .connect_async   = &nm_minidriver_largeshm_connect_async,
    .send_data_post  = &nm_minidriver_largeshm_send_data_post,
    .send_iov_post   = NULL,
    .send_poll       = &nm_minidriver_largeshm_send_poll,
    .recv_iov_post   = NULL,
    .recv_data_post  = &nm_minidriver_largeshm_recv_data_post,
    .recv_poll_one   = &nm_minidriver_largeshm_recv_poll_one,
    .recv_cancel     = &nm_minidriver_largeshm_recv_cancel,
    .get_rdv_data    = &nm_minidriver_largeshm_get_rdv_data,
    .set_rdv_data    = &nm_minidriver_largeshm_set_rdv_data
  };

/* ********************************************************* */

PADICO_MODULE_COMPONENT(Minidriver_largeshm,
  puk_component_declare("Minidriver_largeshm",
                        puk_component_provides("PadicoComponent", "component", &nm_minidriver_largeshm_component),
                        puk_component_provides("NewMad_minidriver", "minidriver", &nm_minidriver_largeshm_minidriver),
                        puk_component_attr("network", "localhost")));

/* ********************************************************* */


/** 'shm' per-context data.
 */
struct nm_minidriver_largeshm_context_s
{
  struct nm_minidriver_largeshm_s*conns[PADICO_SHM_NUMNODES]; /**< connections in the context, indexed by destination rank */
  struct padico_shm_s*shm;                 /**< shm segment */
  char*url;
#ifdef NMAD_PROFILE
  struct
  {
    unsigned long long n_packets;       /**< total number of packets sent */
    unsigned long long n_blocks;        /**< total number of blocks allocated so far */
    unsigned long long no_free_queue;   /**< no free queue when allocating a queue for recv */
    unsigned long long dest_queue_full; /**< destination queue is full while enqueueing pending block */
    unsigned long long no_free_block;   /**< no block available when trying to allocate a block for send */
    unsigned long long enqueue_retry;   /**< needed to retry enqueue of queue_num in frer_queues (race condition detected) */
  } profile;
#endif /* NMAD_PROFILE */
};

/** 'shm' per-instance status.
 */
struct nm_minidriver_largeshm_s
{
  struct padico_shm_node_s*dest;           /**< destination node */
  int dest_rank;                           /**< destination rank in shm directory */
  struct nm_minidriver_largeshm_context_s*p_shm_context;
  struct
  {
    nm_data_slicer_t slicer;
    nm_len_t chunk_len;
    nm_len_t done;
    int queue_num;
  } recv;
  struct
  {
    nm_data_slicer_t slicer;
    nm_len_t len;
    nm_len_t done;
    int queue_num;
    int pending_block;
  } send;
};

/** metadata attached to rdv */
struct nm_minidriver_largeshm_rdv_data_s
{
  int pipeline; /**< index of the pipeline to use for data */
};


/* ********************************************************* */

static void*nm_minidriver_largeshm_instantiate(puk_instance_t instance, puk_context_t context)
{
  struct nm_minidriver_largeshm_s*p_status = padico_malloc(sizeof(struct nm_minidriver_largeshm_s));
  p_status->dest = NULL;
  p_status->dest_rank = -1;
  p_status->p_shm_context = puk_context_get_status(context);
  assert(p_status->p_shm_context->shm->seg != NULL);
  p_status->send.queue_num = -1;
  p_status->send.pending_block = -1;
  p_status->recv.queue_num = -1;
  return p_status;

}

static void nm_minidriver_largeshm_destroy(void*_status)
{
  struct nm_minidriver_largeshm_s*p_status = _status;
  if(p_status->dest_rank > -1)
    p_status->p_shm_context->conns[p_status->dest_rank] = NULL;
  padico_free(_status);
}

/* ********************************************************* */

static void nm_minidriver_largeshm_getprops(puk_context_t context, struct nm_minidriver_properties_s*p_props)
{
  p_props->profile.latency = 200;
  p_props->profile.bandwidth = 10000;
  p_props->capabilities.supports_data = 1;
  p_props->capabilities.supports_iovec = 0;
  p_props->capabilities.trk_rdv = 1;
  p_props->capabilities.needs_rdv_data = 1;
  p_props->nickname = "largeshm";
}

static void nm_minidriver_largeshm_init(puk_context_t context, const void**drv_url, size_t*url_size)
{
  struct nm_minidriver_largeshm_context_s*p_shm_context = padico_malloc(sizeof(struct nm_minidriver_largeshm_context_s));
  padico_string_t segment_name = padico_string_new();
  padico_string_printf(segment_name, "minidriver-large-%s", puk_context_getattr(context, "network"));
  p_shm_context->shm = padico_shm_init(padico_string_get(segment_name));
  int i;
  for(i = 0; i < PADICO_SHM_NUMNODES; i++)
    {
      p_shm_context->conns[i] = NULL;
    }
#ifdef NMAD_PROFILE
  puk_profile_var_defx(unsigned_long_long, counter, &p_shm_context->profile.n_packets, 0,
                       "Minidriver_largeshm", "total number of packets sent",
                       "Minidriver_largeshm.context-%p.n_packets", p_shm_context);
  puk_profile_var_defx(unsigned_long_long, counter, &p_shm_context->profile.n_blocks, 0,
                       "Minidriver_largeshm", "total number of blocks allocated",
                       "Minidriver_largeshm.context-%p.n_blocks", p_shm_context);
  puk_profile_var_defx(unsigned_long_long, counter, &p_shm_context->profile.no_free_queue, 0,
                       "Minidriver_largeshm", "event no free queue when allocating a queue for recv",
                       "Minidriver_largeshm.context-%p.no_free_queue", p_shm_context);
  puk_profile_var_defx(unsigned_long_long, counter, &p_shm_context->profile.dest_queue_full, 0,
                       "Minidriver_largeshm", "event destination queue is full while enqueuing pending block",
                       "Minidriver_largeshm.context-%p.dest_queue_full", p_shm_context);
  puk_profile_var_defx(unsigned_long_long, counter, &p_shm_context->profile.no_free_block, 0,
                       "Minidriver_largeshm", "event no block available when trying to allocate block for send",
                       "Minidriver_largeshm.context-%p.no_free_block", p_shm_context);
  puk_profile_var_defx(unsigned_long_long, counter, &p_shm_context->profile.enqueue_retry, 0,
                       "Minidriver_largeshm", "event needed to retry enqueue of queue_num in free_queues",
                       "Minidriver_largeshm.context-%p.enqueue_retry", p_shm_context);
#endif /* NMAD_PROFILE */
  puk_context_set_status(context, p_shm_context);
  p_shm_context->url = padico_strdup((char*)padico_topo_node_getuuid(padico_topo_getlocalnode()));
  padico_string_delete(segment_name);
  *drv_url = p_shm_context->url;
  *url_size = strlen(p_shm_context->url);
}

static void nm_minidriver_largeshm_close(puk_context_t context)
{
  struct nm_minidriver_largeshm_context_s*p_shm_context = puk_context_get_status(context);
  padico_shm_close(p_shm_context->shm);
  puk_context_set_status(context, NULL);
  padico_free(p_shm_context->url);
  padico_free(p_shm_context);
}

static void nm_minidriver_largeshm_connect(void*_status, const void*remote_url, size_t url_size)
{
  nm_minidriver_largeshm_connect_async(_status, remote_url, url_size);
}

static void nm_minidriver_largeshm_connect_async(void*_status, const void*remote_url, size_t url_size)
{
  struct nm_minidriver_largeshm_s*p_status = _status;
  const char*remote_uuid = remote_url;
  padico_topo_node_t remote_node = padico_topo_getnodebyuuid((padico_topo_uuid_t)remote_uuid);
  assert(p_status->p_shm_context->shm->seg != NULL);
  p_status->dest = padico_shm_directory_node_lookup(p_status->p_shm_context->shm, remote_node);
  if(p_status->dest == NULL)
    {
      padico_fatal("cannot find peer node %s in shm directory.\n", remote_uuid);
    }
  else
    {
      p_status->dest_rank = p_status->dest->rank;
      assert(p_status->dest_rank >= 0);
      assert(p_status->dest_rank < PADICO_SHM_NUMNODES);
      p_status->p_shm_context->conns[p_status->dest_rank] = p_status;
    }
}

static int nm_minidriver_largeshm_send_data_post(void*_status, const struct nm_data_s*p_data, nm_len_t chunk_offset, nm_len_t chunk_len)
{
  struct nm_minidriver_largeshm_s*p_status = _status;
  if(p_status->send.queue_num == -1)
    return -NM_EINVAL;
  p_status->send.done = 0;
  p_status->send.len = chunk_len;
  nm_data_slicer_init(&p_status->send.slicer, p_data);
  if(chunk_offset > 0)
    nm_data_slicer_forward(&p_status->send.slicer, chunk_offset);
  return NM_ESUCCESS;
}

static int nm_minidriver_largeshm_send_poll(void*_status)
{
  struct nm_minidriver_largeshm_s*p_status = _status;
  if(p_status->send.pending_block != -1)
    {
      int rc = padico_shm_large_lfqueue_enqueue(&p_status->p_shm_context->shm->seg->buffer.queues[p_status->send.queue_num], p_status->send.pending_block);
      if(rc != 0)
        {
          nm_profile_inc(p_status->p_shm_context->profile.dest_queue_full);
          return -NM_EAGAIN;
        }
      p_status->send.pending_block = -1;
    }
  while(p_status->send.done < p_status->send.len)
    {
      const int block_num = padico_shm_block_alloc(p_status->p_shm_context->shm);
      if(block_num == -1)
        {
          nm_profile_inc(p_status->p_shm_context->profile.no_free_block);
          return -NM_EAGAIN;
        }
      nm_profile_inc(p_status->p_shm_context->profile.n_blocks);
      void*block = padico_shm_block_get_ptr(p_status->p_shm_context->shm, block_num);
      const int todo = p_status->send.len - p_status->send.done;
      const int b = (todo > PADICO_SHM_BLOCKSIZE) ? PADICO_SHM_BLOCKSIZE : todo;
      nm_data_slicer_copy_from(&p_status->send.slicer, block, b);
      p_status->send.done += b;
      int rc = padico_shm_large_lfqueue_enqueue(&p_status->p_shm_context->shm->seg->buffer.queues[p_status->send.queue_num], block_num);
      if(rc != 0)
        {
          p_status->send.pending_block = block_num;
          return -NM_EAGAIN;
        }
    }
  if(p_status->send.done == p_status->send.len)
    {
      p_status->send.queue_num = -1;
      nm_data_slicer_destroy(&p_status->send.slicer);
      p_status->send.slicer = NM_DATA_SLICER_NULL;
      nm_profile_inc(p_status->p_shm_context->profile.n_packets);
      return NM_ESUCCESS;
    }
  else
    {
      return -NM_EAGAIN;
    }
}

static int nm_minidriver_largeshm_recv_data_post(void*_status, const struct nm_data_s*p_data, nm_len_t chunk_offset, nm_len_t chunk_len)
{
  struct nm_minidriver_largeshm_s*p_status = _status;
  nm_data_slicer_init(&p_status->recv.slicer, p_data);
  if(chunk_offset > 0)
    nm_data_slicer_forward(&p_status->recv.slicer, chunk_offset);
  assert(p_status->p_shm_context->shm->self->mailbox.large[p_status->dest_rank].pipeline == -1);
  /* allocate queue */
  int queue_num = -1;
  while(queue_num == -1)
    {
      queue_num = padico_shm_queue_lfqueue_dequeue(&p_status->p_shm_context->shm->seg->buffer.free_queues);
      if(queue_num == -1)
        {
          nm_profile_inc(p_status->p_shm_context->profile.no_free_queue);
        }
    }
  p_status->recv.chunk_len = chunk_len;
  p_status->recv.done      = 0;
  p_status->recv.queue_num = queue_num;
  return NM_ESUCCESS;
}

static int nm_minidriver_largeshm_recv_poll_one(void*_status)
{
  struct nm_minidriver_largeshm_s*p_status = _status;
  while(p_status->recv.done < p_status->recv.chunk_len)
    {
      int block_num = -1;
      int loops = 0;
      do
        {
          block_num = padico_shm_large_lfqueue_dequeue(&p_status->p_shm_context->shm->seg->buffer.queues[p_status->recv.queue_num]);
          loops++;
          if((block_num == -1) && ((p_status->recv.done == 0) || (loops > 10)))
            {
              return -NM_EAGAIN;
            }
        }
      while(block_num == -1);
      const int todo = p_status->recv.chunk_len - p_status->recv.done;
      int b = (todo > PADICO_SHM_BLOCKSIZE) ? PADICO_SHM_BLOCKSIZE : todo;
      void*block = padico_shm_block_get_ptr(p_status->p_shm_context->shm, block_num);
      nm_data_slicer_copy_to(&p_status->recv.slicer, block, b);
      p_status->recv.done += b;
      padico_shm_block_free(p_status->p_shm_context->shm, block_num);
    }
  assert(p_status->recv.done == p_status->recv.chunk_len);
  nm_data_slicer_destroy(&p_status->recv.slicer);
  p_status->recv.slicer = NM_DATA_SLICER_NULL;
  int rc = -1;
  do
    {
      rc = padico_shm_queue_lfqueue_enqueue(&p_status->p_shm_context->shm->seg->buffer.free_queues, p_status->recv.queue_num);
      if(rc != 0)
        {
          nm_profile_inc(p_status->p_shm_context->profile.enqueue_retry);
        }
    }
  while(rc != 0);
  p_status->recv.queue_num = -1;
  return NM_ESUCCESS;
}

static int nm_minidriver_largeshm_recv_cancel(void*_status)
{
  return -NM_ENOTIMPL;
}

static int nm_minidriver_largeshm_get_rdv_data(void*_status, void*p_ptr, nm_len_t size)
{
  struct nm_minidriver_largeshm_s*p_status = _status;
  assert(p_status->recv.queue_num != -1);
  struct nm_minidriver_largeshm_rdv_data_s*p_rdv_data = p_ptr;
  assert(sizeof(struct nm_minidriver_largeshm_rdv_data_s) <= size);
  p_rdv_data->pipeline = p_status->recv.queue_num;
  return NM_ESUCCESS;
}

static int nm_minidriver_largeshm_set_rdv_data(void*_status, void*p_ptr, nm_len_t size)
{
  struct nm_minidriver_largeshm_s*p_status = _status;
  assert(p_status->send.queue_num == -1);
  struct nm_minidriver_largeshm_rdv_data_s*p_rdv_data = p_ptr;
  assert(p_rdv_data->pipeline >= 0);
  p_status->send.queue_num = p_rdv_data->pipeline;
  return NM_ESUCCESS;
}

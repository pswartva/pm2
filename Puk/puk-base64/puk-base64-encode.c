/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <assert.h>
#include <string.h>

#include "Puk-base64.h"

static char*puk_base64_op(const char*bytes, int*len, char*argv0)
{
  if(strcmp(argv0, "puk-base64-decode") == 0 ||
     (strstr(argv0, "/puk-base64-decode") != NULL))
    {
      return puk_base64_decode(bytes, len, NULL);
    }
  else
    {
      return puk_base64_encode(bytes, len, NULL);
    }
}

int main(int argc, char**argv)
{
  char*outbuf = NULL;
  int len = 0;
  if(argc == 1)
    {
      int bufsize = 4096;
      char*buffer = malloc(bufsize);
      if(buffer == NULL)
        abort();
      int c = -1;
      while(EOF != (c = getchar()))
        {
          if(len >= bufsize)
            {
              bufsize *= 2;
              buffer = realloc(buffer, bufsize);
              if(buffer == NULL)
                abort();
            }
          buffer[len] = c;
          len++;
        }
      outbuf = puk_base64_op(buffer, &len, argv[0]);
      free(buffer);
    }
  else if(argc == 2)
    {
      const char*filename = argv[1];
      int fd = open(filename, O_RDONLY, 0);
      if(fd < 0)
        {
          fprintf(stderr, "%s: file not found: %s\n", argv[0], filename);
          abort();
        }
      struct stat finfo;
      fstat(fd, &finfo);
      const int size = finfo.st_size;
      char*buffer = mmap(NULL, size, PROT_READ, MAP_SHARED, fd, 0);
      assert(buffer != NULL);
      len = size;
      outbuf = puk_base64_op(buffer, &len, argv[0]);
      munmap(buffer, size);
      close(fd);
    }
  else
    {
      fprintf(stderr, "%s: usage- TODO\n", argv[0]);
    }
  if(outbuf != NULL)
    {
      write(1, outbuf, len);
      free(outbuf);
    }
  return 0;
}

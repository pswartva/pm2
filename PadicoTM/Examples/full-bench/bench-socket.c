/* bench-VIO.c
 */

#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <unistd.h>

#include "bench-common.h"
#include <Padico/Module.h>

int bench_socket_init(void);
int bench_socket_run(int argc, char**argv);
void bench_socket_finalize(void);

PADICO_MODULE_DECLARE(bench_socket, bench_socket_init, bench_socket_run, bench_socket_finalize);

#define DEFAULT_PORT       5040

static int port = DEFAULT_PORT;

static struct sockaddr_in local_addr;
static socklen_t          local_addr_size  = sizeof(local_addr);
static struct sockaddr_in remote_addr;
static socklen_t          remote_addr_size = sizeof(remote_addr);
static int bench_fd;
static int server_fd;

int bench_socket_init(void)
{
  if(bench_common_param.is_server)
    {
      int rc;
      server_fd = socket(PF_INET, SOCK_STREAM, 0);
      local_addr.sin_family = AF_INET;
      local_addr.sin_port  = htons(port);
      rc = bind(server_fd, (struct sockaddr*)&local_addr, local_addr_size);
      if(rc != 0)
        {
          padico_warning("cannot bind on port %d\n", port);
          return -1;
        }
      rc = listen(server_fd, 5);
      if(rc != 0)
        {
          padico_warning("listen() %d; error\n", port);
          return -1;
        }
      padico_print("BENCH: server ready\n");
      bench_fd = accept(server_fd, (struct sockaddr*)&remote_addr, &remote_addr_size);
      padico_print("BENCH: received connection.\n");
      close(server_fd);
    }
  else
    {
      struct hostent*he;
      int rc;
      padico_print("BENCH: starting client.\n");
      bench_fd = socket(PF_INET, SOCK_STREAM, 0);
      const char*server_name = padico_topo_host_getname(padico_topo_node_gethost(bench_common_param.server_node));
      he = gethostbyname(server_name);
      if(he == NULL)
        {
          padico_warning("cannot resolve host=**%s**\n", server_name);
          return -1;
        }
      memcpy(&remote_addr.sin_addr, he->h_addr_list[0], sizeof(remote_addr.sin_addr));
      remote_addr.sin_family = AF_INET;
      remote_addr.sin_port  = htons(port);
      do
        {
          sleep(1);
          padico_print("BENCH: connecting to server...\n");
          rc = connect(bench_fd, (struct sockaddr*)&remote_addr, remote_addr_size);
          if(rc != 0)
            {
              padico_warning("cannot connect fd %d. Retrying...\n", bench_fd);
              sleep(1);
              /* TODO: c'est un peu bourrin d'attendre... */
            }
          else
            {
              padico_print("BENCH: client ready.\n");
            }
        }
      while(rc != 0);
    }
  return 0;
}

void bench_socket_finalize(void)
{
  if(bench_common_param.is_server)
    {
      //     close(server_fd);
    }
  close(bench_fd);
}


static ssize_t socket_in(char *buf, size_t nbyte)
{
  size_t done = 0;
  while(done < nbyte)
    {
      int rc = recv(bench_fd, buf+done, nbyte-done, 0);
      if(rc > 0)
        done += rc;
      else if(rc == 0)
        break;
      else{
        return rc;
      }
    }
  if(done != nbyte)
    padico_warning("BENCH: read()- nbyte=%d; rc=%d\n", nbyte, done);
  return done;
}

static ssize_t socket_out(char *buf, size_t nbyte)
{
  size_t done = 0;
  while(done < nbyte)
    {
      int rc = send(bench_fd, buf+done, nbyte-done, 0);
      if(rc > 0)
        done += rc;
      else if(rc == 0)
        break;
      else {
        return rc;
      }
    }
  if(done != nbyte)
    padico_warning("BENCH: write()- nbyte=%d; rc=%d\n", nbyte, done);
  return done;
}

static void socket_server(char*buffer, size_t lenbuf)
{
  socket_out(buffer, lenbuf);
  socket_in(buffer, lenbuf);
}

static void socket_client(char*buffer, size_t lenbuf)
{
  socket_in(buffer, lenbuf);
  socket_out(buffer, lenbuf);
}

static struct bench_desc_s socket_bench =
  {
    "SOCKET",
    &socket_server,
    &socket_client
  };

int bench_socket_run(int argc, char**argv)
{
  the_bench(&socket_bench);
  return 0;
}

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <strings.h>
#include <string.h>
#include <netinet/in.h>
#include <netdb.h>
#include <time.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
#include "helper.h"

#define MSGLEN 512

int main(int argc, char *argv[])
{
  usage(argc, argv);
  int port = atoi(argv[3]);
  int sock_fd = create_socket();
  char msg[MSGLEN];
  memset(msg, 0, MSGLEN);

  if(strncmp("server",argv[1],6)==0)
    {
      bind_and_listen(sock_fd, port);
      struct sockaddr_in cli_addr;
      int clilen = sizeof(cli_addr);
      int clifd = accept(sock_fd,  (struct sockaddr *) &cli_addr,&clilen);
      if (clifd < 0)
        print_error("accept()");
      strcpy(msg, "Hello client\0");
      printf("Server: I send \"%s\"\n", msg);
      if(write(clifd, msg, strlen(msg)) < 0)
        print_error("write()");
      close2(clifd);
    }
  else if(strncmp("client",argv[1],6)==0)
    {
      connect2(sock_fd, port, argv[2]);
      if(read(sock_fd, msg, MSGLEN) < 0)
        print_error("read()");
      printf("Client: I receive \"%s\"\n", msg);
    }
  close2(sock_fd);

  return 0;
}

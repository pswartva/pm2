/*
 * Bench NBC
 * Copyright (C) 2019-2020 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#ifndef MATRIX_H
#define MATRIX_H
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "omp.h"
#include "../bench_constants.h"
#include "../misc.h"
//#include <mpi.h>

// column major
#define icm(size, x, y) (x + size * y)
// row major
#define irm(size, x, y) (size * x + y)

// matrix addition with double
void Matrix_add(double *a, double *b, double *res, long int size);

// matrix multiplication with double and column major
void Matrix_mul(double *a, double *b, double *res, long int size);

// matrix multiplication with double and column major(with OpenMP)
#ifdef GLOBAL_BUFFER
void matrix_compute(long int size);
#else
void matrix_compute(double *a, double *b, double *res,long int size);
#endif

//find matrix size which take "time" to compute
long int init_compute(double time);

long long int interpolate(long long int n1, long long int n2, double t1, double t2, double target);
#endif // MATRIX_H

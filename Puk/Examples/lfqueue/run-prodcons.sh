#! /bin/bash

# run prodcons benchmark for queue types given in args (or all if none given)

args="$*"

qs="atomic nblfq nblfq2 looqueue wcq simqueue koganpetrank crturn lcrq msqueue crdoublelink wfqueue bitnext lineararray scq2"

if [ "x${args}" != "x" ]; then
    qs="${args}"
    echo "# using queue list from command line: ${qs}"
fi

TIMEOUT=10
MAXTHREADS=16 # ## william: 16; henri: 32; adastra: 192
MAXRETRY=2

run_bench() {
    q="$1"
    args="$2"
    b="$3"
    for n in $(seq -w 1 ${MAXTHREADS}); do
	i=0
	rc=1
	while [ $rc != 0 ]; do
	    timeout ${TIMEOUT} stdbuf -o0 puk_bench_lfqueue-${q} ${args} ${n} | tee ${q}-${b}-${n}.dat
	    v=$(echo ${n} | sed 's/^0*//')
	    if [ "x${args}" = "x-pc" ]; then
		grep -P "^${v} \t ${v}" ${q}-${b}-${n}.dat
		rc=$?
	    else
		grep -P "^${v} " ${q}-${b}-${n}.dat
		rc=$?
	    fi
	    echo "# n = ${n}; i = ${i}; rc = ${rc}"
	    i=$(( ${i} + 1 ))
	    if [ ${i} -gt ${MAXRETRY} ]; then
		echo "# giving up"
		break
	    fi
	done
	cat ${q}-${b}-${n}.dat >> ${q}-${b}.dat
	rm ${q}-${b}-${n}.dat
    done
}

for q in ${qs}; do
    echo "# ## starting ${q}..."
    run_bench ${q} -pc prodcons
    run_bench ${q} -p  pairwise
    run_bench ${q} -pp preload
    run_bench ${q} -e  empty
done


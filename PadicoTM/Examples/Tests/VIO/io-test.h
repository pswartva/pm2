
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netinet/tcp.h>

#define DEFAULT_SNDBUF 262144
#define DEFAULT_RCVBUF 131072 

#define config_num_roundtrip 500
#define config_check_buffer  0
#define config_reinit_buffer 0

#define BUFFER_SIZE (8*1024*1024)
#define MAX_CLIENTS 10
#define SERVER_PORT_NUMBER 4576
#define ALIGN_VAL  ((unsigned)sizeof(long))
#define ALIGN_MASK (~((ALIGN_VAL)-1))
#define ALIGNED(N) (((unsigned)(N)) & (ALIGN_MASK))

#define my_warning printf
#define my_fatal(MSG) ( printf(MSG), abort() )

int  make_socket(void);
void send_msg1(int fd, unsigned char*buffer, int size, int size1);
void send_msg(int fd, unsigned char*buffer, int size);
void recv_msg(int fd, unsigned char*buffer, int size);
void fill_buffer(unsigned char*buffer, int size, int val);
void check_buffer(unsigned char*buffer, int s, int i);
void test_failed(void);


/* bench-VIO.c
 */

#include <netdb.h>
#include <Padico/VIO.h>
#include <Padico/Timing.h>
#include <Padico/Module.h>
#include <Padico/NetAccess.h>

int vio_ring_init(void);
int vio_ring_run(int argc, char**argv);
void vio_ring_finalize(void);

PADICO_MODULE_DECLARE(VIO_ring, vio_ring_init, vio_ring_run, vio_ring_finalize);

#define DEFAULT_PORT       5041

static int port = DEFAULT_PORT;

static struct sockaddr_in local_addr;
static socklen_t          local_addr_size  = sizeof(local_addr);
static struct sockaddr_in remote_addr;
static socklen_t          remote_addr_size = sizeof(remote_addr);
static int in_fd;
static int out_fd;
static int server_fd;
static int size;
static int rank;

#define DEFAULT_ROUNDTRIP       1000
#define DEFAULT_MAX_LEN         32*1024*1024
#define DEFAULT_LOGSCALE_FACTOR 1.8

static long maxlen            = DEFAULT_MAX_LEN;
static int  num_roundtrip     = DEFAULT_ROUNDTRIP;
static char *buffer           = NULL;
static int logscale_threshold = 192;
static double logscale_factor = DEFAULT_LOGSCALE_FACTOR;
static int len_step  = 4;
static int len_start = 1;
static const int downramp_threshold = 128 * 1024;
static const int downramp_guard = 20;
static const int dryrun_count = 500;


int vio_ring_init(void)
{
  padico_group_t group = padico_group_lookup("VIO-ring");
  if(!group)
    padico_fatal("You need to create a \"VIO-ring\" group to execute this test.\n");

  size = padico_group_size(group);
  rank = padico_group_rank(group, padico_topo_getlocalnode());

  if(rank == 0)
    {
      /* 0 Node */
      // Accept conn
      int rc;
      server_fd = padico_vio_socket(PF_INET, SOCK_STREAM, 0);
      local_addr.sin_family = AF_INET;
      local_addr.sin_port   = htons(port);
      local_addr.sin_port   += rank;
      rc = padico_vio_bind(server_fd, (struct sockaddr*)&local_addr, local_addr_size);
      if(rc != 0)
        {
          padico_warning("BENCH: cannot bind on port %d\n", port);
          return -1;
        }
      rc = padico_vio_listen(server_fd, 5);
      if(rc != 0)
        {
          padico_warning("listen() error\n", port);
          return -1;
        }
      padico_print("BENCH: [node 0] server ready\n");
      in_fd = padico_vio_accept(server_fd, (struct sockaddr*)&remote_addr, &remote_addr_size);
      if(in_fd < 0)
        {
          int err = padico_vio_errno;
          padico_warning("BENCH: accept()- errno=%d (%s).\n", err, strerror(err));
          return -1;
        }
      padico_print("BENCH: [node 0] received connection.\n");
      padico_vio_close(server_fd);
      // Then Connect
      rc = -1;
      int err = -1;
      out_fd = padico_vio_socket(PF_INET, SOCK_STREAM, 0);
      remote_addr.sin_addr   = *padico_topo_host_getaddr(padico_topo_node_gethost(padico_group_node(group, 1)));
      remote_addr.sin_family = AF_INET;
      remote_addr.sin_port   = htons(port);
      remote_addr.sin_port   += (rank+1)%size;
      do
        {
          padico_timing_t tick1, tick2;
          padico_print("BENCH: [node 0] waiting for the server to come up...\n");
          padico_tm_sleep(2);
          padico_print("BENCH: [node 0] connecting to server...\n");
          padico_timing_get_tick(&tick1);
          rc = padico_vio_connect(out_fd, (struct sockaddr*)&remote_addr, remote_addr_size);
          padico_timing_get_tick(&tick2);
          if(rc != 0)
            {
              err = padico_vio_errno;
              padico_warning("cannot connect- errno=%d (%s). Retrying...\n", err, strerror(err));
              padico_tm_sleep(1);
              /* TODO: c'est un peu bourrin d'attendre... */
            }
          else
            {
              padico_print("BENCH: [node 0] client ready- connect = %g usec.\n",
                           padico_timing_diff_usec(&tick1, &tick2));
            }
        }
      while(rc != 0);
    } else {
      /* Others Nodes */
      int rc = -1, err = -1;
      out_fd = padico_vio_socket(PF_INET, SOCK_STREAM, 0);
      remote_addr.sin_addr   = *padico_topo_host_getaddr(padico_topo_node_gethost(padico_group_node(group, (rank+1)%size)));
      remote_addr.sin_family = AF_INET;
      remote_addr.sin_port   = htons(port);
      remote_addr.sin_port   += (rank+1)%size;
      do
        {
          padico_timing_t tick1, tick2;
          padico_print("BENCH: [node %i] waiting for the server to come up...\n", rank);
          padico_tm_sleep(2);
          padico_print("BENCH: [node %i] connecting to server...\n", rank);
          padico_timing_get_tick(&tick1);
          rc = padico_vio_connect(out_fd, (struct sockaddr*)&remote_addr, remote_addr_size);
          padico_timing_get_tick(&tick2);
          if(rc != 0)
            {
              err = padico_vio_errno;
              padico_warning("cannot connect- errno=%d (%s). Retrying...\n", err, strerror(err));
              padico_tm_sleep(1);
              /* TODO: c'est un peu bourrin d'attendre... */
            }
          else
            {
              padico_print("BENCH: [node %i] client ready- connect = %g usec.\n", rank,
                           padico_timing_diff_usec(&tick1, &tick2));
            }
        }
      while(rc != 0);
      server_fd = padico_vio_socket(PF_INET, SOCK_STREAM, 0);
      local_addr.sin_family = AF_INET;
      local_addr.sin_port   = htons(port);
      local_addr.sin_port   += rank;
      rc = padico_vio_bind(server_fd, (struct sockaddr*)&local_addr, local_addr_size);
      if(rc != 0)
        {
          padico_warning("BENCH: cannot bind on port %d\n", port);
          return -1;
        }
      rc = padico_vio_listen(server_fd, 5);
      if(rc != 0)
        {
          padico_warning("listen() error\n", port);
          return -1;
        }
      padico_print("BENCH: [node %i] server ready\n", rank);
      in_fd = padico_vio_accept(server_fd, (struct sockaddr*)&remote_addr, &remote_addr_size);
      if(in_fd < 0)
        {
          int err = padico_vio_errno;
          padico_warning("BENCH: accept()- errno=%d (%s).\n", err, strerror(err));
          return -1;
        }
      padico_print("BENCH: [node %i] received connection.\n", rank);
      padico_vio_close(server_fd);
    }
  padico_print("BENCH: [node %i] All connections ready.\n", rank);

  if ( (buffer = (char *) padico_malloc((unsigned) maxlen)) == (char *) NULL)
    {
      padico_fatal("could not allocate buffer of size %d.\n", maxlen);
      return 1;
    }
  int i;
  for(i = 0; i < maxlen; i++)
    {
      buffer[i] = i;
    }

  return 0;
}

void vio_ring_finalize(void)
{
  padico_vio_close(in_fd);
  padico_vio_close(out_fd);
}

static ssize_t vio_test_in(int fildes, void *buf, size_t nbyte)
{
  int done = 0;
  while(done < nbyte)
    {
      int rc = padico_vio_read(fildes, buf+done, nbyte - done);

      if(rc > 0)
        done += rc;
      else if(rc == 0)
        break;
      else{
        return rc;
      }
    }
  if(done != nbyte)
    padico_warning("BENCH: read()- nbyte=%d; rc=%d\n", nbyte, done);
  return done;
}

static ssize_t vio_test_out(int fildes, const void *buf, size_t nbyte)
{
  int done = 0;
  while(done < nbyte)
    {
      int rc = padico_vio_write(fildes, buf+done, nbyte-done);
      if(rc > 0)
        done += rc;
      else if(rc == 0)
        break;
      else {
        return rc;
      }
    }
  if(done != nbyte)
    padico_warning("BENCH: write()- nbyte=%d; rc=%d\n", nbyte, done);
  return done;
}

int vio_ring_run(int argc, char**argv)
{
  double total_time_usec;
  padico_timing_t tick_start, tick_end;
  double avg_time_usec;
  double bw_MBps;
  int current_rt = num_roundtrip;
  int lenbuf = len_start;
  if(rank == 0)
    {
      padico_print("VIO-ring bench ----------------------------------------\n");
    }

  /* dryrun */
  int i;
  for(i = 0; i < dryrun_count; i++)
    {
      if(rank == 0)
        {
          vio_test_out(out_fd, buffer, lenbuf);
          vio_test_in(in_fd, buffer, lenbuf);
        } else {
          vio_test_in(in_fd, buffer, lenbuf);
          vio_test_out(out_fd, buffer, lenbuf);
        }
    }
  total_time_usec = 0;

  while (lenbuf <= maxlen)
    {
      if(lenbuf > downramp_threshold && current_rt > downramp_guard)
        current_rt /= logscale_factor;
      total_time_usec = 0;
      padico_out(40, "starting bench- len=%d\n", lenbuf);
      padico_timing_get_tick(&tick_start);
      if(rank == 0)
        {
          for(i = 0; i < current_rt; i++)
            {
              padico_timing_get_tick(&tick_start);
              vio_test_out(out_fd, buffer, lenbuf);
              vio_test_in(in_fd, buffer, lenbuf);
              padico_timing_get_tick(&tick_end);
              total_time_usec += padico_timing_diff_usec(&tick_start, &tick_end);
            }
        }
      else
        {
          for(i = 0; i < current_rt; i++)
            {
              vio_test_in(in_fd, buffer, lenbuf);
              vio_test_out(out_fd, buffer, lenbuf);
            }
        }

      padico_out(40, "done bench- len=%d\n", lenbuf);
      avg_time_usec = total_time_usec / (double)(current_rt * size);
      if (avg_time_usec > 0)
        bw_MBps = (double)(lenbuf / avg_time_usec);
      else
        bw_MBps = 0.0;
      if(rank == 0)
        {
          padico_print("%7d bytes, time=%7.2f usec, bw=%6.2f MB/s rt=%d\n",
                       lenbuf, avg_time_usec, bw_MBps, current_rt);
        }
      if(lenbuf <= sizeof(long))
        {
          lenbuf *= 2;
        }
      else if(lenbuf >= logscale_threshold)
        {
          lenbuf *= logscale_factor;
        }
      else
        {
          lenbuf += len_step;
        }
    }
  if(rank == 0)
    {
      padico_print("VIO-ring bench ok -------------------------------------\n");
    }
  return 0;
}

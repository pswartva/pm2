/*
 * NewMadeleine
 * Copyright (C) 2011-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <sys/uio.h>
#include <assert.h>
#include <values.h>

#include <nm_private.h>
#include <nm_launcher_interface.h>
#include <nm_minidriver_interface.h>

#define MAXLEN (1024 * 1024 * 32)
#define ROUNDTRIPS 10000

static inline void nm_minidriver_rdv_send(struct puk_receptacle_NewMad_minidriver_s*r_small,
                                          const struct nm_minidriver_properties_s*p_props_small,
                                          struct puk_receptacle_NewMad_minidriver_s*r_large,
                                          const struct nm_minidriver_properties_s*p_props_large,
                                          void*buf, size_t len)
{
  int rdv = 0, rtr;
  nm_minidriver_send(r_small, p_props_small, &rdv, sizeof(rdv));
  nm_minidriver_recv(r_small, p_props_small, &rtr, sizeof(rtr));
  nm_minidriver_send(r_large, p_props_large, buf, len);
}

static inline void nm_minidriver_rdv_recv(struct puk_receptacle_NewMad_minidriver_s*r_small,
                                          const struct nm_minidriver_properties_s*p_props_small,
                                          struct puk_receptacle_NewMad_minidriver_s*r_large,
                                          const struct nm_minidriver_properties_s*p_props_large,
                                          void*buf, size_t len)
{
  int rdv = 0, rtr;
  nm_minidriver_recv(r_small, p_props_small, &rdv, sizeof(rdv));
  nm_minidriver_send(r_small, p_props_small, &rtr, sizeof(rtr));
  nm_minidriver_recv(r_large, p_props_large, buf, len);
}

static inline nm_len_t _iterations(int iterations, nm_len_t len)
{
  const uint64_t max_data = 512 * 1024 * 1024;
  if(len <= 0)
    len = 1;
  uint64_t data_size = ((uint64_t)iterations * (uint64_t)len);
  if(data_size  > max_data)
    {
      iterations = (max_data / (uint64_t)len);
      if(iterations < 2)
        iterations = 2;
    }
  return iterations;
}

int main(int argc, char **argv)
{
  /* launch nmad session, get gate and driver */
  nm_launcher_init(&argc, argv);
  int rank = -1;
  nm_launcher_get_rank(&rank);
  const int is_server = !rank;
  const int peer = 1 - rank;
  nm_gate_t p_gate = NULL;
  nm_launcher_get_gate(peer, &p_gate);
  assert(p_gate != NULL);
  assert(p_gate->status == NM_GATE_STATUS_CONNECTED);

  /* take over the driver */
  nm_core_schedopt_disable(p_gate->p_core);

  /* hack here-
   * make sure the peer node has flushed its pending recv requests,
   * so that the pw we send are not processed by schedopt.
   */
  puk_usleep(500 * 1000);

  /* benchmark */
  struct nm_trk_s*p_trk_small = nm_trk_get_by_index(p_gate, NM_TRK_SMALL);
  struct puk_receptacle_NewMad_minidriver_s*r = &p_trk_small->receptacle;
  const struct nm_minidriver_properties_s*p_props = &p_trk_small->p_drv->props;
  struct nm_trk_s*p_trk_large = nm_trk_get_by_index(p_gate, NM_TRK_LARGE);
  struct puk_receptacle_NewMad_minidriver_s*r_large = &p_trk_large->receptacle;
  const struct nm_minidriver_properties_s*p_props_large = &p_trk_large->p_drv->props;
  void*buffer = malloc(MAXLEN);

  const nm_len_t max_small = (p_props->capabilities.max_msg_size > NM_MAX_UNEXPECTED) ?
    NM_MAX_UNEXPECTED : p_props->capabilities.max_msg_size;

  printf("# driver small = %s\n", p_trk_small->instance->component->name);
  printf("# driver large = %s\n", p_trk_large->instance->component->name);

  printf("# rendez-vous threshold = %lu\n", max_small);
  if(is_server)
    {
      printf("# size (bytes) \t|  latency \t| 10^6 B/s \t| MB/s\n");
      nm_len_t len;
      for(len = 1; len <= MAXLEN; len *= 2)
        {
          int roundtrips = _iterations(ROUNDTRIPS, len);
          double min_lat = DBL_MAX;
          int i;
          for(i = 0; i < roundtrips; i++)
            {
              puk_tick_t t1, t2;
              PUK_GET_TICK(t1);
              if(len <= max_small)
                {
                  nm_minidriver_send(r, p_props, buffer, len);
                  nm_minidriver_recv(r, p_props, buffer, len);
                }
              else
                {
                  nm_minidriver_rdv_send(r, p_props, r_large, p_props_large, buffer, len);
                  nm_minidriver_rdv_recv(r, p_props, r_large, p_props_large, buffer, len);
                }
              PUK_GET_TICK(t2);
              const double delay = PUK_TIMING_DELAY(t1, t2);
              const double t = delay / 2;
              if(t < min_lat)
                min_lat = t;
            }
          const double bw_million_byte = len / min_lat;
          const double bw_mbyte        = bw_million_byte / 1.048576;
          printf("%8lu \t%9.3lf\t%9.3lf\t%9.3lf\n", len, min_lat, bw_million_byte, bw_mbyte);
        }
    }
  else
    {
      nm_len_t len;
      for(len = 1; len <= MAXLEN; len *= 2)
        {
          int roundtrips = _iterations(ROUNDTRIPS, len);
          int i;
          for(i = 0; i < roundtrips; i++)
            {
              if(len <= max_small)
                {
                  nm_minidriver_recv(r, p_props, buffer, len);
                  nm_minidriver_send(r, p_props, buffer, len);
                }
              else
                {
                  nm_minidriver_rdv_recv(r, p_props, r_large, p_props_large, buffer, len);
                  nm_minidriver_rdv_send(r, p_props, r_large, p_props_large, buffer, len);
                }
            }
        }
    }

  return 0;
}

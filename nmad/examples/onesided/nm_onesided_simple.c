/*
 * NewMadeleine
 * Copyright (C) 2006-2021 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <nm_onesided_interface.h>
#include "../common/nm_examples_helper.h"
#include <stdio.h>

static int a = 0;

int main(int argc, char**argv)
{
  const nm_tag_t tag = 0x01;
  nm_examples_init(&argc, argv);

  /* intialize the onesided interface */
  nm_onesided_t p_onesided;
  nm_onesided_init(p_session, &p_onesided);

  if(is_server)
    {
      /* ** target side */
      /* send address of variable 'a' */
      int*p_a = &a;
      nm_sr_send(p_session, p_gate, tag, &p_a, sizeof(int*));
      /* 1. target of non-blocking put */
      nm_coll_barrier(p_comm, tag);
      printf("a = %d (1)\n", a);
      /* 2. target of blocking put */
      nm_coll_barrier(p_comm, tag);
      printf("a = %d (2)\n", a);
      /* 3. target of non-blocking get */
      a = 3;
      nm_coll_barrier(p_comm, tag);
      /* 4. target-side completion for a put, NULL completion data */
      nm_onesided_target_request_t target_req;
      nm_onesided_target_irecv(p_onesided, p_gate, NULL, 0, &target_req);
      nm_onesided_target_wait(&target_req);
      printf("a = %d (4)\n", a);
      nm_coll_barrier(p_comm, tag);
      /* 5. target-side completion for a put, non-NULL completion data */
      int c = 0;
      nm_onesided_target_irecv(p_onesided, p_gate, &c, sizeof(c), &target_req);
      nm_onesided_target_wait(&target_req);
      printf("a = %d (5); c = %d (1)\n", a, c);
      a = 6;
      nm_coll_barrier(p_comm, tag);
      /* 6. target-side completion for a get */
      c = 0;
      nm_onesided_target_irecv(p_onesided, p_gate, &c, sizeof(c), &target_req);
      nm_onesided_target_wait(&target_req);
      printf("c = %d (1)\n", c);
      nm_coll_barrier(p_comm, tag);
    }
  else
    {
      /* ** initiator side */
      /* receive address for remote variable 'a' */
      int*p_a = NULL;
      nm_sr_recv(p_session, p_gate, tag, &p_a, sizeof(int*));
      a = 1;
      /* build data descriptor for local a */
      struct nm_data_s data;
      nm_data_contiguous_build(&data, &a, sizeof(a));
      /* 1. non-blocking put */
      nm_onesided_request_t req;
      nm_onesided_iput_data(p_onesided, p_gate, &data, (uintptr_t)p_a, &req);
      nm_onesided_request_t freq;
      nm_onesided_ifence(p_onesided, p_gate, &freq);
      nm_onesided_request_t*reqs[2] = { &req, &freq };;
      nm_onesided_req_wait_all(reqs, 2);
      nm_coll_barrier(p_comm, tag);
      /* 2. blocking put */
      a = 2;
      nm_onesided_put(p_onesided, p_gate, &a, sizeof(a), (uintptr_t)p_a);
      nm_onesided_fence(p_onesided, p_gate);
      nm_coll_barrier(p_comm, tag);
      /* 3. non-blocking get */
      nm_onesided_iget_data(p_onesided, p_gate, &data, (uintptr_t)p_a, &req);
      nm_onesided_req_wait(&req);
      printf("a = %d (3)\n", a);
      nm_coll_barrier(p_comm, tag);
      /* 4. put with target-side completion, NULL completion data */
      a = 4;
      nm_onesided_iput_with_target(p_onesided, p_gate, &a, sizeof(a), (uintptr_t)p_a, NULL, 0, &req);
      nm_onesided_req_wait(&req);
      nm_coll_barrier(p_comm, tag);
      /* 5. put with target-side completion, non-NULL completion data */
      int c = 1;
      a = 5;
      nm_onesided_iput_with_target(p_onesided, p_gate, &a, sizeof(a), (uintptr_t)p_a, &c, sizeof(c), &req);
      nm_onesided_req_wait(&req);
      nm_coll_barrier(p_comm, tag);
      /* 6. get with target-side completion */
      nm_onesided_iget_with_target(p_onesided, p_gate, &a, sizeof(a), (uintptr_t)p_a, &c, sizeof(c), &req);
      nm_onesided_req_wait(&req);
      printf("a = %d (6)\n", a);
      nm_coll_barrier(p_comm, tag);
    }

  /* stop the onesided interface */
  nm_onesided_finalize(p_onesided);
  nm_examples_exit();
}

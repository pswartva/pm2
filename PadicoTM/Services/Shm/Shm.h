/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file Padico Shared memory common interface
 */

#ifndef PADICO_SHM_H
#define PADICO_SHM_H

#include <sys/types.h>
#include <semaphore.h>
#include <unistd.h>

#define SHM_USE_POSIX

#if defined(SHM_USE_POSIX)
#  include <sys/mman.h>
#  include <fcntl.h>
#elif defined(SHM_USE_IPC)
#  warning "Using IPC shm"
#  include <sys/shm.h>
#else
#  error "No shm method defined"
#endif

#include <Padico/Puk.h>
#include <Padico/Topology.h>
#include <Padico/PSP.h>

#include <Padico/Puk.h>

PUK_MOD_AUTO_DEP(Shm, PADICO_LAST_MODULE_NAME);
#undef PADICO_LAST_MODULE_NAME
#define PADICO_LAST_MODULE_NAME Shm

/* ********************************************************* */

#define PADICO_SHM_NUMNODES  128
/** Number of blocks allocated in shm buffer
 * @note to prevent deadlocks, should be at least num_nodes+1
 */
#define PADICO_SHM_NUMBLOCKS 4096
/** size of data block in segment */
#define PADICO_SHM_BLOCKSIZE (16 * 1024)
/** number of pending packets per queue (short & large) */
#define PADICO_SHM_QUEUESIZE 64
/** number of queues for large packets in segment */
#define PADICO_SHM_NUMQUEUES 512
#define PADICO_SHM_BLOCKCACHESIZE 16
#define PADICO_YIELD_ON_WAIT

#define PADICO_SHM_ALIGN 4096

/** type for short messages queues- contains block index */
PUK_LFQUEUE_ATOMIC_TYPE(padico_shm_short, int, -1, PADICO_SHM_QUEUESIZE);
/** type for free blocks queue- contains block index */
PUK_LFQUEUE_ATOMIC_TYPE(padico_shm_block, int, -1, PADICO_SHM_NUMBLOCKS*2);
/** type for free blocks cache- contains block index */
PUK_LFQUEUE_ATOMIC_TYPE(padico_shm_blockcache, int, -1, PADICO_SHM_BLOCKCACHESIZE);
/** type for large message queues- contains block index */
PUK_LFQUEUE_ATOMIC_TYPE(padico_shm_large, int, -1, PADICO_SHM_QUEUESIZE);
/** type for queues of queues- contains large queues index */
PUK_LFQUEUE_ATOMIC_TYPE(padico_shm_queue, int, -1, PADICO_SHM_NUMQUEUES*2);

/** A node descriptor. One is allocated in the shared segment
 * for every node that opened the segment.
 */
struct padico_shm_node_s
{
  struct
  {
    /* note: drivers are expected to use either shared_queue or per-node short_queues[], not both */
    struct padico_shm_short_lfqueue_s shared_queue; /**< shared queue for short messages sent eagerly; contains block index */
    struct padico_shm_short_lfqueue_s short_queues[PADICO_SHM_NUMNODES]; /**< queues for short messages sent eagerly; contains block index */
    struct padico_shm_blockcache_lfqueue_s block_cache; /**< free blocks cache */
    struct
    {
      volatile int pipeline;                 /**< allocated queue for transfer */
    } large[PADICO_SHM_NUMNODES]; /**< large messages mbox; unlocked acces, valid when pipeline != -1 */
  } mailbox;
  char node_uuid[PADICO_TOPO_UUID_SIZE + 1]; /**< UUID identifying the node + null byte */
  int pid;                                   /**< process id */
  int rank;                                  /**< index of the node in directory */
  volatile int enabled;
} __attribute__((aligned(PADICO_SHM_ALIGN)));

/** The mmaped segment
 */
struct padico_shm_seg_s
{
  struct
  {
    char date[16];
    char time[16];
  } build_string;  /**< build string to ensure all nodes use the same build */
  struct
  {
    struct padico_shm_node_s nodes[PADICO_SHM_NUMNODES];            /**< directory of node connected to the shared segment */
  } directory;
  struct
  {
    char block[PADICO_SHM_BLOCKSIZE * PADICO_SHM_NUMBLOCKS];        /**< raw blocks of data */
    struct padico_shm_block_lfqueue_s free_blocks;                  /**< block allocator, i.e. queue of block id */
    struct padico_shm_large_lfqueue_s queues[PADICO_SHM_NUMQUEUES]; /**< pool of queues to pipeline large transfers */
    struct padico_shm_queue_lfqueue_s free_queues;                  /**< queue allocator, i.e. queue of queue id */
    struct padico_shm_block_lfqueue_s pending_blocks[PADICO_SHM_NUMNODES]; /**< pending blocks to send (used only by nm_strat_shm) */
  } buffer;
};

/** Generic shm header for short messages in the eager queue
 */
struct padico_shm_short_header_s
{
  volatile uint32_t from;  /**< index of sender in directory */
  volatile uint32_t size;  /**< actual size of data payload in the message (excluding header size) */
};

#define PADICO_SHM_SHORT_PAYLOAD (PADICO_SHM_BLOCKSIZE - sizeof(struct padico_shm_short_header_s))

/** global state of the Shm segment
 */
struct padico_shm_s
{
  struct padico_shm_seg_s*seg;
  struct padico_shm_node_s*self;
  int rank;
  char*shm_path;
  char*sem_path;
  sem_t*sem;
};

/* ********************************************************* */

extern struct padico_shm_s*padico_shm_init(const char*name);

extern void padico_shm_close(struct padico_shm_s*shm);

extern struct padico_shm_node_s*padico_shm_directory_node_lookup(const struct padico_shm_s*shm, padico_topo_node_t node);

/** dump content of shm directory, for debug */
extern void padico_shm_directory_dump(struct padico_shm_s*shm);

extern void padico_shm_large_send(struct padico_shm_s*shm, const void*bytes, size_t size, struct padico_shm_node_s*dest);
extern int  padico_shm_large_recv_post(struct padico_shm_s*shm, int from);
extern int  padico_shm_large_recv_poll(struct padico_shm_s*shm, void*bytes, size_t size, int queue_num);
extern void padico_shm_large_recv(struct padico_shm_s*shm, void*bytes, size_t size, int from);


/* ********************************************************* */
/* ** shared memory blocks allocator */

/** allocate a memory block in shm buffer */
int padico_shm_block_alloc(struct padico_shm_s*shm);

/** get the pointer to a block given its block number */
static inline void*padico_shm_block_get_ptr(struct padico_shm_s*shm, int block_num)
{
  assert(block_num >= 0);
  assert(block_num < PADICO_SHM_NUMBLOCKS);
  return shm->seg->buffer.block + block_num * PADICO_SHM_BLOCKSIZE;
}
/** return a free block back to the free blocks pool */
static inline void padico_shm_block_free(struct padico_shm_s*shm, int block_num)
{
  assert(block_num >= 0);
  assert(block_num < PADICO_SHM_NUMBLOCKS);
  int rc = -1;
 retry:
  rc = padico_shm_blockcache_lfqueue_enqueue_single_writer(&shm->self->mailbox.block_cache, block_num);
  if(rc)
    rc = padico_shm_block_lfqueue_enqueue(&shm->seg->buffer.free_blocks, block_num);
  if(rc)
    {
      /* lfqueue cannot distinguish between queue full and queue empty with a concurrent
       * dequeue; we know free_blocks cannot be full, so retry. */
      goto retry;
    }
}

/* ********************************************************* */
/* ** messaging */

static inline int padico_shm_short_send_commit(struct padico_shm_s*shm, struct padico_shm_node_s*dest, int block_num, size_t size, int shared)
{
  void*block = padico_shm_block_get_ptr(shm, block_num);
  struct padico_shm_short_header_s*shm_header = block;
  shm_header->from = shm->rank;
  shm_header->size = size;
  int rc = -1, retry = 0;
  while(rc)
    {
      if(shared)
        rc = padico_shm_short_lfqueue_enqueue(&dest->mailbox.shared_queue, block_num);
      else
        rc = padico_shm_short_lfqueue_enqueue(&dest->mailbox.short_queues[shm->rank], block_num);
      if(rc)
        {
          retry++;
          if(retry > 10)
            {
              /* queue may not be actually full, rc=-1 may be a memory consistency
               * artefact. Retry immediately, and issue a warning and reduce
               * throttle only in case error persists.
               */
              puk_usleep(1);
            }
          if(retry > 11)
            {
              return rc;
            }
        }
    }
  return rc;
}
static inline void*padico_shm_short_get_ptr(struct padico_shm_s*shm, int block_num)
{
  assert(block_num != -1);
  void*block = padico_shm_block_get_ptr(shm, block_num);
  return block + sizeof(struct padico_shm_short_header_s);
}
static inline int padico_shm_short_recv_poll(struct padico_shm_s*shm, struct padico_shm_node_s*from)
{
  /* from == NULL => shared queue */
  if(from == NULL)
    {
      int block_num = padico_shm_short_lfqueue_dequeue(&shm->self->mailbox.shared_queue);
      return block_num;
    }
  else
    {
      assert(from->rank >= 0);
      int block_num = padico_shm_short_lfqueue_dequeue(&shm->self->mailbox.short_queues[from->rank]);
      return block_num;
    }
}


/* ********************************************************* */

#endif /* PADICO_SHM_H */

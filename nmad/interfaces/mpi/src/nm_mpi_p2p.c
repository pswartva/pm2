/*
 * NewMadeleine
 * Copyright (C) 2014-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include "nm_mpi_private.h"
#include "nm_mpi_private_syms.h"
#include <assert.h>
#include <errno.h>

#include <Padico/Module.h>
PADICO_MODULE_HOOK(MadMPI);


/* ********************************************************* */

NM_MPI_ALIAS(MPI_Send,               mpi_send);
NM_MPI_ALIAS(MPI_Send_c,             mpi_send_c);
NM_MPI_ALIAS(MPI_Isend,              mpi_isend);
NM_MPI_ALIAS(MPI_Isend_c,            mpi_isend_c);
NM_MPI_ALIAS(MPI_Issend,             mpi_issend);
NM_MPI_ALIAS(MPI_Issend_c,           mpi_issend_c);
NM_MPI_ALIAS(MPI_Bsend,              mpi_bsend);
NM_MPI_ALIAS(MPI_Bsend_c,            mpi_bsend_c);
NM_MPI_ALIAS(MPI_Irsend,             mpi_irsend);
NM_MPI_ALIAS(MPI_Irsend_c,           mpi_irsend_c);
NM_MPI_ALIAS(MPI_Rsend,              mpi_rsend);
NM_MPI_ALIAS(MPI_Rsend_c,            mpi_rsend_c);
NM_MPI_ALIAS(MPI_Ssend,              mpi_ssend);
NM_MPI_ALIAS(MPI_Ssend_c,            mpi_ssend_c);
NM_MPI_ALIAS(MPI_Recv,               mpi_recv);
NM_MPI_ALIAS(MPI_Recv_c,             mpi_recv_c);
NM_MPI_ALIAS(MPI_Irecv,              mpi_irecv);
NM_MPI_ALIAS(MPI_Irecv_c,            mpi_irecv_c);
NM_MPI_ALIAS(MPI_Mrecv,              mpi_mrecv);
NM_MPI_ALIAS(MPI_Mrecv_c,            mpi_mrecv_c);
NM_MPI_ALIAS(MPI_Imrecv,             mpi_imrecv);
NM_MPI_ALIAS(MPI_Imrecv_c,           mpi_imrecv_c);
NM_MPI_ALIAS(MPI_Sendrecv,           mpi_sendrecv);
NM_MPI_ALIAS(MPI_Sendrecv_c,         mpi_sendrecv_c);
NM_MPI_ALIAS(MPI_Sendrecv_replace,   mpi_sendrecv_replace);
NM_MPI_ALIAS(MPI_Sendrecv_replace_c, mpi_sendrecv_replace_c);
NM_MPI_ALIAS(MPI_Iprobe,             mpi_iprobe);
NM_MPI_ALIAS(MPI_Probe,              mpi_probe);
NM_MPI_ALIAS(MPI_Improbe,            mpi_improbe);
NM_MPI_ALIAS(MPI_Mprobe,             mpi_mprobe);
NM_MPI_ALIAS(MPI_Send_init,        mpi_send_init);
NM_MPI_ALIAS(MPI_Rsend_init,       mpi_rsend_init);
NM_MPI_ALIAS(MPI_Ssend_init,       mpi_ssend_init);
NM_MPI_ALIAS(MPI_Recv_init,        mpi_recv_init);
NM_MPI_ALIAS(MPI_Psend_init,         mpi_psend_init);
NM_MPI_ALIAS(MPI_Precv_init,         mpi_precv_init);
NM_MPI_ALIAS(MPI_Pready,             mpi_pready);
NM_MPI_ALIAS(MPI_Pready_range,       mpi_pready_range);
NM_MPI_ALIAS(MPI_Pready_list,        mpi_pready_list);
NM_MPI_ALIAS(MPI_Parrived,           mpi_parrived);

/* ********************************************************* */

/** get the nmad tag for the given user tag and communicator. */
static inline void nm_mpi_get_tag(nm_mpi_communicator_t*p_comm, int user_tag, nm_tag_t*nm_tag, nm_tag_t*tag_mask)
{
  if(user_tag == MPI_ANY_TAG)
    {
      *nm_tag   = 0;
      *tag_mask = NM_MPI_TAG_PRIVATE_MASK; /* mask out private tags */
    }
  else
    {
      *nm_tag  = user_tag;
      *tag_mask = NM_TAG_MASK_FULL;
    }
}

__PUK_SYM_INTERNAL
int nm_mpi_isend_init(nm_mpi_request_t*p_req, int dest, nm_mpi_communicator_t*p_comm)
{
  int err = MPI_SUCCESS;
  assert(p_req->request_type & NM_MPI_REQUEST_SEND);
  if(!p_req->p_datatype->committed)
    {
      NM_MPI_WARNING("trying to send with a non-committed datatype.\n");
      return NM_MPI_ERROR_INTERNAL(MPI_ERR_TYPE);
    }
  if(dest == MPI_PROC_NULL)
    {
      p_req->gate = NULL;
    }
  else
    {
      nm_gate_t p_gate = nm_mpi_communicator_get_gate(p_comm, dest);
      if(p_gate == NULL)
        {
          NM_MPI_WARNING("Cannot find rank %d in comm %p.\n", dest, p_comm);
          return NM_MPI_ERROR_INTERNAL(MPI_ERR_RANK);
        }
      p_req->gate = p_gate;
    }
  return NM_MPI_ERROR_INTERNAL(err);
}

__PUK_SYM_INTERNAL
int nm_mpi_isend_start(nm_mpi_request_t*p_req)
{
  int err = MPI_SUCCESS;
  if(p_req->gate != NULL) /* don't send in case of MPI_PROC_NULL */
    {
      nm_tag_t nm_tag, tag_mask;
      nm_mpi_get_tag(p_req->p_comm, p_req->user_tag, &nm_tag, &tag_mask);
      nm_session_t p_session = nm_mpi_communicator_get_session(p_req->p_comm);
      struct nm_data_s data;
      nm_mpi_data_build(&data, (void*)p_req->sbuf, p_req->p_datatype, p_req->count);
      nm_sr_send_init(p_session, &p_req->request_nmad);
      nm_sr_send_pack_data(p_session, &p_req->request_nmad, &data);
      switch(p_req->request_type)
        {
        case NM_MPI_REQUEST_SEND:
          err = nm_sr_send_isend(p_session, &p_req->request_nmad, p_req->gate, nm_tag);
          break;
        case NM_MPI_REQUEST_RSEND:
          err = nm_sr_send_rsend(p_session, &p_req->request_nmad, p_req->gate, nm_tag);
          break;
        case NM_MPI_REQUEST_SSEND:
          err = nm_sr_send_issend(p_session, &p_req->request_nmad, p_req->gate, nm_tag);
          break;
        case NM_MPI_REQUEST_PSEND:
          /* only set dest for now; actual submit will be done in MPI_Pready() */
          err = nm_sr_send_dest(p_session, &p_req->request_nmad, p_req->gate, nm_tag);
          break;
        default:
          NM_MPI_WARNING("madmpi: unkown type 0x%x for isend\n", p_req->request_type);
          return NM_MPI_ERROR_INTERNAL(MPI_ERR_REQUEST);
          break;
        }
    }
  p_req->request_error = err;
  return NM_MPI_ERROR_INTERNAL(err);
}

/** send chunks from a partitioned request */
__PUK_SYM_INTERNAL
int nm_mpi_isend_chunks(nm_mpi_request_t*p_req, int n, const struct nm_chunk_s*p_chunks)
{
  int err = MPI_SUCCESS;
  assert(p_req->request_type & NM_MPI_REQUEST_SEND);
  assert(p_req->request_type & NM_MPI_REQUEST_PARTITIONED);
  if(p_req->gate != NULL)
    {
      nm_session_t p_session = nm_mpi_communicator_get_session(p_req->p_comm);
      err = nm_sr_send_submit_chunks(p_session, &p_req->request_nmad, n, p_chunks);
    }
  p_req->request_error = err;
  return NM_MPI_ERROR_INTERNAL(err);
}

__PUK_SYM_INTERNAL
int nm_mpi_isend(nm_mpi_request_t*p_req, int dest, nm_mpi_communicator_t*p_comm)
{
  int err;
  err = nm_mpi_isend_init(p_req, dest, p_comm);
  if(err == MPI_SUCCESS)
    {
      err = nm_mpi_isend_start(p_req);
    }
  return NM_MPI_ERROR_INTERNAL(err);
}

__PUK_SYM_INTERNAL
int nm_mpi_irecv_init(nm_mpi_request_t*p_req, int source, nm_mpi_communicator_t*p_comm)
{
  assert(p_req->request_type & NM_MPI_REQUEST_RECV);
  if(source == MPI_ANY_SOURCE || source == MPI_PROC_NULL)
    {
      p_req->gate = NM_ANY_GATE;
    }
  else
    {
      p_req->gate = nm_mpi_communicator_get_gate(p_comm, source);
      if(p_req->gate == NULL)
        {
          NM_MPI_WARNING("Cannot find rank %d in comm %p\n", source, p_comm);
          return NM_MPI_ERROR_INTERNAL(MPI_ERR_RANK);
        }
    }
  p_req->request_source = source;
  return MPI_SUCCESS;
}


__PUK_SYM_INTERNAL
int nm_mpi_irecv_start(nm_mpi_request_t*p_req)
{
  if(p_req->request_source != MPI_PROC_NULL)  /* don't receive in case of MPI_PROC_NULL */
    {
      assert(p_req->request_type & NM_MPI_REQUEST_RECV);
      nm_tag_t nm_tag, tag_mask;
      nm_mpi_get_tag(p_req->p_comm, p_req->user_tag, &nm_tag, &tag_mask);
      nm_session_t p_session = nm_mpi_communicator_get_session(p_req->p_comm);
      if(!p_req->p_datatype->committed)
        {
          NM_MPI_WARNING("trying to recv with a non-committed datatype.\n");
          return NM_MPI_ERROR_INTERNAL(MPI_ERR_TYPE);
        }
      struct nm_data_s data;
      nm_mpi_data_build(&data, p_req->rbuf, p_req->p_datatype, p_req->count);
      nm_sr_recv_init(p_session, &(p_req->request_nmad));
      nm_sr_recv_unpack_data(p_session, &(p_req->request_nmad), &data);
      if(p_req->request_type & NM_MPI_REQUEST_PARTITIONED)
        {
          nm_core_unpack_partition_set(&p_req->request_nmad.req, p_req->partitioned.partitions);
        }
      nm_sr_recv_irecv(p_session, &(p_req->request_nmad), p_req->gate, nm_tag, tag_mask);
    }

  p_req->request_error = NM_ESUCCESS;
  return NM_MPI_ERROR_INTERNAL(p_req->request_error);
}

__PUK_SYM_INTERNAL
int nm_mpi_irecv(nm_mpi_request_t *p_req, int source, nm_mpi_communicator_t *p_comm)
{
  int err;
  err = nm_mpi_irecv_init(p_req, source, p_comm);
  if(err == MPI_SUCCESS)
    {
      err = nm_mpi_irecv_start(p_req);
    }
  return NM_MPI_ERROR_INTERNAL(err);
}

/* ********************************************************* */

int mpi_isend_c(const void*buffer, MPI_Count count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm, MPI_Request*request)
{
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  NM_MPI_COMMUNICATOR_CHECK(p_comm);
  nm_mpi_datatype_t*p_datatype = nm_mpi_datatype_get(datatype);
  NM_MPI_DATATYPE_CHECK(p_datatype);
  NM_MPI_TAG_CHECK_SEND(tag);
  nm_mpi_request_t *p_req = nm_mpi_request_alloc_send(NM_MPI_REQUEST_SEND, count, buffer, p_datatype, tag, p_comm);
  *request = p_req->id;
  int err = nm_mpi_isend(p_req, dest, p_comm);
  return NM_MPI_ERROR_COMM(p_comm, err);
}

int mpi_isend(const void*buffer, int count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm, MPI_Request*request)
{
  return mpi_isend_c(buffer, count, datatype, dest, tag, comm, request);
}

int mpi_issend_c(const void*buffer, MPI_Count count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm, MPI_Request*request)
{
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  NM_MPI_COMMUNICATOR_CHECK(p_comm);
  nm_mpi_datatype_t*p_datatype = nm_mpi_datatype_get(datatype);
  NM_MPI_DATATYPE_CHECK(p_datatype);
  NM_MPI_TAG_CHECK_SEND(tag);
  nm_mpi_request_t *p_req = nm_mpi_request_alloc_send(NM_MPI_REQUEST_SSEND, count, buffer, p_datatype, tag, p_comm);
  *request = p_req->id;
  int err = nm_mpi_isend(p_req, dest, p_comm);
  return NM_MPI_ERROR_COMM(p_comm, err);
}

int mpi_issend(const void*buffer, int count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm, MPI_Request*request)
{
  return mpi_issend_c(buffer, count, datatype, dest, tag, comm, request);
}

int mpi_irsend_c(const void*buffer, MPI_Count count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm, MPI_Request*request)
{
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  NM_MPI_COMMUNICATOR_CHECK(p_comm);
  nm_mpi_datatype_t*p_datatype = nm_mpi_datatype_get(datatype);
  NM_MPI_DATATYPE_CHECK(p_datatype);
  NM_MPI_TAG_CHECK_SEND(tag);
  nm_mpi_request_t *p_req = nm_mpi_request_alloc_send(NM_MPI_REQUEST_RSEND, count, buffer, p_datatype, tag, p_comm);
  *request = p_req->id;
  int err = nm_mpi_isend(p_req, dest, p_comm);
  return NM_MPI_ERROR_COMM(p_comm, err);
}

int mpi_irsend(const void*buffer, int count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm, MPI_Request*request)
{
  return mpi_irsend_c(buffer, count, datatype, dest, tag, comm, request);
}

int mpi_send_c(const void*buffer, MPI_Count count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm)
{
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  NM_MPI_COMMUNICATOR_CHECK(p_comm);
  MPI_Request request;
  int err = mpi_isend_c(buffer, count, datatype, dest, tag, comm, &request);
  if(err == MPI_SUCCESS)
    {
      err = mpi_wait(&request, MPI_STATUS_IGNORE);
    }
  return NM_MPI_ERROR_COMM(p_comm, err);
}

int mpi_send(const void*buffer, int count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm)
{
  return mpi_send_c(buffer, count, datatype, dest, tag, comm);
}

int mpi_rsend_c(const void*buffer, MPI_Count count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm)
{
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  NM_MPI_COMMUNICATOR_CHECK(p_comm);
  MPI_Request request;
  int err = mpi_irsend_c(buffer, count, datatype, dest, tag, comm, &request);
  if(err == MPI_SUCCESS)
    {
      err = mpi_wait(&request, MPI_STATUS_IGNORE);
    }
  return NM_MPI_ERROR_COMM(p_comm, err);
}

int mpi_rsend(const void*buffer, int count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm)
{
  return mpi_rsend_c(buffer, count, datatype, dest, tag, comm);
}

int mpi_ssend_c(const void*buffer, MPI_Count count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm)
{
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  NM_MPI_COMMUNICATOR_CHECK(p_comm);
  MPI_Request request;
  int err = mpi_issend_c(buffer, count, datatype, dest, tag, comm, &request);
  if(err == MPI_SUCCESS)
    {
      err = mpi_wait(&request, MPI_STATUS_IGNORE);
    }
  return NM_MPI_ERROR_COMM(p_comm, err);
}

int mpi_ssend(const void*buffer, int count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm)
{
  return mpi_ssend_c(buffer, count, datatype, dest, tag, comm);
}

int mpi_bsend_c(const void*buffer, MPI_Count count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm)
{
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  NM_MPI_COMMUNICATOR_CHECK(p_comm);
  /* todo: only valid for small messages ? */
  NM_MPI_WARNING("Warning: MPI_Bsend called. it may be invalid\n");
  int err = mpi_send_c(buffer, count, datatype, dest, tag, comm);
  return NM_MPI_ERROR_COMM(p_comm, err);
}

int mpi_bsend(const void*buffer, int count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm)
{
  return mpi_bsend_c(buffer, count, datatype, dest, tag, comm);
}


int mpi_irecv_c(void*buffer, MPI_Count count, MPI_Datatype datatype, int source, int tag, MPI_Comm comm, MPI_Request*request)
{
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  NM_MPI_COMMUNICATOR_CHECK(p_comm);
  nm_mpi_datatype_t*p_datatype = nm_mpi_datatype_get(datatype);
  NM_MPI_DATATYPE_CHECK(p_datatype);
  NM_MPI_TAG_CHECK_RECV(tag);
  nm_mpi_request_t*p_req = nm_mpi_request_alloc_recv(count, buffer, p_datatype, tag, p_comm);
  int err = nm_mpi_irecv(p_req, source, p_comm);
  *request = p_req->id;
  return NM_MPI_ERROR_COMM(p_comm, err);
}

int mpi_irecv(void*buffer, int count, MPI_Datatype datatype, int source, int tag, MPI_Comm comm, MPI_Request*request)
{
  return mpi_irecv_c(buffer, count, datatype, source, tag, comm, request);
}

int mpi_recv_c(void*buffer, MPI_Count count, MPI_Datatype datatype, int source, int tag, MPI_Comm comm, MPI_Status*status)
{
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  NM_MPI_COMMUNICATOR_CHECK(p_comm);
  MPI_Request request;
  int err = mpi_irecv_c(buffer, count, datatype, source, tag, comm, &request);
  if(err == MPI_SUCCESS)
    {
      err = mpi_wait(&request, status);
    }
  return NM_MPI_ERROR_COMM(p_comm, err);
}

int mpi_recv(void*buffer, int count, MPI_Datatype datatype, int source, int tag, MPI_Comm comm, MPI_Status*status)
{
  return mpi_recv_c(buffer, count, datatype, source, tag, comm, status);
}

int mpi_sendrecv_c(const void*sendbuf, MPI_Count sendcount, MPI_Datatype sendtype, int dest, int sendtag,
                   void*recvbuf, MPI_Count recvcount, MPI_Datatype recvtype, int source, int recvtag,
                   MPI_Comm comm, MPI_Status*status)
{
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  NM_MPI_COMMUNICATOR_CHECK(p_comm);
  int err;
  MPI_Request srequest, rrequest;
  err = mpi_isend_c(sendbuf, sendcount, sendtype, dest, sendtag, comm, &srequest);
  err = mpi_irecv_c(recvbuf, recvcount, recvtype, source, recvtag, comm, &rrequest);
  err = mpi_wait(&srequest, MPI_STATUS_IGNORE);
  err = mpi_wait(&rrequest, status);
  return NM_MPI_ERROR_COMM(p_comm, err);
}

int mpi_sendrecv(const void*sendbuf, int sendcount, MPI_Datatype sendtype, int dest, int sendtag,
                 void*recvbuf, int recvcount, MPI_Datatype recvtype, int source, int recvtag,
                 MPI_Comm comm, MPI_Status*status)
{
  return mpi_sendrecv_c(sendbuf, sendcount, sendtype, dest, sendtag, recvbuf, recvcount, recvtype, source, recvtag, comm, status);
}

int mpi_sendrecv_replace_c(void*buf, MPI_Count count, MPI_Datatype datatype, int dest, int sendtag, int source, int recvtag,
                         MPI_Comm comm, MPI_Status*status)
{
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  NM_MPI_COMMUNICATOR_CHECK(p_comm);
  int err;
  MPI_Request srequest;
  err = mpi_isend_c(buf, count, datatype, dest, sendtag, comm, &srequest);
  int flag = 0;
  err = mpi_test(&srequest, &flag, MPI_STATUS_IGNORE);
  if(flag)
    {
      /* send completed immediately- reuse buffer */
      err = mpi_recv_c(buf, count, datatype, source, recvtag, comm, status);
    }
  else
    {
      /* send is pending- use tmp buffer */
      MPI_Request rrequest;
      MPI_Count size = 0;
      mpi_pack_size_c(count, datatype, comm, &size);
      void*tmpbuf = padico_malloc(size);
      err = mpi_irecv_c(tmpbuf, size, MPI_PACKED, source, recvtag, comm, &rrequest);
      err = mpi_wait(&srequest, MPI_STATUS_IGNORE);
      err = mpi_wait(&rrequest, status);
      MPI_Count position = 0;
      mpi_unpack_c(tmpbuf, size, &position, buf, count, datatype, comm);
      padico_free(tmpbuf);
    }
  return NM_MPI_ERROR_COMM(p_comm, err);
}

int mpi_sendrecv_replace(void*buf, int count, MPI_Datatype datatype, int dest, int sendtag, int source, int recvtag,
                         MPI_Comm comm, MPI_Status*status)
{
  return mpi_sendrecv_replace_c(buf, count, datatype, dest, sendtag, source, recvtag, comm, status);
}

int mpi_iprobe(int source, int tag, MPI_Comm comm, int*flag, MPI_Status*status)
{
  nm_gate_t gate = NULL;
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  NM_MPI_COMMUNICATOR_CHECK(p_comm);
  if(source == MPI_ANY_SOURCE)
    {
      gate = NM_ANY_GATE;
    }
  else
    {
      gate = nm_mpi_communicator_get_gate(p_comm, source);
      if(gate == NULL)
        {
          return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_RANK);
        }
    }
  nm_tag_t nm_tag, tag_mask, out_tag;
  nm_mpi_get_tag(p_comm, tag, &nm_tag, &tag_mask);
  nm_gate_t out_gate = NULL;
  nm_len_t out_len = -1;
  int rc = nm_sr_probe(nm_mpi_communicator_get_session(p_comm), gate, &out_gate, nm_tag, tag_mask, &out_tag, &out_len);;
  if(rc == NM_ESUCCESS)
    {
      *flag = 1;
      if(status != NULL)
        {
          status->MPI_TAG = out_tag;
          status->MPI_SOURCE = nm_mpi_communicator_get_dest(p_comm, out_gate);
          status->size = out_len;
        }
      return MPI_SUCCESS;
    }
  else if(rc == -NM_EAGAIN)
    {
      *flag = 0;
      return MPI_SUCCESS;
    }
  return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_OTHER);
}

int mpi_probe(int source, int tag, MPI_Comm comm, MPI_Status*status)
{
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  NM_MPI_COMMUNICATOR_CHECK(p_comm);
  int flag = 0;
  int err = MPI_ERR_UNKNOWN;
  do
    {
      err = mpi_iprobe(source, tag, comm, &flag, status);
    }
  while(flag != 1);
  return NM_MPI_ERROR_COMM(p_comm, err);
}

int mpi_send_init_c(const void*buffer, MPI_Count count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm, MPI_Request*request)
{
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  NM_MPI_COMMUNICATOR_CHECK(p_comm);
  nm_mpi_datatype_t*p_datatype = nm_mpi_datatype_get(datatype);
  NM_MPI_DATATYPE_CHECK(p_datatype);
  NM_MPI_TAG_CHECK_SEND(tag);
  nm_mpi_request_t *p_req = nm_mpi_request_alloc_send(NM_MPI_REQUEST_SEND, count, buffer, p_datatype, tag, p_comm);
  p_req->status |= NM_MPI_REQUEST_PERSISTENT;
  int err = nm_mpi_isend_init(p_req, dest, p_comm);
  *request = p_req->id;
  return NM_MPI_ERROR_COMM(p_comm, err);
}

int mpi_send_init(const void*buffer, int count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm, MPI_Request*request)
{
  return mpi_send_init_c(buffer, count, datatype, dest, tag, comm, request);
}

int mpi_rsend_init_c(const void*buffer, MPI_Count count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm, MPI_Request*request)
{
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  NM_MPI_COMMUNICATOR_CHECK(p_comm);
  nm_mpi_datatype_t*p_datatype = nm_mpi_datatype_get(datatype);
  NM_MPI_DATATYPE_CHECK(p_datatype);
  NM_MPI_TAG_CHECK_SEND(tag);
  nm_mpi_request_t *p_req = nm_mpi_request_alloc_send(NM_MPI_REQUEST_RSEND, count, buffer, p_datatype, tag, p_comm);
  p_req->status |= NM_MPI_REQUEST_PERSISTENT;
  int err = nm_mpi_isend_init(p_req, dest, p_comm);
  *request = p_req->id;
  return NM_MPI_ERROR_COMM(p_comm, err);
}

int mpi_rsend_init(const void*buffer, int count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm, MPI_Request*request)
{
  return mpi_rsend_init_c(buffer, count, datatype, dest, tag, comm, request);
}

int mpi_ssend_init_c(const void*buffer, MPI_Count count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm, MPI_Request*request)
{
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  NM_MPI_COMMUNICATOR_CHECK(p_comm);
  nm_mpi_datatype_t*p_datatype = nm_mpi_datatype_get(datatype);
  NM_MPI_DATATYPE_CHECK(p_datatype);
  NM_MPI_TAG_CHECK_SEND(tag);
  nm_mpi_request_t *p_req = nm_mpi_request_alloc_send(NM_MPI_REQUEST_SSEND, count, buffer, p_datatype, tag, p_comm);
  p_req->status |= NM_MPI_REQUEST_PERSISTENT;
  int err = nm_mpi_isend_init(p_req, dest, p_comm);
  *request = p_req->id;
  return NM_MPI_ERROR_COMM(p_comm, err);
}

int mpi_ssend_init(const void*buffer, int count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm, MPI_Request*request)
{
  return mpi_ssend_init_c(buffer, count, datatype, dest, tag, comm, request);
}

int mpi_recv_init_c(void*buffer, MPI_Count count, MPI_Datatype datatype, int source, int tag, MPI_Comm comm, MPI_Request*request)
{
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  NM_MPI_COMMUNICATOR_CHECK(p_comm);
  nm_mpi_datatype_t*p_datatype = nm_mpi_datatype_get(datatype);
  NM_MPI_DATATYPE_CHECK(p_datatype);
  NM_MPI_TAG_CHECK_RECV(tag);
  nm_mpi_request_t*p_req = nm_mpi_request_alloc_recv(count, buffer, p_datatype, tag, p_comm);
  p_req->status |= NM_MPI_REQUEST_PERSISTENT;
  int err = nm_mpi_irecv_init(p_req, source, p_comm);
  *request = p_req->id;
  return NM_MPI_ERROR_COMM(p_comm, err);
}

int mpi_recv_init(void*buffer, int count, MPI_Datatype datatype, int source, int tag, MPI_Comm comm, MPI_Request*request)
{
  return mpi_recv_init_c(buffer, count, datatype, source, tag, comm, request);
}

int mpi_improbe(int source, int tag, MPI_Comm comm, int*flag, MPI_Message*message, MPI_Status*status)
{
  int err = MPI_SUCCESS;
  nm_gate_t p_gate = NULL;
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  NM_MPI_COMMUNICATOR_CHECK(p_comm);
  if(source == MPI_ANY_SOURCE)
    {
      p_gate = NM_ANY_GATE;
    }
  else
    {
      p_gate = nm_mpi_communicator_get_gate(p_comm, source);
      if(p_gate == NULL)
        {
          return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_RANK);
        }
    }
  if((tag != MPI_ANY_TAG) && (tag < 0 || tag > NM_MPI_TAG_MAX))
    {
      return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_TAG);
    }
  nm_tag_t nm_tag, tag_mask;
  nm_mpi_get_tag(p_comm, tag, &nm_tag, &tag_mask);
  nm_session_t p_session = nm_mpi_communicator_get_session(p_comm);
  nm_mpi_request_t*p_req = nm_mpi_request_alloc_recv(0, NULL, NULL, nm_tag, p_comm);
  err = nm_mpi_irecv_init(p_req, source, p_comm);
  if(err != MPI_SUCCESS)
    return NM_MPI_ERROR_COMM(p_comm, err);
  p_req->request_type = NM_MPI_REQUEST_PROBE;
  nm_sr_recv_init(p_session, &p_req->request_nmad);
  nm_sr_recv_match(p_session, &p_req->request_nmad, p_gate, nm_tag, tag_mask);
  int rc = nm_sr_recv_iprobe(p_session, &p_req->request_nmad);
  if(rc == NM_ESUCCESS)
    {
      /* post the request if probe was successfull, so as to be in the same state
       * as the blocking mpi_mprobe() for consistency upon entry in the next mpi_mrecv() */
      nm_sr_recv_post(p_session, &p_req->request_nmad);
      if(status != NULL)
        {
          nm_tag_t out_tag = nm_sr_request_get_tag(&p_req->request_nmad);
          nm_gate_t out_gate = nm_sr_request_get_gate(&p_req->request_nmad);
          nm_len_t out_len;
          nm_sr_request_get_expected_size(&p_req->request_nmad, &out_len);
          assert(out_len != NM_LEN_UNDEFINED);
          status->MPI_TAG = out_tag;
          status->MPI_SOURCE = nm_mpi_communicator_get_dest(p_comm, out_gate);
          status->size = out_len;
          status->MPI_ERROR = MPI_SUCCESS;
          status->cancelled = 0;
        }
      *flag = 1;
      *message = p_req->id;
      err = MPI_SUCCESS;
    }
  else
    {
      err = MPI_SUCCESS;
      nm_mpi_request_free(p_req);
      *flag = 0;
    }
  return NM_MPI_ERROR_COMM(p_comm, err);
}

int mpi_mprobe(int source, int tag, MPI_Comm comm, MPI_Message*message, MPI_Status*status)
{
  int err = MPI_SUCCESS;
  nm_gate_t p_gate = NULL;
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  NM_MPI_COMMUNICATOR_CHECK(p_comm);
  if(source == MPI_ANY_SOURCE)
    {
      p_gate = NM_ANY_GATE;
    }
  else
    {
      p_gate = nm_mpi_communicator_get_gate(p_comm, source);
      if(p_gate == NULL)
        {
          return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_RANK);
        }
    }
  if((tag != MPI_ANY_TAG) && (tag < 0 || tag > NM_MPI_TAG_MAX))
    {
      return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_TAG);
    }
  nm_tag_t nm_tag, tag_mask;
  nm_mpi_get_tag(p_comm, tag, &nm_tag, &tag_mask);
  nm_session_t p_session = nm_mpi_communicator_get_session(p_comm);
  nm_mpi_request_t*p_req = nm_mpi_request_alloc_recv(0, NULL, NULL, nm_tag, p_comm);
  err = nm_mpi_irecv_init(p_req, source, p_comm);
  if(err != MPI_SUCCESS)
    return NM_MPI_ERROR_COMM(p_comm, err);
  p_req->request_type = NM_MPI_REQUEST_PROBE;
  nm_sr_recv_init(p_session, &p_req->request_nmad);
  nm_sr_recv_match(p_session, &p_req->request_nmad, p_gate, nm_tag, tag_mask);
  nm_sr_recv_post(p_session, &p_req->request_nmad);
  nm_sr_recv_data_size_wait(p_session, &p_req->request_nmad);
  err = MPI_SUCCESS;
  *message = p_req->id;
  if(status != NULL)
    {
      nm_tag_t out_tag = nm_sr_request_get_tag(&p_req->request_nmad);
      nm_gate_t out_gate = nm_sr_request_get_gate(&p_req->request_nmad);
      nm_len_t out_len;
      nm_sr_request_get_expected_size(&p_req->request_nmad, &out_len);
      assert(out_len != NM_LEN_UNDEFINED);
      status->MPI_TAG = out_tag;
      status->MPI_SOURCE = nm_mpi_communicator_get_dest(p_comm, out_gate);
      status->size = out_len;
      status->MPI_ERROR = MPI_SUCCESS;
      status->cancelled = 0;
    }
  return NM_MPI_ERROR_COMM(p_comm, err);
}

int mpi_imrecv_c(void*buf, MPI_Count count, MPI_Datatype datatype, MPI_Message*message, MPI_Request*request)
{
  nm_mpi_request_t*p_req = nm_mpi_request_get(*message);
  if(p_req == NULL)
    {
      return NM_MPI_ERROR_WORLD(MPI_ERR_REQUEST);
    }
  nm_mpi_communicator_t*p_comm = p_req->p_comm;
  NM_MPI_COMMUNICATOR_CHECK(p_comm);
  /* promote PROBE request to full RECV request */
  assert(p_req->request_type == NM_MPI_REQUEST_PROBE);
  nm_mpi_datatype_t*p_datatype = nm_mpi_datatype_get(datatype);
  NM_MPI_DATATYPE_CHECK(p_datatype);
  p_req->count = count;
  p_req->rbuf  = buf;
  nm_mpi_request_set_datatype(p_req, p_datatype);
  p_req->request_type = NM_MPI_REQUEST_RECV;
  p_req->request_error = NM_ESUCCESS;
  nm_session_t p_session = nm_mpi_communicator_get_session(p_req->p_comm);
  struct nm_data_s data;
  nm_mpi_data_build(&data, p_req->rbuf, p_req->p_datatype, p_req->count);
  nm_sr_recv_unpack_data(p_session, &p_req->request_nmad, &data);

  *request = p_req->id;
  *message = MPI_MESSAGE_NULL;
  return MPI_SUCCESS;
}

int mpi_imrecv(void*buf, int count, MPI_Datatype datatype, MPI_Message*message, MPI_Request*request)
{
  return mpi_imrecv_c(buf, count, datatype, message, request);
}

int mpi_mrecv_c(void*buf, MPI_Count count, MPI_Datatype datatype, MPI_Message*message, MPI_Status*status)
{
  MPI_Request request;
  int err = mpi_imrecv_c(buf, count, datatype, message, &request);
  if(err == MPI_SUCCESS)
    {
      err = mpi_wait(&request, status);
    }
  return NM_MPI_ERROR_WORLD(err);
}

int mpi_mrecv(void*buf, int count, MPI_Datatype datatype, MPI_Message*message, MPI_Status*status)
{
  return mpi_mrecv_c(buf, count, datatype, message, status);
}

/* ** Partitioned communication **************************** */

int mpi_psend_init(const void*buf, int partitions, MPI_Count count,
                   MPI_Datatype datatype, int dest, int tag, MPI_Comm comm,
                   MPI_Info info, MPI_Request*request)
{
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  NM_MPI_COMMUNICATOR_CHECK(p_comm);
  nm_mpi_datatype_t*p_datatype = nm_mpi_datatype_get(datatype);
  NM_MPI_DATATYPE_CHECK(p_datatype);
  NM_MPI_TAG_CHECK_SEND(tag);
  /* for now, aggregate all partitions into a single message */
  nm_mpi_request_t*p_req = nm_mpi_request_alloc_send(NM_MPI_REQUEST_SEND, count * partitions, buf, p_datatype, tag, p_comm);
  p_req->status |= NM_MPI_REQUEST_PERSISTENT;
  p_req->request_type |= NM_MPI_REQUEST_PARTITIONED;
  p_req->partitioned.partitions = partitions;
  int err = nm_mpi_isend_init(p_req, dest, p_comm);
  *request = p_req->id;
  return NM_MPI_ERROR_COMM(p_comm, err);
}
int mpi_precv_init(void*buf, int partitions, MPI_Count count,
                   MPI_Datatype datatype, int source, int tag, MPI_Comm comm,
                   MPI_Info info, MPI_Request*request)
{
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  NM_MPI_COMMUNICATOR_CHECK(p_comm);
  nm_mpi_datatype_t*p_datatype = nm_mpi_datatype_get(datatype);
  NM_MPI_DATATYPE_CHECK(p_datatype);
  NM_MPI_TAG_CHECK_RECV(tag);
  /* for now, aggregate all partitions into a single message */
  nm_mpi_request_t*p_req = nm_mpi_request_alloc_recv(count * partitions, buf, p_datatype, tag, p_comm);
  p_req->status |= NM_MPI_REQUEST_PERSISTENT;
  p_req->request_type |= NM_MPI_REQUEST_PARTITIONED;
  p_req->partitioned.partitions = partitions;
  int err = nm_mpi_irecv_init(p_req, source, p_comm);
  *request = p_req->id;
  return NM_MPI_ERROR_COMM(p_comm, err);
}

int mpi_pready(int partition, MPI_Request request)
{
  nm_mpi_request_t*p_req = nm_mpi_request_get(request);
  if(p_req == NULL)
    {
      return NM_MPI_ERROR_WORLD(MPI_ERR_REQUEST);
    }
  if(!(p_req->request_type & NM_MPI_REQUEST_PARTITIONED))
    {
      return NM_MPI_ERROR_WORLD(MPI_ERR_REQUEST);
    }
  assert(p_req->count % p_req->partitioned.partitions == 0);
  const size_t partition_size = (p_req->count / p_req->partitioned.partitions) * nm_mpi_datatype_size(p_req->p_datatype);
  const struct nm_chunk_s chunk =
    {
      .chunk_offset = partition * partition_size,
      .chunk_len = partition_size
    };
  if((p_req->count * nm_mpi_datatype_size(p_req->p_datatype) != 0) || (partition == 0))
    {
      /* don't partition 0-length requests, since multiple chunks on
       * range 0-0 would create overlapping chunks which is prohibited by nmad
       */
      nm_mpi_isend_chunks(p_req, 1, &chunk);
    }
  return MPI_SUCCESS;
}

int mpi_pready_range(int partition_low, int partition_high, MPI_Request request)
{
  nm_mpi_request_t*p_req = nm_mpi_request_get(request);
  if(p_req == NULL)
    {
      return NM_MPI_ERROR_WORLD(MPI_ERR_REQUEST);
    }
  if(!(p_req->request_type & NM_MPI_REQUEST_PARTITIONED))
    {
      return NM_MPI_ERROR_WORLD(MPI_ERR_REQUEST);
    }
  assert(p_req->count % p_req->partitioned.partitions == 0);
  const size_t partition_size = (p_req->count / p_req->partitioned.partitions) * nm_mpi_datatype_size(p_req->p_datatype);
  const struct nm_chunk_s chunk =
    {
      .chunk_offset = partition_low * partition_size,
      .chunk_len = partition_size * (partition_high - partition_low + 1)
    };
  nm_mpi_isend_chunks(p_req, 1, &chunk);
  return MPI_SUCCESS;
}

int mpi_pready_list(int length, const int array_of_partitions[], MPI_Request request)
{
  nm_mpi_request_t*p_req = nm_mpi_request_get(request);
  if(p_req == NULL)
    {
      return NM_MPI_ERROR_WORLD(MPI_ERR_REQUEST);
    }
  if(!(p_req->request_type & NM_MPI_REQUEST_PARTITIONED))
    {
      return NM_MPI_ERROR_WORLD(MPI_ERR_REQUEST);
    }
  assert(p_req->count % p_req->partitioned.partitions == 0);
  const size_t partition_size = (p_req->count / p_req->partitioned.partitions) * nm_mpi_datatype_size(p_req->p_datatype);
  struct nm_chunk_s chunks[length];
  int i;
  for(i = 0; i < length; i++)
    {
      chunks[i].chunk_offset = array_of_partitions[i] * partition_size;
      chunks[i].chunk_len = partition_size;
    }
  nm_mpi_isend_chunks(p_req, length, chunks);
  return MPI_SUCCESS;
}

int mpi_parrived(MPI_Request request, int partition, int*flag)
{
  nm_mpi_request_t*p_req = nm_mpi_request_get(request);
  if(p_req == NULL)
    {
      return NM_MPI_ERROR_WORLD(MPI_ERR_REQUEST);
    }
  if(!(p_req->request_type & NM_MPI_REQUEST_PARTITIONED))
    {
      return NM_MPI_ERROR_WORLD(MPI_ERR_REQUEST);
    }
  int rc = nm_core_unpack_partition_test(&p_req->request_nmad.req, partition);
  if(rc == NM_ESUCCESS)
    {
      *flag = 1;
      return MPI_SUCCESS;
    }
  else if(rc == -NM_EAGAIN)
    {
      nm_core_t p_core = nm_core_get_singleton();
      nm_schedule(p_core);
      *flag = 0;
      return MPI_SUCCESS;
    }
  else
    {
      NM_MPI_WARNING("Unexpected rc = %d in MPI_Parrived().\n", rc);
      return NM_MPI_ERROR_WORLD(MPI_ERR_OTHER);
    }
  return MPI_SUCCESS;
}

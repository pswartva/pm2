/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <nm_private.h>

PADICO_MODULE_HOOK(nmad);

PADICO_MODULE_ATTR(nuioa_enable, "NMAD_NUIOA_ENABLE", "enable automatic process binding to the NUMA node near the network board (default: off)", bool, 0);

/** Load and initialize a driver with a parameter,
 * and applying numa binding in-between.
 */
int nm_core_driver_load_init(nm_core_t p_core, puk_component_t driver_component, nm_trk_kind_t kind,
                             nm_drv_t *pp_drv, const char**p_url)
{
  /* ** allocate & init nm_drv */
  nm_drv_t p_drv = nm_drv_new();
  assert(driver_component != NULL);
  p_drv->driver_id = padico_strdup(driver_component->name);
  p_drv->p_core    = p_core;
  p_drv->assembly  = driver_component;
  p_drv->driver    = puk_component_get_driver_NewMad_minidriver(p_drv->assembly, NULL);
  p_drv->minidriver_context = puk_component_get_context(driver_component, puk_iface_NewMad_minidriver(), NULL);
  p_drv->p_pw_recv_any = NULL;
  p_drv->props.capabilities = (struct nm_minidriver_capabilities_s){ 0 };
  p_drv->props.hints.kind = kind;
  p_drv->url       = NULL;
#ifdef NMAD_PROFILE
  puk_profile_var_defx(unsigned_long_long, counter, &p_drv->profiling.n_recv_packets, 0,
                       "nm_drv", "number of packets received by the driver",
                       "nm_drv.%s.n_recv_packets", p_drv->driver_id);
  puk_profile_var_defx(unsigned_long_long, aggregate, &p_drv->profiling.n_recv_bytes, 0,
                       "nm_drv", "cumulative number of bytes received by the driver",
                       "nm_drv.%s.n_recv_bytes", p_drv->driver_id);
  puk_profile_var_defx(unsigned_long_long, counter, &p_drv->profiling.n_send_packets, 0,
                       "nm_drv", "number of packets sent by the driver",
                       "nm_drv.%s.n_send_packets", p_drv->driver_id);
  puk_profile_var_defx(unsigned_long_long, aggregate, &p_drv->profiling.n_send_bytes, 0,
                       "nm_drv", "cumultative number of bytes sent by the driver",
                       "nm_drv.%s.n_send_bytes", p_drv->driver_id);
  puk_profile_var_defx(unsigned_long_long, counter, &p_drv->profiling.n_send_prefetch, 0,
                       "nm_drv", "number of sender-side packets which were prefetched by the driver",
                       "nm_drv.%s.n_send_prefetch", p_drv->driver_id);
  puk_profile_var_defx(unsigned_long_long, counter, &p_drv->profiling.n_send_unfetch, 0,
                       "nm_drv", "number of sender-side packets which were unfetched by the driver",
                       "nm_drv.%s.n_send_unfetch", p_drv->driver_id);
  puk_profile_var_defx(unsigned_long_long, counter, &p_drv->profiling.n_recv_prefetch, 0,
                       "nm_drv", "number of receiver-side packets which were prefetched by the driver",
                       "nm_drv.%s.n_recv_prefetch", p_drv->driver_id);
  puk_profile_var_defx(unsigned_long_long, aggregate, &p_drv->profiling.n_recv_prefetch_bytes, 0,
                       "nm_drv", "cumulative number of bytes of receiver-side packets which were prefetched by the driver",
                       "nm_drv.%s.n_recv_prefetch_bytes", p_drv->driver_id);
  puk_profile_var_defx(unsigned_long_long, counter, &p_drv->profiling.n_recv_unfetch, 0,
                       "nm_drv", "number of receiver-side packets which were unfetched by the driver",
                       "nm_drv.%s.n_recv_unfetch", p_drv->driver_id);
  puk_profile_var_defx(double, timer, &p_drv->profiling.total_send_post_usecs, 0.0,
                       "nm_drv", "total time spent in the driver function to post an outgoing packet",
                       "nm_drv.%s.total_send_post_usecs", p_drv->driver_id);
  puk_profile_var_defx(double, timer, &p_drv->profiling.total_send_usecs, 0.0,
                       "nm_drv", "total time spent in the driver function to send an outgoing packet",
                       "nm_drv.%s.total_send_usecs", p_drv->driver_id);
  puk_profile_var_defx(double, timer, &p_drv->profiling.total_send_prefetch_usecs, 0.0,
                       "nm_drv", "total time spent in the driver send prefetch function",
                       "nm_drv.%s.total_send_prefetch_usecs", p_drv->driver_id);
  puk_profile_var_defx(double, timer, &p_drv->profiling.total_recv_prefetch_usecs, 0.0,
                       "nm_drv", "total time spent in the driver recv prefetch function",
                       "nm_drv.%s.total_recv_prefetch_usecs", p_drv->driver_id);
  puk_profile_var_defx(double, timer, &p_drv->profiling.average_send_bw_Mbps, 0.0,
                       "nm_drv", "average bandwidth to send outgoing packets",
                       "nm_drv.%s.average_send_bw_Mbps", p_drv->driver_id);
  puk_profile_var_defx(double, timer, &p_drv->profiling.average_send_prefetch_bw_Mbps, 0.0,
                       "nm_drv","average bandwidth for send prefetch",
                       "nm_drv.%s.average_send_prefetch_bw_Mbps", p_drv->driver_id);
  puk_profile_var_defx(double, timer, &p_drv->profiling.average_recv_prefetch_bw_Mbps, 0.0,
                       "nm_drv","average bandwidth for recv prefetch",
                       "nm_drv.%s.average_recv_prefetch_bw_Mbps", p_drv->driver_id);
#endif /* NMAD_PROFILE */
#ifdef NMAD_HWLOC
  p_drv->props.profile.cpuset = NULL;
#endif /* NMAD_HWLOC */
  nm_drv_list_push_back(&p_core->driver_list, p_drv);
  p_core->nb_drivers++;
  if(!p_drv->minidriver_context)
    {
      NM_FATAL("cannot find context in minidriver component %s. Bad assembly.\n",
               driver_component->name);
    }

  /* ** driver pre-init */
  if(!p_drv->driver->getprops)
    {
      NM_FATAL("driver %s has no 'getprops' method.\n", driver_component->name);
    }
  (*p_drv->driver->getprops)(p_drv->minidriver_context, &p_drv->props);

#ifdef NMAD_HWLOC
  /* ** NUIOA */
  static int nuioa_done = 0;
  if(nuioa_done)
    {
      NM_DISPF("nuioa- already bound by another driver.\n");
    }
  else if(padico_module_attr_nuioa_enable_getvalue())
    {
      hwloc_cpuset_t cpuset = p_drv->props.profile.cpuset;
      if(cpuset != NULL
         && !hwloc_bitmap_isequal(cpuset, hwloc_topology_get_complete_cpuset(p_core->topology)))
        {
          /* if this driver wants something */
          hwloc_cpuset_t current = hwloc_bitmap_alloc();
          int rc = hwloc_get_cpubind(p_core->topology, current, HWLOC_CPUBIND_THREAD);
          char*s_current = NULL;
          hwloc_bitmap_asprintf(&s_current, current);
          char*s_complete = NULL;
          hwloc_bitmap_asprintf(&s_complete, hwloc_topology_get_complete_cpuset(p_core->topology));
          char*s_drv_cpuset = NULL;
          hwloc_bitmap_asprintf(&s_drv_cpuset, p_drv->props.profile.cpuset);
          if((rc == 0) && !hwloc_bitmap_isequal(current, hwloc_topology_get_complete_cpuset(p_core->topology)))
            {
              NM_DISPF("nuioa- thread already bound to %s; complete = %s. Not binding to %s.\n",
                       s_current, s_complete, s_drv_cpuset);
            }
          else
            {
              NM_DISPF("nuioa- driver '%s' has a preferred location. Binding to %s.\n",
                       p_drv->assembly->name, s_drv_cpuset);
              rc = hwloc_set_cpubind(p_core->topology, p_drv->props.profile.cpuset, HWLOC_CPUBIND_THREAD);
              if(rc)
                {
                  NM_WARN("hwloc_set_cpubind failed.\n");
                }
            }
          padico_free(s_drv_cpuset);
          padico_free(s_complete);
          padico_free(s_current);
          hwloc_bitmap_free(current);
          nuioa_done = 1;
        }
      else
        {
          NM_DISPF("nuioa- driver '%s' has no preferred location. Not binding.\n", p_drv->assembly->name);
        }
    }
  else
    {
      NM_DISPF("nuioa- disabled by user\n");
    }
#else /* NMAD_HWLOC */
  NM_DISPF("nuioa- hwloc not available\n");
#endif /* NMAD_HWLOC */

  /* ** driver init*/
  if(!p_drv->driver->init)
    {
      NM_FATAL("driver %s has no 'init' method.\n", p_drv->assembly->name);
    }
  /* init driver & get url */
  const void*url = NULL;
  size_t url_size = 0;
  (*p_drv->driver->init)(p_drv->minidriver_context, &url, &url_size);
  p_drv->url = puk_hex_encode(url, &url_size, NULL);
  *p_url = p_drv->url;

#ifdef PIOMAN
  p_drv->ltask_binding = NULL;
#endif

  nm_ns_update(p_core, p_drv);

  if(p_drv->props.capabilities.max_msg_size == 0)
    {
      p_drv->props.capabilities.max_msg_size = NM_LEN_MAX;
    }

  NM_DISPF("loaded driver '%s'; url = '%s'; max_msg_size = %llu\n",
           driver_component->name, *p_url, (unsigned long long)p_drv->props.capabilities.max_msg_size);
  *pp_drv = p_drv;
  return NM_ESUCCESS;
}

void nm_core_driver_cancel_recv_any(struct nm_core*p_core, struct nm_drv_s*p_drv)
{
  nm_core_lock_assert(p_core);
  struct nm_pkt_wrap_s*p_pw = p_drv->p_pw_recv_any;
  if(p_pw)
    {
      /* manually cancel recv_any _before_ stopping polling to unlock blocking calls */
      if(p_drv->driver->recv_cancel_any)
        {
          nm_pw_ref_inc(p_pw);
          int rc = (*p_drv->driver->recv_cancel_any)(p_drv->minidriver_context);
          if(rc != NM_ESUCCESS)
            {
              NM_WARN("drv->recv_cancel_any() rc = %d (%s)\n", rc, nm_strerror(rc));
            }
        }
      /* stop polling */
#ifdef PIOMAN
      piom_ltask_cancel_request(&p_pw->ltask);
#else /* PIOMAN */
      nm_pkt_wrap_list_remove(&p_core->pending_recv_list, p_pw);
#endif /* PIOMAN */
      nm_core_unlock(p_core);
#ifdef PIOMAN
      piom_ltask_cancel_wait(&p_pw->ltask);
#endif
      nm_pw_ref_dec(p_pw);
      nm_core_lock(p_core);
    }
}

/** Flush all pending pw on all drivers.
 */
void nm_core_driver_flush(struct nm_core*p_core)
{
  nm_core_lock_assert(p_core);
  nm_drv_t p_drv = NULL;
  NM_FOR_EACH_DRIVER(p_drv, p_core)
    {
      /* cancel pre-posted pw on trk #0 for anygate */
      nm_core_driver_cancel_recv_any(p_core, p_drv);
    }

  /* cancel pre-posted pw on trk #0 for each gate */
  struct nm_pkt_wrap_list_s*pending_pw = nm_pkt_wrap_list_new();
  nm_gate_t p_gate = NULL;
  NM_FOR_EACH_GATE(p_gate, p_core)
    {
      int i;
      for(i = 0; i < p_gate->n_trks; i++)
        {
          struct nm_trk_s*p_trk = &p_gate->trks[i];
          struct nm_pkt_wrap_s*p_pw = p_trk->p_pw_recv;
          if(p_pw)
            {
              /* stop polling on driver */
#ifndef PIOMAN
              nm_pkt_wrap_list_remove(&p_core->pending_recv_list, p_pw);
              nm_pkt_wrap_list_push_back(pending_pw, p_pw);
#else
              int rc = piom_ltask_cancel_request(&p_pw->ltask);
              if(rc == 0)
                {
                  nm_pkt_wrap_list_push_back(pending_pw, p_pw);
                }
#endif
              /* cancel pw */
              struct puk_receptacle_NewMad_minidriver_s*r = &p_trk->receptacle;
              if(r->driver->recv_cancel && !(p_pw->flags & NM_PW_BUF_RECV))
                {
                  nm_pw_ref_inc(p_pw);
                  int rc = (*r->driver->recv_cancel)(r->_status);
                  if(rc != NM_ESUCCESS)
                    {
                      NM_WARN("drv->recv_cancel() rc = %d (%s)\n", rc, nm_strerror(rc));
                    }
                }
              p_trk->p_pw_recv = NULL;

            }
          p_trk->p_pw_recv= NULL;
        }
    }

  /* cancel ltasks with core unlocked */
  nm_core_unlock(p_core);
  struct nm_pkt_wrap_s*p_pw = NULL;
  do
    {
      p_pw = nm_pkt_wrap_list_pop_front(pending_pw);
      if(p_pw)
        {
#ifdef PIOMAN
          piom_ltask_cancel_wait(&p_pw->ltask);
#endif
          nm_pw_ref_dec(p_pw);
        }
    }
  while(p_pw != NULL);
  nm_core_lock(p_core);
  nm_pkt_wrap_list_delete(pending_pw);
}

/** close the given driver. */
void nm_core_driver_close(struct nm_drv_s*p_drv)
{
  if(p_drv->driver->close)
    {
      (*p_drv->driver->close)(p_drv->minidriver_context);
    }
}

/** free the driver structures. */
void nm_core_driver_destroy(struct nm_drv_s*p_drv)
{
#ifdef NMAD_HWLOC
  if(p_drv->props.profile.cpuset != NULL)
    {
      hwloc_bitmap_free(p_drv->props.profile.cpuset);
    }
#endif /* NMAD_HWLOC */
  padico_free((void*)p_drv->driver_id);
  padico_free((void*)p_drv->url);
  puk_component_destroy(p_drv->assembly);
  nm_drv_delete(p_drv);
}

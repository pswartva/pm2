/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Puk priority list, implemented as pairing heap
 * @ingroup Puk
 */

#ifndef PUK_PRIO_H
#define PUK_PRIO_H

/** implement priority queue as a pairing heap */
#define PUK_PRIO_PAIRING_HEAP
#undef PUK_PRIO_LIST

#define PUK_PRIO_TYPE(ENAME, DECL)              \
  PUK_PRIO_DECLARE_TYPE(ENAME)                  \
  PUK_PRIO_CREATE_TYPE(ENAME, DECL)             \
  PUK_PRIO_CREATE_FUNCS(ENAME)

#define PUK_PRIO_CREATE_TYPE(ENAME, DECL)       \
  struct ENAME##_prio_cell_s                    \
  {                                             \
    PUK_PRIO_LINK(ENAME);                       \
    DECL;                                       \
  };

#define PUK_PRIO_DECLARE_TYPE(ENAME)                            \
  PUK_PRIO_DECLARE_TYPE2(ENAME, struct ENAME##_prio_cell_s)

#if defined(PUK_PRIO_LIST)

/* ** priority queue as flat list ************************** */
/* use only for debug */

#define PUK_PRIO_DECLARE_TYPE2(ENAME, CELL)                     \
  PUK_LIST_DECLARE_TYPE2(ENAME##_prio_cell, CELL);              \
  typedef CELL*ENAME##_prio_cell_t;                             \
  typedef struct ENAME##_prio_list_s*ENAME##_prio_list_t;       \
  struct ENAME##_prio_list_s                                    \
  {                                                             \
    struct ENAME##_prio_cell_list_s _cells;                     \
  };                                                            \

#define PUK_PRIO_LINK(ENAME)                    \
  int ENAME##_prio;                             \
  PUK_LIST_LINK(ENAME##_prio_cell)

#define PUK_PRIO_CREATE_FUNCS(ENAME)                                    \
  PUK_LIST_CREATE_FUNCS(ENAME##_prio_cell)                              \
  static inline void ENAME##_prio_list_init(struct ENAME##_prio_list_s*l) \
  {                                                                     \
    ENAME##_prio_cell_list_init(&l->_cells);                            \
  }                                                                     \
  static inline void ENAME##_prio_list_insert(ENAME##_prio_list_t l, int prio, ENAME##_prio_cell_t c) \
  {                                                                     \
    c->ENAME##_prio = prio;                                             \
    ENAME##_prio_cell_itor_t i = ENAME##_prio_cell_list_rbegin(&l->_cells); \
    while((i != NULL) && (prio > i->ENAME##_prio))                      \
      {                                                                 \
        i = ENAME##_prio_cell_list_rnext(i);                            \
      }                                                                 \
    if(i)                                                               \
      ENAME##_prio_cell_list_insert_after(&l->_cells, i, c);            \
    else                                                                \
      ENAME##_prio_cell_list_push_front(&l->_cells, c);                 \
  }                                                                     \
  static inline ENAME##_prio_cell_t ENAME##_prio_list_pop(ENAME##_prio_list_t l) \
  {                                                                     \
    return ENAME##_prio_cell_list_pop_front(&l->_cells);                \
  }                                                                     \
  static inline ENAME##_prio_cell_t ENAME##_prio_list_top(ENAME##_prio_list_t l) \
  {                                                                     \
    return ENAME##_prio_cell_list_begin(&l->_cells);                    \
  }                                                                     \
  static inline int ENAME##_prio_cell_get_prio(ENAME##_prio_cell_t c)   \
  {                                                                     \
    return c->ENAME##_prio;                                             \
  }

#elif defined(PUK_PRIO_PAIRING_HEAP)

/* ** priority queue as pairing heap *********************** */

/* Operations on prio lists:
 *  - init
 *  - insert
 *  - top
 *  - pop
 */

/** types for pairing heap */
#define PUK_PRIO_DECLARE_TYPE2(ENAME, CELL)                     \
  typedef CELL*ENAME##_prio_cell_t;                             \
  typedef struct ENAME##_prio_list_s*ENAME##_prio_list_t;       \
  struct ENAME##_prio_list_s                                    \
  {                                                             \
    ENAME##_prio_cell_t _root;                                  \
  };                                                            \

/** link for pairing heap */
#define PUK_PRIO_LINK(ENAME)                                            \
  struct ENAME##_prio_link_s                                            \
  {                                                                     \
    int _prio;                                                          \
    ENAME##_prio_cell_t _child                                          \
      __attribute__((aligned((sizeof(void*)))));                        \
    ENAME##_prio_cell_t _sibling                                        \
      __attribute__((aligned((sizeof(void*)))));                        \
  } ENAME##_prio_link;


/** func for pairing heap */
#define PUK_PRIO_CREATE_FUNCS(ENAME)                                    \
                                                                        \
  /** @internal merge 2 heaps */                                        \
  static inline ENAME##_prio_cell_t ENAME##_prio_list_meld(ENAME##_prio_cell_t a, ENAME##_prio_cell_t b) \
  {                                                                     \
    if(a == NULL)                                                       \
      {                                                                 \
        return b;                                                       \
      }                                                                 \
    else if( b == NULL)                                                 \
      {                                                                 \
        return a;                                                       \
      }                                                                 \
    else if(a->ENAME##_prio_link._prio < b->ENAME##_prio_link._prio)    \
      {                                                                 \
        a->ENAME##_prio_link._sibling = b->ENAME##_prio_link._child;    \
        b->ENAME##_prio_link._child = a;                                \
        return b;                                                       \
      }                                                                 \
    else                                                                \
      {                                                                 \
        b->ENAME##_prio_link._sibling = a->ENAME##_prio_link._child;    \
        a->ENAME##_prio_link._child = b;                                \
        return a;                                                       \
      }                                                                 \
  }                                                                     \
  /** @internal */                                                      \
  static inline ENAME##_prio_cell_t ENAME##_prio_list_merge_binary(ENAME##_prio_cell_t*c, int depth) \
  {                                                                     \
    if(*c == NULL)                                                      \
      {                                                                 \
        return NULL;                                                    \
      }                                                                 \
    else if(depth == 0)                                                 \
      {                                                                 \
        ENAME##_prio_cell_t a = *c;                                     \
        *c = (*c)->ENAME##_prio_link._sibling;                          \
        a->ENAME##_prio_link._sibling = NULL;                           \
        return a;                                                       \
      }                                                                 \
    else                                                                \
      {                                                                 \
        ENAME##_prio_cell_t a = ENAME##_prio_list_merge_binary(c, depth - 1); \
        ENAME##_prio_cell_t b = ENAME##_prio_list_merge_binary(c, depth - 1); \
        return ENAME##_prio_list_meld(a, b);                            \
      }                                                                 \
  }                                                                     \
                                                                        \
  /** @internal */                                                      \
  static inline ENAME##_prio_cell_t ENAME##_prio_list_merge_exp(ENAME##_prio_cell_t*c) \
  {                                                                     \
    ENAME##_prio_cell_t a = ENAME##_prio_list_merge_binary(c, 0);       \
    int depth = 0;                                                      \
    while(*c)                                                           \
      {                                                                 \
        a = ENAME##_prio_list_meld(a, ENAME##_prio_list_merge_binary(c, depth)); \
        depth++;                                                        \
      }                                                                 \
    return a;                                                           \
  }                                                                     \
  /** return top cell */                                                \
  static inline ENAME##_prio_cell_t ENAME##_prio_list_top(ENAME##_prio_list_t l) \
  {                                                                     \
    return l->_root;                                                    \
  }                                                                     \
  /** remove top cell */                                                \
  static inline ENAME##_prio_cell_t ENAME##_prio_list_pop(ENAME##_prio_list_t l) \
  {                                                                     \
    if(l->_root == NULL)                                                \
      {                                                                 \
        return NULL;                                                    \
      }                                                                 \
    else                                                                \
      {                                                                 \
        ENAME##_prio_cell_t top = l->_root;                             \
        l->_root = ENAME##_prio_list_merge_exp(&l->_root->ENAME##_prio_link._child); \
        return top;                                                     \
      }                                                                 \
  }                                                                     \
  /** insert a cell in the prio list */                                 \
  static inline void ENAME##_prio_list_insert(ENAME##_prio_list_t l, int prio, ENAME##_prio_cell_t c) \
  {                                                                     \
    c->ENAME##_prio_link._prio = prio;                                  \
    c->ENAME##_prio_link._child = NULL;                                 \
    c->ENAME##_prio_link._sibling = NULL;                               \
    l->_root = ENAME##_prio_list_meld(l->_root, c);                     \
  }                                                                     \
  /** allocate new prio list */                                         \
  static inline void ENAME##_prio_list_init(struct ENAME##_prio_list_s*l) \
  {                                                                     \
    l->_root = NULL;                                                    \
  }                                                                     \
  static inline int ENAME##_prio_cell_get_prio(ENAME##_prio_cell_t c)   \
  {                                                                     \
    return c->ENAME##_prio_link._prio;                                  \
  }

#endif /* PUK_PRIO_PAIRING_HEAP */

/* ** Priority table *************************************** */

/* combines priority list, hashtable for easy lookup, and cell allocator */

/* Operations on prio tables:
 *  - init
 *  - destroy
 *  - insert
 *  - top
 *  - pop
 *  - lookup
 *  - cell_alloc
 *  - cell_free
 */

#define PUK_PRIO_TABLE_TYPE(ENAME, DECL)                                \
  PUK_PRIO_TYPE(ENAME, DECL);                                           \
  PUK_ALLOCATOR_TYPE(ENAME, struct ENAME##_prio_cell_s);                \
  static int ENAME##_prio_eq(const int*p_prio1, const int*p_prio2)      \
  {                                                                     \
    return (*p_prio1 == *p_prio2);                                      \
  }                                                                     \
  /** hash function for priority level */                               \
  static uint32_t ENAME##_prio_hash(const int*p_prio)                   \
  {                                                                     \
    return *p_prio;                                                     \
  }                                                                     \
  /** hashtable to index cells by prio */                               \
  PUK_HASHTABLE_TYPE(ENAME##_prio_cell, int*, struct ENAME##_prio_cell_s*, &ENAME##_prio_hash, &ENAME##_prio_eq, NULL); \
  struct ENAME##_prio_table_s                                           \
  {                                                                     \
    struct ENAME##_prio_list_s prio_list;                               \
    struct ENAME##_prio_cell_hashtable_s cells_by_prio;                 \
    struct ENAME##_allocator_s allocator;                               \
  };                                                                    \
  static inline void ENAME##_prio_table_init(struct ENAME##_prio_table_s*p_table) \
  {                                                                     \
    ENAME##_prio_list_init(&p_table->prio_list);                        \
    ENAME##_prio_cell_hashtable_init(&p_table->cells_by_prio);          \
    ENAME##_allocator_init(&p_table->allocator, 16);                    \
  }                                                                     \
  static inline void ENAME##_prio_table_insert(struct ENAME##_prio_table_s*p_table, int prio, ENAME##_prio_cell_t c) \
  {                                                                     \
    ENAME##_prio_list_insert(&p_table->prio_list, prio, c);             \
    ENAME##_prio_cell_hashtable_insert(&p_table->cells_by_prio, &c->ENAME##_prio_link._prio, c); \
  }                                                                     \
  static inline ENAME##_prio_cell_t ENAME##_prio_table_top(struct ENAME##_prio_table_s*p_table) \
  {                                                                     \
    return ENAME##_prio_list_top(&p_table->prio_list);                  \
  }                                                                     \
  static inline ENAME##_prio_cell_t ENAME##_prio_table_pop(struct ENAME##_prio_table_s*p_table) \
  {                                                                     \
    ENAME##_prio_cell_t top = ENAME##_prio_list_top(&p_table->prio_list); \
    if(top != NULL)                                                     \
      {                                                                 \
        ENAME##_prio_cell_hashtable_remove(&p_table->cells_by_prio, &top->ENAME##_prio_link._prio); \
        ENAME##_prio_list_pop(&p_table->prio_list);                     \
      }                                                                 \
    return top;                                                         \
  }                                                                     \
  static inline ENAME##_prio_cell_t ENAME##_prio_table_lookup(struct ENAME##_prio_table_s*p_table, int prio) \
  {                                                                     \
    return ENAME##_prio_cell_hashtable_lookup(&p_table->cells_by_prio, &prio); \
  }                                                                     \
  static inline void ENAME##_prio_table_destroy(struct ENAME##_prio_table_s*p_table) \
  {                                                                     \
    ENAME##_prio_cell_hashtable_destroy(&p_table->cells_by_prio);       \
    ENAME##_allocator_destroy(&p_table->allocator);                     \
  }                                                                     \
  static inline struct ENAME##_prio_cell_s*ENAME##_prio_table_cell_alloc(struct ENAME##_prio_table_s*p_table) \
  {                                                                     \
    return ENAME##_malloc(&p_table->allocator);                         \
  }                                                                     \
  static inline void ENAME##_prio_table_cell_free(struct ENAME##_prio_table_s*p_table, struct ENAME##_prio_cell_s*p_cell )\
  {                                                                     \
    ENAME##_free(&p_table->allocator, p_cell);                          \
  }


#endif /* PUK_PRIO_H */

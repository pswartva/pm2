/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * bootstrap PadicoTM as a library
 */

#include <stdio.h>

#include <Padico/Puk.h>
#include <Padico/PadicoTM.h>
#include <Padico/Module.h>

static int  padico_bootlib_init(void);
static void padico_bootlib_finalize(void);

PADICO_MODULE_DECLARE(PadicoBootLib, padico_bootlib_init, NULL, padico_bootlib_finalize);

/* ********************************************************* */

static padico_tm_bgthread_pool_t bgthread_pool = NULL;

static void*bootlib_main_thread(void*dummy)
{
  padico_out(20, "starting main...\n");
  padico_tm_main();
  padico_out(20, "main started.\n");
  return NULL;
}

static int padico_bootlib_init(void)
{
  padico_out(20, "init...\n");

  /* defeat LD_PRELOAD inheritance! */
  puk_abi_disable_preload();

  if(!padico_puk_initialized())
    {
      padico_fatal("Puk not loaded.");
    }

  padico_tm_init();
  padico_out(20, "init done.\n");

  bgthread_pool = padico_tm_bgthread_pool_create("PadicoBootLib");
  padico_tm_bgthread_start(bgthread_pool, &bootlib_main_thread, NULL, "bootlib_main_thread");

  padico_out(20, "waiting ready signal...\n");
  padico_tm_ready_wait();
  padico_out(20, "ready!\n");

  return 0;
}


static void padico_bootlib_finalize(void)
{
  padico_out(20, "exit.\n");
  padico_tm_finalize();
  padico_tm_bgthread_pool_wait(bgthread_pool);
}

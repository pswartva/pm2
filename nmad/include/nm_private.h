/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


#ifndef NM_PRIVATE_H
#define NM_PRIVATE_H

/** @defgroup nmad_private nmad internal interfaces.
 * End-users are not supposed to use it. The interface is
 * not usable outside of nmad core.
 * @{
 */

#include "nm_private_config.h"
#include <Padico/Puk.h>
#ifdef PIOMAN
#include <pioman.h>
#endif

#if defined(PUKABI)
#include <Padico/Puk-ABI.h>
#endif /* PUKABI */
#if defined(PUKABI_ENABLE_FSYS)
#define NM_SYS(SYMBOL) PUK_ABI_FSYS_WRAP(SYMBOL)
#else  /* PUKABI_ENABLE_FSYS */
#define NM_SYS(SYMBOL) SYMBOL
#endif /* PUKABI_ENABLE_FSYS */

#ifndef NMAD_BUILD
#  error "NMAD_BUILD flag not set while including nm_private.h. This header is not part of API and cannot be used outside of nmad."
#endif /* NMAD_BUILD */

#include <nm_public.h>
#include <nm_core_interface.h>
#include <nm_log.h>

#include <nm_parameters.h>
#include <nm_trace.h>

#include <Padico/Module.h>

#ifdef NMAD_HWLOC
#include <hwloc.h>
#endif

#include <nm_core_private.h>
#include <nm_tags.h>
#include <nm_pkt_wrap.h>
#include <nm_minidriver.h>
#include <nm_drv.h>
#include <nm_log.h>
#ifdef PIOMAN
#include <nm_piom_ltasks.h>
#endif
#include <nm_headers.h>
#include <nm_strategy.h>
#include <nm_gate.h>
#include <nm_core.h>
#include <nm_lock.h>

#ifdef NMAD_PIOMAN
#  define NMAD_CONFIG_PIOMAN 1
#else
#  define NMAD_CONFIG_PIOMAN 0
#endif

#ifdef NMAD_HWLOC
#  define NMAD_CONFIG_HWLOC 1
#else
#  define NMAD_CONFIG_HWLOC 0
#endif

#ifdef NMAD_PUKABI
#  define NMAD_CONFIG_PUKABI 1
#else
#  define NMAD_CONFIG_PUKABI 0
#endif

#ifdef NMAD_DEBUG
#  define NMAD_CONFIG_DEBUG 1
#else
#  define NMAD_CONFIG_DEBUG 0
#endif

#ifdef NDEBUG
#  define NMAD_CONFIG_NDEBUG 1
#else
#  define NMAD_CONFIG_NDEBUG 0
#endif

#ifdef NMAD_SIMGRID
#  define NMAD_CONFIG_SIMGRID 1
#else
#  define NMAD_CONFIG_SIMGRID 0
#endif

#define _tostring2(s) #s
#define _tostring(s) _tostring2(s)

#define NMAD_CONFIG_STRING                           \
  " pioman = "  _tostring(NMAD_CONFIG_PIOMAN)        \
  " hwloc = "   _tostring(NMAD_CONFIG_HWLOC)         \
  " pukabi = "  _tostring(NMAD_CONFIG_PUKABI)        \
  " debug = "   _tostring(NMAD_CONFIG_DEBUG)         \
  " ndebug = "  _tostring(NMAD_CONFIG_NDEBUG)        \
  " simgrid = " _tostring(NMAD_CONFIG_SIMGRID)


/* ** core ************************************************* */

void nm_core_post_send(struct nm_pkt_wrap_s*p_pw, nm_gate_t p_gate, nm_trk_id_t trk_id);

void nm_core_post_recv(struct nm_pkt_wrap_s*p_pw, nm_gate_t p_gate, nm_trk_id_t trk_id, nm_drv_t p_drv);

__PUK_SYM_INTERNAL
void nm_core_driver_flush(struct nm_core*p_core);

__PUK_SYM_INTERNAL
void nm_core_driver_connect_wait_all(struct nm_core*p_core, struct nm_drv_s*p_drv);

__PUK_SYM_INTERNAL
void nm_core_driver_close(struct nm_drv_s*p_drv);

__PUK_SYM_INTERNAL
void nm_core_driver_destroy(struct nm_drv_s*p_drv);

__PUK_SYM_INTERNAL
void nm_core_gate_destroy(struct nm_gate_s*p_gate);

__PUK_SYM_INTERNAL
void nm_core_status_event(nm_core_t p_core, const struct nm_core_event_s*const p_event, struct nm_req_s*p_req);

__PUK_SYM_INTERNAL
void nm_core_events_dispatch(struct nm_core*p_core);

__PUK_SYM_INTERNAL
void nm_so_schedule_clean(struct nm_core*p_core);

__PUK_SYM_INTERNAL
void nm_core_progress(struct nm_core*p_core);

__PUK_SYM_INTERNAL
void nm_core_task_flush(struct nm_core*p_core);

__PUK_SYM_INTERNAL
void nm_drv_refill_recv(nm_drv_t p_drv, nm_gate_t p_gate);

__PUK_SYM_INTERNAL
void nm_core_gate_disconnect(struct nm_gate_s*p_gate);

__PUK_SYM_INTERNAL
void nm_core_gate_disconnected(struct nm_core*p_core, nm_gate_t p_gate, nm_drv_t p_drv);

__PUK_SYM_INTERNAL
void nm_core_matching_check(struct nm_core*p_core);

void nm_core_unpack_trigger_next(struct nm_core*p_core, struct nm_core_task_s*p_core_task);

/* ** packet wrappers ************************************** */

__PUK_SYM_INTERNAL
void nm_pw_send_prefetch(struct nm_pkt_wrap_s*p_pw);

__PUK_SYM_INTERNAL
void nm_pw_send_unfetch(struct nm_pkt_wrap_s*p_pw);

void nm_pw_send_trigger(struct nm_pkt_wrap_s*p_pw);

void nm_pw_send_post_default(struct nm_pkt_wrap_s*p_pw);

void nm_pw_send_complete_default(struct nm_pkt_wrap_s*p_pw);

__PUK_SYM_INTERNAL
void nm_pw_send_progress(struct nm_pkt_wrap_s*p_pw);

void nm_pw_send_post(struct nm_pkt_wrap_s*p_pw);

void nm_pw_send_poll(struct nm_pkt_wrap_s *p_pw);

__PUK_SYM_INTERNAL
void nm_pw_send_wait(struct nm_pkt_wrap_s*p_pw);

__PUK_SYM_INTERNAL
void nm_pw_recv_prefetch(struct nm_pkt_wrap_s*p_pw);

__PUK_SYM_INTERNAL
void nm_pw_recv_unfetch(struct nm_pkt_wrap_s*p_pw);

__PUK_SYM_INTERNAL
void nm_pw_recv_progress(struct nm_pkt_wrap_s*p_pw);

__PUK_SYM_INTERNAL
void nm_pw_recv_wait(struct nm_pkt_wrap_s*p_pw);

/** Process a complete successful outgoing request.
 */
void nm_pw_process_complete_send(struct nm_core*p_core, struct nm_pkt_wrap_s*p_pw);

/** Process complete incoming request.
 */
void nm_pw_process_complete_recv(struct nm_core*p_core, struct nm_pkt_wrap_s*p_pw);

__PUK_SYM_INTERNAL
void nm_rtr_handler(struct nm_pkt_wrap_s*p_rtr_pw, const struct nm_header_ctrl_rtr_s*p_header);

/* ** data ************************************************* */

/** pack data from nm_data format to pkt format */
void nm_data_pkt_pack(struct nm_pkt_wrap_s*p_pw, nm_core_tag_t tag, nm_seq_t seq,
                      const struct nm_data_s*p_data, nm_len_t chunk_offset, nm_len_t chunk_len, uint8_t flags);

/** unpack from pkt format to data format */
void nm_data_pkt_unpack(const struct nm_data_s*p_data, const struct nm_header_pkt_data_s*h, const struct nm_pkt_wrap_s*p_pw,
                        nm_len_t chunk_offset, nm_len_t chunk_len);

#include "nm_core_inline.h"
#include "nm_tactics.h"
#include "nm_sampling.h"

/** @} */

#endif /* NM_PRIVATE_H */

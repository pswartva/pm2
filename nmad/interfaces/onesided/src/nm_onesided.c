/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <nm_onesided_interface.h>
#include <nm_sendrecv_interface.h>
#include <nm_core_interface.h>
#include <nm_private.h>
#ifdef PIOMAN
#include <pioman.h>
#endif /* PIOMAN */
#include <nm_log.h>
#include <stdlib.h>

#include <Padico/Module.h>
PADICO_MODULE_BUILTIN(nm_onesided, NULL, NULL, NULL);

/** tag to send op requests */
#define NM_ONESIDED_TAG_REQUEST     0x01
/** tag for a reply to a get request */
#define NM_ONESIDED_TAG_REPLY_GET   0x02
/** tag for a reply to a fence request */
#define NM_ONESIDED_TAG_REPLY_FENCE 0x03
/** tag to inject target completions */
#define NM_ONESIDED_TAG_TARGET_REQ  0x04


/* ********************************************************* */
/* ** data */

struct nm_onesided_data_content_s
{
  struct nm_onesided_header_s*p_header;
  const struct nm_data_s*p_data;        /**< data for the onesided operation */
  const struct nm_data_s*p_data_target; /**< data delivered to target completion */
};

static void nm_onesided_data_traversal(const void*_content, nm_data_apply_t apply, void*_context);
const struct nm_data_ops_s nm_onesided_data_ops =
  {
    .p_traversal = &nm_onesided_data_traversal
  };
NM_DATA_TYPE(onesided, struct nm_onesided_data_content_s, &nm_onesided_data_ops);

static void nm_onesided_data_traversal(const void*_content, nm_data_apply_t apply, void*_context)
{
  const struct nm_onesided_data_content_s*p_content = _content;
  (*apply)(p_content->p_header, sizeof(struct nm_onesided_header_s), _context);
  if(p_content->p_data_target != NULL)
    nm_data_traversal_apply(p_content->p_data_target, apply, _context);
  if(p_content->p_data != NULL)
    nm_data_traversal_apply(p_content->p_data, apply, _context);
}

static inline void nm_onesided_data_build(struct nm_data_s*p_onesided_data,
                                          struct nm_onesided_header_s*p_header, const struct nm_data_s*p_data,
                                          const struct nm_data_s*p_data_target)
{
  nm_data_onesided_set(p_onesided_data, (struct nm_onesided_data_content_s)
                       {
                         .p_header      = p_header,
                         .p_data        = p_data,
                         .p_data_target = p_data_target
                       });
}

/* ********************************************************* */
/* ** handler */

static void nm_onesided_handler(nm_sr_event_t event, const nm_sr_event_info_t*p_info, void*_ref);

static void nm_onesided_req_reload(nm_onesided_t p_onesided)
{
  if(p_onesided->running)
    {
      nm_sr_recv_init(p_onesided->p_session, &p_onesided->request.sr_req);
      nm_sr_request_set_ref(&p_onesided->request.sr_req, p_onesided);
      nm_sr_request_monitor(p_onesided->p_session, &p_onesided->request.sr_req,
                            NM_SR_EVENT_FINALIZED | NM_SR_EVENT_RECV_DATA | NM_SR_EVENT_RECV_CANCELLED,
                            &nm_onesided_handler);
      nm_sr_recv_irecv(p_onesided->p_session, &p_onesided->request.sr_req, NM_ANY_GATE, NM_ONESIDED_TAG_REQUEST, NM_TAG_MASK_FULL);
    }
  else
    {
      /* acknowledge termination */
      nm_cond_signal(&p_onesided->done, 1);
    }
}

/** data injector for target-side completion data */
void nm_onesided_inject_pull_data(struct nm_req_s*p_req, const struct nm_data_s*p_data, nm_len_t chunk_offset, nm_len_t chunk_len, void*p_ref)
{
  struct nm_onesided_target_completion_s*p_target = p_ref;
  if(chunk_len != p_target->target_size)
    {
      NM_FATAL("size mismatch in target-side completion data; posted = %d; received = %d\n",
               (int)chunk_len, (int)p_target->target_size);
    }
  nm_data_copy_to(p_data, chunk_offset, chunk_len, &p_target->target_buf[0]);
  /* fill completion status */
  struct nm_sr_request_s*p_request = nm_container_of(p_req, struct nm_sr_request_s, req);
  struct nm_onesided_target_request_s*p_target_request = nm_sr_request_get_ref(p_request);
  p_target_request->size = p_target->op_size;
  p_target_request->op   = p_target->op;
  p_target_request->addr = p_target->addr;
  nm_core_inject_complete_finalize(nm_core_get_singleton(), p_req, chunk_offset, chunk_len);
  nm_onesided_target_completion_free(p_target->p_onesided->completion_allocator, p_target);
}


static void nm_onesided_ack_handler(nm_sr_event_t event, const nm_sr_event_info_t*p_info, void*_ref)
{
  nm_sr_request_t*p_req = p_info->req.p_request;
  if(event & NM_SR_EVENT_FINALIZED)
    {
      struct nm_onesided_s*p_onesided = _ref;
      nm_sr_req_free(p_onesided->req_allocator, p_req);
    }
}

static void nm_onesided_handler(nm_sr_event_t event, const nm_sr_event_info_t*p_info, void*_ref)
{
  assert(! ((event & NM_SR_EVENT_FINALIZED) &&
            (event & NM_SR_EVENT_RECV_DATA)));
  nm_sr_request_t*pp_req = p_info->req.p_request;
  struct nm_onesided_s*p_onesided = nm_sr_request_get_ref(pp_req);
  assert(p_onesided != NULL);
  nm_gate_t p_gate = nm_sr_request_get_gate(pp_req);
  nm_tag_t tag = nm_sr_request_get_tag(pp_req);
  if(!(event & NM_SR_EVENT_RECV_CANCELLED))
    {
      if(event & NM_SR_EVENT_FINALIZED)
        {
          if(p_onesided->p_target != NULL)
            {
              /* notify target-side completion */
              const nm_seq_t seq = p_onesided->request.header.seq;
              assert(seq != NM_SEQ_NONE);
              const nm_core_tag_t core_tag = nm_core_tag_build(p_onesided->p_session->hash_code, NM_ONESIDED_TAG_TARGET_REQ);
              nm_core_inject_chunk(nm_core_get_singleton(), p_gate, core_tag, seq,
                                   0 /* chunk_offset */, p_onesided->p_target->target_size, 1 /* last chunk */,
                                   &nm_onesided_inject_pull_data, p_onesided->p_target);
              p_onesided->p_target = NULL;
            }
          nm_onesided_req_reload(p_onesided);
        }
      if(event & NM_SR_EVENT_RECV_DATA)
        {
          /* receive header */
          struct nm_data_s data_header;
          nm_data_contiguous_build(&data_header, &p_onesided->request.header, sizeof(struct nm_onesided_header_s));
          int rc = nm_sr_recv_peek(p_onesided->p_session, &p_onesided->request.sr_req, &data_header);
          if(rc != NM_ESUCCESS)
            {
              NM_FATAL("rc = %d in nm_sr_recv_peek()\n", rc);
            }
          nm_sr_recv_offset(p_onesided->p_session, &p_onesided->request.sr_req, sizeof(struct nm_onesided_header_s));
          if(p_onesided->request.header.flags & NM_ONESIDED_FLAG_TARGET_COMPLETION)
            {
              /* gather target-side completion status then adjust offset */
              const nm_len_t target_size = p_onesided->request.header.target_size;
              struct nm_onesided_target_completion_s*p_target = nm_onesided_target_completion_malloc(p_onesided->completion_allocator);
              p_target->target_size = target_size;
              p_target->op_size = p_onesided->request.header.size;
              p_target->op = p_onesided->request.header.flags & NM_ONESIDED_OP_MASK;
              p_target->addr = p_onesided->request.header.addr;
              p_target->p_onesided = p_onesided;
              nm_data_contiguous_build(&p_onesided->request.target_data, &p_target->target_buf[0], target_size);
              nm_onesided_data_build(&p_onesided->data, &p_onesided->request.header, NULL, &p_onesided->request.target_data);
              rc = nm_sr_recv_peek_offset(p_onesided->p_session, &p_onesided->request.sr_req, &p_onesided->data,
                                          sizeof(struct nm_onesided_header_s), target_size);
              if(rc != NM_ESUCCESS)
                {
                  NM_FATAL("rc = %d in nm_sr_recv_peek()\n", rc);
                }
              nm_sr_recv_offset(p_onesided->p_session, &p_onesided->request.sr_req,
                                sizeof(struct nm_onesided_header_s) + target_size);
              p_onesided->p_target = p_target;
            }
          else
            {
              p_onesided->p_target = NULL;
            }
          switch(p_onesided->request.header.flags & NM_ONESIDED_OP_MASK)
            {
            case NM_ONESIDED_OP_PUT:
              {
                const nm_len_t size = p_onesided->request.header.size;
                void*ptr = (void*)p_onesided->request.header.addr;
                nm_data_contiguous_build(&p_onesided->request.user_data, ptr, size);
                nm_onesided_data_build(&p_onesided->data, &p_onesided->request.header, &p_onesided->request.user_data,
                                       (p_onesided->p_target != NULL) ? &p_onesided->request.target_data : NULL);
                nm_sr_recv_unpack_data(p_onesided->p_session, &p_onesided->request.sr_req, &p_onesided->data);
              }
              break;
            case NM_ONESIDED_OP_GET:
              {
                const nm_len_t size = p_onesided->request.header.size;
                const void*ptr = (const void*)p_onesided->request.header.addr;
                nm_onesided_data_build(&p_onesided->data, &p_onesided->request.header, NULL,
                                       (p_onesided->p_target != NULL) ? &p_onesided->request.target_data : NULL);
                nm_sr_recv_unpack_data(p_onesided->p_session, &p_onesided->request.sr_req, &p_onesided->data);
                nm_sr_request_t*p_req_ack = nm_sr_req_malloc(p_onesided->req_allocator);
                nm_sr_isend(p_onesided->p_session, p_gate, NM_ONESIDED_TAG_REPLY_GET, ptr, size, p_req_ack);
                nm_sr_request_set_ref(p_req_ack, p_onesided);
                nm_sr_request_monitor(p_onesided->p_session, p_req_ack, NM_SR_EVENT_FINALIZED, &nm_onesided_ack_handler);
              }
              break;
            case NM_ONESIDED_OP_FENCE:
              {
                nm_sr_recv_unpack_data(p_onesided->p_session, &p_onesided->request.sr_req, &data_header);
                nm_sr_request_t*p_req_ack = nm_sr_req_malloc(p_onesided->req_allocator);
                nm_sr_isend(p_onesided->p_session, p_gate, NM_ONESIDED_TAG_REPLY_FENCE, NULL, 0, p_req_ack);
                nm_sr_request_set_ref(p_req_ack, p_onesided);
                nm_sr_request_monitor(p_onesided->p_session, p_req_ack, NM_SR_EVENT_FINALIZED, &nm_onesided_ack_handler);
              }
              break;
            case NM_ONESIDED_OP_QUEUE:
              {
                const int queue = p_onesided->request.header.addr;
                const nm_len_t size = p_onesided->request.header.size;
                void*ptr = padico_malloc(size);
                nm_data_contiguous_build(&p_onesided->request.user_data, ptr, size);
                nm_onesided_data_build(&p_onesided->data, &p_onesided->request.header, &p_onesided->request.user_data,
                                       (p_onesided->p_target != NULL) ? &p_onesided->request.target_data : NULL);
                nm_sr_recv_unpack_data(p_onesided->p_session, &p_onesided->request.sr_req, &p_onesided->data);
                nm_len_t req_size, expected_size;
                nm_sr_request_get_size(p_info->req.p_request, &req_size);
                nm_sr_request_get_expected_size(p_info->req.p_request, &expected_size);
                if(req_size != expected_size)
                  {
                    NM_FATAL("trying to enqueue a long message in remote queue- entry size = %ld; req_size = %ld; expected_size = %ld.\n",
                             size, req_size, expected_size);
                  }
                if((queue >= 0) && (queue < nm_onesided_queue_vect_size(&p_onesided->queues)))
                  {
                    struct nm_onesided_queue_entry_dlfq_s*p_queue = nm_onesided_queue_vect_at(&p_onesided->queues, queue);
                    nm_onesided_queue_entry_dlfq_enqueue_single_writer(p_queue, ptr);
                  }
                else
                  {
                    NM_WARN("received message on non-existing queue %d. Discarding %ld bytes.\n", queue, size);
                    padico_free(ptr);
                  }
              }
              break;
            default:
              fprintf(stderr, "# nm_onesided: unknown op = %d\n", p_onesided->request.header.flags);
              break;
            }
        }
    }
}


/* ********************************************************* */

void nm_onesided_init(nm_session_t p_session, nm_onesided_t*pp_onesided)
{
#if !defined(PIOMAN_MULTITHREAD)
#ifdef NMAD_DEBUG
  NM_WARN("nm_onesided: a multi-threaded flavor of pioman is recommended for 'onesided' interface. Please rebuild with a multi-threaded flavor of pioman for background progression.\n");
#endif /* NMAD_DEBUG */
#endif /* PIOMAN_MULTITHREAD*/
  struct nm_onesided_s*p_onesided = padico_malloc(sizeof(struct nm_onesided_s));
  padico_string_t s_name = padico_string_new();
  padico_string_printf(s_name, "%s-onesided", nm_session_get_name(p_session));
  nm_session_open(&p_onesided->p_session, padico_string_get(s_name));
  padico_string_delete(s_name);
  p_onesided->req_allocator = nm_sr_req_allocator_new(4);
  p_onesided->completion_allocator = nm_onesided_target_completion_allocator_new(4);
  nm_onesided_queue_vect_init(&p_onesided->queues);
  p_onesided->running = 1;
  nm_cond_init(&p_onesided->done, 1);
  nm_onesided_req_reload(p_onesided);
  *pp_onesided = p_onesided;
}

void nm_onesided_finalize(nm_onesided_t p_onesided)
{
  nm_sr_flush(p_onesided->p_session);
  nm_atomic_dec(&p_onesided->running); /* disallow recv reloaad */
  int rc = nm_sr_rcancel(p_onesided->p_session, &p_onesided->request.sr_req);
  if(rc != NM_ESUCCESS)
    {
      /* concurrent handler detected; wait for completion */
      nm_cond_wait(&p_onesided->done, 1, nm_core_get_singleton());
    }
  nm_sr_req_allocator_delete(p_onesided->req_allocator);
  nm_onesided_target_completion_allocator_delete(p_onesided->completion_allocator);
  nm_onesided_queue_vect_destroy(&p_onesided->queues);
  nm_session_close(p_onesided->p_session);
  p_onesided->p_session = NULL;
  padico_free(p_onesided);
}

/* ********************************************************* */

void nm_onesided_iput(nm_onesided_t p_onesided, struct nm_gate_s*p_gate,
                      const void*p_ptr, nm_len_t len, uintptr_t dest_addr, nm_onesided_request_t*p_req)
{
  nm_data_contiguous_build(&p_req->user_data, (void*)p_ptr, len);
  nm_onesided_iput_data(p_onesided, p_gate, &p_req->user_data, dest_addr, p_req);
}

void nm_onesided_iput_data(nm_onesided_t p_onesided, struct nm_gate_s*p_gate,
                           struct nm_data_s*p_data, uintptr_t dest_addr, nm_onesided_request_t*p_req)
{
  p_req->header.addr  = dest_addr;
  p_req->header.size  = nm_data_size(p_data);
  p_req->header.flags = NM_ONESIDED_OP_PUT;
  p_req->header.seq   = NM_SEQ_NONE;
  struct nm_data_s data;
  nm_onesided_data_build(&data, &p_req->header, p_data, NULL);
  nm_sr_send_init(p_onesided->p_session, &p_req->sr_req);
  nm_sr_send_pack_data(p_onesided->p_session, &p_req->sr_req, &data);
  nm_sr_send_dest(p_onesided->p_session, &p_req->sr_req, p_gate, NM_ONESIDED_TAG_REQUEST);
  nm_sr_send_header(p_onesided->p_session, &p_req->sr_req, sizeof(struct nm_onesided_header_s));
  nm_sr_send_submit(p_onesided->p_session, &p_req->sr_req);
}

void nm_onesided_iget(nm_onesided_t p_onesided, struct nm_gate_s*p_gate,
                      void*p_ptr, nm_len_t len, uintptr_t dest_addr, nm_onesided_request_t*p_req)
{
  nm_data_contiguous_build(&p_req->user_data, p_ptr, len);
  nm_onesided_iget_data(p_onesided, p_gate, &p_req->user_data, dest_addr, p_req);
}

void nm_onesided_iget_data(nm_onesided_t p_onesided, struct nm_gate_s*p_gate,
                           struct nm_data_s*p_data, uintptr_t dest_addr, nm_onesided_request_t*p_req)
{
  struct nm_onesided_header_s header = { .addr = dest_addr, .size = nm_data_size(p_data), .flags = NM_ONESIDED_OP_GET };
  nm_sr_send(p_onesided->p_session, p_gate, NM_ONESIDED_TAG_REQUEST, &header, sizeof(header));
  nm_sr_recv_init(p_onesided->p_session, &p_req->sr_req);
  nm_sr_recv_unpack_data(p_onesided->p_session, &p_req->sr_req, p_data);
  nm_sr_recv_irecv(p_onesided->p_session, &p_req->sr_req, p_gate, NM_ONESIDED_TAG_REPLY_GET, NM_TAG_MASK_FULL);
}

void nm_onesided_ifence(nm_onesided_t p_onesided, struct nm_gate_s*p_gate, nm_onesided_request_t*p_req)
{
  struct nm_onesided_header_s header = { .addr = (uintptr_t)0, .flags = NM_ONESIDED_OP_FENCE };
  nm_sr_send(p_onesided->p_session, p_gate, NM_ONESIDED_TAG_REQUEST, &header, sizeof(header));
  nm_sr_irecv(p_onesided->p_session, p_gate, NM_ONESIDED_TAG_REPLY_FENCE, NULL, 0, &p_req->sr_req);
}

void nm_onesided_req_wait(nm_onesided_request_t*p_req)
{
  nm_sr_request_wait(&p_req->sr_req);
}

void nm_onesided_req_wait_all(nm_onesided_request_t**p_reqs, int n)
{
  assert((void*)&p_reqs[0]->sr_req == (void*)p_reqs[0]); /* nm_sr_request_t must be the first field in nm_onesided_request_s */
  nm_sr_request_wait_all((nm_sr_request_t**)p_reqs, n);
}

int nm_onesided_req_test(nm_onesided_request_t*p_req)
{
  return nm_sr_stest(p_req->sr_req.p_session, &p_req->sr_req);
}

/* ********************************************************* */

nm_onesided_queue_id_t nm_onesided_queue_create(nm_onesided_t p_onesided)
{
  struct nm_onesided_queue_entry_dlfq_s*p_queue = padico_malloc(sizeof(struct nm_onesided_queue_entry_dlfq_s));
  nm_onesided_queue_entry_dlfq_init(p_queue, 32);
  struct nm_onesided_queue_entry_dlfq_s**pp_queue = nm_onesided_queue_vect_push_back(&p_onesided->queues, p_queue);
  const int index = nm_onesided_queue_vect_rank(&p_onesided->queues, pp_queue);
  return index;
}

void nm_onesided_queue_ienqueue(nm_onesided_t p_onesided, struct nm_gate_s*p_gate, nm_onesided_queue_id_t queue,
                                const void*p_ptr, nm_len_t len, nm_onesided_request_t*p_req)
{
  nm_data_contiguous_build(&p_req->user_data, (void*)p_ptr, len);
  p_req->header.addr  = queue;
  p_req->header.size  = len;
  p_req->header.flags = NM_ONESIDED_OP_QUEUE;
  p_req->header.seq   = NM_SEQ_NONE;
  struct nm_data_s data;
  nm_onesided_data_build(&data, &p_req->header, &p_req->user_data, NULL);
  nm_sr_send_init(p_onesided->p_session, &p_req->sr_req);
  nm_sr_send_pack_data(p_onesided->p_session, &p_req->sr_req, &data);
  nm_sr_send_dest(p_onesided->p_session, &p_req->sr_req, p_gate, NM_ONESIDED_TAG_REQUEST);
  nm_sr_send_header(p_onesided->p_session, &p_req->sr_req, sizeof(struct nm_onesided_header_s));
  nm_sr_send_submit(p_onesided->p_session, &p_req->sr_req);
}

void*nm_onesided_queue_dequeue(nm_onesided_t p_onesided, nm_onesided_queue_id_t queue)
{
  struct nm_onesided_queue_entry_dlfq_s*p_queue = nm_onesided_queue_vect_at(&p_onesided->queues, queue);
  void*p_ptr = nm_onesided_queue_entry_dlfq_dequeue_single_reader(p_queue);
  if(p_ptr == NULL)
    {
      nm_sr_progress(p_onesided->p_session);
    }
  return p_ptr;
}

/* ********************************************************* */

void nm_onesided_target_irecv(nm_onesided_t p_onesided, nm_gate_t p_gate, void*p_ptr, nm_len_t len,
                              nm_onesided_target_request_t*p_target_req)
{
  nm_sr_recv_init(p_onesided->p_session, &p_target_req->local_req);
  nm_sr_recv_unpack_contiguous(p_onesided->p_session, &p_target_req->local_req, p_ptr, len);
  nm_sr_request_set_ref(&p_target_req->local_req, p_target_req);
  nm_sr_recv_irecv(p_onesided->p_session, &p_target_req->local_req, p_gate, NM_ONESIDED_TAG_TARGET_REQ, NM_TAG_MASK_FULL);
}


void nm_onesided_iput_with_target(nm_onesided_t p_onesided, struct nm_gate_s*p_gate,
                                  void*p_ptr, nm_len_t len, uintptr_t dest_addr,
                                  void*p_target_ptr, nm_len_t target_len,
                                  nm_onesided_request_t*p_req)
{
  if(target_len > NM_ONESIDED_TARGET_COMPLETION_MAX)
    {
      NM_FATAL("target-side completion maxium size = %d; requested = %d\n",
               NM_ONESIDED_TARGET_COMPLETION_MAX, (int)target_len);
    }
  const nm_core_tag_t core_tag = nm_core_tag_build(p_onesided->p_session->hash_code, NM_ONESIDED_TAG_TARGET_REQ);
  const nm_seq_t seq = nm_core_send_seq_get(nm_core_get_singleton(), p_gate, core_tag);
  p_req->header.addr        = dest_addr;
  p_req->header.size        = len;
  p_req->header.flags       = NM_ONESIDED_OP_PUT | NM_ONESIDED_FLAG_TARGET_COMPLETION;
  p_req->header.seq         = seq;
  p_req->header.target_size = target_len;
  nm_data_contiguous_build(&p_req->user_data, p_ptr, len);
  nm_data_contiguous_build(&p_req->target_data, p_target_ptr, target_len);
  struct nm_data_s data;
  nm_onesided_data_build(&data, &p_req->header, &p_req->user_data, &p_req->target_data);
  nm_sr_send_init(p_onesided->p_session, &p_req->sr_req);
  nm_sr_send_pack_data(p_onesided->p_session, &p_req->sr_req, &data);
  nm_sr_send_dest(p_onesided->p_session, &p_req->sr_req, p_gate, NM_ONESIDED_TAG_REQUEST);
  nm_sr_send_header(p_onesided->p_session, &p_req->sr_req, sizeof(struct nm_onesided_header_s));
  nm_sr_request_set_ref(&p_req->sr_req, p_req);
  nm_sr_send_submit(p_onesided->p_session, &p_req->sr_req);
}

void nm_onesided_iget_with_target(nm_onesided_t p_onesided, struct nm_gate_s*p_gate,
                                  void*p_ptr, nm_len_t len, uintptr_t dest_addr,
                                  void*p_target_ptr, nm_len_t target_len,
                                  nm_onesided_request_t*p_req)
{
  if(target_len > NM_ONESIDED_TARGET_COMPLETION_MAX)
    {
      NM_FATAL("target-side completion maxium size = %d; requested = %d\n",
               NM_ONESIDED_TARGET_COMPLETION_MAX, (int)target_len);
    }
  const nm_core_tag_t core_tag = nm_core_tag_build(p_onesided->p_session->hash_code, NM_ONESIDED_TAG_TARGET_REQ);
  const nm_seq_t seq = nm_core_send_seq_get(nm_core_get_singleton(), p_gate, core_tag);
  p_req->header.addr        = dest_addr;
  p_req->header.size        = len;
  p_req->header.flags       = NM_ONESIDED_OP_GET | NM_ONESIDED_FLAG_TARGET_COMPLETION;
  p_req->header.seq         = seq;
  p_req->header.target_size = target_len;
  nm_data_contiguous_build(&p_req->target_data, p_target_ptr, target_len);
  struct nm_data_s data;
  nm_onesided_data_build(&data, &p_req->header, NULL, &p_req->target_data);
  nm_sr_request_t request;
  nm_sr_isend_data(p_onesided->p_session, p_gate, NM_ONESIDED_TAG_REQUEST, &data, &request);
  nm_sr_swait(p_onesided->p_session, &request);
  nm_sr_recv_init(p_onesided->p_session, &p_req->sr_req);
  nm_sr_recv_unpack_contiguous(p_onesided->p_session, &p_req->sr_req, p_ptr, len);
  nm_sr_request_set_ref(&p_req->sr_req, p_req);
  nm_sr_recv_irecv(p_onesided->p_session, &p_req->sr_req, p_gate, NM_ONESIDED_TAG_REPLY_GET, NM_TAG_MASK_FULL);
}

void nm_onesided_target_wait(nm_onesided_target_request_t*p_req)
{
  nm_sr_request_wait(&p_req->local_req);
}

nm_gate_t nm_onesided_target_get_source(nm_onesided_target_request_t*p_req)
{
  nm_gate_t p_gate = nm_sr_request_get_gate(&p_req->local_req);
  return p_gate;
}

uintptr_t nm_onesided_target_get_addr(nm_onesided_target_request_t*p_req)
{
  return p_req->addr;
}

nm_len_t nm_onesided_target_get_size(nm_onesided_target_request_t*p_req)
{
  return p_req->size;
}


/* ********************************************************* */
/* ** blocking functions */

void nm_onesided_put(nm_onesided_t p_onesided, struct nm_gate_s*p_gate,
                     void*p_ptr, nm_len_t len, uintptr_t dest_addr)
{
  nm_onesided_request_t req;
  nm_onesided_iput(p_onesided, p_gate, p_ptr, len, dest_addr, &req);
  nm_onesided_req_wait(&req);
}

void nm_onesided_get(nm_onesided_t p_onesided, struct nm_gate_s*p_gate,
                     void*p_ptr, nm_len_t len, uintptr_t dest_addr)
{
  nm_onesided_request_t req;
  nm_onesided_iget(p_onesided, p_gate, p_ptr, len, dest_addr, &req);
  nm_onesided_req_wait(&req);
}

void nm_onesided_fence(nm_onesided_t p_onesided, struct nm_gate_s*p_gate)
{
  nm_onesided_request_t req;
  nm_onesided_ifence(p_onesided, p_gate, &req);
  nm_onesided_req_wait(&req);
}

void nm_onesided_queue_enqueue(nm_onesided_t p_onesided, struct nm_gate_s*p_gate, nm_onesided_queue_id_t queue,
                               const void*p_ptr, nm_len_t len)
{
  nm_onesided_request_t req;
  nm_onesided_queue_ienqueue(p_onesided, p_gate, queue, p_ptr, len, &req);
  nm_onesided_req_wait(&req);
}

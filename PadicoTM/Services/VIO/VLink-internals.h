/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Internal structures definition for VLink -- This is *not* the public API.
 * @ingroup VIO
 */

#ifndef VLINK_INTERNALS_H
#define VLINK_INTERNALS_H

#include <Padico/Puk.h>
#include <Padico/PadicoTM.h>
#include <Padico/PM2.h>
#include <Padico/Topology.h>
#include <Padico/PadicoControl.h>
#include <Padico/NetSelector.h>

#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <assert.h>
#include <fcntl.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/un.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/mman.h>

#include "VLink-API.h"
#include "VLink-debug.h"


/* ********************************************************* */
/* *** Adapter-specific state management ******************* */

/** This macro creates accessors functions for component-specific status.
 * For component 'foo', the following functions and types are created:
 *  vlink_foo_specific_t
 *  vnode_is_foo()
 *  vnode_foo_check()
 *  vnode_foo_activate()
 *  vnode_foo_deactivate()
 *  vnode_foo()
 */
#define VLINK_CREATE_SPECIFIC_ACCESSORS(NAME) \
typedef struct vlink_##NAME##_specific_s* vobs_##NAME##_t; \
static inline vobs_##NAME##_t NAME##_specific(padico_vnode_t vnode) \
{ return vnode->assembly_stuff.specific; } \
static inline int vnode_is_##NAME(padico_vnode_t vnode) \
{ return (NAME##_specific(vnode) != NULL); } \
static inline void vobs_##NAME##_check(padico_vnode_t vnode) \
{ assert(NAME##_specific(vnode) != NULL); } \
static inline void vobs_##NAME##_activate(padico_vnode_t vnode) \
{ assert(NAME##_specific(vnode) == NULL); \
  vnode->assembly_stuff.specific = padico_malloc(sizeof(struct vlink_##NAME##_specific_s)); } \
static inline void vobs_##NAME##_deactivate(padico_vnode_t vnode) \
{ vobs_##NAME##_check(vnode); padico_free(NAME##_specific(vnode)); vnode->assembly_stuff.specific = NULL; } \
static inline vobs_##NAME##_t vobs_##NAME(padico_vnode_t vnode) \
{ vobs_##NAME##_check(vnode); return NAME##_specific(vnode); }

/** @internal for use within components */
#define vnode_activate(VNODE, FIELD)   (VNODE)->content.FIELD = 1
#define vnode_deactivate(VNODE, FIELD) (VNODE)->content.FIELD = 0



/* ********************************************************* */
/* *** assembly ******************************************** */

extern const struct puk_component_driver_s padico_vnode_component_driver;

/* *** accessor functions */
static inline const struct padico_vlink_driver_s*padico_vnode_get_vlink_driver(padico_vnode_t vnode)
{
  const struct padico_vlink_driver_s*driver = vnode->assembly_stuff.vlink_driver;
  return driver;
}
static inline puk_context_t padico_vnode_get_context(padico_vnode_t vnode)
{
  puk_context_t context = vnode->assembly_stuff.context;
  return context;
}
static inline const char*padico_vnode_component_name(padico_vnode_t vnode)
{
  puk_context_t context = vnode->assembly_stuff.context;
  puk_instance_t instance = vnode->assembly_stuff.instance;
  const char*name = context ? context->component->name : instance->component->name;
  return name;
}

/** returns the instance for the given vnode.
 */
static inline puk_instance_t padico_vnode_get_self_instance(padico_vnode_t vnode)
{
  puk_instance_t instance = vnode->assembly_stuff.instance;
  assert(instance->status == vnode);
  return instance;
}

/** auto-instantiate a vnode-
 * forcibly hooks an component to a vnode (without using NetSelector)
 */
static inline padico_vnode_t padico_vnode_instantiate_entry(puk_component_t component, puk_mod_t owner)
{
  const puk_instance_t instance = puk_component_instantiate_internal(component, NULL, NULL, owner);
  struct puk_receptacle_VLink_s r;
  puk_instance_indirect_VLink(instance, NULL, &r);
  padico_vnode_t vnode = r._status;
  return vnode;
}

/** Clones the given instance to make a new incoming connection, from the bottom-most component
 */
static inline padico_vnode_t padico_vnode_component_acceptor(padico_vnode_t vnode, puk_mod_t owner)
{
  puk_instance_t instance      = padico_vnode_get_self_instance(vnode);
  puk_instance_t container     = puk_instance_get_root_container(instance);
  puk_instance_t new_container = puk_component_instantiate_internal(container->component, NULL, NULL, owner);
  puk_instance_t new_instance  = puk_instance_find_context(new_container, instance);
  padico_vnode_t new_vnode     = puk_instance_self_status(new_instance);
  return new_vnode;
}

/* *** Misc functions for internal use only ***************** */

/* init functions */
void padico_vio_init(void);
void padico_vio_abi_init(void);
void padico_viostat_init(void);

/* VSock toolbox */
void padico_vsock_connector_finalize(padico_vnode_t vnode);
void padico_vsock_acceptor_finalize(padico_vnode_t vnode, padico_vnode_t server_vnode,
                                    const struct sockaddr*addr, int addrlen);
/* vnode-hfd toolbox for all SysIO-based drivers */
void vnode_hfd_create     (padico_vnode_t, int fd, int flags);
int  vnode_hfd_close      (padico_vnode_t);
void vnode_hfd_read_post  (padico_vnode_t);
void vnode_hfd_write_post (padico_vnode_t);
int  vnode_hfd_fcntl      (padico_vnode_t vnode, int cmd, void*arg);

/* temporary */
ssize_t vsock_madio_sendto(padico_vnode_t vnode, const void*buf, size_t len,
                           padico_topo_node_t node, int port);
ssize_t vsock_madio_recvfrom(padico_vnode_t vnode, void*buf, size_t len);


#endif /* VLINK_INTERNALS_H */

/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Debug tools (traces, gdb, logs, ...)
 * @ingroup Puk
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <assert.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <ctype.h>
#include <errno.h>
#include <signal.h>
#include <execinfo.h>

#include "Puk-internals.h"
#include "Module.h"

PADICO_MODULE_HOOK(Puk);

PUK_LIST_TYPE(puk_trace_filter,
              int action; /**< 2: hide, 3: show */
              int level;  /**< -1 matches all */
              char*mod;   /**< NULL matches all */
              char*func;  /**< NULL matches all */
              char*file;  /**< NULL matches all */
              int line;   /**< -1 matches all */
              );
/** information to show on trace messages */
struct puk_trace_info_s
{
  unsigned int level:  1;
  unsigned int mod:    1;
  unsigned int depth:  1;
  unsigned int file:   1;
  unsigned int line:   1;
  unsigned int func:   1;
  unsigned int date:   1;
  unsigned int plugin: 1;
};

struct puk_trace_policy_s
{
  int defaultlevel; /**< default level for traces not filtered */
  puk_trace_filter_list_t filter_list;
  struct puk_trace_info_s show; /**< what field we should show */
};

static struct
{
  /** Current trace policy */
  struct puk_trace_policy_s trace_policy;
  /** plug-in for traces */
  puk_trace_plugin_t trace_plugin;
  /** extension to the hostname to distinguish nodes (typically, mpi rank) */
  char*name_ext;
  /** pid of attached gdb (-1 if none) */
  int gdb_pid;
  /** enable verbosity */
  int verbose;
} puk_debug =
  {
    .trace_policy.filter_list = NULL,
    .trace_policy.defaultlevel = PUK_TRACE_DEFAULT_LEVEL,
    .trace_policy.show.mod = 1,
    .trace_plugin = NULL,
    .name_ext     = NULL,
    .gdb_pid      = -1
  };

/* traces policy example:
  <tracepolicy defaultlevel="10">
    <info file line mod func />
    <show mod="VIO" level="50"/>
    <show file="vnode.c" func="vsock_madw_close"/>
    <show mod="NetAccess"/>
    <hide mod="NetAccess" file="na-tcp.c"/>
  </tracepolicy>

 */

/* ********************************************************* */

void puk_trace_breakpoint(void)
{
  /* empty */
}

/******************************************************/
/* ** display functions*/

void padico_fatal_func(const char*mod_name,
                       const char*function,
                       const char*file,
                       int line,
                       const char*format, ...)
{
  fprintf(stderr, "\n# %s: %s: FATAL- error in %s() %s:%d\n\t",
          padico_hostname(),
          mod_name,
          function, file, line);
  va_list ap;
  va_start(ap, format);
  vfprintf(stderr, format, ap);
  fprintf(stderr, "\n\n");
  va_end(ap);
  void*buffer[100];
  int nptrs = backtrace(buffer, 100);
  backtrace_symbols_fd(buffer, nptrs, STDERR_FILENO);
  fflush(stderr);
  abort();
}

/******************************************************/


static void tracepolicy_start_handler(puk_parse_entity_t e)
{
  const char*defaultlevel = puk_parse_getattr(e, "defaultlevel");
  struct puk_trace_policy_s*policy = padico_malloc(sizeof(struct puk_trace_policy_s));

  policy->filter_list = puk_trace_filter_list_new();
  memset(&policy->show, 0, sizeof(struct puk_trace_info_s));
  if(defaultlevel)
    {
      policy->defaultlevel = atoi(defaultlevel);
    }
  puk_parse_set_content(e, policy);
}
static void tracepolicy_end_handler(puk_parse_entity_t e)
{
  struct puk_trace_policy_s*policy = puk_parse_get_content(e);
  puk_debug.trace_policy = *policy;
  padico_free(policy);
}

static inline int attr_isset(puk_parse_entity_t e, const char*label)
{
  const char*value = puk_parse_getattr(e, label);
  return (value && (strcmp(value, "yes") == 0));
}
static void info_start_handler(puk_parse_entity_t e)
{
  struct puk_trace_info_s*ti = padico_malloc(sizeof(struct puk_trace_info_s));
  ti->level  = attr_isset(e, "level");
  ti->mod    = attr_isset(e, "mod");
  ti->depth  = attr_isset(e, "depth");
  ti->file   = attr_isset(e, "file");
  ti->line   = attr_isset(e, "line");
  ti->func   = attr_isset(e, "func");
  ti->date   = attr_isset(e, "date");
  ti->plugin = attr_isset(e, "plugin");
  puk_parse_set_content(e, ti);
}
static void info_end_handler(puk_parse_entity_t e)
{
  struct puk_trace_info_s*ti = puk_parse_get_content(e);
  struct puk_trace_policy_s*policy = puk_parse_get_content(puk_parse_parent(e));
  if(ti)
    {
      memcpy(&policy->show, ti, sizeof(struct puk_trace_info_s));
      padico_free(ti);
    }
}

static void showhide_start_handler(puk_parse_entity_t e)
{
  puk_trace_filter_t f = puk_trace_filter_new();
  const char*line  = puk_parse_getattr(e, "line");
  const char*level = puk_parse_getattr(e, "level");
  const char*mod   = puk_parse_getattr(e, "mod");
  const char*func  = puk_parse_getattr(e, "func");
  const char*file  = puk_parse_getattr(e, "file");
  f->mod   = mod?padico_strdup(mod):NULL;
  f->func  = func?padico_strdup(func):NULL;
  f->file  = file?padico_strdup(file):NULL;
  f->line  = line?atoi(line):-1;
  f->level = level?atoi(level):-1;
  puk_parse_set_content(e, f);
}
static void showhide_end_handler(puk_parse_entity_t e)
{
  puk_trace_filter_t f = puk_parse_get_content(e);
  struct puk_trace_policy_s*policy = puk_parse_get_content(puk_parse_parent(e));
  if(puk_parse_is(e, "show"))
    {
      f->action = PUK_TRACE_ACTION_SHOW;
    }
  else if(puk_parse_is(e, "hide"))
    {
      f->action = PUK_TRACE_ACTION_HIDE;
    }
  else
    {
      fprintf(stderr, "ooops! tag=%s\n", e->tag);
    }
  puk_trace_filter_list_push_back(policy->filter_list, f);
}

static const struct puk_tag_action_s tracepolicy_action =
{
  .xml_tag        = "tracepolicy",
  .start_handler  = &tracepolicy_start_handler,
  .end_handler    = &tracepolicy_end_handler,
  .required_level = PUK_TRUST_CONTROL
};
static const struct puk_tag_action_s info_action =
{
  .xml_tag        = "info",
  .start_handler  = &info_start_handler,
  .end_handler    = &info_end_handler,
  .required_level = PUK_TRUST_CONTROL
};
static const struct puk_tag_action_s show_action =
{
  .xml_tag        = "show",
  .start_handler  = &showhide_start_handler,
  .end_handler    = &showhide_end_handler,
  .required_level = PUK_TRUST_CONTROL
};
static const struct puk_tag_action_s hide_action =
{
  .xml_tag        = "hide",
  .start_handler  = &showhide_start_handler,
  .end_handler    = &showhide_end_handler,
  .required_level = PUK_TRUST_CONTROL
};

/* ********************************************************* */

enum puk_verbosity_level_e puk_verbose_level(void)
{
  const int debug =
#ifdef PUK_DEBUG
    1
#else
    0
#endif
    ;
  int verbose = debug ? puk_verbose_info : puk_verbose_notice;
  const char*s_verbose = padico_getenv("PADICO_VERBOSE");
  if(s_verbose)
    {
      if(strcmp(s_verbose, "fatal") == 0)
        {
          verbose = puk_verbose_fatal;
        }
      else if(strcmp(s_verbose, "critical") == 0)
        {
          verbose = puk_verbose_critical;
        }
      else if(strcmp(s_verbose, "warning") == 0)
        {
          verbose = puk_verbose_warning;
        }
      else if(strcmp(s_verbose, "notice") == 0)
        {
          verbose = puk_verbose_notice;
        }
      else if(strcmp(s_verbose, "info") == 0)
        {
          verbose = puk_verbose_info;
        }
      else
        {
          verbose = atoi(s_verbose);
          if((verbose == 0) && (strcmp(s_verbose, "0") != 0))
            {
              padico_fatal("Cannot parse %s as a verbosity level.\n", s_verbose);
            }
        }
    }
  return verbose;
}

void padico_puk_debug_init(void)
{
  int verbose = puk_verbose_level();

  puk_xml_add_action(tracepolicy_action);
  puk_xml_add_action(info_action);
  puk_xml_add_action(show_action);
  puk_xml_add_action(hide_action);

  /* load trace policy */
  const char*policy_name = padico_getenv("PADICO_TRACE");
  char*policy_file = NULL;
  if(policy_name != NULL)
    {
      if(policy_name[0] != '/')
        {
          padico_string_t s_traces = padico_string_new();
          padico_string_catf(s_traces, "%s/etc/padico/%s.xml", padico_root(), policy_name);
          policy_file = padico_strdup(padico_string_get(s_traces));
          padico_string_delete(s_traces);
        }
      else
        {
          policy_file = padico_strdup(policy_name);
        }

      struct puk_parse_entity_s e = puk_xml_parse_file(policy_file, PUK_TRUST_CONTROL);
      if(padico_rc_iserror(puk_parse_get_rc(&e)))
        {
          padico_out(puk_verbose_critical, "parse error while parsing traces policy file (%s).\n",
                     padico_rc_gettext(puk_parse_get_rc(&e)));
        }
      padico_out(puk_verbose_notice, "trace policy: %s\n", policy_file?:"(none)");
      padico_free(policy_file);
      if(padico_getenv("PADICO_VERBOSE") != NULL)
        {
          puk_debug.trace_policy.defaultlevel = verbose;
        }
    }
  else
    {
      puk_debug.trace_policy.defaultlevel = verbose;
      padico_out(puk_verbose_info, "verbosity level = %d\n", verbose);
    }

  if(PUK_VG_RUNNING_ON_VALGRIND)
    {
      padico_out(puk_verbose_notice, "running on valgrind with valgrind integration.\n");
    }
}

void padico_puk_debug_finalize(void)
{
  if(puk_debug.name_ext != NULL)
    {
      padico_free(puk_debug.name_ext);
      puk_debug.name_ext = NULL;
    }
  if(puk_debug.gdb_pid != -1)
    {
      kill(puk_debug.gdb_pid, SIGTERM);
    }
  puk_trace_filter_list_delete(puk_debug.trace_policy.filter_list);
  puk_debug.trace_policy.filter_list = NULL;
}

void puk_trace_setplugin(puk_trace_plugin_t plugin)
{
  puk_debug.trace_plugin = plugin;
}

void puk_trace_setnameext(const char*name_ext)
{
  puk_debug.name_ext = padico_strdup(name_ext);
}

void puk_trace_add_filter(int action, int level, const char*mod, const char*func, const char*file, int line)
{
  puk_trace_filter_t f = puk_trace_filter_new();
  assert( (action == PUK_TRACE_ACTION_SHOW) || (action == PUK_TRACE_ACTION_HIDE));
  f->action = action;
  f->level = level;
  f->mod   = mod ? padico_strdup(mod) : NULL;
  f->func  = func ? padico_strdup(func) : NULL;
  f->file  = file ? padico_strdup(file) : NULL;
  f->line  = line;
  puk_trace_filter_list_push_back(puk_debug.trace_policy.filter_list, f);
}

char*puk_trace_policy_serialize(void)
{
  padico_string_t s = padico_string_new();
  padico_string_catf(s, "<?xml version=\"1.0\"?>\n");
  padico_string_catf(s, "<tracepolicy defaultlevel=\"%d\">\n", puk_debug.trace_policy.defaultlevel);
  padico_string_catf(s, "  <info");
  if(puk_debug.trace_policy.show.level)
    padico_string_catf(s, " level=\"yes\"");
  if(puk_debug.trace_policy.show.mod)
    padico_string_catf(s, " mod=\"yes\"");
  if(puk_debug.trace_policy.show.depth)
    padico_string_catf(s, " depth=\"yes\"");
  if(puk_debug.trace_policy.show.file)
    padico_string_catf(s, " file=\"yes\"");
  if(puk_debug.trace_policy.show.line)
    padico_string_catf(s, " line=\"yes\"");
  if(puk_debug.trace_policy.show.func)
    padico_string_catf(s, " func=\"yes\"");
  if(puk_debug.trace_policy.show.date)
    padico_string_catf(s, " date=\"yes\"");
  if(puk_debug.trace_policy.show.plugin)
    padico_string_catf(s, " plugin=\"yes\"");
  padico_string_catf(s, ">\n");
  puk_trace_filter_itor_t i;
  puk_list_foreach(puk_trace_filter, i, puk_debug.trace_policy.filter_list)
    {
      if(i->action == PUK_TRACE_ACTION_SHOW)
        {
          padico_string_catf(s, "  <show");
        }
      else if(i->action == PUK_TRACE_ACTION_HIDE)
        {
          padico_string_catf(s, "  <hide");
        }
      else
        {
          padico_fatal("wrong action in trace filter.\n");
        }
      if(i->level != -1)
        padico_string_catf(s, " level=\"%d\"", i->level);
      if(i->line != -1)
        padico_string_catf(s, " line=\"%d\"", i->line);
      if(i->mod != NULL)
        padico_string_catf(s, " mod=\"%s\"", i->mod);
      if(i->func != NULL)
        padico_string_catf(s, " func=\"%s\"", i->func);
      if(i->file != NULL)
        padico_string_catf(s, " file=\"%s\"", i->file);
      padico_string_catf(s, "/>\n");

    }
  padico_string_catf(s, "</tracepolicy>\n");
  char*r = padico_strdup(padico_string_get(s));
  padico_string_delete(s);
  return r;
}

/* ********************************************************* */

/** @brief apply a trace filter to a context
 * @return 0: hide, 1: show
 */
int puk_trace_filter_apply(int level, const char*mod_name, const char*func, const char*file, int line)
{
  puk_trace_filter_itor_t fi;
  int show = PUK_TRACE_ACTION_DEFAULT;
  if(puk_debug.trace_policy.filter_list != NULL)
    {
      for(fi  = puk_trace_filter_list_begin(puk_debug.trace_policy.filter_list);
          fi != puk_trace_filter_list_end(puk_debug.trace_policy.filter_list);
          fi  = puk_trace_filter_list_next(fi))
        {
          const int matches = (((fi->level == -1)  || (level <= fi->level)) &&
                               ((fi->func == NULL) || (strcmp(func, fi->func) == 0)) &&
                               ((fi->file == NULL) || (strstr(file, fi->file) != NULL)) &&
                               ((fi->line == -1)   || (fi->line == line)) &&
                               ((fi->mod == NULL)  || ((mod_name != NULL) && (strcmp(mod_name, fi->mod) == 0))) );
          if(matches)
            {
              show = fi->action;
            }
        }
    }
  return (show == PUK_TRACE_ACTION_SHOW) ||
    ((show == PUK_TRACE_ACTION_DEFAULT) && (level <= puk_debug.trace_policy.defaultlevel));
}

void padico_out_func(int level,
                     const char*mod_name,
                     const char*func,
                     const char*file,
                     int line,
                     const char*format, ...)
{
  if((func == NULL) || (file == NULL) || (format == NULL))
    {
      /* something is wrong with the args- further interpretation would segfault. */
      return;
    }
  int show = 0;
  if(padico_puk_finalized())
    {
      puk_debug.trace_policy.show.plugin = 0;
      show = (level <= puk_debug.trace_policy.defaultlevel);
    }
  else
    {
      show = puk_trace_filter_apply(level, mod_name, func, file, line);
    }
  if(show)
    {
#define BIG_ENOUGH 4096
      /* it IS safe to use a hard-coded value. Longer messages will be
       * truncated by snprintf/vsnprintf. Static buffer protected by puk_spinlock. */
      char buffer[BIG_ENOUGH];
      char*current = buffer;
      int done = 0;
      va_list ap;
      va_start(ap, format);
      done += snprintf(current, BIG_ENOUGH - done, "# %s: ", padico_hostname());
      current = buffer + strlen(buffer);
      if(puk_debug.name_ext != NULL)
        {
          done += snprintf(current, BIG_ENOUGH - done, "%s ", puk_debug.name_ext);
          current = buffer + strlen(buffer);
        }
      if(level <= puk_verbose_warning)
        {
          const char*label = NULL;
          if(level == puk_verbose_fatal)
            label = "FATAL";
          else if(level == puk_verbose_critical)
            label = "CRITICAL";
          else if(level == puk_verbose_warning)
            label = "WARNING";
          if(label != NULL)
            {
              done += snprintf(current, BIG_ENOUGH - done, "%s- ", label);
              current = buffer + strlen(buffer);
            }
        }
      if(puk_debug.trace_policy.show.plugin)
        {
          if(puk_debug.trace_plugin != NULL)
            {
              done += snprintf(current, BIG_ENOUGH - done, "#%p ", (*puk_debug.trace_plugin)());
              current = buffer + strlen(buffer);
            }
        }
      if(puk_debug.trace_policy.show.date)
        {
          const double now = puk_timing_stamp();
          done += snprintf(current, BIG_ENOUGH - done, "[%9.3f] ", now);
          current = buffer+strlen(buffer);
        }
      if(puk_debug.trace_policy.show.mod && (mod_name != NULL))
        {
          done += snprintf(current, BIG_ENOUGH - done, "%s: ", mod_name);
          current = buffer+strlen(buffer);
        }
      if(puk_debug.trace_policy.show.depth)
        {
          const int bt_size = 1024;
          void*bt_buf[bt_size];
          const int rc = backtrace(bt_buf, bt_size);
          int z;
          for(z = 0; z < rc; z++)
            {
              done += snprintf(current, BIG_ENOUGH - done, ".");
              current = buffer+strlen(buffer);
            }
        }
      if(puk_debug.trace_policy.show.file)
        {
          done += snprintf(current, BIG_ENOUGH - done, "%s:%d ", file, line);
          current = buffer+strlen(buffer);
        }
      if(puk_debug.trace_policy.show.level)
        {
          done += snprintf(current, BIG_ENOUGH - done, "[%d] ", level);
          current = buffer+strlen(buffer);
        }
      if(puk_debug.trace_policy.show.func)
        {
          done += snprintf(current, BIG_ENOUGH - done, "%s() ", func);
          current = buffer+strlen(buffer);
        }
      vsnprintf(current, BIG_ENOUGH - done, format, ap);
      va_end(ap);
      buffer[BIG_ENOUGH-1] = 0;
      fputs(buffer, stderr);
    }
}


/* ********************************************************* */
/* *** gdb */


static void puk_debug_sigcont(int sig)
{
  /* Do nothing, but we _need_ a signal handler to work-around
   * the sigsuspend() bug in Linux 2.6.15.
   */
}

void puk_debug_gdb_attach(void)
{
  char s_padicod_bin[512];
  char s_gdbinit[512];
  char s_name[512];
  char s_pid[10];
  snprintf(s_name,        512, "gdb:%s/%s", padico_hostname(), padico_getenv("PADICO_NODE_UUID"));
  if(padico_getenv("PADICO_LEGACY"))
    {
      snprintf(s_padicod_bin, 512, "%s", padico_getenv("PADICO_LEGACY"));
    }
  else
    {
      snprintf(s_padicod_bin, 512, "%s/libexec/padico/padico-d.bin", padico_root());
    }
  snprintf(s_gdbinit, 512, "%s/etc/padico/Puk.gdbinit", PUK_ROOT);
  snprintf(s_pid,      10, "%d", getpid());
  fprintf(stderr, "%s: Puk: spawning gdb...\n", padico_hostname());

  /* mask SIGCONT */
  struct sigaction oldact, act = { .sa_handler = &puk_debug_sigcont, .sa_flags = 0 };
  sigemptyset(&act.sa_mask);
  sigaction(SIGCONT, &act, &oldact);
  sigset_t set, oldset;
  sigemptyset(&set);
  sigaddset(&set, SIGCONT);
  sigprocmask(SIG_BLOCK, &set, &oldset);
  puk_debug.gdb_pid = fork();
  if(puk_debug.gdb_pid == 0)
    {
      /* restore sigmask */
      sigprocmask(SIG_SETMASK, &oldset, NULL);
      /* restore clean env */
      putenv("LD_PRELOAD=");
      putenv("LD_LIBRARY_PATH=");

      const char*xterm = padico_getenv("XTERM");
#ifdef XTERM
      if(!xterm)
        xterm = XTERM;
#endif
      if(!xterm)
        padico_fatal("XTERM not available; cannot spawn gdb.\n");

      char*const argv[] =
        {
          padico_strdup(xterm),
          "-title", s_name,
          "-e",
          padico_strdup(padico_getenv("GDB")?:GDB),
          "-x", s_gdbinit,
          s_padicod_bin,
          s_pid,
          NULL
        };
      int rc = execv(argv[0], argv);
      if(rc)
        {
          puk_sleep(10);
          padico_fatal("%s", strerror(errno));
        }
    }
  sigemptyset(&set);
  sigaddset(&set, SIGINT);
  sigaddset(&set, SIGCONT);
  sigaddset(&set, SIGTRAP);
  sigaddset(&set, SIGIOT);
  sigwaitinfo(&set, NULL);
  sigaction(SIGCONT, &oldact, NULL);
  sigprocmask(SIG_SETMASK, &oldset, NULL);
  if(padico_getenv("PADICO_LEGACY"))
    {
      puk_sleep(2);
    }
}

void puk_debug_gdb_backtrace(void)
{
  /*
    info threads
    thread apply all bt
   */
  char s_pid[10];
  char s_binary[512];
  FILE*f = fopen("/proc/self/cmdline", "r");
  fgets(s_binary, 512, f);
  fclose(f);
  snprintf(s_pid, 10, "%d", getpid());
  padico_out(puk_verbose_notice, "spawning gdb to dump backtrace...\n");

  int pid = fork();
  if(pid == 0)
    {
      putenv("LD_PRELOAD=");
      putenv("LD_LIBRARY_PATH=");
      char*const argv[] =
        {
          padico_strdup(padico_getenv("GDB")?:GDB),
          "--quiet",
          "--batch",
          "-ex", "info threads",
          "-ex", "thread apply all bt",
          padico_strdup(s_binary),
          s_pid,
          NULL
        };
      int rc = execv(argv[0], argv);
      if(rc)
        {
          puk_sleep(10);
          padico_fatal("%s", strerror(errno));
        }
    }
  else
    {
      sleep(3);
    }
}

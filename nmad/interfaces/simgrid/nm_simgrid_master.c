/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

/** @file
 * starts multiple nodes in the same process for simulation with simgrid
 */


#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <assert.h>
#include <dlfcn.h>

#include <simgrid/engine.h>
#include <simgrid/actor.h>
#include <simgrid/host.h>
#include <simgrid/mutex.h>
#include <simgrid/cond.h>

#include <Padico/Puk.h>
#include <Padico/Puk-mult.h>

static const char*sim_root = NULL;
static const char*sim_binary = NULL;
static int sim_N = -1;

static int sim_argc = -1;
static char**sim_argv = NULL;


/* ********************************************************* */
/* overload Puk mult functions to use simgrid */

int puk_mult_getsize(void)
{
  return sim_N;
}

static inline void puk_mult_attr_destructor(const char*label, const char*value)
{
  padico_free((char*)label);
  padico_free((char*)value);
}

PUK_HASHTABLE_TYPE(puk_mult_attr, const char*, const char*,
                   &puk_hash_string_default_hash, &puk_hash_string_default_eq,
                   &puk_mult_attr_destructor);

static struct
{
  sg_mutex_t lock;
  struct puk_mult_attr_hashtable_s attrs;
  int in_barrier;
  sg_cond_t barrier_cond;
  sg_mutex_t exclusive;
} puk_mult;

void puk_mult_exclusive_lock(void)
{
  sg_mutex_lock(puk_mult.exclusive);
}

void puk_mult_exclusive_unlock(void)
{
  sg_mutex_unlock(puk_mult.exclusive);
}

void puk_mult_publish(const char*label, const char*value)
{
  sg_mutex_lock(puk_mult.lock);
  puk_mult_attr_hashtable_insert(&puk_mult.attrs, padico_strdup(label), padico_strdup(value));
  sg_mutex_unlock(puk_mult.lock);
}

char*puk_mult_lookup(const char*label)
{
  sg_mutex_lock(puk_mult.lock);
  const char*value = puk_mult_attr_hashtable_lookup(&puk_mult.attrs, label);
  sg_mutex_unlock(puk_mult.lock);
  return padico_strdup(value);
}

void puk_mult_barrier(void)
{
  sg_mutex_lock(puk_mult.lock);
  puk_mult.in_barrier++;
  assert(puk_mult.in_barrier <= sim_N);
  if(puk_mult.in_barrier == sim_N)
    {
      puk_mult.in_barrier = 0;
      sg_cond_notify_all(puk_mult.barrier_cond);
    }
  else
    {
      sg_cond_wait(puk_mult.barrier_cond, puk_mult.lock);
    }
  sg_mutex_unlock(puk_mult.lock);
}

static void puk_mult_init(void)
{
  puk_mult.lock = sg_mutex_init();
  puk_mult_attr_hashtable_init(&puk_mult.attrs);
  puk_mult.in_barrier = 0;
  puk_mult.barrier_cond = sg_cond_init();
  puk_mult.exclusive = sg_mutex_init();
}

/* ********************************************************* */

static void nm_simgrid_actor(int argc, char*argv[])
{
  const int n = atoi(argv[1]);

  char*objname = malloc(256);
  snprintf(objname, 256, "%s/lib/sim%d_%s", sim_root, n, sim_binary);

  fprintf(stderr, "# nm_simgrid_run: loading %s\n", objname);
  void*handle = dlopen(objname, RTLD_NOW | RTLD_GLOBAL);
  if(handle == NULL)
    {
      fprintf(stderr, "# cannot open object file %s (%s).\n", objname, dlerror());
      abort();
    }
  char*main_name = malloc(256);
  snprintf(main_name, 256, "sim%d_main", n);
  void*sym = dlsym(handle, main_name);
  if(sym == NULL)
    {
      fprintf(stderr, "# cannot find symbol '%s' in object file %s.\n", main_name, objname);
      abort();
    }
  fprintf(stderr, "# nm_simgrid_run: starting %s...\n", main_name);

  void(*smain)(int, char**) = sym;

  (*smain)(sim_argc, sim_argv);

  free(objname);
  free(main_name);
}

int main(int argc, char*argv[])
{
  const char*platform = getenv("NM_SIMGRID_PLATFORM");
  if(platform == NULL)
    {
      fprintf(stderr, "~ cannot find NM_SIMGRID_PLATFORM in environment.\n");
      abort();
    }

  fprintf(stderr, "# nm_simgrid_run: init with platform = %s.\n", platform);

  simgrid_init(&argc, argv);

  sim_argc = argc;
  sim_argv = argv;

  simgrid_load_platform(platform);

  const char*s_N = getenv("PUK_MULT_N");
  if(s_N == NULL)
    {
      fprintf(stderr, "# cannot find PUK_MULT_N in environment.\n");
      abort();
    }
  sim_N = atoi(s_N);

  sim_root = getenv("PUK_MULT_ROOT");
  if(sim_root == NULL)
    {
      fprintf(stderr, "# cannot find PUK_MULT_ROOT in environment.\n");
      abort();
    }

  sim_binary = getenv("PUK_MULT_BINARY");
  if(sim_binary == NULL)
    {
      fprintf(stderr, "# cannot find PUK_MULT_BINARY in environment.\n");
      abort();
    }

  puk_mult_init();

  size_t count = sg_host_count();
  fprintf(stderr, "# nm_simgrid_run: starting N = %d instances; host count = %lu.\n", sim_N, count);
  sg_host_t*p_hosts = sg_host_list();
  struct nm_simgrid_node_s
  {
    sg_actor_t actor;
    char*p_snum;
    char*p_nodename;
    char*argv[3];
  };
  struct nm_simgrid_node_s*p_nodes = malloc(count * sizeof(struct nm_simgrid_node_s));
  assert(sim_N <= count);
  int n;
  for(n = 0; n < sim_N; n++)
    {
      padico_string_t s = padico_string_new();
      padico_string_printf(s, "node-%d", n);
      p_nodes[n].p_nodename = padico_strdup(padico_string_get(s));
      padico_string_printf(s, "%d", n);
      p_nodes[n].p_snum = padico_strdup(padico_string_get(s));
      padico_string_delete(s);
      fprintf(stderr, "# nm_simgrid_run: starting actor %s\n", p_nodes[n].p_nodename);
      p_nodes[n].argv[0] = "sim";
      p_nodes[n].argv[1] = p_nodes[n].p_snum;
      p_nodes[n].argv[2] = NULL;
      p_nodes[n].actor = sg_actor_create(p_nodes[n].p_nodename, p_hosts[n], &nm_simgrid_actor, 2, p_nodes[n].argv);
    }

  simgrid_run();

  for(n = 0; n < sim_N; n++)
    {
      sg_actor_join(p_nodes[n].actor, -1);
    }

  fprintf(stderr, "# nm_simgrid_run: exit.\n");

  return 0;
}

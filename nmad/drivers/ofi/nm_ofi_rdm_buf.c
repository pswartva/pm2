/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>

#include <Padico/Module.h>
#include <nm_minidriver.h>
#include <nm_private.h>

#include "nm_ofi.h"

/*
 * TODO-
 *  - multiple recv buffers (w/ and w/o FI_MULTI_RECV)
 *  - dynamic/shared send buffer
 *  - fi_progress
 *  - fi_threading
 */

static void*nm_ofi_rdmbuf_instantiate(puk_instance_t instance, puk_context_t context);
static void nm_ofi_rdmbuf_destroy(void*status);

static const struct puk_component_driver_s nm_ofi_rdmbuf_component =
  {
    .instantiate = &nm_ofi_rdmbuf_instantiate,
    .destroy     = &nm_ofi_rdmbuf_destroy
  };

static void nm_ofi_rdmbuf_getprops(puk_context_t context, struct nm_minidriver_properties_s*props);
static void nm_ofi_rdmbuf_init(puk_context_t context, const void**drv_url, size_t*url_size);
static void nm_ofi_rdmbuf_close(puk_context_t context);
static void nm_ofi_rdmbuf_connect(void* _status, const void*remote_url, size_t url_size);
static int  nm_ofi_rdmbuf_send_buf_get(void* _status, void**p_buffer, nm_len_t*p_len);
static int  nm_ofi_rdmbuf_send_buf_post(void*_status, nm_len_t len);
static int  nm_ofi_rdmbuf_send_poll(void*_status);
static int  nm_ofi_rdmbuf_recv_buf_poll(void*_status, void**p_buffer, nm_len_t*p_len);
static int  nm_ofi_rdmbuf_recv_buf_release(void*_status);
static int  nm_ofi_rdmbuf_recv_probe_any(puk_context_t p_context, void**_status);
static int  nm_ofi_rdmbuf_recv_wait_any(puk_context_t p_context, void**_status);
static int  nm_ofi_rdmbuf_recv_cancel(void*_status);
static int  nm_ofi_rdmbuf_recv_cancel_any(puk_context_t p_context);

static const struct nm_minidriver_iface_s nm_ofi_rdmbuf_minidriver =
  {
    .getprops         = &nm_ofi_rdmbuf_getprops,
    .init             = &nm_ofi_rdmbuf_init,
    .close            = &nm_ofi_rdmbuf_close,
    .connect          = &nm_ofi_rdmbuf_connect,
    .send_buf_get     = &nm_ofi_rdmbuf_send_buf_get,
    .send_buf_post    = &nm_ofi_rdmbuf_send_buf_post,
    .send_poll        = &nm_ofi_rdmbuf_send_poll,
    .recv_buf_poll    = &nm_ofi_rdmbuf_recv_buf_poll,
    .recv_buf_release = &nm_ofi_rdmbuf_recv_buf_release,
    .recv_probe_any   = &nm_ofi_rdmbuf_recv_probe_any,
    .recv_wait_any    = &nm_ofi_rdmbuf_recv_wait_any,
    .recv_cancel      = &nm_ofi_rdmbuf_recv_cancel,
    .recv_cancel_any  = &nm_ofi_rdmbuf_recv_cancel_any
  };

/* ********************************************************* */

PADICO_MODULE_COMPONENT(Minidriver_ofi_rdm_buf,
    puk_component_declare("Minidriver_ofi_rdm_buf",
                          puk_component_attr("provider", "auto"),
                          puk_component_attr("fabric", "auto"),
                          puk_component_attr("domain", "auto"),
                          puk_component_provides("PadicoComponent", "component", &nm_ofi_rdmbuf_component),
                          puk_component_provides("NewMad_minidriver", "minidriver", &nm_ofi_rdmbuf_minidriver)));

/* ********************************************************* */

/** buffer size for buffer-based send/recv */
#define NM_OFI_BUF_SIZE (24 * 1024)

static inline uint32_t nm_ofi_addr_hash(fi_addr_t addr)
{
  return puk_hash_default(&addr, sizeof(addr));
}
static inline int nm_ofi_addr_eq(fi_addr_t addr1, fi_addr_t addr2)
{
  return addr1 == addr2;
}

struct nm_ofi_rdmbuf_s;
PUK_HASHTABLE_TYPE2(nm_ofi_rdmbuf_addr, fi_addr_t, FI_ADDR_UNSPEC, struct nm_ofi_rdmbuf_s*,
                   &nm_ofi_addr_hash, &nm_ofi_addr_eq, NULL);

/** 'ofi_rdmbuf' driver per-context data. */
struct nm_ofi_rdmbuf_context_s
{
  struct nm_ofi_common_context_s ofi;
  int next_key;          /**< next requested registration key */
  struct fid_ep*ep;      /**< per-context endpoint, used for FI_EP_RDM type */
  struct fid_av*av;      /**< address vector for RDM type */
  struct fid_cq*recv_cq; /**< completion queue for recv */
  struct fid_cq*send_cq; /**< completion queue for send */
  struct nm_ofi_rdmbuf_s*p_ready_status; /**< next status ready for recv */
  void*url;
  size_t url_size;
  struct nm_ofi_rdmbuf_addr_hashtable_s statuses_by_addr; /**< table of statuses hashed by remote address */
  struct
  {
    char buffer[NM_OFI_BUF_SIZE];
    int posted;                    /**< an fi_recv() has been posted */
    int completed;                 /**< we got a completion for this recv */
    nm_len_t len;                  /**< length actually received */
    struct fid_mr*mr;
    struct nm_ofi_context context; /**< context for ths OFI provider */
  } recv;
};

/** 'ofi_rdmbuf' per-instance status. */
struct nm_ofi_rdmbuf_s
{
  struct nm_ofi_rdmbuf_context_s*p_ofi_context;
  fi_addr_t peer_addr;             /**< peer address */
  struct
  {
    char buffer[NM_OFI_BUF_SIZE];
    int posted;
    int completed;
    nm_len_t len;
    struct fid_mr*mr;
    struct nm_ofi_context context; /**< context for ths OFI provider */
  } send;
};

/* ********************************************************* */

static void* nm_ofi_rdmbuf_instantiate(puk_instance_t instance, puk_context_t context)
{
  struct nm_ofi_rdmbuf_s*p_status = padico_malloc(sizeof(struct nm_ofi_rdmbuf_s));
  struct nm_ofi_rdmbuf_context_s*p_ofi_context = puk_context_get_status(context);
  assert(p_ofi_context != NULL);
  p_status->p_ofi_context = p_ofi_context;
  p_status->send.posted = 0;
  p_status->send.mr = NULL;
  /* register global MR */
  int rc = fi_mr_reg(p_ofi_context->ofi.domain, p_status->send.buffer, NM_OFI_BUF_SIZE, FI_MSG, 0,
                     p_ofi_context->next_key++, 0, &p_status->send.mr, NULL);
  NM_OFI_CHECK_RC(rc, "fi_mr_reg");
  return p_status;
}

static void nm_ofi_rdmbuf_destroy(void*_status)
{
  struct nm_ofi_rdmbuf_s*p_status = _status;
  NM_TRACEF("destroy()\n");
  fi_close(&p_status->send.mr->fid);
  padico_free(p_status);
}

/* ********************************************************* */

static void nm_ofi_rdmbuf_getprops(puk_context_t context, struct nm_minidriver_properties_s*p_props)
{
  struct nm_ofi_rdmbuf_context_s*p_ofi_context = padico_malloc(sizeof(struct nm_ofi_rdmbuf_context_s));
  puk_context_set_status(context, p_ofi_context);

  nm_ofi_common_init(&p_ofi_context->ofi, context, FI_EP_RDM, FI_SOURCE);

  /* fill properties */
  p_props->profile.latency = 500;
  p_props->profile.bandwidth = 8000;
  p_props->capabilities.supports_data = 0;
  p_props->capabilities.prefers_wait_any = 0;
  p_props->capabilities.supports_recv_any = 1;
  p_props->nickname = "ofi_rdma_buf";
  p_props->capabilities.max_msg_size = NM_OFI_BUF_SIZE;
  p_props->capabilities.supports_buf_send = 1;
  p_props->capabilities.supports_buf_recv = 1;
  p_props->capabilities.supports_iovec = 0;
}

static void nm_ofi_rdmbuf_init(puk_context_t context, const void**drv_url, size_t*url_size)
{
  struct nm_ofi_rdmbuf_context_s*p_ofi_context = puk_context_get_status(context);

  nm_ofi_common_open(&p_ofi_context->ofi);

  int rc = fi_endpoint(p_ofi_context->ofi.domain, p_ofi_context->ofi.fi, &p_ofi_context->ep, NULL);
  NM_OFI_CHECK_RC(rc, "fi_endpoint");
  const char*s_size = puk_context_getattr(context, "session_size");
  int session_size = 0;
  if(s_size != NULL)
    {
      session_size = atoi(s_size);
    }
  struct fi_av_attr av_attr =
    {
      .type        = FI_AV_MAP,
      .rx_ctx_bits = 0,
      .count       = session_size,
      .ep_per_node = 0, /* TODO- optimize */
      .name        = NULL, /* TODO- padico UUID */
      .map_addr    = NULL,
      .flags       = 0
    };
  rc = fi_av_open(p_ofi_context->ofi.domain, &av_attr, &p_ofi_context->av, NULL);
  NM_OFI_CHECK_RC(rc, "fi_av_open");
  rc = fi_ep_bind(p_ofi_context->ep, &p_ofi_context->av->fid, 0);
  NM_OFI_CHECK_RC(rc, "fi_ep_bind");

  /* open send & recv CQs */
  struct fi_cq_attr cq_attr;
  memset(&cq_attr, 0, sizeof(cq_attr));
  cq_attr.size = p_ofi_context->ofi.fi->tx_attr->size;
  cq_attr.flags = 0;
  cq_attr.format = FI_CQ_FORMAT_MSG;
  cq_attr.wait_obj = FI_WAIT_NONE;
  rc = fi_cq_open(p_ofi_context->ofi.domain, &cq_attr, &p_ofi_context->send_cq, NULL);
  NM_OFI_CHECK_RC(rc, "fi_cq_open");
  rc = fi_ep_bind(p_ofi_context->ep, &p_ofi_context->send_cq->fid, FI_TRANSMIT);
  NM_OFI_CHECK_RC(rc, "fi_ep_bind");
  rc = fi_cq_open(p_ofi_context->ofi.domain, &cq_attr, &p_ofi_context->recv_cq, NULL);
  NM_OFI_CHECK_RC(rc, "fi_cq_open");
  rc = fi_ep_bind(p_ofi_context->ep, &p_ofi_context->recv_cq->fid, FI_RECV);
  NM_OFI_CHECK_RC(rc, "fi_ep_bind");
  rc = fi_enable(p_ofi_context->ep);
  NM_OFI_CHECK_RC(rc, "fi_enable");

  /* init shared recv */
  p_ofi_context->p_ready_status = NULL;
  p_ofi_context->recv.posted = 0;
  p_ofi_context->recv.mr = NULL;
  rc = fi_mr_reg(p_ofi_context->ofi.domain, p_ofi_context->recv.buffer, NM_OFI_BUF_SIZE, FI_MSG, 0,
                 p_ofi_context->next_key++, 0, &p_ofi_context->recv.mr, NULL);
  NM_OFI_CHECK_RC(rc, "fi_mr_reg");
  nm_ofi_rdmbuf_addr_hashtable_init(&p_ofi_context->statuses_by_addr);

  /* get address */
  p_ofi_context->url_size = 0;
  rc = fi_getname(&p_ofi_context->ep->fid, NULL, &p_ofi_context->url_size);
  if(rc != -FI_ETOOSMALL)
    {
      NM_OFI_CHECK_RC(rc, "fi_getname");
    }
  assert(p_ofi_context->url_size != 0);
  p_ofi_context->url = padico_malloc(p_ofi_context->url_size);
  rc = fi_getname(&p_ofi_context->ep->fid, p_ofi_context->url, &p_ofi_context->url_size);
  NM_OFI_CHECK_RC(rc, "fi_getname");

  *drv_url = p_ofi_context->url;
  *url_size = p_ofi_context->url_size;
  NM_TRACEF("# nm_ofi- init done; url_size = %zd.\n", p_ofi_context->url_size);
}

static void nm_ofi_rdmbuf_close(puk_context_t context)
{
  struct nm_ofi_rdmbuf_context_s*p_ofi_context = puk_context_get_status(context);
  NM_TRACEF("# nm_ofi- closing...\n");
  fi_close(&p_ofi_context->recv.mr->fid);
  puk_context_set_status(context, NULL);
  padico_free(p_ofi_context->url);
  nm_ofi_rdmbuf_addr_hashtable_destroy(&p_ofi_context->statuses_by_addr);
  nm_ofi_common_close(&p_ofi_context->ofi);
  padico_free(p_ofi_context);
}

static void nm_ofi_rdmbuf_connect(void*_status, const void*remote_url, size_t url_size)
{
  struct nm_ofi_rdmbuf_s*p_status = _status;
  struct nm_ofi_rdmbuf_context_s*p_ofi_context = p_status->p_ofi_context;
  NM_TRACEF("# nm_ofi- connecting...\n");

  /* TODO- vectored connection */
  const int count = 1;
  int rc = fi_av_insert(p_ofi_context->av, remote_url, count, &p_status->peer_addr, 0, NULL);
  if(rc != count)
    {
      NM_FATAL("error in fi_av_insert().\n");
    }
  nm_ofi_rdmbuf_addr_hashtable_insert(&p_ofi_context->statuses_by_addr, p_status->peer_addr, p_status);
}

static int nm_ofi_rdmbuf_send_buf_get(void*_status, void**p_buffer, nm_len_t*p_len)
{
  struct nm_ofi_rdmbuf_s*p_status = _status;
  *p_buffer = &p_status->send.buffer;
  *p_len = NM_OFI_BUF_SIZE;
  return NM_ESUCCESS;
}

static int nm_ofi_rdmbuf_send_buf_post(void*_status, nm_len_t len)
{
  struct nm_ofi_rdmbuf_s*p_status = _status;
  assert(len <= NM_OFI_BUF_SIZE);
  p_status->send.len = len;
  p_status->send.posted = 0;
  p_status->send.completed = 0;
  return NM_ESUCCESS;
}

static void nm_ofi_rdmbuf_send_progress(struct nm_ofi_rdmbuf_context_s*p_ofi_context)
{
  struct fi_cq_msg_entry entry;
  int rc = fi_cq_read(p_ofi_context->send_cq, &entry, 1);
  if(rc == 1)
    {
      NM_TRACEF("# nm_ofi- send SUCCESS; len = %d.\n", (int)entry.len);
      assert(entry.flags & FI_MSG);
      assert(entry.flags & FI_SEND);
      struct nm_ofi_rdmbuf_s*p_status = nm_container_of(entry.op_context, struct nm_ofi_rdmbuf_s, send.context);
      p_status->send.completed = 1;
    }
  else if(rc == -FI_EAVAIL)
    {
      /* TODO- fi_cq_readerr() */
      NM_FATAL("fi_cq_read()- asynchronous operation failed.");
    }
  else if(rc != -FI_EAGAIN)
    {
      NM_FATAL("fi_cq_read()- error %d (%s)\n", rc, fi_strerror(-rc));
    }
}

static int nm_ofi_rdmbuf_send_poll(void*_status)
{
  struct nm_ofi_rdmbuf_s*p_status = _status;
  struct nm_ofi_rdmbuf_context_s*p_ofi_context = p_status->p_ofi_context;
  if(!p_status->send.posted)
    {
#ifdef NM_OFI_USE_INJECT
      if(p_status->send.len < p_ofi_context->ofi.fi->tx_attr->inject_size)
        {
          int rc = fi_inject(p_ofi_context->ep, p_status->send.buffer, p_status->send.len, p_status->peer_addr);
          if(rc == FI_SUCCESS)
            {
              return NM_ESUCCESS;
            }
          else if(rc == -FI_EAGAIN)
            {
              nm_ofi_rdmbuf_send_progress(p_ofi_context);
              return -NM_EAGAIN;
            }
          else
            {
              NM_OFI_CHECK_RC(rc, "fi_inject");
            }
        }
      else
#endif /* NM_OFI_USE_INJECT */
        {
          int rc = fi_send(p_ofi_context->ep, p_status->send.buffer, p_status->send.len,
                           fi_mr_desc(p_status->send.mr), p_status->peer_addr, &p_status->send.context);
          if(rc == FI_SUCCESS)
            {
              p_status->send.posted = 1;
            }
          else if(rc == -FI_EAGAIN)
            {
              nm_ofi_rdmbuf_send_progress(p_ofi_context);
              return -NM_EAGAIN;
            }
          else
            {
              NM_OFI_CHECK_RC(rc, "fi_send");
            }
        }
    }
  nm_ofi_rdmbuf_send_progress(p_ofi_context);
  if(p_status->send.completed)
    {
      p_status->send.posted = 0;
      return NM_ESUCCESS;
    }
  else
    {
      return -NM_EAGAIN;
    }
}

static void nm_ofi_rdmbuf_recv_progress(struct nm_ofi_rdmbuf_context_s*p_ofi_context)
{
  /* post requests */
  if(!p_ofi_context->recv.posted)
    {
      int rc = fi_recv(p_ofi_context->ep, &p_ofi_context->recv.buffer[0], NM_OFI_BUF_SIZE,
                       fi_mr_desc(p_ofi_context->recv.mr), FI_ADDR_UNSPEC, &p_ofi_context->recv.context);
      if(rc == -FI_EAGAIN)
        {
          /* network queues full */
          return;
        }
      else
        {
          NM_OFI_CHECK_RC(rc, "fi_recv");
          p_ofi_context->recv.posted = 1;
          p_ofi_context->recv.completed = 0;
        }
    }
  /* poll CQ */
  struct fi_cq_msg_entry entry;
  fi_addr_t addr = FI_ADDR_UNSPEC;
  int rc = fi_cq_readfrom(p_ofi_context->recv_cq, &entry, 1, &addr);
  if(rc == 1)
    {
      NM_TRACEF("# nm_ofi- recv SUCCESS - len = %d.\n", (int)entry.len);
      p_ofi_context->recv.completed = 1;
      p_ofi_context->recv.len = entry.len;
      assert(addr != FI_ADDR_NOTAVAIL);
      assert(addr != FI_ADDR_UNSPEC);
      struct nm_ofi_rdmbuf_s*p_status = nm_ofi_rdmbuf_addr_hashtable_lookup(&p_ofi_context->statuses_by_addr, addr);
      assert(p_status != NULL);
      assert(p_ofi_context->p_ready_status == NULL);
      p_ofi_context->p_ready_status = p_status;
    }
  else if(rc == -FI_EAVAIL)
    {
      struct fi_cq_err_entry err_entry;
      rc = fi_cq_readerr(p_ofi_context->recv_cq, &err_entry, 0);
      NM_FATAL("fi_cq_read()- asynchronous operation failed- err = %d (%s); prov_errno = %d.",
               err_entry.err, fi_strerror(err_entry.err), err_entry.prov_errno);
    }
  else if(rc != -FI_EAGAIN)
    {
      NM_FATAL("fi_cq_read()- error %d (%s)\n", rc, fi_strerror(-rc));
    }
}

static int nm_ofi_rdmbuf_recv_buf_poll(void*_status, void**p_buffer, nm_len_t*p_len)
{
  struct nm_ofi_rdmbuf_s*p_status = _status;
  struct nm_ofi_rdmbuf_context_s*p_ofi_context = p_status->p_ofi_context;
  nm_ofi_rdmbuf_recv_progress(p_ofi_context);
  if(p_ofi_context->p_ready_status == p_status)
    {
      *p_buffer = &p_ofi_context->recv.buffer[0];
      *p_len = p_ofi_context->recv.len;
      return NM_ESUCCESS;
    }
  else
    {
      return -NM_EAGAIN;
    }
}

static int nm_ofi_rdmbuf_recv_buf_release(void*_status)
{
  struct nm_ofi_rdmbuf_s*p_status = _status;
  struct nm_ofi_rdmbuf_context_s*p_ofi_context = p_status->p_ofi_context;
  assert(p_ofi_context->p_ready_status == p_status);
  p_ofi_context->recv.posted = 0;
  p_ofi_context->p_ready_status = NULL;
  return NM_ESUCCESS;
}

static int nm_ofi_rdmbuf_recv_probe_any(puk_context_t p_context, void**_status)
{
  struct nm_ofi_rdmbuf_context_s*p_ofi_context = puk_context_get_status(p_context);
  if(p_ofi_context->p_ready_status == NULL)
    {
      nm_ofi_rdmbuf_recv_progress(p_ofi_context);
    }
  if(p_ofi_context->p_ready_status != NULL)
    {
      *_status = p_ofi_context->p_ready_status;
       return NM_ESUCCESS;
    }
  else
    {
      *_status = NULL;
      return -NM_EAGAIN;
    }
}

static int nm_ofi_rdmbuf_recv_wait_any(puk_context_t p_context, void**_status)
{
  /* TODO */
  return -NM_ENOTIMPL;
}

static int nm_ofi_rdmbuf_recv_cancel(void*_status)
{
  /* TODO */
  return -NM_ENOTIMPL;
}

static int nm_ofi_rdmbuf_recv_cancel_any(puk_context_t p_context)
{
  /* TODO */
  return -NM_ENOTIMPL;
}

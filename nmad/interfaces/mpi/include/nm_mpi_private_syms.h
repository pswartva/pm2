/*
 * NewMadeleine
 * Copyright (C) 2006-2023 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#ifndef NM_MPI_SYMS_H
#define NM_MPI_SYMS_H

#include <nm_mpi.h>

/** @ingroup mpi_private_interface
 * @file
 * @brief internal symbols for MPI functions
 * @{ */


/** defines public symbols MPI_* and PMPI_* as alias on internal symbols mpi_* */
#define NM_MPI_ALIAS(SYM_MPI, SYM_INTERNAL)                             \
  /* enforces symbol type consistency */                                \
  __typeof__(SYM_MPI) SYM_INTERNAL;                                     \
  /* define public MPI_* symbol */                                      \
  __typeof__(SYM_MPI)      SYM_MPI __attribute__ ((alias (#SYM_INTERNAL))) __attribute__ ((weak)); \
  /* define public PMPI_* profiling symbol */                           \
  __typeof__(SYM_MPI) P ## SYM_MPI __attribute__ ((alias (#SYM_INTERNAL)));


/* ********************************************************* */

int mpi_issend(const void* buf,
               int count,
               MPI_Datatype datatype,
               int dest,
               int tag,
               MPI_Comm comm,
               MPI_Request *request);

int mpi_waitsome(int incount,
                 MPI_Request *array_of_requests,
                 int *outcount,
                 int *array_of_indices,
                 MPI_Status *array_of_statuses);

int mpi_init(int *argc,
             char ***argv);

int mpi_init_thread(int *argc,
                    char ***argv,
                    int required __attribute__((unused)),
                    int *provided);

int mpi_query_thread(int*provided);

int mpi_initialized(int *flag);

int mpi_finalize(void);

int mpi_finalized(int*flag);

int mpi_abort(MPI_Comm comm __attribute__((unused)),
              int errorcode);

int mpi_comm_size(MPI_Comm comm, int *size);

int mpi_comm_rank(MPI_Comm comm, int *rank);

int mpi_keyval_create(MPI_Copy_function*copy_fn, MPI_Delete_function*delete_fn, int*keyval, void*extra_state);

int mpi_keyval_free(int*keyval);

int mpi_attr_get(MPI_Comm comm, int keyval, void *attr_value, int *flag );

int mpi_attr_put(MPI_Comm comm, int keyval, void*attr_value);

int mpi_attr_delete(MPI_Comm comm, int keyval);

int mpi_comm_create_keyval(MPI_Comm_copy_attr_function*comm_copy_attr_fn,
                           MPI_Comm_delete_attr_function*comm_delete_attr_fn,
                           int*comm_keyval, void*extra_state);

int mpi_comm_free_keyval(int*comm_keyval);

int mpi_comm_get_attr(MPI_Comm comm, int comm_keyval, void*attr_value, int*flag);

int mpi_comm_set_attr(MPI_Comm comm, int comm_keyval, void*attr_value);

int mpi_comm_delete_attr(MPI_Comm comm, int comm_keyval);

int mpi_comm_set_name(MPI_Comm comm, char*comm_name);

int mpi_comm_get_name(MPI_Comm comm, char*comm_name, int*resultlen);

int mpi_get_processor_name(char *name,
                           int *resultlen);

double mpi_wtime(void);

double mpi_wtick(void);

int mpi_error_class(int errorcode,
                    int*errorclass);

int mpi_error_string(int errorcode,
                     char *string,
                     int *resultlen);

int mpi_comm_set_errhandler(MPI_Comm comm, MPI_Errhandler errhandler);

int mpi_comm_get_errhandler(MPI_Comm comm, MPI_Errhandler*errhandler);

int mpi_comm_call_errhandler(MPI_Comm comm, int errorcode);

int mpi_errhandler_set(MPI_Comm comm,
                       MPI_Errhandler errhandler);

int mpi_errhandler_free(MPI_Errhandler*errhandler);

int mpi_get_version(int *version,
                    int *subversion);

int mpi_get_library_version(char *version, int *resultlen);

int mpi_bsend(const void *buffer,
              int count,
              MPI_Datatype datatype,
              int dest,
              int tag,
              MPI_Comm comm);

int mpi_send(const void *buffer,
             int count,
             MPI_Datatype datatype,
             int dest,
             int tag,
             MPI_Comm comm);

int mpi_isend(const void *buffer,
              int count,
              MPI_Datatype datatype,
              int dest,
              int tag,
              MPI_Comm comm,
              MPI_Request *request);

int mpi_irsend(const void*buffer,
               int count,
               MPI_Datatype datatype,
               int dest,
               int tag,
               MPI_Comm comm,
               MPI_Request*request);

int mpi_rsend(const void* buffer,
              int count,
              MPI_Datatype datatype,
              int dest,
              int tag,
              MPI_Comm comm);

int mpi_ssend(const void* buffer,
              int count,
              MPI_Datatype datatype,
              int dest,
              int tag,
              MPI_Comm comm);

int mpi_pack(const void* inbuf,
             int incount,
             MPI_Datatype datatype,
             void *outbuf,
             int outsize,
             int *position,
             MPI_Comm comm);

int mpi_pack_size(int incount, MPI_Datatype datatype, MPI_Comm comm, int*size);

int mpi_type_set_name(MPI_Datatype datatype, char*type_name);

int mpi_type_get_name(MPI_Datatype datatype, char*type_name, int*resultlen);

int mpi_type_create_keyval(MPI_Type_copy_attr_function*type_copy_attr_fn,
                           MPI_Type_delete_attr_function*type_delete_attr_fn,
                           int*keyval, void*extra_state);

int mpi_type_free_keyval(int*keyval);

int mpi_type_delete_attr(MPI_Datatype datatype, int keyval);

int mpi_type_set_attr(MPI_Datatype datatype, int type_keyval, void*attribute_val);

int mpi_type_get_attr(MPI_Datatype datatype, int type_keyval, void*attribute_val, int*flag);

int mpi_recv(void *buffer,
             int count,
             MPI_Datatype datatype,
             int source,
             int tag,
             MPI_Comm comm,
             MPI_Status *status);

int mpi_irecv(void *buffer,
              int count,
              MPI_Datatype datatype,
              int source,
              int tag,
              MPI_Comm comm,
              MPI_Request *request);

int mpi_sendrecv(const void *sendbuf,
                 int sendcount,
                 MPI_Datatype sendtype,
                 int dest,
                 int sendtag,
                 void *recvbuf,
                 int recvcount,
                 MPI_Datatype recvtype,
                 int source,
                 int recvtag,
                 MPI_Comm comm,
                 MPI_Status *status);

int mpi_sendrecv_replace(void*buf,
                         int count,
                         MPI_Datatype datatype,
                         int dest,
                         int sendtag,
                         int source,
                         int recvtag,
                         MPI_Comm comm,
                         MPI_Status*status);

int mpi_unpack(const void* inbuf,
               int insize,
               int *position,
               void *outbuf,
               int outcount,
               MPI_Datatype datatype,
               MPI_Comm comm);

int mpi_wait(MPI_Request *request,
             MPI_Status *status);

int mpi_waitall(int count,
                MPI_Request *array_of_requests,
                MPI_Status *array_of_statuses);

int mpi_waitany(int count,
                MPI_Request *array_of_requests,
                int *rqindex,
                MPI_Status *status);

int mpi_test(MPI_Request *request,
             int *flag,
             MPI_Status *status);

int mpi_testany(int count,
                MPI_Request *array_of_requests,
                int *rqindex,
                int *flag,
                MPI_Status *status);

int mpi_testall(int count,
                MPI_Request *array_of_requests,
                int *flag,
                MPI_Status *statuses);

int mpi_testsome(int count,
                 MPI_Request *array_of_requests,
                 int *outcount,
                 int *indices,
                 MPI_Status *statuses);

int mpi_iprobe(int source,
               int tag,
               MPI_Comm comm,
               int *flag,
               MPI_Status *status);

int mpi_probe(int source,
              int tag,
              MPI_Comm comm,
              MPI_Status *status);

int mpi_cancel(MPI_Request *request);

int mpi_test_cancelled(const MPI_Status*status,
                       int*flag);

int mpi_request_free(MPI_Request *request);

int mpi_get_count(MPI_Status *status,
                  MPI_Datatype datatype __attribute__((unused)),
                  int *count);

int mpi_get_elements_x(const MPI_Status*status,
                       MPI_Datatype datatype,
                       MPI_Count*count);

int mpi_get_elements(const MPI_Status*status,
                     MPI_Datatype datatype,
                     int*count);

int mpi_request_is_equal(MPI_Request request1,
                         MPI_Request request2);

int mpi_send_init(const void* buf,
                  int count,
                  MPI_Datatype datatype,
                  int dest,
                  int tag,
                  MPI_Comm comm,
                  MPI_Request *request);

int mpi_rsend_init(const void*buffer,
                   int count,
                   MPI_Datatype datatype,
                   int dest,
                   int tag,
                   MPI_Comm comm,
                   MPI_Request*request);

int mpi_ssend_init(const void* buf,
                   int count,
                   MPI_Datatype datatype,
                   int dest,
                   int tag,
                   MPI_Comm comm,
                   MPI_Request *request);

int mpi_recv_init(void* buf,
                  int count,
                  MPI_Datatype datatype,
                  int source,
                  int tag,
                  MPI_Comm comm,
                  MPI_Request *request);

int mpi_improbe(int source,
                int tag,
                MPI_Comm comm,
                int*flag,
                MPI_Message*message,
                MPI_Status*status);

int mpi_mprobe(int source,
               int tag,
               MPI_Comm comm,
               MPI_Message*message,
               MPI_Status*status);

int mpi_imrecv(void*buf,
               int count,
               MPI_Datatype datatype,
               MPI_Message*message,
               MPI_Request*request);

int mpi_mrecv(void*buf,
              int count,
              MPI_Datatype datatype,
              MPI_Message*message,
              MPI_Status*status);

int mpi_start(MPI_Request *request);

int mpi_startall(int count,
                 MPI_Request *array_of_requests);

int mpi_barrier(MPI_Comm comm);

int mpi_ibarrier(MPI_Comm comm,
                 MPI_Request*request);

int mpi_bcast(void* buffer,
              int count,
              MPI_Datatype datatype,
              int root,
              MPI_Comm comm);

int mpi_ibcast(void*buffer,
               int count,
               MPI_Datatype datatype,
               int root,
               MPI_Comm comm,
               MPI_Request*request);

int mpi_gather(const void *sendbuf,
               int sendcount,
               MPI_Datatype sendtype,
               void *recvbuf,
               int recvcount,
               MPI_Datatype recvtype,
               int root,
               MPI_Comm comm);

int mpi_igather(const void*sendbuf,
                int sendcount,
                MPI_Datatype sendtype,
                void*recvbuf,
                int recvcount,
                MPI_Datatype recvtype,
                int root,
                MPI_Comm comm,
                MPI_Request*request);

int mpi_gatherv(const void *sendbuf,
                int sendcount,
                MPI_Datatype sendtype,
                void *recvbuf,
                const int *recvcounts,
                const int *displs,
                MPI_Datatype recvtype,
                int root,
                MPI_Comm comm);

int mpi_allgather(const void *sendbuf,
                  int sendcount,
                  MPI_Datatype sendtype,
                  void *recvbuf,
                  int recvcount,
                  MPI_Datatype recvtype,
                  MPI_Comm comm);

int mpi_iallgather(const void*sendbuf,
                   int sendcount,
                   MPI_Datatype sendtype,
                   void*recvbuf,
                   int recvcount,
                   MPI_Datatype recvtype,
                   MPI_Comm comm,
                   MPI_Request*request);

int mpi_allgatherv(const void *sendbuf,
                   int sendcount,
                   MPI_Datatype sendtype,
                   void *recvbuf,
                   const int *recvcounts,
                   const int *displs,
                   MPI_Datatype recvtype,
                   MPI_Comm comm);

int mpi_scatter(const void *sendbuf,
                int sendcount,
                MPI_Datatype sendtype,
                void *recvbuf,
                int recvcount,
                MPI_Datatype recvtype,
                int root,
                MPI_Comm comm);

int mpi_scatterv(const void*sendbuf, const int sendcounts[], const int displs[], MPI_Datatype sendtype,
                 void*recvbuf, int recvcount, MPI_Datatype recvtype,
                 int root, MPI_Comm comm);

int mpi_alltoall(const void* sendbuf,
                 int sendcount,
                 MPI_Datatype sendtype,
                 void *recvbuf,
                 int recvcount,
                 MPI_Datatype recvtype,
                 MPI_Comm comm);

int mpi_ialltoall(const void*sendbuf, int sendcount, MPI_Datatype sendtype,
                  void*recvbuf, int recvcount, MPI_Datatype recvType,
                  MPI_Comm comm, MPI_Request*request);

int mpi_alltoallv(const void* sendbuf,
                  const int *sendcounts,
                  const int *sdispls,
                  MPI_Datatype sendtype,
                  void *recvbuf,
                  const int *recvcounts,
                  const int *rdispls,
                  MPI_Datatype recvtype,
                  MPI_Comm comm);

int mpi_op_create(MPI_User_function *function,
                  int commute,
                  MPI_Op *op);

int mpi_op_free(MPI_Op *op);

int mpi_reduce(const void* sendbuf,
               void* recvbuf,
               int count,
               MPI_Datatype datatype,
               MPI_Op op,
               int root,
               MPI_Comm comm);

int mpi_reduce_local(const void*sendbuf,
                     void*recvbuf,
                     int count,
                     MPI_Datatype datatype,
                     MPI_Op op);

int mpi_ireduce(const void*sendbuf,
                void*recvbuf,
                int count,
                MPI_Datatype datatype,
                MPI_Op op,
                int root,
                MPI_Comm comm,
                MPI_Request*request);

int mpi_scan(const void*sendbuf,
             void*recvbuf,
             int count,
             MPI_Datatype datatype,
             MPI_Op op,
             MPI_Comm comm);

int mpi_allreduce(const void* sendbuf,
                  void* recvbuf,
                  int count,
                  MPI_Datatype datatype,
                  MPI_Op op,
                  MPI_Comm comm);

int mpi_iallreduce(const void*sendbuf,
                   void*recvbuf,
                   int count,
                   MPI_Datatype datatype,
                   MPI_Op op,
                   MPI_Comm comm,
                   MPI_Request*request);

int mpi_reduce_scatter(const void *sendbuf,
                       void *recvbuf,
                       const int *recvcounts,
                       MPI_Datatype datatype,
                       MPI_Op op,
                       MPI_Comm comm);

int mpi_get_address(void *location,
                    MPI_Aint *address);

int mpi_address(void *location,
                MPI_Aint *address);

int mpi_info_create(MPI_Info*info);

int mpi_info_free(MPI_Info*info);

int mpi_info_dup(MPI_Info info, MPI_Info*newinfo);

int mpi_info_set(MPI_Info info, const char*key, const char*value);

int mpi_info_get(MPI_Info info, const char*key, int valuelen, char*value, int*flag);

int mpi_info_delete(MPI_Info info, const char*key);

int mpi_info_get_nkeys(MPI_Info info, int*nkeys);

int mpi_info_get_nthkey(MPI_Info info, int n, char*key);

int mpi_info_get_valuelen(MPI_Info info, const char*key, int*valuelen, int*flag);

int mpi_pcontrol(const int level, ...);

int mpi_type_size(MPI_Datatype datatype,
                  int *size);

int mpi_type_size_x(MPI_Datatype datatype,
                    MPI_Count*size);

int mpi_type_get_extent(MPI_Datatype datatype,
                        MPI_Aint *lb,
                        MPI_Aint *extent);

int mpi_type_get_extent_x(MPI_Datatype datatype,
                          MPI_Count*lb,
                          MPI_Count*extent);

int mpi_type_extent(MPI_Datatype datatype,
                    MPI_Aint *extent);

int mpi_type_get_true_extent(MPI_Datatype datatype,
                             MPI_Aint*true_lb,
                             MPI_Aint*true_extent);

int mpi_type_get_true_extent_x(MPI_Datatype datatype,
                               MPI_Count*true_lb,
                               MPI_Count*true_extent);

int mpi_type_lb(MPI_Datatype datatype,
                MPI_Aint *lb);

int mpi_type_dup(MPI_Datatype oldtype,
                 MPI_Datatype*newtype);

int mpi_type_create_resized(MPI_Datatype oldtype,
                            MPI_Aint lb,
                            MPI_Aint extent,
                            MPI_Datatype *newtype);

int mpi_type_commit(MPI_Datatype *datatype);

int mpi_type_free(MPI_Datatype *datatype);

int mpi_type_contiguous(int count,
                        MPI_Datatype oldtype,
                        MPI_Datatype *newtype);

int mpi_type_vector(int count,
                    int blocklength,
                    int stride,
                    MPI_Datatype oldtype,
                    MPI_Datatype *newtype);

int mpi_type_hvector(int count,
                     int blocklength,
                     MPI_Aint stride,
                     MPI_Datatype oldtype,
                     MPI_Datatype *newtype);

int mpi_type_indexed(int count,
                     const int*array_of_blocklengths,
                     const int*array_of_displacements,
                     MPI_Datatype oldtype,
                     MPI_Datatype *newtype);

int mpi_type_hindexed(int count,
                      int *array_of_blocklengths,
                      MPI_Aint *array_of_displacements,
                      MPI_Datatype oldtype,
                      MPI_Datatype *newtype);

int mpi_type_struct(int count,
                    int *array_of_blocklengths,
                    MPI_Aint *array_of_displacements,
                    MPI_Datatype *array_of_types,
                    MPI_Datatype *newtype);

int mpi_type_create_subarray(int ndims,
                             const int array_of_sizes[],
                             const int array_of_subsizes[],
                             const int array_of_starts[],
                             int order,
                             MPI_Datatype oldtype,
                             MPI_Datatype*newtype);

int mpi_type_create_darray(int size, int rank, int ndims,
                           const int array_of_gsizes[], const int array_of_distribs[],
                           const int array_of_dargs[], const int array_of_psizes[],
                           int order, MPI_Datatype oldtype, MPI_Datatype*newtype);

int mpi_type_create_struct(int count,
                           int array_of_blocklengths[],
                           MPI_Aint array_of_displacements[],
                           MPI_Datatype array_of_types[],
                           MPI_Datatype *newtype);

int mpi_type_get_envelope(MPI_Datatype datatype,
                          int*num_integers,
                          int*num_addresses,
                          int*num_datatypes,
                          int*combiner);

int mpi_type_get_contents(MPI_Datatype datatype,
                          int max_integers,
                          int max_addresses,
                          int max_datatypes,
                          int array_of_integers[],
                          MPI_Aint array_of_addresses[],
                          MPI_Datatype array_of_datatypes[]);

int mpi_type_create_hindexed(int count,
                             const int array_of_blocklengths[],
                             const MPI_Aint array_of_displacements[],
                             MPI_Datatype oldtype,
                             MPI_Datatype*newtype);

int mpi_type_create_indexed_block(int count,
                                  int blocklength,
                                  const int array_of_displacements[],
                                  MPI_Datatype oldtype,
                                  MPI_Datatype*newtype);

int mpi_type_create_hindexed_block(int count,
                                   int blocklength,
                                   const MPI_Aint array_of_displacements[],
                                   MPI_Datatype oldtype,
                                   MPI_Datatype*newtype);

/* ** large count version of datatype symbols */
int mpi_type_size_c(MPI_Datatype datatype, MPI_Count*size);
int mpi_type_get_extent_c(MPI_Datatype datatype, MPI_Count*lb, MPI_Count*extent);
int mpi_type_get_true_extent_c(MPI_Datatype datatype, MPI_Count*true_lb, MPI_Count*true_extent);
int mpi_type_create_resized_c(MPI_Datatype oldtype, MPI_Count lb, MPI_Count extent, MPI_Datatype*newtype);
int mpi_type_contiguous_c(MPI_Count count, MPI_Datatype oldtype, MPI_Datatype*newtype);
int mpi_type_vector_c(MPI_Count count, MPI_Count blocklength, MPI_Count stride, MPI_Datatype oldtype, MPI_Datatype*newtype);
int mpi_type_create_hvector_c(MPI_Count count, MPI_Count blocklength, MPI_Count hstride, MPI_Datatype oldtype, MPI_Datatype*newtype);
int mpi_type_indexed_c(MPI_Count count, const MPI_Count array_of_blocklengths[], const MPI_Count array_of_displacements[], MPI_Datatype oldtype, MPI_Datatype*newtype);
int mpi_type_create_hindexed_c(MPI_Count count, const MPI_Count array_of_blocklengths[], const MPI_Count array_of_displacements[], MPI_Datatype oldtype, MPI_Datatype*newtype);
int mpi_type_create_indexed_block_c(MPI_Count count, MPI_Count blocklength, const MPI_Count array_of_displacements[], MPI_Datatype oldtype, MPI_Datatype*newtype);
int mpi_type_create_hindexed_block_c(MPI_Count count, MPI_Count blocklength, const MPI_Count array_of_displacements[], MPI_Datatype oldtype, MPI_Datatype*newtype);
int mpi_type_create_struct_c(MPI_Count count, MPI_Count array_of_blocklengths[], MPI_Count array_of_displacements[], MPI_Datatype array_of_types[], MPI_Datatype*newtype);
int mpi_type_create_subarray_c(int ndims, const MPI_Count array_of_sizes[], const MPI_Count array_of_subsizes[],
                               const MPI_Count array_of_starts[], int order, MPI_Datatype oldtype, MPI_Datatype*newtype);
int mpi_pack_c(const void*inbuf, MPI_Count incount, MPI_Datatype datatype, void*outbuf, MPI_Count outsize, MPI_Count*position, MPI_Comm comm);
int mpi_unpack_c(const void*inbuf, MPI_Count insize, MPI_Count*position, void*outbuf, MPI_Count outcount, MPI_Datatype datatype, MPI_Comm comm);
int mpi_pack_size_c(MPI_Count incount, MPI_Datatype datatype, MPI_Comm comm, MPI_Count*size);


int mpi_comm_test_inter(MPI_Comm comm, int*flag);

int mpi_comm_compare(MPI_Comm comm1, MPI_Comm comm2, int*result);

int mpi_comm_group(MPI_Comm comm, MPI_Group *group);

int mpi_comm_create(MPI_Comm comm, MPI_Group group, MPI_Comm*newcomm);

int mpi_comm_create_group(MPI_Comm comm, MPI_Group group, int tag, MPI_Comm*newcomm);

int mpi_comm_split(MPI_Comm comm,
                   int color,
                   int key,
                   MPI_Comm *newcomm);

int mpi_comm_split_type(MPI_Comm oldcomm, int split_type, int key, MPI_Info info, MPI_Comm*newcomm);

int mpi_comm_dup(MPI_Comm comm,
                 MPI_Comm *newcomm);

int mpi_comm_free(MPI_Comm *comm);

int mpi_comm_get_parent(MPI_Comm*parent);

int mpi_comm_remote_size(MPI_Comm comm, int*size);

int mpi_comm_remote_group(MPI_Comm comm, MPI_Group*group);

int mpi_intercomm_create(MPI_Comm local_comm, int local_leader,
                         MPI_Comm parent_comm, int remote_leader,
                         int tag, MPI_Comm*newintercomm);

int mpi_intercomm_merge(MPI_Comm intercomm, int high, MPI_Comm*newcomm);



int mpi_group_size(MPI_Group group, int*size);

int mpi_group_rank(MPI_Group group, int*rank);

int mpi_group_union(MPI_Group group1, MPI_Group group2, MPI_Group*newgroup);

int mpi_group_intersection(MPI_Group group1, MPI_Group group2, MPI_Group*newgroup);

int mpi_group_difference(MPI_Group group1, MPI_Group group2, MPI_Group*newgroup);

int mpi_group_compare(MPI_Group group1, MPI_Group group2, int*result);

int mpi_group_incl(MPI_Group group, int n, const int*ranks, MPI_Group*newgroup);

int mpi_group_range_incl(MPI_Group group, int n, int ranges[][3], MPI_Group*newgroup);

int mpi_group_excl(MPI_Group group, int n, const int*ranks, MPI_Group*newgroup);

int mpi_group_range_excl(MPI_Group group, int n, int ranges[][3], MPI_Group*newgroup);

int mpi_group_free(MPI_Group*group);

int mpi_group_translate_ranks(MPI_Group group1, int n, const int *ranks1, MPI_Group group2, int *ranks2);

int mpi_dims_create(int nnodes, int ndims, int*dims);

int mpi_cart_create(MPI_Comm comm_old, int ndims, const int*dims, const int*periods, int reorder, MPI_Comm*_comm_cart);

int mpi_cart_coords(MPI_Comm comm, int rank, int ndims, int*coords);

int mpi_cart_rank(MPI_Comm comm, const int*coords, int*rank);

int mpi_cart_shift(MPI_Comm comm, int direction, int displ, int*source, int*dest);

int mpi_cart_get(MPI_Comm comm, int maxdims, int*dims, int*periods, int*coords);

int mpi_cart_sub(MPI_Comm comm, const int*remain_dims, MPI_Comm*newcomm);

int mpi_topo_test(MPI_Comm comm, int*topo_type);

int mpi_cartdim_get(MPI_Comm comm, int*ndims);

int mpi_win_create(void *base, MPI_Aint size, int disp_unit, MPI_Info info,
                   MPI_Comm comm, MPI_Win *win);

int mpi_win_allocate(MPI_Aint size, int disp_unit, MPI_Info info,
                     MPI_Comm comm, void *baseptr, MPI_Win *win);

int mpi_win_allocate_shared(MPI_Aint size, int disp_unit, MPI_Info info,
                            MPI_Comm comm, void *baseptr, MPI_Win *win);

int mpi_win_shared_query(MPI_Win win, int rank, MPI_Aint *size,
                         int *disp_unit, void *baseptr);

int mpi_win_create_dynamic(MPI_Info info, MPI_Comm comm, MPI_Win *win);

int mpi_win_attach(MPI_Win win, void *base, MPI_Aint size);

int mpi_win_detach(MPI_Win win, const void *base);

int mpi_win_free(MPI_Win *win);

int mpi_win_get_group(MPI_Win win, MPI_Group *group);

int mpi_win_set_info(MPI_Win win, MPI_Info info);

int mpi_win_get_info(MPI_Win win, MPI_Info *info_used);

int mpi_win_create_keyval(MPI_Win_copy_attr_function*win_copy_attr_fn,
                          MPI_Win_delete_attr_function*win_delete_attr_fn,
                          int*keyval, void*extra_state);

int mpi_win_free_keyval(int*keyval);

int mpi_win_delete_attr(MPI_Win win, int keyval);

int mpi_win_set_attr(MPI_Win win, int win_keyval, void *attribute_val);

int mpi_win_get_attr(MPI_Win win, int win_keyval, void *attribute_val, int *flag);

int mpi_win_set_name(MPI_Win win, char*win_name);

int mpi_win_get_name(MPI_Win win, char*win_name, int*resultlen);

int mpi_win_set_errhandler(MPI_Win win, MPI_Errhandler errhandler);

int mpi_win_get_errhandler(MPI_Win win, MPI_Errhandler*errhandler);

int mpi_win_call_errhandler(MPI_Win win, int errorcode);

int mpi_win_fence(int assert, MPI_Win win);

int mpi_win_start(MPI_Group group, int assert, MPI_Win win);

int mpi_win_complete(MPI_Win win);

int mpi_win_post(MPI_Group group, int assert, MPI_Win win);

int mpi_win_wait(MPI_Win win);

int mpi_win_test(MPI_Win win, int *flag);

int mpi_win_lock(int lock_type, int rank, int assert, MPI_Win win);

int mpi_win_lock_all(int assert, MPI_Win win);

int mpi_win_unlock(int rank, MPI_Win win);

int mpi_win_unlock_all(MPI_Win win);

int mpi_win_flush(int rank, MPI_Win win);

int mpi_win_flush_all(MPI_Win win);

int mpi_win_flush_local(int rank, MPI_Win win);

int mpi_win_flush_local_all(MPI_Win win);

int mpi_win_sync(MPI_Win win);

int mpi_file_open(MPI_Comm comm, const char*filename, int amode, MPI_Info info, MPI_File*fh);

int mpi_file_close(MPI_File*fh);

int mpi_file_delete(const char*filename, MPI_Info info);

int mpi_file_sync(MPI_File fh);

int mpi_file_set_atomicity(MPI_File fh, int flag);

int mpi_file_get_atomicity(MPI_File fh, int*flag);

int mpi_file_set_info(MPI_File fh, MPI_Info info);

int mpi_file_get_info(MPI_File fh, MPI_Info*info_used);

int mpi_file_seek(MPI_File fh, MPI_Offset offset, int whence);

int mpi_file_get_position(MPI_File fh, MPI_Offset*offset);

int mpi_file_get_byte_offset(MPI_File fh, MPI_Offset offset, MPI_Offset*disp);

int mpi_file_preallocate(MPI_File fh, MPI_Offset size);

int mpi_file_set_size(MPI_File fh, MPI_Offset size);

int mpi_file_get_size(MPI_File fh, MPI_Offset*size);

int mpi_file_get_amode(MPI_File fh, int*amode);

int mpi_file_get_group(MPI_File fh, MPI_Group*group);

int mpi_file_get_type_extent(MPI_File fh, MPI_Datatype datatype, MPI_Aint*extent);

int mpi_file_set_view(MPI_File fh, MPI_Offset disp, MPI_Datatype etype, MPI_Datatype filetype, const char*datarep, MPI_Info info);

int mpi_file_get_view(MPI_File fh, MPI_Offset*disp, MPI_Datatype*etype, MPI_Datatype*filetype, char*datarep);

int mpi_file_read(MPI_File fh, void*buf, int count, MPI_Datatype datatype, MPI_Status*status);

int mpi_file_write(MPI_File fh, const void*buf, int count, MPI_Datatype datatype, MPI_Status*status);

int mpi_file_read_at(MPI_File fh, MPI_Offset offset, void*buf, int count, MPI_Datatype datatype, MPI_Status*status);

int mpi_file_write_at(MPI_File fh, MPI_Offset offset, const void*buf, int count, MPI_Datatype datatype, MPI_Status*status);

int mpi_file_read_all(MPI_File fh, void*buf, int count, MPI_Datatype datatype, MPI_Status*status);

int mpi_file_write_all(MPI_File fh, void*buf, int count, MPI_Datatype datatype, MPI_Status*status);

int mpi_file_read_at_all(MPI_File fh, MPI_Offset offset, void*buf, int count, MPI_Datatype datatype, MPI_Status*status);

int mpi_file_write_at_all(MPI_File fh, MPI_Offset offset, const void*buf, int count, MPI_Datatype datatype, MPI_Status*status);

int mpi_file_read_all_begin(MPI_File fh, void*buf, int count, MPI_Datatype datatype);

int mpi_file_read_all_end(MPI_File fh, void*buf, MPI_Status*status);

int mpi_file_write_all_begin(MPI_File fh, const void*buf, int count, MPI_Datatype datatype);

int mpi_file_write_all_end(MPI_File fh, const void*buf, MPI_Status*status);

int mpi_file_read_at_all_begin(MPI_File fh, MPI_Offset offset, void*buf, int count, MPI_Datatype datatype);

int mpi_file_read_at_all_end(MPI_File fh, void*buf, MPI_Status*status);

int mpi_file_write_at_all_begin(MPI_File fh, MPI_Offset offset, const void*buf, int count, MPI_Datatype datatype);

int mpi_file_write_at_all_end(MPI_File fh, const void*buf, MPI_Status*status);

int mpi_file_set_errhandler(MPI_File fh, MPI_Errhandler errhandler);

int mpi_file_get_errhandler(MPI_File fh, MPI_Errhandler*errhandler);

int mpi_file_call_errhandler(MPI_File fh, int errorcode);

int mpi_file_get_position_shared(MPI_File fh, MPI_Offset*offset);

int mpi_file_seek_shared(MPI_File fh, MPI_Offset offset, int whence);

int mpi_file_read_shared(MPI_File fh, void*buf, int count, MPI_Datatype datatype, MPI_Status*status);

int mpi_file_write_shared(MPI_File fh, const void*buf, int count, MPI_Datatype datatype, MPI_Status*status);

int mpi_file_iread_shared(MPI_File fh, void*buf, int count, MPI_Datatype datatype, MPI_Request*request);

int mpi_file_iwrite_shared(MPI_File fh, const void*buf, int count, MPI_Datatype datatype, MPI_Request*request);

int mpi_file_read_ordered(MPI_File fh, void*buf, int count, MPI_Datatype datatype, MPI_Status*status);

int mpi_file_write_ordered(MPI_File fh, const void*buf, int count, MPI_Datatype datatype, MPI_Status*status);

int mpi_file_write_ordered_begin(MPI_File fh, const void*buf, int count, MPI_Datatype datatype);

int mpi_file_write_ordered_end(MPI_File fh, const void*buf, MPI_Status*status);

int mpi_file_read_ordered_begin(MPI_File fh, void*buf, int count, MPI_Datatype datatype);

int mpi_file_read_ordered_end(MPI_File fh, void*buf, MPI_Status*status);

int mpi_file_iwrite(MPI_File fh, const void*buf, int count, MPI_Datatype datatype, MPI_Request*request);

int mpi_file_iread(MPI_File fh, void*buf, int count, MPI_Datatype datatype, MPI_Request*request);

int mpi_file_iwrite_at(MPI_File fh, MPI_Offset offset, const void*buf,
                       int count, MPI_Datatype datatype, MPI_Request*request);

int mpi_file_iread_at(MPI_File fh, MPI_Offset offset, void*buf, int count,
                      MPI_Datatype datatype, MPI_Request*request);

int mpi_put(const void *origin_addr, int origin_count, MPI_Datatype origin_datatype,
            int target_rank, MPI_Aint target_disp, int target_count, MPI_Datatype target_datatype,
            MPI_Win win);

int mpi_get(void *origin_addr, int origin_count, MPI_Datatype origin_datatype,
            int target_rank, MPI_Aint target_disp, int target_count, MPI_Datatype target_datatype,
            MPI_Win win);

int mpi_accumulate(const void *origin_addr, int origin_count, MPI_Datatype origin_datatype,
                   int target_rank, MPI_Aint target_disp, int target_count,
                   MPI_Datatype target_datatype, MPI_Op op, MPI_Win win);

int mpi_get_accumulate(const void *origin_addr, int origin_count, MPI_Datatype origin_datatype,
                       void *result_addr, int result_count, MPI_Datatype result_datatype,
                       int target_rank, MPI_Aint target_disp, int target_count,
                       MPI_Datatype target_datatype, MPI_Op op, MPI_Win win);

int mpi_fetch_and_op(const void *origin_addr, void *result_addr,
                     MPI_Datatype datatype, int target_rank, MPI_Aint target_disp,
                     MPI_Op op, MPI_Win win);

int mpi_compare_and_swap(const void *origin_addr, const void *compare_addr,
                         void *result_addr, MPI_Datatype datatype, int target_rank,
                         MPI_Aint target_disp, MPI_Win win);

int mpi_rput(const void *origin_addr, int origin_count,
             MPI_Datatype origin_datatype, int target_rank,
             MPI_Aint target_disp, int target_count,
             MPI_Datatype target_datatype, MPI_Win win,
             MPI_Request *request);

int mpi_rget(void *origin_addr, int origin_count,
             MPI_Datatype origin_datatype, int target_rank,
             MPI_Aint target_disp, int target_count,
             MPI_Datatype target_datatype, MPI_Win win,
             MPI_Request *request);

int mpi_raccumulate(const void *origin_addr, int origin_count,
                    MPI_Datatype origin_datatype, int target_rank,
                    MPI_Aint target_disp, int target_count,
                    MPI_Datatype target_datatype, MPI_Op op, MPI_Win win,
                    MPI_Request *request);

int mpi_rget_accumulate(const void *origin_addr, int origin_count,
                        MPI_Datatype origin_datatype, void *result_addr,
                        int result_count, MPI_Datatype result_datatype,
                        int target_rank, MPI_Aint target_disp, int target_count,
                        MPI_Datatype target_datatype, MPI_Op op, MPI_Win win,
                        MPI_Request *request);


/** @} */

#endif /* NM_MPI_SYMS_H */

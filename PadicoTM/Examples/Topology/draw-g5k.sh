#! /bin/sh

# example script to draw Grid'5000 frontend topology

#GRAPH=twopi
#GRAPH="sfdp -Goverlap=prism"
GRAPH="neato -Goverlap=prism"

padico-topo-print -ig5k.xml -iflille.xml -ifnancy.xml -iflyon.xml -ifsophia.xml -iftoulouse.xml -ifbordeaux.xml dot - | ${GRAPH} -Tpdf | okular -


/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Padico control channel - Router component
 * @ingroup Control
 */

#include <Padico/Puk.h>
#include <Padico/Module.h>
#include <Padico/PadicoTM.h>
#include <Padico/PadicoControl.h>
#include <Padico/Topology.h>

static int control_router_init(void);
static void control_router_finalize(void);

PADICO_MODULE_DECLARE(Control_Router, control_router_init, NULL, control_router_finalize,
                      "PadicoControl", "Topology", "NetSelector");

/** @defgroup Control_Router component: Control/Router- control channel over a predefined channel
 * @ingroup PadicoComponent
 */

/* ********************************************************* */

/** private data used internally by the Control_Router module.
 */
static struct
{
  int running;
  padico_tm_bgthread_pool_t pool;
  puk_component_t component;      /**< component for control router */
  puk_instance_t instance;        /**< singleton instance of the control router */
  puk_hashtable_t directory;      /**< node -> next hop node */
} router_control = { .running = 0, .component = NULL, .instance = NULL };

/** a received and/or to-send message
 */
struct router_message_s
{
  padico_topo_node_t recipient; /**< final recipient (receiver) */
  const char*from;   /**< name of the originating node */
  char*bytes;        /**< message body */
  size_t size;       /**< message size */
};

/** notification for a lost node
 */
struct disappear_message_s
{
  char*recipient; /**< final recipient (receiver) */
  char*from;      /**< name of the originating node */
};


static void router_message_process(struct router_message_s*message);
static void control_router_event_listener(void*arg);

/* *** PadicoControl driver ******************************** */

static void router_control_send_common(void*_status, padico_topo_node_t node, padico_control_msg_t msg);

/** 'Control' facet for the Control/Router component
 * @ingroup Control_Router
 */
static const struct padico_control_driver_s router_control_driver =
{
  .send_common = &router_control_send_common
};


/* ********************************************************* */
/* *** PadicoControl *************************************** */

static void control_router_table_print(void)
{
#ifdef DEBUG
  padico_topo_node_t node = NULL, next_hop = NULL;
  puk_hashtable_enumerator_t e = puk_hashtable_enumerator_new(router_control.directory);
  puk_hashtable_enumerator_next2(e, &node, &next_hop);
  padico_out(20,"### Routing table:\n");
  while(node)
    {
      padico_out(20," |- dst: %s >> gw: %s\n", padico_topo_node_getname(node), padico_topo_node_getname(next_hop));
      puk_hashtable_enumerator_next2(e, &node, &next_hop);
    }
  puk_hashtable_enumerator_delete(e);
  padico_out(20,"### End\n");
#endif /* DEBUG */
}


static void router_control_send_common(void*_status, padico_topo_node_t recipient, padico_control_msg_t msg)
{
  assert(recipient);
  padico_topo_node_t next_hop = puk_hashtable_lookup(router_control.directory, recipient);
  if(next_hop == recipient)
    {
      padico_fatal("trying to route a message to node %s with an existing direct route.\n",
                   padico_topo_node_getname(recipient));
    }
  if(recipient == padico_topo_getlocalnode())
    {
      padico_fatal("trying to route a message to self.\n");
    }
  /* encode message */
  const struct iovec v = padico_control_msg_get_bytes(msg);
  struct router_message_s message =
    {
      .recipient = recipient,
      .from      = padico_topo_nodename(),
      .bytes     = v.iov_base,
      .size      = v.iov_len
    };
  router_message_process(&message);
}


static void*control_router_disappeared(void*arg)
{
  struct disappear_message_s*message = (struct disappear_message_s*)arg;
  /* We have to warn the sender that the node
     he tried to send a message to is no more reachable */
  padico_topo_node_t node = padico_topo_getnodebyname(message->from);
  if(node != NULL)
    {
      padico_string_t dmessage = padico_string_new();
      padico_control_event_disable();
      padico_string_catf(dmessage, "<Control_Router:disap name=\"%s\" />\n", message->recipient);
      padico_control_send_oneway(node, padico_string_get(dmessage));
      padico_string_delete(dmessage);
      padico_control_event_enable();
    }
  padico_free(message->from);
  padico_free(message->recipient);
  padico_free(message);
  padico_out(20, "done.\n");
  return NULL;
}

/** Process a message received on a router: forward or deliver locally
 */
static void router_message_process(struct router_message_s*message)
{
  /* local node is final recipient- decode and deliver message */
  if(message->recipient == padico_topo_getlocalnode())
    {
      padico_out(50, "deliver message from=%s\n", message->from);
      const padico_topo_node_t from = padico_topo_getnodebyname(message->from);
      padico_control_deliver_message(router_control.instance, from, message->bytes, message->size);
      message->bytes = NULL; /* padico_control_deliver_message() takes ownership of msg body */
    }
  else
    {
      padico_control_event_disable();
      padico_control_event_flush();
      padico_topo_node_t next_hop = puk_hashtable_lookup(router_control.directory, message->recipient);
      padico_control_event_enable();
      /* send to the next hop */
      if(next_hop != NULL)
        {
          padico_out(50, "message from=%s to recipient=%s, send to next hop=%s\n",
                     message->from, padico_topo_node_getname(message->recipient), padico_topo_node_getname(next_hop));
          padico_control_msg_t msg = padico_control_msg_new();
          padico_control_msg_add_cmd(msg, "<Control_Router:message from=\"%s\" recipient=\"%s\">",
                                     message->from, padico_topo_node_getname(message->recipient));
          padico_control_msg_add_part(msg, message->bytes, message->size);
          padico_control_msg_add_cmd(msg, "</Control_Router:message>");
          padico_control_send_msg(next_hop, msg);
          msg = NULL; /* control_send_msg() takes ownership of msg */
        }
      else
        {
          padico_warning("no route found for message- from=%s; recipient=%s\n",
                         message->from, padico_topo_node_getname(message->recipient));
          struct disappear_message_s*dmess = padico_malloc(sizeof(struct disappear_message_s));
          dmess->from      = padico_strdup(message->from);
          dmess->recipient = padico_strdup(padico_topo_node_getname(message->recipient));
          padico_tm_bgthread_start(router_control.pool, &control_router_disappeared, (void*)dmess, "Control-router:node_disappeared");
        }
      padico_free(message->bytes);
    }
}

static void router_message_end_handler(puk_parse_entity_t e)
{
  const char*s_from      = puk_parse_getattr(e, "from");
  const char*s_recipient = puk_parse_getattr(e, "recipient");
  assert(s_from && s_recipient);
  puk_parse_entity_vect_t c = puk_parse_get_contained(e);
  assert(puk_parse_entity_vect_size(c) == 1);
  struct iovec part = puk_parse_getpart(puk_parse_entity_vect_at(c, 0));
  padico_out(40, "received message from node=%s\n", s_from);
  const padico_topo_node_t node = padico_topo_getnodebyname(s_recipient);
  char*bytes = padico_malloc(part.iov_len);
  memcpy(bytes, part.iov_base, part.iov_len);
  if(node != NULL)
    {
      struct router_message_s message =
        {
          .from      = s_from,
          .recipient = node,
          .bytes     = bytes,
          .size      = part.iov_len
        };
      router_message_process(&message);
    }
  else
    {
      struct disappear_message_s*dmess = padico_malloc(sizeof(struct disappear_message_s));
      dmess->from      = padico_strdup(s_from);
      dmess->recipient = padico_strdup(s_recipient);
      padico_tm_bgthread_start(router_control.pool, &control_router_disappeared, (void*)dmess, "Control-router:node_disappeared");
    }
}


/* ********************************************************* */
/* *** Discovery */

/* *********************************************************
 * -1- update: node -> new node:
 *     # update( {known nodes (of the node)} - node - new node)
 * -2- broadcast: new node -> {known nodes (of the new node)} - {known nodes (of the node)}
 *     # announce( {known nodes (of the node)} + node )
 * *********************************************************
 */

static void control_router_broadcast(padico_topo_node_t new_hop, padico_string_t announce)
{
  padico_topo_node_vect_t dests = padico_topo_node_vect_new();
  puk_hashtable_enumerator_t e = puk_hashtable_enumerator_new(router_control.directory);
  padico_topo_node_t node = NULL;
  padico_topo_node_t next_hop = NULL;
  /* build a list of dest nodes */
  padico_out(40, "announce=*%s*\n", padico_string_get(announce));
  puk_hashtable_enumerator_next2(e, &node, &next_hop);
  while(node)
    {
      if(next_hop != new_hop && node != padico_topo_getlocalnode() && node != NULL)
        {
          padico_out(20, "announcing to node: %s\n", padico_topo_node_getname(node));
          padico_topo_node_vect_push_back(dests, node);
          padico_out(20, "announce to node %s done.\n", padico_topo_node_getname(node));
        }
      puk_hashtable_enumerator_next2(e, &node, &next_hop);
    }
  puk_hashtable_enumerator_delete(e);
  /* send announce *after* building the list since control_send()
   * may generate control events that may crash the hashtable enumerator.
   */
  int i;
  for(i = 0; i < padico_topo_node_vect_size(dests); i++)
    {
      node = padico_topo_node_vect_at(dests, i);
      padico_control_send_oneway(node, padico_string_get(announce));
    }
  padico_topo_node_vect_delete(dests);
}

static void router_new_node(padico_topo_node_t node, padico_topo_node_t hop)
{
  assert(node != NULL);
  assert(hop != NULL);
  if(node != padico_topo_getlocalnode())
    {
      const padico_topo_node_t next_hop = puk_hashtable_lookup(router_control.directory, node);
      if(next_hop == NULL)
        {
          puk_hashtable_insert(router_control.directory, node, hop);
          padico_control_new_node(router_control.instance, node);
        }
    }
}

static void router_announce_end_handler(puk_parse_entity_t e)
{
  const char*hop_name = puk_parse_getattr(e, "hop");
  padico_out(20, "received a Control_Router:announce from node=%s\n", hop_name);
  padico_topo_node_t hop = padico_topo_getnodebyname(hop_name);
  puk_parse_entity_vect_t entities = puk_parse_get_contained(e);
  puk_parse_entity_vect_itor_t i;
  padico_control_event_disable();
  puk_vect_foreach(i, puk_parse_entity, entities)
    {
      if(puk_parse_is(*i, "Topology:node"))
        {
          padico_topo_node_t node = puk_parse_get_content(*i);
          router_new_node(node, hop);
        }
    }
  padico_control_event_enable();
}

static void router_update_end_handler(puk_parse_entity_t e)
{
  const char*hop_name = puk_parse_getattr(e, "hop");
  padico_out(20, "received a Control_Router:update- hop=%s\n", hop_name);
  assert(hop_name != NULL);
  padico_topo_node_t hop = padico_topo_getnodebyname(hop_name);
  /* build announce message */
  padico_string_t announce = padico_string_new();
  padico_string_catf(announce, "<Control_Router:announce hop=\"%s\">\n", padico_topo_nodename());
  /* handle embedded entities & on the fly re-serialize */
  puk_parse_entity_vect_t entities = puk_parse_get_contained(e);
  puk_parse_entity_vect_itor_t i;
  padico_control_event_disable();
  puk_vect_foreach(i, puk_parse_entity, entities)
    {
      padico_string_t s_entity = NULL;
      if(puk_parse_is(*i, "Topology:node"))
        {
          padico_topo_node_t node = puk_parse_get_content(*i);
          router_new_node(node, hop);
          s_entity = padico_topo_node_serialize(node);
        }
      else if(puk_parse_is(*i, "Topology:host"))
        {
          padico_topo_host_t host = puk_parse_get_content(*i);
          s_entity = padico_topo_host_serialize(host);
        }
      else if(puk_parse_is(*i, "Topology:network"))
        {
          padico_topo_network_t network = puk_parse_get_content(*i);
          s_entity = padico_topo_network_serialize(network);
        }
      else
        {
          padico_fatal("unexpected entity *%s* in announce.\n", (*i)->tag);
        }
      padico_string_cat(announce, padico_string_get(s_entity));
      padico_string_delete(s_entity);
    }
  padico_string_catf(announce, "</Control_Router:announce>\n");
  padico_control_event_flush();
  control_router_broadcast(hop, announce);
  padico_string_delete(announce);
  padico_control_event_enable();
  padico_out(20, "done.\n");
}

static void cascade_delete_end_handler(puk_parse_entity_t e)
{
  padico_out(20, "received a Control_Router:delete\n");
  puk_parse_entity_vect_t entities = puk_parse_get_contained(e);
  puk_parse_entity_vect_itor_t i;
  puk_vect_foreach(i, puk_parse_entity, entities)
    {
      if(puk_parse_is(*i, "Topology:node"))
        {
          /* Remove concerned routes */
          padico_topo_node_t node = puk_parse_get_content(*i);
          padico_out(20,"Removing node %s\n", padico_topo_node_getname(node));
          padico_topo_node_t start_hop = NULL, next_hop = NULL;
          padico_control_event_disable();
          puk_hashtable_enumerator_t enu = puk_hashtable_enumerator_new(router_control.directory);
          puk_hashtable_enumerator_next2(enu, &start_hop, &next_hop);
          while(start_hop)
            {
              const padico_topo_node_t this_start = start_hop;
              const padico_topo_node_t this_next = next_hop;
              puk_hashtable_enumerator_next2(enu, &start_hop, &next_hop);
              if(this_next == node || this_start == node)
                {
                  puk_hashtable_remove(router_control.directory, this_start);
                }
            }
          puk_hashtable_enumerator_delete(enu);
          padico_control_event_enable();
          padico_control_delete_node(router_control.instance, node);
        }
      else
        {
          padico_fatal("unexpected entity *%s* in delete.\n", (*i)->tag);
        }
    }
}

static void disappear_end_handler(puk_parse_entity_t e)
{
  padico_out(20, "received a Control_Router:disap\n");
  const char*node_name = puk_parse_getattr(e, "name");
  assert(node_name != NULL);
  padico_topo_node_t node = padico_topo_getnodebyname(node_name);
  padico_control_delete_node(router_control.instance, node);
}

static void control_router_event_listener(void*arg)
{
  padico_control_event_t event = (struct padico_control_event_s*)arg;
  padico_out(20, "event kind: %d\n", event->kind);
  switch(event->kind)
    {
    case PADICO_CONTROL_EVENT_INIT_CONTROLER:
        {
          const padico_topo_node_vect_t nodes = event->INIT_CONTROLER.nodes;
          const puk_instance_t controler __attribute__((unused)) = event->INIT_CONTROLER.controler;
          padico_topo_node_vect_itor_t i;
          puk_vect_foreach(i, padico_topo_node, nodes)
            {
              const padico_topo_node_t node = *i;
              /* if the node and the next_hop are the same : the node is directly reachable */
              puk_hashtable_insert(router_control.directory, node, node);
              padico_out(40, "INIT_CONTROLER: register new node: %s from controler: %s\n",
                         padico_topo_node_getname(node), controler->component->name);
            }
        }
      break;

    case PADICO_CONTROL_EVENT_NEW_NODE:
        {
          const puk_instance_t controler = event->NODE.controler;
          const padico_topo_node_t node = event->NODE.node;
          padico_out(20, "NEW_NODE: %s\n", padico_topo_node_getname(node));
          assert(node != NULL);
          if(controler->component != router_control.component) /* avoid recursion */
            {
              const padico_topo_node_t next_hop = puk_hashtable_lookup(router_control.directory, node);
              if(next_hop == NULL)
                {
                  padico_out(40, "NEW_NODE: register new node: %s from controler: %s\n",
                             padico_topo_node_getname(node), controler->component->name);
                  puk_hashtable_insert(router_control.directory, node, node);
                  /* force all control events flush- following event listeners in the queue
                   * are likely to update NetSelector needed to send requests (announce/update) */
                  padico_control_event_sync();
                  /* build & send an update request */
                  padico_string_t request = padico_string_new();
                  padico_string_t topology = padico_na_topology_serialize(PADICO_TOPO_SCOPE_ALL);
                  padico_out(20, "send topology to new node %s \n", padico_topo_node_getname(node));
                  padico_string_catf(request, "<Control_Router:update hop=\"%s\">\n", padico_topo_nodename());
                  padico_string_catf(request, padico_string_get(topology));
                  padico_string_catf(request, "</Control_Router:update>\n");
                  padico_string_delete(topology);
                  const char*msg = padico_string_get(request);
                  padico_out(60, "request: %s\n", msg);
                  padico_out(20, "sending.\n");
                  padico_control_send_oneway(node, msg);
                  padico_out(20, "sent.\n");
                  padico_string_delete(request);
                }
            }
        }
        break;

    case PADICO_CONTROL_EVENT_DELETE_NODE:
      {
        padico_out(10, "node event- delete node *%s*\n", padico_topo_node_getname(event->NODE.node));
        padico_topo_node_t node = NULL, next_hop = NULL;
        puk_hashtable_enumerator_t e = puk_hashtable_enumerator_new(router_control.directory);
        puk_hashtable_enumerator_next2(e, &node, &next_hop);
        padico_topo_node_vect_t nodes_lost = padico_topo_node_vect_new();
        while(node)
          {
            if(next_hop == event->NODE.node)
              {
                padico_topo_node_vect_push_back(nodes_lost, node);
              }
            puk_hashtable_enumerator_next2(e, &node, &next_hop);
          }
        puk_hashtable_enumerator_delete(e);
        /* cascade delete */
        int i = 0;
        padico_string_t delete = padico_string_new();
        padico_control_event_disable();
        padico_string_catf(delete, "<Control_Router:delete hop=\"%s\">\n", padico_topo_nodename());
        for(i = 0; i < padico_topo_node_vect_size(nodes_lost); i++)
          {
            padico_topo_node_t node = padico_topo_node_vect_at(nodes_lost, i);
            padico_string_t node_string = padico_topo_node_serialize(node);
            padico_string_catf(delete, padico_string_get(node_string));
            puk_hashtable_remove(router_control.directory, node);
            padico_string_delete(node_string);
          }
        padico_string_catf(delete, "</Control_Router:delete>\n");
        if(router_control.running)
          {
            control_router_broadcast(NULL, delete);
          }
        padico_string_delete(delete);
        padico_control_event_enable();
        padico_topo_node_vect_delete(nodes_lost);
        padico_out(20, "done.\n");
      }
      break;

    case PADICO_CONTROL_EVENT_SHUTDOWN:
      router_control.running = 0;
      break;

    default:
      break;
    }
}


/* ********************************************************* */

static int control_router_init(void)
{
  router_control.pool = padico_tm_bgthread_pool_create("Control-router");
  router_control.component =
    puk_component_declare("Control_Router",
                          puk_component_provides("PadicoControl", "control", &router_control_driver));

  puk_xml_add_action((struct puk_tag_action_s){
                     .xml_tag        = "Control_Router:update",
                     .start_handler  = NULL,
                     .end_handler    = &router_update_end_handler,
                     .required_level = PUK_TRUST_CONTROL
                     });
  puk_xml_add_action((struct puk_tag_action_s){
                     .xml_tag        = "Control_Router:announce",
                     .start_handler  = NULL,
                     .end_handler    = &router_announce_end_handler,
                     .required_level = PUK_TRUST_CONTROL
                     });
  puk_xml_add_action((struct puk_tag_action_s){
                     .xml_tag        = "Control_Router:message",
                     .start_handler  = NULL,
                     .end_handler    = &router_message_end_handler,
                     .required_level = PUK_TRUST_CONTROL
                     });
  puk_xml_add_action((struct puk_tag_action_s){
                     .xml_tag        = "Control_Router:delete",
                     .start_handler  = NULL,
                     .end_handler    = &cascade_delete_end_handler,
                     .required_level = PUK_TRUST_CONTROL
                     });
  puk_xml_add_action((struct puk_tag_action_s){
                     .xml_tag        = "Control_Router:disap",
                     .start_handler  = NULL,
                     .end_handler    = &disappear_end_handler,
                     .required_level = PUK_TRUST_CONTROL
                     });

  router_control.directory = puk_hashtable_new_ptr();
  router_control.instance = puk_component_instantiate(router_control.component);
  padico_control_plug_controler(router_control.instance, NULL);

  padico_control_event_subscribe(&control_router_event_listener);

  router_control.running = 1;

  return 0;
}

static void control_router_finalize(void)
{
  padico_control_event_sync();
  padico_control_unplug_controler(router_control.instance);
  padico_control_event_unsubscribe(&control_router_event_listener);
  padico_control_event_sync();
  padico_tm_bgthread_pool_wait(router_control.pool);
  puk_instance_destroy(router_control.instance);
  puk_hashtable_delete(router_control.directory, NULL);
  puk_xml_delete_action("Control_Router:update");
  puk_xml_delete_action("Control_Router:announce");
  puk_xml_delete_action("Control_Router:message");
  puk_xml_delete_action("Control_Router:delete");
  puk_xml_delete_action("Control_Router:disap");
  puk_component_destroy(router_control.component);
}

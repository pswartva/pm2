/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief LZ4 Packet Filter component
 */

#include <Padico/Module.h>
#include "PacketFilter.h"
#include <lz4.h>


/* ********************************************************* */
/* *** LZ4 Packet Filter *********************************** */
/* ********************************************************* */


/** @defgroup PacketFilter_LZ4 component: PacketFilter_LZ4- a packet filter that performs LZ4 compression
 * @ingroup PadicoComponent
 */

static void*lz4pf_instantiate(puk_instance_t assembly, puk_context_t context);
static void lz4pf_destroy(void*state);
static void lz4pf_init(void);
static void lz4pf_encode(void*, const void*, size_t, void**, size_t*);
static void lz4pf_decode(void*, const void*, size_t, void*, size_t*);

/** instanciation facet for PacketFilter_LZ4
 * @ingroup PacketFilter_LZ4
 */
static const struct puk_component_driver_s lz4_component_driver =
  {
    .instantiate    = &lz4pf_instantiate,
    .destroy        = &lz4pf_destroy,
    .component_init = &lz4pf_init
  };
/** 'PacketFilter' facet for PacketFilter_LZ4
 * @ingroup PacketFilter_LZ4
 */
static const struct packetfilter_driver_s lz4pf_driver =
  {
    .pf_encode = &lz4pf_encode,
    .pf_decode = &lz4pf_decode
  };

/* ********************************************************* */

static void lz4pf_init(void)
{
  padico_out(puk_verbose_notice, "using LZ4 version %d.%d.%d\n", LZ4_VERSION_MAJOR, LZ4_VERSION_MINOR, LZ4_VERSION_RELEASE);
}

PADICO_MODULE_COMPONENT(PacketFilter_LZ4,
                        puk_component_declare("PacketFilter_LZ4",
                                              puk_component_provides("PadicoComponent", "component", &lz4_component_driver),
                                              puk_component_provides("PacketFilter", "pf", &lz4pf_driver)));

/* ********************************************************* */

/** instance status for PacketFilter_LZ4
 * @ingroup PacketFilter_LZ4
 */
struct lz4_state_s
{
  int dummy;
};


static void* lz4pf_instantiate(puk_instance_t assembly, puk_context_t context)
{
  struct lz4_state_s*state = padico_malloc(sizeof(struct lz4_state_s));
  state->dummy = 0;
  return state;
}

static void lz4pf_destroy(void*state)
{
  assert(state != NULL);
  padico_free(state);
}


/* 'out' allocated by encode function
 */
static void lz4pf_encode(void*_status,
                         const void*in, size_t in_size,
                         void**out, size_t* out_size)
{
  unsigned int s = LZ4_COMPRESSBOUND(in_size);
  void*p = padico_malloc(s);
  int rc = LZ4_compress_default((void*)in, p, in_size, s);
  if(rc > 0)
    {
      /* success */
      *out = p;
      *out_size = rc;
    }
  else
    {
      /* compression failed: don't compress */
      padico_fatal("compression failed- rc=%d\n", rc);
      memcpy(p, in, in_size);
      *out = p;
      *out_size = in_size;
    }
}

/* 'out' allocated by caller
*/
static void lz4pf_decode(void*_status,
                          const void*in, size_t in_size,
                          void*out, size_t* out_size)
{
  unsigned int s = *out_size;
  int rc = LZ4_decompress_safe((void*)in, out, in_size, *out_size);
  if(rc == 0)
    {
      padico_fatal("CRC error while uncompressing- in_size=%d; out_size=%d; rc=%d\n",
                   (int)in_size, (int)*out_size, rc);
    }
  *out_size = s;
}

/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Puk components definition and API
 * @note shouldn't be included directly: #include <Padico/Puk.h> instead.
 * @ingroup Puk
 */

#ifndef PUK_COMPONENTS_H
#define PUK_COMPONENTS_H


/** @defgroup PadicoComponents API: Puk components description
 * @ingroup Puk
 */

/** @defgroup PadicoComponentRegistry API: Puk components registry- registration and query
 * @ingroup Puk
 */

/** @defgroup PadicoAssembly API: Puk components assembly and instanciation interface
 * @see @ref PadicoComponents
 * @ingroup Puk
 */

/** @defgroup PadicoComponent List of Puk components
 * @ingroup Puk
 */

/* ********************************************************* */
/* *** Components description ****************************** */


/** @addtogroup PadicoComponents
 * @{
 */

/* *** Types *********************************************** */

typedef struct puk_iface_s*puk_iface_t;
typedef struct puk_facet_s*puk_facet_t;
typedef struct puk_component_s*puk_component_t;

/* *** Facets and attributes ******************************* */

/** An interface from a component
 */
struct puk_iface_s
{
  const char*name;
};
PUK_VECT_TYPE(puk_iface, struct puk_iface_s*)

/** Attach a driver to an iface (e.g. for iface 'foo'):
 *    -- create an iface lookup function
 *    -- create the receptacle type
 *    -- typed context and instance indirections
 * Prototypes:
 *   padico_iface_t padico_iface_foo(void);
 *   struct puk_receptacle_foo_s;
 *   void puk_instance_indirect_foo(puk_instance_t, struct puk_receptacle_foo_s*);
 *   void puk_context_indirect_foo(puk_instance_t, const char*label, struct puk_receptacle_foo_s*);
 *   const struct foo_driver_s*puk_component_get_driver_foo(puk_component_t, const char*label);
 */
#define PUK_IFACE_TYPE(IFACE_NAME, DRIVER_TYPE) \
  static inline puk_iface_t puk_iface_##IFACE_NAME(void) \
  { static puk_iface_t _iface = NULL; \
    if(!_iface) _iface = puk_iface_register(#IFACE_NAME); \
    return _iface; } \
  struct puk_receptacle_##IFACE_NAME##_s \
  { \
    DRIVER_TYPE*driver; \
    void*_status; \
  }; \
  /* returns a null value with correct iface type */                    \
  static inline struct puk_receptacle_##IFACE_NAME##_s puk_receptacle_##IFACE_NAME##_null(void) \
  {                                                                     \
    struct puk_receptacle_##IFACE_NAME##_s r;                           \
    r.driver = NULL;                                                    \
    r._status = NULL;                                                   \
    return r;                                                           \
  }                                                                     \
  /* finds the entry point for the instance, with given label and of given iface type */ \
  static inline void puk_instance_indirect_##IFACE_NAME(puk_instance_t instance, const char*label, \
                                                        struct puk_receptacle_##IFACE_NAME##_s*receptacle) \
  { puk_instance_entry_point(instance, puk_iface_##IFACE_NAME(), label, &receptacle->driver, &receptacle->_status); } \
  /* resolves a receptacle for a local instance */                      \
  static inline void puk_context_indirect_##IFACE_NAME(puk_instance_t instance, const char*label, \
                                                       struct puk_receptacle_##IFACE_NAME##_s*receptacle) \
  { puk_context_indirect(instance, puk_iface_##IFACE_NAME(), label, &receptacle->driver, &receptacle->_status); } \
  static inline const DRIVER_TYPE*puk_component_get_driver_##IFACE_NAME(puk_component_t component, const char*label) \
  { puk_facet_t f = puk_component_get_facet(component, puk_iface_##IFACE_NAME(), label); \
    const DRIVER_TYPE*d = (const DRIVER_TYPE*)(f?f->driver:NULL); \
    return d; } \
  static inline puk_facet_t puk_component_get_facet_##IFACE_NAME(puk_component_t c, const char*label) \
  { return puk_component_get_facet(c, puk_iface_##IFACE_NAME(), label); }

/** A facet from a component
 */
struct puk_facet_s
{
  puk_iface_t iface; /**< interface exposed by the facet */
  const char*label;  /**< label (name) of the facet */
  const void*driver; /**< implementation of the facet */
};
PUK_VECT_TYPE(puk_facet, struct puk_facet_s)

/** A receptacle from a component
 */
struct puk_uses_s
{
  puk_iface_t iface;
  const char*label;
};
PUK_VECT_TYPE(puk_uses, struct puk_uses_s)

/* *** Component ******************************************* */

void puk_attr_destructor(char*label, char*value);

PUK_HASHTABLE_TYPE(puk_attr, char*, char*,
                   &puk_hash_string_default_hash, &puk_hash_string_default_eq, &puk_attr_destructor);

/** An abstract component descriptor
 */
struct puk_component_s
{
  const char*name;                  /**< entry name (eg. "VLink-Packet") )*/
  puk_attr_hashtable_t attrs;       /**< attributes table: label -> value @note for undef attr: value=&attr_undefined */
  struct puk_uses_vect_s  uses;     /**< the interfaces needed by this component */
  struct puk_facet_vect_s provides; /**< the interfaces provided by this component */
  puk_mod_t home;     /**< home module managing this component */
};
PUK_VECT_TYPE(puk_component, struct puk_component_s*)

puk_facet_t puk_component_get_facet(puk_component_t a, puk_iface_t iface, const char*label);

/** get the value of an attribute in a component & recursively in its contexts */
const char*puk_component_getattr(puk_component_t c, const char*label);

/** set the value of an attribute in a component & recursively in its contexts
 * @note sets the value of an _existing_ attribute, do not create a new label
 */
void puk_component_setattr(puk_component_t c, const char*label, const char*value);

/** add an attribute to a component & recursively in its contexts.
 */
void puk_component_addattr(puk_component_t c, const char*label, const char*value);

/** list attrs & values from a component */
void puk_component_listattrs(puk_component_t c, const char**labels, const char**values, int*n);

/** unload all components with mod as their home */
void puk_component_home_flush(puk_mod_t mod);

/** @} */

/* *** Register & query ************************************ */

/** @addtogroup PadicoComponentRegistry
 * @{
 */

/* ** interface registry */

puk_iface_t puk_iface_register(const char*name);
puk_iface_t puk_iface_lookup(const char*name);

/* ** component registry */

/** @internal resolve a component by name and hook it to its containing module */
puk_component_t puk_component_resolve_internal(const char*component_name, puk_mod_t owner);
/** resolve a component by name (end-user interface) */
#define puk_component_resolve(NAME) puk_component_resolve_internal((NAME), padico_module_self())

void puk_component_destroy(puk_component_t c);


/* *** Structure for ports declaration ********************* */

struct puk_component_port_s
{
  enum { PUK_COMPONENT_PROVIDES, PUK_COMPONENT_USES, PUK_COMPONENT_ATTR } kind;
  const char*label;
  union
  {
    struct
    {
      const char*iface_name;
      const void*driver;
    } provides;
    struct
    {
      const char*iface_name;
    } uses;
    struct
    {
      const char*value;
    } attr;
  } content;
};
#define puk_component_provides(IFACE_NAME, PORT_LABEL, DRIVER) \
  ((struct puk_component_port_s){ \
    .kind = PUK_COMPONENT_PROVIDES, \
    .label = (PORT_LABEL), \
    .content = {.provides = { .iface_name = (IFACE_NAME), .driver = (DRIVER)} }})

#define puk_component_uses(IFACE_NAME, PORT_LABEL) \
  ((struct puk_component_port_s){ \
    .kind = PUK_COMPONENT_USES, \
    .label = (PORT_LABEL), \
    .content = { .uses = { .iface_name = (IFACE_NAME) } } })

#define puk_component_attr(LABEL, VALUE) \
  ((struct puk_component_port_s){ \
    .kind = PUK_COMPONENT_ATTR, \
    .label = (LABEL), \
    .content = { .attr = { .value = (VALUE) } } })

puk_component_t puk_component_declare2(const char component_name[], puk_mod_t home_mod,
                                       int n_ports, const struct puk_component_port_s ports[]);

#define puk_component_declare(COMPONENT_NAME, ...) \
  puk_component_declare2((COMPONENT_NAME), padico_module_self(), \
    sizeof((struct puk_component_port_s[]){ __VA_ARGS__ }) / \
     sizeof(struct puk_component_port_s), \
    ((struct puk_component_port_s[]){ __VA_ARGS__ }))

/** @} */


/* ********************************************************* */
/* *** Assembly ******************************************** */

/** @addtogroup PadicoAssembly
 * @{
 */

/* *** Types ********************************************* */

typedef struct puk_component_conn_s*puk_component_conn_t;
typedef struct puk_component_context_s*puk_context_t;
typedef struct puk_instance_s*puk_instance_t;

/* *** Component context ********************************* */

/** a component port connection, between a receptacle and a facet */
struct puk_component_conn_s
{
  struct puk_uses_s*receptacle; /**< receptacle of the user component */
  struct puk_facet_s*facet;     /**< facet of the provider component */
  puk_context_t context;        /**< context of the provider component */
};
PUK_VECT_TYPE(puk_component_conn, struct puk_component_conn_s)


/** a context for a component in an assembly
 * (connections, value of attributes, containing composite)
 */
struct puk_component_context_s
{
  const char*id;              /**< context ID, as given in the XML assembly description */
  puk_component_t component;  /**< the component this context corresponds to */
  puk_component_t assembly;   /**< the assembly this context belongs to */
  struct puk_component_conn_vect_s conns; /**< interfaces connections */
  puk_attr_hashtable_t attrs; /**< attributes */
  void*status;                /**< component context status */
};
PUK_VECT_TYPE(puk_context, puk_context_t)

/** get the value of an attribute in a context */
const char*puk_context_getattr(puk_context_t context, const char*label);

/** store an attribute in a context */
void puk_context_putattr(puk_context_t context, const char*label, const char*value);

/** get the context status */
static inline void*puk_context_get_status(puk_context_t context)
{
  return context->status;
}
/** set the context status */
static inline void puk_context_set_status(puk_context_t context, void*_status)
{
  context->status = _status;
}
/** get a receptacle of a component context */
puk_component_conn_t puk_context_conn_lookup(puk_context_t context, puk_iface_t iface, const char*label);

/** get the inner context of a component providing the given interface */
puk_context_t puk_component_get_context(puk_component_t component, puk_iface_t iface, const char*label);

/** get the context of a component attached to a receptacle for the local context */
puk_context_t puk_context_get_subcontext(puk_context_t context, puk_iface_t iface, const char*label);

/** get all the inner contexts of a composite */
puk_context_vect_t puk_component_get_contexts(puk_component_t component);

/** create a new empty context attached to a component */
puk_context_t puk_context_new(puk_component_t component, puk_component_t composite, const char*context_id);

/** destroy manually created contexts */
void puk_context_destroy(puk_context_t context);

/** connect a receptacle in te given context to the given port of the provider context */
void puk_context_conn_connect(puk_context_t user, const char*port_name, const char*provider_id, puk_iface_t iface, const char*provider_port);

/* *** Composite ******************************************* */

/** a component made of several components and their connections */
struct puk_composite_content_s
{
  struct puk_composite_driver_s
  {
    struct puk_composite_content_s*content;
  } driver;
  struct puk_component_conn_vect_s entry_points; /**< connections for the entry points */
  puk_hashtable_t contexts; /**< contexts in this composite- hashed by context ID */
  int checked; /**< whether the composite as been checked (ok to be instantiated) */
};

puk_component_t puk_composite_new(const char*id);

/** encapsulate the given component into a composite */
puk_component_t puk_component_encapsulate(puk_component_t, const char*);

/** duplicate a composite */
puk_component_t puk_composite_duplicate(puk_component_t component, const char*id);

/** make a composite ready to be configured out of the given component,
 * either by encapsulation if it's native,
 * or duplication it it's already a composite */
puk_component_t puk_compositize(puk_component_t component, const char*id);

void puk_composite_add_entrypoint(puk_component_t composite, puk_iface_t iface, const char*port_name,
                                  const char*provider_port, const char*provider_id);


/* *** Serialize & parse *********************************** */

padico_string_t puk_component_serialize(puk_component_t);

padico_string_t puk_component_serialize_id(puk_component_t, const char*);

puk_component_t puk_component_parse(const char*);

puk_component_t puk_component_parse_file(const char*filename);


/* ********************************************************* */
/* *** Instanciation *************************************** */


/* *** Components instance ********************************* */

/** An instance of a component */
struct puk_instance_s
{
  puk_component_t component;/**< the component of which this is an instance */
  void*status;              /**< implementation internal state */
  puk_context_t context;    /**< context of the component (if any) */
  puk_instance_t container; /**< containing instance if inside a composite */
  puk_mod_t owner;       /**< module that instantiated this component */
};

/** Status of a composite component */
struct puk_composite_status_s
{
  puk_hashtable_t instance_table; /**< hash: context -> instance */
  puk_instance_t instance;        /**< component instance containing this composite status */
};

/** a predefined descriptor for the instanciation interface: PadicoComponent */
struct puk_component_driver_s
{
  void*(*instantiate)(puk_instance_t, puk_context_t); /**< called upon component instanciation */
  void (*destroy)(void*);                             /**< called upon instance destruction */
  void (*component_init)(void);                       /**< called when component is declared */
  void (*component_finalize)(void);                   /**< called when component is destroyed */
};

/** @internal */
puk_instance_t puk_component_instantiate_internal(puk_component_t component, puk_instance_t container,
                                                  puk_context_t context, puk_mod_t owner);


/** Creates an instance of a given component. */
#define puk_component_instantiate(component)                            \
  puk_component_instantiate_internal(component, NULL, NULL, padico_module_self())

/** Creates an instance of a given component context. */
#define puk_context_instantiate(context)                                \
  puk_component_instantiate_internal(context->component, NULL, context, padico_module_self())

/** Destroys an instance */
void puk_instance_destroy(puk_instance_t instance);


/** Finds driver & status of a sub-context */
void puk_context_indirect(puk_instance_t instance,
                          puk_iface_t iface,
                          const char*label,
                          void*_driver, void *_status);

/** Finds an entry point of an instance */
void puk_instance_entry_point(puk_instance_t instance,
                              puk_iface_t iface,
                              const char*label,
                              void*_driver, void*_status);

/** finds in 'container' an instance with the same context as 'local' */
puk_instance_t puk_instance_find_context(puk_instance_t container, puk_instance_t local);

/** Get attributes for an instance */
const char*puk_instance_getattr(puk_instance_t instance, const char*label);

/* *** various inline functions */

PUK_IFACE_TYPE(PadicoComponent, struct puk_component_driver_s)
PUK_IFACE_TYPE(PadicoComposite, struct puk_composite_driver_s)

/** Finds component instance status for a given (in-context) instance */
static inline void*puk_instance_self_status(puk_instance_t instance)
{
  return instance->status;
}

/** returns the root container (outermost containing instance) for the given instance.
 */
static inline puk_instance_t puk_instance_get_root_container(puk_instance_t instance)
{
  while(instance->container != NULL)
    instance = instance->container;
  return instance;
}


/** @} */

/* *** Enumerator **************************************** */

/* Component enumerators provide an _ordered_ enumeration of
 * the contexts of an assembly, so as to be able to calculate
 * inter-component dependancies when instanciating assemblies.
 */

typedef struct puk_component_enumerator_s*puk_component_enumerator_t;

/** direction of component enumeration : top-down, or bottom-up
 */
enum component_enumerator_way_e
  {
    TOP_DOWN = 1,
    BOTTOM_UP
  };

puk_component_enumerator_t puk_component_enumerator_new(puk_component_t assembly, int way);
puk_context_t              puk_component_enumerator_next(puk_component_enumerator_t e);
void                       puk_component_enumerator_delete(puk_component_enumerator_t e);


/* ********************************************************* */



#endif /* PUK_COMPONENTS_H */

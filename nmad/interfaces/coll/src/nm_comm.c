/*
 * NewMadeleine
 * Copyright (C) 2014-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <nm_public.h>
#include <nm_sendrecv_interface.h>
#include <Padico/Puk.h>
#include <nm_coll_private.h>
#include <nm_launcher_interface.h>
#include <sched.h>
#include <Padico/Module.h>

PADICO_MODULE_BUILTIN(nm_coll, NULL, NULL, NULL);


PUK_HASHTABLE_TYPE(nm_comm, struct nm_session_s*, struct nm_comm_s*,
                   puk_hash_pointer_default_hash, puk_hash_pointer_default_eq, NULL);

static struct
{
  nm_spinlock_t lock;
  struct nm_comm_hashtable_s table; /**< communicators hashed by session */
} nm_comm;

/* ********************************************************* */

static void nm_comm_table_init(void)
{
  nm_spin_init(&nm_comm.lock);
  nm_comm_hashtable_init(&nm_comm.table);
}
static void nm_comm_table_destroy(void)
{
  nm_spin_destroy(&nm_comm.lock);
  nm_comm_hashtable_destroy(&nm_comm.table);
}
NM_LAZY_INITIALIZER(nm_comm_table, &nm_comm_table_init, &nm_comm_table_destroy);

/* ********************************************************* */

static void nm_comm_commit(nm_comm_t p_comm)
{
  /* hash gates */
  int j;
  for(j = 0; j < nm_gate_vect_size(p_comm->group); j++)
    {
      nm_gate_t p_gate = nm_gate_vect_at(p_comm->group, j);
      nm_gate_reverse_hashtable_insert(&p_comm->reverse, p_gate, j);
    }
  /* add communicator to reverse table session -> comm */
  nm_comm_table_lazy_init();
  nm_spin_lock(&nm_comm.lock);
  nm_comm_hashtable_insert(&nm_comm.table, p_comm->p_session, p_comm);
  nm_spin_unlock(&nm_comm.lock);
}

nm_comm_t nm_comm_world(const char*label)
{
  nm_group_t p_group = nm_group_world();
  nm_comm_t p_comm = padico_malloc(sizeof(struct nm_comm_s));
  p_comm->group = p_group;
  p_comm->rank = nm_group_rank(p_group);
  nm_gate_reverse_hashtable_init(&p_comm->reverse);
  padico_string_t s_name = padico_string_new();
  padico_string_catf(s_name, "nm_comm_world/%s", label);
  int rc = nm_session_open(&p_comm->p_session, padico_string_get(s_name));
  if(rc != NM_ESUCCESS)
    {
      NM_FATAL("failed to open session.\n");
    }
  padico_string_delete(s_name);
  nm_comm_commit(p_comm);
  return p_comm;
}

nm_comm_t nm_comm_self(const char*label)
{
  nm_group_t p_group = nm_group_self();
  nm_comm_t p_comm = padico_malloc(sizeof(struct nm_comm_s));
  p_comm->group = p_group;
  p_comm->rank = nm_group_rank(p_group);
  nm_gate_reverse_hashtable_init(&p_comm->reverse);
  padico_string_t s_name = padico_string_new();
  padico_string_catf(s_name, "nm_comm_self/%s", label);
  int rc = nm_session_open(&p_comm->p_session, padico_string_get(s_name));
  if(rc != NM_ESUCCESS)
    {
      NM_FATAL("failed to open session.\n");
    }
  nm_comm_commit(p_comm);
  padico_string_delete(s_name);
  return p_comm;
}


nm_comm_t nm_comm_dup(nm_comm_t p_comm)
{
  nm_group_t group = nm_comm_group(p_comm);
  nm_comm_t p_newcomm = nm_comm_create(p_comm, group);
  return p_newcomm;
}

nm_comm_t nm_comm_create_group(nm_session_t p_session, nm_group_t p_newcomm_group, nm_group_t p_bcast_group)
{
  const int self = nm_group_rank(p_bcast_group);
  assert((self >= 0) && (self < nm_group_size(p_bcast_group))); /* assume self is in group */
  const int root = 0;
  const int newrank = nm_group_rank(p_newcomm_group);
  struct nm_comm_create_header_s
  {
    int commit;
    char session_name[64];
  } header = { .session_name = { '\0'}, .commit = 0 };
  const nm_tag_t tag1 = NM_COLL_TAG_COMM_CREATE_1;
  const nm_tag_t tag2 = NM_COLL_TAG_COMM_CREATE_2;

  nm_session_t p_new_session = NULL;
  while(!header.commit)
    {
      if(self == root)
        {
          snprintf(&header.session_name[0], 64, "nm_comm-%p%08x", p_newcomm_group, (unsigned)random());
          nm_session_open(&p_new_session, header.session_name);
          if(p_new_session != NULL)
            {
              int i;
              header.commit = 0;
              nm_coll_group_bcast(p_session, p_bcast_group, root, self, &header, sizeof(header), tag1);
              const int group_size = nm_group_size(p_bcast_group);
              if(group_size < 0)
                {
                  NM_FATAL("invalid group size %d.\n", group_size);
                }
              int*acks = padico_malloc(sizeof(int) * group_size);
              int ack = 1;
              nm_coll_group_gather(p_session, p_bcast_group, root, self, &ack, sizeof(ack), acks, sizeof(ack), tag2);
              int total_acks = 0;
              for(i = 0; i < nm_group_size(p_bcast_group); i++)
                {
                  total_acks += acks[i];
                }
              padico_free(acks);
              if(total_acks == nm_group_size(p_bcast_group))
                {
                  header.commit = 1;
                  nm_coll_group_bcast(p_session, p_bcast_group, root, self, &header, sizeof(header), tag1);
                }
            }
        }
      else
        {
          nm_coll_group_bcast(p_session, p_bcast_group, root, self, &header, sizeof(header), tag1);
          if(header.commit == 0)
            {
              if(p_new_session != NULL)
                {
                  /* release previous round failed session creation */
                  nm_session_close(p_new_session);
                }
              nm_session_open(&p_new_session, header.session_name);
              int ack = (p_new_session != NULL);
              nm_coll_group_gather(p_session, p_bcast_group, root, self, &ack, sizeof(ack), NULL, 0, tag2);
            }
        }
    }
  if(newrank != -1)
    {
      nm_comm_t p_newcomm  = padico_malloc(sizeof(struct nm_comm_s));
      p_newcomm->rank      = newrank;
      p_newcomm->group     = nm_group_dup(p_newcomm_group);
      p_newcomm->p_session = NULL;
      nm_gate_reverse_hashtable_init(&p_newcomm->reverse);
      p_newcomm->p_session = p_new_session;
      nm_comm_commit(p_newcomm);
      return p_newcomm;
    }
  else
    {
      return NULL;
    }
}

nm_comm_t nm_comm_create(nm_comm_t p_comm, nm_group_t group)
{
  nm_session_t p_session = nm_comm_get_session(p_comm);
  return nm_comm_create_group(p_session, group, nm_comm_group(p_comm));
}

void nm_comm_destroy(nm_comm_t p_comm)
{
  if(p_comm != NULL)
    {
      nm_spin_lock(&nm_comm.lock);
      nm_comm_hashtable_remove(&nm_comm.table, p_comm->p_session);
      nm_spin_unlock(&nm_comm.lock);
      nm_session_close(p_comm->p_session);
      nm_group_free(p_comm->group);
      nm_gate_reverse_hashtable_destroy(&p_comm->reverse);
      padico_free(p_comm);
    }
}

nm_comm_t nm_comm_get_by_session(nm_session_t p_session)
{
  nm_comm_table_lazy_init();
  nm_spin_lock(&nm_comm.lock);
  nm_comm_t p_comm = nm_comm_hashtable_lookup(&nm_comm.table, p_session);
  nm_spin_unlock(&nm_comm.lock);
  return p_comm;
}

/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <stdint.h>
#include <sys/uio.h>
#include <assert.h>
#include <sched.h>

#include <nm_private.h>

#include <Padico/Puk.h>
#include <Padico/Module.h>
PADICO_MODULE_BUILTIN(nmad, NULL, NULL, NULL);

PADICO_MODULE_ATTR(thread_level, "NM_THREAD_LEVEL",
                   "level of multithreading support: single, funneled, serialized, multiple",
                   string, "default");

PADICO_MODULE_ATTR(auto_flush, "NMAD_AUTO_FLUSH",
                   "asks nmad to flush outgoing packets after every posted send. It ensures data is sent earlier; in return, it increses contention between threads and prevent the 'aggreg' strategy to actually aggregate messages.",
                   bool, 0);

PADICO_MODULE_ATTR(isend_check, "NMAD_ISEND_CHECK",
                   "checks whether user buffer has been modified between nm_sr_isend and nm_sr_swait, or between MPI_Isend and MPI_Test or MPI_Wait. This feature is only available when nmad is built in *debug* mode.",
                   bool, 0);

PADICO_MODULE_ATTR(pwsend_timeout, "NMAD_PWSEND_TIMEOUT",
                   "detect stalled packets that do not make any progress. This feature is only available when nmad is built with profiling enabled.",
                   bool, 0);

PADICO_MODULE_ATTR(enable_prefetch, "NMAD_ENABLE_PREFETCH", "enable prefetching of large messages while rdv is in progress; default = true", bool, 1);

PADICO_MODULE_ATTR(enable_send_prefetch, "NMAD_ENABLE_SEND_PREFETCH", "enable prefetching of large messages on the sender side while rdv is in progress; default = false (temporary bug workaround)", bool, 0);

PADICO_MODULE_ATTR(enable_recv_prefetch, "NMAD_ENABLE_RECV_PREFETCH", "enable prefetching of large messages on the receiver side while rdv is in progress; default = true", bool, 1);

/** singleton object for nm_core */
struct nm_core_internal_s nm_core_internal = { .p_core_singleton = NULL };

#if defined(NMAD_PADICOTM) && defined(NMAD_PROFILE)
#include <Padico/PadicoTM.h>
static struct
{
  padico_tm_bgthread_pool_t pool;
  volatile int interrupt;
  volatile double last_core;
} nm_core_watchdog =
  {
    .last_core = 0.0
  };

static void*nm_core_watchdog_worker(void*_arg)
{
  struct nm_core*p_core = _arg;
  while(!nm_core_watchdog.interrupt)
    {
      puk_tick_t t;
      PUK_GET_TICK(t);
      const double curtime = PUK_TIMING_DELAY(p_core->time_orig, t);
      padico_out(puk_verbose_notice, "nm_core_watchdog: current = %f; delta = %f.\n",
                 curtime, curtime - nm_core_watchdog.last_core);
      if(nm_core_watchdog.last_core < curtime - (5.0 * 1000000.0))
        {
          NM_WARN("nm_core_watchdog: deadlock detected; last = %f; current = %f\n",
                  nm_core_watchdog.last_core, curtime);
          puk_debug_gdb_backtrace();
          puk_sleep(10);
        }
      puk_sleep(5);
    }
  return NULL;
}
#endif /* NMAD_PADICOTM && NMAD_PROFILE */

/** checks whether an event matches a given core monitor */
static inline int nm_core_event_matches(const struct nm_core_monitor_s*p_core_monitor,
                                        const struct nm_core_event_s*p_event)
{
  const nm_status_t status = p_event->status & ~NM_STATUS_FINALIZED;
  const int matches =
    ( (p_core_monitor->monitor.event_mask & status) &&
      (p_core_monitor->matching.p_gate == NM_GATE_NONE || p_core_monitor->matching.p_gate == p_event->p_gate) &&
      (nm_core_tag_match(p_event->tag, p_core_monitor->matching.tag, p_core_monitor->matching.tag_mask))
      );
  return matches;
}

/* actually submit the given req; supposed to be called from a task */
static inline void nm_core_pack_submit_flush(struct nm_core*p_core, struct nm_req_chunk_s*p_req_chunk)
{
  nm_core_lock_assert(p_core);
  struct nm_req_s*p_pack = p_req_chunk->p_req;
  assert(p_pack != NULL);
  assert(p_pack->p_gate != NULL);
  if(p_pack->seq == NM_SEQ_NONE)
    {
      /* first chunk in pack- allocate req sequence number */
      p_core->n_packs++;
      nm_trace_var(NM_TRACE_EVENT_VAR_N_PACKS, p_core->n_packs, NULL, NULL);
#ifdef NMAD_PROFILE
      if(p_core->n_packs > p_core->profiling.max_packs)
        p_core->profiling.max_packs = p_core->n_packs;
#endif /* NMAD_PROFILE */
      p_pack->p_gtag = nm_gtag_get(&p_pack->p_gate->tags, p_pack->tag);
      const nm_seq_t seq = nm_seq_next(p_pack->p_gtag->send_seq_number);
      p_pack->p_gtag->send_seq_number = seq;
      p_pack->seq = seq;
      nm_trace_event(NM_TRACE_EVENT_CORE_PACK_FLUSH, p_pack, p_pack->p_gate, NULL, p_pack->tag, seq, p_pack->pack.len);
    }
  if(p_pack->flags & NM_REQ_FLAG_PACK_SYNCHRONOUS)
    {
      nm_req_list_push_back(&p_pack->p_gtag->pending_packs, p_pack);
    }
  nm_strat_submit_req_chunk(p_core, p_pack->p_gate, p_req_chunk, 0);
  nm_core_polling_level(p_core);
}

void nm_core_task_submit_unlocked(struct nm_core*p_core, void (*p_handler)(void))
{
  struct nm_core_task_s*p_core_task = padico_malloc(sizeof(struct nm_core_task_s));
  p_core_task->kind = NM_CORE_TASK_HANDLER;
  p_core_task->content.handler.p_handler = p_handler;
  nm_core_task_enqueue(p_core, 0, p_core_task);
}

void nm_core_task_submit_locked(struct nm_core*p_core, void (*p_handler)(void))
{
  struct nm_core_task_s*p_core_task = padico_malloc(sizeof(struct nm_core_task_s));
  p_core_task->kind = NM_CORE_TASK_HANDLER;
  p_core_task->content.handler.p_handler = p_handler;
  nm_core_lock(p_core);
  nm_core_task_enqueue(p_core, 1, p_core_task);
  nm_core_unlock(p_core);
}

static inline void nm_core_task_process(struct nm_core*p_core, struct nm_core_task_s*p_core_task)
{
  nm_core_lock_assert(p_core);
  switch(p_core_task->kind)
    {
    case NM_CORE_TASK_UNPACK_NEXT:
      {
        nm_trace_state(NM_TRACE_STATE_CORE_UNPACK_NEXT, NULL, NULL);
        nm_core_unpack_trigger_next(p_core, p_core_task);
      }
      break;
    case NM_CORE_TASK_COMPLETED_PW:
      {
        nm_trace_state(NM_TRACE_STATE_CORE_COMPLETED_PW, NULL, NULL);
        struct nm_pkt_wrap_s*p_pw = p_core_task->content.completed_pw.p_pw;
        assert(p_pw->flags & NM_PW_COMPLETED);
        if(p_pw->flags & NM_PW_SEND)
          nm_pw_process_complete_send(p_core, p_pw);
        else if(p_pw->flags & NM_PW_RECV)
          nm_pw_process_complete_recv(p_core, p_pw);
        else
          NM_FATAL("wrong state for completed pw.");
      }
      break;
    case NM_CORE_TASK_COMPLETED_PREFETCH:
      {
        struct nm_pkt_wrap_s*p_pw = p_core_task->content.completed_prefetch.p_pw;
        if(p_pw->flags & NM_PW_SEND)
          {
            while(!nm_pw_pending_rtr_list_empty(&p_pw->pending_rtrs))
              {
                struct nm_pw_pending_rtr_s*p_pending_rtr = nm_pw_pending_rtr_list_pop_front(&p_pw->pending_rtrs);
                assert(p_pw->flags & NM_PW_RDV_READY);
                struct nm_pkt_wrap_s*p_rtr_pw = p_pending_rtr->p_rtr_pw;
                const struct nm_header_ctrl_rtr_s*p_header = p_pending_rtr->p_header;
                padico_free(p_pending_rtr);
                nm_rtr_handler(p_rtr_pw, p_header);
              }
          }
        else
          {
            if(p_pw->p_unpack != NULL)
              {
                nm_status_add(p_pw->p_unpack, NM_STATUS_UNPACK_PREFETCHED);
              }
            if(p_pw->flags & NM_PW_RECV)
              {
                /* recv was requested before prefetch completion */
#ifdef PIOMAN
                nm_ltask_submit_pw_recv(p_pw);
#else /* PIOMAN */
                NM_FATAL("prefetch inconsistency detected. We cannot be here without pioman.\n");
#endif /* PIOMAN */
              }
          }
      }
      break;
    case NM_CORE_TASK_PACK_SUBMISSION:
      {
        nm_trace_state(NM_TRACE_STATE_CORE_PACK_SUBMIT, NULL, NULL);
        struct nm_req_chunk_s*p_req_chunk = p_core_task->content.pack_submission.p_req_chunk;
        nm_core_pack_submit_flush(p_core, p_req_chunk);
      }
      break;
    case NM_CORE_TASK_RTR_SEND:
      {
        struct nm_pkt_wrap_s*p_pw = p_core_task->content.rtr_send.p_pw;
        const nm_seq_t seq = p_pw->p_unpack->seq;
        const nm_core_tag_t tag = p_pw->p_unpack->tag;
        nm_trace_event(NM_TRACE_EVENT_CORE_UNPACK_RTR, p_pw->p_unpack, p_pw->p_gate, p_pw->p_drv, tag, seq, p_pw->p_unpack->unpack.expected_len);
        nm_core_post_rtr(p_pw->p_gate, p_pw->p_unpack->tag, p_pw->p_unpack->seq, p_pw->trk_id, p_pw->chunk_offset, p_pw->length, &p_pw->rdv_data[0]);
      }
      break;
    case NM_CORE_TASK_HANDLER:
      nm_trace_state(NM_TRACE_STATE_CORE_HANDLER, NULL, NULL);
      (*p_core_task->content.handler.p_handler)();
      padico_free(p_core_task);
      break;
    default:
      NM_FATAL("wrong kind %d for core task\n", p_core_task->kind);
      break;
    }
}

__PUK_SYM_INTERNAL
void nm_core_task_flush(struct nm_core*p_core)
{
  /* flush pending_tasks: list of tasks submitted from within locked
   * and unlocked sections.
   * Tasks are dequeues only from here (locked), hence using *_single_reader().
   */
  nm_trace_state(NM_TRACE_STATE_CORE_TASK_FLUSH, NULL, NULL);
  nm_core_lock_assert(p_core);
  struct nm_core_task_s*p_core_task = nm_core_task_lfqueue_dequeue_single_reader(&p_core->pending_tasks);
  while(p_core_task)
    {
      nm_core_task_process(p_core, p_core_task);
      p_core_task = nm_core_task_lfqueue_dequeue_single_reader(&p_core->pending_tasks);
    }
  nm_trace_state(NM_TRACE_STATE_CORE_NONE, NULL, NULL);
}

/** make progress on core pending operations.
 * all operations that need lock when multithreaded.
 */
__PUK_SYM_INTERNAL
void nm_core_progress(struct nm_core*p_core)
{
#ifdef NMAD_DEBUG
  static int inprogress = 0;
  assert(!inprogress); /* make sure this function is never called recursively */
  inprogress = 1;
#endif /* NMAD_DEBUG */
#ifdef NMAD_PROFILE
  /* watchdog */
  static double last_core_watchdog = 0.0;  /**< last time core watchdog was displayed */
  static double last_small_watchdog = 0.0; /**< last time there was non-NULL in pw recv on small trk */
  if(p_core->enable_pwsend_timeout)
    {
      puk_tick_t t;
      PUK_GET_TICK(t);
      const double curtime = PUK_TIMING_DELAY(p_core->time_orig, t);
#ifdef NMAD_PADICOTM
      nm_core_watchdog.last_core = curtime;
#endif /* NMAD_PADICOTM */
      /* watchdog for core progression */
      if(curtime > last_core_watchdog + 1000000.0 * 5.0)
        {
          last_core_watchdog = curtime;
          padico_out(puk_verbose_notice, "watchdog %g.\n", curtime);
        }
      /* watchdog for recv on small trk */
      {
        int gates_null = 0; /* whether there is a gate with NULL pw */
        struct nm_gate_s*p_gate;
        NM_FOR_EACH_GATE(p_gate, p_core)
          {
            struct nm_trk_s*p_trk = &p_gate->trks[0];
            assert(p_trk->kind == nm_trk_small);
            if(p_trk->p_drv->props.capabilities.supports_recv_any)
              {
                if(p_trk->p_drv->p_pw_recv_any == NULL)
                  {
                    gates_null = 1;
                  }
              }
            else if(p_trk->p_pw_recv == NULL)
              {
                gates_null++;
                break;
              }
          }
        if(gates_null)
          {
            if(curtime > last_small_watchdog + 1000000.0 * 5.0)
              {
                NM_WARN("more than 5s without recv posted on trk #0 for all gates (%gs)...\n", curtime - last_small_watchdog);
                puk_debug_gdb_backtrace();
              }
          }
        else
          {
            last_small_watchdog = curtime;
          }
      }
    }
#endif /* NMAD_PROFILE */
  nm_core_lock_assert(p_core);
  nm_profile_inc(p_core->profiling.n_strat_apply);
  nm_core_task_flush(p_core);
#ifdef NMAD_DEBUG
  nm_core_matching_check(p_core); /* check that matching did the job */
#endif /* NMAD_DEBUG */
  nm_trace_state(NM_TRACE_STATE_CORE_STRATEGY, NULL, NULL);
  /* apply strategy on each active gate */
  nm_strat_schedule(p_core, NULL);
#ifdef NMAD_DEBUG
  inprogress = 0;
#endif /* NMAD_DEBUG */
  nm_trace_state(NM_TRACE_STATE_CORE_NONE, NULL, NULL);
}

/** flush pending events, but do not schedule new ones */
__PUK_SYM_INTERNAL
void nm_core_flush(struct nm_core*p_core)
{
  nm_core_nolock_assert(p_core);
  if(!nm_core_task_lfqueue_empty(&p_core->pending_tasks))
    {
      nm_core_lock(p_core);
      nm_core_task_flush(p_core);
      nm_core_unlock(p_core);
    }
  nm_core_events_dispatch(p_core);
}

/** Main function of the core scheduler loop.
 *
 * This is the heart of NewMadeleine...
 */
int nm_schedule(struct nm_core*p_core)
{
  nm_profile_inc(p_core->profiling.n_schedule);
#ifdef PIOMAN
#ifdef NMAD_DEBUG
  piom_ltask_assert_noschedule(); /* cannot schedule from a ltask context */
#endif /* NMAD_DEBUG */
  piom_polling_force();
#else /* PIOMAN */

#ifdef NMAD_DEBUG
  static int scheduling_in_progress = 0;
  assert(!scheduling_in_progress);
  scheduling_in_progress = 1;
#endif /* NMAD_DEBUG */

  /* non-pioman code here; pretend to lock the core for the sake of consistency */
  nm_core_lock(p_core);
  nm_core_progress(p_core);
  nm_core_unlock(p_core);

  nm_trace_state(NM_TRACE_STATE_CORE_POLLING, NULL, NULL);
  /* poll pending recv requests */
  if(!nm_pkt_wrap_list_empty(&p_core->pending_recv_list))
    {
      NM_TRACEF("polling inbound requests\n");
      struct nm_pkt_wrap_s*p_pw, *p_pw2;
      puk_list_foreach_safe(nm_pkt_wrap, p_pw, p_pw2, &p_core->pending_recv_list)
        {
          nm_pw_recv_progress(p_pw);
        }
    }
  /* poll pending out requests */
  if(!nm_pkt_wrap_list_empty(&p_core->pending_send_list))
    {
      NM_TRACEF("polling outbound requests\n");
      struct nm_pkt_wrap_s*p_pw, *p_pw2;
      puk_list_foreach_safe(nm_pkt_wrap, p_pw, p_pw2, &p_core->pending_send_list)
        {
          nm_pw_send_progress(p_pw);
        }
    }
  /* prefetch send requests
   * note: prefetch _after_ pw send, to ensure we are prefetching after rdv are sent */
  if(!nm_pkt_wrap_list_empty(&p_core->prefetch_large_send))
    {
      NM_TRACEF("prefetching outbound requests\n");
      struct nm_pkt_wrap_s*p_pw, *p_pw2;
      puk_list_foreach_safe(nm_pkt_wrap, p_pw, p_pw2, &p_core->prefetch_large_send)
        {
          nm_pkt_wrap_list_remove(&p_core->prefetch_large_send, p_pw);
          nm_pkt_wrap_list_push_back(&p_pw->p_gate->pending_large_send, p_pw);
          nm_pw_send_prefetch(p_pw);
        }
    }
  nm_trace_state(NM_TRACE_STATE_CORE_NONE, NULL, NULL);
  /* flush tasks produced by pw completions */
  nm_core_lock(p_core);
  nm_core_task_flush(p_core);
  nm_core_unlock(p_core);

#ifdef NMAD_DEBUG
  scheduling_in_progress = 0;
#endif /* NMAD_DEBUG */

#endif /* PIOMAN */
  nm_core_events_dispatch(p_core);
  if(PUK_VG_RUNNING_ON_VALGRIND)
    sched_yield();
  return NM_ESUCCESS;
}

void nm_core_req_monitor(struct nm_core*p_core, struct nm_req_s*p_req, struct nm_monitor_s monitor)
{
  nm_core_lock(p_core); /* lock needed for atomicity of set monitor + tests status */
  assert(p_req->monitor.p_notifier == NULL);
  p_req->monitor = monitor;
  nm_status_t status = nm_status_test(p_req, monitor.event_mask);
  if(status)
    {
      /* immediate event */
      const struct nm_core_event_s event =
        {
          .status = status,
          .p_req  = p_req
        };
      nm_core_status_event(p_core, &event, p_req);
    }
  nm_core_unlock(p_core);
  nm_core_events_dispatch(p_core);
}

/** dispatch all ready events */
void nm_core_events_dispatch(struct nm_core*p_core)
{
  nm_trace_state(NM_TRACE_STATE_CORE_DISPATCHING, NULL, NULL);
  nm_core_nolock_assert(p_core);
  if(!nm_core_dispatching_event_lfqueue_empty(&p_core->dispatching_events))
    {
      /* take a lock to access the _lock-free_ queue...
       * concurrency between reader & writer is lock-free, concurrency between
       * readers uses a lock to allow for atomic check for in-progress requests and dequeue.
       */
      int rc = nm_spin_trylock(&p_core->dispatching_lock);
      if(rc)
        {
          for(;;)
            {
              if(nm_core_dispatching_event_lfqueue_empty(&p_core->dispatching_events)) /* empty queue */
                {
                  break;
                }
              struct nm_core_dispatching_event_s*p_dispatching_event =
                nm_core_dispatching_event_lfqueue_dequeue_single_reader(&p_core->dispatching_events);
              if(p_dispatching_event == NULL)
                break;
              if(p_dispatching_event->p_req != NULL)
                {
                  /* status bits are added in nm_core_status_event(), except FINALIZED */
                  if(p_dispatching_event->event.status & NM_STATUS_FINALIZED)
                    {
                      nm_status_add(p_dispatching_event->p_req, NM_STATUS_FINALIZED);
                    }
                }
              if(p_dispatching_event->event.status & p_dispatching_event->p_monitor->event_mask)
                {
                  /* notify only if bitmask matches */
                  (*p_dispatching_event->p_monitor->p_notifier)(&p_dispatching_event->event, p_dispatching_event->p_monitor->ref);
                }
              nm_core_dispatching_event_free(&p_core->dispatching_event_allocator, p_dispatching_event);
            }
          nm_spin_unlock(&p_core->dispatching_lock);
        }
      else
        {
          /* monitor already in progress; another thread is already dispatching
           * return & don't process further events to preserve order */
          return;
        }
    }
  nm_trace_state(NM_TRACE_STATE_CORE_NONE, NULL, NULL);
}

static inline void nm_core_dispatching_event_enqueue(struct nm_core*p_core, const struct nm_core_event_s*const p_event,
                                                     struct nm_monitor_s*p_monitor, struct nm_req_s*p_req)
{
  nm_core_lock_assert(p_core);
  struct nm_core_dispatching_event_s*p_dispatching_event =
    nm_core_dispatching_event_malloc(&p_core->dispatching_event_allocator);
  p_dispatching_event->event = *p_event;
  p_dispatching_event->p_monitor = p_monitor;
  p_dispatching_event->p_req = p_req;
  int rc = 0;
  do
    {
      rc = nm_core_dispatching_event_lfqueue_enqueue_single_writer(&p_core->dispatching_events, p_dispatching_event);
      if(rc)
        {
          /* dispatching queue full; dispatch some events to make room and retry */
          nm_profile_inc(p_core->profiling.n_event_queue_full);
          nm_core_unlock(p_core);
          nm_core_events_dispatch(p_core);
          nm_core_lock(p_core);
        }
    }
  while(rc);
}

/** dispatch a global event that matches the given monitor */
static void nm_core_event_notify(nm_core_t p_core, const struct nm_core_event_s*const p_event, struct nm_core_monitor_s*p_core_monitor)
{
  nm_core_lock_assert(p_core);
  assert(nm_core_event_matches(p_core_monitor, p_event));
  if(p_event->status & NM_STATUS_UNEXPECTED)
    {
      struct nm_gtag_s*p_gtag = nm_gtag_get(&p_event->p_gate->tags, p_event->tag);
      const nm_seq_t next_seq = nm_seq_next(p_gtag->recv_seq_number);
      if(p_event->seq == next_seq)
        {
          /* next packet in sequence; commit matching and enqueue for dispatch */
          p_gtag->recv_seq_number = next_seq;
          nm_core_dispatching_event_enqueue(p_core, p_event, &p_core_monitor->monitor, NULL);

        restart:
          if(!nm_core_pending_event_list_empty(&p_gtag->pending_events))
            {
              /* recover pending events */
              struct nm_core_pending_event_s*p_pending_event = nm_core_pending_event_list_front(&p_gtag->pending_events);
              const nm_seq_t next_seq = nm_seq_next(p_gtag->recv_seq_number);
              if(p_pending_event->event.seq == next_seq)
                {
                  assert(nm_core_event_matches(p_pending_event->p_core_monitor, &p_pending_event->event));
                  assert(p_pending_event->event.status & NM_STATUS_UNEXPECTED);
                  p_gtag->recv_seq_number = next_seq;
                  nm_core_pending_event_list_pop_front(&p_gtag->pending_events);
                  nm_core_dispatching_event_enqueue(p_core, &p_pending_event->event,
                                                    &p_pending_event->p_core_monitor->monitor, NULL);
                  nm_core_pending_event_delete(p_pending_event);
                  goto restart; /* retry to find another matching event */
                }
            }
        }
      else
        {
          /* out-of-order event; store as pending */
          nm_profile_inc(p_core->profiling.n_outoforder_event);
          struct nm_core_pending_event_s*p_pending_event = nm_core_pending_event_new();
          p_pending_event->event = *p_event;
          p_pending_event->p_core_monitor = p_core_monitor;
          if(nm_core_pending_event_list_empty(&p_gtag->pending_events))
            {
              nm_core_pending_event_list_push_back(&p_gtag->pending_events, p_pending_event);
            }
          else
            {
              /* assume an out-of-order event comes last */
              nm_core_pending_event_itor_t i = nm_core_pending_event_list_rbegin(&p_gtag->pending_events);
              while(i && (nm_seq_compare(p_gtag->recv_seq_number, i->event.seq, p_pending_event->event.seq) > 0))
                {
                  i = nm_core_pending_event_list_rnext(i);
                }
              if(i)
                {
                  nm_core_pending_event_list_insert_after(&p_gtag->pending_events, i, p_pending_event);
                }
              else
                {
                  nm_core_pending_event_list_push_front(&p_gtag->pending_events, p_pending_event);
                }
            }
          assert(nm_core_pending_event_list_size(&p_gtag->pending_events) < NM_SEQ_MAX - 1); /* seq number overflow */
        }
    }
}

/** Add an event monitor to the list */
void nm_core_monitor_add(nm_core_t p_core, struct nm_core_monitor_s*p_core_monitor)
{
  nm_core_lock(p_core);
  if(p_core_monitor->monitor.event_mask == NM_STATUS_UNEXPECTED)
    {
      assert(p_core_monitor->matching.tag_mask.hashcode == NM_CORE_TAG_HASH_FULL);
      struct nm_matching_wildcard_s*p_wildcard = nm_matching_wildcard_bytag(p_core, p_core_monitor->matching.tag);
      struct nm_unexpected_s*p_unexpected = NULL, *p_tmp;
      puk_list_foreach_safe(nm_unexpected_wildcard, p_unexpected, p_tmp, &p_wildcard->unexpected) /* use 'safe' iterator since notifier is likely to post a recv */
        {
          if(p_unexpected->msg_len != NM_LEN_UNDEFINED)
            {
              const struct nm_core_event_s event =
                {
                  .status = NM_STATUS_UNEXPECTED,
                  .p_gate = p_unexpected->chunk.p_gate,
                  .tag    = p_unexpected->chunk.tag,
                  .seq    = p_unexpected->chunk.seq,
                  .len    = p_unexpected->msg_len
                };
              if(nm_core_event_matches(p_core_monitor, &event))
                {
                  nm_core_event_notify(p_core, &event, p_core_monitor);
                }
            }
        }
    }
  nm_core_monitor_vect_push_back(&p_core->monitors, p_core_monitor);
  nm_core_unlock(p_core);
}

void nm_core_monitor_remove(nm_core_t p_core, struct nm_core_monitor_s*m)
{
  nm_core_lock(p_core);
  nm_core_monitor_vect_itor_t i = nm_core_monitor_vect_find(&p_core->monitors, m);
  if(i)
    {
      nm_core_monitor_vect_erase(&p_core->monitors, i);
    }
  nm_core_unlock(p_core);
}

/** Fires an event
 */
void nm_core_status_event(nm_core_t p_core, const struct nm_core_event_s*const p_event, struct nm_req_s*p_req)
{
  if(p_req)
    {
      assert(!(nm_status_test(p_req, NM_STATUS_UNPACK_DATA0) && (p_event->status & NM_STATUS_UNPACK_DATA0)));
      if(p_req->monitor.p_notifier)
        {
          if(p_event->status & ~NM_STATUS_FINALIZED)
            {
              /* in case a monitor is registered, add flags to status, no signal.
               * For FINALIZED status, wait for the dispatching so that the request
               * is not freed to early
               */
              nm_status_add(p_req, p_event->status & ~NM_STATUS_FINALIZED);
            }
          /* when a monitor is registered, all events and status changes go through the dispatcher */
          nm_core_dispatching_event_enqueue(p_core, p_event, &p_req->monitor, p_req);
        }
      else
        {
          /* no monitor: full signal */
          nm_status_signal(p_req, p_event->status);
        }
    }
  else
    {
      /* fire global monitors */
#ifdef NMAD_DEBUG
      int matched = 0;
#endif /* NMAD_DEBUG */
      nm_core_monitor_vect_itor_t i;
      puk_vect_foreach(i, nm_core_monitor, &p_core->monitors)
        {
          struct nm_core_monitor_s*p_core_monitor = *i;
          if(nm_core_event_matches(p_core_monitor, p_event))
            {
#ifdef NMAD_DEBUG
              if(matched != 0)
                {
                  NM_FATAL("multiple monitors match the same core event.\n");
                }
              matched++;
#endif /* NMAD_DEBUG */
              nm_core_event_notify(p_core, p_event, p_core_monitor);
            }
        }
    }
}

const char*nm_strerror(int rc)
{
  static const char*error_msg[_NM_ERR_MAX] =
    {
     [NM_ESUCCESS]    = "success",
     [NM_EUNKNOWN]    = "unknown error",
     [NM_ENOTIMPL]    = "not implemented",
     [NM_ESCFAILD]    = "syscall failed",
     [NM_EAGAIN]      = "poll again",
     [NM_ECLOSED]     = "connection closed",
     [NM_EBROKEN]     = "error condition on connection",
     [NM_EINVAL]      = "invalid parameter",
     [NM_ENOTFOUND]   = "not found",
     [NM_ENOMEM]      = "out of memory",
     [NM_EALREADY]    = "already in progress",
     [NM_ETIMEDOUT]   = "operation timeout",
     [NM_EINPROGRESS] = "operation in progress",
     [NM_EUNREACH]    = "destination unreachable",
     [NM_ECANCELED]   = "operation canceled",
     [NM_EABORTED]    = "operation aborted",
     [NM_EBUSY]       = "gate/trk is busy",
     [NM_ENOTPOSTED]  = "request not posted",
     [NM_ETRUNCATED]  = "message truncated"
    };
  if(rc <= 0 && rc > -_NM_ERR_MAX)
    {
      return error_msg[-rc];
    }
  else
    {
      return "out of bound error code.";
    }
}

/** Load a newmad component from disk. The actual component is
 * NewMad_<entity>_<name> where <entity> is either 'driver' or 'strategy'.
 */
puk_component_t nm_core_component_load(const char*entity, const char*name)
{
  puk_component_t component = NULL;
  char component_name[1024];
  snprintf(component_name, 1024, "NewMad_%s_%s", entity, name);
  component = puk_component_resolve(component_name);
  if(component == NULL)
    {
      NM_FATAL("failed to load component '%s'\n", component_name);
    }
  return component;
}


static const char*const nm_thread_labels[4] =
  {
    [ NM_THREAD_SINGLE ]     = "single",
    [ NM_THREAD_FUNNELED ]   = "funneled",
    [ NM_THREAD_SERIALIZED ] = "serialized",
    [ NM_THREAD_MULTIPLE ]   = "multiple"
  };

/** compute the thread level to use from attributes */
nm_thread_level_t nm_core_compute_thread_level(void)
{
  nm_thread_level_t thread_level;
#ifdef PIOMAN_MULTITHREAD
  thread_level = NM_THREAD_MULTIPLE;
#else /* PIOMAN_MULTITHREAD */
  thread_level = NM_THREAD_SINGLE;
#endif /* PIOMAN_MULTITHREAD */
  const char*s_thread_level = padico_module_attr_thread_level_getvalue();
  if(strcmp(s_thread_level, "default") != 0)
    {
      int i;
      for(i = 0; i < 4; i++)
        {
          if(strcmp(s_thread_level, nm_thread_labels[i]) == 0)
            {
              if(i > thread_level)
                {
                  thread_level = i;
                }
              break;
            }
        }
      if(i == 4)
        {
          NM_FATAL("illegal thread level = %s\n", s_thread_level);
        }
    }
  return thread_level;
}

/** return the cached value for thread_level, or compute it on the fly if init not done */
nm_thread_level_t nm_core_get_thread_level(nm_core_t p_core)
{
  if(p_core == NULL)
    {
      /* init not done yet */
      return nm_core_compute_thread_level();
    }
  else
    {
      return p_core->thread_level;
    }
}

void nm_core_set_thread_level(nm_thread_level_t l)
{
  if((l < 0 ) || (l >= 4))
    {
      NM_FATAL("illegal value for thread level: %d\n", l);
    }
  padico_module_attr_thread_level_setvalue(nm_thread_labels[l]);
  padico_out(puk_verbose_info, "setting thread level = %s\n", nm_thread_labels[l]);
}

/** Initialize NewMad core and SchedOpt.
 */
int nm_core_init(nm_core_t*pp_core)
{
  int err = NM_ESUCCESS;

  /*
   * Lazy Puk initialization (it may already have been initialized in PadicoTM or MPI_Init)
   */
  if(!padico_puk_initialized())
    {
      padico_puk_init();
    }
  /* load nmad as a module */
  puk_mod_t mod = NULL;
  padico_puk_mod_resolve(&mod, "nmad");
  if(mod == NULL)
    {
      NM_FATAL("failed to load nmad builtin module.\n");
    }
  padico_puk_mod_load(mod);

  /* allocate core object and init lists */

  struct nm_core*p_core = padico_malloc(sizeof(struct nm_core));
  memset(p_core, 0, sizeof(struct nm_core));

  if(nm_core_internal.p_core_singleton != NULL)
    {
      NM_FATAL("duplicate init.\n");
    }
  nm_core_internal.p_core_singleton = p_core;

  /* init thread level early- before any locking init */
  p_core->thread_level = nm_core_compute_thread_level();
  /* make attr consistent with actual level */
  padico_module_attr_thread_level_setvalue(nm_thread_labels[p_core->thread_level]);
  padico_out(puk_verbose_info, "core init with thread level = %s\n", nm_thread_labels[p_core->thread_level]);
#ifndef PIOMAN_MULTITHREAD
  if(p_core->thread_level == NM_THREAD_MULTIPLE)
    {
      NM_FATAL("cannot use thread level 'multiple' without pioman.\n");
    }
#endif

  nm_gate_list_init(&p_core->gate_list);
  nm_active_gate_list_init(&p_core->active_gates);
  nm_drv_list_init(&p_core->driver_list);
  p_core->nb_drivers = 0;

  /* Initialize "Lightning Fast" Packet Wrappers Manager */
  nm_pw_nohd_allocator_init(&p_core->pw_nohd_allocator, INITIAL_PKT_NUM);
  nm_pw_buf_allocator_init(&p_core->pw_buf_allocator, INITIAL_PKT_NUM);

  nm_core_monitor_vect_init(&p_core->monitors);

  nm_matching_wildcard_table_init(&p_core->wildcard_table);
  nm_matching_tag_table_init(&p_core->tag_table);
  p_core->unpack_seq = 1;
  p_core->n_packs = 0;
  p_core->n_unpacks = 0;

  nm_req_chunk_allocator_init(&p_core->req_chunk_allocator, NM_REQ_CHUNK_QUEUE_SIZE);
  nm_ctrl_chunk_allocator_init(&p_core->ctrl_chunk_allocator, NM_REQ_CHUNK_QUEUE_SIZE);

  nm_core_task_lfqueue_init(&p_core->pending_tasks);

  nm_core_dispatching_event_allocator_init(&p_core->dispatching_event_allocator, 16);
  nm_core_dispatching_event_lfqueue_init(&p_core->dispatching_events);
  nm_spin_init(&p_core->dispatching_lock);

  nm_trk_hashtable_init(&p_core->trk_table);

  p_core->enable_schedopt = 1;
  p_core->strategy_component = NULL;
  if(padico_module_attr_auto_flush_getvalue())
    {
      p_core->enable_auto_flush = 1;
      NM_DISPF("auto-flush on send enabled.\n");
    }
  else
    {
      p_core->enable_auto_flush = 0;
    }
  if(padico_module_attr_enable_prefetch_getvalue())
    {
      p_core->enable_send_prefetch = padico_module_attr_enable_send_prefetch_getvalue();
      p_core->enable_recv_prefetch = padico_module_attr_enable_recv_prefetch_getvalue();
    }
  else
    {
      p_core->enable_send_prefetch = 0;
      p_core->enable_recv_prefetch = 0;
      NM_DISPF("prefetch disabled by user.\n");
    }
  p_core->enable_isend_csum = 0;
  if(padico_module_attr_isend_check_getvalue())
    {
#ifdef NMAD_DEBUG
      p_core->enable_isend_csum = 1;
      NM_DISPF("isend integrity check enabled.\n");
#else
      NM_WARN("isend integrity check available only in DEBUG mode. Please rebuild nmad in debug mode.\n");
#endif
    }
  p_core->enable_pwsend_timeout = 0;
  if(padico_module_attr_pwsend_timeout_getvalue())
    {
#ifdef NMAD_PROFILE
      p_core->enable_pwsend_timeout = 1;
      padico_out(puk_verbose_notice, "pw send timeout enabled; timeout = %d usec. (%d s)\n",
                 NM_PW_TIMEOUT, NM_PW_TIMEOUT / 1000000);
#ifdef NMAD_PADICOTM
      padico_out(puk_verbose_notice, "active watchdog enabled\n");
      nm_core_watchdog.pool = padico_tm_bgthread_pool_create("nm_core_watchdog");
      padico_tm_bgthread_start(nm_core_watchdog.pool, &nm_core_watchdog_worker, p_core, "nm_core_watchdog");
#endif /* NMAD_PADICOTM */
#else /* NMAD_PROFILE */
      NM_WARN("cannot enable pw send timeout without profiling.\n");
#endif /* NMAD_PROFILE */
    }

#ifdef NMAD_PROFILE
  PUK_GET_TICK(p_core->time_orig);
  puk_profile_var_defx(unsigned_long_long, counter, &p_core->profiling.n_locks, 0,
                       "nm_core", "number of times the core lock has been acquired & released",
                       "nm_core.n_lock");
  puk_profile_var_defx(unsigned_long_long, counter, &p_core->profiling.n_schedule, 0,
                       "nm_core", "number of times the function nm_schedule() was called",
                       "nm_core.n_schedule");
  puk_profile_var_defx(unsigned_long_long, counter, &p_core->profiling.n_packs, 0,
                       "nm_core", "total number of packs submitted",
                       "nm_core.n_packs");
  puk_profile_var_defx(unsigned_long_long, counter, &p_core->profiling.n_unpacks, 0,
                       "nm_core", "total number of unpacks submitted",
                       "nm_core.n_unpacks");
  puk_profile_var_defx(unsigned_long_long, counter, &p_core->profiling.n_unexpected, 0,
                       "nm_core", "total number of received unexpected messages",
                       "nm_core.n_unexpected");
  puk_profile_var_defx(unsigned_long_long, counter, &p_core->profiling.n_rdvs, 0,
                       "nm_core", "total number of rdv received",
                       "nm_core.n_rdvs");
  puk_profile_var_defx(unsigned_long_long, counter, &p_core->profiling.n_rdvs_accept_small, 0,
                       "nm_core", "total number of rdvs accepted on small track",
                       "nm_core.n_rdvs_accept_small");
  puk_profile_var_defx(unsigned_long_long, counter, &p_core->profiling.n_rdvs_accept_single, 0,
                       "nm_core", "total number of rdvs accepted as single chunk",
                       "nm_core.n_rdvs_accept_single");
  puk_profile_var_defx(unsigned_long_long, counter, &p_core->profiling.n_rdvs_accept_split, 0,
                       "nm_core", "total number of rdvs accepted with split",
                       "nm_core.n_rdvs_accept_split");
  puk_profile_var_defx(unsigned_long_long, counter, &p_core->profiling.n_pw_split, 0,
                       "nm_core", "total number of packet split",
                       "nm_core.n_pw_split");
  puk_profile_var_defx(unsigned_long_long, counter, &p_core->profiling.n_pw_out, 0,
                       "nm_core", "total number of packet wrappers sent",
                       "nm_core.n_pw_out");
  puk_profile_var_defx(unsigned_long_long, counter, &p_core->profiling.n_pw_in, 0,
                       "nm_core", "total number of packet wrappers received",
                       "nm_core.n_pw_in");
  puk_profile_var_defx(unsigned_long_long, counter, &p_core->profiling.n_try_and_commit, 0,
                       "nm_core", "number of times try_and_commit was invoked",
                       "nm_core.n_try_and_commit");
  puk_profile_var_defx(unsigned_long_long, counter, &p_core->profiling.n_strat_apply, 0,
                       "nm_core", "number of times strat_apply was invoked",
                       "nm_core.n_strat_apply");
  puk_profile_var_defx(unsigned_long_long, counter, &p_core->profiling.n_outoforder_event, 0,
                       "nm_core", "total number of out-of-order events received",
                       "nm_core.n_out_of_order_event");
  puk_profile_var_defx(unsigned_long_long, counter, &p_core->profiling.n_event_queue_full, 0,
                       "nm_core", "number of times the event queue was full before enqueing a new event",
                       "nm_core.n_event_queue_full");
  puk_profile_var_defx(unsigned_long_long, counter, &p_core->profiling.n_packs_partitioned, 0,
                       "nm_core", "number of packs using partitioned communication",
                       "nm_core.n_packs_partitioned");
  puk_profile_var_defx(unsigned_long, highwatermark, &p_core->profiling.max_unpacks, 0,
                       "nm_core", "maximum number of simultaneous active unpacks",
                       "nm_core.max_unpacks");
  puk_profile_var_defx(unsigned_long, highwatermark, &p_core->profiling.max_packs, 0,
                       "nm_core", "maximum number of simultaneous active packs",
                       "nm_core.max_packs");
#endif /* NMAD_PROFILE */

#if defined(NMAD_HWLOC)
  hwloc_topology_init(&p_core->topology);
  hwloc_topology_load(p_core->topology);
#endif /* NMAD_HWLOC */

#ifdef PIOMAN
  pioman_init();
  nm_core_lock_init(p_core);
  nm_ltask_set_policy();
  nm_ltask_submit_core_progress(p_core);
#else
  nm_pkt_wrap_list_init(&p_core->pending_recv_list);
  nm_pkt_wrap_list_init(&p_core->pending_send_list);
#endif /* PIOMAN */

  *pp_core = p_core;

  return err;
}

int nm_core_set_strategy(nm_core_t p_core, puk_component_t strategy)
{
  nm_core_lock(p_core);
  puk_facet_t strat_facet = puk_component_get_facet_NewMad_Strategy(strategy, NULL);
  if(strat_facet == NULL)
    {
      NM_FATAL("component %s given as strategy has no interface 'NewMad_Strategy'\n", strategy->name);
    }
  p_core->strategy_component = strategy;
  p_core->strategy_iface = puk_component_get_driver_NewMad_Strategy(p_core->strategy_component, NULL);
  p_core->strategy_context = puk_component_get_context(p_core->strategy_component, puk_iface_NewMad_Strategy(), NULL);
  if(p_core->strategy_context == NULL)
    {
      p_core->strategy_context = puk_context_new(p_core->strategy_component, NULL, "auto");
    }
  assert(p_core->strategy_context != NULL);
  if(p_core->strategy_iface->init != NULL)
    {
      (*p_core->strategy_iface->init)(p_core->strategy_context);
    }

  NM_DISPF("loaded strategy '%s'\n", strategy->name);
  nm_core_unlock(p_core);
  return NM_ESUCCESS;
}

void nm_core_schedopt_disable(nm_core_t p_core)
{
  nm_core_lock(p_core);
  p_core->enable_schedopt = 0;
  nm_core_driver_flush(p_core);
  nm_core_unlock(p_core);
}

/** Shutdown the core struct and the main scheduler.
 */
int nm_core_exit(nm_core_t p_core)
{
  nm_schedule(p_core);   /* schedule last pws */
  nm_core_flush(p_core); /* flush all pending tasks */

#ifdef PIOMAN
  /* stop posting new pw */
  piom_ltask_cancel(&p_core->ltask);
#endif
  nm_core_lock(p_core);
  nm_core_task_flush(p_core); /* last flush for core tasks */
  nm_core_driver_flush(p_core);
  /* disconnect all gates */
  nm_gate_t p_gate = NULL;
  struct nm_drv_s*p_drv = NULL;
  NM_FOR_EACH_GATE(p_gate, p_core)
    {
      nm_core_gate_disconnect(p_gate);
    }
  /* close all drivers */
  NM_FOR_EACH_DRIVER(p_drv, p_core)
    {
      nm_core_driver_close(p_drv);
    }
  /* close all gates */
  nm_gate_t p_gate2;
  puk_list_foreach_safe(nm_gate, p_gate, p_gate2, &p_core->gate_list)
    {
      nm_core_gate_destroy(p_gate);
    }
  /* cleanup schedopt scheduler */
  nm_so_schedule_clean(p_core);
  /* close strategy */
  if(p_core->strategy_iface->close != NULL)
    {
      (*p_core->strategy_iface->close)(p_core->strategy_context);
    }

  /* delete drivers */
  do
    {
      p_drv = nm_drv_list_pop_front(&p_core->driver_list);
      if(p_drv)
        {
          nm_core_driver_destroy(p_drv);
        }
    }
  while(p_drv);

#if defined(NMAD_PROFILE) && defined(NMAD_PADICOTM)
  if(p_core->enable_pwsend_timeout)
    {
      /* stop watchdog */
      nm_core_watchdog.interrupt = 1;
      padico_tm_bgthread_pool_wait(nm_core_watchdog.pool);
    }
#endif /* NMAD_PADICOTM && NMAD_PROFILE */

  nm_trk_hashtable_destroy(&p_core->trk_table);

#if(defined(PIOMAN))
  pioman_exit();
#else /* PIOMAN */
  /* Sanity check- everything is supposed to be empty here */
  struct nm_pkt_wrap_s*p_pw, *p_pw2;
  puk_list_foreach_safe(nm_pkt_wrap, p_pw, p_pw2, &p_core->pending_recv_list)
    {
      NM_WARN("purging pw = %p from pending_recv_list\n", p_pw);
      nm_pw_ref_dec(p_pw);
    }
  puk_list_foreach_safe(nm_pkt_wrap, p_pw, p_pw2, &p_core->pending_send_list)
    {
      NM_WARN("purging pw = %p from pending_send_list\n", p_pw);
      nm_pw_ref_dec(p_pw);
    }
#endif /* !PIOMAN */

  /* shutdown strategy */
  if(p_core->strategy_component)
    {
      if((p_core->strategy_context != NULL) &&
         (puk_component_get_context(p_core->strategy_component, puk_iface_NewMad_Strategy(), NULL) == NULL))
        {
          puk_context_destroy(p_core->strategy_context);
        }
     }

#ifdef NMAD_DEBUG
  /* check completion for core tasks */
  if(!nm_core_task_lfqueue_empty(&p_core->pending_tasks))
    {
      struct nm_core_task_s*p_core_task = NULL;
      do
        {
          p_core_task = nm_core_task_lfqueue_dequeue_single_reader(&p_core->pending_tasks);
          if(p_core_task != NULL)
            {
              NM_WARN("pending core task in submission queue- kind = %d\n", p_core_task->kind);
            }
        }
      while(p_core_task != NULL);
    }
#endif /* NMAD_DEBUG */

  /* Shutdown "Lightning Fast" Packet Wrappers Manager */
  nm_pw_nohd_allocator_destroy(&p_core->pw_nohd_allocator);
  nm_pw_buf_allocator_destroy(&p_core->pw_buf_allocator);
  nm_req_chunk_allocator_destroy(&p_core->req_chunk_allocator);
  nm_ctrl_chunk_allocator_destroy(&p_core->ctrl_chunk_allocator);
  nm_core_dispatching_event_allocator_destroy(&p_core->dispatching_event_allocator);
  nm_core_monitor_vect_destroy(&p_core->monitors);

#if defined(NMAD_HWLOC)
  hwloc_topology_destroy(p_core->topology);
#endif /* NMAD_HWLOC */

  nm_core_unlock(p_core);
  nm_core_lock_destroy(p_core);

  nm_ns_exit(p_core);
  padico_free(p_core);

  return NM_ESUCCESS;
}

/** @file
 * Tree-based data structures: red-black trees, interval trees.
 */

#include "Puk.h"
#include <assert.h>

/* ** red-black tree *************************************** */

static inline void puk_rbtree_show_node(struct puk_rbtree_s*t, struct puk_rbtree_node_s*x, int offset);
static inline void puk_rbtree_check_consistency_node(struct puk_rbtree_s*t, struct puk_rbtree_node_s*x);
static inline void puk_rbtree_check_color_consistency_node(struct puk_rbtree_s*t, struct puk_rbtree_node_s*x);
static inline void puk_rbtree_check_consistency(struct puk_rbtree_s*t);
static inline void puk_rbtree_check_color_consistency(struct puk_rbtree_s*t);
static inline void puk_rbtree_rightrotate(struct puk_rbtree_s*tree, struct puk_rbtree_node_s*node);
static inline void puk_rbtree_leftrotate(struct puk_rbtree_s*tree, struct puk_rbtree_node_s*node);
static inline void puk_rbtree_insert_fixup(struct puk_rbtree_s*t, struct puk_rbtree_node_s*z);
static inline void puk_rbtree_node_inorder(struct puk_rbtree_s*t, struct puk_rbtree_node_s*node);
static inline void puk_rbtree_transplant(struct puk_rbtree_s*t, struct puk_rbtree_node_s*u, struct puk_rbtree_node_s*v);
static inline struct puk_rbtree_node_s*puk_rbtree_minimum(struct puk_rbtree_s*t, struct puk_rbtree_node_s*x);
static inline void puk_rbtree_delete_fixup(struct puk_rbtree_s*t, struct puk_rbtree_node_s*x);
static inline struct puk_rbtree_node_s*puk_rbtree_node_lookup(struct puk_rbtree_s*t, struct puk_rbtree_node_s*w, int data);
static inline int puk_rbtree_height_node(struct puk_rbtree_s*t, struct puk_rbtree_node_s*x);


void puk_rbtree_show(struct puk_rbtree_s*t)
{
  puk_rbtree_show_node(t, t->root, 0);
}

void puk_rbtree_insert(struct puk_rbtree_s*t, int data)
{
  /* check consistency before insert */
  puk_rbtree_check_consistency(t);
  puk_rbtree_check_color_consistency(t);
  struct puk_rbtree_node_s*z = malloc(sizeof(struct puk_rbtree_node_s));
  z->data   = data;
  z->left   = t->NIL;
  z->right  = t->NIL;
  z->parent = t->NIL;
  z->color  = PUK_RBTREE_RED;
  struct puk_rbtree_node_s*y = t->NIL;
  struct puk_rbtree_node_s*x = t->root;
  while(x != t->NIL)
    {
      y = x;
      if(z->data < x->data)
        x = x->left;
      else
        x = x->right;
    }
  z->parent = y;

  if(y == t->NIL)
    t->root = z;
  else if(z->data < y->data)
    y->left = z;
  else
    y->right = z;

  z->right = t->NIL;
  z->left = t->NIL;
  puk_rbtree_check_consistency(t);
  /* do not check color consistency before fixup; at this point, the tree
   * is a valid BST but not a valid RB-tree */
  puk_rbtree_insert_fixup(t, z);
  puk_rbtree_check_consistency(t);
  puk_rbtree_check_color_consistency(t);
}

void puk_rbtree_traversal(struct puk_rbtree_s*tree)
{
  puk_rbtree_node_inorder(tree, tree->root);
  printf("\n");
}

void puk_rbtree_delete(struct puk_rbtree_s*t, struct puk_rbtree_node_s*z)
{
  struct puk_rbtree_node_s*y = z;
  struct puk_rbtree_node_s*x;
  enum puk_rbtree_color_e y_orignal_color = y->color;

  puk_rbtree_check_consistency(t);
  puk_rbtree_check_color_consistency(t);

  if(z->left == t->NIL)
    {
      x = z->right;
      puk_rbtree_transplant(t, z, z->right);
    }
  else if(z->right == t->NIL)
    {
      x = z->left;
      puk_rbtree_transplant(t, z, z->left);
    }
  else
    {
      y = puk_rbtree_minimum(t, z->right);
      y_orignal_color = y->color;
      x = y->right;
      if(y->parent == z)
        {
          x->parent = y;
        }
      else
        {
          puk_rbtree_transplant(t, y, y->right);
          y->right = z->right;
          y->right->parent = y;
        }
      puk_rbtree_transplant(t, z, y);
      y->left = z->left;
      y->left->parent = y;
      y->color = z->color;
    }
  puk_rbtree_check_consistency(t);
  if(y_orignal_color == PUK_RBTREE_BLACK)
    {
      puk_rbtree_delete_fixup(t, x);
    }
  puk_rbtree_check_consistency(t);
  puk_rbtree_check_color_consistency(t);
}

struct puk_rbtree_node_s*puk_rbtree_lookup(struct puk_rbtree_s*t, int data)
{
  return puk_rbtree_node_lookup(t, t->root, data);
}

void puk_rbtree_init(struct puk_rbtree_s*t)
{
  t->NIL = malloc(sizeof(struct puk_rbtree_node_s));
  t->NIL->data = -1;
  t->NIL->left   = NULL;
  t->NIL->right  = NULL;
  t->NIL->parent = NULL;
  t->NIL->color  = PUK_RBTREE_BLACK;
  t->root = t->NIL;
}

int puk_rbtree_height(struct puk_rbtree_s*t)
{
  return puk_rbtree_height_node(t, t->root);
}

/* ** rbtree internals ************************************* */

static inline void puk_rbtree_show_node(struct puk_rbtree_s*t, struct puk_rbtree_node_s*x, int offset)
{
  static int OFFSET = 6;
  if(x->right != t->NIL)
    {
      puk_rbtree_show_node(t, x->right, offset + OFFSET);
    }
  int i;
  for(i = 0; i < offset; i++)
    printf(" ");
  char*link = " ";
  if( (x->parent != t->NIL) && (x->parent->left == x) )
    link = "\\";
  else if( (x->parent != t->NIL) && (x->parent->right == x) )
    link = "/";
  printf("%s-%s%d\n", link, (x->color == PUK_RBTREE_RED) ? "*" : " ", x->data);
  if(x->left != t->NIL)
    {
      puk_rbtree_show_node(t, x->left, offset + OFFSET);
    }
}

/** check that the parent of our sons is ourself */
static inline void puk_rbtree_check_consistency_node(struct puk_rbtree_s*t, struct puk_rbtree_node_s*x)
{
  if( (x == NULL) || (x == t->NIL) )
    return;
  assert( (x->left == t->NIL) || (x->left == NULL) || (x->left->parent == x) );
  assert( (x->right == t->NIL) || (x->right == NULL) || (x->right->parent == x) );
  if(x->left != t->NIL)
    {
      puk_rbtree_check_consistency_node(t, x->left);
    }
  if(x->right != t->NIL)
    {
      puk_rbtree_check_consistency_node(t, x->right);
    }
}

static inline void puk_rbtree_check_color_consistency_node(struct puk_rbtree_s*t, struct puk_rbtree_node_s*x)
{
  if(x == t->root)
    assert(x->color == PUK_RBTREE_BLACK);
  if( (x == NULL) || (x == t->NIL) )
    return;
  if(x->left != t->NIL)
    {
      assert((x->left->color == PUK_RBTREE_BLACK) || (x->color == PUK_RBTREE_BLACK));
      puk_rbtree_check_color_consistency_node(t, x->left);
    }
  if(x->right != t->NIL)
    {
      assert((x->right->color == PUK_RBTREE_BLACK) || (x->color == PUK_RBTREE_BLACK));
      puk_rbtree_check_color_consistency_node(t, x->right);
    }
}

static inline void puk_rbtree_check_consistency(struct puk_rbtree_s*t)
{
  puk_rbtree_check_consistency_node(t, t->root);
}

static inline void puk_rbtree_check_color_consistency(struct puk_rbtree_s*t)
{
  assert(t->NIL->color == PUK_RBTREE_BLACK);
  puk_rbtree_check_color_consistency_node(t, t->root);
}

/** @internal right rotation of the passed node */
static inline void puk_rbtree_rightrotate(struct puk_rbtree_s*tree, struct puk_rbtree_node_s*node)
{
  struct puk_rbtree_node_s*left = node->left;
  node->left = left->right;
  if(node->left != tree->NIL)
    node->left->parent = node;
  left->parent = node->parent;
  if(node->parent == tree->NIL)
    tree->root = left;
  else if(node == node->parent->left)
    node->parent->left = left;
  else
    node->parent->right = left;
  left->right = node;
  node->parent = left;
}

/** @internal left rotation of the passed node */
static inline void puk_rbtree_leftrotate(struct puk_rbtree_s*tree, struct puk_rbtree_node_s*node)
{
  struct puk_rbtree_node_s*right = node->right;
  node->right = right->left;
  if(node->right != tree->NIL)
    node->right->parent = node;
  right->parent = node->parent;
  if(node->parent == tree->NIL)
    tree->root = right;
  else if(node == node->parent->left)
    node->parent->left = right;
  else
    node->parent->right = right;
  right->left = node;
  node->parent = right;
}

/** @internal fix violations caused by BST insertion */
static inline void puk_rbtree_insert_fixup(struct puk_rbtree_s*t, struct puk_rbtree_node_s*z)
{
  while(z != t->root && z->parent->color == PUK_RBTREE_RED)
    {
      if(z->parent == z->parent->parent->left)
        {
          struct puk_rbtree_node_s*y = z->parent->parent->right;
          if(y->color == PUK_RBTREE_RED)
            {
              z->parent->color = PUK_RBTREE_BLACK;
              y->color = PUK_RBTREE_BLACK;
              z->parent->parent->color = PUK_RBTREE_RED;
              z = z->parent->parent;
            }
          else
            {
              if(z == z->parent->right)
                {
                  z = z->parent;
                  puk_rbtree_leftrotate(t, z);
                }
              z->parent->color = PUK_RBTREE_BLACK;
              z->parent->parent->color = PUK_RBTREE_RED;
              puk_rbtree_rightrotate(t, z->parent->parent);
            }
        }
      else
        {
          struct puk_rbtree_node_s*y = z->parent->parent->left;
          if(y->color == PUK_RBTREE_RED)
            {
              z->parent->color = PUK_RBTREE_BLACK;
              y->color = PUK_RBTREE_BLACK;
              z->parent->parent->color = PUK_RBTREE_RED;
              z = z->parent->parent;
            }
          else
            {
              if(z == z->parent->left)
                {
                  z = z->parent;
                  puk_rbtree_rightrotate(t, z);
                }
              z->parent->color = PUK_RBTREE_BLACK;
              z->parent->parent->color = PUK_RBTREE_RED;
              puk_rbtree_leftrotate(t, z->parent->parent);
            }
        }
    }
  t->root->color = PUK_RBTREE_BLACK;
}

static inline void puk_rbtree_node_inorder(struct puk_rbtree_s*t, struct puk_rbtree_node_s*node)
{
  if(node == NULL || node == t->NIL)
    return;
  puk_rbtree_node_inorder(t, node->left);
  printf("%d ", node->data);
  puk_rbtree_node_inorder(t, node->right);
}

static inline void puk_rbtree_transplant(struct puk_rbtree_s*t, struct puk_rbtree_node_s*u, struct puk_rbtree_node_s*v)
{
  if(u->parent == t->NIL)
    t->root = v;
  else if(u == u->parent->left)
    u->parent->left = v;
  else
    u->parent->right = v;
  v->parent = u->parent;
}

static inline struct puk_rbtree_node_s*puk_rbtree_minimum(struct puk_rbtree_s*t, struct puk_rbtree_node_s*x)
{
  while(x->left != t->NIL)
    x = x->left;
  return x;
}

static inline void puk_rbtree_delete_fixup(struct puk_rbtree_s*t, struct puk_rbtree_node_s*x)
{
  while(x != t->root && x->color == PUK_RBTREE_BLACK)
    {
      if(x == x->parent->left)
        {
          struct puk_rbtree_node_s*w = x->parent->right;
          if(w->color == PUK_RBTREE_RED)
            {
              w->color = PUK_RBTREE_BLACK;
              x->parent->color = PUK_RBTREE_RED;
              puk_rbtree_leftrotate(t, x->parent);
              w = x->parent->right;
            }
          if(w->left->color == PUK_RBTREE_BLACK && w->right->color == PUK_RBTREE_BLACK)
            {
              w->color = PUK_RBTREE_RED;
              x = x->parent;
            }
          else
            {
              if(w->right->color == PUK_RBTREE_BLACK)
                {
                  w->left->color = PUK_RBTREE_BLACK;
                  w->color = PUK_RBTREE_RED;
                  puk_rbtree_rightrotate(t, w);
                  w = x->parent->right;
                }
              w->color = x->parent->color;
              x->parent->color = PUK_RBTREE_BLACK;
              w->right->color = PUK_RBTREE_BLACK;
              puk_rbtree_leftrotate(t, x->parent);
              x = t->root;
            }
        }
      else
        {
          struct puk_rbtree_node_s*w = x->parent->left;
          if(w->color == PUK_RBTREE_RED)
            {
              w->color = PUK_RBTREE_BLACK;
              x->parent->color = PUK_RBTREE_RED;
              puk_rbtree_rightrotate(t, x->parent);
              w = x->parent->left;
            }
          if(w->right->color == PUK_RBTREE_BLACK && w->left->color == PUK_RBTREE_BLACK)
            {
              w->color = PUK_RBTREE_RED;
              x = x->parent;
            }
          else
            {
              if(w->left->color == PUK_RBTREE_BLACK)
                {
                  w->right->color = PUK_RBTREE_BLACK;
                  w->color = PUK_RBTREE_RED;
                  puk_rbtree_leftrotate(t, w);
                  w = x->parent->left;
                }
              w->color = x->parent->color;
              x->parent->color = PUK_RBTREE_BLACK;
              w->left->color = PUK_RBTREE_BLACK;
              puk_rbtree_rightrotate(t, x->parent);
              x = t->root;
            }
        }
    }
  x->color = PUK_RBTREE_BLACK;
}

static inline struct puk_rbtree_node_s*puk_rbtree_node_lookup(struct puk_rbtree_s*t, struct puk_rbtree_node_s*w, int data)
{
  if(w == NULL || w == t->NIL)
    return NULL;
  else if(data == w->data)
    return w;
  else if(data < w->data)
    return puk_rbtree_node_lookup(t, w->left, data);
  else
    return puk_rbtree_node_lookup(t, w->right, data);
}

static inline int puk_rbtree_height_node(struct puk_rbtree_s*t, struct puk_rbtree_node_s*x)
{
  if(x == t->NIL)
    {
      return 0;
    }
  else
    {
      const int h1 = puk_rbtree_height_node(t, x->left);
      const int h2 = puk_rbtree_height_node(t, x->right);
      if(h1 > h2)
        return 1 + h1;
      else
        return 1 + h2;
    }
}


/* ** interval tree **************************************** */

static inline void puk_itree_show_node(struct puk_itree_s*t, struct puk_itree_node_s*x, int offset);
static inline void puk_itree_check_consistency_node(struct puk_itree_s*t, struct puk_itree_node_s*x);
static inline void puk_itree_check_color_consistency_node(struct puk_itree_s*t, struct puk_itree_node_s*x);
static inline void puk_itree_check_max_consistency_node(struct puk_itree_s*t, struct puk_itree_node_s*x);
static inline void puk_itree_check_consistency(struct puk_itree_s*t);
static inline void puk_itree_check_color_consistency(struct puk_itree_s*t);
static inline void puk_itree_update_max(struct puk_itree_s*t, struct puk_itree_node_s*node);
static inline void puk_itree_rightrotate(struct puk_itree_s*tree, struct puk_itree_node_s*node);
static inline void puk_itree_leftrotate(struct puk_itree_s*tree, struct puk_itree_node_s*node);
static inline void puk_itree_insert_fixup(struct puk_itree_s*t, struct puk_itree_node_s*z);
static inline void puk_itree_node_inorder(struct puk_itree_s*t, struct puk_itree_node_s*node);
static inline void puk_itree_transplant(struct puk_itree_s*t, struct puk_itree_node_s*u, struct puk_itree_node_s*v);
static inline struct puk_itree_node_s*puk_itree_minimum(struct puk_itree_s*t, struct puk_itree_node_s*x);
static inline void puk_itree_delete_fixup(struct puk_itree_s*t, struct puk_itree_node_s*x);
static inline int puk_itree_height_node(struct puk_itree_s*t, struct puk_itree_node_s*x);
static inline int puk_itree_count_node(struct puk_itree_s*t, struct puk_itree_node_s*x);


void puk_itree_init(struct puk_itree_s*t)
{
  t->NIL = &t->nil;
  t->NIL->base   = 0;
  t->NIL->len    = 0;
  t->NIL->left   = NULL;
  t->NIL->right  = NULL;
  t->NIL->parent = NULL;
  t->NIL->color  = PUK_ITREE_BLACK;
  t->root = t->NIL;
}

void puk_itree_destroy(struct puk_itree_s*t)
{
  if(t->root != t->NIL)
    {
      padico_warning("interval tree not empty before destroy.\n");
    }
}

void puk_itree_show(struct puk_itree_s*t)
{
  puk_itree_show_node(t, t->root, 0);
}

struct puk_itree_node_s*puk_itree_lookup(struct puk_itree_s*t, uintptr_t base)
{
  struct puk_itree_node_s*x = t->root;
  while(x != t->NIL)
    {
      if(base == x->base)
        return x;
      else if(base < x->base)
        x = x->left;
      else
        x = x->right;
    }
  return NULL;
}

struct puk_itree_node_s*puk_itree_exact_lookup(struct puk_itree_s*t, uintptr_t base, size_t len)
{
  struct puk_itree_node_s*x = t->root;
  while(x != t->NIL)
    {
      if( (base == x->base) && (x->len == len) )
        {
          return x;
        }
      else if(base < x->base)
        x = x->left;
      else
        x = x->right;
    }
  return NULL;
}

struct puk_itree_node_s*puk_itree_interval_lookup(struct puk_itree_s*t, uintptr_t base, size_t len)
{
  struct puk_itree_node_s*x = t->root;
  while(x != t->NIL)
    {
      if( (base < x->base + x->len) && (x->base < base + len) )
        {
          assert( ((x->base >= base) && (x->base <= base + len)) ||
                  ((x->base + x->len >= base) && (x->base + x->len <= base + len)) ||
                  ((x->base < base) && (x->base + x->len) > base + len) );
          return x;
        }
      else if( (x->left != t->NIL) && (x->left->max >= base) )
        x = x->left;
      else
        x = x->right;
    }
  return NULL;
}

void puk_itree_insert(struct puk_itree_s*t, struct puk_itree_node_s*z, uintptr_t base, size_t len)
{
  assert(len > 0);
  /* check consistency before insert */
  puk_itree_check_consistency(t);
  puk_itree_check_color_consistency(t);
  z->base   = base;
  z->len    = len;
  z->max    = base + len;
  z->left   = t->NIL;
  z->right  = t->NIL;
  z->parent = t->NIL;
  z->color  = PUK_ITREE_RED;
  struct puk_itree_node_s*y = t->NIL;
  struct puk_itree_node_s*x = t->root;
  while(x != t->NIL)
    {
      if(x->max < z->max) /* update max while descending tree */
        x->max = z->max;
      y = x;
      if(z->base < x->base)
        x = x->left;
      else
        x = x->right;
    }
  z->parent = y;

  if(y == t->NIL)
    t->root = z;
  else if(z->base < y->base)
    y->left = z;
  else
    y->right = z;

  z->right = t->NIL;
  z->left = t->NIL;
  puk_itree_check_consistency(t);
  /* do not check color consistency before fixup; at this point, the tree
   * is a valid BST but not a valid RB-tree */
  puk_itree_insert_fixup(t, z);
  puk_itree_check_consistency(t);
  puk_itree_check_color_consistency(t);
}

void puk_itree_traversal(struct puk_itree_s*tree)
{
  puk_itree_node_inorder(tree, tree->root);
  printf("\n");
}

void puk_itree_delete(struct puk_itree_s*t, struct puk_itree_node_s*z)
{
  struct puk_itree_node_s*y = z;
  struct puk_itree_node_s*x;
  enum puk_itree_color_e y_orignal_color = y->color;
  struct puk_itree_node_s*p = z->parent; /* save orig parent to fix max */

  puk_itree_check_consistency(t);
  puk_itree_check_color_consistency(t);

  if(z->left == t->NIL)
    {
      x = z->right;
      puk_itree_transplant(t, z, z->right);
    }
  else if(z->right == t->NIL)
    {
      x = z->left;
      puk_itree_transplant(t, z, z->left);
    }
  else
    {
      y = puk_itree_minimum(t, z->right);
      y_orignal_color = y->color;
      x = y->right;
      if(y->parent == z)
        {
          x->parent = y;
        }
      else
        {
          puk_itree_transplant(t, y, y->right);
          y->right = z->right;
          y->right->parent = y;
        }
      puk_itree_transplant(t, z, y);
      y->left = z->left;
      y->left->parent = y;
      y->color = z->color;
    }
  /* fix max from parent of deleted node up to root */
  while( (p != t->NIL) && (p != NULL) )
    {
      puk_itree_update_max(t, p);
      p = p->parent;
    }
  assert(z != t->NIL);
  puk_itree_check_consistency(t);

  if(y_orignal_color == PUK_ITREE_BLACK)
    {
      puk_itree_delete_fixup(t, x);
    }
  puk_itree_check_consistency(t);
  puk_itree_check_color_consistency(t);
}

int puk_itree_height(struct puk_itree_s*t)
{
  return puk_itree_height_node(t, t->root);
}

int puk_itree_count(struct puk_itree_s*t)
{
  return puk_itree_count_node(t, t->root);
}


/* ** itree internals ************************************** */

static inline void puk_itree_show_node(struct puk_itree_s*t, struct puk_itree_node_s*x, int offset)
{
  static int OFFSET = 6;
  if( (x == NULL) || (x == t->NIL) )
    return;
  if(x->right != t->NIL)
    {
      puk_itree_show_node(t, x->right, offset + OFFSET);
    }
  int i;
  for(i = 0; i < offset; i++)
    printf(" ");
  char*link = " ";
  if( (x->parent != t->NIL) && (x->parent->left == x) )
    link = "\\";
  else if( (x->parent != t->NIL) && (x->parent->right == x) )
    link = "/";
  printf("%s-%s%lu:%lu:%lu (%p)\n", link, (x->color == PUK_ITREE_RED) ? "*" : " ", x->base, x->len, x->max, x);
  if(x->left != t->NIL)
    {
      puk_itree_show_node(t, x->left, offset + OFFSET);
    }
}

/** check that the parent of our sons is ourself */
static inline void puk_itree_check_consistency_node(struct puk_itree_s*t, struct puk_itree_node_s*x)
{
  if( (x == NULL) || (x == t->NIL) )
    return;
  assert( (x->left == t->NIL) || (x->left == NULL) || (x->left->parent == x) );
  assert( (x->right == t->NIL) || (x->right == NULL) || (x->right->parent == x) );
  if(x->left != t->NIL)
    {
      puk_itree_check_consistency_node(t, x->left);
    }
  if(x->right != t->NIL)
    {
      puk_itree_check_consistency_node(t, x->right);
    }
}

/** check consistency of colors of red-black tree */
static inline void puk_itree_check_color_consistency_node(struct puk_itree_s*t, struct puk_itree_node_s*x)
{
  if(x == t->root)
    assert(x->color == PUK_ITREE_BLACK);
  if( (x == NULL) || (x == t->NIL) )
    return;
  if(x->left != t->NIL)
    {
      assert((x->left->color == PUK_ITREE_BLACK) || (x->color == PUK_ITREE_BLACK));
      puk_itree_check_color_consistency_node(t, x->left);
    }
  if(x->right != t->NIL)
    {
      assert((x->right->color == PUK_ITREE_BLACK) || (x->color == PUK_ITREE_BLACK));
      puk_itree_check_color_consistency_node(t, x->right);
    }
}

/** check consistency of the max field of interval tree */
static inline void puk_itree_check_max_consistency_node(struct puk_itree_s*t, struct puk_itree_node_s*x)
{
  if(x == t->root)
    return;
  if(x->left != t->NIL)
    {
      puk_itree_check_max_consistency_node(t, x->left);
    }
  if(x->right != t->NIL)
    {
      puk_itree_check_max_consistency_node(t, x->right);
    }
  uintptr_t max = x->base + x->len;
  if( (x->left != t->NIL) && (x->left->max > max) )
    {
      max = x->left->max;
    }
  if( (x->right != t->NIL) && (x->right->max > max) )
    {
      max = x->right->max;
    }
  assert(max == x->max);
}

static inline void puk_itree_check_consistency(struct puk_itree_s*t)
{
  puk_itree_check_consistency_node(t, t->root);
  puk_itree_check_max_consistency_node(t, t->root);
}

static inline void puk_itree_check_color_consistency(struct puk_itree_s*t)
{
  assert(t->NIL->color == PUK_ITREE_BLACK);
  puk_itree_check_color_consistency_node(t, t->root);
}

/** @internal update max value from immediate sons */
static inline void puk_itree_update_max(struct puk_itree_s*t, struct puk_itree_node_s*node)
{
  node->max = node->base + node->len;
  if( (node->left != t->NIL) && (node->left->max > node->max) )
    node->max = node->left->max;
  if( (node->right != t->NIL) && (node->right->max > node->max) )
    node->max = node->right->max;
}

/** @internal right rotation of the passed node */
static inline void puk_itree_rightrotate(struct puk_itree_s*tree, struct puk_itree_node_s*node)
{
  struct puk_itree_node_s*left = node->left;
  node->left = left->right;
  if(node->left != tree->NIL)
    node->left->parent = node;
  left->parent = node->parent;
  if(node->parent == tree->NIL)
    tree->root = left;
  else if(node == node->parent->left)
    node->parent->left = left;
  else
    node->parent->right = left;
  left->right = node;
  node->parent = left;
  /* left is the new subtree root; update max in 'node' and 'left' */
  puk_itree_update_max(tree, node);
  puk_itree_update_max(tree, left);
}

/** @internal left rotation of the passed node */
static inline void puk_itree_leftrotate(struct puk_itree_s*tree, struct puk_itree_node_s*node)
{
  struct puk_itree_node_s*right = node->right;
  node->right = right->left;
  if(node->right != tree->NIL)
    node->right->parent = node;
  right->parent = node->parent;
  if(node->parent == tree->NIL)
    tree->root = right;
  else if(node == node->parent->left)
    node->parent->left = right;
  else
    node->parent->right = right;
  right->left = node;
  node->parent = right;
  /* right is the new subtree root; update max in 'node' and 'right' */
  puk_itree_update_max(tree, node);
  puk_itree_update_max(tree, right);
}

/** @internal fix violations caused by BST insertion */
static inline void puk_itree_insert_fixup(struct puk_itree_s*t, struct puk_itree_node_s*z)
{
  while(z != t->root && z->parent->color == PUK_ITREE_RED)
    {
      if(z->parent == z->parent->parent->left)
        {
          struct puk_itree_node_s*y = z->parent->parent->right;
          if(y->color == PUK_ITREE_RED)
            {
              z->parent->color = PUK_ITREE_BLACK;
              y->color = PUK_ITREE_BLACK;
              z->parent->parent->color = PUK_ITREE_RED;
              z = z->parent->parent;
            }
          else
            {
              if(z == z->parent->right)
                {
                  z = z->parent;
                  puk_itree_leftrotate(t, z);
                }
              z->parent->color = PUK_ITREE_BLACK;
              z->parent->parent->color = PUK_ITREE_RED;
              puk_itree_rightrotate(t, z->parent->parent);
            }
        }
      else
        {
          struct puk_itree_node_s*y = z->parent->parent->left;
          if(y->color == PUK_ITREE_RED)
            {
              z->parent->color = PUK_ITREE_BLACK;
              y->color = PUK_ITREE_BLACK;
              z->parent->parent->color = PUK_ITREE_RED;
              z = z->parent->parent;
            }
          else
            {
              if(z == z->parent->left)
                {
                  z = z->parent;
                  puk_itree_rightrotate(t, z);
                }
              z->parent->color = PUK_ITREE_BLACK;
              z->parent->parent->color = PUK_ITREE_RED;
              puk_itree_leftrotate(t, z->parent->parent);
            }
        }
    }
  t->root->color = PUK_ITREE_BLACK;
}

static inline void puk_itree_node_inorder(struct puk_itree_s*t, struct puk_itree_node_s*node)
{
  if(node == NULL || node == t->NIL)
    return;
  puk_itree_node_inorder(t, node->left);
  printf("%lu ", node->base);
  puk_itree_node_inorder(t, node->right);
}

static inline void puk_itree_transplant(struct puk_itree_s*t, struct puk_itree_node_s*u, struct puk_itree_node_s*v)
{
  if(u->parent == t->NIL)
    t->root = v;
  else if(u == u->parent->left)
    u->parent->left = v;
  else
    u->parent->right = v;
  v->parent = u->parent;
}

static inline struct puk_itree_node_s*puk_itree_minimum(struct puk_itree_s*t, struct puk_itree_node_s*x)
{
  while(x->left != t->NIL)
    x = x->left;
  return x;
}

static inline void puk_itree_delete_fixup(struct puk_itree_s*t, struct puk_itree_node_s*x)
{
  while(x != t->root && x->color == PUK_ITREE_BLACK)
    {
      if(x == x->parent->left)
        {
          struct puk_itree_node_s*w = x->parent->right;
          if(w->color == PUK_ITREE_RED)
            {
              w->color = PUK_ITREE_BLACK;
              x->parent->color = PUK_ITREE_RED;
              puk_itree_leftrotate(t, x->parent);
              w = x->parent->right;
            }
          if(w->left->color == PUK_ITREE_BLACK && w->right->color == PUK_ITREE_BLACK)
            {
              w->color = PUK_ITREE_RED;
              x = x->parent;
            }
          else
            {
              if(w->right->color == PUK_ITREE_BLACK)
                {
                  w->left->color = PUK_ITREE_BLACK;
                  w->color = PUK_ITREE_RED;
                  puk_itree_rightrotate(t, w);
                  w = x->parent->right;
                }
              w->color = x->parent->color;
              x->parent->color = PUK_ITREE_BLACK;
              w->right->color = PUK_ITREE_BLACK;
              puk_itree_leftrotate(t, x->parent);
              x = t->root;
            }
        }
      else
        {
          struct puk_itree_node_s*w = x->parent->left;
          if(w->color == PUK_ITREE_RED)
            {
              w->color = PUK_ITREE_BLACK;
              x->parent->color = PUK_ITREE_RED;
              puk_itree_rightrotate(t, x->parent);
              w = x->parent->left;
            }
          if(w->right->color == PUK_ITREE_BLACK && w->left->color == PUK_ITREE_BLACK)
            {
              w->color = PUK_ITREE_RED;
              x = x->parent;
            }
          else
            {
              if(w->left->color == PUK_ITREE_BLACK)
                {
                  w->right->color = PUK_ITREE_BLACK;
                  w->color = PUK_ITREE_RED;
                  puk_itree_leftrotate(t, w);
                  w = x->parent->left;
                }
              w->color = x->parent->color;
              x->parent->color = PUK_ITREE_BLACK;
              w->left->color = PUK_ITREE_BLACK;
              puk_itree_rightrotate(t, x->parent);
              x = t->root;
            }
        }
    }
  x->color = PUK_ITREE_BLACK;
}

static inline int puk_itree_height_node(struct puk_itree_s*t, struct puk_itree_node_s*x)
{
  if(x == t->NIL)
    {
      return 0;
    }
  else
    {
      const int h1 = puk_itree_height_node(t, x->left);
      const int h2 = puk_itree_height_node(t, x->right);
      if(h1 > h2)
        return 1 + h1;
      else
        return 1 + h2;
    }
}

static inline int puk_itree_count_node(struct puk_itree_s*t, struct puk_itree_node_s*x)
{
  if(x == t->NIL)
    {
      return 0;
    }
  else
    {
      return 1 + puk_itree_count_node(t, x->left) + puk_itree_count_node(t, x->right);
    }
}

dnl -*- mode: Autoconf;-*-

dnl -- Optionnal features
dnl --------------------------------------------------------

AC_DEFUN([AC_PADICO_CHECK_ZLIB],
         [
           AC_CHECK_HEADER(zlib.h,
                [ HAVE_ZLIB_H="yes"
                  HAVE_ZLIB="yes"
                  AC_DEFINE(HAVE_ZLIB_H, 1, [Define if you have the <zlib.h> header file.])
                  STATIC_LIBS="${STATIC_LIBS} -lz"
                ],
                [ HAVE_ZLIB_H="no"
                  HAVE_ZLIB="no" ])
           AC_SUBST(HAVE_ZLIB)
         ])

AC_DEFUN([AC_PADICO_CHECK_BZLIB],
         [
           AC_CHECK_HEADER(bzlib.h,
                [ HAVE_BZLIB_H="yes"
                  HAVE_BZLIB="yes"
                  AC_DEFINE(HAVE_BZLIB_H, 1, [Define if you have the <bzlib.h> header file.])
                  STATIC_LIBS="${STATIC_LIBS} -lbz2"
                ],
                [ HAVE_BZLIB_H="no"
                  HAVE_BZLIB="no" ])
           AC_SUBST(HAVE_BZLIB)
         ])

AC_DEFUN([AC_PADICO_CHECK_LZ4],
         [
           AC_CHECK_HEADER(lz4.h,
                [ HAVE_LZ4_H="yes"
                  HAVE_LIBLZ4="yes"
                  AC_DEFINE(HAVE_LZ4_H, 1, [Define if you have the <lz4.h> header file.])
                  STATIC_LIBS="${STATIC_LIBS} -llz4"
                ],
                [ HAVE_LZ4_H="no"
                  HAVE_LIBLZ4="no" ])
           AC_SUBST(HAVE_LIBLZ4)
         ])

AC_DEFUN([AC_PADICO_CHECK_LIBCURL],
         [
           AC_REQUIRE([PKG_PROG_PKG_CONFIG])
           PKG_CHECK_MODULES([libcurl], [libcurl], [HAVE_LIBCURL=yes], [HAVE_LIBCURL=no])
           if test x${HAVE_LIBCURL} = xyes; then
             AC_CHECK_HEADER(curl/curl.h, [ ], [ AC_MSG_ERROR([unusable libcurl]) ])
           fi
           AC_SUBST([HAVE_LIBCURL])
         ])

AC_DEFUN([AC_PADICO_CHECK_JANSSON],
         [
           AC_REQUIRE([PKG_PROG_PKG_CONFIG])
           PKG_CHECK_MODULES([jansson], [jansson], [HAVE_JANSSON=yes], [HAVE_JANSSON=no])
           if test x${HAVE_JANSSON} = xyes; then
             AC_CHECK_HEADER(jansson.h, [ ], [ AC_MSG_ERROR([unusable jansson]) ])
           fi
           AC_SUBST([HAVE_JANSSON])
         ])

AC_DEFUN([AC_PADICO_CHECK_VFS],
         [
          AC_CHECK_HEADER(sys/vfs.h, [ HAVE_SYS_VFS_H=yes ], [ HAVE_SYS_VFS_H=no ])
           if test x${HAVE_SYS_VFS_H} = xyes; then
            AC_DEFINE(HAVE_SYS_VFS_H, 1, [Define if you have the <sys/vfs.h> header file.])
           fi
         ])

# ## checks for hwloc
# output: with_hwloc, requires_hwloc, hwloc_CFLAGS|LIBS
AC_DEFUN([AC_PADICO_CHECK_HWLOC],
         [
           AC_REQUIRE([PKG_PROG_PKG_CONFIG])
           PKG_CHECK_MODULES([hwloc], [ hwloc ], [HAVE_HWLOC=yes], [HAVE_HWLOC=no])
           if test "${HAVE_HWLOC}" = "no"; then
             AC_MSG_ERROR([ required library hwloc not found.])
           fi
           if test "${HAVE_HWLOC}" = "yes"; then
             with_hwloc=yes
             requires_hwloc=hwloc
             AC_DEFINE([HAVE_HWLOC], [1], [whether hwloc is present])
           fi
           AC_SUBST([requires_hwloc])
           AC_SUBST([with_hwloc])
           AC_PATH_PROGS(HWLOC_BIND, [ hwloc-bind ])
           if test "x$HWLOC_BIND" = "x"; then
             AC_MSG_ERROR([hwloc-bind not found.])
           fi
           AC_PATH_PROGS(HWLOC_DISTRIB, [ hwloc-distrib ])
           if test "x$HWLOC_DISTRIB" = "x"; then
             AC_MSG_ERROR([hwloc-distrib not found.])
           fi
         ])


dnl -- check for Infiniband (ibverbs)
dnl --
AC_DEFUN([AC_PADICO_CHECK_INFINIBAND],
  [
    HAVE_INFINIBAND=no
    AC_ARG_WITH([ibverbs],
      [AS_HELP_STRING([--with-ibverbs], [use Infiniband ibverbs @<:@default=check@:>@] )])
    AC_ARG_VAR(IBHOME, [root of the ibverbs installation])
    if test "x${with_ibverbs}" != "xno"; then
      save_CPPFLAGS="${CPPFLAGS}"
      save_LIBS="${LIBS}"
      if test "x${with_ibverbs}" != "x" -a "${with_ibverbs}" != "xyes"; then
        IBHOME="${with_ibverbs}"
      fi
      if test "x$IBHOME" != "x" -a "x${IBHOME}" != "x/usr"; then
        ibverbs_CPPLAGS="-I${IBHOME}/include"
        ibverbs_LIBS="-Wl,-rpath,${IBHOME}/lib -L${IBHOME}/lib"
      fi
      CPPFLAGS="${CPPFLAGS} ${ibverbs_CPPFLAGS}"
      LIBS="${LIBS} ${ibverbs_LIBS}"
      AC_CHECK_HEADER(infiniband/verbs.h, [ HAVE_INFINIBAND_H=yes ], [ HAVE_INFINIBAND_H=no  ])
      AC_CHECK_LIB(ibverbs, ibv_open_device, [ HAVE_LIBIBVERBS=yes ], [ HAVE_LIBIBVERBS=no ])
      if test ${HAVE_INFINIBAND_H} = yes -a ${HAVE_LIBIBVERBS} = yes; then
        HAVE_INFINIBAND=yes
        ibverbs_LIBS="${ibverbs_LIBS} -libverbs"
        STATIC_LIBS="${STATIC_LIBS} ${ibverbs_LIBS}"
        AC_DEFINE(HAVE_IBVERBS, [ 1 ], [whether libibverbs is present ])
      else
        HAVE_INFINIBAND=no
        ibverbs_CPPFLAGS=
        ibverbs_LIBS=
        if test "x${with_ibverbs}" != "x"; then
          AC_MSG_FAILURE([--with-ibverbs was given, but infiniband/verbs.h is not found])
        fi
      fi
      LIBS="${LIBS} ${ibverbs_LIBS}"
      dnl -- check for ibv_query_device_ex
      AC_MSG_CHECKING([for ibv_query_device_ex])
      AC_LINK_IFELSE([AC_LANG_SOURCE([
#include <infiniband/verbs.h>
#include <stdlib.h>
int main()
{
  return ibv_query_device_ex(NULL, NULL, NULL);
}
])], [ HAVE_IBV_QUERY_DEVICE_EX=1 ], [ ])
      if test "x${HAVE_IBV_QUERY_DEVICE_EX}" = "x1"; then
        AC_MSG_RESULT([yes])
        AC_DEFINE(HAVE_IBV_QUERY_DEVICE_EX, [1 ], [whether libibverbs has ibv_query_device_ex() ])
      else
        AC_MSG_RESULT([no])
      fi
      dnl -- check for ODP symbols
      AC_MSG_CHECKING([for ibv ODP symbols])
      AC_LINK_IFELSE([AC_LANG_SOURCE([
#include <infiniband/verbs.h>
#include <stdlib.h>
#include <stdint.h>
int main()
{
  ibv_reg_mr(NULL, NULL, SIZE_MAX, IBV_ACCESS_REMOTE_WRITE | IBV_ACCESS_LOCAL_WRITE | IBV_ACCESS_ON_DEMAND);
  ibv_advise_mr(NULL, IBV_ADVISE_MR_ADVICE_PREFETCH_WRITE, IB_UVERBS_ADVISE_MR_FLAG_FLUSH, NULL, 0);
  return 0;
}
])], [ HAVE_IBVERBS_ODP=1 ], [ ])
      if test "x${HAVE_IBVERBS_ODP}" = "x1"; then
        AC_MSG_RESULT([yes])
        AC_DEFINE(HAVE_IBVERBS_ODP, [ 1 ], [whether libibverbs contains symbols for ODP ])
      else
        AC_MSG_RESULT([no])
      fi
      dnl -- check for implicit ODP symbols
      AC_MSG_CHECKING([for ibv implicit ODP symbols])
      AC_LINK_IFELSE([AC_LANG_SOURCE([
#include <infiniband/verbs.h>
#include <stdlib.h>
#include <stdint.h>
int main()
{
  int flag = IBV_ODP_SUPPORT_IMPLICIT;
  ibv_reg_mr(NULL, NULL, SIZE_MAX, IBV_ACCESS_REMOTE_WRITE | IBV_ACCESS_LOCAL_WRITE | IBV_ACCESS_ON_DEMAND);
  ibv_advise_mr(NULL, IBV_ADVISE_MR_ADVICE_PREFETCH_WRITE, IB_UVERBS_ADVISE_MR_FLAG_FLUSH, NULL, 0);
  return 0;
}
])], [ HAVE_IBVERBS_IODP=1 ], [ ])
      if test "x${HAVE_IBVERBS_IODP}" = "x1"; then
        AC_MSG_RESULT([yes])
        AC_DEFINE(HAVE_IBVERBS_IODP, [ 1 ], [whether libibverbs contains symbols for implicit ODP ])
      else
        AC_MSG_RESULT([no])
      fi
      CPPFLAGS="${save_CPPFLAGS}"
      LIBS="${save_LIBS}"
    fi
    AC_MSG_CHECKING([whether to build the ibverbs driver])
    AC_MSG_RESULT([${HAVE_INFINIBAND}])
    AC_SUBST(HAVE_INFINIBAND)
    AC_SUBST(ibverbs_CPPFLAGS)
    AC_SUBST(ibverbs_LIBS)
  ])

dnl -- check for Intel PSM
dnl --
AC_DEFUN([AC_PADICO_CHECK_PSM],
  [
    HAVE_PSM=no
    AC_ARG_WITH([psm],
                [AS_HELP_STRING([--with-psm],
                 [use Intel Performance Scaled Messaging (PSM) @<:@default=check@:>@] )],
                 [], [ with_psm=check ])
    if test "x${with_psm}" != "xno"; then
      save_CPPFLAGS="${CPPFLAGS}"
      save_LIBS="${LIBS}"
      if test "x${with_psm}" != "xyes" -a "x${with_psm}" != "xcheck"; then
        PSM_DIR="${with_psm}"
      fi
      if test "x${PSM_DIR}" != "x"; then
        psm_CPPFLAGS="-I${PSM_DIR}/include"
        psm_LIBS="-Wl,-rpath,${PSM_DIR}/lib -L${PSM_DIR}/lib"
      fi
      CPPFLAGS="${CPPFLAGS} ${psm_CPPFLAGS}"
      LIBS="${LIBS} ${psm_LIBS}"
      AC_CHECK_HEADER(psm.h, [ HAVE_PSM_H=yes ], [ HAVE_PSM_H=no ])
      AC_SEARCH_LIBS(psm_init, [ psm psm_infinipath ], [ HAVE_LIBPSM=yes ], [ HAVE_LIBPSM=no ])
      if test ${HAVE_PSM_H} = yes -a ${HAVE_LIBPSM} = yes; then
        HAVE_PSM=yes
        psm_LIBS="${psm_LIBS} ${ac_cv_search_psm_init}"
        STATIC_LIBS="${STATIC_LIBS} ${psm_LIBS}"
        AC_DEFINE(HAVE_PSM, [ 1 ], [whether libpsm is present ])
      else
        HAVE_PSM=no
        psm_CPPFLAGS=
        psm_LIBS=
        if test "x${with_psm}" != "xcheck"; then
          AC_MSG_FAILURE([--with-psm was given but psm.h is not found.])
        fi
      fi
      CPPFLAGS="${save_CPPFLAGS}"
      LIBS="${save_LIBS}"
    fi
    AC_MSG_CHECKING([whether to build the psm driver])
    AC_MSG_RESULT([${HAVE_PSM}])
    AC_SUBST(HAVE_PSM)
    AC_SUBST([psm_CPPFLAGS])
    AC_SUBST([psm_LIBS])
  ])

dnl -- check for Intel PSM2
dnl --
AC_DEFUN([AC_PADICO_CHECK_PSM2],
  [
    HAVE_PSM2=no
    AC_ARG_WITH([psm2],
                [AS_HELP_STRING([--with-psm2],
                 [use Intel Performance Scaled Messaging 2 (PSM2) @<:@default=check@:>@] )],
                 [], [ with_psm2=check ])
    if test "x${with_psm2}" != "xno"; then
      save_CPPFLAGS="${CPPFLAGS}"
      save_LIBS="${LIBS}"
      if test "x${with_psm2}" != "xyes" -a "x${with_psm2}" != "xcheck"; then
        PSM2_DIR="${with_psm2}"
      fi
      if test "x${PSM2_DIR}" != "x"; then
        psm2_CPPFLAGS="-I${PSM2_DIR}/include"
        psm2_LIBS="-Wl,-rpath,${PSM2_DIR}/lib -L${PSM2_DIR}/lib"
      fi
      CPPFLAGS="${CPPFLAGS} ${psm2_CPPFLAGS}"
      LIBS="${LIBS} ${psm2_LIBS}"
      AC_CHECK_HEADER(psm2.h, [ HAVE_PSM2_H=yes ], [ HAVE_PSM2_H=no ])
      AC_CHECK_LIB(psm2, psm2_init, [ HAVE_LIBPSM2=yes ], [ HAVE_LIBPSM2=no ])
      if test ${HAVE_PSM2_H} = yes -a ${HAVE_LIBPSM2} = yes; then
        HAVE_PSM2=yes
        psm2_LIBS="${psm2_LIBS} -lpsm2"
        STATIC_LIBS="${STATIC_LIBS} ${psm2_LIBS}"
        AC_DEFINE(HAVE_PSM2, [ 1 ], [whether libpsm2 is present ])
      else
        HAVE_PSM2=no
        psm2_CPPFLAGS=
        psm2_LIBS=
        if test "x${with_psm2}" != "xcheck"; then
          AC_MSG_FAILURE([--with-psm2 was given but psm2.h is not found.])
        fi
      fi
      CPPFLAGS="${save_CPPFLAGS}"
      LIBS="${save_LIBS}"
    fi
    AC_MSG_CHECKING([whether to build the psm2 driver])
    AC_MSG_RESULT([${HAVE_PSM2}])
    AC_SUBST(HAVE_PSM2)
    AC_SUBST([psm2_CPPFLAGS])
    AC_SUBST([psm2_LIBS])
  ])

dnl -- check for OFI (libfabric)
dnl --
AC_DEFUN([AC_PADICO_CHECK_OFI],
  [
    HAVE_OFI=no
    AC_ARG_WITH([ofi],
                [AS_HELP_STRING([--with-ofi],
                 [use OpenFabric Interface (libfabric) @<:@default=check@:>@] )],
                 [], [ with_ofi=check ])
    if test "x${with_ofi}" != "xno"; then
      PKG_CHECK_MODULES([ofi], [libfabric], [HAVE_OFI=yes], [HAVE_OFI=no])
    fi
    if test "x${with_ofi}" = "xyes" -a "${HAVE_OFI}" = "no"; then
      AC_MSG_ERROR([ required library OFI (libfabric) not found.])
    fi
    if test "${HAVE_OFI}" = "yes"; then
      save_CPPFLAGS="${CPPFLAGS}"
      save_LIBS="${LIBS}"
      CPPFLAGS="${CPPFLAGS} ${ofi_CFLAGS}"
      LIBS="${LIBS} ${ofi_LIBS}"
      AC_CHECK_HEADER(rdma/fabric.h,
        [
          HAVE_FABRIC_H=yes
          AC_DEFINE(HAVE_FABRIC_H, [ 1 ], [whether libfabric headers are present ])
          STATIC_LIBS="${STATIC_LIBS} `${PKG_CONFIG:-pkg-config} --libs --static libfabric`"
        ],
        [
          HAVE_FABRIC_H=no
          AC_MSG_WARN([ libfabric detected, but rdma/fabric.h not detected. Disable OFI. ])
          HAVE_OFI=no
        ])
      AC_CHECK_DECLS([FI_CONTEXT], [], [], [[#include <rdma/fabric.h>]])
      AC_CHECK_DECLS([FI_CONTEXT2], [], [], [[#include <rdma/fabric.h>]])
      CPPFLAGS="${save_CPPFLAGS}"
      LIBS="${save_LIBS}"
    fi
    AC_MSG_CHECKING([whether to build the OFI driver])
    AC_MSG_RESULT([${HAVE_OFI}])
    AC_SUBST([HAVE_OFI])
    AC_SUBST([ofi_CFLAGS])
    AC_SUBST([ofi_LIBS])
  ])

dnl -- check for portals4
dnl --
AC_DEFUN([AC_PADICO_CHECK_PORTALS4],
  [
    AC_REQUIRE([PKG_PROG_PKG_CONFIG])
    AC_REQUIRE([AC_PADICO_TYPES])
    HAVE_PORTALS4=no
    AC_ARG_WITH([portals4],
                [AS_HELP_STRING([--with-portals4],
                [use Portals4 network support @<:@default=check@:>@] )],
                [], [ with_portals4=check ])
    AC_ARG_VAR(PORTALS4_DIR, [root of the Portals4 installation])
    if test "x${with_portals4}" != "xno"; then
      PKG_CHECK_MODULES([portals], [portals4], [HAVE_PORTALS4=yes], [HAVE_PORTALS4=no])
      if test $HAVE_PORTALS4 = yes; then
        dnl -- Portals4 found through pkg-config
          STATIC_LIBS="${STATIC_LIBS} `${PKG_CONFIG:-pkg-config} --libs --static portals`"
          AC_DEFINE([HAVE_PORTALS4], [1], [whether portals4 is present])
      else
        dnl -- no pkg-config, try $PORTALS4_DIR
        if test "x${with_portals4}" != "xyes" -a "x${with_portals4}" != "xcheck"; then
          PORTALS4_DIR="${with_portals4}"
        fi
        if test "x${PORTALS4_DIR}" != "x"; then
          if test "$ac_cv_sizeof_ssize_t" = "8"; then
            bits=64
          elif test "${ac_cv_sizeof_ssize_t}" = "4"; then
            bits=32
          else
            bits=unknown
            AC_MSG_WARN([ cannot detect whether system is 32 or 64 bits. ])
          fi
          portals4_CFLAGS="-I${PORTALS4_DIR}/include/"
        fi
        save_CPPFLAGS="${CPPFLAGS}"
        save_LIBS="${LIBS}"
        CPPFLAGS="${save_CPPFLAGS} ${portals4_CFLAGS}"
        AC_CHECK_HEADER(portals4.h, [ HAVE_PORTALS4_H=yes ], [ HAVE_PORTALS4_H=no ])
        if test ${HAVE_PORTALS4_H} = yes; then
          portals4_LIBS="-Wl,-rpath,${PORTALS4_DIR}/lib -L${PORTALS4_DIR}/lib"
          LIBS="${save_LIBS} ${portals4_LIBS}"
          AC_CHECK_LIB(portals, PtlInit, [ HAVE_LIBPORTALS4=yes ], [ HAVE_LIBPORTALS4=no ])
          if test "${HAVE_LIBPORTALS4}" = "no"; then
            unset ac_cv_lib_portals_PtlInit
            portals4_LIBS="-Wl,-rpath,${PORTALS4_DIR}/lib${bits} -L${PORTALS4_DIR}/lib${bits}"
            LIBS="${save_LIBS} ${portals4_LIBS}"
            AC_CHECK_LIB(portals, PtlInit, [ HAVE_LIBPORTALS4=yes ], [ HAVE_LIBPORTALS4=no ])
          fi
        fi
        if test x${HAVE_PORTALS4_H} = xyes -a x${HAVE_LIBPORTALS4} = xyes; then
          HAVE_PORTALS4=yes
          portals4_LIBS="${portals4_LIBS} -lportals"
          STATIC_LIBS="${STATIC_LIBS} ${portals4_LIBS}"
          AC_DEFINE([HAVE_PORTALS4], [1], [whether portals4 is present])
        else
          HAVE_PORTALS4=no
          portals4_CFLAGS=
          portals4_LIBS=
          if test "x${with_portals4}" != "xcheck"; then
            AC_MSG_FAILURE([--with-portals4 was given but portals4.h is not found.])
          fi
        fi
        CPPFLAGS="${save_CPPFLAGS}"
        LIBS="${save_LIBS}"
      fi
    fi
    AC_MSG_CHECKING([whether to build the portals4 driver])
    AC_MSG_RESULT([${HAVE_PORTALS4}])
    AC_SUBST([HAVE_PORTALS4])
    AC_SUBST([portals4_CFLAGS])
    AC_SUBST([portals4_LIBS])
  ])

dnl -- check for slurm PMI2
dnl --
AC_DEFUN([AC_PADICO_CHECK_PMI2],
  [
  HAVE_PMI2=no
  AC_ARG_WITH([pmi2],
                [AS_HELP_STRING([--with-pmi2],
                 [use slurm pmi2 support @<:@default=check@:>@] )],
                 [], [ with_pmi2=check ])
  if test "x${with_pmi2}" != "xno"; then
    save_CPPFLAGS="${CPPFLAGS}"
    save_LIBS="${LIBS}"
    if test "x${with_pmi2}" != "xyes" -a "x${with_pmi2}" != "xcheck"; then
      PMI2_DIR="${with_pmi2}"
    fi
    if test "x${PMI2_DIR}" != "x"; then
      pmi2_CPPFLAGS="-I${PMI2_DIR}/include/"
      pmi2_LIBS="-Wl,-rpath,${PMI2_DIR}/lib64 -L${PMI2_DIR}/lib64"
    fi
    PKG_CHECK_MODULES([craypmi], [cray-pmi], [HAVE_CRAY_PMI=yes], [HAVE_CRAY_PMI=no])
    CPPFLAGS="${CPPFLAGS} ${pmi2_CPPFLAGS} ${craypmi_CFLAGS}"
    LIBS="${LIBS} ${pmi2_LIBS} ${craypmi_LIBS}"
    AC_CHECK_HEADER(slurm/pmi2.h, [ HAVE_SLURM_PMI2_H=yes ], [ HAVE_SLURM_PMI2_H=no ])
    AC_CHECK_HEADER(pmi2.h, [ HAVE_PMI2_H=yes ], [ HAVE_PMI2_H=no ])
    AC_CHECK_LIB(pmi2, PMI2_Init, [ HAVE_LIBPMI2=yes ], [ HAVE_LIBPMI2=no ])
    AC_MSG_CHECKING([for PMI2 support])
    CPPFLAGS="${save_CPPFLAGS}"
    LIBS="${save_LIBS}"
    if test ${HAVE_CRAY_PMI} = yes -a ${HAVE_PMI2_H} = yes; then
      AC_MSG_RESULT([craypmi])
      HAVE_PMI2=yes
      pmi2_CPPFLAGS="${craypmi_CFLAGS}"
      pmi2_LIBS="${craypmi_LIBS}"
      AC_DEFINE([HAVE_PMI2], [1], [whether pmi2 is present])
      AC_DEFINE([HAVE_PMI2_H], [1], [ whether pmi2.h is present ])
      AC_DEFINE([HAVE_CRAY_PMI], [1], [whether pmi2 is Cray PMI])
    elif test ${HAVE_SLURM_PMI2_H} = yes -a ${HAVE_LIBPMI2} = yes; then
      AC_MSG_RESULT([slurm pmi2])
      HAVE_PMI2=yes
      pmi2_LIBS="${pmi2_LIBS} -lpmi2"
      AC_DEFINE([HAVE_PMI2], [1], [whether pmi2 is present])
      AC_DEFINE([HAVE_SLURM_PMI2_H], [1], [ whether slurm/pmi2.h is present ])
    else
      AC_MSG_RESULT([none])
      HAVE_PMI2=no
      pmi2_CPPFLAGS=
      pmi2_LIBS=
      if test "x${with_pmi2}" != "xcheck"; then
        AC_MSG_FAILURE([--with-pmi2 was given but slurm/pmi2.h is not found.])
      fi
    fi
    STATIC_LIBS="${STATIC_LIBS} ${pmi2_LIBS}"
  fi
  AC_SUBST([HAVE_PMI2])
  AC_SUBST([HAVE_CRAY_PMI])
  AC_SUBST([pmi2_CPPFLAGS])
  AC_SUBST([pmi2_LIBS])
  ])

dnl -- check for slurm PMIX
dnl --
AC_DEFUN([AC_PADICO_CHECK_PMIX],
  [
    AC_REQUIRE([PKG_PROG_PKG_CONFIG])
    HAVE_PMIX=no
    AC_ARG_WITH([pmix],
                  [AS_HELP_STRING([--with-pmix],
                   [use slurm PMIx support @<:@default=check@:>@] )],
                   [], [ with_pmix=check ])
    if test "x${with_pmix}" != "xno"; then
      PKG_CHECK_MODULES([pmix], [ pmix ], [HAVE_PMIX=yes], [HAVE_PMIX=no])
    fi
    if test x${HAVE_PMIX} = xyes; then
      STATIC_LIBS="${STATIC_LIBS} `${PKG_CONFIG:-pkg-config} --libs --static pmix`"
      AC_DEFINE([HAVE_PMIX], [1], [whether slurm PMIx is present])
    else
      if test ${with_pmix} = yes; then
        AC_MSG_FAILURE([--with-pmix was given but pkg-config cannot find PMIx.])
      fi
      HAVE_PMIX=no
      pmix_CFLAGS=
      pmix_LIBS=
    fi
    AC_SUBST([HAVE_PMIX])
    AC_SUBST([pmix_CFLAGS])
    AC_SUBST([pmix_LIBS])
  ])

dnl -- libssh2
dnl --
AC_DEFUN([AC_PADICO_CHECK_LIBSSH2],
  [
    AC_CHECK_HEADER(libssh2.h,
      AC_CHECK_LIB(ssh2, libssh2_session_handshake,
        [ HAVE_LIBSSH2_H=yes
          AC_DEFINE(HAVE_LIBSSH2_H, 1, [Define if you have the <libssh2.h> header file.])
        STATIC_LIBS="${STATIC_LIBS} -lssh2"
        ],
        [ HAVE_LIBSSH2_H=no ])
          AC_SUBST(HAVE_LIBSSH2_H))
  ])

dnl -- libcgraph
dnl --
AC_DEFUN([AC_PADICO_CHECK_CGRAPH],
  [
    AC_REQUIRE([PKG_PROG_PKG_CONFIG])
    HAVE_CGRAPH=no
    AC_ARG_WITH([cgraph],
                  [AS_HELP_STRING([--with-cgraph],
                   [enable Graphviz cgraph for topology display @<:@default=check@:>@] )],
                   [], [ with_cgraph=check ])
    if test "x${with_cgraph}" != "xno"; then
      PKG_CHECK_MODULES([cgraph], [ libcgraph ], [HAVE_CGRAPH=yes], [HAVE_CGRAPH=no])
    fi
    if test x${HAVE_CGRAPH} = xyes; then
      STATIC_LIBS="${STATIC_LIBS} `${PKG_CONFIG:-pkg-config} --libs --static cgraph`"
      AC_DEFINE([HAVE_CGRAPH], [1], [whether Graphviz libcgraph is present])
    else
      if test ${with_cgraph} = yes; then
        AC_MSG_FAILURE([--with-cgraph was given but pkg-config cannot find libcgraph.])
      fi
      HAVE_CGRAPH=no
      cgraph_CFLAGS=
      cgraph_LIBS=
    fi
    AC_SUBST([HAVE_CGRAPH])
    AC_SUBST([cgraph_CFLAGS])
    AC_SUBST([cgraph_LIBS])
  ])

dnl -- simgrid
dnl --
AC_DEFUN([AC_PADICO_CHECK_SIMGRID],
         [
           AC_REQUIRE([PKG_PROG_PKG_CONFIG])
           HAVE_SIMGRID=no
           AC_ARG_WITH([simgrid],
             [AS_HELP_STRING([--with-simgrid], [build simgrid driver @<:@default=check@:>@] )],
             [], [ with_simgrid=check ])
           if test "x${with_simgrid}" != "xno" ; then
             PKG_CHECK_MODULES([simgrid], [ simgrid ], [HAVE_SIMGRID=yes], [HAVE_SIMGRID=no])
           fi
           if test x${HAVE_SIMGRID} = xyes; then
             AC_SUBST([simgrid_CFLAGS])
             AC_SUBST([simgrid_LIBS])
             AC_DEFINE(HAVE_SIMGRID, [ 1 ], [whether simgrid is present ])
           fi
           AC_SUBST([HAVE_SIMGRID])
           AC_MSG_CHECKING([whether simgrid supports header inclusion in C++ through C])
           AC_LANG_PUSH([C++])
           padico_simgrid_cplusplus=no
           AC_COMPILE_IFELSE(
             [ AC_LANG_PROGRAM([
                                 extern "C" {
                                 #include <simgrid/mailbox.h>
                                 #include <simgrid/comm.h>
                                 #include <simgrid/link.h>
                                 #include <simgrid/host.h>
                                 #include <simgrid/actor.h>
                                 #include <simgrid/engine.h>
                                 #include <simgrid/mutex.h>
                                 #include <simgrid/cond.h>
                                 }
                               ],
                      [ /* empty */ ] )],
             [padico_simgrid_cplusplus=yes],
             [padico_simgrid_cplusplus=no])
          AC_LANG_POP([C++])
          AC_MSG_RESULT([${padico_simgrid_cplusplus}])
          AC_SUBST([padico_simgrid_cplusplus])
        ])

dnl --- Types
dnl --------------------------------------------------------

# ## socklen_t
AC_DEFUN(AC_TYPE_SOCKLEN_T,
[AC_CACHE_CHECK([for socklen_t], ac_cv_type_socklen_t,
[
  AC_COMPILE_IFELSE(
    [ AC_LANG_PROGRAM([#include <sys/socket.h>
                      ],
                      [socklen_t len = 42; return len;] )],
    [ac_cv_type_socklen_t=yes],
    [ac_cv_type_socklen_t=no])
])
  if test $ac_cv_type_socklen_t = yes; then
    AC_DEFINE(HAVE_SOCKLEN_T, [], [whether socklen_t is defined in sys/socket.h])
  fi
])

# ## off64_t
AC_DEFUN([AC_TYPE_OFF64_T],
         [ AC_CACHE_CHECK([for off64_t], ac_cv_type_off64_t,
[
  AC_COMPILE_IFELSE(
    [ AC_LANG_PROGRAM( [#define _LARGEFILE64_SOURCE
                        #include <sys/types.h>
                       ],
                       [off64_t off = 42;]) ],
    [ac_cv_type_off64_t=yes],
    [ac_cv_type_off64_t=no])
])
  if test $ac_cv_type_off64_t = yes; then
    AC_DEFINE(HAVE_OFF64_T, [], [whether off64_t is defined in sys/types.h using _LARGEFILE64_SOURCE])
    largefilecflags=-D_LARGEFILE64_SOURCE
  fi
  AC_SUBST(largefilecflags)
])

# ## check base required types
AC_DEFUN([AC_PADICO_TYPES],
         [ AC_REQUIRE([AC_PADICO_COMPILER])
           AC_C_BIGENDIAN
           AC_CHECK_SIZEOF(int)
           AC_CHECK_SIZEOF(long)
           AC_CHECK_SIZEOF(long long)
           AC_CHECK_SIZEOF(ssize_t)
           AC_CHECK_SIZEOF(float)
           AC_CHECK_SIZEOF(double)
           AC_TYPE_SOCKLEN_T
           AC_TYPE_OFF_T
           AC_TYPE_OFF64_T
           AC_TYPE_SIZE_T
           AC_TYPE_PID_T
         ])

dnl --- libs
dnl --------------------------------------------------------

AC_DEFUN([AC_PADICO_LIBS],
         [ AC_REQUIRE([AC_PADICO_LINKER])
           AC_SEARCH_LIBS(dlopen,  [dl dlcompat c c++], [], [ AC_MSG_ERROR([Cannot find dlopen() ]) ])
           AC_SEARCH_LIBS(dlclose, [dl dlcompat c c++], [], [ AC_MSG_ERROR([Cannot find dlclose() ]) ])
           AC_SEARCH_LIBS(dlsym,   [dl dlcompat c c++], [], [ AC_MSG_ERROR([Cannot find dlsym() ]) ])

           AC_SEARCH_LIBS(sched_yield,   [c rt], [], [ AC_MSG_ERROR([Cannot find sched_yield() ]) ])

           AC_SEARCH_LIBS(socket, nsl socket, [], [ AC_MSG_ERROR([Cannot find socket() ]) ])
           AC_SEARCH_LIBS(listen, nsl socket, [], [ AC_MSG_ERROR([Cannot find listen() ]) ])
           AC_SEARCH_LIBS(accept, nsl socket, [], [ AC_MSG_ERROR([Cannot find accept() ]) ])
           AC_SEARCH_LIBS(gethostbyname, nsl socket, [], [ AC_MSG_ERROR([Cannot find gethostbyname() ]) ])

         ])


dnl --- Arguments
dnl --------------------------------------------------------

AC_DEFUN([AC_PADICO_OPTIMIZE],
         [ padico_enable_optimize="yes";
           AC_ARG_ENABLE(optimize,
                         [  --enable-optimize       Build with optimization compiler flags @<:@default: yes@:>@ ],
                         [ if test $enableval = no; then
                             padico_enable_optimize="no";
                           fi
                         ],
                         [])
           AC_SUBST(padico_enable_optimize)
         ])

AC_DEFUN([AC_PADICO_DEBUG],
         [ padico_enable_debug="no";
           AC_ARG_ENABLE(debug,
                         [  --enable-debug          Build with debug flags @<:@default: no@:>@],
                         [ if test $enableval = yes; then
                             padico_enable_debug="yes";
                           fi
                         ],
                         [])
           AC_SUBST(padico_enable_debug)
         ])

AC_DEFUN([AC_PADICO_TRACE],
         [ AC_ARG_ENABLE(trace,
                         [  --enable-trace ],
                         [ if test "x$enableval" = "xyes"; then
                             AC_DEFINE(PADICO_TRACE_ON, 1, [enable Padico trace system])
                           fi],
                         [])
         ])

AC_DEFUN([AC_PADICO_BUILTIN],
         [ padico_builtin_mods=no
           AC_ARG_ENABLE(builtin,
                         [  --enable-builtin        build all modules as builtin @<:@default: no@:>@],
                         [ if test "x$enableval" = "xyes"; then
                             padico_builtin_mods=yes
                           fi],
                         [])
           AC_SUBST(padico_builtin_mods)
         ])

AC_DEFUN([AC_PADICO_STATIC],
         [ padico_enable_static=no
           AC_ARG_ENABLE(static,
                         [  --enable-static         build static libraries @<:@default: no@:>@],
                         [ if test "x$enableval" = "xyes"; then
                             padico_enable_static=yes
                           fi],
                         [])
           AC_SUBST(padico_enable_static)
           PKG_CONFIG="${PKG_CONFIG} --static"
         ])

AC_DEFUN([AC_PADICO_DYNAMIC],
         [ padico_enable_dynamic=yes
           AC_ARG_ENABLE(dynamic,
                         [  --enable-dynamic        build dynamic libraries @<:@default: yes@:>@],
                         [ if test "x$enableval" = "xno"; then
                             padico_enable_dynamic=no
                           fi],
                         [])
           AC_SUBST(padico_enable_dynamic)
         ])

dnl -- enable simgrid
dnl --
AC_DEFUN([AC_PADICO_SIMGRID],
         [AC_REQUIRE([AC_PADICO_CHECK_SIMGRID])
          AC_ARG_ENABLE(simgrid,
             [ AS_HELP_STRING([--enable-simgrid], [Enable Simgrid simulation mode  @<:@default=no@:>@]) ],
             [ if test $enableval = yes; then
                 if test x${HAVE_SIMGRID} != xyes; then
                   AC_MSG_ERROR([trying to enable simgrid mode, but libsimgrid not detected.])
                 fi
                 padico_enable_simgrid=yes
                 AC_DEFINE([PADICO_ENABLE_SIMGRID], 1, [enable Simgrid simulation mode])
               else
                 padico_enable_simgrid=no
               fi
             ],
             [ padico_enable_simgrid=no ])
             AC_SUBST([padico_enable_simgrid])
         ])

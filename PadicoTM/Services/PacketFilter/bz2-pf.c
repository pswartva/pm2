/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief BZ2 Packet Filter component
 */

#include <Padico/Module.h>
#include "PacketFilter.h"
#include <bzlib.h>


/* ********************************************************* */
/* *** BZ2 Packet Filter *********************************** */
/* ********************************************************* */


/** @defgroup PacketFilter_BZ2 component: PacketFilter_BZ2- a packet filter that performs bzip2 compression
 * @ingroup PadicoComponent
 */

static void* bz2pf_instantiate(puk_instance_t assembly, puk_context_t context);
static void bz2pf_destroy(void*state);
static void bz2pf_init(void);
static void bz2pf_encode(void*, const void*, size_t, void**, size_t*);
static void bz2pf_decode(void*, const void*, size_t, void*, size_t*);

/** instanciation facet for PacketFilter_BZ2
 * @ingroup PacketFilter_BZ2
 */
static const struct puk_component_driver_s bz2_component_driver =
  {
    .instantiate    = &bz2pf_instantiate,
    .destroy        = &bz2pf_destroy,
    .component_init = &bz2pf_init
  };
/** 'PacketFilter' facet for PacketFilter_BZ2
 * @ingroup PacketFilter_BZ2
 */
static const struct packetfilter_driver_s bz2pf_driver =
  {
    .pf_encode = &bz2pf_encode,
    .pf_decode = &bz2pf_decode
  };

/* ********************************************************* */

static void bz2pf_init(void)
{
  padico_out(puk_verbose_notice, "using bzlib version %s\n", BZ2_bzlibVersion());
}

PADICO_MODULE_COMPONENT(PacketFilter_BZ2,
                        puk_component_declare("PacketFilter_BZ2",
                                              puk_component_provides("PadicoComponent", "component", &bz2_component_driver),
                                              puk_component_provides("PacketFilter", "pf", &bz2pf_driver),
                                              puk_component_attr("block_size", "5"),
                                              puk_component_attr("work_factor", "30")));

/* ********************************************************* */

/** instance status for PacketFilter_BZ2
 * @ingroup PacketFilter_BZ2
 */
struct bz2_state_s
{
  int block_size;  /* default: 5 */
  int work_factor; /* default: 0 (30) */
  int small;
};


static void* bz2pf_instantiate(puk_instance_t assembly, puk_context_t context)
{
  struct bz2_state_s*state = padico_malloc(sizeof(struct bz2_state_s));
  const char*block_size = puk_context_getattr(context, "block_size");
  const char*work_factor = puk_context_getattr(context, "work_factor");
  assert(block_size && work_factor);
  state->block_size = atoi(block_size);
  state->work_factor = atoi(work_factor);
  state->small = 0;
  return state;
}

static void bz2pf_destroy(void*state)
{
  assert(state != NULL);
  padico_free(state);
}


/* 'out' allocated by encode function
 */
static void bz2pf_encode(void*w,
                       const void*in, size_t in_size,
                       void**out, size_t* out_size)
{
  struct bz2_state_s*state = w;
  unsigned int s = in_size + in_size / 16 + 600;
  void*p = padico_malloc(s);
  int rc = BZ2_bzBuffToBuffCompress(p, &s, (void*)in, in_size,
                                    state->block_size, 0, state->work_factor);
  if(rc != BZ_OK)
    {
      /* compression failed: don't compress */
      padico_fatal("compression failed- rc=%d\n", rc);
      memcpy(p, in, in_size);
      s = in_size;
    }
  *out = p;
  *out_size = s;
}

/* 'out' allocated by caller
*/
static void bz2pf_decode(void*w,
                          const void*in, size_t in_size,
                          void*out, size_t* out_size)
{
  struct bz2_state_s*state = w;
  unsigned int s = *out_size;
  int rc = BZ2_bzBuffToBuffDecompress(out, &s, (void*)in, in_size,
                                      state->small, 0);
  if(rc != BZ_OK)
    {
      padico_fatal("CRC error while uncompressing- in_size=%d; out_size=%d; rc=%d\n",
                   (int)in_size, (int)*out_size, rc);
    }
  *out_size = s;
}

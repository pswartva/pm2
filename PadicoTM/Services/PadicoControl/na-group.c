/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Padico PadicoControl group control
 * @ingroup PadicoControl
 */

#include <Padico/Puk.h>
#include <Padico/Module.h>
#include <Padico/PadicoTM.h>
#include <Padico/Topology.h>

#include "PadicoControl-internals.h"

PADICO_MODULE_HOOK(PadicoControl);

/* ********************************************************* */

/** a group of nodes */
struct padico_group_s
{
  const char*group_id;
  padico_topo_node_vect_t nodes;
};

/** a group request.
 * @note this is a struct only to allow a private definition of
 * the content of padico_group_req_t
 */
struct padico_group_req_s
{
  padico_reqs_vect_t reqs;
};

/** a running barrier */
struct na_group_barrier_s
{
  const char*group_id; /**< group name; not group pointer, because it may not be created yet when receiving stage1 msgs */
  int count;           /**< on node0: number of ready nodes; on other nodes, whether stage2 has been received */
};

PUK_HASHTABLE_TYPE(na_group_barrier, const char*, struct na_group_barrier_s*,
                   puk_hash_string_default_hash, puk_hash_string_default_eq, NULL);

static void na_group_destructor(const char*label, struct padico_group_s*group);

PUK_HASHTABLE_TYPE(padico_group, const char*, struct padico_group_s*,
                   puk_hash_string_default_hash, puk_hash_string_default_eq, &na_group_destructor);

static struct
{
  marcel_mutex_t lock;
  marcel_cond_t cond;
  struct padico_group_hashtable_s groups;
  struct na_group_barrier_hashtable_s barriers;
} na_group;

static void na_group_destructor(const char*label, struct padico_group_s*group)
{
  padico_group_destroy(group);
}

/* ********************************************************* */

static void defgroup_end_handler(puk_parse_entity_t e)
{
  const int publish = puk_opt_parse_bool(puk_parse_getattr(e, "publish"));
  padico_group_t group = padico_group_init(puk_parse_getattr(e, "name"));

  puk_parse_entity_t*i;
  puk_vect_foreach(i, puk_parse_entity, puk_parse_get_contained(e))
    {
      padico_topo_node_t node = puk_parse_get_content(*i);
      if(node == NULL)
        {
          padico_warning("bad node in group.\n");
          puk_parse_set_rc(e, padico_rc_error("bad node in group"));
          return;
        }
      padico_group_node_add(group, node);
    }
  if(publish)
    {
      padico_group_publish(group);
    }
  else
    {
      padico_group_store(group);
    }
}
static struct puk_tag_action_s defgroup_action =
{
  .xml_tag        = "PadicoControl:defgroup",
  .end_handler    = &defgroup_end_handler,
  .required_level = PUK_TRUST_CONTROL
};

/* ********************************************************* */

static void killgroup_end_handler(puk_parse_entity_t e)
{
  const char*groupid = puk_parse_getattr(e, "GroupID");
  assert(groupid != NULL);
  padico_group_t group = padico_group_lookup(groupid);
  assert(group != NULL);
  padico_group_kill(group, 0);
}

static struct puk_tag_action_s killgroup_action =
{
  .xml_tag        = "PadicoControl:killgroup",
  .start_handler  = NULL,
  .end_handler    = &killgroup_end_handler,
  .required_level = PUK_TRUST_CONTROL
};

static void group_barrier_stage1_end_handler(puk_parse_entity_t e)
{
  const char*group_name = puk_parse_getattr(e, "group");
  marcel_mutex_lock(&na_group.lock);
  struct na_group_barrier_s*barrier = na_group_barrier_hashtable_lookup(&na_group.barriers, group_name);
  if(barrier == NULL)
    {
      barrier = padico_malloc(sizeof(struct na_group_barrier_s));
      barrier->group_id = padico_strdup(group_name);
      barrier->count = 0;
      na_group_barrier_hashtable_insert(&na_group.barriers, barrier->group_id, barrier);
    }
  barrier->count++;
  marcel_cond_signal(&na_group.cond);
  marcel_mutex_unlock(&na_group.lock);
}
static struct puk_tag_action_s group_barrier_stage1_action =
{
  .xml_tag        = "PadicoControl:barrier-stage1",
  .start_handler  = NULL,
  .end_handler    = &group_barrier_stage1_end_handler,
  .required_level = PUK_TRUST_CONTROL
};

static void group_barrier_stage2_end_handler(puk_parse_entity_t e)
{
  const char*group_name = puk_parse_getattr(e, "group");
  marcel_mutex_lock(&na_group.lock);
  struct na_group_barrier_s*barrier = na_group_barrier_hashtable_lookup(&na_group.barriers, group_name);
  assert(barrier != NULL);
  barrier->count++;
  marcel_cond_signal(&na_group.cond);
  marcel_mutex_unlock(&na_group.lock);
}
static struct puk_tag_action_s group_barrier_stage2_action =
{
  .xml_tag        = "PadicoControl:barrier-stage2",
  .start_handler  = NULL,
  .end_handler    = &group_barrier_stage2_end_handler,
  .required_level = PUK_TRUST_CONTROL
};


/* ********************************************************* */

void padico_control_group_init(void)
{
  padico_group_hashtable_init(&na_group.groups);
  na_group_barrier_hashtable_init(&na_group.barriers);
  marcel_mutex_init(&na_group.lock, NULL);
  marcel_cond_init(&na_group.cond, NULL);
  puk_xml_add_action(defgroup_action);
  puk_xml_add_action(killgroup_action);
  puk_xml_add_action(group_barrier_stage1_action);
  puk_xml_add_action(group_barrier_stage2_action);
}

void padico_control_group_finalize(void)
{
  padico_group_hashtable_destroy(&na_group.groups);
  na_group_barrier_hashtable_destroy(&na_group.barriers);
  puk_xml_delete_action("PadicoControl:defgroup");
  puk_xml_delete_action("PadicoControl:killgroup");
  puk_xml_delete_action("PadicoControl:barrier-stage1");
  puk_xml_delete_action("PadicoControl:barrier-stage2");
}

/* *** Group ************************************************ */

padico_group_t padico_group_init(const char*id)
{
  padico_group_t g = padico_malloc(sizeof(struct padico_group_s));
  g->nodes    = padico_topo_node_vect_new();
  g->group_id = padico_strdup(id);
  return g;
}

int padico_group_store(padico_group_t g)
{
  padico_trace("storing group %s nodes=%d\n", g->group_id, padico_topo_node_vect_size(g->nodes));
  marcel_mutex_lock(&na_group.lock);
  padico_group_hashtable_insert(&na_group.groups, g->group_id, g);
  marcel_mutex_unlock(&na_group.lock);
  return 0;
}

padico_group_t padico_group_lookup(const char*id)
{
  marcel_mutex_lock(&na_group.lock);
  padico_group_t g = padico_group_hashtable_lookup(&na_group.groups, id);
  marcel_mutex_unlock(&na_group.lock);
  return g;
}

void padico_group_node_add(padico_group_t g, padico_topo_node_t node)
{
  if(node != NULL)
    {
      padico_topo_node_vect_push_back(g->nodes, node);
    }
  else
    {
      padico_warning("trying to add a NULL node to a group.\n");
    }
}

padico_group_t padico_group_dup(padico_group_t g, const char*new_name)
{
  padico_group_t ng = padico_group_init(new_name);
  int i;
  for(i = 0; i < padico_topo_node_vect_size(g->nodes); i++)
    {
      padico_group_node_add(ng, padico_topo_node_vect_at(g->nodes, i));
    }
  padico_group_store(ng);
  return ng;
}

int padico_group_size(padico_group_t g)
{
  return padico_topo_node_vect_size(g->nodes);
}

int padico_group_rank(padico_group_t g, padico_topo_node_t node)
{
  int i;
  for(i = 0; i < padico_topo_node_vect_size(g->nodes); i++)
    {
      if(padico_topo_node_vect_at(g->nodes, i) == node)
        return i;
    }
  return -1;
}

const char*padico_group_name(padico_group_t group)
{
  return group->group_id;
}

padico_topo_node_t padico_group_node(padico_group_t g, int rank)
{
  return padico_topo_node_vect_at(g->nodes, rank);
}


void padico_group_destroy(padico_group_t g)
{
  marcel_mutex_lock(&na_group.lock);
  padico_group_hashtable_remove(&na_group.groups, g->group_id);
  marcel_mutex_unlock(&na_group.lock);
  padico_free((void*)g->group_id);
  padico_topo_node_vect_delete(g->nodes);
  padico_free(g);
}


void padico_group_publish(padico_group_t group)
{
  padico_topo_node_vect_itor_t nodes_itor;
  padico_string_t cmd = padico_string_new();
  padico_group_req_t greq;
  padico_group_store(group);
  if(padico_topo_getlocalnode() == padico_topo_node_vect_at(group->nodes, 0))
    {
      padico_string_printf(cmd, "<PadicoControl:defgroup name=\"%s\">\n", group->group_id);
      for(nodes_itor  = padico_topo_node_vect_begin(group->nodes);
          nodes_itor != padico_topo_node_vect_end(group->nodes);
          nodes_itor  = padico_topo_node_vect_next(nodes_itor))
        {
          padico_string_catf(cmd, "  <Topology:node name=\"%s\"/>\n",
                             padico_topo_node_getname(*nodes_itor));
        }
      padico_string_cat(cmd, "</PadicoControl:defgroup>\n");
      greq = padico_group_send_to_all(group, padico_string_get(cmd));
      padico_group_wait_all(group, greq);
    }
  padico_string_delete(cmd);
}

void padico_group_kill(padico_group_t group, int rc)
{
  padico_string_t cmd = padico_string_new();
  padico_string_printf(cmd, "<kill rc=\"%d\"/>", rc);
  int i;
  for(i = 0; i < padico_topo_node_vect_size(group->nodes); i++)
    {
      const padico_topo_node_t node = padico_topo_node_vect_at(group->nodes, i);
      if(node != padico_topo_getlocalnode())
        {
          padico_control_send_oneway(node, padico_string_get(cmd));
        }
    }
  padico_string_delete(cmd);
}

static void group_host_table_destructor(void*key, void*data)
{
  padico_topo_node_vect_t v = data;
  if(v != NULL)
    padico_topo_node_vect_delete(v);
}

padico_group_t padico_group_topology(padico_group_t g, const char*distrib)
{
  puk_hashtable_t table = puk_hashtable_new_ptr();
  padico_topo_host_vect_t hosts = padico_topo_host_vect_new();
  /* build hastable: host -> nodes vect; build hosts index *with order preserved* */
  padico_topo_node_vect_itor_t i;
  puk_vect_foreach(i, padico_topo_node, g->nodes)
    {
      padico_topo_node_t n = *i;
      padico_topo_host_t h = padico_topo_node_gethost(n);
      padico_topo_node_vect_t v = puk_hashtable_lookup(table, h);
      if(v == NULL)
        {
          v = padico_topo_node_vect_new();
          puk_hashtable_insert(table, h, v);
          padico_topo_host_vect_push_back(hosts, h);
        }
      padico_topo_node_vect_push_back(v, n);
    }
  /* compute matrix bounds */
  const int maxhosts = puk_hashtable_size(table);
  int maxnodes = 0;
  {
    puk_hashtable_enumerator_t e = puk_hashtable_enumerator_new(table);
    padico_topo_node_vect_t v = puk_hashtable_enumerator_next_data(e);
    while(v != NULL)
      {
        const int size = padico_topo_node_vect_size(v);
        if(size > maxnodes)
          maxnodes = size;
        v = puk_hashtable_enumerator_next_data(e);
      }
    puk_hashtable_enumerator_delete(e);
  }
  padico_topo_node_t*matrix = calloc(maxhosts * maxnodes, sizeof(padico_topo_node_t));
  padico_out(40, "maxhosts = %d; maxnodes = %d\n", maxhosts, maxnodes);
  /* store nodes at their new place in matrix */
  {
    padico_topo_host_vect_itor_t h = NULL;
    puk_vect_foreach(h, padico_topo_host, hosts)
      {
        padico_topo_node_vect_t v = puk_hashtable_lookup(table, *h);
        puk_vect_foreach(i, padico_topo_node, v)
          {
            const int nhost = padico_topo_host_vect_rank(hosts, h);
            const int nnode = padico_topo_node_vect_rank(v, i);
            padico_topo_node_t n = *i;
            if(strcmp(distrib, "cyclic") == 0)
              {
                assert(matrix[nnode * maxhosts + nhost] == NULL);
                matrix[nnode * maxhosts + nhost] = n;
              }
            else if(strcmp(distrib, "block") == 0)
              {
                assert(matrix[nhost * maxnodes + nnode] == NULL);
                matrix[nhost * maxnodes + nnode] = n;
              }
            else
              {
                padico_fatal("unknown distribution %s\n", distrib);
              }
            *i = NULL;
          }
      }
  }
  /* flatten matrix to eliminate holes, in case number of nodes is not multiple of number of hosts */
  padico_group_t newgroup = padico_group_init(distrib);
  int k;
  for(k = 0; k < maxhosts * maxnodes; k++)
    {
      padico_topo_node_t n = matrix[k];
      if(n != NULL)
        {
          padico_group_node_add(newgroup, n);
          padico_out(40, "group: add node %s\n", padico_topo_node_getname(n));
        }
    }
  /* cleanup */
  puk_hashtable_delete(table, &group_host_table_destructor);
  padico_topo_host_vect_delete(hosts);
  padico_free(matrix);
  return newgroup;
}


/* *** Group-control **************************************** */

/** Sends a command to each member of a group (except self)
 * @param group The destination group of nodes (local node should
 *  be in the group, but won't receive the command)
 * @param cmd the command to send (NULL-terminated text, in XML)
 * @param greq a pointer to a grouped request descriptor
 */
padico_group_req_t padico_group_send_to_all(padico_group_t group, const char*cmd)
{
  padico_group_req_t greq = padico_malloc(sizeof(struct padico_group_req_s));
  int i;
  padico_out(40, "sending command to group %s\n%s", group->group_id, cmd);
  greq->reqs = padico_reqs_vect_new();
  for(i = 0; i < padico_topo_node_vect_size(group->nodes); i++)
    {
      const padico_topo_node_t node = padico_topo_node_vect_at(group->nodes, i);
      padico_out(60, "  %s -> node %d/%d\n", cmd, i, padico_topo_node_vect_size(group->nodes) );
      if(node != padico_topo_getlocalnode())
        {
          padico_req_t req = padico_control_send(node, cmd);
          padico_reqs_vect_push_back(greq->reqs, req);
        }
    }
  return greq;
}

padico_rc_t padico_group_wait_all(padico_group_t group, padico_group_req_t greq)
{
  padico_rc_t rc = NULL;
  int i;
  padico_out(50, "waiting ACKs for group %s\n", group->group_id);
  for(i = 0; i < padico_reqs_vect_size(greq->reqs); i++)
    {
      padico_rc_t urc = NULL;
      padico_req_t req = padico_reqs_vect_at(greq->reqs, i);
      padico_out(50, "waiting reply for grank=%d; req_id=%d\n",
                     i, req->id);
      urc = padico_tm_req_wait(req);
      rc = padico_rc_cat(rc, urc);
      if(urc)
        {
          padico_rc_delete(urc);
        }
      padico_out(50, "got reply from node grank=%d\n", i);
    }
  padico_reqs_vect_delete(greq->reqs);
  padico_free(greq);
  padico_out(50, "request completed on group %s\n", group->group_id);
  return rc;
}

/* ** Group barrier **************************************** */

void padico_group_barrier(padico_group_t group)
{
  padico_topo_node_t node0 = padico_group_node(group, 0);
  struct na_group_barrier_s*barrier = NULL;
  if(padico_group_size(group) <= 1)
    return;
  if(node0 == padico_topo_getlocalnode())
    {
      /* wait barrier-stage1 */
      while((barrier == NULL) || (barrier->count < padico_group_size(group) - 1))
        {
          marcel_mutex_lock(&na_group.lock);
          if(barrier == NULL)
            {
              barrier = na_group_barrier_hashtable_lookup(&na_group.barriers, padico_group_name(group));
            }
          if((barrier == NULL) || (barrier->count < padico_group_size(group) - 1))
            {
              marcel_cond_wait(&na_group.cond, &na_group.lock);
            }
          marcel_mutex_unlock(&na_group.lock);
        }

      /* send barrier-stage2 */
      int i;
      for(i = 0; i < padico_group_size(group); i++)
        {
          const padico_topo_node_t node = padico_group_node(group, i);
          if(node != padico_topo_getlocalnode())
            {
              padico_string_t msg_stage2 = padico_string_new();
              padico_string_printf(msg_stage2, "<PadicoControl:barrier-stage2 group=\"%s\"/>",
                                   padico_group_name(group));
              padico_control_send_oneway(node, padico_string_get(msg_stage2));
              padico_string_delete(msg_stage2);
            }
        }
    }
  else
    {
      /* init */
      barrier = padico_malloc(sizeof(struct na_group_barrier_s));
      barrier->group_id = padico_strdup(padico_group_name(group));
      barrier->count = 0;
      marcel_mutex_lock(&na_group.lock);
      na_group_barrier_hashtable_insert(&na_group.barriers, barrier->group_id, barrier);
      marcel_mutex_unlock(&na_group.lock);
      /* send barrier-stage1 */
      padico_string_t msg_ready = padico_string_new();
      padico_string_printf(msg_ready, "<PadicoControl:barrier-stage1 group=\"%s\" node=\"%s\"/>",
                            padico_group_name(group), padico_topo_node_getuuid(padico_topo_getlocalnode()));
      padico_control_send_oneway(node0, padico_string_get(msg_ready));
      padico_string_delete(msg_ready);
      /* wait barrier-stage2 */
      while(barrier->count == 0)
        {
          marcel_mutex_lock(&na_group.lock);
          marcel_cond_wait(&na_group.cond, &na_group.lock);
          marcel_mutex_unlock(&na_group.lock);
        }
    }
  marcel_mutex_lock(&na_group.lock);
  na_group_barrier_hashtable_remove(&na_group.barriers, barrier->group_id);
  marcel_mutex_unlock(&na_group.lock);
  padico_free((char*)barrier->group_id);
  padico_free(barrier);
}

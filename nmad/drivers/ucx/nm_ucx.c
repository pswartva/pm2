/*
 * NewMadeleine
 * Copyright (C) 2013-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <assert.h>

#include <nm_minidriver.h>
#include <nm_private.h>
#include <Padico/Module.h>

#include <ucp/api/ucp.h>

static void*nm_ucx_instantiate(puk_instance_t instance, puk_context_t context);
static void nm_ucx_destroy(void*status);

static const struct puk_component_driver_s nm_ucx_component =
  {
    .instantiate = &nm_ucx_instantiate,
    .destroy     = &nm_ucx_destroy
  };

static void nm_ucx_getprops(puk_context_t context, struct nm_minidriver_properties_s*props);
static void nm_ucx_init(puk_context_t context, const void**drv_url, size_t*url_size);
static void nm_ucx_close(puk_context_t context);
static void nm_ucx_connect(void*_status, const void*remote_url, size_t url_size);
static void nm_ucx_disconnect(void*_status);
static int  nm_ucx_send_iov_post(void*_status, const struct iovec*v, int n);
static int  nm_ucx_send_poll(void*_status);
static int  nm_ucx_recv_iov_post(void*_status,  struct iovec*v, int n);
static int  nm_ucx_recv_poll_one(void*_status);
static int  nm_ucx_recv_probe_any(puk_context_t p_context, void**_status);
static int  nm_ucx_recv_cancel(void*_status);

static const struct nm_minidriver_iface_s nm_ucx_minidriver =
  {
    .getprops         = &nm_ucx_getprops,
    .init             = &nm_ucx_init,
    .close            = &nm_ucx_close,
    .connect          = &nm_ucx_connect,
    .disconnect       = &nm_ucx_disconnect,
    .send_iov_post    = &nm_ucx_send_iov_post,
    .send_data_post   = NULL,
    .send_buf_get     = NULL,
    .send_buf_post    = NULL,
    .send_poll        = &nm_ucx_send_poll,
    .recv_iov_post    = &nm_ucx_recv_iov_post,
    .recv_data_post   = NULL,
    .recv_buf_poll    = NULL,
    .recv_buf_release = NULL,
    .recv_poll_one    = &nm_ucx_recv_poll_one,
    .recv_probe_any   = &nm_ucx_recv_probe_any,
    .recv_wait_any    = NULL,
    .recv_cancel      = &nm_ucx_recv_cancel,
    .recv_cancel_any  = NULL
  };

static int nm_ucx_convert_status(ucs_status_t status);
static void nm_ucx_err_handler(void*arg, ucp_ep_h ep, ucs_status_t status);

/* ********************************************************* */

PADICO_MODULE_COMPONENT(Minidriver_ucx,
  puk_component_declare("Minidriver_ucx",
                        puk_component_provides("PadicoComponent", "component", &nm_ucx_component),
                        puk_component_provides("NewMad_minidriver", "minidriver", &nm_ucx_minidriver), puk_component_attr("wait_any", "auto")));

/* ********************************************************* */

PUK_VECT_TYPE(nm_ucx_status, struct nm_ucx_s*);

#define NM_UCX_IOV_PREALLOC 8

struct nm_ucx_send_recv_ctx_s
{
  ucs_status_t status;
  void*p_req;
  struct ucp_dt_iov*payload;
  struct ucp_dt_iov payload_prealloc[NM_UCX_IOV_PREALLOC];
};

static inline void nm_ucx_iov_alloc(struct nm_ucx_send_recv_ctx_s*p_req, size_t n)
{
  if(n > NM_UCX_IOV_PREALLOC)
    {
      p_req->payload = padico_malloc(sizeof(struct ucp_dt_iov) * n);
    }
  else
    {
      p_req->payload = p_req->payload_prealloc;
    }
}
static inline void nm_ucx_iov_free(struct nm_ucx_send_recv_ctx_s*p_req)
{
  if(p_req->payload != p_req->payload_prealloc)
    {
      padico_free(p_req->payload);
    }
}

/** 'ucx' driver per-context data. */
struct nm_ucx_context_s
{
  ucp_context_h ucp_context;       /**< Server ucp context */
  ucp_worker_h  ucp_worker;        /**< Worker (connection client/server + send/recv) */
  ucp_address_t*local_addr;
  size_t local_addr_len;
  struct nm_ucx_status_vect_s p_statuses;
  int preserve_boundaries;
  nm_len_t request_size;           /**< size of ucp requests */
};

/* ********************************************************* */

/** 'ucx' per-instance status. */
struct nm_ucx_s
{
  ucp_ep_h ep;                            /**< ucp endpoint */
  ucp_tag_t local_tag;                    /**< local tag for the peer, used to match recv tags */
  ucp_tag_t remote_tag;                   /**< our tag in the remote host, used for sending */
  struct nm_ucx_context_s*p_ucx_context;  /**< per-context data */
  struct nm_ucx_send_recv_ctx_s send;
  struct nm_ucx_send_recv_ctx_s recv;
};

/* ********************************************************* */

static void*nm_ucx_instantiate(puk_instance_t instance, puk_context_t context)
{
  struct nm_ucx_s*p_status = padico_malloc(sizeof(struct nm_ucx_s));
  struct nm_ucx_context_s*p_ucx_context = puk_context_get_status(context);
  if(p_ucx_context == NULL)
    {
      NM_FATAL("trying to instantiate before context init.");
    }
  p_status->p_ucx_context = p_ucx_context;
  p_status->send.p_req = padico_malloc(p_ucx_context->request_size);
  p_status->recv.p_req = padico_malloc(p_ucx_context->request_size);
  p_status->local_tag = nm_ucx_status_vect_size(&p_ucx_context->p_statuses);
  nm_ucx_status_vect_push_back(&p_ucx_context->p_statuses, p_status);
  return p_status;
}

static void nm_ucx_destroy(void*_status)
{
  struct nm_ucx_s*p_status = _status;
  struct nm_ucx_context_s*p_ucx_context = p_status->p_ucx_context;

  /* Remove instance from global polling */
  nm_ucx_status_vect_itor_t it = nm_ucx_status_vect_find(&p_ucx_context->p_statuses, p_status);
  nm_ucx_status_vect_erase(&p_ucx_context->p_statuses, it);
  padico_free(p_status->send.p_req);
  padico_free(p_status->recv.p_req);
  padico_free(p_status);
}

/* ********************************************************* */

static void nm_ucx_getprops(puk_context_t context, struct nm_minidriver_properties_s*p_props)
{
  struct nm_ucx_context_s*p_ucx_context = padico_malloc(sizeof(struct nm_ucx_context_s));
  puk_context_set_status(context, p_ucx_context);

  p_props->capabilities.supports_recv_any = 1;
  p_props->capabilities.supports_data     = 0;
  p_props->capabilities.supports_buf_send = 0;
  p_props->capabilities.supports_buf_recv = 0;
  p_props->capabilities.supports_wait_any = 0;
  p_props->capabilities.supports_iovec    = 1;
  p_props->profile.latency = 500;
  p_props->profile.bandwidth = 8000;
  if(p_props->hints.kind == nm_trk_large)
    {
      /* we are guaranteed this trk is for rendez-vous -> don't need to preserve boundaries */
      p_props->capabilities.trk_rdv = 1;
      p_ucx_context->preserve_boundaries = 0;
    }
  else
    {
      p_ucx_context->preserve_boundaries = 1;
    }
  p_props->nickname = "ucx";
}

static void nm_ucx_init(puk_context_t context, const void**drv_url, size_t*url_size)
{
  struct nm_ucx_context_s*p_ucx_context = puk_context_get_status(context);

  ucp_config_t*config;
  ucs_status_t status = ucp_config_read(NULL, NULL, &config);
  if(status != UCS_OK)
  {
    NM_FATAL("ucp_config_read() failed (%s).\n", ucs_status_string(status));
  }
  ucp_params_t ucp_params;
  memset(&ucp_params, 0, sizeof(ucp_params));
  ucp_params.field_mask =
    UCP_PARAM_FIELD_FEATURES |
    UCP_PARAM_FIELD_MT_WORKERS_SHARED;
  ucp_params.features = UCP_FEATURE_TAG | UCP_FEATURE_STREAM;
  ucp_params.mt_workers_shared = 0;
  const char*s_size = puk_context_getattr(context, "session_size");
  if(s_size != NULL)
    {
      const int hint_size = atoi(s_size);
      if(hint_size > 0)
        {
          NM_DISPF("setting hint size = %d\n", hint_size);
          ucp_params.field_mask |= UCP_PARAM_FIELD_ESTIMATED_NUM_EPS;
          ucp_params.estimated_num_eps = hint_size;
        }
    }
  status = ucp_init(&ucp_params, config, &p_ucx_context->ucp_context);

  ucp_config_release(config);
  if(status != UCS_OK)
  {
    NM_FATAL("ucp_init() failed (%s).\n", ucs_status_string(status));
  }

  ucp_context_attr_t ucp_attr;
  ucp_attr.field_mask = UCP_ATTR_FIELD_REQUEST_SIZE;
  status = ucp_context_query(p_ucx_context->ucp_context, &ucp_attr);
  if(status != UCS_OK)
  {
    NM_FATAL("faild to query context (%s).\n", ucs_status_string(status));
  }
  p_ucx_context->request_size = ucp_attr.request_size;

  ucp_worker_params_t worker_params;
  memset(&worker_params, 0, sizeof(worker_params));
  worker_params.field_mask  = UCP_WORKER_PARAM_FIELD_THREAD_MODE;
  worker_params.thread_mode = UCS_THREAD_MODE_SINGLE;
  status = ucp_worker_create(p_ucx_context->ucp_context, &worker_params, &p_ucx_context->ucp_worker);
  if(status != UCS_OK)
  {
    NM_FATAL("ucp_worker_create() failed (%s).\n", ucs_status_string(status));
  }

  status = ucp_worker_get_address(p_ucx_context->ucp_worker,
                                  &p_ucx_context->local_addr,
                                  &p_ucx_context->local_addr_len);
  if(status != UCS_OK)
  {
    NM_FATAL("ucp_worker_get_address() failed (%s).\n", ucs_status_string(status));
  }

  nm_ucx_status_vect_init(&p_ucx_context->p_statuses);

  *drv_url = p_ucx_context->local_addr;
  *url_size = p_ucx_context->local_addr_len;
}

static void nm_ucx_close(puk_context_t context)
{
  struct nm_ucx_context_s*p_ucx_context = puk_context_get_status(context);
  puk_context_set_status(context, NULL);
  ucp_worker_release_address(p_ucx_context->ucp_worker, p_ucx_context->local_addr);
  ucp_worker_destroy(p_ucx_context->ucp_worker);
  ucp_cleanup(p_ucx_context->ucp_context);
  nm_ucx_status_vect_destroy(&p_ucx_context->p_statuses);
  padico_free(p_ucx_context);
}

static void nm_ucx_connect(void*_status, const void*remote_url, size_t url_size)
{
  struct nm_ucx_s*p_status = _status;
  struct nm_ucx_context_s*p_ucx_context = p_status->p_ucx_context;
  ucs_status_t status;
  ucp_ep_params_t ep_params;
  ep_params.field_mask      =
    UCP_EP_PARAM_FIELD_REMOTE_ADDRESS |
    UCP_EP_PARAM_FIELD_ERR_HANDLING_MODE |
    UCP_EP_PARAM_FIELD_ERR_HANDLER |
    UCP_EP_PARAM_FIELD_USER_DATA;
  ep_params.address         = (const ucp_address_t*)remote_url;
  ep_params.err_mode        = UCP_ERR_HANDLING_MODE_NONE;
  ep_params.err_handler.cb  = &nm_ucx_err_handler;
  ep_params.err_handler.arg = NULL;
  ep_params.user_data       = p_status;
  status = ucp_ep_create(p_ucx_context->ucp_worker, &ep_params, &p_status->ep);
  if(status != UCS_OK)
    {
      NM_FATAL("ucp_ep_create() failed (%s).\n", ucs_status_string(status));
    }
  /* ** exchange tags */
  {
    ucp_request_param_t sparam = { .op_attr_mask = 0 };
    ucs_status_ptr_t send_status = ucp_stream_send_nbx(p_status->ep, &p_status->local_tag, sizeof(ucp_tag_t), &sparam);
    if(UCS_PTR_IS_ERR(send_status))
      {
        NM_FATAL("error while sending tag (%s)\n", ucs_status_string(UCS_PTR_STATUS(send_status)));
      }
    size_t length;
    ucp_request_param_t rparam =
      {
       .op_attr_mask = UCP_OP_ATTR_FIELD_FLAGS,
       .flags = UCP_STREAM_RECV_FLAG_WAITALL
      };
    ucs_status_ptr_t recv_status = ucp_stream_recv_nbx(p_status->ep, &p_status->remote_tag, sizeof(ucp_tag_t), &length, &rparam);
    if(UCS_PTR_IS_ERR(recv_status))
      {
        NM_FATAL("error while receiving tag (%s)\n", ucs_status_string(UCS_PTR_STATUS(send_status)));
      }
    ucs_status_t status;
    if(send_status != NULL)
      {
        do
          {
            ucp_worker_progress(p_ucx_context->ucp_worker);
            status = ucp_request_check_status(send_status);
          }
        while(status == UCS_INPROGRESS);
        ucp_request_free(send_status);
        if(status != UCS_OK)
          {
            NM_FATAL("error while sending tag (%d)\n", status);
          }
      }
    if(recv_status != NULL)
      {
        do
          {
            ucp_worker_progress(p_ucx_context->ucp_worker);
            status = ucp_request_check_status(recv_status);
          }
        while(status == UCS_INPROGRESS);
        ucp_request_free(recv_status);
        if(status != UCS_OK)
          {
            NM_FATAL("error while receiving tag (%d)\n", status);
          }
      }
  }
}

static void nm_ucx_disconnect(void*_status)
{
  struct nm_ucx_s*p_status = _status;
  struct nm_ucx_context_s*p_ucx_context = p_status->p_ucx_context;
  void*close_req = ucp_ep_close_nb(p_status->ep, UCP_EP_CLOSE_MODE_FLUSH);
  if(UCS_PTR_IS_PTR(close_req))
  {
    ucs_status_t status;
    do
    {
      ucp_worker_progress(p_ucx_context->ucp_worker);
      status = ucp_request_check_status(close_req);
    }
    while(status == UCS_INPROGRESS);
    ucp_request_free(close_req);
  }
  else if(UCS_PTR_STATUS(close_req) != UCS_OK)
  {
    ucs_status_t status = UCS_PTR_STATUS(close_req);
    NM_FATAL("failed to close ep %p (%s)\n", (void*)p_status->ep, ucs_status_string(status));
  }
}

static int nm_ucx_send_iov_post(void*_status, const struct iovec*v, int n)
{
  struct nm_ucx_s*p_status = _status;
  struct nm_ucx_context_s*p_ucx_context = p_status->p_ucx_context;
  int i;
  nm_ucx_iov_alloc(&p_status->send, n);
  if(n > 1)
    {
      for(i = 0; i < n; i++)
        {
          p_status->send.payload[i].buffer = v[i].iov_base;
          p_status->send.payload[i].length = v[i].iov_len;
        }
      p_status->send.status = ucp_tag_send_nbr(p_status->ep, p_status->send.payload, n, UCP_DATATYPE_IOV,
                                               p_status->remote_tag,
                                               p_status->send.p_req + p_ucx_context->request_size);
    }
  else
    {
      assert(n == 1);
      p_status->send.status = ucp_tag_send_nbr(p_status->ep, v[0].iov_base, v[0].iov_len, ucp_dt_make_contig(1),
                                               p_status->remote_tag,
                                               p_status->send.p_req + p_ucx_context->request_size);
    }
  if((p_status->send.status != UCS_OK) && (p_status->send.status != UCS_INPROGRESS))
    {
      NM_WARN("error %d while posting send (%s)\n",
              p_status->send.status, ucs_status_string(p_status->send.status));
      return nm_ucx_convert_status(p_status->send.status);
    }
  else
    {
      return NM_ESUCCESS;
    }
}

static int nm_ucx_send_poll(void*_status)
{
  struct nm_ucx_s*p_status = _status;
  struct nm_ucx_context_s*p_ucx_context = p_status->p_ucx_context;

  /* Operation was completed immediately */
  if(p_status->send.status == UCS_OK)
  {
    nm_ucx_iov_free(&p_status->send);
    return NM_ESUCCESS;
  }
  ucp_worker_progress(p_ucx_context->ucp_worker);
  p_status->send.status = ucp_request_check_status(p_status->send.p_req + p_ucx_context->request_size);
  if(p_status->send.status == UCS_INPROGRESS)
    {
      return -NM_EAGAIN;
    }
  else
    {
      nm_ucx_iov_free(&p_status->send);
      if(p_status->send.status == UCS_OK)
        {
          return NM_ESUCCESS;
        }
      else
        {
          NM_WARN("error %d while sending (%s)\n",
                  p_status->send.status, ucs_status_string(p_status->send.status));
          return nm_ucx_convert_status(p_status->send.status);
        }
    }
}

static int nm_ucx_recv_iov_post(void*_status, struct iovec*v, int n)
{
  struct nm_ucx_s*p_status = _status;
  struct nm_ucx_context_s*p_ucx_context = p_status->p_ucx_context;
  nm_ucx_iov_alloc(&p_status->recv, n);
  if(n > 1)
    {
      int i;
      for(i = 0; i < n; i++)
        {
          p_status->recv.payload[i].buffer = v[i].iov_base;
          p_status->recv.payload[i].length = v[i].iov_len;
        }
      p_status->recv.status = ucp_tag_recv_nbr(p_ucx_context->ucp_worker,
                                               p_status->recv.payload, n, UCP_DATATYPE_IOV,
                                               p_status->local_tag, (ucp_tag_t)0xFFFFFFFF,
                                               p_status->recv.p_req + p_ucx_context->request_size);
    }
  else
    {
      p_status->recv.status = ucp_tag_recv_nbr(p_ucx_context->ucp_worker,
                                               v[0].iov_base, v[0].iov_len, ucp_dt_make_contig(1),
                                               p_status->local_tag, (ucp_tag_t)0xFFFFFFFF,
                                               p_status->recv.p_req + p_ucx_context->request_size);
    }
  if((p_status->recv.status != UCS_OK) && (p_status->recv.status != UCS_INPROGRESS))
    {
      NM_WARN("error %d while posting receive (%s)\n",
              p_status->recv.status, ucs_status_string(p_status->recv.status));
      return nm_ucx_convert_status(p_status->recv.status);
    }
  else
    {
      return NM_ESUCCESS;
    }
}

static int nm_ucx_recv_poll_one(void*_status)
{
  struct nm_ucx_s*p_status = _status;
  struct nm_ucx_context_s*p_ucx_context = p_status->p_ucx_context;
  p_status->recv.status = ucp_request_check_status(p_status->recv.p_req + p_ucx_context->request_size);
  if(p_status->recv.status == UCS_OK)
  {
#ifdef DEBUG
    ucp_tag_recv_info_t info;
    ucs_status_t status = ucp_tag_recv_request_test(p_status->recv.p_req + p_ucx_context->request_size, &info);
    assert(status == UCS_OK);
    assert(info.sender_tag == p_status->local_tag);
#endif /* DEBUG */
    nm_ucx_iov_free(&p_status->recv);
    return NM_ESUCCESS;
  }
  ucp_worker_progress(p_ucx_context->ucp_worker);
  p_status->recv.status = ucp_request_check_status(p_status->recv.p_req + p_ucx_context->request_size);
  if(p_status->recv.status == UCS_INPROGRESS)
    {
      return -NM_EAGAIN;
    }
  else
    {
      nm_ucx_iov_free(&p_status->recv);
      if(p_status->recv.status == UCS_OK)
        {
          return NM_ESUCCESS;
        }
      else
        {
          NM_WARN("error %d while receiving (%s)\n",
                  p_status->recv.status, ucs_status_string(p_status->recv.status));
          return nm_ucx_convert_status(p_status->recv.status);
        }
    }
}

static int nm_ucx_recv_probe_any(puk_context_t p_context, void**_status)
{
  struct nm_ucx_context_s*p_ucx_context = puk_context_get_status(p_context);
  ucp_worker_progress(p_ucx_context->ucp_worker);
  ucp_tag_recv_info_t info;
  ucp_tag_message_h msg = ucp_tag_probe_nb(p_ucx_context->ucp_worker, 0 /* tag */, 0 /* tag_mask */,
                                           0 /* remove */, &info);
  if(msg == NULL)
    {
      return -NM_EAGAIN;
    }
  else
    {
      const ucp_tag_t tag = info.sender_tag;
      if((tag < 0) || (tag >= nm_ucx_status_vect_size(&p_ucx_context->p_statuses)))
        return -NM_EUNKNOWN;
      struct nm_ucx_s*p_status = nm_ucx_status_vect_at(&p_ucx_context->p_statuses, tag);
      *_status = p_status;
      return NM_ESUCCESS;
    }
}

static int nm_ucx_recv_cancel(void*_status)
{
  struct nm_ucx_s*p_status = _status;
  struct nm_ucx_context_s*p_ucx_context = p_status->p_ucx_context;
  ucp_request_cancel(p_ucx_context->ucp_worker, p_status->recv.p_req);
  ucs_status_t status = UCS_PTR_STATUS(p_status->recv.p_req);
  if(status == UCS_OK || status == UCS_INPROGRESS)
    {
      /* receive is in progress, it is too late to cancel */
      //      return -NM_EINPROGRESS;
      return NM_ESUCCESS;
    }
  else if(status == UCS_ERR_CANCELED)
    {
      /* cancellation ok */
      return NM_ESUCCESS;
    }
  else
    {
      return nm_ucx_convert_status(status);
    }
}

/******************** Sub functions ********************/


static void nm_ucx_err_handler(void*arg, ucp_ep_h ep, ucs_status_t status)
{
  char*str = arg;
  NM_FATAL("[%s] error handling callback was invoked with status %d (%s)\n",
          str, status, ucs_status_string(status));
}

static int nm_ucx_convert_status(ucs_status_t status)
{
  switch(status)
  {
    case UCS_OK:
      return NM_ESUCCESS;

    case UCS_INPROGRESS:
      return -NM_EAGAIN;

    case UCS_ERR_CANCELED:
      return -NM_ECANCELED;

    case UCS_ERR_TIMED_OUT:
      return -NM_ETIMEDOUT;

    case UCS_ERR_NO_MEMORY:
      NM_WARN("message aborted. status: (%s)\n", ucs_status_string(status));
      return -NM_ENOMEM;

    default:  /* Any problem happened. */
      NM_WARN("message aborted. status: (%s)\n", ucs_status_string(status));
      return -NM_EUNKNOWN;
  }
}

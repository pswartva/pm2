/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief PadicoTM RT Task manager
 * @ingroup TaskManager
 */

#include <unistd.h>

#include <Padico/Puk.h>
#include <Padico/Module.h>

#include "PM2.h"
#include "TaskManager-internals.h"

PADICO_MODULE_HOOK(PadicoTM);



/* *** RTTASK MANAGER ************************************** */

void padico_tm_rttask_start(padico_thread_fun_t f, void*arg, const char*name __attribute__((unused)))
{
  marcel_t       background_thread;
  marcel_attr_t  attr;
  padico_trace("starting RT thread\n");
  marcel_attr_init(&attr);
  marcel_attr_setrealtime(&attr, MARCEL_CLASS_REALTIME);
  marcel_attr_setdetachstate(&attr, 1);
  marcel_create(&background_thread, &attr, f, arg);
  padico_tm_thread_givename_tid("PadicoTM_RT_Task", background_thread);
}

padico_tm_bgthread_pool_t padico_tm_bgthread_pool_create(const char*name)
{
  struct padico_tm_bgthread_pool_s*pool = padico_malloc(sizeof(struct padico_tm_bgthread_pool_s));
  pool->name = padico_strdup(name);
  marcel_mutex_init(&pool->lock, NULL);
  padico_tm_bgthread_list_init(&pool->running);
  padico_tm_bgthread_list_init(&pool->done);
  return pool;
}

/** garbage collect threads: join threads that are done. */
static inline void padico_tm_bgthread_pool_collect(padico_tm_bgthread_pool_t pool)
{
  while(!padico_tm_bgthread_list_empty(&pool->done))
    {
      padico_tm_bgthread_t bgthread = padico_tm_bgthread_list_pop_front(&pool->done);
      marcel_join(bgthread->tid, NULL);
      padico_free(bgthread->name);
      padico_tm_bgthread_delete(bgthread);
    }
}

void padico_tm_bgthread_pool_wait(padico_tm_bgthread_pool_t pool)
{
  padico_out(20, "finalizing bgthread pool *%s*.\n", pool->name);
  int empty = 0;
  while(!empty)
    {
      marcel_mutex_lock(&pool->lock);
      empty = padico_tm_bgthread_list_empty(&pool->running);
      if(!empty)
        {
          padico_warning("finalizing bgthread pool *%s*: waiting %d threads.\n",
                         pool->name, padico_tm_bgthread_list_size(&pool->running));
        }
      marcel_mutex_unlock(&pool->lock);
      if(!empty)
        {
          puk_usleep(500);
        }
      /* TODO- timeout */
    }
  marcel_mutex_lock(&pool->lock);
  padico_tm_bgthread_pool_collect(pool);
  marcel_mutex_unlock(&pool->lock);
  padico_free((char*)pool->name);
  padico_free(pool);
}

static void*padico_tm_bgthread_stub(void*_arg)
{
  struct padico_tm_bgthread_s*bgthread = _arg;
  padico_tm_thread_givename_tid(bgthread->name, marcel_self());
  void*res = (bgthread->f)(bgthread->arg);
  if(bgthread->pool)
    {
      marcel_mutex_lock(&bgthread->pool->lock);
      padico_tm_bgthread_list_remove(&bgthread->pool->running, bgthread);
      padico_tm_bgthread_list_push_back(&bgthread->pool->done, bgthread);
      marcel_mutex_unlock(&bgthread->pool->lock);
    }
  else
    {
      marcel_detach(marcel_self());
      padico_free(bgthread->name);
      padico_tm_bgthread_delete(bgthread);
    }
  marcel_exit(res);
}

void padico_tm_bgthread_start(padico_tm_bgthread_pool_t pool, padico_thread_fun_t f, void*arg, const char*name)
{
  struct padico_tm_bgthread_s*bgthread = padico_tm_bgthread_new();
  bgthread->f = f;
  bgthread->arg = arg;
  bgthread->name = padico_strdup(name);
  bgthread->pool = pool;
  if(bgthread->pool)
    {
      marcel_mutex_lock(&pool->lock);
      padico_tm_bgthread_pool_collect(pool);
      padico_tm_bgthread_list_push_back(&pool->running, bgthread);
      marcel_mutex_unlock(&pool->lock);
    }
  marcel_create(&bgthread->tid, NULL, &padico_tm_bgthread_stub, bgthread);
}

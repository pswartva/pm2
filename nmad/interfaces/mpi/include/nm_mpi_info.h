/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#ifndef NM_MPI_INFO_H
#define NM_MPI_INFO_H

/**
 * @ingroup mpi_interface
 * @file
 * declarations for MPI info objects
 * @{
 */

/** @name Functions: the Info object
 * @{ */

/** Creates a new info object
 */
int MPI_Info_create(MPI_Info*info);

/** Frees an info object
 */
int MPI_Info_free(MPI_Info*info);

/** Return a duplicate of an info object.
 */
int MPI_Info_dup(MPI_Info info, MPI_Info*newinfo);

/** Adds a <key, value> pair to info object
 */
int MPI_Info_set(MPI_Info info,
                 const char*key,
                 const char*value);

/** Retrieves the value associated with a key
 */
int MPI_Info_get(MPI_Info info, const char*key, int valuelen, char*value, int*flag);

int MPI_Info_get_string(MPI_Info info, const char*key, int*buflen, char*value, int*flag);

/** Deletes a <key, value> pair from info object
 */
int MPI_Info_delete(MPI_Info info, const char*key);

/** Returns the number of currently defined keys in info
 */
int MPI_Info_get_nkeys(MPI_Info info,
                       int*nkeys);

/** Returns the nth key in info
 */
int MPI_Info_get_nthkey(MPI_Info info, int n, char*key);

/** Retrieves the length of the value associated with a key
 */
int MPI_Info_get_valuelen(MPI_Info info, const char*key, int*valuelen, int*flag);

int MPI_Info_create_env(int argc, char argv[], MPI_Info *info);

/** @} */
/** @} */


#endif /* NM_MPI_INFO_H */

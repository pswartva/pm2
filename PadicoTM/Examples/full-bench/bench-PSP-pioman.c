/* bench-PSP-pioman.c
 */

#include "bench-common.h"
#include <Padico/PSP.h>
#include <Padico/NetSelector.h>
#include <Padico/PadicoTM.h>
#include <Padico/Module.h>

#include <pioman.h>

int bench_psp_init(void);
int bench_psp_run(int argc, char**argv);

PADICO_MODULE_DECLARE(bench_PSP_pioman, bench_psp_init, bench_psp_run, NULL);

static char*volatile   recv_buffer = NULL; /**< buffer for receipt */
static volatile size_t recv_lenbuf = 0; /**< len of above buffer */
static size_t len_offset = 0;

static padico_topo_node_t peer;
static struct puk_receptacle_PadicoSimplePackets_s psp;

static piom_cond_t piom_cond;


/* init a new benchmark */
static void bench_psp_post_recv(char*buffer, size_t lenbuf)
{
  padico_out(40, "post recv- len=%d\n", lenbuf);
  recv_lenbuf = lenbuf;
  recv_buffer = buffer;
  piom_cond_mask(&piom_cond, 1);
}

static void bench_psp_wait(void)
{
  piom_cond_wait(&piom_cond, 1);
  while(recv_buffer != NULL)
    {
      padico_out(20, "waiting synchro...\n");
    }
  recv_lenbuf = 0;
  padico_out(60, "done..\n");
}

static void bench_psp_send(void*buffer, size_t lenbuf)
{
  void*_header;
  padico_psp_connection_t conn = (*psp.driver->new_message)(psp._status, peer, &_header);
  uint32_t*header = (uint32_t*)(_header);
  header[0] = htonl(lenbuf);
  size_t blen = 0, hlen = 0;
  if(lenbuf > len_offset)
    {
      hlen = len_offset;
      blen = lenbuf - len_offset;
    }
  else
    {
      hlen = lenbuf;
      blen = 0;
    }
  memcpy(header + 2, buffer, hlen);
  if(blen > 0)
    {
      (*psp.driver->pack)(psp._status, conn, buffer + len_offset, blen);
    }
  (*psp.driver->end_message)(psp._status, conn);
}

static void bench_psp_handler(const void*_header,
                              padico_topo_node_t from,
                              void*key,
                              padico_psp_pump_t pump,
                              void*token)
{
  const uint32_t*header = _header;
  const uint32_t len = ntohl(header[0]);

  while(recv_buffer == NULL)
    {
      padico_out(10, "Ooops! Waiting in handler- recv_lenbuf=%d; len=%d;\n",
                 recv_lenbuf, len);
    }
  assert(len == recv_lenbuf);
  size_t blen = 0, hlen = 0;
  if(len > len_offset)
    {
      hlen = len_offset;
      blen = len - len_offset;
    }
  else
    {
      hlen = len;
      blen = 0;
    }
  memcpy(recv_buffer, header + 2, hlen);
  if(blen > 0)
    {
      (*pump)(token, recv_buffer + len_offset, blen);
    }
  __sync_synchronize();
  recv_buffer = NULL;
  piom_cond_signal(&piom_cond, 1);
}

int bench_psp_init(void)
{
  peer = bench_common_param.is_server?
    bench_common_param.client_node:
    bench_common_param.server_node;
  const char*component_s = padico_getenv("BENCH_PSP_ADAPTER");
  puk_component_t component = component_s?puk_component_resolve(component_s) : padico_ns_serial_selector(peer, "default", puk_iface_PadicoSimplePackets());
  puk_instance_t instance = puk_component_instantiate(component);
  puk_instance_indirect_PadicoSimplePackets(instance, NULL, &psp);

  padico_string_t c = puk_component_serialize(component);
  padico_print("bench-PSP: using component %s\n", padico_string_get(c));
  padico_string_delete(c);

  piom_cond_init(&piom_cond, 0);

  size_t h_len = 2*sizeof(uint32_t);
  (*psp.driver->init)(psp._status, 2, "bench-PSP", &h_len, &bench_psp_handler, NULL);
  len_offset = h_len - 8;
  assert(len_offset < h_len);
  padico_print("bench-PSP: header = %d; offset = %d\n", h_len, len_offset);
  if(psp.driver->connect)
    (*psp.driver->connect)(psp._status, peer);

  padico_tm_msleep(500);
  return 0;
}

static void psp_server(char*buffer, size_t lenbuf)
{
  padico_out(20, "starting- len=%d...\n", lenbuf);
  bench_psp_post_recv(buffer, lenbuf);
  bench_psp_wait();
  bench_psp_send(buffer, lenbuf);
  padico_out(20, "done- len=%d...\n", lenbuf);
}

static void psp_client(char*buffer, size_t lenbuf)
{
  padico_out(20, "starting- len=%d...\n", lenbuf);
  bench_psp_post_recv(buffer, lenbuf);
  bench_psp_send(buffer, lenbuf);
  bench_psp_wait();
  padico_out(20, "done- len=%d...\n", lenbuf);
}

static const struct bench_desc_s psp_bench =
{
  "PSP",
  &psp_server,
  &psp_client
};

int bench_psp_run(int argc, char**argv)
{
  the_bench(&psp_bench);
  return 0;
}

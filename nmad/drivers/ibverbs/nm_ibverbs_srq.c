/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include "nm_ibverbs.h"
#include <errno.h>

#include <Padico/Module.h>
#include <nm_private.h>


/* *** method: 'srq' *************************************** */

#define NM_IBVERBS_SRQ_NBUFS_MIN 64
#define NM_IBVERBS_SRQ_NBUFS_MAX 1024
#define NM_IBVERBS_SRQ_BLOCKSIZE (64 * 1024)
#define NM_IBVERBS_SRQ_BUFSIZE   ( NM_IBVERBS_SRQ_BLOCKSIZE - sizeof(struct nm_ibverbs_srq_header_s) )

/** header for each packet */
struct nm_ibverbs_srq_header_s
{
  uint32_t csum;
  uint64_t len;
} __attribute__((packed));

/** An "on the wire" packet for 'srq' minidriver */
struct nm_ibverbs_srq_packet_s
{
  struct nm_ibverbs_srq_header_s header;
  char data[NM_IBVERBS_SRQ_BUFSIZE];
} __attribute__((packed));

/** Connection state for srq tracks
 */
struct nm_ibverbs_srq_s
{
  struct
  {
    const struct nm_ibverbs_regpool_entry_s*p_sbuf_entry;
    struct nm_ibverbs_srq_packet_s*p_sbuf;
    nm_len_t chunk_len;              /**< length of chunk to send */
    int posted;
  } send;
  struct nm_ibverbs_cnx_s*p_cnx;
  struct nm_ibverbs_srq_context_s*p_ibverbs_srq_context;
  struct
  {
    void*p_remote_url;
    size_t url_size;
  } lazy; /**< store remote url for later use in case of lazy connection */
};

PUK_HASHTABLE_TYPE(nm_ibverbs_srq_qpn, const uint32_t*, struct nm_ibverbs_srq_s*,
                    &nm_ibverbs_qpn_hash, &nm_ibverbs_qpn_eq, NULL);

PUK_HASHTABLE_TYPE(nm_ibverbs_srq_addr, const struct nm_ibverbs_addr_s*, struct nm_ibverbs_srq_s*,
                   &nm_ibverbs_addr_hash, &nm_ibverbs_addr_eq, NULL);

/** context for ibverbs srq */
struct nm_ibverbs_srq_context_s
{
#ifdef NMAD_PROFILE
  unsigned long max_nbufs;
#endif /* NMAD_PROFILE */
  struct
  {
    const struct nm_ibverbs_regpool_entry_s*p_ready_entry; /**< pending buffer, ready for next recv_probe_any */
    nm_len_t chunk_len;                    /**< length of chunk received */
    struct nm_ibverbs_srq_s*p_ibverbs_srq; /**< status of the pending receive */
  } recv;
  struct nm_ibverbs_srq_qpn_hashtable_s statuses_by_qpn; /**< statuses hashed by QPN */
  struct nm_ibverbs_srq_addr_hashtable_s statuses_by_addr; /**< table of statuses hashed by remote address */
  int nbufs;                               /**< dynamic number of receive buffers */
  struct nm_ibverbs_regpool_s buf_regpool; /**< pool of buffers for sending */
  struct nm_ibverbs_context_s*p_ibverbs_context;
  int running;                             /**< whether the driver is running */
  int vector_connect;                      /**< whether we use vectored connection establishment */
  int lazy_connect;                        /**< whether we used lazy connection establishment */
};


static void nm_ibverbs_srq_getprops(puk_context_t context, struct nm_minidriver_properties_s*p_props);
static void nm_ibverbs_srq_init(puk_context_t context, const void**p_url, size_t*p_url_size);
static void nm_ibverbs_srq_close(puk_context_t context);
static void nm_ibverbs_srq_connect(void*_status, const void*p_remote_url, size_t url_size);
static void nm_ibverbs_srq_connect_async(void*_status, const void*p_remote_url, size_t url_size);
static void nm_ibverbs_srq_connect_wait(void*_status);
static int  nm_ibverbs_srq_send_buf_get(void*_status, void**p_buffer, nm_len_t*p_len);
static int  nm_ibverbs_srq_send_buf_post(void*_status, nm_len_t len);
static int  nm_ibverbs_srq_send_poll(void*_status);
static int  nm_ibverbs_srq_recv_probe_any(puk_context_t p_context, void**_status);
static int  nm_ibverbs_srq_recv_wait_any(puk_context_t p_context, void**_status);
static int  nm_ibverbs_srq_recv_buf_poll(void*_status, void**p_buffer, nm_len_t*p_len);
static int  nm_ibverbs_srq_recv_buf_release(void*_status);
static int  nm_ibverbs_srq_recv_cancel_any(puk_context_t p_context);

static const struct nm_minidriver_iface_s nm_ibverbs_srq_minidriver =
  {
    .getprops         = &nm_ibverbs_srq_getprops,
    .init             = &nm_ibverbs_srq_init,
    .close            = &nm_ibverbs_srq_close,
    .connect          = &nm_ibverbs_srq_connect,
    .connect_async    = &nm_ibverbs_srq_connect_async,
    .connect_wait     = &nm_ibverbs_srq_connect_wait,
    .send_iov_post    = NULL,
    .send_data_post   = NULL,
    .send_poll        = &nm_ibverbs_srq_send_poll,
    .send_buf_get     = &nm_ibverbs_srq_send_buf_get,
    .send_buf_post    = &nm_ibverbs_srq_send_buf_post,
    .recv_iov_post    = NULL,
    .recv_data_post   = NULL,
    .recv_poll_one    = NULL,
    .recv_probe_any   = &nm_ibverbs_srq_recv_probe_any,
    .recv_wait_any    = &nm_ibverbs_srq_recv_wait_any,
    .recv_buf_poll    = &nm_ibverbs_srq_recv_buf_poll,
    .recv_buf_release = &nm_ibverbs_srq_recv_buf_release,
    .recv_cancel      = NULL,
    .recv_cancel_any  = &nm_ibverbs_srq_recv_cancel_any
  };

static void*nm_ibverbs_srq_instantiate(puk_instance_t instance, puk_context_t context);
static void nm_ibverbs_srq_destroy(void*);

static const struct puk_component_driver_s nm_ibverbs_srq_component =
  {
    .instantiate = &nm_ibverbs_srq_instantiate,
    .destroy = &nm_ibverbs_srq_destroy
  };

PADICO_MODULE_COMPONENT(NewMad_ibverbs_srq,
  puk_component_declare("NewMad_ibverbs_srq",
                        puk_component_provides("PadicoComponent", "component", &nm_ibverbs_srq_component),
                        puk_component_provides("NewMad_minidriver", "minidriver", &nm_ibverbs_srq_minidriver),
                        puk_component_uses("NewMad_ibverbs", "ibverbs"),
                        puk_component_attr("comp_channel", NULL) ));

/* ********************************************************* */


static inline void nm_ibverbs_srq_refill(struct nm_ibverbs_srq_context_s*p_ibverbs_srq_context);
static inline void nm_ibverbs_srq_post_entry(struct nm_ibverbs_srq_context_s*p_ibverbs_srq_context,
                                             const struct nm_ibverbs_regpool_entry_s*p_rbuf_entry);

static inline int nm_ibverbs_srq_ready(struct nm_ibverbs_srq_s*p_ibverbs_srq)
{
  return (!p_ibverbs_srq->p_ibverbs_srq_context->lazy_connect) || nm_ibverbs_cnx_lazy_ready(p_ibverbs_srq->p_cnx);
}

static void* nm_ibverbs_srq_instantiate(puk_instance_t instance, puk_context_t context)
{
  struct nm_ibverbs_srq_context_s*p_ibverbs_srq_context = puk_context_get_status(context);
 /* check parameters consistency */
  assert(sizeof(struct nm_ibverbs_srq_packet_s) % 1024 == 0);
  /* init */
  struct nm_ibverbs_srq_s*p_ibverbs_srq = NULL;
  if(nm_ibverbs_memalign > 0)
    {
      posix_memalign((void**)&p_ibverbs_srq, nm_ibverbs_memalign, sizeof(struct nm_ibverbs_srq_s));
    }
  else
    {
      p_ibverbs_srq = padico_malloc(sizeof(struct nm_ibverbs_srq_s));
    }
  p_ibverbs_srq->send.p_sbuf = NULL;
  p_ibverbs_srq->send.p_sbuf_entry = NULL;
  p_ibverbs_srq->send.chunk_len  = NM_LEN_UNDEFINED;
  p_ibverbs_srq->p_cnx           = NULL;
  p_ibverbs_srq->p_ibverbs_srq_context = p_ibverbs_srq_context;
  return p_ibverbs_srq;
}

static void nm_ibverbs_srq_destroy(void*_status)
{
  struct nm_ibverbs_srq_s*p_ibverbs_srq = _status;
  struct nm_ibverbs_srq_context_s*p_ibverbs_srq_context = p_ibverbs_srq->p_ibverbs_srq_context;
  if(p_ibverbs_srq->p_cnx)
    {
      nm_ibverbs_srq_qpn_hashtable_remove(&p_ibverbs_srq_context->statuses_by_qpn, &p_ibverbs_srq->p_cnx->local_addr.addr.qpn);
      if(p_ibverbs_srq_context->lazy_connect)
        {
          nm_ibverbs_srq_addr_hashtable_remove(&p_ibverbs_srq_context->statuses_by_addr, p_ibverbs_srq->lazy.p_remote_url);
          padico_free(p_ibverbs_srq->lazy.p_remote_url);
        }
      nm_ibverbs_cnx_close(p_ibverbs_srq->p_cnx);
    }
  padico_free(p_ibverbs_srq);
}


static void nm_ibverbs_srq_getprops(puk_context_t p_context, struct nm_minidriver_properties_s*p_props)
{
  int use_comp_channel = 0;
  assert(p_context != NULL);
  struct nm_ibverbs_srq_context_s*p_ibverbs_srq_context = padico_malloc(sizeof(struct nm_ibverbs_srq_context_s));
  puk_context_set_status(p_context, p_ibverbs_srq_context);

  const char*s_comp_channel = puk_context_getattr(p_context, "comp_channel");
  if(s_comp_channel)
    {
      use_comp_channel = puk_opt_parse_bool(s_comp_channel);
      NM_DISPF("comp_channel = %d; forced by user.\n", use_comp_channel);
    }
  if(puk_opt_parse_bool(getenv("NMAD_IBVERBS_COMP_CHANNEL")))
    {
      puk_context_putattr(p_context, "ibv_comp_channel", "1");
      use_comp_channel = 1;
      NM_DISPF("comp channel forced by environment.\n");
    }
  /* we need srq */
  struct nm_ibverbs_opts_s ib_opts;
  nm_ibverbs_context_default_opts(&ib_opts); /* get default options, then add srq & comp_channel */
  ib_opts.use_srq = 1;
  ib_opts.use_comp_channel = use_comp_channel;
  p_ibverbs_srq_context->p_ibverbs_context = nm_ibverbs_context_resolve_opts(p_context, &ib_opts);
  nm_ibverbs_context_get_profile(p_ibverbs_srq_context->p_ibverbs_context, &p_props->profile);
  p_props->capabilities.supports_data     = 0;
  p_props->capabilities.supports_iovec    = 0;
  p_props->capabilities.supports_buf_send = 1;
  p_props->capabilities.supports_buf_recv = 1;
  p_props->capabilities.supports_wait_any = 1;
  p_props->capabilities.prefers_wait_any  = p_ibverbs_srq_context->p_ibverbs_context->ib_opts.use_comp_channel;
  p_props->capabilities.supports_recv_any = 1;
  p_props->capabilities.max_msg_size      = NM_IBVERBS_SRQ_BUFSIZE;
  p_props->nickname = "ibverbs_srq";
}

static void nm_ibverbs_srq_init(puk_context_t context, const void**p_url, size_t*p_url_size)
{
  struct nm_ibverbs_srq_context_s*p_ibverbs_srq_context = puk_context_get_status(context);
  if(!p_ibverbs_srq_context->p_ibverbs_context->ib_opts.use_srq)
    {
      NM_FATAL("board does not provide SRQ. Cannot use srq driver.\n");
    }
  nm_ibverbs_srq_qpn_hashtable_init(&p_ibverbs_srq_context->statuses_by_qpn);
  nm_ibverbs_srq_addr_hashtable_init(&p_ibverbs_srq_context->statuses_by_addr);
  p_ibverbs_srq_context->recv.chunk_len  = NM_LEN_UNDEFINED;
  p_ibverbs_srq_context->recv.p_ready_entry = NULL;
  p_ibverbs_srq_context->recv.p_ibverbs_srq = NULL;
  p_ibverbs_srq_context->running = 1;
  nm_ibverbs_regpool_init(&p_ibverbs_srq_context->buf_regpool, p_ibverbs_srq_context->p_ibverbs_context,
                          sizeof(struct nm_ibverbs_srq_packet_s), NM_IBVERBS_SRQ_NBUFS_MIN);

  /* ** check init method */
  p_ibverbs_srq_context->vector_connect = 0;
  p_ibverbs_srq_context->lazy_connect = 0;
  /* detect availability of vectored connection */
  const char*s_session_size = puk_context_getattr(context, "session_size");
  const char*s_rank = puk_context_getattr(context, "rank");
  const char*s_wide_url_support = puk_context_getattr(context, "wide_url_support");
  if((s_session_size != NULL) && (s_rank != NULL) && (s_wide_url_support != NULL))
    {
      const int wide_url_support = puk_opt_parse_bool(s_wide_url_support);
      if(wide_url_support)
        {
          p_ibverbs_srq_context->vector_connect = 1;
        }
    }
   /* check whether lazy connect was requested */
  if(nm_ibverbs_cnx_lazy_enabled())
    {
      p_ibverbs_srq_context->vector_connect = 0;
      NM_WARN("using lazy connect.\n");
      p_ibverbs_srq_context->lazy_connect = 1;
      p_ibverbs_srq_context->p_ibverbs_context->ib_opts.lazy_connect = 1;
    }
  /* do the init */
  if(p_ibverbs_srq_context->vector_connect)
    {
      const int session_size = atoi(s_session_size);
      const int rank = atoi(s_rank);
      NM_DISPF("using vector connect; session_size = %d; rank = %d\n", session_size, rank);
      nm_ibverbs_context_get_wide_url(p_ibverbs_srq_context->p_ibverbs_context, session_size, rank, p_url, p_url_size);
    }
  else
    {
      nm_ibverbs_context_get_url(p_ibverbs_srq_context->p_ibverbs_context, p_url, p_url_size);
    }
  /* post recv requests */
  p_ibverbs_srq_context->nbufs = NM_IBVERBS_SRQ_NBUFS_MIN;
  int i;
  for(i = 0; i < p_ibverbs_srq_context->nbufs; i++)
    {
      const struct nm_ibverbs_regpool_entry_s*p_rbuf_entry = nm_ibverbs_regpool_alloc_entry(&p_ibverbs_srq_context->buf_regpool);
      nm_ibverbs_srq_post_entry(p_ibverbs_srq_context, p_rbuf_entry);
    }
  nm_ibverbs_context_set_srq_limit(p_ibverbs_srq_context->p_ibverbs_context, p_ibverbs_srq_context->nbufs / 2);
#ifdef NMAD_PROFILE
  puk_profile_var_defx(unsigned_long, highwatermark, &p_ibverbs_srq_context->max_nbufs, p_ibverbs_srq_context->nbufs,
                       "nm_ibverbs_srq", padico_string_get(s), "maximum number of pre-posted srq requests",
                       "nm_ibverbs_srq.context-%p.max_nbufs", p_ibverbs_srq_context);
#endif
}

static void nm_ibverbs_srq_close(puk_context_t context)
{
  struct nm_ibverbs_srq_context_s*p_ibverbs_srq_context = puk_context_get_status(context);
  nm_ibverbs_srq_qpn_hashtable_destroy(&p_ibverbs_srq_context->statuses_by_qpn);
  if(p_ibverbs_srq_context->lazy_connect)
    {
      nm_ibverbs_srq_addr_hashtable_destroy(&p_ibverbs_srq_context->statuses_by_addr);
    }
  nm_ibverbs_regpool_destroy(&p_ibverbs_srq_context->buf_regpool);
  nm_ibverbs_context_delete(p_ibverbs_srq_context->p_ibverbs_context);
  puk_context_set_status(context, NULL);
  padico_free(p_ibverbs_srq_context);
}


static void nm_ibverbs_srq_connect(void*_status, const void*p_remote_url, size_t url_size)
{
  nm_ibverbs_srq_connect_async(_status, p_remote_url, url_size);
  nm_ibverbs_srq_connect_wait(_status);
}

static void nm_ibverbs_srq_connect_async(void*_status, const void*p_remote_url, size_t url_size)
{
  struct nm_ibverbs_srq_s*p_ibverbs_srq = _status;
  struct nm_ibverbs_srq_context_s*p_ibverbs_srq_context = p_ibverbs_srq->p_ibverbs_srq_context;
  if(p_ibverbs_srq_context->lazy_connect)
    {
      p_ibverbs_srq->p_cnx = NULL;
      p_ibverbs_srq->lazy.url_size = url_size;
      p_ibverbs_srq->lazy.p_remote_url = padico_malloc(url_size);
      memcpy(p_ibverbs_srq->lazy.p_remote_url, p_remote_url, url_size);
    }
  else if(p_ibverbs_srq_context->vector_connect)
    {
      p_ibverbs_srq->p_cnx = nm_ibverbs_cnx_vector_connect(p_ibverbs_srq_context->p_ibverbs_context, p_remote_url, url_size);
    }
  else
    {
      p_ibverbs_srq->p_cnx = nm_ibverbs_cnx_create(p_ibverbs_srq_context->p_ibverbs_context);
      nm_ibverbs_cnx_connect_async(p_ibverbs_srq->p_cnx, p_remote_url, url_size);
    }
}

static void nm_ibverbs_srq_connect_wait(void*_status)
{
  struct nm_ibverbs_srq_s*p_ibverbs_srq = _status;
  struct nm_ibverbs_srq_context_s*p_ibverbs_srq_context = p_ibverbs_srq->p_ibverbs_srq_context;
  if(p_ibverbs_srq_context->lazy_connect)
    {
      nm_ibverbs_srq_addr_hashtable_insert(&p_ibverbs_srq_context->statuses_by_addr,
                                           p_ibverbs_srq->lazy.p_remote_url, p_ibverbs_srq);
    }
  else
    {
      if(!p_ibverbs_srq_context->vector_connect)
        {
          /* wait for addresses only in case of non-vectored connection */
          nm_ibverbs_cnx_connect_wait(p_ibverbs_srq->p_cnx);
        }
      nm_ibverbs_srq_qpn_hashtable_insert(&p_ibverbs_srq_context->statuses_by_qpn,
                                          &p_ibverbs_srq->p_cnx->local_addr.addr.qpn, p_ibverbs_srq);
    }
}


/* ** srq I/O ******************************************* */

static int nm_ibverbs_srq_send_buf_get(void*_status, void**p_buffer, nm_len_t*p_len)
{
  struct nm_ibverbs_srq_s*__restrict__ p_ibverbs_srq = _status;
  assert(p_ibverbs_srq->send.p_sbuf_entry == NULL);
  p_ibverbs_srq->send.p_sbuf_entry = nm_ibverbs_regpool_alloc_entry(&p_ibverbs_srq->p_ibverbs_srq_context->buf_regpool);
  p_ibverbs_srq->send.p_sbuf = p_ibverbs_srq->send.p_sbuf_entry->p_ptr;
  assert(p_ibverbs_srq->send.chunk_len == NM_LEN_UNDEFINED);
  *p_buffer = p_ibverbs_srq->send.p_sbuf->data;
  *p_len = NM_IBVERBS_SRQ_BUFSIZE;
  if(p_ibverbs_srq->p_cnx == NULL)
    {
      nm_ibverbs_cnx_lazy_create(p_ibverbs_srq->p_ibverbs_srq_context->p_ibverbs_context, &p_ibverbs_srq->p_cnx,
                                 p_ibverbs_srq->lazy.p_remote_url, p_ibverbs_srq->lazy.url_size);
    }
  return NM_ESUCCESS;
}

static inline int nm_ibverbs_srq_send_do_post(struct nm_ibverbs_srq_s*p_ibverbs_srq)
{
  assert(!p_ibverbs_srq->send.posted);
  struct ibv_sge list =
    {
      .addr   = (uintptr_t)p_ibverbs_srq->send.p_sbuf,
      .length = p_ibverbs_srq->send.chunk_len + sizeof(struct nm_ibverbs_srq_header_s),
      .lkey   = p_ibverbs_srq->send.p_sbuf_entry->p_mr->lkey
    };
  int do_inline = (p_ibverbs_srq->send.chunk_len + sizeof(struct nm_ibverbs_srq_header_s) < p_ibverbs_srq->p_cnx->max_inline);
  struct ibv_send_wr wr;
 retry:
  wr = (struct ibv_send_wr)
    {
      .wr_id      = NM_IBVERBS_WRID_SEND,
      .sg_list    = &list,
      .num_sge    = 1,
      .opcode     = IBV_WR_SEND,
      .send_flags = (do_inline ?
                     (IBV_SEND_SIGNALED | IBV_SEND_INLINE ) : IBV_SEND_SIGNALED),
      .next       = NULL
    };
  struct ibv_send_wr*bad_wr = NULL;
  int rc = ibv_post_send(p_ibverbs_srq->p_cnx->qp, &wr, &bad_wr);
  if(rc)
    {
      if(do_inline && (rc == ENOMEM))
        {
          NM_WARN("post send failed len = %d; rc = %d (%s). Retrying.\n", (int)p_ibverbs_srq->send.chunk_len, rc, strerror(rc));
          do_inline = 0;
          goto retry;
        }
      else
        {
          NM_WARN("post send failed len = %d; rc = %d (%s).\n", (int)p_ibverbs_srq->send.chunk_len, rc, strerror(rc));
          return -NM_EBROKEN;
        }
    }
  p_ibverbs_srq->send.posted = 1;
  return NM_ESUCCESS;
}

static int nm_ibverbs_srq_send_buf_post(void*_status, nm_len_t len)
{
  struct nm_ibverbs_srq_s*__restrict__ p_ibverbs_srq = _status;
  p_ibverbs_srq->send.chunk_len = len;
  assert(len <= NM_IBVERBS_SRQ_BUFSIZE);
  const uint32_t csum = nm_ibverbs_checksum(&p_ibverbs_srq->send.p_sbuf->data[0], len);
  p_ibverbs_srq->send.p_sbuf->header.csum = csum;
  p_ibverbs_srq->send.p_sbuf->header.len = len;
  p_ibverbs_srq->send.posted = 0;
  if(nm_ibverbs_srq_ready(p_ibverbs_srq))
    {
      return nm_ibverbs_srq_send_do_post(p_ibverbs_srq);
    }
  else
    {
      nm_ibverbs_cnx_lazy_progress(p_ibverbs_srq->p_ibverbs_srq_context->p_ibverbs_context);
      return NM_ESUCCESS;
    }
}

static int nm_ibverbs_srq_send_poll(void*_status)
{
  struct nm_ibverbs_srq_s*__restrict__ p_ibverbs_srq = _status;
  assert(p_ibverbs_srq->send.chunk_len != NM_LEN_UNDEFINED);

  if(!nm_ibverbs_srq_ready(p_ibverbs_srq))
    {
      nm_ibverbs_cnx_lazy_progress(p_ibverbs_srq->p_ibverbs_srq_context->p_ibverbs_context);
      return -NM_EAGAIN;
    }

  if(!p_ibverbs_srq->send.posted)
    {
      int rc = nm_ibverbs_srq_send_do_post(p_ibverbs_srq);
      if(rc != NM_ESUCCESS)
        return rc;
    }

  struct ibv_wc wc;
  int ne = ibv_poll_cq(p_ibverbs_srq->p_cnx->of_cq, 1, &wc);
  if(ne < 0)
    {
      NM_WARN("poll out CQ failed rc = %d (%s).\n", ne, strerror(-ne));
      return -NM_EBROKEN;
    }
  if(ne > 0)
    {
      nm_ibverbs_regpool_free_entry(&p_ibverbs_srq->p_ibverbs_srq_context->buf_regpool, p_ibverbs_srq->send.p_sbuf_entry);
      p_ibverbs_srq->send.chunk_len = NM_LEN_UNDEFINED;
      p_ibverbs_srq->send.p_sbuf = NULL;
      p_ibverbs_srq->send.p_sbuf_entry = NULL;
      return NM_ESUCCESS;
    }
  else
    {
      return -NM_EAGAIN;
    }
}

static inline void nm_ibverbs_srq_post_entry(struct nm_ibverbs_srq_context_s*p_ibverbs_srq_context,
                                             const struct nm_ibverbs_regpool_entry_s*p_rbuf_entry)
{
  struct nm_ibverbs_srq_packet_s*p_rbuf = p_rbuf_entry->p_ptr;
  p_rbuf->header.len = NM_LEN_UNDEFINED;
  struct ibv_sge list =
    {
      .addr   = (uintptr_t)p_rbuf,
      .length = NM_IBVERBS_SRQ_BLOCKSIZE,
      .lkey   = p_rbuf_entry->p_mr->lkey
    };
  struct ibv_recv_wr wr =
    {
      .wr_id   = (uintptr_t)p_rbuf_entry,
      .sg_list = &list,
      .num_sge = 1,
    };
  struct ibv_recv_wr*bad_wr = NULL;
  int rc = ibv_post_srq_recv(p_ibverbs_srq_context->p_ibverbs_context->srq.p_srq, &wr, &bad_wr);
  if(rc)
    {
      NM_FATAL("ibv_post_srq_recv failed rc = %d (%s).\n", rc, strerror(rc));
    }
}

/** check whether we need to increase the number of pre-posted requests */
static inline void nm_ibverbs_srq_refill(struct nm_ibverbs_srq_context_s*p_ibverbs_srq_context)
{
  int i;
  if(p_ibverbs_srq_context->p_ibverbs_context->hca.async_events.srq_limit_reached)
    {
      if(p_ibverbs_srq_context->nbufs < NM_IBVERBS_SRQ_NBUFS_MAX)
        {
          int old_nbufs = p_ibverbs_srq_context->nbufs;
          p_ibverbs_srq_context->nbufs *= 2;
#ifdef NMAD_PROFILE
          p_ibverbs_srq_context->max_nbufs = p_ibverbs_srq_context->nbufs;
#endif
          for(i = old_nbufs ; i < p_ibverbs_srq_context->nbufs; i++)
            {
              const struct nm_ibverbs_regpool_entry_s*p_rbuf_entry = nm_ibverbs_regpool_alloc_entry(&p_ibverbs_srq_context->buf_regpool);
              nm_ibverbs_srq_post_entry(p_ibverbs_srq_context, p_rbuf_entry);
            }
          /* set limit _after_ posting requests, so as not to trigger an immediate srq_limit event */
          nm_ibverbs_context_set_srq_limit(p_ibverbs_srq_context->p_ibverbs_context, 1 + p_ibverbs_srq_context->nbufs / 4);
          padico_out(puk_verbose_notice, "SRQ increase requests number = %d.\n", p_ibverbs_srq_context->nbufs);
        }
      else
        {
          NM_WARN("SRQ limit reached but nbufs is already at its maximum = %d.\n", p_ibverbs_srq_context->nbufs);
        }
      __sync_fetch_and_sub(&p_ibverbs_srq_context->p_ibverbs_context->hca.async_events.srq_limit_reached, 1);
    }
}

static inline void nm_ibverbs_srq_recv_progress(struct nm_ibverbs_srq_context_s*p_ibverbs_srq_context)
{
  assert(p_ibverbs_srq_context->recv.p_ready_entry == NULL);
  assert(p_ibverbs_srq_context->recv.p_ibverbs_srq == NULL);
  nm_ibverbs_srq_refill(p_ibverbs_srq_context);
  struct ibv_wc wc;
  int ne = ibv_poll_cq(p_ibverbs_srq_context->p_ibverbs_context->srq.srq_cq, 1, &wc);
  if(ne < 0)
    {
      NM_FATAL("poll CQ failed (status=%d; %s).\n", wc.status, nm_ibverbs_status_strings[wc.status]);
    }
  else if(ne > 0)
    {
      const uintptr_t wrid = wc.wr_id;
      const struct nm_ibverbs_regpool_entry_s*p_rbuf_entry = (void*)wrid;
      const uint32_t qpn = wc.qp_num;
      struct nm_ibverbs_srq_packet_s*p_packet = p_rbuf_entry->p_ptr;
      struct nm_ibverbs_srq_s*__restrict__ p_ibverbs_srq =
        nm_ibverbs_srq_qpn_hashtable_lookup(&p_ibverbs_srq_context->statuses_by_qpn, &qpn);
      if(p_ibverbs_srq == NULL)
        {
          /* lazy connect, new connection not finalized yet */
          struct nm_ibverbs_context_s*p_ibverbs_context = p_ibverbs_srq_context->p_ibverbs_context;
          nm_spin_lock(&p_ibverbs_context->connect.lock);
          struct nm_ibverbs_cnx_s*p_cnx = nm_ibverbs_cnx_lazy_by_qpn(p_ibverbs_context, qpn);
          assert(p_cnx != NULL);
          p_ibverbs_srq = nm_ibverbs_srq_addr_hashtable_lookup(&p_ibverbs_srq_context->statuses_by_addr, &p_cnx->p_addr_entry->msg.addr);
          assert(p_ibverbs_srq != NULL);
          assert((p_ibverbs_srq->p_cnx == NULL) || (p_ibverbs_srq->p_cnx == p_cnx));
          p_ibverbs_srq->p_cnx = p_cnx;
          nm_ibverbs_srq_qpn_hashtable_insert(&p_ibverbs_srq_context->statuses_by_qpn,
                                              &p_ibverbs_srq->p_cnx->local_addr.addr.qpn, p_ibverbs_srq);
          nm_spin_unlock(&p_ibverbs_context->connect.lock);
        }
      assert(p_ibverbs_srq != NULL);
      const nm_len_t chunk_len = wc.byte_len - sizeof(struct nm_ibverbs_srq_header_s);
      assert(p_packet->header.len != NM_LEN_UNDEFINED);
      assert(chunk_len == p_packet->header.len);
      const uint32_t csum = nm_ibverbs_checksum(p_packet->data, chunk_len);
      assert(p_packet->header.csum == csum);
      p_ibverbs_srq_context->recv.chunk_len = chunk_len;
      p_ibverbs_srq_context->recv.p_ready_entry = p_rbuf_entry;
      p_ibverbs_srq_context->recv.p_ibverbs_srq = p_ibverbs_srq;
    }
  else if((ne == 0) && p_ibverbs_srq_context->lazy_connect)
    {
      nm_ibverbs_cnx_lazy_progress(p_ibverbs_srq_context->p_ibverbs_context);
    }
}

static int nm_ibverbs_srq_recv_probe_any(puk_context_t p_context, void**_status)
{
  struct nm_ibverbs_srq_context_s*__restrict__ p_ibverbs_srq_context = puk_context_get_status(p_context);
  nm_ibverbs_srq_recv_progress(p_ibverbs_srq_context);
  if(p_ibverbs_srq_context->recv.p_ibverbs_srq != NULL)
    {
      *_status = p_ibverbs_srq_context->recv.p_ibverbs_srq;
      return NM_ESUCCESS;
    }
  else
    {
      *_status = NULL;
      return -NM_EAGAIN;
    }
}

static int nm_ibverbs_srq_recv_wait_any(puk_context_t p_context, void**_status)
{
  struct nm_ibverbs_srq_context_s*__restrict__ p_ibverbs_srq_context = puk_context_get_status(p_context);
  int rc = -NM_EUNKNOWN;
  do
    {
      if(!p_ibverbs_srq_context->running)
        return -NM_ECANCELED;
      if(p_ibverbs_srq_context->p_ibverbs_context->ib_opts.use_comp_channel)
        {
          int rc = nm_ibverbs_context_wait_event(p_ibverbs_srq_context->p_ibverbs_context);
          if(rc != NM_ESUCCESS)
            {
              return rc;
            }
        }
      rc = nm_ibverbs_srq_recv_probe_any(p_context, _status);
    }
  while(rc != NM_ESUCCESS);
  return NM_ESUCCESS;
}

static int nm_ibverbs_srq_recv_buf_poll(void*_status, void**p_buffer, nm_len_t*p_len)
{
  struct nm_ibverbs_srq_s*__restrict__ p_ibverbs_srq = _status;
  struct nm_ibverbs_srq_context_s*__restrict__ p_ibverbs_srq_context = p_ibverbs_srq->p_ibverbs_srq_context;
 retry:
  if(p_ibverbs_srq_context->recv.p_ibverbs_srq == p_ibverbs_srq)
    {
      const struct nm_ibverbs_regpool_entry_s*p_rbuf_entry = p_ibverbs_srq_context->recv.p_ready_entry;
      struct nm_ibverbs_srq_packet_s*p_packet = p_rbuf_entry->p_ptr;
      *p_buffer = p_packet->data;
      *p_len = p_ibverbs_srq_context->recv.chunk_len;
      return NM_ESUCCESS;
    }
  else
    {
      if(p_ibverbs_srq_context->recv.p_ibverbs_srq == NULL)
        {
          nm_ibverbs_srq_recv_progress(p_ibverbs_srq_context);
          if(p_ibverbs_srq_context->recv.p_ibverbs_srq == p_ibverbs_srq)
            {
              goto retry;
            }
        }
      return -NM_EAGAIN;
    }
}

static int nm_ibverbs_srq_recv_buf_release(void*_status)
{
  struct nm_ibverbs_srq_s*__restrict__ p_ibverbs_srq = _status;
  struct nm_ibverbs_srq_context_s*__restrict__ p_ibverbs_srq_context = p_ibverbs_srq->p_ibverbs_srq_context;
  assert(p_ibverbs_srq_context->recv.p_ibverbs_srq == p_ibverbs_srq);
  assert(p_ibverbs_srq_context->recv.chunk_len != NM_LEN_UNDEFINED);
  /* re-post buffer */
  const struct nm_ibverbs_regpool_entry_s*p_rbuf_entry = p_ibverbs_srq_context->recv.p_ready_entry;
  nm_ibverbs_srq_post_entry(p_ibverbs_srq_context, p_rbuf_entry);
  /* clear context status */
  p_ibverbs_srq_context->recv.chunk_len = NM_LEN_UNDEFINED;
  p_ibverbs_srq_context->recv.p_ready_entry = NULL;
  p_ibverbs_srq_context->recv.p_ibverbs_srq = NULL;
  return NM_ESUCCESS;
}

static int nm_ibverbs_srq_recv_cancel_any(puk_context_t p_context)
{
  struct nm_ibverbs_srq_context_s*p_ibverbs_srq_context = puk_context_get_status(p_context);
  nm_ibverbs_context_cancel_wait(p_ibverbs_srq_context->p_ibverbs_context);
  p_ibverbs_srq_context->running = 0;
  return NM_ESUCCESS;
}

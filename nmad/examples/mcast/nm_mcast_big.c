/*
 * NewMadeleine
 * Copyright (C) 2021-2022 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

/* Example / test of the mcast interface sending big chunk of data */

#include <stdlib.h>
#include <stdio.h>

#include <nm_launcher_interface.h>
#include <nm_coll_interface.h>
#include <nm_mcast_interface.h>

#include "../common/nm_examples_helper.h"

#define N_TESTS 10
#define NX 26214400 // * sizeof(int) = 100 MB

int main(int argc, char** argv)
{
  int i, j, worldsize;
  nm_tag_t tag = 0xbeef;

  nm_examples_init_topo(&argc, argv, NM_EXAMPLES_TOPO_STAR);
  nm_mcast_service_t mcast_service = nm_mcast_init(p_comm);

  nm_launcher_get_size(&worldsize);
  if(worldsize == 1)
    {
      fprintf(stderr, "We need several nodes.\n");
      nm_examples_exit();
      return EXIT_FAILURE;
    }

  int* array = malloc(NX * sizeof(int));
  for (i = 0; i < NX; i++)
    {
      array[i] = -1;
    }

  if (is_server)
    {
      int* dests = malloc(sizeof(int) * (worldsize - 1));
      for (i = 1; i < worldsize; i++)
        {
         dests[i-1] = i;
        }

      for (j = 0; j < N_TESTS; j++)
        {
          for(i = 0; i < NX; i++)
            {
              array[i] = j*i;
            }

          struct nm_data_s data;
          nm_data_contiguous_build(&data, (int*) array, NX*sizeof(int));
          nm_mcast_send(mcast_service, p_comm, dests, NULL, worldsize-1, tag, &data, 0, NM_COLL_TREE_DEFAULT);
        }

      free(dests);
    }
  else
    {
      for (j = 0; j < N_TESTS; j++)
        {
          nm_sr_recv(nm_comm_get_session(p_comm), 0, tag, (int*) array, NX*sizeof(int));

          for (i = 0; i < NX; i++)
            {
              if (array[i] != i*j)
                {
                  NM_FATAL("Test %d: array[%d] = %d (should be %d)\n", j, i, array[i], i*j);
                }
            }
        }
    }

  printf("success\n");

  free(array);
  nm_mcast_finalize(mcast_service);
  nm_examples_exit();
  return EXIT_SUCCESS;
}

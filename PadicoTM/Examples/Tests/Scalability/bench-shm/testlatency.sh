#!/bin/bash 

rm datalatency$1.dat
for ((i=2;i<=16;i=i+2))
do
    rm tmp.dat  
    padico-launch -q -n  $i -q testdrivershmlatency >> tmp.dat
    
    ./form_latency tmp.dat $i >> datalatency$1.dat
    
done
pltcmd="plot 'datalatency$1.dat' using 1:3:2:4 with yerrorlines title 'Latency without positioning' lc 8 lw 2 pt 2"

lstopo --merge --output-format console --no-io -p > topo$1.txt
sed -i '1d' topo$1.txt

k=$(./testcpubinding topo$1.txt) 

for((j=-1;j < k;j++))
do
    if [ $j -eq -1 ];then
	rm datalatency$1coretocore.dat
    else
	name=$(./cachename topo$1.txt $k $j)
	rm datalatency$1$name.dat 
    fi
    for ((i=2;i<=16;i=i+2))
    do
	rm tmp.dat
	binding=$(./testcpubinding2 topo$1.txt $k $j $i)
	padico-launch -q -n $i $binding testdrivershmlatency >> tmp.dat
	if [ $j -eq -1 ];then
	    ./form_latency tmp.dat $i >> datalatency$1coretocore.dat
	    
	else
	    ./form_latency tmp.dat $i >> datalatency$1$name.dat 
	fi
    done
    if [ $j -eq -1 ];then
	pltcmd=$pltcmd",'datalatency$1coretocore.dat' using 1:3:2:4 with yerrorlines title 'Latency with core-to-core positioning' lc 9 lw 2 pt 2"
    else
	pltcmd=$pltcmd",'datalatency$1$name.dat' using 1:3:2:4 with yerrorlines title 'Latency with positioning mode $name' lc $j lw 2 pt 2" 
    fi
done
rm tmp.dat
rm topo$1.txt
         gnuplot<<EOF
set terminal png size 1500,1000
set output 'testlatency$1.png'
set xlabel 'Nb. pair process'
set key box
set ytics .1
set ylabel 'Latency(usec)'
set grid
set title 'Latency on $1 depending on number of pair processus running'
$pltcmd
EOF
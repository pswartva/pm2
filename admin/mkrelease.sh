#! /bin/sh

version="$1"

if [ "x${version}" = "x" ]; then
    echo "usage: $0 <version>"
    exit 1
fi

VERSION="`cat ../building-tools/VERSION`"

if [ "${version}" != "${VERSION}" ]; then
    echo "version mismatch ${version} != ${VERSION}"
    exit 1
fi

echo "# packaging version ${VERSION}..."


tmpdir=`mktemp -d /tmp/pm2-release-XXXXXXXX`
RELEASE_NAME="pm2-${VERSION}"

DISTRIB_DIR=${tmpdir}/${RELEASE_NAME}

echo
echo "# ## Generating release ${RELEASE_NAME} in ${tmpdir}"
echo
mkdir -p ${DISTRIB_DIR}

echo "# Exporting from git HEAD to ${DISTRIB_DIR}"
( cd .. ; git archive --format=tar HEAD . | tar x -C ${DISTRIB_DIR} )

echo "# Generating configuration files..."
( cd ${DISTRIB_DIR}; ./autogen.sh )

echo
echo "# Packaging ${RELEASE_NAME}.tar.gz"
( cd ${tmpdir} ; tar czf ${RELEASE_NAME}.tar.gz ${RELEASE_NAME} )

echo
echo "# Purging ${RELEASE_NAME}/..."
( cd ${tmpdir} ; rm -rf ${RELEASE_NAME}/ )

echo
echo "# ## pm2 distribution ready in: ${tmpdir}"
echo
ls -l ${tmpdir}
echo

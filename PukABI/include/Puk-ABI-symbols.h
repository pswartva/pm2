/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file PukABI symbols declaration
 * @warning to prevent conflicts from minor variations in function
 * prototypes, this file must include no header from standard libc.
 * @note this file is not supposed to be included directly
 */

/* no libc header allowed in this file to avoid conflicting declarations,
 * except starg.h required for variadic functions interception. */
#include <stdarg.h>

#if(!defined(MKSYMFX) || !defined(MKSYMPX) || !defined(MKSYMNOSUPPORT))
#error "Puk-ABI-symbols.h included in bad context: required macros no defined."
#endif

static inline int puk_abi_symbol_libcdefault(struct puk_abi_sym_s*s, void*libc_sym)
{
  if(!s->ptr_real)
    {
      s->ptr_real = libc_sym;
    }
  return (s->ptr_virtual == NULL);
}

/* *** PukABI toolbox ************************************** */

/* pre/post processing to add around symbol invocations */

#define NOPREPROCESSING    /**< no pre-processing */
#define NOPOSTPROCESSING   /**< no post-processing */

/* policies to choose between real/virtual symbols */

/** always use the real symbol */
#define ALWAYSREAL       1
/** always use the virtual symbol */
#define ALWAYSVIRTUAL    0
/** use virtual if available (i.e. non-NULL), real else */
#define DEFAULTTOREAL(SYM) ((PUK_ABI_SYM_DESCNAME(SYM)).ptr_virtual == NULL)
/** use virtual if available, real initialized on the fly from libc else */
#define DEFAULTTOLIBCREAL(SYM) (puk_abi_symbol_libcdefault(&PUK_ABI_SYM_DESCNAME(SYM), &__libc_##SYM))
/** chose virtual/real depending on the given file descriptor */
#define FOLLOWFD(FD) (puk_fds_isreal(FD))

#ifdef PUKABI_ENABLE_FSYS

/* file descriptors manipulations  */

/** convert an FD from ABI to virtual fd space */
#define FDFROMABI(FD) { /* (FD) = puk_fds_from_abi(FD); */ }
/** create a new entry in the virtual fd table */
#define VIRTUALIZEFD(FD) \
     { if(((FD) > -1) && _will_use_real) { (FD) = puk_fds_alloc(1, (FD), __FUNCTION__); } }

#endif

/* *** errno */
#define PUK_ABI_MKERRNO(SYM) PUK_ABI_MKTYPE(SYM, int*, (void))
#if defined(PUKABI_SYMBOL_ERRNO)
PUK_ABI_MKERRNO(PUKABI_SYMBOL_ERRNO);
#else
PUK_ABI_MKERRNO(puk_abi_errno_sym);
#endif

/* *** fixed size integers */
#if (PUKABI_SIZEOF_INT == 8)
#  define __puk_abi_int64_t int
#elif (PUKABI_SIZEOF_LONG == 8)
#  define __puk_abi_int64_t long
#elif (PUKABI_SIZEOF_LONG_LONG == 8)
#  define __puk_abi_int64_t long long
#else
#  error "PukABI: cannot find 64 bits integers"
#endif
#if (PUKABI_SIZEOF_INT == 4)
#  define __puk_abi_int32_t int
#elif (PUKABI_SIZEOF_LONG == 4)
#  define __puk_abi_int32_t long
#elif (PUKABI_SIZEOF_LONG_LONG == 4)
#  define __puk_abi_int32_t long long
#else
#  error "PukABI: cannot find 32 bits integers"
#endif
#if  (PUKABI_SIZEOF_SSIZE_T == PUKABI_SIZEOF_INT)
#define __puk_abi_ssize_t int
#elif (PUKABI_SIZEOF_SSIZE_T == PUKABI_SIZEOF_LONG)
#define __puk_abi_ssize_t long
#elif (PUKABI_SIZEOF_SSIZE_T == PUKABI_SIZEOF_LONG_LONG)
#define __puk_abi_ssize_t long long
#else
#define __puk_abi_ssize_t int
#endif

#ifdef PUKABI_ENABLE_MEM

/* external declaration for internal libc symbols */
extern void*__libc_malloc();
extern void*__libc_calloc();
extern void*__libc_realloc();
extern void __libc_free();
extern void*__libc_memalign();

// void*malloc(size_t);
MKSYMFX(malloc,  void*, (unsigned long size), (size),
        NOPREPROCESSING, { /* puk_abi_mem_invalidate(result, size); */ }, DEFAULTTOLIBCREAL(malloc));

// void free(void*ptr);
MKSYMPX(free,    void,  (void*ptr), (ptr),
        { puk_abi_mem_free_invalidate(ptr); }, NOPOSTPROCESSING, DEFAULTTOLIBCREAL(free));

// void*realloc(void*ptr, size_t size);
MKSYMFX(realloc, void*, (void *ptr, unsigned long size), (ptr, size),
        { puk_abi_mem_free_invalidate(ptr); }, { puk_abi_mem_invalidate(result, size); }, DEFAULTTOLIBCREAL(realloc));

// void*calloc(size_t nelem, size_t elsize);
MKSYMFX(calloc,  void*, (unsigned long nelem, unsigned long elsize), (nelem, elsize),
        NOPREPROCESSING, { /* puk_abi_mem_invalidate(result, nelem*elsize); */ }, DEFAULTTOLIBCREAL(calloc));

//int munmap(void *addr, size_t len);
MKSYMFX(munmap, int, (void*addr, int len), (addr, len),
        { /* puk_abi_print("munmap addr = %p; len = %d\n", addr, (int)len); */ puk_abi_mem_invalidate(addr, len); },
        NOPOSTPROCESSING, DEFAULTTOREAL(munmap));

#ifdef PUKABI_ENABLE_FSYS
//void *mmap(void *addr, size_t len, int prot, int flags,  int  fildes, off_t off);
MKSYMFX(mmap, void*, (void*addr, int len, int prot, int flags, int fd, int off), (addr, len, prot, flags, fd, off),
        NOPREPROCESSING,
        { /* puk_abi_mem_invalidate(result, len); */ },
        ((fd==-1)?(DEFAULTTOREAL(mmap)):(FOLLOWFD(fd))));
#else
MKSYMFX(mmap, void*, (void*addr, int len, int prot, int flags, int fd, int off), (addr, len, prot, flags, fd, off),
        NOPREPROCESSING, { /* puk_abi_mem_invalidate(result, len); */ }, DEFAULTTOREAL(mmap));
#endif

// void *mremap(void *old_address, size_t old_size, size_t new_size, int flags);
MKSYMFX(mremap, void*, (void*old_addr, unsigned long old_size, unsigned long new_size, int flags),
        (old_addr, old_size, new_size, flags),
        { /* puk_abi_print("mremap()\n"); */ puk_abi_mem_invalidate(old_addr, old_size); },
        { /* puk_abi_mem_invalidate(result, new_size); */ },
        DEFAULTTOREAL(mremap));

// char *strdup(const char *s);
MKSYMFX(strdup,  char*, (const char*s), (s),
        NOPREPROCESSING, NOPOSTPROCESSING, DEFAULTTOREAL(strdup));

// int posix_memalign(void **memptr, size_t alignment, size_t size);
MKSYMFX(posix_memalign, int, (void**memptr, unsigned long alignment, unsigned long size),
        (memptr, alignment, size),
        NOPREPROCESSING,
        { /* if(result==0)puk_abi_mem_invalidate(*memptr, size); */ },
        DEFAULTTOREAL(posix_memalign));

// void *memalign(size_t boundary, size_t size);
MKSYMFX(memalign,  void*, (unsigned long boundary, unsigned long size), (boundary, size),
        NOPREPROCESSING, { /* puk_abi_mem_invalidate(result, size); */ }, DEFAULTTOLIBCREAL(memalign));

MKSYMFX(brk, int, (void**addr), (addr),
        { /* puk_abi_print("brk()\n"); */ }, NOPOSTPROCESSING, DEFAULTTOREAL(brk));

MKSYMFX(sbrk, void*, (long increment), (increment),
        NOPREPROCESSING, NOPOSTPROCESSING, DEFAULTTOREAL(sbrk));

#endif /* PUKABI_ENABLE_MEM */

#ifdef PUKABI_ENABLE_PROC

// int posix_spawn(pid_t *restrict pid, const char *restrict path, const posix_spawn_file_actions_t *file_actions,
//                 const posix_spawnattr_t *restrict attrp, char *const argv[restrict], char *const envp[restrict]);
MKSYMFX(posix_spawn,
        int,
        (int *pid, const char *path, const void *file_actions,
         const void *attrp, char *const argv[], char *const envp[]),
        (pid, path, file_actions, attrp, argv, envp),
        NOPREPROCESSING,
        NOPOSTPROCESSING,
        DEFAULTTOREAL(posix_spawn));

// int posix_spawnp(pid_t *restrict pid, const char *restrict file, const posix_spawn_file_actions_t *file_actions,
//                  const posix_spawnattr_t *restrict attrp, char *const argv[restrict], char * const envp[restrict]);
MKSYMFX(posix_spawnp,
        int,
        (int *pid, const void *file, const void *file_actions,
         const void *attrp, char *const argv[], char *const envp[]),
        (pid, file, file_actions, attrp, argv, envp),
        NOPREPROCESSING,
        NOPOSTPROCESSING,
        DEFAULTTOREAL(posix_spawnp));

//void exit(int status);
MKSYMPX(exit, void, (int status), (status),
  NOPREPROCESSING,
  NOPOSTPROCESSING,
  DEFAULTTOREAL(exit));

#endif /* PUKABI_ENABLE_PROC */


// int fprintf(FILE *stream, const char *format, ...);
MKSYMVX2(fprintf, int, (void *stream, const char *format, ...), stream, format,
{
  va_list _args;
  va_start(_args, format);
  _result = puk_abi_helper__vfprintf(stream, format, _args);
  va_end(_args);
},
  DEFAULTTOREAL(fprintf));

// FILE *fopen(const char *path, const char *mode);
MKSYMFX(fopen,  void*, (const char *path, const char *mode), (path, mode),
       NOPREPROCESSING, NOPOSTPROCESSING, DEFAULTTOREAL(fopen));

// int fclose(FILE *fp);
MKSYMFX(fclose,  int, (void *fp), (fp),
       NOPREPROCESSING, NOPOSTPROCESSING, DEFAULTTOREAL(fclose));

// int feof(FILE *stream);
MKSYMFX(feof,  int, (void *stream), (stream),
       NOPREPROCESSING, NOPOSTPROCESSING, DEFAULTTOREAL(feof));

// int fflush(FILE *stream);
MKSYMFX(fflush,  int, (void *stream), (stream),
       NOPREPROCESSING, NOPOSTPROCESSING, DEFAULTTOREAL(fflush));

// char *fgets(char *s, int size, FILE *stream);
MKSYMFX(fgets,  char*, (char *s, int size, void *stream), (s, size, stream),
       NOPREPROCESSING, NOPOSTPROCESSING, DEFAULTTOREAL(fgets));

// int fscanf(FILE *stream, const char *format, ...);
MKSYMVX2(fscanf, int, (void *stream, const char *format, ...), stream, format,
{
  va_list _args;
  va_start(_args, format);
  _result = puk_abi_helper__vfscanf(stream, format, _args);
  va_end(_args);
 },
  DEFAULTTOREAL(fscanf));

// int printf(const char *format, ...);
MKSYMVX1(printf, int, (const char *format, ...), format,
{
  va_list _args;
  va_start(_args, format);
  _result = puk_abi_helper__vprintf(format, _args);
  va_end(_args);
},
  DEFAULTTOREAL(printf));

// int puts(const char *str);
MKSYMFX(puts, int, (const char *str), (str),
        NOPREPROCESSING, NOPOSTPROCESSING, DEFAULTTOREAL(puts));

// int putchar(int chr);
MKSYMFX(putchar, int, (int chr), (chr),
        NOPREPROCESSING, NOPOSTPROCESSING, DEFAULTTOREAL(putchar));

// int fputs(const char *str, FILE *out);
MKSYMFX(fputs, int, (const char *str, void *out), (str, out),
        NOPREPROCESSING, NOPOSTPROCESSING, DEFAULTTOREAL(fputs));

// int putc(int chr, FILE *out);
MKSYMFX(putc, int, (int chr, void *out), (chr, out),
        NOPREPROCESSING, NOPOSTPROCESSING, DEFAULTTOREAL(putc));

#ifdef PUKABI_ENABLE_FSYS

/* Functions that operate on file descriptors.  */

// int socket(int domain, int type, int protocol)
MKSYMFX(socket,  int, (int domain, int type, int protocol), (domain, type, protocol),
        NOPREPROCESSING,
        VIRTUALIZEFD(result),
        DEFAULTTOREAL(socket));

// int flock(int fd, int operation);
MKSYMFX(flock, int, (int fd, int op), (fd, op),
        { puk_abi_print("flock()fd=%d", fd); FDFROMABI(fd)},
        NOPOSTPROCESSING,
        FOLLOWFD(fd));

// int open(const char *path, int oflag, /* mode_t mode */...);
MKSYMVX2(open, int, (const char*path, int oflag, ...), path, oflag,
{
  va_list _ap;
  va_start(_ap, oflag);
  const int o = puk_abi_oracle__open_flags(oflag);
  if(o)
    {
      unsigned mode = va_arg(_ap, unsigned);
      puk_abi_trace("PukABI: open()- path=%s; mode=%d", path, (int)mode);
      _result = (*_fun)(path, oflag, mode);
    }
  else
    {
      puk_abi_trace("PukABI: open()- path=%s", path);
      _result = (*_fun)(path, oflag);
    }
  puk_abi_trace("PukABI: open()- result=%d", _result);
  VIRTUALIZEFD(_result);
  va_end(_ap);
},
         (puk_abi_oracle__open_kind(path))?ALWAYSREAL:DEFAULTTOREAL(open));
// int open64(const char *path, int oflag, /* mode_t mode */...);
MKSYMVX2(open64, int, (const char*path, int oflag, ...), path, oflag,
{
  va_list _ap;
  va_start(_ap, oflag);
  const int o = puk_abi_oracle__open_flags(oflag);
  if(o)
    {
      unsigned mode = va_arg(_ap, unsigned);
      puk_abi_trace("PukABI: open64()- fun=%p; path=%s; flags=%d; mode=%d", _fun, path, oflag, (int)mode);
      _result = (*_fun)(path, oflag, mode);
    }
  else
    {
      puk_abi_trace("PukABI: open64()- fun=%p; path=%s; flags=%d", _fun, path, oflag);
      _result = (*_fun)(path, oflag);
    }
  puk_abi_trace("PukABI: open64()- result=%d", _result);
  VIRTUALIZEFD(_result);
  va_end(_ap);
},
         (puk_abi_oracle__open_kind(path))?ALWAYSREAL:DEFAULTTOREAL(open64));
// int close(int fd);
MKSYMFX(close,   int, (int fd), (fd),
        NOPREPROCESSING, {if(result==0)puk_fds_free(fd);}, FOLLOWFD(fd));
// int dup(int fd);
MKSYMFX(dup, int, (int fd), (fd),
        FDFROMABI(fd),
        VIRTUALIZEFD(result),
        DEFAULTTOREAL(dup));
// int dup2(int fd1, int fd2);
MKSYMFX(dup2, int, (int fd1, int fd2), (fd1, fd2),
        FDFROMABI(fd1),
        VIRTUALIZEFD(result),
        DEFAULTTOREAL(dup2));

// FILE *fdopen(int fd, const char *mode);
MKSYMFX(fdopen, void*, (int fd, const char*mode), (fd, mode),
        FDFROMABI(fd),
        NOPOSTPROCESSING,
        DEFAULTTOREAL(fdopen));

//DIR *fdopendir(int fd);
MKSYMFX(fdopendir, void*, (int fd), (fd),
       FDFROMABI(fd),
       NOPOSTPROCESSING,
       DEFAULTTOREAL(fdopendir));

// int bind(int s, const struct sockaddr *name, int namelen);
MKSYMFX(bind, int, (int s, const void*name, unsigned int namelen), (s, name, namelen),
  FDFROMABI(s),
  NOPOSTPROCESSING,
  FOLLOWFD(s));
// int listen(int s, int backlog);
MKSYMFX(listen, int, (int s, int backlog), (s, backlog),
  FDFROMABI(s),
  NOPOSTPROCESSING,
  FOLLOWFD(s));
// int connect(int s, const struct sockaddr*name, socklen_t namelen);
MKSYMFX(connect, int, (int s, const void*name, unsigned int namelen), (s, name, namelen),
  FDFROMABI(s),
  NOPOSTPROCESSING,
  FOLLOWFD(s));
// int accept(int s, struct sockaddr*addr, socklen_t*addrlen);
MKSYMFX(accept, int, (int s, void*addr, unsigned int*addrlen), (s, addr, addrlen),
  FDFROMABI(s),
  {if(result>=0){VIRTUALIZEFD(result);}}, FOLLOWFD(s));
// int shutdown(int s, int how);
MKSYMFX(shutdown, int, (int s, int how), (s, how),
  FDFROMABI(s),
  NOPOSTPROCESSING,
  FOLLOWFD(s));
// ssize_t read(int fildes, void *buf, size_t nbyte);
MKSYMFX(read, __puk_abi_ssize_t, (int fd, void*buf, int nbytes), (fd, buf, nbytes),
  FDFROMABI(fd),
  NOPOSTPROCESSING,
  FOLLOWFD(fd));
// ssize_t pread(int fildes, void *buf, size_t nbyte, off_t offset);
MKSYMFX(pread, __puk_abi_ssize_t, (int fd, void*buf, int nbytes, int offset), (fd, buf, nbytes, offset),
  FDFROMABI(fd),
  NOPOSTPROCESSING,
  FOLLOWFD(fd));
// ssize_t write(int fildes, const void *buf, size_t nbyte)
MKSYMFX(write, __puk_abi_ssize_t, (int fd, const void*buf, int nbytes), (fd, buf, nbytes),
  FDFROMABI(fd),
  NOPOSTPROCESSING,
  FOLLOWFD(fd));
// ssize_t pwrite(int fildes, const void *buf, size_t nbyte, off_t offset)
MKSYMFX(pwrite, __puk_abi_ssize_t, (int fd, const void*buf, int nbytes, int offset), (fd, buf, nbytes, offset),
  FDFROMABI(fd),
  NOPOSTPROCESSING,
  FOLLOWFD(fd));
// ssize_t send(int s, const void *msg, size_t len, int flags);
MKSYMFX(send, __puk_abi_ssize_t, (int s, const void*msg, unsigned len, int flags), (s, msg, len, flags),
  FDFROMABI(s),
  NOPOSTPROCESSING,
  FOLLOWFD(s));
// ssize_t recv(int s, void *buf, size_t len, int flags);
MKSYMFX(recv, __puk_abi_ssize_t, (int s, void*buf, unsigned len, int flags), (s, buf, len, flags),
  FDFROMABI(s),
  NOPOSTPROCESSING,
  FOLLOWFD(s));
// ssize_t sendto(int s,  const  void  *msg,  size_t  len,  int  flags, const struct sockaddr *to, socklen_t tolen);
MKSYMFX(sendto, __puk_abi_ssize_t, (int s, const void*msg, unsigned len, int flags, const void*to, unsigned int tolen),
  (s, msg, len, flags, to, tolen),
  FDFROMABI(s),
  NOPOSTPROCESSING,
  FOLLOWFD(s));
//  ssize_t recvfrom(int s, void *buf, size_t  len,  int  flags,  struct sockaddr *from, socklen_t *fromlen);
MKSYMFX(recvfrom, __puk_abi_ssize_t, (int s, void*buf, unsigned len, int flags, void*to, unsigned int*tolen),
  (s, buf, len, flags, to, tolen),
  FDFROMABI(s),
  NOPOSTPROCESSING,
  FOLLOWFD(s));
// ssize_t readv(int fildes, const struct iovec *iov, int iovcnt);
MKSYMFX(readv, __puk_abi_ssize_t, (int fd, void*iov, int iovcount), (fd, iov, iovcount),
  FDFROMABI(fd),
  NOPOSTPROCESSING,
  FOLLOWFD(fd));
// ssize_t writev(int fildes,  const  struct  iovec  *iov,  int iovcnt);
MKSYMFX(writev, __puk_abi_ssize_t, (int fd, void*iov, int iovcount), (fd, iov, iovcount),
  FDFROMABI(fd),
  NOPOSTPROCESSING,
  FOLLOWFD(fd));
// ssize_t sendmsg(int s, const struct msghdr *msg, int flags);
MKSYMFX(sendmsg, __puk_abi_ssize_t, (int fd, void*msg, int flags), (fd, msg, flags),
  FDFROMABI(fd),
  NOPOSTPROCESSING,
  FOLLOWFD(fd));
// ssize_t recvmsg(int s, struct msghdr *msg, int flags);
MKSYMFX(recvmsg, __puk_abi_ssize_t, (int fd, void*msg, int flags), (fd, msg, flags),
  FDFROMABI(fd),
  NOPOSTPROCESSING,
  FOLLOWFD(fd));
// int fileno(FILE *stream);
MKSYMFX(fileno, int, (void*stream), (stream),
  NOPREPROCESSING,
{
  if(result > 2) { puk_fds_isreal(result); }
}, ALWAYSREAL);
// int fcntl(int fildes, int cmd, /* arg */ ...);
MKSYMVX2(fcntl, int, (int fd, int cmd, ...), fd, cmd,
  {
    int oldfd = fd;
    va_list _ap;
    va_start(_ap, cmd);
    FDFROMABI(fd);
    puk_abi_trace("fcntl()- will_use_real=%d; fd=%d; oldfd=%d; cmd=%d; fcntl=0x%lx [%s]",
      _will_use_real, fd, oldfd, cmd, _fun, __FUNCTION__);
    const int o = puk_abi_oracle__fcntl(cmd);
    switch(o)
    {
      case 0:
        _result = (*_fun)(fd, cmd);
        break;
      case 1:
        {
          long a = va_arg(_ap, long);
          _result = (*_fun)(fd, cmd, a);
        }
        break;
      case 2:
        {
          void*ptr = va_arg(_ap, void*);
          _result = (*_fun)(fd, cmd, ptr);
        }
        break;
      default:
      _result = -1;
      break;
    }
    puk_abi_trace("fcntl()- rc=%d [%s]", _result, __FUNCTION__);
    va_end(_ap);
   },
  FOLLOWFD(fd));
// int getsockname(int  s,  struct  sockaddr  *name,  socklen_t *namelen);
MKSYMFX(getsockname, int, (int s, void*name, void*namelen), (s, name, namelen),
  FDFROMABI(s),
  NOPOSTPROCESSING,
  FOLLOWFD(s));
// int select(int nfds, fd_set*readfds, fd_set*writefds, fd_set*errorfds, struct timeval*timeout);
MKSYMFX(select, int, (int nfds, void*rfd, void*wfd, void*efd, void*to), (nfds, rfd, wfd, efd, to),
{
  puk_abi_trace("stub__select()- entering- nfds=%d", nfds);
},
{
  puk_abi_trace("stub__select()- exiting- result=%d", result);
},
  puk_fdset_isreal(rfd, wfd, efd, nfds));
// int poll(struct pollfd fds[], nfds_t nfds, int timeout);
MKSYMFX(poll, int, (void*fds, unsigned int nfds, int timeout), (fds, nfds, timeout),
        NOPREPROCESSING,
        NOPOSTPROCESSING,
        FOLLOWFD(puk_pollfd_isreal(fds, nfds)));

// int setsockopt(int s, int level,  int  optname,  const  void *optval, int optlen)
MKSYMFX(setsockopt, int,
  (int s, int level, int optname, const void*optval, int optlen),
  (s, level, optname, optval, optlen),
  FDFROMABI(s),
  NOPOSTPROCESSING,
  FOLLOWFD(s));
// int getsockopt(int s, int level, int optname, void  *optval, int *optlen)
MKSYMFX(getsockopt, int,
  (int s, int level, int optname, void*optval, int*optlen),
  (s, level, optname, optval, optlen),
  FDFROMABI(s),
  NOPOSTPROCESSING,
  FOLLOWFD(s));
// off_t lseek(int fildes, off_t offset, int whence);
MKSYMFX(lseek, int, (int fd, int offset, int whence), (fd, offset, whence),
  FDFROMABI(fd),
  NOPOSTPROCESSING,
  FOLLOWFD(fd));
// off64_t lseek64(int fildes, off64_t offset, int whence);
MKSYMFX(lseek64, __puk_abi_int64_t, (int fd, __puk_abi_int64_t offset, int whence), (fd, offset, whence),
  FDFROMABI(fd),
  NOPOSTPROCESSING,
  FOLLOWFD(fd));
// int ftruncate(int fildes, off_t length);
MKSYMFX(ftruncate, int, (int fd, long length), (fd, length),
  FDFROMABI(fd),
  NOPOSTPROCESSING,
  FOLLOWFD(fd));
// int ftruncate64(int fildes, off64_t length);
MKSYMFX(ftruncate64, int, (int fd, __puk_abi_int64_t length), (fd, length),
  FDFROMABI(fd),
  NOPOSTPROCESSING,
  FOLLOWFD(fd));
// int creat(const char *path, mode_t mode);
MKSYMFX(creat, int, (const char*path, unsigned mode), (path, mode),
  NOPREPROCESSING,
{
  if(result >= 0)
    { VIRTUALIZEFD(result); }
},
         (puk_abi_oracle__open_kind(path))?ALWAYSREAL:DEFAULTTOREAL(creat));
// int fsync(int fd);
MKSYMFX(fsync, int, (int fd), (fd),
  FDFROMABI(fd),
  NOPOSTPROCESSING,
  FOLLOWFD(fd));
// int pipe(int fildes[2]);
MKSYMFX(pipe, int, (int fd[]), (fd),
  NOPREPROCESSING,
{
  if(result==0) {
    VIRTUALIZEFD(fd[0]);
    VIRTUALIZEFD(fd[1]);
  }
}, DEFAULTTOREAL(pipe));
// int mkfifo(const char *pathname, mode_t mode);
MKSYMFX(mkfifo, int, (const char*pathname, unsigned mode), (pathname, mode),
  NOPREPROCESSING,
  { if(result >= 0) { VIRTUALIZEFD(result); } },
  DEFAULTTOREAL(mkfifo));
// int socketpair(int d, int type, int protocol, int sv[2]);
MKSYMFX(socketpair, int, (int fd, int type, int protocol, int sv[2]), (fd, type, protocol, sv),
  NOPREPROCESSING,
{
  if(result==0) {
    VIRTUALIZEFD(sv[0]);
    VIRTUALIZEFD(sv[1]);
  }
}, DEFAULTTOREAL(socketpair));
// int getpeername(int  s,  struct  sockaddr  *name,  socklen_t *namelen);
MKSYMFX(getpeername, int, (int s, void*name, int*namelen), (s, name, namelen),
  FDFROMABI(s),
  NOPOSTPROCESSING,
  FOLLOWFD(s));

#ifdef PUKABI_HAVE___FXSTAT
/** @note Typical GLIBC *statically* define 'fstat' as an alias to '__fxstat'.
 * So we must override '__fxstat', not 'fstat'.
 */
// int __fxstat(int version, int fildes, struct stat *buf);
MKSYMFX(__fxstat, int, (int version, int fd, void*buf), (version, fd, buf),
  { FDFROMABI(fd); puk_abi_trace("PukABI: __fxstat() fd=%d", fd);  },
  { puk_abi_trace("PukABI: __fxstat()- rc=%d", result); } /* NOPOSTPROCESSING */,
  FOLLOWFD(fd));
// int __fxstat64(int version, int fildes, struct stat *buf);
MKSYMFX(__fxstat64, int, (int version, int fd, void*buf), (version, fd, buf),
  { FDFROMABI(fd); puk_abi_trace("PukABI: __fxstat64() fd=%d", fd);  },
  { puk_abi_trace("PukABI: __fxstat64()- rc=%d", result); } /* NOPOSTPROCESSING */,
  FOLLOWFD(fd));
#else /* HAVE___FXSTAT */
// int fstat(int fildes, struct stat *buf);
MKSYMFX(fstat, int, (int fd, void*buf), (fd, buf),
  FDFROMABI(fd),
  NOPOSTPROCESSING,
  FOLLOWFD(fd));
#endif /* HAVE___FXSTAT */
/** @note not strictly required to wrap 'unlink' but needed for YAMPI to avoid EINTR.
 */
// int unlink(const char *pathname);
MKSYMFX(unlink, int, (const char*pathname), (pathname),
  NOPREPROCESSING,
  NOPOSTPROCESSING,
  ALWAYSREAL);

/** @todo We assume the form:
 * ioctl(int fd, int req, unsigned long arg);
 * It is not a big issue, though, since it covers 99%
 * of ioctl typical usage.
 */
// int ioctl(int fildes, int request, /* arg */ ...);
MKSYMVX2(ioctl, int, (int fd, int req, ...), fd, req,
{
  va_list _ap;
  va_start(_ap, req);
  FDFROMABI(fd);
  unsigned long v = va_arg(_ap, unsigned long);
  puk_abi_trace("PukABI: ioctl()- fd=%d", fd);
  _result = (*_fun)(fd, req, v);
  va_end(_ap);
},
  FOLLOWFD(fd));

#ifdef PUKABI_HAVE_INOTIFY_H
// int inotify_init(void)
MKSYMFX(inotify_init,  int, (void), (),
  NOPREPROCESSING,
  VIRTUALIZEFD(result),
  DEFAULTTOREAL(socket));
// int inotify_add_watch(int fd, const char *pathname, uint32_t mask);
MKSYMFX(inotify_add_watch, int, (int fd, const char*pathname, __puk_abi_int32_t mask), (fd, pathname, mask),
  FDFROMABI(fd),
  NOPOSTPROCESSING,
  FOLLOWFD(fd));
// int inotify_rm_watch(int fd, uint32_t wd);
MKSYMFX(inotify_rm_watch, int, (int fd, __puk_abi_int32_t wd), (fd, wd),
  FDFROMABI(fd),
  NOPOSTPROCESSING,
  FOLLOWFD(fd));
#endif /* PUKABI_HAVE_INOTIFY_H */

MKSYMFX(dirfd, int, (void*dirp), (dirp),
  NOPREPROCESSING,
  VIRTUALIZEFD(result),
  DEFAULTTOREAL(dirfd));

MKSYMNOSUPPORT(sendfile);
MKSYMNOSUPPORT(sendfile64);

MKSYMNOSUPPORT(epoll_create);
MKSYMNOSUPPORT(epoll_ctl);
MKSYMNOSUPPORT(epoll_wait);

#endif /* PUKABI_ENABLE_FSYS */


#ifdef PUKABI_ENABLE_PROC

// int system(const char *string);
MKSYMFX(system, int, (const char*cmd), (cmd),
  NOPREPROCESSING,
  NOPOSTPROCESSING,
  DEFAULTTOREAL(system));

//int execve(const char *filename, char *const argv[], char *const envp[]);
MKSYMFX(execve, int, (const char*name, char*const argv[], char*const arge[]), (name, argv, arge),
  NOPREPROCESSING,
  NOPOSTPROCESSING,
  DEFAULTTOREAL(execve));

//int execv(const char *file, char *const argv[]);
MKSYMFX(execv, int, (const char *name, char *const argv[]), (name, argv),
  NOPREPROCESSING,
  NOPOSTPROCESSING,
  DEFAULTTOREAL(execv));

//int execvp(const char *file, char *const argv[]);
MKSYMFX(execvp, int, (const char *name, char *const argv[]), (name, argv),
  NOPREPROCESSING,
  NOPOSTPROCESSING,
  DEFAULTTOREAL(execvp));


// int execl(const char *path, const char *arg, ...);
MKSYMVX2(execl, int, (const char *path, const char *arg, ...), path, arg,
{
  va_list args;
  va_start (args, arg);
  int need_free = 0;
  const char**argv = puk_abi_helper_build_argv(arg, args, &need_free);
  va_end (args);
  const int ret = execv(path, (char *const *) argv);
  if(need_free)
    puk_abi_helper_free_argv(argv);
  return ret;
},
  DEFAULTTOREAL(execl));

// int execlp(const char *file, const char *arg, ...);
MKSYMVX2(execlp, int, (const char *file, const char *arg, ...), file, arg,
{
  va_list args;
  va_start (args, arg);
  int need_free = 0;
  const char**argv = puk_abi_helper_build_argv(arg, args, &need_free);
  va_end (args);
  const int ret = execvp(file, (char *const *) argv);
  if(need_free)
    puk_abi_helper_free_argv(argv);
  return ret;
},
  DEFAULTTOREAL(execlp));

// int execle(const char *path, const char *arg, ..., char * const envp[]);
MKSYMVX2(execle, int, (const char *path, const char *arg, ...), path, arg,
{
  va_list args;
  va_start (args, arg);
  int need_free = 0;
  const char**argv = puk_abi_helper_build_argv(arg, args, &need_free);
  const char *const *envp = va_arg(args, const char *const *);
  va_end (args);
  const int ret = execve(path, (char *const *) argv, (char *const *)envp);
  if(need_free)
    puk_abi_helper_free_argv(argv);
  return ret;
},
  DEFAULTTOREAL(execle));

#endif /* PUKABI_ENABLE_PROC */

#undef INITIAL_ARGV_MAX

//void usleep(unsigned long usec);
MKSYMPX(usleep, void, (unsigned long usec), (usec),
  NOPREPROCESSING,
  NOPOSTPROCESSING,
  DEFAULTTOREAL(usleep));

//unsigned int sleep(unsigned int seconds);
MKSYMFX(sleep, unsigned int, (unsigned int sec), (sec),
  NOPREPROCESSING,
  NOPOSTPROCESSING,
  DEFAULTTOREAL(sleep));

//int nanosleep(const struct timespec *req, struct timespec *rem)
MKSYMFX(nanosleep, int, (const void*req, void*rem), (req, rem),
  NOPREPROCESSING,
  NOPOSTPROCESSING,
  DEFAULTTOREAL(nanosleep));

MKSYMNOSUPPORT(ulimit);
MKSYMNOSUPPORT(valloc);
MKSYMNOSUPPORT(pvalloc);

#if (defined PUKABI_ENABLE_FSYS) && (defined PUKABI_HAVE_ATFILE_FUNCTIONS)

// int openat(int dirfd, const char *path, int oflag, /* mode_t mode */...);
MKSYMVX3(openat, int, (int dirfd, const char *path, int oflag, ...), dirfd, path, oflag,
{
  va_list _ap;
  FDFROMABI(dirfd);
  va_start(_ap, oflag);
  const int o = puk_abi_oracle__open_flags(oflag);
  if(o)
    {
      unsigned mode = va_arg(_ap, unsigned);
      puk_abi_trace("PukABI: openat()- dirfd=%i; path=%s; mode=%d", dirfd, path, (int)mode);
      _result = (*_fun)(dirfd, path, oflag, mode);
    }
  else
    {
      puk_abi_trace("PukABI: openat()- dirfd=%i; path=%s", dirfd, path);
      _result = (*_fun)(dirfd, path, oflag);
    }
  puk_abi_trace("PukABI: openat()- result=%d", _result);
  VIRTUALIZEFD(_result);
  va_end(_ap);
},
  DEFAULTTOREAL(openat));

// int faccessat(int dirfd, const char *path, int mode, int flags);
MKSYMFX(faccessat, int, (int dirfd, const char *path, int mode, int flags), (dirfd, path, mode, flags),
       {
         FDFROMABI(dirfd);
         puk_abi_trace("PukABI: faccessat()- dirfd=%i; path=%s; mode=%d; flags=%d", dirfd, path, mode, flags);
       },
       NOPOSTPROCESSING,
       DEFAULTTOREAL(faccessat));

#endif

#if (defined PUKABI_ENABLE_FSYS) && (defined PUKABI_HAVE_ATFILE64_FUNCTIONS)

// int openat64(int dirfd, const char *path, int oflag, /* mode_t mode */...);
MKSYMVX3(openat64, int, (int dirfd, const char *path, int oflag, ...), dirfd, path, oflag,
{
  va_list _ap;
  FDFROMABI(dirfd);
  va_start(_ap, oflag);
  const int o = puk_abi_oracle__open_flags(oflag);
  if(o)
    {
      unsigned mode = va_arg(_ap, unsigned);
      puk_abi_trace("PukABI: openat64()- dirfd=%i; path=%s; mode=%d", dirfd, path, (int)mode);
      _result = (*_fun)(dirfd, path, oflag, mode);
    }
  else
    {
      puk_abi_trace("PukABI: openat64()- dirfd=%i; path=%s", dirfd, path);
      _result = (*_fun)(dirfd, path, oflag);
    }
  puk_abi_trace("PukABI: openat64()- result=%d", _result);
  VIRTUALIZEFD(_result);
  va_end(_ap);
},
  DEFAULTTOREAL(openat64));

/* There's no `faccessat64()' as of Glibc 2.7.  */

#endif

//int backtrace(void **buffer, int size);
MKSYMFX(backtrace, int, (void**buffer, int size), (buffer, size),
  NOPREPROCESSING,
  NOPOSTPROCESSING,
  DEFAULTTOREAL(backtrace));

//void perror(const char *s)
MKSYMPX(perror, void, (const char *s), (s),
  NOPREPROCESSING,
  NOPOSTPROCESSING,
  DEFAULTTOREAL(perror));


#ifdef PUKABI_ENABLE_RESOLV

//name resolution functions
MKSYMFX(gethostbyname,  void*, (const char *name), (name),
        NOPREPROCESSING,
        NOPOSTPROCESSING,
        DEFAULTTOREAL(gethostbyname));

MKSYMFX(gethostbyname_r,  int, (const char *name,  void *ret, char *buf, unsigned long size, void **res, int *h_errnop),
        (name, ret, buf, size, res, h_errnop),
        NOPREPROCESSING,
        NOPOSTPROCESSING,
        DEFAULTTOREAL(gethostbyname_r));

MKSYMFX(gethostbyname2,  void*, (const char *name, int af), (name, af),
        NOPREPROCESSING,
        NOPOSTPROCESSING,
        DEFAULTTOREAL(gethostbyname2));

MKSYMFX(gethostbyname2_r,  int, (const char *name, int af,  void *ret, char *buf, unsigned long size, void **res, int *h_errnop),
        (name, af, ret, buf, size, res, h_errnop),
        NOPREPROCESSING,
        NOPOSTPROCESSING,
        DEFAULTTOREAL(gethostbyname2_r));

MKSYMFX(getaddrinfo,  int, (const char *node, const char *service,void *hints, void **res),
        (node, service, hints, res),
        NOPREPROCESSING,
        NOPOSTPROCESSING,
        DEFAULTTOREAL(getaddrinfo));

#endif /* PUKABI_ENABLE_RESOLV */

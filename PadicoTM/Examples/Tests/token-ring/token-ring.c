/* token-ring.c
 */
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <Padico/VIO.h>
#include <Padico/Timing.h>
#include <Padico/Module.h>
#include <Padico/Topology.h>
#include <Padico/PadicoControl.h>


int token_ring_init(void);
int token_ring_run(int argc, char**argv);
void token_ring_finalize(void);

PADICO_MODULE_DECLARE(token_ring, token_ring_init, token_ring_run, token_ring_finalize);

#define DEFAULT_PORT       5040

static int port = DEFAULT_PORT;

static struct sockaddr_in local_addr;
static socklen_t          local_addr_size  = sizeof(local_addr);
static struct sockaddr_in remote_addr;
static socklen_t          remote_addr_size = sizeof(remote_addr);
static int in_fd;
static int out_fd;
static int server_fd;
static int size;
static int rank;

padico_topo_node_t next;

int token_ring_init(void)
{
  size = padico_na_size();
  rank = padico_na_rank();
  const char*session = padico_topo_node_getsession(padico_topo_getlocalnode());

  next = padico_topo_session_getnodebyrank(session, (rank+1)%size);

  int rc = -1, err = -1;
  if(rank == 0)
    {
      /* 0 Node */
      // Then Connect
      out_fd = padico_vio_socket(PF_INET, SOCK_STREAM, 0);
      remote_addr.sin_addr   = *padico_topo_host_getaddr(padico_topo_node_gethost(next));
      remote_addr.sin_family = AF_INET;
      remote_addr.sin_port   = htons(port + (rank + 1) % size);
      do
        {
          padico_timing_t tick1, tick2;
          padico_print("TOKEN-RING: [node 0] waiting for the server to come up...\n");
          padico_tm_sleep(2);
          padico_print("TOKEN-RING: [node 0] connecting to server...\n");
          padico_timing_get_tick(&tick1);
          rc = padico_vio_connect(out_fd, (struct sockaddr*)&remote_addr, remote_addr_size);
          padico_timing_get_tick(&tick2);
          if(rc != 0)
            {
              err = padico_vio_errno;
              padico_warning("cannot connect- errno=%d (%s). Retrying...\n", err, strerror(err));
              padico_tm_sleep(1);
              /* TODO: c'est un peu bourrin d'attendre... */
            }
          else
            {
              padico_print("TOKEN-RING: [node 0] client ready- connect = %g usec.\n",
                           padico_timing_diff_usec(&tick1, &tick2));
              padico_print("TOKEN-RING: [node 0] client connected to %s:%d.\n",
                           inet_ntoa(remote_addr.sin_addr), ntohs(remote_addr.sin_port));
            }
        }
      while(rc != 0);
      // Accept conn
      server_fd = padico_vio_socket(PF_INET, SOCK_STREAM, 0);
      local_addr.sin_family = AF_INET;
      local_addr.sin_port   = htons(port);
      rc = padico_vio_bind(server_fd, (struct sockaddr*)&local_addr, local_addr_size);
      if(rc != 0)
        {
          padico_warning("TOKEN-RING: cannot bind on port %d\n", port);
          return -1;
        }
      rc = padico_vio_listen(server_fd, 5);
      if(rc != 0)
        {
          padico_warning("listen() error port:%d\n", port);
          return -1;
        }
      in_fd = padico_vio_accept(server_fd, (struct sockaddr*)&remote_addr, &remote_addr_size);
      if(in_fd < 0)
        {
          err = padico_vio_errno;
          padico_warning("TOKEN-RING: accept()- errno=%d (%s).\n", err, strerror(err));
          return -1;
        }
      padico_print("TOKEN-RING: [node 0] received connection from %s:%d.\n",
                   inet_ntoa(remote_addr.sin_addr), ntohs(remote_addr.sin_port));
      padico_vio_close(server_fd);
    }
  else
    {
      /* Others Nodes */
      server_fd = padico_vio_socket(PF_INET, SOCK_STREAM, 0);
      local_addr.sin_family = AF_INET;
      local_addr.sin_port   = htons(port + rank);
      rc = padico_vio_bind(server_fd, (struct sockaddr*)&local_addr, local_addr_size);
      if(rc != 0)
        {
          padico_warning("TOKEN-RING: cannot bind on port %d\n", port);
          return -1;
        }
      rc = padico_vio_listen(server_fd, 5);
      if(rc != 0)
        {
          padico_warning("listen() error port:%d\n", port);
          return -1;
        }
      padico_print("TOKEN-RING: [node %i] server ready\n", rank);
      in_fd = padico_vio_accept(server_fd, (struct sockaddr*)&remote_addr, &remote_addr_size);
      if(in_fd < 0)
        {
          err = padico_vio_errno;
          padico_warning("TOKEN-RING: accept()- errno=%d (%s).\n", err, strerror(err));
          return -1;
        }
      padico_print("TOKEN-RING: [node %i] received connection from %s:%d.\n",
                   rank, inet_ntoa(remote_addr.sin_addr), ntohs(remote_addr.sin_port));
      padico_vio_close(server_fd);
      out_fd = padico_vio_socket(PF_INET, SOCK_STREAM, 0);
      remote_addr.sin_addr   = *padico_topo_host_getaddr(padico_topo_node_gethost(next));
      remote_addr.sin_family = AF_INET;
      remote_addr.sin_port   = htons(port + (rank + 1) % size);
      do
        {
          padico_timing_t tick1, tick2;
          padico_print("TOKEN-RING: [node %i] waiting for the server to come up...\n", rank);
          padico_print("TOKEN-RING: [node %i] connecting to server...\n", rank);
          padico_timing_get_tick(&tick1);
          rc = padico_vio_connect(out_fd, (struct sockaddr*)&remote_addr, remote_addr_size);
          padico_timing_get_tick(&tick2);
          if(rc != 0)
            {
              err = padico_vio_errno;
              padico_warning("cannot connect- errno=%d (%s). Retrying...\n", err, strerror(err));
              padico_tm_sleep(1);
              /* TODO: c'est un peu bourrin d'attendre... */
            }
          else
            {
              padico_print("TOKEN-RING: [node %i] client ready- connect = %g usec.\n", rank,
                           padico_timing_diff_usec(&tick1, &tick2));
              padico_print("TOKEN-RING: [node %i] client connected to %s:%d.\n",
                           rank, inet_ntoa(remote_addr.sin_addr), ntohs(remote_addr.sin_port));
            }
        }
      while(rc != 0);
    }
  padico_print("TOKEN-RING: [node %i] All connections ready.\n", rank);

  return 0;
}

void token_ring_finalize(void)
{
  padico_vio_close(in_fd);
  padico_vio_close(out_fd);
}

int token_ring_run(int argc, char**argv)
{
  int rc;
  int token=1;
  if(rank == 0)
    {
      rc = padico_vio_write(out_fd, &token, sizeof(int));
      if(rc < 0)
        {
          padico_warning("write() error rc=%d\n", rc);
          return 1;
        }
      rc = padico_vio_read(in_fd, &token, sizeof(int));
      if(rc < 0)
        {
          padico_warning("write() error rc=%d\n", rc);
          return 1;
        }
      if(token!=size)
        {
          padico_warning("token:%d;  size:%d\n", token, size);
          return 1;
        }
      else
        printf("TEST_SUCCESS\n");
    }
  else{
    rc = padico_vio_read(in_fd, &token, sizeof(int));
    if(rc < 0)
      {
        padico_warning("write() error rc=%d\n", rc);
          return 1;
      }
    token++;
    rc = padico_vio_write(out_fd, &token, sizeof(int));
    if(rc < 0)
      {
        padico_warning("write() error rc=%d\n", rc);
        return 1;
      }
  }
  return 0;
}

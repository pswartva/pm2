/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <nm_launcher_interface.h>
#include <nm_sync_clocks_interface.h>


int main(int argc, char**argv)
{
  int rank, worldsize;
  static nm_session_t p_session = NULL;
  nm_gate_t gate;

  nm_launcher_init(&argc, argv);
  nm_session_open(&p_session, "nm_example");
  nm_launcher_get_rank(&rank);
  nm_launcher_get_size(&worldsize);

  nm_comm_t p_comm = nm_comm_world("sync_clocks_test");
  nm_sync_clocks_t p_clocks = nm_sync_clocks_init(p_comm);

  double* times = malloc(3 * worldsize * sizeof(double));

  puk_sleep(5);
  nm_coll_barrier(p_comm, 0);

  if(rank == 0) // server
    {
      for(int i = 1; i < worldsize; i++)
        {
          nm_launcher_get_gate(i, &gate);

          times[i*3] = nm_sync_clocks_get_time_usec(p_clocks);
          nm_sr_send(p_session, gate, 1, NULL, 0);
          nm_sr_recv(p_session, gate, 1, times + (i*3) + 1, sizeof(double));
          times[i*3 + 2] = nm_sync_clocks_get_time_usec(p_clocks);
        }

    }
  else
    {
      double t_mid;
      nm_launcher_get_gate(0, &gate);

      nm_sr_recv(p_session, gate, 1, NULL, 0);
      t_mid = nm_sync_clocks_get_time_usec(p_clocks);
      nm_sr_send(p_session, gate, 1, &t_mid, sizeof(t_mid));
    }

  /* ** test barrier */
  nm_sync_clocks_barrier(p_clocks, NULL);
  double t_barrier = nm_sync_clocks_get_time_usec(p_clocks);
  const double t_global_barrier =  nm_sync_clocks_local_to_global(p_clocks, t_barrier);
  double*t_barrier_nodes = malloc(worldsize * sizeof(double));
  nm_coll_gather(p_comm, 0, &t_barrier, sizeof(double), &t_barrier_nodes[0], sizeof(double), 0x42);

  nm_sync_clocks_synchronize(p_clocks);
  /* compare computed global time before & after re-synchronizing offset */
  const double t_global_barrier2 = nm_sync_clocks_local_to_global(p_clocks, t_barrier);
  fprintf(stderr, "[%d] global time after barrier = %.3lf (%.3lf); drift = %6.3lf\n",
          rank, t_global_barrier2, t_global_barrier, t_global_barrier2 - t_global_barrier);

  if (rank == 0)
    {
      for(int i = 1; i < worldsize; i++)
        {
          double shifted = nm_sync_clocks_remote_to_global(p_clocks, i, times[i*3+1]);
          printf("[%d]: %.3lf < %.3lf (%.3lf) < %.3lf\n", i, times[i*3], times[i*3+1], shifted, times[i*3+2]);

          /*printf("[%d]   t1 = %lu us\n", i, t1);
          printf("[%d] tmid = %lu us\n", i, mid);
          printf("[%d]   t2 = %lu us\n", i, t2);*/

          if(times[i*3] <= shifted && shifted <= times[i*3+2])
            {
              printf("[%d] ok: %lf\n", i, ((double) shifted - (double) times[i*3])/((double) times[i*3+2] - (double) times[i*3]));
            }
          else
            {
              printf("[%d] BAD\n", i);
            }
        }
    }

  if(rank == 0)
    {
      int i;
      for(i = 0; i < worldsize; i++)
        {
          printf("[%d] barrier error = %6.3lf usec.\n", i, t_barrier - nm_sync_clocks_remote_to_global(p_clocks, i, t_barrier_nodes[i]));
        }
    }

  nm_sync_clocks_shutdown(p_clocks);
  nm_launcher_exit();

  free(times);

  exit(0);
}

/*
 * NewMadeleine
 * Copyright (C) 2016-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


/** @file basic example for the RPC interface
 */

#include <stdlib.h>
#include <stdio.h>

#include <nm_launcher_interface.h>
#include <nm_coll_interface.h>
#include <nm_rpc_interface.h>

const char msg[] = "Hello, world!";
const nm_tag_t tag = 0x02;

/** handler called upon incoming RPC request */
static void rpc_hello_handler(nm_rpc_token_t p_token)
{
  nm_len_t rlen = NM_LEN_UNDEFINED;
  nm_rpc_recv_header(p_token, &rlen, sizeof(rlen));
  printf("rpc_hello_handler()- received header: len = %lu\n", rlen);

  char*buf = malloc(rlen);
  nm_rpc_token_set_ref(p_token, buf);
  nm_rpc_irecv_body(p_token, buf, rlen);
}

/** handler called after all data for the incoming RPC request was received */
static void rpc_hello_finalizer(nm_rpc_token_t p_token)
{
  char*buf = nm_rpc_token_get_ref(p_token);
  printf("received body: %s\n", buf);
  free(buf);
}

int main(int argc, char**argv)
{
  /* generic nmad init */
  nm_launcher_init(&argc, argv);
  nm_session_t p_session = NULL;
  nm_session_open(&p_session, "nm_rpc_hello");
  int size = 0;
  nm_launcher_get_size(&size);
  if(size != 2)
    {
      NM_FATAL("This example works only with two processes.\n");
    }
  int rank = -1;
  nm_launcher_get_rank(&rank);
  int peer = (size > 1) ? (1 - rank) : rank;
  nm_gate_t p_gate = NULL;
  nm_launcher_get_gate(peer, &p_gate);

  nm_comm_t p_comm = nm_comm_world("nm_rpc_hello");

  /* register the RPC service */
  nm_rpc_service_t p_service = nm_rpc_register(p_session, tag, NM_TAG_MASK_FULL,
                                               &rpc_hello_handler, &rpc_hello_finalizer, NULL);

  nm_len_t len = strlen(msg) + 1; /* header */
  /* create rpc request */
  nm_rpc_req_t p_req = nm_rpc_req_init(p_service, p_gate, tag);
  nm_rpc_req_pack_header(p_req, &len, sizeof(len));
  nm_rpc_req_pack_body(p_req, msg, len);
  nm_rpc_req_isend(p_req);
  nm_rpc_req_wait(p_req);

  /* generic nmad termination */
  nm_coll_barrier(p_comm, 0xF2);
  nm_rpc_unregister(p_service);
  nm_comm_destroy(p_comm);
  nm_session_close(p_session);
  nm_launcher_exit();
  return 0;

}

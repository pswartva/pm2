/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief VLink packet filtering framework
 */

#include <Padico/VLink-internals.h>
#include <Padico/NetSelector.h>
#include <Padico/Module.h>
#include <Padico/PacketFilter.h>



/** @defgroup VLink_PacketFilter component: VLink_PacketFilter- VLink over VLink with pluggable packet filtering
 * @ingroup PadicoComponent
 */

static void vpf_init        (padico_vnode_t);
static int  vpf_create      (padico_vnode_t);
static int  vpf_bind        (padico_vnode_t);
static int  vpf_listen      (padico_vnode_t);
static int  vpf_connect     (padico_vnode_t);
static void vpf_fullshutdown(padico_vnode_t);
static void vpf_send_post   (padico_vnode_t);
static void vpf_recv_post   (padico_vnode_t);


/** 'VLink' facet for VLink_PacketFilter
 * @ingroup VLink_PacketFilter
 */
static const struct padico_vlink_driver_s vpf_driver =
{
  .create       = &vpf_create,
  .bind         = &vpf_bind,
  .listen       = &vpf_listen,
  .connect      = &vpf_connect,
  .fullshutdown = &vpf_fullshutdown,
  .close        = NULL,
  .send_post    = &vpf_send_post,
  .recv_post    = &vpf_recv_post,
  .dumpstat     = NULL
};

enum vpf_status_e
  {
    VPF_NO_DATA = 0,
    VPF_HEADER,
    VPF_DATA,
    VPF_COMPLETED,
    VPF_FRAGMENT
  };

struct vpf_header_s
{
  uint32_t encoded_size; /**< size of data "on the wire" */
  uint32_t decoded_size; /**< original size of data */
};

/** VLink_PacketFilter instance status
 * @ingroup VLink_PacketFilter
 */
struct vlink_vpf_specific_s
{
  padico_vnode_t sub_vnode;
  const struct packetfilter_driver_s*encoder;
  void*encoder_state;
  struct
  {
    enum vpf_status_e stage;
    void*data;
    size_t data_size;
    size_t encoded_size;
    struct vpf_header_s packed_header; /**< packed header, i.e. in network byte order */

  } out;
  struct
  {
    enum vpf_status_e stage;
    void*data;
    size_t decoded_size; /**< in host by order */
    size_t encoded_size; /**< in host by order */
    struct vpf_header_s packed_header; /**< packed header, i.e. in network byte order */
    size_t pending;
  } in;
};

VLINK_CREATE_SPECIFIC_ACCESSORS(vpf);

static void vpf_notifier(padico_vnode_t sub, vobs_attr_t attr, void*key);
static void vpf_commit(padico_vnode_t vnode, vobs_attr_t attr);
static void vpf_make_new_vnode(padico_vnode_t vnode, padico_vnode_t sub);



/* ********************************************************* */

PADICO_MODULE_COMPONENT(VLink_PacketFilter,
  puk_component_declare("VLink_PacketFilter",
                        puk_component_uses("VLink", "vlink"),
                        puk_component_uses("PacketFilter", "pf"),
                        puk_component_provides("PadicoComponent", "component", &padico_vnode_component_driver),
                        puk_component_provides("VLink", "vlink", &vpf_driver)));

/* ********************************************************* */

static inline void vpf_copy_status(padico_vnode_t vnode)
{
  padico_vnode_t sub = vobs_vpf(vnode)->sub_vnode;
  vnode->status = sub->status;
  padico_out(40, "vnode=%p; error=%d; readable=%d; writable=%d; opened=%d; connecting=%d; EOF=%d\n",
             vnode, vnode->status.error, vnode->status.readable, vnode->status.writable,
             vnode->status.opened, vnode->status.connecting, vnode->status.end_of_file);
}

static void vpf_make_new_vnode(padico_vnode_t vnode, padico_vnode_t sub)
{
  if(!padico_vnode_queue_empty(sub->listener.backlog))
    {
      puk_instance_t self_instance = padico_vnode_get_self_instance(vnode);
      padico_vnode_t new_sub       = padico_vnode_queue_retrieve(sub->listener.backlog);
      puk_instance_t new_container = puk_instance_get_root_container(padico_vnode_get_self_instance(new_sub));
      puk_instance_t new_instance  = puk_instance_find_context(new_container, self_instance);
      padico_vnode_t new_vpf       = puk_instance_self_status(new_instance);
      vpf_init(new_vpf);
      padico_vsock_init(new_vpf,
                        vnode->socket.so_family,
                        vnode->socket.so_type,
                        vnode->socket.so_protocol);

      /* copy status and local binding */
      new_vpf->status  = new_sub->status;
      vnode_activate(new_vpf, binding);
      new_vpf->binding = new_sub->binding;
      vobs_vpf(new_vpf)->in.stage = VPF_NO_DATA;
      /* remote binding, enqueue and signal */
      padico_vsock_acceptor_finalize(new_vpf, vnode,
                                     new_sub->remote.remote_addr,
                                     new_sub->remote.remote_addrlen);
    }
  else
    {
      padico_warning("Empty backlog in VLink_ID notifier\n");
    }
}

static void vpf_commit(padico_vnode_t vnode, vobs_attr_t attr)
{
  padico_vnode_t sub = vobs_vpf(vnode)->sub_vnode;
  padico_out(40, "vnode=%p; sub=%p; attr=%d (%s)\n",
             vnode, sub, attr, vobs_attr_label[attr]);
  switch(attr)
    {
    case vobs_attr_rbox:
      assert(!vnode->content.rbox);
      assert(sub->rbox.cant_interrupt_read == 0);
      sub->content.rbox = vnode->content.rbox;
      sub->rbox = vnode->rbox;
      break;
    case vobs_attr_wbox:
      assert(!vnode->content.wbox);
      sub->content.wbox = vnode->content.wbox;
      sub->wbox = vnode->wbox;
      break;
    default:
      padico_fatal("Unexpected attribute in vpf_commit\n");
      break;
    }
  vobs_commit(sub, attr);
}

static void vpf_notifier(padico_vnode_t sub, vobs_attr_t attr, void*key)
{
  padico_vnode_t vnode = (padico_vnode_t)key;
  assert(vobs_vpf(vnode)->sub_vnode == sub);
  padico_out(10, "vnode=%p; sub=%p; attr=%d (%s)\n",
             vnode, sub, attr, vobs_attr_label[attr]);
  switch(attr)
    {
    case vobs_attr_status:
      vpf_copy_status(vnode);
      vobs_signal(vnode, attr);
      break;
    case vobs_attr_remote:
      {
        /* new connection as client:
         * copy status, remote and binding from sub_vnode
         */
        vpf_copy_status(vnode);
        vnode->content.remote = sub->content.remote;
        vnode->remote = sub->remote;
        vnode->content.binding = sub->content.binding;
        vnode->binding = sub->binding;
        padico_out(30, "vnode=%p; sub=%p vnode.established=%d sub.established=%d\n",
                   vnode, sub, vnode->remote.established, sub->remote.established);
        vobs_signal(vnode, attr);
      }
      break;
    case vobs_attr_listener:
      {
        /* new connection as server:
         * create a new stack of vnodes
         */
        vpf_copy_status(vnode);
        vpf_make_new_vnode(vnode, sub);
        padico_out(30, "vnode=%p; sub=%p; %d connection(s) in backlog\n",
                   vnode, sub, padico_vnode_queue_size(vnode->listener.backlog));
      }
      break;
    case vobs_attr_rbox:
      if(sub->content.rbox && sub->rbox.read_done == sub->rbox.read_posted)
        {
          switch(vobs_vpf(vnode)->in.stage)
            {
            case VPF_HEADER:
              {
                /* header completed- recv data */
                size_t decoded_size =
                  ntohl(vobs_vpf(vnode)->in.packed_header.decoded_size);
                size_t encoded_size =
                  ntohl(vobs_vpf(vnode)->in.packed_header.encoded_size);
                padico_out(10, "received header- decoded=%ld; encoded=%ld\n",
                           puk_ulong(decoded_size), puk_ulong(encoded_size));
                vobs_vpf(vnode)->in.data = padico_malloc(encoded_size);
                vobs_vpf(vnode)->in.stage = VPF_DATA;
                vobs_vpf(vnode)->in.decoded_size = decoded_size;
                vobs_vpf(vnode)->in.encoded_size = encoded_size;
                assert(!sub->rbox.cant_interrupt_read);
                padico_vnode_read_clear(sub);
                assert(encoded_size > 0);
                padico_vnode_read_post(sub,
                                       vobs_vpf(vnode)->in.data,
                                       encoded_size);
              }
              break;
            case VPF_DATA:
              {
                if(vobs_vpf(vnode)->in.decoded_size <= vnode->rbox.read_posted)
                  {
                    /* decode in place */
                    padico_out(10, "decode in place- decoded_size=%ld; read_posted=%ld\n",
                               puk_ulong(vobs_vpf(vnode)->in.decoded_size),
                               puk_ulong(vnode->rbox.read_posted));
                    vnode->rbox.read_done = vobs_vpf(vnode)->in.decoded_size;
                    (*(vobs_vpf(vnode)->encoder->pf_decode))
                      (vobs_vpf(vnode)->encoder_state,
                       vobs_vpf(vnode)->in.data,
                       vobs_vpf(vnode)->in.encoded_size,
                       vnode->rbox.read_buffer,
                       &vnode->rbox.read_done);
                    padico_free(vobs_vpf(vnode)->in.data);
                    vobs_vpf(vnode)->in.data    = NULL;
                    vobs_vpf(vnode)->in.stage   = VPF_COMPLETED;
                    vobs_vpf(vnode)->in.pending = 0;
                    assert(!sub->rbox.cant_interrupt_read);
                    padico_vnode_read_clear(sub);
                  }
                else
                  {
                    /* decode in buffer and store the remaining fragment */
                    void*buffer_copy = padico_malloc(vobs_vpf(vnode)->in.decoded_size);
                    padico_out(10, "decode in buffer- decoded_size=%ld; read_posted=%ld\n",
                               puk_ulong(vobs_vpf(vnode)->in.decoded_size),
                               puk_slong(vnode->rbox.read_posted));
                    (*vobs_vpf(vnode)->encoder->pf_decode)
                      (vobs_vpf(vnode)->encoder_state,
                       vobs_vpf(vnode)->in.data,
                       vobs_vpf(vnode)->in.encoded_size,
                       buffer_copy,
                       &vobs_vpf(vnode)->in.decoded_size);
                    padico_free(vobs_vpf(vnode)->in.data);
                    vobs_vpf(vnode)->in.data    = buffer_copy;
                    vobs_vpf(vnode)->in.stage   = VPF_FRAGMENT;
                    vobs_vpf(vnode)->in.pending =
                      vobs_vpf(vnode)->in.decoded_size - vnode->rbox.read_posted;
                    memcpy(vnode->rbox.read_buffer,
                           vobs_vpf(vnode)->in.data,
                           vnode->rbox.read_posted);
                    vnode->rbox.read_done = vnode->rbox.read_posted;
                    assert(!sub->rbox.cant_interrupt_read);
                    padico_vnode_read_clear(sub);
                  }
              }
              vnode->rbox.cant_interrupt_read = 0;
              break;
            default:
              padico_warning("rbox notifier with no data! stage=%d\n", vobs_vpf(vnode)->in.stage);
              break;
            }
        }
      else if(sub->content.rbox && sub->rbox.read_done > 0)
        {
          /* incomplete read- repost */
          const int posted = sub->rbox.read_posted;
          const int done = sub->rbox.read_done;
          void*buffer = sub->rbox.read_buffer;
          assert(vnode->content.rbox);
          assert(!sub->rbox.cant_interrupt_read);
          padico_out(10, "rbox signal with incomplete sub->rbox; reposting- buffer=%p; posted=%d; done=%d\n",
                     buffer, posted, done);
          padico_vnode_read_clear(sub);
          assert(done < posted);
          padico_vnode_read_post(sub, buffer + done, posted - done);
        }
      else
        {
          /* rbox signal with rbox.read_done == 0- ignore */
          padico_warning("rbox signal with no rbox- content.rbox=%d; sub->read_done=%lu\n",
                         vnode->content.rbox, (unsigned long)sub->rbox.read_done);
        }
      vobs_signal(vnode, attr);
      break;
    case vobs_attr_wbox:
      if(sub->wbox.write_done == sub->wbox.write_posted)
        {
          if(vobs_vpf(vnode)->out.stage == VPF_HEADER)
            {
              /* header completed- send data */
              padico_out(10, "wbox signal- header completed\n");
              vobs_vpf(vnode)->out.stage = VPF_DATA;
              padico_vnode_write_clear(sub);
              padico_vnode_write_post(sub,
                                      vobs_vpf(vnode)->out.data,
                                      vobs_vpf(vnode)->out.encoded_size);
              vpf_copy_status(vnode);

            }
          else
            {
              /* data completed */
              padico_out(10, "wbox signal- data completed\n");
              padico_free(vobs_vpf(vnode)->out.data);
              vobs_vpf(vnode)->out.data = NULL;
              vobs_vpf(vnode)->out.stage = VPF_COMPLETED;
              vnode->wbox.write_done = vnode->wbox.write_posted;
            }
        }
      else
        {
          /* partial write */ /* XXX TODO */
          padico_warning("partial read on wbox signal- posted=%lu; done=%lu\n",
                         (unsigned long)sub->wbox.write_posted, (unsigned long)sub->wbox.write_done);
        }
      vobs_signal(vnode, attr);
      break;
    default:
      padico_fatal("Unexpected attribute %d in VLink_ID notifier.\n", (int)attr);
      break;
    }
  padico_out(40, "vnode=%p; sub=%p; attr=%d (%s) done.\n",
             vnode, sub, attr, vobs_attr_label[attr]);
}


/* ********************************************************* */

static void vpf_init(padico_vnode_t vnode)
{
  const puk_instance_t instance = padico_vnode_get_self_instance(vnode);
  vobs_vpf_activate(vnode);
  vobs_vpf(vnode)->out.stage = VPF_NO_DATA;
  vobs_vpf(vnode)->in.stage  = VPF_NO_DATA;
  struct puk_receptacle_VLink_s vlink;
  puk_context_indirect_VLink(instance, "vlink", &vlink);
  vobs_vpf(vnode)->sub_vnode = vlink._status;
  struct puk_receptacle_PacketFilter_s pf;
  puk_context_indirect_PacketFilter(instance, "pf", & pf);
  vobs_vpf(vnode)->encoder = pf.driver;
  vobs_vpf(vnode)->encoder_state = pf._status;
  vobs_subscribe(vobs_vpf(vnode)->sub_vnode, &vpf_notifier, vnode);
  vobs_set_commit(vnode, &vpf_commit);
}

/* ********************************************************* */

static int vpf_create(padico_vnode_t vnode)
{
  vpf_init(vnode);
  int rc = padico_vsock_create(vobs_vpf(vnode)->sub_vnode,
                               vnode->socket.so_family,
                               vnode->socket.so_type,
                               vnode->socket.so_protocol);
  return rc;
}

static int vpf_bind(padico_vnode_t vnode)
{
  int rc = -1;
  assert(vnode->binding.autobind);
  vobs_vpf(vnode)->sub_vnode->binding = vnode->binding;
  rc = padico_vsock_autobind(vobs_vpf(vnode)->sub_vnode);
  vnode->binding = vobs_vpf(vnode)->sub_vnode->binding;
  return rc;
}

static int vpf_listen(padico_vnode_t vnode)
{
  int rc = -1;
  rc = padico_vsock_passive_connect(vobs_vpf(vnode)->sub_vnode,
                                    vnode->listener.backlog_size, NULL);
  vpf_copy_status(vnode);
  return rc;
}

static int vpf_connect(padico_vnode_t vnode)
{
  int rc = -1;
  rc = padico_vsock_active_connect(vobs_vpf(vnode)->sub_vnode,
                                   vnode->remote.remote_addr,
                                   vnode->remote.remote_addrlen);
  vpf_copy_status(vnode);
  return rc;
}

static void vpf_fullshutdown(padico_vnode_t vnode)
{
  padico_vsock_fullshutdown(vobs_vpf(vnode)->sub_vnode);
  vpf_copy_status(vnode);
}

static void vpf_send_post(padico_vnode_t vnode)
{
  int rc = -1;
  (*(vobs_vpf(vnode)->encoder->pf_encode))
    (vobs_vpf(vnode)->encoder_state,
     vnode->wbox.write_buffer,
     vnode->wbox.write_posted,
     &vobs_vpf(vnode)->out.data,
     &vobs_vpf(vnode)->out.encoded_size);
  vobs_vpf(vnode)->out.data_size = vnode->wbox.write_posted;
  vobs_vpf(vnode)->out.stage = VPF_HEADER;
  vobs_vpf(vnode)->out.packed_header.encoded_size =
    htonl((uint32_t)vobs_vpf(vnode)->out.encoded_size);
  vobs_vpf(vnode)->out.packed_header.decoded_size =
    htonl((uint32_t)vobs_vpf(vnode)->out.data_size);
  /* send header */
  rc = padico_vnode_write_post(vobs_vpf(vnode)->sub_vnode,
                               &vobs_vpf(vnode)->out.packed_header,
                               sizeof(vobs_vpf(vnode)->out.packed_header));
  vpf_copy_status(vnode);
  if(rc < 0)
    {
      vnode_set_error(vnode, EIO);
    }
}

static void vpf_recv_post(padico_vnode_t vnode)
{
  int rc;
  padico_out(20, "recv_post- posted=%ld\n", puk_slong(vnode->rbox.read_posted));
  switch(vobs_vpf(vnode)->in.stage)
    {
    case VPF_FRAGMENT:
      {
        size_t pending = vobs_vpf(vnode)->in.pending;
        if(pending <= vnode->rbox.read_posted)
          {
            /* take the whole pending fragment */
            memcpy(vnode->rbox.read_buffer,
                   vobs_vpf(vnode)->in.data + (vobs_vpf(vnode)->in.decoded_size - pending),
                   pending);
            vnode->rbox.read_done = pending;
            vobs_vpf(vnode)->in.stage = VPF_COMPLETED;
            padico_free(vobs_vpf(vnode)->in.data);
            vobs_vpf(vnode)->in.data = NULL;
            vobs_vpf(vnode)->in.pending = 0;
          }
        else
          {
            /* take a part of the pending fragment */
            size_t todo = vnode->rbox.read_posted;
            memcpy(vnode->rbox.read_buffer,
                   vobs_vpf(vnode)->in.data + (vobs_vpf(vnode)->in.decoded_size - pending),
                   todo);
            vnode->rbox.read_done = todo;
            vobs_vpf(vnode)->in.pending -= todo;
          }
        vobs_signal(vnode, vobs_attr_rbox);
      }
      break;

    case VPF_COMPLETED:
    case VPF_NO_DATA:
      vobs_vpf(vnode)->in.stage = VPF_HEADER;
      rc = padico_vnode_read_post(vobs_vpf(vnode)->sub_vnode,
                                  &vobs_vpf(vnode)->in.packed_header,
                                  sizeof(vobs_vpf(vnode)->in.packed_header));
      vpf_copy_status(vnode);
      if(rc == -1)
        {
          vnode_set_error(vnode, EIO);
          vobs_vpf(vnode)->in.stage = VPF_NO_DATA;
          return;
        }
      if(vobs_vpf(vnode)->in.stage == VPF_COMPLETED)
        {
          vobs_vpf(vnode)->in.stage = VPF_NO_DATA;
        }
      else
        {
          vnode->rbox.cant_interrupt_read = 1;
        }
      break;
    default:
      padico_fatal("bad value for vpf stage- stage=%d\n", vobs_vpf(vnode)->in.stage);
      break;
    }
}

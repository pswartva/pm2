/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

/** @file
 * launcher for NewMad using slurm PMI2/PMIx, using either padico-launch or srun
 * @note PMIx version is automatically generated from the PMI2 version.
 */

#include <nm_private.h>
#include <nm_launcher.h>
#include <nm_core_interface.h>
#include "nm_private_config.h"

#include <nm_madico_launcher.h>

#include <Padico/Module.h>
#include <Padico/Puk.h>
#include <Padico/Topology.h>
#include <Padico/NetSelector.h>
#include <Padico/PadicoControl.h>
#include <Padico/PMI2.h>

/** An instance of NewMad_Launcher_madico
 */
struct nm_pmi2_launcher_status_s
{
  const char*local_url;   /**< nmad url for local node */
  struct nm_launcher_gates_s gates;
};

/* ********************************************************* */

static void*nm_pmi2_launcher_instantiate(puk_instance_t, puk_context_t);
static void nm_pmi2_launcher_destroy(void*_status);

static const struct puk_component_driver_s nm_pmi2_launcher_component =
  {
    .instantiate = &nm_pmi2_launcher_instantiate,
    .destroy     = &nm_pmi2_launcher_destroy
  };

static void nm_pmi2_launcher_init(void*_status, int*argc, char**argv, const char*group_name);
static void nm_pmi2_launcher_barrier(void*_status);
static void nm_pmi2_launcher_get_gates(void*_status, nm_gate_t*gates);
static void nm_pmi2_launcher_abort(void*_status, int rc);

static const struct nm_launcher_driver_s nm_pmi2_launcher_driver =
  {
    .init        = &nm_pmi2_launcher_init,
    .barrier     = &nm_pmi2_launcher_barrier,
    .get_gates   = &nm_pmi2_launcher_get_gates,
    .abort       = &nm_pmi2_launcher_abort
  };

PADICO_MODULE_COMPONENT(NewMad_Launcher_pmi2,
  puk_component_declare("NewMad_Launcher_pmi2",
                        puk_component_provides("PadicoComponent", "component",  &nm_pmi2_launcher_component),
                        puk_component_provides("NewMad_Launcher", "launcher", &nm_pmi2_launcher_driver ))
                        );
#ifdef NMAD_PROFILE
#define NM_LAUNCHER_PROFILE 1
#else
#define NM_LAUNCHER_PROFILE 0
#endif

PADICO_MODULE_ATTR(profile, "NMAD_PMI2_LAUNCHER_PROFILE", "profile PMI2 launcher.", bool, NM_LAUNCHER_PROFILE);

static double prof_init = 0.0, prof_publish_topo = 0.0,
  prof_receive_topo = 0.0, prof_select_drv = 0.0,
  prof_exchange_urls = 0.0, prof_connected = 0.0, prof_destroy = 0.0;

/* ********************************************************* */


static void*nm_pmi2_launcher_instantiate(puk_instance_t instance, puk_context_t context)
{
  struct nm_pmi2_launcher_status_s*status = padico_malloc(sizeof(struct nm_pmi2_launcher_status_s));
  /* ** init PMI2 */
  int size = padico_pmi2_getsize();
  int rank = padico_pmi2_getrank();
  NM_DISPF("size = %d; rank = %d\n", size, rank);
  /* ** init PadicoTM default attributes */
  if( (getenv("PADICO_NETSELECTOR_inet_0") != NULL) ||
      (getenv("PADICO_NETSELECTOR_default_0") != NULL) ||
      (getenv("PADICO_NETSELECTOR_trk_small_0") != NULL) ||
      (getenv("PADICO_NETSELECTOR_trk_large_0") != NULL) )
    {
      NM_DISPF("using user-provided NetSelector config.\n");
    }
  else
    {
      padico_setenv("PADICO_NETSELECTOR_inet_0", "NetSelector-inet-default");
      padico_setenv("PADICO_NETSELECTOR_default_0", "NetSelector-besteffort");
      padico_setenv("PADICO_NETSELECTOR_trk_small_0", "NetSelector-besteffort");
      padico_setenv("PADICO_NETSELECTOR_trk_large_0", "NetSelector-besteffort");
    }

  /* ** init state */
  nm_launcher_gates_init(&status->gates, size, rank);
  return status;
}

static void nm_pmi2_launcher_destroy(void*_status)
{
  struct nm_pmi2_launcher_status_s*status = _status;
  nm_launcher_gates_destroy(&status->gates);
  padico_free(_status);
  prof_destroy = puk_timing_stamp();
}

/* ********************************************************* */


static void nm_pmi2_launcher_init(void*_status, int*argc, char**argv, const char*group_name)
{
  struct nm_pmi2_launcher_status_s*status = _status;

  if(padico_module_attr_profile_getvalue())
  {
    puk_profile_var_def(double, timer, &prof_init, "nm_pmi2_launcher", "nm_pmi2_launcher.init",
                        "time stamp at launcher init (in usecs.).");
    puk_profile_var_def(double, timer, &prof_publish_topo, "nm_pmi2_launcher", "nm_pmi2_launcher.publish_topo",
                        "time stamp after topology has been published (in usecs.).");
    puk_profile_var_def(double, timer, &prof_receive_topo, "nm_pmi2_launcher", "nm_pmi2_launcher.receive_topo",
                        "time stamp after tpology has been received (in usecs.).");
    puk_profile_var_def(double, timer, &prof_select_drv, "nm_pmi2_launcher", "nm_pmi2_launcher.select_drv",
                        "time stamp after drivers have been selected (in usecs.).");
    puk_profile_var_def(double, timer, &prof_exchange_urls, "nm_pmi2_launcher", "nm_pmi2_launcher.exchange_url",
                        "time stamp after urls have been exchanged (in usecs.).");
    puk_profile_var_def(double, timer, &prof_connected, "nm_pmi2_launcher", "nm_pmi2_launcher.connected",
                        "time stamp once everything is connected (in usecs.).");
    puk_profile_var_def(double, timer, &prof_destroy, "nm_pmi2_launcher", "nm_pmi2_launcher.destroy",
                        "time stamp after the launcher has been destroyed (in usecs.).");
  }

  prof_init = puk_timing_stamp();
  const int size = status->gates.size;
  const int rank = status->gates.rank;
  const struct nm_launcher_info_s info =
    {
      .size = size,
      .rank = rank,
      .wide_url_support = PADICO_PMI2_WIDE_URL_SUPPORT
    };
  nm_launcher_set_info(&info);

  /* ** publish local topology (node & host) */
  padico_string_t s_key = padico_string_new();
  padico_string_printf(s_key, "nm_node_%d", rank);
  padico_string_t s_localnode = padico_topo_node_serialize(padico_topo_getlocalnode());
  padico_pmi2_kvs_publish(padico_string_get(s_key), padico_string_get(s_localnode));
  padico_string_printf(s_key, "nm_host_%d", rank);
  padico_string_t s_localhost = padico_topo_host_serialize(padico_topo_getlocalhost());
  padico_pmi2_kvs_publish(padico_string_get(s_key), padico_string_get(s_localhost));
  padico_pmi2_kvs_fence();
  padico_string_delete(s_localnode);
  padico_string_delete(s_localhost);

  prof_publish_topo = puk_timing_stamp();

  padico_topo_node_t*peers = padico_malloc(size * sizeof(padico_topo_node_t));
  int i;
  for(i = 0; i < size; i++)
    {
      /* ** receive remote topology (node & host) */
      padico_string_printf(s_key, "nm_node_%d", i);
      char*remote_topo_node = padico_pmi2_kvs_lookup(padico_string_get(s_key), i);
      padico_control_event_disable();
      struct puk_parse_entity_s e = puk_xml_parse_buffer(remote_topo_node, strlen(remote_topo_node), PUK_TRUST_CLUSTER);
      padico_rc_t rc2 = puk_parse_get_rc(&e);
      if(padico_rc_iserror(rc2))
        {
          padico_rc_show(rc2);
        }
      peers[i] = puk_parse_get_content(&e);
      padico_control_event_enable();

      padico_string_printf(s_key, "nm_host_%d", i);
      char*remote_topo_host = padico_pmi2_kvs_lookup(padico_string_get(s_key), i);
      padico_control_event_disable();
      e = puk_xml_parse_buffer(remote_topo_host, strlen(remote_topo_host), PUK_TRUST_CLUSTER);
      rc2 = puk_parse_get_rc(&e);
      if(padico_rc_iserror(rc2))
        {
          padico_rc_show(rc2);
        }
      padico_control_event_enable();

      padico_free(remote_topo_node);
      padico_free(remote_topo_host);
    }

  prof_receive_topo = puk_timing_stamp();

  nm_launcher_gates_populate_ns(&status->gates, peers);
  padico_free(peers);

  prof_select_drv = puk_timing_stamp();

  int rc = nm_launcher_get_url(&status->local_url);
  if(rc != NM_ESUCCESS)
    {
      NM_FATAL("nm_launcher_get_url_init()- rc = %d\n", rc);
    }
  NM_DISPF("nm_launcher_get_url done- url = %s; size = %d; rank = %d\n",
           status->local_url, size, rank);

  /* ** publish local url */
  padico_string_printf(s_key, "nm_launcher_url_%d", rank);
  padico_pmi2_kvs_publish(padico_string_get(s_key), status->local_url);
  padico_pmi2_kvs_fence();

  /* ** resolve remote urls */
  for(i = 0; i < size; i++)
    {
      if(i == rank)
        {
          status->gates.p_gates[i].p_url = padico_strdup(status->local_url);
        }
      else
        {
          padico_string_printf(s_key, "nm_launcher_url_%d", i);
          status->gates.p_gates[i].p_url = padico_pmi2_kvs_lookup(padico_string_get(s_key), i);
          assert(strcmp(status->gates.p_gates[i].p_url, status->local_url) != 0);
        }
    }

  prof_exchange_urls = puk_timing_stamp();

  /* connect gates */
  nm_launcher_gates_connect(&status->gates);
  padico_string_delete(s_key);

  prof_connected = puk_timing_stamp();
}

static void nm_pmi2_launcher_barrier(void*_status)
{
  padico_pmi2_barrier();
}

static void nm_pmi2_launcher_get_gates(void*_status, nm_gate_t*gates)
{
  struct nm_pmi2_launcher_status_s*status = _status;
  int i;
  for(i = 0; i < status->gates.size; i++)
    {
      gates[i] = status->gates.p_gates[i].p_gate;
      padico_out(50, "get_gates()- i = %d; gate = %p\n", i, gates[i]);
    }
}

static void nm_pmi2_launcher_abort(void*_status, int rc)
{
  padico_pmi2_abort("NewMad_Launcher_pmi2: abort");
  exit(rc);
}

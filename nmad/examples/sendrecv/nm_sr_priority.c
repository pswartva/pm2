/*
 * NewMadeleine
 * Copyright (C) 2006-2023 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>

#include "../common/nm_examples_helper.h"
#include <Padico/Puk.h>

#define MSG_NUM 1000
#define MAX_PRIO 250
#define MSG_BINS 100
#define MAX_MSG_SIZE (1 * 1024 * 1024L)

#define SINGLE_RECV

int main(int argc, char**argv)
{
  nm_examples_init(&argc, argv);

  void*buffer = malloc(MSG_NUM * MAX_MSG_SIZE);
  nm_sr_request_t*requests = malloc(MSG_NUM * sizeof(nm_sr_request_t));
  puk_tick_t t1, t2;
  PUK_GET_TICK(t1);
  if(is_server)
    {
      int i;
#ifndef SINGLE_RECV
      printf("# posting receive requests...\n");
      for(i = 0; i < MSG_NUM; i++)
        {
          int*value = buffer + i * MAX_MSG_SIZE;
          nm_sr_recv_init(p_session, &requests[i]);
          nm_sr_recv_unpack_contiguous(p_session, &requests[i], value, MAX_MSG_SIZE);
          nm_sr_recv_irecv(p_session, &requests[i], p_gate, 0, NM_TAG_MASK_NONE); /* any tag */
        }
      printf("# waiting for receive completion...\n");
      for(i = 0; i < MSG_NUM; i++)
        {
          int*value = buffer + i * MAX_MSG_SIZE;
          nm_sr_rwait(p_session, &requests[i]);
          nm_tag_t tag = nm_sr_request_get_tag(&requests[i]);
          nm_len_t len = NM_LEN_UNDEFINED;
          nm_sr_request_get_size(&requests[i], &len);
          PUK_GET_TICK(t2);
          const double delay = PUK_TIMING_DELAY(t1, t2);
          printf("# %10.0f: %3d: i = %3d; prio = %3d; tag = %3lu; len = %lu\n", delay, i, value[1], value[0], tag, len);
        }
#else /* SINGLE_RECV */
      printf("# single receive request at a time.\n");
      for(i = 0; i < MSG_NUM; i++)
        {
          int*value = buffer + i * MAX_MSG_SIZE;
          nm_sr_recv_init(p_session, &requests[i]);
          nm_sr_recv_unpack_contiguous(p_session, &requests[i], value, MAX_MSG_SIZE);
          nm_sr_recv_irecv(p_session, &requests[i], p_gate, 0, NM_TAG_MASK_NONE); /* any tag */
          nm_sr_rwait(p_session, &requests[i]);
          nm_tag_t tag = nm_sr_request_get_tag(&requests[i]);
          nm_len_t len = NM_LEN_UNDEFINED;
          nm_sr_request_get_size(&requests[i], &len);
          PUK_GET_TICK(t2);
          const double delay = PUK_TIMING_DELAY(t1, t2);
          printf("# %10.0f: %3d: i = %3d; prio = %3d; tag = %3lu; len = %lu\n", delay, i, value[1], value[0], tag, len);
        }
#endif /* SINGLE_RECV */
    }
  else
    {
      printf("# posting send requests...\n");
      int i;
      for(i = 0; i < MSG_NUM; i++)
        {
          const int tag = i % MSG_BINS;
          int*value = buffer + i * MAX_MSG_SIZE;
          const nm_prio_t prio = i % MAX_PRIO;
          value[0] = prio;
          value[1] = i;
          /* on each tag, odd messages are short; even messages are large */
          nm_len_t len = ((i / MSG_BINS) % 2 == 0) ? MAX_MSG_SIZE : 2 * sizeof(int);
          nm_sr_send_init(p_session, &requests[i]);
          nm_sr_send_pack_contiguous(p_session, &requests[i], value, len);
          nm_sr_send_set_priority(p_session, &requests[i], prio);
          nm_sr_send_isend(p_session, &requests[i], p_gate, tag);
        }
      printf("# checking send status...\n");
      for(i = 0; i < MSG_NUM; i++)
        {
          /* check status without making progress */
          int rc = nm_status_test_allbits(&requests[i].req, NM_STATUS_PACK_COMPLETED | NM_STATUS_FINALIZED);
          nm_len_t len = NM_LEN_UNDEFINED;
          nm_sr_request_get_expected_size(&requests[i], &len);
          printf("# %i = %s; len = %lu\n", i, rc ? "COMPLETED":"INPROGRESS", len);
        }
      printf("# waiting for send completion...\n");
      for(i = 0; i < MSG_NUM; i++)
        {
          nm_sr_swait(p_session, &requests[i]);
          PUK_GET_TICK(t2);
          const double delay = PUK_TIMING_DELAY(t1, t2);
          printf("# %10.0f: %3d\n", delay, i);
        }
    }
  free(buffer);
  free(requests);

  nm_examples_exit();
  exit(0);
}

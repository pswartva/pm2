/*
 * NewMadeleine
 * Copyright (C) 2011-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

/** @file
 * frontend to directly use Minidriver components.
 * This is _not_ supposed to be used by end-users.
 */

#ifndef NM_MINIDRIVER_INTERFACE_H
#define NM_MINIDRIVER_INTERFACE_H

#include <nm_minidriver.h>
#include <nm_log.h>
#include <nm_errno.h>
#include <nm_parameters.h>

#include <sys/uio.h>

#define NM_MINIDRIVER_CHECK_RC(OP, RC)                                  \
  {                                                                     \
    if(((RC) != NM_ESUCCESS) && ((RC) != -NM_EAGAIN))                   \
      NM_FATAL("nm_minidriver: op %s returned rc = %d (%s)", (OP), rc, nm_strerror(rc)); \
  }


/* ** init/close ******************************************* */

static inline void nm_minidriver_getprops(const struct nm_minidriver_iface_s*p_minidriver,
                                          puk_context_t p_context, struct nm_minidriver_properties_s*p_props)
{
  (*p_minidriver->getprops)(p_context, p_props);
}

static inline void nm_minidriver_init(const struct nm_minidriver_iface_s*p_minidriver,
                                      puk_context_t p_context, const void**drv_url, size_t*url_size)
{
  (*p_minidriver->init)(p_context, drv_url, url_size);
}

static inline void nm_minidriver_close(const struct nm_minidriver_iface_s*p_minidriver,
                                       puk_context_t p_context)
{
  if(p_minidriver->close != NULL)
    {
      (*p_minidriver->close)(p_context);
    }
}


/* ** connect ********************************************** */

static inline void nm_minidriver_connect(struct puk_receptacle_NewMad_minidriver_s*r,
                                         const void*remote_url, size_t url_size)
{
  if(r->driver->connect != NULL)
    {
      (*r->driver->connect)(r->_status, remote_url, url_size);
    }
  else
    {
      assert(r->driver->connect_async != NULL);
      (*r->driver->connect_async)(r->_status, remote_url, url_size);
      if(r->driver->connect_wait != NULL)
        {
          (*r->driver->connect_wait)(r->_status);
        }
    }
}

static inline void nm_minidriver_disconnect(struct puk_receptacle_NewMad_minidriver_s*r)
{
  if(r->driver->disconnect != NULL)
    {
      (*r->driver->disconnect)(r->_status);
    }
}

/* ** send ************************************************* */

static inline void nm_minidriver_isend(struct puk_receptacle_NewMad_minidriver_s*r,
                                       const struct nm_minidriver_properties_s*p_props __attribute__((unused)),
                                       const void*buf, size_t len)
{
  /* static data descriptor with a lifespan at least up to send_poll;
   * only _one_ send may be active at the same time */
  static struct iovec v = { .iov_base = NULL, .iov_len = -1 };
  static struct nm_data_s data;

  if(r->driver->send_buf_get)
    {
      void*p_driver_buf = NULL;
      nm_len_t driver_len = NM_LEN_UNDEFINED;
      int rc = (*r->driver->send_buf_get)(r->_status, &p_driver_buf, &driver_len);
      NM_MINIDRIVER_CHECK_RC("send_buf_get", rc);
      memcpy(p_driver_buf, buf, len);
      rc = (*r->driver->send_buf_post)(r->_status, len);
      NM_MINIDRIVER_CHECK_RC("send_buf_post", rc);
    }
  else if(r->driver->send_iov_post)
    {
      v = (struct iovec){ .iov_base = (void*)buf, .iov_len = len };
      int rc = (*r->driver->send_iov_post)(r->_status, &v, 1);
      NM_MINIDRIVER_CHECK_RC("send_iov_post", rc);
    }
  else
    {
      nm_data_contiguous_build(&data, (void*)buf, len);
      int rc = (*r->driver->send_data_post)(r->_status, &data, 0, len);
      NM_MINIDRIVER_CHECK_RC("send_data_post", rc);
    }
}

static inline int nm_minidriver_spoll(struct puk_receptacle_NewMad_minidriver_s*r,
                                      const struct nm_minidriver_properties_s*p_props,
                                      const void*buf __attribute__((unused)), size_t len __attribute__((unused)))
{
  int rc = (*r->driver->send_poll)(r->_status);
  NM_MINIDRIVER_CHECK_RC("send_poll", rc);
  return rc;
}

static inline void nm_minidriver_send(struct puk_receptacle_NewMad_minidriver_s*r,
                                      const struct nm_minidriver_properties_s*p_props,
                                      const void*buf, size_t len)
{
  nm_minidriver_isend(r, p_props, buf, len);
  int rc = -1;
  do
    {
      rc = nm_minidriver_spoll(r, p_props, buf, len);
    }
  while(rc != NM_ESUCCESS);
}


/* ** receive ********************************************** */

static inline void nm_minidriver_irecv(struct puk_receptacle_NewMad_minidriver_s*r,
                                       const struct nm_minidriver_properties_s*p_props __attribute__((unused)),
                                       void*buf, size_t len)
{
  /* static data descriptor with a lifespan at least up to recv_poll;
   * only _one_ send may be active at the same time */
  static struct iovec v = { .iov_base = NULL, .iov_len = -1 };
  static struct nm_data_s data;

  if(r->driver->recv_buf_poll)
    {
      /* nothing to do */
    }
  else
    {
      if(r->driver->recv_iov_post)
        {
          v = (struct iovec){ .iov_base = buf, .iov_len = len };
          int rc = (*r->driver->recv_iov_post)(r->_status, &v, 1);
          NM_MINIDRIVER_CHECK_RC("recv_iov_post", rc);
        }
      else
        {
          nm_data_contiguous_build(&data, buf, len);
          int rc = (*r->driver->recv_data_post)(r->_status, &data, 0, len);
          NM_MINIDRIVER_CHECK_RC("recv_data_post", rc);
        }
    }
}

static inline int nm_minidriver_rpoll(struct puk_receptacle_NewMad_minidriver_s*r,
                                      const struct nm_minidriver_properties_s*p_props __attribute__((unused)),
                                      void*buf, size_t len)
{
  if(r->driver->recv_buf_poll)
    {
      void*p_driver_buf = NULL;
      nm_len_t driver_len = NM_LEN_UNDEFINED;
      int rc = (*r->driver->recv_buf_poll)(r->_status, &p_driver_buf, &driver_len);
      NM_MINIDRIVER_CHECK_RC("recv_buf_poll", rc);
      if(rc == 0)
        {
          memcpy(buf, p_driver_buf, len);
          rc = (*r->driver->recv_buf_release)(r->_status);
          NM_MINIDRIVER_CHECK_RC("recv_buf_release", rc);
        }
      return rc;
    }
  else
    {
      int rc = (*r->driver->recv_poll_one)(r->_status);
      NM_MINIDRIVER_CHECK_RC("recv_poll_one", rc);
      return rc;
    }
}

static inline void nm_minidriver_recv(struct puk_receptacle_NewMad_minidriver_s*r,
                                      const struct nm_minidriver_properties_s*p_props,
                                      void*buf, size_t len)
{
  nm_minidriver_irecv(r, p_props, buf, len);
  int rc = -1;
  do
    {
      rc = nm_minidriver_rpoll(r, p_props, buf, len);
    }
  while(rc != NM_ESUCCESS);
}

/* ** receive any ****************************************** */

static inline void nm_minidriver_recv_probe_any(const struct nm_minidriver_iface_s*p_minidriver,
                                                puk_context_t p_context, void**_status)
{
  int rc = (*p_minidriver->recv_probe_any)(p_context, _status);
  NM_MINIDRIVER_CHECK_RC("recv_probe_any", rc);
}

static inline int nm_minidriver_recv_cancel(struct puk_receptacle_NewMad_minidriver_s*r,
                                            const struct nm_minidriver_properties_s*p_props __attribute__((unused)))
{
  if(r->driver->recv_cancel == NULL)
    return -NM_ENOTIMPL;
  else
    return (*r->driver->recv_cancel)(r->_status);
}

/* ** rdv send/recv **************************************** */

static inline void nm_minidriver_recv_rdv(struct puk_receptacle_NewMad_minidriver_s*r,
                                           const struct nm_minidriver_properties_s*p_props __attribute__((unused)),
                                           void*buf, size_t len,
                                           struct puk_receptacle_NewMad_minidriver_s*rrdv,
                                           const struct nm_minidriver_properties_s*p_props_rdv)
{
  char rdv_data[NM_HEADER_RTR_DATA_SIZE];
  const nm_len_t rdv_data_size = NM_HEADER_RTR_DATA_SIZE;
  nm_minidriver_irecv(r, p_props, buf, len);
  if(p_props->capabilities.needs_rdv_data)
    {
      int rc = (*r->driver->get_rdv_data)(r->_status, rdv_data, rdv_data_size);
      NM_MINIDRIVER_CHECK_RC("get_rdv_data", rc);
    }
  nm_minidriver_send(rrdv, p_props_rdv, rdv_data, rdv_data_size);
  int rc = -1;
  do
    {
      rc = nm_minidriver_rpoll(r, p_props, buf, len);
    }
  while(rc != NM_ESUCCESS);
}

static inline void nm_minidriver_send_rdv(struct puk_receptacle_NewMad_minidriver_s*r,
                                          const struct nm_minidriver_properties_s*p_props,
                                          const void*buf, size_t len,
                                          struct puk_receptacle_NewMad_minidriver_s*rrdv,
                                          const struct nm_minidriver_properties_s*p_props_rdv)
{
  char rdv_data[NM_HEADER_RTR_DATA_SIZE];
  const nm_len_t rdv_data_size = NM_HEADER_RTR_DATA_SIZE;
  nm_minidriver_recv(rrdv, p_props_rdv, rdv_data, rdv_data_size);
  if(p_props->capabilities.needs_rdv_data)
    {
      int rc = (*r->driver->set_rdv_data)(r->_status, rdv_data, rdv_data_size);
      NM_MINIDRIVER_CHECK_RC("set_rdv_data", rc);
    }
  nm_minidriver_isend(r, p_props, buf, len);
  int rc = -1;
  do
    {
      rc = nm_minidriver_spoll(r, p_props, buf, len);
    }
  while(rc != NM_ESUCCESS);
}


#endif /* NM_MINIDRIVER_INTERFACE_H */

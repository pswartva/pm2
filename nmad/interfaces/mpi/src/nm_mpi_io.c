/*
 * NewMadeleine
 * Copyright (C) 2015-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include "nm_mpi_private.h"
#include "nm_mpi_private_syms.h"
#include <fcntl.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <libgen.h>
#include <dirent.h>
#include <sys/file.h>
#include <sys/types.h>


#include <Padico/Module.h>
PADICO_MODULE_HOOK(MadMPI);

#define NM_MPI_IO_TAG_OPEN1           ((nm_tag_t)0x0001)
#define NM_MPI_IO_TAG_OPEN2           ((nm_tag_t)0x0002)
#define NM_MPI_IO_TAG_OPEN_SHARED     ((nm_tag_t)0x0003)
#define NM_MPI_IO_TAG_CLOSE_BARRIER   ((nm_tag_t)0x0004)
#define NM_MPI_IO_TAG_CLOSE_DELETE    ((nm_tag_t)0x0005)
#define NM_MPI_IO_TAG_PREALLOCATE     ((nm_tag_t)0x0006)
#define NM_MPI_IO_TAG_ORDERED         ((nm_tag_t)0x0007)

/** @name File */
/* @{ */

/** types of operations on files */
enum nm_mpi_file_op_kind_e
  {
    NM_MPI_FILE_OP_NONE = 0,
    NM_MPI_FILE_OP_READ,
    NM_MPI_FILE_OP_WRITE
  };

/** file operation to be performed by the generic handler */
struct nm_mpi_file_op_s
{
  enum nm_mpi_file_op_kind_e op;
  struct nm_mpi_file_s*p_file;
  nm_mpi_offset_t offset;  /**< offset in etype units */
  void*buf;
  nm_mpi_count_t count;
  nm_mpi_datatype_t*p_datatype;
  enum nm_mpi_file_pointer_e
    {
     NM_MPI_FILE_POINTER_NONE = 0,
     NM_MPI_FILE_POINTER_LOCAL = 1,
     NM_MPI_FILE_POINTER_SHARED = 2
    } file_pointer;   /**< which pointer the operation should update */
  int err; /**< error status for the file IO */
  nm_mpi_offset_t done;
};

typedef struct nm_mpi_file_s
{
  MPI_File id; /**< identifier of the file */
  char*filename;
  int amode;   /**< access mode */
  struct nm_mpi_communicator_s*p_parent_comm;
  struct nm_comm_s*p_nm_comm;  /**< communicator attached to file */
  struct nm_mpi_info_s*p_hints;
  /** file view */
  struct
  {
    MPI_Offset disp;                     /**< absolute displacement of beginning of view (in bytes) */
    struct nm_mpi_datatype_s*p_etype;
    struct nm_mpi_datatype_s*p_filetype;
  } view;
  /** file kind */
  enum
    {
      NM_MPI_FILE_POSIX
    } kind;
  struct
  {
    int atomic; /**< whether access should be atomic */
    int locked; /**< whether we are holding the lock */
  } atomicity;  /**< atomic access for sequential consistency */
  int fd;       /**< POSIX file descriptor */
  nm_len_t esize;      /**< size of elements for access granularity; either 1 or sizeof(etype) if view is set */
  uint64_t cur;        /**< current local pointer position, in elements (etype) */
  nm_mpi_errhandler_t*p_errhandler;   /**< error handler attached to file */
  struct nm_mpi_file_op_s pending_op; /**< pending non-blocking operation */
  struct
  {
    int shadow_fd;                    /**< file descriptor for the shared pointer */
    char*shadow_name;                 /**< file nameof the shadow file containing shared pointer */
    nm_mpi_offset_t cur_shared;       /**< current shared pointer position */
  } shared;
} nm_mpi_file_t;

/* @} */

NM_MPI_HANDLE_TYPE(file, struct nm_mpi_file_s, _NM_MPI_FILE_OFFSET, 256);

static struct nm_mpi_handle_file_s nm_mpi_files = NM_MPI_HANDLE_NULL;
__PUK_SYM_INTERNAL struct nm_mpi_errhandler_s*nm_mpi_file_errhandler = NULL;  /**< default error handler for file IO */


/* ** Building blocks for file I/O
*/

static void nm_mpi_file_build_disk_data(struct nm_mpi_file_s*p_file,
                                        nm_mpi_offset_t offset, nm_mpi_count_t count, nm_mpi_datatype_t*p_datatype,
                                        struct nm_data_s*p_disk_data);
static int nm_mpi_file_op_build(MPI_File fh, enum nm_mpi_file_op_kind_e op, MPI_Offset offset, void*buf, int count, MPI_Datatype datatype, struct nm_mpi_file_op_s*p_op);
static int nm_mpi_file_op_build_pending(MPI_File fh, enum nm_mpi_file_op_kind_e op, MPI_Offset offset, void*buf, int count, MPI_Datatype datatype);
static void nm_mpi_file_op_exec(struct nm_mpi_file_op_s*p_op);
static int nm_mpi_file_op_exec_pending(MPI_File fh, struct nm_mpi_status_s*p_status);
static void nm_mpi_file_status_init(struct nm_mpi_status_s*p_status);

static void nm_mpi_file_status_update(struct nm_mpi_file_op_s*p_op, struct nm_mpi_status_s*p_status);

/* ** Interface with generic MPI requests
 */

nm_mpi_request_t*nm_mpi_file_request_alloc(nm_mpi_count_t count, MPI_Datatype datatype);

  __PUK_SYM_INTERNAL
void nm_mpi_file_request_status_update(nm_mpi_request_t*p_req, struct nm_mpi_status_s*p_status);

__PUK_SYM_INTERNAL
int nm_mpi_file_request_test(nm_mpi_request_t*p_req);

__PUK_SYM_INTERNAL
int nm_mpi_file_request_wait(nm_mpi_request_t*p_req);

/* ** Shared pointer
 */

static void nm_mpi_file_shared_acquire(struct nm_mpi_file_s*p_file);
static void nm_mpi_file_shared_release(struct nm_mpi_file_s*p_file);

/* ** Ordered IO
 */

static void nm_mpi_file_ordered_acquire(struct nm_mpi_file_s*p_file);
static void nm_mpi_file_ordered_release(struct nm_mpi_file_s*p_file);

/* ********************************************************* */

NM_MPI_ALIAS(MPI_File_open,               mpi_file_open);
NM_MPI_ALIAS(MPI_File_set_size,           mpi_file_set_size);
NM_MPI_ALIAS(MPI_File_get_size,           mpi_file_get_size);
NM_MPI_ALIAS(MPI_File_get_amode,          mpi_file_get_amode);
NM_MPI_ALIAS(MPI_File_get_group,          mpi_file_get_group);
NM_MPI_ALIAS(MPI_File_get_type_extent,    mpi_file_get_type_extent);
NM_MPI_ALIAS(MPI_File_set_view,           mpi_file_set_view);
NM_MPI_ALIAS(MPI_File_get_view,           mpi_file_get_view);
NM_MPI_ALIAS(MPI_File_close,              mpi_file_close);
NM_MPI_ALIAS(MPI_File_delete,             mpi_file_delete);
NM_MPI_ALIAS(MPI_File_get_position,       mpi_file_get_position);
NM_MPI_ALIAS(MPI_File_get_byte_offset,    mpi_file_get_byte_offset);
NM_MPI_ALIAS(MPI_File_seek,               mpi_file_seek);
NM_MPI_ALIAS(MPI_File_preallocate,        mpi_file_preallocate);
NM_MPI_ALIAS(MPI_File_create_errhandler,  mpi_file_create_errhandler);
NM_MPI_ALIAS(MPI_File_set_errhandler,     mpi_file_set_errhandler);
NM_MPI_ALIAS(MPI_File_get_errhandler,     mpi_file_get_errhandler);
NM_MPI_ALIAS(MPI_File_call_errhandler,    mpi_file_call_errhandler);
NM_MPI_ALIAS(MPI_File_sync,               mpi_file_sync);
NM_MPI_ALIAS(MPI_File_set_atomicity,      mpi_file_set_atomicity);
NM_MPI_ALIAS(MPI_File_get_atomicity,      mpi_file_get_atomicity);
NM_MPI_ALIAS(MPI_File_get_info,           mpi_file_get_info);
NM_MPI_ALIAS(MPI_File_set_info,           mpi_file_set_info);

NM_MPI_ALIAS(MPI_File_read,               mpi_file_read);
NM_MPI_ALIAS(MPI_File_write,              mpi_file_write);
NM_MPI_ALIAS(MPI_File_read_all,           mpi_file_read);
NM_MPI_ALIAS(MPI_File_write_all,          mpi_file_write);
NM_MPI_ALIAS(MPI_File_read_at,            mpi_file_read_at);
NM_MPI_ALIAS(MPI_File_write_at,           mpi_file_write_at);
NM_MPI_ALIAS(MPI_File_read_at_all,        mpi_file_read_at_all);
NM_MPI_ALIAS(MPI_File_write_at_all,       mpi_file_write_at_all);

NM_MPI_ALIAS(MPI_File_read_all_begin,     mpi_file_read_all_begin);
NM_MPI_ALIAS(MPI_File_read_all_end,       mpi_file_read_all_end);
NM_MPI_ALIAS(MPI_File_write_all_begin,    mpi_file_write_all_begin);
NM_MPI_ALIAS(MPI_File_write_all_end,      mpi_file_write_all_end);

NM_MPI_ALIAS(MPI_File_read_at_all_begin,  mpi_file_read_at_all_begin);
NM_MPI_ALIAS(MPI_File_read_at_all_end,    mpi_file_read_at_all_end);
NM_MPI_ALIAS(MPI_File_write_at_all_begin, mpi_file_write_at_all_begin);
NM_MPI_ALIAS(MPI_File_write_at_all_end,   mpi_file_write_at_all_end);

NM_MPI_ALIAS(MPI_Register_datarep,        mpi_register_datarep);

NM_MPI_ALIAS(MPI_File_iread,              mpi_file_iread);
NM_MPI_ALIAS(MPI_File_iread_at,           mpi_file_iread_at);
NM_MPI_ALIAS(MPI_File_iwrite,             mpi_file_iwrite);
NM_MPI_ALIAS(MPI_File_iwrite_at,          mpi_file_iwrite_at);

NM_MPI_ALIAS(MPI_File_get_position_shared, mpi_file_get_position_shared);
NM_MPI_ALIAS(MPI_File_seek_shared,        mpi_file_seek_shared);
NM_MPI_ALIAS(MPI_File_write_shared,       mpi_file_write_shared);
NM_MPI_ALIAS(MPI_File_iwrite_shared,      mpi_file_iwrite_shared);
NM_MPI_ALIAS(MPI_File_read_shared,        mpi_file_read_shared);
NM_MPI_ALIAS(MPI_File_iread_shared,       mpi_file_iread_shared);
NM_MPI_ALIAS(MPI_File_write_ordered,      mpi_file_write_ordered);
NM_MPI_ALIAS(MPI_File_read_ordered,       mpi_file_read_ordered);
NM_MPI_ALIAS(MPI_File_write_ordered_begin, mpi_file_write_ordered_begin);
NM_MPI_ALIAS(MPI_File_read_ordered_begin, mpi_file_read_ordered_begin);
NM_MPI_ALIAS(MPI_File_write_ordered_end,  mpi_file_write_ordered_end);
NM_MPI_ALIAS(MPI_File_read_ordered_end,   mpi_file_read_ordered_end);


/* ********************************************************* */

__PUK_SYM_INTERNAL
void nm_mpi_io_init(void)
{
  nm_mpi_handle_file_init(&nm_mpi_files);
  nm_mpi_file_errhandler = nm_mpi_errhandler_get(MPI_ERRORS_RETURN, NM_MPI_ERRHANDLER_FILE);
}

__PUK_SYM_INTERNAL
void nm_mpi_io_exit(void)
{
  nm_mpi_handle_file_finalize(&nm_mpi_files, NULL /* &nm_mpi_files_free */);
}

__PUK_SYM_INTERNAL
nm_mpi_file_t*nm_mpi_file_get(MPI_File file)
{
  nm_mpi_file_t*p_file = nm_mpi_handle_file_get(&nm_mpi_files, file);
  return p_file;
}

/* ********************************************************* */

int mpi_file_open(MPI_Comm comm, const char*filename, int amode, MPI_Info info, MPI_File*fh)
{
  NM_MPI_TRACE("MPI_File_open() filename = %s\n", filename);

  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  if(p_comm == NULL)
    return NM_MPI_ERROR_WORLD(MPI_ERR_COMM);
  const int rank = nm_comm_rank(p_comm->p_nm_comm);
  const int size = nm_comm_size(p_comm->p_nm_comm);
  if(rank < 0)
    return MPI_SUCCESS;
  if(p_comm->kind != NM_MPI_COMMUNICATOR_INTRA)
    return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_COMM);
  /* check parameters consistency */
  {
    const int root = 0;
    int*amodes = (rank == root) ? padico_malloc(sizeof(int) * size) : NULL;
    nm_coll_gather(p_comm->p_nm_comm, root, &amode, sizeof(amode), amodes, sizeof(int), NM_MPI_IO_TAG_OPEN1);
    int ok = 1;
    if(rank == root)
      {
        int i;
        for(i = 0; i < size; i++)
          {
            if(amodes[i] != amode)
              {
                ok = 0;
              }
          }
      }
    nm_coll_bcast(p_comm->p_nm_comm, root, &ok, sizeof(ok), NM_MPI_IO_TAG_OPEN2);
    if(amodes != NULL)
      padico_free(amodes);
    if(!ok)
      return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_NOT_SAME);
  }

  nm_mpi_file_t*p_file = nm_mpi_handle_file_alloc(&nm_mpi_files);
  p_file->filename = padico_strdup(filename);
  p_file->amode = amode;
  p_file->p_parent_comm = p_comm;
  p_file->p_nm_comm = nm_comm_dup(p_comm->p_nm_comm);
  p_file->kind = NM_MPI_FILE_POSIX;
  p_file->atomicity.atomic = 1;
  p_file->atomicity.locked = 0;
  p_file->esize = 1;
  p_file->cur = 0;
  p_file->pending_op.op = NM_MPI_FILE_OP_NONE;
  p_file->view.disp = 0;
  p_file->view.p_etype = NULL;
  p_file->view.p_filetype = NULL;
  p_file->p_hints = (info == MPI_INFO_NULL) ? NULL : nm_mpi_info_get(info);
  p_file->p_errhandler = nm_mpi_file_errhandler;

  /* create shared pointer */
  {
    /* copy filename since dirname & basename may modify the string */
    char*dfilename = padico_strdup(filename);
    char*bfilename = padico_strdup(filename);
    padico_string_t shared_filename = padico_string_new();
    padico_string_printf(shared_filename, "%s/.madmpi_%s_%x-%s.shared",
                         dirname(dfilename), getlogin(),
                         nm_comm_get_session(p_file->p_nm_comm)->hash_code, basename(bfilename));
    padico_free(dfilename);
    padico_free(bfilename);
    p_file->shared.shadow_name = padico_strdup(padico_string_get(shared_filename));
    padico_string_delete(shared_filename);
    p_file->shared.cur_shared = -1;
    if(rank == 0)
      {
        /* TODO- get file permission from 'file_perm' hint */
        p_file->shared.shadow_fd = NM_SYS(open)(p_file->shared.shadow_name, O_CREAT|O_TRUNC|O_RDWR, 0666);
        int err = errno;
        nm_coll_barrier(p_file->p_nm_comm, NM_MPI_IO_TAG_OPEN_SHARED);
        if(p_file->shared.shadow_fd < 0)
          {
            if(err == EPERM || err == EACCES)
              return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_ACCESS);
            else if(err == ENOENT)
              return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_NO_SUCH_FILE);
            else if(err == EEXIST)
              return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_FILE_EXISTS);
            else
              return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_IO);
          }
        typeof(p_file->shared.cur_shared) dummy_shared = 0;
        NM_SYS(pwrite)(p_file->shared.shadow_fd, &dummy_shared, sizeof(dummy_shared), 0);
      }
    else
      {
        nm_coll_barrier(p_file->p_nm_comm, NM_MPI_IO_TAG_OPEN_SHARED);
        p_file->shared.shadow_fd = NM_SYS(open)(p_file->shared.shadow_name, O_RDWR);
        int err = errno;
        if(p_file->shared.shadow_fd < 0)
          {
            if(err == EPERM || err == EACCES)
              return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_ACCESS);
            else if(err == ENOENT)
              return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_NO_SUCH_FILE);
            else if(err == EEXIST)
              return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_FILE_EXISTS);
            else
              return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_IO);
          }
      }
  }

  NM_SYS(sync)(); /* synchronize fs before open, in case we are running on NFS */

  const int mode =
    ((amode & MPI_MODE_RDONLY) ? O_RDONLY : 0) |
    ((amode & MPI_MODE_WRONLY) ? O_WRONLY : 0) |
    ((amode & MPI_MODE_RDWR)   ? O_RDWR   : 0) |
    ((amode & MPI_MODE_CREATE) ? O_CREAT  : 0) |
    ((amode & MPI_MODE_APPEND) ? O_APPEND : 0) ;
  int rc = -1;
  if(amode & MPI_MODE_EXCL)
    {
      if(nm_comm_rank(p_comm->p_nm_comm) == 0)
        {
          rc = NM_SYS(open)(filename, mode | O_EXCL, 0666);
        }
      nm_coll_bcast(p_comm->p_nm_comm, 0, &rc, sizeof(rc), NM_MPI_TAG_PRIVATE_FILE_OPEN);
      if(rc < 0)
        {
          return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_FILE);
        }
      if(nm_comm_rank(p_comm->p_nm_comm) != 0)
        {
          rc = NM_SYS(open)(filename, mode, 0666);
        }
    }
  else
    {
      rc = NM_SYS(open)(filename, mode, 0666); /* TODO: fix permissions */
    }
  if(rc >= 0)
    {
      p_file->fd = rc;
    }
  else
    {
      NM_MPI_WARNING("MPI_File_open() failed- filename = %s; rc = %d (%s).\n", filename, rc, strerror(errno));
      return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_NO_SUCH_FILE);
    }
  *fh = p_file->id;
  if(amode & MPI_MODE_APPEND)
    {
      struct stat stat;
      int rc = fstat(p_file->fd, &stat);
      if(rc)
        return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_FILE);
      p_file->cur = stat.st_size;
    }
  return MPI_SUCCESS;
}

int mpi_file_close(MPI_File*fh)
{
  NM_MPI_TRACE("MPI_File_close()- fh = %d\n", *fh);
  if(*fh == MPI_FILE_NULL)
    return NM_MPI_ERROR_FILE_DEFAULT(MPI_ERR_FILE);
  nm_mpi_file_t*p_file = nm_mpi_handle_file_get(&nm_mpi_files, *fh);
  if(p_file == NULL)
    return NM_MPI_ERROR_FILE_DEFAULT(MPI_ERR_FILE);
  NM_SYS(fsync)(p_file->fd);
  nm_coll_barrier(p_file->p_nm_comm, NM_MPI_IO_TAG_CLOSE_BARRIER);
  NM_SYS(close)(p_file->fd);
  if(p_file->amode & MPI_MODE_DELETE_ON_CLOSE)
    {
      if(nm_comm_rank(p_file->p_nm_comm) == 0)
        {
          unlink(p_file->filename);
          NM_SYS(sync)();
        }
      nm_coll_barrier(p_file->p_nm_comm, NM_MPI_IO_TAG_CLOSE_BARRIER);
      NM_SYS(sync)();
    }
  NM_SYS(close)(p_file->shared.shadow_fd);
  if(nm_comm_rank(p_file->p_nm_comm) == 0)
    {
      unlink(p_file->shared.shadow_name);
    }
  padico_free(p_file->shared.shadow_name);
  padico_free(p_file->filename);
  nm_mpi_handle_file_free(&nm_mpi_files, p_file);
  *fh = MPI_FILE_NULL;
  return MPI_SUCCESS;
}

int mpi_file_delete(const char*filename, MPI_Info info)
{
  NM_MPI_TRACE("MPI_File_delete()- filename = %s\n", filename);
  int rc = NM_SYS(unlink)(filename);
  if(rc != 0)
    return NM_MPI_ERROR_FILE_DEFAULT(MPI_ERR_NO_SUCH_FILE);
  return MPI_SUCCESS;
}

int mpi_file_sync(MPI_File fh)
{
  NM_MPI_TRACE("MPI_File_sync()- fh = %d\n", fh);
  nm_mpi_file_t*p_file = nm_mpi_handle_file_get(&nm_mpi_files, fh);
  if(p_file == NULL)
    return NM_MPI_ERROR_FILE_DEFAULT(MPI_ERR_FILE);
  int rc = syncfs(p_file->fd);
  if(rc != 0)
    return NM_MPI_ERROR_FILE(p_file, MPI_ERR_IO);
  return MPI_SUCCESS;
}

int mpi_file_set_atomicity(MPI_File fh, int flag)
{
  NM_MPI_TRACE("MPI_File_set_atomicity()- fh = %d; flag = %d\n", fh, flag);
  nm_mpi_file_t*p_file = nm_mpi_handle_file_get(&nm_mpi_files, fh);
  if(p_file == NULL)
    return NM_MPI_ERROR_FILE_DEFAULT(MPI_ERR_FILE);
  p_file->atomicity.atomic = flag;
  return MPI_SUCCESS;
}

int mpi_file_get_atomicity(MPI_File fh, int*flag)
{
  NM_MPI_TRACE("MPI_File_get_atomicity()- fh = %d\n", fh);
  nm_mpi_file_t*p_file = nm_mpi_handle_file_get(&nm_mpi_files, fh);
  if(p_file == NULL)
    return NM_MPI_ERROR_FILE_DEFAULT(MPI_ERR_FILE);
  *flag = p_file->atomicity.atomic;
  return MPI_SUCCESS;
}

int mpi_file_set_info(MPI_File fh, MPI_Info info)
{
  NM_MPI_TRACE("MPI_File_set_info()- fh = %d\n", fh);
  nm_mpi_file_t*p_file = nm_mpi_handle_file_get(&nm_mpi_files, fh);
  if(p_file == NULL)
    return NM_MPI_ERROR_FILE_DEFAULT(MPI_ERR_FILE);
  if(info != MPI_INFO_NULL)
    {
      NM_MPI_WARNING("MPI_File_set_info()- info ignored.\n");
    }
  return MPI_SUCCESS;
}

int mpi_file_get_info(MPI_File fh, MPI_Info*info_used)
{
  NM_MPI_TRACE("MPI_File_get_info()- fh = %d\n", fh);
  nm_mpi_file_t*p_file = nm_mpi_handle_file_get(&nm_mpi_files, fh);
  if(p_file == NULL)
    return NM_MPI_ERROR_FILE_DEFAULT(MPI_ERR_FILE);
  if(info_used)
    {
      if(p_file->p_hints)
        {
          struct nm_mpi_info_s*p_info = nm_mpi_info_dup(p_file->p_hints);
          *info_used = p_info->id;
        }
      else
        {
          *info_used = MPI_INFO_NULL;
        }
    }
  return MPI_SUCCESS;
}

int mpi_file_seek(MPI_File fh, MPI_Offset offset, int whence)
{
  NM_MPI_TRACE("MPI_File_seek()- fh = %d; offset = %lu; whence = %d\n", fh, offset, whence);
  nm_mpi_file_t*p_file = nm_mpi_handle_file_get(&nm_mpi_files, fh);
  if(p_file == NULL)
    return NM_MPI_ERROR_FILE_DEFAULT(MPI_ERR_FILE);
  switch(whence)
    {
    case MPI_SEEK_SET:
      p_file->cur = offset;
      break;
    case MPI_SEEK_CUR:
      p_file->cur += offset;
      break;
    case MPI_SEEK_END:
      {
        NM_MPI_WARNING("MPI_SEEK_END not implemented yet for files with view. Behavior may be wrong.\n");
        off_t rc = NM_SYS(lseek)(p_file->fd, offset, SEEK_END);
        if(rc < 0)
          return NM_MPI_ERROR_FILE(p_file, MPI_ERR_IO);
        p_file->cur = rc;
      }
      break;
    default:
      return NM_MPI_ERROR_FILE(p_file, MPI_ERR_ARG);
      break;
    }
  return MPI_SUCCESS;
}

/** get current position in etype unit */
int mpi_file_get_position(MPI_File fh, MPI_Offset*offset)
{
  NM_MPI_TRACE("MPI_File_get_position()- fh = %d\n", fh);
  nm_mpi_file_t*p_file = nm_mpi_handle_file_get(&nm_mpi_files, fh);
  if(p_file == NULL)
    return NM_MPI_ERROR_FILE_DEFAULT(MPI_ERR_FILE);
  *offset = p_file->cur;
  return MPI_SUCCESS;
}

struct nm_mpi_file_get_byte_offset_apply_s
{
  nm_mpi_offset_t byte_offset;
};

static void nm_mpi_file_get_byte_offset_apply(void*ptr, nm_len_t len, void*_context)
{
  struct nm_mpi_file_get_byte_offset_apply_s*p_apply = _context;
  p_apply->byte_offset = (intptr_t)ptr;
}

/** get position in bytes from offset in file view */
int mpi_file_get_byte_offset(MPI_File fh, MPI_Offset offset, MPI_Offset*disp)
{
  NM_MPI_TRACE("MPI_File_get_bye_offset()- fh = %d; offset =%lu\n", fh, offset);
  nm_mpi_file_t*p_file = nm_mpi_handle_file_get(&nm_mpi_files, fh);
  if(p_file == NULL)
    return NM_MPI_ERROR_FILE_DEFAULT(MPI_ERR_FILE);
  /* access 1 byte at given logical offset to get address in bytes */
  struct nm_mpi_datatype_s*p_datatype = nm_mpi_datatype_get(MPI_BYTE);
  struct nm_data_s disk_data;
  nm_mpi_file_build_disk_data(p_file, offset, 1, p_datatype, &disk_data);
  struct nm_mpi_file_get_byte_offset_apply_s context = { .byte_offset = -1 };
  nm_data_chunk_extractor_traversal(&disk_data, offset * p_file->esize, 1 /* size */, &nm_mpi_file_get_byte_offset_apply, &context);
  *disp = context.byte_offset;
  return MPI_SUCCESS;
}

int mpi_file_preallocate(MPI_File fh, MPI_Offset size)
{
  NM_MPI_TRACE("MPI_File_preallocate()- fh = %d; size =%lu\n", fh, size);
  nm_mpi_file_t*p_file = nm_mpi_handle_file_get(&nm_mpi_files, fh);
  if(p_file == NULL)
    return NM_MPI_ERROR_FILE_DEFAULT(MPI_ERR_FILE);
  const int rank = nm_comm_rank(p_file->p_nm_comm);
  const int root = 0;
  if(rank == root)
    {
      off_t s = NM_SYS(lseek)(p_file->fd, 0, SEEK_END);
      if(s < 0)
        return NM_MPI_ERROR_FILE(p_file, MPI_ERR_IO);
      if(s < size)
        {
          int rc = NM_SYS(ftruncate)(p_file->fd, size); /* size is given in bytes by user */
          if(rc < 0)
            return NM_MPI_ERROR_FILE(p_file, MPI_ERR_IO);
        }
      NM_SYS(fsync)(p_file->fd);
    }
  else
    {
      /* the only way to force NFS to flush attribute cache is to close/open the file */
      NM_SYS(close)(p_file->fd);
    }
  nm_coll_barrier(p_file->p_nm_comm, NM_MPI_IO_TAG_PREALLOCATE);
  if(rank != root)
    {
      sync();
      const int mode =
        ((p_file->amode & MPI_MODE_RDONLY) ? O_RDONLY : 0) |
        ((p_file->amode & MPI_MODE_WRONLY) ? O_WRONLY : 0) |
        ((p_file->amode & MPI_MODE_RDWR)   ? O_RDWR   : 0);
      int rc = NM_SYS(open)(p_file->filename, mode, 0666);
      if(rc >= 0)
        {
          p_file->fd = rc;
        }
      else
        {
          NM_MPI_WARNING("MPI_File_preallocate() failed- filename = %s; rc = %d (%s).\n",
                         p_file->filename, rc, strerror(errno));
          return NM_MPI_ERROR_FILE(p_file, MPI_ERR_IO);
        }
      {
        /* sanity check for NFS */
        off_t s = 0;
        while(s < size)
          {
            s = NM_SYS(lseek)(p_file->fd, 0, SEEK_END);
            if(s < size)
              {
                NM_MPI_WARNING("MPI_File_preallocate: size inconsistency- size = %ld (%ld); waiting...\n", s, size);
                puk_sleep(1);
              }
          }
      }
    }
  return MPI_SUCCESS;
}

int mpi_file_set_size(MPI_File fh, MPI_Offset size)
{
  NM_MPI_TRACE("MPI_File_set_size()- fh = %d; size = %lu\n", fh, size);
  nm_mpi_file_t*p_file = nm_mpi_handle_file_get(&nm_mpi_files, fh);
  if(p_file == NULL)
    return NM_MPI_ERROR_FILE_DEFAULT(MPI_ERR_FILE);
  int rc = NM_SYS(ftruncate)(p_file->fd, size);
  if(rc != 0)
    return NM_MPI_ERROR_FILE(p_file, MPI_ERR_IO);
  return MPI_SUCCESS;
}

int mpi_file_get_size(MPI_File fh, MPI_Offset*size)
{
  NM_MPI_TRACE("MPI_File_get_size()- fh = %d\n", fh);
  nm_mpi_file_t*p_file = nm_mpi_handle_file_get(&nm_mpi_files, fh);
  if(p_file == NULL)
    return NM_MPI_ERROR_FILE_DEFAULT(MPI_ERR_FILE);
  struct stat stat;
  int rc = fstat(p_file->fd, &stat);
  if(rc != 0)
    return NM_MPI_ERROR_FILE(p_file, MPI_ERR_IO);
  *size = stat.st_size;
  NM_MPI_TRACE("MPI_File_get_size()- fh = %d; size = %lu\n", fh, *size);
  return MPI_SUCCESS;
}

int mpi_file_get_amode(MPI_File fh, int*amode)
{
  NM_MPI_TRACE("MPI_File_get_amode()- fh = %d\n", fh);
  nm_mpi_file_t*p_file = nm_mpi_handle_file_get(&nm_mpi_files, fh);
  if(p_file == NULL)
    return NM_MPI_ERROR_FILE_DEFAULT(MPI_ERR_FILE);
  *amode = p_file->amode;
  return MPI_SUCCESS;
}

int mpi_file_get_group(MPI_File fh, MPI_Group*group)
{
  NM_MPI_TRACE("MPI_File_get_group()- fh = %d\n", fh);
  nm_mpi_file_t*p_file = nm_mpi_handle_file_get(&nm_mpi_files, fh);
  if(p_file == NULL)
    return NM_MPI_ERROR_FILE_DEFAULT(MPI_ERR_FILE);
  nm_mpi_group_t*p_group = nm_mpi_group_alloc();
  p_group->p_nm_group = nm_group_dup(nm_comm_group(p_file->p_nm_comm));
  *group = p_group->id;
  return MPI_SUCCESS;
}

int mpi_file_get_type_extent(MPI_File fh, MPI_Datatype datatype, MPI_Aint*extent)
{
  NM_MPI_TRACE("MPI_File_get_type_extent()- fh = %d; datatype = %d\n", fh, datatype);
  nm_mpi_file_t*p_file = nm_mpi_handle_file_get(&nm_mpi_files, fh);
  if(p_file == NULL)
    return NM_MPI_ERROR_FILE_DEFAULT(MPI_ERR_FILE);
  nm_mpi_datatype_t*p_datatype = nm_mpi_datatype_get(datatype);
  if(p_datatype == NULL)
    return NM_MPI_ERROR_FILE(p_file, MPI_ERR_TYPE);
  if(extent != NULL)
    *extent = p_datatype->extent;
  return MPI_SUCCESS;
}

int mpi_file_set_view(MPI_File fh, MPI_Offset disp, MPI_Datatype etype, MPI_Datatype filetype, const char*datarep, MPI_Info info)
{
  NM_MPI_TRACE("MPI_File_set_view()- fh = %d; disp = %lu; etype = %d; filetype = %d; datarep = %s\n", fh, disp, etype, filetype, datarep);
  nm_mpi_file_t*p_file = nm_mpi_handle_file_get(&nm_mpi_files, fh);
  if(p_file == NULL)
    return NM_MPI_ERROR_FILE_DEFAULT(MPI_ERR_FILE);
  if((datarep != NULL) && (strcmp(datarep, "native") != 0))
    return NM_MPI_ERROR_FILE(p_file, MPI_ERR_UNSUPPORTED_DATAREP);
  nm_mpi_datatype_t*p_etype = nm_mpi_datatype_get(etype);
  if(p_etype == NULL)
    return NM_MPI_ERROR_FILE(p_file, MPI_ERR_TYPE);
  nm_mpi_datatype_t*p_filetype = nm_mpi_datatype_get(filetype);
  if(p_filetype == NULL)
    return NM_MPI_ERROR_FILE(p_file, MPI_ERR_TYPE);
  if(nm_mpi_datatype_size(p_filetype) % nm_mpi_datatype_size(p_etype) != 0)
    return NM_MPI_ERROR_FILE(p_file, MPI_ERR_ARG);
  p_file->view.disp = disp;
  p_file->view.p_etype = p_etype; /* TODO- check that etype is a portable datatype */
  p_file->view.p_filetype = p_filetype;
  NM_SYS(lseek)(p_file->fd, disp, SEEK_SET);
  p_file->esize = nm_mpi_datatype_size(p_etype);
  p_file->cur = 0;
  return MPI_SUCCESS;
}

int mpi_file_get_view(MPI_File fh, MPI_Offset*disp, MPI_Datatype*etype, MPI_Datatype*filetype, char*datarep)
{
  nm_mpi_file_t*p_file = nm_mpi_handle_file_get(&nm_mpi_files, fh);
  if(p_file == NULL)
    return NM_MPI_ERROR_FILE_DEFAULT(MPI_ERR_FILE);
  *disp = p_file->view.disp;
  if(p_file->view.p_etype == NULL)
    {
      *etype = MPI_BYTE;
    }
  else
    {
      nm_mpi_datatype_ref_inc(p_file->view.p_etype, NULL);
      *etype = p_file->view.p_etype->id;
    }
  if(p_file->view.p_filetype == NULL)
    {
      *filetype = MPI_BYTE;
    }
  else
    {
      nm_mpi_datatype_ref_inc(p_file->view.p_filetype, NULL);
      *filetype = p_file->view.p_filetype->id;
    }
  strncpy(datarep, "native", MPI_MAX_DATAREP_STRING);
  return MPI_SUCCESS;
}

/* ** File I/O ********************************************* */

/** build a file operation as pending operation on the given file handle */
static int nm_mpi_file_op_build_pending(MPI_File fh, enum nm_mpi_file_op_kind_e op, MPI_Offset offset, void*buf, int count, MPI_Datatype datatype)
{
  nm_mpi_file_t*p_file = nm_mpi_handle_file_get(&nm_mpi_files, fh);
  if(p_file == NULL)
    return NM_MPI_ERROR_INTERNAL(MPI_ERR_FILE);
  struct nm_mpi_file_op_s*p_op = &p_file->pending_op;
  return nm_mpi_file_op_build(fh, op, offset, buf, count, datatype, p_op);
}

/** build a file operation request from user-level info
 * use offset = -1 for current local position, -2 for current shared */
static int nm_mpi_file_op_build(MPI_File fh, enum nm_mpi_file_op_kind_e op, MPI_Offset offset, void*buf, int count, MPI_Datatype datatype, struct nm_mpi_file_op_s*p_op)
{
  nm_mpi_file_t*p_file = nm_mpi_handle_file_get(&nm_mpi_files, fh);
  if(p_file == NULL)
    return NM_MPI_ERROR_INTERNAL(MPI_ERR_FILE);
  nm_mpi_datatype_t*p_datatype = nm_mpi_datatype_get(datatype);
  if(p_datatype == NULL)
    return NM_MPI_ERROR_INTERNAL(MPI_ERR_TYPE);
  if(count < 0)
    return NM_MPI_ERROR_INTERNAL(MPI_ERR_COUNT);
  if( ((op == NM_MPI_FILE_OP_READ) && (p_file->amode & MPI_MODE_WRONLY)) ||
      ((op == NM_MPI_FILE_OP_WRITE) && (p_file->amode & MPI_MODE_RDONLY)) )
    {
      return NM_MPI_ERROR_INTERNAL(MPI_ERR_ACCESS);
    }
  assert(p_op->op == NM_MPI_FILE_OP_NONE);
  p_op->op         = op;
  p_op->p_file     = p_file;
  if(offset == -1)
    {
      /* use & update local pointer */
      p_op->offset = p_file->cur;
      p_op->file_pointer = NM_MPI_FILE_POINTER_LOCAL;
    }
  else if(offset == -2)
    {
      /* use & update shared pointer */
      p_op->offset = p_file->shared.cur_shared;
      p_op->file_pointer = NM_MPI_FILE_POINTER_SHARED;
    }
  else
    {
      /* explicit offset */
      p_op->offset = offset;
      p_op->file_pointer = NM_MPI_FILE_POINTER_NONE;
    }
  p_op->buf        = buf;
  p_op->count      = count;
  p_op->p_datatype = p_datatype;
  p_op->err        = 0;
  p_op->done       = 0;
  return MPI_SUCCESS;
}

struct nm_mpi_file_op_apply_s
{
  enum nm_mpi_file_op_kind_e op;
  struct nm_mpi_file_s*p_file;
  void*buf;
  nm_len_t done; /**< bytes done so far */
  int err;
};

static void nm_mpi_file_op_apply(void*ptr, nm_len_t len, void*_context)
{
  struct nm_mpi_file_op_apply_s*p_apply = _context;
  if((len > 0) && (p_apply->err == 0))
    {
      if(p_apply->op == NM_MPI_FILE_OP_READ)
        {
          const ssize_t rc = NM_SYS(pread)(p_apply->p_file->fd, p_apply->buf + p_apply->done, len, (uintptr_t)ptr);
          if(rc < 0)
            {
              NM_MPI_WARNING("read error rc = %lld; chunk size = %lld; errno = %d (%s)\n",
                                 (long long)rc, (long long)len, errno, strerror(errno));
              p_apply->err = MPI_ERR_FILE;
            }
          else if(rc < len)
            {
              NM_MPI_TRACE("read less data than wanted; rc = %lld; chunk size = %lld", (long long)rc, (long long)len);
              p_apply->done += rc;
            }
          else
            {
              p_apply->done += len;
            }
        }
      else if(p_apply->op == NM_MPI_FILE_OP_WRITE)
        {
          const ssize_t rc = NM_SYS(pwrite)(p_apply->p_file->fd, p_apply->buf + p_apply->done, len, (uintptr_t)ptr);
          if(rc < 0)
            {
              NM_MPI_WARNING("write error rc = %lld; chunk size = %lld; errno = %d (%s)\n",
                                 (long long)rc, (long long)len, errno, strerror(errno));
              p_apply->err = MPI_ERR_FILE;
            }
          else if(rc < len)
            {
              /* less bytes than asked have been written; what does if mean?! */
              NM_MPI_WARNING("write less data than wanted; rc = %lld; chunk size = %lld\n", (long long)rc, (long long)len);
              p_apply->done += rc;
            }
          else
            {
              p_apply->done += len;
            }
        }
      else
        {
          NM_MPI_FATAL_ERROR("wrong op = %d in file op iterator.\n", p_apply->op);
        }
    }
}

/** fills disk data with a data descriptor suitable for disk access, according to file view;
 * file acceess is: count * datatype, at given offset (in etype units) in file view */
static void nm_mpi_file_build_disk_data(struct nm_mpi_file_s*p_file,
                                        nm_mpi_offset_t offset, nm_mpi_count_t count, nm_mpi_datatype_t*p_datatype,
                                        struct nm_data_s*p_disk_data)
{
  if((p_file->view.p_filetype != NULL) && (nm_mpi_datatype_size(p_file->view.p_filetype) != 0))
    {
      const size_t view_data_size = nm_mpi_datatype_size(p_file->view.p_filetype);
      const size_t unrolls = (offset * p_file->esize
                              + count * nm_mpi_datatype_size(p_datatype)
                              + (view_data_size - 1)) / view_data_size; /* integer version of ceil(size/view_data_size) */
      nm_mpi_data_build(p_disk_data, (void*)(uintptr_t)p_file->view.disp, p_file->view.p_filetype, unrolls);
    }
  else
    {
      nm_data_contiguous_build(p_disk_data, NULL, offset + count * nm_mpi_datatype_size(p_datatype));
    }
}


static void nm_mpi_file_op_exec(struct nm_mpi_file_op_s*p_op)
{
  assert(p_op->op != NM_MPI_FILE_OP_NONE);
  struct nm_mpi_file_s*p_file = p_op->p_file;
  nm_mpi_datatype_t*p_datatype = p_op->p_datatype;
  const nm_mpi_count_t count = p_op->count;
  const size_t size = count * nm_mpi_datatype_size(p_datatype);
  void*ptr = padico_malloc(size);
  /* pack data for write */
  if(p_op->op == NM_MPI_FILE_OP_WRITE)
    {
      nm_mpi_datatype_pack(ptr, p_op->buf, p_datatype, count);
    }
  /* prepare type for file view */
  struct nm_data_s disk_data;
  nm_mpi_file_build_disk_data(p_file, p_op->offset, count, p_datatype, &disk_data);
  /* lock */
  if(p_file->atomicity.atomic && (p_file->amode & (MPI_MODE_RDWR | MPI_MODE_WRONLY)))
    {
      /* lock only in case we are in atomic mode and file is open for writing */
      int rc = NM_SYS(flock)(p_file->fd, LOCK_EX);
      if(rc)
        {
          NM_MPI_FATAL_ERROR("error %d (%s) while trying to acquire file lock.\n", errno, strerror(errno));
        }
      fdatasync(p_file->fd);
    }
  /* do the I/O */
  struct nm_mpi_file_op_apply_s op_context =
    { .op = p_op->op, .p_file = p_file, .buf = ptr, .done = 0, .err = 0 };
  nm_data_chunk_extractor_traversal(&disk_data, p_op->offset * p_file->esize, size, &nm_mpi_file_op_apply, &op_context);
  /* unlock */
  if(p_file->atomicity.atomic && (p_file->amode & (MPI_MODE_RDWR | MPI_MODE_WRONLY)))
    {
      fdatasync(p_file->fd);
      int rc = NM_SYS(flock)(p_file->fd, LOCK_UN);
      if(rc)
        {
          NM_MPI_FATAL_ERROR("error %d (%s) while trying to release file lock.\n", errno, strerror(errno));
        }
    }
  /* unpack data for read */
  if(p_op->op == NM_MPI_FILE_OP_READ)
    {
      nm_mpi_datatype_unpack(ptr, p_op->buf, p_datatype, count);
    }
  padico_free(ptr);
  if(op_context.done % p_file->esize != 0)
    {
      NM_MPI_WARNING("nm_mpi_file_op_exec()- IO operation not multiple of base element size: done = %ld; esize = %ld\n",
                     op_context.done, p_file->esize);
    }
  if(p_op->file_pointer == NM_MPI_FILE_POINTER_LOCAL)
    {
      p_file->cur += op_context.done / p_file->esize;
    }
  else if(p_op->file_pointer == NM_MPI_FILE_POINTER_SHARED)
    {
      p_file->shared.cur_shared += op_context.done / p_file->esize;
    }
  p_op->err = (op_context.err == 0) ? MPI_SUCCESS : op_context.err;
  p_op->done = op_context.done;
}

static int nm_mpi_file_op_exec_pending(MPI_File fh, struct nm_mpi_status_s*p_status)
{
  nm_mpi_file_t*p_file = nm_mpi_handle_file_get(&nm_mpi_files, fh);
  if(p_file == NULL)
    return NM_MPI_ERROR_INTERNAL(MPI_ERR_FILE);
  struct nm_mpi_file_op_s*p_op = &p_file->pending_op;
  nm_mpi_file_op_exec(p_op);
  nm_mpi_file_status_update(p_op, p_status);
  return MPI_SUCCESS;
}

static void nm_mpi_file_status_init(struct nm_mpi_status_s*p_status)
{
  p_status->MPI_ERROR = MPI_SUCCESS;
  p_status->size = 0;
}

void nm_mpi_file_status_update(struct nm_mpi_file_op_s*p_op, struct nm_mpi_status_s*p_status)
{
  if(p_status != NULL)
    {
      p_status->MPI_TAG = MPI_ANY_TAG;
      p_status->MPI_SOURCE = MPI_ANY_SOURCE;
      p_status->MPI_ERROR = p_op->err;
      p_status->size = p_op->done;
    }
  p_op->op = NM_MPI_FILE_OP_NONE; /* mark op as completed. */
}

void nm_mpi_file_request_status_update(nm_mpi_request_t*p_req, struct nm_mpi_status_s*p_status)
{
  if(p_req->p_file_op->file_pointer == NM_MPI_FILE_POINTER_SHARED)
    {
      nm_mpi_file_shared_release(p_req->p_file_op->p_file);
    }
  nm_mpi_file_status_update(p_req->p_file_op, p_status);
  padico_free(p_req->p_file_op);
  p_req->p_file_op = NULL;
}

int nm_mpi_file_request_test(nm_mpi_request_t*p_req)
{
  nm_mpi_file_op_exec(p_req->p_file_op); /* TODO- make it really asynchronous */
  return MPI_SUCCESS;
}

int nm_mpi_file_request_wait(nm_mpi_request_t*p_req)
{
  nm_mpi_file_op_exec(p_req->p_file_op);
  return MPI_SUCCESS;
}


/* ** blocking file I/O ************************************ */

int mpi_file_read(MPI_File fh, void*buf, int count, MPI_Datatype datatype, MPI_Status*status)
{
  nm_mpi_file_t*p_file = nm_mpi_handle_file_get(&nm_mpi_files, fh);
  if(p_file == NULL)
    return NM_MPI_ERROR_FILE_DEFAULT(MPI_ERR_FILE);
  NM_MPI_TRACE("MPI_File_read()- fh = %d; count = %d; datatype = %d\n", fh, count, datatype);
  nm_mpi_file_status_init(status);
  int err = nm_mpi_file_op_build_pending(fh, NM_MPI_FILE_OP_READ, -1, buf, count, datatype);
  if(err == MPI_SUCCESS)
    {
      nm_mpi_file_op_exec_pending(fh, status);
    }
  return NM_MPI_ERROR_FILE(p_file, err);
}

int mpi_file_write(MPI_File fh, const void*buf, int count, MPI_Datatype datatype, MPI_Status*status)
{
  nm_mpi_file_t*p_file = nm_mpi_handle_file_get(&nm_mpi_files, fh);
  if(p_file == NULL)
    return NM_MPI_ERROR_FILE_DEFAULT(MPI_ERR_FILE);
  NM_MPI_TRACE("MPI_File_write()- fh = %d; count = %d; datatype = %d\n", fh, count, datatype);
  nm_mpi_file_status_init(status);
  int err = nm_mpi_file_op_build_pending(fh, NM_MPI_FILE_OP_WRITE, -1, (void*)buf, count, datatype);
  if(err == MPI_SUCCESS)
    {
      nm_mpi_file_op_exec_pending(fh, status);
    }
  return NM_MPI_ERROR_FILE(p_file, err);
}

int mpi_file_read_at(MPI_File fh, MPI_Offset offset, void*buf, int count, MPI_Datatype datatype, MPI_Status*status)
{
  nm_mpi_file_t*p_file = nm_mpi_handle_file_get(&nm_mpi_files, fh);
  if(p_file == NULL)
    return NM_MPI_ERROR_FILE_DEFAULT(MPI_ERR_FILE);
  NM_MPI_TRACE("MPI_File_read_at()- fh = %d; offset = %lu; count = %d; datatype = %d\n", fh, offset, count, datatype);
  nm_mpi_file_status_init(status);
  int err = nm_mpi_file_op_build_pending(fh, NM_MPI_FILE_OP_READ, offset, buf, count, datatype);
  if(err == MPI_SUCCESS)
    {
      nm_mpi_file_op_exec_pending(fh, status);
    }
  return NM_MPI_ERROR_FILE(p_file, err);
}

int mpi_file_write_at(MPI_File fh, MPI_Offset offset, const void*buf, int count, MPI_Datatype datatype, MPI_Status*status)
{
  nm_mpi_file_t*p_file = nm_mpi_handle_file_get(&nm_mpi_files, fh);
  if(p_file == NULL)
    return NM_MPI_ERROR_FILE_DEFAULT(MPI_ERR_FILE);
  NM_MPI_TRACE("MPI_File_write_at()- fh = %d; offset = %lu; count = %d; datatype = %d\n", fh, offset, count, datatype);
  nm_mpi_file_status_init(status);
  int err = nm_mpi_file_op_build_pending(fh, NM_MPI_FILE_OP_WRITE, offset, (void*)buf, count, datatype);
  if(err == MPI_SUCCESS)
    {
      nm_mpi_file_op_exec_pending(fh, status);
    }
  return NM_MPI_ERROR_FILE(p_file, err);
}

int mpi_file_read_all(MPI_File fh, void*buf, int count, MPI_Datatype datatype, MPI_Status*status)
{
  return mpi_file_read(fh, buf, count, datatype, status);
}

int mpi_file_write_all(MPI_File fh, void*buf, int count, MPI_Datatype datatype, MPI_Status*status)
{
  return mpi_file_write(fh, buf, count, datatype, status);
}

int mpi_file_read_at_all(MPI_File fh, MPI_Offset offset, void*buf, int count, MPI_Datatype datatype, MPI_Status*status)
{
  return mpi_file_read_at(fh, offset, buf, count, datatype, status);
}

int mpi_file_write_at_all(MPI_File fh, MPI_Offset offset, const void*buf, int count, MPI_Datatype datatype, MPI_Status*status)
{
  return mpi_file_write_at(fh, offset, buf, count, datatype, status);
}


/* ** non-blocking file I/O ******************************** */

int mpi_file_read_all_begin(MPI_File fh, void*buf, int count, MPI_Datatype datatype)
{
  NM_MPI_TRACE("MPI_File_read_all_begin()- fh = %d; count = %d; datatype = %d\n", fh, count, datatype);
  int rc = nm_mpi_file_op_build_pending(fh, NM_MPI_FILE_OP_READ, -1, buf, count, datatype);
  return rc;
}
int mpi_file_read_all_end(MPI_File fh, void*buf, MPI_Status*status)
{
  NM_MPI_TRACE("MPI_File_read_all_end()- fh = %d\n", fh);
  return nm_mpi_file_op_exec_pending(fh, status);
}

int mpi_file_write_all_begin(MPI_File fh, const void*buf, int count, MPI_Datatype datatype)
{
  NM_MPI_TRACE("MPI_File_write_all_begin()- fh = %d; count = %d; datatype = %d\n", fh, count, datatype);
  int rc = nm_mpi_file_op_build_pending(fh, NM_MPI_FILE_OP_WRITE, -1, (void*)buf, count, datatype);
  return rc;
}
int mpi_file_write_all_end(MPI_File fh, const void*buf, MPI_Status*status)
{
  NM_MPI_TRACE("MPI_File_write_all_end()- fh = %d\n", fh);
  return nm_mpi_file_op_exec_pending(fh, status);
}

int mpi_file_read_at_all_begin(MPI_File fh, MPI_Offset offset, void*buf, int count, MPI_Datatype datatype)
{
  NM_MPI_TRACE("MPI_File_read_at_all_begin()- fh = %d; offset = %lu; count = %d; datatype = %d\n", fh, offset, count, datatype);
  int rc = nm_mpi_file_op_build_pending(fh, NM_MPI_FILE_OP_READ, offset, buf, count, datatype);
  return rc;
}
int mpi_file_read_at_all_end(MPI_File fh, void*buf, MPI_Status*status)
{
  NM_MPI_TRACE("MPI_File_read_at_all_end()- fh = %d\n", fh);
  return nm_mpi_file_op_exec_pending(fh, status);
}

int mpi_file_write_at_all_begin(MPI_File fh, MPI_Offset offset, const void*buf, int count, MPI_Datatype datatype)
{
  NM_MPI_TRACE("MPI_File_write_at_all_begin()- fh = %d; offset = %lu; count = %d; datatype = %d\n", fh, offset, count, datatype);
  int rc = nm_mpi_file_op_build_pending(fh, NM_MPI_FILE_OP_WRITE, offset, (void*)buf, count, datatype);
  return rc;
}
int mpi_file_write_at_all_end(MPI_File fh, const void*buf, MPI_Status*status)
{
  NM_MPI_TRACE("MPI_File_write_at_all_end()- fh = %d\n", fh);
  return nm_mpi_file_op_exec_pending(fh, status);
}


/* ** errhandler ******************************************* */

int mpi_file_create_errhandler(MPI_File_errhandler_fn*function, MPI_Errhandler*errhandler)
{
  NM_MPI_TRACE("MPI_File_create_errhandler()\n");
  struct nm_mpi_errhandler_s*p_errhandler = nm_mpi_errhandler_alloc(NM_MPI_ERRHANDLER_FILE, function);
  *errhandler = p_errhandler->id;
  return MPI_SUCCESS;
}

int mpi_file_set_errhandler(MPI_File fh, MPI_Errhandler errhandler)
{
  NM_MPI_TRACE("MPI_File_set_errhandler()- fh = %d\n", fh);
  nm_mpi_file_t*p_file = nm_mpi_handle_file_get(&nm_mpi_files, fh);
  if(p_file == NULL)
    return NM_MPI_ERROR_FILE_DEFAULT(MPI_ERR_FILE);
  struct nm_mpi_errhandler_s*p_errhandler = nm_mpi_errhandler_get(errhandler, NM_MPI_ERRHANDLER_FILE);
  if(p_errhandler == NULL)
    return NM_MPI_ERROR_FILE(p_file, MPI_ERR_ARG);
  if(fh == MPI_FILE_NULL)
    {
      nm_mpi_file_errhandler = p_errhandler;
    }
  else
    {
      p_file->p_errhandler = p_errhandler;
    }
  return MPI_SUCCESS;
}

int mpi_file_get_errhandler(MPI_File fh, MPI_Errhandler*errhandler)
{
  NM_MPI_TRACE("MPI_File_get_errhandler()- fh = %d\n", fh);
  struct nm_mpi_errhandler_s*p_errhandler = NULL;
  if(fh == MPI_FILE_NULL)
    {
      p_errhandler = nm_mpi_file_errhandler;
    }
  else
    {
      nm_mpi_file_t*p_file = nm_mpi_handle_file_get(&nm_mpi_files, fh);
      if(p_file == NULL)
        return NM_MPI_ERROR_FILE_DEFAULT(MPI_ERR_FILE);
      p_errhandler = p_file->p_errhandler;
    }
  if(p_errhandler != NULL)
    {
      *errhandler = p_errhandler->id;
    }
  else
    {
      *errhandler = MPI_ERRHANDLER_NULL;
    }
  return MPI_SUCCESS;
}

int mpi_file_call_errhandler(MPI_File fh, int errorcode)
{
  NM_MPI_TRACE("MPI_File_call_errhandler()- fh = %d; errorcode = %d\n", fh, errorcode);
  nm_mpi_file_t*p_file = nm_mpi_handle_file_get(&nm_mpi_files, fh);
  if(p_file == NULL)
    return NM_MPI_ERROR_FILE_DEFAULT(MPI_ERR_FILE);
  nm_mpi_errhandler_exec(p_file->p_errhandler, NM_MPI_ERRHANDLER_FILE, fh, errorcode);
  return MPI_SUCCESS;
}


/* ** shared file pointer ********************************** */

static void nm_mpi_file_shared_acquire(struct nm_mpi_file_s*p_file)
{
  assert(p_file->shared.cur_shared == -1);
  int rc = NM_SYS(flock)(p_file->shared.shadow_fd, LOCK_EX);
  if(rc)
    {
      NM_MPI_FATAL_ERROR("error %d (%s) while trying to acquire file lock.\n", errno, strerror(errno));
    }
  fdatasync(p_file->shared.shadow_fd);
  rc = NM_SYS(pread)(p_file->shared.shadow_fd, &p_file->shared.cur_shared, sizeof(p_file->shared.cur_shared), 0);
  if(rc != sizeof(p_file->shared.cur_shared))
    {
      NM_MPI_FATAL_ERROR("error %d (%s) while reading shared pointer.\n", errno, strerror(errno));
    }
  assert(p_file->shared.cur_shared != -1);
}
static void nm_mpi_file_shared_release(struct nm_mpi_file_s*p_file)
{
  assert(p_file->shared.cur_shared != -1);
  int rc = NM_SYS(pwrite)(p_file->shared.shadow_fd, &p_file->shared.cur_shared, sizeof(p_file->shared.cur_shared), 0);
  if(rc != sizeof(p_file->shared.cur_shared))
    {
      NM_MPI_FATAL_ERROR("error %d (%s) while writing shared pointer.\n", errno, strerror(errno));
    }
  fdatasync(p_file->shared.shadow_fd);
  rc = NM_SYS(flock)(p_file->shared.shadow_fd, LOCK_UN);
  if(rc)
    {
      NM_MPI_FATAL_ERROR("error %d (%s) while trying to release file lock.\n", errno, strerror(errno));
    }
  p_file->shared.cur_shared = -1;
}

int mpi_file_get_position_shared(MPI_File fh, MPI_Offset*offset)
{
  NM_MPI_TRACE("MPI_File_get_position()- fh = %d\n", fh);
  nm_mpi_file_t*p_file = nm_mpi_handle_file_get(&nm_mpi_files, fh);
  if(p_file == NULL)
    return NM_MPI_ERROR_FILE_DEFAULT(MPI_ERR_FILE);
  nm_mpi_file_shared_acquire(p_file);
  *offset = p_file->shared.cur_shared;
  nm_mpi_file_shared_release(p_file);
  return MPI_SUCCESS;
}

int mpi_file_seek_shared(MPI_File fh, MPI_Offset offset, int whence)
{
  NM_MPI_TRACE("MPI_File_seek_shared()- fh = %d\n", fh);
  nm_mpi_file_t*p_file = nm_mpi_handle_file_get(&nm_mpi_files, fh);
  if(p_file == NULL)
    return NM_MPI_ERROR_FILE_DEFAULT(MPI_ERR_FILE);
  nm_mpi_file_shared_acquire(p_file);
  switch(whence)
    {
    case MPI_SEEK_SET:
      p_file->shared.cur_shared = offset;
      break;
    case MPI_SEEK_CUR:
      p_file->shared.cur_shared += offset;
      break;
    case MPI_SEEK_END:
      {
        if(p_file->view.p_etype)
          NM_MPI_WARNING("MPI_SEEK_END not implemented yet for files with view. Behavior may be wrong.\n");
        off_t rc = NM_SYS(lseek)(p_file->fd, offset, SEEK_END);
        if(rc < 0)
          return NM_MPI_ERROR_FILE(p_file, MPI_ERR_IO);
        p_file->shared.cur_shared = rc;
      }
      break;
    default:
      return NM_MPI_ERROR_FILE(p_file, MPI_ERR_ARG);
      break;
    }
  nm_mpi_file_shared_release(p_file);
  return MPI_SUCCESS;
}

int mpi_file_read_shared(MPI_File fh, void*buf, int count, MPI_Datatype datatype, MPI_Status*status)
{
  nm_mpi_file_t*p_file = nm_mpi_handle_file_get(&nm_mpi_files, fh);
  if(p_file == NULL)
    return NM_MPI_ERROR_FILE_DEFAULT(MPI_ERR_FILE);
  nm_mpi_file_shared_acquire(p_file);
  nm_mpi_file_status_init(status);
  int err = nm_mpi_file_op_build_pending(fh, NM_MPI_FILE_OP_READ, -2, buf, count, datatype);
  if(err == MPI_SUCCESS)
    {
      nm_mpi_file_op_exec_pending(fh, status);
    }
  nm_mpi_file_shared_release(p_file);
  return NM_MPI_ERROR_FILE(p_file, err);
}

int mpi_file_write_shared(MPI_File fh, const void*buf, int count, MPI_Datatype datatype, MPI_Status*status)
{
  nm_mpi_file_t*p_file = nm_mpi_handle_file_get(&nm_mpi_files, fh);
  if(p_file == NULL)
    return NM_MPI_ERROR_FILE_DEFAULT(MPI_ERR_FILE);
  nm_mpi_file_shared_acquire(p_file);
  nm_mpi_file_status_init(status);
  int err = nm_mpi_file_op_build_pending(fh, NM_MPI_FILE_OP_WRITE, -2, (void*)buf, count, datatype);
  if(err == MPI_SUCCESS)
    {
      nm_mpi_file_op_exec_pending(fh, status);
    }
  nm_mpi_file_shared_release(p_file);
  return NM_MPI_ERROR_FILE(p_file, err);
}

int mpi_file_iread_shared(MPI_File fh, void*buf, int count, MPI_Datatype datatype, MPI_Request*request)
{
  struct nm_mpi_file_op_s*p_op = padico_malloc(sizeof(struct nm_mpi_file_op_s));
  p_op->op = NM_MPI_FILE_OP_NONE;
  nm_mpi_file_t*p_file = nm_mpi_handle_file_get(&nm_mpi_files, fh);
  if(p_file == NULL)
    return NM_MPI_ERROR_FILE_DEFAULT(MPI_ERR_FILE);
  nm_mpi_file_shared_acquire(p_file);
  int err = nm_mpi_file_op_build(fh, NM_MPI_FILE_OP_READ, -2, buf, count, datatype, p_op);
  if(err == MPI_SUCCESS)
    {
      struct nm_mpi_request_s*p_req = nm_mpi_file_request_alloc(count, datatype);
      p_req->p_file_op = p_op;
      *request = p_req->id;
    }
  else
    {
      *request = MPI_REQUEST_NULL;
    }
  return NM_MPI_ERROR_FILE(p_file, err);
}
int mpi_file_iwrite_shared(MPI_File fh, const void*buf, int count, MPI_Datatype datatype, MPI_Request*request)
{
  struct nm_mpi_file_op_s*p_op = padico_malloc(sizeof(struct nm_mpi_file_op_s));
  p_op->op = NM_MPI_FILE_OP_NONE;
  nm_mpi_file_t*p_file = nm_mpi_handle_file_get(&nm_mpi_files, fh);
  if(p_file == NULL)
    return NM_MPI_ERROR_FILE_DEFAULT(MPI_ERR_FILE);
  nm_mpi_file_shared_acquire(p_file);
  int err = nm_mpi_file_op_build(fh, NM_MPI_FILE_OP_WRITE, -2, (void*)buf, count, datatype, p_op);
  if(err == MPI_SUCCESS)
    {
      struct nm_mpi_request_s*p_req = nm_mpi_file_request_alloc(count, datatype);
      p_req->p_file_op = p_op;
      *request = p_req->id;
    }
  else
    {
      *request = MPI_REQUEST_NULL;
    }
  return NM_MPI_ERROR_FILE(p_file, err);
}


/* ** shared file pointer, ordered IO ********************** */

/** acquire token for ordered IO */
static void nm_mpi_file_ordered_acquire(struct nm_mpi_file_s*p_file)
{
  const int rank = nm_comm_rank(p_file->p_nm_comm);
  if(rank > 0)
    {
      nm_sr_recv(nm_comm_get_session(p_file->p_nm_comm), nm_comm_get_gate(p_file->p_nm_comm, rank - 1), NM_MPI_IO_TAG_ORDERED, NULL, 0);
    }
}
/** release token for ordered IO */
static void nm_mpi_file_ordered_release(struct nm_mpi_file_s*p_file)
{
  const int rank = nm_comm_rank(p_file->p_nm_comm);
  const int size = nm_comm_size(p_file->p_nm_comm);
  if(rank < size -1)
    {
      nm_sr_send(nm_comm_get_session(p_file->p_nm_comm), nm_comm_get_gate(p_file->p_nm_comm, rank + 1), NM_MPI_IO_TAG_ORDERED, NULL, 0);
    }
}

int mpi_file_read_ordered(MPI_File fh, void*buf, int count, MPI_Datatype datatype, MPI_Status*status)
{
  nm_mpi_file_t*p_file = nm_mpi_handle_file_get(&nm_mpi_files, fh);
  if(p_file == NULL)
    return NM_MPI_ERROR_FILE_DEFAULT(MPI_ERR_FILE);
  int err = mpi_file_read_ordered_begin(fh, buf, count, datatype);
  if(err == MPI_SUCCESS)
    {
      err = mpi_file_read_ordered_end(fh, buf, status);
    }
  return NM_MPI_ERROR_FILE(p_file, err);
}

int mpi_file_write_ordered(MPI_File fh, const void*buf, int count, MPI_Datatype datatype, MPI_Status*status)
{
  nm_mpi_file_t*p_file = nm_mpi_handle_file_get(&nm_mpi_files, fh);
  if(p_file == NULL)
    return NM_MPI_ERROR_FILE_DEFAULT(MPI_ERR_FILE);
  int err = mpi_file_write_ordered_begin(fh, buf, count, datatype);
  if(err == MPI_SUCCESS)
    {
      err = mpi_file_write_ordered_end(fh, buf, status);
    }
  return NM_MPI_ERROR_FILE(p_file, err);
}

int mpi_file_write_ordered_begin(MPI_File fh, const void*buf, int count, MPI_Datatype datatype)
{
  nm_mpi_file_t*p_file = nm_mpi_handle_file_get(&nm_mpi_files, fh);
  if(p_file == NULL)
    return NM_MPI_ERROR_FILE_DEFAULT(MPI_ERR_FILE);
  nm_mpi_file_ordered_acquire(p_file);
  nm_mpi_file_shared_acquire(p_file);
  int err = nm_mpi_file_op_build_pending(fh, NM_MPI_FILE_OP_WRITE, -2, (void*)buf, count, datatype);
  return NM_MPI_ERROR_FILE(p_file, err);
}

int mpi_file_write_ordered_end(MPI_File fh, const void*buf, MPI_Status*status)
{
  nm_mpi_file_t*p_file = nm_mpi_handle_file_get(&nm_mpi_files, fh);
  if(p_file == NULL)
    return NM_MPI_ERROR_FILE_DEFAULT(MPI_ERR_FILE);
  nm_mpi_file_op_exec_pending(fh, status);
  nm_mpi_file_shared_release(p_file);
  nm_mpi_file_ordered_release(p_file);
  return MPI_SUCCESS;
}

int mpi_file_read_ordered_begin(MPI_File fh, void*buf, int count, MPI_Datatype datatype)
{
  nm_mpi_file_t*p_file = nm_mpi_handle_file_get(&nm_mpi_files, fh);
  if(p_file == NULL)
    return NM_MPI_ERROR_FILE_DEFAULT(MPI_ERR_FILE);
  nm_mpi_file_ordered_acquire(p_file);
  nm_mpi_file_shared_acquire(p_file);
  int err = nm_mpi_file_op_build_pending(fh, NM_MPI_FILE_OP_READ, -2, (void*)buf, count, datatype);
  return NM_MPI_ERROR_FILE(p_file, err);
}

int mpi_file_read_ordered_end(MPI_File fh, void*buf, MPI_Status*status)
{
  nm_mpi_file_t*p_file = nm_mpi_handle_file_get(&nm_mpi_files, fh);
  if(p_file == NULL)
    return NM_MPI_ERROR_FILE_DEFAULT(MPI_ERR_FILE);
  nm_mpi_file_op_exec_pending(fh, status);
  nm_mpi_file_shared_release(p_file);
  nm_mpi_file_ordered_release(p_file);
  return MPI_SUCCESS;
}


/* ** Non-blocking IO ************************************** */

nm_mpi_request_t*nm_mpi_file_request_alloc(nm_mpi_count_t count, MPI_Datatype datatype)
{
  struct nm_mpi_request_s*p_req = nm_mpi_request_alloc();
  nm_mpi_datatype_t*p_datatype = nm_mpi_datatype_get(datatype);
  assert(p_datatype != NULL);
  p_req->request_type = NM_MPI_REQUEST_FILE;
  p_req->count = count;
  nm_mpi_request_set_datatype(p_req, p_datatype);
  return p_req;
}

int mpi_file_iwrite(MPI_File fh, const void*buf, int count, MPI_Datatype datatype, MPI_Request*request)
{
  nm_mpi_file_t*p_file = nm_mpi_handle_file_get(&nm_mpi_files, fh);
  if(p_file == NULL)
    return NM_MPI_ERROR_FILE_DEFAULT(MPI_ERR_FILE);
  struct nm_mpi_file_op_s*p_op = padico_malloc(sizeof(struct nm_mpi_file_op_s));
  p_op->op = NM_MPI_FILE_OP_NONE;
  int err = nm_mpi_file_op_build(fh, NM_MPI_FILE_OP_WRITE, -1, (void*)buf, count, datatype, p_op);
  if(err == MPI_SUCCESS)
    {
      struct nm_mpi_request_s*p_req = nm_mpi_file_request_alloc(count, datatype);
      p_req->p_file_op = p_op;
      *request = p_req->id;
    }
  else
    {
      *request = MPI_REQUEST_NULL;
    }
  return NM_MPI_ERROR_FILE(p_file, err);
}

int mpi_file_iread(MPI_File fh, void*buf, int count, MPI_Datatype datatype, MPI_Request*request)
{
  nm_mpi_file_t*p_file = nm_mpi_handle_file_get(&nm_mpi_files, fh);
  if(p_file == NULL)
    return NM_MPI_ERROR_FILE_DEFAULT(MPI_ERR_FILE);
  struct nm_mpi_file_op_s*p_op = padico_malloc(sizeof(struct nm_mpi_file_op_s));
  p_op->op = NM_MPI_FILE_OP_NONE;
  int err = nm_mpi_file_op_build(fh, NM_MPI_FILE_OP_READ, -1, (void*)buf, count, datatype, p_op);
  if(err == MPI_SUCCESS)
    {
      struct nm_mpi_request_s*p_req = nm_mpi_file_request_alloc(count, datatype);
      p_req->p_file_op = p_op;
      *request = p_req->id;
    }
  else
    {
      *request = MPI_REQUEST_NULL;
    }
  return NM_MPI_ERROR_FILE(p_file, err);
}

int mpi_file_iwrite_at(MPI_File fh, MPI_Offset offset, const void*buf,
                       int count, MPI_Datatype datatype, MPI_Request*request)
{
  nm_mpi_file_t*p_file = nm_mpi_handle_file_get(&nm_mpi_files, fh);
  if(p_file == NULL)
    return NM_MPI_ERROR_FILE_DEFAULT(MPI_ERR_FILE);
  struct nm_mpi_file_op_s*p_op = padico_malloc(sizeof(struct nm_mpi_file_op_s));
  p_op->op = NM_MPI_FILE_OP_NONE;
  int err = nm_mpi_file_op_build(fh, NM_MPI_FILE_OP_WRITE, offset, (void*)buf, count, datatype, p_op);
  if(err == MPI_SUCCESS)
    {
      struct nm_mpi_request_s*p_req = nm_mpi_file_request_alloc(count, datatype);
      p_req->p_file_op = p_op;
      *request = p_req->id;
    }
  else
    {
      *request = MPI_REQUEST_NULL;
    }
  return NM_MPI_ERROR_FILE(p_file, err);
}
int mpi_file_iread_at(MPI_File fh, MPI_Offset offset, void*buf, int count,
                      MPI_Datatype datatype, MPI_Request*request)
{
  nm_mpi_file_t*p_file = nm_mpi_handle_file_get(&nm_mpi_files, fh);
  if(p_file == NULL)
    return NM_MPI_ERROR_FILE_DEFAULT(MPI_ERR_FILE);
  struct nm_mpi_file_op_s*p_op = padico_malloc(sizeof(struct nm_mpi_file_op_s));
  p_op->op = NM_MPI_FILE_OP_NONE;
  int err = nm_mpi_file_op_build(fh, NM_MPI_FILE_OP_READ, offset, buf, count, datatype, p_op);
  if(err == MPI_SUCCESS)
    {
      struct nm_mpi_request_s*p_req = nm_mpi_file_request_alloc(count, datatype);
      p_req->p_file_op = p_op;
      *request = p_req->id;
    }
  else
    {
      *request = MPI_REQUEST_NULL;
    }
  return NM_MPI_ERROR_FILE(p_file, err);
}


/* ********************************************************* */

/* ** not implemented */

int mpi_register_datarep(const char*datarep,
                         MPI_Datarep_conversion_function*read_conversion_fn,
                         MPI_Datarep_conversion_function*write_conversion_fn,
                         MPI_Datarep_extent_function*dtype_file_extent_fn,
                         void*extra_state)
{
  NM_MPI_WARNING("%s: not implemented.\n", __func__);
  return NM_MPI_ERROR_WORLD(MPI_ERR_UNSUPPORTED_OPERATION);
}

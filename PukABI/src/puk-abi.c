/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @ingroup PukABI
 */

/** @note No other include file is allowed! */
#include "Puk-ABI-internals.h"

#include <stdarg.h>

struct puk_spinlock_s puk_spinlock =
  {
    .acquire = NULL,
    .release = NULL,
    .trylock = NULL
  };


/* ********************************************************* */
/* ** Stub and wrapper builders **************************** */

/* ** __ Aliasing rules for PukABI __
 *
 * 'foo'            is an alias for '__puk_abi_foo'
 * 'foo_func_t'     is the type of function 'foo'
 * '__puk_abi_desc__foo__' is a "struct puk_abi_sym_s" describing symbol 'foo'
 * '__puk_abi_stub__foo__' is a stub with automatic selection for real/virtual
 * '__puk_abi_wrap__foo' is a wrapper for real+lock
 */

#define _MK_ALIAS(NAME, ALIAS) \
  extern __typeof (NAME) ALIAS __attribute__ ((alias (_TOSTRING(NAME))));

/* ** lazy init */
int puk_abi_init_done = 0;
static inline void puk_abi_check_init(void)
{
  if(!puk_abi_init_done)
    {
      puk_abi_preinit();
      puk_abi_init_done = 1;
      puk_abi_trace("PukABI: lazy init done.\n");
    }
}

/** Generates symbol stub-
 *  function version
 * OPTION: bitmap-
 * OPTION & 0x01: use_real
 * OPTION & 0x02: use_vararg
 */
#define _PUKABI_MK_SYM_STUBF(SYM, RETTYPE, ARGSDECL, ARGSUSE, PREPROCESSING, POSTPROCESSING, OPTIONS) \
  RETTYPE PUK_ABI_SYM_STUB(SYM) ARGSDECL { \
    puk_abi_check_init(); \
    RETTYPE result; \
    const int _options = (OPTIONS); \
    const int _will_use_real = _options; \
    if(_will_use_real){ \
      const SYM##_func_t fun = (SYM##_func_t)(PUK_ABI_SYM_REAL(SYM)); \
      puk_spinlock_acquire(); \
      PREPROCESSING \
      result = ((*fun) ARGSUSE); \
      PUK_ABI_ERRNO_INHERIT; \
      POSTPROCESSING \
      puk_spinlock_release(); \
    } else { \
      const SYM##_func_t fun = (SYM##_func_t)(PUK_ABI_SYM_VIRTUAL(SYM)); \
      PREPROCESSING \
      result = ((*fun) ARGSUSE); \
      POSTPROCESSING \
    } \
    return result; }

#define _PUKABI_MK_SYM_WRAPF(SYM, RETTYPE, ARGSDECL, ARGSUSE) \
  RETTYPE PUK_ABI_SYM_WRAP(SYM) ARGSDECL { \
    puk_abi_check_init(); \
    RETTYPE result; \
    const SYM ## _func_t fun = (SYM ## _func_t)(PUK_ABI_SYM_REAL(SYM)); \
    puk_spinlock_acquire(); \
    result = ((*fun) ARGSUSE); \
    PUK_ABI_ERRNO_INHERIT; \
    puk_spinlock_release(); \
    return result; }

#define _PUKABI_MK_SYM_STUBV(SYM, RETTYPE, ARGSDECL, PROCESSING, OPTIONS) \
  RETTYPE PUK_ABI_SYM_STUB(SYM) ARGSDECL { \
    puk_abi_check_init(); \
    RETTYPE _result; \
    const int _options = (OPTIONS); \
    const int _will_use_real = _options; \
    if(_will_use_real){ \
      const SYM##_func_t PUK_UNUSED _fun = (SYM##_func_t)(PUK_ABI_SYM_REAL(SYM)); \
      puk_spinlock_acquire(); \
      PUK_ABI_ERRNO_INHERIT; \
      PROCESSING \
      puk_spinlock_release(); \
    } else { \
      const SYM##_func_t PUK_UNUSED _fun = (SYM##_func_t)(PUK_ABI_SYM_VIRTUAL(SYM)); \
      PROCESSING \
    } \
    return _result; \
  }
#define _PUKABI_MK_SYM_WRAPV1(SYM, RETTYPE, ARGSDECL, ARG1) \
  RETTYPE PUK_ABI_SYM_WRAP(SYM) ARGSDECL { \
    puk_abi_check_init(); \
    va_list _ap; \
    va_start(_ap, ARG1); \
    long _a1 = va_arg(_ap, long); \
    long _a2 = va_arg(_ap, long); \
    va_end(_ap); \
    const SYM ## _func_t fun = (SYM ## _func_t)(PUK_ABI_SYM_REAL(SYM)); \
    puk_spinlock_acquire(); \
    RETTYPE _result = (*fun)(ARG1, _a1, _a2); \
    PUK_ABI_ERRNO_INHERIT; \
    puk_spinlock_release(); \
    return _result; }
#define _PUKABI_MK_SYM_WRAPV2(SYM, RETTYPE, ARGSDECL, ARG1, ARG2) \
  RETTYPE PUK_ABI_SYM_WRAP(SYM) ARGSDECL { \
    puk_abi_check_init(); \
    va_list _ap; \
    va_start(_ap, ARG2); \
    long _a1 = va_arg(_ap, long); \
    long _a2 = va_arg(_ap, long); \
    va_end(_ap); \
    const SYM ## _func_t fun = (SYM ## _func_t)(PUK_ABI_SYM_REAL(SYM)); \
    puk_spinlock_acquire(); \
    RETTYPE _result = (*fun)(ARG1, ARG2, _a1, _a2); \
    PUK_ABI_ERRNO_INHERIT; \
    puk_spinlock_release(); \
    return _result; }
#define _PUKABI_MK_SYM_WRAPV3(SYM, RETTYPE, ARGSDECL, ARG1, ARG2, ARG3)         \
  RETTYPE PUK_ABI_SYM_WRAP(SYM) ARGSDECL { \
    puk_abi_check_init(); \
    va_list _ap; \
    va_start(_ap, ARG3); \
    long _a1 = va_arg(_ap, long); \
    long _a2 = va_arg(_ap, long); \
    va_end(_ap); \
    const SYM ## _func_t fun = (SYM ## _func_t)(PUK_ABI_SYM_REAL(SYM)); \
    puk_spinlock_acquire(); \
    RETTYPE _result = (*fun)(ARG1, ARG2, ARG3, _a1, _a2);       \
    PUK_ABI_ERRNO_INHERIT; \
    puk_spinlock_release(); \
    return _result; }

/** Generates symbol stub-
 *  procedure version */
#define _PUKABI_MK_SYM_STUBP(SYM, RETTYPE, ARGSDECL, ARGSUSE, PREPROCESSING, POSTPROCESSING, USEREAL) \
  RETTYPE PUK_ABI_SYM_STUB(SYM) ARGSDECL { \
    puk_abi_check_init(); \
    if(USEREAL){ \
      const SYM##_func_t fun = (SYM##_func_t)(PUK_ABI_SYM_REAL(SYM)); \
      puk_spinlock_acquire(); \
      PREPROCESSING \
      ((*fun) ARGSUSE); \
      PUK_ABI_ERRNO_INHERIT; \
      POSTPROCESSING \
      puk_spinlock_release(); \
    } else { \
      const SYM##_func_t fun = (SYM##_func_t)(PUK_ABI_SYM_VIRTUAL(SYM)); \
      PREPROCESSING \
      ((*fun) ARGSUSE); \
      POSTPROCESSING \
    } \
  }

#define _PUKABI_MK_SYM_WRAPP(SYM, RETTYPE, ARGSDECL, ARGSUSE) \
  RETTYPE PUK_ABI_SYM_WRAP(SYM) ARGSDECL { \
    puk_abi_check_init(); \
    const SYM ## _func_t fun = (SYM ## _func_t)(PUK_ABI_SYM_REAL(SYM)); \
    puk_spinlock_acquire(); \
    ((*fun) ARGSUSE); \
    PUK_ABI_ERRNO_INHERIT; \
    puk_spinlock_release(); }


/** Declares a symbol descriptor structure. */
#define _PUKABI_MK_SYM_DESC(SYM, INIT) \
  static struct puk_abi_sym_s PUK_ABI_SYM_DESCNAME(SYM) = \
  { .ptr_real = (INIT), .ptr_virtual = NULL, .name =_TOSTRING(SYM) }; \
  extern struct puk_abi_sym_s*PUK_ABI_SYM_GETDESC(SYM)(void) \
  { return &(PUK_ABI_SYM_DESCNAME(SYM)); }

/** Creates a symbol 'foo' as an alias for '__puk_abi_stub__foo'
 */
#define _PUKABI_MK_SYM_ALIAS(SYM) \
  _MK_ALIAS(PUK_ABI_SYM_STUB(SYM), SYM)

/** creates a type, descriptor, stub, wrapper and alias for a symbol- "function" flavor.
 */
#define MKSYMFX(SYM, RETTYPE, ARGSDECL, ARGSUSE, PREPROCESSING, POSTPROCESSING, USEREAL) \
  PUK_ABI_MKTYPE(SYM, RETTYPE, ARGSDECL); \
  _PUKABI_MK_SYM_DESC(SYM, NULL) \
  _PUKABI_MK_SYM_STUBF(SYM, RETTYPE, ARGSDECL, ARGSUSE, PREPROCESSING, POSTPROCESSING, USEREAL) \
  _PUKABI_MK_SYM_WRAPF(SYM, RETTYPE, ARGSDECL, ARGSUSE) \
  _PUKABI_MK_SYM_ALIAS(SYM)

/** creates a type, descriptor, stub, wrapper and alias for a symbol- "procedure" flavor.
 */
#define MKSYMPX(SYM, RETTYPE, ARGSDECL, ARGSUSE, PREPROCESSING, POSTPROCESSING, USEREAL) \
  PUK_ABI_MKTYPE(SYM, RETTYPE, ARGSDECL); \
  _PUKABI_MK_SYM_DESC(SYM, NULL) \
  _PUKABI_MK_SYM_STUBP(SYM, RETTYPE, ARGSDECL, ARGSUSE, PREPROCESSING, POSTPROCESSING, USEREAL) \
  _PUKABI_MK_SYM_WRAPP(SYM, RETTYPE, ARGSDECL, ARGSUSE) \
  _PUKABI_MK_SYM_ALIAS(SYM)

/** creates a type, descriptor, stub, wrapper and alias for a symbol- "varargs with 1 fixed arg" flavor.
 */
#define MKSYMVX1(SYM, RETTYPE, ARGSDECL, ARG1, PROCESSING, USEREAL) \
  PUK_ABI_MKTYPE(SYM, RETTYPE, ARGSDECL); \
  _PUKABI_MK_SYM_DESC(SYM, NULL) \
  _PUKABI_MK_SYM_STUBV(SYM, RETTYPE, ARGSDECL, PROCESSING, USEREAL) \
  _PUKABI_MK_SYM_WRAPV1(SYM, RETTYPE, ARGSDECL, ARG1) \
  _PUKABI_MK_SYM_ALIAS(SYM)
/** creates a type, descriptor, stub, wrapper and alias for a symbol- "varargs with 2 fixed args" flavor.
 */
#define MKSYMVX2(SYM, RETTYPE, ARGSDECL, ARG1, ARG2, PROCESSING, USEREAL) \
  PUK_ABI_MKTYPE(SYM, RETTYPE, ARGSDECL); \
  _PUKABI_MK_SYM_DESC(SYM, NULL) \
  _PUKABI_MK_SYM_STUBV(SYM, RETTYPE, ARGSDECL, PROCESSING, USEREAL) \
  _PUKABI_MK_SYM_WRAPV2(SYM, RETTYPE, ARGSDECL, ARG1, ARG2) \
  _PUKABI_MK_SYM_ALIAS(SYM)
/** creates a type, descriptor, stub, wrapper and alias for a symbol- "varargs with 3 fixed args" flavor.
 */
#define MKSYMVX3(SYM, RETTYPE, ARGSDECL, ARG1, ARG2, ARG3, PROCESSING, USEREAL) \
  PUK_ABI_MKTYPE(SYM, RETTYPE, ARGSDECL); \
  _PUKABI_MK_SYM_DESC(SYM, NULL) \
  _PUKABI_MK_SYM_STUBV(SYM, RETTYPE, ARGSDECL, PROCESSING, USEREAL) \
  _PUKABI_MK_SYM_WRAPV3(SYM, RETTYPE, ARGSDECL, ARG1, ARG2, ARG3)   \
  _PUKABI_MK_SYM_ALIAS(SYM)

/** creates a stub for an unsupported symbol.
 */
#define MKSYMNOSUPPORT(SYM) \
  void SYM(void) { puk_abi_fatal("# PukABI: symbol '%s' not supported.\n", #SYM); }


/* *** errno declarations ********************************** */

#ifndef puk_abi_errno_sym
#  error "puk_abi_errno_sym not defined."
#endif

static int puk_abi_errno_use_virtual = 0;
static int puk_abi_errno_value = 0;
static int*puk_abi_errno_base_handler(void)
{
  return &puk_abi_errno_value;
}

typedef int*(*puk_abi_errno_fun_t)(void);
extern int* PUK_ABI_SYM_STUB(puk_abi_errno_sym)(void); /* forward declaration for some picky compilers */
_MK_ALIAS(PUK_ABI_SYM_STUB(puk_abi_errno_sym), puk_abi_errno_stub);
_MK_ALIAS(PUK_ABI_SYM_STUB(puk_abi_errno_sym), PUK_ABI_SYM_WRAP(puk_abi_errno_sym));
_PUKABI_MK_SYM_DESC(puk_abi_errno_sym, &puk_abi_errno_base_handler);
_PUKABI_MK_SYM_ALIAS(puk_abi_errno_sym);


/* *** Generate symbols ************************************ */

/* All standard interception symbols are generated *here*.
 */
#include "Puk-ABI-symbols.h"


/* *** Aliasing ******************************************** */

/* generated aliasing rule */
#include "Puk-ABI-alias.h"

/* additional aliasing for malloc & co. */

#ifdef PUKABI_ENABLE_MEM
/* for backward compatibility, define 'cfree' as an alias for 'free' */
/* _MK_ALIAS(free, cfree); */
/* for some memory operations- intercept even internals */
_MK_ALIAS(PUK_ABI_SYM_STUB(malloc),  __malloc);
_MK_ALIAS(PUK_ABI_SYM_STUB(free),    __free);
_MK_ALIAS(PUK_ABI_SYM_STUB(free),    __cfree);
_MK_ALIAS(PUK_ABI_SYM_STUB(realloc), __realloc);
_MK_ALIAS(PUK_ABI_SYM_STUB(calloc),  __calloc);
#endif /* PUKABI_ENABLE_MEM */


/* *** errno  implementation *******************************
 *
 * 'puk_abi_errno_sym': #defined to the real __errno_location()
 * 'puk_abi_errno_stub': alias to use PUK_ABI_SYM_STUB(errno) from outside PukABI
 * 'puk_abi_errno_real': the *real* __errno_location(), supposedly not usefull for the end-user.
 */


extern int* PUK_ABI_SYM_STUB(puk_abi_errno_sym)(void)
{
  int*result;
  if(puk_abi_errno_use_virtual)
    {
      result = (*((PUK_ABI_SYM_FUNCTYPE(puk_abi_errno_sym))
                  (PUK_ABI_SYM_VIRTUAL(puk_abi_errno_sym))))();
    }
  else
    {
      const puk_abi_errno_fun_t _fun = (puk_abi_errno_fun_t)(PUK_ABI_SYM_REAL(puk_abi_errno_sym));
      result = (*_fun)();
    }
  return result;
}

extern void puk_abi_set_errno_handler(int*(*errno_handler)(void))
{
  PUK_ABI_SYM_DESCNAME(puk_abi_errno_sym).ptr_virtual = errno_handler;
  puk_abi_errno_use_virtual = 1;
  *(puk_abi_errno_stub()) = 0;
  PUK_ABI_SYM_DESCNAME(perror).ptr_virtual = &puk_abi_helper__perror;
}

extern void puk_abi_unset_errno_handler(void)
{
  /* revert to real errno from libc. It was already resolved in puk_abi_preinit. */
  puk_abi_errno_use_virtual = 0;
  PUK_ABI_SYM_DESCNAME(perror).ptr_virtual = NULL;
}

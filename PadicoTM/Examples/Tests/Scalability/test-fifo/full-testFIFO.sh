#!/bin/bash

if [ $# != 1 ]
then
    
    echo $#
    
    echo './full-testFIFO.sh <machine>'
    
else
    
    echo $#
    ./compilTestFifo.sh
    ./testFIFO.sh 0 $1
    ./testFIFO.sh 1 $1
    ./testFIFO.sh 2 $1
    ./testFIFO.sh 3 $1
    
fi
    


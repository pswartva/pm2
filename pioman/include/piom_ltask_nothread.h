/*
 * Pioman: a Generic I/O Manager
 * Copyright (C) 2008-2022 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#ifndef PIOM_LTASK_NOTHREAD_H
#define PIOM_LTASK_NOTHREAD_H

#ifndef PIOM_CONFIG_H
#  error "Cannot include this file directly. Please include <piom_private.h>."
#endif /* PIOM_CONFIG_H */

#ifndef PIOMAN_NOTHREAD
#error "inconsistency detected: PIOMAN_NOTHREAD not defined in piom_ltask_nothread.h"
#endif /* PIOMAN_NOTHREAD */

__PUK_SYM_INTERNAL void piom_ltask_nothread_init(void);
__PUK_SYM_INTERNAL void piom_ltask_nothread_exit(void);

__PUK_SYM_INTERNAL void piom_ltask_nothread_hook_preinvoke(void);
__PUK_SYM_INTERNAL void piom_ltask_nothread_hook_postinvoke(void);
__PUK_SYM_INTERNAL void piom_ltask_nothread_check_wait(void);

__PUK_SYM_INTERNAL struct piom_ltask_threadstate_s*piom_ltask_nothread_getthreadstate(void);

static inline struct piom_ltask_threadstate_s*piom_ltask_getthreadstate(void)
{
  return piom_ltask_nothread_getthreadstate();
}


#endif /* PIOM_LTASK_NOTHREAD_H */

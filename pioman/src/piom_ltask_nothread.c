/*
 * Pioman: a Generic I/O Manager
 * Copyright (C) 2008-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include "piom_private.h"

#include <Padico/Module.h>

PADICO_MODULE_HOOK(pioman);

#ifdef PIOMAN_NOTHREAD

static struct piom_ltask_threadstate_s piom_nothread_threadstate = { .scheduling_queue = NULL };

void piom_ltask_nothread_init(void)
{
}

void piom_ltask_nothread_exit(void)
{
}

struct piom_ltask_threadstate_s*piom_ltask_nothread_getthreadstate(void)
{
  return &piom_nothread_threadstate;
}

int piom_ltask_submit_in_lwp(struct piom_ltask*task)
{
  return -1; /* unsupported by this backend */
}

#endif /* PIOMAN_NOTHREAD */

dnl -*- mode: Autoconf;-*-

# ## detect padico building-tools
AC_DEFUN([AC_PADICO_BUILDING_TOOLS],
         [
           AC_MSG_CHECKING([for common building-tools])
           if test -r ${srcdir}/building-tools/common_rules.mk.in; then
             building_tools=${srcdir}/building-tools/
           elif test -r ${srcdir}/../building-tools/common_rules.mk.in; then
             building_tools=${srcdir}/../building-tools/
           else
             AC_MSG_ERROR([missing building-tools directory in srcdir: ${srcdir}])
           fi
           AC_MSG_RESULT([${building_tools}])
           AC_SUBST([building_tools])
         ])

# ## set package name
AC_DEFUN([AC_PADICO_PACKAGE],
         [
           AC_MSG_CHECKING([for package name])
           package_name="$1"
           package_short_name="$2"
           if test "x${package_short_name}" = "x"; then
             package_short_name="${package_name}"
           fi
           PACKAGE_NAME="${package_name}"
           if test "x${PACKAGE_NAME}" = "x"; then
             AC_MSG_ERROR([ cannot find package name. ])
           else
             AC_MSG_RESULT([${package_name} (${package_short_name})])
           fi
           AC_SUBST([package_name])
           AC_SUBST([package_short_name])
         ])

# ## set package root
AC_DEFUN([AC_PADICO_PACKAGE_ROOT],
         [
           AC_REQUIRE([AC_PADICO_PACKAGE])
           AC_MSG_CHECKING([for package root])
           padico_root=${prefix}
           AC_MSG_RESULT([${padico_root})])
           AC_SUBST([padico_root])
         ])

# ## check build directory
AC_DEFUN([AC_PADICO_BUILDDIR],
         [
           AC_REQUIRE([AC_PADICO_PACKAGE])
           AC_MSG_CHECKING([for package build directory])
           padico_builddir=`pwd`
           AC_SUBST(padico_builddir)
           AC_MSG_RESULT(${padico_builddir})
         ])
# ## check source directory
AC_DEFUN([AC_PADICO_SRCDIR],
         [
           AC_REQUIRE([AC_PADICO_PACKAGE])
           AC_MSG_CHECKING([for package ${PACKAGE_NAME} source directory])
           case ${srcdir} in
             /*) padico_srcdir=${srcdir} ;;
             *)  padico_srcdir="`pwd`/${srcdir}" ;;
           esac
           AC_SUBST(padico_srcdir)
           AC_MSG_RESULT(${padico_srcdir})
         ])

# ## Version and version strings
AC_DEFUN([AC_PADICO_VERSION],
         [
           AC_REQUIRE([AC_PADICO_BUILDING_TOOLS])
           AC_REQUIRE([AC_PADICO_PACKAGE])
           AC_REQUIRE([AC_PADICO_SRCDIR])
           AC_MSG_CHECKING([${PACKAGE_NAME} version])
           version=`${building_tools}/package-version.sh ${building_tools} ${padico_srcdir}`
           padico_VERSION=${version}
           padico_VERSION_STRING="${PACKAGE_NAME}-${padico_VERSION}"
           padico_VERSION_ALPHANUM=`echo ${PADICO_VERSION} | tr -c "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" "_"`
           padico_BUILD_STRING="${padico_VERSION_STRING} (build `date`)"

           AC_MSG_RESULT([ ${padico_VERSION_STRING} ])

           AC_SUBST([$1]_VERSION, ${padico_VERSION})
           AC_SUBST([$1]_VERSION_ALPHANUM, ${padico_VERSION_ALPHANUM})
           AC_DEFINE_UNQUOTED([$1]_BUILD_STRING, "${padico_BUILD_STRING}", [date of the build])
           AC_DEFINE_UNQUOTED([$1]_VERSION_STRING, "${padico_VERSION_STRING}", [[$1] version as string])
         ])

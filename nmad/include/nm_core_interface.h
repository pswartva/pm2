/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


#ifndef NM_CORE_INTERFACE_H
#define NM_CORE_INTERFACE_H

/** @file */

#include <nm_public.h>
#include <nm_log.h>
#include <Padico/Puk.h>
#include <sys/uio.h>

#ifdef PIOMAN
#include <pioman.h>
#else
#include <pthread.h>
#endif

/** @defgroup core_interface nmad core interface
 * This is the interface of the NewMad core. All other
 * include files in the core are internal and shouldn't be
 * included outside of the core.
 *
 * End-users are not supposed to use directly this core
 * interface. They should use instead a higher level interface
 * such as @ref sr_interface (sendrecv), @ref pack_interface (pack),
 * or @ref mpi_interface (MadMPI) for messaging, and
 * @ref launcher_interface (launcher) for init and connection establishment.
 * @{
 */

/* ** Core init ******************************************** */

typedef struct nm_core*nm_core_t;

puk_component_t nm_core_component_load(const char*entity, const char*name);

int nm_core_init(nm_core_t *pp_core);

int nm_core_set_strategy(nm_core_t p_core, puk_component_t strategy);

int nm_core_exit(nm_core_t p_core);

/** disable schedopt for raw driver use */
void nm_core_schedopt_disable(nm_core_t p_core);

/** @private exposed here for inlining; do not use this value, use the accessor nm_core_get_singleton() */
extern struct nm_core_internal_s
{
  nm_core_t p_core_singleton;
} nm_core_internal;

__attribute__((pure))
static inline nm_core_t nm_core_get_singleton(void)
{
  return nm_core_internal.p_core_singleton;
}


/* ** Drivers ********************************************** */

/** a nmad driver; opaque type for the user */
typedef struct nm_drv_s*nm_drv_t;

PUK_VECT_TYPE(nm_drv, nm_drv_t);

int nm_core_driver_load_init(nm_core_t p_core, puk_component_t driver, nm_trk_kind_t kind,
                             nm_drv_t *pp_drv, const char**p_url);


/* ** Gates ************************************************ */

/** Init a new gate, using the given set of drivers */
nm_gate_t nm_core_gate_new(nm_core_t p_core, nm_drv_vect_t*p_drvs);
/** start connection process on given gate/trk */
void nm_core_gate_connect_async(nm_core_t p_core, nm_gate_t gate, nm_drv_t p_drv, nm_trk_id_t trk_id, const char*url);
/** wait for connection completion */
void nm_core_gate_connect_wait(nm_core_t p_core, struct nm_trk_s*p_trk);


/* ** Threads ********************************************** */

enum nm_thread_level_e
  {
    NM_THREAD_SINGLE     = 0,
    NM_THREAD_FUNNELED   = 1,
    NM_THREAD_SERIALIZED = 2,
    NM_THREAD_MULTIPLE   = 3
  };
typedef enum nm_thread_level_e nm_thread_level_t;

/** Get the current thread level
 */
nm_thread_level_t nm_core_get_thread_level(nm_core_t);

/** Sets the thread level before nm core init.
 * @note should be called _before_ init
 */
void nm_core_set_thread_level(nm_thread_level_t);

/* ** Progression ****************************************** */

int nm_schedule(nm_core_t p_core);

/* ** Status *********************************************** */

#ifdef PIOMAN
/** status bits of pack/unpack requests */
typedef piom_cond_value_t nm_status_t;
/** status with synchronization (wait/signal) */
typedef piom_cond_t nm_cond_status_t;
#else /* PIOMAN */
/** status bits of pack/unpack requests */
typedef uint32_t nm_status_t;
/** status with synchronization (wait/signal) */
typedef nm_status_t nm_cond_status_t;
#endif /* PIOMAN */

/* ** status and flags, used in pack/unpack requests and events */

/** empty request */
#define NM_STATUS_NONE                     ((nm_status_t)0x00000000)
/** request initialized, not sent yet */
#define NM_STATUS_PACK_INIT                ((nm_status_t)0x00000001)
/** request initialized, not sent yet */
#define NM_STATUS_UNPACK_INIT              ((nm_status_t)0x00000002)
/** sending operation has completed */
#define NM_STATUS_PACK_COMPLETED           ((nm_status_t)0x00000004)
/** unpack operation has completed */
#define NM_STATUS_UNPACK_COMPLETED         ((nm_status_t)0x00000008)
/** data or rdv has arrived, with no matching unpack */
#define NM_STATUS_UNEXPECTED               ((nm_status_t)0x00000010)
/** unpack operation has been cancelled */
#define NM_STATUS_UNPACK_CANCELLED         ((nm_status_t)0x00000020)
/** sending operation is in progress */
#define NM_STATUS_PACK_POSTED              ((nm_status_t)0x00000040)
/** unpack operation is in progress */
#define NM_STATUS_UNPACK_POSTED            ((nm_status_t)0x00000080)
/** ack received for the given pack */
#define NM_STATUS_ACK_RECEIVED             ((nm_status_t)0x00000100)
/** first byte of data arrived, not unpacked yet- event triggered only if unpack is posted without data spec */
#define NM_STATUS_UNPACK_DATA0             ((nm_status_t)0x00000200)
/** size of data is known (last chunk of data arrived), not unpacked yet- event triggered only if unpack is posted without data spec */
#define NM_STATUS_UNPACK_DATA_SIZE         ((nm_status_t)0x00000400)
/** request is finalized, may be freed */
#define NM_STATUS_FINALIZED                ((nm_status_t)0x00000800)
/** request is in error; see error state in request */
#define NM_STATUS_ERROR                    ((nm_status_t)0x00001000)
/** msg size already sent; used only if flag pack_partitioned is set */
#define NM_STATUS_PACK_MSG_SIZE            ((nm_status_t)0x00002000)
/** flag unpack request as prefetched */
#define NM_STATUS_UNPACK_PREFETCHED        ((nm_status_t)0x00004000)

/** mask to catch all bits of status */
#define NM_STATUS_MASK_FULL                ((nm_status_t)-1)


/** pack/unpack flags */
typedef uint32_t nm_req_flag_t;

/** no flag set */
#define NM_REQ_FLAG_NONE                   ((nm_req_flag_t)0x00000000)
/** flag pack as synchronous (i.e. request the receiver to send an ack) */
#define NM_REQ_FLAG_PACK_SYNCHRONOUS       ((nm_req_flag_t)0x00001000)
/** request is a pack */
#define NM_REQ_FLAG_PACK                   ((nm_req_flag_t)0x00002000)
/** request is an unpack */
#define NM_REQ_FLAG_UNPACK                 ((nm_req_flag_t)0x00004000)
/** flag unpack request as data information present */
#define NM_REQ_FLAG_UNPACK_DATA_INFO       ((nm_req_flag_t)0x00008000)
/** flag unpack request as matching information present */
#define NM_REQ_FLAG_UNPACK_MATCHING_INFO   ((nm_req_flag_t)0x00010000)
/** flag unpack request needs prefetching */
#define NM_REQ_FLAG_UNPACK_PREFETCHING     ((nm_req_flag_t)0x00020000)
/** request submited in wildcard queue */
#define NM_REQ_FLAG_MATCHING_WILDCARD      ((nm_req_flag_t)0x00100000)
/** request matching performed by gate */
#define NM_REQ_FLAG_MATCHING_GATE          ((nm_req_flag_t)0x00200000)
/** request matching performed by tag */
#define NM_REQ_FLAG_MATCHING_TAG           ((nm_req_flag_t)0x00400000)
/** request matching by gate + tag */
#define NM_REQ_FLAG_MATCHING_FULL          ((nm_req_flag_t)0x00800000)
/** statuses NM_STATUS_UNPACK_COMPLETED and NM_STATUS_FINALIZED will be set separately */
#define NM_REQ_FLAG_FINALIZE_LATER         ((nm_req_flag_t)0x01000000)
/** unpack request is partitioned; each chunk whill be notified separately */
#define NM_REQ_FLAG_UNPACK_PARTITIONED     ((nm_req_flag_t)0x02000000)
/** pack request is partitioned; send a separate msg header before data chunks */
#define NM_REQ_FLAG_PACK_PARTITIONED       ((nm_req_flag_t)0x04000000)

/** flags for req_chunk */
typedef uint32_t nm_req_chunk_flag_t;

/** no flag set */
#define NM_REQ_CHUNK_FLAG_NONE             ((nm_req_chunk_flag_t)0x00000000)
/** flag req_chunk as short */
#define NM_REQ_CHUNK_FLAG_SHORT            ((nm_req_chunk_flag_t)0x00020000)
/** flatten data as contiguous block before send */
#define NM_REQ_CHUNK_FLAG_USE_COPY         ((nm_req_chunk_flag_t)0x00080000)
/** use iterator-based data description in pw */
#define NM_REQ_CHUNK_FLAG_DATA_ITERATOR    ((nm_req_chunk_flag_t)0x00100000)


/* ** tags ************************************************* */

/** a session hashcode in tags, used to multiplex sessions */
typedef uint32_t nm_session_hash_t;

/** mask for all sessions */
#define NM_CORE_TAG_HASH_FULL ((nm_session_hash_t)0xFFFFFFFF)

/** An internal tag */
struct nm_core_tag_s
{
  nm_tag_t tag;               /**< the user-supplied tag */
  nm_session_hash_t hashcode; /**< the session hashcode */
} __attribute__((packed));

typedef struct nm_core_tag_s nm_core_tag_t;

#define NM_CORE_TAG_MASK_FULL ((nm_core_tag_t){ .tag = NM_TAG_MASK_FULL, .hashcode = NM_CORE_TAG_HASH_FULL })
#define NM_CORE_TAG_NONE      ((nm_core_tag_t){ .tag = 0, .hashcode = 0x0 })

static inline nm_core_tag_t nm_core_tag_build(nm_session_hash_t hashcode, nm_tag_t tag)
{
  nm_core_tag_t core_tag;
  core_tag.tag = tag;
  core_tag.hashcode = hashcode;
  return core_tag;
}
static inline nm_tag_t nm_core_tag_get_tag(nm_core_tag_t core_tag)
{
  return core_tag.tag;
}
static inline nm_session_hash_t nm_core_tag_get_hashcode(nm_core_tag_t core_tag)
{
  return core_tag.hashcode;
}


/* ** Event notification *********************************** */

/** An event, generated by the NewMad core. */
struct nm_core_event_s
{
  nm_status_t status; /**< status flags- describe the event */
  nm_gate_t p_gate;
  nm_core_tag_t tag;
  nm_seq_t seq;
  nm_len_t len;
  struct nm_req_s*p_req; /**< the request that matched the event- NULL in case of unexpected packets */
};

/** an event notifier, fired upon status transition */
typedef void (*nm_core_event_notifier_t)(const struct nm_core_event_s*const event, void*ref);

/** matching info for global monitors */
struct nm_core_event_matching_s
{
  nm_gate_t p_gate;       /**< the gate to listen to, or NM_ANY_GATE for any */
  nm_core_tag_t tag;      /**< the tag to listen too */
  nm_core_tag_t tag_mask; /**< the mask to apply before comparing tags (only bits set in mask will be checked) */
};

/** generic monitor, used for requests and for global events (with matching) */
struct nm_monitor_s
{
  nm_core_event_notifier_t p_notifier; /**< notification function called to fire events */
  nm_status_t event_mask;              /**< mask applied to status to check whether to fire events */
  void*ref;                            /**< opaque user-supplied pointer passed to notifier */
};

/** global monitor for status transitions */
struct nm_core_monitor_s
{
  struct nm_monitor_s monitor;               /**< the monitor to fire upon matching event */
  struct nm_core_event_matching_s matching;  /**< packet matching information */
};
/** Register an event monitor. */
void nm_core_monitor_add(nm_core_t p_core, struct nm_core_monitor_s*m);
/** Unregister an event monitor. */
void nm_core_monitor_remove(nm_core_t p_core, struct nm_core_monitor_s*m);
/** set a per-request monitor. Fire event immediately if pending */
void nm_core_req_monitor(struct nm_core*p_core, struct nm_req_s*p_req, struct nm_monitor_s monitor);


/** matches any event */
#define NM_EVENT_MATCHING_ANY ((struct nm_core_event_matching_s){ .p_gate = NM_ANY_GATE, .tag = NM_CORE_TAG_NONE, .tag_mask = NM_CORE_TAG_NONE })

#define NM_MONITOR_NULL ((struct nm_monitor_s){ .p_notifier = NULL, .event_mask = 0, .ref = NULL })

#define NM_CORE_MONITOR_NULL ((struct nm_core_monitor_s){ .monitor = NM_MONITOR_NULL, .matching = NM_EVENT_MATCHING_ANY })


/* ** Packs/unpacks **************************************** */

struct nm_chunk_s
{
  nm_len_t chunk_offset;
  nm_len_t chunk_len;
};

/** containers for matching info, used for caching */
struct nm_matching_container_s
{
  struct nm_gtag_s*p_gtag;                    /**< cache of gtag */
  struct nm_matching_wildcard_s*p_wildcard;   /**< cache of matching wildcard */
  struct nm_matching_gsession_s*p_gsession;   /**< cache of matching gsession */
  struct nm_matching_tag_s*p_matching_tag;    /**< cache of matching tag */
};
#define NM_MATCHING_CONTAINER_NULL ((struct nm_matching_container_s) { NULL })


enum nm_core_task_kind_e
  {
   NM_CORE_TASK_NONE               = 0,
   NM_CORE_TASK_UNPACK_NEXT        = 1, /**< try to match the next unpack on the given gate/tag/gtag */
   NM_CORE_TASK_COMPLETED_PW       = 2, /**< process a completed pw */
   NM_CORE_TASK_COMPLETED_PREFETCH = 3, /**< prefetch completed; process RTR if received */
   NM_CORE_TASK_PACK_SUBMISSION    = 4, /**< process a submitted pack request */
   NM_CORE_TASK_RTR_SEND           = 5, /**< send a RTR once the large pw for recv has been posted */
   NM_CORE_TASK_HANDLER            = 6  /**< call a user handler, mainly for testing/benchmarking */
  };
typedef enum nm_core_task_kind_e nm_core_task_kind_t;

/** asynchronous tasks for nmad core.
 * These tasks are sheduled asynchronously and not immediately
 * in 2 cases:
 *   1. they need the core lock but are scheduled from a code that
 *      doesn't hold the lock (we avoid to acquire/release the lock
 *      too often).
 *   2. they trigger the processing of one more request or packet after
 *      a completion, which could lead to unbounded recursion.
 * Tasks are comprised of a kind and a parameter. They are combined in
 * a struct preallocated in the parameter.
 */
struct nm_core_task_s
{
  enum nm_core_task_kind_e kind;
  union
  {
    struct
    {
      struct nm_matching_container_s matching;
    } unpack_next;
    struct
    {
      struct nm_pkt_wrap_s*p_pw;
    } completed_pw;
    struct
    {
      struct nm_pkt_wrap_s*p_pw;
    } completed_prefetch;
    struct
    {
      struct nm_req_chunk_s*p_req_chunk;
    } pack_submission;
    struct
    {
      struct nm_pkt_wrap_s*p_pw;
    } rtr_send;
    struct
    {
      void (*p_handler)(void);
    } handler;
  } content;
};

PUK_LIST_DECLARE_TYPE(nm_req_chunk);

/** a chunk of request */
struct nm_req_chunk_s
{
  PUK_LIST_LINK(nm_req_chunk);
  struct nm_core_task_s core_task; /** link to insert the req chunk as a core task */
  struct nm_req_s*p_req;  /**< the request this chunk belongs to */
  nm_len_t chunk_len;     /**< length of the chunk */
  nm_len_t chunk_offset;  /**< offset of the chunk relative to the full data in the req */
  nm_proto_t proto_flags; /**< pre-computed proto flags */
  struct nm_data_properties_s chunk_props; /**< properties of the data chunk */
};

PUK_LIST_DECLARE_TYPE(nm_req);

/** a generic pack/unpack request */
struct nm_req_s
{
  PUK_LIST_LINK(nm_req);        /**< link to enqueue req in pending requests lists */
  nm_cond_status_t status;      /**< status, including status bits and synchronization */
  struct nm_data_s data;        /**< data descriptor to send/recv */
  struct nm_monitor_s monitor;  /**< monitor attached to this request (only 1) */
  nm_req_flag_t flags;          /**< flags given by user */
  nm_gate_t p_gate;             /**< dest/src gate; NULL if recv from any source */
  nm_core_tag_t tag;            /**< tag to send to/from (works in combination with tag_mask for recv) */
  nm_seq_t seq;                 /**< packet sequence number on the given tag */
  struct nm_gtag_s*p_gtag;      /**< cache for tag status on gate; NULL if tag or gate is unspecified yet */
  int err;                      /**< error status of the request */
  union
  {
    struct
    {
      nm_len_t len;             /**< cumulated data length */
      nm_len_t done;            /**< cumulated length of data sent so far */
      nm_len_t hlen;            /**< length of header to send eagerly */
      nm_prio_t priority;       /**< request priority level */
      uint32_t checksum;        /**< data checkusm when pack was submitted- for debug only */
    } pack;
    struct
    {
      nm_len_t expected_len;  /**< length of posted recv (may be updated if matched packet is shorter) */
      nm_len_t cumulated_len; /**< amount of data unpacked so far */
      nm_len_t offset;        /**< offset of data partially received */
      nm_req_seq_t req_seq;   /**< request sequence number used to interleave wildcard/non-wildcard requests */
      nm_core_tag_t tag_mask; /**< mask applied to tag for matching (only bits in mask need to match) */
      struct nm_matching_container_s matching; /**< link to store request in a matching map */
      struct
      {
        int n_partitions;
        struct nm_req_pchunk_s
        {
          struct nm_req_pchunk_s*p_next;
          nm_len_t chunk_offset;
          nm_len_t chunk_len;
        } *p_pchunks;          /**< unsorted list of arrived chunks; reads are lock-free, writes are within core_core_lock sections */
      } partition;            /**< partitioned unpack, used only if NM_REQ_FLAG_UNPACK_PARTITIONED is set */
      struct nm_pkt_wrap_s*p_prefetch_pw; /**< packet wrapper to prefetch recv */
    } unpack;
  };
  struct nm_req_chunk_s req_chunk; /**< preallocated chunk for the common case (single-chunk) */
};

/** initializes an empty pack request */
void nm_core_pack_init(struct nm_core*p_core, struct nm_req_s*p_pack);

/** build a pack request from data descriptor */
void nm_core_pack_data(nm_core_t p_core, struct nm_req_s*p_pack, const struct nm_data_s*p_data);

/** set tag/gate/flags for pack request */
void nm_core_pack_send(struct nm_core*p_core, struct nm_req_s*p_pack, nm_core_tag_t tag, nm_gate_t p_gate, nm_req_flag_t flags);

/** post a pack request */
void nm_core_pack_submit(struct nm_core*p_core, struct nm_req_s*p_pack);

/** set a priority for the given pack request */
void nm_core_pack_set_priority(struct nm_core*p_core, struct nm_req_s*p_pack, nm_prio_t priority);

/** set a header length for the given pack request */
static inline void nm_core_pack_set_hlen(struct nm_core*p_core __attribute__((unused)), struct nm_req_s*p_pack, nm_len_t hlen)
{
  p_pack->pack.hlen = hlen;
}

void nm_core_pack_submit_chunks(struct nm_core*p_core, struct nm_req_s*p_pack, int n, const struct nm_chunk_s*p_chunks);

/** initializes an empty unpack request */
void nm_core_unpack_init(struct nm_core*p_core, struct nm_req_s*p_unpack);

/** set an offset on data; data before offset will be discarded */
void nm_core_unpack_offset(struct nm_core*p_core, struct nm_req_s*p_unpack, nm_len_t offset);

/** build an unpack request from data descriptor */
void nm_core_unpack_data(struct nm_core*p_core, struct nm_req_s*p_unpack, const struct nm_data_s*p_data);

/** match an unpack request with given gate/tag, next sequence number assumed */
void nm_core_unpack_match_recv(struct nm_core*p_core, struct nm_req_s*p_unpack, nm_gate_t p_gate, nm_core_tag_t tag, nm_core_tag_t tag_mask);

/** match an unpack request with a packet that triggered an event */
void nm_core_unpack_match_event(struct nm_core*p_core, struct nm_req_s*p_unpack, const struct nm_core_event_s*p_event);

/** submit an unpack request */
void nm_core_unpack_submit(struct nm_core*p_core, struct nm_req_s*p_unpack, nm_req_flag_t flags);

/** peeks unexpected data without consumming it. offset & len are relative to p_data */
int nm_core_unpack_peek(struct nm_core*p_core, struct nm_req_s*p_unpack, const struct nm_data_s*p_data,
                        nm_len_t peek_offset, nm_len_t peek_len);

/** probes whether an incoming packet matched this unposted request.
 * in case of success, request and packet are associated and request _must_ be posted. */
int nm_core_unpack_iprobe(struct nm_core*p_core, struct nm_req_s*p_unpack);

/** cancel a pending unpack
 * @note cancel may fail if matching was already done.
 */
int nm_core_unpack_cancel(struct nm_core*p_core, struct nm_req_s*p_unpack);

/** probe unexpected packet, check matching for (packet_tag & tag_mask) == tag */
int nm_core_iprobe(struct nm_core*p_core,
                   nm_gate_t p_gate, nm_core_tag_t tag, nm_core_tag_t tag_mask,
                   nm_gate_t *pp_out_gate, nm_core_tag_t*p_out_tag, nm_len_t*p_out_size);

/** Flush pending packs (if supported by the strategy). */
void nm_core_flush(struct nm_core*p_core);


/* ** Packet injection from outside of nmad core */

/** get a seq number in the out stream, to route packet outside of nmad core */
nm_seq_t nm_core_send_seq_get(struct nm_core*p_core, nm_gate_t p_gate, nm_core_tag_t tag);

/** user-supplied function called to pull data to posted request through nmad core
 * p_req is the user request that matched this chunk
 * p_data is the memory buffer where to copy the chunk (may be different from p_req->data in case we are actually peeking.
 * chunk_offset, chunk_len describe the chunk we are receiving
 * p_ref user-supplied reference given to nm_core_inject_chunk()
 */
typedef void(*nm_injector_pull_data_t)(struct nm_req_s*p_req, const struct nm_data_s*p_data, nm_len_t chunk_offset, nm_len_t chunk_len, void*p_ref);

/** inject a packet in nmad core as if it arrived from network.
 * p_pull_data is called when a matching request is posted */
void nm_core_inject_chunk(struct nm_core*p_core, nm_gate_t p_gate, nm_core_tag_t tag, nm_seq_t seq,
                          nm_len_t chunk_offset, nm_len_t chunk_len, int is_last_chunk,
                          nm_injector_pull_data_t p_pull_data, void*p_ref);

/** notify data was injected in a matched request, but do not finalize the request (the status will be NM_STATUS_UNPACK_COMPLETED). */
void nm_core_inject_complete(struct nm_core*p_core, struct nm_req_s*p_req, nm_len_t chunk_offset, nm_len_t chunk_len);

/** finalize an injected request that was only completed. */
void nm_core_inject_finalize(struct nm_core*p_core, struct nm_req_s*p_req);

/** notify data was injected in a matched request and finalize this request. */
void nm_core_inject_complete_finalize(struct nm_core*p_core, struct nm_req_s*p_req, nm_len_t chunk_offset, nm_len_t chunk_len);

/* ** partitioned unpack */

void nm_core_unpack_partition_set(struct nm_req_s*p_unpack, int n_partitions);

void nm_core_unpack_partition_free(struct nm_req_s*p_unpack);

int nm_core_unpack_partition_test(struct nm_req_s*p_unpack, int partition);

/* ** Core tasks */

/** lock then submit task to pending list */
void nm_core_task_submit_locked(struct nm_core*p_core, void (*p_handler)(void));

/** submit task lock-free to the submission list */
void nm_core_task_submit_unlocked(struct nm_core*p_core, void (*p_handler)(void));


/* ** synchronization primitives *************************** */

/** @defgroup nm_cond synchronization
 * nm_cond_* functions are used to manage synchronization
 * with & without pioman. They are used internally for request status
 * but may be used by endusers for synchronization with monitor handlers
 * @example nm_bench_req_monitor.c
 */

/** @ingroup nm_cond
 * @{
 */

/** initialize a nm_cond_status_t object */
static inline void nm_cond_init(nm_cond_status_t*p_cond, nm_status_t bitmask);

/** free resources associated with a nm_cond_status_t object */
static inline void nm_cond_destroy(nm_cond_status_t*p_cond);

/** test whether the given bit is set in the status; unlocked, weak consistency */
static inline nm_status_t nm_cond_test(const nm_cond_status_t*p_cond, nm_status_t bitmask);

/** test whether the given bit is set in the status; locked, guaranteed consistency, slower */
static inline nm_status_t nm_cond_test_locked(const nm_cond_status_t*p_cond, nm_status_t bitmask);

/** add a bit to the bitmask in the status, do not unlock waiters
 * (for bits that will not be waited for) */
static inline void nm_cond_add(nm_cond_status_t*p_cond, nm_status_t bitmask);

/** wait for the given bit to be set in the status; do active polling while waiting */
static inline void nm_cond_wait(nm_cond_status_t*p_cond, nm_status_t bitmask, nm_core_t p_core);

/** add a bit and wake up threads waiting for it */
static inline void nm_cond_signal(nm_cond_status_t*p_cond, nm_status_t bitmask);

/** wait on multiple statuses at the same time */
static inline void nm_cond_wait_all(void**pp_conds, int n, uintptr_t offset, nm_status_t bitmask, nm_core_t p_core);

/** @} */

#if defined(PIOMAN)

/** @internal */
static inline void nm_cond_init(nm_cond_status_t*p_cond, nm_status_t bitmask)
{
  piom_cond_init(p_cond, bitmask);
}
/** @internal */
static inline void nm_cond_destroy(nm_cond_status_t*p_cond)
{
  piom_cond_destroy(p_cond);
}
/** @internal */
static inline nm_status_t nm_cond_test(const nm_cond_status_t*p_cond, nm_status_t bitmask)
{
  return piom_cond_test(p_cond, bitmask);
}
/** @internal */
static inline nm_status_t nm_cond_test_locked(const nm_cond_status_t*p_cond, nm_status_t bitmask)
{
  return piom_cond_test_locked((nm_cond_status_t*)p_cond, bitmask);
}
/** @internal */
static inline void nm_cond_add(nm_cond_status_t*p_cond, nm_status_t bitmask)
{
  piom_cond_add(p_cond, bitmask);
}
/** @internal */
static inline void nm_cond_mask(nm_cond_status_t*p_cond, nm_status_t bitmask)
{
  piom_cond_mask(p_cond, bitmask);
}
/** @internal */
static inline void nm_cond_wait(nm_cond_status_t*p_cond, nm_status_t bitmask, nm_core_t p_core __attribute__((unused)))
{
  piom_cond_wait(p_cond, bitmask);
}
/** @internal */
static inline void nm_cond_signal(nm_cond_status_t*p_cond, nm_status_t bitmask)
{
  piom_cond_signal(p_cond, bitmask);
}
/** @internal */
static inline void nm_cond_wait_all(void**pp_conds, int n, uintptr_t offset, nm_status_t bitmask, nm_core_t p_core  __attribute__((unused)))
{
  piom_cond_wait_all(pp_conds, n, offset, bitmask);
}
#else /* PIOMAN */
/** @internal */
static inline void nm_cond_init(nm_cond_status_t*p_cond, nm_status_t bitmask)
{
  *p_cond = bitmask;
}
/** @internal */
static inline void nm_cond_destroy(nm_cond_status_t*p_cond __attribute__((unused)))
{
}
/** @internal */
static inline nm_status_t nm_cond_test(const nm_cond_status_t*p_cond, nm_status_t bitmask)
{
  return ((*p_cond) & bitmask);
}
/** @internal */
static inline nm_status_t nm_cond_test_locked(const nm_cond_status_t*p_cond, nm_status_t bitmask)
{
  return nm_cond_test(p_cond, bitmask);
}
/** @internal */
static inline void nm_cond_add(nm_cond_status_t*p_cond, nm_status_t bitmask)
{
  *p_cond |= bitmask;
}
/** @internal */
static inline void nm_cond_mask(nm_cond_status_t*p_cond, nm_status_t bitmask)
{
  *p_cond &= ~bitmask;
}
/** @internal */
static inline void nm_cond_signal(nm_cond_status_t*p_cond, nm_status_t bitmask)
{
  *p_cond |= bitmask;
}
/** @internal */
static inline void nm_cond_wait(nm_cond_status_t*p_cond, nm_status_t bitmask, nm_core_t p_core)
{
  while(!nm_cond_test(p_cond, bitmask))
    {
      nm_schedule(p_core);
    }
}
/** @internal */
static inline void nm_cond_wait_all(void**pp_conds, int n, uintptr_t offset, nm_status_t bitmask, nm_core_t p_core)
{
  int i;
  for(i = 0; i < n; i++)
    {
      if(pp_conds[i] != NULL)
        {
          nm_cond_status_t*p_cond = (nm_cond_status_t*)((uintptr_t)pp_conds[i] + offset);
          nm_cond_wait(p_cond, bitmask, p_core);
        }
    }
}
#endif /* PIOMAN */

/* ** convenient frontends to deal with status in requests */

/** initialize cond status with given initial value */
static inline void nm_status_init(struct nm_req_s*p_req, nm_status_t bitmask)
{
  nm_cond_init(&p_req->status, bitmask);
}
static inline void nm_status_destroy(struct nm_req_s*p_req)
{
  nm_cond_destroy(&p_req->status);
}
/** query for given bits in req status; returns matched bits */
static inline nm_status_t nm_status_test(const struct nm_req_s*p_req, nm_status_t bitmask)
{
  if(bitmask & NM_STATUS_FINALIZED) /* status FINALIZED needs strong consistency to avoid use after free */
    return nm_cond_test_locked(&p_req->status, bitmask);
  else
    return nm_cond_test(&p_req->status, bitmask);
}
static inline void nm_status_add(struct nm_req_s*p_req, nm_status_t bitmask)
{
  nm_cond_add(&p_req->status, bitmask);
}
/** remove bits of bitmask from req status */
static inline void nm_status_unset(struct nm_req_s*p_req, nm_status_t bitmask)
{
  nm_cond_mask(&p_req->status, bitmask);
}
/** wait for _any bit_ matching in req status */
static inline void nm_status_wait(struct nm_req_s*p_req, nm_status_t bitmask, nm_core_t p_core)
{
  nm_cond_wait(&p_req->status, bitmask, p_core);
  assert(nm_status_test(p_req, bitmask) != 0);
}
static inline void nm_status_signal(struct nm_req_s*p_req, nm_status_t bitmask)
{
  nm_cond_signal(&p_req->status, bitmask);
}
/** wait for _all reqs_, _any bit_ in bitmask */
static inline void nm_status_wait_all(void**pp_reqs, int n, uintptr_t offset,
                                      nm_status_t bitmask, nm_core_t p_core)
{
  const struct nm_req_s*p_req = NULL;
  const uintptr_t status_offset = (uintptr_t)&p_req->status - (uintptr_t)p_req; /* offset of 'status' in nm_req_s */
  nm_cond_wait_all(pp_reqs, n, offset + status_offset, bitmask, p_core);
}
static inline void nm_status_assert(struct nm_req_s*p_req __attribute__((unused)), nm_status_t value __attribute__((unused)))
{
  assert(nm_status_test(p_req, NM_STATUS_MASK_FULL) == value);
}

static inline void nm_status_spinwait(struct nm_req_s*p_req, nm_status_t status)
{
  while(!nm_status_test(p_req, status))
    { /* bust wait*/ }
}
/** tests for all given bits in status */
static inline int nm_status_test_allbits(struct nm_req_s*p_req, nm_status_t bitmask)
{
  return (nm_status_test(p_req, bitmask) == bitmask);
}

/* ** frontends for atomic ops ***************************** */

/** memory fence, always */
static inline void nm_mem_fence_always(void)
{
  __sync_synchronize();
}

/** memory fence only when multithread */
static inline void nm_mem_fence(void)
{
#if defined(PIOMAN_MULTITHREAD)
  __sync_synchronize();
#else
  nm_core_t p_core = nm_core_get_singleton();
  if(nm_core_get_thread_level(p_core) >= NM_THREAD_SERIALIZED)
    {
      __sync_synchronize();
    }
#endif /* PIOMAN_MULTITHREAD */
}

/** increment int, atomic only when multithread */
static inline int nm_atomic_inc(int*v)
{
#if defined(PIOMAN_MULTITHREAD)
  return __sync_fetch_and_add(v, 1);
#else
  nm_core_t p_core = nm_core_get_singleton();
  if(nm_core_get_thread_level(p_core) >= NM_THREAD_SERIALIZED)
    {
      return __sync_fetch_and_add(v, 1);
    }
  else
    {
      return (*v)++;
    }
#endif /* PIOMAN_MULTITHREAD */
}

/** increment int, always atomic */
static inline int nm_atomic_alway_inc(int*v)
{
  return __sync_fetch_and_add(v, 1);
}

/** decrement int, atomic only when multithread */
static inline int nm_atomic_dec(int*v)
{
#if defined(PIOMAN_MULTITHREAD)
  return __sync_sub_and_fetch(v, 1);
#else
  nm_core_t p_core = nm_core_get_singleton();
  if(nm_core_get_thread_level(p_core) >= NM_THREAD_SERIALIZED)
    {
      return __sync_sub_and_fetch(v, 1);
    }
  else
    {
      return --(*v);
    }
#endif /* PIOMAN_MULTITHREAD */
}

/** decrement int, always atomic */
static inline int nm_atomic_always_dec(int*v)
{
  return __sync_sub_and_fetch(v, 1);
}

/** int add, atomic only when multithread */
static inline void nm_atomic_add(int*v, int v2)
{
#if defined(PIOMAN_MULTITHREAD)
  __sync_fetch_and_add(v, v2);
#else
  nm_core_t p_core = nm_core_get_singleton();
  if(nm_core_get_thread_level(p_core) >= NM_THREAD_SERIALIZED)
    {
      __sync_fetch_and_add(v, v2);
    }
  else
    {
      (*v) += v2;
    }
#endif /* PIOMAN_MULTITHREAD */
}

/** int add, always atomic */
static inline void nm_atomic_always_add(int*v, int v2)
{
  __sync_fetch_and_add(v, v2);
}

/** boolean int compare and swap, atomic only when multithread */
static inline int nm_atomic_compare_and_swap(int*v, int oldval, int newval)
{
#if defined(PIOMAN_MULTITHREAD)
  return __sync_bool_compare_and_swap(v, oldval, newval);
#else
  nm_core_t p_core = nm_core_get_singleton();
  if(nm_core_get_thread_level(p_core) >= NM_THREAD_SERIALIZED)
    {
      return __sync_bool_compare_and_swap(v, oldval, newval);
    }
  else
    {
      if(*v == oldval)
        {
          *v = newval;
          return 1;
        }
      else
        {
          return 0;
        }
    }
#endif /* PIOMAN_MULTITHREAD */
}

/** boolean int compare and swap, always atomic */
static inline int nm_atomic_always_compare_and_swap(int*v, int oldval, int newval)
{
  return __sync_bool_compare_and_swap(v, oldval, newval);
}

/* ** frontend for generic locking ************************* */

/*
 * locking with pioman:
 *   always use all locks (even if the application is not threaded, pioman itself is threaded).
 *
 * locking without pioman:
 *   - single     : no locking
 *   - funneled   : lock in rcache
 *   - serialized : + mem fence in spinlocks (no locks) + lock in allocators + atomics in refcounts
 *   - multiple   : not supported without pioman
 */

#ifdef PIOMAN
typedef piom_spinlock_t nm_spinlock_t;
#else /* PIOMAN */
struct nm_spinlock_s
{
#ifdef NMAD_DEBUG
  int lock;
  pthread_t last_tid;
#endif /* NMAD_DEBUG */
};
typedef struct nm_spinlock_s nm_spinlock_t;
#endif /* PIOMAN */

/** init the spin lock */
static inline void nm_spin_init(nm_spinlock_t*p_spin);

/** destroy the spin lock */
static inline void nm_spin_destroy(nm_spinlock_t*p_spin);

/** acquire the spin lock */
static inline void nm_spin_lock(nm_spinlock_t*p_spin);

/** release the spin lock */
static inline void nm_spin_unlock(nm_spinlock_t*p_spin);

/** try to lock the spin lock
 * return 1 if lock is successfully acquired, 0 otherwise
 */
static inline int nm_spin_trylock(nm_spinlock_t*p_spin);

/** assert that current thread holds the lock */
static inline void nm_spin_assert_locked(nm_spinlock_t*p_spin);

/** assert that current thread doesn't hold the lock */
static inline void nm_spin_assert_notlocked(nm_spinlock_t*p_spin);


/** check that we are always called from the same thread in case of non-threaded mode */
static inline void nm_spin_check_nothread(nm_spinlock_t*p_spin __attribute__((unused)))
{
#if defined(NMAD_DEBUG) && !defined(PIOMAN)
  nm_core_t p_core = nm_core_get_singleton();
  assert(nm_core_get_thread_level(p_core) <= NM_THREAD_SERIALIZED);
  if(p_spin->last_tid == (pthread_t)0)
    {
      p_spin->last_tid = pthread_self();
      __sync_synchronize();
    }
  else
    {
      if(p_spin->last_tid != pthread_self())
        {
          NM_FATAL("detected calls from multiple threads in non-threaded mode. Please use pioman-enabled build for multi-threaded use or give thread level using nm_core_set_thread_level(NM_THREAD_SERIALIZED) for serialized thread level.");
        }
    }
#endif /* NMAD_DEBUG && !PIOMAN */
}

/** clear the last_tid tracking for lock consistency checking */
static inline void nm_spin_clear_nothread(nm_spinlock_t*p_spin __attribute__((unused)))
{
#if defined(NMAD_DEBUG) && !defined(PIOMAN)
  nm_core_t p_core = nm_core_get_singleton();
  assert(nm_core_get_thread_level(p_core) <= NM_THREAD_SERIALIZED);
  if(p_spin->last_tid == 0)
    {
      NM_FATAL("unlocking while no thread is holding the lock.");
    }
  else if(p_spin->last_tid != pthread_self())
    {
      NM_WARN("unlocking from another thread than where lock was acquired.\n");
    }
  if(nm_core_get_thread_level(p_core) == NM_THREAD_SERIALIZED)
    {
      p_spin->last_tid = (pthread_t)0;
    }
#endif /* NMAD_DEBUG && !PIOMAN */
}

static inline void nm_spin_init(nm_spinlock_t*p_spin __attribute__((unused)))
{
#ifdef PIOMAN
  piom_spin_init(p_spin);
#else /* PIOMAN */
#ifdef NMAD_DEBUG
  p_spin->lock = 0;
  p_spin->last_tid = 0;
#endif /* NMAD_DEBUG */
#endif /* PIOMAN */
}

static inline void nm_spin_destroy(nm_spinlock_t*p_spin __attribute__((unused)))
{
#ifdef PIOMAN
  piom_spin_destroy(p_spin);
#else /* PIOMAN */
#ifdef NMAD_DEBUG
  assert(p_spin->lock == 0);
#endif /* NMAD_DEBUG */
#endif /* PIOMAN */
}

static inline void nm_spin_lock(nm_spinlock_t*p_spin __attribute__((unused)))
{
#ifdef PIOMAN
  piom_spin_lock(p_spin);
#else /* PIOMAN */
#ifdef NMAD_DEBUG
  __sync_synchronize();
  if(p_spin->lock != 0)
    {
      NM_FATAL("spinlock is not free in nm_spin_lock(); detected concurrent access from thread = %p. Suspecting multi-threaded use by the application while library is initialized in non-threaded mode.\n",
               (void*)p_spin->last_tid);
    }
  p_spin->lock = 1;
#endif /* NMAD_DEBUG */
  nm_spin_check_nothread(p_spin);
  nm_core_t p_core = nm_core_get_singleton();
  if(nm_core_get_thread_level(p_core) >= NM_THREAD_SERIALIZED)
    {
      __sync_synchronize();
    }
#endif /* PIOMAN */
}

static inline void nm_spin_unlock(nm_spinlock_t*p_spin __attribute__((unused)))
{
#ifdef PIOMAN
  piom_spin_unlock(p_spin);
#else /* PIOMAN */
  nm_spin_clear_nothread(p_spin);
#ifdef NMAD_DEBUG
  __sync_synchronize();
  assert(p_spin->lock == 1);
  p_spin->lock = 0;
#endif /* NMAD_DEBUG */
  nm_core_t p_core = nm_core_get_singleton();
  if(nm_core_get_thread_level(p_core) >= NM_THREAD_SERIALIZED)
    {
      __sync_synchronize();
    }
#endif /* PIOMAN */
}

static inline int nm_spin_trylock(nm_spinlock_t*p_spin __attribute__((unused)))
{
#ifdef PIOMAN
  return piom_spin_trylock(p_spin);
#else /* PIOMAN */
  int rc = 1;
#ifdef NMAD_DEBUG
  __sync_synchronize();
  if(p_spin->lock)
    {
      assert(p_spin->lock == 1);
      rc = 0;
    }
  else
    {
      rc = 1;
      p_spin->lock = 1;
      nm_spin_check_nothread(p_spin);
    }
#endif /* NMAD_DEBUG */
  nm_core_t p_core = nm_core_get_singleton();
  if(nm_core_get_thread_level(p_core) >= NM_THREAD_SERIALIZED)
    {
      __sync_synchronize();
    }
  return rc;
#endif /* PIOMAN */
}

static inline void nm_spin_assert_locked(nm_spinlock_t*p_spin __attribute__((unused)))
{
#ifdef PIOMAN
  piom_spin_assert_locked(p_spin);
#else /* PIOMAN */
#ifdef NMAD_DEBUG
  assert(p_spin->lock == 1);
  assert(p_spin->last_tid == pthread_self());
#endif /* NMAD_DEBUG */
#endif /* PIOMAN */
}

static inline void nm_spin_assert_notlocked(nm_spinlock_t*p_spin __attribute__((unused)))
{
#ifdef PIOMAN
  piom_spin_assert_notlocked(p_spin);
#else /* PIOMAN */
#ifdef NMAD_DEBUG
  assert(p_spin->lock == 0);
#endif /* NMAD_DEBUG */
#endif /* PIOMAN */
}


/** @} */

#endif /* NM_CORE_INTERFACE_H */

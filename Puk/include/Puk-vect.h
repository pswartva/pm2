/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Puk automatic vectors definition
 * @ingroup Puk
 */

#include <string.h>
#include <stdlib.h>

#define PUK_VECT_INITSIZE 2

/**
 * @def PUK_VECT_TYPE(ENAME, TYPE)
 * @brief Creates a new vector type
 *
 *  with elements of type TYPE,
 *  with name built from ENAME (foo -> foo_vect_t)
 * Example:
 *   PUK_VECT_TYPE(foo, const char*);
 *   declares foo_vect_t as a vector of const char*.
 */

#define PUK_VECT_TYPE(ENAME, TYPE)                                      \
  struct ENAME##_vect_s                                                 \
  {                                                                     \
    TYPE*_data;                                                         \
    int  _capacity;                                                     \
    int  _size;                                                         \
  };                                                                    \
  typedef struct ENAME##_vect_s* ENAME##_vect_t;                        \
  /** @internal */                                                      \
  __attribute__((__unused__))                                           \
  static inline void ENAME##_vect_init(ENAME##_vect_t v)                \
  { v->_data = NULL;                                                    \
    v->_capacity = 0;                                                   \
    v->_size = 0; }                                                     \
  /** @internal */                                                      \
  __attribute__((__unused__))                                           \
  static inline void ENAME##_vect_destroy(ENAME##_vect_t v)             \
  { if(v->_data) padico_free(v->_data); v->_data = NULL; v->_size = 0; v->_capacity = 0; } \
  /** @internal */                                                      \
  __attribute__((__unused__))                                           \
  static inline ENAME##_vect_t ENAME##_vect_new(void)                   \
  { ENAME##_vect_t v; v = (ENAME##_vect_t)padico_malloc(sizeof(struct ENAME##_vect_s)); \
    ENAME##_vect_init(v); return v; }                                   \
  /** @internal */                                                      \
  __attribute__((__unused__))                                           \
  static inline void ENAME##_vect_delete(ENAME##_vect_t v)              \
  { ENAME##_vect_destroy(v); padico_free(v); }                          \
  /** @internal */                                                      \
  __attribute__((__unused__))                                           \
  static inline int ENAME##_vect_capacity(const struct ENAME##_vect_s*v) \
  { return v->_capacity; }                                              \
  /** @internal */                                                      \
  __attribute__((__unused__))                                           \
  static inline int ENAME##_vect_size(const struct ENAME##_vect_s*v)    \
  { return v->_size; }                                                  \
  /** @internal */                                                      \
  __attribute__((__unused__))                                           \
  static inline int ENAME##_vect_empty(const struct ENAME##_vect_s*v)   \
  { return (ENAME##_vect_size(v) == 0); }                               \
  /** @internal */                                                      \
  __attribute__((__unused__))                                           \
  static inline TYPE ENAME##_vect_at(const struct ENAME##_vect_s*v, int i) \
  { return v->_data[i]; }                                               \
  /** @internal */                                                      \
  __attribute__((__unused__))                                           \
  static inline TYPE*ENAME##_vect_ptr(ENAME##_vect_t v, int i)          \
  { return &v->_data[i]; }                                              \
  /** @internal */                                                      \
  __attribute__((__unused__))                                           \
  static inline void ENAME##_vect_reserve(ENAME##_vect_t v, int capacity) \
  { if(capacity>v->_capacity) {                                         \
      v->_capacity = capacity;                                          \
      v->_data = (TYPE*)padico_realloc(v->_data, v->_capacity * sizeof(TYPE)); } } \
  /** @internal */                                                      \
  __attribute__((__unused__))                                           \
  static inline void ENAME##_vect_resize(ENAME##_vect_t v, int size)    \
  { int c = v->_capacity; if(!c) { c = PUK_VECT_INITSIZE; } while(c < size) { c *= 2; } \
    v->_size = size;                                                    \
    if(c > v->_capacity) { ENAME##_vect_reserve(v, c); }                \
  }                                                                     \
  /** @internal */                                                      \
  __attribute__((__unused__))                                           \
  static inline void ENAME##_vect_put(ENAME##_vect_t v, TYPE e, int i)  \
  { if(i+1>v->_size){ENAME##_vect_resize(v, i+1);} v->_data[i] = e; }   \
  /** @internal */                                                      \
  __attribute__((__unused__))                                           \
  static inline void ENAME##_vect_copy_into(ENAME##_vect_t dest, ENAME##_vect_t src) \
  { ENAME##_vect_resize(dest, ENAME##_vect_size(src));                  \
    memcpy(dest->_data, src->_data, src->_size*sizeof(TYPE)); }         \
  /** @internal */                                                      \
  __attribute__((__unused__))                                           \
  static inline ENAME##_vect_t ENAME##_vect_copy(ENAME##_vect_t v)      \
  { ENAME##_vect_t w = ENAME##_vect_new();                              \
    ENAME##_vect_copy_into(w, v);                                       \
    return w; }                                                         \
  /** @internal */                                                      \
  __attribute__((__unused__))                                           \
  static inline TYPE* ENAME##_vect_push_back(ENAME##_vect_t v, TYPE e)  \
  { ENAME##_vect_resize(v, v->_size+1);                                 \
    ENAME##_vect_put(v, e, v->_size-1);                                 \
    return v->_data+v->_size-1; }                                       \
  /** @internal */                                                      \
  __attribute__((__unused__))                                           \
  static inline TYPE ENAME##_vect_pop_back(ENAME##_vect_t v)            \
  { TYPE e = ENAME##_vect_at(v, ENAME##_vect_size(v)-1);                \
    ENAME##_vect_resize(v, v->_size-1); return e; }                     \
  /** @internal */                                                      \
  typedef TYPE*ENAME##_vect_itor_t;                                     \
  /** @internal */                                                      \
  __attribute__((__unused__))                                           \
  static inline ENAME##_vect_itor_t ENAME##_vect_begin(ENAME##_vect_t v) \
  { return v->_data; }                                                  \
  /** @internal */                                                      \
  __attribute__((__unused__))                                           \
  static inline ENAME##_vect_itor_t ENAME##_vect_end(ENAME##_vect_t v)  \
  { return v->_data+v->_size; }                                         \
  /** @internal */                                                      \
  __attribute__((__unused__))                                           \
  static inline ENAME##_vect_itor_t ENAME##_vect_rbegin(ENAME##_vect_t v) \
  { return v->_data+v->_size-1; }                                       \
  /** @internal */                                                      \
  __attribute__((__unused__))                                           \
  static inline ENAME##_vect_itor_t ENAME##_vect_rend(ENAME##_vect_t v) \
  { return v->_data-1; }                                                \
  /** @internal */                                                      \
  __attribute__((__unused__))                                           \
  static inline ENAME##_vect_itor_t ENAME##_vect_next(ENAME##_vect_itor_t i) \
  { return i+1; }                                                       \
  /** @internal */                                                      \
  __attribute__((__unused__))                                           \
  static inline ENAME##_vect_itor_t ENAME##_vect_rnext(ENAME##_vect_itor_t i) \
  { return i-1; }                                                       \
  /** @internal */                                                      \
  __attribute__((__unused__))                                           \
  static inline void ENAME##_vect_erase(ENAME##_vect_t v, ENAME##_vect_itor_t i) \
  { v->_size--; while(i < v->_data+v->_size) { i[0]=i[1]; i++; }  }     \
  /** @internal */                                                      \
  __attribute__((__unused__))                                           \
  static inline ENAME##_vect_itor_t ENAME##_vect_find(ENAME##_vect_t v, TYPE e) \
  { TYPE*i = v->_data;                                                  \
    while((i < v->_data+v->_size) && (memcmp(i, &e, sizeof(TYPE))))     \
      {i++;}                                                            \
    if(i == v->_data+v->_size) i = NULL;                                \
    return i; }                                                         \
  __attribute__((__unused__))                                           \
  static inline int ENAME##_vect_rank(ENAME##_vect_t v, ENAME##_vect_itor_t i) \
  { if((i < v->_data) || (i > v->_data + v->_size)) { return -1; }      \
    else { return i - v->_data; } }

#define PUK_VECT_STATIC_INITIALIZER { ._data = NULL, ._capacity = 0, ._size = 0 }

PUK_VECT_TYPE(puk_int, int)

#define puk_vect_foreach(ITOR, ENAME, VECT) \
  for((ITOR)  = ENAME ## _vect_begin(VECT); \
      (ITOR) != ENAME ## _vect_end(VECT); \
      (ITOR)  = ENAME ## _vect_next(ITOR))

#define puk_vect_foreach_reverse(ITOR, ENAME, VECT) \
  for((ITOR)  = ENAME ## _vect_rbegin(VECT); \
      (ITOR) != ENAME ## _vect_rend(VECT); \
      (ITOR)  = ENAME ## _vect_rnext(ITOR))


#define PUK_STACK_TYPE(ENAME, TYPE) \
  typedef ENAME##_vect_t ENAME##_stack_t; \
  static inline ENAME##_stack_t ENAME##_stack_new(void) \
  { return ENAME##_vect_new(); } \
  static inline void ENAME##_stack_init(ENAME##_stack_t s) \
  { ENAME##_vect_init(s); } \
  static inline void ENAME##_stack_delete(ENAME##_stack_t s) \
  { ENAME##_vect_delete(s); } \
  static inline TYPE ENAME##_stack_peek(ENAME##_stack_t s) \
  { return ENAME##_vect_at(s, ENAME##_vect_size(s)-1); } \
  static inline TYPE* ENAME##_stack_top(ENAME##_stack_t s) \
  { return ENAME##_vect_rbegin(s); } \
  static inline TYPE* ENAME##_stack_below(TYPE*e) \
  { return ENAME##_vect_rnext(e); } \
  static inline TYPE* ENAME##_stack_push(ENAME##_stack_t s, TYPE e) \
  { ENAME##_vect_push_back(s, e); return ENAME##_stack_top(s); } \
  static inline TYPE ENAME##_stack_pop(ENAME##_stack_t s) \
  { TYPE e = ENAME##_vect_at(s, ENAME##_vect_size(s)-1); \
    ENAME##_vect_pop_back(s); return e; } \
  static inline int ENAME##_stack_empty(ENAME##_stack_t s) \
  { return ENAME##_vect_empty(s); } \
  static inline int ENAME##_stack_size(ENAME##_stack_t s) \
  { return ENAME##_vect_size(s); }

#define PUK_QUEUE_TYPE(ENAME, TYPE)                                     \
  struct ENAME##_queue_s                                                \
  {                                                                     \
    TYPE*_data;     /**< @internal */                                   \
    int  _capacity; /**< @internal */                                   \
    int  _front;    /**< @internal */                                   \
    int  _rear;     /**< @internal */                                   \
  };                                                                    \
  typedef struct ENAME##_queue_s* ENAME##_queue_t;                      \
  __attribute__((__unused__))                                           \
  static inline void ENAME##_queue_init(ENAME##_queue_t q, int capacity) \
  {                                                                     \
    q->_data = (TYPE*)padico_malloc(capacity * sizeof(TYPE));           \
    q->_capacity = capacity; q->_front = 0; q->_rear = capacity-1;      \
  }                                                                     \
  __attribute__((__unused__))                                           \
  static inline ENAME##_queue_t ENAME##_queue_new(int capacity)         \
  { ENAME##_queue_t q = (ENAME##_queue_t)padico_malloc(sizeof(struct ENAME##_queue_s)); \
    ENAME##_queue_init(q, capacity);                                    \
    return q;                                                           \
  }                                                                     \
  __attribute__((__unused__))                                           \
  static inline void ENAME##_queue_destroy(ENAME##_queue_t q)           \
  {                                                                     \
    padico_free(q->_data);                                              \
  }                                                                     \
  __attribute__((__unused__))                                           \
  static inline void ENAME##_queue_delete(ENAME##_queue_t q)            \
  {                                                                     \
    ENAME##_queue_destroy(q);                                           \
    padico_free(q);                                                     \
  }                                                                     \
  __attribute__((__unused__))                                           \
  static inline void ENAME##_queue_append(ENAME##_queue_t q, TYPE e)    \
  {                                                                     \
    q->_rear = (q->_rear+1)%q->_capacity;                               \
    q->_data[q->_rear] = e;                                             \
  }                                                                     \
  __attribute__((__unused__))                                           \
  static inline TYPE ENAME##_queue_peek(ENAME##_queue_t q)              \
  {                                                                     \
    TYPE e = q->_data[q->_front];                                       \
    return e;                                                           \
  }                                                                     \
  __attribute__((__unused__))                                           \
  static inline TYPE ENAME##_queue_retrieve(ENAME##_queue_t q)          \
  {                                                                     \
    TYPE e = q->_data[q->_front];                                       \
    q->_front=(q->_front+1)%q->_capacity;                               \
    return e;                                                           \
  }                                                                     \
  __attribute__((__unused__))                                           \
  static inline int ENAME##_queue_size(ENAME##_queue_t q)               \
  {                                                                     \
    return (q->_rear - q->_front + 1 +  q->_capacity) % q->_capacity;   \
  }                                                                     \
  __attribute__((__unused__))                                           \
  static inline int ENAME##_queue_full(ENAME##_queue_t q)               \
  {                                                                     \
    return ((q->_rear - q->_front + 2 +  q->_capacity) % q->_capacity == 0); \
  }                                                                     \
  __attribute__((__unused__))                                           \
  static inline int ENAME##_queue_empty(ENAME##_queue_t q)              \
  {                                                                     \
    return (ENAME##_queue_size(q)==0);                                  \
  }

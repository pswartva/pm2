/* bench-VIO.c
 */

#include <netdb.h>
#include "bench-common.h"
#include <Padico/VIO.h>
#include <Padico/Module.h>
#include <Padico/NetAccess.h>

int bench_udp_init(void);
int bench_udp_run(int argc, char**argv);
void bench_udp_finalize(void);

PADICO_MODULE_DECLARE(bench_UDP, bench_udp_init, bench_udp_run, bench_udp_finalize);

#define DEFAULT_PORT       5040

static int port = DEFAULT_PORT;

static struct sockaddr_in local_addr;
static socklen_t          local_addr_size  = sizeof(local_addr);
static struct sockaddr_in remote_addr;
static socklen_t          remote_addr_size = sizeof(remote_addr);
static int bench_fd;

int bench_udp_init(void)
{
  int rc;
  struct hostent*he;
  bench_fd = padico_vio_socket(PF_INET, SOCK_DGRAM, 0);
  local_addr.sin_family = AF_INET;
  local_addr.sin_port  = htons(port);
  rc = padico_vio_bind(bench_fd, (struct sockaddr*)&local_addr, local_addr_size);
  if(rc != 0)
    {
      padico_warning("cannot bind on port %d\n", port);
      return -1;
    }
  he = gethostbyname(server_name);
  if(he == NULL)
    {
      padico_warning("cannot resolve host=**%s**\n", server_name);
      return -1;
    }
  //  memcpy(&remote_addr.sin_addr, he->h_addr_list[0], sizeof(remote_addr.sin_addr));
  memcpy(&remote_addr.sin_addr,
         padico_topo_host_getaddr(padico_topo_node_gethost(padico_na_madio_getnodebyrank((padico_na_rank()+1)%2))),
         sizeof(remote_addr.sin_addr));
  remote_addr.sin_family = AF_INET;
  remote_addr.sin_port  = htons(port);
  return 0;
}

void bench_udp_finalize(void)
{
  padico_vio_close(bench_fd);
}

void udp_server(char*buffer, int lenbuf, int num_roundtrip)
{
  int i;
  struct sockaddr from_addr;
  int from_addr_size = sizeof(from_addr);
  for(i=0; i<num_roundtrip; i++)
    {
      padico_vio_sendto(bench_fd, buffer, lenbuf, 0, &remote_addr, remote_addr_size);
      padico_vio_recvfrom_madio(bench_fd, buffer, lenbuf, 0, &from_addr, &from_addr_size);
    }
}

void udp_client(char*buffer, int lenbuf, int num_roundtrip)
{
  int i;
  struct sockaddr from_addr;
  int from_addr_size = sizeof(from_addr);
  for(i=0; i<num_roundtrip; i++)
    {
      padico_vio_recvfrom_madio(bench_fd, buffer, lenbuf, 0, &from_addr, &from_addr_size);
      padico_vio_sendto(bench_fd, buffer, lenbuf, 0, &remote_addr, remote_addr_size);
    }
}

struct bench_desc_s udp_bench =
{
  "UDP",
  &udp_server,
  &udp_client
};

int bench_udp_run(int argc, char**argv)
{
  the_bench(&udp_bench);
  return 0;
}

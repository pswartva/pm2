/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Early definitions for Puk module API.
 * These definitions are required by macros to get local
 * module. They are defined separately from Module.h so
 * as to be included early in Puk.h.
 * @ingroup Puk
 */

#ifndef PUK_MODULE_LOCAL_H
#define PUK_MODULE_LOCAL_H

/* ** auto dep tracking ************************************ */

/** a cell for a list of depedancies */
struct puk_mod_dep_cell_s
{
  const char*mod_name;
  const struct puk_mod_dep_cell_s*link;
};

/** root of dep list */
static const struct puk_mod_dep_cell_s puk_mod_dep_0 = { .mod_name = NULL, .link = NULL };
#define PADICO_LAST_MODULE_NAME 0

#define PASTE_(A, B) A ## B
#define PASTE(A, B) PASTE_(A, B)

#define PUK_MOD_AUTO_DEP(MOD, PREV)                                     \
  static const struct puk_mod_dep_cell_s PASTE(puk_mod_dep_, MOD) =     \
    {                                                                   \
      .mod_name = #MOD,                                                 \
      .link = &PASTE(puk_mod_dep_, PREV)                                \
    };
#define PUK_MOD_AUTO_DEP_LIST &PASTE(puk_mod_dep_, PADICO_LAST_MODULE_NAME)

/* ** local module ***************************************** */

/** local definitions for a binary module, that can be initialized statically */
struct puk_mod_binary_s
{
  const char*const name;
  const struct puk_mod_funcs_s*const funcs;
  puk_mod_t mod;
  struct puk_profile_mem_s profile; /**< memory profiling for this module */
  const struct puk_mod_dep_cell_s*auto_deps; /**< list of automatically detected deps */
  const int n_deps;                          /**< number of declare deps */
  const char*const deps[];                   /**< declared deps */
};

/** local pointer to the binary module definition; this pointer is
 * explicitely initialized through constructors to control the init order.
 * It must be initialized before all memory allocations (and thus before
 * attributes definition, whose constructors get a lower priority */
__attribute__ ((__unused__))
static struct puk_mod_binary_s*padico_mod_binary_local = NULL;

__attribute__ ((__unused__))
static const char*padico_mod_local_name; /* tentative variable definition; actual declaration in Module.h */

__attribute__ ((__unused__))
static inline puk_mod_t padico_module_self(void)
{
  return padico_mod_binary_local?padico_mod_binary_local->mod:NULL;
}

__attribute__ ((__unused__))
static inline const char*padico_module_self_name(void)
{
  return padico_mod_local_name;
}

__attribute__ ((__unused__))
/** get a pointer to the memory profiling for the local module */
static inline struct puk_profile_mem_s*puk_mod_binary_get_profile(void)
{
#ifdef PUK_ENABLE_PROFILE
  return padico_mod_binary_local?&padico_mod_binary_local->profile:NULL;
#else
  return NULL;
#endif
}

#endif /* PUK_MODULE_LOCAL_H */

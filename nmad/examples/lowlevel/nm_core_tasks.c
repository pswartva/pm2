/*
 * NewMadeleine
 * Copyright (C) 2021-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <stdio.h>

#include <Padico/Puk-timing.h>

#include <nm_session_interface.h>
#include <nm_launcher_interface.h>
#include <nm_core_interface.h>
#include <nm_log.h>

#define MAX_ITERS 1000000

static volatile int count = 0;
static int verbose = 1;

static void task_handler(void)
{
  count++;
  if(verbose)
    fprintf(stderr, "# task_handler; count = %d\n", count);
}

static void benchmark(struct nm_core*p_core)
{
  verbose = 0;

  int iters;
  fprintf(stderr, "unlocked submission:\n");
  for(iters = 1; iters <= MAX_ITERS; iters *= 2)
    {
      puk_tick_t t1, t2;
      count = 0;
      PUK_GET_TICK(t1);
      int i;
      for(i = 0; i < iters; i++)
        {
          nm_core_task_submit_unlocked(p_core, &task_handler);
        }
      while(count < iters)
        {
          nm_schedule(p_core);
        }
      PUK_GET_TICK(t2);
      const double usec = PUK_TIMING_DELAY(t1, t2);
      fprintf(stderr, " time = %f usec./iter; iters = %d\n", usec/iters, iters);
    }

  fprintf(stderr, "locked submission:\n");
  for(iters = 1; iters <= MAX_ITERS; iters *= 2)
    {
      puk_tick_t t1, t2;
      PUK_GET_TICK(t1);
      count = 0;
      int i;
      for(i = 0; i < iters; i++)
        {
          nm_core_task_submit_locked(p_core, &task_handler);
        }
      while(count < iters)
        {
          nm_schedule(p_core);
        }
      PUK_GET_TICK(t2);
      const double usec = PUK_TIMING_DELAY(t1, t2);
      fprintf(stderr, " time = %f usec./iter; iters = %d\n", usec/iters, iters);
    }

  fprintf(stderr, "unlocked & schedule:\n");
  for(iters = 1; iters <= MAX_ITERS; iters *= 2)
    {
      puk_tick_t t1, t2;
      count = 0;
      PUK_GET_TICK(t1);
      int i;
      for(i = 0; i < iters; i++)
        {
          nm_core_task_submit_unlocked(p_core, &task_handler);
          nm_schedule(p_core);
        }
      while(count < iters)
        {
          nm_schedule(p_core);
        }
      PUK_GET_TICK(t2);
      const double usec = PUK_TIMING_DELAY(t1, t2);
      fprintf(stderr, " time = %f usec./iter; iters = %d\n", usec/iters, iters);
    }

  fprintf(stderr, "schedule only:\n");
  for(iters = 1; iters <= MAX_ITERS; iters *= 2)
    {
      puk_tick_t t1, t2;
      PUK_GET_TICK(t1);
      int i;
      for(i = 0; i < iters; i++)
        {
          nm_schedule(p_core);
        }
      PUK_GET_TICK(t2);
      const double usec = PUK_TIMING_DELAY(t1, t2);
      fprintf(stderr, " time = %f usec./iter; iters = %d\n", usec/iters, iters);
    }
}

int main(int argc, char **argv)
{
#ifdef RAW_INIT

  struct nm_core*p_core = NULL;
  int err = nm_core_init(&p_core);
  if(err != NM_ESUCCESS)
    {
      NM_FATAL("# error %d while initializing nmad core.\n", err);
    }
  fprintf(stderr, " submit task locked...\n");
  nm_core_task_submit_locked(p_core, &task_handler);
  fprintf(stderr, " submit task unlocked...\n");
  nm_core_task_submit_unlocked(p_core, &task_handler);
  fprintf(stderr, " submission done.\n");

  while(count < 2)
    {
      nm_schedule(p_core);
    }
  fprintf(stderr, "done.\n");

  benchmark(p_core);

  const char*strategy_name = "aggreg";
  fprintf(stderr, "# Loading strategy %s\n", strategy_name);
  puk_component_t strategy = nm_core_component_load("Strategy", strategy_name);
  err = nm_core_set_strategy(p_core, strategy);
  if(err != NM_ESUCCESS)
    {
      NM_FATAL("# error %d while loading strategy '%s'.\n", err, strategy_name);
    }

  benchmark(p_core);

  fprintf(stderr, "exit.\n");
  nm_core_exit(p_core);

#elif defined (SESSION_INIT)

  nm_session_t p_session = NULL;
  nm_session_create(&p_session, "nm_core_tasks");
  struct nm_core*p_core = nm_core_get_singleton();
  fprintf(stderr, "# after session create.\n");
  benchmark(p_core);
  const char*local_url = NULL;
  nm_session_init(p_session, &local_url);
  fprintf(stderr, "# after session init.\n");
  benchmark(p_core);

#else

  nm_launcher_init(&argc, argv);
  nm_session_t p_session = NULL;
  nm_session_open(&p_session, "nm_core_tasks");
  struct nm_core*p_core = nm_core_get_singleton();
  benchmark(p_core);
  nm_launcher_exit();
#endif

  return 0;
}

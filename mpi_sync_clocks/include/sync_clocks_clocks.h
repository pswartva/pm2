/*
 * MPI sync clocks
 * Copyright (C) 2006-2021 (see AUTHORS file)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

#ifndef SYNC_CLOCKS_CLOCKS_H
#define SYNC_CLOCKS_CLOCKS_H

/*
 * Some functions to deal with local clocks.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>

/* Use POSIX clock_gettime to measure time. Some MPI
 * implementations still use gettimeofday for MPI_Wtime(),
 * we cannot rely on it. Use RDTSC on mckernel (and
 * assume constant_tsc).
 */


#ifdef MCKERNEL
#define SYNC_CLOCKS_USE_RDTSC
#else
#define SYNC_CLOCKS_USE_CLOCK_GETTIME
#endif

#if defined(SYNC_CLOCKS_USE_PUK)
/* ** Puk-based timing ** */
#include <Padico/Puk.h>

typedef puk_tick_t sync_clocks_generic_tick_t;

#define sync_clocks_generic_get_tick PUK_GET_TICK

#define sync_clocks_generic_ticks2delay puk_ticks2delay

#define sync_clocks_generic_tick2usec puk_tick2usec

#elif defined(SYNC_CLOCKS_USE_CLOCK_GETTIME)
/* ** clock_gettime timing ** */

#include <time.h>
#include <sys/types.h>
#include <sys/time.h>

typedef struct timespec sync_clocks_generic_tick_t;

#if defined(__bg__)
#define sync_clocks_generic_get_tick(t) clock_gettime(CLOCK_REALTIME, &(t))
#elif defined(CLOCK_MONOTONIC_RAW)
#define sync_clocks_generic_get_tick(t) clock_gettime(CLOCK_MONOTONIC_RAW, &(t))
#else
#define sync_clocks_generic_get_tick(t) clock_gettime(CLOCK_MONOTONIC, &(t))
#endif

static inline struct timespec sync_clocks_generic_tick_diff(const struct timespec*t1, const struct timespec*t2)
{
  struct timespec diff;
  if(t2->tv_nsec > t1->tv_nsec)
    {
      diff.tv_sec  = t2->tv_sec - t1->tv_sec;
      diff.tv_nsec = t2->tv_nsec - t1->tv_nsec;
    }
  else
    {
      diff.tv_sec  = t2->tv_sec - t1->tv_sec - 1;
      diff.tv_nsec = t2->tv_nsec - t1->tv_nsec + 1000000000L;
    }
  return diff;
}

static inline double sync_clocks_generic_tick2usec(struct timespec t)
{
  double delay = 1000000.0 * t.tv_sec + t.tv_nsec / 1000.0;
  return delay;
}

static inline double sync_clocks_generic_ticks2delay(const struct timespec*t1, const struct timespec*t2)
{
  struct timespec diff = sync_clocks_generic_tick_diff(t1, t2);
  return sync_clocks_generic_tick2usec(diff);
}
#elif defined(SYNC_CLOCKS_USE_RDTSC)
/* ** rdtsc-based clock ** */

#include <time.h>

typedef uint64_t sync_clocks_generic_tick_t;

static inline void sync_clocks_generic_get_ptick(sync_clocks_generic_tick_t*t)
{
#ifdef __i386
  uint64_t r;
  __asm__ volatile ("rdtsc" : "=A" (r));
  *t = r;
#elif defined __amd64
  uint64_t a, d;
  __asm__ volatile ("rdtsc" : "=a" (a), "=d" (d));
  *t = (d<<32) | a;
#else
#error
#endif
}

#define sync_clocks_generic_get_tick(t) sync_clocks_generic_get_ptick(&(t))

static inline double sync_clocks_generic_ticks2delay(const sync_clocks_generic_tick_t*t1, const sync_clocks_generic_tick_t*t2)
{
  static double scale = -1;
  if(scale < 0)
    {
      uint64_t s1, s2;
      struct timespec ts = { 0, 10000000 /* 10 ms */ };
      sync_clocks_generic_get_tick(s1);
      nanosleep(&ts, NULL);
      sync_clocks_generic_get_tick(s2);
      scale = (ts.tv_sec * 1e6 + ts.tv_nsec / 1e3) / (double)(s2 - s1);
    }
  const uint64_t tick_diff = *t2 - *t1;
  const double delay = (tick_diff * scale);
  return delay;
}

static inline double sync_clocks_generic_tick2usec(const sync_clocks_generic_tick_t t)
{
  uint64_t t0 = 0;
  const double usec = sync_clocks_generic_ticks2delay(&t0, &t);
  return usec;
}
#else
#error no clock type defined.
#endif /* SYNC_CLOCKS_USE_PUK */

#endif /* SYNC_CLOCKS_CLOCKS_H */

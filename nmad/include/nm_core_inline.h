/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#ifndef NM_CORE_INLINE_H
#define NM_CORE_INLINE_H

#include <nm_private.h>

/** @ingroup nmad_private
 * @file
 * Inline functions for nmad core. These functions are defined inlined for performance.
 */

/* ** Driver management ************************************ */

/** Get the track per-gate data */
static inline struct nm_trk_s*nm_gate_trk_get(nm_gate_t p_gate, nm_drv_t p_drv)
{
  assert(p_drv != NULL);
  int i;
  for(i = 0; i < p_gate->n_trks; i++)
    {
      if(p_gate->trks[i].p_drv == p_drv)
        return &p_gate->trks[i];
    }
  return NULL;
}

/** Get a driver given its id.
 */
static inline struct nm_trk_s*nm_trk_get_by_index(nm_gate_t p_gate, int index)
{
  assert(p_gate->n_trks > 0);
  assert(index < p_gate->n_trks);
  assert(index >= 0);
  return &p_gate->trks[index];
}

/** get maximum size for small messages for the given driver */
static inline nm_len_t nm_drv_max_small(const struct nm_drv_s*const p_drv)
{
  const nm_len_t max_small = (NM_MAX_UNEXPECTED - NM_HEADER_DATA_SIZE - NM_ALIGN_FRONTIER);
  return (p_drv->props.capabilities.max_msg_size > max_small) ? max_small : p_drv->props.capabilities.max_msg_size;
}


/* ** Packet wrapper management **************************** */

/** assign packet to given driver, gate, and track */
static inline void nm_pw_assign(struct nm_pkt_wrap_s*p_pw, nm_trk_id_t trk_id, struct nm_drv_s*p_drv, nm_gate_t p_gate)
{
  p_pw->trk_id = trk_id;
  if(p_gate == NM_GATE_NONE)
    {
      assert(p_drv->p_pw_recv_any == NULL);
      assert(p_drv != NULL);
      p_drv->p_pw_recv_any = p_pw;
      p_pw->p_trk = NULL;
      p_pw->p_drv = p_drv;
    }
  else
    {
      assert(trk_id >= 0);
      assert(trk_id < p_gate->n_trks);
      p_pw->p_gate = p_gate;
      p_pw->p_trk = &p_gate->trks[trk_id];
      p_pw->p_drv = p_pw->p_trk->p_drv;
    }
}

static inline void nm_pw_ref_inc(struct nm_pkt_wrap_s*p_pw)
{
  nm_atomic_inc(&p_pw->ref_count);
}
static inline void nm_pw_ref_dec(struct nm_pkt_wrap_s*p_pw)
{
  const int count = nm_atomic_dec(&p_pw->ref_count);
  assert(count >= 0);
  if(count == 0)
    {
      assert(p_pw->p_drv != NULL);
      nm_pw_free(p_pw->p_drv->p_core, p_pw);
    }
}

/* ** Gate ************************************************* */

static inline int nm_gate_isactive(struct nm_gate_s*p_gate)
{
  return( (!nm_req_chunk_list_empty(&p_gate->req_chunk_list)) ||
          (!nm_ctrl_chunk_list_empty(&p_gate->ctrl_chunk_list)) ||
          (p_gate->strat_todo) ||
          (!nm_pkt_wrap_list_empty(&p_gate->pending_large_recv)));
}

static inline int nm_gate_is_in_active_list(struct nm_core*p_core, struct nm_gate_s*p_gate)
{
  return ((p_gate == nm_active_gate_list_begin(&p_core->active_gates)) ||
          (!nm_active_gate_list_cell_isnull(p_gate)));
}

/** mark gate as having active requests */
static inline void nm_gate_set_active(struct nm_gate_s*p_gate)
{
  nm_core_lock_assert(p_gate->p_core);
  if( (!nm_gate_is_in_active_list(p_gate->p_core, p_gate)) &&
      nm_gate_isactive(p_gate))
    {
      assert(!nm_active_gate_list_contains(&p_gate->p_core->active_gates, p_gate));
      nm_active_gate_list_push_back(&p_gate->p_core->active_gates, p_gate);
      assert(nm_gate_is_in_active_list(p_gate->p_core, p_gate));
    }
}

/* ** Core tasks ******************************************* */

static inline void nm_core_task_enqueue(struct nm_core*p_core, int holding_lock, struct nm_core_task_s*p_core_task)
{
  if(holding_lock)
    {
      nm_core_lock_assert(p_core);
    }
  else
    {
      nm_core_nolock_assert(p_core);
    }
  int rc;
  do
    {
#ifdef PIOMAN_MULTITHREAD
      rc = nm_core_task_lfqueue_enqueue(&p_core->pending_tasks, p_core_task);
#else
      rc = nm_core_task_lfqueue_enqueue_single_writer(&p_core->pending_tasks, p_core_task);
#endif /* PIOMAN_MULTITHREAD */
      if(rc)
        {
          if(holding_lock)
            {
              nm_core_task_flush(p_core);
            }
          else
            {
              if(nm_core_trylock(p_core))
                {
                  nm_core_task_flush(p_core);
                  nm_core_unlock(p_core);
                }
            }
        }
    }
  while(rc);
}

/* ** Strategy ********************************************* */

static inline void nm_strat_submit_req_chunk(nm_core_t p_core, nm_gate_t p_gate, struct nm_req_chunk_s*p_req_chunk, int front)
{
  nm_core_lock_assert(p_core);
  if(p_core->strategy_iface->submit_req_chunk == NULL)
    {
      /* default implem for strat submit_req_chunk */
      if(front)
        {
          /* submit to front of list (for repacks) */
          nm_req_chunk_list_push_front(&p_gate->req_chunk_list, p_req_chunk);
        }
      else
        {
          /* regular: enqueue at back of list */
          nm_req_chunk_list_push_back(&p_gate->req_chunk_list, p_req_chunk);
        }
    }
  else
    {
      /* call strategy submit_req_chunk */
      (*p_core->strategy_iface->submit_req_chunk)(p_core->strategy_context, p_req_chunk, front);
    }
  nm_gate_set_active(p_gate);
}

/** process postponed rdv requests */
static inline void nm_strat_rdv_accept(nm_core_t p_core, nm_gate_t p_gate)
{
  nm_core_lock_assert(p_core);
  if(!nm_pkt_wrap_list_empty(&p_gate->pending_large_recv))
    {
      struct puk_receptacle_NewMad_Strategy_s*r = &p_gate->strategy_receptacle;
      if(r->driver->rdv_accept)
        {
          (*r->driver->rdv_accept)(r->_status, p_gate);
        }
    }
  nm_gate_set_active(p_gate);
}

/** apply strategy on the given gate, for the case where strategy doesn't have global scheduling  */
static inline void nm_strat_gate_schedule(nm_core_t p_core, nm_gate_t p_gate)
{
  nm_core_lock_assert(p_core);
  assert(p_gate != NULL);
  assert(p_core->strategy_iface->schedule == NULL);
  nm_strat_rdv_accept(p_core, p_gate); /* accept rdv early so that rtr may be schedule by the try_and_commit below */
  struct puk_receptacle_NewMad_Strategy_s*r = &p_gate->strategy_receptacle;
  nm_profile_inc(p_gate->p_core->profiling.n_try_and_commit);
  /* schedule new outgoing requests on all gates */
  (*r->driver->try_and_commit)(r->_status, p_gate);
}

/** apply strategy on the given gate (all active gates if p_gate = NULL) */
static inline void nm_strat_schedule(nm_core_t p_core, nm_gate_t p_gate)
{
#ifdef NMAD_DEBUG
  static int inprogress = 0;
  if(p_gate == NULL)
    {
      assert(!inprogress); /* make sure no global strat scheduling is done recursively */
      inprogress = 1;
    }
#endif /* NMAD_DEBUG */
  nm_core_lock_assert(p_core);
  if(p_core->strategy_iface != NULL && p_core->strategy_iface->schedule != NULL)
    {
      /* context-wide strategy */
      (*p_core->strategy_iface->schedule)(p_core->strategy_context);
    }
  else
    {
      if(p_gate == NULL)
        {
          nm_gate_t p_tmp_gate;
          puk_list_foreach_safe(nm_active_gate, p_gate, p_tmp_gate, &p_core->active_gates)
            {
              assert(nm_active_gate_list_contains(&p_core->active_gates, p_gate));
              if(p_gate->status == NM_GATE_STATUS_CONNECTED)
                {
                  nm_strat_gate_schedule(p_core, p_gate);
                  assert(nm_active_gate_list_contains(&p_core->active_gates, p_gate));
                  if(!nm_gate_isactive(p_gate))
                    {
                      nm_active_gate_list_remove(&p_core->active_gates, p_gate);
                      nm_active_gate_list_cell_setnull(p_gate); /* mark cell as not enqueued in any list */
                    }
                }
            }
        }
      else
        {
          nm_strat_gate_schedule(p_core, p_gate);
        }
    }
#ifdef NMAD_DEBUG
  inprogress = 0;
#endif /* NMAD_DEBUG */
}

static inline void nm_strat_pack_ctrl(nm_gate_t p_gate, nm_header_ctrl_generic_t*p_header)
{
  struct nm_core*p_core = p_gate->p_core;
  nm_core_lock_assert(p_core);
  struct nm_ctrl_chunk_s*p_ctrl_chunk = nm_ctrl_chunk_malloc(&p_core->ctrl_chunk_allocator);
  nm_ctrl_chunk_list_cell_init(p_ctrl_chunk);
  p_ctrl_chunk->ctrl = *p_header;
  p_ctrl_chunk->p_gate = p_gate;
  if(p_core->strategy_iface->submit_ctrl_chunk == NULL)
    {
      nm_ctrl_chunk_list_push_back(&p_gate->ctrl_chunk_list, p_ctrl_chunk);
      nm_gate_set_active(p_gate);
    }
  else
    {
      (*p_core->strategy_iface->submit_ctrl_chunk)(p_core->strategy_context, p_ctrl_chunk);
    }
}

/** enqueue a pw completion, or process immediately if possible */
static inline void nm_pw_completed_enqueue(struct nm_core*p_core, struct nm_pkt_wrap_s*p_pw)
{
  nm_core_nolock_assert(p_core);
  assert(!(p_pw->flags & NM_PW_COMPLETED));
  p_pw->flags |= NM_PW_COMPLETED;
#ifdef PIOMAN
  piom_ltask_completed(&p_pw->ltask);
#endif
  p_pw->core_task.kind = NM_CORE_TASK_COMPLETED_PW;
  p_pw->core_task.content.completed_pw.p_pw = p_pw;
  nm_core_task_enqueue(p_core, 0, &p_pw->core_task);
}


/* ** req chunks ******************************************* */

static inline void nm_req_chunk_submit(struct nm_core*p_core, struct nm_req_chunk_s*p_req_chunk)
{
  nm_core_nolock_assert(p_core);
  assert(p_req_chunk->p_req != NULL);
  p_req_chunk->core_task.kind = NM_CORE_TASK_PACK_SUBMISSION;
  p_req_chunk->core_task.content.pack_submission.p_req_chunk = p_req_chunk;
  nm_core_task_enqueue(p_core, 0, &p_req_chunk->core_task);
}

static inline void nm_req_chunk_destroy(struct nm_core*p_core, struct nm_req_chunk_s*p_req_chunk)
{
  struct nm_req_s*p_pack = p_req_chunk->p_req;
#ifdef NMAD_DEBUG
  p_req_chunk->chunk_len = NM_LEN_UNDEFINED;
  p_req_chunk->chunk_offset = NM_LEN_UNDEFINED;
#endif
  if(p_req_chunk != &p_pack->req_chunk)
    {
      nm_req_chunk_free(&p_core->req_chunk_allocator, p_req_chunk);
    }
  else
    {
      p_pack->req_chunk.p_req = NULL;
    }
}

static inline struct nm_req_chunk_s*nm_req_chunk_alloc(struct nm_core*p_core)
{
  struct nm_req_chunk_s*p_req_chunk = nm_req_chunk_malloc(&p_core->req_chunk_allocator);
  nm_req_chunk_list_cell_init(p_req_chunk);
  return p_req_chunk;
}

static inline void nm_req_chunk_init(struct nm_req_chunk_s*p_req_chunk, struct nm_req_s*p_req,
                                     nm_len_t chunk_offset, nm_len_t chunk_len)
{
  nm_req_chunk_list_cell_init(p_req_chunk);
  p_req_chunk->p_req        = p_req;
  p_req_chunk->chunk_offset = chunk_offset;
  p_req_chunk->chunk_len    = chunk_len;
  p_req_chunk->proto_flags  = 0;
  assert(chunk_offset + chunk_len <= p_req->pack.len);
  if(chunk_offset + chunk_len == p_req->pack.len)
    {
      p_req_chunk->proto_flags |= NM_PROTO_FLAG_LASTCHUNK;
    }
  if(p_req->flags & NM_REQ_FLAG_PACK_SYNCHRONOUS)
    {
      p_req_chunk->proto_flags |= NM_PROTO_FLAG_ACKREQ;
    }
  nm_data_chunk_properties_compute(&p_req->data, chunk_offset, chunk_len, &p_req_chunk->chunk_props);
}

/** Post a ready-to-receive to accept chunk on given trk_id
 */
static inline void nm_core_post_rtr(nm_gate_t p_gate,  nm_core_tag_t tag, nm_seq_t seq,
                                    nm_trk_id_t trk_id, nm_len_t chunk_offset, nm_len_t chunk_len,
                                    const void*p_rdv_data)
{
  nm_header_ctrl_generic_t h;
  if(chunk_len == 0)
    {
      NM_FATAL("trying to post RTR with chunk_offset = %lu; chunk_len = %lu\n", chunk_offset, chunk_len);
    }
  nm_header_init_rtr(&h, tag, seq, trk_id, chunk_offset, chunk_len, p_rdv_data);
  nm_strat_pack_ctrl(p_gate, &h);
}

/** Post an ACK
 */
static inline void nm_core_post_ack(nm_gate_t p_gate, nm_core_tag_t tag, nm_seq_t seq)
{
  nm_header_ctrl_generic_t h;
  nm_header_init_ack(&h, tag, seq);
  nm_strat_pack_ctrl(p_gate, &h);
}

static inline void nm_core_post_msg(nm_gate_t p_gate, nm_core_tag_t tag, nm_seq_t seq, nm_len_t msg_len)
{
  nm_header_ctrl_generic_t h;
  nm_header_init_msg(&h, tag, seq, msg_len);
  nm_strat_pack_ctrl(p_gate, &h);
}

/** dynamically adapt pioman polling frequency level depending on the number of pending requests */
static inline void nm_core_polling_level(struct nm_core*p_core)
{
  nm_core_lock_assert(p_core);
  assert(p_core->n_packs >= 0);
  assert(p_core->n_unpacks >= 0);
#ifdef PIOMAN
  const int high =
    (p_core->n_packs > 0) ||
    (p_core->n_unpacks > 0) ||
    (!nm_core_task_lfqueue_empty(&p_core->pending_tasks));
  piom_ltask_poll_level_set(high);
#endif /* PIOMAN */
}

/* ** Tags & matching ************************************** */

static inline struct nm_matching_wildcard_s*nm_matching_wildcard_bytag(struct nm_core*p_core, nm_core_tag_t core_tag)
{
  const nm_core_tag_t session_tag = nm_core_tag_build(nm_core_tag_get_hashcode(core_tag), 0);
  struct nm_matching_wildcard_s*p_wildcard = nm_matching_wildcard_get(&p_core->wildcard_table, session_tag);
  return p_wildcard;
}

static inline struct nm_matching_gsession_s*nm_matching_gsession_bytag(struct nm_gate_s*p_gate, nm_core_tag_t core_tag)
{
  const nm_core_tag_t session_tag = nm_core_tag_build(nm_core_tag_get_hashcode(core_tag), 0);
  struct nm_matching_gsession_s*p_gsession = nm_matching_gsession_get(&p_gate->gsessions, session_tag);
  return p_gsession;
}


#endif /* NM_CORE_INLINE_H */

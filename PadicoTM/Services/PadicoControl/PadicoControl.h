/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file NetAccess remote control -- control channel management
 * @ingroup NetAccess
 */

#ifndef PADICO_REMOTECONTROL_H
#define PADICO_REMOTECONTROL_H

#include <Padico/Puk.h>
#include <Padico/PadicoTM.h>
#include <Padico/Topology.h>

PUK_MOD_AUTO_DEP(PadicoControl, PADICO_LAST_MODULE_NAME);
#undef PADICO_LAST_MODULE_NAME
#define PADICO_LAST_MODULE_NAME PadicoControl

#ifdef __cplusplus
extern "C" {
#endif

  /** @defgroup NetAccessControl API: NetAccess control channel
   * @ingroup NetAccess
   */

  /** @addtogroup NetAccessControl
   * @{
   */

  /* *** Internal PadicoControl API */

  typedef struct padico_control_msg_s*padico_control_msg_t;

  /** create a new empty control message */
  extern padico_control_msg_t padico_control_msg_new(void);

  /** destroy a control message */
  extern void padico_control_msg_destroy(padico_control_msg_t msg);

  /** @internal get a pointer to the cmd part */
  extern padico_string_t padico_control_msg_get_cmd(padico_control_msg_t msg);

#define padico_control_msg_add_cmd(MSG, TEXT, ...)                      \
  padico_string_catf(padico_control_msg_get_cmd(MSG), (TEXT), ##__VA_ARGS__ )


  /** add an attached binary part to a control message */
  extern void padico_control_msg_add_part(padico_control_msg_t msg, const void*text, size_t len);

  /** get the number of iovec entries in the control message */
  extern int padico_control_msg_get_iovec_size(padico_control_msg_t msg);

  /** get an iovec from the control message content; caller takes ownership of iovec and must free it */
  extern const struct iovec*padico_control_msg_get_iovec(padico_control_msg_t msg);

  /** get the total number of bytes in the control message */
  extern size_t padico_control_msg_get_size(padico_control_msg_t msg);

  /** get a (contiguous) copy of the content of the control message; caller takes ownership of the copy */
  extern struct iovec padico_control_msg_get_bytes(padico_control_msg_t msg);

  /* The home interface for PadicoControl */

  /** @internal */
  padico_tasklet_t padico_control_get_event_channel(void);
  /** @internal */
  void             padico_control_gen_init_events(padico_event_listener_t l);

  /** deliver a message from a Controler */
  void padico_control_deliver_message(puk_instance_t controler, padico_topo_node_t from, char*cmd, size_t size);

  void padico_control_send_ext(padico_topo_node_t node, const char*cmd, uint32_t req_id);

  /** the real function to send a message on control channel- most of the time, we use a wraper, however. */
  int  padico_control_send_msg(padico_topo_node_t node, padico_control_msg_t msg);

#ifndef __cplusplus

  /** Send a "one way" message on the control channel. */
  static inline void padico_control_send_oneway(padico_topo_node_t node, const char*msg)
  {
    padico_control_send_ext(node, msg, PADICO_REQ_ID_NONE);
  }

  /** Send an async request on the control channel. */
  static inline void padico_control_send_async(padico_topo_node_t node, const char*cmd,
                                               padico_task_notifier_t notify, void*key)
  {
    padico_req_t req = padico_tm_req_new(notify, key);
    padico_control_send_ext(node, cmd, req->id);
  }

  /** Send a blocking request on the control channel. */
  static inline padico_req_t padico_control_send(padico_topo_node_t node, const char*cmd)
  {
    padico_req_t req = padico_tm_req_new(NULL, NULL);
    padico_control_send_ext(node, cmd, req->id);
    return req;
  }

  /** Get the control channel as a network */
  static inline padico_topo_network_t padico_control_getnetwork(void)
  {
    static padico_topo_network_t net = NULL;
    if(net == NULL)
      net = padico_topo_network_getbyid("ControlNetwork");
    return net;
  }

#endif /* __cplusplus */


  /* *** NetAccess Control *********************************** */

  struct padico_control_event_s
  {
    enum padico_control_event_kind_e
      {
        /** a new node appeared in the known topology */
        PADICO_CONTROL_EVENT_NEW_NODE         = 0x01,
        /** a node was deleted/disconnected/became unreachable */
        PADICO_CONTROL_EVENT_DELETE_NODE      = 0x02,
        /** a new controler (PadicoControl) is loaded */
        PADICO_CONTROL_EVENT_PLUG_CONTROLER   = 0x03,
        /** a new connector (PadicoConnector) is loaded */
        PADICO_CONTROL_EVENT_PLUG_CONNECTOR   = 0x04,
        /** a new control selector (ControlSelector) is loaded */
        PADICO_CONTROL_EVENT_PLUG_SELECTOR    = 0x05,
        /** a controler (PadicoControl) is unloaded */
        PADICO_CONTROL_EVENT_UNPLUG_CONTROLER = 0x06,
        /** a connector (PadicoConnector) is unloaded */
        PADICO_CONTROL_EVENT_UNPLUG_CONNECTOR = 0x07,
        /** a control selector (ControlSelector) is unloaded */
        PADICO_CONTROL_EVENT_UNPLUG_SELECTOR  = 0x08,
        /** initial event sent to new controlers to get the past events and current topology state */
        PADICO_CONTROL_EVENT_INIT_CONTROLER   = 0x09,
        /** @internal force processing in the XML tasklet */
        PADICO_CONTROL_EVENT_PROCESSING       = 0x0a,
        /** local node is shuting down, stop processing some events */
        PADICO_CONTROL_EVENT_SHUTDOWN         = 0x0b
      } kind;
    union
    {
      struct
      {
        puk_instance_t controler;
        padico_topo_node_vect_t nodes;
      } INIT_CONTROLER;
      struct
      {
        puk_instance_t controler;
        union
        {
          padico_topo_node_t node;
          padico_topo_node_vect_t nodes;
        };
      } NODE;
      struct
      {
        puk_instance_t controler;
        puk_instance_t server;    /**< the server instance that created the new controler; may be NULL */
      } PLUG_CONTROLER;
      struct
      {
        puk_component_t connector;
      } PLUG_CONNECTOR;
      struct
      {
        puk_component_t selector;
      } PLUG_SELECTOR;
      struct
      {
        puk_instance_t controler;
      } UNPLUG_CONTROLER;
      struct
      {
        puk_component_t connector;
      } UNPLUG_CONNECTOR;
      struct
      {
        puk_component_t selector;
      } UNPLUG_SELECTOR;
      struct /** Control command descriptor, in HOST byte order; describes a received request. */
      {
        padico_topo_node_t node;         /**< sender of this request */
        char*cmd;                      /**< msg of the request */
        size_t size;                   /**< message size (including attachments) */
      } PROCESSING;
    };
  };
  typedef struct padico_control_event_s*padico_control_event_t;

  /* shortcuts for the event channel */

#ifndef __cplusplus

  static inline void padico_control_event_enable(void)
  {
    padico_tasklet_release(padico_control_get_event_channel());
  }
  static inline void padico_control_event_disable(void)
  {
    padico_tasklet_acquire(padico_control_get_event_channel());
  }
  static inline void padico_control_event_flush(void)
  {
    padico_tasklet_flush(padico_control_get_event_channel());
  }
  static inline void padico_control_event_sync(void)
  {
    padico_control_event_disable();
    padico_control_event_flush();
    padico_control_event_enable();
  }
  static inline void padico_control_event_subscribe(padico_event_listener_t l)
  {
    padico_control_event_disable();
    padico_control_gen_init_events(l);
    padico_tasklet_subscribe(padico_control_get_event_channel(), l);
    padico_control_event_flush();
    padico_control_event_enable();
  }
  static inline void padico_control_event_unsubscribe(padico_event_listener_t l)
  {
    padico_control_event_disable();
    padico_tasklet_unsubscribe(padico_control_get_event_channel(), l);
    padico_control_event_flush();
    padico_control_event_enable();
  }


  static inline void padico_control_new_node(puk_instance_t controler,
                                             padico_topo_node_t node)
  {
    struct padico_control_event_s*event = padico_malloc(sizeof(struct padico_control_event_s));
    event->kind = PADICO_CONTROL_EVENT_NEW_NODE;
    event->NODE.controler = controler;
    event->NODE.node      = node;
    padico_tasklet_schedule(padico_control_get_event_channel(), event, "event new node");
  }
  static inline void padico_control_new_node_vect(puk_instance_t controler,
                                                  padico_topo_node_vect_t nodes)
  {
    padico_topo_node_vect_itor_t i;
    puk_vect_foreach(i, padico_topo_node, nodes)
      {
        padico_control_new_node(controler, *i);
      }
  }
  static inline void padico_control_plug_controler(puk_instance_t controler, puk_instance_t server)
  {
    struct padico_control_event_s*event = padico_malloc(sizeof(struct padico_control_event_s));
    event->kind = PADICO_CONTROL_EVENT_PLUG_CONTROLER;
    event->PLUG_CONTROLER.controler = controler;
    event->PLUG_CONTROLER.server = server;
    padico_tasklet_schedule(padico_control_get_event_channel(), event, "event plug controler");
  }

  static inline void padico_control_plug_connector(puk_component_t connector)
  {
    struct padico_control_event_s*event = padico_malloc(sizeof(struct padico_control_event_s));
    event->kind = PADICO_CONTROL_EVENT_PLUG_CONNECTOR;
    event->PLUG_CONNECTOR.connector = connector;
    padico_tasklet_schedule(padico_control_get_event_channel(), event, "event plug connector");
  }

  static inline void padico_control_plug_selector(puk_component_t selector)
  {
    struct padico_control_event_s*event = padico_malloc(sizeof(struct padico_control_event_s));
    event->kind = PADICO_CONTROL_EVENT_PLUG_SELECTOR;
    event->PLUG_SELECTOR.selector = selector;
    padico_tasklet_schedule(padico_control_get_event_channel(), event, "event plug selector");
  }
  static inline void padico_control_unplug_controler(puk_instance_t controler)
  {
    struct padico_control_event_s*event = padico_malloc(sizeof(struct padico_control_event_s));
    event->kind = PADICO_CONTROL_EVENT_UNPLUG_CONTROLER;
    event->UNPLUG_CONTROLER.controler = controler;
    padico_tasklet_schedule(padico_control_get_event_channel(), event, "event unplug controler");
  }
  static inline void padico_control_unplug_connector(puk_component_t connector)
  {
    struct padico_control_event_s*event = padico_malloc(sizeof(struct padico_control_event_s));
    event->kind = PADICO_CONTROL_EVENT_UNPLUG_CONNECTOR;
    event->UNPLUG_CONNECTOR.connector = connector;
    padico_tasklet_schedule(padico_control_get_event_channel(), event, "event unplug connector");
  }

  static inline void padico_control_unplug_selector(puk_component_t selector)
  {
    struct padico_control_event_s*event = padico_malloc(sizeof(struct padico_control_event_s));
    event->kind = PADICO_CONTROL_EVENT_UNPLUG_SELECTOR;
    event->UNPLUG_SELECTOR.selector = selector;
    padico_tasklet_schedule(padico_control_get_event_channel(), event, "event unplug selector");
  }

  static inline void padico_control_delete_node(puk_instance_t controler,
                                                padico_topo_node_t node)
  {
    if(node != NULL)
      {
        struct padico_control_event_s*event = padico_malloc(sizeof(struct padico_control_event_s));
        event->kind = PADICO_CONTROL_EVENT_DELETE_NODE;
        event->NODE.controler = controler;
        event->NODE.node      = node;
        padico_tasklet_schedule(padico_control_get_event_channel(), event, "event delete node");
      }
  }

#endif /* __cplusplus */


  /* ** PadicoControl ************************************** */

  /** Interface: "PadicoControl" */
  struct padico_control_driver_s
  {
    void (*send_common)(void*_status, padico_topo_node_t node, padico_control_msg_t msg);
  };
  PUK_IFACE_TYPE(PadicoControl, struct padico_control_driver_s);


  /* ** PadicoConnector ************************************ */

  /** Interface: "PadicoConnector"
   * Connection for PadicoControl with explicit address transfer.
   * @note either init() (address by context) or connector_new() (address by instance)
   * is non-null */
  struct padico_control_connector_driver_s
  {
    /** initialize a new connector context- may be NULL */
    int (*connector_init)(puk_context_t context);
    /** activate an instanciated connector */
    int (*connector_new)(void*_status, const void**local_addr, size_t*addr_size);
    /** establishes a control connection to the remote given address
     * The call is blocking.
     * Expects both peers do a 'connect' to each other */
    int (*connector_connect)(void*_status, const void*remote_addr, size_t addr_size);
    /** close a connector context- may be NULL */
    void (*connector_close)(puk_context_t context);
  };
  PUK_IFACE_TYPE(PadicoConnector, struct padico_control_connector_driver_s);


  /* ** PadicoBootstrap ************************************ */

  /** Interface: "PadicoBootstrap"
   * Connection for PadicoControl with implicit address  */
  struct padico_bootstrap_driver_s
  {
    /** create a new listening bootstrap endpoint. */
    int (*bootstrap_new)    (void*_status);
    /** connect to a listening bootstrap endpoint */
    int (*bootstrap_connect)(void*_status, const char*hostname, padico_topo_uuid_t uuid);
  };
  PUK_IFACE_TYPE(PadicoBootstrap, struct padico_bootstrap_driver_s);


  /* ** ControlSelector ************************************ */

  /** Interface: "ControlSelector" */
  struct padico_control_selector_driver_s
  {
    /** returns an instance with a PadicoControl facet */
    puk_instance_t (*router)(padico_topo_node_t node);
  };
  PUK_IFACE_TYPE(ControlSelector, struct padico_control_selector_driver_s);


  /** @} */


#include "na-Group.h"


#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* PADICO_REMOTECONTROL_H */

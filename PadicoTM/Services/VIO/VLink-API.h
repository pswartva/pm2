/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief API definition for VLink
 * @ingroup VIO
 */

#ifndef VLINK_API_H
#define VLINK_API_H

#include <Padico/Puk.h>
#include <Padico/PadicoTM.h>
#include <Padico/Topology.h>
#include <Padico/NetSelector.h>
#include <Padico/SysIO.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#ifdef HAVE_POLL_H
#include <poll.h>
#endif

/* *** Opaque types **************************************** */

typedef struct padico_vnode_s*padico_vnode_t;
typedef struct vlink_assembly_request_s*vlink_assembly_request_t;


/* ********************************************************* */
/* *** VLink drivers *************************************** */

/** Driver for interface: "VLink"
 */
struct padico_vlink_driver_s
{
  /* *** Fabric */
  /** Creates the driver-specific part
   * @return -errno (0 if success)
   */
  int  (*create)      (padico_vnode_t);
  /** binds the vnode to a local address
   */
  int  (*bind)        (padico_vnode_t);
  int  (*listen)      (padico_vnode_t); /**< listens for incoming connections */
  int  (*connect)     (padico_vnode_t); /**< connects as client */
  void (*fullshutdown)(padico_vnode_t); /**< shutdowns the connection (but do not close the vnode) */
  int  (*close)       (padico_vnode_t); /**< closes (destroy) the vnode */
  /* *** Connected streaming-based messaging */
  void (*send_post)   (padico_vnode_t);
  void (*recv_post)   (padico_vnode_t);
  /* *** Debug */
  padico_rc_t (*dumpstat)(padico_vnode_t);
};
PUK_IFACE_TYPE(VLink, struct padico_vlink_driver_s);

/** Driver for interface: 'VLinkMessaging' (connection-less messaging)
 */
struct padico_vlink_messaging_driver_s
{
  ssize_t (*sendto)(padico_vnode_t, const void*, size_t, padico_topo_node_t, int port);
  ssize_t (*recvfrom)(padico_vnode_t, void*buf, size_t);
};
typedef struct padico_vlink_messaging_driver_s*padico_vlink_messaging_driver_t;
PUK_IFACE_TYPE(VLinkMessaging, struct padico_vlink_messaging_driver_s);

/** Driver for interface: 'VPipe'
 */
struct padico_vpipe_driver_s
{
  int (*pipe)(padico_vnode_t vnodes[2]);
};
PUK_IFACE_TYPE(VPipe, struct padico_vpipe_driver_s);

/** Driver for interface: 'VFile'
*/
struct padico_vfile_driver_s
{
  int   (*open)     (padico_vnode_t vnode, const char*path, int oflag, mode_t mode);
  off_t (*lseek)    (padico_vnode_t, off_t offset, int whence);
  int   (*ftruncate)(padico_vnode_t, off_t);
  int   (*fxstat)   (int version, padico_vnode_t vnode, struct stat*buf);
  void* (*mmap)     (void*addr, size_t len, int prot, int flags, padico_vnode_t vnode, off_t off);
  int   (*fcntl)    (padico_vnode_t vnode, int cmd, void*arg);
  int   (*fsync)    (padico_vnode_t vnode);
};
PUK_IFACE_TYPE(VFile, struct padico_vfile_driver_s);

/** Driver for interface: 'VFile64'
*/
struct padico_vfile64_driver_s
{
  int   (*open)     (padico_vnode_t vnode, const char*path, int oflag, mode_t mode);
  off_t (*lseek)    (padico_vnode_t, off64_t offset, int whence);
  int   (*ftruncate)(padico_vnode_t, off64_t);
  int   (*fxstat)   (int version, padico_vnode_t vnode, struct stat64*buf);
  void* (*mmap)     (void*addr, size_t len, int prot, int flags, padico_vnode_t vnode, off64_t off);
  int   (*fcntl)    (padico_vnode_t vnode, int cmd, void*arg);
  int   (*fsync)    (padico_vnode_t vnode);
};
PUK_IFACE_TYPE(VFile64, struct padico_vfile64_driver_s);


/* ********************************************************* */
/* *** Attributes for vobs ********************************* */

/** enumerates common attributes in vnodes
 */
typedef enum
  {
    vobs_attr_status = 0,
    vobs_attr_socket,
    vobs_attr_remote,
    vobs_attr_listener,
    vobs_attr_rbox,
    vobs_attr_wbox,
    vobs_attr_pivot,
    VOBS_ATTR_MAX
  }
vobs_attr_t;

/** status description for generic I/O
 */
struct vobs_attr_status_s
{
  volatile int error;       /**< error state */
  unsigned readable    : 1; /**< immediate 'read' will not block */
  unsigned writable    : 1; /**< immediate 'write' will not block */
  unsigned opened      : 1; /**< this vnode is currently opened */
  unsigned connecting  : 1; /**< we are in the connection phase */
  unsigned end_of_file : 1; /**< we reached end-of-file */
  uint8_t  pending_op;      /**< asynchronous operation in progress- vnode cannot be closed */
};

/** Capabilities of VLink socket drivers. These attributes are supposed to be put
 * into vnode->socket.capabilities by the driver in the 'create()' phase. They
 * are read by VLink_selector.
 * @note default capabilities (if none given): driver is able to direct-connect
 * and be used as secondary (secondary is preferred is possible), driver is
 * able to be used as client and server, client is not required to bind
 * before connecting.
 * @warning some attributes are mutually exclusive:
 *   directconnect and secondaryonly
 *   clientonly and serveronly
 *   directconnect and afpadicoonly
 * afpadicoonly implies secondaryonly.
 */
enum padico_vsock_capabilities_e
  {
    /** don't attempt to send an assembly request- directconnect only (through real sockets) */
    padico_vsock_caps_directconnect  = 0x01,
    /** only usable as secondary- cannot do direct-connect */
    padico_vsock_caps_secondaryonly  = 0x02,
    /** only usable as client- no server-side implementation */
    padico_vsock_caps_clientonly     = 0x04,
    /** only usable as server- no client-side implementation */
    padico_vsock_caps_serveronly     = 0x08,
    /** even clients must bind()- perform autobind if needed */
    padico_vsock_caps_clientmustbind = 0x10,
    /** driver understands only addresses in the AF_PADICO family- AF_INET, AF_UNIX not supported */
    padico_vsock_caps_afpadicoonly   = 0x20
  };

/** status description specialized for virtual sockets (vsocks).
 */
struct vobs_attr_socket_s
{
  unsigned capabilities; /**< socket capabilities exposed by the driver (bitmask) [read-only for the user] */
  unsigned so_options;   /**< socket options (bitmask) @note see setsockopt() */
  /** @note among so_* fields, only so_options is actually used by VIO.
   * so_family, so_type and so_protocol are required only to create a
   * socket of the same type upon accept() and to reply to getsockname().
   */
  int      so_family;   /**< network family (AF_INET,...) */
  int      so_type;     /**< protocol type (SOCK_STREAM, SOCK_DGRAM,...) */
  int      so_protocol; /**< specific protocol */
};

/** a (local) binding for a socket
 */
struct vobs_attr_binding_s
{
  struct sockaddr*primary;
  socklen_t primary_len;
  int autobind;  /**< auto-bind request: padico_vsock_bind() will allocate a new address */
};

/* This directive builds 'padico_vnode_queue_t' based on 'padico_vnode_t' */
PUK_QUEUE_TYPE(padico_vnode, struct padico_vnode_s*);

/** a listener for a socket
 */
struct vobs_attr_listener_s
{
  padico_vnode_queue_t backlog; /**< incoming connections queue. */
  int backlog_size;             /**< max size of the backlog queue */
  const struct padico_vaddr_s*src_addr;
};

/** the peer of a connected socket
 */
struct vobs_attr_remote_s
{
  struct sockaddr* remote_addr;
  socklen_t remote_addrlen;
  unsigned established: 1;
  unsigned can_send: 1;
  unsigned can_recv: 1;
};

/** an incoming (read) mailbox
 */
struct vobs_attr_rbox_s
{
  ssize_t read_posted;
  size_t  read_done;
  void*   read_buffer;
  int     cant_interrupt_read; /**< lock this flag not to interrupt a read operation */
  struct sockaddr*from;
  int*fromlen;
  marcel_t tid;
};

/** an outgoing (write) mailbox
 */
struct vobs_attr_wbox_s
{
  ssize_t write_posted;
  ssize_t write_done;
  void*   write_buffer;
  int     is_owner;
};

/** a pivot request
 */
struct vobs_attr_pivot_s
{
  padico_vnode_t new_vnode;
};


/* ********************************************************* */
/* *** Observers (vobs) ************************************ */

/** type of handler function for notifying changes in observed attributes (bottom-up)
 */
typedef void (*vobs_notifier_func_t)(padico_vnode_t vnode, vobs_attr_t attr, void*key);


/** type of handler function for committing direct changed attributes (top-down)
 */
typedef void (*vobs_commit_func_t)(padico_vnode_t vnode, vobs_attr_t attr);

/** A reference to a secondary observer watching us (us, the vnode)
 */
struct vobs_secondary_ref_s
{
  struct vobs_secondary_s*obs; /**< the secondary observer */
  int index;                   /**< our rank in the above observer */
};
PUK_VECT_TYPE(vobs_secondary_ref, struct vobs_secondary_ref_s);

/** An observer record inside a vnode.
 * @note 3 kinds of observers:
 *   -- 'commit' is used by the vnode itself to track changes done
 *      in its own attributes by upper layers;
 *   -- 'notifier' (primary observer) is used by upper layers to track
 *      events in the vnode they are watching;
 *   -- 'secondary' is used for aggregation of multiple vnodes (select()-like).
 * These fields are not supposed to be used directly. Use the
 * set of vobs_* inline functions instead.
 */
struct vobs_attr_observer_s
{
  vobs_commit_func_t commit;   /**< 'commit' observer: invoked to signal changes in direct-access attributes */
  marcel_cond_t wait_event;    /**< cond signaled upon io event. */
  struct vobs_notify_s
  {
    vobs_notifier_func_t func; /**< function called upon event (NULL if none) */
    void*key;                  /**< arg given to above function */
  } notifier;   /**< handler for notifying changes in observed attributes (called upon event) */
  struct vobs_secondary_ref_vect_s secondary;  /**< secondary observers who are watching us */
};

/** A single vnode R/W observer
 */
struct padico_vnode_observer_s
{
  padico_vnode_t vnode;
  short oevents; /**< original events form pollfd struct */
  short event;   /**< event beeing observed */
  short revent;  /**< result value */
};

#ifdef POLLIN
#define VNODE_EVENT_READ  POLLIN
#define VNODE_EVENT_WRITE POLLOUT
#define VNODE_EVENT_ERROR POLLERR
#else
#define VNODE_EVENT_READ  0x01
#define VNODE_EVENT_WRITE 0x02
#define VNODE_EVENT_ERROR 0x04
#endif

/** A secondary vnode R/W observer, for a group of vnodes.
 * @note typically used by higher level poll() or select().
 */
struct vobs_secondary_s
{
  struct padico_vnode_observer_s*observers; /**< the primary observers we are watching */
  int            n_obs;                     /**< number of primary observers */
  marcel_mutex_t lock;
  marcel_cond_t  waiting;
};
typedef struct vobs_secondary_s*vobs_secondary_t;


/* ********************************************************* */
/* *** vnode content *************************************** */


/** virtual node descriptor specialized for real (kernel) file descriptors.
 */
struct vnode_hfd_s
{
  int         fd;  /**< associated file descriptor */
  padico_io_t io_read;
  padico_io_t io_write;
};

struct padico_vnode_content_t
{
  unsigned hfd      : 1;
  unsigned socket   : 1;
  unsigned binding  : 1;
  unsigned remote   : 1;
  unsigned listener : 1;
  unsigned rbox     : 1;
  unsigned wbox     : 1;
  unsigned pivot    : 1;
  unsigned connect  : 1;
};

struct vnode_assembly_stuff_s
{
  puk_instance_t instance; /**< the component instance the vnode is status (i.e. instance->_status == vnode) */
  puk_context_t context;   /**< the vnode's context in the assembly (may be NULL) */
  void*specific;           /**< component-specific state */
  const struct padico_vlink_driver_s*vlink_driver;
};

/** virtual node descriptor, common to any file type
 */
struct padico_vnode_s
{
  marcel_mutex_t*lock_ptr; /**< lock from the instance */
#ifdef DEBUG
  volatile int lock_count;
  volatile marcel_t lock_owner;
#endif
  void*cookie;          /**< data used by the user of this vnode */
  struct vnode_assembly_stuff_s assembly_stuff;
  /* ** content fields */
  struct padico_vnode_content_t content;
  volatile struct vobs_attr_status_s   status;
  struct vobs_attr_rbox_s     rbox;      /* generic I/O   */
  struct vobs_attr_wbox_s     wbox;      /* generic I/O   */
  struct vobs_attr_observer_s observer;  /* generic I/O   */
  struct vobs_attr_pivot_s    pivot;     /* generic I/O   */
  struct vobs_attr_socket_s   socket;    /* generic vsock */
  struct vobs_attr_remote_s   remote;    /* generic vsock */
  struct vobs_attr_binding_s  binding;   /* generic vsock */
  struct vobs_attr_listener_s listener;  /* generic vsock */
  struct vnode_hfd_s          hfd;       /* hfd toolbox   */
};

/* *** Locking ********************************************* */

#include <execinfo.h>

static inline void vnode_lock(padico_vnode_t vnode)
{
  marcel_mutex_lock(vnode->lock_ptr);
#ifdef DEBUG
  assert(vnode->lock_count == 0);
  vnode->lock_count++;
  vnode->lock_owner = marcel_self();
#endif
}
static inline void vnode_unlock(padico_vnode_t vnode)
{
#ifdef DEBUG
  assert(vnode->lock_owner == marcel_self());
  vnode->lock_owner = (marcel_t)NULL;
  vnode->lock_count--;
  assert(vnode->lock_count == 0);
#endif
  marcel_mutex_unlock(vnode->lock_ptr);
}
static inline int vnode_trylock(padico_vnode_t vnode)
{
  const int success = (marcel_mutex_trylock(vnode->lock_ptr));
#ifdef DEBUG
  if(success)
    {
      assert(vnode->lock_count == 0);
      vnode->lock_count++;
      vnode->lock_owner = marcel_self();
    }
#endif
  return success;
}
static inline void vnode_condwait(padico_vnode_t vnode, marcel_cond_t*cond)
{
#ifdef DEBUG
  vnode->lock_count--;
  vnode->lock_owner = (marcel_t)NULL;
#endif
  marcel_cond_wait(cond, vnode->lock_ptr);
#ifdef DEBUG
  vnode->lock_owner = marcel_self();
  vnode->lock_count++;
#endif
}

/* *** vobs secondary ************************************** */

void vobs_secondary_init(vobs_secondary_t obs, int size);
int  vobs_secondary_update(vobs_secondary_t obs);
void vobs_secondary_wait(vobs_secondary_t obs);
int  vobs_secondary_timedwait(vobs_secondary_t obs, const struct timespec *timeout);
void vobs_secondary_destroy(struct vobs_secondary_s*obs);

static inline void vobs_secondary_lock(vobs_secondary_t obs)
{
  marcel_mutex_lock(&obs->lock);
}
static inline void vobs_secondary_unlock(vobs_secondary_t obs)
{
  marcel_mutex_unlock(&obs->lock);
}

/* *** vobs primary **************************************** */

/** signal an event to the observer.
 * We assume we hold the lock on vnode, but the notifier runs with
 * mutex unlocked to prevent deadlocks when multiple locks are involved.
 */
static inline void vobs_signal(padico_vnode_t vnode, vobs_attr_t attr)
{
#ifdef DEBUG
  assert(vnode->lock_count > 0);
  assert(vnode->lock_owner == marcel_self());
#endif

  /* notifiers (from primary observer) */
  if(vnode->observer.notifier.func != NULL)
    {
      /* since the invocation runs unlocked, we cannot invoke directly
       * vnode->observer.notifier.
       * Take a copy of the notifier and invoke the copy.
       */
      struct vobs_notify_s notify = vnode->observer.notifier;
      vnode_unlock(vnode);
      (*notify.func)(vnode, attr, notify.key);
      vnode_lock(vnode);
    }
  /* secondary observers */
  vobs_secondary_ref_vect_itor_t s;
  puk_vect_foreach(s, vobs_secondary_ref, &vnode->observer.secondary)
    {
      assert(s->obs != NULL);
      marcel_mutex_lock(&s->obs->lock);
      marcel_cond_signal(&s->obs->waiting);
      marcel_mutex_unlock(&s->obs->lock);
    }
  /* primary observer */
  marcel_cond_broadcast(&vnode->observer.wait_event);
}
static inline void vobs_wait(padico_vnode_t vnode)
{
#ifdef DEBUG
  assert(vnode->lock_count > 0);
  assert(vnode->lock_owner == marcel_self());
#endif
  vnode_condwait(vnode, &vnode->observer.wait_event);
}

static inline void vobs_subscribe(padico_vnode_t vnode, vobs_notifier_func_t callback, void*key)
{
  assert(vnode->observer.notifier.func == NULL);
  vnode->observer.notifier.func = callback;
  vnode->observer.notifier.key = key ;
}

static inline void vobs_unsubscribe(padico_vnode_t vnode)
{
  vnode->observer.notifier.func = NULL;
}

static inline void vobs_set_commit(padico_vnode_t vnode, vobs_commit_func_t commit)
{
  assert(vnode->observer.commit == NULL);
  vnode->observer.commit = commit;
}

static inline void vobs_commit(padico_vnode_t vnode, vobs_attr_t attr)
{
  if(vnode->observer.commit != NULL)
    {
      (*vnode->observer.commit)(vnode, attr);
    }
}

/* *** Generic vnode API *********************************** */

ssize_t     padico_vnode_read_post  (padico_vnode_t, void*, size_t);
void        padico_vnode_read_clear (padico_vnode_t);
ssize_t     padico_vnode_write_post (padico_vnode_t, const void*, size_t);
void        padico_vnode_write_clear(padico_vnode_t);
int         padico_vnode_close      (padico_vnode_t);

padico_rc_t padico_vio_dumpvnode    (padico_vnode_t);

/* *** VSock API ******************************************* */

void    padico_vsock_init(padico_vnode_t vnode, int domain, int type, int protocol);
int     padico_vsock_create(padico_vnode_t vnode, int domain, int type, int protocol);
int     padico_vsock_bind(padico_vnode_t, const struct sockaddr*, socklen_t);
int     padico_vsock_autobind(padico_vnode_t);
int     padico_vsock_passive_connect(padico_vnode_t, int, const struct padico_vaddr_s*);
int     padico_vsock_active_connect(padico_vnode_t, const struct sockaddr*, socklen_t);
ssize_t padico_vsock_sendto(padico_vnode_t vnode, const void*buf, size_t len,
                            int flags, const struct sockaddr *to, int  tolen);
ssize_t padico_vsock_recvfrom(padico_vnode_t vnode, void *buf, size_t len,
                              int flags, struct sockaddr *from, int *fromlen);
void    padico_vsock_fullshutdown(padico_vnode_t vnode);

/* *** Error management ************************************ */

static inline int vlink_is_transient_error(int err)
{
  return ( (err) == EINPROGRESS ||
           (err) == EAGAIN ||
           (err) == EINTR ||
           (err) == EWOULDBLOCK );
}
static inline void vnode_set_error(padico_vnode_t vnode, int err)
{
  vnode->status.error = err;
}
static inline int vnode_get_error(padico_vnode_t vnode)
{
  return vnode->status.error;
}
static inline void vnode_clear_error(padico_vnode_t vnode)
{
  vnode_set_error(vnode, 0);
}
static inline int vnode_is_fatal_error(padico_vnode_t vnode)
{
  int err = vnode_get_error(vnode);
  return (err != 0 && !(vlink_is_transient_error(err)));
}
static inline int vnode_is_transient_error(padico_vnode_t vnode)
{
  int err = vnode_get_error(vnode);
  return (err != 0 && (vlink_is_transient_error(err)));
}

#endif /* VLINK_API_H */

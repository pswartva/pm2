srcdir := @srcdir@
VPATH  := $(srcdir)

include @abs_top_builddir@/common_vars.mk

TARGET_BIN =
TARGET_LIB =
SUBDIRS =

INSTALL_BIN     = $(TARGET_BIN)
INSTALL_LIB     = $(TARGET_LIB)
INSTALL_MOD     = $(TARGET_MOD)
INSTALL_INCLUDE =

INSTALL_PREFIX := @nmad_root@

TARGETS = subdirs $(TARGET_BIN) $(TARGET_LIB)

EXAMPLE_DIRS = pack sendrecv rpc benchmarks lowlevel launcher coll sync_clocks onesided bench-coll mcast
ifeq (@with_pioman@,yes)
  EXAMPLE_DIRS += piom coll_dynamic
  ifeq (@nmad_enable_mpi@,yes)
    EXAMPLE_DIRS += mpi/pioman
  endif
endif
ifeq (@nmad_enable_mpi@,yes)
  EXAMPLE_DIRS += mpi/basics mpi/oldbench mpi/rma mpi/forum
  ifneq (@nmad_fortran_target@,none)
    EXAMPLE_DIRS += mpi/f77 mpi/f90
  endif
endif
ifeq (@HAVE_CXX@,yes)
# some versions of simgrid strugle to include their header through extern "C" in C++
ifneq (@padico_enable_simgrid@@padico_simgrid_cplusplus@,yesno)
  EXAMPLE_DIRS += cxx
endif
endif

SUBDIRS += $(EXAMPLE_DIRS)


include @abs_top_builddir@/common_rules.mk

# ## Tests
#
check: tests


-include $(TESTS_RESULTS)

tests:
	@( 	 								\
          TESTS_RESULTS=`mktemp --suffix=-nmad-tests` ;			\
	  echo "# automatically generated tests results" > $${TESTS_RESULTS} ;  \
	  echo "TESTS_SUCCESS = "                       >> $${TESTS_RESULTS} ;  \
	  echo "TESTS_FAILED = "                        >> $${TESTS_RESULTS} ;  \
	  for d in $(EXAMPLE_DIRS); do 					\
              echo "  [TESTS]  in $$d/"; 					\
              $(MAKE) -C $$d tests TESTS_RESULTS=$${TESTS_RESULTS} ; 		\
          done ; 								\
          $(MAKE) show-tests-results TESTS_RESULTS=$${TESTS_RESULTS} ; 	\
	  /bin/rm $${TESTS_RESULTS} 2> /dev/null 				\
	)


show-tests-results:
	@echo
	@echo "%-------------------"
	@echo
	@echo " Tests series: $(EXAMPLE_DIRS)"
	@echo
	@echo " Tests results:"
	@echo "   success: $(words $(TESTS_SUCCESS))"
	@echo "   failed:  $(words $(TESTS_FAILED))"
	@( if [ "$(words $(TESTS_FAILED))" != 0 ]; then echo "     [ $(TESTS_FAILED) ]"; fi )
	@echo

# ## Benchmarks
#

BENCH_RESULTS := @nmad_builddir@/.bench-results
CLEAN_MORE += $(BENCH_RESULTS)

bench:
	@-/bin/rm $(BENCH_RESULTS) 2> /dev/null
	$(Q)echo "# automatically generated tests results" > $(BENCH_RESULTS)
	$(Q)( for d in $(EXAMPLE_DIRS); do \
                echo "  [BENCH]  in $$d/"; \
                $(MAKE) -C $$d bench BENCH_RESULTS=$(BENCH_RESULTS) ; \
              done )
	@echo
	@echo "%-------------------"
	@echo
	@echo " Bench summary"
	@echo
	@grep "latency:" $(BENCH_RESULTS)
	@echo

/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Internal definitions for use inside Puk only.
 * @ingroup Puk
 */

/** @defgroup Puk Core module: Puk -- PadicoTM micro-kernel
 * @author Alexandre Denis
 * Puk is the core of PadicoTM. It manages dynamically loadable
 * modules, software components, and basic data structures (lists,
 * vectors, hashtables, lock-free queues). It may be used with
 * PadicoTM or standalone.
 */


#ifndef PUK_INTERNALS_H
#define PUK_INTERNALS_H

/* include autoconf'ed configuration */
#include "puk_config.h"

#include "Puk.h"

/* we need socklen_t */
#ifndef HAVE_SOCKLEN_T
typedef int socklen_t;
#endif


#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>
#include <fcntl.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <stdarg.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>

__PUK_SYM_INTERNAL void padico_puk_debug_init(void);
__PUK_SYM_INTERNAL void padico_puk_builtin_init(void);
__PUK_SYM_INTERNAL void padico_puk_pkg_init(void);
__PUK_SYM_INTERNAL void padico_puk_monitor_init(void);
__PUK_SYM_INTERNAL void padico_puk_mod_init(void);
__PUK_SYM_INTERNAL void padico_puk_xml_init (void);
__PUK_SYM_INTERNAL void padico_puk_components_init(void);
__PUK_SYM_INTERNAL void padico_puk_binary_init(void);
__PUK_SYM_INTERNAL void padico_puk_opt_init(void);

__PUK_SYM_INTERNAL void padico_puk_debug_finalize(void);
__PUK_SYM_INTERNAL void padico_puk_xml_finalize(void);
__PUK_SYM_INTERNAL void padico_puk_components_finalize(void);
__PUK_SYM_INTERNAL void padico_puk_builtin_finalize();
__PUK_SYM_INTERNAL void padico_puk_mod_finalize(void);
__PUK_SYM_INTERNAL void padico_puk_binary_finalize(void);

__PUK_SYM_INTERNAL padico_rc_t  puk_mod_open(puk_mod_t*mod, padico_string_t filename);
__PUK_SYM_INTERNAL puk_mod_t    puk_mod_new(const char*name, const char*driver_name);
__PUK_SYM_INTERNAL void         puk_mod_destroy(puk_mod_t m);
__PUK_SYM_INTERNAL void         puk_mod_builtin_tryload(puk_mod_t*mod, const char*mod_name);

/* ** path */

PUK_VECT_TYPE(puk_path, const char*);
__PUK_SYM_INTERNAL puk_path_vect_t puk_path_get(void);

/** Returns the value of PADICO_ROOT */
__PUK_SYM_INTERNAL const char*padico_root(void);

/* lock Puk internal structures (xml/mod/components) */
__PUK_SYM_INTERNAL void __puk_lock_acquire(const char*func);
__PUK_SYM_INTERNAL void __puk_lock_release(const char*func);
#define puk_lock_acquire() __puk_lock_acquire(__FUNCTION__)
#define puk_lock_release() __puk_lock_release(__FUNCTION__)

/* ** units */
PUK_VECT_TYPE(puk_unit, struct puk_unit_s);

/* ** mod_refs */
PUK_VECT_TYPE(puk_mod, struct puk_mod_s*);

/* ** module */

/** an attribute in a module.
 */
struct puk_mod_attr_s
{
  char*label;           /**< name of the attribute */
  char*value;           /**< content as a string; if opt is non-NULL, content is in opt and this field may contains a cached stringified value */
  struct puk_opt_s*opt; /**< attribute descriptor, if any. May be NULL. */
};
PUK_VECT_TYPE(puk_mod_attr, struct puk_mod_attr_s);

const char*puk_mod_attr_get_value(struct puk_mod_attr_s*attr);

struct puk_mod_s
{
  char*                     mod_name;    /**< name of the module */
  const char*               mod_path;    /**< path of the module on disk (or NULL) */
  puk_component_t           driver;      /**< loader used to manage this module */
  struct puk_unit_vect_s    units;       /**< units- content of the module */
  puk_mod_attr_vect_t       attrs;       /**< attributes of the module */
  puk_mod_t                 inherits;    /**< ancestor for attributes inheriting */
  struct puk_mod_vect_s     requires;    /**< modules used by this module */
  struct puk_mod_vect_s     rdeps;       /**< reverse dependancies, i.e. modules using this one */
  struct padico_modID_vect_s unresolved_deps; /**< unresolved requirements */
  int                       used_by;     /**< number of modules using this one */
  int                       ref_count;   /**< number of references (explicit 'load') */
  int                       managed_instances; /**< number of components instances whose home is this module */
  int                       managed_components; /**< number of components whose home is this module */
  int                       own_instances; /**< number of components that have this module as owner and home */
};

#endif /* PUK_INTERNALS_H */

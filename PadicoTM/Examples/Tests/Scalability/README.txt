This document describes the principle of thes tests and how to use them.

Sources
=======

Sources for FIFO test:
	- Puk-lfqueue.h : Contains the definition of FIFO lock-free
	- Puk-lqueuemutex.h : Contains the definition of FIFO with mutex
	- Puk-lqueuespinlock.h : Contains the definition of FIFO with spinlock
	- testFIFO.sh : Script to launch FIFO test on a specific mode (0: 1 producer-N consumer, 1: N producer-1 consumer, 2: N producer-N consumer) and generate data files and charts. It needs executable generated by compilTestFifo.sh
	- testFIFO_lqueuespinlock.c : Source of the test on FIFO with spinlock. It needs : Puk-lqueuespinlock.h
 	- testFIFO_lqueuemutex.c : Source of the test on FIFO with mutex. It needs : Puk-lqueuemutex.h
	- testFIFO_lfqueue.c : Source of the test on FIFO lock-free. It needs : Puk-lfqueue.h
	- full-testFIFO.sh : General launcher to test FIFOs (compilation of sources, launching of tests and creation of data files and charts). It needs : testFIFO.sh and compilTestFifo.sh (and the files needed by those scripts) 
	- compilTestFifo.sh : Compilation launcher. It needs :	testFIFO_lqueuespinlock.c, testFIFO_lqueuemutex.c and testFIFO_lfqueue.c
 
Sources for driver SHM test:
	- cachename.c : Source for the programme giving the name of a specific cache level defined by the mode given in input (Usefull to create more readable charts).
        - helper.h : Source used to generate executables testdrivershmflow and testdrivershmlatency.
	- form_flow.c : Source for a programme needing a data file in input and dispalying datas with a right format to create flow chart in output.
	- form_latency.c : Source for a programme needing a data file in input and dispalying datas with a right format to create latency chart in output.
	- testdrivershmflow.c : Source for the test of the flow for the SHM driver.
	- testdrivershmlatency.c : Source for the test of the latency for the SHM driver.
	- testcpubinding.c : Source for the programme giving the number of cache level useful for the hierarchy of the of the machine.
	- testcpubinding2.c : Source for the programme displaying a part of a command line to bind processus on specific CPU according to a mode given in input.
	- testflow.sh : Script launching flow test and creating data file and charts.
	- testlatency.sh : Script launching latency test and creating data file and charts.


Configurations for FIFO test
============================

full-testFIFO.sh needs to be launch with command line:
./full-testFIFO.sh <machine name>

compilTestFifo.sh needs to be launch with command line:
./compilTestFIFO.sh 

testFIFO.sh needs to be launch with command line:
./testFIFO.sh <mode> <machine name>
For mode refers to description of the source testFIFO.sh above.

After compiling sources 3 executables will be generated:

      - testFIFOlqueuespinlock : launch a test of the FIFO with spinlock on a specific mode and display the results on the standard output
      - testFIFOlqueuemutex : launch a test of the FIFO with mutex on a specific mode and display the results on the standard output
      - testFIFOlfqueue : launch a test of the FIFO lock-free on a specific mode and display the results on the standard output

testFIFOlqueuespinlock needs to be launch with command line:
./testFIFOlqueuespinlock <mode>

testFIFOlqueuemutex needs to be launch with command line:
./testFIFOlqueuemutex <mode>

testFIFOlfqueue needs to be launch with command line:
./testFIFOlfqueue <mode>

Configurations for driver SHM test
==================================



testflow.sh needs to be launch with command line:
./testflow.sh <machine name>

Machine name is optional.

testlatency.sh needs to be launch with command line:
./testlatency.sh <machine name>

Machine name is optional. 

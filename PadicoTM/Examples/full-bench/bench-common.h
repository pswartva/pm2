/* bench-common.h
 */

#include <Padico/Topology.h>
#include <Padico/PadicoControl.h>

#define DEFAULT_MAX_LEN    8*1024*1024

struct bench_desc_s
{
  char*name;
  void (*server)(char*buffer, size_t size);
  void (*client)(char*buffer, size_t size);
};

extern void the_bench(const struct bench_desc_s*b);

struct bench_common_param_s
{
  int is_server;
  padico_topo_node_t client_node;
  padico_topo_node_t server_node;
};

extern struct bench_common_param_s bench_common_param;


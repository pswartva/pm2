/** @file bench-NewMad.c
 */

#include "bench-common.h"
#include <Padico/Module.h>
#include <Padico/Puk.h>
#include <nm_public.h>
#include <nm_sendrecv_interface.h>
#include <nm_session_interface.h>
#include <nm_launcher_interface.h>
#include <nm_launcher.h>

int bench_newmad_init(void);
int bench_newmad_run(int argc, char**argv);

PADICO_MODULE_DECLARE(bench_NewMad, bench_newmad_init, bench_newmad_run, NULL);

static nm_gate_t peer_gate = NULL;
static nm_session_t p_session = NULL;

int bench_newmad_init(void)
{
  int fake_argc = 1;
  char*fake_argv[] = { "bench-NewMad", NULL };
  puk_component_t launcher = puk_component_resolve("NewMad_Launcher_madico");
  puk_instance_t instance = puk_component_instantiate(launcher);
  struct puk_receptacle_NewMad_Launcher_s r;
  puk_instance_indirect_NewMad_Launcher(instance, NULL, &r);
  (*r.driver->init)(r._status, &fake_argc, fake_argv, "bench-common");
  int rank = (*r.driver->get_rank)(r._status);
  int size = (*r.driver->get_size)(r._status);
  nm_session_create(&p_session, "bench-NewMad");
  nm_gate_t*gates = padico_malloc(size * sizeof(nm_gate_t));
  (*r.driver->get_gates)(r._status, gates);
  int peer = (rank % 2 == 0) ? (rank + 1) : (rank - 1);
  assert(peer >= 0 && peer < size);
  peer_gate = gates[peer];

  padico_print("bench-NewMad: size = %d; rank = %d; peer_gate = %p\n", size, rank, peer_gate);

  return 0;
}

void newmad_server(char*buf, size_t len)
{
  nm_sr_request_t request;

  nm_sr_isend(p_session, peer_gate, 0, buf, len, &request);
  nm_sr_swait(p_session, &request);

  nm_sr_irecv(p_session, peer_gate, 0, buf, len, &request);
  nm_sr_rwait(p_session, &request);
}

void newmad_client(char*buf, size_t len)
{
  nm_sr_request_t request;

  nm_sr_irecv(p_session, peer_gate, 0, buf, len, &request);
  nm_sr_rwait(p_session, &request);

  nm_sr_isend(p_session, peer_gate, 0, buf, len, &request);
  nm_sr_swait(p_session, &request);
}

static const struct bench_desc_s newmad_bench =
{
  "NewMad",
  &newmad_server,
  &newmad_client
};

int bench_newmad_run(int argc, char**argv)
{
  the_bench(&newmad_bench);
  return 0;
}

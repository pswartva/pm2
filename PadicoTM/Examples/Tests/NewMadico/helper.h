#include <tbx.h>

#include <nm_public.h>
#include <nm_so_public.h>

#include <nm_so_sendrecv_interface.h>
#include <nm_so_pack_interface.h>

#include <nm_drivers.h>

#include <nm_drv.h>

#include <Padico/Puk.h>
#include <Padico/NetAccess.h>
#include <Padico/Module.h>
#include <Padico/NetSelector.h>

PADICO_MODULE_DECLARE(sr_bench, NULL, NULL, NULL);


static struct nm_core           *p_core                 = NULL;
static struct nm_so_interface   *sr_if  = NULL;
static nm_so_pack_interface     pack_if;

static char     *r_url  = NULL;
static char*l_url       = NULL;
static uint8_t   drv_id                 =    0;
static gate_id_t gate_id        =    0;
static int       is_server;
static int client_is_connected = 0;
static marcel_mutex_t lock;
static marcel_cond_t cond;
static char*server_url = NULL;


void waiting_connection(){
  marcel_mutex_lock(&lock);
  while(!client_is_connected)
    {
      marcel_cond_wait(&cond, &lock);
    }
  marcel_mutex_unlock(&lock);
}

void waiting_url(){
  marcel_mutex_lock(&lock);
  while(!server_url)
    {
      marcel_cond_wait(&cond, &lock);
    }
  marcel_mutex_unlock(&lock);
}
static void sr_bench_url_handler(puk_parse_entity_t e)
{
  const char*s_url = puk_parse_getattr(e, "url");

  marcel_mutex_lock(&lock);
  server_url = padico_malloc(strlen(s_url)+1);
  strcpy(server_url, s_url);
  marcel_cond_signal(&cond);
  marcel_mutex_unlock(&lock);
}

static struct puk_tag_action_s sr_bench_url_action =
  {
    .xml_tag        = "sr_bench:Url",
    .start_handler  = &sr_bench_url_handler,
    .end_handler    = NULL,
    .required_level = PUK_TRUST_CONTROL
  };

/*
 * Returns process rank
 */
int get_rank() {
  fprintf(stderr, "not implemented\n");
  exit(EXIT_FAILURE);
}

/*
 * Returns the number of nodes
 */
int get_size() {
  fprintf(stderr, "not implemented\n");
  exit(EXIT_FAILURE);
}

/*
 * Returns the gate id of the process dest
 */
int get_gate_in_id(int dest) {
  fprintf(stderr, "not implemented\n");
  exit(EXIT_FAILURE);
}

/*
 * Returns the gate id of the process dest
 */
int get_gate_out_id(int dest) {
  fprintf(stderr, "not implemented\n");
  exit(EXIT_FAILURE);
}


/* initialize everything
 *
 * returns 1 if server, 0 if client
 */
void
sr_bench_init(int        *argc,
     char       **argv) {
        int err;

        marcel_mutex_init(&lock, NULL);
        marcel_cond_init(&cond, NULL);

        puk_xml_add_action(sr_bench_url_action);

        err = nm_core_init(argc, argv, &p_core, nm_so_load);
        if (err != NM_ESUCCESS) {
                printf("nm_core_init returned err = %d\n", err);
                goto out_err;
        }

        err = nm_so_sr_init(p_core, &sr_if);
        if(err != NM_ESUCCESS) {
          printf("nm_so_sr_init return err = %d\n", err);
          goto out_err;
        }

        err = nm_so_pack_interface_init(p_core, &pack_if);
        if(err != NM_ESUCCESS) {
          printf("nm_so_pack_interface_init return err = %d\n", err);
          goto out_err;
        }

        /* we have to decide who is server */
        assert(!r_url);
        padico_group_t group = padico_group_lookup("sr_bench");
        assert(group != NULL);
        int rank = padico_group_rank(group, padico_topo_getlocalnode());
        assert(padico_group_size(group) == 2);
        is_server = (rank == 0);

        const padico_topo_node_t peer = padico_group_node(group, 1 - rank);
        const puk_component_t component = padico_ns_serial_selector(peer, NULL, puk_iface_NewMad_Driver());
        padico_string_t s_component = puk_component_serialize(component);
        err = nm_core_driver_load_init(p_core, padico_string_get(s_component), &drv_id, &l_url);
        padico_string_delete(s_component);
        if (err != NM_ESUCCESS) {
                printf("nm_core_driver_load_init returned err = %d\n", err);
                goto out_err;
        }

        err = nm_core_gate_init(p_core, &gate_id);
        if (err != NM_ESUCCESS) {
                printf("nm_core_gate_init returned err = %d\n", err);
                goto out_err;
        }

        if (is_server) {
          printf("running as server (url:%s)\n",l_url);
          /* Send url */
          padico_string_t s_url = padico_string_new();
          padico_string_catf(s_url, "<sr_bench:Url url=\"%s\"/>\n", l_url);
          padico_control_send(padico_group_node(group, 1), padico_string_get(s_url));

          err = nm_core_gate_accept(p_core, gate_id, drv_id, NULL);
          if (err != NM_ESUCCESS) {
            printf("nm_core_gate_accept returned err = %d\n", err);
            goto out_err;
          }
        } else {
          printf("running as client (waiting for remote url)\n");
          waiting_url();
          int err = nm_core_gate_connect(p_core, gate_id, drv_id, (char*)server_url);
          if (err != NM_ESUCCESS) {
            padico_warning("nm_core_gate_connect returned err = %d\n", err);
          }
          marcel_mutex_lock(&lock);
          client_is_connected = 1;
          marcel_cond_signal(&cond);
          marcel_mutex_unlock(&lock);
        }

        return;

 out_err:
        exit(EXIT_FAILURE);
}

/* clean session
 *
 * returns NM_ESUCCESS or EXIT_FAILURE
 */
int nmad_exit() {
  int err, ret = NM_ESUCCESS;

  err = nm_core_driver_exit(p_core);
  if(err != NM_ESUCCESS) {
    printf("nm_core_driver_exit return err = %d\n", err);
    ret = EXIT_FAILURE;
  }
  err = nm_core_exit(p_core);
  if(err != NM_ESUCCESS) {
    printf("nm_core__exit return err = %d\n", err);
    ret = EXIT_FAILURE;
  }
  err = nm_so_sr_exit(sr_if);
  if(err != NM_ESUCCESS) {
    printf("nm_so_sr_exit return err = %d\n", err);
    ret = EXIT_FAILURE;
  }
  return ret;
}

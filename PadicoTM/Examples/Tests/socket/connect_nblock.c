#include <stdio.h>
#include <strings.h>
#include <string.h>
#include <netinet/in.h>
#include <netdb.h>
#include <time.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
#include <errno.h>
#include "helper.h"

#define MSGLEN 512

int main(int argc, char *argv[])
{
  usage(argc, argv);
  int port = atoi(argv[3]);
  int sock_fd = create_socket();
  char msg[MSGLEN];
  memset(msg, 0, MSGLEN);

  if(strncmp("server",argv[1],6)==0)
    {
      bind_and_listen(sock_fd, port);
      struct sockaddr_in cli_addr;
      int clilen = sizeof(cli_addr);
      int clifd = accept(sock_fd,  (struct sockaddr *) &cli_addr,&clilen);
      if (clifd < 0)
        print_error("accept()");
      read(clifd, msg, MSGLEN);
      printf("Server: I receive \"%s\"\n", msg);
   }
  else if(strncmp("client",argv[1],6)==0)
    {

      struct hostent *info_serv;
      info_serv=gethostbyname(argv[2]);
      struct sockaddr_in serv_addr;
      bzero(&serv_addr,sizeof(serv_addr));
      serv_addr.sin_family = AF_INET;
      serv_addr.sin_port = htons(port);
      memcpy(&serv_addr.sin_addr, info_serv->h_addr_list[0], info_serv->h_length);

      int flags, rc;
      fd_set rset, wset;
      flags = fcntl(sock_fd, F_GETFL, 0);
      if(flags < 0)
        print_error("fcntl() get options:");
      if(fcntl(sock_fd, F_SETFL, flags|O_NONBLOCK))
        print_error("fcntl() set options:");
      printf("Client: Non-Blocking Connect\n");
      if( (rc = connect(sock_fd, (struct sockaddr *) &serv_addr,sizeof(serv_addr))) < 0)
        {
          if(errno != EINPROGRESS)
            print_error("connect()");
        }
      if(rc!=0)
        {
          FD_ZERO(&rset);
          FD_SET(sock_fd, &rset);
          wset = rset;
          if ((rc = select(sock_fd+1, &rset, &wset, NULL, NULL)) < 0)
            print_error("select()");
          strcpy(msg, "Hello Server\0");
          if (write(sock_fd, msg, strlen(msg)) < 0)
            print_error("write()");
          printf("Client: I send \"%s\"\n", msg);
        }

    }
  close2(sock_fd);

  return 0;
}

/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Best-effort selector for profile 'inet'.
 */

#include <Padico/NetSelector.h>
#include <Padico/Module.h>
#include <Padico/Puk.h>
#include <Padico/VLink-API.h>
#include <Padico/SocketFactory.h>
#include <Padico/PadicoControl.h>

static int ns_inet_default_init(void);
static void ns_inet_default_finalize(void);

PADICO_MODULE_DECLARE(NetSelector_inet_default, ns_inet_default_init, NULL, ns_inet_default_finalize,
                      "Topology", "NetSelector");

/* ********************************************************* */

static puk_component_t ns_inet_default_selector_host(padico_topo_host_t host, const char*_profile, puk_iface_t iface, uint16_t port, int protocol);

static const char assembly_vlink_sfinet[] =
  "<puk:composite id=\"nsinet:vlink_sfinet\">"
  "  <puk:component id=\"0\" name=\"SocketFactory_Plain\"/>"
  "  <puk:component id=\"1\" name=\"VLink_SocketFactory\">"
  "    <puk:uses iface=\"SocketFactory\" provider-id=\"0\" />"
  "  </puk:component>"
  "  <puk:entry-point iface=\"VLink\" provider-id=\"1\"/>"
  "</puk:composite>";

static const char assembly_sf_plain[] =
  "<puk:composite id=\"nsinet:sf_plain\">"
  "  <puk:component id=\"0\" name=\"SocketFactory_Plain\"/>"
  "  <puk:entry-point iface=\"SocketFactory\" provider-id=\"0\"/>"
  "</puk:composite>";

static const char assembly_control_sysio[] =
"<puk:composite id=\"nsinet:control_sysio\">"
"  <puk:component id=\"0\" name=\"SocketFactory_Plain\"/>"
"  <puk:component id=\"1\" name=\"Control_SysIO\">"
"    <puk:uses iface=\"SocketFactory\" provider-id=\"0\" />"
"  </puk:component>"
"  <puk:entry-point iface=\"PadicoControl\"   provider-id=\"1\" />"
"  <puk:entry-point iface=\"PadicoBootstrap\" provider-id=\"1\" />"
"  <puk:entry-point iface=\"PadicoConnector\" provider-id=\"1\" />"
"</puk:composite>";

static puk_component_t inet_vlink_assembly = NULL;
static puk_component_t inet_sf_assembly = NULL;
static puk_component_t inet_control_assembly = NULL;

static const struct padico_netselector_driver_s ns_inet_default_selector =
  {
    .name    = "NetSelector-inet-default",
    .profile = "inet",
    .serial  = NULL,
    .host    = &ns_inet_default_selector_host,
  };

/* ********************************************************* */

static int ns_inet_default_init(void)
{
  /* inet default selector */
  padico_ns_selector_register(&ns_inet_default_selector);
  return 0;
}

static void ns_inet_default_finalize(void)
{
  padico_ns_selector_unregister(&ns_inet_default_selector);
}

/* ********************************************************* */
/* *** inet default selector ******************************* */


static puk_component_t ns_inet_default_selector_host(padico_topo_host_t host, const char*_profile,
                                                     puk_iface_t iface, uint16_t port, int protocol)
{
  if(iface == puk_iface_VLink())
    {
      if(!inet_vlink_assembly)
        {
          inet_vlink_assembly = puk_component_parse(assembly_vlink_sfinet);
          if(inet_vlink_assembly == NULL)
            padico_fatal("failed to parse assembly %s.\n", assembly_vlink_sfinet);
        }
      return inet_vlink_assembly;
    }
  else if(iface == puk_iface_SocketFactory())
    {
      if(!inet_sf_assembly)
        {
          inet_sf_assembly = puk_component_parse(assembly_sf_plain);
          if(inet_sf_assembly == NULL)
            padico_fatal("failed to parse assembly %s.\n", assembly_sf_plain);
        }
      return inet_sf_assembly;
    }
  else if(iface == puk_iface_PadicoBootstrap())
    {
      if(!inet_control_assembly)
        {
          inet_control_assembly = puk_component_parse(assembly_control_sysio);
          if(inet_control_assembly == NULL)
            padico_fatal("failed to parse assembly %s.\n", assembly_control_sysio);
        }
      return inet_control_assembly;
    }
  else if(iface == puk_iface_PadicoConnector())
    {
      if(!inet_control_assembly)
        {
          inet_control_assembly = puk_component_parse(assembly_control_sysio);
          if(inet_control_assembly == NULL)
            padico_fatal("failed to parse assembly %s.\n", assembly_control_sysio);
        }
      return inet_control_assembly;
    }
  else
    return NULL;
}

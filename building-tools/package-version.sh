#! /bin/sh

# $1: building-tools directory to consider
# $2: srcdir
p="$1"
srcdir="$2"

if [ -r "${srcdir}/VERSION" ]; then
    VERSION="`cat ${srcdir}/VERSION`"
elif [ -r "${p}/VERSION" ]; then
    VERSION="`cat ${p}/VERSION`"
else
    exit 1
fi

if [ "x${srcdir}" != "x" ]; then
    cd "${srcdir}"
    git rev-parse HEAD > /dev/null 2>&1
    rc=$?
    if [ ${rc} = 0 ]; then
	rev="+`git rev-parse HEAD`"
    fi
fi

# strip newline for m4 parsing
echo "${VERSION}${rev}" | tr '\n' ' '

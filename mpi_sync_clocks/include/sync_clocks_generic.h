/*
 * MPI sync clocks
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

#ifndef SYNC_CLOCKS_GENERIC_H
#define SYNC_CLOCKS_GENERIC_H

/*
 * Some functions to deal with clock synchronization on many nodes.
 *
 * Clock offsets are computed before and after the timed stuff. Then,
 * offsets can be applied to measured times.
 * Only the node with rank 0 knows the offsets for all nodes.
 *
 *
 * Usage:
 *
 * 1. Call nm_sync_clocks_init() (for nmad backend)
 *    or mpi_sync_clocks_init() (for mpi backend)
 *
 * 2. Measure time with nm_sync_clocks_get_time_usec()
 *
 * 3. Call nm_sync_clocks_synchronize()
 *
 * 4. Get synchronized times by calling nm_sync_clocks_remote_to_global()
 * (time for any rank computed from rank #0)
 * or nm_sync_clocks_local_to_global() to translat local time to global time.
 *
 * 5. Free memory with nm_sync_clocks_shutdown()
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>

#include "sync_clocks_clocks.h"

/* ********************************************************* */
/* ** generic implementation */

#define SYNC_MAX_ROUNDTRIPS 2000
#define SYNC_CONVERGE_THRESHOLD 100
#define SYNC_MARGIN_RATIO 1.05 /* 5% */
/** minimum time before starting a barrier, in usec.
 * sync global barrier time is _extrapolated_, we need to wait enough to get good precision.
 */
#define SYNC_BARRIER_MIN 1000.0

/** an opaque sync-clocks object */
typedef struct sync_clocks_generic_s*sync_clocks_generic_t;

struct sync_clocks_backend_s
{
  void(*send)(sync_clocks_generic_t, int, int, void*, int);
  void(*recv)(sync_clocks_generic_t, int, int, void*, int);
  void(*bcast)(sync_clocks_generic_t, int, int, void*, int);
  void(*gather)(sync_clocks_generic_t, int, int, void*, int, void*);
  void(*barrier)(sync_clocks_generic_t);
};


/** a synchronization point, with the same event in slave time and master time */
struct sync_clocks_generic_offset_s
{
  double t_slave;  /**< the event in slave time */
  double t_master; /**< the same event in master time */
};

struct sync_clocks_generic_offset_pair_s
{
  struct sync_clocks_generic_offset_s begin, end;
};
/** an instance of synchronized clocks */
struct sync_clocks_generic_s
{
  double*latencies;
  sync_clocks_generic_tick_t time_orig; /**< origin of time */
  struct sync_clocks_backend_s backend;
  struct
  {
    void*_comm;
  } backend_data;
  int comm_size;
  int rank;
  struct sync_clocks_generic_offset_pair_s*p_offsets; /**< array of offsets so that node #0 can compute global clock for all nodes*/
  struct sync_clocks_generic_offset_pair_s local_offset; /**< offset for local node */
};

static void __sync_clocks_generic_synchronize(sync_clocks_generic_t p_clocks, unsigned is_end);

/* ********************************************************* */

static inline void sync_clocks_generic_init(sync_clocks_generic_t p_clocks)
{
#if defined(SYNC_CLOCKS_USE_PUK)
  static int init_done = 0;
  if(!init_done)
    {
      fprintf(stderr, "# sync_clocks: using Puk timing method '%s'.\n", PUK_TIMING_METHOD);
      init_done = 1;
    }
#endif
  if(p_clocks->comm_size < 0)
    {
      fprintf(stderr, "# sync_clocks: wrong size %d for communicator.\n", p_clocks->comm_size);
      abort();
    }
  p_clocks->latencies = malloc(sizeof(double) * p_clocks->comm_size);
  int i;
  for(i = 0; i < p_clocks->comm_size; i++)
    {
      p_clocks->latencies[i] = -1.0;
    }
  sync_clocks_generic_get_tick(p_clocks->time_orig);
  if(p_clocks->rank == 0)
    {
      p_clocks->p_offsets = calloc(p_clocks->comm_size, sizeof(struct sync_clocks_generic_offset_pair_s));
    }
  else
    {
      p_clocks->p_offsets = NULL;
    }
  __sync_clocks_generic_synchronize(p_clocks, 0);
}

static inline void sync_clocks_generic_shutdown(sync_clocks_generic_t p_clocks)
{
  if(p_clocks->p_offsets)
    {
      free(p_clocks->p_offsets);
    }
  free(p_clocks->latencies);
  free(p_clocks);
}

static inline double sync_clocks_generic_get_time_usec(sync_clocks_generic_t p_clocks)
{
  assert(p_clocks != NULL);
  sync_clocks_generic_tick_t tick;
  sync_clocks_generic_get_tick(tick);
  const double delay = sync_clocks_generic_ticks2delay(&p_clocks->time_orig, &tick);
  return delay;
}

static inline double sync_clocks_generic_remote_to_global(sync_clocks_generic_t p_clocks, int rank, double t)
{
  if(p_clocks->rank != 0)
    {
      fprintf(stderr, "# sync_clocks: cannot compute clocks for other nodes on rank != 0.\n");
      abort();
    }
  if(rank == 0)
    return t;
  const struct sync_clocks_generic_offset_s*p_begin = &p_clocks->p_offsets[rank].begin;
  const struct sync_clocks_generic_offset_s*p_end = &p_clocks->p_offsets[rank].end;
  const double result = ((p_end->t_master - p_begin->t_master) * (t - p_begin->t_slave)) / (p_end->t_slave - p_begin->t_slave) + p_begin->t_master;
  return result;
}

static inline double sync_clocks_generic_local_to_global(sync_clocks_generic_t p_clocks, double local_t)
{
  if(p_clocks->rank == 0)
    return local_t;
  const struct sync_clocks_generic_offset_s*p_begin = &p_clocks->local_offset.begin;
  const struct sync_clocks_generic_offset_s*p_end = &p_clocks->local_offset.end;
  const double result = ((p_end->t_master - p_begin->t_master) * (local_t - p_begin->t_slave)) / (p_end->t_slave - p_begin->t_slave) + p_begin->t_master;
  return result;
}

/* Just inverse function of sync_clocks_generic_local_to_global */
static inline double sync_clocks_generic_global_to_local(sync_clocks_generic_t p_clocks, double global_t)
{
  if(p_clocks->rank == 0)
    return global_t;
  const struct sync_clocks_generic_offset_s*p_begin = &p_clocks->local_offset.begin;
  const struct sync_clocks_generic_offset_s*p_end = &p_clocks->local_offset.end;
  const double result = ((p_end->t_slave - p_begin->t_slave) * (global_t - p_begin->t_master)) / (p_end->t_master - p_begin->t_master) + p_begin->t_slave;
  return result;
}

static inline long double sync_clocks_generic_get_skew(sync_clocks_generic_t p_clocks, int rank)
{
  if(rank == 0)
    return 1.0;
  const struct sync_clocks_generic_offset_s*p_begin = &p_clocks->p_offsets[rank].begin;
  const struct sync_clocks_generic_offset_s*p_end = &p_clocks->p_offsets[rank].end;
  const long double ratio = ((long double)(p_end->t_master - p_begin->t_master)) /
    (long double)(p_end->t_slave - p_begin->t_slave);
  return ratio;
}

static inline void __sync_clocks_generic_synchronize(sync_clocks_generic_t p_clocks, unsigned is_end)
{
  if(p_clocks->rank == 0)
    {
      /* master */
      int i;
      for(i = 1; i < p_clocks->comm_size; i++)
        {
          struct sync_clocks_generic_offset_s*p_offset = is_end ? &p_clocks->p_offsets[i].end : &p_clocks->p_offsets[i].begin;
          const double ref_lat = p_clocks->latencies[i];
          double lat_min = -1.0;
          double t_dummy = 1.0;
          int converge = 0;
          int j;
          for(j = 0; j < SYNC_MAX_ROUNDTRIPS; j++)
            {
              const double t_begin = sync_clocks_generic_get_time_usec(p_clocks);
              double t_client = 0.0;
              (*p_clocks->backend.send)(p_clocks, i, 0x01, &t_dummy, sizeof(t_dummy)); /* send dummy time for symmetry */
              (*p_clocks->backend.recv)(p_clocks, i, 0x02, &t_client, sizeof(t_client));
              const double t_end = sync_clocks_generic_get_time_usec(p_clocks);
              const double latency = (t_end - t_begin) / 2.0;
              if((lat_min < 0) || (latency < lat_min))
                {
                  lat_min = latency;
                  p_offset->t_slave = t_client - latency;
                  p_offset->t_master = t_begin;
                  converge = 0;
                }
              else
                {
                  converge++;
                }
              if(converge > SYNC_CONVERGE_THRESHOLD)
                break;
              if((ref_lat > 0.0) && (lat_min <= (ref_lat * SYNC_MARGIN_RATIO)))
                break;
            }
          t_dummy = -1.0; /* notify loop exit */
          (*p_clocks->backend.send)(p_clocks, i, 0x01, &t_dummy, sizeof(t_dummy));
          (*p_clocks->backend.send)(p_clocks, i, 0x03, p_offset, sizeof(*p_offset));
          if(ref_lat < 0.0)
            {
              p_clocks->latencies[i] = lat_min;
            }
        }
    }
  else
    {
      /* slave */
      struct sync_clocks_generic_offset_s*p_local_offset = is_end ? &p_clocks->local_offset.end : &p_clocks->local_offset.begin;
      double t_dummy = 1.0;
      while(t_dummy > 0)
        {
          (*p_clocks->backend.recv)(p_clocks, 0, 0x01, &t_dummy, sizeof(t_dummy));
          if(t_dummy < 0)
            break;
          double t = sync_clocks_generic_get_time_usec(p_clocks);
          (*p_clocks->backend.send)(p_clocks, 0, 0x02, &t, sizeof(t));
        }
      (*p_clocks->backend.recv)(p_clocks, 0, 0x03, p_local_offset, sizeof(*p_local_offset));
    }
}

static inline void sync_clocks_generic_synchronize(sync_clocks_generic_t p_clocks)
{
  __sync_clocks_generic_synchronize(p_clocks, 1);
}

static inline int sync_clocks_generic_barrier(sync_clocks_generic_t p_clocks, double* barrier_local_time)
{
  while(sync_clocks_generic_get_time_usec(p_clocks) < SYNC_BARRIER_MIN)
    {
#if defined(SYNC_CLOCKS_USE_PUK)
      puk_usleep(10);
#else
      usleep(10);
#endif
    }
  (*p_clocks->backend.barrier)(p_clocks);
  sync_clocks_generic_synchronize(p_clocks);
  double t_start = -1.0;
  const double t_local = sync_clocks_generic_get_time_usec(p_clocks);
  (*p_clocks->backend.gather)(p_clocks, 0, 0x04, NULL, 0, NULL);
  if(p_clocks->rank == 0)
    {
      /* compute start time on master */
      const double t_gather = sync_clocks_generic_get_time_usec(p_clocks);
      const double delay_gather = t_gather - t_local;
      t_start = t_local + 20 * delay_gather + 200.0;
    }
  (*p_clocks->backend.bcast)(p_clocks, 0, 0x05, &t_start, sizeof(t_start));
  double t_now = sync_clocks_generic_get_time_usec(p_clocks);
  double t_global = sync_clocks_generic_local_to_global(p_clocks, t_now);

  if(barrier_local_time != NULL)
    {
      *barrier_local_time = sync_clocks_generic_global_to_local(p_clocks, t_start);
    }

  if(t_global > t_start)
    {
      if(getenv("SYNC_CLOCKS_QUIET") == NULL)
        {
          char host[255];
          gethostname(host, 255);
          fprintf(stderr, "# sync_clocks: [%s] missed deadline in barrier- now = %.2lf; barrier = %.2lf (delta = %.2lf usec.)\n",
                  host, t_global, t_start, t_global - t_start);
        }
      return -1;
    }
  do
    {
      t_now = sync_clocks_generic_get_time_usec(p_clocks);
      t_global = sync_clocks_generic_local_to_global(p_clocks, t_now);
    }
  while(t_global < t_start);
  return 0;
}

static inline double sync_clocks_generic_get_origin(sync_clocks_generic_t p_clocks)
{
  return sync_clocks_generic_tick2usec(p_clocks->time_orig);
}


#endif /* SYNC_CLOCKS_GENERIC_H */

/*
 * NewMadeleine
 * Copyright (C) 2006-2021 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#ifndef NM_SYNC_CLOCKS_H
#define NM_SYNC_CLOCKS_H

#include <nm_coll_interface.h>
#include <nm_sendrecv_interface.h>

#define SYNC_CLOCKS_USE_PUK
#include "nm_sync_clocks_generic.h"


#ifdef __cplusplus
extern "C"
{
#endif

  /* ********************************************************* */
  /* ** nmad sync clocks public API */

  /** an opaque sync-clocks object */
  typedef struct sync_clocks_generic_s*nm_sync_clocks_t;

  static inline nm_sync_clocks_t nm_sync_clocks_init(nm_comm_t p_comm);

  /** destroy the given clock */
  static inline void nm_sync_clocks_shutdown(nm_sync_clocks_t p_clocks);

  /** get local time, in usec. from origin */
  static inline double nm_sync_clocks_get_time_usec(nm_sync_clocks_t p_clocks);

  /** get global clock (in usec.) from remote local clock */
  static inline double nm_sync_clocks_remote_to_global(nm_sync_clocks_t p_clocks, int rank, double t);

  /** get local time for the given global time */
  static inline double nm_sync_clocks_global_to_local(nm_sync_clocks_t p_clocks, double global_time);

  /** compute global time for the given local time */
  static inline double nm_sync_clocks_local_to_global(nm_sync_clocks_t p_clocks, double local_t);

  /** synchronize time offsets between nodes; collective operation on communicator */
  static inline void nm_sync_clocks_synchronize(nm_sync_clocks_t p_clocks);

  /** a barrier that unlocks all node at the same time as much as possible
   * parameter barrier_local_time will contain barrier time in local clock
   * returns -1 if the barrier was missed, 0 otherwise
   * */
  static inline int nm_sync_clocks_barrier(nm_sync_clocks_t p_clocks, double*barrier_local_time);

  /** get the clock skew between master and given rank */
  static inline long double nm_sync_clocks_get_skew(nm_sync_clocks_t p_clocks, int rank);

  /** get the real time origin */
  static inline double nm_sync_clocks_get_origin(nm_sync_clocks_t p_clocks);

  /* ********************************************************* */
  /* legacy function, for ascending compatibility */

#define nm_sync_clocks_compute_offsets sync_clocks_generic_synchronize

#define nm_sync_clocks_apply_offset sync_clocks_generic_remote_to_global

  static inline double nm_sync_clocks_add_real_origin_usec(nm_sync_clocks_t p_clocks, double t)
  {
    return t + sync_clocks_generic_get_origin(p_clocks);;
  }


  /* ********************************************************* */
  /* ** nmad sync clocks internals */

  struct nm_sync_clocks_nmad_s
  {
    nm_comm_t p_comm;
  };

  static inline void nm_sync_clocks_send(nm_sync_clocks_t p_clocks, int dest, int tag, void*p_data, int size)
  {
    struct nm_sync_clocks_nmad_s*p_nm_clocks = (struct nm_sync_clocks_nmad_s*)&p_clocks->backend_data;
    nm_session_t p_session = nm_comm_get_session(p_nm_clocks->p_comm);
    nm_gate_t p_gate = nm_comm_get_gate(p_nm_clocks->p_comm, dest);
    nm_sr_send(p_session, p_gate, tag, p_data, size);
  }
  static inline void nm_sync_clocks_recv(nm_sync_clocks_t p_clocks, int from, int tag, void*p_data, int size)
  {
    struct nm_sync_clocks_nmad_s*p_nm_clocks = (struct nm_sync_clocks_nmad_s*)&p_clocks->backend_data;
    nm_session_t p_session = nm_comm_get_session(p_nm_clocks->p_comm);
    nm_gate_t p_gate = nm_comm_get_gate(p_nm_clocks->p_comm, from);
    nm_sr_recv(p_session, p_gate, tag, p_data, size);
  }
  static inline void nm_sync_clocks_bcast(nm_sync_clocks_t p_clocks, int root, int tag, void*p_data, int size)
  {
    struct nm_sync_clocks_nmad_s*p_nm_clocks = (struct nm_sync_clocks_nmad_s*)&p_clocks->backend_data;
    nm_coll_bcast(p_nm_clocks->p_comm, root, p_data, size, tag);
  }
  static inline void nm_sync_clocks_gather(nm_sync_clocks_t p_clocks, int root, int tag, void*p_sbuf, int size, void*p_rbuf)
  {
    struct nm_sync_clocks_nmad_s*p_nm_clocks = (struct nm_sync_clocks_nmad_s*)&p_clocks->backend_data;
    nm_coll_gather(p_nm_clocks->p_comm, root, p_sbuf, size, p_rbuf, size, tag);
  }
  static inline void nm_sync_clocks_nm_barrier(nm_sync_clocks_t p_clocks)
  {
    const nm_tag_t tag = 0x01;
    struct nm_sync_clocks_nmad_s*p_nm_clocks = (struct nm_sync_clocks_nmad_s*)&p_clocks->backend_data;
    nm_coll_barrier(p_nm_clocks->p_comm, tag);
  }

  /* ******************************************************* */
  /* nmad sync clocks implementation */

  static inline nm_sync_clocks_t nm_sync_clocks_init(nm_comm_t p_comm)
  {
    static const struct sync_clocks_backend_s backend =
      {
       .send     = &nm_sync_clocks_send,
       .recv     = &nm_sync_clocks_recv,
       .bcast    = &nm_sync_clocks_bcast,
       .gather   = &nm_sync_clocks_gather,
       .barrier  = &nm_sync_clocks_nm_barrier
      };
    struct sync_clocks_generic_s*p_clocks = malloc(sizeof(struct sync_clocks_generic_s));
    p_clocks->backend = backend;
    struct nm_sync_clocks_nmad_s*p_nm_clocks = (struct nm_sync_clocks_nmad_s*)&p_clocks->backend_data;
    p_nm_clocks->p_comm = nm_comm_dup(p_comm);
    p_clocks->comm_size = nm_comm_size(p_nm_clocks->p_comm);
    p_clocks->rank = nm_comm_rank(p_nm_clocks->p_comm);
    sync_clocks_generic_init(p_clocks);
    return p_clocks;
  }

  static inline void nm_sync_clocks_shutdown(nm_sync_clocks_t p_clocks)
  {
    struct nm_sync_clocks_nmad_s*p_nm_clocks = (struct nm_sync_clocks_nmad_s*)&p_clocks->backend_data;
    nm_comm_destroy(p_nm_clocks->p_comm);
    sync_clocks_generic_shutdown(p_clocks);
  }

  static inline double nm_sync_clocks_get_time_usec(nm_sync_clocks_t p_clocks)
  {
    return sync_clocks_generic_get_time_usec(p_clocks);
  }

  static inline double nm_sync_clocks_remote_to_global(nm_sync_clocks_t p_clocks, int rank, double remote_time)
  {
    return sync_clocks_generic_remote_to_global(p_clocks, rank, remote_time);
  }

  static inline double nm_sync_clocks_local_to_global(nm_sync_clocks_t p_clocks, double local_time)
  {
    return sync_clocks_generic_local_to_global(p_clocks, local_time);
  }

  static inline double nm_sync_clocks_global_to_local(nm_sync_clocks_t p_clocks, double global_time)
  {
    return sync_clocks_generic_global_to_local(p_clocks, global_time);
  }

  static inline void nm_sync_clocks_synchronize(nm_sync_clocks_t p_clocks)
  {
    sync_clocks_generic_synchronize(p_clocks);
  }

  static inline int nm_sync_clocks_barrier(nm_sync_clocks_t p_clocks, double*barrier_local_time)
  {
    return sync_clocks_generic_barrier(p_clocks, barrier_local_time);
  }

  static inline long double nm_sync_clocks_get_skew(nm_sync_clocks_t p_clocks, int rank)
  {
    return sync_clocks_generic_get_skew(p_clocks, rank);
  }

  static inline double nm_sync_clocks_get_origin(nm_sync_clocks_t p_clocks)
  {
    return sync_clocks_generic_get_origin(p_clocks);
  }

#ifdef __cplusplus
}
#endif

#endif /* NM_SYNC_CLOCKS_H */

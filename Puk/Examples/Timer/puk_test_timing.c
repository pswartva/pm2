
#include <Padico/Puk.h>
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#define ITERS 500

struct timing_method_s
{
  const char*name;
  double (*get_microseconds)(void);
};

static double hpet_get_microseconds(void)
{
  return puk_timing_hpet_get_microseconds();
}

static double monotonic_get_microseconds(void)
{
  static int init_done = 0;
  static struct timespec ts_orig;
  if(!init_done)
    {
      clock_gettime(CLOCK_MONOTONIC, &ts_orig);
      init_done = 1;
    }
  struct timespec ts;
  clock_gettime(CLOCK_MONOTONIC, &ts);
  const double t = (ts.tv_sec - ts_orig.tv_sec) * 1000000.0 + (ts.tv_nsec - ts_orig.tv_nsec) / 1000.0;
  return t;
}

static double raw_get_microseconds(void)
{
  static int init_done = 0;
  static struct timespec ts_orig;
  if(!init_done)
    {
      clock_gettime(CLOCK_MONOTONIC_RAW, &ts_orig);
      init_done = 1;
    }
  struct timespec ts;
  clock_gettime(CLOCK_MONOTONIC_RAW, &ts);
  const double t = (ts.tv_sec - ts_orig.tv_sec) * 1000000.0 + (ts.tv_nsec - ts_orig.tv_nsec) / 1000.0;
  return t;
}

static double rdtsc_get_microseconds(void)
{
  static int init_done = 0;
  static puk_timing_rdtsc_tick_t rdtsc_orig;
  if(!init_done)
    {
      PUK_TIMING_RDTSC_GET_TICK(rdtsc_orig);
      init_done = 1;
    }
  puk_timing_rdtsc_tick_t t;
  PUK_TIMING_RDTSC_GET_TICK(t);
  return puk_timing_rdtsc_tick2usec_raw(PUK_TIMING_RDTSC_TICK_RAW_DIFF(rdtsc_orig, t));
}

static double gettimeofday_get_microseconds(void)
{
  static int init_done = 0;
  static puk_timing_gettimeofday_tick_t gettimeofday_orig;
  if(!init_done)
    {
      PUK_TIMING_GETTIMEOFDAY_GET_TICK(gettimeofday_orig);
      init_done = 1;
    }
  puk_timing_gettimeofday_tick_t t;
  PUK_TIMING_GETTIMEOFDAY_GET_TICK(t);
  return puk_timing_gettimeofday_tick2usec_raw(PUK_TIMING_GETTIMEOFDAY_TICK_RAW_DIFF(gettimeofday_orig, t));
}


static struct timing_method_s methods[] =
  {
    { .name = "hpet",          .get_microseconds = &hpet_get_microseconds },
    { .name = "monotonic",     .get_microseconds = &monotonic_get_microseconds },
    { .name = "raw",           .get_microseconds = &raw_get_microseconds },
    { .name = "rdtsc",         .get_microseconds = &rdtsc_get_microseconds },
    { .name = "gettimeofday",  .get_microseconds = &gettimeofday_get_microseconds },
  };
const int n_methods = sizeof(methods) / sizeof(struct timing_method_s);

static void sample(void)
{
  /* separate printf from timing to avoid disturbance */
  double t[n_methods];
  int i;
  for(i = 0; i < n_methods; i++)
    {
      t[i] = (*methods[i].get_microseconds)();
    }
  for(i = 0; i < n_methods; i++)
    {
      printf("%f\t ", t[i]);
    }
  printf("\n");
}

static void detect_clocksource(void)
{
  int fd = open("/sys/bus/clocksource/devices/clocksource0/current_clocksource", O_RDONLY);
  if(fd < 1)
    {
      printf("# unknown clock source.\n");
      return;
    }
  char s[256];
  int len = read(fd, s, 256);
  s[len] = '\0';
  printf("# clocksource = %s\n", s);
}

int main(int argc, char**argv)
{
  int i;
  for(i = 0; i < n_methods; i++)
    {
      printf("# method #%d = %s\n", i, methods[i].name);
    }
  detect_clocksource();
  fprintf(stderr, "# %s: starting %d iterations...\n", argv[0], ITERS);
  for(i = 0; i < ITERS; i++)
    {
      sample();
      puk_sleep(1);
    }

  return 0;
}

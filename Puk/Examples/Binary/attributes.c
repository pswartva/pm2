/** @file
 * Puk test: attributes with driver 'builtin'
 * @author Alexandre Denis
 */

#include <Padico/Puk.h>
#include <Padico/Module.h>

PADICO_MODULE_BUILTIN(example, NULL, NULL, NULL);

PADICO_MODULE_ATTR(test_attribute, "TEST_ATTRIBUTE", "attribute for tests with boolean", bool, 0);
PADICO_MODULE_ATTR(test_attribute2, "TEST_ATTRIBUTE2", "attribute for tests with integers", int, 0);
PADICO_MODULE_ATTR(test_attribute3, "TEST_ATTRIBUTE3", "attribute for tests with strings", string, "default");

int main(int argc, char**argv)
{
  padico_puk_init();

  struct puk_opt_s*attr1 = puk_opt_find("example", "test_attribute");
  char*v1 = puk_opt_value_to_string(attr1);
  printf("attr1 = %s\n", v1);
  padico_free(v1);

  struct puk_opt_s*attr2 = puk_opt_find("example", "test_attribute2");
  char*v2 = puk_opt_value_to_string(attr2);
  printf("attr2 = %s\n", v2);
  padico_free(v2);

  struct puk_opt_s*attr3 = puk_opt_find("example", "test_attribute3");
  char*v3 = puk_opt_value_to_string(attr3);
  printf("attr3 = %s\n", v3);
  padico_free(v3);

  padico_puk_shutdown();
  return 0;
}

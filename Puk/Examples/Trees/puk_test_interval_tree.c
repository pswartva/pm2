#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include <Padico/Puk.h>

#define N 200

struct puk_test_tree_node_s
{
  struct puk_itree_node_s itree;
};


PUK_ALLOCATOR_TYPE_SINGLE(puk_test_tree_node, struct puk_test_tree_node_s);


int main(int argc, char**argv)
{
  struct puk_test_tree_node_allocator_s allocator;
  puk_test_tree_node_allocator_init(&allocator, 16);

  struct puk_itree_s tree;
  puk_itree_init(&tree);

  int i;
  for(i = 0; i < N; i++)
    {
      int d = random() % N;
      while(puk_itree_lookup(&tree, d) != NULL)
        {
          d = random() % N;
        }
      printf("# i = %d; add data %d; height = %d\n", i, d, puk_itree_height(&tree));
      struct puk_test_tree_node_s*node = puk_test_tree_node_malloc(&allocator);
      puk_itree_insert(&tree, &node->itree, d, 10);
      puk_itree_traversal(&tree);
      assert(puk_itree_lookup(&tree, d) != NULL);

      puk_itree_show(&tree);
      printf("# #####\n");
    }

  for(i = 0; i < N; i++)
    {
      int d = random() % N;
      int l = random() % 20;
      printf("# i = %d; looking-up base = %d; len = %d\n", i, d, l);
      struct puk_itree_node_s*node = puk_itree_interval_lookup(&tree, d, l);
      if(node != NULL)
        {
          printf("#   -> base = %lu; len = %lu\n", node->base, node->len);
        }
      printf("# #####\n");
    }


  for(i = 0; i < N; i++)
    {
      int d = random() % N;
      while(puk_itree_lookup(&tree, d) == NULL)
        {
          d = random() % N;
            }
      struct puk_itree_node_s*z = puk_itree_lookup(&tree, d);
      printf("# node for data = %d: z = %p\n", d, z);
      puk_itree_delete(&tree, z);
      struct puk_test_tree_node_s*node = puk_container_of(z, struct puk_test_tree_node_s, itree);
      puk_test_tree_node_free(&allocator, node);
      printf("# i = %d; delete data %d; height = %d\n", i, d, puk_itree_height(&tree));
      puk_itree_show(&tree);
      printf("# #####\n");
      assert(puk_itree_lookup(&tree, d) == NULL);
    }

  puk_itree_destroy(&tree);

  puk_test_tree_node_allocator_destroy(&allocator);


  return 0;
}

dnl  Padico -- a High Performance Parallel and Distributed Computing Environment
dnl  Copyright (c) 2002-2023 INRIA and the University of Rennes 1
dnl  Alexandre DENIS <Alexandre.Denis@inria.fr>
dnl  Christian PEREZ <Christian.Perez@inria.fr>
dnl
dnl  The software has been registered at the Agency for the Protection of
dnl  Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
dnl
dnl  This program is free software; you can redistribute it and/or modify
dnl  it under the terms of the GNU General Public License as published by
dnl  the Free Software Foundation; either version 2 of the License, or
dnl  (at your option) any later version.
dnl
dnl  This program is distributed in the hope that it will be useful,
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
dnl  GNU General Public License for more details.
dnl
dnl  You should have received a copy of the GNU General Public License
dnl  along with this program; if not, write to the Free Software
dnl  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

dnl --------------------------------------------------------
dnl --- dynamic libraries and symbols black magic
dnl --------------------------------------------------------
dnl - Note: the goal here is to know how to get a copy of a
dnl - system library where some libc calls are wrapped.
dnl - Several methods are possible: re-link objects from
dnl - a .a archive with --wrap flag; convert a static library
dnl - with -whole-archive or -z extract; mangle with
dnl - objdump, objcopy and the likes; patch directly the ELF
dnl - library. No method works everywhere.


# ## enable/disable I/O interception
AC_DEFUN([AC_PUKABI_FSYS],
         [
           PUKABI_ENABLE_FSYS=no
           AC_MSG_CHECKING([whether to enable fsys interception])
           AC_ARG_ENABLE([fsys],
                        [AS_HELP_STRING([--enable-fsys],
                          [enable PukABI filesystem virtualization mechanism])],
                        [PUKABI_ENABLE_FSYS=$enableval])
           AC_MSG_RESULT([$PUKABI_ENABLE_FSYS])
           AC_SUBST([PUKABI_ENABLE_FSYS])
           if test "x$PUKABI_ENABLE_FSYS" = "xyes"; then
             AC_DEFINE([PUKABI_ENABLE_FSYS], [1],
                       [Define to 1 to enable interception of filesystem-related calls by PukABI])
             pukabi_preload_libresolv=${pukabi_root}/lib/PukABI/libresolv.so
           fi
           AC_SUBST([pukabi_preload_libresolv])
         ])

AC_DEFUN([AC_PUKABI_PROC],
         [
           PUKABI_ENABLE_PROC=no
           AC_MSG_CHECKING([whether to enable proc interception])
           AC_ARG_ENABLE([proc],
                        [AS_HELP_STRING([--enable-proc],
                          [enable PukABI filesystem virtualization mechanism])],
                        [PUKABI_ENABLE_PROC=$enableval])
           AC_MSG_RESULT([$PUKABI_ENABLE_PROC])
           AC_SUBST([PUKABI_ENABLE_PROC])
           if test "x$PUKABI_ENABLE_PROC" = "xyes"; then
             AC_DEFINE([PUKABI_ENABLE_PROC], [1],
                       [Define to 1 to enable interception of process-related calls by PukABI])
           fi
         ])

AC_DEFUN([AC_PUKABI_MEM],
         [
           PUKABI_ENABLE_MEM=no
           AC_MSG_CHECKING([whether to enable mem interception])
           AC_ARG_ENABLE([mem],
                        [AS_HELP_STRING([--enable-mem],
                          [enable PukABI filesystem virtualization mechanism])],
                        [PUKABI_ENABLE_MEM=$enableval])
           AC_MSG_RESULT([$PUKABI_ENABLE_MEM])
           AC_SUBST([PUKABI_ENABLE_MEM])
           if test "x$PUKABI_ENABLE_MEM" = "xyes"; then
             AC_DEFINE([PUKABI_ENABLE_MEM], [1],
                       [Define to 1 to enable interception of memory-related calls by PukABI])
           fi
         ])

AC_DEFUN([AC_PUKABI_RESOLV],
         [
           PUKABI_ENABLE_RESOLV=no
           AC_MSG_CHECKING([whether to enable resolv interception])
           AC_ARG_ENABLE([resolv],
                        [AS_HELP_STRING([--enable-resolv],
                          [enable PukABI filesystem virtualization mechanism])],
                        [PUKABI_ENABLE_RESOLV=$enableval])
           AC_MSG_RESULT([$PUKABI_ENABLE_RESOLV])
           AC_SUBST([PUKABI_ENABLE_RESOLV])
           if test "x$PUKABI_ENABLE_RESOLV" = "xyes"; then
             AC_DEFINE([PUKABI_ENABLE_RESOLV], [1],
                       [Define to 1 to enable interception of resolver-related calls by PukABI])
           fi
         ])


# ## detect 'errno' indirection symbol
AC_DEFUN([AC_PADICO_ERRNO],
         [ AC_REQUIRE([AC_PADICO_COMPILER])
           AC_MSG_CHECKING([for symbol indirection to errno])
cat > conftest.c <<EOF
#include <pthread.h>
#ifndef _REENTRANT
#define _REENTRANT
#endif
#include <errno.h>
#define PADICOERRNO errno
PADICOBEGIN PADICOERRNO PADICOEND
EOF
dnl temporarly change quoting rules. sed expressions are hell when [] are M4 quote symbols :-)
changequote(<<, >>)dnl
symbol="`${CC} -E ${CPPFLAGS} conftest.c | sed -e "/PADICOBEGIN/,/PADICOEND/!d;s,PADICOBEGIN,,;s,PADICOEND,," | tr '\n' ' '  | sed -e 's/.*\*[ (]*\([a-zA-Z0-9_]*[ ]*\)(.*/\1/'`"
changequote([, ])dnl
rm -f conftest.c
if test x${symbol} = x ; then
  AC_MSG_ERROR([Cannot resolve errno indirection (symbol=${symbol}).])
else
  AC_MSG_RESULT(${symbol})
  AC_DEFINE_UNQUOTED([PUKABI_SYMBOL_ERRNO], ${symbol}, [indirection to errno])
fi
         ])



dnl - Note: several methods are available:
dnl - elfwrap: symbol wrapping in ELF objects
dnl - gnu-ld:  GNU ld's --wrap,
dnl - BSD:     BSD's symbol interception
dnl - SUN:     Sun's symbolinterposition.

# ## symbol wrapping
AC_DEFUN([AC_PADICO_WRAPPING],
         [ AC_REQUIRE([AC_PADICO_COMPILER])
           AC_REQUIRE([AC_PADICO_LINKER])
           save_LDFLAGS="${LDFLAGS}"
           AC_MSG_CHECKING([for symbol wrapping method])
           AC_LANG_C
           SYMBOL_WRAPPING_METHOD=none

dnl - 1. elfwrap
if test "$SYMBOL_WRAPPING_METHOD" = "none"; then
  if test "$binary_format" = "elf"; then
    SYMBOL_WRAPPING_METHOD=elfwrap
  fi
fi

dnl - 2. gnu-ld
if test "$SYMBOL_WRAPPING_METHOD" = "none"; then
  if test "x${ATOSO_OK}" = "xyes" ; then
    LDFLAGS="${save_LDFLAGS} -Wl,--wrap,foo"
    AC_LINK_IFELSE(
       [AC_LANG_SOURCE([[
extern int foo(void);
int __wrap_foo(void)
  { return 0; }
int main(int argc, char**argv)
  { return foo(); }
         ]])
       ],
       [SYMBOL_WRAPPING_METHOD=gnu-ld],
       [])
  fi
fi

dnl - 3. Sun
if test "$SYMBOL_WRAPPING_METHOD" = "none"; then
  gcc_linker="`${CC} -print-file-name=ld`"
  if test "x${gcc_linker}" = "x/usr/ccs/bin/ld"; then
    SYMBOL_WRAPPING_METHOD=SUN
  fi
fi

dnl - 4. BSD
if test "$SYMBOL_WRAPPING_METHOD" = "none"; then
  LDFLAGS="${save_LDFLAGS} -Wl,-i${SYMBOL_PREFIX}foo:${SYMBOL_PREFIX}padico_wrap_foo"
  AC_LINK_IFELSE(
     [AC_LANG_SOURCE([[
extern int foo(void);
int padico_wrap_foo(void)
  { return 0; }
int main(int argc, char**argv)
  { return foo(); }
       ]])
     ],
     [SYMBOL_WRAPPING_METHOD=BSD],
     [])
fi

           AC_MSG_RESULT([${SYMBOL_WRAPPING_METHOD}])
           case $SYMBOL_WRAPPING_METHOD in
             elfwrap*)
               AC_DEFINE([PADICO_WRAPPING_ELFWRAP], 1,
                         [Use ELF patch in dynamic libraries.])
             ;;
             objcopy*)
               AC_DEFINE([PADICO_WRAPPING_OBJCOPY], 1,
                         [Use GNU objcopy redefine-symbol.])
             ;;
             gnu-ld*)
               AC_DEFINE([PADICO_WRAPPING_GNU], 1,
                         [Use GNU ld symbol wrapping.])
             ;;
             BSD*)
               AC_DEFINE([PADICO_WRAPPING_BSD], 1,
                         [Use BSD linker symbol interception.])
             ;;
             SUN*)
               AC_DEFINE([PADICO_WRAPPING_SUN], 1,
                         [Use SUN symbol interposition.])
             ;;
             *)
               AC_MSG_ERROR([No wrapping method found for linker.])
             ;;
           esac
           AC_SUBST(SYMBOL_WRAPPING_METHOD)
           LDFLAGS="${save_LDFLAGS}"
         ])

AC_DEFUN([AC_PADICO_OBJCOPY],
         [ if test "${SYMBOL_WRAPPING_METHOD}" = "objcopy" ; then
             AC_PATH_PROG(OBJCOPY, [ ${OBJCOPY} objcopy ])
             AC_PATH_PROG(OBJDUMP, [ ${OBJDUMP} objdump ])
             if test "x${OBJCOPY}" = "x" -o "x${OBJDUMP}" = "x" ; then
               AC_MSG_ERROR([ objcopy or objdump not found. PadicoTM requires GNU binutils. ])
             fi
             AC_ARG_VAR(OBJCOPY, [ objcopy from GNU binutils ])
             AC_ARG_VAR(OBJDUMP, [ objdump from GNU binutils ])
           fi
         ])

AC_DEFUN([AC_PADICO_ELFWRAP],
         [ if test "${SYMBOL_WRAPPING_METHOD}" = "elfwrap" -o "${SYMBOL_WRAPPING_METHOD}" = "gnu-ld" ; then
             AC_PATH_PROG(READELF, [ ${READELF} readelf ])
             if test "x${READELF}" = "x" ; then
               AC_MSG_ERROR([ readelf not found. PadicoTM requires GNU binutils. ])
             fi
             AC_ARG_VAR(READELF, [ readelf from GNU binutils ])
           fi
         ])

# ## find libresolv for wrapping
AC_DEFUN([AC_PADICO_LIBRESOLV],
         [ AC_REQUIRE([AC_PADICO_COMPILER])
           AC_MSG_CHECKING([for libresolv.a])
           LIBRESOLV_A=`${CC} -print-file-name=libresolv.a`
           AC_MSG_RESULT([$LIBRESOLV_A])
           AC_MSG_CHECKING([for libresolv.so.2])
           LIBRESOLV_SO=`${CC} -print-file-name=libresolv.so`
           AC_MSG_RESULT([$LIBRESOLV_SO])
           AC_CHECK_FILES([$LIBRESOLV_SO $LIBRESOLV_A],
                          [HAVE_LIB_RESOLV=yes],
                          [HAVE_LIB_RESOLV=no])
           AC_SUBST(HAVE_LIB_RESOLV)
           AC_SUBST(LIBRESOLV_A)
           AC_SUBST(LIBRESOLV_SO)
        ])

# ## glibc detection black magic
AC_DEFUN([AC_PADICO_LIBCMAGIC],
         [ AC_REQUIRE([AC_PADICO_COMPILER])
           AC_MSG_CHECKING([Libc system])
           case $binary_format in
             elf)
               case `uname -s` in
                 Linux)
                   dnl -- searching for GNU C Library on Linux
                   echo "int main(int argc, char**argv){ return 1;}"  > ac_test.c
                   $CC ac_test.c
                   src_libc=`ldd a.out | grep libc | cut -f 3 -d " " `
                   src_ld=`ldd a.out | grep ld-linux | cut -f 3 -d " " `
                   dnl -- beware: sometime libc is not executable
                   res=`$src_ld $src_libc | grep "GNU C Library" | head -1`
                   if test "x$res" == "x" ; then
                     res="Unknown libc: $res"
                     libc_ver="Unknown"
                   else
                     name_libc=`ldd a.out | grep libc | cut -f 1 -d " " | sed 's/.*\(lib.*\)/\1/'`
                     glibc_ver=`readlink $src_libc | sed 's/libc-\(.*\).so/\1/'`
                     libc_ver="Linux-GLIBC-$glibc_ver"
                     res=$libc_ver
                   fi
                   rm -f ac_test.c ac_test.d a.out
                  ;;
                *)
                  res="Unknown"
                  libc_ver="Unknown"
                ;;
              esac
              ;;
            *)
              res="Unsupported for non ELF system"
              libc_ver="Unsupported"
              ;;
           esac
           dnl -- only used in Puk/System
           AC_SUBST(src_libc)  dnl -- full path to the "original" shared libc
           AC_SUBST(name_libc) dnl -- short name of the libc (used for dependencies)
           AC_SUBST(glibc_ver) dnl -- just the version of the GLIBC (only on Linuc/GLIBC)
           AC_SUBST(libc_ver)  dnl -- version of the libc of the form "Linux-GLIBC-2.2.4" (OS-LibcName-Version)
           AC_SUBST(src_ld)    dnl -- full path to the "original" system loader
           AC_MSG_RESULT($libc_ver)
         ])

dnl -- miscellaneous features for PukABI
AC_DEFUN([AC_PUKABI_FEATURES],
         [
dnl - Look for the `at' file functions introduced in POSIX 2008 and
dnl - earlier in Glibc.
           AC_CHECK_FUNC([openat],
                         [AC_DEFINE([PUKABI_HAVE_ATFILE_FUNCTIONS], [1], [enable openat])])
           AC_CHECK_FUNC([openat64],
                         [AC_DEFINE([PUKABI_HAVE_ATFILE64_FUNCTIONS], [1], [enable openat64])])
dnl - Solaris needs <sys/link.h> to parse ELF files
           AC_CHECK_HEADER(sys/link.h,
             [ PUKABI_HAVE_SYS_LINK_H="yes"
               AC_DEFINE(PUKABI_HAVE_SYS_LINK_H, 1, [Define if you have the <sys/link.h> header file.])],
             [PUKABI_HAVE_SYS_LINK_H="no"])

           AC_CHECK_FUNC([__fxstat],
                         [AC_DEFINE([PUKABI_HAVE___FXSTAT], [1], [have __fxstat])])

           AC_CHECK_HEADER(sys/inotify.h,
             [ PUKABI_HAVE_INOTIFY_H="yes"
               AC_DEFINE(PUKABI_HAVE_INOTIFY_H, 1, [Define if you have the <sys/inotify.h> header file.])],
             [PUKABI_HAVE_INOTIFY_H="no"])

         ])

AC_DEFUN([AC_PADICO_OUT_PUKABI],
         [
           dnl --- PukABI
           AC_PADICO_OUT_MK([Makefile PukABI.pc])
           AC_PADICO_OUT_SH([puk-abi-getsymbols.sh])
           AC_PADICO_OUT_SH([puk-abi-wrap.sh])
           AC_DEFINE_UNQUOTED([PUKABI_SIZEOF_INT],       [$ac_cv_sizeof_int], [sizeof(int)])
           AC_DEFINE_UNQUOTED([PUKABI_SIZEOF_LONG],      [$ac_cv_sizeof_long], [sizeof(long)])
           AC_DEFINE_UNQUOTED([PUKABI_SIZEOF_LONG_LONG], [$ac_cv_sizeof_long_long], [sizeof(long long)])
           AC_DEFINE_UNQUOTED([PUKABI_SIZEOF_SSIZE_T],   [$ac_cv_sizeof_ssize_t], [sizeof(ssize_t)])
         ])

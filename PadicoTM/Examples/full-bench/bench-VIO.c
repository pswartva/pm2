/* bench-VIO.c
 */

#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "bench-common.h"
#include <Padico/VIO.h>
#include <Padico/Timing.h>
#include <Padico/Module.h>

int bench_vio_init(void);
int bench_vio_run(int argc, char**argv);
void bench_vio_finalize(void);

PADICO_MODULE_DECLARE(bench_VIO, bench_vio_init, bench_vio_run, bench_vio_finalize);

#define DEFAULT_PORT       5040

static int port = DEFAULT_PORT;

static struct sockaddr_in local_addr;
static socklen_t          local_addr_size  = sizeof(local_addr);
static struct sockaddr_in remote_addr;
static socklen_t          remote_addr_size = sizeof(remote_addr);
static int bench_fd = -1;
static int server_fd = -1;

int bench_vio_init(void)
{
  if(bench_common_param.is_server)
    {
      /* server socket */
      int rc;
      server_fd = padico_vio_socket(PF_INET, SOCK_STREAM, 0);
      local_addr.sin_family = AF_INET;
      local_addr.sin_port   = htons(port);
      rc = padico_vio_bind(server_fd, (struct sockaddr*)&local_addr, local_addr_size);
      if(rc != 0)
        {
          padico_warning("BENCH: cannot bind on port %d\n", port);
          return -1;
        }
      rc = padico_vio_listen(server_fd, 5);
      if(rc != 0)
        {
          padico_warning("listen() error port = %d\n", port);
          return -1;
        }
      padico_print("BENCH: server ready\n");
      bench_fd = padico_vio_accept(server_fd, (struct sockaddr*)&remote_addr, &remote_addr_size);
      if(bench_fd < 0)
        {
          int err = padico_vio_errno;
          padico_warning("BENCH: accept()- errno=%d (%s).\n", err, strerror(err));
          return -1;
        }
      padico_print("BENCH: received connection.\n");
      padico_vio_close(server_fd);
    }
  else
    {
      /* client socket */
      int rc = -1, err = -1;
      padico_print("BENCH: starting client.\n");
      bench_fd = padico_vio_socket(PF_INET, SOCK_STREAM, 0);
      remote_addr.sin_addr   = *padico_topo_host_getaddr(padico_topo_node_gethost(bench_common_param.server_node));
      remote_addr.sin_family = AF_INET;
      remote_addr.sin_port   = htons(port);
      do
        {
          padico_timing_t tick1, tick2;
          padico_print("BENCH: waiting for the server to come up...\n");
          padico_tm_sleep(2);
          padico_print("BENCH: connecting to server %s...\n", inet_ntoa(remote_addr.sin_addr));
          padico_timing_get_tick(&tick1);
          rc = padico_vio_connect(bench_fd, (struct sockaddr*)&remote_addr, remote_addr_size);
          padico_timing_get_tick(&tick2);
          if(rc != 0)
            {
              err = padico_vio_errno;
                        if (err==EISCONN) {
                          padico_warning("Already connected !!!\n");
                          break;
                        }
              padico_warning("cannot connect- errno=%d (%s). Retrying...\n", err, strerror(err));
              padico_tm_sleep(1);
              /* TODO: c'est un peu bourrin d'attendre... */
            }
          else
            {
              padico_print("BENCH: client ready- connect = %g usec.\n",
                           padico_timing_diff_usec(&tick1, &tick2));
            }
        }
      while(rc != 0);
    }
  puk_component_t component = padico_vio_component(bench_fd);
  padico_string_t c = puk_component_serialize(component);
  padico_print("bench-VIO: component used is %s\n", padico_string_get(c));
  padico_string_delete(c);

  return 0;
}

void bench_vio_finalize(void)
{
  padico_vio_close(bench_fd);
}

static ssize_t vio_test_in(int fildes, void *buf, size_t nbyte)
{
  size_t done = 0;
  //  fprintf(stderr, "entering test_in\n");
  while(done < nbyte)
    {
      int rc = padico_vio_read(fildes, buf+done, nbyte - done);

      if(rc > 0)
        done += rc;
      else if(rc == 0)
        break;
      else{
        //      fprintf(stderr, " test_in done\n");
        return rc;
      }
    }
  if(done != nbyte)
    padico_warning("BENCH: read()- nbyte=%d; rc=%d\n", nbyte, done);
  //    fprintf(stderr, " test_in done\n");
  return done;
}

static ssize_t vio_test_out(int fildes, const void *buf, size_t nbyte)
{
  //  fprintf(stderr, "Entering test_out\n");
  size_t done = 0;
  while(done < nbyte)
    {
      int rc = padico_vio_write(fildes, buf+done, nbyte-done);
      if(rc > 0)
        done += rc;
      else if(rc == 0)
        break;
      else {
        //      fprintf(stderr, "test_out done\n");
        return rc;
      }
    }
  if(done != nbyte)
    padico_warning("BENCH: write()- nbyte=%d; rc=%d\n", nbyte, done);
  //  fprintf(stderr, "test_out done\n");
  return done;
}

static void vio_server(char*buffer, size_t lenbuf)
{
  vio_test_out(bench_fd, buffer, lenbuf);
  vio_test_in(bench_fd, buffer, lenbuf);
}

static void vio_client(char*buffer, size_t lenbuf)
{
  vio_test_in(bench_fd, buffer, lenbuf);
  vio_test_out(bench_fd, buffer, lenbuf);
}

static const struct bench_desc_s vio_bench =
{
  "VIO",
  &vio_server,
  &vio_client
};

int bench_vio_run(int argc, char**argv)
{
  the_bench(&vio_bench);
  return 0;
}

/*
 * NewMadeleine
 * Copyright (C) 2009-2022 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

/** @ingroup nmad_private
 * @file
 * Locking interface used in nm core. This is not part of the public API
 * and may not be used to lock something else than nmad core.
 */

#include <pthread.h>

#ifndef NM_LOCK_H
#define NM_LOCK_H

#ifndef NM_PRIVATE_H
#  error "Cannot include this file directly. Please include <nm_private.h>"
#endif

/** acquire the nm core lock */
static inline void nm_core_lock(struct nm_core*p_core);

/** try to lock the nm core core
 * return 1 is lock is successfully acquired, 0 otherwise
 */
static inline int nm_core_trylock(struct nm_core*p_core);

/** unlock the nm core lock */
static inline void nm_core_unlock(struct nm_core*p_core);

/** init the core lock */
static inline void nm_core_lock_init(struct nm_core*p_core);

/** destroy the core lock */
static inline void nm_core_lock_destroy(struct nm_core*p_core);

/** assert that current thread doesn't hold the lock */
static inline void nm_core_nolock_assert(struct nm_core*p_core);

/** assert that current thread holds the lock */
static inline void nm_core_lock_assert(struct nm_core*p_core);


/* ********************************************************* */
/* inline definition */

static inline void nm_core_lock(struct nm_core*p_core __attribute__((unused)))
{
  nm_spin_lock(&p_core->lock);
  nm_profile_inc(p_core->profiling.n_locks);
}

static inline int nm_core_trylock(struct nm_core*p_core __attribute__((unused)))
{
  int rc = nm_spin_trylock(&p_core->lock);
  if(rc)
    nm_profile_inc(p_core->profiling.n_locks);
  return rc;
}

static inline void nm_core_unlock(struct nm_core*p_core __attribute__((unused)))
{
  nm_spin_unlock(&p_core->lock);
}

static inline void nm_core_lock_init(struct nm_core*p_core __attribute__((unused)))
{
  nm_spin_init(&p_core->lock);
}

static inline void nm_core_lock_destroy(struct nm_core*p_core __attribute__((unused)))
{
  nm_spin_destroy(&p_core->lock);
}

static inline void nm_core_lock_assert(struct nm_core*p_core __attribute__((unused)))
{
  nm_spin_assert_locked(&p_core->lock);
}

static inline void nm_core_nolock_assert(struct nm_core*p_core __attribute__((unused)))
{
  nm_spin_assert_notlocked(&p_core->lock);
}

#endif  /* NM_LOCK_H */

/**
 * A simple JuxMem client (do nothing)
 * $Id: simple_provider.c,v 1.4 2005/06/29 11:56:58 mjan Exp $
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <apr_time.h>

#include <juxmem.h>

#define DEBUG 1

int main(int argc, char **argv)
{
    JuxMem_peer *juxmem_peer = NULL;
    int uptime = 0;
    long memory = 0;

    if (argc != 3) {
        printf("Syntax: simple_provider [uptime in seconds] [memory_size in number of blocks]\n");
        exit(0);
    }

    uptime = atoi(argv[1]);
    memory = atol(argv[2]);

    juxmem_peer = juxmem_initialize(NULL, (int) DEBUG, "jxta.log");
    assert(juxmem_peer != NULL);

    juxmem_memory_configure(juxmem_peer, memory);

    apr_sleep(APR_USEC_PER_SEC * uptime);

    assert(juxmem_terminate(juxmem_peer) == (int) TRUE);

    return 0;
}

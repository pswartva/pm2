/*
 * NewMadeleine
 * Copyright (C) 2016-2019 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 *
 * This example is a draft showing how to build and use diffusion trees for
 * multicast.
 * The data to send is wrapped within a structure containing:
 * - the data itself
 * - ids of the nodes to which the data needs to be forwarded
 * - a header containing the size of the data and the number of nodes
 */


#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

#include <nm_launcher_interface.h>
#include <nm_coll_interface.h>
#include <nm_log.h>
#include <Padico/Puk.h>

#include "../common/nm_examples_helper.h"


/** header of mutlicast requests */
struct multicast_header_s
{
  nm_len_t body_len;
  int nb_nodes;
}  __attribute__((packed));


const nm_tag_t TAG_DYNAMIC_COLL = ((nm_tag_t) 1 << 33);


/** a multicast request (outgoing or ingoing) */
struct nm_multicast_req_s
{
  nm_sr_request_t request;                /**< the sendrecv request, actually allocated here */
  nm_session_t p_session;                 /**< session the incoming request belongs to */
  struct nm_data_s body_data;             /**< the user-supplied body data descriptor, copied to
                                               allow asynchronicity */
  struct nm_data_s nodes_data;            /**< data descriptor for nodes to still send data */
  struct nm_data_s header_data;           /**< pre-built type for header recv */
  struct nm_data_s multicast_data;        /**< data descriptor for multicast request */
  int* ref_nodes;                         /**< ref for nodes to still send data */
  void* ref_body;                         /**< user ref for sent data */
  struct multicast_header_s* ref_header;  /**< header of the multicast request */
  int last;                               /**< boolean to 1 if it'is the last request of the multicast,
                                               to free resources */
};

PUK_ALLOCATOR_TYPE(nm_multicast_req, struct nm_multicast_req_s);

static nm_multicast_req_allocator_t nm_multicast_req_allocator = NULL;


/** data content for a mutlicast request (header + nodes to reach + body) */
struct nm_multicast_content_s
{
  void* header_ptr;                /**< pointer to a header buffer */
  const struct nm_data_s* p_nodes; /**< list of nodes to which send data */
  const struct nm_data_s* p_body;  /**< user-supplied body */
};

static void nm_multicast_traversal(const void*_content, nm_data_apply_t apply, void*_context);
const struct nm_data_ops_s nm_multicast_ops =
  {
    .p_traversal = &nm_multicast_traversal
  };

NM_DATA_TYPE(multicast, struct nm_multicast_content_s, &nm_multicast_ops);

static void nm_multicast_traversal(const void*_content, nm_data_apply_t apply, void*_context)
{
  const struct nm_multicast_content_s*p_content = _content;

  (*apply)(p_content->header_ptr, sizeof(struct multicast_header_s), _context);

  nm_data_traversal_apply(p_content->p_body, apply, _context);
  nm_data_traversal_apply(p_content->p_nodes, apply, _context);
}

void nm_multicast_data_build(struct nm_data_s* p_multicast_data, void* hptr,
                             const struct nm_data_s* p_nodes, const struct nm_data_s* p_body)
{
  nm_data_multicast_set(p_multicast_data, (struct nm_multicast_content_s)
      {
        .header_ptr = hptr,
        .p_nodes    = p_nodes,
        .p_body     = p_body
      });
}

static inline void free_request_nodes_data(int* nodes, void* data)
{
  free(nodes);

  if(data != NULL)
    {
      free(data);
    }
}

static void monitor_send(nm_sr_event_t event, const nm_sr_event_info_t*p_info, void*_ref)
{
  struct nm_multicast_req_s* p_req = (struct nm_multicast_req_s*) _ref;

  free(p_req->ref_header);

  if(p_req->last)
    {
      free_request_nodes_data(p_req->ref_nodes, p_req->ref_body);
    }

  nm_multicast_req_free(nm_multicast_req_allocator, p_req);
}

/**
 * @param nodes: destinatory nodes, excluding src node
 * @param nb_nodes: nb_nodes in mutlicast, including src node
 */
void send_multicast(nm_session_t p_session, int* nodes, int nb_nodes, void* data, nm_len_t body_len, int free_data)
{
  int rank;
  nm_launcher_get_rank(&rank);
  int separator_id = nb_nodes;

  struct nm_coll_tree_info_s tree;

  nm_coll_tree_init(&tree, NM_COLL_TREE_BINOMIAL, nb_nodes, 0, 0);
  int* children = malloc(sizeof(int) * tree.max_arity);

  for(int i = 0; i < tree.height; i++)
    {
      int parent = -1;
      int n_children = 0;

      nm_coll_tree_step(&tree, i, &parent, children, &n_children);

      for(int c = 0; c < n_children; c++)
        {
          children[c]--;

          int nb_nodes_in_remaining_multicast = separator_id - children[c] - 2;

          struct nm_multicast_req_s* p_req = nm_multicast_req_malloc(nm_multicast_req_allocator);
          p_req->ref_header = malloc(sizeof(struct multicast_header_s));
          p_req->ref_header->body_len = body_len;
          printf("%d\n", nb_nodes_in_remaining_multicast);
          p_req->ref_header->nb_nodes = nb_nodes_in_remaining_multicast;
          p_req->p_session = p_session;

          nm_gate_t gate = NULL;
          nm_launcher_get_gate(nodes[children[c]], &gate);

          struct nm_data_s body_data;
          nm_data_contiguous_build(&body_data, data, body_len);

          struct nm_data_s nodes_data;
          nm_data_contiguous_build(&nodes_data, (void*) (nodes + children[c] + 1),
                                    nb_nodes_in_remaining_multicast * sizeof(int));

          p_req->body_data = body_data;
          p_req->nodes_data = nodes_data;
          p_req->ref_nodes = nodes;
          p_req->ref_body = free_data ? data : NULL;

          if(i == (tree.height-1) && c == (n_children-1))
            {
              p_req->last = 1;
            }
          else
            {
              p_req->last = 0;
            }

          struct nm_data_s multicast_data;
          nm_multicast_data_build(&multicast_data, p_req->ref_header, &p_req->nodes_data, &p_req->body_data);
          nm_sr_send_init(p_session, &p_req->request);
          nm_sr_send_pack_data(p_session, &p_req->request, &multicast_data);
          nm_sr_request_set_ref(&p_req->request, p_req);
          nm_sr_request_monitor(p_session, &p_req->request, NM_SR_EVENT_FINALIZED, &monitor_send);
          nm_sr_send_dest(p_session, &p_req->request, gate, 1);

          fprintf(stderr, "%d sending to %d\n", rank, nodes[children[c]]);

          nm_sr_send_header(p_session, &p_req->request, sizeof(struct multicast_header_s));
          nm_sr_send_submit(p_session, &p_req->request);

          separator_id = children[c] + 1; // TODO FIX: not working when n_children > 1
        }
    }

  free(children);
}

static inline void send_multicast_and_free_data(nm_session_t p_session, int* nodes, int nb_nodes,
                                                void* data, nm_len_t body_len)
{
   send_multicast(p_session, nodes, nb_nodes, data, body_len, 1);
}

static inline void send_multicast_no_free_data(nm_session_t p_session, int* nodes, int nb_nodes,
                                                void* data, nm_len_t body_len)
{
   send_multicast(p_session, nodes, nb_nodes, data, body_len, 0);
}


static void nm_multicast_handler(nm_sr_event_t event, const nm_sr_event_info_t*p_info, void*_ref)
{
  struct nm_multicast_req_s* p_req = _ref;

  assert(! ((event & NM_SR_EVENT_FINALIZED) &&
            (event & NM_SR_EVENT_RECV_DATA)));

  int rank = -1;
  nm_launcher_get_rank(&rank);

  if(event & NM_SR_EVENT_FINALIZED)
    {
      fprintf(stderr, "[%d] multicast_hello_finalizer()- received body: %s\n",
              rank, (char*) p_req->ref_body);

      if(p_req->ref_header->nb_nodes > 0)
        {
          send_multicast_and_free_data(p_session, p_req->ref_nodes, p_req->ref_header->nb_nodes + 1,
                                      (void*) p_req->ref_body, p_req->ref_header->body_len);
        }
      else
        {
          free_request_nodes_data(p_req->ref_nodes, p_req->ref_body);
        }

      free(p_req->ref_header);
      nm_multicast_req_free(nm_multicast_req_allocator, p_req);
    }

  if(event & NM_SR_EVENT_RECV_DATA)
    {
      nm_tag_t received_tag = nm_sr_request_get_tag(&p_req->request);
      printf("tag: %lx\n", received_tag);

      nm_data_null_build(&p_req->body_data);
      nm_data_null_build(&p_req->nodes_data);
      int rc = nm_sr_recv_peek(p_req->p_session, &p_req->request, &p_req->header_data);

      if(rc != NM_ESUCCESS)
        {
          NM_FATAL("# nm_multicast: rc = %d in nm_sr_recv_peek()\n", rc);
        }

      const nm_len_t body_len = p_req->ref_header->body_len;
      const int nb_nodes = p_req->ref_header->nb_nodes;

      /*printf("[%d] multicast_hello_handler()- received header: len = %lu, nb_nodes = %d\n",
              rank, body_len, nb_nodes);*/

      p_req->ref_body = malloc(body_len);
      nm_data_contiguous_build(&p_req->body_data, p_req->ref_body, body_len);

      p_req->ref_nodes = malloc(nb_nodes * sizeof(int));
      nm_data_contiguous_build(&p_req->nodes_data, p_req->ref_nodes, nb_nodes * sizeof(int));

      nm_multicast_data_build(&p_req->multicast_data, p_req->ref_header,
                              &p_req->nodes_data, &p_req->body_data);
      nm_sr_recv_unpack_data(p_req->p_session, &p_req->request, &p_req->multicast_data);
    }
}

void receive_multicast(nm_session_t p_session)
{
  struct nm_multicast_req_s* p_req = nm_multicast_req_malloc(nm_multicast_req_allocator);
  p_req->p_session = p_session;
  p_req->ref_header = malloc(sizeof(struct multicast_header_s));
  nm_data_contiguous_build(&p_req->header_data, p_req->ref_header, sizeof(struct multicast_header_s));
  nm_sr_recv_init(p_session, &p_req->request);
  nm_sr_request_set_ref(&p_req->request, p_req);
  nm_sr_request_monitor(p_session, &p_req->request, NM_SR_EVENT_FINALIZED | NM_SR_EVENT_RECV_DATA,
                        &nm_multicast_handler);
  nm_sr_recv_irecv(p_session, &p_req->request, NM_ANY_GATE, 1, NM_TAG_MASK_FULL ^ TAG_DYNAMIC_COLL);
}


const char msg[] = "Hello, world!";


int main(int argc, char**argv)
{
  nm_examples_init_topo(&argc, argv, NM_EXAMPLES_TOPO_STAR);

  nm_multicast_req_allocator = nm_multicast_req_allocator_new(8);

  if(is_server)
    {
      int n;
      nm_launcher_get_size(&n);

      int nb_dest_nodes = n - 1;

      int* nodes = malloc(nb_dest_nodes * sizeof(int)); // freed by send_mutlicast

      for(int i = 1; i < n; i++)
        {
          nodes[i-1] = i;
        }

      send_multicast_no_free_data(p_session, nodes, n, (void*) msg, sizeof(msg));
    }
  else
    {
      receive_multicast(p_session);
    }

  nm_examples_exit();
  return 0;

}

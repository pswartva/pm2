/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file ns-basic-parse.c
 * @brief XML parser of NS-basic module
 * @ingroup NetSelector-basic
 */

#include "Ns-basic-internals.h"

PADICO_MODULE_HOOK(NetSelector_basic);


/* *** NS db *********************************************** */
/* Note: these functions are used only once.
 * 'inline' to get a more compact binary code.
 */

static inline ns_entity_t ns_entity_new(enum ns_entity_kind_e kind)
{
  ns_entity_t entity = padico_malloc(sizeof(struct ns_entity_s));
  entity->kind = kind;
  ns_entity_vect_init(&entity->contained);
  return entity;
}

static inline void nsdb_group_flatten(ns_entity_t group)
{
  ns_entity_t*_i;
  puk_vect_foreach(_i, ns_entity, &group->group.entities)
    {
      ns_entity_t i = *_i;
      switch(i->kind)
        {
        case NS_ENTITY_NODE:
        case NS_ENTITY_HOST:
          ns_entity_vect_push_back(&group->group.leaves, i);
          break;
        case NS_ENTITY_GROUP:
          {
            ns_entity_t*_e;
            puk_vect_foreach(_e, ns_entity, &i->group.leaves)
              {
                ns_entity_t e = *_e;
                ns_entity_vect_push_back(&group->group.leaves, e);
                ns_entity_vect_push_back(&e->contained, group);
              }
          }
          break;
        default:
          padico_fatal("bad entity type in group.\n");
          break;
        }
      ns_entity_vect_push_back(&i->contained, group);
    }
}

static inline ns_entity_t nsdb_create_node(const char*_node_name)
{
  padico_topo_node_t node = padico_topo_getnodebyname(_node_name);
  const char*node_name = node?padico_topo_node_getname(node):_node_name;
  ns_entity_t entity = puk_hashtable_lookup(nsdb.nodes_by_name, node_name);
  if(!entity)
    {
      entity = ns_entity_new(NS_ENTITY_NODE);
      entity->node.name = strcmp(node_name, "*") == 0?NULL:padico_strdup(node_name);
      entity->node.node = node;
      if(entity->node.name)
        puk_hashtable_insert(nsdb.nodes_by_name, entity->node.name, entity);
    }
  return entity;
}

static inline ns_entity_t nsdb_create_host(const char*name, const char*addr)
{
  ns_entity_t entity = NULL;
  const int is_wildcard = name && (strchr(name, '*') || strchr(name, '?') || strchr(name, '['));
  padico_topo_host_t host = (name && !is_wildcard) ? padico_topo_host_resolvebyname(name) : NULL;
  if(addr)
    {
      struct in_addr inaddr;
      inet_aton(addr, &inaddr);
      if(host)
        {
          padico_topo_host_bind(host, padico_topo_network_getbyid("inet"), &inaddr);
        }
      else
        {
          host = padico_topo_host_resolve_byinaddr(&inaddr);
        }
    }
  const char*host_name = host ? padico_topo_host_getname(host) : padico_strdup(name);
  entity = puk_hashtable_lookup(nsdb.hosts_by_name, host_name);
  if(!entity)
    {
      entity = ns_entity_new(NS_ENTITY_HOST);
      entity->host.host = host;
      entity->host.name = host_name;
      if(entity->host.name)
        puk_hashtable_insert(nsdb.hosts_by_name, entity->host.name, entity);
    }
  return entity;
}

static inline ns_entity_t nsdb_create_group(const char*group_name)
{
  ns_entity_t entity = ns_entity_new(NS_ENTITY_GROUP);
  entity->group.name = padico_strdup(group_name);
  ns_entity_vect_init(&entity->group.entities);
  ns_entity_vect_init(&entity->group.leaves);
  puk_hashtable_insert(nsdb.groups_by_name, entity->group.name, entity);
  return entity;
}

static inline ns_rule_t nsdb_create_rule(const char*profile,
                                         puk_component_t assembly,
                                         puk_iface_t iface)
{
  ns_rule_t rule = ns_rule_new();
  static const char*const default_profile = PADICO_NS_DEFAULT_PROFILE;
  const char*const p = profile?:default_profile;
  struct ns_rule_bucket_s rulebucket =
    {
      .profile = p,
      .iface   = iface
    };
  ns_rule_list_t rl =
    puk_hashtable_lookup(nsdb.rulebuckets, &rulebucket);
  if(rl == NULL)
    {
      struct ns_rule_bucket_s*rb = padico_malloc(sizeof(struct ns_rule_bucket_s));
      rl = ns_rule_list_new();
      *rb = rulebucket;
      puk_hashtable_insert(nsdb.rulebuckets, rb, rl);
    }
  rule->assembly = assembly;
  ns_target_vect_init(&rule->targets);
  ns_rule_list_push_back(rl, rule);
  return rule;
}


/* ********************************************************* */
/* *** Parsing ********************************************* */


/* *** <NS:rule-set> */

static void ns_ruleset_start_handler (puk_parse_entity_t e)
{
  padico_out(40, "Start of parsing \n");
}
static void ns_ruleset_end_handler(puk_parse_entity_t e)
{
  padico_out(40, "End of parsing \n");
}
static struct puk_tag_action_s action_ruleset =
{
  .xml_tag        = "NS:rule-set",
  .start_handler  = &ns_ruleset_start_handler,
  .end_handler    = &ns_ruleset_end_handler,
  .required_level = PUK_TRUST_CONTROL
};

/* *** <NS:group> */

static void ns_basic_group_start_handler(puk_parse_entity_t e)
{
  const char*group_id  = puk_parse_getattr(e, "id");
  const char*group_ref = puk_parse_getattr(e, "ref");
  ns_entity_t group = NULL;
  assert((group_id != NULL) ^ (group_ref != NULL));
  if(group_id)
    {
      /* new group */
      group = nsdb_create_group(group_id);
      padico_out(40, "created cluster %s\n", group_id);
    }
  else
    {
      /* group reference. Register according to the container nature */
      group = puk_hashtable_lookup(nsdb.groups_by_name, group_ref);
      if(group == NULL)
        {
          padico_fatal("unknown group: %s\n", group_ref);
          puk_parse_set_rc(e, padico_rc_error("Unknown group: %s\n", group_ref));
          return;
        }
    }
  assert(group != NULL && group->kind == NS_ENTITY_GROUP);
  puk_parse_set_content(e, group);
}
static void ns_basic_group_end_handler(puk_parse_entity_t e)
{
  ns_entity_t group = puk_parse_get_content(e);
  puk_parse_entity_vect_t v = puk_parse_get_contained(e);
  puk_parse_entity_t*_i;
  assert(group != NULL);
  /* TODO- we don't check that only 'id' groups contain sub-entities */
  puk_vect_foreach(_i, puk_parse_entity, v)
    {
      /* add sub-entity to group */
      puk_parse_entity_t i = *_i;
      ns_entity_t sub_entity = puk_parse_get_content(i);
      ns_entity_vect_push_back(&group->group.entities, sub_entity);
    }
  nsdb_group_flatten(group);
}
static struct puk_tag_action_s action_cluster =
{
  .xml_tag        = "NS:group",
  .start_handler  = &ns_basic_group_start_handler,
  .end_handler    = &ns_basic_group_end_handler,
  .required_level = PUK_TRUST_CONTROL
};

/* *** <NS:node> */

static void ns_basic_node_start_handler(puk_parse_entity_t e)
{
  const char*name = puk_parse_getattr(e, "name");
  ns_entity_t node = nsdb_create_node(name);
  puk_parse_set_content(e, node);
  padico_out(40, "node=%s\n", name);
}
static struct puk_tag_action_s action_node =
{
  .xml_tag        = "NS:node",
  .start_handler  = &ns_basic_node_start_handler,
  .end_handler    = NULL,
  .required_level = PUK_TRUST_CONTROL
};

/* *** <NS:host> */

static void ns_basic_host_start_handler(puk_parse_entity_t e)
{
  const char*name = puk_parse_getattr(e, "name");
  const char*addr = puk_parse_getattr(e, "addr");
  ns_entity_t host = nsdb_create_host(name, addr);
  puk_parse_set_content(e, host);
  padico_out(40, "host=%s\n", name);
}
static struct puk_tag_action_s action_host =
{
  .xml_tag        = "NS:host",
  .start_handler  = &ns_basic_host_start_handler,
  .end_handler    = NULL,
  .required_level = PUK_TRUST_CONTROL
};

/* *** <NS:rule> */

static void ns_basic_rule_end_handler(puk_parse_entity_t e)
{
  int i;
  puk_parse_entity_vect_t v = puk_parse_get_contained(e);
  assert(puk_parse_entity_vect_size(v) > 1);
  puk_parse_entity_t as_entity = puk_parse_entity_vect_at(v, 0);
  puk_component_t component = puk_parse_get_content(as_entity);
  if(component)
    {
      if(!puk_parse_is(as_entity, "puk:component") && !puk_parse_is(as_entity, "puk:composite"))
        {
          padico_warning("syntax error- rule contains no component.\n");
          puk_parse_set_rc(e, padico_rc_error("Syntax error: rule contains no component.\n"));
          return;
        }
      for(i = 1; i < puk_parse_entity_vect_size(v); i++)
        {
          struct ns_target_s*target = puk_parse_get_content(puk_parse_entity_vect_at(v, i));
          int j;
          for(j = 0; j < puk_facet_vect_size(&component->provides); j++)
            {
              ns_rule_t rule = nsdb_create_rule(target->profile, component,
                                                puk_facet_vect_at(&component->provides, j).iface);
              padico_out(40, "register facet %s of component %s.\n",
                         (puk_facet_vect_at(&component->provides, j).iface)->name,
                         component->name);
              ns_target_vect_push_back(&rule->targets, *target);
            }
          padico_free(target);
        }
    }
  else
    {
      padico_warning("invalid rule (no component)- skipping.\n");
      puk_parse_set_rc(e, padico_rc_error("Syntax error: rule contains no component.\n"));
    }
}
static struct puk_tag_action_s action_rule =
{
  .xml_tag        = "NS:rule",
  .start_handler  = NULL,
  .end_handler    = &ns_basic_rule_end_handler,
  .required_level = PUK_TRUST_CONTROL
};


/* *** <NS:target> */

static void ns_basic_target_start_handler(puk_parse_entity_t e)
{
  struct ns_target_s*target = padico_malloc(sizeof(struct ns_target_s));
  const char*profile = puk_parse_getattr(e, "profile");
  const char*kind = puk_parse_getattr(e, "kind");
  if(strcmp(kind, "clique") == 0)
    {
      target->kind = NS_TARGET_CLIQUE;
      target->clique.entity = NULL;
    }
  else if(strcmp(kind, "arrow") == 0)
    {
      target->kind = NS_TARGET_ARROW;
      target->arrow.tail = NULL;
      target->arrow.head = NULL;
    }
  else if(strcmp(kind, "multipartite") == 0)
    {
      target->kind = NS_TARGET_MULTIPARTITE;
      target->multipartite.subsets = ns_entity_vect_new();
    }
  else if(strcmp(kind, "unknown") == 0)
    {
      target->kind = NS_TARGET_UNKNOWN;
      padico_warning("target kind=unknown not supported yet. Ignored.\n");
    }
  else
    padico_fatal("unknown target kind: *%s*\n", kind);
  target->profile = padico_strdup(profile?profile:PADICO_NS_DEFAULT_PROFILE);
  puk_parse_set_content(e, target);
  padico_out(40, "created target kind=%s\n", kind);
}
static void ns_basic_target_end_handler(puk_parse_entity_t e)
{
  struct ns_target_s*target = puk_parse_get_content(e);
  puk_parse_entity_vect_t v = puk_parse_get_contained(e);
  switch(target->kind)
    {
    case NS_TARGET_CLIQUE:
      if(puk_parse_entity_vect_size(v) != 1)
        {
          puk_parse_set_rc(e, padico_rc_error("NetSelector-basic: target kind='clique' requires only 1 entity.\n"));
          return;
        }
      target->clique.entity = puk_parse_get_content(puk_parse_entity_vect_at(v, 0));
      break;
    case NS_TARGET_MULTIPARTITE:
      {
        int i;
        ns_entity_vect_resize(target->multipartite.subsets, puk_parse_entity_vect_size(v));
        for(i = 0; i < puk_parse_entity_vect_size(v); i++)
          {
            ns_entity_t entity = puk_parse_get_content(puk_parse_entity_vect_at(v, i));
            ns_entity_vect_put(target->multipartite.subsets, entity, i);
          }
      }
      break;
    case NS_TARGET_ARROW:
      {
        if(puk_parse_entity_vect_size(v) != 2)
          padico_fatal("target kind='arrow' requires 2 entities.\n");
        target->arrow.tail = puk_parse_get_content(puk_parse_entity_vect_at(v, 0));
        target->arrow.head = puk_parse_get_content(puk_parse_entity_vect_at(v, 1));
      }
      break;
    case NS_TARGET_UNKNOWN:
      break;
    default:
      padico_fatal("bad target type in parsing.\n");
      break;
    }
}
static struct puk_tag_action_s action_target =
{
  .xml_tag        = "NS:target",
  .start_handler  = &ns_basic_target_start_handler,
  .end_handler    = &ns_basic_target_end_handler,
  .required_level = PUK_TRUST_CONTROL
};

/* *** Main parts ****************************************** */

int ns_basic_parse_init(void)
{
  puk_xml_add_action(action_ruleset);
  puk_xml_add_action(action_cluster);
  puk_xml_add_action(action_node);
  puk_xml_add_action(action_host);
  puk_xml_add_action(action_rule);
  puk_xml_add_action(action_target);
  return 0;
}

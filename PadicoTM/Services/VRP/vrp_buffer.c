/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief VRP buffers (frame) management
 * @ingroup VRP
 */

#include "VRP-internals.h"



/* ******************************************************************
 * **            VRP buffers
 * ******************************************************************
 */


vrp_buffer_t vrp_buffer_construct(char*data, int size)
{
  vrp_buffer_t buffer = vrp_malloc(sizeof(struct vrp_buffer_s));
  buffer->data = data;
  buffer->size = size;
  buffer->critical = NULL;
  buffer->max_loss_rate = -1;
  buffer->max_cons_loss = -1;
  return buffer;
}

void vrp_buffer_destroy(vrp_buffer_t buffer)
{
  struct vrp_critical_cell_s*cs;

  cs = buffer->critical;
  while(cs != NULL)
    {
      buffer->critical = cs->next;
      padico_free(cs);
      cs = buffer->critical;
    }
  buffer->critical = NULL;
  vrp_free(buffer);
}

void vrp_buffer_add_critical(vrp_buffer_t buffer, char*data, int size)
{
  struct vrp_critical_cell_s*cell;

  if((data < buffer->data) || (data > buffer->data + buffer->size))
    {
      vrp_warning("VRP: given critical interval outside data");
    }
  else
    {
      cell = (struct vrp_critical_cell_s*) padico_malloc(sizeof(struct vrp_critical_cell_s));
      cell->next = buffer->critical;
      buffer->critical = cell;
      cell->begin = data - buffer->data;
      cell->end   = cell->begin + size;
    }
}

void vrp_buffer_set_loss_rate(vrp_buffer_t buffer, int max, int cons)
{
  buffer->max_loss_rate = max * 256 / 100;
  buffer->max_cons_loss = cons;
}

/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 * Timing.c
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <unistd.h>

#include "Timing.h"
#include <Padico/Module.h>

static int padico_timing_init(void);

PADICO_MODULE_DECLARE(Timing, padico_timing_init, NULL, NULL);


static int padico_timing_init(void)
{
#if 0
  int i;
  /* correction to avoid the cost of the time measure itself */
  timing_resolution = (uint64_t)1 << 32;
  for(i=0; i<10; i++)
    {
      padico_timing_t t1, t2;
      int64_t diff;
      padico_timing_get_tick(&t1);
      padico_timing_get_tick(&t2);
      diff = PADICO_TIMING_TICK_RAW_DIFF(t1, t2);
      if(diff < timing_resolution)
        timing_resolution = diff;
    }

#ifdef USE_PENTIUM_TIMER
  /* calibrate cpu cycle */
  {
    padico_timing_t t1, t2;
    struct timeval tv1,tv2;
    int64_t diff_usec = 0;

    padico_timing_get_tick(&t1);
    gettimeofday(&tv1,0);
    while(diff_usec < 1000000) /* wait at least 0.1 second, even if usleep() is interrupted */
      {
        usleep(100000);
        padico_timing_get_tick(&t2);
        gettimeofday(&tv2,0);
        diff_usec = ((tv2.tv_sec*1e6 + tv2.tv_usec) - (tv1.tv_sec*1e6 + tv1.tv_usec));
      }
    scale = diff_usec / (double)(PADICO_TIMING_TICK_DIFF(t1, t2));
    padico_print("Pentium timing- clock at %G MHz (resolution=%lld cycles, %.2G nanosec).\n",
                 1/scale, (long long)timing_resolution, (timing_resolution*scale)*1000.0);
  }
#else
  padico_print("Use standard timer.\n");
#endif
#endif
  return 0;
}


/* *** Profiling ******************************************* */

padico_timing_profiler_t padico_timing_profiler_new(int num_points,
                                                    int num_iter,
                                                    int ignore,
                                                    const char*filename)
{
  struct padico_timing_profiler_s*p = padico_malloc(sizeof(struct padico_timing_profiler_s));
  p->num_points   = num_points;
  p->num_iter     = num_iter;
  p->current_iter = -ignore;
  p->points       = padico_calloc(num_points*num_iter, sizeof(padico_timing_t));
  p->filename     = filename;
  p->labels       = padico_calloc(num_points, sizeof(char*));
  return p;
}

void padico_timing_profile_point_label(padico_timing_profiler_t p, int n_point, const char*label)
{
  int offset = p->current_iter * p->num_points + n_point;
  if(n_point >= p->num_points)
    abort();
  if(p->current_iter < p->num_iter && p->current_iter >= 0)
    {
      padico_timing_get_tick(&p->points[offset]);
    }
  if(p->labels[n_point] == NULL)
    p->labels[n_point] = label;
}

void padico_timing_profile_flush(padico_timing_profiler_t p)
{
  padico_print("flush profiler %s\n", p->filename);
  /* flush profiling table in file */
  int i, j;
  FILE*f = stdout; /* fopen(p->filename, "w"); */
  double*mins = padico_calloc(p->num_points, sizeof(double));
  double*maxs = padico_calloc(p->num_points, sizeof(double));
  double*accu = padico_calloc(p->num_points, sizeof(double));
  for(i = 0; i < p->num_iter ; i++)
    {
      padico_timing_t*t_orig = &p->points[i * p->num_points];
      fprintf(f, "%d: ", i);
      for(j = 1; j < p->num_points; j++)
        {
          double diff = -1;
          padico_timing_t*t_now = &p->points[i * p->num_points + j];
          diff = padico_timing_diff_usec(t_orig, t_now);
          fprintf(f, "%5.2f ", diff);
          /* compute mins */
          if(i == 0 || diff < mins[j])
            mins[j] = diff;
          /* compute maxs */
          if(i == 0 || diff > maxs[j])
            maxs[j] = diff;
          /* compute average */
          accu[j] += diff;
        }
      fprintf(f, "\n");
    }
  for(j = 1; j < p->num_points; j++)
    {
      fprintf(f, "[%3d] min=%7.2f; max=%7.2f; avg=%7.2f; %s\n",
              j, mins[j], maxs[j], (accu[j] / p->num_iter), p->labels[j]?:"-");
    }
  padico_free(mins);
  padico_free(maxs);
  padico_free(accu);
  /* fclose(f); */
}

void padico_timing_profile_start(padico_timing_profiler_t p)
{
  p->current_iter++;
#ifdef PADICO_TIMING_DO_FLUSH
  if(p->current_iter == p->num_iter)
    {
      padico_timing_profile_flush(p);
    }
#endif /* PADICO_TIMING_DO_FLUSH */
  padico_timing_profile_point(p, 0);
}

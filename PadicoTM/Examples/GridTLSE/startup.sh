#! /bin/bash 

PORT=8080

ENABLE_PADICO=yes

config_padico() {
    if [ "x${ENABLE_PADICO}" = "xyes" ]; then
        PADICO_OPTION="-i${PWD}/topo-kvm.xml -i${PWD}/topo-kvm2.xml -i${PWD}/topo-pointvirgule.xml -i${PWD}/topo-william0.xml -iload-Resolve -v"
	#PADICO_OPTION="-c -p -DNS_BASIC_CONF=${HOME}/controlssh.xml"
	BOOTSTRAP_PORT=23456
	export PADICO_LAUNCH="padico-launch ${PADICO_OPTION} -DPADICO_BOOTSTRAP_PORT=${BOOTSTRAP_PORT} -DPADICO_BOOTSTRAP_HOST=${ALTO_HOST}"
	export PADICO_LAUNCH_RDV="padico-launch ${PADICO_OPTION} -DPADICO_BOOTSTRAP_LISTEN=${BOOTSTRAP_PORT}"
	PORT=8181
    fi
}

# ## variables used in this script
export TOMCAT_ROOT=/opt/jakarta-tomcat-5.0.28
export BASSO_ROOT=/home/gridtlse/TLSE_BASSO
export ALTO_ROOT=/home/gridtlse/TLSE_ALTO

# ## variables used by sub-programs
export TLSE_INSTALL=${HOME}/TLSE_BASSO
export TLSE_SERVICE_PATH=${TLSE_INSTALL}/service
export TLSE_SOLVER_PATH=${TLSE_INSTALL}/solver
export TLSE_CLIENT_PATH=${TLSE_INSTALL}/client
export TLSE_DAGDA_PATH=${TLSE_INSTALL}/dagda/
export PATH=${TLSE_CLIENT_PATH}/bin:${TLSE_SERVICE_PATH}/bin:${PATH}
export OMNIORB_CONFIG=${HOME}/omniORB.conf


start_omninames() {
    omninames_logdir="/tmp/omniNames_logdir_${RANDOM}"
    mkdir ${omninames_logdir}
    ${PADICO_LAUNCH} omniNames -start -always -logdir ${omninames_logdir} &
}

start_proxy() {
    if [ "x${ENABLE_PADICO}" = "xyes" ]; then
        ${PADICO_LAUNCH_RDV} /usr/sbin/haproxy -f $HOME/config/haproxy_tlse.cfg &
    fi
}

config_omniorb() {
    cat > ${OMNIORB_CONFIG} <<EOF
InitRef = NameService=corbaname::${ALTO_HOST}
giopMaxMsgSize = 33554432
EOF
}

start_BASSO() {
    echo "# start BASSO gridtlse side- ALTO host is ${ALTO_HOST}"
#    ${PADICO_LAUNCH} ${BASSO_ROOT}/service/bin/DIET_TLSE_SeD ${BASSO_ROOT}/service/config/TLSE_SeD.cfg ${ALTO_HOST} &
#    ${PADICO_LAUNCH} ${BASSO_ROOT}/service/bin/DAGDA_TLSE_SeD ${BASSO_ROOT}/service/config/TLSE_SeD.cfg &
    cd ~/gridtlse-basso-ng.uc1/bin
 #   ./tlse_clean
 #   ./tlse_launchMA
    ./tlse_launchSeD
}

start_ALTO() {
    echo "start ALTO gridtlse side on ${ALTO_HOST}"
    mkdir -p /tmp/TLSE/log4j
    mkdir -p /tmp/tlse/supp_file
    psql -d tlse_db -c "update computer set full_name='${ALTO_HOST}' where web_server=1;"
    cat ${TOMCAT_ROOT}/webapps/weaver/WEB-INF/classes/tlse.properties.in | sed s/@GRIDTLSE8080@/${ALTO_HOST}:${PORT}/ > ${TOMCAT_ROOT}/webapps/weaver/WEB-INF/classes/tlse.properties
    cat ${TOMCAT_ROOT}/webapps/weaver/grammar.xml.in | sed s/@GRIDTLSE8080@/${ALTO_HOST}:${PORT}/  > ${TOMCAT_ROOT}/webapps/weaver/grammar.xml
    cat ${TOMCAT_ROOT}/webapps/websolve/WEB-INF/classes/tlse.properties.in | sed s/@GRIDTLSE8080@localhost:8080/${ALTO_HOST}:${PORT}/ >  ${TOMCAT_ROOT}/webapps/websolve/WEB-INF/classes/tlse.properties
    cat ${ALTO_ROOT}/weaver/config/weaver.xml.in | sed s/@GRIDTLSE8080@/${ALTO_HOST}:${PORT}/ > ${ALTO_ROOT}/weaver/config/weaver.xml
    ${TOMCAT_ROOT}/bin/startup.sh
    ${ALTO_ROOT}/start_tlse_servers.sh &
#    ${PADICO_LAUNCH} dietAgent ${TLSE_SERVICE_PATH}/config/MA_TLSE.cfg >& ${TLSE_SERVICE_PATH}/log/MA_TLSE.trace & 
}


if [ "x$1" = "xBASSO" ] && [ "x$2" != "x" ]
then
    export ALTO_HOST=$2
    config_padico
    export DEPLOYMENT="http://${ALTO_HOST}:${PORT}/websolve/Services?type=xml&object=deployment"
    config_omniorb
    start_BASSO
elif [ "x$1" = "xALTO" ] 
then
    export ALTO_HOST=$(hostname)
    config_padico
    export DEPLOYMENT="http://${ALTO_HOST}:${PORT}/websolve/Services?type=xml&object=deployment"
    start_proxy
#    start_omninames
    config_omniorb
    start_ALTO

    cd ~/gridtlse-basso-ng.uc1/bin
    ./tlse_clean
    ./tlse_launchMA
    
fi
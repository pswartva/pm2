/*
 * NewMadeleine
 * Copyright (C) 2021-2022 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

/* Example to see when each node receives data (just received by nmad and
 * completely released to the application). It is usefull to see if priorities
 * are correctly respected. */

#include <stdlib.h>
#include <stdio.h>

#include <nm_launcher_interface.h>
#include <nm_coll_interface.h>
#include <nm_mcast_interface.h>
#include <nm_sync_clocks_interface.h>

#define DEFAULT_ROUNDS 200
#define DEFAULT_SIZE (10 * 1024)

static void usage(void)
{
  fprintf(stderr, "-s array size - number of bytes to broadcast [%d]\n", DEFAULT_SIZE);
  fprintf(stderr, "-r rounds - number of repetitions of the benchmark [%d]\n", DEFAULT_ROUNDS);
}

int main(int argc, char**argv)
{
  int worldsize = 0;
  int rank = -1;
  int i;
  nm_len_t msg_size = DEFAULT_SIZE;
  int rounds = DEFAULT_ROUNDS;
  const nm_tag_t tag = 0x02;
  const int hlen = 0;

  for(i = 1; i < argc; i++)
    {
      if(strcmp(argv[i], "-s") == 0)
        {
          msg_size = (nm_len_t) atoi(argv[++i]);
          continue;
        }
      if(strcmp(argv[i], "-r") == 0)
        {
          rounds = atoi(argv[++i]);
          continue;
        }
      else
        {
          fprintf(stderr, "%s: illegal argument %s\n", argv[0], argv[i]);
          usage();
          exit(1);
        }
    }

  /* generic nmad init */
  nm_launcher_init(&argc, argv);
  nm_launcher_get_size(&worldsize);
  nm_launcher_get_rank(&rank);

  if(worldsize == 1)
    {
      fprintf(stderr, "We need several nodes.\n");

      nm_launcher_exit();
      return EXIT_FAILURE;
    }

  nm_comm_t p_comm = nm_comm_world("nm_mcast_hello");
  nm_mcast_service_t mcast_service = nm_mcast_init(p_comm);

  nm_sync_clocks_t p_clocks = nm_sync_clocks_init(p_comm);

  if (rank == 0)
    {
      /* send mcast */
      char*msg = malloc(msg_size);
      memset(msg, 0, msg_size);
      const int n_dests = worldsize - 1;
      int*p_dests = malloc(sizeof(int) * n_dests);
      int*p_prios = malloc(sizeof(int) * n_dests);
      double total = 0;
      for(i = 0; i < n_dests; i++)
        {
          p_dests[i] = i + 1;
          p_prios[i] = i;
        }
      struct nm_data_s data;
      nm_data_contiguous_build(&data, (char*)msg, msg_size);
      for(i = 0; i < rounds; i++)
        {
          nm_sync_clocks_barrier(p_clocks, NULL);
          const double t_begin = nm_sync_clocks_get_time_usec(p_clocks);
          nm_mcast_send(mcast_service, p_comm, p_dests, p_prios, n_dests, tag, &data, hlen, NM_COLL_TREE_DEFAULT);
          const double t_end = nm_sync_clocks_get_time_usec(p_clocks);
          total += t_end - t_begin;
        }
      total /= rounds;
      double*totals_finalized = malloc(sizeof(double) * worldsize);
      double*totals_data = malloc(sizeof(double) * worldsize);
      nm_coll_gather(p_comm, 0 /* root */, &total, sizeof(total), totals_finalized, sizeof(double), 0xFF);
      nm_coll_gather(p_comm, 0 /* root */, &total, sizeof(total), totals_data, sizeof(double), 0xFF);
      printf("# msg size = %d B\n", (int)msg_size);
      printf("# delay on root node = %g usec.\n", total);
      printf("# node ; prio ; delay data (usec.); finalized (usec.)\n");
      for(i = 1; i < worldsize; i++)
        {
          printf("%d \t %d \t %g \t %g\n",
                 i, p_prios[i - 1], totals_data[i], totals_finalized[i]);
        }
      free(totals_finalized);
      free(totals_data);
      free(p_dests);
      free(p_prios);
      free(msg);
    }
  else
    {
      /* receive data from an mcast with a point-to-point receive */
      nm_session_t p_session = nm_comm_get_session(p_comm);
      char*buffer = malloc(msg_size);
      double total_finalized = 0.0, total_data = 0.0;
      for(i = 0; i < rounds; i++)
        {
          nm_sync_clocks_barrier(p_clocks, NULL);
          nm_sr_request_t request;
          const double t_begin = nm_sync_clocks_get_time_usec(p_clocks);
          nm_sr_irecv(p_session, 0, tag, buffer, msg_size, &request);
          nm_sr_rwait_data(p_session, &request);
          const double t_middle = nm_sync_clocks_get_time_usec(p_clocks);
          nm_sr_rwait(p_session, &request);
          const double t_end = nm_sync_clocks_get_time_usec(p_clocks);
          total_finalized += t_end - t_begin;
          total_data += t_middle - t_begin;
        }
      total_finalized /= rounds;
      total_data /= rounds;
      nm_coll_gather(p_comm, 0 /* root */, &total_finalized, sizeof(total_finalized), NULL, 0, 0xFF);
      nm_coll_gather(p_comm, 0 /* root */, &total_data, sizeof(total_data), NULL, 0, 0xFF);
      free(buffer);
    }

  nm_sync_clocks_shutdown(p_clocks);

  /* generic nmad termination */
  nm_coll_barrier(p_comm, 0xF2);
  nm_mcast_finalize(mcast_service);
  nm_comm_destroy(p_comm);
  nm_launcher_exit();
  return EXIT_SUCCESS;
}

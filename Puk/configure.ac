dnl -*- mode: Autoconf;-*-

dnl ----- configure.ac for Puk -----

AC_PREREQ(2.60)

AC_INIT(Puk)
AC_CONFIG_SRCDIR([include/Puk.h])
AC_CONFIG_HEADERS([puk_config.h Puk-config.h])

m4_include(pkg.m4)
m4_include(misc.m4)
m4_include([compiler.m4])
m4_include([tools.m4])
m4_include([features.m4])
m4_include([out.m4])
m4_include([./puk.m4])

dnl --- Directories and version
dnl --------------------------------------------------------

AC_PADICO_PACKAGE([Puk])
AC_PADICO_BUILDDIR
AC_PADICO_SRCDIR
AC_PUK_ROOT
AC_PADICO_VERSION([PUK])
puk_root=${prefix}
AC_SUBST([puk_root])

dnl --- Base flags
dnl --------------------------------------------------------

AC_PADICO_OPTIMIZE
AC_PADICO_DEBUG
AC_PADICO_TRACE
AC_PADICO_STATIC
AC_PADICO_DYNAMIC
AC_PADICO_BUILTIN
if test x${padico_builtin_mods} = xyes; then
  AC_DEFINE(PUK_BUILTIN_MODS, 1, [build all modules as builtins])
fi

dnl --- Compiler and base tools
dnl --------------------------------------------------------

AC_PADICO_BASE_TOOLS
AC_PADICO_GNU_MAKE
AC_PADICO_COMPILER
AC_PADICO_CDEP
AC_PADICO_CXX
AC_PADICO_CXXDEP
AC_PADICO_LINKER
AC_PADICO_DYNLD
AC_PADICO_DSO
AC_PADICO_ASAN
AC_PADICO_FORTIFY
AC_PADICO_EXPAT
AC_PADICO_CHECK_GDB

AC_PUK_NOSTDLIB
AC_PUK_ATOSO
AC_PUK_GNU_TM
AC_PUK_RTM
AC_PUK_PROFILE
AC_PUK_LFQUEUES

dnl --- check features
dnl --------------------------------------------------------

AC_PADICO_TYPES
AC_PADICO_LIBS
AC_PUK_FEATURES

dnl -- multiple instances
AC_PUK_MULT

dnl -- simgrid
AC_PADICO_CHECK_SIMGRID
AC_PADICO_SIMGRID
AC_MSG_CHECKING([whether we are building for simgrid])
if test x${padico_enable_simgrid} = xyes; then
  if test x${puk_enable_mult} != xyes; then
    AC_MSG_ERROR([some tools are missing to enable simgrid mode; need: dladdr, objdump, patchelf >= 0.18 ])
  fi
  dnl flags to build Puk
  CPPFLAGS="${CPPFLAGS} ${simgrid_CFLAGS}"
  LIBS="${LIBS} ${simgrid_LIBS}"
  dnl flags for applications
  PUK_CFLAGS="${PUK_CFLAGS} ${simgrid_CFLAGS} -fPIC"
  PUK_LIBS="${PUK_LIBS} ${simgrid_LIBS}"
  AC_DEFINE([PUK_SIMGRID], [1], [whether Puk is built for simgrid])
  AC_MSG_RESULT([yes])
else
  AC_MSG_RESULT([no])
fi

PUK_LIBS="-L${prefix}/lib -lPadicoPuk ${PUK_LIBS}"
if test x${padico_enable_dynamic} = xyes; then
  PUK_LIBS="-Wl,-rpath,${prefix}/lib ${PUK_LIBS}"
fi
LIBS="${LIBS} ${STATIC_LIBS}"

dnl --- output
dnl --------------------------------------------------------

PUK_CFLAGS="-I${prefix}/include -DPADICO ${PUK_CFLAGS} ${EXPORT_CFLAGS}"
PUK_LIBS="${PUK_LIBS} ${EXPORT_LIBS}"

AC_SUBST(PUK_CFLAGS)
AC_SUBST(PUK_LIBS)

AC_PADICO_OUT_COMMON_MK
AC_PUK_OUT
AC_PADICO_OUT_ROOT
AC_PADICO_OUTPUT

/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <nm_private.h>

PADICO_MODULE_HOOK(nmad);

/** state for nm_tactic_pack_rdv_repack_chunk */
struct nm_tactic_rdv_repack_s
{
  struct nm_req_chunk_s*p_req_chunk_orig;
  nm_len_t offset;
};

/** repack each chunk of a large message separately */
static void nm_tactic_pack_rdv_repack_chunk(void*ptr __attribute__((unused)), nm_len_t len, void*_context)
{
  struct nm_tactic_rdv_repack_s*p_repack = _context;
  struct nm_req_chunk_s*p_req_chunk_orig = p_repack->p_req_chunk_orig;
  struct nm_req_s*p_pack = p_req_chunk_orig->p_req;
  struct nm_core*p_core = p_pack->p_gate->p_core;
  struct nm_req_chunk_s*p_req_chunk = nm_req_chunk_alloc(p_core);
  nm_req_chunk_init(p_req_chunk, p_pack, p_repack->offset, len);
  nm_strat_submit_req_chunk(p_core, p_pack->p_gate, p_req_chunk, 1);
  p_repack->offset += len;
}

/* TODO- switch p_drv -> p_trk */

int nm_tactic_pack_rdv(nm_gate_t p_gate, nm_drv_t p_drv,
                       struct nm_req_chunk_list_s*p_req_chunk_list,
                       struct nm_req_chunk_s*p_req_chunk,
                       struct nm_pkt_wrap_s*p_pw)
{
  if(sizeof(struct nm_header_ctrl_rdv_s) < nm_pw_remaining_buf(p_pw))
    {
      if(p_req_chunk_list)
        nm_req_chunk_list_remove(p_req_chunk_list, p_req_chunk);
      struct nm_req_s*p_pack = p_req_chunk->p_req;
      const struct nm_data_properties_s*p_props = &p_req_chunk->chunk_props;
      const nm_len_t density = p_props->size / p_props->blocks;
      const int is_lastchunk = (p_req_chunk->chunk_offset + p_req_chunk->chunk_len == p_pack->pack.len);
      nm_pw_flag_t flags = NM_PW_FLAG_NONE;
      if( (!p_props->is_contig) &&
          (!p_drv->props.capabilities.supports_data) &&
          ( (!p_drv->props.capabilities.supports_iovec) || ((p_drv->props.capabilities.max_iovecs > 0) && (p_drv->props.capabilities.max_iovecs < p_props->blocks))))
        {
          /* chunk is not contiguous & driver does not support data iterators nor iovecs */
          if(density < NM_LARGE_MIN_DENSITY)
            {
              /* low density -> copy */
              flags |= NM_REQ_CHUNK_FLAG_USE_COPY;
            }
          else
            {
              /* high density (= large chunks) -> repack as multiple chunks */
              struct nm_tactic_rdv_repack_s repack =
                {
                  .p_req_chunk_orig = p_req_chunk,
                  .offset = p_req_chunk->chunk_offset
                };
              nm_data_chunk_extractor_traversal(&p_pack->data, p_req_chunk->chunk_offset, p_req_chunk->chunk_len,
                                                &nm_tactic_pack_rdv_repack_chunk, &repack);
              return -NM_EAGAIN;
            }
        }
      struct nm_pkt_wrap_s*p_large_pw = nm_pw_alloc_noheader(p_gate->p_core);
      p_large_pw->p_gate = p_gate;
      nm_pw_add_req_chunk(p_large_pw, p_req_chunk, flags);
      struct nm_trk_s*p_trk = &p_gate->trks[NM_TRK_LARGE];
      assert(p_drv == p_trk->p_drv);
      struct nm_core*p_core = p_gate->p_core;
      if( p_core->enable_send_prefetch &&
          (p_gate->n_trks == 2) &&
          (p_gate->trks[NM_TRK_LARGE].p_drv->props.capabilities.supports_send_prefetch) &&
          (p_props->blocks <= NM_PREALLOC_IOV_LEN) &&
          ( (p_drv->props.capabilities.supports_iovec && ((p_drv->props.capabilities.max_iovecs == 0) || (p_drv->props.capabilities.max_iovecs > p_props->blocks)))
            || p_props->is_contig ) &&
          (!(p_large_pw->flags & NM_PW_DATA_COPY)) &&
          (p_props->size < p_drv->props.capabilities.max_msg_size) )
        {
          /* mark pw for prefetching (but prefetch will be done _after_ rdv is sent) */
          p_large_pw->flags |= NM_PW_PREFETCHING;
          nm_pkt_wrap_list_push_back(&p_gate->p_core->prefetch_large_send, p_large_pw);
        }
      else
        {
          nm_pkt_wrap_list_push_back(&p_pack->p_gate->pending_large_send, p_large_pw);
        }
      nm_trace_event(NM_TRACE_EVENT_PW_SEND_LARGE_PENDING, p_pw, p_pw->p_gate, NULL, p_pack->tag, p_pack->seq, p_pw->length);
      union nm_header_ctrl_generic_s rdv;
      nm_header_init_rdv(&rdv, p_pack, p_req_chunk->chunk_len, p_req_chunk->chunk_offset, is_lastchunk ? NM_PROTO_FLAG_LASTCHUNK : 0);
      nm_pw_add_control(p_pw, &rdv);
      nm_trace_event(NM_TRACE_EVENT_CORE_PACK_RDV, p_pack, p_pw->p_gate, NULL, p_pack->tag, p_pack->seq, p_pack->pack.len);
      return NM_ESUCCESS;
    }
  else
    {
      return -NM_EBUSY;
    }
}


/** Packs a series of RTR for multiple chunks of a pw, and post corresponding recv
*/
void nm_tactic_rtr_pack(struct nm_core*p_core, struct nm_pkt_wrap_s*p_pw, int nb_chunks, const struct nm_rdv_chunk_s*p_chunks)
{
  int i;
  nm_len_t chunk_offset = p_pw->chunk_offset;
  const nm_seq_t seq = p_pw->p_unpack->seq;
  const nm_core_tag_t tag = p_pw->p_unpack->tag;
  nm_gate_t p_gate = p_pw->p_gate;
  for(i = 0; i < nb_chunks; i++)
    {
      struct nm_pkt_wrap_s*p_pw2 = NULL;
      if(p_chunks[i].len < p_pw->length)
        {
          /* create a new pw with the remaining data */
          nm_pw_split(p_core, p_pw, &p_pw2, p_chunks[i].len);
          assert(p_pw->length == p_chunks[i].len);
        }
      nm_trace_event(NM_TRACE_EVENT_PW_RECV_LARGE, p_pw, p_pw->p_gate, p_pw->p_drv, tag, seq, p_pw->length);
      if(p_gate->trks[p_chunks[i].trk_id].kind == nm_trk_large)
        {
          /* large trk */
          nm_core_post_recv(p_pw, p_pw->p_gate, p_chunks[i].trk_id, NULL);
          /* send of RTR is postponed after PW is actually posted */
          assert(p_pw->chunk_offset == chunk_offset);
          assert(p_pw->length == p_chunks[i].len);
        }
      else
        {
          /* small track- immediate RTR */
          nm_pw_free(p_core, p_pw);
          nm_trace_event(NM_TRACE_EVENT_CORE_UNPACK_RTR, p_pw->p_unpack, p_pw->p_gate, p_pw->p_drv, tag, seq, p_pw->p_unpack->unpack.expected_len);
          nm_core_post_rtr(p_gate, tag, seq, p_chunks[i].trk_id, chunk_offset, p_chunks[i].len, NULL);
        }
      chunk_offset += p_chunks[i].len;
      p_pw = p_pw2;
      p_pw2 = NULL;
    }
}

/** Accept a rdv for the given large pw: split if needed, pack RTR, post recv, and store remaining part
 */
void nm_tactic_rdv_accept(struct nm_core*p_core, struct nm_pkt_wrap_s*p_pw)
{
  nm_gate_t p_gate = p_pw->p_gate;
  nm_core_lock_assert(p_gate->p_core);
  struct nm_trk_s*p_trk_large = &p_gate->trks[NM_TRK_LARGE];
  struct nm_trk_s*p_trk_small = &p_gate->trks[NM_TRK_SMALL];
  assert(p_trk_large->p_pw_recv == NULL);
  const nm_len_t max_msg_size = p_trk_large->p_drv->props.capabilities.max_msg_size;
  const struct nm_data_properties_s*p_props = &p_pw->data_props;
  const nm_len_t density = (p_props->blocks > 0) ? p_props->size / p_props->blocks : p_props->size; /* average block size */
  const nm_len_t max_small = nm_drv_max_small(p_trk_small->p_drv);
  if(p_pw->length > max_msg_size)
    {
      /* split pw larger than allowed by driver */
      struct nm_pkt_wrap_s*p_pw2 = NULL;
      nm_pw_split(p_core, p_pw, &p_pw2, max_msg_size);
      nm_pkt_wrap_list_push_front(&p_gate->pending_large_recv, p_pw2);
      assert(p_pw->length <= max_msg_size);
    }
  assert(p_pw->length > 0);
  if(p_pw->length < NM_LARGE_MIN_DENSITY)
    {
      /* small chunk in a large packet- send on trk#0 */
      nm_profile_inc(p_core->profiling.n_rdvs_accept_small);
      struct nm_rdv_chunk_s chunk =
        { .len = p_pw->length, .trk_id = NM_TRK_SMALL };
      nm_tactic_rtr_pack(p_gate->p_core, p_pw, 1, &chunk);
    }
  else if( (p_props->is_contig == 1) ||
           p_trk_large->p_drv->props.capabilities.supports_data ||
           ( (p_trk_large->p_drv->props.capabilities.supports_iovec && ((p_trk_large->p_drv->props.capabilities.max_iovecs < 0) || (p_trk_large->p_drv->props.capabilities.max_iovecs > p_props->blocks))) ) ||
           (density < NM_LARGE_MIN_DENSITY))
    {
      /* case for single chunk-
       *   single block (contiguous)
       *   driver supports data natively -> ok for any pw ||
       *   driver supports iovec -> ok if iovec fits driver capabilities ||
       *   low-density -> send all & copy */
      nm_profile_inc(p_core->profiling.n_rdvs_accept_single);
      struct nm_rdv_chunk_s chunk =
        { .len = p_pw->length, .trk_id = NM_TRK_LARGE };
      nm_tactic_rtr_pack(p_gate->p_core, p_pw, 1, &chunk);
    }
  else
    {
      /* other cases: one rdv per chunk; extract first chunk */
      assert(p_props->blocks > 1);
      struct nm_data_generator_s generator;
      nm_data_generator_init(p_pw->p_data, &generator);
      const struct nm_data_chunk_s first_chunk = nm_data_generator_next(&generator);
      nm_data_generator_destroy(&generator);
      struct nm_pkt_wrap_s*p_pw2 = NULL;
      nm_pw_split(p_core, p_pw, &p_pw2, first_chunk.len);
      nm_pkt_wrap_list_push_front(&p_gate->pending_large_recv, p_pw2);
      nm_profile_inc(p_core->profiling.n_rdvs_accept_split);
      struct nm_rdv_chunk_s chunk =
        { .len = first_chunk.len, .trk_id = (first_chunk.len < max_small) ? NM_TRK_SMALL : NM_TRK_LARGE };
      nm_tactic_rtr_pack(p_gate->p_core, p_pw, 1, &chunk);
    }
}

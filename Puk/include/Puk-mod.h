/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Puk module management
 */

/** @defgroup PukMod API: Puk modules -- module management
 * @ingroup Puk
 */

  /** @addtogroup PukMod
   * @{
   */

  typedef int  (*puk_init_fun_t)(void);
  typedef int  (*puk_run_fun_t)(int, char**);
  typedef void (*puk_finalize_fun_t)(void);
  struct puk_mod_funcs_s
  {
    puk_init_fun_t     init;
    puk_run_fun_t      run;
    puk_finalize_fun_t finalize;
  };
  PUK_IFACE_TYPE(PadicoModule, struct puk_mod_funcs_s);

  typedef const char*padico_modID_t;
  PUK_VECT_TYPE(padico_modID, padico_modID_t)

  struct puk_unit_s
  {
    char* name;            /**< name of the unit as in the xml description file */
    puk_mod_t mod;         /**< the owner mod of this unit */
    void* driver_specific; /**< info-block for driver-dependant use */
  };
  typedef struct puk_unit_s* puk_unit_t;

  typedef void (*puk_notifier_t)(void*);
  /** a Puk job descriptor */
  struct puk_job_s
  {
    int            argc;       /**< argument count */
    char**         argv;       /**< arguments value */
    puk_notifier_t notify;     /**< notifier called at the end of the job */
    void*          notify_key; /**< key given to the notifier */
    padico_rc_t    rc;         /**< return code of the job */
    void*          driver_specific;
  };
  typedef struct puk_job_s*puk_job_t;

  /** A module driver */
  struct padico_loader_s
  {
    const char*name;                               /**< name for the driver */
    padico_rc_t (*load)   (puk_unit_t);            /**< function able to load one unit */
    padico_rc_t (*start)  (puk_unit_t, puk_job_t); /**< function to start a new job */
    padico_rc_t (*unload) (puk_unit_t);            /**< function to unload one unit */
  };
  PUK_IFACE_TYPE(PadicoLoader, struct padico_loader_s)


  padico_rc_t  padico_puk_mod_open     (puk_mod_t*, const char*mod_name);
  padico_rc_t  padico_puk_mod_open_file(puk_mod_t*mod, const char*file_name);
  padico_rc_t  padico_puk_mod_resolve  (puk_mod_t*, const char*);
  padico_rc_t  padico_puk_mod_load     (puk_mod_t);
  padico_rc_t  padico_puk_mod_start    (puk_mod_t, puk_job_t job);
  padico_rc_t  padico_puk_mod_unload   (puk_mod_t);
  padico_modID_vect_t puk_mod_getmodIDs(void);
  /** @todo padico_puk_driver_remove() maybe? :-) */

  void            puk_job_notify(puk_job_t);

  padico_modID_t  puk_mod_getname(puk_mod_t mod);
  void            puk_mod_set_ancestor(puk_mod_t mod, puk_mod_t ancestor);
  puk_mod_t       puk_mod_getbyname(const char*mod_name);
  void            puk_mod_setattr(puk_mod_t, const char*label, const char*value);
  const char*     puk_mod_getattr(puk_mod_t, const char*label);
  const char*     puk_mod_local_getattr(puk_mod_t mod, const char*label);
  const char*     puk_mod_global_getattr(puk_mod_t mod, const char*label);
void puk_mod_lock_use(puk_mod_t mod);
void puk_mod_unlock_use(puk_mod_t mod);
void puk_mod_add_dep(puk_mod_t mod, puk_mod_t dep);

  /** @} */

/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file sh-driver.c
 * @ingroup ShDriver
 */

/** @defgroup ShDriver Service: Sh-driver -- Bourne shell scripts support in PadicoTM
 * @author Christian Perez
 */

#include <Padico/Puk.h>
#include <Padico/PM2.h>
#include <Padico/PadicoTM.h>
#include <Padico/Module.h>

#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/wait.h>

static int padico_sh_driver_init(void);

PADICO_MODULE_DECLARE(Sh_driver, padico_sh_driver_init, NULL, NULL);

static padico_rc_t sh_unit_load  (puk_unit_t unit);
static padico_rc_t sh_unit_start (puk_unit_t unit, puk_job_t job);
static padico_rc_t sh_unit_unload(puk_unit_t unit);
static struct padico_loader_s sh_class_driver =
{
  .name   = "bin/sh",
  .load   = &sh_unit_load,
  .start  = &sh_unit_start,
  .unload = &sh_unit_unload,
};

/* ********************************************************** */

typedef struct wait_args_s {
  puk_job_t job;
  int pid;
  int sync;
  marcel_t tid;
} wait_args_t;

/* ********************************************************** */

static int padico_sh_driver_init(void)
{
  puk_component_declare("PadicoLoader-sh",
                        puk_component_provides("PadicoLoader", "loader", &sh_class_driver));
  return 0;
}

/* ********************************************************** */
/* *** DRIVER: CLASS ********************************* */

static padico_rc_t sh_unit_load(puk_unit_t unit)
{
  padico_trace("bin/sh: load handler: mod=%s unit %s.\n", puk_mod_getname(unit->mod), unit->name);

  unit->driver_specific = NULL;

  padico_trace("bin/sh: load handler: calling init with mod=%p\n", unit->mod);

  return padico_rc_ok();
}

static void* waiting_child(void *arg) {
  wait_args_t* pwa = (wait_args_t*) (arg);
  int pid = pwa->pid;
  puk_job_t job;
  int status;
  int myerrno = 0;
  int wp=0;
  do {
    padico_tm_sleep(1);
    puk_spinlock_acquire();
    wp = waitpid(pid, &status, WNOHANG);
    puk_spinlock_release();
    switch (wp) {
    case -1:
      if (errno != EINTR) {
        padico_warning("bin/sh pb with process of pid %d: errno = %d\n", pid, errno);
        wp=0; // EXITING the DO...WHILE loop
      } else {
        padico_warning("bin/sh got EINTR in waitpid -- ignoring\n");
      }
      myerrno=errno;
      break;
    case 0: padico_trace("bin/sh waiting for child terminaison\n");
      break;
    default:
      if (WIFEXITED(status)) {
        // ok
        padico_trace("bin/sh child %d terminates with value %d\n",wp, WEXITSTATUS(status));
      } else {
        // smthing goes wrong
        if (WIFSIGNALED(status))
          padico_warning("bin/sh child %d SIGNALED with signal %d\n",wp, WTERMSIG(status));
      if (WIFSTOPPED(status))
          padico_warning("bin/sh child %d STOPPED by signal %d\n",wp, WSTOPSIG(status));
      }
    }
  } while(!wp);

  padico_trace("bin/sh run exiting...\n");

  if (pwa->sync) {
    padico_trace("bin/sh notifying of job terminaison (SYNC)\n");
    job = pwa->job;
    job->rc = (wp!=-1?padico_rc_ok():padico_rc_error("errno = %d", myerrno));
    puk_job_notify(job);
  }

  padico_free(pwa);
  return (void*) 0;
}


static padico_rc_t sh_unit_start(puk_unit_t unit, puk_job_t job)
{

  int pid;
  const char* dir;
  const char* smode;
  padico_string_t path = padico_string_new();

  padico_rc_t ret = padico_rc_ok();

  // Data for the child
  int i;
  char **largv = (char**) padico_malloc(sizeof(char*)*(job->argc+1));
  padico_string_t cmd = padico_string_new();
  padico_string_t tcmd = padico_string_new();
  // End data for the child

  padico_trace("bin/sh: run handler: unit=%s argc=%d\n", unit->name, job->argc);

  dir = puk_mod_local_getattr(unit->mod, "Dir");
  smode = puk_mod_local_getattr(unit->mod, "Sync");

  {
    char buf[1024];
    if (getcwd(buf, 1024)!=NULL)
      padico_print("bin/sh: CURRENT DIR IS %s\n",buf);
  }

  padico_trace("bin/sh: run handler: unit %s sync=%s (%p)\n", unit->name, smode, smode);

  // Preparing data for the child
  if (dir) {
    padico_string_printf(path, "%s/%s", getenv("HOME"), dir);
    padico_print("bin/sh: chdir to %s\n",dir);
    if (chdir(dir) != 0) {
      padico_warning("bin/sh chdir failed with errno %d\n", errno);
    }
  } else {
    padico_warning("bin/sh no Dir attribute specified\n");
  }

  padico_string_printf(cmd, "%s/var/mod/%s/sh/%s", getenv("PADICO_ROOT"),
                       padico_getenv("PADICO_CORE"),unit->name);
  padico_string_printf(tcmd, "bin/sh exec of %s ", padico_string_get(cmd));
  for(i=0;i<job->argc;i++) {
    largv[i]=job->argv[i];
    padico_string_catf(tcmd, "%s ", job->argv[i]);
  }
  largv[job->argc]=0;
  padico_string_catf(tcmd, "\n");
  padico_print(padico_string_get(tcmd));
  // End preparing data for the child

  puk_spinlock_acquire();
  pid = fork();
  puk_spinlock_release();
  if (pid == -1) {
    padico_print("bin/sh fork failed with errno=%d\n", errno);
    ret =  padico_rc_error("bin/sh fork failed with errno=%d\n", errno);
  } else {
    if (pid == 0) {
      // child
      if (execv(padico_string_get(cmd), largv) == -1) {
        // Gloops
        padico_warning("bin/sh excv failed because of %d:%s\n", errno, strerror(errno));
        exit(127);
      } else {
        // Should be dead code ;))
        exit(126);
      }

    } else {
      // parent
      wait_args_t* pwa;

      pwa = (wait_args_t*) padico_malloc(sizeof(wait_args_t));

      if ( smode!= NULL && ((smode[0]=='n') || (smode[0]=='N'))) {
        padico_trace("bin/sh notifying of job terminaison (ASYNC)\n");
        job->rc = padico_rc_ok();
        puk_job_notify(job);
        pwa->sync = 0;
      } else {
        pwa->sync = 1;
      }

      pwa->job = job;
      pwa->pid = pid;

      marcel_create(&pwa->tid, NULL, waiting_child, (void*) pwa);
    }
  }

  return ret;
}

static padico_rc_t sh_unit_unload(puk_unit_t unit)
{
  padico_trace("bin/sh: unload handler: unit %s.\n", unit->name);

  return padico_rc_ok();
}

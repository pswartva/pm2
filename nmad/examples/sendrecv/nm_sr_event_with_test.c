/*
 * NewMadeleine
 * Copyright (C) 2006-2021 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>

#include "../common/nm_examples_helper.h"

static const char short_msg[] = "hello, world";
static char *buf = NULL;
static size_t short_len;
static unsigned happened_event_finalized = 0;

static void request_notifier(nm_sr_event_t event, const nm_sr_event_info_t*p_info, void*_ref)
{
  nm_sr_request_t* p_request = p_info->req.p_request;

  if(nm_sr_request_test(p_request, NM_SR_EVENT_RECV_DATA) && !nm_sr_request_test(p_request, NM_SR_EVENT_FINALIZED))
    {
      printf(":: event NM_SR_EVENT_RECV_DATA.\n");
      nm_sr_recv_unpack_contiguous(p_session, p_request, buf, short_len);
      // return; // Problem is solved with this return.
    }
  else if(nm_sr_request_test(p_request, NM_SR_EVENT_FINALIZED))
    {
      printf(":: event NM_SR_EVENT_FINALIZED.\n");
      assert(happened_event_finalized == 0);
      assert(strcmp(short_msg, buf) == 0);
      happened_event_finalized = 1;
    }
}

int main(int argc, char **argv)
{
  short_len = 1 + strlen(short_msg);
  nm_sr_request_t request;

  buf = malloc(short_len);

  nm_examples_init(&argc, argv);

  if(is_server)
    {
      /* ** server */
      memset(buf, 0, short_len);

      nm_sr_recv_init(p_session, &request);

      nm_sr_request_monitor(p_session, &request, NM_SR_EVENT_FINALIZED | NM_SR_EVENT_RECV_DATA,
                            &request_notifier);
      nm_sr_recv_irecv(p_session, &request, NM_ANY_GATE, 0, NM_TAG_MASK_FULL);

      fprintf(stderr, "# done.\n");
    }
  else
    {
      /* ** client */
      fprintf(stderr, "# send tag 0- short\n");
      nm_sr_isend(p_session, p_gate, 0, short_msg, short_len, &request);
      nm_sr_swait(p_session, &request);
    }

  nm_examples_exit();
  free(buf); /* free buf after exit, since requests may run asynchronously up to there. */
  exit(0);
}

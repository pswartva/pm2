import java.io.*;
public class SocketParam {
/* Cette classe d�finit
   - un tableau (byte[] buf)
   - sa capacit� (max), le nb d'octets qu'il peut contenir
   - le nombre d'octets qu'il contient (n)
   Pour la v�rification des transmissions on impose byte[i] = i % 256.
   - init remplit buf avec les valeurs appropri�es.
   - check v�rifie les valeurs.
   - setBuf() permet d'agrandir le tableau si on re�oit plus d'octets que la
     capacit� actuelle du tableau.
*/


    public static int NOT_DEFINED = -1;

    public int n, max;
    public byte[] buf;

    public SocketParam() {
        n = NOT_DEFINED;
    }
    public SocketParam(int in) {
        n = in;
        max = in;
        setBuf(in);
    }

    public SocketParam(int in, int imax) {
        n = in;
        max = imax;
        setBuf(imax);
    }

    public void setBuf() {
        if (n > max) {
            System.out.println("Growing buffer !");
            max = n;
            buf = new byte[n];
            init();         
        }
    }
    private void setBuf(int in) {
        buf = new byte[in];
        init();             
    }

    private void init() {
        for(int i=0;i<max;i++)
            buf[i]=(byte)(i&0xFF);
    }

    public void check() {
        boolean bug = false;
        int i, num = 0, first = 0;
        for(i=0;i<n;i++) {
            if (buf[i] != (byte) (i&0xFF)) {
              if (bug == false) {
                bug=true;
                first = i;
              }
                num++;
            }
        }
        if (bug) {
            System.err.println("ERROR ! " +  num + " differences on "+n+" bytes, 1st at "
                               + first + " (" + buf[first]
                               + " " + (byte)(first&0xFF) + ")");
            /*      try {
            File outputFile = new File("bug_byte_array");
            System.out.println(outputFile.getAbsolutePath());
            FileOutputStream out = new FileOutputStream(outputFile);
            out.write(buf);
            out.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.exit(-1);*/
        }
    }

    public void clear() {
        for(int i=0;i<max;i++)
            buf[i]=13;
    }
}


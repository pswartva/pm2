/* bench-CORBA-server.cc
 */

#include <omniORB4/CORBA.h>
#include <omniORB4/Naming.hh>
#include "bench-CORBA.h"
#include <iostream>
#include <stdlib.h>

static CORBA::ORB_ptr                 orb;

class bench_CORBA_impl : public POA_bench_CORBA
{
public:
  /* ** Send a sequence of octets */
  void SqO(const bench_CORBA::SeqOctet_t &msg)
  {
    static int i = 0;
    i++;
  }
  /* ** Round-trip with a sequence of octets */
  void RO(bench_CORBA::SeqOctet_t &msg)
  {
  }
  /* ** Latency test: empty method, no args */
  void LT(void)
  {
  }
  void Finish(void)
  {
    std::cerr << "Finish!\n" << std::endl;
    orb->shutdown(false);
    std::cerr << "Finish done.\n" << std::endl;
  }
};



int main(int argc, char*argv[])
{
  bench_CORBA_impl*              bench_servant;
  bench_CORBA_var                bench_ref = NULL;
  PortableServer::POA_var        poa;
  PortableServer::POAManager_var poa_manager;
  CosNaming::NamingContext_var   naming_context;
  PortableServer::ObjectId_var   bench_server_oid;
  CosNaming::Name                bench_server_name;

  orb = CORBA::ORB_init(argc, argv);

  std::cerr << "CORBA- resolving NameService...\n" << std::endl;
  try
    {
      CORBA::Object_var poa_ref = orb->resolve_initial_references ("RootPOA");
      poa = PortableServer::POA::_narrow (poa_ref);
      poa_manager = poa->the_POAManager();
      poa_manager->activate();
      
      CORBA::Object_var ns_ref = orb->resolve_initial_references ("NameService");
      naming_context = CosNaming::NamingContext::_narrow (ns_ref);
    }
  catch(...)
    {
      std::cerr <<  "CORBA- error while resolving NameService.\n" << std::endl;
      abort();
    }
  bench_server_name.length (1);
  bench_server_name[0].id = CORBA::string_dup("default");
  bench_server_name[0].kind = CORBA::string_dup("bench_CORBA");
  
  std::cerr << "CORBA- activating the servant...\n" << std::endl;
  try
    {
      bench_servant = new bench_CORBA_impl;
      bench_server_oid = poa->activate_object(bench_servant);
      naming_context->rebind(bench_server_name, bench_servant->_this());
      std::cerr << "CORBA- server ready [" << orb->object_to_string(bench_servant->_this()) << "]\n" << std::endl;
    }
  catch(...)
    {
      std::cerr << "CORBA- error while registering server.\n" << std::endl;
      abort();
    }
  
  orb->run();

#if 0

  naming_context->unbind(bench_server_name);

  std::cerr << "CORBA- finalizing the servant...\n" << std::endl;
  try
    {
      poa->deactivate_object(bench_server_oid);
    }
  catch(const PortableServer::POA::ObjectNotActive &e)
    {
      std::cerr << "CORBA- exception while deactivating object: object not active\n" << std::endl;
    }
  catch(const PortableServer::POA::WrongPolicy &e)
    {
      std::cerr << "CORBA- exception while deactivating object: wrong policy\n" << std::endl;
    }
  catch(...)
    {
      std::cerr << "CORBA- exception while deactivating object\n" << std::endl;
    }
#endif 

  orb->destroy();

  delete bench_servant;

  return 0;
}


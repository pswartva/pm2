#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#define BUFFMAX 5000


//sort algorithm
void fusion(double* tableau, int debut1, int fin1, int fin2)
{

   int debut2 = fin1+1;
   int compteur1 = debut1;
   int compteur2 = debut2;
   int i;

   double* tableau2 = malloc((fin1-debut1+1)*sizeof(double));

   // copy of the element of the beginning of the array
   for(i=debut1; i<=fin1; i++)
      tableau2[i-debut1] = tableau[i];

   // fusion of the two arrays
   for(i=debut1; i<=fin2; i++)
   {
      if(compteur1==debut2) // every element of the first array used
         break; // every element ranked
      else if(compteur2==(fin2+1)) //every element of the second array used
      { // copy of the element of the first under-array at the end of the array
         tableau[i] = tableau2[compteur1-debut1];
         compteur1++;
      }
      else if(tableau2[compteur1-debut1]<tableau[compteur2])
      { // adding of 1 element of the first under-array
         tableau[i] = tableau2[compteur1-debut1];
         compteur1++; // going the next item of the first under-array
      }
      else
      { // copy of the element at the end of the array
         tableau[i] = tableau[compteur2];
         compteur2++; // going the next item of the second under-array
      }
   }
   free(tableau2);
}

void triFusionAux(double* tableau, int debut, int fin)
{
   if(debut!=fin) // stop condition
   {
      int milieu = (debut+fin)/2;
      triFusionAux(tableau, debut, milieu); // sort part1
      triFusionAux(tableau, milieu+1, fin); // sort part2
      fusion(tableau, debut, milieu, fin); // fusion of the two parts
   }
}

void triFusion(double* tableau, int longueur)
{
   if(longueur>0)
      triFusionAux(tableau, 0, longueur-1);
}


//end sort algorithm

//begin median

double median(double* tab, int length){
  if(length%2==0){
    return (tab[length/2-1]+tab[length/2])/2;
  }
  else{
    return tab[length/2];
  }
}

//end median

int main(int argc, char *argv[])
{
  if (argc!=3){
    printf("wrong\n");
    return 0;
  }
  else
    {
    FILE* fichier = NULL;

    fichier = fopen(argv[1], "r");

    char buff[BUFFMAX];
    int nbprocess=atoi(argv[2])/2;
    double latencymin[nbprocess];
    double latencymed[nbprocess];
    double latencymax[nbprocess];
    int i=0;
    int a;

    if (fichier != NULL)
      {
        while(fgets(buff,BUFFMAX,fichier)!=NULL)
          {
            if(strchr(buff,'#')==NULL && i<nbprocess )
              {
                sscanf(buff," %d %lf %lf %lf ",&a,&latencymin[i],&latencymed[i],&latencymax[i]);
                i++;
              }
          }
        triFusion(latencymin, nbprocess);
        triFusion(latencymed, nbprocess);
        triFusion(latencymax, nbprocess);
        printf("%9d\t%9.3f\t%9.3f\t%9.3f\n",nbprocess,latencymin[0],median(latencymed,nbprocess),latencymax[nbprocess-1]);
      }

    fclose(fichier);
    }
  return 0;
}

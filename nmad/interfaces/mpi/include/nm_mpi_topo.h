/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


#ifndef NM_MPI_TOPO_H
#define NM_MPI_TOPO_H

/** \addtogroup mpi_interface */
/* @{ */

/** @name Functions: Topology */
/* @{ */

int MPI_Dims_create(int nnodes, int ndims, int*dims);

int MPI_Cart_create(MPI_Comm comm_old, int ndims, const int*dims, const int*periods, int reorder, MPI_Comm*_comm_cart);

int MPI_Cart_coords(MPI_Comm comm, int rank, int ndims, int*coords);

int MPI_Cart_rank(MPI_Comm comm, const int*coords, int*rank);

int MPI_Cart_shift(MPI_Comm comm, int direction, int displ, int*source, int*dest);

int MPI_Cart_get(MPI_Comm comm, int maxdims, int*dims, int*periods, int*coords);

int MPI_Cart_sub(MPI_Comm comm, const int*remain_dims, MPI_Comm*newcomm);

int MPI_Topo_test(MPI_Comm comm, int*topo_type);

int MPI_Cartdim_get(MPI_Comm comm, int*ndims);


/* @}*/
/* @}*/

#endif /* NM_MPI_COMMUNICATOR_H */

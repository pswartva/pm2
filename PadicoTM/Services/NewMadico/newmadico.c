/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief PadicoTM NewMadico Service: NewMad over PSP.
 * @ingroup NewMadico
 */

#undef NEWMADICO_PROFILE

#include <Padico/Puk.h>
#include <Padico/Topology.h>
#include <Padico/PSP.h>
#include <Padico/PadicoTM.h>
#include <Padico/Module.h>
#ifdef NEWMADICO_PROFILE
#include <Padico/Timing.h>
#endif

static int padico_newmadico_init(void);

PADICO_MODULE_DECLARE(NewMadico, padico_newmadico_init, NULL, NULL);

#if defined(NMAD)

#ifdef MARCEL
#ifndef PIOMAN
#  error "PIOMan is required for NewMadico with Marcel"
#endif
#endif

#include <nm_public.h>
#include <nm_private.h>

struct nm_newmadico_drv
{
  const char*url;          /**< nmad url = node UUID*/
  puk_hashtable_t gates_by_node; /**< hash: node -> gate */
  puk_hashtable_t nodes_by_gate; /**< hash: gate -> node */
};

/** NewMadico instance status (gate data) */
struct nm_newmadico
{
  padico_topo_node_t remote_node;                    /**< node for the gate */
  struct puk_receptacle_PadicoSimplePackets_s psp; /**< receptacle for the used PSP */
  size_t h_size;                                   /**< PSP header size */
};

/** a 'pending receive' entry, i.e. recv has been posted but data has not arrived yet.
 */
PUK_LIST_TYPE(newmadico_pending_recv,
              struct nm_pkt_wrap_s*p_pw;
              nm_trk_id_t trk;
              padico_topo_node_t node;
              volatile int fullfilled;
              void*base;
              );

/** an 'unexpected' entry, i.e. data has arrived but recv has not been posted
 */
PUK_LIST_TYPE(newmadico_unexpected,
              nm_trk_id_t trk;
              padico_topo_node_t node;
              uint64_t length;
              char data[32 * 1024];
              );

/** the PSP header for newmadico
 */
struct newmadico_hdr_s
{
  uint64_t length;
  uint16_t h_size;
  nm_trk_id_t trk_id;
  char data;
} __attribute__((packed));

static struct
{
  struct newmadico_pending_recv_list_s pending_recv; /**< posted recv waiting for data from network */
  struct newmadico_pending_recv_list_s pending_pool; /**< cache of preallocated entries for the above list */
  struct newmadico_unexpected_list_s unexpected;     /**< unexpected packets */
  struct nm_newmadico_drv*drv;                       /**< newmad driver for NewMadico */
} newmadico;

#ifdef NEWMADICO_PROFILE
static struct
{
  padico_timing_t t1, t2, t3;
  int count;
} ticks = { .count = 0};
#endif

/* ** Newmadico NewMad Driver */

static int nm_newmadico_query(nm_drv_t p_drv, struct nm_driver_query_param *params, int nparam);
static int nm_newmadico_init(nm_drv_t p_drv, struct nm_minidriver_capabilities_s*trk_caps, int nb_trks);
static int nm_newmadico_close(nm_drv_t p_drv);
static int nm_newmadico_connect(void*_status, nm_gate_t p_gate, nm_drv_t p_drv, nm_trk_id_t trk_id, const char*remote_url);
static int nm_newmadico_disconnect(void*_status, nm_gate_t p_gate, nm_drv_t p_drv, nm_trk_id_t trk_id);
static int nm_newmadico_post_send_iov(void*_status, struct nm_pkt_wrap_s *p_pw);
static int nm_newmadico_post_recv_iov(void*_status, struct nm_pkt_wrap_s *p_pw);
static int nm_newmadico_poll_send_iov(void*_status, struct nm_pkt_wrap_s *p_pw);
static int nm_newmadico_poll_recv_iov(void*_status, struct nm_pkt_wrap_s *p_pw);
static const char*nm_newmadico_get_driver_url(nm_drv_t p_drv);

static const struct nm_drv_iface_s nm_newmadico_driver =
  {
    .name               = "newmadico",

    .query              = &nm_newmadico_query,
    .init               = &nm_newmadico_init,
    .close              = &nm_newmadico_close,

    .connect            = &nm_newmadico_connect,
    .disconnect         = &nm_newmadico_disconnect,

    .post_send_iov      = &nm_newmadico_post_send_iov,
    .post_recv_iov      = &nm_newmadico_post_recv_iov,

    .poll_send_iov      = &nm_newmadico_poll_send_iov,
    .poll_recv_iov      = &nm_newmadico_poll_recv_iov,

    .get_driver_url     = &nm_newmadico_get_driver_url,

  };

/* ** 'PadicoComponent' facet for Newmadico driver */
static void*nm_newmadico_instantiate(puk_instance_t, puk_context_t);
static void nm_newmadico_destroy(void*);

static const struct puk_component_driver_s nm_newmadico_component_driver =
  {
    .instantiate = &nm_newmadico_instantiate,
    .destroy     = &nm_newmadico_destroy
  };


static void newmadico_callback(const void*header, padico_topo_node_t from,
                               void*key, padico_psp_pump_t pump,
                               void*token);

#endif /* NMAD */


/* ********************************************************* */

/** NewMadico module init function
 */
static int padico_newmadico_init(void)
{
#if defined (NMAD)

  padico_print("loading...\n");

  puk_iface_register("NewMad_Driver");
  puk_component_declare("NewMadico",
                        puk_component_provides("PadicoComponent", "component",  &nm_newmadico_component_driver),
                        puk_component_provides("NewMad_Driver", "nmdriver", &nm_newmadico_driver),
                        puk_component_uses("PadicoSimplePackets", "psp"));
  newmadico_pending_recv_list_init(&newmadico.pending_recv);
  newmadico_pending_recv_list_init(&newmadico.pending_pool);
  newmadico_unexpected_list_init(&newmadico.unexpected);

  /* Allocate p_drv */
  newmadico.drv = padico_malloc(sizeof(struct nm_newmadico_drv));
  memset(newmadico.drv, 0, sizeof (struct nm_newmadico_drv));
  newmadico.drv->gates_by_node = puk_hashtable_new_ptr();
  newmadico.drv->nodes_by_gate = puk_hashtable_new_ptr();

#else
  padico_warning("not initialized- NewMad not built in PadicoTM core.\n");
  return -1;
#endif /* NMAD */
  return 0;
}


#if defined (NMAD)

/****************************************************************
 *** PadicoComponent functions
 */

/** Instanciation function */
static void*nm_newmadico_instantiate(puk_instance_t instance, puk_context_t context)
{
  struct nm_newmadico*status = padico_malloc(sizeof (struct nm_newmadico));
  memset(status, 0, sizeof(struct nm_newmadico));
  puk_context_indirect_PadicoSimplePackets(instance, "psp", &status->psp);
  return status;
}

static void nm_newmadico_destroy(void*_status)
{
  padico_free(_status);
}



/****************************************************************
 *** NewMad_Driver functions
 */

static int nm_newmadico_query(nm_drv_t p_drv, struct nm_driver_query_param *params, int nparam)
{
  return 0;
}

static int nm_newmadico_init(nm_drv_t p_drv, struct  nm_minidriver_capabilities_s*trk_caps, int nb_trks)
{
  /* Set the url */
  padico_topo_node_t self = padico_topo_getlocalnode();
  padico_topo_uuid_t uuid = padico_topo_node_getuuid(self);
  newmadico.drv->url = padico_strdup((char*)uuid);

  trk_caps[NM_TRK_SMALL].max_msg_size = 32 * 1024;

  return 0;
}

static int nm_newmadico_close(nm_drv_t p_drv)
{
  return 0;
}


static const char*nm_newmadico_get_driver_url(nm_drv_t p_drv)
{
  return newmadico.drv->url;
}

static padico_topo_node_t nm_newmadico_url_to_node(const char* url)
{
  padico_topo_uuid_t uuid = (padico_topo_uuid_t)url;
  padico_topo_node_t node = padico_topo_getnodebyuuid(uuid);
  return node;
}

/* Connection Functions */

static int nm_newmadico_connect(void*_status, nm_gate_t p_gate, nm_drv_t p_drv, nm_trk_id_t trk_id, const char*remote_url)
{
  struct nm_newmadico*status = _status;
  padico_topo_node_t peer_node = nm_newmadico_url_to_node(remote_url);
  status->remote_node = peer_node;
  puk_hashtable_insert(newmadico.drv->gates_by_node, peer_node, p_gate);
  puk_hashtable_insert(newmadico.drv->nodes_by_gate, p_gate, peer_node);
  /* establish PSP connection */
  status->h_size = sizeof(struct newmadico_hdr_s);
  padico_psp_init(&status->psp, padico_psp_tag_build(PADICO_PSP_NEWMADICO, trk_id),
                  "NewMadico", &status->h_size, &newmadico_callback, p_gate->p_core);
  padico_psp_connect(&status->psp, status->remote_node);
  return 0;
}

static int nm_newmadico_disconnect(void*_status, nm_gate_t p_gate, nm_drv_t p_drv, nm_trk_id_t trk_id)
{
  return 0;
}

/* Send / Recv Functions */
static int nm_newmadico_post_send_iov(void*_status, struct nm_pkt_wrap_s *p_pw)
{
  struct nm_newmadico*status = _status;
  struct newmadico_hdr_s*hdr = NULL;
  padico_out(40, "trk_id = %d; gate = %p\n", p_pw->trk_id, p_pw->p_gate);
  padico_psp_connection_t psp_conn =
    padico_psp_new_message(&status->psp, status->remote_node, (void**)&hdr);
  hdr->length = p_pw->length;
  hdr->trk_id = p_pw->trk_id;
  hdr->h_size = status->h_size;
  void*flattened = NULL;
  if(p_pw->length <= status->h_size - sizeof(struct newmadico_hdr_s))
    {
      int offset = 0;
      int i;
      for(i = 0; i < p_pw->v_nb; i++)
        {
          memcpy(&hdr->data + offset, p_pw->v[i].iov_base, p_pw->v[i].iov_len);
          offset += p_pw->v[i].iov_len;
        }
    }
  else
    {
      int i;
      if(p_pw->v_nb > 1)
        {
          int offset = 0;
          flattened = padico_malloc(p_pw->length);
#warning TODO- add iovec support in PSP
          for(i = 0; i < p_pw->v_nb; i++)
            {
              memcpy(flattened + offset, p_pw->v[i].iov_base, p_pw->v[i].iov_len);
              offset += p_pw->v[i].iov_len;
            }
          padico_psp_pack(&status->psp, psp_conn, flattened, p_pw->length);
        }
      else
        {
          padico_psp_pack(&status->psp, psp_conn, p_pw->v[0].iov_base, p_pw->v[0].iov_len);
        }
    }
  padico_psp_end_message(&status->psp, psp_conn);
  if(flattened)
    padico_free(flattened);
  padico_out(40, "trk_id = %d; gate = %p; SUCCESS.\n", p_pw->trk_id, p_pw->p_gate);
  return NM_ESUCCESS;
}

static int nm_newmadico_poll_send_iov(void*_status, struct nm_pkt_wrap_s *p_pw)
{
  padico_out(40, "trk_id = %d; gate = %p; SUCCESS.\n", p_pw->trk_id, p_pw->p_gate);
  return NM_ESUCCESS;
}

static int nm_newmadico_post_recv_iov(void*_status, struct nm_pkt_wrap_s *p_pw)
{
  const padico_topo_node_t posted_node = p_pw->p_gate ? puk_hashtable_lookup(newmadico.drv->nodes_by_gate, p_pw->p_gate) : NULL;
  const nm_trk_id_t posted_track = p_pw->trk_id;
  padico_out(40, "trk_id = %d; gate = %p\n", p_pw->trk_id, p_pw->p_gate);
  newmadico_unexpected_itor_t itor;
  assert(p_pw->v_nb == 1);
  PUK_LIST_FIND(newmadico_unexpected, itor, &newmadico.unexpected,
                (( posted_node == NULL) ||
                 ( posted_node == itor->node && posted_track == itor->trk)));
  if(itor)
    {
      /* Message was received as 'unecpected' */
      memcpy(p_pw->v->iov_base, itor->data, itor->length);
      newmadico_unexpected_list_remove(&newmadico.unexpected, itor);
      newmadico_unexpected_delete(itor);
      padico_out(40, "trk_id = %d- SUCCESS\n", posted_track);
      return NM_ESUCCESS;
    }
  else
    {
      /* Not received yet. Enqueue in 'pending' list. */
      newmadico_pending_recv_t new_pending =
        newmadico_pending_recv_list_empty(&newmadico.pending_pool) ?
        newmadico_pending_recv_new() :
        newmadico_pending_recv_list_pop_front(&newmadico.pending_pool);
      new_pending->node       = posted_node;
      new_pending->trk        = posted_track;
      new_pending->fullfilled = 0;
      new_pending->p_pw       = p_pw;
      new_pending->base       = p_pw->v->iov_base;
      newmadico_pending_recv_list_push_back(&newmadico.pending_recv, new_pending);
      p_pw->drv_priv = new_pending;
      padico_out(40, "trk_id = %d- EAGAIN\n", posted_track);
      return -NM_EAGAIN;
    }
}

static int nm_newmadico_poll_recv_iov(void*_status, struct nm_pkt_wrap_s *p_pw)
{
  newmadico_pending_recv_t itor = p_pw->drv_priv;
  padico_out(40, "trk_id = %d; pw = %p\n", p_pw->trk_id, p_pw);
  if(itor->fullfilled)
    {
      newmadico_pending_recv_list_remove(&newmadico.pending_recv, itor);
      newmadico_pending_recv_list_push_back(&newmadico.pending_pool, itor);
      padico_out(40, "trk_id = %d- SUCCESS\n", p_pw->trk_id);
#ifdef NEWMADICO_PROFILE
      if(ticks.count++ % 3517 == 319)
        {
          padico_timing_get_tick(&ticks.t3);
          double t2 = padico_timing_diff_usec(&ticks.t1, &ticks.t2);
          double t3 = padico_timing_diff_usec(&ticks.t2, &ticks.t3);
          padico_print("t1 = %f; t2 = %f\n", t2, t3);
        }
#endif
      return NM_ESUCCESS;
    }
  else
    {
      padico_out(40, "trk_id = %d- EAGAIN\n", p_pw->trk_id);
      return -NM_EAGAIN;
    }
  return -NM_ENOTFOUND;
}

static void newmadico_callback(const void*_hdr, padico_topo_node_t from, void*_key,
                               padico_psp_pump_t pump, void*token)
{
  struct nm_core*p_core = _key;
  const struct newmadico_hdr_s*const hdr = _hdr;
  const nm_trk_id_t incoming_trk = hdr->trk_id;
  const uint16_t h_size = hdr->h_size;
#ifdef NEWMADICO_PROFILE
  padico_timing_get_tick(&ticks.t1);
#endif
  padico_out(40, "trk_id = %d\n", incoming_trk);
  nm_core_lock(p_core);
  /* See if a pending_recv matches */
  newmadico_pending_recv_itor_t itor;
  PUK_LIST_FIND(newmadico_pending_recv, itor, &newmadico.pending_recv,
                ((incoming_trk == itor->trk) && (!itor->fullfilled) &&
                 ((itor->node == NULL) || (itor->node == from)))
                );
  nm_core_unlock(p_core);
  if(itor)
    {
      /* Expected packet */
      assert(itor->p_pw->length >= hdr->length);
      assert(itor->p_pw->v_nb == 1);
      itor->p_pw->length = hdr->length;
      if(hdr->length <= h_size - sizeof(struct newmadico_hdr_s))
        {
          assert(itor->p_pw->v_nb == 1);
          memcpy(itor->base, &hdr->data, hdr->length);
        }
      else
        {
          (*pump)(token, itor->base, hdr->length);
        }
      itor->fullfilled = 1;
#ifdef NEWMADICO_PROFILE
      padico_timing_get_tick(&ticks.t2);
#endif
    }
  else
    {
      /* Unexpected packet */
      assert(hdr->length < 32*1024);
      newmadico_unexpected_t new_received = newmadico_unexpected_new();
      new_received->node   = from;
      new_received->trk    = incoming_trk;
      new_received->length = hdr->length;
      if(hdr->length <= h_size - sizeof(struct newmadico_hdr_s))
        {
          memcpy(new_received->data, &hdr->data, hdr->length);
        }
      else
        {
          (*pump)(token, new_received->data, new_received->length);
        }
      nm_core_lock(p_core);
      newmadico_unexpected_list_push_back(&newmadico.unexpected, new_received);
      nm_core_unlock(p_core);
    }
  padico_out(40, "trk_id = %d; done.\n", incoming_trk);
}

#endif /* NMAD */

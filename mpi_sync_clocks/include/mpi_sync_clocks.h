/*
 * MPI sync clocks
 * Copyright (C) 2006-2020 (see AUTHORS file)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

#ifndef MPI_SYNC_CLOCKS_H
#define MPI_SYNC_CLOCKS_H

#include <mpi.h>

#include "sync_clocks_generic.h"

#ifdef __cplusplus
extern "C"
{
#endif

  /* ********************************************************* */
  /* ** MPI sync clocks public API */

  /** opaque type for MPI sync clocks */
  typedef struct sync_clocks_generic_s*mpi_sync_clocks_t;

  /** create an MPI sync clock object on the given communicator (collective operation) */
  static inline mpi_sync_clocks_t mpi_sync_clocks_init(MPI_Comm comm);

  /** destroy the given clock */
  static inline void mpi_sync_clocks_shutdown(mpi_sync_clocks_t p_clocks);

  /** get local time, in usec. */
  static inline double mpi_sync_clocks_get_time_usec(mpi_sync_clocks_t p_clocks);

  /** get origin of local time, in usec. */
  static inline double mpi_sync_clocks_get_time_origin_usec(sync_clocks_generic_t p_clocks);

  /** convert local time to global time */
  static inline double mpi_sync_clocks_local_to_global(mpi_sync_clocks_t p_clocks, double local_time);

  /** convert remote local time to global time */
  static inline double mpi_sync_clocks_remote_to_global(mpi_sync_clocks_t p_clocks, int rank, double remote_time);

  /** convert global time to local time */
  static inline double mpi_sync_clocks_global_to_local(mpi_sync_clocks_t p_clocks, double global_time);

  /** synchronize clocks (collective operation); must have been called before any clock conversion */
  static inline void mpi_sync_clocks_synchronize(mpi_sync_clocks_t p_clocks);

  /** a barrier with precise synchronization; return 0 in case of success, -1 if we missed the deadline;
   * barrier_local_time returns the local time at which barrier was supposed to take place */
  static inline int mpi_sync_clocks_barrier(mpi_sync_clocks_t p_clocks, double*barrier_local_time);



  /* ********************************************************* */
  /* ** MPI sync clocks internals */

  /** internal structure for MPI backend in sync_clocks_generic */
  struct mpi_sync_clocks_data_s
  {
    MPI_Comm comm;
  };

  static inline void mpi_sync_clocks_send(sync_clocks_generic_t p_clocks, int dest, int tag, void*p_data, int size)
  {
    struct mpi_sync_clocks_data_s*p_mpi_clocks = (struct mpi_sync_clocks_data_s*)&p_clocks->backend_data;
    MPI_Send(p_data, size, MPI_BYTE, dest, tag, p_mpi_clocks->comm);
  }
  static inline void mpi_sync_clocks_recv(sync_clocks_generic_t p_clocks, int from, int tag, void*p_data, int size)
  {
    struct mpi_sync_clocks_data_s*p_mpi_clocks = (struct mpi_sync_clocks_data_s*)&p_clocks->backend_data;
    MPI_Recv(p_data, size, MPI_BYTE, from, tag, p_mpi_clocks->comm, MPI_STATUS_IGNORE);
  }
  static inline void mpi_sync_clocks_bcast(sync_clocks_generic_t p_clocks, int root, int tag  __attribute__((unused)), void*p_data, int size)
  {
    struct mpi_sync_clocks_data_s*p_mpi_clocks = (struct mpi_sync_clocks_data_s*)&p_clocks->backend_data;
    MPI_Bcast(p_data, size, MPI_BYTE, root, p_mpi_clocks->comm);
  }
  static inline void mpi_sync_clocks_gather(sync_clocks_generic_t p_clocks, int root, int tag  __attribute__((unused)), void*p_sbuf, int size, void*p_rbuf)
  {
    struct mpi_sync_clocks_data_s*p_mpi_clocks = (struct mpi_sync_clocks_data_s*)&p_clocks->backend_data;
    MPI_Gather(p_sbuf, size, MPI_BYTE, p_rbuf, size, MPI_BYTE, root, p_mpi_clocks->comm);
  }
  static inline void mpi_sync_clocks_mpi_barrier(sync_clocks_generic_t p_clocks)
  {
    struct mpi_sync_clocks_data_s*p_mpi_clocks = (struct mpi_sync_clocks_data_s*)&p_clocks->backend_data;
    MPI_Barrier(p_mpi_clocks->comm);
  }

  /* ********************************************************* */
  /* ** MPI sync clocks implementation */

  static inline mpi_sync_clocks_t mpi_sync_clocks_init(MPI_Comm comm)
  {
    static const struct sync_clocks_backend_s backend =
      {
       .send     = &mpi_sync_clocks_send,
       .recv     = &mpi_sync_clocks_recv,
       .bcast    = &mpi_sync_clocks_bcast,
       .gather   = &mpi_sync_clocks_gather,
       .barrier  = &mpi_sync_clocks_mpi_barrier
      };
    struct sync_clocks_generic_s*p_clocks = malloc(sizeof(struct sync_clocks_generic_s));
    p_clocks->backend = backend;
    struct mpi_sync_clocks_data_s*p_mpi_clocks = (struct mpi_sync_clocks_data_s*)&p_clocks->backend_data;
    MPI_Comm_dup(comm, &p_mpi_clocks->comm);
    MPI_Comm_size(p_mpi_clocks->comm, &p_clocks->comm_size);
    MPI_Comm_rank(p_mpi_clocks->comm, &p_clocks->rank);
    sync_clocks_generic_init(p_clocks);
    return p_clocks;
  }

  static inline void mpi_sync_clocks_shutdown(mpi_sync_clocks_t p_clocks)
  {
    struct mpi_sync_clocks_data_s*p_mpi_clocks = (struct mpi_sync_clocks_data_s*)&p_clocks->backend_data;
    MPI_Comm_free(&p_mpi_clocks->comm);
    sync_clocks_generic_shutdown(p_clocks);
  }

  static inline double mpi_sync_clocks_get_time_usec(mpi_sync_clocks_t p_clocks)
  {
    return sync_clocks_generic_get_time_usec(p_clocks);
  }

  static inline double mpi_sync_clocks_get_time_origin_usec(mpi_sync_clocks_t p_clocks)
  {
    return sync_clocks_generic_tick2usec(p_clocks->time_orig);
  }

  static inline double mpi_sync_clocks_remote_to_global(mpi_sync_clocks_t p_clocks, int rank, double remote_time)
  {
    return sync_clocks_generic_remote_to_global(p_clocks, rank, remote_time);
  }

  static inline double mpi_sync_clocks_local_to_global(mpi_sync_clocks_t p_clocks, double local_time)
  {
    return sync_clocks_generic_local_to_global(p_clocks, local_time);
  }

  static inline double mpi_sync_clocks_global_to_local(mpi_sync_clocks_t p_clocks, double global_time)
  {
    return sync_clocks_generic_global_to_local(p_clocks, global_time);
  }

  static inline long double mpi_sync_clocks_get_skew(mpi_sync_clocks_t p_clocks, int rank)
  {
    return sync_clocks_generic_get_skew(p_clocks, rank);
  }

  static inline void mpi_sync_clocks_synchronize(mpi_sync_clocks_t p_clocks)
  {
    sync_clocks_generic_synchronize(p_clocks);
  }

  static inline int mpi_sync_clocks_barrier(mpi_sync_clocks_t p_clocks, double*barrier_local_time)
  {
    return sync_clocks_generic_barrier(p_clocks, barrier_local_time);
  }

#ifdef __cplusplus
}
#endif

#endif /* MPI_SYNC_CLOCKS_H */

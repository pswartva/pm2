/*
 * NewMadeleine
 * Copyright (C) 2016-2022 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * Programme qui plante en le lançant sur 8 noeuds sur les datlons, sans préciser le driver:
 * mpirun -n 8 -nodelist jack0,jack1,william0,william1,joe0,joe1,henri0,henri1 ./nm_coll_bcast_big
 */


#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <math.h>

#include <nm_coll_interface.h>

#include "../common/nm_examples_helper.h"

#define MAXLEN (16*1024*1024)

int main(int argc, char**argv)
{
  nm_examples_init_topo(&argc, argv, NM_EXAMPLES_TOPO_STAR);

  const int root = 0;
  const int self = nm_comm_rank(p_comm);
  const int size = nm_comm_size(p_comm);
  const nm_tag_t tag = 0x42;

  int n;
  for(n = 2; n < size ; n++)
    {
      nm_len_t len;
      for(len = 1; len < MAXLEN; len = _next(len, 1.01, 1))
        {
          if(self == root)
            {
              printf("nodes = %d; len = %d; ", n + 1, (int)len);
            }

          if(self <= n)
            {
              nm_group_t p_group = nm_group_new();
              int i;
              for(i = 0; i <= n; i++)
                {
                  nm_group_add_node(p_group, nm_comm_get_gate(p_comm, i));
                }

              char*buffer = malloc(len);
              if(self == root)
                {
                  fill_buffer(buffer, len);
                }
              else
                {
                  clear_buffer(buffer, len);
                }
              struct nm_data_s data;
              nm_data_contiguous_build(&data, buffer, len);

              struct nm_coll_req_s* p_bcast = nm_coll_group_data_ibcast(p_session, p_group, root, self, &data, tag, NULL, NULL);
              nm_coll_req_wait(p_bcast);

              if(self != root)
                {
                  control_buffer(buffer, len);
                }
              free(buffer);
              nm_group_free(p_group);
            }

          nm_examples_barrier(1);

          if(self == root)
            {
              printf("ok\n");
            }
        }
    }

  nm_examples_exit();
  return 0;

}

/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 */

#ifndef PADICO_SPINLOCK_H
#define PADICO_SPINLOCK_H

#ifndef MARCEL
#  define PADICO_SHM_PTHREAD
#endif


#ifndef PADICO_SHM_PTHREAD

/* ********************************************************* */
/* ** with Marcel */

#include <Padico/PM2.h>
#include <sched.h>

typedef volatile unsigned int padico_spinlock_t;

#define PADICO_SPINS_MAX 100
#define PADICO_SPINLOCK_INIT 0

static inline void padico_spin_lock(padico_spinlock_t*spinlock)
{
  int count = 0;
  while(pm2_spinlock_testandset(spinlock))
  {
    if(count++ > PADICO_SPINS_MAX)
      {
        count = 0;
        sched_yield();
        /* marcel_yield(); */
      }
  }
}
static inline void padico_spin_unlock(padico_spinlock_t*spinlock)
{
  pm2_spinlock_release(spinlock);
}
static inline void padico_spinlock_init(padico_spinlock_t*spinlock)
{
  *spinlock = PADICO_SPINLOCK_INIT;
}

#else /* PADICO_SHM_PTHREAD */

/* ********************************************************* */
/* ** with pthread */

#include <semaphore.h>
#include <pthread.h>
#include <sched.h>

#define USE_SPINLOCK

#ifdef USE_SPINLOCK
typedef pthread_mutex_t padico_spinlock_t;
#else
typedef pthread_spinlock_t padico_spinlock_t;
#endif

#define PADICO_SPINLOCK_INIT 1

static inline void padico_spin_lock(padico_spinlock_t*spinlock)
{
#ifdef USE_SPINLOCK
  pthread_spin_lock((pthread_spinlock_t*)spinlock);
#else
  pthread_mutex_lock((pthread_mutex_t*)spinlock);
#endif
}

static inline void padico_spin_unlock(padico_spinlock_t*spinlock)
{
#ifdef USE_SPINLOCK
  pthread_spin_unlock((pthread_spinlock_t*)spinlock);
#else
  pthread_mutex_unlock((pthread_mutex_t*)spinlock);
#endif
}

static inline void padico_spinlock_init(padico_spinlock_t*spinlock)
{
#ifdef USE_SPINLOCK
  pthread_spin_init((pthread_spinlock_t*)spinlock, PTHREAD_PROCESS_SHARED);
#else
  pthread_mutexattr_t attr;
  pthread_mutexattr_init(&attr);
  pthread_mutexattr_setpshared(&attr, PTHREAD_PROCESS_SHARED);
  pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_ADAPTIVE_NP);
  pthread_mutexattr_setrobust_np(&attr, PTHREAD_MUTEX_ROBUST_NP);
  pthread_mutex_init((pthread_mutex_t*)spinlock, &attr);
#endif
}

#endif /* PADICO_SHM_PTHREAD */


#endif /* PADICO_SPINLOCK_H */

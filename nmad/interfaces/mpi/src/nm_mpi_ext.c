/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


#include "nm_mpi_private.h"
#include "nm_mpi_private_syms.h"
#include "mpi-ext.h"

#include <Padico/Puk.h>
#include <Padico/Module.h>
PADICO_MODULE_HOOK(MadMPI);

/* ********************************************************* */
/* Aliases */

NM_MPI_ALIAS(MPIX_Query_cuda_support, mpix_query_cuda_support);

/* ********************************************************* */

int mpix_query_cuda_support(void)
{
  return 0; /* unsupported for now */
}

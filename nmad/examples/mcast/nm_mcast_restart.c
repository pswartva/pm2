/*
 * NewMadeleine
 * Copyright (C) 2022-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

/* Example restarting the mcast service */

#include <stdlib.h>
#include <stdio.h>

#include <nm_launcher_interface.h>
#include <nm_coll_interface.h>
#include <nm_mcast_interface.h>

#include "../common/nm_examples_helper.h"

void do_test(int rank, int worldsize)
{
  fprintf(stderr, "[%d] start of do_test\n", rank);
  int var = -1;
  int i;
  const nm_tag_t tag = 0x02;
  struct nm_data_s data;

  nm_mcast_service_t mcast_service = nm_mcast_init(p_comm);

  /* Simple mcast send: 0 sends to 1,2,3 */
  nm_data_contiguous_build(&data, &var, sizeof(var));

  if(rank == 0)
    {
      int* dests = malloc(sizeof(int) * (worldsize - 1));
      for(i = 1; i < worldsize; i++)
        {
          dests[i-1] = i;
        }

      var = 42;
      nm_mcast_send(mcast_service, p_comm, dests, NULL, worldsize-1, tag, &data, sizeof(var), NM_COLL_TREE_DEFAULT);

      free(dests);
    }
  else
    {
      nm_sr_recv(nm_comm_get_session(p_comm), NM_ANY_GATE, tag, &var, sizeof(var));
      assert(var == 42);
    }
  /* End of simple mcast */


  nm_coll_barrier(p_comm, 0xF3);
  if(rank == 3) // simulate rank 3 is a little bit late to go out of the barrier
    puk_sleep(2);

  fprintf(stderr, "[%d] before shutting down mcast\n", rank);
  nm_mcast_finalize(mcast_service);
}

int main(int argc, char**argv)
{
  int rank, worldsize;

  nm_examples_init_topo(&argc, argv, NM_EXAMPLES_TOPO_STAR);
  nm_launcher_get_size(&worldsize);
  nm_launcher_get_rank(&rank);

  if(worldsize != 4)
    {
      fprintf(stderr, "We need 4 nodes.\n");

      nm_launcher_exit();
      return EXIT_FAILURE;
    }

  do_test(rank, worldsize);
  do_test(rank, worldsize);

  nm_examples_exit();

  return EXIT_SUCCESS;
}

/** @file
 * @brief Padico SOAP Gatekeeper
 * @ingroup SOAPGatekeeper
 */

/** @defgroup SOAPGatekeeper Service: SOAP-Gatekeeper -- server-side for SOAP remote control
 */

#include <Padico/Puk.h>
#include <Padico/PadicoTM.h>
#include <Padico/PM2.h>
#include <Padico/NetAccess.h>
#include <Padico/Module.h>

#include <stdsoap2.h>

#include "soapH.h"
#include "HelloSOAP.nsmap"

#define SOAP_DEFAULT_PORT 18084

static struct soap soap;

static int hello_soap_init(void);
static void hello_soap_stop(void);

PADICO_MODULE_DECLARE(HelloSOAP_server, hello_soap_init, NULL, hello_soap_stop);

/* ********************************************************* */

int HelloSOAP__SayHello(struct soap *soap, char**out)
{
  padico_print("Hello world from SOAP!\n");
  out = NULL;
  return 0;
}

/* ********************************************************* */

static int hello_soap_start(void)
{
  int rc = 0;
  int m;
  int soap_port = SOAP_DEFAULT_PORT;
  char* soap_port_string = padico_getattr("SOAP_PORT");

  if(soap_port_string != NULL)
    {
      soap_port = atoi(soap_port_string);
    }

  padico_trace("creating HelloSOAP server on port %d\n", soap_port);
  soap_init(&soap);
  m = soap_bind(&soap, NULL, soap_port, 100);
  if (m < 0)
    {
      soap_print_fault(&soap, stderr);
      rc = -1;
    }
  else
    {
      padico_print("URL=http://%s:%d/HelloSOAP\n",
                   padico_topo_host_getname(padico_topo_getlocalhost()), soap_port);
    }
  return rc;
}

static void* hello_soap_worker(void*dummy)
{
  int s;
  padico_trace("starting HelloSOAP server\n");
  for ( ; ; )
    {
      s = soap_accept(&soap);
      padico_trace("Socket connection successful: slave socket = %d\n", s);
      if (s < 0)
        {
          soap_print_fault(&soap, stderr);
        }
      else
        {
          soap_serve(&soap);
          soap_end(&soap);
        }
    }
  return NULL;
}

/* **** module entry points ******************************** */

static marcel_t hello_soap_tid = 0;

static int hello_soap_init(void)
{
  int rc;
  rc = hello_soap_start();
  if(!rc)
    {
      marcel_create(&hello_soap_tid, NULL, &hello_soap_worker, NULL);
    }
  return rc;
}

static void hello_soap_stop(void)
{
  padico_trace("stop %s\n", padico_topo_nodename());
  if(hello_soap_tid)
    marcel_cancel(hello_soap_tid);
  /* TODO: actually kill the thread. */
}

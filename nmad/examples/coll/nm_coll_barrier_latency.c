/*
 * NewMadeleine
 * Copyright (C) 2006-2022 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>

#include "../common/nm_examples_helper.h"

#include <Padico/Puk.h>
#include <nm_sync_clocks_interface.h>

#define NB_BARRIERS 1000

int main(int argc, char**argv)
{
  nm_examples_init(&argc, argv);

  nm_group_t p_group = nm_comm_group(p_comm);
  const int self = nm_group_rank(p_group);
  const nm_tag_t tag = 0x42;

  nm_comm_t p_comm = nm_comm_world("nm_coll_barrier");
  nm_sync_clocks_t p_clocks = nm_sync_clocks_init(p_comm);

  nm_sync_clocks_barrier(p_clocks, NULL);
  const double t_begin = nm_sync_clocks_get_time_usec(p_clocks);
  puk_tick_t t1, t2;
  PUK_GET_TICK(t1);
  for(int i = 0; i < NB_BARRIERS; i++)
    {
      struct nm_coll_req_s*p_coll_req = nm_coll_group_ibarrier(p_session, p_group, self, tag);
      nm_coll_req_wait(p_coll_req);
    }
  PUK_GET_TICK(t2);
  const double t_end = nm_sync_clocks_get_time_usec(p_clocks);
  const double diff = PUK_TIMING_DELAY(t1, t2);

  printf("[%d] barrier latency: %lf usec.; global time: %lf usec.\n",
         self, diff / NB_BARRIERS, (t_end - t_begin) / NB_BARRIERS);

  nm_sync_clocks_shutdown(p_clocks);
  nm_examples_exit();
  return 0;
}

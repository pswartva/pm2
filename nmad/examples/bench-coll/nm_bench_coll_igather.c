/*
 * NewMadeleine
 * Copyright (C) 2019-2022 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


#include <nm_coll_interface.h>
#include "nm_bench_coll_generic.h"

static const int root = 0;
static const nm_tag_t tag = 0x42;
static char*gather_buf = NULL;

static void nm_bench_coll_igather_init(struct nm_bench_coll_common_s*p_common, void*buf, nm_len_t len)
{
  gather_buf = malloc(len * nm_comm_size(p_common->p_comm));
}
static void nm_bench_coll_igather_finalize(struct nm_bench_coll_common_s*p_common)
{
  free(gather_buf);
}

static void nm_bench_coll_igather_run(struct nm_bench_coll_common_s*p_common, void*buf, nm_len_t len)
{
  struct nm_coll_req_s*p_coll_req = nm_coll_igather(p_common->p_comm, root, buf, len, gather_buf, len, tag, NULL, NULL);
  nm_coll_req_wait(p_coll_req);
}

struct nm_bench_coll_s nm_bench_coll =
  {
    .label    = "nm_bench_coll_igather",
    .name     = "igather",
    .init     = &nm_bench_coll_igather_init,
    .run      = &nm_bench_coll_igather_run,
    .finalize = &nm_bench_coll_igather_finalize
  };

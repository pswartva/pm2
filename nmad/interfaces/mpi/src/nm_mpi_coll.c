/*
 * NewMadeleine
 * Copyright (C) 2014-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include "nm_mpi_private.h"
#include "nm_mpi_private_syms.h"
#include "nm_coll_private.h"
#include <assert.h>
#include <errno.h>

#include <Padico/Module.h>
PADICO_MODULE_HOOK(MadMPI);

/* ********************************************************* */

NM_MPI_ALIAS(MPI_Barrier,          mpi_barrier);
NM_MPI_ALIAS(MPI_Ibarrier,         mpi_ibarrier);
NM_MPI_ALIAS(MPI_Bcast,            mpi_bcast);
NM_MPI_ALIAS(MPI_Bcast_c,          mpi_bcast_c);
NM_MPI_ALIAS(MPI_Ibcast,           mpi_ibcast);
NM_MPI_ALIAS(MPI_Ibcast_c,         mpi_ibcast_c);
NM_MPI_ALIAS(MPI_Gather,           mpi_gather);
NM_MPI_ALIAS(MPI_Gather_c,         mpi_gather_c);
NM_MPI_ALIAS(MPI_Igather,          mpi_igather);
NM_MPI_ALIAS(MPI_Igather_c,        mpi_igather_c);
NM_MPI_ALIAS(MPI_Gatherv,          mpi_gatherv);
NM_MPI_ALIAS(MPI_Gatherv_c,        mpi_gatherv_c);
NM_MPI_ALIAS(MPI_Allgather,        mpi_allgather);
NM_MPI_ALIAS(MPI_Allgather_c,      mpi_allgather_c);
NM_MPI_ALIAS(MPI_Iallgather,       mpi_iallgather);
NM_MPI_ALIAS(MPI_Iallgather_c,     mpi_iallgather_c);
NM_MPI_ALIAS(MPI_Allgatherv,       mpi_allgatherv);
NM_MPI_ALIAS(MPI_Allgatherv_c,     mpi_allgatherv_c);
NM_MPI_ALIAS(MPI_Scatter,          mpi_scatter);
NM_MPI_ALIAS(MPI_Scatter_c,        mpi_scatter_c);
NM_MPI_ALIAS(MPI_Scatterv,         mpi_scatterv);
NM_MPI_ALIAS(MPI_Scatterv_c,       mpi_scatterv_c);
NM_MPI_ALIAS(MPI_Alltoall,         mpi_alltoall);
NM_MPI_ALIAS(MPI_Alltoall_c,       mpi_alltoall_c);
NM_MPI_ALIAS(MPI_Ialltoall,        mpi_ialltoall);
NM_MPI_ALIAS(MPI_Ialltoall_c,      mpi_ialltoall_c);
NM_MPI_ALIAS(MPI_Alltoallv,        mpi_alltoallv);
NM_MPI_ALIAS(MPI_Alltoallv_c,      mpi_alltoallv_c);
NM_MPI_ALIAS(MPI_Ialltoallv,       mpi_ialltoallv);
NM_MPI_ALIAS(MPI_Ialltoallv_c,     mpi_ialltoallv_c);
NM_MPI_ALIAS(MPI_Reduce,           mpi_reduce);
NM_MPI_ALIAS(MPI_Reduce_c,         mpi_reduce_c);
NM_MPI_ALIAS(MPI_Reduce_local,     mpi_reduce_local);
NM_MPI_ALIAS(MPI_Reduce_local_c,   mpi_reduce_local_c);
NM_MPI_ALIAS(MPI_Ireduce,          mpi_ireduce);
NM_MPI_ALIAS(MPI_Ireduce_c,        mpi_ireduce_c);
NM_MPI_ALIAS(MPI_Scan,             mpi_scan);
NM_MPI_ALIAS(MPI_Scan_c,           mpi_scan_c);
NM_MPI_ALIAS(MPI_Allreduce,        mpi_allreduce);
NM_MPI_ALIAS(MPI_Allreduce_c,      mpi_allreduce_c);
NM_MPI_ALIAS(MPI_Iallreduce,       mpi_iallreduce);
NM_MPI_ALIAS(MPI_Iallreduce_c,     mpi_iallreduce_c);
NM_MPI_ALIAS(MPI_Reduce_scatter,   mpi_reduce_scatter);
NM_MPI_ALIAS(MPI_Reduce_scatter_c, mpi_reduce_scatter_c);

/* ********************************************************* */

/* ** building blocks */

__PUK_SYM_INTERNAL
nm_mpi_request_t*nm_mpi_coll_isend(const void*buffer, nm_mpi_count_t count, nm_mpi_datatype_t*p_datatype, int dest, nm_tag_t tag, nm_mpi_communicator_t*p_comm)
{
  assert(tag & NM_MPI_TAG_PRIVATE_MASK);
  nm_mpi_request_t*p_req = nm_mpi_request_alloc_send(NM_MPI_REQUEST_SEND, count, buffer, p_datatype, tag, p_comm);
  int err = nm_mpi_isend(p_req, dest, p_comm);
  if(err != MPI_SUCCESS)
    {
      NM_MPI_FATAL_ERROR("nm_mpi_isend returned %d in collective.\n", err);
    }
  return p_req;
}

__PUK_SYM_INTERNAL
nm_mpi_request_t*nm_mpi_coll_irecv(void*buffer, nm_mpi_count_t count, nm_mpi_datatype_t*p_datatype, int source, nm_tag_t tag, nm_mpi_communicator_t*p_comm)
{
  assert(tag & NM_MPI_TAG_PRIVATE_MASK);
  nm_mpi_request_t*p_req = nm_mpi_request_alloc_recv(count, buffer, p_datatype, tag, p_comm);
  int err = nm_mpi_irecv(p_req, source, p_comm);
  if(err != MPI_SUCCESS)
    {
      NM_MPI_FATAL_ERROR("nm_mpi_irecv returned %d in collective.\n", err);
    }
  return p_req;
}

__PUK_SYM_INTERNAL
void nm_mpi_coll_wait(nm_mpi_request_t*p_req)
{
  int err = nm_mpi_request_wait(p_req);
  if(err != MPI_SUCCESS)
    {
      NM_MPI_FATAL_ERROR("nm_mpi_request_wait returned %d in collective.\n", err);
    }
  nm_mpi_request_complete(p_req, NULL);
}


/* ********************************************************* */

int mpi_barrier(MPI_Comm comm)
{
  const nm_tag_t tag = NM_MPI_TAG_PRIVATE_BARRIER;
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  if(p_comm == NULL || comm == MPI_COMM_NULL)
    return NM_MPI_ERROR_WORLD(MPI_ERR_COMM);
  nm_coll_barrier(p_comm->p_nm_comm, tag);
  return MPI_SUCCESS;
}

int mpi_ibarrier(MPI_Comm comm, MPI_Request*request)
{
  const nm_tag_t tag = NM_MPI_TAG_PRIVATE_BARRIER;
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  if(p_comm == NULL || comm == MPI_COMM_NULL)
    return NM_MPI_ERROR_WORLD(MPI_ERR_COMM);
  nm_group_t p_group = nm_comm_group(p_comm->p_nm_comm);
  const int self = nm_comm_rank(p_comm->p_nm_comm);
  nm_mpi_request_t*p_req = nm_mpi_request_alloc_icol(NM_MPI_REQUEST_IBARRIER, 0, NULL, p_comm);
  p_req->collective.p_coll_req = nm_coll_group_ibarrier(nm_comm_get_session(p_comm->p_nm_comm), p_group, self, tag);
  *request = p_req->id;
  return MPI_SUCCESS;
}

int mpi_bcast_c(void*buffer, MPI_Count count, MPI_Datatype datatype, int root, MPI_Comm comm)
{
  const nm_tag_t tag = NM_MPI_TAG_PRIVATE_BCAST;
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  if(p_comm == NULL || comm == MPI_COMM_NULL)
    return NM_MPI_ERROR_WORLD(MPI_ERR_COMM);
  nm_mpi_datatype_t*p_datatype = nm_mpi_datatype_get(datatype);
  if(p_datatype == NULL)
    return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_TYPE);
  struct nm_data_s data;
  nm_mpi_data_build(&data, buffer, p_datatype, count);
  nm_coll_data_bcast(p_comm->p_nm_comm, root, &data, tag);
  return MPI_SUCCESS;
}

int mpi_bcast(void*buffer, int count, MPI_Datatype datatype, int root, MPI_Comm comm)
{
  return mpi_bcast_c(buffer, count, datatype, root, comm);
}

int mpi_ibcast_c(void*buffer, MPI_Count count, MPI_Datatype datatype, int root, MPI_Comm comm, MPI_Request*request)
{
  const nm_tag_t tag = NM_MPI_TAG_PRIVATE_BCAST;
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  if(p_comm == NULL || comm == MPI_COMM_NULL)
    return NM_MPI_ERROR_WORLD(MPI_ERR_COMM);
  nm_mpi_datatype_t*p_datatype = nm_mpi_datatype_get(datatype);
  if(p_datatype == NULL)
    return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_TYPE);
  nm_group_t p_group = nm_comm_group(p_comm->p_nm_comm);
  const int self = nm_comm_rank(p_comm->p_nm_comm);
  struct nm_data_s data;
  nm_mpi_data_build(&data, buffer, p_datatype, count);
  nm_mpi_request_t*p_req = nm_mpi_request_alloc_icol(NM_MPI_REQUEST_IBCAST, count, p_datatype, p_comm);
  p_req->collective.p_coll_req = nm_coll_group_data_ibcast(nm_comm_get_session(p_comm->p_nm_comm), p_group, root, self, &data, tag, NULL, NULL);
  *request = p_req->id;
  return MPI_SUCCESS;
}

int mpi_ibcast(void*buffer, int count, MPI_Datatype datatype, int root, MPI_Comm comm, MPI_Request*request)
{
  return mpi_ibcast_c(buffer, count, datatype, root, comm, request);
}

int mpi_gather_c(const void*sendbuf, MPI_Count sendcount, MPI_Datatype sendtype, void*recvbuf, MPI_Count recvcount, MPI_Datatype recvtype, int root, MPI_Comm comm)
{
  const nm_tag_t tag = NM_MPI_TAG_PRIVATE_GATHER;
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  if(p_comm == NULL || comm == MPI_COMM_NULL)
    return NM_MPI_ERROR_WORLD(MPI_ERR_COMM);
  nm_mpi_datatype_t*p_recv_datatype = nm_mpi_datatype_get(recvtype);
  if(p_recv_datatype == NULL)
    return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_TYPE);
  nm_mpi_datatype_t*p_send_datatype = nm_mpi_datatype_get(sendtype);
  if((p_send_datatype == NULL) && (sendbuf != MPI_IN_PLACE))
    return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_TYPE);
  struct nm_data_s send_data;
  struct nm_data_s*p_recv_data = NULL;
  if(sendbuf != MPI_IN_PLACE)
    {
      nm_mpi_data_build(&send_data, (void*)sendbuf, p_send_datatype, sendcount);
    }
  else
    {
      nm_data_null_build(&send_data);
    }
  if(nm_comm_rank(p_comm->p_nm_comm) == root)
    {
      const int size = nm_comm_size(p_comm->p_nm_comm);
      p_recv_data = padico_malloc(size * sizeof(struct nm_data_s));
      int i;
      for(i = 0; i < size; i++)
        {
          nm_mpi_data_build(&p_recv_data[i],
                            nm_mpi_datatype_get_ptr(recvbuf, (i * recvcount), p_recv_datatype),
                            p_recv_datatype, recvcount);
        }
    }
  nm_coll_data_gather(p_comm->p_nm_comm, root, &send_data, p_recv_data, tag);
  if(p_recv_data)
    padico_free(p_recv_data);
  return MPI_SUCCESS;
}

int mpi_gather(const void*sendbuf, int sendcount, MPI_Datatype sendtype, void*recvbuf, int recvcount, MPI_Datatype recvtype, int root, MPI_Comm comm)
{
  return mpi_gather_c(sendbuf, sendcount, sendtype, recvbuf, recvcount, recvtype, root, comm);
}

int mpi_igather_c(const void*sendbuf, MPI_Count sendcount, MPI_Datatype sendtype, void*recvbuf, MPI_Count recvcount, MPI_Datatype recvtype, int root, MPI_Comm comm, MPI_Request*request)
{
  const nm_tag_t tag = NM_MPI_TAG_PRIVATE_GATHER;
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  if(p_comm == NULL || comm == MPI_COMM_NULL)
    return NM_MPI_ERROR_WORLD(MPI_ERR_COMM);
  nm_mpi_datatype_t*p_recv_datatype = nm_mpi_datatype_get(recvtype);
  if(p_recv_datatype == NULL)
    return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_TYPE);
  nm_mpi_datatype_t*p_send_datatype = nm_mpi_datatype_get(sendtype);
  if((p_send_datatype == NULL) && (sendbuf != MPI_IN_PLACE))
    return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_TYPE);
  struct nm_data_s send_data;
  struct nm_data_s*p_recv_data = NULL;
  if(sendbuf != MPI_IN_PLACE)
    {
      nm_mpi_data_build(&send_data, (void*)sendbuf, p_send_datatype, sendcount);
    }
  else
    {
      nm_data_null_build(&send_data);
    }
  if(nm_comm_rank(p_comm->p_nm_comm) == root)
    {
      const int size = nm_comm_size(p_comm->p_nm_comm);
      p_recv_data = padico_malloc(size * sizeof(struct nm_data_s));
      int i;
      for(i = 0; i < size; i++)
        {
          nm_mpi_data_build(&p_recv_data[i],
                            nm_mpi_datatype_get_ptr(recvbuf, (i * recvcount), p_recv_datatype),
                            p_recv_datatype, recvcount);
        }
    }
  nm_mpi_request_t*p_req = nm_mpi_request_alloc_icol(NM_MPI_REQUEST_IGATHER, sendcount, p_send_datatype, p_comm);
  nm_mpi_request_add_datatype2(p_req, p_recv_datatype);
  p_req->collective.p_coll_req = nm_coll_group_data_igather(nm_comm_get_session(p_comm->p_nm_comm), nm_comm_group(p_comm->p_nm_comm),
                                                 root, nm_comm_rank(p_comm->p_nm_comm),
                                                 &send_data, p_recv_data, tag, NULL, NULL);
  *request = p_req->id;
  if(p_recv_data)
    padico_free(p_recv_data);
  return MPI_SUCCESS;
}

int mpi_igather(const void*sendbuf, int sendcount, MPI_Datatype sendtype, void*recvbuf, int recvcount, MPI_Datatype recvtype, int root, MPI_Comm comm, MPI_Request*request)
{
  return mpi_igather_c(sendbuf, sendcount, sendtype, recvbuf, recvcount, recvtype, root, comm, request);
}

int mpi_gatherv_c(const void*sendbuf, MPI_Count sendcount, MPI_Datatype sendtype,
                  void*recvbuf, const MPI_Count recvcounts[], const MPI_Aint displs[], MPI_Datatype recvtype, int root, MPI_Comm comm)
{
  const nm_tag_t tag = NM_MPI_TAG_PRIVATE_GATHERV;
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  if(p_comm == NULL || comm == MPI_COMM_NULL)
    return NM_MPI_ERROR_WORLD(MPI_ERR_COMM);
  nm_mpi_datatype_t*p_recv_datatype = nm_mpi_datatype_get(recvtype);
  if(p_recv_datatype == NULL)
    return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_TYPE);
  nm_mpi_datatype_t*p_send_datatype = nm_mpi_datatype_get(sendtype);
  if((p_send_datatype == NULL) && (sendbuf != MPI_IN_PLACE))
    return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_TYPE);
  if(nm_comm_rank(p_comm->p_nm_comm) == root)
    {
      nm_mpi_request_t**requests = padico_malloc(nm_comm_size(p_comm->p_nm_comm) * sizeof(nm_mpi_request_t*));
      int i;
      /* receive data from other processes */
      for(i = 0; i < nm_comm_size(p_comm->p_nm_comm); i++)
        {
          if(i == root)
            continue;
          requests[i] = nm_mpi_coll_irecv(nm_mpi_datatype_get_ptr(recvbuf, displs[i], p_recv_datatype),
                                          recvcounts[i], p_recv_datatype, i, tag, p_comm);
        }
      for(i = 0; i < nm_comm_size(p_comm->p_nm_comm); i++)
        {
          if(i == root)
            continue;
          nm_mpi_coll_wait(requests[i]);
        }
      /* copy local data for self */
      if(sendbuf != MPI_IN_PLACE)
        {
          nm_mpi_datatype_copy(sendbuf, p_send_datatype, sendcount,
                               nm_mpi_datatype_get_ptr(recvbuf, displs[nm_comm_rank(p_comm->p_nm_comm)], p_recv_datatype), p_recv_datatype, recvcounts[nm_comm_rank(p_comm->p_nm_comm)]);
        }
      /* free memory */
      FREE_AND_SET_NULL(requests);
    }
  else
    {
      nm_mpi_request_t*p_req = nm_mpi_coll_isend(sendbuf, sendcount, p_send_datatype, root, tag, p_comm);
      nm_mpi_coll_wait(p_req);
    }
  return MPI_SUCCESS;
}

int mpi_gatherv(const void*sendbuf, int sendcount, MPI_Datatype sendtype,
                void*recvbuf, const int recvcounts[], const int displs[], MPI_Datatype recvtype, int root, MPI_Comm comm)
{
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  if(p_comm == NULL || comm == MPI_COMM_NULL)
    return NM_MPI_ERROR_WORLD(MPI_ERR_COMM);
  MPI_Count*recvcounts_count = nm_mpi_array_count_from_int(nm_comm_size(p_comm->p_nm_comm), recvcounts);
  MPI_Count*displs_aint = nm_mpi_array_aint_from_int(nm_comm_size(p_comm->p_nm_comm), displs);
  int err = mpi_gatherv_c(sendbuf, sendcount, sendtype, recvbuf, recvcounts_count, displs_aint, recvtype, root, comm);
  nm_mpi_array_count_free(recvcounts_count);
  nm_mpi_array_aint_free(displs_aint);
  return err;
}

int mpi_allgather_c(const void*sendbuf, MPI_Count sendcount, MPI_Datatype sendtype, void*recvbuf, MPI_Count recvcount, MPI_Datatype recvtype, MPI_Comm comm)
{
  const int root = 0; /* gather on node #0 then broadcast */
  const nm_tag_t tag = NM_MPI_TAG_PRIVATE_ALLGATHER;
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  if(p_comm == NULL || comm == MPI_COMM_NULL)
    return NM_MPI_ERROR_WORLD(MPI_ERR_COMM);
  nm_mpi_datatype_t*p_recv_datatype = nm_mpi_datatype_get(recvtype);
  if(p_recv_datatype == NULL)
    return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_TYPE);
  nm_mpi_datatype_t*p_send_datatype = nm_mpi_datatype_get(sendtype);
  if((p_send_datatype == NULL) && (sendbuf != MPI_IN_PLACE))
    return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_TYPE);
  /* build send & recv data descriptor for gather from allgather info.
   * There is no direct matching because of MPI_IN_PLACE.
   */
  struct nm_data_s send_data;
  struct nm_data_s*p_recv_data = NULL;
  const int self = nm_comm_rank(p_comm->p_nm_comm);
  if(sendbuf == MPI_IN_PLACE)
    {
      if(root == self)
        {
          nm_data_null_build(&send_data);
        }
      else
        {
          nm_mpi_data_build(&send_data,
                            nm_mpi_datatype_get_ptr(recvbuf, (self * recvcount), p_recv_datatype),
                            p_recv_datatype, recvcount);
        }
    }
  else
    {
      nm_mpi_data_build(&send_data, (void*)sendbuf, p_send_datatype, sendcount);
    }
  if(root == self)
    {
      const int size = nm_comm_size(p_comm->p_nm_comm);
      p_recv_data = padico_malloc(size * sizeof(struct nm_data_s));
      int i;
      for(i = 0; i < size; i++)
        {
          nm_mpi_data_build(&p_recv_data[i],
                            nm_mpi_datatype_get_ptr(recvbuf, (i * recvcount), p_recv_datatype),
                            p_recv_datatype, recvcount);
        }
    }
  else
    {
      p_recv_data = NULL;
    }
  nm_coll_data_gather(p_comm->p_nm_comm, root, &send_data, p_recv_data, tag);
  if(p_recv_data)
    padico_free(p_recv_data);
  struct nm_data_s bcast_data;
  nm_mpi_data_build(&bcast_data, recvbuf, p_recv_datatype, recvcount * nm_comm_size(p_comm->p_nm_comm));
  nm_coll_data_bcast(p_comm->p_nm_comm, root, &bcast_data, tag);
  return MPI_SUCCESS;
}

int mpi_allgather(const void*sendbuf, int sendcount, MPI_Datatype sendtype, void*recvbuf, int recvcount, MPI_Datatype recvtype, MPI_Comm comm)
{
  return mpi_allgather_c(sendbuf, sendcount, sendtype, recvbuf, recvcount, recvtype, comm);
}

int mpi_iallgather_c(const void*sendbuf, MPI_Count sendcount, MPI_Datatype sendtype, void*recvbuf, MPI_Count recvcount, MPI_Datatype recvtype, MPI_Comm comm, MPI_Request*request)
{
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  if(p_comm == NULL || comm == MPI_COMM_NULL)
    return NM_MPI_ERROR_WORLD(MPI_ERR_COMM);
  nm_mpi_datatype_t*p_recv_datatype = nm_mpi_datatype_get(recvtype);
  if(p_recv_datatype == NULL)
    return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_TYPE);
  nm_mpi_datatype_t*p_send_datatype = nm_mpi_datatype_get(sendtype);
  if((p_send_datatype == NULL) && (sendbuf != MPI_IN_PLACE))
    return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_TYPE);
  nm_mpi_request_t*p_req = nm_mpi_request_alloc_icol(NM_MPI_REQUEST_IALLGATHER, sendcount, p_send_datatype, p_comm);
  nm_mpi_request_add_datatype2(p_req, p_recv_datatype);
  p_req->collective.p_coll_req = nm_mpi_coll_iallgather(p_comm, sendbuf, sendcount, p_send_datatype, recvbuf, recvcount, p_recv_datatype);
  *request = p_req->id;
  return MPI_SUCCESS;
}

int mpi_iallgather(const void*sendbuf, int sendcount, MPI_Datatype sendtype, void*recvbuf, int recvcount, MPI_Datatype recvtype, MPI_Comm comm, MPI_Request*request)
{
  return mpi_iallgather_c(sendbuf, sendcount, sendtype, recvbuf, recvcount, recvtype, comm, request);
}

int mpi_allgatherv_c(const void*sendbuf, MPI_Count sendcount, MPI_Datatype sendtype,
                     void*recvbuf, const MPI_Count recvcounts[], const MPI_Aint displs[], MPI_Datatype recvtype, MPI_Comm comm)
{
  nm_mpi_count_t recvcount = 0;
  const int root = 0;
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  /* Broadcast the total receive count to all processes */
  if(nm_comm_rank(p_comm->p_nm_comm) == root)
    {
      int i;
      for(i = 0; i < nm_comm_size(p_comm->p_nm_comm); i++)
        {
          recvcount += recvcounts[i];
        }
    }
  int err = mpi_bcast_c(&recvcount, 1, MPI_COUNT, root, comm);
  if(err != MPI_SUCCESS)
    return NM_MPI_ERROR_COMM(p_comm, err);
  /* Gather on process 0 */
  err = mpi_gatherv_c(sendbuf, sendcount, sendtype, recvbuf, recvcounts, displs, recvtype, 0, comm);
  if(err != MPI_SUCCESS)
    return NM_MPI_ERROR_COMM(p_comm, err);
  /* Broadcast the result to all processes */
  MPI_Datatype bcast_type;
  mpi_type_indexed_c(nm_comm_size(p_comm->p_nm_comm), recvcounts, displs, recvtype, &bcast_type);
  mpi_type_commit(&bcast_type);
  err = mpi_bcast_c(recvbuf, 1, bcast_type, root, comm);
  mpi_type_free(&bcast_type);
  return NM_MPI_ERROR_COMM(p_comm, err);
}

int mpi_allgatherv(const void*sendbuf, int sendcount, MPI_Datatype sendtype,
                   void*recvbuf, const int recvcounts[], const int displs[], MPI_Datatype recvtype, MPI_Comm comm)
{
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  if(p_comm == NULL || comm == MPI_COMM_NULL)
    return NM_MPI_ERROR_WORLD(MPI_ERR_COMM);
  MPI_Count*recvcounts_count = nm_mpi_array_count_from_int(nm_comm_size(p_comm->p_nm_comm), recvcounts);
  MPI_Aint*displs_aint = nm_mpi_array_aint_from_int(nm_comm_size(p_comm->p_nm_comm), displs);
  int err = mpi_allgatherv_c(sendbuf, sendcount, sendtype, recvbuf, recvcounts_count, displs_aint, recvtype, comm);
  nm_mpi_array_count_free(recvcounts_count);
  nm_mpi_array_count_free(displs_aint);
  return err;
}

int mpi_scatter_c(const void*sendbuf, MPI_Count sendcount, MPI_Datatype sendtype,
                void*recvbuf, MPI_Count recvcount, MPI_Datatype recvtype, int root, MPI_Comm comm)
{
  const nm_tag_t tag = NM_MPI_TAG_PRIVATE_SCATTER;
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  if(p_comm == NULL || comm == MPI_COMM_NULL)
    return NM_MPI_ERROR_WORLD(MPI_ERR_COMM);
  nm_mpi_datatype_t*p_recv_datatype = nm_mpi_datatype_get(recvtype);
  if(p_recv_datatype == NULL)
    return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_TYPE);
  nm_mpi_datatype_t*p_send_datatype = nm_mpi_datatype_get(sendtype);
  if((p_send_datatype == NULL) && (sendbuf != MPI_IN_PLACE))
    return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_TYPE);
  struct nm_data_s*p_send_data = NULL;
  struct nm_data_s recv_data;
  if(recvbuf != MPI_IN_PLACE)
    {
      nm_mpi_data_build(&recv_data, recvbuf, p_recv_datatype, recvcount);
    }
  else
    {
      nm_data_null_build(&recv_data);
    }
  if(nm_comm_rank(p_comm->p_nm_comm) == root)
    {
      const int size = nm_comm_size(p_comm->p_nm_comm);
      p_send_data = padico_malloc(size * sizeof(struct nm_data_s));
      int i;
      for(i = 0; i < size; i++)
        {
          nm_mpi_data_build(&p_send_data[i],
                            nm_mpi_datatype_get_ptr((void*)sendbuf, i * sendcount, p_send_datatype),
                            p_send_datatype, sendcount);
        }
    }
  nm_coll_data_scatter(p_comm->p_nm_comm, root, p_send_data, &recv_data, tag);
  if(p_send_data)
    padico_free(p_send_data);
  return MPI_SUCCESS;
}

int mpi_scatter(const void*sendbuf, int sendcount, MPI_Datatype sendtype,
                void*recvbuf, int recvcount, MPI_Datatype recvtype, int root, MPI_Comm comm)
{
  return mpi_scatter_c(sendbuf, sendcount, sendtype, recvbuf, recvcount, recvtype, root, comm);
}

int mpi_scatterv_c(const void*sendbuf, const MPI_Count sendcounts[], const MPI_Aint displs[], MPI_Datatype sendtype,
                 void*recvbuf, MPI_Count recvcount, MPI_Datatype recvtype, int root, MPI_Comm comm)
{
  const nm_tag_t tag = NM_MPI_TAG_PRIVATE_SCATTERV;
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  if(p_comm == NULL || comm == MPI_COMM_NULL)
    return NM_MPI_ERROR_WORLD(MPI_ERR_COMM);
  nm_mpi_datatype_t*p_recv_datatype = nm_mpi_datatype_get(recvtype);
  if(p_recv_datatype == NULL)
    return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_TYPE);
  nm_mpi_datatype_t*p_send_datatype = nm_mpi_datatype_get(sendtype);
  if((p_send_datatype == NULL) && (sendbuf != MPI_IN_PLACE))
    return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_TYPE);
  if (nm_comm_rank(p_comm->p_nm_comm) == root)
    {
      int i;
      nm_mpi_request_t**requests = padico_malloc(nm_comm_size(p_comm->p_nm_comm) * sizeof(nm_mpi_request_t*));
      // send data to other processes
      for(i = 0; i < nm_comm_size(p_comm->p_nm_comm); i++)
        {
          if(i == root) continue;
          requests[i] = nm_mpi_coll_isend(nm_mpi_datatype_get_ptr((void*)sendbuf, displs[i], p_send_datatype),
                                          sendcounts[i], p_send_datatype, i, tag, p_comm);
        }
      for(i = 0; i < nm_comm_size(p_comm->p_nm_comm); i++)
        {
          if(i == root) continue;
          nm_mpi_coll_wait(requests[i]);
        }
      // copy local data for self
      if(sendbuf != MPI_IN_PLACE)
        {
          nm_mpi_datatype_copy(nm_mpi_datatype_get_ptr((void*)sendbuf, displs[root], p_recv_datatype), p_send_datatype, sendcounts[root],
                               recvbuf, p_recv_datatype, recvcount);
        }
      FREE_AND_SET_NULL(requests);
    }
  else
    {
      nm_mpi_request_t*p_req = nm_mpi_coll_irecv(recvbuf, recvcount, p_recv_datatype, root, tag, p_comm);
      nm_mpi_coll_wait(p_req);
    }
  return MPI_SUCCESS;
}

int mpi_scatterv(const void*sendbuf, const int sendcounts[], const int displs[], MPI_Datatype sendtype,
                 void*recvbuf, int recvcount, MPI_Datatype recvtype, int root, MPI_Comm comm)
{
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  if(p_comm == NULL || comm == MPI_COMM_NULL)
    return NM_MPI_ERROR_WORLD(MPI_ERR_COMM);
  MPI_Count*sendcounts_count = nm_mpi_array_count_from_int(nm_comm_size(p_comm->p_nm_comm), sendcounts);
  MPI_Aint*displs_aint = nm_mpi_array_aint_from_int(nm_comm_size(p_comm->p_nm_comm), displs);
  int err = mpi_scatterv_c(sendbuf, sendcounts_count, displs_aint, sendtype, recvbuf, recvcount, recvtype, root, comm);
  nm_mpi_array_count_free(sendcounts_count);
  nm_mpi_array_aint_free(displs_aint);
  return err;
}

int mpi_alltoall_c(const void*sendbuf, MPI_Count sendcount, MPI_Datatype sendtype,
                   void*recvbuf, MPI_Count recvcount, MPI_Datatype recvtype, MPI_Comm comm)
{
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  if(p_comm == NULL || comm == MPI_COMM_NULL)
    return NM_MPI_ERROR_WORLD(MPI_ERR_COMM);
  nm_mpi_datatype_t*p_recv_datatype = nm_mpi_datatype_get(recvtype);
  if(p_recv_datatype == NULL)
    return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_TYPE);
  nm_mpi_datatype_t*p_send_datatype = nm_mpi_datatype_get(sendtype);
  if((p_send_datatype == NULL) && (sendbuf != MPI_IN_PLACE))
    return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_TYPE);
  struct nm_coll_req_s*p_coll_req = nm_mpi_coll_ialltoall(p_comm, sendbuf, sendcount, p_send_datatype,
                                                          recvbuf, recvcount, p_recv_datatype);
  nm_coll_req_wait(p_coll_req);
  return MPI_SUCCESS;
}

int mpi_alltoall(const void*sendbuf, int sendcount, MPI_Datatype sendtype,
                 void*recvbuf, int recvcount, MPI_Datatype recvtype, MPI_Comm comm)
{
  return mpi_alltoall_c(sendbuf, sendcount, sendtype, recvbuf, recvcount, recvtype, comm);
}

int mpi_ialltoall_c(const void*sendbuf, MPI_Count sendcount, MPI_Datatype sendtype,
                    void*recvbuf, MPI_Count recvcount, MPI_Datatype recvtype, MPI_Comm comm, MPI_Request*request)
{
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  if(p_comm == NULL || comm == MPI_COMM_NULL)
    return NM_MPI_ERROR_WORLD(MPI_ERR_COMM);
  nm_mpi_datatype_t*p_recv_datatype = nm_mpi_datatype_get(recvtype);
  if(p_recv_datatype == NULL)
    return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_TYPE);
  nm_mpi_datatype_t*p_send_datatype = nm_mpi_datatype_get(sendtype);
  if((p_send_datatype == NULL) && (sendbuf != MPI_IN_PLACE))
    return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_TYPE);
  nm_mpi_request_t*p_req = nm_mpi_request_alloc_icol(NM_MPI_REQUEST_IALLTOALL, sendcount, p_send_datatype, p_comm);
  nm_mpi_request_add_datatype2(p_req, p_recv_datatype);
  p_req->collective.p_coll_req = nm_mpi_coll_ialltoall(p_comm, sendbuf, sendcount, p_send_datatype,
                                            recvbuf, recvcount, p_recv_datatype);
  *request = p_req->id;
  return MPI_SUCCESS;
}

int mpi_ialltoall(const void*sendbuf, int sendcount, MPI_Datatype sendtype,
                  void*recvbuf, int recvcount, MPI_Datatype recvtype, MPI_Comm comm, MPI_Request*request)
{
  return mpi_ialltoall_c(sendbuf, sendcount, sendtype, recvbuf, recvcount, recvtype, comm, request);
}

int mpi_alltoallv_c(const void*sendbuf, const MPI_Count sendcounts[], const MPI_Aint sdispls[], MPI_Datatype sendtype,
                  void*recvbuf, const MPI_Count recvcounts[], const MPI_Aint rdispls[], MPI_Datatype recvtype, MPI_Comm comm)
{
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  if(p_comm == NULL || comm == MPI_COMM_NULL)
    return NM_MPI_ERROR_WORLD(MPI_ERR_COMM);
  nm_mpi_datatype_t*p_recv_datatype = nm_mpi_datatype_get(recvtype);
  if(p_recv_datatype == NULL)
    return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_TYPE);
  nm_mpi_datatype_t*p_send_datatype = nm_mpi_datatype_get(sendtype);
  if((p_send_datatype == NULL) && (sendbuf != MPI_IN_PLACE))
    return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_TYPE);
  struct nm_coll_req_s*p_coll_req =
    nm_mpi_coll_ialltoallv(p_comm, sendbuf, sendcounts, sdispls, p_send_datatype,
                           recvbuf, recvcounts, rdispls, p_recv_datatype);
  nm_coll_req_wait(p_coll_req);
  return MPI_SUCCESS;
}

int mpi_alltoallv(const void*sendbuf, const int sendcounts[], const int sdispls[], MPI_Datatype sendtype,
                  void*recvbuf, const int recvcounts[], const int rdispls[], MPI_Datatype recvtype, MPI_Comm comm)
{
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  if(p_comm == NULL || comm == MPI_COMM_NULL)
    return NM_MPI_ERROR_WORLD(MPI_ERR_COMM);
  MPI_Count*sendcounts_count = nm_mpi_array_count_from_int(nm_comm_size(p_comm->p_nm_comm), sendcounts);
  MPI_Count*recvcounts_count = nm_mpi_array_count_from_int(nm_comm_size(p_comm->p_nm_comm), recvcounts);
  MPI_Aint*sdispls_aint = nm_mpi_array_aint_from_int(nm_comm_size(p_comm->p_nm_comm), sdispls);
  MPI_Aint*rdispls_aint = nm_mpi_array_aint_from_int(nm_comm_size(p_comm->p_nm_comm), rdispls);
  int err = mpi_alltoallv_c(sendbuf, sendcounts_count, sdispls_aint, sendtype, recvbuf, recvcounts_count, rdispls_aint, recvtype, comm);
  nm_mpi_array_count_free(sendcounts_count);
  nm_mpi_array_count_free(recvcounts_count);
  nm_mpi_array_count_free(sdispls_aint);
  nm_mpi_array_count_free(rdispls_aint);
  return err;
}

int mpi_ialltoallv_c(const void*sendbuf, const MPI_Count sendcounts[], const MPI_Aint sdispls[], MPI_Datatype sendtype,
                   void*recvbuf, const MPI_Count recvcounts[], const MPI_Aint rdispls[], MPI_Datatype recvtype,
                   MPI_Comm comm, MPI_Request*request)
{
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  if(p_comm == NULL || comm == MPI_COMM_NULL)
    return NM_MPI_ERROR_WORLD(MPI_ERR_COMM);
  nm_mpi_datatype_t*p_recv_datatype = nm_mpi_datatype_get(recvtype);
  if(p_recv_datatype == NULL)
    return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_TYPE);
  nm_mpi_datatype_t*p_send_datatype = nm_mpi_datatype_get(sendtype);
  if((p_send_datatype == NULL) && (sendbuf != MPI_IN_PLACE))
    return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_TYPE);
  nm_mpi_request_t*p_req = nm_mpi_request_alloc_icol(NM_MPI_REQUEST_IALLTOALLV, 0, p_send_datatype, p_comm);
  nm_mpi_request_add_datatype2(p_req, p_recv_datatype);
  p_req->collective.p_coll_req = nm_mpi_coll_ialltoallv(p_comm, sendbuf, sendcounts, sdispls, p_send_datatype,
                                             recvbuf, recvcounts, rdispls, p_recv_datatype);
  *request = p_req->id;
  return MPI_SUCCESS;
}

int mpi_ialltoallv(const void*sendbuf, const int sendcounts[], const int sdispls[], MPI_Datatype sendtype,
                   void*recvbuf, const int recvcounts[], const int rdispls[], MPI_Datatype recvtype,
                   MPI_Comm comm, MPI_Request*request)
{
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  if(p_comm == NULL || comm == MPI_COMM_NULL)
    return NM_MPI_ERROR_WORLD(MPI_ERR_COMM);
  MPI_Count*sendcounts_count = nm_mpi_array_count_from_int(nm_comm_size(p_comm->p_nm_comm), sendcounts);
  MPI_Count*recvcounts_count = nm_mpi_array_count_from_int(nm_comm_size(p_comm->p_nm_comm), recvcounts);
  MPI_Aint*sdispls_aint = nm_mpi_array_aint_from_int(nm_comm_size(p_comm->p_nm_comm), sdispls);
  MPI_Aint*rdispls_aint = nm_mpi_array_aint_from_int(nm_comm_size(p_comm->p_nm_comm), rdispls);
  int err = mpi_ialltoallv_c(sendbuf, sendcounts_count, sdispls_aint, sendtype, recvbuf, recvcounts_count, rdispls_aint, recvtype, comm, request);
  nm_mpi_array_count_free(sendcounts_count);
  nm_mpi_array_count_free(recvcounts_count);
  nm_mpi_array_count_free(sdispls_aint);
  nm_mpi_array_count_free(rdispls_aint);
  return err;
}

int mpi_reduce_c(const void*sendbuf, void*recvbuf, MPI_Count count, MPI_Datatype datatype, MPI_Op op, int root, MPI_Comm comm)
{
  const nm_tag_t tag = NM_MPI_TAG_PRIVATE_REDUCE;
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  if(comm == MPI_COMM_NULL)
    {
      return NM_MPI_ERROR_WORLD(MPI_ERR_COMM);
    }
  if(op == MPI_OP_NULL || op == MPI_REPLACE || op == MPI_NO_OP)
    {
      return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_OP);
    }
  nm_mpi_operator_t*p_operator = nm_mpi_operator_get(op);
  if((p_operator->function == NULL) && (p_operator->function_c == NULL))
    {
      NM_MPI_FATAL_ERROR("Operation %d not implemented\n", op);
      return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_INTERN);
    }
  nm_mpi_datatype_t*p_datatype = nm_mpi_datatype_get(datatype);
  if(p_datatype == NULL)
    return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_TYPE);
  int err = nm_mpi_coll_reduce(p_comm, root, count, p_datatype, sendbuf, recvbuf, tag, p_operator);
  return NM_MPI_ERROR_COMM(p_comm, err);
}

int mpi_reduce(const void*sendbuf, void*recvbuf, int count, MPI_Datatype datatype, MPI_Op op, int root, MPI_Comm comm)
{
  return mpi_reduce_c(sendbuf, recvbuf, count, datatype, op, root, comm);
}

int mpi_reduce_local_c(const void*sendbuf, void*recvbuf, MPI_Count count, MPI_Datatype datatype, MPI_Op op)
{
  if(op == MPI_OP_NULL || op == MPI_REPLACE || op == MPI_NO_OP)
    {
      return NM_MPI_ERROR_WORLD(MPI_ERR_OP);
    }
  nm_mpi_operator_t*p_operator = nm_mpi_operator_get(op);
  if((p_operator->function == NULL) && (p_operator->function_c == NULL))
    {
      NM_MPI_FATAL_ERROR("Operation %d not implemented\n", op);
      return NM_MPI_ERROR_WORLD(MPI_ERR_INTERN);
    }
  nm_mpi_datatype_t*p_datatype = nm_mpi_datatype_get(datatype);
  if(p_datatype == NULL)
    return NM_MPI_ERROR_WORLD(MPI_ERR_TYPE);
  nm_mpi_operator_apply(&sendbuf, recvbuf, count, p_datatype, p_operator);
  return MPI_SUCCESS;
}

int mpi_reduce_local(const void*sendbuf, void*recvbuf, int count, MPI_Datatype datatype, MPI_Op op)
{
  return MPI_Reduce_local(sendbuf, recvbuf, count, datatype, op);
}

int mpi_ireduce_c(const void*sendbuf, void*recvbuf, MPI_Count count, MPI_Datatype datatype, MPI_Op op, int root, MPI_Comm comm, MPI_Request*request)
{
  const nm_tag_t tag = NM_MPI_TAG_PRIVATE_REDUCE;
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  if(p_comm == NULL || comm == MPI_COMM_NULL)
    return NM_MPI_ERROR_WORLD(MPI_ERR_COMM);
  if(op == MPI_OP_NULL || op == MPI_REPLACE || op == MPI_NO_OP)
    {
      return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_OP);
    }
  nm_mpi_operator_t*p_operator = nm_mpi_operator_get(op);
  nm_mpi_datatype_t*p_datatype = nm_mpi_datatype_get(datatype);
  if(p_datatype == NULL)
    return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_TYPE);
  assert(p_datatype->is_contig);
  assert(p_datatype->size == p_datatype->extent);
  if((p_operator->function == NULL) && (p_operator->function_c == NULL))
    {
      NM_MPI_FATAL_ERROR("Operation %d not implemented\n", op);
      return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_INTERN);
    }
  nm_mpi_request_t*p_req = nm_mpi_request_alloc_icol(NM_MPI_REQUEST_IREDUCE, count, p_datatype, p_comm);
  p_req->collective.p_coll_req = nm_mpi_coll_ireduce_init(p_comm, root, count, p_datatype, sendbuf, recvbuf, tag, p_operator, NULL, NULL);
  nm_mpi_coll_ireduce_start(p_req->collective.p_coll_req);
  assert(p_req->collective.p_coll_req != NULL);
  *request = p_req->id;
  return MPI_SUCCESS;
}

int mpi_ireduce(const void*sendbuf, void*recvbuf, int count, MPI_Datatype datatype, MPI_Op op, int root, MPI_Comm comm, MPI_Request*request)
{
  return mpi_ireduce_c(sendbuf, recvbuf, count, datatype, op, root, comm, request);
}

int mpi_scan_c(const void*sendbuf, void*recvbuf, MPI_Count count, MPI_Datatype datatype, MPI_Op op, MPI_Comm comm)
{
  const nm_tag_t tag = NM_MPI_TAG_PRIVATE_SCAN;
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  if(p_comm == NULL || comm == MPI_COMM_NULL)
    return NM_MPI_ERROR_WORLD(MPI_ERR_COMM);
  if(op == MPI_OP_NULL || op == MPI_REPLACE || op == MPI_NO_OP)
    {
      return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_OP);
    }
  nm_mpi_operator_t*p_operator = nm_mpi_operator_get(op);
  nm_mpi_datatype_t*p_datatype = nm_mpi_datatype_get(datatype);
  if(p_datatype == NULL)
    return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_TYPE);
  assert(p_datatype->is_contig);
  assert(p_datatype->size == p_datatype->extent);
  if((p_operator->function == NULL) && (p_operator->function_c == NULL))
    {
      NM_MPI_FATAL_ERROR("Operation %d not implemented\n", op);
      return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_INTERN);
    }
  const int rank = nm_comm_rank(p_comm->p_nm_comm);
  const nm_len_t slot_size = count * nm_mpi_datatype_size(p_datatype);
  char*buf = NULL;
  nm_mpi_request_t*p_sreq = NULL;
  nm_mpi_request_t*p_rreq = NULL;
  if(rank > 0)
    {
      buf = padico_malloc(slot_size);
      p_rreq = nm_mpi_coll_irecv(buf, count, p_datatype, rank - 1, tag, p_comm);
    }
  if(sendbuf != MPI_IN_PLACE)
    memcpy(recvbuf, sendbuf, slot_size);
  if(rank > 0)
    {
      nm_mpi_coll_wait(p_rreq);
      nm_mpi_operator_apply(&buf, recvbuf, count, p_datatype, p_operator);
      FREE_AND_SET_NULL(buf);
    }
  if(rank !=  nm_comm_size(p_comm->p_nm_comm) - 1)
    {
      p_sreq = nm_mpi_coll_isend(recvbuf, count, p_datatype, rank + 1, tag, p_comm);
      nm_mpi_coll_wait(p_sreq);
    }
  return MPI_SUCCESS;
}

int mpi_scan(const void*sendbuf, void*recvbuf, int count, MPI_Datatype datatype, MPI_Op op, MPI_Comm comm)
{
  return mpi_scan_c(sendbuf, recvbuf, count, datatype, op, comm);
}

int mpi_allreduce_c(const void*sendbuf, void*recvbuf, MPI_Count count, MPI_Datatype datatype, MPI_Op op, MPI_Comm comm)
{
  const int root = 0;
  const nm_tag_t tag = NM_MPI_TAG_PRIVATE_ALLREDUCE;
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  if(p_comm == NULL || comm == MPI_COMM_NULL)
    return NM_MPI_ERROR_WORLD(MPI_ERR_COMM);
  if(op == MPI_OP_NULL || op == MPI_REPLACE || op == MPI_NO_OP)
    {
      return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_OP);
    }
  nm_mpi_operator_t*p_operator = nm_mpi_operator_get(op);
  if((p_operator->function == NULL) && (p_operator->function_c == NULL))
    {
      NM_MPI_FATAL_ERROR("Operation %d not implemented\n", op);
      return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_INTERN);
    }
  nm_mpi_datatype_t*p_datatype = nm_mpi_datatype_get(datatype);
  if(p_datatype == NULL)
    return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_TYPE);
  /* reduce on node 0 */
  int err = nm_mpi_coll_reduce(p_comm, root, count, p_datatype, sendbuf, recvbuf, tag, p_operator);
  if(err != MPI_SUCCESS)
    return NM_MPI_ERROR_COMM(p_comm, err);
  /* broadcast result */
  struct nm_data_s data;
  nm_mpi_data_build(&data, recvbuf, p_datatype, count);
  nm_coll_data_bcast(p_comm->p_nm_comm, root, &data, tag);
  return MPI_SUCCESS;
}

int mpi_allreduce(const void*sendbuf, void*recvbuf, int count, MPI_Datatype datatype, MPI_Op op, MPI_Comm comm)
{
  return mpi_allreduce_c(sendbuf, recvbuf, count, datatype, op, comm);
}

int mpi_iallreduce_c(const void*sendbuf, void*recvbuf, MPI_Count count, MPI_Datatype datatype, MPI_Op op, MPI_Comm comm, MPI_Request*request)
{
  const nm_tag_t tag = NM_MPI_TAG_PRIVATE_ALLREDUCE;
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  if(p_comm == NULL || comm == MPI_COMM_NULL)
    return NM_MPI_ERROR_WORLD(MPI_ERR_COMM);
  if(op == MPI_OP_NULL || op == MPI_REPLACE || op == MPI_NO_OP)
    {
      return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_OP);
    }
  nm_mpi_operator_t*p_operator = nm_mpi_operator_get(op);
  if((p_operator->function == NULL) && (p_operator->function_c == NULL))
    {
      NM_MPI_FATAL_ERROR("Operation %d not implemented\n", op);
      return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_INTERN);
    }
  nm_mpi_datatype_t*p_datatype = nm_mpi_datatype_get(datatype);
  if(p_datatype == NULL)
    return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_TYPE);
  nm_mpi_request_t*p_req = nm_mpi_request_alloc_icol(NM_MPI_REQUEST_IALLREDUCE, count, p_datatype, p_comm);
  p_req->collective.p_coll_req = nm_mpi_coll_iallreduce(p_comm, count, p_datatype, sendbuf, recvbuf, tag, p_operator);
  *request = p_req->id;
  return MPI_SUCCESS;
}

int mpi_iallreduce(const void*sendbuf, void*recvbuf, int count, MPI_Datatype datatype, MPI_Op op, MPI_Comm comm, MPI_Request*request)
{
  return mpi_iallreduce_c(sendbuf, recvbuf, count, datatype, op, comm, request);
}

int mpi_reduce_scatter_c(const void*sendbuf, void*recvbuf, const MPI_Count recvcounts[], MPI_Datatype datatype, MPI_Op op, MPI_Comm comm)
{
  const nm_tag_t tag = NM_MPI_TAG_PRIVATE_REDUCESCATTER;
  nm_mpi_count_t count = 0, i;
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  nm_mpi_datatype_t*p_datatype = nm_mpi_datatype_get(datatype);
  if(p_comm == NULL || comm == MPI_COMM_NULL)
    return NM_MPI_ERROR_WORLD(MPI_ERR_COMM);
  if(p_datatype == NULL)
    return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_TYPE);
  void*reducebuf = NULL;
  assert(p_datatype->is_contig);
  for(i = 0; i < nm_comm_size(p_comm->p_nm_comm) ; i++)
    {
      count += recvcounts[i];
    }
  if(nm_comm_rank(p_comm->p_nm_comm) == 0)
    {
      reducebuf = padico_malloc(count * p_datatype->size);
    }
  mpi_reduce(sendbuf, reducebuf, count, datatype, op, 0, comm);

  // Scatter the result
  if (nm_comm_rank(p_comm->p_nm_comm) == 0)
    {
      nm_mpi_request_t**requests = padico_malloc(nm_comm_size(p_comm->p_nm_comm) * sizeof(nm_mpi_request_t*));
      nm_mpi_count_t cum_sum_recvcounts = recvcounts[0];
      // send data to other processes
      for(i = 1 ; i < nm_comm_size(p_comm->p_nm_comm); i++)
        {
          requests[i] = nm_mpi_coll_isend(nm_mpi_datatype_get_ptr(reducebuf, cum_sum_recvcounts, p_datatype),
                                          recvcounts[i], p_datatype, i, tag, p_comm);
          cum_sum_recvcounts += recvcounts[i];
          assert(cum_sum_recvcounts <= count);
        }
      for(i = 1; i < nm_comm_size(p_comm->p_nm_comm); i++)
        {
          nm_mpi_coll_wait(requests[i]);
        }
      // copy local data for self
      memcpy(recvbuf, reducebuf, recvcounts[0] * p_datatype->extent);
      FREE_AND_SET_NULL(requests);
    }
  else
    {
      nm_mpi_request_t*p_req = nm_mpi_coll_irecv(recvbuf, recvcounts[nm_comm_rank(p_comm->p_nm_comm)], p_datatype, 0, tag, p_comm);
      nm_mpi_coll_wait(p_req);
    }
  if (nm_comm_rank(p_comm->p_nm_comm) == 0)
    {
      FREE_AND_SET_NULL(reducebuf);
    }
  return MPI_SUCCESS;
}

int mpi_reduce_scatter(const void*sendbuf, void*recvbuf, const int recvcounts[], MPI_Datatype datatype, MPI_Op op, MPI_Comm comm)
{
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  if(p_comm == NULL || comm == MPI_COMM_NULL)
    return NM_MPI_ERROR_WORLD(MPI_ERR_COMM);
  MPI_Count*recvcounts_count = nm_mpi_array_count_from_int(nm_comm_size(p_comm->p_nm_comm), recvcounts);
  int err = mpi_reduce_scatter_c(sendbuf, recvbuf, recvcounts_count, datatype, op, comm);
  nm_mpi_array_count_free(recvcounts_count);
  return err;
}

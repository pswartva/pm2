/*
 * NewMadeleine
 * Copyright (C) 2014-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <nm_private.h>

PADICO_MODULE_HOOK(nmad);

PADICO_MODULE_ATTR(trace, "NMAD_TRACE", "Control the content of traces; default is: all,^core",
                   string, "all,^core");

PADICO_MODULE_ATTR(trace_suffix, "NMAD_TRACE_SUFFIX", "Add a suffix to the default trace file name",
                   string, "");

PADICO_MODULE_ATTR(trace_path, "NMAD_TRACE_PATH", "Directory where to save the trace file; default is '.'",
                   string, ".");

#ifdef NMAD_TRACE

#include <nm_coll_interface.h>
#include <nm_sync_clocks_interface.h>
#include <stdio.h>
#include <string.h>
#include <GTG.h>

/** maximum number of events */
#define NM_TRACE_MAX (1024*1024*8)

#define CHECK_RETURN(val) { if (val!=TRACE_SUCCESS){NM_FATAL("function failed line %d.\n", __LINE__);} }

PUK_VECT_TYPE(nm_trace_object_container, uintptr_t);

/** a trace event, as captured */
struct nm_trace_capture_entry_s
{
  double local_time;       /**< time of event in local time */
  nm_trace_event_t event;
  uintptr_t value;
  nm_gate_t p_gate;
  nm_drv_t p_drv;
  nm_core_tag_t core_tag;
  nm_seq_t seq;
  nm_len_t len;            /**< length of data in request; NM_LEN_UNDEFINED if unknown */
};

/** a packed trace event */
struct nm_trace_packed_entry_s
{
  double global_time;
  nm_trace_event_t event;
  uintptr_t value;
  int gate_id;
  int driver_id;
  nm_core_tag_t core_tag;
  nm_seq_t seq;
  nm_len_t len;
};

struct nm_trace_packed_s
{
  struct nm_trace_packed_entry_s*p_entries;
  int n_entries;
  char**p_drivers;
  int n_drivers;
  int n_gates;
  int rank;
};

static struct
{
  nm_sync_clocks_t p_clocks;
  int tracing;   /**< whether we are currently tracing */
  struct nm_trace_capture_entry_s entries[NM_TRACE_MAX];
  int last;      /**< last allocated entry in the table */
  int n_gates;   /**< number of gates; 0 .. n_gates - 1 */
  nm_comm_t p_comm;
  int rank;
  int size;
  int filters;
} nm_trace = { .p_clocks = NULL, .tracing = 0, .last = 0, .filters = 0 };

static struct nm_trace_capture_entry_s*nm_trace_get_entry(void)
{
  if(nm_trace.tracing)
    {
      const int i = __sync_fetch_and_add(&nm_trace.last, 1);
      if(i >= NM_TRACE_MAX)
        {
          nm_trace.tracing = 0;
          nm_trace.last = NM_TRACE_MAX;
          NM_DISPF("trace buffer full. Stop capturing traces.\n");
          return NULL;
        }
      return &nm_trace.entries[i];
    }
  else
    {
      return NULL;
    }
}


void nm_trace_event(nm_trace_event_t event, void*value, nm_gate_t p_gate, nm_drv_t p_drv, nm_core_tag_t core_tag, nm_seq_t seq, nm_len_t len)
{
  if((event & nm_trace.filters) != 0)
    {
      struct nm_trace_capture_entry_s*p_entry = nm_trace_get_entry();
      if(p_entry)
        {
          p_entry->local_time = nm_sync_clocks_get_time_usec(nm_trace.p_clocks);
          p_entry->event  = event;
          p_entry->value  = (uintptr_t)value;
          p_entry->p_gate = p_gate;
          p_entry->p_drv  = p_drv;
          p_entry->core_tag = core_tag;
          p_entry->seq    = seq;
          p_entry->len    = len;
        }
    }
}

void nm_trace_state(nm_trace_event_t event, nm_gate_t p_gate, nm_drv_t p_drv)
{
  nm_trace_event(event, NULL, p_gate, p_drv, NM_CORE_TAG_NONE, NM_SEQ_NONE, NM_LEN_UNDEFINED);
}

void nm_trace_var(nm_trace_event_t event, int _value, nm_gate_t p_gate, nm_drv_t p_drv)
{
  uintptr_t value = (uintptr_t)_value;
  nm_trace_event(event, (void*)value, p_gate, p_drv, NM_CORE_TAG_NONE, NM_SEQ_NONE, NM_LEN_UNDEFINED);
}

static struct nm_trace_packed_s*nm_trace_pack(nm_core_t p_core)
{
  struct nm_trace_packed_s*p_packed = padico_malloc(sizeof(struct nm_trace_packed_s));
  p_packed->rank = nm_trace.rank;
  p_packed->p_entries = padico_malloc(sizeof(struct nm_trace_packed_entry_s) * nm_trace.last);
  p_packed->n_entries = nm_trace.last;
  p_packed->n_gates = nm_trace.size;
  p_packed->n_drivers = 0;
  p_packed->p_drivers = NULL;
  nm_drv_t p_drv;
  NM_FOR_EACH_DRIVER(p_drv, p_core)
    {
      p_packed->n_drivers++;
      p_packed->p_drivers = realloc(p_packed->p_drivers, p_packed->n_drivers * sizeof(char*));
      p_packed->p_drivers[p_packed->n_drivers - 1] = padico_strdup(p_drv->props.nickname);
    }
  int i;
  for(i = 0; i < nm_trace.last; i++)
    {
      struct nm_trace_packed_entry_s*p = &p_packed->p_entries[i];
      const struct nm_trace_capture_entry_s*c = &nm_trace.entries[i];
      p->global_time = nm_sync_clocks_local_to_global(nm_trace.p_clocks, c->local_time);
      p->event = c->event;
      p->value = c->value;
      if(c->p_gate != NULL)
        {
          p->gate_id = nm_comm_get_dest(nm_trace.p_comm, c->p_gate);
        }
      else
        {
          p->gate_id = -1;
        }
      p->driver_id = -1;
      if(c->p_drv != NULL)
        {
          int d;
          for(d = 0; d < p_packed->n_drivers; d++)
            {
              if(strcmp(p_packed->p_drivers[d], c->p_drv->props.nickname) == 0)
                {
                  p->driver_id = d;
                  break;
                }
            }
        }
      p->core_tag = c->core_tag;
      p->seq = c->seq;
      p->len = (c->len == NM_LEN_UNDEFINED) ? -1 : c->len;
    }
  return p_packed;
}

static void nm_trace_packed_free(struct nm_trace_packed_s*p_packed)
{
  padico_free(p_packed->p_entries);
  padico_free(p_packed->p_drivers);
  padico_free(p_packed);
}

static void nm_trace_packed_send(struct nm_trace_packed_s*p_packed, nm_gate_t p_gate)
{
  nm_session_t p_session = nm_comm_get_session(nm_trace.p_comm);
  nm_sr_send(p_session, p_gate, 0, p_packed, sizeof(struct nm_trace_packed_s));
  int i;
  for(i = 0; i < p_packed->n_drivers; i++)
    {
      nm_sr_send(p_session, p_gate, 0, p_packed->p_drivers[i], strlen(p_packed->p_drivers[i]) + 1);
    }
  nm_sr_send(p_session, p_gate, 0, p_packed->p_entries, p_packed->n_entries * sizeof(struct nm_trace_packed_entry_s));
}

static struct nm_trace_packed_s*nm_trace_packed_recv(nm_gate_t p_gate)
{
  struct nm_trace_packed_s*p_packed = padico_malloc(sizeof(struct nm_trace_packed_s));
  nm_session_t p_session = nm_comm_get_session(nm_trace.p_comm);
  nm_sr_recv(p_session, p_gate, 0, p_packed, sizeof(struct nm_trace_packed_s));
  p_packed->p_drivers = padico_malloc(sizeof(char*) * p_packed->n_drivers);
  int i;
  for(i = 0; i < p_packed->n_drivers; i++)
    {
      nm_sr_request_t req;
      nm_sr_recv_init(p_session, &req);
      nm_sr_recv_match(p_session, &req, p_gate, 0, NM_TAG_MASK_FULL);
      nm_sr_recv_post(p_session, &req);
      nm_sr_recv_data_size_wait(p_session, &req);
      nm_len_t out_len;
      nm_sr_request_get_expected_size(&req, &out_len);
      p_packed->p_drivers[i] = padico_malloc(out_len);
      nm_sr_recv_unpack_contiguous(p_session, &req, p_packed->p_drivers[i], out_len);
    }
  p_packed->p_entries = padico_malloc(sizeof(struct nm_trace_packed_entry_s) * p_packed->n_entries);
  nm_sr_recv(p_session, p_gate, 0, p_packed->p_entries, p_packed->n_entries * sizeof(struct nm_trace_packed_entry_s));
  return p_packed;
}

static nm_trace_object_container_vect_itor_t nm_trace_object_find_slot(struct nm_trace_object_container_vect_s*p_objects, uintptr_t value,
                                                                       int rank, const char*object_type, const char*container_type, const char*parent_container)
{
   nm_trace_object_container_vect_itor_t i = nm_trace_object_container_vect_find(p_objects, value);
   if(i == NULL)
     {
       i = nm_trace_object_container_vect_find(p_objects, (uintptr_t)NULL);
       if(i == NULL)
         {
           padico_string_t cont_obj = padico_string_new();
           padico_string_printf(cont_obj, "n%d_%s#%d", rank, object_type, nm_trace_object_container_vect_size(p_objects));
           addContainer(0.0, padico_string_get(cont_obj), container_type, parent_container, padico_string_get(cont_obj), "0");
           padico_string_delete(cont_obj);
           i = nm_trace_object_container_vect_push_back(p_objects, value);
         }
       else
         {
           *i = value;
         }
     }
   return i;
}

/** gen trace global header */
static void nm_trace_header(void)
{
  AddComment("nmad build string: " NMAD_BUILD_STRING);
  AddComment("nmad config: " NMAD_CONFIG_STRING);
  AddComment("NMAD_ROOT = " NMAD_ROOT);

  /* container types */
  CHECK_RETURN(addContType("Container_World", "0", "Container_World"));
  CHECK_RETURN(addContType("Container_Node", "Container_World", "Container_Node"));
  CHECK_RETURN(addContType("Container_Pack", "Container_Node", "Container_Pack"));
  CHECK_RETURN(addContType("Container_Unpack", "Container_Node", "Container_Unpack"));
  CHECK_RETURN(addContType("Container_Core", "Container_Node", "Container_Core"));
  CHECK_RETURN(addContType("Container_Driver", "Container_Node", "Container_Driver"));
  CHECK_RETURN(addContType("Container_Pw", "Container_Node", "Container_Pw"));

  /* links */
  addLinkType("nm_core_pack_link", "nm_core_pack_link", "Container_World", "Container_Node", "Container_Node");

  /* states for pack/unpack/pw */
  addStateType("Pack_state", "Container_Pack", "Pack_state");
  addStateType("Unpack_state", "Container_Pack", "Unpack_state");
  addStateType("Pw_state", "Container_Pw", "Pw_state");

  /* entities for pack */
  addEntityValue("pack_submit",      "Pack_state", "pack_submit",    GTG_LIGHTBROWN);
  addEntityValue("pack_flush",       "Pack_state", "pack_flush",     GTG_GREEN);
  addEntityValue("pack_rdv",         "Pack_state", "pack_rdv",       GTG_PINK);
  addEntityValue("pack_in_pw",       "Pack_state", "pack_in_pw",     GTG_BLUE);
  addEntityValue("pack_pw_posted",   "Pack_state", "pack_pw_posted", GTG_RED);
  addEntityValue("pack_completed",   "Pack_state", "pack_completed", GTG_BLACK);

  /* entities for unpack */
  addEntityValue("unpack_submit",    "Unpack_state", "unpack_submit",    GTG_GREEN);
  addEntityValue("unpack_rdv",       "Unpack_state", "unpack_rdv",       GTG_YELLOW);
  addEntityValue("unpack_rtr",       "Unpack_state", "unpack_rtr",       GTG_ORANGE);
  addEntityValue("unpack_match_first", "Unpack_state", "unpack_match_first", GTG_TEAL);
  addEntityValue("unpack_match_last", "Unpack_state", "unpack_match_last",   GTG_BLUE);
  addEntityValue("unpack_completed", "Unpack_state", "unpack_completed", GTG_BLACK);

  /* states & vars for core */
  CHECK_RETURN(addStateType("Core_Task", "Container_Core", "Core task"));
  addEntityValue("core_none",         "Core_Task", "core_none",         GTG_BLACK);
  addEntityValue("core_strategy",     "Core_Task", "core_strategy",     GTG_GREEN);
  addEntityValue("core_polling",      "Core_Task", "core_polling",      GTG_RED);
  addEntityValue("core_task_flush",   "Core_Task", "core_task_flush",   GTG_BLUE);
  addEntityValue("core_dispatching",  "Core_Task", "core_dispatching",  GTG_PURPLE);
  addEntityValue("core_unpack_next",  "Core_Task", "core_unpack_next",  GTG_ORANGE);
  addEntityValue("core_completed_pw", "Core_Task", "core_completed_pw", GTG_YELLOW);
  addEntityValue("core_pack_submit",  "Core_Task", "core_pack_submit",  GTG_WHITE);
  addEntityValue("core_handler",      "Core_Task", "core_handler",      GTG_PINK);

  CHECK_RETURN(addVarType("Var_n_packs", "Var_n_packs", "Container_Core"));
  CHECK_RETURN(addVarType("Var_n_unpacks", "Var_n_unpacks", "Container_Core"));

  /* events & entities for pw */
  CHECK_RETURN(addEventType("nm_pw_send_post", "Container_Driver", "nm_pw_send_post"));
  CHECK_RETURN(addEventType("nm_pw_recv_post", "Container_Driver", "nm_pw_recv_post"));
  CHECK_RETURN(addEventType("nm_pw_send_complete", "Container_Driver", "nm_pw_send_complete"));
  CHECK_RETURN(addEventType("nm_pw_recv_complete", "Container_Driver", "nm_pw_recv_complete"));

  addEntityValue("pw_send_pending_large", "Pw_state", "pw_send_pending_large", GTG_PINK);
  addEntityValue("pw_recv_large",         "Pw_state", "pw_recv_large",         GTG_ORANGE);
  addEntityValue("pw_send_post_rtr",      "Pw_state", "pw_send_post_rtr",      GTG_LIGHTBROWN);
  addEntityValue("pw_recv_post",          "Pw_state", "pw_recv_post",          GTG_GREEN);
  addEntityValue("pw_send_complete",      "Pw_state", "pw_send_complete",      GTG_BLACK);
  addEntityValue("pw_recv_complete",      "Pw_state", "pw_recv_complete",      GTG_BLACK);

  /* root container */
  addContainer(0.0, "World", "Container_World", "0", "World", "0");
}

static void nm_trace_packed_flush(struct nm_trace_packed_s*p_packed)
{
  /* ** create trace types */

  /* node */
  padico_string_t s_node_container = padico_string_new();
  padico_string_printf(s_node_container, "Node_%d", p_packed->rank);
  const char*node_container = padico_string_get(s_node_container);
  addContainer(0.0, node_container, "Container_Node", "World", node_container, "0");

  /* reqs */
  padico_string_t s_pack_container = padico_string_new();
  padico_string_printf(s_pack_container, "n%d_Pack", p_packed->rank);
  const char*pack_container = padico_string_get(s_pack_container);
  addContainer(0.0, pack_container, "Container_Pack", node_container, pack_container, "0");

  struct nm_trace_object_container_vect_s packs;
  nm_trace_object_container_vect_init(&packs);

  padico_string_t s_unpack_container = padico_string_new();
  padico_string_printf(s_unpack_container, "n%d_Unpack", p_packed->rank);
  const char*unpack_container = padico_string_get(s_unpack_container);
  addContainer(0.0, unpack_container, "Container_Unpack", node_container, unpack_container, "0");

  struct nm_trace_object_container_vect_s unpacks;
  nm_trace_object_container_vect_init(&unpacks);

  /* core */
  padico_string_t s_core_container = padico_string_new();
  padico_string_printf(s_core_container, "n%d_Core", p_packed->rank);
  const char*core_container = padico_string_get(s_core_container);
  addContainer(0.0, core_container, "Container_Core", node_container, core_container, "0");

  /* drivers */
  int d;
  for(d = 0; d < p_packed->n_drivers; d++)
    {
      padico_string_t cont_drv = padico_string_new();
      padico_string_printf(cont_drv, "n%d_Driver_%s", p_packed->rank, p_packed->p_drivers[d]);
      addContainer(0.0, padico_string_get(cont_drv), "Container_Driver", node_container, padico_string_get(cont_drv), "0");
      padico_string_delete(cont_drv);
    }

  /* pw */
  struct nm_trace_object_container_vect_s pws;
  nm_trace_object_container_vect_init(&pws);

  puk_tick_t t1, t2;
  PUK_GET_TICK(t1);
  /* flush entries */
  int i;
  for(i = 0; i < p_packed->n_entries; i++)
    {
      const struct nm_trace_packed_entry_s* e = &p_packed->p_entries[i];
      char container[128];
      char pw_container[128];
      padico_string_t v = padico_string_new();
      switch(e->event)
        {
        case NM_TRACE_EVENT_PW_SEND_LARGE_PENDING:
        case NM_TRACE_EVENT_PW_RECV_LARGE:
        case NM_TRACE_EVENT_PW_POST_SEND:
        case NM_TRACE_EVENT_PW_POST_RECV:
        case NM_TRACE_EVENT_PW_POLL_SEND:
        case NM_TRACE_EVENT_PW_POLL_RECV:
        case NM_TRACE_EVENT_PW_COMPLETE_SEND:
        case NM_TRACE_EVENT_PW_COMPLETE_RECV:
          {
            padico_string_printf(v, "p_pw = %p", e->value);
            nm_trace_object_container_vect_itor_t i = nm_trace_object_find_slot(&pws, e->value,
                                                                                p_packed->rank, "Pw", "Container_Pw", node_container);
            sprintf(pw_container, "n%d_Pw#%d", p_packed->rank, nm_trace_object_container_vect_rank(&pws, i));
            if( (e->event == NM_TRACE_EVENT_PW_COMPLETE_SEND) ||
                (e->event == NM_TRACE_EVENT_PW_COMPLETE_RECV) )
              {
                *i = (uintptr_t)NULL;
              }
            if( (e->driver_id >= 0) &&
                (e->driver_id < p_packed->n_drivers) )
              {
                sprintf(container, "n%d_Driver_%s", p_packed->rank, p_packed->p_drivers[e->driver_id]);
              }
            else
              {
                memcpy(container, pw_container, sizeof(container));
              }
          }
          break;
        case NM_TRACE_EVENT_CORE_PACK_SUBMIT:
        case NM_TRACE_EVENT_CORE_PACK_FLUSH:
        case NM_TRACE_EVENT_CORE_PACK_RDV:
        case NM_TRACE_EVENT_CORE_PACK_IN_PW:
        case NM_TRACE_EVENT_CORE_PACK_PW_POSTED:
        case NM_TRACE_EVENT_CORE_PACK_COMPLETED:
          {
            nm_trace_object_container_vect_itor_t i = nm_trace_object_find_slot(&packs, e->value,
                                                                                p_packed->rank, "Packs", "Container_Pack", pack_container);
            padico_string_printf(v, "gate = %d; driver = %s; tag = %x:%x; len = %lld",
                                 e->gate_id, ((e->driver_id >= 0) ? p_packed->p_drivers[e->driver_id] : "none"),
                                 e->core_tag.hashcode, e->core_tag.tag, e->len);
            sprintf(container, "n%d_Packs#%d", p_packed->rank, nm_trace_object_container_vect_rank(&packs, i));
            if(e->event == NM_TRACE_EVENT_CORE_PACK_COMPLETED)
              {
                *i = (uintptr_t)NULL;
              }
          }
          break;
        case NM_TRACE_EVENT_CORE_UNPACK_SUBMIT:
        case NM_TRACE_EVENT_CORE_UNPACK_RDV:
        case NM_TRACE_EVENT_CORE_UNPACK_RTR:
        case NM_TRACE_EVENT_CORE_UNPACK_MATCH_FIRST:
        case NM_TRACE_EVENT_CORE_UNPACK_MATCH_LAST:
        case NM_TRACE_EVENT_CORE_UNPACK_COMPLETED:
          {
            nm_trace_object_container_vect_itor_t i = nm_trace_object_find_slot(&unpacks, e->value,
                                                                                p_packed->rank, "Unpacks", "Container_Unpack", unpack_container);
            padico_string_printf(v, "gate = %d; driver = %s; tag = %x:%x; len = %lld",
                                 e->gate_id, ((e->driver_id >= 0) ? p_packed->p_drivers[e->driver_id] : "none"),
                                 e->core_tag.hashcode, e->core_tag.tag, e->len);
            sprintf(container, "n%d_Unpacks#%d", p_packed->rank, nm_trace_object_container_vect_rank(&unpacks, i));
            if(e->event == NM_TRACE_EVENT_CORE_UNPACK_COMPLETED)
              {
                *i = (uintptr_t)NULL;
              }
          }
          break;
        case NM_TRACE_EVENT_VAR_N_PACKS:
        case NM_TRACE_EVENT_VAR_N_UNPACKS:
          sprintf(container, core_container);
          break;
        default:
          sprintf(container, node_container);
          break;
        }

      const char*value = padico_string_get(v);
      switch(e->event)
        {
        case NM_TRACE_EVENT_CONNECT:
          break;
        case NM_TRACE_EVENT_DISCONNECT:
          break;

          /* core pack / unpack */
        case NM_TRACE_EVENT_CORE_PACK_SUBMIT:
          setState(e->global_time, "Pack_state", container,  "pack_submit");
          break;
        case NM_TRACE_EVENT_CORE_PACK_FLUSH:
          setState(e->global_time, "Pack_state", container,  "pack_flush");
          break;
        case NM_TRACE_EVENT_CORE_PACK_RDV:
          setState(e->global_time, "Pack_state", container,  "pack_rdv");
          break;
        case NM_TRACE_EVENT_CORE_PACK_IN_PW:
            setState(e->global_time, "Pack_state", container,  "pack_in_pw");
          break;
        case NM_TRACE_EVENT_CORE_PACK_PW_POSTED:
          {
            setState(e->global_time, "Pack_state", container,  "pack_pw_posted");
            padico_string_t target_container = padico_string_new();
            padico_string_printf(target_container, "n%d_Gate_%d\n", e->gate_id, p_packed->rank);
            padico_string_t link_key = padico_string_new();
            padico_string_printf(link_key, "from = %d; to = %d; tag = %x:%x; seq = %u",
                                 p_packed->rank, e->gate_id, e->core_tag.hashcode, e->core_tag.tag, e->seq);
            startLink(e->global_time, "nm_core_pack_link", "World", container, padico_string_get(target_container),
                      value, padico_string_get(link_key));
            padico_string_delete(target_container);
            padico_string_delete(link_key);
          }
          break;
        case NM_TRACE_EVENT_CORE_PACK_COMPLETED:
          setState(e->global_time, "Pack_state", container,  "pack_completed");
          break;

        case NM_TRACE_EVENT_CORE_UNPACK_SUBMIT:
          setState(e->global_time, "Unpack_state", container,  "unpack_submit");
          break;
        case NM_TRACE_EVENT_CORE_UNPACK_RDV:
          setState(e->global_time, "Unpack_state", container,  "unpack_rdv");
          break;
        case NM_TRACE_EVENT_CORE_UNPACK_RTR:
          setState(e->global_time, "Unpack_state", container,  "unpack_rtr");
          break;
        case NM_TRACE_EVENT_CORE_UNPACK_MATCH_FIRST:
          {
            setState(e->global_time, "Unpack_state", container,  "unpack_match_first");
            padico_string_t src_container = padico_string_new();
            padico_string_printf(src_container, "n%d_Gate_%d\n", e->gate_id, p_packed->rank);
            padico_string_t link_key = padico_string_new();
            padico_string_printf(link_key, "from = %d; to = %d; tag = %x:%x; seq = %u",
                                 e->gate_id, p_packed->rank, e->core_tag.hashcode, e->core_tag.tag, e->seq);
            endLink(e->global_time, "nm_core_pack_link", "World", padico_string_get(src_container), container,
                    value, padico_string_get(link_key));
            padico_string_delete(src_container);
            padico_string_delete(link_key);
          }
          break;
        case NM_TRACE_EVENT_CORE_UNPACK_MATCH_LAST:
          setState(e->global_time, "Unpack_state", container,  "unpack_match_last");
          break;
        case NM_TRACE_EVENT_CORE_UNPACK_COMPLETED:
          setState(e->global_time, "Unpack_state", container,  "unpack_completed");
          break;

        case NM_TRACE_EVENT_VAR_N_PACKS:
          CHECK_RETURN(setVar(e->global_time, "Var_n_packs", core_container, (int)e->value));
          break;
        case NM_TRACE_EVENT_VAR_N_UNPACKS:
          CHECK_RETURN(setVar(e->global_time, "Var_n_unpacks", core_container, (int)e->value));
          break;

          /* drivers */
        case NM_TRACE_EVENT_PW_SEND_LARGE_PENDING:
          setState(e->global_time, "Pw_state", pw_container,  "pw_send_large_pending");
          break;
        case NM_TRACE_EVENT_PW_RECV_LARGE:
          setState(e->global_time, "Pw_state", pw_container,  "pw_recv_large");
          break;
        case NM_TRACE_EVENT_PW_POST_SEND:
          setState(e->global_time, "Pw_state", pw_container,  "pw_send_post");
          break;
        case NM_TRACE_EVENT_PW_POST_RECV:
          setState(e->global_time, "Pw_state", pw_container,  "pw_recv_post");
          break;
        case NM_TRACE_EVENT_PW_COMPLETE_SEND:
          setState(e->global_time, "Pw_state", pw_container,  "pw_send_complete");
          break;
        case NM_TRACE_EVENT_PW_COMPLETE_RECV:
          setState(e->global_time, "Pw_state", pw_container,  "pw_recv_complete");
          break;

          /* core states */
        case NM_TRACE_STATE_CORE_NONE:
          CHECK_RETURN(setState(e->global_time, "Core_Task", core_container,  "core_none"));
          break;
        case NM_TRACE_STATE_CORE_STRATEGY:
          CHECK_RETURN(setState(e->global_time, "Core_Task", core_container,  "core_strategy"));
          break;
        case NM_TRACE_STATE_CORE_POLLING:
          CHECK_RETURN(setState(e->global_time, "Core_Task", core_container,  "core_polling"));
          break;
        case NM_TRACE_STATE_CORE_TASK_FLUSH:
          CHECK_RETURN(setState(e->global_time, "Core_Task", core_container,  "core_task_flush"));
          break;
        case NM_TRACE_STATE_CORE_DISPATCHING:
          CHECK_RETURN(setState(e->global_time, "Core_Task", core_container,  "core_dispatching"));
          break;
        case NM_TRACE_STATE_CORE_UNPACK_NEXT:
          CHECK_RETURN(setState(e->global_time, "Core_Task", core_container,  "core_unpack_next"));
          break;
        case NM_TRACE_STATE_CORE_COMPLETED_PW:
          CHECK_RETURN(setState(e->global_time, "Core_Task", core_container,  "core_completed_pw"));
          break;
        case NM_TRACE_STATE_CORE_PACK_SUBMIT:
          CHECK_RETURN(setState(e->global_time, "Core_Task", core_container,  "core_pack_submit"));
          break;
        case NM_TRACE_STATE_CORE_HANDLER:
          CHECK_RETURN(setState(e->global_time, "Core_Task", core_container,  "core_handler"));
          break;

        default:
          break;
        }
      padico_string_delete(v);
    }
  PUK_GET_TICK(t2);
  const double t = PUK_TIMING_DELAY(t1, t2);
  const double t_seconds = t / 1000000.0;
  padico_out(puk_verbose_notice, "trace flushed %d entries in %f seconds (%d entries / second.)\n",
             p_packed->n_entries, t_seconds, (int)((double)p_packed->n_entries / t_seconds));
  padico_string_delete(s_core_container);
  padico_string_delete(s_node_container);
  nm_trace_object_container_vect_destroy(&packs);
  nm_trace_object_container_vect_destroy(&unpacks);
  nm_trace_object_container_vect_destroy(&pws);
}

static void nm_trace_flush(nm_core_t p_core)
{
  static int flushed = 0;
  if(flushed)
    return;
  padico_out(puk_verbose_notice, "flush traces (%d entries)\n", nm_trace.last);
  flushed = 1;
  /* check if there is any trace to write */
  int*n_entries = padico_malloc(sizeof(int) * nm_trace.size);
  nm_coll_gather(nm_trace.p_comm, 0 /* root */, &nm_trace.last, sizeof(int), n_entries, sizeof(int), 1 /* tag */);
  nm_coll_bcast(nm_trace.p_comm, 0 /* root */, n_entries, sizeof(int) * nm_trace.size, 2 /* tag */);
  int total = 0;
  int k;
  for(k = 0; k < nm_trace.size; k++)
    {
      total += n_entries[k];
    }
  padico_free(n_entries);
  if(total == 0)
    {
      padico_out(puk_verbose_notice, "trace empty, not writting file.\n");
      return;
    }
  struct nm_trace_packed_s*p_packed = nm_trace_pack(p_core);

  if(nm_trace.rank == 0)
    {
      /* init GTG */
      char trace_name[256];
      char hostname[256];
      gethostname(hostname, 256);
      sprintf(trace_name, "%s/nmad_%s_%d%s",
              padico_module_attr_trace_path_getvalue(),
              hostname, getpid(), padico_module_attr_trace_suffix_getvalue());
      setTraceType(PAJE);
      CHECK_RETURN(initTrace(trace_name, 0, GTG_FLAG_NONE));

      padico_out(puk_verbose_notice, "writing trace file %s\n", trace_name);
      nm_trace_header();
      nm_trace_packed_flush(p_packed);
      nm_trace_packed_free(p_packed);

      int i;
      for(i = 1; i < nm_trace.size; i++)
        {
          padico_out(puk_verbose_notice, "writing trace for node #%d\n", i);
          nm_gate_t p_gate = nm_comm_get_gate(nm_trace.p_comm, i);
          p_packed = nm_trace_packed_recv(p_gate);
          nm_trace_packed_flush(p_packed);
          nm_trace_packed_free(p_packed);
        }
      endTrace();
    }
  else
    {
      nm_gate_t p_gate = nm_comm_get_gate(nm_trace.p_comm, 0);
      nm_trace_packed_send(p_packed, p_gate);
      nm_trace_packed_free(p_packed);
    }
}

void nm_trace_init(nm_core_t p_core)
{
  char*s_trace = padico_strdup(padico_module_attr_trace_getvalue()); /* make a copy since strtok writes in the buffer */
  NM_DISPF("trace enabled- NMAD_TRACE = %s\n", s_trace);
  char*saveptr = NULL;
  char*s = strtok_r(s_trace, ",", &saveptr);
  while(s != NULL)
    {
      if(strcmp(s, "none") == 0)
        {
          nm_trace.filters = 0;
        }
      else if(strcmp(s, "all") == 0)
        {
          nm_trace.filters = NM_TRACE_FILTER_ALL;
        }
      else if(strcmp(s, "core") == 0)
        {
          nm_trace.filters |= NM_TRACE_FILTER_CORE;
        }
      else if(strcmp(s, "driver") == 0)
        {
          nm_trace.filters |= NM_TRACE_FILTER_DRIVER;
        }
      else if(strcmp(s, "pack") == 0)
        {
          nm_trace.filters |= NM_TRACE_FILTER_PACK;
        }
      else if(strcmp(s, "link") == 0)
        {
          nm_trace.filters |= NM_TRACE_FILTER_LINK;
        }
      else if(strcmp(s, "^core") == 0)
        {
          nm_trace.filters &= ~NM_TRACE_FILTER_CORE;
        }
      else if(strcmp(s, "^driver") == 0)
        {
          nm_trace.filters &= ~NM_TRACE_FILTER_DRIVER;
        }
      else if(strcmp(s, "^pack") == 0)
        {
          nm_trace.filters &= ~NM_TRACE_FILTER_PACK;
        }
      else if(strcmp(s, "^link") == 0)
        {
          nm_trace.filters &= ~NM_TRACE_FILTER_LINK;
        }
      else
        {
          NM_FATAL("unknown trace filter: %s\n", s);
        }
      s = strtok_r(NULL, ",", &saveptr);
    }
  padico_free(s_trace);
  nm_trace.p_comm = nm_comm_world("nm_trace");
  nm_trace.p_clocks = nm_sync_clocks_init(nm_trace.p_comm);
  nm_trace.tracing = 1;
  nm_trace.rank = nm_comm_rank(nm_trace.p_comm);
  nm_trace.size = nm_comm_size(nm_trace.p_comm);
}

void nm_trace_exit(nm_core_t p_core)
{
  nm_trace.tracing = 0;
  nm_sync_clocks_synchronize(nm_trace.p_clocks);
  nm_trace_flush(p_core);
  nm_sync_clocks_shutdown(nm_trace.p_clocks);
  nm_comm_destroy(nm_trace.p_comm);
}


#endif /* NMAD_TRACE */

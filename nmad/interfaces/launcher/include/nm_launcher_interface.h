/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#ifndef NM_LAUNCHER_INTERFACE_H
#define NM_LAUNCHER_INTERFACE_H

/** @file
 * @ingroup launcher_interface
 */

#include <nm_public.h>
#include <nm_session_interface.h>

/** @defgroup launcher_interface Launcher interface
 * This is the launcher interface, the high level nmad interface used to launch sessions.
 * @example nm_launcher_mini.c
 */

/** @ingroup launcher_interface
 * @{
 */

/** Initializes nmad. */
static inline int nm_launcher_init(int*argc, char**argv);

/** Cleans session. Returns NM_ESUCCESS or EXIT_FAILURE. */
int nm_launcher_exit(void);

/** Returns process rank */
int nm_launcher_get_rank(int*rank);

/** Returns the number of nodes */
int nm_launcher_get_size(int*size);

/** Returns the gate for the process dest */
int nm_launcher_get_gate(int dest, nm_gate_t*gate);

/** Returns the dest rank for the given gate */
int nm_launcher_get_dest(nm_gate_t p_gate, int*dest);

/** Abort all processes */
void nm_launcher_abort(int rc);

/** @} */

/** @internal init launcher with ABI check */
int nm_launcher_init_checked(int*argc, char**argv, const struct nm_abi_config_s*p_nm_abi_config);

static inline int nm_launcher_init(int*argc, char**argv)
{
  /* capture ABI config in application context
   * and compare with nmad builtin ABI config */
  struct nm_abi_config_s nm_abi_config = { 0 };
  nm_abi_config_capture(&nm_abi_config);
  return nm_launcher_init_checked(argc, argv, &nm_abi_config);
}

/** shortcut to get the gate to self */
static inline nm_gate_t nm_launcher_self_gate(void)
{
  int rank;
  nm_launcher_get_rank(&rank);
  nm_gate_t p_gate;
  nm_launcher_get_gate(rank, &p_gate);
  return p_gate;
}

/** print on stderr loaded drivers and the selected strategy */
void nm_launcher_print_drivers_strategy(nm_core_t p_core);

/** Checks whether the networks are homogeneous across all nodes.
 */
int nm_launcher_homogeneous_network(void);


#endif /* NM_LAUNCHER_INTERFACE_H */

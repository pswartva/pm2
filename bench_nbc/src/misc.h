/*
 * Bench NBC
 * Copyright (C) 2019-2020 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#ifndef MISC_H
#define MISC_H

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <float.h>
#include <math.h>
#include <string.h>
#include <stdint.h>
#ifdef SYNC_1
#include "sync/Timer.h"
#include "sync/Sync_Time_MPI.h"
#endif
#ifdef SYNC_2
#include "mpi_sync_clocks.h"
#endif
#include "math/Matrix.h"
#include "bench_constants.h"

extern uint64_t get_time();
extern void get_time_r(uint64_t *);
extern double get_proc_freq();
extern int compare_doubles(const void*,const void*);
extern double mean(double *, int);
extern double median(double*, int);
extern double variance(double*, int);
extern double max(double*,int);
extern double min(double*,int);
extern double sum(double*, int);
extern double racine3(double);
extern int get_nb_rep(double,double,int);


typedef struct bench_time_s
{
  double**all_ranks;
  double*median;
  double*min;
  double*max;
  double*var;
}bench_time_t;

extern bench_time_t*estimate_comp_time(int);

typedef struct comp_time_info_s
{
  int theorical_time;/*time of compute we try to get*/
  bench_time_t*real_time;/*real time of compute, as close as possible from theorical*/
  int matrix_size;/*matrix size needed to get real time compute*/
} comp_time_info_t;


#ifdef GLOBAL_BUFFER
extern void*data,*data2;
extern double*a,*b,*res;
#endif

#endif

/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Topology description API
 * @ingroup Topology
 */

/** @defgroup Topology API: Grid Topology management
 * @ingroup Topology
 */

#ifndef PADICO_TOPOLOGY_H
#define PADICO_TOPOLOGY_H

#include <Padico/Puk.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <errno.h>
#include <netinet/tcp.h>

 PUK_MOD_AUTO_DEP(Topology, PADICO_LAST_MODULE_NAME);
#undef PADICO_LAST_MODULE_NAME
#define PADICO_LAST_MODULE_NAME Topology

#ifdef __cplusplus
extern "C" {
#endif

/* ********************************************************* */
/* *** Topology *************************************************** */

/* Definitions for topology management:
 *  -- host: a machine, identified by a hostname.
 *  -- node: a computing node (ie. process). There may
 *     be several nodes per host. A node is identified
 *     by a nodename.
 *  -- network: a list (order matters) of nodes or hosts with
 *     bindings between network and nodes/hosts.
 *     Binding address may be rank-based (nodes) or
 *     in_addr-based (hosts).
 *
 */


/** @addtogroup Topology
 * @{
 */

/* *** Types */

#define PADICO_TOPO_UUID_SIZE 32
typedef const struct topo_uuid_s*padico_topo_uuid_t;
typedef       struct topo_host_s*padico_topo_host_t;
typedef       struct topo_node_s*padico_topo_node_t;
typedef       struct topo_network_s*padico_topo_network_t;
typedef       struct topo_binding_s*padico_topo_binding_t;
typedef const struct topo_hwaddr_s*padico_topo_hwaddr_t;
typedef const struct topo_address_s*padico_topo_address_t;
typedef       struct topo_filter_s*padico_topo_filter_t;
typedef       struct topo_service_s*padico_topo_service_t;

PUK_VECT_TYPE(padico_topo_node,    padico_topo_node_t);
PUK_VECT_TYPE(padico_topo_network, padico_topo_network_t);
PUK_VECT_TYPE(padico_topo_host,    padico_topo_host_t);
PUK_VECT_TYPE(padico_topo_binding, padico_topo_binding_t);

/* *** Addresses and bindings */

typedef enum
  {
    PADICO_TOPO_SCOPE_NONE = 0x00,
    PADICO_TOPO_SCOPE_NODE = 0x01,
    PADICO_TOPO_SCOPE_HOST = 0x02,
    PADICO_TOPO_SCOPE_ALL  = 0xFF
  } padico_topo_binding_scope_t;

typedef enum
  {
    PADICO_TOPO_ADDR_VISIBILITY_NONE      = 0,
    PADICO_TOPO_ADDR_VISIBILITY_LOOPBACK  = 1, /**< loopback address- only local node/host can see it */
    PADICO_TOPO_ADDR_VISIBILITY_LINKLOCAL = 2, /**< link-local address- nodes on the same physical link can see it */
    PADICO_TOPO_ADDR_VISIBILITY_PRIVATE   = 3, /**< private address- some node/host cannot see it */
    PADICO_TOPO_ADDR_VISIBILITY_PUBLIC    = 4, /**< public address */
  } padico_topo_addr_visibility_t;

typedef enum
  {
    _PADICO_TOPO_FILTER_NONE     = 0x00,
    PADICO_TOPO_FILTER_BLOCK_IN  = 0x01,
    PADICO_TOPO_FILTER_BLOCK_OUT = 0x02,
    PADICO_TOPO_FILTER_ALLOW_IN  = 0x04,
    PADICO_TOPO_FILTER_ALLOW_OUT = 0x08,
    _PADICO_TOPO_FILTER_MAX
  } padico_topo_filter_policy_t;
#define PADICO_TOPO_FILTER_MASK_IN    (PADICO_TOPO_FILTER_BLOCK_IN  | PADICO_TOPO_FILTER_ALLOW_IN)
#define PADICO_TOPO_FILTER_MASK_OUT   (PADICO_TOPO_FILTER_BLOCK_OUT | PADICO_TOPO_FILTER_ALLOW_OUT)
#define PADICO_TOPO_FILTER_MASK_BLOCK (PADICO_TOPO_FILTER_BLOCK_IN  | PADICO_TOPO_FILTER_BLOCK_OUT)
#define PADICO_TOPO_FILTER_MASK_ALLOW (PADICO_TOPO_FILTER_ALLOW_IN  | PADICO_TOPO_FILTER_ALLOW_AOUT)

#define PORT_ANY 0

  /* *** Host ********************************************** */

  /**  Create new host
   */
  padico_topo_host_t   padico_topo_host_create(const char*hostname);

  /**  Add a name to a host;
   * @param resolvable whether the host is DNS-resolvable (0: no; 1: yes; -1: don't known => detect).
   */
  void padico_topo_host_add_name(padico_topo_host_t host, const char*name, int resolvable);

  /**  Add a service to a host
   */
  void padico_topo_host_add_service(padico_topo_host_t host, const char*service_name, uint16_t service_port);

  /**  Add a property to a host
   */
  void padico_topo_host_add_property(padico_topo_host_t host, const char*label, const char*name);

  /** Get a host description record from an inet address (create if it didn't exist)
   */
  padico_topo_host_t    padico_topo_host_resolve_byinaddr(const struct in_addr*addr);

  /** Get a host from an inet address (return NULL if host doesn't exist).
   */
  padico_topo_host_t    padico_topo_host_getbyinaddr(const struct in_addr*addr);

  /** Get a host description record from an inet6 address (create if it didn't exist)
   */
  padico_topo_host_t    padico_topo_host_resolve_byin6addr(const struct in6_addr*addr);

  /** Get a host from an inet6 address (return NULL if host doesn't exist).
   */
  padico_topo_host_t    padico_topo_host_getbyin6addr(const struct in6_addr*addr);

  /** Get a host description record (lookup in cache, create if it didn't exist)
   */
  padico_topo_host_t    padico_topo_host_resolvebyname(const char*hostname);

  /** Get the canonical name of a host
   */
  const char*           padico_topo_host_getname(padico_topo_host_t host);

  /** Get the canonical name of a host on a network
   */
  const char*           padico_topo_host_getname2(padico_topo_host_t host, padico_topo_network_t network);

  /** Get the canonical IP address of a host
   */
  const struct in_addr* padico_topo_host_getaddr(padico_topo_host_t host);

  /** Get the canonical IP address of a host on a network
   */
  const struct in_addr* padico_topo_host_getaddr2(padico_topo_host_t host, padico_topo_network_t network);

  /** Get the bindings of a host on a network. The caller takes ownership of the result.
   */
  padico_topo_binding_vect_t padico_topo_host_getbindings(padico_topo_host_t host, padico_topo_network_t network);

  /** Get the nodes on a host
   */
  padico_topo_node_vect_t padico_topo_host_getnodes(padico_topo_host_t host);

  /** Get the networks a host is connected to
   */
  void padico_topo_host_getnetworks(padico_topo_host_t host, padico_topo_network_vect_t networks);

  /** Get the service port of the service on a host
   */
  int padico_topo_host_getservice(padico_topo_host_t host, char*service_name);

  /** Get property on a host
   */
  const char*padico_topo_host_getproperty(padico_topo_host_t host, const char*label);

  /** Verify if the port is filtered output between host and network
   */
  int padico_topo_host_can_output(padico_topo_host_t host, padico_topo_network_t network, uint16_t port, int protocol);

  /** Verify if the port is filtered input between host and network
   */
  int padico_topo_host_can_input(padico_topo_host_t host, padico_topo_network_t network, uint16_t port, int protocol);

  int padico_topo_network_can_input(padico_topo_network_t subnet, padico_topo_network_t network, uint16_t port, int protocol);
  int padico_topo_network_can_output(padico_topo_network_t subnet, padico_topo_network_t network, uint16_t port, int protocol);

  /** Declare a new binding for a host
   */
  padico_topo_binding_t padico_topo_host_bind(padico_topo_host_t host, padico_topo_network_t network, const void*address);

  /** Get known hosts*/
  padico_topo_host_vect_t padico_topo_gethosts();

  /** Get the first node of a host
   * @deprecated
   */
  static inline padico_topo_node_t padico_topo_host_getnode(padico_topo_host_t host)
  {
    const padico_topo_node_vect_t nodes = padico_topo_host_getnodes(host);
    return padico_topo_node_vect_empty(nodes)?NULL:*(padico_topo_node_vect_begin(nodes));
  }

  /* *** Nodes ********************************************* */

  /** node lookup, by node name
   */
  padico_topo_node_t    padico_topo_getnodebyname(const char*nodename);

  /** node lookup, by UUID
   */
  padico_topo_node_t    padico_topo_getnodebyuuid(padico_topo_uuid_t uuid);

  /** Get the UUID of a node
   */
  padico_topo_uuid_t    padico_topo_node_getuuid(padico_topo_node_t node);

  /** Get the canonical name of a node
   */
  const char*           padico_topo_node_getname(padico_topo_node_t node);

  /** Get the host hosting a node
   */
  padico_topo_host_t    padico_topo_node_gethost(padico_topo_node_t node);

  /** Get the address of the node in the given network
   */
  padico_topo_address_t padico_topo_node_getaddress(padico_topo_node_t node, padico_topo_network_t network);

  /** Get the boot session ID
   */
  const char*           padico_topo_node_getsession(padico_topo_node_t node);

  /** Get the networks a node is connected to
   */
  void padico_topo_node_getnetworks(padico_topo_node_t node, padico_topo_network_vect_t networks);

  /** Remove a disconnected node from topology
   */
  void padico_topo_node_disconnect(padico_topo_node_t node);

  /** Declare a new binding for a node
   */
  padico_topo_binding_t padico_topo_node_bind(padico_topo_node_t node, padico_topo_network_t network, const void*address);


  /* *** Networks ****************************************** */

  padico_topo_network_t padico_topo_network_create(const char*id, padico_topo_binding_scope_t scope, size_t addr_size, int family);

  padico_topo_network_t padico_topo_network_getbyid(const char*network_id);

  /** Get networks of a given address family (e.g. AF_INET, AF_INET6, etc.)
   */
  padico_topo_network_vect_t padico_topo_networks_getbyfamily(int family);

  /** tests whether the given node belong to the network
   */
  int                   padico_topo_network_contains_node(padico_topo_network_t network, padico_topo_node_t node);
  const char*           padico_topo_network_getname(padico_topo_network_t network);
  int                   padico_topo_network_size   (padico_topo_network_t network);
  /** Get a host description record from an address
   */
  padico_topo_host_t    padico_topo_network_gethostbyaddr(padico_topo_network_t, const void*);

  /** Get a node descriptor from an address
   */
  padico_topo_node_t    padico_topo_network_getnodebyaddr(padico_topo_network_t network, const void*addr);

  void                  padico_topo_network_gethosts(padico_topo_network_t n, padico_topo_host_vect_t hosts);

  void                  padico_topo_network_get_common_host(padico_topo_network_t n1, padico_topo_network_t n2, padico_topo_host_vect_t hosts);

  void                  padico_topo_network_set_perf(padico_topo_network_t n, int latency_nsec, int bandwidth_kbps);

  void                  padico_topo_network_get_perf(padico_topo_network_t n, int*latency_nsec, int*bandwidth_kbps);

  /* *** serialization ************************************* */

  padico_string_t padico_topo_host_serialize(padico_topo_host_t host);
  padico_string_t padico_topo_node_serialize(padico_topo_node_t node);
  padico_string_t padico_topo_network_serialize(padico_topo_network_t net);
  padico_string_t padico_na_topology_serialize(padico_topo_binding_scope_t scope);


  /* *** address *******************************************/

  const void*padico_topo_address_getbytes(padico_topo_address_t address);


  /* *** local info *****************************************/

  padico_topo_node_t padico_topo_getlocalnode(void);

#define padico_topo_nodename()     padico_topo_node_getname(padico_topo_getlocalnode())
#define padico_topo_nodealias()    padico_topo_node_getalias(padico_topo_getlocalnode())
#define padico_topo_getlocalhost() padico_topo_node_gethost(padico_topo_getlocalnode())
#define padico_topo_hostname()     padico_topo_host_getname(padico_topo_getlocalhost())
#define padico_topo_session()      padico_topo_node_getsession(padico_topo_getlocalnode())
  /* *** backward compatibility */
#define padico_na_size()         padico_topo_network_size(padico_topo_session_getnetwork(padico_topo_session()))
#define padico_na_rank()         padico_topo_session_getrank(padico_topo_session(), padico_topo_getlocalnode())



  /* *** session ********************************************/

  static inline padico_topo_network_t padico_topo_session_getnetwork(const char*session_id)
  {
    padico_string_t network_name = padico_string_new();
    padico_string_printf(network_name, "Session-[%s]", session_id);
    padico_topo_network_t network = padico_topo_network_getbyid(padico_string_get(network_name));
    padico_string_delete(network_name);
    return network;
  }
  static inline padico_topo_node_t padico_topo_session_getnodebyrank(const char*session_id, int rank)
  {
    padico_topo_network_t network = padico_topo_session_getnetwork(session_id);
    padico_topo_node_t node = padico_topo_network_getnodebyaddr(network, &rank);
    return node;
  }
  static inline int padico_topo_session_getrank(const char*session_id, padico_topo_node_t node)
  {
    padico_topo_network_t network = padico_topo_session_getnetwork(session_id);
    padico_topo_address_t address = padico_topo_node_getaddress(node, network);
    const void*addr_bytes = padico_topo_address_getbytes(address);
    const int*rank = addr_bytes;
    return *rank;
  }

  /* *** hwaddr ********************************************/

  padico_topo_hwaddr_t padico_topo_hwaddr_create(const char*name, int index, int type, size_t len, const void*addr);

  void padico_topo_hwaddr_get(padico_topo_hwaddr_t hwaddr, const char**name, int*index, int*type, size_t*len, const void**addr);

  void padico_topo_hwaddr_add(padico_topo_host_t host, padico_topo_hwaddr_t*_hwaddr);

  void padico_topo_hwaddr_destroy(padico_topo_hwaddr_t hwaddr);

  /* *** bindings ********************************************/

  void padico_topo_binding_add_addr(padico_topo_binding_t binding, const void*addr);

  void padico_topo_binding_add_index(padico_topo_binding_t binding, int64_t index);

  void padico_topo_binding_add_hwaddr(padico_topo_binding_t binding, padico_topo_hwaddr_t hwaddr);

  void padico_topo_binding_add_gw_addr(padico_topo_binding_t binding, struct in_addr*inaddr);

  void padico_topo_binding_add_hostname(padico_topo_binding_t binding, const char*name);

  void padico_topo_binding_add_route(padico_topo_binding_t binding, padico_topo_network_t dest, const void*gwaddr);

  void padico_topo_binding_add_filter(padico_topo_binding_t binding, padico_topo_filter_policy_t policy, uint16_t port, int protocol);

  void padico_topo_binding_add_perf(padico_topo_binding_t binding, int latency, int bandwidth);

  void padico_topo_binding_get_perf(padico_topo_binding_t binding, int*latency_nsec, int*bandwidth_kbps);

  padico_topo_hwaddr_t padico_topo_binding_gethwaddr(padico_topo_binding_t binding);

  int padico_topo_binding_get_index(padico_topo_binding_t binding, int64_t*index);

  /* *** address visibility ********************************/

  padico_topo_addr_visibility_t padico_topo_inet_visibility(in_addr_t _inaddr);

  padico_topo_addr_visibility_t padico_topo_inet6_visibility(const struct in6_addr*in6addr);



/* ********************************************************* */
/* *** Padico addresses as sockaddr ************************ */

/* padico_sockaddr (struct sockaddr_pa) define a new AF_PADICO
 * address space for Padico-to-Padico communications with
 * non-interoperable methods.
 */

typedef union
{
  char bytes[4];
  uint32_t integer;
} padico_address_kind_t;

#define AF_PADICO (AF_MAX + 1) /* just to make sure that AF_PADICO addresses
                                  are never leaked into the OS... */
#define PF_PADICO AF_PADICO

/* make uint32_t from bytes */
#define AF_PADICO_MAGIC(B1, B2, B3, B4) \
  (((padico_address_kind_t){ .bytes = { B1, B2, B3, B4 } }).integer)
/* make address_kind_t from uint32_t */
#define AF_PADICO_MAKE(I) \
  ((padico_address_kind_t){ .integer = (I) })
/* make bytes from uint32_t */
#define AF_PADICO_REVERSE(I) \
  ((AF_PADICO_MAKE(I)).bytes)

/* embed an AF_INET address into an AF_PADICO */
#define AF_PADICO_INET AF_PADICO_MAGIC('I', 'N', 'E', 'T')
/* embed an AF_UNIX address into an AF_PADICO */
#define AF_PADICO_UNIX AF_PADICO_MAGIC('U', 'N', 'I', 'X')
/* requests an autobind (only suitable as *input* for bind(), should not be returned by bind()). */
#define AF_PADICO_AUTO AF_PADICO_MAGIC('A', 'U', 'T', 'O')

#define AF_PADICO_PAYLOAD 64

struct sockaddr_pa
{
  sa_family_t   spa_family; /* =AF_PADICO, host byte order */
  uint32_t      spa_magic;  /* magic number for sub-family, host byte order */
  unsigned char spa_payload[AF_PADICO_PAYLOAD];
};


void padico_sockaddr_init(struct sockaddr_pa*addr, socklen_t*addrlen, int magic);

#define padico_sockaddr_payload(TYPE, ADDRESS) \
  ( assert(sizeof(TYPE) <= AF_PADICO_PAYLOAD), \
    (TYPE*)(((struct sockaddr_pa*)(ADDRESS))->spa_payload) )

#define padico_sockaddr_magic(ADDRESS)                                  \
  ( assert(((struct sockaddr_pa*)(ADDRESS))->spa_family == AF_PADICO),  \
    ((struct sockaddr_pa*)(ADDRESS))->spa_magic )

int padico_sockaddr_check(const struct sockaddr_pa*addr, int magic);


/* ********************************************************* */
/* *** Padico vaddr **************************************** */

/* Padico vaddr (padico_vaddr_t) is a generic address used for
 * transmissions. It is mainly a packed, self-contained and
 * serializable form of struct sockaddr.
 */

struct padico_vaddr_s
{
  struct sockaddr*addr;
  socklen_t addrlen;
  int proto;
};

typedef struct padico_vaddr_s*padico_vaddr_t;

padico_string_t padico_vaddr_marshall(const struct padico_vaddr_s*);
padico_vaddr_t  padico_vaddr_unmarshall(const char*);

/* C++ doesn't like inlined GNU extensions */
#ifndef __cplusplus
static inline padico_vaddr_t padico_vaddr_new(struct sockaddr*addr, socklen_t addrlen, int proto)
{
  padico_vaddr_t vaddr = padico_malloc(sizeof(struct padico_vaddr_s));
  *vaddr = (struct padico_vaddr_s)
    {
      .addr    = addr,
      .addrlen = addrlen,
      .proto   = proto
    };
  return vaddr;
}
static inline void padico_vaddr_delete(padico_vaddr_t vaddr)
{
  padico_free(vaddr);
}
#endif /* __cplusplus */



/** @} */

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* PADICO_TOPOLOGY_H */

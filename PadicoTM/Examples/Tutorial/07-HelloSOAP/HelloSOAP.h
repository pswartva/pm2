/** @file
 * @brief gSOAP 'Hello' interface example
 */

//gsoap HelloSOAP service name: HelloSOAP
//gsoap HelloSOAP schema namespace: urn:Padico

int HelloSOAP__SayHello(char**out);


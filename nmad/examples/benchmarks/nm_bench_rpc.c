/*
 * NewMadeleine
 * Copyright (C) 2016-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include "nm_bench_generic.h"
#include <nm_rpc_interface.h>

#define DATA_TAG 0x05
static const nm_tag_t data_tag = DATA_TAG;
static void*volatile rbuf = NULL;
static nm_rpc_service_t p_service = NULL;
static nm_cond_status_t cond;

static void bench_rpc_handler(nm_rpc_token_t p_token)
{
  assert(rbuf != NULL);
  nm_len_t rlen = NM_LEN_UNDEFINED;
  nm_rpc_recv_header(p_token, &rlen, sizeof(rlen));
  nm_rpc_irecv_body(p_token, rbuf, rlen);
}

static void bench_rpc_finalizer(nm_rpc_token_t p_token)
{
  nm_cond_signal(&cond, NM_STATUS_FINALIZED);
}

static void bench_rpc_send(void*buf, nm_len_t len)
{
  nm_len_t sheader = len;
  struct nm_data_s data;
  nm_data_contiguous_build(&data, buf, len);
  nm_rpc_send(p_service, nm_bench_common.p_gate, data_tag, &sheader, sizeof(sheader), &data);
}

static void bench_rpc_recv(void*buf, nm_len_t len)
{
  nm_cond_wait(&cond, NM_STATUS_FINALIZED, nm_core_get_singleton());
  nm_cond_init(&cond, 0); /* re-init for next round */
}

static void bench_rpc_server(void*buf, nm_len_t len)
{
  bench_rpc_recv(buf, len);
  bench_rpc_send(buf, len);
}

static void bench_rpc_client(void*buf, nm_len_t len)
{
  bench_rpc_send(buf, len);
  bench_rpc_recv(buf, len);
}

static void bench_rpc_init(void*buf, nm_len_t len)
{
  rbuf = buf;
  if(p_service == NULL)
    p_service = nm_rpc_register(nm_bench_common.p_session, data_tag, NM_TAG_MASK_FULL,
                                &bench_rpc_handler, &bench_rpc_finalizer, NULL);
  nm_cond_init(&cond, 0);
}

const struct nm_bench_s nm_bench =
  {
    .name = "rpc interface",
    .server = &bench_rpc_server,
    .client = &bench_rpc_client,
    .init   = &bench_rpc_init
  };

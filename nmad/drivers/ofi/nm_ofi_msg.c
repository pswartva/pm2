/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>

#include <Padico/Module.h>
#include <nm_minidriver.h>
#include <nm_private.h>

#include "nm_ofi.h"

static void*nm_ofi_msg_instantiate(puk_instance_t instance, puk_context_t context);
static void nm_ofi_msg_destroy(void*status);

static const struct puk_component_driver_s nm_ofi_msg_component =
  {
    .instantiate = &nm_ofi_msg_instantiate,
    .destroy     = &nm_ofi_msg_destroy
  };

static void nm_ofi_msg_getprops(puk_context_t context, struct nm_minidriver_properties_s*props);
static void nm_ofi_msg_init(puk_context_t context, const void**drv_url, size_t*url_size);
static void nm_ofi_msg_close(puk_context_t context);
static void nm_ofi_msg_connect(void* _status, const void*remote_url, size_t url_size);
static int  nm_ofi_msg_send_iov_post(void*_status, const struct iovec*v, int n);
static int  nm_ofi_msg_send_poll(void*_status);
static int  nm_ofi_msg_recv_iov_post(void*_status, struct iovec*v, int n);
static int  nm_ofi_msg_recv_poll_one(void*_status);
static int  nm_ofi_msg_recv_cancel(void*_status);

static const struct nm_minidriver_iface_s nm_ofi_msg_minidriver =
  {
    .getprops         = &nm_ofi_msg_getprops,
    .init             = &nm_ofi_msg_init,
    .close            = &nm_ofi_msg_close,
    .connect          = &nm_ofi_msg_connect,
    .send_iov_post    = &nm_ofi_msg_send_iov_post,
    .send_poll        = &nm_ofi_msg_send_poll,
    .recv_iov_post    = &nm_ofi_msg_recv_iov_post,
    .recv_poll_one    = &nm_ofi_msg_recv_poll_one,
    .recv_cancel      = &nm_ofi_msg_recv_cancel,
  };

/* ********************************************************* */

PADICO_MODULE_COMPONENT(Minidriver_ofi_msg,
    puk_component_declare("Minidriver_ofi_msg",
                          puk_component_attr("provider", "auto"),
                          puk_component_attr("fabric", "auto"),
                          puk_component_attr("domain", "auto"),
                          puk_component_provides("PadicoComponent", "component", &nm_ofi_msg_component),
                          puk_component_provides("NewMad_minidriver", "minidriver", &nm_ofi_msg_minidriver)));

/* ********************************************************* */

/** 'ofi_msg' driver per-context data. */
struct nm_ofi_msg_context_s
{
  struct nm_ofi_common_context_s ofi;
  struct fid_poll*poll;
  int next_key;         /**< next requested registration key */
  int need_mr;
  struct fid_pep*pep;   /**< passive endpoint for connection */
  void*url;
  size_t url_size;
};

/** 'ofi_msg' per-instance status. */
struct nm_ofi_msg_s
{
  struct nm_ofi_msg_context_s*p_ofi_context;
  struct fid_ep*ep;  /**< active endpoint, used for data exchange with FI_EP_MSG type */
  struct
  {
    const struct iovec*v; /**< iovec to data to send in case of iovec-based operation; NULL otherwise */
    int n;                /**< number of entries in iovec */
    nm_len_t len;
    struct fid_mr*mr;
    struct fid_cq*cq;  /**< completion queue for send */
    struct nm_ofi_context context;
  } send;
  struct
  {
    struct fid_mr*mr;
    struct fid_cq*cq;  /**< completion queue for recv */
    struct nm_ofi_context context;
  } recv;
};

/* ********************************************************* */

static void* nm_ofi_msg_instantiate(puk_instance_t instance, puk_context_t context)
{
  struct nm_ofi_msg_s*p_status = padico_malloc(sizeof(struct nm_ofi_msg_s));
  struct nm_ofi_msg_context_s*p_ofi_context = puk_context_get_status(context);
  assert(p_ofi_context != NULL);
  p_status->p_ofi_context = p_ofi_context;
  p_status->recv.mr = NULL;
  p_status->recv.cq = NULL;
  p_status->send.v = NULL;
  p_status->send.n = 0;
  p_status->send.mr = NULL;
  p_status->send.cq = NULL;
  p_status->ep = NULL;
  return p_status;
}

static void nm_ofi_msg_destroy(void*_status)
{
  struct nm_ofi_msg_s*p_status = _status;
  NM_TRACEF("destroy()\n");
  fi_close(&p_status->send.mr->fid);
  fi_close(&p_status->recv.mr->fid);
  if(p_status->send.cq)
    {
      fi_close(&p_status->send.cq->fid);
    }
  if(p_status->recv.cq)
    {
      fi_close(&p_status->recv.cq->fid);
    }
  fi_close(&p_status->ep->fid);
  padico_free(p_status);
}

/* ********************************************************* */

static void nm_ofi_msg_endpoint_open(struct nm_ofi_msg_context_s*p_ofi_context, struct nm_ofi_msg_s*p_status, struct fi_info*p_info)
{
  struct fi_cq_attr cq_attr;
  memset(&cq_attr, 0, sizeof(cq_attr));
  cq_attr.size = p_ofi_context->ofi.fi->tx_attr->size;
  cq_attr.flags = 0;
  cq_attr.format = FI_CQ_FORMAT_MSG;
  cq_attr.wait_obj = FI_WAIT_NONE;

  int rc = fi_endpoint(p_ofi_context->ofi.domain, p_info, &p_status->ep, NULL);
  NM_OFI_CHECK_RC(rc, "fi_endpoint");
  /* bind endpoint to global event queue */
  rc = fi_ep_bind(p_status->ep, &p_ofi_context->ofi.eq->fid, 0);
  NM_OFI_CHECK_RC(rc, "fi_ep_bind");
  /* open & bind CQs */
  rc = fi_cq_open(p_ofi_context->ofi.domain, &cq_attr, &p_status->send.cq, NULL);
  NM_OFI_CHECK_RC(rc, "fi_cq_open");
  rc = fi_ep_bind(p_status->ep, &p_status->send.cq->fid, FI_TRANSMIT);
  NM_OFI_CHECK_RC(rc, "fi_ep_bind");
  rc = fi_cq_open(p_ofi_context->ofi.domain, &cq_attr, &p_status->recv.cq, NULL);
  NM_OFI_CHECK_RC(rc, "fi_cq_open");
  rc = fi_ep_bind(p_status->ep, &p_status->recv.cq->fid, FI_RECV);
  NM_OFI_CHECK_RC(rc, "fi_ep_bind");
}

static void nm_ofi_msg_endpoint_wait_connected(struct nm_ofi_msg_context_s*p_ofi_context, struct nm_ofi_msg_s*p_status)
{
  struct fi_eq_cm_entry entry;
  uint32_t event;
  NM_TRACEF("waiting for event...\n");
  int rc = fi_eq_sread(p_ofi_context->ofi.eq, &event, &entry, sizeof entry, -1, 0);
  NM_TRACEF("got event on event queue.\n");
  if(rc == -FI_EAVAIL)
    {
      struct fi_eq_err_entry error_entry;
      fi_eq_readerr(p_ofi_context->ofi.eq, &error_entry, 0);
      char buf[1024];
      NM_FATAL("error event: %s\n",
              fi_eq_strerror(p_ofi_context->ofi.eq, error_entry.prov_errno, error_entry.err_data, buf, 1024));
    }
  else if(rc < 0)
    {
      NM_FATAL("fi_eq_sread failed: %s (%d)\n", fi_strerror(-rc), -rc);
    }
  if(rc != sizeof(entry))
    {
      NM_FATAL("Reading from event queue failed (after connect): %s (%d)\n", fi_strerror(-rc), -rc);
    }
  if(event != FI_CONNECTED || entry.fid != &p_status->ep->fid)
    {
      NM_FATAL("Unexpected CM (event %d fid %p) (pep %p)", event, entry.fid, p_status->ep);
    }
}

static void nm_ofi_msg_getprops(puk_context_t context, struct nm_minidriver_properties_s*p_props)
{
  struct nm_ofi_msg_context_s*p_ofi_context = padico_malloc(sizeof(struct nm_ofi_msg_context_s));
  puk_context_set_status(context, p_ofi_context);

  nm_ofi_common_init(&p_ofi_context->ofi, context, FI_EP_MSG, 0);

  /* fill properties */
  p_props->nickname = "ofi_msg";
  p_props->profile.latency = 500;
  p_props->profile.bandwidth = 8000;
  p_props->capabilities.supports_data = 0;
  p_props->capabilities.prefers_wait_any = 0;
  p_props->capabilities.supports_recv_any = 0;
  p_props->capabilities.supports_buf_send = 0;
  p_props->capabilities.supports_buf_recv = 0;
  p_props->capabilities.supports_iovec = 1;
}

static void nm_ofi_msg_init(puk_context_t context, const void**drv_url, size_t*url_size)
{
  struct nm_ofi_msg_context_s*p_ofi_context = puk_context_get_status(context);
  nm_ofi_common_open(&p_ofi_context->ofi);

  /* open passive endpoint */
  int rc = fi_passive_ep(p_ofi_context->ofi.fabric, p_ofi_context->ofi.fi, &p_ofi_context->pep, NULL);
  NM_OFI_CHECK_RC(rc, "fi_passive_ep");
  rc = fi_pep_bind(p_ofi_context->pep, &p_ofi_context->ofi.eq->fid, 0);
  NM_OFI_CHECK_RC(rc, "fi_pep_bind");
  rc = fi_listen(p_ofi_context->pep);
  NM_OFI_CHECK_RC(rc, "fi_listen");
  /* get URL */
  p_ofi_context->url_size = 0;
  fi_getname(&p_ofi_context->pep->fid, NULL, &p_ofi_context->url_size);
  p_ofi_context->url = padico_malloc(p_ofi_context->url_size);
  rc = fi_getname(&p_ofi_context->pep->fid, p_ofi_context->url, &p_ofi_context->url_size);
  NM_OFI_CHECK_RC(rc, "fi_getname");
  p_ofi_context->next_key = 0;

  *drv_url = p_ofi_context->url;
  *url_size = p_ofi_context->url_size;
  NM_TRACEF("# nm_ofi- init done; url_size = %zd.\n", p_ofi_context->url_size);
}

static void nm_ofi_msg_close(puk_context_t context)
{
  struct nm_ofi_msg_context_s*p_ofi_context = puk_context_get_status(context);
  NM_TRACEF("# nm_ofi- closing...\n");
  puk_context_set_status(context, NULL);
  padico_free(p_ofi_context->url);
  fi_close(&p_ofi_context->pep->fid);
  nm_ofi_common_close(&p_ofi_context->ofi);
  padico_free(p_ofi_context);
}

static void nm_ofi_msg_connect(void*_status, const void*remote_url, size_t url_size)
{
  struct nm_ofi_msg_s*p_status = _status;
  struct nm_ofi_msg_context_s*p_ofi_context = p_status->p_ofi_context;
  NM_TRACEF("# nm_ofi- connecting...\n");

  const int is_server = (p_ofi_context->url_size > url_size) ||
    ( (p_ofi_context->url_size == url_size) && (memcmp(p_ofi_context->url, remote_url, url_size) > 0) );
  if(!is_server)
    {
      /* ** active connect */
      NM_TRACEF("active connect...\n");
      /* open connection endpoint */
      nm_ofi_msg_endpoint_open(p_ofi_context, p_status, p_ofi_context->ofi.fi);

      /*
        struct fi_poll_attr poll_attr;
        poll_attr.flags = 0;
        rc = fi_poll_open(p_ofi_context->ofi.domain, &poll_attr, &p_ofi_context->poll);
        if(rc != 0)
        {
        NM_FATAL("fi_poll_open()- error %d (%s).\n", rc, fi_strerror(-rc));
        }
        rc = fi_poll_add(p_ofi_context->poll, &p_status->cq->fid, 0);
        if(rc != 0)
        {
        NM_FATAL("fi_poll_add()- error %d (%s).\n", rc, fi_strerror(-rc));
        }
      */

      /* issue connection request */
      int rc = fi_connect(p_status->ep, remote_url, NULL, 0);
      NM_OFI_CHECK_RC(rc, "fi_connect");
      /* wait for the connection to be established */
      nm_ofi_msg_endpoint_wait_connected(p_ofi_context, p_status);
    }
  else
    {
      /* ** passive connect */
      NM_TRACEF("passive connect...\n");
      /* Wait for a connection request */
      struct fi_eq_cm_entry entry;
      uint32_t event;
      int rc = fi_eq_sread(p_ofi_context->ofi.eq, &event, &entry, sizeof entry, -1, 0);
      if(rc < 0)
        {
          NM_FATAL("fi_eq_sread()- error %d (%s)\n", -rc, fi_strerror(-rc));
        }
      if(rc != sizeof entry)
        {
          NM_FATAL("Reading from event queue failed (after listen): %s (%d)\n", fi_strerror(-rc), -rc);
        }
      NM_TRACEF("got event %d on event queue.\n", event);
      if(event != FI_CONNREQ)
        {
          NM_FATAL("Unexpected event: %d", event);
        }
      /* open active endpoint */
      nm_ofi_msg_endpoint_open(p_ofi_context, p_status, entry.info);
      /* accept connection */
      rc = fi_accept(p_status->ep, NULL, 0);
      NM_OFI_CHECK_RC(rc, "fi_accept");
      /* wait for the connection to be etablished */
      nm_ofi_msg_endpoint_wait_connected(p_ofi_context, p_status);
    }
  NM_TRACEF("connection success!\n");
  int rc1 = fi_enable(p_status->ep);
  NM_OFI_CHECK_RC(rc1, "fi_enable");
}

static int nm_ofi_msg_send_iov_post(void*_status, const struct iovec*v, int n)
{
  struct nm_ofi_msg_s*p_status = _status;
  struct nm_ofi_msg_context_s*p_ofi_context = p_status->p_ofi_context;
  assert(p_status->send.v == NULL);
  p_status->send.v = v;
  p_status->send.n = n;
  p_status->send.len = 0;
  int i;
  for(i = 0; i < n; i++)
    {
      p_status->send.len += v[i].iov_len;
    }
  int rc = fi_sendv(p_status->ep, v, NULL /* fi_mr_desc(p_status->send.mr) */, n, 0, &p_status->send.context);
  NM_OFI_CHECK_RC(rc, "fi_send");
  return NM_ESUCCESS;
}

static int nm_ofi_msg_send_poll(void*_status)
{
  struct nm_ofi_msg_s*p_status = _status;
  struct nm_ofi_msg_context_s*p_ofi_context = p_status->p_ofi_context;
  struct fi_cq_msg_entry entry;
  int rc = fi_cq_read(p_status->send.cq, &entry, 1);
  if(rc == 1)
    {
      NM_TRACEF("# nm_ofi- send SUCCESS; len = %d.\n", (int)entry.len);
      assert(entry.flags & FI_MSG);
      assert(entry.flags & FI_SEND);
      p_status->send.v = NULL;
      return NM_ESUCCESS;
    }
  else if(rc == -FI_EAGAIN)
    {
      return -NM_EAGAIN;
    }
  else if(rc == -FI_EAVAIL)
    {
      /* TODO- fi_cq_readerr() */
      NM_FATAL("fi_cq_read()- asynchronous operation failed.");
    }
  else
    {
      NM_FATAL("fi_cq_read()- error %d (%s)\n", rc, fi_strerror(-rc));
    }
}

static int nm_ofi_msg_recv_iov_post(void*_status, struct iovec*v, int n)
{
  struct nm_ofi_msg_s*p_status = _status;
  struct nm_ofi_msg_context_s*p_ofi_context = p_status->p_ofi_context;
  assert(!p_status->p_ofi_context->need_mr);
  int rc = fi_recvv(p_status->ep, v, NULL /* fi_mr_desc(p_status->recv.mr)*/, n, 0, &p_status->recv.context);
  NM_OFI_CHECK_RC(rc, "fi_recvv");
  return NM_ESUCCESS;
}

static int nm_ofi_msg_recv_poll_one(void*_status)
{
  struct nm_ofi_msg_s*p_status = _status;
  struct fi_cq_msg_entry entry;
  int rc = fi_cq_read(p_status->recv.cq, &entry, 1);
  if(rc == 1)
    {
      NM_TRACEF("# nm_ofi- recv SUCCESS - len = %d.\n", (int)entry.len);
      return NM_ESUCCESS;
    }
  else if(rc == -FI_EAGAIN)
    {
      return -NM_EAGAIN;
    }
  else if(rc == -FI_EAVAIL)
    {
      /* TODO- fi_cq_readerr() */
      NM_FATAL("fi_cq_read()- asynchronous operation failed.");
    }
  else
    {
      NM_FATAL("fi_cq_read()- error %d (%s)\n", rc, fi_strerror(-rc));
    }
}

static int nm_ofi_msg_recv_cancel(void*_status)
{
  /* TODO */
  return -NM_ENOTIMPL;
}

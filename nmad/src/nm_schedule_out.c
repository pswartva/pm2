/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


#include <stdint.h>
#include <sys/uio.h>
#include <assert.h>

#include <nm_private.h>

PADICO_MODULE_HOOK(nmad);


nm_seq_t nm_core_send_seq_get(struct nm_core*p_core, nm_gate_t p_gate, nm_core_tag_t core_tag)
{
  nm_core_lock(p_core);
  nm_core_task_flush(p_core); /* flush all pending packets without a seq yet to ensure consistency */
  struct nm_gtag_s*p_gtag = nm_gtag_get(&p_gate->tags, core_tag);
  nm_seq_t seq = nm_seq_next(p_gtag->send_seq_number);
  p_gtag->send_seq_number = seq;
  nm_core_unlock(p_core);
  return seq;
}

void nm_core_pack_init(struct nm_core*p_core, struct nm_req_s*p_pack)
{
  nm_status_init(p_pack, NM_STATUS_PACK_INIT);
  nm_req_list_cell_init(p_pack);
  p_pack->flags         = NM_REQ_FLAG_PACK;
  p_pack->pack.done     = 0;
  p_pack->pack.hlen     = 0;
  p_pack->pack.priority = 0;
  p_pack->monitor       = NM_MONITOR_NULL;
  p_pack->err           = NM_ESUCCESS;
}

void nm_core_pack_set_priority(struct nm_core*p_core, struct nm_req_s*p_pack, nm_prio_t priority)
{
  const struct nm_strategy_iface_s*strategy_iface = p_core->strategy_iface;
  if((priority != 0) && !strategy_iface->capabilities.supports_priorities)
    {
      static int warning_done = 0;
      if(!warning_done)
        {
          NM_WARN("trying to attach priority to a request with a strategy that does not support priorities.\n");
          warning_done = 1;
        }
    }
  p_pack->pack.priority = priority;
}

void nm_core_pack_data(nm_core_t p_core, struct nm_req_s*p_pack, const struct nm_data_s*p_data)
{
  assert(p_pack->pack.done == 0);
  assert(p_pack->flags & NM_REQ_FLAG_PACK);
  p_pack->data     = *p_data;
  p_pack->pack.len = nm_data_size(&p_pack->data);
#ifdef NMAD_DEBUG
  if(p_core->enable_isend_csum)
    {
      p_pack->pack.checksum = nm_data_checksum(&p_pack->data);
    }
#endif /* NMAD_DEBUG */
}

void nm_core_pack_send(struct nm_core*p_core, struct nm_req_s*p_pack, nm_core_tag_t tag, nm_gate_t p_gate,
                       nm_req_flag_t flags)
{
  assert(p_gate != NULL);
  nm_status_assert(p_pack, NM_STATUS_PACK_INIT);
  nm_status_add(p_pack, NM_STATUS_PACK_POSTED);
  p_pack->flags |= flags;
  p_pack->seq    = NM_SEQ_NONE;
  p_pack->tag    = tag;
  p_pack->p_gate = p_gate;
  p_pack->p_gtag = NULL;
  p_pack->req_chunk.p_req = NULL;
}

void nm_core_pack_submit(struct nm_core*p_core, struct nm_req_s*p_pack)
{
  const nm_len_t size = nm_data_size(&p_pack->data);
  assert(p_pack->p_gate != NULL);
  assert(p_pack->pack.hlen <= size);
  if((p_pack->pack.hlen > 0) && (size >= NM_DATA_IOV_THRESHOLD))
    {
      struct nm_req_chunk_s*p_req_chunk = nm_req_chunk_alloc(p_core);
      nm_req_chunk_init(p_req_chunk, p_pack, 0, p_pack->pack.hlen);
      nm_req_chunk_init(&p_pack->req_chunk, p_pack, p_pack->pack.hlen, size - p_pack->pack.hlen);
      nm_req_chunk_submit(p_core, p_req_chunk);
      nm_req_chunk_submit(p_core, &p_pack->req_chunk);
    }
  else
    {
      nm_req_chunk_init(&p_pack->req_chunk, p_pack, 0, size);
      nm_req_chunk_submit(p_core, &p_pack->req_chunk);
    }
  nm_profile_inc(p_core->profiling.n_packs);
  nm_trace_event(NM_TRACE_EVENT_CORE_PACK_SUBMIT, p_pack, p_pack->p_gate, NULL, p_pack->tag, NM_SEQ_NONE, p_pack->pack.len);
  if(p_core->enable_auto_flush)
    {
      nm_core_flush(p_core);
      nm_schedule(p_core);
    }
}

void nm_core_pack_submit_chunks(struct nm_core*p_core, struct nm_req_s*p_pack, int n, const struct nm_chunk_s*p_chunks)
{
  assert(p_pack->p_gate != NULL);
  const nm_len_t size = nm_data_size(&p_pack->data);
  if((n > 1) || (p_chunks[0].chunk_offset > 0) || (p_chunks[0].chunk_len < p_pack->pack.len))
    {
      /* request is not single-chunk; send msg size asap */
      p_pack->flags |= NM_REQ_FLAG_PACK_PARTITIONED;
      nm_profile_inc(p_core->profiling.n_packs_partitioned);
    }
  int i;
  for(i = 0; i < n; i++)
    {
      assert(p_chunks[i].chunk_offset + p_chunks[i].chunk_len <= size);
      struct nm_req_chunk_s*p_req_chunk = nm_req_chunk_alloc(p_core);
      nm_req_chunk_init(p_req_chunk, p_pack, p_chunks[i].chunk_offset, p_chunks[i].chunk_len);
      nm_req_chunk_submit(p_core, p_req_chunk);
      if(p_chunks[i].chunk_offset == 0)
        {
          nm_profile_inc(p_core->profiling.n_packs);
        }
    }
}


/** Places a packet in the send request list.
 * to be called from a strategy
 */
void nm_core_post_send(struct nm_pkt_wrap_s*p_pw, nm_gate_t p_gate, nm_trk_id_t trk_id)
{
  nm_core_lock_assert(p_gate->p_core);
  /* Packet is assigned to given track, driver, and gate */
  nm_pw_assign(p_pw, trk_id, NULL, p_gate);
  p_pw->flags |= NM_PW_SEND;
  if(p_pw->p_trk->kind == nm_trk_small)
    {
      assert(p_pw->length <= NM_MAX_UNEXPECTED);
    }
  /* append pkt to scheduler post list */
  struct puk_receptacle_NewMad_Strategy_s*p_strategy = &p_pw->p_gate->strategy_receptacle;
  if(p_strategy->driver->pw_send_post == NULL)
    {
      /* default strategy to output pw */
      nm_pw_send_post_default(p_pw);
    }
  else
    {
      (*p_strategy->driver->pw_send_post)(p_strategy->_status, p_pw);
    }
}

/** trigger the sending of the given pw */
void nm_pw_send_trigger(struct nm_pkt_wrap_s*p_pw)
{
  nm_core_lock_assert(p_pw->p_gate->p_core);
  assert(p_pw->p_trk->p_pw_send == NULL);
  p_pw->p_trk->p_pw_send = p_pw;
#ifdef PIOMAN
  if((p_pw->flags & NM_PW_PREFETCHING) && !(p_pw->flags & NM_PW_PREFETCHED))
    {
      /* prefetching was requested for this pw but not completed yet;
       * ltask is in progress; let the prefetch complete, than pw send ltask
       * will be submitted from prefetching completion core task */
    }
  else
    {
      nm_ltask_submit_pw_send(p_pw);
    }
#else /* PIOMAN */
  /* directly call the driver from here, pretending to release the lock (but no pioman here) */
  nm_core_t p_core = nm_core_get_singleton();
  nm_core_unlock(p_core);
  nm_pw_send_post(p_pw);
  nm_core_lock(p_core);
#endif /* PIOMAN */
}

/** default function for pw_send_post- immediate send if trk is available */
void nm_pw_send_post_default(struct nm_pkt_wrap_s*p_pw)
{
  struct nm_trk_s*p_trk = p_pw->p_trk;
  if(p_trk->p_pw_send == NULL)
    {
      assert(nm_pkt_wrap_list_empty(&p_trk->pending_pw_send));
      nm_pw_send_trigger(p_pw);
    }
  else
    {
      nm_pkt_wrap_list_push_back(&p_trk->pending_pw_send, p_pw);
    }
}

/** default function for pw_send_complete- send next pw in pending list, if any */
void nm_pw_send_complete_default(struct nm_pkt_wrap_s*p_pw)
{
  struct nm_trk_s*const p_trk = p_pw->p_trk;
  if(!nm_pkt_wrap_list_empty(&p_trk->pending_pw_send))
    {
      struct nm_pkt_wrap_s*p_pw2 = nm_pkt_wrap_list_pop_front(&p_trk->pending_pw_send);
      nm_pw_send_trigger(p_pw2);
    }
}

void nm_pw_send_progress(struct nm_pkt_wrap_s*p_pw)
{
  const nm_pw_flag_t flags = p_pw->flags;
  assert(flags != NM_PW_FLAG_NONE);
  assert(!((flags & NM_PW_PREFETCHING) && !(flags & NM_PW_PREFETCHED) ));
  if(flags & NM_PW_POSTED)
    {
      nm_pw_send_poll(p_pw);
    }
  else if(flags & NM_PW_SEND)
    {
      nm_pw_send_post(p_pw);
    }
  else
    {
      NM_FATAL("inconsistent state for pw; flags = 0x%x\n", flags);
    }
}

/** Process a complete successful outgoing request.
 */
void nm_pw_process_complete_send(struct nm_core*p_core, struct nm_pkt_wrap_s*p_pw)
{
  struct nm_trk_s*const p_trk = p_pw->p_trk;
  nm_gate_t const p_gate = p_pw->p_gate;
  nm_core_lock_assert(p_core);
  nm_profile_inc(p_core->profiling.n_pw_out);
  nm_trace_event(NM_TRACE_EVENT_PW_COMPLETE_SEND, p_pw, p_pw->p_gate, p_pw->p_drv, NM_CORE_TAG_NONE, NM_SEQ_NONE, p_pw->length);
  NM_TRACEF("send request complete: gate %p, drv %p, trk %d\n", p_pw->p_gate, p_pw->p_drv, p_pw->trk_id);
  assert(p_trk->p_pw_send == p_pw);
  p_trk->p_pw_send = NULL;
  while(!nm_req_chunk_list_empty(&p_pw->req_chunks))
    {
      struct nm_req_chunk_s*p_req_chunk = nm_req_chunk_list_pop_front(&p_pw->req_chunks);
      struct nm_req_s*p_pack = p_req_chunk->p_req;
      const nm_len_t chunk_len = p_req_chunk->chunk_len;
      assert(nm_status_test(p_pack, NM_STATUS_PACK_POSTED));
      p_pack->pack.done += chunk_len;
      nm_req_chunk_destroy(p_core, p_req_chunk);
      if(p_pack->pack.done == p_pack->pack.len)
        {
          NM_TRACEF("all chunks sent for msg seq=%u len=%zu!\n", p_pack->seq, p_pack->pack.len);
#ifdef NMAD_DEBUG
          if(p_core->enable_isend_csum)
            {
              const uint32_t checksum = nm_data_checksum(&p_pack->data);
              if(checksum != p_pack->pack.checksum)
                {
                  NM_FATAL("data corrupted between isend and completion.\n");
                }
            }
#endif /* NMAD_DEBUG */

          const int is_sync = !!(p_pack->flags & NM_REQ_FLAG_PACK_SYNCHRONOUS);
          const int acked = (is_sync && nm_status_test(p_pack, NM_STATUS_ACK_RECEIVED));
          const int finalized = (acked || !is_sync);
          const struct nm_core_event_s event =
            {
              .status = NM_STATUS_PACK_COMPLETED | (finalized ? NM_STATUS_FINALIZED : 0),
              .p_req = p_pack
            };
          if(finalized)
            {
              if(acked)
                {
                  nm_req_list_remove(&p_pack->p_gtag->pending_packs, p_pack);
                }
              p_core->n_packs--;
              nm_trace_var(NM_TRACE_EVENT_VAR_N_PACKS, p_core->n_packs, p_gate, p_trk->p_drv);
              nm_core_polling_level(p_core);
            }
          nm_trace_event(NM_TRACE_EVENT_CORE_PACK_COMPLETED, p_pack, p_gate, p_trk->p_drv, p_pack->tag, p_pack->seq, p_pack->pack.len);
          nm_core_status_event(p_pw->p_gate->p_core, &event, p_pack);
        }
      else if(p_pack->pack.done > p_pack->pack.len)
        {
          NM_FATAL("more bytes sent than posted (should have been = %lu; actually sent = %lu)\n",
                       p_pack->pack.len, p_pack->pack.done);
        }
    }
  struct puk_receptacle_NewMad_Strategy_s*p_strategy = &p_gate->strategy_receptacle;
  if(p_strategy->driver->pw_send_complete == NULL)
    {
      /* default strategy to output pw */
      nm_pw_send_complete_default(p_pw);
    }
  else
    {
      (*p_strategy->driver->pw_send_complete)(p_strategy->_status, p_pw);
    }
  nm_pw_ref_dec(p_pw);
}

/** Poll an active outgoing request.
 */
void nm_pw_send_poll(struct nm_pkt_wrap_s*p_pw)
{
  struct nm_core*p_core = p_pw->p_gate->p_core;
  nm_core_nolock_assert(p_core);
  assert(p_pw->flags & NM_PW_FINALIZED || p_pw->flags & NM_PW_NOHEADER);
  if(PUK_VG_RUNNING_ON_VALGRIND)
    sched_yield();
#ifdef NMAD_PROFILE
  {
    puk_tick_t t;
    PUK_GET_TICK(t);
    const double tdiff = PUK_TIMING_DELAY(p_pw->start_transfer_time, t);
    static double last_warn = 0.0;
    if(p_core->enable_pwsend_timeout)
      {
        if(tdiff > NM_PW_TIMEOUT)
          {
            const double time_warn = PUK_TIMING_DELAY(p_core->time_orig, t);
            if(time_warn > last_warn + NM_PW_TIMEOUT_PERIOD)
              {
                last_warn = time_warn;
                NM_WARN("pw = %p send timeout; trk_id = %d; drv = %s; t = %g usec.\n",
                        p_pw, p_pw->trk_id, p_pw->p_drv->driver_id, tdiff);
                puk_debug_gdb_backtrace();
              }
          }
      }
  }
#endif /* NMAD_PROFILE */
  struct puk_receptacle_NewMad_minidriver_s*r = &p_pw->p_trk->receptacle;
  int err = (*r->driver->send_poll)(r->_status);
#ifdef NMAD_DEBUG
  if(err == NM_ESUCCESS)
    {
      struct nm_data_s*p_data = &p_pw->p_trk->sdata;
      nm_data_null_build(p_data);
    }
#endif /* NMAD_DEBUG */
  if(err == NM_ESUCCESS)
    {
#ifdef NMAD_PROFILE
      {
        puk_tick_t t;
        PUK_GET_TICK(t);
        const double tdiff = PUK_TIMING_DELAY(p_pw->start_transfer_time, t);
        struct nm_drv_s*p_drv = p_pw->p_trk->p_drv;
        p_drv->profiling.total_send_usecs += tdiff;
        p_drv->profiling.average_send_bw_Mbps = (double)p_drv->profiling.n_send_bytes / (double)p_drv->profiling.total_send_usecs;
        if(p_core->enable_pwsend_timeout)
          {
            if(tdiff > NM_PW_TIMEOUT)
              {
                NM_WARN("pw = %p send COMPLETED after timeout; trk_id = %d; drv = %s; t = %g usec.\n",
                        p_pw, p_pw->trk_id, p_pw->p_drv->driver_id, tdiff);
              }
          }
      }
#endif /* NMAD_PROFILE */
#ifndef PIOMAN
      nm_pkt_wrap_list_remove(&p_core->pending_send_list, p_pw);
#endif /* PIOMAN */
      nm_pw_completed_enqueue(p_core, p_pw);
    }
  else if(err != -NM_EAGAIN)
    {
      NM_FATAL("poll_send failed- err = %d", err);
    }
}

/** Wait an active outgoing request.
 */
void nm_pw_send_wait(struct nm_pkt_wrap_s*p_pw)
{
  struct nm_core*p_core = p_pw->p_gate->p_core;
  nm_core_nolock_assert(p_core);
  assert(p_pw->flags & NM_PW_FINALIZED || p_pw->flags & NM_PW_NOHEADER);
  struct puk_receptacle_NewMad_minidriver_s*r = &p_pw->p_trk->receptacle;
  int err = (*r->driver->send_wait)(r->_status);
#ifdef NMAD_DEBUG
  if(err == NM_ESUCCESS)
    {
      struct nm_data_s*p_data = &p_pw->p_trk->sdata;
      nm_data_null_build(p_data);
    }
#endif /* NMAD_DEBUG */
  if(err == NM_ESUCCESS)
    {
#ifdef NMAD_PROFILE
      {
        puk_tick_t t;
        PUK_GET_TICK(t);
        const double tdiff = PUK_TIMING_DELAY(p_pw->start_transfer_time, t);
        struct nm_drv_s*p_drv = p_pw->p_trk->p_drv;
        p_drv->profiling.total_send_usecs += tdiff;
        p_drv->profiling.average_send_bw_Mbps = (double)p_drv->profiling.n_send_bytes / (double)p_drv->profiling.total_send_usecs;
      }
#endif /* NMAD_PROFILE */
#ifndef PIOMAN
      nm_pkt_wrap_list_remove(&p_core->pending_send_list, p_pw);
#endif /* PIOMAN */
      nm_pw_completed_enqueue(p_core, p_pw);
    }
  else
    {
      NM_FATAL("send_wait failed- err = %d (%s)", err, nm_strerror(err));
    }
}

/** prefetch a packet-wrapper on the sender side
 * mostly used to register memory in advance while the rdv is still in progress
 */
void nm_pw_send_prefetch(struct nm_pkt_wrap_s*p_pw)
{
  struct nm_core*p_core = p_pw->p_gate->p_core;
  nm_core_nolock_assert(p_core);
  assert(p_core->enable_send_prefetch);
  struct nm_trk_s*p_trk = &p_pw->p_gate->trks[NM_TRK_LARGE];
  struct nm_drv_s*p_drv = p_trk->p_drv;
  assert(p_drv->props.capabilities.supports_send_prefetch);
  assert(!p_drv->props.capabilities.supports_data); /* prefetching not supported with nm_data yet */
  nm_profile_inc(p_drv->profiling.n_send_prefetch);
  nm_pw_data_to_iovec(p_pw);
  struct puk_receptacle_NewMad_minidriver_s*r = &p_trk->receptacle;
  (*r->driver->send_iov_prefetch)(r->_status, p_pw->v, p_pw->v_nb);
  p_pw->flags |= NM_PW_PREFETCHED;
  /* issue a completion task to trigger normal processing of send */
  p_pw->core_task.kind = NM_CORE_TASK_COMPLETED_PREFETCH;
  p_pw->core_task.content.completed_prefetch.p_pw = p_pw;
  nm_core_task_enqueue(p_core, 0, &p_pw->core_task);
}

/** cancel the send prefetch of a pw, because it eventually got accepted
 * on another track or was split.
 */
void nm_pw_send_unfetch(struct nm_pkt_wrap_s*p_pw)
{
  struct nm_core*p_core = p_pw->p_gate->p_core;
  assert(p_core->enable_send_prefetch);
  assert(p_pw->flags & NM_PW_PREFETCHING);
  assert(p_pw->v_nb > 0);
#ifdef PIOMAN
  /* cancel ltask ifever it is still running, then execute unfetch here */
  piom_ltask_cancel_immediate(&p_pw->ltask);
#endif /* PIOMAN */
  if(p_pw->flags & NM_PW_PREFETCHED)
    {
      /* unfetch only if ltask for prefetching was already scheduled */
      struct nm_trk_s*p_trk = &p_pw->p_gate->trks[NM_TRK_LARGE];
      nm_profile_inc(p_trk->p_drv->profiling.n_send_unfetch);
      struct puk_receptacle_NewMad_minidriver_s*r = &p_trk->receptacle;
      (*r->driver->send_iov_unfetch)(r->_status, p_pw->v, p_pw->v_nb);
    }
  p_pw->flags &= ~(NM_PW_PREFETCHED | NM_PW_PREFETCHING);
  p_pw->v_nb = 0;
}

/** Post a new outgoing request.
    - this function must handle immediately completed requests properly
*/
void nm_pw_send_post(struct nm_pkt_wrap_s*p_pw)
{
  struct nm_core*p_core = p_pw->p_drv->p_core;
  struct puk_receptacle_NewMad_minidriver_s*r = &p_pw->p_trk->receptacle;
  const struct nm_minidriver_capabilities_s*p_capabilities = &p_pw->p_trk->p_drv->props.capabilities;
  /* no lock needed; only this ltask is allowed to touch the pw */
  nm_core_nolock_assert(p_core);

#ifdef PIO_OFFLOAD
  nm_pw_offloaded_finalize(p_pw);
#endif

#ifdef NMAD_PROFILE
  {
    /* no lock needed since we are in the drv ltask */
    struct nm_drv_s*p_drv = p_pw->p_trk->p_drv;
    p_drv->profiling.n_send_packets++;
    p_drv->profiling.n_send_bytes += p_pw->length;
    PUK_GET_TICK(p_pw->start_transfer_time);
  }
#endif /* NMAD_PROFILE */

#ifdef NMAD_TRACE
  nm_trace_event(NM_TRACE_EVENT_PW_POST_SEND, p_pw, p_pw->p_gate, p_pw->p_trk->p_drv, NM_CORE_TAG_NONE, NM_SEQ_NONE, p_pw->length);
  {
    nm_req_chunk_itor_t p_req_chunk;
    puk_list_foreach(nm_req_chunk, p_req_chunk, &p_pw->req_chunks)
      {
        if(p_req_chunk == &p_req_chunk->p_req->req_chunk)
          {
            /* first req chunk in this request */
           nm_trace_event(NM_TRACE_EVENT_CORE_PACK_PW_POSTED, p_req_chunk->p_req, p_pw->p_gate, p_pw->p_trk->p_drv, p_req_chunk->p_req->tag, p_req_chunk->p_req->seq, p_req_chunk->p_req->pack.len);
         }
      }
  }
#endif /* NMAD_TRACE */

  if(p_pw->p_trk->kind == nm_trk_small)
    {
      nm_pw_finalize(p_pw);
    }
  else
    {
      assert(p_pw->p_trk->kind == nm_trk_large);
      if(p_pw->p_trk->p_drv->props.capabilities.needs_rdv_data)
        {
          int rc = (*r->driver->set_rdv_data)(r->_status, &p_pw->rdv_data[0], NM_HEADER_RTR_DATA_SIZE);
          if(rc != NM_ESUCCESS)
            {
              NM_WARN("drv->set_rdv_data() rc = %d (%s)\n", rc, nm_strerror(rc));
            }
        }
    }

  if(p_pw->p_data)
    {
      /* ** pw contains nm_data (=trk large); flatten if needed */
      if(p_capabilities->supports_data)
        {
          /* native nm_data support */
          int rc = (*r->driver->send_data_post)(r->_status, p_pw->p_data, p_pw->chunk_offset, p_pw->length);
          if(rc != NM_ESUCCESS)
            {
              NM_WARN("drv->send_data_post() rc = %d (%s)\n", rc, nm_strerror(rc));
            }
        }
      else if(p_capabilities->supports_buf_send)
        {
          /* buf_send with large messages does not make sens; catch error anyway,
           * just in case one day we have nm_data used on small trks. */
          NM_FATAL("buf_send for nm_data not implemented yet.");
        }
      else
        {
          /* asume prefetching has completed if it was required. */
          assert(!((p_pw->flags & NM_PW_PREFETCHING) && !(p_pw->flags & NM_PW_PREFETCHED)));
          /* no data support, no buf_send ->iovec or contig */
          const int density = p_pw->data_props.size / p_pw->data_props.blocks;
          if( p_pw->data_props.is_contig ||
              ( p_capabilities->supports_iovec &&
                ((p_capabilities->max_iovecs == 0) || (p_capabilities->max_iovecs > p_pw->data_props.blocks)) &&
                (density > NM_LARGE_MIN_DENSITY)) )
            {
              /* convert from nm_data to iovec */
              if(!(p_pw->flags & NM_PW_PREFETCHED))
                {
                  /* with prefetching, data was already flatten to iovec */
                  nm_pw_data_to_iovec(p_pw);
                }
            }
          else
            {
              /* copy to newly allocated buf */
              if(p_pw->flags & NM_PW_PREFETCHING)
                {
                  /* rules for prefetching are likely to not have prefetched
                   * such pw, but cancel prefetching just in case */
                  nm_pw_send_unfetch(p_pw);
                }
              void*buf = padico_malloc(p_pw->length);
              if(buf == NULL)
                {
                  NM_FATAL("out of memory.\n");
                }
              nm_data_copy_from(p_pw->p_data, p_pw->chunk_offset, p_pw->length, buf);
              struct iovec*v0 = nm_pw_grow_iovec(p_pw);
              v0->iov_base = buf;
              v0->iov_len  = p_pw->length;
              p_pw->flags |= NM_PW_DYNAMIC_V0;
            }
          p_pw->p_data = NULL;
          int rc = (*r->driver->send_iov_post)(r->_status, p_pw->v, p_pw->v_nb);
          if(rc != NM_ESUCCESS)
            {
              NM_WARN("drv->send_iov_post() rc = %d (%s)\n", rc, nm_strerror(rc));
            }
        }
    }
  else
    {
      /* ** pw contains iovec (= trk small) */
      if(p_pw->flags & NM_PW_BUF_SEND)
        {
          assert(p_pw->length <= p_pw->max_len);
          int rc = (*r->driver->send_buf_post)(r->_status, p_pw->length);
          if(rc != NM_ESUCCESS)
            {
              NM_WARN("drv->send_buf_post() rc = %d (%s)\n", rc, nm_strerror(rc));
            }
        }
      else if(r->driver->send_iov_post)
        {
          int rc = (*r->driver->send_iov_post)(r->_status, &p_pw->v[0], p_pw->v_nb);
          if(rc != NM_ESUCCESS)
            {
              NM_WARN("drv->send_iov_post() rc = %d (%s)\n", rc, nm_strerror(rc));
            }
        }
      else
        {
          assert(r->driver->send_data_post);
          struct nm_data_s*p_data = &p_pw->p_trk->sdata;
          assert(nm_data_isnull(p_data));
          nm_data_iov_set(p_data, (struct nm_data_iov_s){ .v = &p_pw->v[0], .n = p_pw->v_nb });
          int rc = (*r->driver->send_data_post)(r->_status, p_data, 0 /* chunk_offset */, p_pw->length);
          if(rc != NM_ESUCCESS)
            {
              NM_WARN("drv->send_data_post() rc = %d (%s)\n", rc, nm_strerror(rc));
            }
        }
    }
  p_pw->flags |= NM_PW_POSTED;
#ifndef PIOMAN
  /* put the request in the list of pending requests; no lock needed since no thread without pioman */
  nm_pkt_wrap_list_push_back(&p_core->pending_send_list, p_pw);
#endif /* PIOMAN */

#ifdef NMAD_PROFILE
  {
    puk_tick_t t;
    PUK_GET_TICK(t);
    const double tdiff = PUK_TIMING_DELAY(p_pw->start_transfer_time, t);
    p_pw->p_trk->p_drv->profiling.total_send_post_usecs += tdiff;
  }
#endif /* NMAD_PROFILE */

  if(!p_capabilities->no_send_poll)
    {
      nm_pw_send_poll(p_pw);
    }
}

/*
 * NewMadeleine
 * Copyright (C) 2021-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

/* Example / test of the mcast interface where data is used by the application
 * just after the reception, while mcast still forwards to other nodes. */

#include <stdlib.h>
#include <stdio.h>

#include <nm_launcher_interface.h>
#include <nm_coll_interface.h>
#include <nm_mcast_interface.h>

#include "../common/nm_examples_helper.h"

static void recv_monitor(nm_sr_event_t event, const nm_sr_event_info_t* p_info, void* ref)
{
  // We do NOT want the two events are triggered together:
  if ((event & NM_SR_EVENT_RECV_COMPLETED) && (event & NM_SR_EVENT_FINALIZED))
    {
      NM_FATAL("Both events are triggered at the same time, we don't want that in this test...\n");
    }

  nm_cond_status_t* cond_received = (nm_cond_status_t*) ref;

  if (event & NM_SR_EVENT_RECV_COMPLETED)
    {
      nm_cond_signal(cond_received, 1);
    }
  else if (event & NM_SR_EVENT_FINALIZED)
    {
      nm_cond_signal(cond_received, 2);
    }
}

int main(int argc, char** argv)
{
  int i, worldsize;
  const nm_tag_t tag = 0xbeef;
  const char msg[] = "Hello, world!";
  const nm_len_t msg_size = strlen(msg) + 1;
  struct nm_data_s data;

  nm_examples_init_topo(&argc, argv, NM_EXAMPLES_TOPO_STAR);

  nm_launcher_get_size(&worldsize);
  if(worldsize == 1)
    {
      fprintf(stderr, "We need several nodes.\n");
      nm_examples_exit();
      return EXIT_FAILURE;
    }

  nm_mcast_service_t mcast_service = nm_mcast_init(p_comm);

  if (is_server)
    {
      int* dests = malloc(sizeof(int) * (worldsize - 1));
      for (i = 1; i < worldsize; i++)
        {
          dests[i-1] = i;
        }

      nm_data_contiguous_build(&data, (char*) msg, msg_size);

      nm_mcast_send(mcast_service, p_comm, dests, NULL, worldsize-1, tag, &data, 0, NM_COLL_TREE_DEFAULT);

      free(dests);
    }
  else
    {
      nm_session_t p_session = nm_comm_get_session(p_comm);
      char* buffer = malloc(msg_size);
      nm_sr_request_t req;

      nm_cond_status_t cond_received;
      nm_cond_init(&cond_received, 0);

      nm_data_contiguous_build(&data, (char*) buffer, msg_size);

      nm_sr_recv_init(p_session, &req);
      nm_sr_request_set_ref(&req, &cond_received);
      nm_sr_request_monitor(p_session, &req, NM_SR_EVENT_FINALIZED | NM_SR_EVENT_RECV_COMPLETED, &recv_monitor);
      nm_sr_recv_unpack_data(p_session, &req, &data);
      nm_sr_recv_irecv(p_session, &req, NM_ANY_GATE, tag, NM_TAG_MASK_FULL);

      // We cannot wait with nm_sr_rwait() because the request has a monitor.
      nm_cond_wait(&cond_received, 1, nm_core_get_singleton());

      // Don't printf: could confuse tests if node outputs are mixed.
      /*printf("I received data, I can read it, but NMAD still holds a reference on it.\n");*/
      if (strcmp(buffer, msg))
        {
          printf("Received: '%s' (should be '%s')\n", buffer, msg);
        }

      nm_cond_wait(&cond_received, 2, nm_core_get_singleton());
      /*printf("NMAD released the reference it held on the data, I can do whatever I want with it now.\n");*/

      free(buffer);
      nm_cond_destroy(&cond_received);
    }

  nm_coll_barrier(p_comm, 0xF3);

  printf("success\n");

  nm_mcast_finalize(mcast_service);
  nm_examples_exit();
  return EXIT_SUCCESS;
}

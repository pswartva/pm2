/*
 * NewMadeleine
 * Copyright (C) 2021-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

/* This example / test shows how to receive a data when the size of the data is
 * in the data itself with iovec. */


#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <assert.h>

#include "../common/nm_examples_helper.h"

const char msg[] = "Hello, world!";

struct recv_req
{
  nm_sr_request_t request;
  nm_len_t size;
  char* buffer;
  nm_cond_status_t cond_received;

  struct nm_datav_s datav;
  struct iovec v[2];
  struct nm_data_s data;
};

static void recv_callback_method1(nm_sr_event_t event, const nm_sr_event_info_t* p_info, void* ref)
{
  // Both event can't be triggered at the same time:
  assert(!((event & NM_SR_EVENT_FINALIZED) && (event & NM_SR_EVENT_RECV_DATA)));

  struct recv_req* req = (struct recv_req*) ref;

  if (event & NM_SR_EVENT_RECV_DATA)
    {
      // Header arrived, so get the size and store it in req->size:
      struct nm_data_s data_header;
      nm_data_contiguous_build(&data_header, &req->size, sizeof(nm_len_t));
#ifndef NDEBUG
      int ret =
#endif
      nm_sr_recv_peek(p_session, &req->request, &data_header);
      assert(ret == NM_ESUCCESS);

      // Now we know the size, allocate the buffer:
      req->buffer = malloc(req->size);

      /* Last step: give this buffer to NewMadeleine to receive data
       * We need to use an nm_datav to easily take into account the offset used
       * during the peek. */
      nm_datav_init(&req->datav);
      nm_datav_add_chunk(&req->datav, &req->size, sizeof(nm_len_t));
      nm_datav_add_chunk(&req->datav, req->buffer, req->size);
      nm_data_datav_build(&req->data, &req->datav);
      nm_sr_recv_offset(p_session, &req->request, sizeof(nm_len_t));
      nm_sr_recv_unpack_data(p_session, &req->request, &req->data);
    }
  else if (event & NM_SR_EVENT_FINALIZED)
    {
      nm_datav_destroy(&req->datav);
      nm_cond_signal(&req->cond_received, 1);
    }
}

static void recv_callback_method2(nm_sr_event_t event, const nm_sr_event_info_t* p_info, void* ref)
{
  // Both event can't be triggered at the same time:
  assert(!((event & NM_SR_EVENT_FINALIZED) && (event & NM_SR_EVENT_RECV_DATA)));

  struct recv_req* req = (struct recv_req*) ref;

  if (event & NM_SR_EVENT_RECV_DATA)
    {
      // Header arrived, so get the size and store it in req->size:
      struct nm_data_s data_header;
      nm_data_contiguous_build(&data_header, &req->size, sizeof(nm_len_t));
#ifndef NDEBUG
      int ret =
#endif
      nm_sr_recv_peek(p_session, &req->request, &data_header);
      assert(ret == NM_ESUCCESS);

      // Now we know the size, allocate the buffer:
      req->buffer = malloc(req->size);

      /* Last step: give this buffer to NewMadeleine to receive data
       * We need to use an nm_datav to easily take into account the offset used
       * during the peek. */
      req->v[0] = (struct iovec){ .iov_base = &req->size, .iov_len = sizeof(nm_len_t) };
      req->v[1] = (struct iovec){ .iov_base = (void*) req->buffer, .iov_len = req->size };
      nm_data_iov_build(&req->data, &req->v[0], 2);
      nm_sr_recv_offset(p_session, &req->request, sizeof(nm_len_t));
      nm_sr_recv_unpack_data(p_session, &req->request, &req->data);
    }
  else if (event & NM_SR_EVENT_FINALIZED)
    {
      nm_cond_signal(&req->cond_received, 1);
    }
}

int main(int argc, char**argv)
{
  nm_examples_init(&argc, argv);

  if(is_server)
    {
      /*** Method 1 ***/
      struct recv_req req1;
      nm_cond_init(&req1.cond_received, 0);

      /* we post a recv without giving a buffer because we don't know the required size of this buffer,
        * the buffer will be allocated and provided to nmad when the header of data will be received,
        * in recv_callback() */
      nm_sr_recv_init(p_session, &req1.request);
      nm_sr_request_set_ref(&req1.request, &req1);
      nm_sr_request_monitor(p_session, &req1.request, NM_SR_EVENT_FINALIZED | NM_SR_EVENT_RECV_DATA,
          &recv_callback_method1);
      nm_sr_recv_irecv(p_session, &req1.request, p_gate, 123, NM_TAG_MASK_FULL);

      // We cannot wait with nm_sr_rwait() because the request has a monitor.
      nm_cond_wait(&req1.cond_received, 1, nm_core_get_singleton());
      nm_cond_destroy(&req1.cond_received);

      if (strcmp(req1.buffer, msg))
        {
          NM_FATAL("Received msg: '%s' (should be '%s')\n", req1.buffer, msg);
        }

      free(req1.buffer);

      /*** Method 2 ***/
      struct recv_req req2;
      nm_cond_init(&req2.cond_received, 0);

      /* we post a recv without giving a buffer because we don't know the required size of this buffer,
        * the buffer will be allocated and provided to nmad when the header of data will be received,
        * in recv_callback() */
      nm_sr_recv_init(p_session, &req2.request);
      nm_sr_request_set_ref(&req2.request, &req2);
      nm_sr_request_monitor(p_session, &req2.request, NM_SR_EVENT_FINALIZED | NM_SR_EVENT_RECV_DATA,
          &recv_callback_method2);
      nm_sr_recv_irecv(p_session, &req2.request, p_gate, 123, NM_TAG_MASK_FULL);

      // We cannot wait with nm_sr_rwait() because the request has a monitor.
      nm_cond_wait(&req2.cond_received, 1, nm_core_get_singleton());
      nm_cond_destroy(&req2.cond_received);

      if (strcmp(req2.buffer, msg))
        {
          NM_FATAL("Received msg: '%s' (should be '%s')\n", req2.buffer, msg);
        }

      free(req2.buffer);
    }
  else
    {
      nm_len_t len = strlen(msg) + 1;

      /*** Method 1 ***/
      struct nm_datav_s datav;
      nm_datav_init(&datav);
      nm_datav_add_chunk(&datav, &len, sizeof(nm_len_t));

      struct nm_data_s msg_body;
      nm_data_contiguous_build(&msg_body, (char*) msg, len);
      nm_datav_add_chunk_data(&datav, &msg_body);

      struct nm_data_s data1;
      nm_data_datav_build(&data1, &datav);

      nm_sr_request_t request1;
      nm_sr_send_init(p_session, &request1);
      nm_sr_send_pack_data(p_session, &request1, &data1);
      nm_sr_send_header(p_session, &request1, sizeof(nm_len_t));
      nm_sr_send_isend(p_session, &request1, p_gate, 123);
      nm_sr_swait(p_session, &request1);

      nm_datav_destroy(&datav);

      /*** Method 2 ***/
      struct iovec v[2] = {
        [0] = { .iov_base = &len, .iov_len = sizeof(nm_len_t) },
        [1] = { .iov_base = (void*) msg, .iov_len = len }
      };
      struct nm_data_s data2;
      nm_data_iov_build(&data2, v, 2);

      nm_sr_request_t request2;
      nm_sr_send_init(p_session, &request2);
      nm_sr_send_pack_data(p_session, &request2, &data1);
      nm_sr_send_header(p_session, &request2, sizeof(nm_len_t));
      nm_sr_send_isend(p_session, &request2, p_gate, 123);
      nm_sr_swait(p_session, &request2);
    }

  nm_coll_barrier(p_comm, 0xF2);

  printf("success\n");

  nm_examples_exit();

  return 0;
}

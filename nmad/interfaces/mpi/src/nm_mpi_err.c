/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


#include "nm_mpi_private.h"
#include "nm_mpi_private_syms.h"

#include <Padico/Puk.h>
#include <Padico/Module.h>
PADICO_MODULE_HOOK(MadMPI);

NM_MPI_HANDLE_TYPE(errhandler, struct nm_mpi_errhandler_s, _NM_MPI_ERRHANDLER_OFFSET, 8);
static struct
{
  int refcount;
  struct nm_mpi_handle_errhandler_s errhandlers;
  int next_errorclass;
  int next_errorcode;
  char**errorclass_strings; /**< user-defined error strings; indexed by error class, with MPI_ERR_LASTCODE offset */
  char**errorcode_strings; /**< user-defined error strings; indexed by error code */
} nm_mpi_err = { .refcount = 0, .errhandlers = NM_MPI_HANDLE_NULL };

/* ********************************************************* */
/* Aliases */

NM_MPI_ALIAS(MPI_Error_string,            mpi_error_string);
NM_MPI_ALIAS(MPI_Error_class,             mpi_error_class);
NM_MPI_ALIAS(MPI_Errhandler_create,       mpi_errhandler_create);
NM_MPI_ALIAS(MPI_Errhandler_set,          mpi_errhandler_set);
NM_MPI_ALIAS(MPI_Errhandler_get,          mpi_errhandler_get);
NM_MPI_ALIAS(MPI_Errhandler_free,         mpi_errhandler_free);
NM_MPI_ALIAS(MPI_Comm_create_errhandler,  mpi_comm_create_errhandler);
NM_MPI_ALIAS(MPI_Comm_set_errhandler,     mpi_comm_set_errhandler);
NM_MPI_ALIAS(MPI_Comm_get_errhandler,     mpi_comm_get_errhandler);
NM_MPI_ALIAS(MPI_Comm_call_errhandler,    mpi_comm_call_errhandler);
NM_MPI_ALIAS(MPI_Add_error_class,         mpi_add_error_class);
NM_MPI_ALIAS(MPI_Add_error_code,          mpi_add_error_code);
NM_MPI_ALIAS(MPI_Add_error_string,        mpi_add_error_string);

/* ********************************************************* */

static const char*nm_mpi_error_string(int errorcode);
static const char*nm_mpi_error_label(int errorcode);

__PUK_SYM_INTERNAL
void nm_mpi_err_lazy_init(void)
{
  const int count = nm_atomic_inc(&nm_mpi_err.refcount);
  if(count == 0)
    {
      nm_mpi_err.next_errorclass = MPI_ERR_LASTCODE + 1;
      nm_mpi_err.next_errorcode = 1;
      nm_mpi_err.errorclass_strings = NULL;
      nm_mpi_err.errorcode_strings = NULL;
      nm_mpi_handle_errhandler_init(&nm_mpi_err.errhandlers);
      struct nm_mpi_errhandler_s*p_errhandler_return =
        nm_mpi_handle_errhandler_store(&nm_mpi_err.errhandlers, MPI_ERRORS_RETURN);
      p_errhandler_return->kind = NM_MPI_ERRHANDLER_NONE;
      struct nm_mpi_errhandler_s*p_errhandler_fatal =
        nm_mpi_handle_errhandler_store(&nm_mpi_err.errhandlers, MPI_ERRORS_ARE_FATAL);
      p_errhandler_fatal->kind = NM_MPI_ERRHANDLER_NONE;
    }
}

__PUK_SYM_INTERNAL
void nm_mpi_err_lazy_exit(void)
{
  const int count = nm_atomic_dec(&nm_mpi_err.refcount);
  if(count == 0)
    {
      nm_mpi_handle_errhandler_finalize(&nm_mpi_err.errhandlers, NULL);
      int i;
      for(i = 1; i < nm_mpi_err.next_errorclass - MPI_ERR_LASTCODE; i++)
        {
          padico_free(nm_mpi_err.errorclass_strings[i]);
        }
      if(nm_mpi_err.errorclass_strings != NULL)
        {
          padico_free(nm_mpi_err.errorclass_strings);
          nm_mpi_err.errorclass_strings = NULL;
        }
      for(i = 1; i < nm_mpi_err.next_errorcode; i++)
        {
          padico_free(nm_mpi_err.errorcode_strings[i]);
        }
      if(nm_mpi_err.errorcode_strings != NULL)
        {
          padico_free(nm_mpi_err.errorcode_strings);
          nm_mpi_err.errorcode_strings = NULL;
        }
    }
}

__PUK_SYM_INTERNAL
struct nm_mpi_errhandler_s*nm_mpi_errhandler_get(int errhandler, enum nm_mpi_errhandler_kind_e kind)
{
  if(nm_mpi_err.refcount <= 0)
    return NULL;
  struct nm_mpi_errhandler_s*p_errhandler = nm_mpi_handle_errhandler_get(&nm_mpi_err.errhandlers, errhandler);
  if((p_errhandler->kind != kind) &&
     (kind != NM_MPI_ERRHANDLER_NONE) &&
     (p_errhandler->id >= _NM_MPI_ERRHANDLER_OFFSET))
    {
      NM_MPI_WARNING("wrong type for errhandler = %d; expected = %d; found = %d\n", errhandler, kind, p_errhandler->kind);
      return NULL;
    }
  return p_errhandler;
}

__PUK_SYM_INTERNAL
struct nm_mpi_errhandler_s*nm_mpi_errhandler_alloc(enum nm_mpi_errhandler_kind_e kind, MPI_Handler_function*func)
{
  nm_mpi_err_lazy_init();
  struct nm_mpi_errhandler_s*p_errhandler = nm_mpi_handle_errhandler_alloc(&nm_mpi_err.errhandlers);
  p_errhandler->kind = kind;
  p_errhandler->function.legacy = func;
  return p_errhandler;
}

__PUK_SYM_INTERNAL
void nm_mpi_errhandler_exec_internal(struct nm_mpi_errhandler_s*p_errhandler,
                                     enum nm_mpi_errhandler_kind_e kind, int handle, int err,
                                     const char*func, const char*file, const int line)
{
  assert((p_errhandler->kind == kind) || (p_errhandler->id < _NM_MPI_ERRHANDLER_OFFSET));
  const int id = p_errhandler->id;
  const char*object = NULL;
  switch(kind)
    {
    case NM_MPI_ERRHANDLER_COMM:
      object = "communicator";
      break;
    case NM_MPI_ERRHANDLER_WIN:
      object = "window";
      break;
    case NM_MPI_ERRHANDLER_FILE:
      object = "file";
      break;
    case NM_MPI_ERRHANDLER_SESSION:
      object = "session";
      break;
    case NM_MPI_ERRHANDLER_NONE:
      NM_MPI_FATAL_ERROR("cannot invoke errhandler with kind = NM_MPI_ERRHANDLER_NONE\n");
      break;
    default:
      NM_MPI_FATAL_ERROR("unexpected kind %d for errhandler %d\n", kind, id);
      break;
    }
  const char*err_string = nm_mpi_error_label(err);
  switch(id)
    {
    case MPI_ERRHANDLER_NULL:
      {
        NM_MPI_FATAL_ERROR("trying to invocate MPI_ERRHANDLER_NULL- errorcode = %d (%s) in %s() %s:%d\n",
                           err, err_string, func, file, line);
      }
      break;
    case MPI_ERRORS_ARE_FATAL:
      {
        NM_MPI_FATAL_ERROR("MPI_ERRORS_ARE_FATAL: error %d (%s) on %s %d (%s) in %s() %s:%d\n",
                           err, err_string, object, handle, nm_mpi_error_string(err), func, file, line);
      }
      break;
    case MPI_ERRORS_RETURN:
      {
        NM_MPI_WARNING("MPI_ERRORS_RETURN: error %d (%s) on %s %d (%s) in %s() %s:%d\n",
                       err, err_string, object, handle, nm_mpi_error_string(err), func, file, line);
      }
      break;
    default:
      {
        switch(kind)
          {
          case NM_MPI_ERRHANDLER_COMM:
            (*p_errhandler->function.comm_func)(&handle, &err);
            break;
          case NM_MPI_ERRHANDLER_WIN:
            (*p_errhandler->function.win_func)(&handle, &err);
            break;
          case NM_MPI_ERRHANDLER_FILE:
            (*p_errhandler->function.file_func)(&handle, &err);
            break;
          case NM_MPI_ERRHANDLER_SESSION:
            (*p_errhandler->function.session_func)(&handle, &err);
            break;
          case NM_MPI_ERRHANDLER_NONE:
            NM_MPI_FATAL_ERROR("cannot invoke errhandler with kind = NM_MPI_ERRHANDLER_NONE\n");
            break;
          }
      }
      break;
    }
}

/** get a plain text label for the given error code*/
static const char*nm_mpi_error_label(int errorcode)
{
#define NM_MPI_ERROR_LABEL(ERR) case ERR: return #ERR; break;
  const int errorclass = errorcode & 0xFFFF;
  switch(errorclass)
    {
      NM_MPI_ERROR_LABEL(MPI_SUCCESS);
      NM_MPI_ERROR_LABEL(MPI_ERR_BUFFER);
      NM_MPI_ERROR_LABEL(MPI_ERR_COUNT);
      NM_MPI_ERROR_LABEL(MPI_ERR_TYPE);
      NM_MPI_ERROR_LABEL(MPI_ERR_TAG);
      NM_MPI_ERROR_LABEL(MPI_ERR_COMM);
      NM_MPI_ERROR_LABEL(MPI_ERR_RANK);
      NM_MPI_ERROR_LABEL(MPI_ERR_ROOT);
      NM_MPI_ERROR_LABEL(MPI_ERR_GROUP);
      NM_MPI_ERROR_LABEL(MPI_ERR_OP);
      NM_MPI_ERROR_LABEL(MPI_ERR_TOPOLOGY);
      NM_MPI_ERROR_LABEL(MPI_ERR_DIMS);
      NM_MPI_ERROR_LABEL(MPI_ERR_ARG);
      NM_MPI_ERROR_LABEL(MPI_ERR_UNKNOWN);
      NM_MPI_ERROR_LABEL(MPI_ERR_TRUNCATE);
      NM_MPI_ERROR_LABEL(MPI_ERR_OTHER);
      NM_MPI_ERROR_LABEL(MPI_ERR_INTERN);
      NM_MPI_ERROR_LABEL(MPI_ERR_IN_STATUS);
      NM_MPI_ERROR_LABEL(MPI_ERR_PENDING);
      NM_MPI_ERROR_LABEL(MPI_ERR_INFO);
      NM_MPI_ERROR_LABEL(MPI_ERR_NO_MEM);
      NM_MPI_ERROR_LABEL(MPI_ERR_REQUEST);
      NM_MPI_ERROR_LABEL(MPI_ERR_KEYVAL);
      NM_MPI_ERROR_LABEL(MPI_ERR_INFO_KEY);
      NM_MPI_ERROR_LABEL(MPI_ERR_INFO_VALUE);
      NM_MPI_ERROR_LABEL(MPI_ERR_INFO_NOKEY);
      NM_MPI_ERROR_LABEL(MPI_ERR_VALUE_TOO_LARGE);
      NM_MPI_ERROR_LABEL(MPI_ERR_NAME);
      NM_MPI_ERROR_LABEL(MPI_ERR_PORT);
      NM_MPI_ERROR_LABEL(MPI_ERR_CONVERSION);
      NM_MPI_ERROR_LABEL(MPI_ERR_PROC_ABORTED);
      NM_MPI_ERROR_LABEL(MPI_ERR_SERVICE);
      NM_MPI_ERROR_LABEL(MPI_ERR_SPAWN);
      NM_MPI_ERROR_LABEL(MPI_ERR_FILE);
      NM_MPI_ERROR_LABEL(MPI_ERR_IO);
      NM_MPI_ERROR_LABEL(MPI_ERR_AMODE);
      NM_MPI_ERROR_LABEL(MPI_ERR_UNSUPPORTED_OPERATION);
      NM_MPI_ERROR_LABEL(MPI_ERR_UNSUPPORTED_DATAREP);
      NM_MPI_ERROR_LABEL(MPI_ERR_READ_ONLY);
      NM_MPI_ERROR_LABEL(MPI_ERR_ACCESS);
      NM_MPI_ERROR_LABEL(MPI_ERR_DUP_DATAREP);
      NM_MPI_ERROR_LABEL(MPI_ERR_NO_SUCH_FILE);
      NM_MPI_ERROR_LABEL(MPI_ERR_NOT_SAME);
      NM_MPI_ERROR_LABEL(MPI_ERR_BAD_FILE);
      NM_MPI_ERROR_LABEL(MPI_ERR_FILE_IN_USE);
      NM_MPI_ERROR_LABEL(MPI_ERR_NO_SPACE);
      NM_MPI_ERROR_LABEL(MPI_ERR_QUOTA);
      NM_MPI_ERROR_LABEL(MPI_ERR_FILE_EXISTS);
      NM_MPI_ERROR_LABEL(MPI_ERR_WIN);
      NM_MPI_ERROR_LABEL(MPI_ERR_BASE);
      NM_MPI_ERROR_LABEL(MPI_ERR_SIZE);
      NM_MPI_ERROR_LABEL(MPI_ERR_DISP);
      NM_MPI_ERROR_LABEL(MPI_ERR_LOCKTYPE);
      NM_MPI_ERROR_LABEL(MPI_ERR_ASSERT);
      NM_MPI_ERROR_LABEL(MPI_ERR_RMA_CONFLICT);
      NM_MPI_ERROR_LABEL(MPI_ERR_RMA_SYNC);
      NM_MPI_ERROR_LABEL(MPI_ERR_RMA_RANGE);
      NM_MPI_ERROR_LABEL(MPI_ERR_RMA_ATTACH);
      NM_MPI_ERROR_LABEL(MPI_ERR_RMA_SHARED);
      NM_MPI_ERROR_LABEL(MPI_ERR_RMA_FLAVOR);
      NM_MPI_ERROR_LABEL(MPI_ERR_SESSION);
      NM_MPI_ERROR_LABEL(MPI_T_ERR_INVALID);
      NM_MPI_ERROR_LABEL(MPI_T_ERR_MEMORY);
      NM_MPI_ERROR_LABEL(MPI_T_ERR_NOT_INITIALIZED);
      NM_MPI_ERROR_LABEL(MPI_T_ERR_CANNOT_INIT);
      NM_MPI_ERROR_LABEL(MPI_T_ERR_NOT_ACCESSIBLE);
      NM_MPI_ERROR_LABEL(MPI_T_ERR_INVALID_INDEX);
      NM_MPI_ERROR_LABEL(MPI_T_ERR_INVALID_NAME);
      NM_MPI_ERROR_LABEL(MPI_T_ERR_INVALID_HANDLE);
      NM_MPI_ERROR_LABEL(MPI_T_ERR_OUT_OF_HANDLES);
      NM_MPI_ERROR_LABEL(MPI_T_ERR_OUT_OF_SESSIONS);
      NM_MPI_ERROR_LABEL(MPI_T_ERR_INVALID_SESSION);
      NM_MPI_ERROR_LABEL(MPI_T_ERR_CVAR_SET_NOT_NOW);
      NM_MPI_ERROR_LABEL(MPI_T_ERR_CVAR_SET_NEVER);
      NM_MPI_ERROR_LABEL(MPI_T_ERR_PVAR_NO_STARTSTOP);
      NM_MPI_ERROR_LABEL(MPI_T_ERR_PVAR_NO_WRITE);
      NM_MPI_ERROR_LABEL(MPI_T_ERR_PVAR_NO_ATOMIC);
      NM_MPI_ERROR_LABEL(MPI_T_ERR_NOT_SUPPORTED);
      NM_MPI_ERROR_LABEL(MPI_T_ERR_INVALID_ITEM);
      NM_MPI_ERROR_LABEL(MPI_ERR_LASTCODE)
    default:
      {
        if((errorclass > MPI_ERR_LASTCODE) && (errorclass < nm_mpi_err.next_errorclass))
          return "user defined";
        else
          return "unknown";
        break;
      }
    }
}


static const char*nm_mpi_error_string(int errorcode)
{
  switch(errorcode)
    {
    case MPI_SUCCESS:
      return "No error"; break;
    case MPI_ERR_ACCESS:
      return "Permission denied"; break;
    case MPI_ERR_AMODE:
      return "Error related to the amode passed to MPI_FILE_OPEN"; break;
    case MPI_ERR_ARG:
      return "Invalid argument of some other kind"; break;
    case MPI_ERR_ASSERT:
      return "Invalid assertion argument"; break;
    case MPI_ERR_BAD_FILE:
      return "Invalid file name (e.g., path name too long)"; break;
    case MPI_ERR_BASE:
      return "Invalid base passed to MPI_FREE_MEM"; break;
    case MPI_ERR_BUFFER:
      return "Invalid buffer pointer argument"; break;
    case MPI_ERR_COMM:
      return "Invalid communicator argument"; break;
    case MPI_ERR_CONVERSION:
      return "An error occurred in a user supplied data conversion function"; break;
    case MPI_ERR_COUNT:
      return "Invalid count argument"; break;
    case MPI_ERR_DIMS:
      return "Invalid dimension argument"; break;
    case MPI_ERR_DISP:
      return "Invalid displacement argument"; break;
    case MPI_ERR_DUP_DATAREP:
      return "Conversion functions could not be registered because a data representation identifier that was already defined was passed to MPI_REGISTER_DATAREP"; break;
    case MPI_ERR_FILE:
      return "Invalid file handle argument"; break;
    case MPI_ERR_FILE_EXISTS:
      return "File exists"; break;
    case MPI_ERR_FILE_IN_USE:
      return "File operation could not be completed, as the file is currently open by some process"; break;
    case MPI_ERR_GROUP:
      return "Invalid group argument"; break;
    case MPI_ERR_INFO:
      return "Invalid info argument"; break;
    case MPI_ERR_INFO_KEY:
      return "Key longer than MPI_MAX_INFO_KEY"; break;
    case MPI_ERR_INFO_NOKEY:
      return "Invalid key passed to MPI_INFO_DELETE"; break;
    case MPI_ERR_INFO_VALUE:
      return "Value longer than MPI_MAX_INFO_VAL"; break;
    case MPI_ERR_IN_STATUS:
      return "Error code is in status"; break;
    case MPI_ERR_INTERN:
      return "Internal MPI (implementation) error"; break;
    case MPI_ERR_IO:
      return "Other I/O error"; break;
    case MPI_ERR_KEYVAL:
      return "Invalid keyval argument"; break;
    case MPI_ERR_LOCKTYPE:
      return "Invalid locktype argument"; break;
    case MPI_ERR_NAME:
      return "Invalid service name passed to MPI_LOOKUP_NAME"; break;
    case MPI_ERR_NO_MEM:
      return "MPI_ALLOC_MEM failed because memory is exhausted"; break;
    case MPI_ERR_NO_SPACE:
      return "Not enough space"; break;
    case MPI_ERR_NO_SUCH_FILE:
      return "File does not exist"; break;
    case MPI_ERR_NOT_SAME:
      return "Collective argument not identical on all processes, or collective routines called in a different order by different processes"; break;
    case MPI_ERR_OP:
      return "Invalid operation argument"; break;
    case MPI_ERR_OTHER:
      return "Known error not in this list"; break;
    case MPI_ERR_PENDING:
      return "Pending request"; break;
    case MPI_ERR_PORT:
      return "Invalid port name passed to MPI_COMM_CONNECT"; break;
    case MPI_ERR_PROC_ABORTED:
      return "Operation failed because a peer process has aborted"; break;
    case MPI_ERR_QUOTA:
      return "Quota exceeded"; break;
    case MPI_ERR_RANK:
      return "Invalid rank argument"; break;
    case MPI_ERR_READ_ONLY:
      return "Read-only file or file system"; break;
    case MPI_ERR_REQUEST:
      return "Invalid request argument"; break;
    case MPI_ERR_RMA_ATTACH:
      return "Memory cannot be attached (e.g., because of resource exhaustion)"; break;
    case MPI_ERR_RMA_CONFLICT:
      return "Conflicting accesses to window"; break;
    case MPI_ERR_RMA_FLAVOR:
      return "Passed window has the wrong flavor for the called function"; break;
    case MPI_ERR_RMA_RANGE:
      return "Target memory is not part of the window (in the case of a window created with MPI_WIN_CREATE_DYNAMIC, target memory is not attached)"; break;
    case MPI_ERR_RMA_SHARED:
      return "Memory cannot be shared (e.g., some process in the group of the specified communicator cannot expose shared memory)"; break;
    case MPI_ERR_RMA_SYNC:
      return "Wrong synchronization of RMA calls"; break;
    case MPI_ERR_ROOT:
      return "Invalid root argument"; break;
    case MPI_ERR_SERVICE:
      return "Invalid service name passed to MPI_UNPUBLISH_NAME"; break;
    case MPI_ERR_SESSION:
      return "Invalid session argument"; break;
    case MPI_ERR_SIZE:
      return "Invalid size argument"; break;
    case MPI_ERR_SPAWN:
      return "Error in spawning processes"; break;
    case MPI_ERR_TAG:
      return "Invalid tag argument"; break;
    case MPI_ERR_TOPOLOGY:
      return "Invalid topology argument"; break;
    case MPI_ERR_TRUNCATE:
      return "Message truncated on receive"; break;
    case MPI_ERR_TYPE:
      return "Invalid datatype argument"; break;
    case MPI_ERR_UNKNOWN:
      return "Unknown error"; break;
    case MPI_ERR_UNSUPPORTED_DATAREP:
      return "Unsupported datarep passed to MPI_FILE_SET_VIEW"; break;
    case MPI_ERR_UNSUPPORTED_OPERATION:
      return "Unsupported operation, such as seeking on a file which supports sequential access only"; break;
    case MPI_ERR_VALUE_TOO_LARGE:
      return "Value is too large to store"; break;
    case MPI_ERR_WIN:
      return "Invalid window argument"; break;
    case MPI_T_ERR_INVALID:
      return "Invalid or bad parameter value(s)"; break;
    case MPI_T_ERR_MEMORY:
      return "Out of memory"; break;
    case MPI_T_ERR_NOT_INITIALIZED:
      return "Interface not initialized"; break;
    case MPI_T_ERR_CANNOT_INIT:
      return "Interface not in the state to be initialized"; break;
    case MPI_T_ERR_NOT_ACCESSIBLE:
      return "Requested functionality not accessible"; break;
    case MPI_T_ERR_INVALID_INDEX:
      return "The enumeration/variable/category index is invalid"; break;
    case MPI_T_ERR_INVALID_NAME:
      return "The variable or category name is invalid"; break;
    case MPI_T_ERR_INVALID_HANDLE:
      return "The handle is invalid"; break;
    case MPI_T_ERR_OUT_OF_HANDLES:
      return "No more handles available"; break;
    case MPI_T_ERR_OUT_OF_SESSIONS:
      return "No more sessions available"; break;
    case MPI_T_ERR_INVALID_SESSION:
      return "Session argument is not a valid session"; break;
    case MPI_T_ERR_CVAR_SET_NOT_NOW:
      return "Variable cannot be set at this moment"; break;
    case MPI_T_ERR_CVAR_SET_NEVER:
      return "Variable cannot be set until end of execution"; break;
    case MPI_T_ERR_PVAR_NO_STARTSTOP:
      return "Variable cannot be started or stopped"; break;
    case MPI_T_ERR_PVAR_NO_WRITE:
      return "Variable cannot be written or reset"; break;
    case MPI_T_ERR_PVAR_NO_ATOMIC:
      return "Variable cannot be read and written atomically"; break;
    case MPI_T_ERR_NOT_SUPPORTED:
      return "Requested functionality not supported"; break;
    case MPI_T_ERR_INVALID_ITEM:
      return "deprecated; used MPI_T_ERR_INVALID_INDEX instead"; break;
    case MPI_ERR_LASTCODE:
      return "Last error code"; break;
    default:
      {
        const int c1 = errorcode & 0xFFFF;
        if(errorcode == c1)
          {
            /* this is actually a class */
            if((c1 > MPI_ERR_LASTCODE) && (c1 < nm_mpi_err.next_errorclass))
              return nm_mpi_err.errorclass_strings[c1 - MPI_ERR_LASTCODE];
          }
        else
          {
            /* error code */
            const int c2 = (errorcode - c1) >> 16;
            if((c2 > 0) && (c2 < nm_mpi_err.next_errorcode))
              return nm_mpi_err.errorcode_strings[c2];
          }
        return "unknown error code";
        break;
      }
    }
}

__PUK_SYM_INTERNAL
int nm_mpi_err_lastused(void)
{
  return nm_mpi_err.next_errorclass - 1;
}

/* ********************************************************* */
/* ** MPI API */

int mpi_error_class(int errorcode, int*errorclass)
{
  if(errorcode < MPI_ERR_LASTCODE)
    {
      *errorclass = errorcode;
    }
  else
    {
      const int c = errorcode & 0xFFFF;
      if((c > MPI_ERR_LASTCODE) && (c < nm_mpi_err.next_errorclass))
        {
          *errorclass = c;
          return MPI_SUCCESS;
        }
      else
        {
          return NM_MPI_ERROR_WORLD(MPI_ERR_ARG);
        }
    }
  return MPI_SUCCESS;
}

int mpi_error_string(int errorcode, char*string, int*resultlen)
{
  const char*error_string = nm_mpi_error_string(errorcode);
  strncpy(string, error_string, MPI_MAX_ERROR_STRING);
  *resultlen = strlen(error_string);
  return MPI_SUCCESS;
}

int mpi_add_error_class(int*errorclass)
{
  int c = nm_atomic_inc(&nm_mpi_err.next_errorclass);
  assert(c > MPI_ERR_LASTCODE);
  if(c > 0xFFFF)
    return NM_MPI_ERROR_WORLD(MPI_ERR_NO_SPACE);
  nm_mpi_err.errorclass_strings = padico_realloc(nm_mpi_err.errorclass_strings, (c - MPI_ERR_LASTCODE + 1) * sizeof(char*));
  nm_mpi_err.errorclass_strings[c - MPI_ERR_LASTCODE] = padico_strdup("");
  *errorclass = c;
  return MPI_SUCCESS;
}

int mpi_add_error_code(int errorclass, int*errorcode)
{
  int c = nm_atomic_inc(&nm_mpi_err.next_errorcode);
  assert(c >= 0);
  if(c > 0xFFFF)
    return NM_MPI_ERROR_WORLD(MPI_ERR_NO_SPACE);
  nm_mpi_err.errorcode_strings = padico_realloc(nm_mpi_err.errorcode_strings, (c + 1) * sizeof(char*));
  nm_mpi_err.errorcode_strings[c] = padico_strdup("");
  *errorcode = errorclass + (c << 16);
  return MPI_SUCCESS;
}

int mpi_add_error_string(int errorcode, const char*string)
{
  const int c1 = errorcode & 0xFFFF;
  if(errorcode == c1)
    {
      /* this is actually a class */
      if((c1 <= MPI_ERR_LASTCODE) || (c1 >= nm_mpi_err.next_errorclass))
        return NM_MPI_ERROR_WORLD(MPI_ERR_ARG);
      padico_free(nm_mpi_err.errorclass_strings[c1 - MPI_ERR_LASTCODE]);
      nm_mpi_err.errorclass_strings[c1 - MPI_ERR_LASTCODE] = padico_strdup(string);
    }
  else
    {
      /* error code */
      const int c2 = (errorcode - c1) >> 16;
      if((c2 <= 0) || (c2 >= nm_mpi_err.next_errorcode))
        return NM_MPI_ERROR_WORLD(MPI_ERR_ARG);
      padico_free(nm_mpi_err.errorcode_strings[c2]);
      nm_mpi_err.errorcode_strings[c2] = padico_strdup(string);
    }
  return MPI_SUCCESS;
}


int mpi_comm_create_errhandler(MPI_Comm_errhandler_function*function, MPI_Errhandler*errhandler)
{
  struct nm_mpi_errhandler_s*p_errhandler = nm_mpi_errhandler_alloc(NM_MPI_ERRHANDLER_COMM, function);
  *errhandler = p_errhandler->id;
  return MPI_SUCCESS;
}

int mpi_comm_set_errhandler(MPI_Comm comm, MPI_Errhandler errhandler)
{
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  if(p_comm == NULL)
    return NM_MPI_ERROR_WORLD(MPI_ERR_COMM);
  struct nm_mpi_errhandler_s*p_errhandler = nm_mpi_errhandler_get(errhandler, NM_MPI_ERRHANDLER_COMM);
  if(p_errhandler == NULL)
    return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_ARG);
  p_comm->p_errhandler = p_errhandler;
  return MPI_SUCCESS;
}

int mpi_comm_get_errhandler(MPI_Comm comm, MPI_Errhandler*errhandler)
{
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  if(p_comm == NULL)
    return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_COMM);
  *errhandler = p_comm->p_errhandler->id;
  return MPI_SUCCESS;
}

int mpi_comm_call_errhandler(MPI_Comm comm, int errorcode)
{
  nm_mpi_communicator_t*p_comm = nm_mpi_communicator_get(comm);
  if(p_comm == NULL)
    return NM_MPI_ERROR_WORLD(MPI_ERR_COMM);
  nm_mpi_errhandler_exec(p_comm->p_errhandler, NM_MPI_ERRHANDLER_COMM, comm, errorcode);
  return MPI_SUCCESS;
}

int mpi_errhandler_free(MPI_Errhandler*errhandler)
{
  struct nm_mpi_errhandler_s*p_errhandler = nm_mpi_errhandler_get(*errhandler, NM_MPI_ERRHANDLER_NONE);
  if(p_errhandler == NULL)
    return NM_MPI_ERROR_WORLD(MPI_ERR_ARG);
  nm_mpi_handle_errhandler_free(&nm_mpi_err.errhandlers, p_errhandler);
  nm_mpi_err_lazy_exit();
  *errhandler = MPI_ERRHANDLER_NULL;
  return MPI_SUCCESS;
}


/* ** legacy MPI-1 functions */

int mpi_errhandler_create(MPI_Handler_function*function, MPI_Errhandler*errhandler)
{
  return mpi_comm_create_errhandler(function, errhandler);
}

int mpi_errhandler_set(MPI_Comm comm, MPI_Errhandler errhandler)
{
  return mpi_comm_set_errhandler(comm, errhandler);
}

int mpi_errhandler_get(MPI_Comm comm, MPI_Errhandler*errhandler)
{
  return mpi_comm_get_errhandler(comm, errhandler);
}

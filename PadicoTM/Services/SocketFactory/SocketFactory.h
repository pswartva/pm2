/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 */

#ifndef SOCKETFACTORY_H
#define SOCKETFACTORY_H

#include <Padico/Topology.h>

typedef void (*padico_socketfactory_acceptor_t)(void*key, int fd, const struct sockaddr*addr, socklen_t addrlen);
typedef void (*padico_socketfactory_connector_t)(void*key, int fd);

struct padico_socketfactory_s
{
  /** create the socket attached to the SocketFactory instance
   * @return 0 if success, -errno in case of error.
   */
  int (*create) (void*_status, int family, int type, int protocol);
  /** binds the socket to the given address (expected: AF_INET family)
   * or performs autobind (AF_PADICO family + 'AUTO' magic number).
   * Socket factories are not required to support all address families.
   * @return 0 if success, -errno in case of error.
   * @note 'addr' and 'addrlen' are filled-in with the address.
   */
  int (*bind)   (void*_status, struct sockaddr*addr, socklen_t*addrlen);
  /** listens incoming connections on the socket
   * @return 0 if success, -errno in case of error.
   * @param addr address of the expected client (when used as secondary component).
   * @note when addr is non-NULL (==>secondary), 'backlog' is ignored.
   * @param notifier notification function called upon connection completion
   * @param key key given to the notifier
   */
  int (*listen) (void*_status, int backlog, const struct sockaddr*addr, socklen_t addrlen,
                 padico_socketfactory_acceptor_t notifier, void*key);
  /** connects the socket to the given address
   * @return 0 if success, -errno in case of error.
   * @param addr address of the server.
   * @param notifier notification function called upon connection completion
   * @param key key given to the notifier
   */
  int (*connect)(void*_status, const struct sockaddr*addr, socklen_t addrlen,
                 padico_socketfactory_connector_t notifier, void*key);
  /** closes the given socket
   */
  int (*close)(void*_status);
  /** sets socket options (if appropriate for component).
   */
  int (*setsockopt)(void*_status, int level, int opt, const void*val, int len);
  /** socket capabilities (@see enum padico_vsock_capabilities_e)
   */
  unsigned caps;
};

PUK_IFACE_TYPE(SocketFactory, struct padico_socketfactory_s);

#endif /* SOCKETFACTORY_H */

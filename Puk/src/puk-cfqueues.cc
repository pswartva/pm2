/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * Stub to use ConcurrencyFreaks C++ queues from C code.
 */


#include "Puk-config.h"

#ifdef PUK_ENABLE_CFQUEUES

#include "Puk.h"


#define PUK_CFQUEUE_FUNCS(CFQUEUE)                                      \
  PUK_CFQUEUE_DECLS(CFQUEUE);                                           \
  void puk_cfqueue_ ## CFQUEUE ## _init(void**_q)                       \
  {                                                                     \
    CFQUEUE<int>**q = (CFQUEUE<int>**)_q;                               \
    *q = new CFQUEUE<int>();                                            \
  }                                                                     \
  void puk_cfqueue_ ## CFQUEUE ## _enqueue(void*_q, int tid, void*v)    \
  {                                                                     \
    CFQUEUE<int>*q = (CFQUEUE<int>*)_q;                                 \
    int*_v = (int*)v;                                                   \
    q->enqueue(_v, tid);                                                \
  }                                                                     \
  void*puk_cfqueue_ ## CFQUEUE ## _dequeue(void*_q, int tid)            \
  {                                                                     \
    CFQUEUE<int>*q = (CFQUEUE<int>*)_q;                                 \
    int*_v = q->dequeue(tid);                                           \
    void*v = (void*)_v;                                                 \
    return v;                                                           \
  }                                                                     \
  void puk_cfqueue_ ## CFQUEUE ## _free(void*_q, int tid)               \
  {                                                                     \
    CFQUEUE<int>*q = (CFQUEUE<int>*)_q;                                 \
    delete q;                                                           \
  }

#include "../externals/ConcurrencyFreaks/KoganPetrankQueueCHP.hpp"

extern "C"
{
  PUK_CFQUEUE_FUNCS(KoganPetrankQueueCHP);
}

#include "../externals/ConcurrencyFreaks/CRTurnQueue.hpp"

extern "C"
{
  PUK_CFQUEUE_FUNCS(CRTurnQueue);
}

#if defined(__x86_64__)
/* LCRQ uses CAS2 implemented as x86_64 asm */
#include "../externals/ConcurrencyFreaks/LCRQueue.hpp"

extern "C"
{
  PUK_CFQUEUE_FUNCS(LCRQueue);
}
#else /* __x86_64__ */
extern "C"
{
  void puk_cfqueue_LCRQueue_init(void**_q)
  {
    padico_fatal("LCRQueue not available on non-x86_64 architecture.\n");
  }
  void puk_cfqueue_LCRQueue_enqueue(void*_q, int tid, void*v)
  {
    padico_fatal("LCRQueue not available on non-x86_64 architecture.\n");
  }
  void*puk_cfqueue_LCRQueue_dequeue(void*_q, int tid)
  {
    padico_fatal("LCRQueue not available on non-x86_64 architecture.\n");
    return NULL;
  }
  void puk_cfqueue_LCRQueue_free(void*_q, int tid)
  {
    padico_fatal("LCRQueue not available on non-x86_64 architecture.\n");
  }
}
#endif /* __x86_64__ */

#include "../externals/ConcurrencyFreaks/MichaelScottQueue.hpp"

extern "C"
{
  PUK_CFQUEUE_FUNCS(MichaelScottQueue);
}

#include "../externals/ConcurrencyFreaks/CRDoubleLinkQueue.hpp"

extern "C"
{
  PUK_CFQUEUE_FUNCS(CRDoubleLinkQueue);
}

#include "../externals/ConcurrencyFreaks/BitNextQueue.hpp"

extern "C"
{
  PUK_CFQUEUE_FUNCS(BitNextQueue);
}

#include "../externals/ConcurrencyFreaks/LinearArrayQueue.hpp"

extern "C"
{
  PUK_CFQUEUE_FUNCS(LinearArrayQueue);
}


#endif /* PUK_ENABLE_CFQUEUES */

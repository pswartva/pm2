/* bench-vect.c
 * benchmark for vector-based single process queues
 */

#include <stdio.h>
#include <unistd.h>

#include "bench-common.h"
#include <Padico/Module.h>
#include <Padico/Puk.h>

int bench_vect_init(void);
int bench_vect_run(int argc, char**argv);
void bench_vect_finalize(void);

PADICO_MODULE_DECLARE(bench_vect, bench_vect_init, bench_vect_run, bench_vect_finalize);

/* ********************************************************* */

PUK_QUEUE_TYPE(bench_vect_buffer, void*);

static bench_vect_buffer_queue_t v;

int bench_vect_init(void)
{
  v = bench_vect_buffer_queue_new(1024);
  return 0;
}

void bench_vect_finalize(void)
{
  bench_vect_buffer_queue_delete(v);
}

static void bench_vect_server(char*buffer, size_t lenbuf)
{
  bench_vect_buffer_queue_append(v, buffer);
  bench_vect_buffer_queue_append(v, buffer);
  buffer = bench_vect_buffer_queue_retrieve(v);
  buffer = bench_vect_buffer_queue_retrieve(v);
}

static void bench_vect_client(char*buffer, size_t lenbuf)
{
  /* client does nothing */
}

static struct bench_desc_s bench_vect =
  {
    "vect",
    &bench_vect_server,
    &bench_vect_client
  };

int bench_vect_run(int argc, char**argv)
{
  the_bench(&bench_vect);
  return 0;
}

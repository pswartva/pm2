/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


#include "nm_mpi_private.h"
#include "nm_mpi_private_syms.h"

#include <Padico/Puk.h>
#include <Padico/Module.h>
PADICO_MODULE_HOOK(MadMPI);

NM_MPI_HANDLE_TYPE(info, struct nm_mpi_info_s, _NM_MPI_INFO_OFFSET, 8);
static struct
{
  int refcount;
  struct nm_mpi_handle_info_s infos;
} nm_mpi_info = { .refcount = 0, .infos = NM_MPI_HANDLE_NULL };

/* ********************************************************* */
/* Aliases */

NM_MPI_ALIAS(MPI_Info_create, mpi_info_create);
NM_MPI_ALIAS(MPI_Info_create_env, mpi_info_create_env);
NM_MPI_ALIAS(MPI_Info_free, mpi_info_free);
NM_MPI_ALIAS(MPI_Info_set, mpi_info_set);
NM_MPI_ALIAS(MPI_Info_get, mpi_info_get);
NM_MPI_ALIAS(MPI_Info_get_string, mpi_info_get_string);
NM_MPI_ALIAS(MPI_Info_delete, mpi_info_delete);
NM_MPI_ALIAS(MPI_Info_get_nkeys, mpi_info_get_nkeys);
NM_MPI_ALIAS(MPI_Info_get_nthkey, mpi_info_get_nthkey);
NM_MPI_ALIAS(MPI_Info_get_valuelen, mpi_info_get_valuelen);
NM_MPI_ALIAS(MPI_Info_dup, mpi_info_dup);

/* ********************************************************* */

static void nm_mpi_info_env_fill(struct nm_mpi_info_s*p_info_env);

__PUK_SYM_INTERNAL
void nm_mpi_info_lazy_init(void)
{
  const int count = nm_atomic_inc(&nm_mpi_info.refcount);
  assert(count >= 0);
  if(count == 0)
    {
      nm_mpi_handle_info_init(&nm_mpi_info.infos);
      /* built-in info */
      struct nm_mpi_info_s*p_info_env = nm_mpi_handle_info_store(&nm_mpi_info.infos, MPI_INFO_ENV);
      nm_mpi_info_hashtable_init(&p_info_env->content);
      nm_mpi_info_env_fill(p_info_env);
    }
}

__PUK_SYM_INTERNAL
void nm_mpi_info_lazy_exit(void)
{
  const int count = nm_atomic_dec(&nm_mpi_info.refcount);
  assert(count >= 0);
  if(count == 0)
    {
      struct nm_mpi_info_s*p_info_env = nm_mpi_handle_info_get(&nm_mpi_info.infos, MPI_INFO_ENV);
      nm_mpi_info_hashtable_destroy(&p_info_env->content);
      nm_mpi_handle_info_free(&nm_mpi_info.infos, p_info_env);
      nm_mpi_handle_info_finalize(&nm_mpi_info.infos, NULL);
    }
}

__PUK_SYM_INTERNAL
struct nm_mpi_info_s*nm_mpi_info_alloc(void)
{
  nm_mpi_info_lazy_init();
  struct nm_mpi_info_s*p_info = nm_mpi_handle_info_alloc(&nm_mpi_info.infos);
  nm_mpi_info_hashtable_init(&p_info->content);
  return p_info;
}

 __PUK_SYM_INTERNAL
 void nm_mpi_info_destructor(char*p_key, char*p_data)
{
  padico_free(p_key);
  padico_free(p_data);
}

__PUK_SYM_INTERNAL
void nm_mpi_info_free(struct nm_mpi_info_s*p_info)
{
  nm_mpi_info_hashtable_destroy(&p_info->content);
  nm_mpi_handle_info_free(&nm_mpi_info.infos, p_info);
  nm_mpi_info_lazy_exit();
}

__PUK_SYM_INTERNAL
struct nm_mpi_info_s*nm_mpi_info_get(MPI_Info info)
{
  assert(nm_mpi_handle_info_initialized(&nm_mpi_info.infos));
  struct nm_mpi_info_s*p_info = nm_mpi_handle_info_get(&nm_mpi_info.infos, info);
  return p_info;
}

__PUK_SYM_INTERNAL
void nm_mpi_info_define(struct nm_mpi_info_s*p_info, const char*p_key, const char*p_value)
{
  if(nm_mpi_info_hashtable_probe(&p_info->content, p_key))
    {
      char*oldkey = NULL, *oldvalue = NULL;
      nm_mpi_info_hashtable_lookup2(&p_info->content, p_key, &oldkey, &oldvalue);
      nm_mpi_info_hashtable_remove(&p_info->content, p_key);
      padico_free(oldkey);
      padico_free(oldvalue);
    }
  nm_mpi_info_hashtable_insert(&p_info->content, padico_strdup(p_key), padico_strdup(p_value));
}

__PUK_SYM_INTERNAL
struct nm_mpi_info_s*nm_mpi_info_dup(struct nm_mpi_info_s*p_info)
{
  struct nm_mpi_info_s*p_info_dup = nm_mpi_info_alloc();
  nm_mpi_info_copy(p_info_dup, p_info);
  return p_info_dup;
}

__PUK_SYM_INTERNAL
void nm_mpi_info_copy(struct nm_mpi_info_s*p_dest_info, struct nm_mpi_info_s*p_src_info)
{
  int i;
  nm_mpi_info_hashtable_enumerator_t e = nm_mpi_info_hashtable_enumerator_new(&p_src_info->content);
  char*key, *data;
  for(i = 0; i < nm_mpi_info_hashtable_size(&p_src_info->content); ++i)
    {
      nm_mpi_info_hashtable_enumerator_next2(e, &key, &data);
      nm_mpi_info_hashtable_insert(&p_dest_info->content, padico_strdup(key), padico_strdup(data));
    }
  nm_mpi_info_hashtable_enumerator_delete(e);
}

__PUK_SYM_INTERNAL
void nm_mpi_info_update(struct nm_mpi_info_s*p_info_up, struct nm_mpi_info_s*p_info_origin)
{
  assert(p_info_origin);
  if(NULL == p_info_up) return;
  int i;
  nm_mpi_info_hashtable_enumerator_t e = nm_mpi_info_hashtable_enumerator_new(&p_info_up->content);
  char*key, *data;
  for(i = 0; i < nm_mpi_info_hashtable_size(&p_info_up->content); ++i)
    {
      nm_mpi_info_hashtable_enumerator_next2(e, &key, &data);
      if(nm_mpi_info_hashtable_probe(&p_info_origin->content, key))
        {
          char*oldkey = NULL, *oldvalue = NULL;
          nm_mpi_info_hashtable_lookup2(&p_info_origin->content, key, &oldkey, &oldvalue);
          nm_mpi_info_hashtable_remove(&p_info_origin->content, key);
          padico_free(oldkey);
          padico_free(oldvalue);
          nm_mpi_info_hashtable_insert(&p_info_origin->content, padico_strdup(key), padico_strdup(data));
        }
    }
}

static void nm_mpi_info_env_fill(struct nm_mpi_info_s*p_info_env)
{
  nm_mpi_info_define(p_info_env, "host", padico_hostname());
  nm_mpi_info_define(p_info_env, "mpi_initial_errhandler", "MPI_ERRORS_ARE_FATAL");
  char*cwd = get_current_dir_name();
  nm_mpi_info_define(p_info_env, "wdir", cwd);
  padico_free(cwd);
}

/* ********************************************************* */

int mpi_info_create(MPI_Info*info)
{
  struct nm_mpi_info_s*p_info = nm_mpi_info_alloc();
  *info = p_info->id;
  return MPI_SUCCESS;
}

int mpi_info_create_env(int argc, char argv[], MPI_Info *info)
{
  struct nm_mpi_info_s*p_info = nm_mpi_info_alloc();
  nm_mpi_info_env_fill(p_info);
  *info = p_info->id;
  return MPI_SUCCESS;
}

int mpi_info_free(MPI_Info*info)
{
  struct nm_mpi_info_s*p_info = nm_mpi_handle_info_get(&nm_mpi_info.infos, *info);
  if(p_info == NULL)
    return NM_MPI_ERROR_WORLD(MPI_ERR_INFO);
  nm_mpi_info_free(p_info);
  *info = MPI_INFO_NULL;
  return MPI_SUCCESS;
}

int mpi_info_dup(MPI_Info info, MPI_Info*newinfo)
{
  struct nm_mpi_info_s*p_info = nm_mpi_handle_info_get(&nm_mpi_info.infos, info);
  if(p_info == NULL)
    return NM_MPI_ERROR_WORLD(MPI_ERR_INFO);
  if(newinfo == NULL)
    return NM_MPI_ERROR_WORLD(MPI_ERR_ARG);
  struct nm_mpi_info_s*p_newinfo = nm_mpi_info_dup(p_info);
  *newinfo = p_newinfo->id;
  return MPI_SUCCESS;
}

int mpi_info_set(MPI_Info info, const char*key, const char*value)
{
  struct nm_mpi_info_s*p_info = nm_mpi_handle_info_get(&nm_mpi_info.infos, info);
  if(p_info == NULL)
    return NM_MPI_ERROR_WORLD(MPI_ERR_INFO);
  nm_mpi_info_define(p_info, key, value);
  return MPI_SUCCESS;
}

int mpi_info_get(MPI_Info info, const char*key, int valuelen, char*value, int*flag)
{
  struct nm_mpi_info_s*p_info = nm_mpi_handle_info_get(&nm_mpi_info.infos, info);
  if(p_info == NULL)
    return NM_MPI_ERROR_WORLD(MPI_ERR_INFO);
  char*v = nm_mpi_info_hashtable_lookup(&p_info->content, key);
  if(v == NULL)
    {
      *flag = 0;
    }
  else
    {
      if(strlen(v) >= valuelen)
        {
          return NM_MPI_ERROR_WORLD(MPI_ERR_NO_SPACE);
        }
      strcpy(value, v);
      *flag = 1;
    }
  return MPI_SUCCESS;
}

int mpi_info_get_string(MPI_Info info, const char*key, int*buflen, char*value, int*flag)
{
  struct nm_mpi_info_s*p_info = nm_mpi_handle_info_get(&nm_mpi_info.infos, info);
  if(p_info == NULL)
    return NM_MPI_ERROR_WORLD(MPI_ERR_INFO);
  if(*buflen < 0)
    return NM_MPI_ERROR_WORLD(MPI_ERR_SIZE);
  char*v = nm_mpi_info_hashtable_lookup(&p_info->content, key);
  if(v == NULL)
    {
      *flag = 0;
    }
  else
    {
      if(*buflen != 0)
        {
          strncpy(value, v, *buflen);
          value[*buflen - 1] = '\0';
        }
      *flag = 1;
      *buflen = strlen(v) + 1;
    }
  return MPI_SUCCESS;
}


int mpi_info_delete(MPI_Info info, const char*key)
{
  struct nm_mpi_info_s*p_info = nm_mpi_handle_info_get(&nm_mpi_info.infos, info);
  if(p_info == NULL)
    return NM_MPI_ERROR_WORLD(MPI_ERR_INFO);
  if(!nm_mpi_info_hashtable_probe(&p_info->content, key))
    return NM_MPI_ERROR_WORLD(MPI_ERR_INFO_NOKEY);
  char*oldkey = NULL, *oldvalue = NULL;
  nm_mpi_info_hashtable_lookup2(&p_info->content, key, &oldkey, &oldvalue);
  nm_mpi_info_hashtable_remove(&p_info->content, key);
  padico_free(oldkey);
  padico_free(oldvalue);
  return MPI_SUCCESS;
}

int mpi_info_get_nkeys(MPI_Info info, int*nkeys)
{
  struct nm_mpi_info_s*p_info = nm_mpi_handle_info_get(&nm_mpi_info.infos, info);
  if(p_info == NULL)
    return NM_MPI_ERROR_WORLD(MPI_ERR_INFO);
  *nkeys = nm_mpi_info_hashtable_size(&p_info->content);
  return MPI_SUCCESS;
}

int mpi_info_get_nthkey(MPI_Info info, int n, char*key)
{
  struct nm_mpi_info_s*p_info = nm_mpi_handle_info_get(&nm_mpi_info.infos, info);
  if(p_info == NULL)
    return NM_MPI_ERROR_WORLD(MPI_ERR_INFO);
  nm_mpi_info_hashtable_enumerator_t e = nm_mpi_info_hashtable_enumerator_new(&p_info->content);
  char*ikey = NULL;
  int i;
  for(i = 0; i <= n; i++)
    {
      ikey = nm_mpi_info_hashtable_enumerator_next_key(e);
      if(ikey == NULL)
        break;
    }
  if(ikey != NULL)
    {
      strncpy(key, ikey, MPI_MAX_INFO_KEY);
    }
  else
    {
      return NM_MPI_ERROR_WORLD(MPI_ERR_OTHER);
    }
  return MPI_SUCCESS;
}

int mpi_info_get_valuelen(MPI_Info info, const char*key, int*valuelen, int*flag)
{
  struct nm_mpi_info_s*p_info = nm_mpi_handle_info_get(&nm_mpi_info.infos, info);
  if(p_info == NULL)
    return NM_MPI_ERROR_WORLD(MPI_ERR_INFO);
  char*v = nm_mpi_info_hashtable_lookup(&p_info->content, key);
  if(v == NULL)
    {
      *flag = 0;
    }
  else
    {
      *valuelen = strlen(v);
      *flag = 1;
    }
  return MPI_SUCCESS;
}

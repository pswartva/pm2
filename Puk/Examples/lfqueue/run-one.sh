#! /bin/bash

q=$1
localhost=$2
peer=$3

TIMEOUT=300
MAXTHREADS=32

echo "# queue = ${q}; localhost = ${localhost}; peer = ${peer}"

if [ "x${q}" = "x" ]; then
    exit 1
fi
#if [ "x${localhost}" = "x" ]; then
#    exit 1
#fi
#if [ "x${peer}" = "x" ]; then
#    exit 1
#fi

( cd ${HOME}/soft/pm2/pm2/scripts ; ./pm2-build-packages --prefix=${HOME}/soft/x86_64 --purge ./madmpi-mini.conf -- --enable-libconcurrent --enable-cfqueues --enable-wcq --enable-wfqueue --enable-looqueue --enable-lfqueue=${q} )

make clean && make install

./run-prodcons.sh ${q}

if [ "x${localhost}" != "x" -a "x${peer}" != "x" ]; then

    padico-launch -n 2 -nodelist ${localhost},${peer} --timeout ${TIMEOUT} nm_bench_sendrecv -N 10000 | tee ${q}-sendrecv.dat
    
    ( cd ${HOME}/soft/pm2/pm2/scripts ; ./pm2-build-packages --prefix=${HOME}/soft/x86_64 --purge ./madmpi.conf -- --enable-libconcurrent --enable-cfqueues --enable-wcq --enable-wfqueue --enable-looqueue --enable-lfqueue=${q} )
    
    padico-launch -n 2 -nodelist ${localhost},${peer} --timeout ${TIMEOUT} nm_piom_pingpong -S 1 -E 1 -T ${MAXTHREADS} -N 1000 | tee ${q}-piom.dat

else

    echo "no localhost / peer given, skipping network benchmark"
    
fi

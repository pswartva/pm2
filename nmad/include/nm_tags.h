/*
 * NewMadeleine
 * Copyright (C) 2006-2023 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#ifndef NM_TAGS_H
#define NM_TAGS_H

/** @ingroup nmad_private
 * @file
 * Private definitions for managing tags and structures indexed by tags.
 */

#define NM_TAGS_PREALLOC 255

/* ** Tag-indexed containers- Warning: black-magic here! */


/** checks whether tags are equal. */
static inline int nm_core_tag_eq(const nm_core_tag_t tag1, const nm_core_tag_t tag2)
{
  return ((tag1.hashcode == tag2.hashcode) && (tag1.tag == tag2.tag));
}
/** checks whether matching applies on tags- (recv_tag & mask) == lookup_tag */
static inline int nm_core_tag_match(nm_core_tag_t recv_tag, nm_core_tag_t lookup_tag, nm_core_tag_t mask)
{
  assert((lookup_tag.tag & mask.tag) == lookup_tag.tag);
  return ( ((recv_tag.hashcode & mask.hashcode) == lookup_tag.hashcode) &&
           ((recv_tag.tag & mask.tag) == lookup_tag.tag) );
}

static inline uint32_t nm_core_tag_hash(const nm_core_tag_t*p_tag)
{
  const uint32_t hash = p_tag->tag + p_tag->hashcode;
  return hash;
}
/** checks whether tags are equal, for hashtables. */
static inline int nm_core_tag_heq(const nm_core_tag_t*p_tag1, const nm_core_tag_t*p_tag2)
{
  return ((p_tag1->hashcode == p_tag2->hashcode) && (p_tag1->tag == p_tag2->tag));
}

/** vector of tags, for *_get_tags() functions */
PUK_VECT_TYPE(nm_core_tag, nm_core_tag_t);

/** macro to generate tables indexed by tags */
#define NM_TAG_TABLE_TYPE(NAME, ENTRY_TYPE)                             \
  struct NAME##_entry_s                                                 \
  {                                                                     \
    ENTRY_TYPE _data;                                                   \
    nm_core_tag_t _tag;                                                 \
  };                                                                    \
  NM_ALLOCATOR_TYPE(NAME##_entry, struct NAME##_entry_s);               \
  PUK_HASHTABLE_TYPE(NAME, nm_core_tag_t*, struct NAME##_entry_s*, &nm_core_tag_hash, &nm_core_tag_heq, NULL); \
  struct NAME##_table_s                                                 \
  {                                                                     \
    struct NAME##_hashtable_s _h;                                       \
    struct NAME##_entry_allocator_s _allocator;                         \
  };                                                                    \
  static inline void NAME##_table_init(struct NAME##_table_s*t)         \
  {                                                                     \
    NAME##_hashtable_init(&t->_h);                                      \
    NAME##_entry_allocator_init(&t->_allocator, NM_TAGS_PREALLOC);      \
  }                                                                     \
  static inline void NAME##_table_destroy(struct NAME##_table_s*t)      \
  {                                                                     \
    NAME##_hashtable_enumerator_t e = NAME##_hashtable_enumerator_new(&t->_h); \
    struct NAME##_entry_s*entry = NAME##_hashtable_enumerator_next_data(e); \
    while(entry)                                                        \
      {                                                                 \
        NAME##_dtor(&entry->_data);                                     \
        NAME##_entry_free(&t->_allocator, entry);                       \
        entry = NAME##_hashtable_enumerator_next_data(e);               \
      }                                                                 \
    NAME##_hashtable_enumerator_delete(e);                              \
    NAME##_hashtable_destroy(&t->_h);                                   \
    NAME##_entry_allocator_destroy(&t->_allocator);                     \
  }                                                                     \
  static inline ENTRY_TYPE* NAME##_get(struct NAME##_table_s*t, nm_core_tag_t tag) \
  {                                                                     \
    assert(t != NULL);                                                  \
    struct NAME##_entry_s*entry = NAME##_hashtable_lookup(&t->_h, &tag); \
    if(entry == NULL)                                                   \
      {                                                                 \
        entry = NAME##_entry_malloc(&t->_allocator);                    \
        NAME##_ctor(&entry->_data, tag);                                \
        entry->_tag = tag;                                              \
        NAME##_hashtable_insert(&t->_h, &entry->_tag, entry);           \
      }                                                                 \
    return &entry->_data;                                               \
  }                                                                     \
  static inline void NAME##_delete(struct NAME##_table_s*t, ENTRY_TYPE*_e) \
  {                                                                     \
    struct NAME##_entry_s*entry = (void*)_e;                            \
    NAME##_hashtable_remove(&t->_h, &entry->_tag);                      \
    NAME##_dtor(&entry->_data);                                         \
    NAME##_entry_free(&t->_allocator, entry);                           \
  }                                                                     \
  /* get a vector of all tags in the table */                           \
  static inline nm_core_tag_vect_t NAME##_get_tags(struct NAME##_table_s*t) \
  {                                                                     \
    nm_core_tag_vect_t v = nm_core_tag_vect_new();                      \
    NAME##_hashtable_enumerator_t e = NAME##_hashtable_enumerator_new(&t->_h); \
    struct NAME##_entry_s*entry = NAME##_hashtable_enumerator_next_data(e); \
    while(entry)                                                        \
      {                                                                 \
        nm_core_tag_vect_push_back(v, entry->_tag);                     \
        entry = NAME##_hashtable_enumerator_next_data(e);               \
      }                                                                 \
    NAME##_hashtable_enumerator_delete(e);                              \
    return v;                                                           \
  }

/** struct to store matching info for wildcard requests, one per session */
struct nm_matching_wildcard_s
{
  struct nm_req_list_s unpacks; /**< list of wildcards unpacks; non-wildcard unpacks are in nm_gtag_s */
  struct nm_unexpected_wildcard_list_s unexpected; /**< global list of unexpected chunks */
};
__PUK_SYM_INTERNAL void nm_unexpected_wildcard_clean(struct nm_unexpected_wildcard_list_s*p_unexpected);
static inline void nm_matching_wildcard_ctor(struct nm_matching_wildcard_s*p_wildcard, nm_core_tag_t tag __attribute__((unused)))
{
  nm_req_list_init(&p_wildcard->unpacks);
  nm_unexpected_wildcard_list_init(&p_wildcard->unexpected);
}
static inline void nm_matching_wildcard_dtor(struct nm_matching_wildcard_s*p_wildcard)
{
  nm_unexpected_wildcard_clean(&p_wildcard->unexpected);
}
NM_TAG_TABLE_TYPE(nm_matching_wildcard, struct nm_matching_wildcard_s)
/** get the wildcard structure from a full tag, using only its hashcode */
static inline struct nm_matching_wildcard_s*nm_matching_wildcard_bytag(struct nm_core*p_core, nm_core_tag_t core_tag);

/** struct to store matching info for any-source requests of a given tag */
struct nm_matching_tag_s
{
  struct nm_req_list_s unpacks;
  struct nm_unexpected_tag_list_s unexpected;
};
static inline void nm_matching_tag_ctor(struct nm_matching_tag_s*p_matching_tag, nm_core_tag_t tag __attribute__((unused)))
{
  nm_req_list_init(&p_matching_tag->unpacks);
  nm_unexpected_tag_list_init(&p_matching_tag->unexpected);
}
static inline void nm_matching_tag_dtor(struct nm_matching_tag_s*p_matching_tag __attribute__((unused)))
{
}
NM_TAG_TABLE_TYPE(nm_matching_tag, struct nm_matching_tag_s)

#endif /* NM_TAGS_H */

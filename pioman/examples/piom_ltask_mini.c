/*
 * Pioman: a Generic I/O Manager
 * Copyright (C) 2001-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

/** @file
 * mini ltask example with polling in memory
 */

#include <pioman.h>
#include <stdio.h>

static struct piom_ltask ltask;
static volatile int flag = 0;
static long long int count = 0;
static double seconds = 0.5;

void*worker(void*dummy)
{
  puk_usleep(seconds * 1000.0 * 1000.0);
  flag = 1;
  return NULL;
}

int ltask_handler(void*arg)
{
  count++;
  if(flag)
    {
      fprintf(stderr, "poll success in handler!\n");
      piom_ltask_completed(&ltask);
    }
  return 0;
}

static void usage(char**argv)
{
  fprintf(stderr, "usage: %s [<s>]\n", argv[0]);
  fprintf(stderr, "  <s>  number of seconds to wait.\n");
}

int main(int argc, char**argv)
{
  /* init pioman */
  pioman_init();

  if(argc == 2)
    {
      seconds = atof(argv[1]);
      if(seconds <= 0.0)
        {
          usage(argv);
          abort();
        }
    }
  else if(argc != 1)
    {
      usage(argv);
      exit(1);
    }

  /* create and submit an ltask that polls into memory */
  piom_ltask_create(&ltask, &ltask_handler, NULL, PIOM_LTASK_OPTION_REPEAT);
  piom_ltask_submit(&ltask);

#ifdef PIOMAN_MULTITHREAD
  /* create a thread that will trigger an event in 5 seconds. */
  piom_thread_t tid;
  piom_thread_create(&tid, NULL, &worker, NULL);
#else /* PIOMAN_MULTITHREAD */
  flag = 1;
#endif /* PIOMAN_MULTITHREAD */

  /* wait for the ltask completion */
  fprintf(stderr, "waiting for ltask completion... (%f seconds)\n", seconds);
  piom_ltask_wait(&ltask);
  fprintf(stderr, "task completed after count = %lld iterations (%lld/s; %f usec/iter).\n",
          count, (long long int)(count/seconds), 1000000.0 * (double)seconds/(double)count);

#ifdef PIOMAN_MULTITHREAD
  piom_thread_join(tid);
#endif /* PIOMAN_MULTITHREAD */

  /* finalize pioman */
  pioman_exit();
  return 0;
}

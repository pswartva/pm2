/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file psm2-discover.c
 *  @brief Intel PSM2 detection
 */

#include <Padico/Puk.h>
#include <Padico/Module.h>
#include <Padico/Topology.h>
#include <psm2.h>

static int padico_psm2_init(void);
static void padico_psm2_finalize(void);

PADICO_MODULE_DECLARE(PSM2_discover, padico_psm2_init, NULL, padico_psm2_finalize,
                      "Topology");

/* ********************************************************* */

static uint64_t dummy_addr = 0; /* dummy address; real PSM2 addressing system uses ibverbs */

static int init_done = 0;

static void padico_psm2_discover(void)
{
  if(getenv("HFI_NO_CPUAFFINITY") == NULL)
    {
      setenv("HFI_NO_CPUAFFINITY", "1", 1);
      padico_out(puk_verbose_notice, "HFI_NO_CPUAFFINITY not defined in current environnment; forcing HFI_NO_CPUAFFINITY=1\n");
    }
#ifdef PIOMAN
  if(getenv("PSM2_RCVTHREAD") == NULL)
    {
      setenv("PSM2_RCVTHREAD", "0", 1);
      padico_out(puk_verbose_notice, "PSM2_RCVTHREAD not defined in current environnment; forcing PSM2_RECVTHREAD=0\n");
    }
#endif /* PIOMAN */

  int ver_major = PSM2_VERNO_MAJOR;
  int ver_minor = PSM2_VERNO_MINOR;
  int rc = psm2_init(&ver_major, &ver_minor);
  if(rc != PSM2_OK)
    {
      padico_out(puk_verbose_critical, "cannot init PSM2 library.\n");
      return;
    }
  init_done = 1;
  uint32_t num_units = -1;
  rc = psm2_ep_num_devunits(&num_units);
  if(rc == PSM2_OK)
    {
      padico_print("detected %d devices.\n", num_units);
      const char*s_uuid = padico_getenv("PADICO_BOOT_UUID");
      padico_string_t s_net = padico_string_new();
      padico_string_printf(s_net, "PSM2-[%s]", s_uuid);
      padico_topo_network_t network = padico_topo_network_getbyid(padico_string_get(s_net));
      if(network == NULL)
        {
          network = padico_topo_network_create(padico_string_get(s_net), PADICO_TOPO_SCOPE_HOST, sizeof(uint64_t), AF_UNSPEC);
        }
      padico_string_delete(s_net);
      int i;
      for(i = 1; i <= num_units; i++)
        {
          padico_topo_binding_t binding = padico_topo_host_bind(padico_topo_getlocalhost(), network, &dummy_addr);
          padico_topo_binding_add_index(binding, i);

        }
    }
  else
    {
      padico_print("no device found.\n");
    }
}

static int padico_psm2_init(void)
{
  padico_psm2_discover();
  return 0;
}

static void padico_psm2_finalize(void)
{
  if(init_done)
    {
      init_done = 0;
      psm2_finalize();
    }
}

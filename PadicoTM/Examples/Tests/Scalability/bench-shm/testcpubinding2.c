#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#define BUFFMAX 5000
#define FALSE 0
#define TRUE 1

void razarray(int* tab,int nb)
{
  int i=0;
  for(i=0;i<nb;i++){
    tab[i]=-1;
  }
}

int isused(int* tab,int nb,int a)
{
  int i=0;
  while(tab[i]!=-1&&i<nb){
    if(tab[i]==a)
      {
        return TRUE;
      }
    i++;
  }
  return FALSE;
}

int isfull(int* tab,int nb){
  int i=0;
  while(tab[i]!=-1&&i<nb){
    i++;
  }
  if(i<nb){
    return FALSE;
  }
  return TRUE;
}

void addarray(int* tab,int nb,int a)
{
  int i=0;
  while(tab[i]!=-1&&i<nb){
    i++;
  }
  if(i<nb){
    tab[i]=a;
  }
  else{
    printf("error");
  }
}

int main(int argc, char *argv[])
{
  if (argc!=5){
    printf("wrong\n");
    return 0;
  }
  else
    {
    FILE* fichier = NULL;

    fichier = fopen(argv[1], "r");

    char buff[BUFFMAX];
    int i=0;
    int nblevel=atoi(argv[2]);
    int mode = atoi(argv[3]);
    char namelevel[nblevel][15];
    int nbprocess=atoi(argv[4]);
    int j=0;
    int numberbylevel[nblevel];
    int k=0;
    int a=0;
    int inc=4;
    if(fichier != NULL)
      {
        while(fgets(buff,BUFFMAX,fichier)!=NULL&&strstr(buff,"PU")==NULL)
          {
            sscanf(buff," %s ",namelevel[i]);
            i++;
          }
        rewind(fichier);
        while(fgets(buff,BUFFMAX,fichier)!=NULL)
          {
            if(strstr(buff,"PU")!=NULL)
              {
                j++;
              }
          }
        rewind(fichier);

        int usedPU[j];

        int arrayPU[j];

        j=0;
        int a;
        while(fgets(buff,BUFFMAX,fichier)!=NULL)
          {
            if(strstr(buff,"PU")!=NULL)
              {
                sscanf(buff," PU P#%d ",&arrayPU[j]);
                j++;
              }
          }
        razarray(usedPU,j);

        rewind(fichier);

        for(i=0;i<nblevel;i++){
          while(fgets(buff,BUFFMAX,fichier)!=NULL&&strstr(buff,namelevel[i])==NULL){}
          k=0;
          while(fgets(buff,BUFFMAX,fichier)!=NULL&&strstr(buff,namelevel[i])==NULL)
            {
              if(strstr(buff,"PU")!=NULL)
                {
                  k++;
                }
            }
          numberbylevel[i]=k;
          rewind(fichier);
        }

        if(mode==-1){
          for(i=0;i<nbprocess;i++){
            printf("--padico-cpubind:%d=%d ",i,arrayPU[i%j]);
          }
        }
        else{
          k=0;
          for(i=0;i<nbprocess;i+=2){
            if(isfull(usedPU,j)==TRUE){
              razarray(usedPU,j);
              k=0;
              inc=4;
            }
            if(k>=j){
                k=0;
                if(inc>1){
                  inc/=2;
                }
            }
            a=arrayPU[k];
            while(isused(usedPU,j,a)==TRUE)
              {
                k+=inc;
                if(k>=j){
                  k=0;
                  if(inc>1){
                    inc/=2;
                  }
                }
                a=arrayPU[k];
              }
            a=arrayPU[k%j];
            printf("--padico-cpubind:%d=%d ",i,a);
            addarray(usedPU,j,a);
            a=arrayPU[(k+numberbylevel[mode])%j];
            printf("--padico-cpubind:%d=%d ",i+1,a);
            addarray(usedPU,j,arrayPU[(k+numberbylevel[mode])]);
            k+=inc;
          }
        }
      }
    fclose(fichier);
    }
  return 0;
}

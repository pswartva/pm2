/*
 * MPI sync clocks
 * Copyright (C) 2006-2020 (see AUTHORS file)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

#include <mpi.h>

#include <mpi_sync_clocks.h>


int main(int argc, char**argv)
{
  MPI_Init(&argc,&argv);

  int rank = -1;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  /* initialize sync clocks */
  mpi_sync_clocks_t p_clocks = mpi_sync_clocks_init(MPI_COMM_WORLD);

  /* get time before and after MPI_Barrier */
  double local_t1 = mpi_sync_clocks_get_time_usec(p_clocks);
  MPI_Barrier(MPI_COMM_WORLD);
  double local_t2 = mpi_sync_clocks_get_time_usec(p_clocks);

  /* synchronize clocks to interpolate t1 & t2 */
  mpi_sync_clocks_synchronize(p_clocks);

  printf("rank = %d; local_t1 = %g usec.; global = %g usec. [before MPI_Barrier]\n",
         rank, local_t1, mpi_sync_clocks_local_to_global(p_clocks, local_t1));
  printf("rank = %d; local_t2 = %g usec.; global = %g usec. [after MPI_Barrier]\n",
         rank, local_t2, mpi_sync_clocks_local_to_global(p_clocks, local_t2));

  /* get time before and after sync clocks barrier */
  double local_t3 = mpi_sync_clocks_get_time_usec(p_clocks);
  mpi_sync_clocks_barrier(p_clocks, NULL);
  double local_t4 = mpi_sync_clocks_get_time_usec(p_clocks);

  /* synchronize clocks to interpolate t2 & t3 */
  mpi_sync_clocks_synchronize(p_clocks);

  printf("rank = %d; local_t3 = %g usec.; global = %g usec.\n",
         rank, local_t3, mpi_sync_clocks_local_to_global(p_clocks, local_t3));
  printf("rank = %d; local_t4 = %g usec.; global = %g usec. [after sync clocks barrier]\n",
         rank, local_t4, mpi_sync_clocks_local_to_global(p_clocks, local_t4));

  /* shut down mpi sync clocks */
  mpi_sync_clocks_shutdown(p_clocks);
  MPI_Finalize();
  return 0;
}

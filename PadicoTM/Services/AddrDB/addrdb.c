/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Address DB for easy connection establishment.
 * @ingroup NetAccess
 */

#include <Padico/Puk.h>
#include <Padico/Module.h>
#include <Padico/PadicoControl.h>
#include <Padico/Topology.h>
#include "AddrDB.h"

static int padico_addrdb_init(void);
static void padico_addrdb_finalize(void);

PADICO_MODULE_DECLARE(AddrDB, padico_addrdb_init, NULL, padico_addrdb_finalize,
                      "Topology", "PadicoControl");

static struct
{
  puk_hashtable_t table;
  marcel_mutex_t lock;
  marcel_cond_t event;
} addr_db;

/* ********************************************************* */

/* ** address hashing */

struct padico_address_key_s
{
  padico_topo_node_t node; /**< publisher */
  char*component_name;
  void*instance;
  size_t instance_size;
};
struct padico_address_entry_s
{
  struct padico_address_key_s*key;
  void*addr;
  size_t addr_size;
  void**dyn_addr;
  size_t*dyn_addr_size;
  padico_req_t req; /** request to notify completion */
};

static void padico_addrdb_free_entry(struct padico_address_entry_s*e);

/* ********************************************************* */

static uint32_t padico_addrkey_hash(const void*_addr)
{
  const struct padico_address_key_s*a = _addr;
  uint32_t z = puk_hash_default((const void*)padico_topo_node_getuuid(a->node), PADICO_TOPO_UUID_SIZE) +
    puk_hash_string(a->component_name) +
    puk_hash_default(a->instance, a->instance_size);
  return z;
}

static int padico_addrkey_eq(const void*_addr1, const void*_addr2)
{
  const struct padico_address_key_s*a1 = _addr1;
  const struct padico_address_key_s*a2 = _addr2;
  const int eq = ( (a1->node == a2->node) &&
                   (strcmp(a1->component_name, a2->component_name) == 0) &&
                   (a1->instance_size == a2->instance_size) &&
                   (memcmp(a1->instance, a2->instance, a1->instance_size) == 0));
  return eq;
}
static struct padico_address_key_s*padico_addrkey_new(padico_topo_node_t node, const char*component,
                                                      const void*instance, size_t instance_size)
{
  struct padico_address_key_s*a = padico_malloc(sizeof(struct padico_address_key_s));
  a->node = node;
  a->component_name = padico_strdup(component);
  a->instance_size = instance_size;
  a->instance = padico_malloc(instance_size);
  memcpy(a->instance, instance, instance_size);
  return a;
}
static void padico_addrkey_delete(struct padico_address_key_s*a)
{
  padico_free(a->component_name);
  padico_free(a->instance);
  padico_free(a);
}

static void padico_addrdb_entry_destructor(void*_key, void*_data)
{
  struct padico_address_entry_s*e = _data;
  padico_addrdb_free_entry(e);
}

/* ********************************************************* */

/* this functions takes ownership of 'instance' and 'addr' */
static void padico_addrdb_insert(padico_topo_node_t node, const char*component_name,
                                 const void*instance, size_t instance_size,
                                 void*addr, size_t addr_size);

static void padico_addrdb_publish_handler(puk_parse_entity_t e)
{
  const char*enc_instance = puk_parse_getattr(e, "instance");
  const char*enc_addr = puk_parse_getattr(e, "addr");
  const char*s_from = puk_parse_getattr(e, "from");
  const char*component = puk_parse_getattr(e, "component");
  padico_topo_node_t from = padico_topo_getnodebyname(s_from);
  size_t instance_size = strlen(enc_instance);
  void*instance = puk_hex_decode(enc_instance, &instance_size, NULL);
  size_t addr_size = strlen(enc_addr);
  void*addr = puk_hex_decode(enc_addr, &addr_size, NULL);
  padico_addrdb_insert(from, component, instance, instance_size, addr, addr_size);
  padico_free(instance); /* addrdb_insert takes ownership of 'addr' */
}

static int padico_addrdb_init(void)
{
  addr_db.table = puk_hashtable_new(&padico_addrkey_hash, &padico_addrkey_eq);
  marcel_mutex_init(&addr_db.lock, NULL);
  marcel_cond_init(&addr_db.event, NULL);
  puk_xml_add_action((struct puk_tag_action_s)
                     {
                       .xml_tag        = "AddrDB:publish",
                       .start_handler  = &padico_addrdb_publish_handler,
                       .end_handler    = NULL,
                       .required_level = PUK_TRUST_CONTROL
                     });
  return 0;
}

static void padico_addrdb_finalize(void)
{
  puk_xml_delete_action("AddrDB:publish");
  puk_hashtable_delete(addr_db.table, &padico_addrdb_entry_destructor);
}

void padico_addrdb_publish(padico_topo_node_t node, const char*component,
                           const void*instance, size_t instance_size,
                           const void*addr, size_t addr_size)
{
  char*enc_instance = puk_hex_encode(instance, &instance_size, NULL);
  char*enc_addr = puk_hex_encode(addr, &addr_size, NULL);
  padico_string_t msg = padico_string_new();
  padico_string_catf(msg, "<AddrDB:publish component=\"%s\" from=\"%s\" instance=\"%s\" addr=\"%s\"/>\n",
                     component, padico_topo_node_getname(padico_topo_getlocalnode()), enc_instance, enc_addr);
  padico_control_send_oneway(node, padico_string_get(msg));
  padico_string_delete(msg);
  padico_free(enc_instance);
  padico_free(enc_addr);
}

static struct padico_address_entry_s*padico_addrdb_new_entry(struct padico_address_key_s*k)
{
  struct padico_address_entry_s*e = padico_malloc(sizeof(struct padico_address_entry_s));
  e->key = k;
  e->req = NULL;
  e->addr = NULL;
  e->addr_size = 0;
  e->dyn_addr = NULL;
  e->dyn_addr_size = NULL;
  return e;
}

static void padico_addrdb_free_entry(struct padico_address_entry_s*e)
{
  padico_addrkey_delete(e->key);
  if((e->req == NULL) && (e->addr != NULL))
    {
      padico_free(e->addr);
    }
  padico_free(e);
}

static void padico_addrdb_insert(padico_topo_node_t node, const char*component,
                                 const void*instance, size_t instance_size,
                                 void*addr, size_t addr_size)
{
  struct padico_address_entry_s*e = NULL;
  padico_out(50, "inserting address for node=%p\n", node);
  marcel_mutex_lock(&addr_db.lock);
  struct padico_address_key_s*k = padico_addrkey_new(node, component, instance, instance_size);
  e = puk_hashtable_lookup(addr_db.table, k);
  if(e == NULL)
    {
      e = padico_addrdb_new_entry(k);
      e->addr = addr;
      e->addr_size = addr_size;
      puk_hashtable_insert(addr_db.table, e->key, e);
    }
  else
    {
      padico_addrkey_delete(k);
      k = NULL;
    }
  if(e->req)
    {
      if(e->addr != NULL)
        {
          /* static buffer is waiting */
          assert(e->addr_size >= addr_size);
          memcpy(e->addr, addr, addr_size);
          padico_free(addr);
        }
      else
        {
          /* dynamic buffer */
          assert(e->dyn_addr != NULL);
          *e->dyn_addr = addr;
          *e->dyn_addr_size = addr_size;
        }
      padico_tm_req_notify(e->req->id, NULL);
      puk_hashtable_remove(addr_db.table, e->key);
      padico_addrdb_free_entry(e);
    }
  marcel_mutex_unlock(&addr_db.lock);
  padico_out(50, "insert address for node=%p done.\n", node);
}

void padico_addrdb_get(padico_topo_node_t node, const char*component,
                       const void*instance, size_t instance_size,
                       void*addr, size_t addr_size, padico_req_t req)
{
  struct padico_address_key_s*k = padico_addrkey_new(node, component, instance, instance_size);
  struct padico_address_entry_s*e = NULL;
  padico_out(50, "getting address for node=%p\n", node);
  marcel_mutex_lock(&addr_db.lock);
  e = puk_hashtable_lookup(addr_db.table, k);
  if(e == NULL)
    {
      e = padico_addrdb_new_entry(k);
      e->req = req;
      e->addr = addr;
      e->addr_size = addr_size;
      puk_hashtable_insert(addr_db.table, e->key, e);
    }
  else
    {
      /* addr already received */
      padico_addrkey_delete(k);
      k = NULL;
      assert(e->req == NULL);
      assert(addr_size >= e->addr_size);
      memcpy(addr, e->addr, e->addr_size);
      padico_tm_req_notify(req->id, NULL);
      puk_hashtable_remove(addr_db.table, e->key);
      padico_addrdb_free_entry(e);
    }
  marcel_mutex_unlock(&addr_db.lock);
  padico_out(50, "got address for node=%p\n", node);
}

void padico_addrdb_dynget(padico_topo_node_t node, const char*component,
                          const void*instance, size_t instance_size,
                          void**addr, size_t*addr_size, padico_req_t req)
{
  struct padico_address_key_s*k = padico_addrkey_new(node, component, instance, instance_size);
  struct padico_address_entry_s*e = NULL;
  padico_out(50, "getting address for node=%p\n", node);
  marcel_mutex_lock(&addr_db.lock);
  e = puk_hashtable_lookup(addr_db.table, k);
  if(e == NULL)
    {
      e = padico_addrdb_new_entry(k);
      e->req = req;
      e->dyn_addr = addr;
      e->dyn_addr_size = addr_size;
      puk_hashtable_insert(addr_db.table, e->key, e);
    }
  else
    {
      /* addr already received */
      padico_addrkey_delete(k);
      k = NULL;
      assert(e->req == NULL);
      *addr = e->addr;
      e->addr = NULL;
      *addr_size = e->addr_size;
      padico_tm_req_notify(req->id, NULL);
      puk_hashtable_remove(addr_db.table, e->key);
      padico_addrdb_free_entry(e);
    }
  marcel_mutex_unlock(&addr_db.lock);
}

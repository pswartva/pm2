/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief SysIO: System I/O manager
 */

#ifndef PADICO_SYSIO_H
#define PADICO_SYSIO_H

#include <arpa/inet.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <sys/uio.h>

PUK_MOD_AUTO_DEP(SysIO, PADICO_LAST_MODULE_NAME);
#undef PADICO_LAST_MODULE_NAME
#define PADICO_LAST_MODULE_NAME SysIO

/** @defgroup SysIO API: SysIO interface
 */

/** @addtogroup SysIO
 * @{
 */

/** a NetAccess I/O handle */
typedef struct padico_sysio_s* padico_sysio_t;

/** for backwards compatibility with legacy codes */
#define padico_io_t padico_sysio_t

/** @brief a NetAccess SysIO callback
 * @arg fd the file descriptor on which the event happened
 * @arg key a key given at registration of the callback
 * @return 1 to keep the callback registered, 0 to cancel
 */
typedef int (*padico_sysio_callback_t)(int fd, void*key);

#define PADICO_IO_EVENT_READ   0x01 /**< the fildes is readable */
#define PADICO_IO_EVENT_WRITE  0x02 /**< the fildes is writable */
#define PADICO_IO_EVENT_ERROR  0x04 /**< the fildes has an error */

/** @brief Registers a file descriptor for polling
 * @param fd File descriptor (should already exist)
 * @param what The operation that triggers callback -- several bits may be OR-ed
 * @param callback The callback function that will be called uppon I/O event
 * @param key an argument that will be given to the callback
 * @return an I/O handle for future reference
 * @note after registration, the I/O handle is not activated
 */
padico_io_t padico_io_register(int fd, unsigned int what,
                               padico_sysio_callback_t callback, void*key);

/** @brief Unregister the I/O handle
 * @note even if it is not mandatory, it is better to:
 *  -# deactivate the io
 *  -# perform the required synchronisation (check callback completion)
 *  -# finally, unregister.
 * @note Unregister a callback has a high cost. Most of the time, use
 * deactivation; unregister the callback just before closing the file
 * descriptor. @see padico_io_deactivate()
 */
void padico_io_unregister(padico_io_t);

/** Activates the I/O handle - when not activated, the callback is not triggered */
void padico_io_activate(padico_io_t);

/** Deactivate the I/O handle - should be used for temporarly deactivate callback trigger */
void padico_io_deactivate(padico_io_t);

/** Waits until at least one polling/callback cycle has
 * been performed on the I/O handle.
 */
int padico_sysio_refresh(padico_io_t io);

int padico_sysio_getfd(padico_io_t io);

/** dump internal status of SysIO */
padico_rc_t padico_sysio_stat(void);

int     padico_sysio_socket(int domain, int type, int protocol);
ssize_t padico_sysio_read(int fildes, void *buf, ssize_t nbyte);
ssize_t padico_sysio_write(int fildes, const void *buf, ssize_t nbyte);
ssize_t padico_sysio_writev(int fildes, const struct iovec *iov, int iovcnt);
int     padico_sysio_bound_inet_socket(int*port);;
int     padico_sysio_connected_inet_socket(const struct in_addr*, int port);
int     padico_sysio_connected_inet_socket_sockaddr(const struct sockaddr* addr);
ssize_t padico_sysio_in(int fildes, void *buf, size_t nbyte);
ssize_t padico_sysio_out(int fildes, const void *buf, size_t nbyte);
ssize_t padico_sysio_outv(int fildes, const struct iovec *buf, int iovcnt);
int     padico_sysio_eos(int fd);
int     padico_sysio_shutdown_wr(int s);
int     padico_sysio_close(int fd);
int     padico_sysio_accept(int s, struct sockaddr*addr, socklen_t*addrlen);
int     padico_sysio_errno(void);

/** @} */


#endif /* PADICO_SYSIO_H */

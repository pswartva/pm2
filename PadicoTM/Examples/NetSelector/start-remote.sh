#! /bin/sh


NS_CONFIG=$PWD/local-ssh.xml
rdvport=29856
peer=pointvirgule.bordeaux.inria.fr

scp ${NS_CONFIG} ${peer}:/tmp/

ssh ${peer} env PADICO_CORE=${PADICO_CORE} padico-launch -v -c -nodefault -iNetSelector-preset-vanilla -iControler-preset-router -DNS_BASIC_CONF=/tmp/nsconf.xml -DPADICO_BOOTSTRAP_SYNC=2 -DPADICO_BOOTSTRAP_LISTEN=${rdvport} -DBENCH_PEER=${peer} bench-VIO

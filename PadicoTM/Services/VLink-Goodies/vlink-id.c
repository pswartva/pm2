/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#include <Padico/VLink-internals.h>
#include <Padico/Module.h>


static void vlink_id_init        (padico_vnode_t);
static int  vlink_id_create      (padico_vnode_t);
static int  vlink_id_bind        (padico_vnode_t);
static int  vlink_id_listen      (padico_vnode_t);
static int  vlink_id_connect     (padico_vnode_t);
static void vlink_id_fullshutdown(padico_vnode_t);
static void vlink_id_send_post   (padico_vnode_t);
static void vlink_id_recv_post   (padico_vnode_t);

/** @defgroup VLink_ID component: VLink_ID- VLink over VLink dummy component
 * @ingroup PadicoComponent
 */

/** 'VLink' facet for VLink_ID
 * @ingroup VLink_ID
 */
static const struct padico_vlink_driver_s vlink_id_driver =
{
  .create       = &vlink_id_create,
  .bind         = &vlink_id_bind,
  .listen       = &vlink_id_listen,
  .connect      = &vlink_id_connect,
  .fullshutdown = &vlink_id_fullshutdown,
  .close        = NULL,
  .send_post    = &vlink_id_send_post,
  .recv_post    = &vlink_id_recv_post,
  .dumpstat     = NULL
};

/** VLink_ID instance status
 * @ingroup VLink_ID
 */
struct vlink_id_specific_s
{
  padico_vnode_t sub_vnode;
};

VLINK_CREATE_SPECIFIC_ACCESSORS(id);

static void vlink_id_notifier(padico_vnode_t sub, vobs_attr_t attr, void*key);
static void vlink_id_commit(padico_vnode_t vnode, vobs_attr_t attr);
static void vlink_id_make_new_vnode(padico_vnode_t vnode, padico_vnode_t sub);

/* ********************************************************* */

PADICO_MODULE_COMPONENT(VLink_ID,
  puk_component_declare("VLink_ID",
                        puk_component_uses("VLink", "vlink"),
                        puk_component_provides("VLink", "vlink", &vlink_id_driver),
                        puk_component_provides("PadicoComponent", "component", &padico_vnode_component_driver)));

/* ********************************************************* */

static inline void vlink_id_copy_status(padico_vnode_t vnode)
{
  padico_vnode_t sub = vobs_id(vnode)->sub_vnode;
  vnode->status = sub->status;
  padico_out(40, "vnode=%p; error=%d; readable=%d; writable=%d; opened=%d; connecting=%d; EOF=%d\n",
             vnode, vnode->status.error, vnode->status.readable, vnode->status.writable,
             vnode->status.opened, vnode->status.connecting, vnode->status.end_of_file);
}

static void vlink_id_make_new_vnode(padico_vnode_t vnode, padico_vnode_t sub)
{
  if(!padico_vnode_queue_empty(sub->listener.backlog))
    {
      puk_instance_t self_instance = padico_vnode_get_self_instance(vnode);
      padico_vnode_t new_sub       = padico_vnode_queue_retrieve(sub->listener.backlog);
      puk_instance_t new_container = puk_instance_get_root_container(padico_vnode_get_self_instance(new_sub));
      puk_instance_t new_instance  = puk_instance_find_context(new_container, self_instance);
      padico_vnode_t new_id        = puk_instance_self_status(new_instance);
      vlink_id_init(new_id);
      padico_vsock_init(new_id,
                        vnode->socket.so_family,
                        vnode->socket.so_type,
                        vnode->socket.so_protocol);

      /* copy status and local binding */
      vnode_activate(new_id, binding);
      new_id->status  = new_sub->status;
      new_id->binding = new_sub->binding;
      /* remote binding */
      padico_vsock_acceptor_finalize(new_id, vnode,
                                     new_sub->remote.remote_addr,
                                     new_sub->remote.remote_addrlen);
    }
  else
    {
      padico_warning("Empty backlog in VLink_ID notifier\n");
    }
}

/** tracks changes to our attributes done by the user
 */
static void vlink_id_commit(padico_vnode_t vnode, vobs_attr_t attr)
{
  padico_vnode_t sub = vobs_id(vnode)->sub_vnode;
  padico_out(40, "vnode=%p; sub=%p; attr=%d (%s)\n",
             vnode, sub, attr, vobs_attr_label[attr]);
  switch(attr)
    {
    case vobs_attr_rbox:
      sub->content.rbox = vnode->content.rbox;
      sub->rbox = vnode->rbox;
      break;
    case vobs_attr_wbox:
      sub->content.wbox = vnode->content.wbox;
      sub->wbox = vnode->wbox;
      break;
    default:
      padico_fatal("Unexpected attribute in vlink_id_commit\n");
      break;
    }
  vobs_commit(sub, attr);
}

/** tracks changes in sub-vnode's attributes
 */
static void vlink_id_notifier(padico_vnode_t sub, vobs_attr_t attr, void*key)
{
  padico_vnode_t vnode = (padico_vnode_t)key;
  assert(vobs_id(vnode)->sub_vnode == sub);
  padico_out(40, "vnode=%p; sub=%p; attr=%d (%s)\n",
             vnode, sub, attr, vobs_attr_label[attr]);
  switch(attr)
    {
    case vobs_attr_status:
      vnode->status = sub->status;
      vobs_signal(vnode, attr);
      break;
    case vobs_attr_remote:
      /* new connection as client:
       * copy status, remote and binding from sub_vnode
       */
      vnode->status          = sub->status;
      vnode->content.remote  = sub->content.remote;
      vnode->remote          = sub->remote;
      vnode->content.binding = sub->content.binding;
      vnode->binding         = sub->binding;
      padico_out(30, "vnode=%p; sub=%p vnode.established=%d sub.established=%d\n",
                 vnode, sub, vnode->remote.established, sub->remote.established);
      vobs_signal(vnode, attr);
      break;
    case vobs_attr_listener:
      /* new connection as server:
       * create a new stack of vnodes
       */
      vnode->status = sub->status;
      vlink_id_make_new_vnode(vnode, sub);
      padico_out(30, "vnode=%p; sub=%p; %d connection(s) in backlog\n",
                 vnode, sub, padico_vnode_queue_size(vnode->listener.backlog));
      break;
    case vobs_attr_rbox:
      vnode->rbox = sub->rbox;
      vobs_signal(vnode, attr);
      break;
    case vobs_attr_wbox:
      vnode->wbox = sub->wbox;
      vobs_signal(vnode, attr);
      break;
    default:
      padico_fatal("Unexpected attribute %d in VLink_ID notifier.\n", (int)attr);
      break;
    }
  padico_out(40, "vnode=%p; sub=%p; attr=%d (%s) done.\n",
             vnode, sub, attr, vobs_attr_label[attr]);
}


/* ********************************************************* */

static void vlink_id_init(padico_vnode_t vnode)
{
  const puk_instance_t instance = padico_vnode_get_self_instance(vnode);
  vobs_id_activate(vnode);
  struct puk_receptacle_VLink_s vlink;
  puk_context_indirect_VLink(instance, "vlink", &vlink);
  vobs_id(vnode)->sub_vnode = vlink._status;
  vobs_subscribe(vobs_id(vnode)->sub_vnode, &vlink_id_notifier, vnode);
  vobs_set_commit(vnode, &vlink_id_commit);
}

static int vlink_id_create(padico_vnode_t vnode)
{
  vlink_id_init(vnode);
  int rc = padico_vsock_create(vobs_id(vnode)->sub_vnode,
                               vnode->socket.so_family,
                               vnode->socket.so_type,
                               vnode->socket.so_protocol);
  return rc;
}

static int vlink_id_bind(padico_vnode_t vnode)
{
  int rc = -1;
  assert(vnode->binding.autobind);
  vobs_id(vnode)->sub_vnode->binding = vnode->binding;
  rc = padico_vsock_autobind(vobs_id(vnode)->sub_vnode);
  vnode->binding = vobs_id(vnode)->sub_vnode->binding;
  return rc;
}

static int vlink_id_listen(padico_vnode_t vnode)
{
  int rc = -1;
  rc = padico_vsock_passive_connect(vobs_id(vnode)->sub_vnode,
                                    vnode->listener.backlog_size, NULL);
  vlink_id_copy_status(vnode);
  return rc;
}

static int vlink_id_connect(padico_vnode_t vnode)
{
  int rc = -1;
  rc = padico_vsock_active_connect(vobs_id(vnode)->sub_vnode,
                                   vnode->remote.remote_addr,
                                   vnode->remote.remote_addrlen);
  vlink_id_copy_status(vnode);
  return rc;
}

static void vlink_id_fullshutdown(padico_vnode_t vnode)
{
  padico_vsock_fullshutdown(vobs_id(vnode)->sub_vnode);
  vlink_id_copy_status(vnode);
}

static void vlink_id_send_post(padico_vnode_t vnode)
{
  padico_vnode_write_post(vobs_id(vnode)->sub_vnode,
                          vnode->wbox.write_buffer,
                          vnode->wbox.write_posted);
  vlink_id_copy_status(vnode);
}

static void vlink_id_recv_post(padico_vnode_t vnode)
{
  padico_vnode_read_post(vobs_id(vnode)->sub_vnode,
                         vnode->rbox.read_buffer,
                         vnode->rbox.read_posted);
  vlink_id_copy_status(vnode);
}

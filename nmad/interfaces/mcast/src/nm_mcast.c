/*
 * NewMadeleine
 * Copyright (C) 2021-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <nm_coll_interface.h>
#include <nm_rpc_interface.h>

#include <nm_private.h>

#include "nm_mcast_reorder_trees.h"
#include "nm_mcast_interface.h"

#include <Padico/Module.h>
PADICO_MODULE_BUILTIN(nm_mcast, NULL, NULL, NULL);

PADICO_MODULE_ATTR(tree, "NMAD_MCAST_TREE", "type of tree for mcast; by default, use default bcast tree",
                   string, "default");

PADICO_MODULE_ATTR(delegate, "NMAD_MCAST_DELEGATE", "delegate mcast, i.e. the first node of the group manages the tree instead of the root; the default is 'false'",
                   bool, 0);

PADICO_MODULE_ATTR(reorder_tree, "NMAD_MCAST_REORDER_TREE", "reorder recipients in the broadcast tree according to priorities; the default is 'true'",
                   bool, 1);

#define MCAST_INIT_CANARY 1338

/* To trace mcast, use traces-mcast trace policy:
 * - build with a *-debug.conf
 * - launch with mpirun -t=traces-mcast $program */

#define MCAST_SERVICE_FROM_RPC_TOKEN(__token) nm_rpc_service_get_ref(nm_rpc_get_service(__token))

/** header of RPC requests used for mcast */
struct nm_mcast_header_s
{
  nm_len_t hlen; /**< size of header requested by the user (like if nm_sr_send_header() was called) */
  nm_len_t len;
  nm_tag_t tag;
  nm_seq_t seq;
  nm_session_hash_t session_hash;
  int root_rank;    /**< rank of root sender, in comm_world communicator */
  int n_dests;      /**< number of dests in the sub tree */
  char has_prios:1; /**< wether user provided priority for each recipient: tree will be reordered to respect them */
  nm_coll_tree_kind_t tree_kind; /**< since each mcast can use a different routing algorithm, it needs to be part of the mcast attributes */
} __attribute__((packed));

/** state of a received mcast request, to share state between callbacks */
struct nm_mcast_recv_s
{
  struct nm_mcast_header_s header; /**< header received from network */
  nm_rpc_token_t rpc_token;        /**< token for the incoming RPC request */
  struct nm_req_s*p_req;           /**< core request containing the message */
  int*p_dests;                     /**< received vector of dests; vector size in header */
  int*p_prios;                     /**< received vector of prios, if header.has_prios == 1 */
  nm_seq_t*p_seqs;                 /**< received vector of sequence numbers */
  struct nm_datav_s datav;         /**< vector of all received user data chunk */
  struct nm_data_s data;           /**< content of datav seen as a single data */
  struct nm_mcast_service_s*p_mcast_service;
};

/** a request to send a step for an mcast */
struct nm_mcast_send_s
{
  int*p_dests;                         /**< list of dests for the full request */
  nm_seq_t*p_seqs;
  int*p_prios;                         /**< list of priorities; may be NULL */
  int n_dests;                         /**< number of dests in the multicast (tree size is n_dests + 1) */
  nm_tag_t tag;
  nm_session_hash_t session_hash;
  nm_coll_tree_kind_t tree_kind;
  int root_rank;
  nm_len_t hlen; /**< size of header requested by the user (like if nm_sr_send_header() was called) */
  const struct nm_data_s*p_body_data;  /**< user-supplied data for body of message */
  struct nm_mcast_recv_s*p_mcast_recv; /**< the recv request that triggered this step (needed to finalize the RPC recv) */
  nm_mcast_t*p_user_mcast;             /**< the request posted by the user; either p_mcast_recv or p_user_mcast is NULL */
  int refcount;                        /**< number of children pointing to us */
  struct nm_mcast_send_s*p_next_mcast_send; /** the request for the next step on the same node */
  struct nm_mcast_service_s*p_mcast_service;

  /* Used with bitrees: */
  int*second_p_dests;                         /**< list of dests for the full request in the second tree */
  nm_seq_t*second_p_seqs;
  int*second_p_prios;                         /**< list of priorities in the second tree; may be NULL */
  int second_n_dests;                         /**< number of dests in the second tree (excluding the root) */
};

/** an actual send packet */
struct nm_mcast_send_child_s
{
  struct nm_mcast_header_s header;     /**< header for the message being sent */
  int*p_dests;                         /**< list of dests for the given child */
  nm_seq_t*p_seqs;
  int*p_prios;
  int n_dests;                         /**< number of dests for this sub-tree */
  int index;                           /**< index of the child in the list of dests in the full send */
#ifdef NMAD_DEBUG
  int dest_rank;                       /**< rank of the recipient node */
#endif
  struct nm_mcast_send_s*p_mcast_send; /**< the mcast send this child belongs to */
};

/* ********************************************************* */

/** internal status of mcast module */
struct nm_mcast_service_s
{
  nm_comm_t p_comm;
  nm_rpc_service_t p_service;
  int my_rank;
  nm_coll_tree_kind_t default_tree_kind;
  int use_bitree;
  nm_coll_tree_kind_t bitree_first_kind;
  nm_coll_tree_kind_t bitree_second_kind;
  int bitree_thresold;
  int delegate;
  int reorder_tree;
#ifdef NMAD_PROFILE
  struct
  {
    unsigned long long n_mcast_send;       /**< number of mcast sent */
    unsigned long long n_send_packets;     /**< number of packets sent by mcast */
    unsigned long long n_dests;            /**< total number of destinations; n_dests / n_mcast_send = average number of dests */
    unsigned long long n_mcast_injected;   /**< number of mcast injected locally */
    double total_send_time;
    double total_inject_time;
  } profiling;
#endif /* NMAD_PROFILE */
};

static const nm_tag_t mcast_tag = 0x3dc; // Multicast for IP was introduced in RFC 988 :)

static void nm_mcast_send_step(struct nm_mcast_send_s*p_mcast_send);
static void nm_mcast_step_finalize(struct nm_mcast_send_s*p_mcast_send);

/* ********************************************************* */

/** This function is called when an injected chunk is matched with a user request, thus
 * we can actually post the receive to receive data in place.
 * This function can also by called during peeking (nm_sr_recv_peek()), so it
 * can be called several times for the same request.
 */
static void nm_mcast_notify_req(struct nm_req_s*p_req, const struct nm_data_s*p_data0, nm_len_t chunk_offset, nm_len_t chunk_len, void*p_ref)
{
  struct nm_mcast_recv_s*p_mcast_recv       = p_ref;
  struct nm_mcast_service_s*p_mcast_service = p_mcast_recv->p_mcast_service;
  NM_TRACEF("[%d] req = %p; chunk_offset = %ld; chunk_len = %ld (data size = %ld)\n",
                p_mcast_service->my_rank, p_req, chunk_offset, chunk_len, nm_data_size(p_data0));
  p_mcast_recv->p_req = p_req;
  struct nm_data_s data;
  const struct nm_data_s*p_data = NULL;

  if((chunk_offset > 0) || (chunk_len < nm_data_size(p_data0)))
    {
      /* adjust data to the actual chunk length & offset */
      nm_data_excerpt_build(&data, (struct nm_data_s*)p_data0, chunk_offset, chunk_len);
      p_data = &data;
    }
  else
    {
      p_data = p_data0;
    }

  if(chunk_offset + chunk_len <= p_mcast_recv->header.hlen)
    {
      /* receive the user header (specified by nm_rpc_req_set_hlen(), different
       * from nm_rpc_req_pack_header() !). Data is available immediately. */
      NM_TRACEF("[%d] receiving as header\n", p_mcast_service->my_rank);
      nm_rpc_recv_header_data(p_mcast_recv->rpc_token, (struct nm_data_s*) p_data);
    }
  else
    {
      /* receive data body. Data will be available after the finalizer has been called. */
      NM_TRACEF("[%d] receiving as body\n", p_mcast_service->my_rank);
      nm_rpc_irecv_body_data(p_mcast_recv->rpc_token, (struct nm_data_s*) p_data);
    }

  // Save all received chunks to an nm_datav to then have an nm_data to forward:
  nm_datav_add_chunk_data(&p_mcast_recv->datav, p_data);

  if(chunk_offset + chunk_len >= p_mcast_recv->header.len)
    {
      /* notify that all receive requests have been posted for this RPC incoming request.
      * (needed because we called nm_rpc_token_delay()) */
      NM_TRACEF("[%d] signaling token complete\n", p_mcast_service->my_rank);
      nm_data_datav_build(&p_mcast_recv->data, &p_mcast_recv->datav);
      nm_rpc_token_complete(p_mcast_recv->rpc_token);
    }
  NM_TRACEF("[%d] done.\n", p_mcast_service->my_rank);
}

/* RPC handler: this function is called once for each RPC message received. */
static void nm_mcast_rpc_handler(nm_rpc_token_t p_rpc_token)
{
  struct nm_mcast_service_s*p_mcast_service = MCAST_SERVICE_FROM_RPC_TOKEN(p_rpc_token);
  struct nm_core*p_core = nm_core_get_singleton();
  /* initialize & fill a mcast packet descriptor */
  struct nm_mcast_recv_s*p_mcast_recv = padico_malloc(sizeof(struct nm_mcast_recv_s));
  nm_rpc_token_set_ref(p_rpc_token, p_mcast_recv);
  p_mcast_recv->header.len = NM_LEN_UNDEFINED;
  p_mcast_recv->rpc_token = p_rpc_token;
  p_mcast_recv->p_req = NULL;
  p_mcast_recv->p_dests = NULL;
  p_mcast_recv->p_prios = NULL;
  p_mcast_recv->p_mcast_service = p_mcast_service;
  nm_datav_init(&p_mcast_recv->datav);

  /* receive mcast header (set by nm_rpc_req_pack_header()) */
  nm_rpc_recv_header(p_mcast_recv->rpc_token, &p_mcast_recv->header, sizeof(p_mcast_recv->header));
  NM_TRACEF("[%d] received header: msg len = %lu; seq = %d; hlen = %ld; from = %d; n_dests = %d; tag = %ld\n",
                p_mcast_service->my_rank, p_mcast_recv->header.len, p_mcast_recv->header.seq, p_mcast_recv->header.hlen,
                p_mcast_recv->header.root_rank, p_mcast_recv->header.n_dests, p_mcast_recv->header.tag);

  if(p_mcast_recv->header.n_dests > 0)
    {
      p_mcast_recv->p_dests = padico_malloc(p_mcast_recv->header.n_dests * sizeof(int));
      nm_rpc_recv_header(p_mcast_recv->rpc_token, p_mcast_recv->p_dests, p_mcast_recv->header.n_dests * sizeof(int));

      p_mcast_recv->p_seqs = padico_malloc(p_mcast_recv->header.n_dests * sizeof(nm_seq_t));
      nm_rpc_recv_header(p_mcast_recv->rpc_token, p_mcast_recv->p_seqs, p_mcast_recv->header.n_dests * sizeof(nm_seq_t));

      if(p_mcast_recv->header.has_prios)
        {
          p_mcast_recv->p_prios = padico_malloc(p_mcast_recv->header.n_dests * sizeof(int));
          nm_rpc_recv_header(p_mcast_recv->rpc_token, p_mcast_recv->p_prios, p_mcast_recv->header.n_dests * sizeof(int));
        }
    }

  /* Signal this RPC handler didn't post any recv yet (and maybe won't
   * post any recv in the handler at all).
   * Warning- it needs to be before nm_core_inject_chunk(), since they
   * may trigger (or not) immediate completion and unlock the token.
   * Since we used nm_rpc_token_delay(), we will have to explicitely
   * call nm_rpc_token_complete() to notify the RPC service that we
   * posted all receives for this token.
   */
  nm_rpc_token_delay(p_mcast_recv->rpc_token);

  nm_gate_t p_gate = nm_comm_get_gate(p_mcast_service->p_comm, p_mcast_recv->header.root_rank);
  nm_core_tag_t core_tag = nm_core_tag_build(p_mcast_recv->header.session_hash, p_mcast_recv->header.tag);
  if(p_mcast_recv->header.hlen > 0)
    {
      /* user asked for a non-empty header; inject the header separately, to make it
      * immediately available, without waiting for the body.
      * Here, is_last_chunk is equivalent to "the whole message contains only a
      * header" */
      const int is_last_chunk = p_mcast_recv->header.hlen == p_mcast_recv->header.len;
      nm_core_inject_chunk(p_core, p_gate, core_tag, p_mcast_recv->header.seq,
                           0 /* chunk_offset */, p_mcast_recv->header.hlen, is_last_chunk,
                           &nm_mcast_notify_req, p_mcast_recv);
    }

  if(p_mcast_recv->header.len > p_mcast_recv->header.hlen)
    {
      /* inject the data body in the core, if present. The injected chunk is
         only a placeholder, data won't be available before the rpc finalizer
         is called */
      nm_core_inject_chunk(p_core, p_gate, core_tag, p_mcast_recv->header.seq,
                           p_mcast_recv->header.hlen, p_mcast_recv->header.len - p_mcast_recv->header.hlen, 1 /* last chunk */,
                           &nm_mcast_notify_req, p_mcast_recv);
    }

  NM_TRACEF("[%d] done.\n", p_mcast_service->my_rank);
}

/* called when we finished receiving data from a broadcast: data is received
 * and forwarded, user can do whatever he wants with it. */
static inline void nm_mcast_recv_finalize(struct nm_mcast_recv_s*p_mcast_recv)
{
  /* notify the core injector that the receive is now finalized */
  struct nm_mcast_service_s*p_mcast_service = p_mcast_recv->p_mcast_service;
  nm_core_inject_finalize(nm_core_get_singleton(), p_mcast_recv->p_req);
  nm_profile_inc(p_mcast_service->profiling.n_mcast_injected);

  nm_datav_destroy(&p_mcast_recv->datav);

  if(p_mcast_recv->p_dests != NULL)
    {
      padico_free(p_mcast_recv->p_dests);
      padico_free(p_mcast_recv->p_seqs);
      if(p_mcast_recv->p_prios != NULL)
        {
          padico_free(p_mcast_recv->p_prios);
        }
    }
  padico_free(p_mcast_recv);
  p_mcast_recv = NULL;
}

/* RPC finalizer: this function is called when the RPC is fully received: ie
 * when data from the multicast is completely received, maybe we will forward
 * to other nodes */
static void nm_mcast_rpc_finalizer(nm_rpc_token_t p_rpc_token)
{
  struct nm_mcast_service_s*p_mcast_service = MCAST_SERVICE_FROM_RPC_TOKEN(p_rpc_token);

  NM_TRACEF("[%d]\n", p_mcast_service->my_rank);

  struct nm_mcast_recv_s*p_mcast_recv = nm_rpc_token_get_ref(p_rpc_token);

  /* notify the core injector that data for this request has arrived, but we
   * still need it for potential forwards (user's receive will have status UNPACK_COMPLETED) */
  nm_core_inject_complete(nm_core_get_singleton(), p_mcast_recv->p_req, /* chunk_offset */0, p_mcast_recv->header.len);

  p_mcast_recv->rpc_token = NULL; // rpc_token will be freed after the end of this function

  if(p_mcast_recv->header.n_dests > 0)
    {
      struct nm_mcast_send_s*p_mcast_send = padico_malloc(sizeof(struct nm_mcast_send_s));
      p_mcast_send->p_dests         = p_mcast_recv->p_dests;
      p_mcast_send->p_seqs          = p_mcast_recv->p_seqs;
      p_mcast_send->p_prios         = p_mcast_recv->p_prios;
      p_mcast_send->n_dests         = p_mcast_recv->header.n_dests;
      p_mcast_send->tag             = p_mcast_recv->header.tag;
      p_mcast_send->p_body_data     = &p_mcast_recv->data;
      p_mcast_send->tree_kind       = p_mcast_recv->header.tree_kind;
      p_mcast_send->session_hash    = p_mcast_recv->header.session_hash;
      p_mcast_send->root_rank       = p_mcast_recv->header.root_rank;
      p_mcast_send->p_user_mcast    = NULL;
      p_mcast_send->p_mcast_recv    = p_mcast_recv;
      p_mcast_send->hlen            = p_mcast_recv->header.hlen;
      p_mcast_send->p_mcast_service = p_mcast_service;
      nm_mcast_send_step(p_mcast_send);
    }
  else
    {
      nm_mcast_recv_finalize(p_mcast_recv);
    }
}

/** called when an RPC send is finished: according to the routing algorithm,
 * maybe we need to perform another RPC send. */
static void nm_mcast_send_monitor(nm_rpc_req_t p_rpc_req, void*p_ref)
{
  struct nm_mcast_send_child_s*p_child      = p_ref;
  struct nm_mcast_service_s*p_mcast_service = p_child->p_mcast_send->p_mcast_service;
#ifdef NMAD_DEBUG
  NM_TRACEF("[%d] RPC send to %d with tag %ld is over\n", p_mcast_service->my_rank, p_child->dest_rank, p_child->p_mcast_send->tag);
#else
  NM_TRACEF("[%d] RPC send with tag %ld is over\n", p_mcast_service->my_rank, p_child->p_mcast_send->tag);
#endif
  const int refcount = nm_atomic_dec(&p_child->p_mcast_send->refcount);
  if(refcount == 0)
    {
      nm_mcast_step_finalize(p_child->p_mcast_send);
    }
  padico_free(p_child->p_dests);
  padico_free(p_child->p_seqs);
  if(p_child->p_prios)
    padico_free(p_child->p_prios);
  padico_free(p_child);
}

/** initializes the nm_mcast_send_child_s structure
 * @param n_dests_arrays is the length of dests, prios, and seqs arrays
 * @param n_dests is the number of dests for this child (is equal to n_dests_arrays when delegate is used, 0 otherwise)
 */
static inline struct nm_mcast_send_child_s* nm_mcast_init_send_child(struct nm_mcast_send_s*p_mcast_send, int n_dests_arrays, int n_dests)
{
  struct nm_mcast_send_child_s*child = padico_malloc(sizeof(struct nm_mcast_send_child_s));
  child->p_dests = padico_malloc(sizeof(int) * n_dests_arrays);
  child->p_seqs  = padico_malloc(sizeof(nm_seq_t) * n_dests_arrays);
  if(p_mcast_send->p_prios != NULL)
    child->p_prios = padico_malloc(sizeof(int) * n_dests_arrays);
  else
    child->p_prios    = NULL;
  child->n_dests      = n_dests;
  child->p_mcast_send = p_mcast_send;
  child->index        = -1;
  nm_atomic_inc(&p_mcast_send->refcount);

  return child;
}

/** counts number of leaves by depth using depth-first algorithm
 * @param p_tree_info the tree to explore
 * @param p_count an array of count of leaf, indexed by depth; assumed to be at least of size p_tree_info->height
 */
static inline void nm_mcast_tree_depth_count(const struct nm_coll_tree_info_s*p_tree_info, int*p_count, int start_step)
{
  int step;
  for(step = start_step; step < p_tree_info->height; step++)
    {
      int parent = 0;
      int n_children = 0;
      int children[p_tree_info->max_arity];
      nm_coll_tree_step(p_tree_info, step, &parent, children, &n_children);
      if(n_children > 0)
        {
          p_count[step] += n_children;
          int c;
          for(c = 0; c < n_children; c++)
            {
              struct nm_coll_tree_info_s tree_info;
              nm_coll_tree_init(&tree_info, p_tree_info->kind, p_tree_info->n, children[c] /* self */, 0 /* root */);
              nm_mcast_tree_depth_count(&tree_info, p_count, step + 1);
            }
        }
    }
}

/** count dests for each step in each child
 */
static int*nm_mcast_tree_steps_count(struct nm_mcast_send_s*p_mcast_send, int n_children, const int*children,
                                     const struct nm_coll_tree_info_s*p_tree_info)
{
  /* ** unroll all tree steps to distribute dests among children */
  int*p_count = padico_malloc(sizeof(int) * (1 + n_children) * p_tree_info->height);
  memset(p_count, 0, sizeof(int) * (1 + n_children) * p_tree_info->height);
  nm_mcast_tree_depth_count(p_tree_info, &p_count[0], 1);
  int c;
  for(c = 0; c < n_children; c++)
    {
      struct nm_coll_tree_info_s tree_info_child;
      nm_coll_tree_init(&tree_info_child, p_tree_info->kind, p_tree_info->n, children[c] /* self */, 0 /* root */);
      nm_mcast_tree_depth_count(&tree_info_child, &p_count[(1 + c) * p_tree_info->height], 1);
    }
  /* ** trim empty steps for incomplete trees */
  for(c = 0; c < n_children + 1; c++)
    {
      int f = 1; /* next to fill */
      int h;
      for(h = 1; h < p_tree_info->height; h++)
        {
          if(p_count[c * p_tree_info->height + h] != 0)
            {
              if(h > f)
                {
                  p_count[c * p_tree_info->height + f] = p_count[c * p_tree_info->height + h];
                  p_count[c * p_tree_info->height + h] = 0;
                }
              f++;
            }
        }
    }
  return p_count;
}

/** build children requests using priorities
 */
static void nm_mcast_build_children_prios(struct nm_mcast_send_s*p_mcast_send, int n_children, const int*children,
                                          const struct nm_coll_tree_info_s*p_tree_info,
                                          struct nm_mcast_send_child_s**p_mcast_children)
{
  int*p_count = nm_mcast_tree_steps_count(p_mcast_send, n_children, children, p_tree_info);
  int current_depth = 0;
  /* direct children */
  int k;
  for(k = 0; k < n_children; k++)
    {
      p_mcast_children[k]->index = k;
    }
  /* ** find a slot for each dest- breadth first traversal to enforce priorities */
  int i; /* next dest in the global vect */
  int c = 0;
  for(i = n_children; i < p_mcast_send->n_dests; i++)
    {
      /* find the next child with non-null count */
      while(p_count[current_depth + c * p_tree_info->height] == 0)
        {
          c++;
          if(c >= n_children + 1)
            {
              c = 0;
              current_depth++;
            }
          assert(c < n_children + 1);
          assert(current_depth < p_tree_info->height);
        }
      /* add one dest to the child */
      assert(p_count[current_depth + c * p_tree_info->height] > 0);
      if(c == 0)
        {
          /* next steps on local node */
          const int n = p_mcast_send->p_next_mcast_send->n_dests;
          p_mcast_send->p_next_mcast_send->p_dests[n] = p_mcast_send->p_dests[i];
          p_mcast_send->p_next_mcast_send->p_seqs[n] = p_mcast_send->p_seqs[i];
          if(p_mcast_send->p_prios != NULL)
            p_mcast_send->p_next_mcast_send->p_prios[n] = p_mcast_send->p_prios[i];
          p_mcast_send->p_next_mcast_send->n_dests++;
        }
      else if(c > 0)
        {
          /* child */
          const int n = p_mcast_children[c - 1]->n_dests;
          p_mcast_children[c - 1]->p_dests[n] = p_mcast_send->p_dests[i];
          p_mcast_children[c - 1]->p_seqs[n]  = p_mcast_send->p_seqs[i];
          if(p_mcast_send->p_prios != NULL)
            p_mcast_children[c - 1]->p_prios[n] = p_mcast_send->p_prios[i];
          p_mcast_children[c - 1]->n_dests++;
        }
      p_count[current_depth + c * p_tree_info->height]--;
    }
  padico_free(p_count);
}

/** build children requests without priorities, with each
 * child as a consecutive block (taking locality into account).
 */
static void nm_mcast_build_children_noprio(struct nm_mcast_send_s*p_mcast_send, int n_children, const int*children,
                                           const struct nm_coll_tree_info_s*p_tree_info,
                                           struct nm_mcast_send_child_s**p_mcast_children)
{
  assert(p_mcast_send->p_prios == NULL);
  int*p_count = nm_mcast_tree_steps_count(p_mcast_send, n_children, children, p_tree_info);
  int flattenned[n_children + 1];
  int c;
  for(c = 0; c < n_children + 1; c++)
    {
      flattenned[c] = 0;
      int k;
      for(k = 0; k < p_tree_info->height; k++)
        {
          flattenned[c] += p_count[k + c * p_tree_info->height];
        }
    }
  /* fill-in childs of local node */
  int j;
  for(j = 0; j < flattenned[0]; j++)
    {
      assert(p_mcast_send->p_next_mcast_send != NULL);
      p_mcast_send->p_next_mcast_send->p_dests[j] = p_mcast_send->p_dests[j];
      p_mcast_send->p_next_mcast_send->p_seqs[j] = p_mcast_send->p_seqs[j];
      p_mcast_send->p_next_mcast_send->n_dests++;
    }
  /* fill-in other children in sequence */
  int i = flattenned[0]; /* next dest in global vect */
  for(c = 0; c < n_children; c++)
    {
      p_mcast_children[c]->index = i;
      i++;
      for(j = 0; j < flattenned[c + 1]; j++)
        {
          p_mcast_children[c]->p_dests[j] = p_mcast_send->p_dests[i];
          p_mcast_children[c]->p_seqs[j]  = p_mcast_send->p_seqs[i];
          p_mcast_children[c]->n_dests++;
          i++;
        }
    }
  padico_free(p_count);
}

/* builds and sends one request to a child */
static inline void nm_mcast_send_children(struct nm_mcast_send_child_s*p_child, const int n)
{
  assert(n >= 0);
  struct nm_mcast_send_s* p_mcast_send = p_child->p_mcast_send;
  struct nm_mcast_service_s* p_mcast_service = p_mcast_send->p_mcast_service;
  const nm_session_t p_session = nm_session_lookup(p_mcast_send->session_hash);
  const nm_comm_t p_comm = nm_comm_get_by_session(p_session);
  const nm_gate_t p_gate = nm_comm_get_gate(p_comm, p_mcast_send->p_dests[n]);
  assert(p_gate != NULL);
#ifdef NMAD_DEBUG
  p_child->dest_rank           = p_mcast_send->p_dests[n];
#endif
  p_child->header.hlen         = p_mcast_send->hlen;
  p_child->header.len          = nm_data_size(p_mcast_send->p_body_data);
  p_child->header.tag          = p_mcast_send->tag;
  p_child->header.seq          = p_mcast_send->p_seqs[n];
  p_child->header.session_hash = p_session->hash_code;
  p_child->header.root_rank    = p_mcast_send->root_rank;
  p_child->header.n_dests      = p_child->n_dests;
  p_child->header.has_prios    = (p_mcast_send->p_prios != NULL);
  p_child->header.tree_kind    = p_mcast_send->tree_kind;

  nm_rpc_req_t rpc_req = nm_rpc_req_init(p_mcast_service->p_service, p_gate, mcast_tag);
  nm_rpc_req_pack_header(rpc_req, &p_child->header, sizeof(struct nm_mcast_header_s));
  nm_rpc_req_set_hlen(rpc_req, p_mcast_send->hlen);
  if(p_child->n_dests > 0)
    {
      nm_rpc_req_pack_header(rpc_req, p_child->p_dests, p_child->n_dests * sizeof(int));
      nm_rpc_req_pack_header(rpc_req, p_child->p_seqs, p_child->n_dests * sizeof(nm_seq_t));
      if(p_child->header.has_prios)
        {
          nm_rpc_req_pack_header(rpc_req, p_child->p_prios, p_child->n_dests * sizeof(int));
        }
    }
  /* We pack only the user data as a body, to allow the user to peek on this data during the recv */
  nm_rpc_req_pack_body_data(rpc_req, p_mcast_send->p_body_data);
  if(p_mcast_send->p_prios != NULL)
    {
      nm_rpc_req_set_priority(rpc_req, p_mcast_send->p_prios[n]);
      NM_TRACEF("[%d] sending to %d with priority %d\n", p_mcast_service->my_rank, p_mcast_send->p_dests[n], p_mcast_send->p_prios[n]);
    }
  else
    {
      NM_TRACEF("[%d] sending to %d without priority\n", p_mcast_service->my_rank, p_mcast_send->p_dests[n]);
    }
  nm_rpc_req_set_notifier(rpc_req, &nm_mcast_send_monitor, p_child);
  nm_rpc_req_isend(rpc_req);
  nm_profile_inc(p_mcast_service->profiling.n_send_packets);
}

/** send a step of the given mcast
 * this function takes ownership of the p_mcast_send object.
 * the caller is responsible for destroying p_mcast_send->p_dests|p_seqs|p_prios */
static void nm_mcast_send_step(struct nm_mcast_send_s*p_mcast_send)
{
  struct nm_mcast_service_s*p_mcast_service = p_mcast_send->p_mcast_service;
  NM_TRACEF("[%d] starting send step\n", p_mcast_service->my_rank);
  p_mcast_send->refcount = 0;
#ifdef NMAD_DEBUG
  if(p_mcast_send->p_prios != NULL && p_mcast_service->reorder_tree)
    {
      int i;
      for(i = 0; i < p_mcast_send->n_dests - 1; i++)
        {
          assert(p_mcast_send->p_prios[i] >= p_mcast_send->p_prios[i + 1]);
        }
    }
#endif /* NMAD_DEBUG */

  assert(p_mcast_send->tree_kind != NM_COLL_TREE_NONE);

  if(p_mcast_service->delegate && p_mcast_send->root_rank == p_mcast_service->my_rank)
    {
      // just send to the first recipient, a let it manage the mcast:

      /* ** init child */
      struct nm_mcast_send_child_s*p_child = nm_mcast_init_send_child(p_mcast_send, p_mcast_send->n_dests-1, p_mcast_send->n_dests-1);

      memcpy(p_child->p_dests, p_mcast_send->p_dests+1, sizeof(int) * p_child->n_dests);
      memcpy(p_child->p_seqs, p_mcast_send->p_seqs+1, sizeof(int) * p_child->n_dests);
      if(p_mcast_send->p_prios != NULL)
        memcpy(p_child->p_prios, p_mcast_send->p_prios+1, sizeof(int) * p_child->n_dests);

      /* ** there is no local next step */
      p_mcast_send->p_next_mcast_send = NULL;

      /* ** send to the child (the delegate) */
      nm_mcast_send_children(p_child, /* the first recipient */ 0);

      /* make sure these fields are not reused beyond this point */
      p_mcast_send->p_dests = NULL;
      p_mcast_send->p_seqs = NULL;
      p_mcast_send->p_prios = NULL;

      return;
    }

  struct nm_coll_tree_info_s tree_info;
  nm_coll_tree_init(&tree_info, p_mcast_send->tree_kind, p_mcast_send->n_dests + 1, 0 /* self */, 0 /* root */);
  int parent = 0;
  int n_children = 0;
  int children[tree_info.max_arity];
  nm_coll_tree_step(&tree_info, 0 /* step */, &parent, children, &n_children);
  /* ** init children */
  struct nm_mcast_send_child_s*p_mcast_children[n_children];
  int c;
  for(c = 0; c < n_children; c++)
    {
      p_mcast_children[c] = nm_mcast_init_send_child(p_mcast_send, p_mcast_send->n_dests, 0);
    }
  /* ** build request for local next step */
  if(tree_info.height <= 1)
    {
      p_mcast_send->p_next_mcast_send = NULL;
    }
  else
    {
      p_mcast_send->p_next_mcast_send = padico_malloc(sizeof(struct nm_mcast_send_s));
      *p_mcast_send->p_next_mcast_send = *p_mcast_send;
      p_mcast_send->p_next_mcast_send->refcount = 0;
      p_mcast_send->p_next_mcast_send->p_dests = padico_malloc(sizeof(int) * p_mcast_send->n_dests);
      p_mcast_send->p_next_mcast_send->p_seqs  = padico_malloc(sizeof(nm_seq_t) * p_mcast_send->n_dests);
      if(p_mcast_send->p_prios != NULL)
        p_mcast_send->p_next_mcast_send->p_prios = padico_malloc(sizeof(int) * p_mcast_send->n_dests);
      else
        p_mcast_send->p_next_mcast_send->p_prios = NULL;
      p_mcast_send->p_next_mcast_send->n_dests = 0;
    }

  if(p_mcast_send->p_prios != NULL)
    nm_mcast_build_children_prios(p_mcast_send, n_children, children, &tree_info, p_mcast_children);
  else
    nm_mcast_build_children_noprio(p_mcast_send, n_children, children, &tree_info, p_mcast_children);

  /* ** send to all children */
  for(c = 0; c < n_children; c++)
    {
      nm_mcast_send_children(p_mcast_children[c], p_mcast_children[c]->index);
    }
  /* make sure these fields are not reused beyond this point */
  p_mcast_send->p_dests = NULL;
  p_mcast_send->p_seqs = NULL;
  p_mcast_send->p_prios = NULL;
  if(n_children == 0)
    {
      nm_mcast_step_finalize(p_mcast_send);
    }
}

/** finalize a send step after all children have been sent */
static void nm_mcast_step_finalize(struct nm_mcast_send_s*p_mcast_send)
{
  struct nm_mcast_service_s*p_mcast_service = p_mcast_send->p_mcast_service;
  struct nm_mcast_send_s*p_next_mcast_send  = p_mcast_send->p_next_mcast_send;
  assert(p_mcast_send->refcount == 0);
  if(p_next_mcast_send == NULL)
    {
      /* last step */
      if(p_mcast_send->p_mcast_recv != NULL)
        {
          /* last child of a forward request -> finalize RPC receive */
          assert(p_mcast_send->root_rank != p_mcast_service->my_rank);
          nm_mcast_recv_finalize(p_mcast_send->p_mcast_recv);
        }
      else
        {
          /* last child in the root (but maybe there is a second sub-tree) -> notify completion */
          assert(p_mcast_send->root_rank == p_mcast_service->my_rank);
          if(p_mcast_send->second_n_dests > 0)
            {
              /* This is a bitree, we finished the first tree, let's run the second tree.
               *
               * TODO: refactorize this block with the content of
               * nm_mcast_isend() or nm_mcast_send_step() */
              p_next_mcast_send                 = padico_malloc(sizeof(struct nm_mcast_send_s));
              *p_next_mcast_send                = *p_mcast_send;
              p_next_mcast_send->p_dests        = p_mcast_send->second_p_dests;
              p_next_mcast_send->p_prios        = p_mcast_send->second_p_prios;
              p_next_mcast_send->tree_kind      = p_mcast_service->bitree_second_kind;
              p_next_mcast_send->p_seqs         = p_mcast_send->second_p_seqs;
              p_next_mcast_send->n_dests        = p_mcast_send->second_n_dests;
              p_next_mcast_send->second_n_dests = 0;
              p_next_mcast_send->second_p_dests = NULL;
              p_next_mcast_send->second_p_seqs  = NULL;
              p_next_mcast_send->second_p_prios = NULL;
            }
          else if(p_mcast_send->p_user_mcast != NULL)
            {
              assert(p_mcast_send->p_user_mcast->initialized == MCAST_INIT_CANARY);
              if(p_mcast_send->p_user_mcast->p_notifier == NULL)
                {
                  nm_cond_signal(&p_mcast_send->p_user_mcast->cond_mcast_ended, 1);
                }
              else
                {
                  (*p_mcast_send->p_user_mcast->p_notifier)(p_mcast_send->p_user_mcast->p_notifier_ref);
                }

              /* We can do nothing about p_user_mcast after this, because user
               * may have destroyed it in the above callback or after the
               * cond_signal. */
              p_mcast_send->p_user_mcast = NULL;
            }
        }
    }
  /* free request */
  padico_free(p_mcast_send); /* other fields are freed by caller */
  if(p_next_mcast_send != NULL)
    {
      /* keep pointers to memory to be freed, since p_next_mcast_send
       * may vanish in the call to nm_mcast_send_step() */
      int*p_dests = p_next_mcast_send->p_dests;
      nm_seq_t*p_seqs = p_next_mcast_send->p_seqs;
      int*p_prios = p_next_mcast_send->p_prios;
      /* schedule next step */
      nm_mcast_send_step(p_next_mcast_send);
      padico_free(p_dests);
      padico_free(p_seqs);
      if(p_prios)
        padico_free(p_prios);
    }
}

/* ********************************************************* */

/** initialize the mcast interface */
nm_mcast_service_t nm_mcast_init(nm_comm_t p_comm)
{
  struct nm_mcast_service_s* nm_mcast = padico_malloc(sizeof(struct nm_mcast_service_s));
  nm_mcast->p_comm = nm_comm_dup(p_comm);
  nm_mcast->p_service = nm_rpc_register(nm_comm_get_session(nm_mcast->p_comm), mcast_tag, NM_TAG_MASK_FULL,
                                    &nm_mcast_rpc_handler, &nm_mcast_rpc_finalizer, nm_mcast);
  nm_mcast->my_rank = nm_comm_rank(nm_mcast->p_comm);
  nm_mcast->default_tree_kind = NM_COLL_TREE_DEFAULT;
  nm_mcast->use_bitree = 0;
  nm_mcast->bitree_first_kind = NM_COLL_TREE_NONE;
  nm_mcast->bitree_second_kind = NM_COLL_TREE_NONE;
  nm_mcast->bitree_thresold = -1;
  nm_mcast->delegate = 0;
  nm_mcast->reorder_tree = padico_module_attr_reorder_tree_getvalue();

  const char*s_mcast_tree = padico_module_attr_tree_getvalue();
  if(s_mcast_tree)
    {
      if(strcmp(s_mcast_tree, "bitree") == 0)
        {
          nm_mcast->default_tree_kind = NM_COLL_TREE_NONE;
          nm_mcast->use_bitree = 1;
          const char*s_first_bitree = getenv("NMAD_MCAST_BITREE_FIRST");
          const char*s_second_bitree = getenv("NMAD_MCAST_BITREE_SECOND");
          const char*s_thresold = getenv("NMAD_MCAST_BITREE_THRESOLD");
          if(s_first_bitree && s_second_bitree && s_thresold)
            {
              nm_mcast->bitree_first_kind = nm_coll_tree_kind_by_name(s_first_bitree);
              nm_mcast->bitree_second_kind = nm_coll_tree_kind_by_name(s_second_bitree);
              nm_mcast->bitree_thresold = atoi(s_thresold);
            }
          else
            {
              NM_FATAL("You have to provide NMAD_MCAST_BITREE_FIRST, NMAD_MCAST_BITREE_SECOND and NMAD_MCAST_BITREE_THRESOLD env vars when using bitree tree kind.\n");
            }
        }
      else
        {
          nm_mcast->default_tree_kind = nm_coll_tree_kind_by_name(s_mcast_tree);
          NM_DISPF("tree kind '%s' forced by environment.\n", s_mcast_tree);
        }
    }

  if(padico_module_attr_delegate_getvalue())
    {
      if(nm_mcast->use_bitree)
        {
          NM_FATAL("mcast: can't delegate and use bitrees at the same time (not implemented yet)\n");
        }
      nm_mcast->delegate = 1;
    }
#ifdef NMAD_PROFILE
  const char*comm_name = nm_session_get_name(nm_comm_get_session(nm_mcast->p_comm));
  puk_profile_var_defx(unsigned_long_long, counter, &nm_mcast->profiling.n_mcast_send, 0,
                       "nm_mcast", "total number of mcast sent",
                       "nm_mcast.%s.n_mcast_send", comm_name);
  puk_profile_var_defx(unsigned_long_long, counter, &nm_mcast->profiling.n_send_packets, 0,
                       "nm_mcast","total number of packets sent by mcast",
                       "nm_mcast.%s.n_send_packets", comm_name);
  puk_profile_var_defx(unsigned_long_long, counter, &nm_mcast->profiling.n_dests, 0,
                       "nm_mcast", "total number of destinations",
                       "nm_mcast.%s.n_dest", comm_name);
  puk_profile_var_defx(unsigned_long_long, counter, &nm_mcast->profiling.n_mcast_injected, 0,
                       "nm_mcast", "number of mcast injected locally",
                       "nm_mcast.%s.n_mcast_injected", comm_name);
  puk_profile_var_defx(double, timer, &nm_mcast->profiling.total_send_time, 0.0,
                       "nm_mcast", "total time to send mcast",
                       "nm_mcast.%s.total_send_time", comm_name);
  puk_profile_var_defx(double, timer, &nm_mcast->profiling.total_inject_time, 0.0,
                       "nm_mcast", "total time to inject packets",
                       "nm_mcast.%s.total_inject_time", comm_name);
#endif /* NMAD_PROFILE */

  return nm_mcast;
}

/** destroys mcast interface */
void nm_mcast_finalize(nm_mcast_service_t p_mcast_service)
{
  nm_rpc_unregister(p_mcast_service->p_service);
  nm_comm_destroy(p_mcast_service->p_comm);
  padico_free(p_mcast_service);
}

void nm_mcast_send_init(struct nm_mcast_service_s*p_mcast_service, nm_mcast_t*p_mcast)
{
  if(p_mcast_service == NULL)
    {
      NM_FATAL("mcast service not initialized.\n");
    }
  assert(p_mcast != NULL);

  memset(p_mcast, 0, sizeof(nm_mcast_t));

  nm_cond_init(&p_mcast->cond_mcast_ended, 0);
  p_mcast->initialized     = MCAST_INIT_CANARY;
  p_mcast->p_mcast_service = p_mcast_service;
}

void nm_mcast_send_set_notifier(nm_mcast_t*p_mcast, nm_mcast_send_notifier_t p_notifier, void*p_ref)
{
  assert(p_mcast != NULL);
  assert(p_mcast->initialized == MCAST_INIT_CANARY);
  p_mcast->p_notifier = p_notifier;
  p_mcast->p_notifier_ref = p_ref;
}

/** send an mcast message on the given communicator, to the list of given ranks.
 * @note only comm/ranks addressing is available instead of gates, since addresses
 * need to be serializable on the network.
 * @param hlen header length, i.e. part of length of p_data that needs to be
 * sent eagerly so as to be immediately available in the receiver handler. Set
 * to 0 if not needed.
 */
void nm_mcast_isend(nm_mcast_t*p_mcast, nm_comm_t p_comm, int*p_dests, int*p_prios,
                    int n_dests, nm_tag_t tag, struct nm_data_s*p_data, nm_len_t hlen, nm_coll_tree_kind_t tree_kind)
{
  assert(p_mcast != NULL);
  assert(p_mcast->initialized == MCAST_INIT_CANARY);
  struct nm_mcast_service_s* p_mcast_service = p_mcast->p_mcast_service;
  nm_profile_inc(p_mcast_service->profiling.n_mcast_send);
  if(tree_kind == NM_COLL_TREE_DEFAULT)
    {
      if(p_mcast_service->default_tree_kind == NM_COLL_TREE_DEFAULT)
        tree_kind = nm_coll_tree_heuristic(n_dests, nm_data_size(p_data));
      else
        tree_kind = p_mcast_service->default_tree_kind;
    }

  struct nm_core*p_core = nm_core_get_singleton();
  if(p_prios != NULL)
    {
      /** We reorder node list in order to nodes with higher priority will
       * receive their data first.
       * The only requirement is that array `prios` has to be sorted in
       * descending order (and array `nodes` ordered such as prios[i] is the
       * priority of the send to the node nodes[i])
       * In `p_ordered_dests`, nodes of a subtree are contiguous in the array (to
       * easily send this subtree) */
      int*p_ordered_dests = padico_malloc(n_dests * sizeof(int));
      int*p_ordered_prios = padico_malloc(n_dests * sizeof(int));
      if(p_mcast_service->reorder_tree)
        {
          nm_mcast_generic_reorder_nodes(p_dests, p_prios, p_ordered_dests, p_ordered_prios, n_dests);
        }
      else
        {
          /* Copy content of p_dests and p_prios instead of only changing the
           * pointer value, since we have the ownership of
           * p_ordered_{dests,prios} and user keeps ownership of
           * p_{dests,prios}. */
          memcpy(p_ordered_dests, p_dests, n_dests * sizeof(int));
          memcpy(p_ordered_prios, p_prios, n_dests * sizeof(int));
        }
      p_dests = p_ordered_dests;
      p_prios = p_ordered_prios;
      const struct nm_strategy_iface_s*strategy_iface = p_core->strategy_iface;
      if(!strategy_iface->capabilities.supports_priorities)
        {
          static int warning_done = 0;
          if(!warning_done)
            {
              NM_WARN("trying to attach priorities to mcast with a strategy that does not support priorities.\n");
              warning_done = 1;
            }
        }
    }

  /* Sequence numbers have to be generated by the root of the multicast */
  nm_seq_t*p_seqs = padico_malloc(n_dests * sizeof(nm_seq_t));
  const nm_session_hash_t session_hash = nm_comm_get_session(p_comm)->hash_code;
  int i;
  for(i = 0; i < n_dests; i++)
    {
      const nm_core_tag_t core_tag = nm_core_tag_build(session_hash, tag);
      const nm_gate_t p_gate = nm_comm_get_gate(p_comm, p_dests[i]);
      assert(p_gate != NULL);
      p_seqs[i] = nm_core_send_seq_get(p_core, p_gate, core_tag);
      nm_profile_inc(p_mcast_service->profiling.n_dests);
    }
  /* build the first step then send */
  struct nm_mcast_send_s*p_mcast_send = padico_malloc(sizeof(struct nm_mcast_send_s));
  p_mcast_send->p_dests         = p_dests;
  p_mcast_send->p_prios         = p_prios;
  p_mcast_send->tag             = tag;
  p_mcast_send->p_body_data     = p_data;
  p_mcast_send->session_hash    = session_hash;
  p_mcast_send->root_rank       = p_mcast_service->my_rank;
  p_mcast_send->p_user_mcast    = p_mcast;
  p_mcast_send->p_mcast_recv    = NULL;
  p_mcast_send->hlen            = hlen;
  p_mcast_send->p_seqs          = p_seqs;
  p_mcast_send->p_mcast_service = p_mcast_service;
  if(p_mcast_service->use_bitree && n_dests > p_mcast_service->bitree_thresold)
    {
      assert(p_mcast_service->delegate == 0);
      p_mcast_send->tree_kind      = p_mcast_service->bitree_first_kind;
      p_mcast_send->n_dests        = p_mcast_service->bitree_thresold;
      p_mcast_send->second_n_dests = n_dests - p_mcast_service->bitree_thresold;
      p_mcast_send->second_p_dests = padico_malloc(p_mcast_send->second_n_dests * sizeof(int));
      p_mcast_send->second_p_seqs  = padico_malloc(p_mcast_send->second_n_dests * sizeof(int));
      memcpy(p_mcast_send->second_p_dests, p_mcast_send->p_dests + p_mcast_send->n_dests, p_mcast_send->second_n_dests * sizeof(int));
      memcpy(p_mcast_send->second_p_seqs, p_mcast_send->p_seqs + p_mcast_send->n_dests, p_mcast_send->second_n_dests * sizeof(int));
      if(p_prios != NULL)
        {
          p_mcast_send->second_p_prios = padico_malloc(p_mcast_send->second_n_dests * sizeof(int));
          memcpy(p_mcast_send->second_p_prios, p_mcast_send->p_prios + p_mcast_send->n_dests, p_mcast_send->second_n_dests * sizeof(int));
        }
      else
        {
          p_mcast_send->second_p_prios = NULL;
        }
    }
  else
    {
      p_mcast_send->tree_kind      = p_mcast_service->use_bitree ? p_mcast_service->bitree_first_kind : tree_kind;
      p_mcast_send->n_dests        = n_dests;
      p_mcast_send->second_n_dests = 0;
      p_mcast_send->second_p_dests = NULL;
      p_mcast_send->second_p_seqs  = NULL;
      p_mcast_send->second_p_prios = NULL;
    }
  nm_mcast_send_step(p_mcast_send);
  padico_free(p_seqs);
  if(p_prios != NULL)
    {
      padico_free(p_dests);
      padico_free(p_prios);
    }
}

void nm_mcast_send(struct nm_mcast_service_s*p_mcast_service, nm_comm_t p_recv_comm, int*p_dests, int*p_prios, int n_dests,
                   nm_tag_t tag, struct nm_data_s*p_data, nm_len_t hlen, nm_coll_tree_kind_t tree_kind)
{
  nm_mcast_t this_mcast;
  nm_mcast_send_init(p_mcast_service, &this_mcast);
  nm_mcast_isend(&this_mcast, p_recv_comm, p_dests, p_prios, n_dests, tag, p_data, hlen, tree_kind);
  nm_mcast_wait(&this_mcast);
  nm_mcast_send_destroy(&this_mcast);
}

void nm_mcast_wait(nm_mcast_t*p_mcast)
{
  assert(p_mcast != NULL);
  assert(p_mcast->initialized == MCAST_INIT_CANARY);

  if(p_mcast->p_notifier != NULL)
    {
      // To be consistent with the behavior of the sendrecv interface:
      NM_FATAL("Cannot wait on this mcast: a notifier is defined.\n");
    }
  nm_cond_wait(&p_mcast->cond_mcast_ended, 1, nm_core_get_singleton());
}

void nm_mcast_send_destroy(nm_mcast_t*p_mcast)
{
  assert(p_mcast != NULL);
  assert(p_mcast->initialized == MCAST_INIT_CANARY);
  nm_cond_destroy(&p_mcast->cond_mcast_ended);
  p_mcast->initialized = -1;
}

void nm_mcast_set_default_tree_kind(struct nm_mcast_service_s*const p_mcast_service, const nm_coll_tree_kind_t kind)
{
  p_mcast_service->default_tree_kind = kind;
}

#define _GNU_SOURCE
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <errno.h>
#include <sys/uio.h>

#define SHM_NAME "/nmadshm"

#define BUFSIZE (8 * 1024 * 1024)

#define ROUNDTRIPS 1000

struct buffer_s
{
  volatile int busy;
  volatile int clear;
  char buf[BUFSIZE];
  int rank;
};

struct seg_s
{
  struct buffer_s buffers[2];
};

struct buffer_s*self = NULL;
struct seg_s*seg = NULL;

static inline void my_shm_send(int peer, const char*ptr, int size)
{
  while(!seg->buffers[peer].clear)
    ;
  if(size > 0)
    memcpy(seg->buffers[peer].buf, ptr, size);
  seg->buffers[peer].busy = 1;
}

static inline void my_shm_recv(char*ptr, int size)
{
  while(!self->busy)
    ;
  if(size > 0)
    memcpy(ptr, self->buf, size);
  self->busy = 0;
  self->clear = 1;
}

int main(int argc, char**argv)
{
  int rank = -1;

  /* ******************************************************* */
  /* ** shm */

  int fd = shm_open(SHM_NAME, O_RDWR|O_CREAT|O_EXCL, 00700);
  if(fd == -1)
    {
      fd = shm_open(SHM_NAME, O_RDWR, 00700);
      rank = 0;
    }
  else
    {
      ftruncate(fd, sizeof(struct seg_s));
      rank = 1;
    }
  seg = mmap(NULL, sizeof(struct seg_s), PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
  const int peer = 1 - rank;

  fprintf(stderr, "init- rank = %d\n", rank);

  self = &seg->buffers[rank];

  self->rank  = rank;
  self->busy  = 0;
  self->clear = 1;

  while(!seg->buffers[peer].clear)
    ;

  fprintf(stderr, "rank = %d; connected to %d\n", rank, seg->buffers[peer].rank);

  char*data = malloc(BUFSIZE);
  struct timespec t1, t2;
  int i;
  clock_gettime(CLOCK_REALTIME, &t1);
  for(i = 0; i < ROUNDTRIPS; i++)
    {
      if(rank == 0)
        {
          my_shm_send(peer, data, 0);
          my_shm_recv(data, 0);
        }
      else
        {
          my_shm_recv(data, 0);
          my_shm_send(peer, data, 0);
        }
    }
  clock_gettime(CLOCK_REALTIME, &t2);
  {
    double sec = t2.tv_sec - t1.tv_sec;
    double nanos = t2.tv_nsec - t1.tv_nsec;
    double t = sec * 1000000000 + nanos;
    fprintf(stderr, "t = %f nanos.\n", t / 2 / ROUNDTRIPS);
  }

  fprintf(stderr, "# size\t nanos. \t bw MB/s\n");
  int size;
  for(size = 1; size <= BUFSIZE; size *= 2)
    {
      clock_gettime(CLOCK_REALTIME, &t1);
      for(i = 0; i < ROUNDTRIPS; i++)
        {
          if(rank == 0)
            {
              my_shm_send(peer, data, size);
              my_shm_recv(data, size);
            }
          else
            {
              my_shm_recv(data, size);
              my_shm_send(peer, data, size);
            }
        }
      clock_gettime(CLOCK_REALTIME, &t2);

      double t =  ((t2.tv_sec - t1.tv_sec) * 1000000000.0 + (t2.tv_nsec - t1.tv_nsec)) / 2 / ROUNDTRIPS;
      double bw = 1000.0 * size / t;

      fprintf(stderr, "%d \t %f \t %f\n", size, t, bw);
    }


  /* ******************************************************* */
  /* ** UNIX socket */

  int sock = socket(AF_UNIX, SOCK_STREAM, 0);
  struct sockaddr_un sun;
  sun.sun_family = AF_UNIX;
  strcpy(sun.sun_path, "/tmp/nmad_shm");
  if(rank == 0)
    {
      unlink(sun.sun_path);
      int rc = bind(sock, (struct sockaddr*)&sun, sizeof(sun));
      if(rc != 0)
        {
          fprintf(stderr, "bind error- %s\n", strerror(errno));
          abort();
        }
      rc = listen(sock, 1);
      if(rc != 0)
        {
          fprintf(stderr, "listen error- %s\n", strerror(errno));
          abort();
        }
      struct sockaddr_un sun2;
      socklen_t s2 = sizeof(sun2);
      sun2.sun_family = AF_UNIX;
      sun2.sun_path[0] = 0;
      int c = accept(sock, (struct sockaddr*)&sun2, &s2);
      fprintf(stderr, "client connected\n");
      close(sock);
      sock = c;
    }
  else
    {
      int rc = connect(sock, (struct sockaddr*)&sun, sizeof(sun));
      while(rc != 0)
        {
          fprintf(stderr, "not connected\n");
          sleep(1);
          rc = connect(sock, (struct sockaddr*)&sun, sizeof(sun));
        }
      fprintf(stderr, "connected\n");
    }

  fprintf(stderr, "# size\t nanos. \t bw MB/s\n");
  for(size = 1; size <= BUFSIZE; size *= 2)
    {
      clock_gettime(CLOCK_REALTIME, &t1);
      for(i = 0; i < ROUNDTRIPS; i++)
        {
          if(rank == 0)
            {
              int done = 0;
              while(done < size)
                {
                  done += send(sock, data + done, size - done, 0);
                }
              recv(sock, data, size, MSG_WAITALL);
            }
          else
            {
              recv(sock, data, size, MSG_WAITALL);
              int done = 0;
              while(done < size)
                {
                  done += send(sock, data + done, size - done, 0);
                }
            }
        }
      clock_gettime(CLOCK_REALTIME, &t2);

      double t =  ((t2.tv_sec - t1.tv_sec) * 1000000000.0 + (t2.tv_nsec - t1.tv_nsec)) / 2 / ROUNDTRIPS;
      double bw = 1000.0 * size / t;

      fprintf(stderr, "%d \t  %f \t %f\n", size, t, bw);
    }

  /* ******************************************************* */
  /* ** FIFO with vmsplice */

  mkfifo("/tmp/nmad_fifo1", 0600);
  mkfifo("/tmp/nmad_fifo2", 0600);
  int fifo1 = open("/tmp/nmad_fifo1", O_RDWR);
  int fifo2 = open("/tmp/nmad_fifo2", O_RDWR);
  fprintf(stderr, "# fifo = %d / %d\n", fifo1, fifo2);
  fprintf(stderr, "# size\t nanos. \t bw MB/s\n");
  for(size = 1; size <= BUFSIZE; size *= 2)
    {
      clock_gettime(CLOCK_REALTIME, &t1);
      for(i = 0; i < ROUNDTRIPS; i++)
        {
          if(rank == 0)
            {
              int done = 0;
              while(done < size)
                {
                  /* done += write(fifo1, data + done, size - done); */
                  struct iovec v = { .iov_base = data + done, .iov_len = size - done };
                  done += vmsplice(fifo1, &v, 1, 0);
                }
              done = 0;
              while(done < size)
                done += read(fifo2, data + done, size - done);
            }
          else
            {
              int done = 0;
              while(done < size)
                done += read(fifo1, data + done, size - done);
              done = 0;
              while(done < size)
                {
                  /*done += write(fifo2, data + done, size - done); */
                  struct iovec v = { .iov_base = data + done, .iov_len = size - done };
                  done += vmsplice(fifo2, &v, 1, 0);
                }
            }
        }
      clock_gettime(CLOCK_REALTIME, &t2);

      double t =  ((t2.tv_sec - t1.tv_sec) * 1000000000.0 + (t2.tv_nsec - t1.tv_nsec)) / 2 / ROUNDTRIPS;
      double bw = 1000.0 * size / t;

      fprintf(stderr, "%d \t  %f \t %f\n", size, t, bw);
    }

  /* ******************************************************* */

  sleep(1);
  /* send FD */

  struct msghdr msg = {0};
  struct cmsghdr *cmsg;
  char buf[CMSG_SPACE(sizeof(int))];

  msg.msg_control = buf;
  msg.msg_controllen = sizeof buf;
  cmsg = CMSG_FIRSTHDR(&msg);
  cmsg->cmsg_level = SOL_SOCKET;
  cmsg->cmsg_type  = SCM_RIGHTS;
  cmsg->cmsg_len   = CMSG_LEN(sizeof(int));
  msg.msg_controllen = cmsg->cmsg_len;

  if(rank == 0)
    {
      int*fdptr = (int *) CMSG_DATA(cmsg);
      *fdptr = fd;
      fprintf(stderr, "sending fd = %d\n", fd);
      errno = 0;
      int rc = sendmsg(sock, &msg, 0);
      fprintf(stderr, "rc = %d %s\n", rc, strerror(errno));
    }
  else
    {
      fprintf(stderr, "receving fd\n");
      errno = 0;
      int*fdptr = (int *) CMSG_DATA(cmsg);
      *fdptr = -1;
      int rc = recvmsg(sock, &msg, MSG_WAITALL);
      int err = errno;
      fprintf(stderr, "rc = %d %d %s\n", rc, err, strerror(err));
      fd = *fdptr;
      fprintf(stderr, "received fd = %d\n", fd);
    }
  sleep(3);
  close(fd);
  close(sock);
  shm_unlink(SHM_NAME);

  return 0;
}

/*
 * Pioman: a Generic I/O Manager
 * Copyright (C) 2001-2023 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include "piom_private.h"

#include <Padico/Module.h>

PADICO_MODULE_HOOK(pioman);

#if defined(PIOMAN_TOPOLOGY_HWLOC)

static struct
{
  /** node topology */
  hwloc_topology_t topology;
  /** thread-local location information  */
  pthread_key_t threadinfo_key;
} piom_topo_hwloc;

/** thread-specific information */
struct piom_ltask_threadinfo_s
{
  pthread_t tid;              /**< thread this info is about */
  hwloc_cpuset_t cpuset;      /**< last cpuset location for thread  */
  hwloc_obj_t obj;            /**< last obj encompassing cpuset */
  puk_tick_t timestamp;
  int count;
};


/** Get the queue that matches a topology object
 */
piom_ltask_queue_t*piom_topo_get_queue(piom_topo_obj_t obj)
{
  if(obj == NULL)
    {
      obj = piom_topo_current_obj();
    }
  while(obj != NULL && (obj->userdata == NULL || (((struct piom_ltask_locality_s*)obj->userdata)->queue == NULL)))
    {
      obj = obj->parent;
    }
  if(obj == NULL)
    {
      obj = piom_topo_current_obj();
      while(obj != NULL && (obj->userdata == NULL || (((struct piom_ltask_locality_s*)obj->userdata)->queue == NULL)))
        {
          obj = obj->parent;
        }
    }
  assert(obj != NULL);
  const struct piom_ltask_locality_s*local = obj->userdata;
  return local->queue;
}

static void piom_topo_hwloc_threadinfo_destroy(void*_ptr)
{
  struct piom_ltask_threadinfo_s*threadinfo = _ptr;
  if(threadinfo != NULL)
    {
      hwloc_bitmap_free(threadinfo->cpuset);
      padico_free(threadinfo);
    }
}

/** create queues attached to topology objects */
void piom_topo_hwloc_init(void)
{
  hwloc_topology_init(&piom_topo_hwloc.topology);
  hwloc_topology_load(piom_topo_hwloc.topology);
  const int depth = hwloc_topology_get_depth(piom_topo_hwloc.topology);
  const hwloc_obj_type_t binding_level = piom_parameters.binding_level;
  int d;
  for(d = 0; d < depth; d++)
    {
      hwloc_obj_t o = hwloc_get_obj_by_depth(piom_topo_hwloc.topology, d, 0);
      if(o->type == HWLOC_OBJ_MACHINE || o->type == HWLOC_OBJ_NODE || o->type == HWLOC_OBJ_SOCKET || o->type == HWLOC_OBJ_CORE)
        {
          const int nb = hwloc_get_nbobjs_by_depth(piom_topo_hwloc.topology, d);
          int i;
          for (i = 0; i < nb; i++)
            {
              o = hwloc_get_obj_by_depth(piom_topo_hwloc.topology, d, i);
              struct piom_ltask_locality_s*local = padico_malloc(sizeof(struct piom_ltask_locality_s));
              local->queue = NULL;
              hwloc_obj_t parent = o->parent;
              while(parent && parent->userdata == NULL)
                {
                  parent = parent->parent;
                }
              local->parent = (parent == NULL) ? NULL : parent->userdata;
              o->userdata = local;
              if(o->type == binding_level)
                {
                  /* TODO- allocate memory on given obj */
                  piom_ltask_queue_t*queue = padico_malloc(sizeof(piom_ltask_queue_t));
                  piom_ltask_queue_init(queue, o);
                  local->queue = queue;
                }
#ifdef PIOMAN_TRACE
              char cont_name[32];
              char cont_type[64];
              const char*level_label = NULL;
              switch(o->type)
                {
                case HWLOC_OBJ_MACHINE:
                  level_label = "Machine";
                  break;
                case HWLOC_OBJ_NODE:
                  level_label = "Node";
                  break;
                case HWLOC_OBJ_SOCKET:
                  level_label = "Socket";
                  break;
                case HWLOC_OBJ_CORE:
                  level_label = "Core";
                  break;
                default:
                  break;
                }
              sprintf(cont_name, "%s_%d", level_label, o->logical_index);
              sprintf(cont_type, "Container_%s", level_label);
              local->trace_info.cont_name = strdup(cont_name);
              local->trace_info.cont_type = strdup(cont_type);
              local->trace_info.level = o->type;
              local->trace_info.rank = o->logical_index;
              local->trace_info.parent = local->parent ? &local->parent->trace_info : NULL;
              piom_trace_local_new(&local->trace_info);
              piom_trace_remote_state(o, PIOM_TRACE_STATE_NONE);
#endif /* PIOMAN_TRACE */
            }
        }
    }
  pthread_key_create(&piom_topo_hwloc.threadinfo_key, &piom_topo_hwloc_threadinfo_destroy);
}


void piom_topo_hwloc_exit(void)
{
  const unsigned depth = hwloc_topology_get_depth(piom_topo_hwloc.topology);
  unsigned d, i;
  for(d = 0; d < depth; d++)
    {
      for (i = 0; i < hwloc_get_nbobjs_by_depth(piom_topo_hwloc.topology, d); i++)
        {
          hwloc_obj_t o = hwloc_get_obj_by_depth(piom_topo_hwloc.topology, d, i);
          struct piom_ltask_locality_s*local = o->userdata;
          if(local)
            {
              piom_ltask_queue_t*queue = local->queue;
              if(queue)
                {
                  piom_trace_remote_state(queue->binding, PIOM_TRACE_STATE_NONE);
                  o->userdata = NULL;
                  padico_free(queue);
                }
              padico_free(local);
            }
        }
    }
  hwloc_topology_destroy(piom_topo_hwloc.topology);
  pthread_key_delete(piom_topo_hwloc.threadinfo_key);
}


hwloc_topology_t piom_topo_hwloc_get(void)
{
  assert(piom_topo_hwloc.topology != NULL);
  return piom_topo_hwloc.topology;
}
piom_topo_obj_t piom_topo_current_obj(void)
{
  int update = 0;
  struct piom_ltask_threadinfo_s*threadinfo = pthread_getspecific(piom_topo_hwloc.threadinfo_key);
  if(threadinfo == NULL)
    {
      threadinfo = padico_malloc(sizeof(struct piom_ltask_threadinfo_s));
      pthread_setspecific(piom_topo_hwloc.threadinfo_key, threadinfo);
      threadinfo->tid = pthread_self();
      threadinfo->cpuset = hwloc_bitmap_alloc();
      threadinfo->count = 0;
      update = 1;
    }
  else
    {
      threadinfo->count++;
      if(threadinfo->count % 100 == 0)
        {
          puk_tick_t now;
          PUK_GET_TICK(now);
          double delay = PUK_TIMING_DELAY(threadinfo->timestamp, now);
          if(delay > 10000)
            update = 1;
        }
    }
  if(update)
    {
      int rc = hwloc_get_last_cpu_location(piom_topo_hwloc.topology, threadinfo->cpuset, HWLOC_CPUBIND_THREAD);
      if(rc == 0)
        {
          hwloc_obj_t o = hwloc_get_obj_covering_cpuset(piom_topo_hwloc.topology, threadinfo->cpuset);
          if(o == NULL)
            {
              char*s_cpuset = NULL;
              hwloc_bitmap_asprintf(&s_cpuset, threadinfo->cpuset);
              PIOM_FATAL("hwloc cannot find covering object for cpuset %s.\n", s_cpuset);
              abort();
            }
          threadinfo->obj = o;
          while(threadinfo->obj != NULL && (threadinfo->obj->userdata == NULL))
            {
              threadinfo->obj = threadinfo->obj->parent;
            }
          if(threadinfo->obj == NULL)
            {
              char*s_cpuset = NULL;
              hwloc_bitmap_asprintf(&s_cpuset, threadinfo->cpuset);
              PIOM_FATAL("cannot find object with pioman threadinfo for cpuset %s (%p).\n", s_cpuset, o);
              abort();
            }
          PUK_GET_TICK(threadinfo->timestamp);
        }
    }
  return threadinfo->obj;
}
#endif /* PIOMAN_TOPOLOGY_HWLOC */

int piom_bind_current_thread_to_core(int core_id)
{
#if defined(PIOMAN_TOPOLOGY_HWLOC)
  hwloc_obj_t core = hwloc_get_obj_by_type(piom_topo_hwloc.topology, HWLOC_OBJ_CORE, core_id);

  return hwloc_set_cpubind(piom_topo_hwloc.topology, core->cpuset, HWLOC_CPUBIND_THREAD);
#else
#warning hwloc not availabe
  (void) core_id;

  return -1; // same error code as when hwloc_set_cpufind fails
#endif
}

/*
 * NewMadeleine
 * Copyright (C) 2006-2023 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

/** @ingroup nmad_private
 * @file
 * Private definitions for nmad core. This file is not installed
 * and not part of the public API.
 */

#ifndef NM_CORE_PRIVATE_H
#define NM_CORE_PRIVATE_H

/* ** alignment ******************************************** */

#define NM_ALIGN_FRONTIER  sizeof(NM_ALIGN_TYPE)
#define nm_aligned(x)      nm_aligned_n((x), NM_ALIGN_FRONTIER)

static inline nm_len_t nm_aligned_n(nm_len_t v, nm_len_t a)
{
  return (v + a - 1) & ~(a - 1);
}


#define nm_offset_of(TYPE, MEMBER) ((size_t) &((TYPE *)0)->MEMBER)
#define nm_container_of(ptr, type, member) \
        ((type *)((char *)(__typeof__ (&((type *)0)->member))(ptr)- \
                  nm_offset_of(type,member)))

/* ** Threads ********************************************* */

#ifdef PIOMAN_MULTITHREAD
#define NM_IS_THREADED 1
#else
#define NM_IS_THREADED ((nm_core_get_thread_level(nm_core_get_singleton()) >= NM_THREAD_SERIALIZED) ? 1 : 0)
#endif

/* ** Allocators ****************************************** */

#define NM_ALLOCATOR_TYPE(ENAME, TYPE) PUK_ALLOCATOR_TYPE_EXT(ENAME, TYPE, !NM_IS_THREADED)

/* ** Requests ********************************************* */

PUK_LIST_CREATE_FUNCS(nm_req);

PUK_LIST_CREATE_FUNCS(nm_req_chunk);
PUK_LFQUEUE_TYPE(nm_req_chunk, struct nm_req_chunk_s*, NULL, NM_REQ_CHUNK_QUEUE_SIZE);

/** allocator for request chunks */
NM_ALLOCATOR_TYPE(nm_req_chunk, struct nm_req_chunk_s);


/* ** Packet wrappers ************************************** */

/** LF queue type for completed pw */
PUK_LFQUEUE_TYPE(nm_pkt_wrap, struct nm_pkt_wrap_s*, NULL, 512);


/* ** Tracks *********************************************** */

#define NM_TRK_SMALL ((nm_trk_id_t)0)
#define NM_TRK_LARGE ((nm_trk_id_t)1)
#define NM_TRK_NONE  ((nm_trk_id_t)-1)

/** @deprecated  */
#define NM_MAX_TRACKS   2

/* ** Sequence numbers ************************************* */

/** First sequence number */
#define NM_SEQ_FIRST ((nm_seq_t)1)

/** Compute next sequence number */
static inline nm_seq_t nm_seq_next(nm_seq_t seq)
{
  assert(seq != NM_SEQ_NONE);
  seq++;
  if(seq == NM_SEQ_NONE)
    seq++;
  return seq;
}

/** compare to seq numbers, assuming they are in the future
 * returns -1 if seq1 is before seq2, 0 if equal, 1 if seq1 after seq2 */
static inline int nm_seq_compare(nm_seq_t current, nm_seq_t seq1, nm_seq_t seq2)
{
  assert(seq1 != current);
  assert(seq2 != current);
  const nm_seq_t seq1_abs = (seq1 > current) ? (seq1 - current) : (seq1 - current - 1);
  const nm_seq_t seq2_abs = (seq2 > current) ? (seq2 - current) : (seq2 - current - 1);
  if(seq1_abs < seq2_abs)
    return -1;
  else if(seq1_abs > seq2_abs)
    return 1;
  else
    return 0;
}

/** compare to seq numbers taking into account looping at overflow,
 * assuming they are less than halftrip of overflow
 * returns -1 if seq1 < seq2, 0 if equal, 1 if seq1 > seq2 */
static inline int nm_seq_fuzzy_compare(nm_seq_t seq1, nm_seq_t seq2)
{
  static const nm_seq_t seq_compare_threshold = NM_SEQ_MAX / 4;
  if(seq1 == seq2)
    return 0;
  else if((seq1 < seq2) && (seq2 - seq1 < seq_compare_threshold))
    return -1;
  else if((seq1 > seq2) && (seq1 - seq2 > 3 * seq_compare_threshold))
    return -1;
  else if((seq1 > seq2) && (seq1 - seq2 < seq_compare_threshold))
    return 1;
  else if((seq1 < seq2) && (seq2 - seq1 > 3 * seq_compare_threshold))
    return 1;
  else
    {
      NM_FATAL("cannot compare seq1 = %u; seq2=%u\n", seq1, seq2);
    }
}

/* ** Profiling ******************************************** */

#ifdef NMAD_PROFILE
#define nm_profile_add(COUNTER, VALUE) do { __sync_fetch_and_add(&COUNTER, VALUE); } while(0)
#else
#define nm_profile_add(COUNTER, VALUE) do {} while(0)
#endif

#define nm_profile_inc(COUNTER) nm_profile_add(COUNTER, 1)

/* ** init helper ****************************************** */

struct nm_lazy_initializer_s
{
  int init_done;    /**< whether init is already done */
  int initializing; /**< whether init is in progress */
};

#define NM_LAZY_INITIALIZER_INIT { .init_done = 0, .initializing = 0 }

/** factorize process for lazy initialization.
 * @arg ENAME base name of variable to define
 * @arg CTOR function called as constructor when init is needede
 * @arg DTOR function called as destructor at application end.
 */
#define NM_LAZY_INITIALIZER(ENAME, CTOR, DTOR)                          \
  static struct nm_lazy_initializer_s ENAME ## _lazy_initializer = NM_LAZY_INITIALIZER_INIT; \
                                                                        \
  static void ENAME ## _lazy_init(void)                                 \
  {                                                                     \
    if(ENAME##_lazy_initializer.init_done == 0)                         \
      {                                                                 \
        if(nm_atomic_inc(&ENAME##_lazy_initializer.initializing) == 0)  \
          {                                                             \
            (*CTOR)();                                                  \
            nm_atomic_inc(&ENAME##_lazy_initializer.init_done);         \
          }                                                             \
        else                                                            \
          {                                                             \
            while(!ENAME##_lazy_initializer.init_done)                  \
              {                                                         \
                sched_yield();                                          \
              }                                                         \
          }                                                             \
      }                                                                 \
    else if(ENAME##_lazy_initializer.init_done == -1)                   \
      {                                                                 \
        NM_FATAL("trying to use %s after detructor.\n", #ENAME);        \
      }                                                                 \
  }                                                                     \
  static void ENAME ## _lazy_destructor(void) __attribute__((destructor)); \
  static void ENAME ## _lazy_destructor(void)                           \
  {                                                                     \
    if(ENAME##_lazy_initializer.init_done > 0)                          \
      {                                                                 \
        (*DTOR)();                                                      \
      }                                                                 \
    ENAME##_lazy_initializer.init_done = -1;                            \
  }


/* ** Drivers ********************************************** */


/* ** Events *********************************************** */

PUK_VECT_TYPE(nm_core_monitor, struct nm_core_monitor_s*);

/** a pending event, not dispatched immediately because it was received out of order */
PUK_LIST_TYPE(nm_core_pending_event,
              struct nm_core_event_s event;             /**< the event to dispatch */
              struct nm_core_monitor_s*p_core_monitor;  /**< the monitor to fire (event matching already checked) */
              );

/** an event ready for dispatch (matching already done) */
struct nm_core_dispatching_event_s
{
  struct nm_core_event_s event;
  struct nm_monitor_s*p_monitor;
  struct nm_req_s*p_req;
};
PUK_LFQUEUE_TYPE(nm_core_dispatching_event, struct nm_core_dispatching_event_s*, NULL, 1024);
NM_ALLOCATOR_TYPE(nm_core_dispatching_event, struct nm_core_dispatching_event_s);

/* ** Unexpected chunks ************************************ */

PUK_LIST_DECLARE_TYPE2(nm_unexpected_wildcard, struct nm_unexpected_s);
PUK_LIST_DECLARE_TYPE2(nm_unexpected_gtag,     struct nm_unexpected_s);
PUK_LIST_DECLARE_TYPE2(nm_unexpected_gate,     struct nm_unexpected_s);
PUK_LIST_DECLARE_TYPE2(nm_unexpected_tag,      struct nm_unexpected_s);

/** an incoming chunk of data */
struct nm_in_chunk_s
{
  nm_gate_t p_gate;      /**< gate the chunk arrived from */
  nm_seq_t seq;          /**< sequence number */
  nm_core_tag_t tag;     /**< full tag */
  nm_len_t chunk_offset; /**< offset of the chunk in the full message */
  nm_len_t chunk_len;    /**< length of the chunk itself */
  nm_proto_t flags;      /**< proto flags associated with the chunk (NM_PROTO_FLAG_*) */
  nm_prio_t priority;    /**< priority of the incoming data (only for rdv) */
};

/** a chunk to be injected into nmad core */
struct nm_chunk_injector_s
{
  nm_injector_pull_data_t p_pull_data;      /**< function to call to actually get data */
  void*p_ref;                               /**< user-supplied ref for the above function */
};

/** a chunk of unexpected message to be stored */
struct nm_unexpected_s
{
  PUK_LIST_LINK(nm_unexpected_wildcard);
  PUK_LIST_LINK(nm_unexpected_gtag);          /**< link for list of unexpected per-tag */
  PUK_LIST_LINK(nm_unexpected_gate);
  PUK_LIST_LINK(nm_unexpected_tag);
  const union nm_header_generic_s*p_header;   /**< raw header in pw buffer */
  struct nm_pkt_wrap_s*p_pw;                  /**< pw this chunk arrived from; may be NULL if data is brought by injector */
  struct nm_in_chunk_s chunk;                 /**< metadata of the enclosed data chunk */
  nm_len_t msg_len;                           /**< length of full message on last chunk, NM_LEN_UNDEFINED if not last chunk */
  struct nm_chunk_injector_s injector;        /**< injector for this chunk */
  struct nm_matching_container_s matching;    /**< cache for matching containers */
  int matched;                                /**< flag whether the unexpected is already matched (and delayed), and thus only enqueued in gtag matching containers */
};

PUK_LIST_CREATE_FUNCS(nm_unexpected_wildcard);
PUK_LIST_CREATE_FUNCS(nm_unexpected_gtag);
PUK_LIST_CREATE_FUNCS(nm_unexpected_gate);
PUK_LIST_CREATE_FUNCS(nm_unexpected_tag);


#endif /* NM_CORE_PRIVATE_H */

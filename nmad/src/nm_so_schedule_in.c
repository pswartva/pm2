/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULA R PURPOSE.  See the GNU
 * General Public License for more details.
 */


#include <stdint.h>
#include <sys/uio.h>
#include <assert.h>

#include <nm_private.h>

PADICO_MODULE_HOOK(nmad);

static void nm_pkt_data_handler(struct nm_core*p_core, nm_gate_t p_gate, struct nm_req_s**pp_unpack,
                                const struct nm_header_pkt_data_s*h, struct nm_pkt_wrap_s *p_pw);
static void nm_short_data_handler(struct nm_core*p_core, nm_gate_t p_gate, struct nm_req_s**pp_unpack,
                                  const struct nm_header_short_data_s*h, struct nm_pkt_wrap_s *p_pw);
static void nm_small_data_handler(struct nm_core*p_core, nm_gate_t p_gate, struct nm_req_s**pp_unpack,
                                  const struct nm_header_data_s*h, struct nm_pkt_wrap_s *p_pw);
static void nm_msg_handler(struct nm_core*p_core, nm_gate_t p_gate, struct nm_req_s**pp_unpack,
                           const struct nm_header_msg_s*h, struct nm_pkt_wrap_s*p_pw);
static void nm_rdv_handler(struct nm_core*p_core, nm_gate_t p_gate, struct nm_req_s*p_unpack,
                           const struct nm_header_ctrl_rdv_s*h, struct nm_pkt_wrap_s *p_pw);
static void nm_inject_data_handler(struct nm_core*p_core, struct nm_req_s**pp_unpack,
                                   const struct nm_in_chunk_s*p_chunk,
                                   const struct nm_chunk_injector_s*p_injector);

static void nm_core_unpack_unexpected(struct nm_core*p_core, struct nm_req_s**pp_unpack, struct nm_unexpected_s*p_unexpected);

static inline int nm_core_unpack_match_chunk(struct nm_core*p_core, struct nm_req_s*p_unpack,
                                             const struct nm_in_chunk_s*p_chunk,
                                             const union nm_header_generic_s*p_header,
                                             struct nm_pkt_wrap_s*p_pw,
                                             const struct nm_chunk_injector_s*p_injector);
static inline void nm_core_unpack_match_resolve(struct nm_core*p_core, struct nm_req_s*p_unpack);

static inline void nm_core_unpack_matching_remove(struct nm_core*p_core, struct nm_req_s*p_unpack);

static inline void nm_core_unpack_match_fill(struct nm_core*p_core, struct nm_req_s*p_unpack,
                                             const struct nm_matching_container_s*p_matching_cache,
                                             nm_gate_t p_gate, nm_core_tag_t tag, nm_core_tag_t tag_mask, nm_seq_t seq);

static struct nm_req_s*nm_unpack_find_matching(struct nm_core*p_core, nm_gate_t p_gate, nm_seq_t seq, nm_core_tag_t tag);

static inline void nm_matching_container_fill(struct nm_core*p_core, struct nm_matching_container_s*p_matching,
                                              struct nm_gate_s*p_gate, nm_core_tag_t tag, struct nm_gtag_s*p_gtag);

/* TODO- matching big picture
 * ** Invariants **
 * match_full:
 *   - unexpected list: fully sorted by seq number
 *       . next unexpected is _not_ allways first on list, but next_seq is
 *         predictable since gtag is fixed
 *       . insertion in constant time when packets arrive in order
 *       . lookup in constant time if no delayed requests, in O(d) if there
 *         are $d$ delayed requests
 *   - unpack list: sorted by req_seq, some matched requests at beginning
 *       . next unpack is first unmatched (p_unpack->seq == NM_SEQ_NONE)
 *       . insertion in constant time
 *       . lookup in O(d) if there are $d$ delayed requests (constant time
 *         in the regular case)
 * other lists (gate/gsession/wildcard):
 *   - unexpected: incoming packets inserted in chronological order
 *       . insertion in constant time
 *       . lookup in O(d) with $d$ the number of delayed chunks not bound
 *         yet to their matched request
 *       . unexpected chunks bound to a matched request are remove from all
 *         lists except gtag
 *   - unpack list: sorted by req_seq, contains only unmatched requests
 *       . when a request is matched, it is move to gtag list
 *
 * TODO- optimizations
 *   - do not move unpacks to gtag list if request completes immediately
 *     -> gtag requests are already not moved; performance difference
 *     between gtag and anytag/anygate is marginal-- 10-20ns)
 *   - move matched unpacks & their unexpected chunks to dedicated lists
 *     -> would _always_ move requests, even gtag; not sure it's an optimization
 */


/* ** debugging stuff *************************************** */

#ifdef NMAD_DEBUG
//#define NM_SO_MATCHING_DEBUG 1
#define NM_SO_MATCHING_DEBUG 0
#define NM_SO_MATCHING_RECOVER 0
#else
#define NM_SO_MATCHING_DEBUG 0
#define NM_SO_MATCHING_RECOVER 0
#endif

/* enable linear search in matching algorithm.
 * used only when debugging to check lists state consistency */
#define LINEAR_SEARCH 0


/** iterate over all pending unexpected chunks and try to match them with
 * pending unexpected requests.
 */
void nm_core_matching_check(struct nm_core*p_core)
{
#ifdef NMAD_DEBUG
  if(NM_SO_MATCHING_RECOVER)
    {
      nm_core_lock_assert(p_core);
      nm_core_tag_vect_t v = nm_matching_wildcard_get_tags(&p_core->wildcard_table);
      nm_core_tag_vect_itor_t p_tag;
      puk_vect_foreach(p_tag, nm_core_tag, v) /* loop on all session tags */
        {
          struct nm_matching_wildcard_s*p_wildcard = nm_matching_wildcard_bytag(p_core, *p_tag);
          struct nm_unexpected_s*p_unexpected;
          puk_list_foreach(nm_unexpected_wildcard, p_unexpected, &p_wildcard->unexpected) /* loop on all unexpected */
            {
              struct nm_req_s*p_unpack = nm_unpack_find_matching(p_core, p_unexpected->chunk.p_gate, p_unexpected->chunk.seq, p_unexpected->chunk.tag);
              if(p_unpack != NULL)
                {
                  NM_WARN("matching- chunk_offset = %ld; chunk_len = %ld; p_gate = %p; seq = %u; tag = %lx:%x\n",
                          p_unexpected->chunk.chunk_offset, p_unexpected->chunk.chunk_len,
                          p_unexpected->chunk.p_gate, p_unexpected->chunk.seq,
                          p_unexpected->chunk.tag.tag, p_unexpected->chunk.tag.hashcode);
                  nm_core_unpack_unexpected(p_core, &p_unpack, p_unexpected);
                }
            }
        }
      nm_core_tag_vect_delete(v);
    }
#endif /* NMAD_DEBUG */
}

/* ********************************************************* */
/* ** allocators */

/** allocator for core tasks */
PUK_ALLOCATOR_TYPE_SINGLE(nm_core_task, struct nm_core_task_s);

/** allocator for injectors- single thread, always used from locked section */
PUK_ALLOCATOR_TYPE_SINGLE(nm_chunk_injector, struct nm_chunk_injector_s);

/** fast allocator for struct nm_unexpected_s; use single thread
 * allocator: always use in a locked section */
PUK_ALLOCATOR_TYPE_SINGLE(nm_unexpected, struct nm_unexpected_s);

#define NM_UNEXPECTED_PREALLOC 256

static struct
{
  struct nm_core_task_allocator_s core_task_allocator;
  struct nm_unexpected_allocator_s unexpected_allocator;
} nm_so_schedule;

static void nm_so_schedule_constructor(void);
static void nm_so_schedule_destructor(void);

NM_LAZY_INITIALIZER(nm_so_schedule, &nm_so_schedule_constructor, &nm_so_schedule_destructor);

static void nm_so_schedule_constructor(void)
{
  nm_core_task_allocator_init(&nm_so_schedule.core_task_allocator, 16);
  nm_unexpected_allocator_init(&nm_so_schedule.unexpected_allocator, NM_UNEXPECTED_PREALLOC);
}

static void nm_so_schedule_destructor(void)
{
  nm_core_task_allocator_destroy(&nm_so_schedule.core_task_allocator);
  nm_unexpected_allocator_destroy(&nm_so_schedule.unexpected_allocator);
}

/* ********************************************************* */
/* ** unexpected/matching */

static inline struct nm_unexpected_s*nm_unexpected_alloc(struct nm_core*p_core)
{
  nm_so_schedule_lazy_init();
  nm_core_lock_assert(p_core);
  struct nm_unexpected_s*p_unexpected = nm_unexpected_malloc(&nm_so_schedule.unexpected_allocator);
  nm_unexpected_wildcard_list_cell_init(p_unexpected);
  nm_unexpected_gtag_list_cell_init(p_unexpected);
  nm_unexpected_gate_list_cell_init(p_unexpected);
  nm_unexpected_tag_list_cell_init(p_unexpected);
  p_unexpected->p_pw = NULL;
  p_unexpected->p_header = NULL;
  p_unexpected->injector.p_pull_data = NULL;
  p_unexpected->injector.p_ref = NULL;
  p_unexpected->matching = NM_MATCHING_CONTAINER_NULL;;
  p_unexpected->matched = 0;
  return p_unexpected;
}

/** warn, complete & free pending unexpected chunks in the given list.
 * @note this function gets called once for each session.
 * @note it is enough to iterate only on the _wildcard_ list since
 * unexpected chunks are enqueues in all lists at once, so we are
 * not missing any entry. */
void nm_unexpected_wildcard_clean(struct nm_unexpected_wildcard_list_s*p_unexpected_list)
{
  struct nm_unexpected_s*p_unexpected = nm_unexpected_wildcard_list_pop_front(p_unexpected_list);
  while(p_unexpected)
    {
#ifdef NMAD_DEBUG
      NM_WARN("unexpected chunk %p not completed (gate = %p; seq = %d; tag = %llx:%llx)\n",
              p_unexpected, p_unexpected->chunk.p_gate, p_unexpected->chunk.seq,
              (unsigned long long)nm_core_tag_get_hashcode(p_unexpected->chunk.tag),
              (unsigned long long)nm_core_tag_get_tag(p_unexpected->chunk.tag));
#endif /* NMAD_DEBUG */
      if(p_unexpected->p_pw)
        {
#if(defined(PIOMAN))
          piom_ltask_completed(&p_unexpected->p_pw->ltask);
#else /* PIOMAN */
          nm_pw_ref_dec(p_unexpected->p_pw);
#endif /* PIOMAN */
        }
      nm_unexpected_free(&nm_so_schedule.unexpected_allocator, p_unexpected);
      p_unexpected = nm_unexpected_wildcard_list_pop_front(p_unexpected_list);
    }
}

/** clean nm_so_schedule data upon termination */
void nm_so_schedule_clean(struct nm_core*p_core)
{
  nm_matching_wildcard_table_destroy(&p_core->wildcard_table);
  nm_matching_tag_table_destroy(&p_core->tag_table);
}

/** extract the length from an unexpected chunk (NM_LEN_UNDEFINED if chunk contains no length info) */
static inline nm_len_t nm_unexpected_get_len(struct nm_core*p_core, const struct nm_unexpected_s*p_unexpected)
{
  assert( (p_unexpected->msg_len == NM_LEN_UNDEFINED) ||
          (p_unexpected->chunk.flags & NM_PROTO_FLAG_LASTCHUNK) ||
          (p_unexpected->p_header->generic.proto_id & NM_PROTO_RDV) ||
          (p_unexpected->p_header->generic.proto_id & NM_PROTO_MSG) );
  return p_unexpected->msg_len;
}

/** checks whether an unpack request matches a chunk from gate/seq/tag */
static inline int nm_unpack_is_matching(const struct nm_req_s*p_req, nm_gate_t p_gate, nm_seq_t seq, nm_core_tag_t tag, nm_seq_t next_seq)
{
  assert(p_gate != NULL);
  assert(!nm_status_test(p_req, NM_STATUS_FINALIZED));
  NM_TRACEF("trying p_preq = %p; p_gate = %p (%p); seq = %u (%u); next_seq = %u; tag = %lx:%x (%lx:%x)\n",
            p_req, p_req->p_gate, p_gate, p_req->seq, seq, next_seq,
            p_req->tag.tag, p_req->tag.hashcode, tag.tag, tag.hashcode);
  return (((p_req->p_gate == p_gate) || (p_req->p_gate == NM_ANY_GATE)) && /* gate matches */
          nm_core_tag_match(tag, p_req->tag, p_req->unpack.tag_mask) && /* tag matches */
          ((p_req->seq == seq) || ((p_req->seq == NM_SEQ_NONE) && (seq == next_seq))) /* seq number matches */ );
}

/** for the given p_unexpected chunk, found in chronological order in an
 * unexpected list, find the unexpected chunk on the same gtag that will
 * be next in line to be received, so as to ensure semantics of matching.
 */
static inline struct nm_unexpected_s*nm_unexpected_find_byseq(struct nm_core*p_core, struct nm_unexpected_s*p_unexpected, nm_seq_t next_seq)
{
  NM_TRACEF("p_unexpected = %p; chunk seq = %u; next_seq = %u\n", p_unexpected, p_unexpected->chunk.seq, next_seq);
  assert(nm_seq_fuzzy_compare(p_unexpected->chunk.seq, next_seq) >= 0);
  while( (nm_unexpected_gtag_list_rnext(p_unexpected) != NULL) &&
         (nm_unexpected_gtag_list_rnext(p_unexpected)->chunk.seq >= next_seq))
    {
      /* percolate up to the first chunk on the gtag with the right seq, to ensure semantic consistency */
      NM_TRACEF("p_unexpected = %p; next_seq = %u\n", p_unexpected, next_seq);
      p_unexpected = nm_unexpected_gtag_list_rnext(p_unexpected);
      if(nm_seq_fuzzy_compare(p_unexpected->chunk.seq, next_seq) < 0)
        {
          /* we have a hole in the sequence number; this particular chunk seems to be late */
          return NULL;
        }
    }
  assert(nm_seq_fuzzy_compare(p_unexpected->chunk.seq, next_seq) >= 0);
  if(p_unexpected->chunk.seq == next_seq)
    {
      return p_unexpected;
    }
  else
    {
      /* out of order packet: we don't have the next packet but have the following one. */
      return NULL;
    }
}

/** Look for an unexpected chunk that matches the given posted/unposted request.
 * Request & unexpected are left unmodified, matching is not saved.
 * Perform a linear search in the unexpected lists only up to the wanted sequence
 * number; cost is _constant_ except in case of delayed requests
 */
static struct nm_unexpected_s*nm_unexpected_lookup(struct nm_core*p_core, struct nm_req_s*p_unpack)
{
  assert(!nm_status_test(p_unpack, NM_STATUS_FINALIZED));
  assert(p_unpack->flags & NM_REQ_FLAG_UNPACK_MATCHING_INFO);
  nm_core_lock_assert(p_core);
  nm_core_unpack_match_resolve(p_core, p_unpack);
  NM_TRACEF("p_unpack = %p\n", p_unpack);
  if(p_unpack->flags & NM_REQ_FLAG_MATCHING_FULL)
    {
      /* fully-specified requests, list per gtag, sorted by seq */
      NM_TRACEF("p_unpack = %p; MATCHING FULL\n", p_unpack);
      assert(p_unpack->p_gtag != NULL);
      const nm_seq_t next_seq = nm_seq_next(p_unpack->p_gtag->recv_seq_number);
      struct nm_unexpected_s*p_unexpected = NULL;
      puk_list_foreach(nm_unexpected_gtag, p_unexpected, &p_unpack->p_gtag->unexpected)
        {
          if(nm_seq_fuzzy_compare(p_unexpected->chunk.seq, next_seq) > 0)
            {
              break;
            }
          if(nm_unpack_is_matching(p_unpack, p_unexpected->chunk.p_gate, p_unexpected->chunk.seq, p_unexpected->chunk.tag, next_seq))
            {
              assert(p_unexpected->matching.p_gtag == p_unpack->p_gtag);
              return p_unexpected;
            }
        }
    }
  else if(p_unpack->flags & NM_REQ_FLAG_MATCHING_TAG)
    {
      /* only tag is specified, list per tag, chronological order */
      struct nm_matching_tag_s*p_matching_tag = p_unpack->unpack.matching.p_matching_tag;
      NM_TRACEF("p_unpack = %p; MATCHING TAG; unexpected list size = %d\n",
                p_unpack, nm_unexpected_tag_list_size(&p_matching_tag->unexpected));
      assert(p_unpack->seq == NM_SEQ_NONE); /* request didn't match yet, else it would be in the gtag list */
      struct nm_unexpected_s*p_unexpected = nm_unexpected_tag_list_begin(&p_matching_tag->unexpected);
      while(p_unexpected != NULL)
        {
          const nm_seq_t next_seq = nm_seq_next(p_unexpected->matching.p_gtag->recv_seq_number);
          NM_TRACEF("p_unpack = %p; MATCHING TAG; p_unexpected = %p; seq = %u; next_seq = %u\n",
                    p_unpack, p_unexpected, p_unexpected->chunk.seq, next_seq);
          if(nm_seq_fuzzy_compare(p_unexpected->chunk.seq, next_seq) < 0)
            {
              /* chunk is late on seq; we can suppose it is bound to an already matched (delayed) requests -> skip */
              p_unexpected = nm_unexpected_tag_list_next(p_unexpected);
            }
          else
            {
              NM_TRACEF("before p_unexpected = %p\n", p_unexpected);
              p_unexpected = nm_unexpected_find_byseq(p_core, p_unexpected, next_seq);
              NM_TRACEF("after p_unexpected = %p\n", p_unexpected);
              assert((p_unexpected == NULL) ||
                     nm_unpack_is_matching(p_unpack, p_unexpected->chunk.p_gate, p_unexpected->chunk.seq, p_unexpected->chunk.tag, next_seq));
              break;
            }
        }
      return p_unexpected;
    }
  else if(p_unpack->flags & NM_REQ_FLAG_MATCHING_GATE)
    {
      /* only gate is specified, list per gsession */
      NM_TRACEF("p_unpack = %p; MATCHING GATE\n", p_unpack);
      assert(p_unpack->seq == NM_SEQ_NONE); /* request didn't match yet, else it would be in the gtag list */
      struct nm_matching_gsession_s*p_matching_gate = p_unpack->unpack.matching.p_gsession;
      struct nm_unexpected_s*p_unexpected = nm_unexpected_gate_list_begin(&p_matching_gate->unexpected);
      while(p_unexpected != NULL)
        {
          const nm_seq_t next_seq = nm_seq_next(p_unexpected->matching.p_gtag->recv_seq_number);
          if(nm_seq_fuzzy_compare(p_unexpected->chunk.seq, next_seq) < 0)
            {
              p_unexpected = nm_unexpected_gate_list_next(p_unexpected);
            }
          else
            {
              p_unexpected = nm_unexpected_find_byseq(p_core, p_unexpected, next_seq);
              assert((p_unexpected == NULL) ||
                     nm_unpack_is_matching(p_unpack, p_unexpected->chunk.p_gate, p_unexpected->chunk.seq, p_unexpected->chunk.tag, next_seq));
              break;
            }
        }
      return p_unexpected;
    }
  else if(p_unpack->flags & NM_REQ_FLAG_MATCHING_WILDCARD)
    {
      /* wildcard request or sparse bitmask for tag
       * For wildcard requests, the first chunk not already bound to a matched req will match.
       * For sparse mask_tag, only linear search is possible- use with care.
       */
      NM_TRACEF("p_unpack = %p; MATCHING WILDCARD\n", p_unpack);
      assert(p_unpack->seq == NM_SEQ_NONE); /* request didn't match yet, else it would be in the gtag list */
      struct nm_matching_wildcard_s*p_wildcard = p_unpack->unpack.matching.p_wildcard;
      struct nm_unexpected_s*p_unexpected = nm_unexpected_wildcard_list_begin(&p_wildcard->unexpected);
      if(p_unpack->unpack.tag_mask.tag == NM_TAG_MASK_FULL)
        {
          while(p_unexpected != NULL)
            {
              const nm_seq_t next_seq = nm_seq_next(p_unexpected->matching.p_gtag->recv_seq_number);
              if(nm_seq_fuzzy_compare(p_unexpected->chunk.seq, next_seq) < 0)
                {
                  p_unexpected = nm_unexpected_wildcard_list_next(p_unexpected);
                }
              else
                {
                  p_unexpected = nm_unexpected_find_byseq(p_core, p_unexpected, next_seq);
                  assert((p_unexpected == NULL) ||
                         nm_unpack_is_matching(p_unpack, p_unexpected->chunk.p_gate, p_unexpected->chunk.seq, p_unexpected->chunk.tag, next_seq));
                  break;
                }
            }
        }
      else
        {
          /* !!! linear search when using tag_mask !!! */
          while(p_unexpected != NULL)
            {
              const nm_seq_t next_seq = nm_seq_next(p_unexpected->matching.p_gtag->recv_seq_number);
              if(nm_unpack_is_matching(p_unpack, p_unexpected->chunk.p_gate, p_unexpected->chunk.seq, p_unexpected->chunk.tag, next_seq))
                {
                  return p_unexpected;
                }
              else
                {
                  p_unexpected = nm_unexpected_wildcard_list_next(p_unexpected);
                }
            }
        }
      return p_unexpected;
    }
  else
    {
      NM_FATAL("unspecified matching method in req flags 0x%x.\n", p_unpack->flags);
    }
  return NULL;
}

/** finds the last unexpected chunk matching the same infos the one given
 * (so as to get packet size).
 * @warning linear complexity! [ actually used only in iprobe ] */
static struct nm_unexpected_s*nm_unexpected_find_last(struct nm_core*p_core, struct nm_unexpected_s*p_unexpected0)
{
  nm_core_lock_assert(p_core);
  nm_len_t out_len = nm_unexpected_get_len(p_core, p_unexpected0);
  if(out_len != NM_LEN_UNDEFINED)
    return p_unexpected0;
  /* we have an unexpected chunk, but first chunk does not contain length */
  struct nm_gtag_s*p_gtag = p_unexpected0->matching.p_gtag;
  assert(p_gtag != NULL);
  assert(p_unexpected0->chunk.seq != NM_SEQ_NONE);
  /* walk through all chunks on the same gate/tag/seq */
  struct nm_unexpected_s*p_unexpected = nm_unexpected_gtag_list_begin(&p_gtag->unexpected);
  while( (p_unexpected != NULL) && (p_unexpected->chunk.seq == p_unexpected0->chunk.seq) && (out_len == NM_LEN_UNDEFINED) )
    {
      out_len = nm_unexpected_get_len(p_core, p_unexpected);
      p_unexpected = nm_unexpected_gtag_list_next(p_unexpected);
    }
  return p_unexpected;
}

/** Find an unexpected chunk that matches an unpack request [p_gate, seq, tag]
 *  including matching with any_gate / any_tag in the unpack
 *  Fills tag/gate/seq in request, *not length*
 */
static struct nm_unexpected_s*nm_unexpected_find_matching(struct nm_core*p_core, struct nm_req_s*p_unpack)
{
  NM_TRACEF("p_unpack = %p\n", p_unpack);
  assert(!nm_status_test(p_unpack, NM_STATUS_FINALIZED));
  assert(p_unpack->flags & NM_REQ_FLAG_UNPACK_MATCHING_INFO);
  nm_core_lock_assert(p_core);
  struct nm_unexpected_s*p_unexpected = nm_unexpected_lookup(p_core, p_unpack);
  if(p_unexpected != NULL)
    {
      assert(p_unexpected->chunk.p_gate != NULL);
      assert(p_unexpected->matching.p_gtag != NULL);
      NM_TRACEF("MATCH\n");
      if(p_unpack->seq == NM_SEQ_NONE)
        {
          NM_TRACEF("INCREMENT SEQ; new seq = %d\n", nm_seq_next(p_unexpected->matching.p_gtag->recv_seq_number));
          p_unexpected->matching.p_gtag->recv_seq_number = nm_seq_next(p_unexpected->matching.p_gtag->recv_seq_number);
        }
      nm_core_unpack_match_fill(p_core, p_unpack, &p_unexpected->matching, p_unexpected->chunk.p_gate, p_unexpected->chunk.tag,
                                NM_CORE_TAG_MASK_FULL, p_unexpected->chunk.seq);
      /* remove unexpected from lists, except gtag */
      if(!p_unexpected->matched)
        {
          nm_unexpected_wildcard_list_remove(&p_unexpected->matching.p_wildcard->unexpected, p_unexpected);
          nm_unexpected_tag_list_remove(&p_unexpected->matching.p_matching_tag->unexpected, p_unexpected);
          nm_unexpected_gate_list_remove(&p_unexpected->matching.p_gsession->unexpected, p_unexpected);
          p_unexpected->matched = 1;
        }
    }
  else
    {
      NM_TRACEF("NO MATCH\n");
    }
  return p_unexpected;
}

/** find a matching request in the given list with gate/seq/tag
 * list is sorted by req_seq -> only the first unmatched request may match
 * @param whether the list is supposed to be ordered by seq number (seq per gtag,
 *   not req_seq) and to contain already matched requests.
 */
static inline void nm_unpack_match_list(struct nm_req_s**pp_unpack_best, struct nm_req_list_s*p_list, int seq_ordered,
                                        const nm_gate_t p_gate, const nm_seq_t seq, const nm_seq_t next_seq, const nm_core_tag_t tag)
{
  NM_TRACEF("p_list = %p; list length = %d\n", p_list, nm_req_list_size(p_list));
#ifdef NMAD_DEBUG
  if(NM_SO_MATCHING_DEBUG)
    {
      nm_req_seq_t prev_req_seq = 0;
      int found_unmatched = 0;
      struct nm_req_s*p_req_itor;
      puk_list_foreach(nm_req, p_req_itor, p_list)
        {
          /* check invariant #1: list is sorted by req_seq */
          assert(p_req_itor->unpack.req_seq >= prev_req_seq);
          prev_req_seq = p_req_itor->unpack.req_seq;
          /* check invariant #2: matched requests are on the head */
          if(p_req_itor->seq != NM_SEQ_NONE)
            {
              assert(!found_unmatched); /* no unmatched req found before a matched request */
            }
          else
            {
              found_unmatched = 1;
            }
        }
    }
#endif /* NMAD_DEBUG */
  /* we need a loop for out-of-order packets; anyway, usually the first req will match and early exit will happen */
  struct nm_req_s*p_unpack_itor = NULL;
  puk_list_foreach(nm_req, p_unpack_itor, p_list)
    {
      assert((seq_ordered) || (p_unpack_itor->seq == NM_SEQ_NONE)); /* list is either ordered by gtag-seq, or contains only unmatched requests */
      if(nm_unpack_is_matching(p_unpack_itor, p_gate, seq, tag, next_seq) &&
         ((*pp_unpack_best == NULL) ||
          (p_unpack_itor->unpack.req_seq < (*pp_unpack_best)->unpack.req_seq)))
        {
          /* request matches and is better than previous matching req */
          *pp_unpack_best = p_unpack_itor;
          return;
        }
      if((*pp_unpack_best) &&
         (nm_seq_fuzzy_compare(p_unpack_itor->unpack.req_seq, (*pp_unpack_best)->unpack.req_seq) > 0))
        {
          /* current request is further than previous matching req- exit */
          goto nomatch;;
        }
      if(seq_ordered)
        {
          /* we are on the gtag list, we can safely compare gtag-seq number */
          if((p_unpack_itor->seq != NM_SEQ_NONE) &&
             (nm_seq_fuzzy_compare(p_unpack_itor->seq, seq) > 0))
            {
              if(!(LINEAR_SEARCH))
                goto nomatch;
            }
        }
      else
        {
          if(p_unpack_itor->seq == NM_SEQ_NONE)
            {
              /* first not already matched request; it was the only candidate for matching.
               * If it didn't match -> exit */
              if(!(LINEAR_SEARCH))
                goto nomatch;
            }
        }
    }
 nomatch:
  /* check that we didn't miss a match */
  if(NM_SO_MATCHING_DEBUG)
    {
#ifdef NMAD_DEBUG
      puk_list_foreach(nm_req, p_unpack_itor, p_list)
        {
          if(nm_unpack_is_matching(p_unpack_itor, p_gate, seq, tag, next_seq) &&
             ((pp_unpack_best == NULL) ||((*pp_unpack_best)->unpack.req_seq > p_unpack_itor->unpack.req_seq)) )
            {
              NM_WARN("matching missed in list for seq = %u; p_gate = %p; tag = %lx:%x\n", seq, p_gate, tag.tag, tag.hashcode);
              struct nm_req_s*p_unpack_itor2 = NULL;
              puk_list_foreach(nm_req, p_unpack_itor2, p_list)
                {
                  fprintf(stderr, "# p_unpack = %p; req_seq = %lu; seq = %u; p_gate = %p; tag = %lx:%x; match = %d\n",
                          p_unpack_itor2, p_unpack_itor2->unpack.req_seq, p_unpack_itor2->seq, p_unpack_itor2->p_gate,
                          p_unpack_itor2->tag.tag, p_unpack_itor2->tag.hashcode,
                          nm_unpack_is_matching(p_unpack_itor2, p_gate, seq, tag, next_seq));
                }
              NM_FATAL("matching missed in list\n");
            }
        }
#endif /* NMAD_DEBUG */
    }
}

/** Find an unpack that matches a given packet that arrived from [p_gate, seq, tag]
 *  including matching with any_gate / any_tag in the unpack
 */
static struct nm_req_s*nm_unpack_find_matching(struct nm_core*p_core, nm_gate_t p_gate, nm_seq_t seq, nm_core_tag_t tag)
{
  nm_core_lock_assert(p_core);
  assert(p_gate != NULL);
  struct nm_req_s*p_unpack = NULL;
  struct nm_matching_container_s matching = NM_MATCHING_CONTAINER_NULL;
  nm_matching_container_fill(p_core, &matching, p_gate, tag, NULL);
  const nm_seq_t next_seq = nm_seq_next(matching.p_gtag->recv_seq_number);
  NM_TRACEF("packet tag = %lx:%x; packet seq = %u; expected next seq = %u on this tag\n",
            tag.tag, tag.hashcode, seq, next_seq);
  nm_unpack_match_list(&p_unpack, &matching.p_gtag->unpacks, 1, p_gate, seq, next_seq, tag);
  nm_unpack_match_list(&p_unpack, &matching.p_matching_tag->unpacks, 0, p_gate, seq, next_seq, tag);
  nm_unpack_match_list(&p_unpack, &matching.p_gsession->unpacks, 0, p_gate, seq, next_seq, tag);
  nm_unpack_match_list(&p_unpack, &matching.p_wildcard->unpacks, 0, p_gate, seq, next_seq, tag);
  if(p_unpack)
    {
      NM_TRACEF("seq = %u; p_unpack = %p; MATCHING\n", seq, p_unpack);
      if(p_unpack->seq == NM_SEQ_NONE)
        {
          matching.p_gtag->recv_seq_number = next_seq;
          NM_TRACEF("INCREMENT SEQ; new seq = %u\n", seq);
        }
      nm_core_unpack_match_fill(p_core, p_unpack, &matching, p_gate, tag, NM_CORE_TAG_MASK_FULL, seq);
      return p_unpack;
    }
  else
    {
      NM_TRACEF("seq = %u; NO MATCH\n", seq);
    }
  return NULL;
}

/** After having completed a request with unexpected chunks, try to see
 * if we can complete following requests on the same gtag, same gate, same tag,
 * or wildcard requests, now that seq has been incremented.
 * Usefull for out-of-order packets & for new requests posted while
 * the previous requests is processed (e.g. RPC)
 * @note this function consumes p_next
 */
__PUK_SYM_INTERNAL
void nm_core_unpack_trigger_next(struct nm_core*p_core, struct nm_core_task_s*p_core_task)
{
  struct nm_matching_container_s*p_matching = &p_core_task->content.unpack_next.matching;
  struct nm_gtag_s*p_gtag = p_matching->p_gtag;
  NM_TRACEF("gtag size = %d; recv_seq_number = %u; next_seq = %u\n",
            nm_unexpected_gtag_list_size(&p_gtag->unexpected), p_gtag->recv_seq_number, nm_seq_next(p_gtag->recv_seq_number));
#ifdef NMAD_DEBUG
  {
    nm_unexpected_gtag_itor_t i;
    puk_list_foreach(nm_unexpected_gtag, i, &p_gtag->unexpected)
      {
        NM_TRACEF("p_unexpected = %p; len = %ld; seq = %u; tag = %lx:%x; chunk_len = %ld\n",
                  i, i->msg_len, i->chunk.seq, i->chunk.tag.tag, i->chunk.tag.hashcode, i->chunk.chunk_len);
      }
  }
#endif
  nm_core_lock_assert(p_core);
  /* ** try reqs on gtag */
  {
    struct nm_gtag_s*p_gtag = p_matching->p_gtag;
    struct nm_req_s*p_unpack = nm_req_list_begin(&p_gtag->unpacks);
    while((p_unpack != NULL) && (p_unpack->seq != NM_SEQ_NONE))
      p_unpack = nm_req_list_next(p_unpack); /* skip already matched reqs at list head */
    if(p_unpack)
      {
        assert(p_unpack->seq == NM_SEQ_NONE);
        struct nm_unexpected_s*p_unexpected = nm_unexpected_find_matching(p_core, p_unpack);
        if(p_unexpected)
          {
            nm_core_unpack_unexpected(p_core, &p_unpack, p_unexpected);
          }
      }
  }
  /* ** try reqs on same gate */
  {
    struct nm_matching_gsession_s*p_matching_gsession = p_matching->p_gsession;
    struct nm_req_s*p_unpack = nm_req_list_begin(&p_matching_gsession->unpacks);
    if(p_unpack)
      {
        struct nm_unexpected_s*p_unexpected = nm_unexpected_find_matching(p_core, p_unpack);
        if(p_unexpected)
          {
            nm_core_unpack_unexpected(p_core, &p_unpack, p_unexpected);
          }
      }
  }
  /* ** try reqs on same tag */
  {
    struct nm_matching_tag_s*p_matching_tag = p_matching->p_matching_tag;
    struct nm_req_s*p_unpack = nm_req_list_begin(&p_matching_tag->unpacks);
    if(p_unpack)
      {
        struct nm_unexpected_s*p_unexpected = nm_unexpected_find_matching(p_core, p_unpack);
        if(p_unexpected)
          {
            nm_core_unpack_unexpected(p_core, &p_unpack, p_unexpected);
          }
      }
  }
  /* ** try reqs on wildcard */
  {
    struct nm_matching_wildcard_s*p_wildcard = p_matching->p_wildcard;
    struct nm_req_s*p_unpack = nm_req_list_begin(&p_wildcard->unpacks);
    if(p_unpack)
      {
        struct nm_unexpected_s*p_unexpected = nm_unexpected_find_matching(p_core, p_unpack);
        if(p_unexpected)
          {
            nm_core_unpack_unexpected(p_core, &p_unpack, p_unexpected);
          }
      }
  }
  nm_core_lock_assert(p_core);
  nm_core_task_free(&nm_so_schedule.core_task_allocator, p_core_task);
}

/** mark 'chunk_len' data as received in the given unpack, and check for unpack completion
 * sets pp_unpack to NULL if finalized (req may have been destroyed by user handler) */
static inline void nm_core_unpack_check_completion(struct nm_core*p_core, struct nm_pkt_wrap_s*p_pw, struct nm_req_s**pp_unpack,
                                                   nm_len_t chunk_offset, nm_len_t chunk_len)
{
  nm_core_lock_assert(p_core);
  struct nm_req_s*p_unpack = *pp_unpack;
  NM_TRACEF("p_unpack = %p; cumulated_len = %ld; chunk_len = %ld; tag = %lx:%x\n",
            p_unpack, p_unpack->unpack.cumulated_len, chunk_len, p_unpack->tag.tag, p_unpack->tag.hashcode);
  if(p_unpack->unpack.cumulated_len == 0)
    {
      nm_trace_event(NM_TRACE_EVENT_CORE_UNPACK_MATCH_FIRST, p_unpack, p_pw->p_gate, p_pw->p_drv, p_unpack->tag, p_unpack->seq, p_unpack->unpack.expected_len);
    }
  p_unpack->unpack.cumulated_len += chunk_len;
  if(p_unpack->flags & NM_REQ_FLAG_UNPACK_PARTITIONED)
    {
      struct nm_req_pchunk_s*p_pchunk = padico_malloc(sizeof(struct nm_req_pchunk_s));
      p_pchunk->p_next = p_unpack->unpack.partition.p_pchunks;
      p_pchunk->chunk_offset = chunk_offset;
      p_pchunk->chunk_len = chunk_len;
      p_unpack->unpack.partition.p_pchunks = p_pchunk; /* no need for atomics, since we hold nm_core_lock */
      __sync_synchronize(); /* sync for nm_core_unpack_partition_test() */
    }
  assert(p_unpack->unpack.matching.p_wildcard != NULL);
  struct nm_matching_container_s matching_cache = p_unpack->unpack.matching; /* save matching container before it disappear when request is finalized */
  if(p_unpack->unpack.cumulated_len == p_unpack->unpack.expected_len)
    {
      const nm_req_flag_t flags = p_unpack->flags; /* save flags to keep access after FINALIZED has been signaled */
      NM_TRACEF("COMPLETION\n");
      nm_core_unpack_matching_remove(p_core, p_unpack);
      struct nm_core_event_s event =
        {
          .status = NM_STATUS_UNPACK_COMPLETED,
          .p_req = p_unpack
        };
      if(!(flags & NM_REQ_FLAG_FINALIZE_LATER))
        {
          event.status |= NM_STATUS_FINALIZED;
        }
      nm_trace_event(NM_TRACE_EVENT_CORE_UNPACK_COMPLETED, *pp_unpack, p_pw->p_gate, p_pw->p_drv, p_unpack->tag, p_unpack->seq, p_unpack->unpack.expected_len);
      nm_core_status_event(p_core, &event, p_unpack);
      p_core->n_unpacks--;
      nm_trace_var(NM_TRACE_EVENT_VAR_N_UNPACKS, p_core->n_unpacks, NULL, NULL);
      nm_core_polling_level(p_core);
      if(!(flags & NM_REQ_FLAG_FINALIZE_LATER))
        {
          *pp_unpack = NULL; /* unpack is finalized; erase pointer so as to never touch it again */
          p_unpack = NULL;
        }
    }
  else if(p_unpack->unpack.cumulated_len > p_unpack->unpack.expected_len)
    {
      NM_FATAL("copied more data than expected in unpack (unpacked = %lu; expected = %lu; chunk = %lu).\n",
               p_unpack->unpack.cumulated_len, p_unpack->unpack.expected_len, chunk_len);
      abort();
    }
  {
    /* check for out-of-order packets that could become ready after this unpack */
    nm_core_lock_assert(p_core);
    nm_so_schedule_lazy_init();
    struct nm_core_task_s*p_core_task = nm_core_task_malloc(&nm_so_schedule.core_task_allocator);
    p_core_task->kind = NM_CORE_TASK_UNPACK_NEXT;
    p_core_task->content.unpack_next.matching = matching_cache;
    nm_core_task_enqueue(p_core, 1, p_core_task);
  }
}

/** store an unexpected chunk of data (data/short_data/rdv) */
static inline void
nm_unexpected_store(struct nm_core*p_core, const struct nm_in_chunk_s*p_chunk,
                    const union nm_header_generic_s*p_header, struct nm_pkt_wrap_s*p_pw,
                    const struct nm_chunk_injector_s*p_injector)
{
  NM_TRACEF("p_pw = %p; p_injector = %p; is_last = 0x%x\n",
            p_pw, p_injector, (p_chunk->flags & NM_PROTO_FLAG_LASTCHUNK) );
#ifdef NMAD_DEBUG
  if(NM_SO_MATCHING_DEBUG)
    {
      /* check invariant: match_full list for this gtag is sorted by seq number
       * Note: other lists are sorted by chronological order; since they contain
       * chunks from different gtags, it doesn't lead to sorting by seq.
       */
      struct nm_gtag_s*p_gtag = nm_gtag_get(&p_chunk->p_gate->tags, p_chunk->tag);
      struct nm_unexpected_s*p_unexpected = NULL;
      nm_seq_t prev_seq = NM_SEQ_NONE;
      puk_list_foreach(nm_unexpected_gtag, p_unexpected, &p_gtag->unexpected)
        {
          assert((prev_seq == NM_SEQ_NONE) ||
                 nm_seq_fuzzy_compare(prev_seq, p_unexpected->chunk.seq) <= 0);
          prev_seq = p_unexpected->chunk.seq;
        }
    }
#endif /* NMAD_DEBUG */
  if(p_pw != NULL)
    {
      if(p_pw->flags & NM_PW_BUF_RECV)
        {
          if(!(p_pw->flags & NM_PW_BUF_MIRROR))
            {
              memcpy(p_pw->buf, p_pw->v[0].iov_base, p_pw->v[0].iov_len);
              p_pw->flags |= NM_PW_BUF_MIRROR;
            }
          const nm_len_t header_offset = ((void*)p_header) - p_pw->v[0].iov_base;
          p_header = ((void*)p_pw->buf) + header_offset;
        }
      nm_pw_ref_inc(p_pw);
      assert(p_pw != NULL);
    }
  struct nm_unexpected_s*p_unexpected = nm_unexpected_alloc(p_core);
  p_unexpected->p_header = p_header;
  p_unexpected->p_pw     = p_pw;
  p_unexpected->chunk    = *p_chunk;
  if(p_injector)
    {
      p_unexpected->injector = *p_injector;
    }
  nm_profile_inc(p_core->profiling.n_unexpected);
  /* insert unexpected chunk in matching lists and cache pointers to hashtable entries
   * gtag list is sorted by seq number; other lists are in chronological order.
   */
  nm_core_lock_assert(p_core);
  nm_matching_container_fill(p_core, &p_unexpected->matching, p_unexpected->chunk.p_gate, p_unexpected->chunk.tag, NULL);

  assert(!nm_unexpected_wildcard_list_contains(&p_unexpected->matching.p_wildcard->unexpected, p_unexpected));
  assert(!nm_unexpected_tag_list_contains(&p_unexpected->matching.p_matching_tag->unexpected, p_unexpected));
  assert(!nm_unexpected_gate_list_contains(&p_unexpected->matching.p_gsession->unexpected, p_unexpected));

  {
    /* insert chunk in wildcard list at the end, and make it percolate up to what its priority aks for */
    nm_unexpected_wildcard_list_t p_list = &p_unexpected->matching.p_wildcard->unexpected;
    nm_unexpected_wildcard_itor_t i = nm_unexpected_wildcard_list_rbegin(p_list);
    /* we can go up in the list as long as tag or gate is different, so as not to break causality */
    while( (i != NULL) &&
           ( (p_unexpected->chunk.priority > i->chunk.priority) &&
             ( (!nm_core_tag_eq(p_unexpected->chunk.tag, i->chunk.tag)) ||
               (p_unexpected->chunk.p_gate != i->chunk.p_gate))))
      {
        i = nm_unexpected_wildcard_list_rnext(i);
      }
    if(i)
      {
        nm_unexpected_wildcard_list_insert_after(p_list, i, p_unexpected);
      }
    else
      {
        nm_unexpected_wildcard_list_push_front(p_list, p_unexpected);
      }
  }
  {
    /* insert chunk in tag list, at the end, and percolate according to priority */
    nm_unexpected_tag_list_t p_list = &p_unexpected->matching.p_matching_tag->unexpected;
    nm_unexpected_tag_itor_t i = nm_unexpected_tag_list_rbegin(p_list);
    /* we can go up in the list as long as gate is different, so as not to break causality */
    while( (i != NULL) &&
           ( (p_unexpected->chunk.priority > i->chunk.priority) &&
             (p_unexpected->chunk.p_gate != i->chunk.p_gate)))
      {
        i = nm_unexpected_tag_list_rnext(i);
      }
    if(i)
      {
        nm_unexpected_tag_list_insert_after(p_list, i, p_unexpected);
      }
    else
      {
        nm_unexpected_tag_list_push_front(p_list, p_unexpected);
      }

  }
  {
    /* insert chunk in gate list, at the end, and percolate according to priority */
    nm_unexpected_gate_list_t p_list = &p_unexpected->matching.p_gsession->unexpected;
    nm_unexpected_gate_itor_t i = nm_unexpected_gate_list_rbegin(p_list);
    /* we can go up in the list as long as tag is different, so as not to break causality */
    while( (i != NULL) &&
           ( (p_unexpected->chunk.priority > i->chunk.priority) &&
             (!nm_core_tag_eq(p_unexpected->chunk.tag, i->chunk.tag))))
      {
        i = nm_unexpected_gate_list_rnext(i);
      }
    if(i)
      {
        nm_unexpected_gate_list_insert_after(p_list, i, p_unexpected);
      }
    else
      {
        nm_unexpected_gate_list_push_front(p_list, p_unexpected);
      }

  }
  {
    /* insert unexpected chunk in gtag list. List is sorted by seq number. No priority
     * is involved here since chunks will be delivered in seq order anyway since tag/gate
     * are fixed. Start from the end of the list since it is expected the right position
     * is on the tail, except when chunks are received out of order */
    nm_unexpected_gtag_list_t p_list = &p_unexpected->matching.p_gtag->unexpected;
    nm_unexpected_gtag_itor_t i = nm_unexpected_gtag_list_rbegin(p_list);
    while((i != NULL) && (p_unexpected->chunk.seq < i->chunk.seq))
      {
        i = nm_unexpected_gtag_list_rnext(i);
      }
    if(i)
      {
        nm_unexpected_gtag_list_insert_after(p_list, i, p_unexpected);
      }
    else
      {
        nm_unexpected_gtag_list_push_front(p_list, p_unexpected);
      }
  }

  if(p_chunk->flags & NM_PROTO_FLAG_LASTCHUNK)
    {
      p_unexpected->msg_len = p_chunk->chunk_offset + p_chunk->chunk_len;
    }
  else if((p_header != NULL) && (p_header->generic.proto_id & NM_PROTO_RDV))
    {
      p_unexpected->msg_len = p_header->rdv.msg_len;
    }
  else if((p_header != NULL) && (p_header->generic.proto_id & NM_PROTO_MSG))
    {
      p_unexpected->msg_len = p_header->msg.msg_len;
    }
  else
    {
      p_unexpected->msg_len = NM_LEN_UNDEFINED;
    }
  /* trigger event *unexpected* */
  if(p_unexpected->msg_len != NM_LEN_UNDEFINED)
    {
      NM_TRACEF("event status UNEXPECTED; seq = %u; tag = %lx:%x; chunk offset = %ld chunk len = %ld\n",
                p_chunk->seq, p_chunk->tag.tag, p_chunk->tag.hashcode, p_chunk->chunk_offset, p_chunk->chunk_len);
      const struct nm_core_event_s event =
        {
          .status = NM_STATUS_UNEXPECTED,
          .p_gate = p_chunk->p_gate,
          .tag    = p_chunk->tag,
          .seq    = p_chunk->seq,
          .len    = p_chunk->chunk_offset + p_chunk->chunk_len
        };
      nm_core_status_event(p_core, &event, NULL);
    }
}

static inline void nm_matching_container_fill(struct nm_core*p_core, struct nm_matching_container_s*p_matching,
                                              struct nm_gate_s*p_gate, nm_core_tag_t tag, struct nm_gtag_s*p_gtag)
{
  assert(p_gate != NULL);
  p_matching->p_wildcard     = nm_matching_wildcard_bytag(p_core, tag);
  p_matching->p_matching_tag = nm_matching_tag_get(&p_core->tag_table, tag);
  p_matching->p_gsession     = nm_matching_gsession_bytag(p_gate, tag);
  p_matching->p_gtag         = (p_gtag != NULL) ? p_gtag : nm_gtag_get(&p_gate->tags, tag);
}

void nm_core_unpack_init(struct nm_core*p_core, struct nm_req_s*p_unpack)
{
  nm_status_init(p_unpack, NM_STATUS_UNPACK_INIT);
  nm_req_list_cell_init(p_unpack);
#ifdef NMAD_DEBUG
  nm_data_null_build(&p_unpack->data);
#endif
  p_unpack->flags   = NM_REQ_FLAG_UNPACK;
  p_unpack->monitor = NM_MONITOR_NULL;
  p_unpack->err     = NM_ESUCCESS;
  p_unpack->unpack.req_seq       = 0;
  p_unpack->unpack.cumulated_len = 0;
  p_unpack->unpack.expected_len  = NM_LEN_UNDEFINED;
  p_unpack->unpack.offset = 0;
  p_unpack->unpack.matching = NM_MATCHING_CONTAINER_NULL;
  p_unpack->unpack.partition.n_partitions = 0;
  p_unpack->unpack.p_prefetch_pw = NULL;
}

void nm_core_unpack_data(struct nm_core*p_core, struct nm_req_s*p_unpack, const struct nm_data_s*p_data)
{
  NM_TRACEF("p_unpack = %p\n", p_unpack);
#ifdef NMAD_DEBUG
  assert(nm_data_isnull(&p_unpack->data));
#endif /* NMAD_DEBUG */
  assert(!(p_unpack->flags & NM_REQ_FLAG_UNPACK_DATA_INFO));
  assert((p_unpack->unpack.offset == 0) || (nm_data_size(p_data) >= p_unpack->unpack.offset));
  if(nm_status_test(p_unpack, NM_STATUS_UNPACK_POSTED))
    {
      /* req posted- lock & check for unexpected data */
      nm_core_lock(p_core);
      p_unpack->data = *p_data;
      p_unpack->unpack.expected_len = nm_data_size(p_data);
      p_unpack->flags |= NM_REQ_FLAG_UNPACK_DATA_INFO;
      if(nm_status_test(p_unpack, NM_STATUS_UNPACK_DATA0))
        {
          struct nm_unexpected_s*p_unexpected = nm_unexpected_find_matching(p_core, p_unpack);
          while(p_unexpected)
            {
              NM_TRACEF("p_unpack = %p; data already present; p_unexpected = %p\n", p_unpack, p_unexpected);
              nm_core_unpack_unexpected(p_core, &p_unpack, p_unexpected);
              if((p_unpack != NULL) && ((p_unpack->err != NM_ESUCCESS) ||
                                        (nm_status_test(p_unpack, NM_STATUS_UNPACK_COMPLETED))))
                {
                  break;
                }
              p_unexpected = p_unpack ? nm_unexpected_find_matching(p_core, p_unpack) : NULL;
            }
        }
      nm_core_unlock(p_core);
    }
  else
    {
      /* req not posted yet- no lock needed */
      p_unpack->data = *p_data;
      p_unpack->unpack.expected_len = nm_data_size(p_data);
      p_unpack->flags |= NM_REQ_FLAG_UNPACK_DATA_INFO;
    }
}

/** ensure the cache for matching info is resolved
 * @param full whether to resolve all fields or only for the actual kind of request */
static inline void nm_core_unpack_match_resolve(struct nm_core*p_core, struct nm_req_s*p_unpack)
{
  nm_core_lock_assert(p_core);
  assert(p_unpack->flags & NM_REQ_FLAG_UNPACK_MATCHING_INFO);
  if(p_unpack->flags & NM_REQ_FLAG_MATCHING_FULL)
    {
      if(p_unpack->p_gtag == NULL)
        {
          p_unpack->p_gtag = nm_gtag_get(&p_unpack->p_gate->tags, p_unpack->tag);
        }
    }
  else if(p_unpack->flags & NM_REQ_FLAG_MATCHING_TAG)
    {
      if(p_unpack->unpack.matching.p_matching_tag == NULL)
        {
          p_unpack->unpack.matching.p_matching_tag = nm_matching_tag_get(&p_core->tag_table, p_unpack->tag);
        }
    }
  else if(p_unpack->flags & NM_REQ_FLAG_MATCHING_GATE)
    {
      if(p_unpack->unpack.matching.p_gsession == NULL)
        {
          p_unpack->unpack.matching.p_gsession = nm_matching_gsession_bytag(p_unpack->p_gate, p_unpack->tag);
        }
    }
  else if(p_unpack->flags & NM_REQ_FLAG_MATCHING_WILDCARD)
    {
      if(p_unpack->unpack.matching.p_wildcard == NULL)
        {
          p_unpack->unpack.matching.p_wildcard = nm_matching_wildcard_bytag(p_core, p_unpack->tag);
        }
    }
  else
    {
      NM_FATAL("matching method not specified.\n");
    }
}

/** remove request from matching lists */
static inline void nm_core_unpack_matching_remove(struct nm_core*p_core, struct nm_req_s*p_unpack)
{
  NM_TRACEF("p_unpack = %p; REMOVE\n", p_unpack);
  nm_core_lock_assert(p_core);
  assert(p_unpack->flags & NM_REQ_FLAG_UNPACK_MATCHING_INFO);
  if(nm_status_test(p_unpack, NM_STATUS_UNPACK_POSTED))
    {
      if(p_unpack->flags & NM_REQ_FLAG_MATCHING_WILDCARD)
        {
          nm_req_list_remove(&p_unpack->unpack.matching.p_wildcard->unpacks, p_unpack);
        }
      else if(p_unpack->flags & NM_REQ_FLAG_MATCHING_TAG)
        {
          nm_req_list_remove(&p_unpack->unpack.matching.p_matching_tag->unpacks, p_unpack);
        }
      else if(p_unpack->flags & NM_REQ_FLAG_MATCHING_GATE)
        {
          nm_req_list_remove(&p_unpack->unpack.matching.p_gsession->unpacks, p_unpack);
        }
      else if(p_unpack->flags & NM_REQ_FLAG_MATCHING_FULL)
        {
          nm_req_list_remove(&p_unpack->p_gtag->unpacks, p_unpack);
        }
      else
        {
          NM_FATAL("unspecified matching method.\n");
        }
    }
  p_unpack->flags &= ~(NM_REQ_FLAG_UNPACK_MATCHING_INFO |
                       NM_REQ_FLAG_MATCHING_WILDCARD |
                       NM_REQ_FLAG_MATCHING_TAG |
                       NM_REQ_FLAG_MATCHING_GATE |
                       NM_REQ_FLAG_MATCHING_FULL);
}

/** set the request in error state */
static void nm_core_unpack_set_error(struct nm_core*p_core, struct nm_req_s*p_unpack, int err)
{
  nm_core_lock_assert(p_core);
  nm_core_unpack_matching_remove(p_core, p_unpack);
  p_unpack->err = err;
  const struct nm_core_event_s event =
    {
     .status = NM_STATUS_ERROR | NM_STATUS_FINALIZED,
     .p_req  = p_unpack
    };
  nm_core_status_event(p_core, &event, p_unpack);
}

/** fill-in the unpack request with matching information; may be called from
 * different contexts: when user gives matching info or when the request
 * matches a chunk of data (unexpected, when posting, or in event handler)
 * p_matching_cache allows to reuse pre-computing matching container; may be NULL if still unknown */
static inline void nm_core_unpack_match_fill(struct nm_core*p_core, struct nm_req_s*p_unpack,
                                             const struct nm_matching_container_s*p_matching_cache,
                                             nm_gate_t p_gate, nm_core_tag_t tag, nm_core_tag_t tag_mask, nm_seq_t seq)
{
  int need_requeue = 0;
  assert(nm_status_test(p_unpack, NM_STATUS_UNPACK_INIT));
  if((p_unpack->flags & NM_REQ_FLAG_UNPACK_MATCHING_INFO) &&
     !((p_unpack->flags & NM_REQ_FLAG_MATCHING_FULL)))
    {
      nm_core_unpack_matching_remove(p_core, p_unpack);
      need_requeue = nm_status_test(p_unpack, NM_STATUS_UNPACK_POSTED);
    }
  p_unpack->p_gate = p_gate;
  p_unpack->tag    = tag;
  p_unpack->seq    = seq;
  p_unpack->p_gtag = p_matching_cache ? p_matching_cache->p_gtag : NULL;
  p_unpack->unpack.tag_mask = tag_mask;
  if(p_matching_cache)
    {
      p_unpack->unpack.matching = *p_matching_cache;
    }
  if(!(p_unpack->flags & NM_REQ_FLAG_UNPACK_MATCHING_INFO))
    {
      if((p_unpack->p_gate != NM_GATE_NONE) && (p_unpack->unpack.tag_mask.tag == NM_TAG_MASK_FULL))
        {
          p_unpack->flags |= NM_REQ_FLAG_UNPACK_MATCHING_INFO | NM_REQ_FLAG_MATCHING_FULL;
        }
      else if((p_unpack->p_gate == NM_GATE_NONE) && (p_unpack->unpack.tag_mask.tag == NM_TAG_MASK_FULL))
        {
          p_unpack->flags |= NM_REQ_FLAG_UNPACK_MATCHING_INFO | NM_REQ_FLAG_MATCHING_TAG;
        }
      else if((p_unpack->p_gate != NM_GATE_NONE) && (p_unpack->unpack.tag_mask.tag != NM_TAG_MASK_FULL))
        {
          p_unpack->flags |= NM_REQ_FLAG_UNPACK_MATCHING_INFO | NM_REQ_FLAG_MATCHING_GATE;
        }
      else if((p_unpack->p_gate == NM_GATE_NONE) && (p_unpack->unpack.tag_mask.tag != NM_TAG_MASK_FULL))
        {
          p_unpack->flags |= NM_REQ_FLAG_UNPACK_MATCHING_INFO | NM_REQ_FLAG_MATCHING_WILDCARD;
        }
      else
        {
          NM_FATAL("matching method not specified.\n");
        }
    }
  if(need_requeue)
    {
      /* re-queue request in the right list; when matching is done a second time,
      * we are sure that we are in the match full case and gtag is known, since
      * the only reason to do it twice is upon packet arrival */
      assert(p_unpack->flags & NM_REQ_FLAG_MATCHING_FULL);
      assert(p_unpack->p_gtag != NULL);
      if(nm_req_list_empty(&p_unpack->p_gtag->unpacks))
        {
          nm_req_list_push_front(&p_unpack->p_gtag->unpacks, p_unpack);
        }
      else
        {
          struct nm_req_s*p_itor = nm_req_list_begin(&p_unpack->p_gtag->unpacks);
          if(p_unpack->unpack.req_seq < p_itor->unpack.req_seq)
          {
            nm_req_list_push_front(&p_unpack->p_gtag->unpacks, p_unpack);
          }
          for(;;)
            {
              struct nm_req_s*p_next = nm_req_list_next(p_itor);
              if(p_next == NULL)
                {
                  nm_req_list_push_back(&p_unpack->p_gtag->unpacks, p_unpack);
                  break;
                }
              else if((p_unpack->unpack.req_seq > p_itor->unpack.req_seq) &&
                      (p_unpack->unpack.req_seq < p_next->unpack.req_seq))
                {
                  nm_req_list_insert_after(&p_unpack->p_gtag->unpacks, p_itor, p_unpack);
                  break;
                }
              else
                {
                  p_itor = p_next;
                }
            }
        }
      NM_TRACEF("p_unpack = %p; MATCHING FULL- p_list = %p\n", p_unpack, &p_unpack->p_gtag->unpacks);
    }

}

/** Match an unpack request with an event.
 */
void nm_core_unpack_match_event(struct nm_core*p_core, struct nm_req_s*p_unpack, const struct nm_core_event_s*p_event)
{
  nm_status_assert(p_unpack, NM_STATUS_UNPACK_INIT);
  nm_core_unpack_match_fill(p_core, p_unpack, NULL, p_event->p_gate, p_event->tag, NM_CORE_TAG_MASK_FULL, p_event->seq);
}

/** Fill matching info in an unpack with regular gate/tag/mask info
 */
void nm_core_unpack_match_recv(struct nm_core*p_core, struct nm_req_s*p_unpack, nm_gate_t p_gate,
                               nm_core_tag_t tag, nm_core_tag_t tag_mask)
{
  nm_status_assert(p_unpack, NM_STATUS_UNPACK_INIT);
  nm_core_unpack_match_fill(p_core, p_unpack, NULL, p_gate, tag, tag_mask, NM_SEQ_NONE);
}

int nm_core_unpack_iprobe(struct nm_core*p_core, struct nm_req_s*p_unpack)
{
  NM_TRACEF("p_unpack = %p\n", p_unpack);
  if(!(p_unpack->flags & NM_REQ_FLAG_UNPACK_MATCHING_INFO))
    {
      NM_WARN("cannot iprobe request without matching info.\n");
      return -NM_EINVAL;
    }
  if(nm_status_test(p_unpack, NM_STATUS_UNPACK_POSTED))
    {
      NM_WARN("cannot iprobe posted request.\n");
      return -NM_EINVAL;
    }
  nm_core_lock(p_core);
  struct nm_unexpected_s*p_unexpected = nm_unexpected_find_matching(p_core, p_unpack);
  nm_len_t out_len = NM_LEN_UNDEFINED;
  if(p_unexpected != NULL)
    {
      if(p_unexpected != NULL)
        p_unexpected = nm_unexpected_find_last(p_core, p_unexpected);
      if(p_unexpected != NULL)
        {
          out_len = nm_unexpected_get_len(p_core, p_unexpected);
        }
    }
  nm_core_unlock(p_core);
  if((p_unexpected == NULL) || (out_len == NM_LEN_UNDEFINED))
    {
      nm_schedule(p_core);
      return -NM_EAGAIN;
    }
  else
    {
      assert(p_unpack->unpack.expected_len == NM_LEN_UNDEFINED);
      p_unpack->unpack.expected_len = out_len;
      return NM_ESUCCESS;
    }
}

int nm_core_unpack_peek(struct nm_core*p_core, struct nm_req_s*p_unpack, const struct nm_data_s*p_data,
                        nm_len_t peek_offset, nm_len_t peek_len)
{
  if(!(p_unpack->flags & NM_REQ_FLAG_UNPACK_MATCHING_INFO))
    {
      NM_WARN("cannot peek request without matching info.\n");
      return -NM_EINVAL;
    }
  if( (p_unpack->seq == NM_SEQ_NONE) ||
      (p_unpack->p_gate == NM_GATE_NONE) )
    {
      NM_WARN("cannot peek request: no data.\n");
      return -NM_EINVAL;
    }
  const nm_len_t data_len = nm_data_size(p_data);
  if(peek_offset + peek_len > data_len)
    {
      NM_WARN("cannot peek: chunk offset/len not consistent with data size; data size = %ld; peek_offset = %ld; peek_len = %ld.\n",
              data_len, peek_offset, peek_len);
      return -NM_EINVAL;
    }
  nm_len_t done = 0;
  nm_core_lock(p_core);
  struct nm_unexpected_s*p_unexpected = NULL;
  struct nm_gtag_s*p_gtag = nm_gtag_get(&p_unpack->p_gate->tags, p_unpack->tag);
  puk_list_foreach(nm_unexpected_gtag, p_unexpected, &p_gtag->unexpected)
    {
      assert(p_unpack->p_gate == p_unexpected->chunk.p_gate);
      assert(nm_core_tag_match(p_unexpected->chunk.tag, p_unpack->tag, p_unpack->unpack.tag_mask));
      if(p_unpack->seq == p_unexpected->chunk.seq)
        {
          nm_len_t chunk_offset = p_unexpected->chunk.chunk_offset;
          nm_len_t chunk_len    = p_unexpected->chunk.chunk_len;
          nm_len_t ptr_offset = 0; /* offset to apply to data pointer */
          if( (!(chunk_offset + chunk_len <= peek_offset))
              &&
              (!(peek_offset + peek_len <= chunk_offset)) )
            {
              if(chunk_offset + chunk_len > peek_offset + peek_len)
                {
                  /* truncate chunk end */
                  chunk_len = peek_offset + peek_len - chunk_offset;
                }
              if(peek_offset > chunk_offset)
                {
                  /* truncate chunk begin */
                  ptr_offset = peek_offset - chunk_offset;
                  chunk_len -= (peek_offset - chunk_offset);
                  chunk_offset = peek_offset;
                }
              assert((p_unexpected->p_pw != NULL) ^ (p_unexpected->injector.p_pull_data != NULL));
              if(p_unexpected->p_pw)
                {
                  const nm_header_data_generic_t*p_header = (const nm_header_data_generic_t*)p_unexpected->p_header;
                  const nm_proto_t proto_id = p_header->generic.proto_id & NM_PROTO_ID_MASK;
                  switch(proto_id)
                    {
                    case NM_PROTO_PKT_DATA:
                      {
                        nm_data_pkt_unpack(p_data, &p_header->pkt_data, p_unexpected->p_pw, chunk_offset, chunk_len);
                        done += chunk_len;
                      }
                      break;
                    case NM_PROTO_SHORT_DATA:
                      {
                        const void*ptr = ((void*)p_header) + NM_HEADER_SHORT_DATA_SIZE + ptr_offset;
                        nm_data_copy_to(p_data, chunk_offset, chunk_len, ptr);
                        done += chunk_len;
                      }
                      break;
                    case NM_PROTO_SMALL_DATA:
                      {
                        const void*ptr = ((p_header->data.skip == 0xFFFF) ? (((void*)p_header) + NM_HEADER_DATA_SIZE) :
                                          p_unexpected->p_pw->v[0].iov_base + (p_header->data.skip + nm_header_global_v0len(p_unexpected->p_pw)))
                          + ptr_offset;
                        nm_data_copy_to(p_data, chunk_offset, chunk_len, ptr);
                        done += chunk_len;
                      }
                      break;
                    case NM_PROTO_MSG:
                      {
                        /* nothing to do */
                      }
                      break;
                    case NM_PROTO_RDV:
                      {
                        NM_FATAL("# nmad: nm_core_unpack_peek()- not implemented for large messages. Use nm_sr_send_header() to send header eagerly.\n");
                      }
                      break;
                    default:
                      NM_FATAL("unknown proto_id %d", proto_id);
                      break;
                    }
                }
              else
                {
                  const struct nm_chunk_injector_s*p_injector = &p_unexpected->injector;
                  const int offset = peek_offset;
                  nm_core_unlock(p_core);
                  (*p_injector->p_pull_data)(p_unpack, p_data, offset, chunk_len, p_injector->p_ref);
                  nm_core_lock(p_core);
                  done += chunk_len;
                }
            }
        }
      if(done == peek_len)
        break;
      else if(done > peek_len)
        {
          NM_FATAL("nm_core_unpack_peek()- copyed more data than needed. Internal error.\n");
        }
    }
  nm_core_unlock(p_core);
  if(done == peek_len)
    {
      return NM_ESUCCESS;
    }
  else
    {
      NM_WARN("incomplete data- peek_offset = %ld; peek_len = %ld; done = %ld\n", peek_offset, peek_len, done);
      return -NM_EAGAIN;
    }
}

void nm_core_unpack_offset(struct nm_core*p_core, struct nm_req_s*p_unpack, nm_len_t offset)
{
  assert(p_unpack->flags & NM_REQ_FLAG_UNPACK_MATCHING_INFO); /* request must be matched to set offset */
  assert(!(p_unpack->flags & NM_REQ_FLAG_UNPACK_DATA_INFO)); /* cannot set offset once data layout is set */
  assert(offset >= p_unpack->unpack.offset); /* cannot receive data that was first ignored */
  assert(p_unpack->unpack.cumulated_len == 0); /* too late to set offset once data receive as begun */
  p_unpack->unpack.offset = offset;
}

/** unpack an unexpected chunk to a matched req */
static void nm_core_unpack_unexpected(struct nm_core*p_core, struct nm_req_s**pp_unpack, struct nm_unexpected_s*p_unexpected)
{
  struct nm_req_s*p_unpack = *pp_unpack;
  NM_TRACEF("p_unexpected = %p; len = %ld; seq = %u; tag = %lx:%x; chunk_len = %ld REMOVE\n",
              p_unexpected, p_unexpected->msg_len, p_unexpected->chunk.seq,
              p_unexpected->chunk.tag.tag, p_unexpected->chunk.tag.hashcode, p_unexpected->chunk.chunk_len);
  /* remove unexpected chunk from lists early */
  if(!p_unexpected->matched)
    {
      nm_unexpected_wildcard_list_remove(&p_unexpected->matching.p_wildcard->unexpected, p_unexpected);
      nm_unexpected_tag_list_remove(&p_unexpected->matching.p_matching_tag->unexpected, p_unexpected);
      nm_unexpected_gate_list_remove(&p_unexpected->matching.p_gsession->unexpected, p_unexpected);
    }
  nm_unexpected_gtag_list_remove(&p_unexpected->matching.p_gtag->unexpected, p_unexpected);

  if(p_unexpected->p_pw)
    {
      const void*p_header = p_unexpected->p_header;
      const nm_proto_t proto_id = p_unexpected->p_header->generic.proto_id & NM_PROTO_ID_MASK;
      switch(proto_id)
        {
        case NM_PROTO_PKT_DATA:
          {
            const struct nm_header_pkt_data_s*h = p_header;
            nm_pkt_data_handler(p_core, p_unpack->p_gate, pp_unpack, h, p_unexpected->p_pw);
          }
          break;
        case NM_PROTO_SHORT_DATA:
          {
            const struct nm_header_short_data_s*h = p_header;
            nm_short_data_handler(p_core, p_unpack->p_gate, pp_unpack, h, p_unexpected->p_pw);
          }
          break;
        case NM_PROTO_SMALL_DATA:
          {
            const struct nm_header_data_s*h = p_header;
            nm_small_data_handler(p_core, p_unpack->p_gate, pp_unpack, h, p_unexpected->p_pw);
          }
          break;
        case NM_PROTO_MSG:
          {
            const struct nm_header_msg_s*h = p_header;
            nm_msg_handler(p_core, p_unpack->p_gate, pp_unpack, h, p_unexpected->p_pw);
          }
          break;
        case NM_PROTO_RDV:
          {
            const struct nm_header_ctrl_rdv_s*h = p_header;
            nm_rdv_handler(p_core, p_unpack->p_gate, p_unpack, h, p_unexpected->p_pw);
          }
          break;
        }
      /* Decrement the packet wrapper reference counter. If no other
         chunks are still in use, the pw will be destroyed. */
      nm_pw_ref_dec(p_unexpected->p_pw);
    }
  else
    {
      NM_TRACEF("INJECT; p_prefetch_pw = %p\n", p_unpack->unpack.p_prefetch_pw);
      assert(p_unexpected->injector.p_pull_data);
      nm_inject_data_handler(p_core, pp_unpack, &p_unexpected->chunk, &p_unexpected->injector);
    }
  nm_core_lock_assert(p_core);
  nm_unexpected_free(&nm_so_schedule.unexpected_allocator, p_unexpected);
}

/** Store the unpack request. */
void nm_core_unpack_submit(struct nm_core*p_core, struct nm_req_s*p_unpack, nm_req_flag_t flags)
{
  nm_status_assert(p_unpack, NM_STATUS_UNPACK_INIT);
  assert(p_unpack->flags & NM_REQ_FLAG_UNPACK_MATCHING_INFO);
  NM_TRACEF("p_unpack = %p; p_gate = %p; tag = %lx:%x; seq = %u; p_prefetch_pw = %p\n",
            p_unpack, p_unpack->p_gate, p_unpack->tag.tag, p_unpack->tag.hashcode, p_unpack->seq,
            p_unpack->unpack.p_prefetch_pw);
  nm_status_add(p_unpack, NM_STATUS_UNPACK_POSTED);
  p_unpack->flags |= flags;
  nm_core_lock(p_core);
  nm_core_unpack_match_resolve(p_core, p_unpack);
  assert(p_unpack->unpack.req_seq == 0);
  p_unpack->unpack.req_seq = p_core->unpack_seq;
  p_core->unpack_seq++;
  assert(p_unpack->unpack.tag_mask.hashcode == NM_CORE_TAG_HASH_FULL);
  if(p_unpack->flags & NM_REQ_FLAG_MATCHING_FULL)
    {
      nm_req_list_push_back(&p_unpack->p_gtag->unpacks, p_unpack);
      NM_TRACEF("p_unpack = %p; MATCHING FULL- p_list = %p\n", p_unpack, &p_unpack->p_gtag->unpacks);
    }
  else if(p_unpack->flags & NM_REQ_FLAG_MATCHING_TAG)
    {
      nm_req_list_push_back(&p_unpack->unpack.matching.p_matching_tag->unpacks, p_unpack);
      NM_TRACEF("p_unpack = %p; MATCHING TAG- p_list = %p\n", p_unpack, &p_unpack->unpack.matching.p_matching_tag->unpacks);
    }
  else if(p_unpack->flags & NM_REQ_FLAG_MATCHING_GATE)
    {
      nm_req_list_push_back(&p_unpack->unpack.matching.p_gsession->unpacks, p_unpack);
      NM_TRACEF("p_unpack = %p; MATCHING GATE- p_list = %p\n", p_unpack, &p_unpack->unpack.matching.p_gsession->unpacks);
    }
  else if(p_unpack->flags & NM_REQ_FLAG_MATCHING_WILDCARD)
    {
      nm_req_list_push_back(&p_unpack->unpack.matching.p_wildcard->unpacks, p_unpack);
      NM_TRACEF("p_unpack = %p; MATCHING WILDCARD- p_list = %p\n", p_unpack, &p_unpack->unpack.matching.p_wildcard->unpacks);
    }
  else
    {
      NM_FATAL("matching method not specified.\n");
    }
  nm_profile_inc(p_core->profiling.n_unpacks);
  nm_trace_event(NM_TRACE_EVENT_CORE_UNPACK_SUBMIT, p_unpack, p_unpack->p_gate, NULL, p_unpack->tag, p_unpack->seq, p_unpack->unpack.expected_len);
  p_core->n_unpacks++;
  nm_trace_var(NM_TRACE_EVENT_VAR_N_UNPACKS, p_core->n_unpacks, NULL, NULL);
#ifdef NMAD_PROFILE
  if(p_core->n_unpacks > p_core->profiling.max_unpacks)
    p_core->profiling.max_unpacks = p_core->n_unpacks;
#endif /* NMAD_PROFILE */
  nm_core_polling_level(p_core);
  struct nm_unexpected_s*p_unexpected = nm_unexpected_find_matching(p_core, p_unpack);
  if(p_unexpected == NULL)
    {
      /* try to prefetch large message */
      const struct nm_data_properties_s*p_props = nm_data_properties_get(&p_unpack->data);
      if( (p_unpack->flags & NM_REQ_FLAG_UNPACK_DATA_INFO) &&
          (nm_data_size(&p_unpack->data) > NM_MAX_UNEXPECTED) &&
          (p_core->enable_recv_prefetch) &&
          (p_unpack->p_gate != NULL) &&
          (p_unpack->p_gate->n_trks == 2) &&
          (p_unpack->p_gate->trks[NM_TRK_LARGE].p_drv->props.capabilities.supports_recv_prefetch) &&
          ( (p_unpack->p_gate->trks[NM_TRK_LARGE].p_drv->props.capabilities.supports_iovec &&
             ((p_unpack->p_gate->trks[NM_TRK_LARGE].p_drv->props.capabilities.max_iovecs == 0) ||
              (p_unpack->p_gate->trks[NM_TRK_LARGE].p_drv->props.capabilities.max_iovecs > p_props->blocks)))
            || p_props->is_contig) )
        {
          /* mark for prefetching- prefetching will be done in nm_pw_recv_progress() */
          p_unpack->flags |= NM_REQ_FLAG_UNPACK_PREFETCHING;
          struct nm_pkt_wrap_s*p_large_pw = nm_pw_alloc_noheader(p_core);
          p_large_pw->p_unpack = p_unpack;
          p_large_pw->p_gate   = p_unpack->p_gate;
          nm_pw_set_data_raw(p_large_pw, &p_unpack->data, nm_data_size(&p_unpack->data), 0);
          p_unpack->unpack.p_prefetch_pw = p_large_pw;
          NM_TRACEF("new pw for PREFETCH; p_large_pw = %p\n", p_large_pw);
        }
    }
  while(p_unexpected != NULL)
    {
      if(!(p_unpack->flags & NM_REQ_FLAG_UNPACK_DATA_INFO))
        {
          /* data spec still undefined- check whether we have first/last bytes, then fire event(s) */
          NM_TRACEF("p_unpack = %p; no DATA_INFO; p_unexpected = %p\n", p_unpack, p_unexpected);
          nm_status_t status = NM_STATUS_NONE;
          struct nm_gtag_s*p_gtag = p_unpack->p_gtag;
          assert(p_gtag != NULL);
          assert(p_unpack->seq != NM_SEQ_NONE);
          /* walk through all chunks on the same gate/tag/seq */
          while( (p_unexpected != NULL) && (p_unexpected->chunk.seq == p_unpack->seq) )
            {
              if((p_unexpected->chunk.chunk_offset == 0) && !nm_status_test(p_unpack, NM_STATUS_UNPACK_DATA0))
                {
                  status |= NM_STATUS_UNPACK_DATA0;
                }
              const nm_len_t out_len = nm_unexpected_get_len(p_core, p_unexpected);
              if(out_len != NM_LEN_UNDEFINED)
                {
                  p_unpack->unpack.expected_len = out_len;
                  status |= NM_STATUS_UNPACK_DATA_SIZE;
                }
              p_unexpected = nm_unexpected_gtag_list_next(p_unexpected);
            }
          if(status != NM_STATUS_NONE)
            {
              /* fire data event(s) */
              const struct nm_core_event_s event =
                {
                 .status = status,
                 .p_req = p_unpack
                };
              nm_core_status_event(p_core, &event, p_unpack);
            }
          /* process event then exit (since other chunks cannot be received yet) */
          break;
        }
      else
        {
          /* data is already here (at least one chunk)- process all matching chunks */
          NM_TRACEF("p_unpack = %p; data is already present; p_unexpected = %p\n", p_unpack, p_unexpected);
          nm_core_unpack_unexpected(p_core, &p_unpack, p_unexpected);
          if((p_unpack != NULL) && ((p_unpack->err != NM_ESUCCESS) ||
                                    (nm_status_test(p_unpack, NM_STATUS_UNPACK_COMPLETED))))
            {
              break;
            }
          p_unexpected = p_unpack ? nm_unexpected_find_matching(p_core, p_unpack) : NULL;
        }
    }
  nm_core_unlock(p_core);
  if((p_unpack != NULL) && (p_unpack->unpack.p_prefetch_pw))
    {
#ifndef PIOMAN
      nm_pw_recv_prefetch(p_unpack->unpack.p_prefetch_pw);
#else /* PIOMAN */
      nm_ltask_submit_pw_recv_prefetch(p_unpack->unpack.p_prefetch_pw);
#endif /* PIOMAN */
    }
}

int nm_core_iprobe(struct nm_core*p_core,
                   nm_gate_t p_gate, nm_core_tag_t tag, nm_core_tag_t tag_mask,
                   nm_gate_t *pp_out_gate, nm_core_tag_t*p_out_tag, nm_len_t*p_out_size)
{
  int rc = -NM_EAGAIN;
  NM_TRACEF("p_gate = %p; tag = %lx:%x; tasg_mask = %lx:%x\n", p_gate, tag.tag, tag.hashcode, tag_mask.tag, tag_mask.hashcode);
  /* create an ephemeral request for matching */
  struct nm_req_s unpack;
  nm_core_unpack_init(p_core, &unpack);
  nm_core_unpack_match_recv(p_core, &unpack, p_gate, tag, tag_mask);
  nm_core_lock(p_core);
  struct nm_unexpected_s*p_unexpected = nm_unexpected_lookup(p_core, &unpack);
  if(p_unexpected != NULL)
    p_unexpected = nm_unexpected_find_last(p_core, p_unexpected);
  if(p_unexpected != NULL)
    {
      if(pp_out_gate)
        *pp_out_gate = p_unexpected->chunk.p_gate;
      if(p_out_tag)
        *p_out_tag = p_unexpected->chunk.tag;
      if(p_out_size)
        *p_out_size = nm_unexpected_get_len(p_core, p_unexpected);
      rc = NM_ESUCCESS;
      goto out;
    }
  *pp_out_gate = NM_ANY_GATE;
 out:
  nm_core_unlock(p_core);
  return rc;
}

int nm_core_unpack_cancel(struct nm_core*p_core, struct nm_req_s*p_unpack)
{
  int rc = NM_ESUCCESS;
  nm_core_lock(p_core);
  if(nm_status_test(p_unpack, NM_STATUS_UNPACK_CANCELLED))
    {
      /* received already canacelled */
      rc = -NM_ECANCELED;
    }
  else if(nm_status_test(p_unpack, NM_STATUS_UNPACK_COMPLETED))
    {
      /* receive already completed */
      rc = -NM_EALREADY;
    }
  else if(!nm_status_test(p_unpack, NM_STATUS_UNPACK_POSTED))
    {
      /* request was not posted yet */
      rc = -NM_ENOTPOSTED;
    }
  else if(p_unpack->seq == NM_SEQ_NONE)
    {
      nm_core_unpack_matching_remove(p_core, p_unpack);
      nm_core_polling_level(p_core);
      const struct nm_core_event_s event =
        {
          .status = NM_STATUS_UNPACK_CANCELLED | NM_STATUS_FINALIZED,
          .p_req = p_unpack
        };
      nm_core_status_event(p_core, &event, p_unpack);
      rc = NM_ESUCCESS;
    }
  else
    {
      /* receive is already in progress- too late to cancel */
      rc = -NM_EINPROGRESS;
    }
  nm_core_unlock(p_core);
  nm_core_events_dispatch(p_core);
  return rc;
}

/* ** partitioned unpack */

void nm_core_unpack_partition_set(struct nm_req_s*p_unpack, int n_partitions)
{
  assert(p_unpack->unpack.partition.n_partitions == 0);
  assert(!nm_status_test(p_unpack, NM_STATUS_UNPACK_POSTED));
  p_unpack->flags |= NM_REQ_FLAG_UNPACK_PARTITIONED;
  p_unpack->unpack.partition.n_partitions = n_partitions;
  p_unpack->unpack.partition.p_pchunks = NULL;
}

void nm_core_unpack_partition_free(struct nm_req_s*p_unpack)
{
  assert(p_unpack->flags & NM_REQ_FLAG_UNPACK_PARTITIONED);
  struct nm_req_pchunk_s*p_pchunk = p_unpack->unpack.partition.p_pchunks;
  while(p_pchunk != NULL)
    {
      struct nm_req_pchunk_s*p_next = p_pchunk->p_next;
      padico_free(p_pchunk);
      p_pchunk = p_next;
    }
  p_unpack->unpack.partition.p_pchunks = NULL;
}

struct nm_chunk_interval_s
{
  nm_len_t offset;
  nm_len_t len;
};
PUK_VECT_TYPE(nm_chunk_interval, struct nm_chunk_interval_s);

int nm_core_unpack_partition_test(struct nm_req_s*p_unpack, int partition)
{
  assert(p_unpack->flags & NM_REQ_FLAG_UNPACK_PARTITIONED);
  if(!nm_status_test(p_unpack, NM_STATUS_UNPACK_DATA_SIZE))
    {
      return -NM_EAGAIN;
    }
  if(!nm_status_test(p_unpack, NM_STATUS_UNPACK_COMPLETED))
    {
      return NM_ESUCCESS;
    }
  assert(partition >= 0);
  assert(partition < p_unpack->unpack.partition.n_partitions);
  assert(p_unpack->unpack.expected_len % p_unpack->unpack.partition.n_partitions == 0);
  struct nm_chunk_interval_vect_s intervals = PUK_VECT_STATIC_INITIALIZER; /**< list of intervals of missing data */
  struct nm_chunk_interval_s start =
    {
      .offset = partition * (p_unpack->unpack.expected_len / p_unpack->unpack.partition.n_partitions),
      .len    = p_unpack->unpack.expected_len / p_unpack->unpack.partition.n_partitions
    };
  nm_chunk_interval_vect_push_back(&intervals, start);
  /* for every chunk of arrived data, prune list of missing intervals */
  struct nm_req_pchunk_s*p_pchunk = p_unpack->unpack.partition.p_pchunks;
  __sync_synchronize();
  while(p_pchunk != NULL)
    {
      nm_chunk_interval_vect_itor_t i;
    restart: /* in case we removed an entry from vect; iterator may be invalid */
      if(nm_chunk_interval_vect_empty(&intervals))
        break;
      puk_vect_foreach(i, nm_chunk_interval, &intervals)
        {
          if( (p_pchunk->chunk_offset >= i->offset) &&
              (p_pchunk->chunk_offset < i->offset + i->len) &&
              (p_pchunk->chunk_offset + p_pchunk->chunk_len >= i-> offset + i->len))
            {
              /* p begin is inside i; crop end of i */
              i->len = p_pchunk->chunk_offset - i->offset;
            }
          else if( (p_pchunk->chunk_offset + p_pchunk->chunk_len >= i->offset) &&
                   (p_pchunk->chunk_offset + p_pchunk->chunk_len < i->offset + i->len) &&
                   (p_pchunk->chunk_offset <= i->offset) )
            {
              /* p end is inside i; crop begin of i */
              const nm_len_t offset = p_pchunk->chunk_offset + p_pchunk->chunk_len - i->offset;
              i->offset += offset;
              i->len -= offset;
            }
          else if( (p_pchunk->chunk_offset >= i->offset) &&
                   (p_pchunk->chunk_offset + p_pchunk->chunk_len <= i->offset + i->len) )
            {
              /* p is included in i; crop both ends */
              if(p_pchunk->chunk_offset > i->offset)
                {
                  struct nm_chunk_interval_s i1 = { .offset = i->offset, .len = p_pchunk->chunk_offset - i->offset };
                  nm_chunk_interval_vect_push_back(&intervals, i1);
                }
              if(p_pchunk->chunk_offset + p_pchunk->chunk_len < i->offset + i->len)
                {
                  struct nm_chunk_interval_s i2 = {
                    .offset = p_pchunk->chunk_offset + p_pchunk->chunk_len,
                    .len = i->offset + i->len - (p_pchunk->chunk_offset + p_pchunk->chunk_len)
                  };
                  nm_chunk_interval_vect_push_back(&intervals, i2);
                }
              nm_chunk_interval_vect_erase(&intervals, i);
              goto restart;
            }
          else if( (p_pchunk->chunk_offset <= i->offset) &&
                   (p_pchunk->chunk_offset + p_pchunk->chunk_len >= i->offset + i->len) )
            {
              /* i is included in p; erase i */
              nm_chunk_interval_vect_erase(&intervals, i);
              goto restart;
            }
          if(i->len == 0)
            {
              nm_chunk_interval_vect_erase(&intervals, i);
              goto restart;
            }
        }
      p_pchunk = p_pchunk->p_next;
    }
  const int done = nm_chunk_interval_vect_empty(&intervals);
  nm_chunk_interval_vect_destroy(&intervals);
  if(done)
    return NM_ESUCCESS;
  else
    return -NM_EAGAIN;
}


/** checks whether the given unpack request is ready to receive the given chunk of data,
    store chunk as unexpected otherwise */
static inline int nm_core_unpack_match_chunk(struct nm_core*p_core, struct nm_req_s*p_unpack,
                                             const struct nm_in_chunk_s*p_chunk,
                                             const union nm_header_generic_s*p_header,
                                             struct nm_pkt_wrap_s*p_pw,
                                             const struct nm_chunk_injector_s*p_injector)
{
  NM_TRACEF("p_unpack = %p; p_pw = %p; p_injector = %p\n", p_unpack, p_pw, p_injector);
  const nm_len_t chunk_end = p_chunk->chunk_offset + p_chunk->chunk_len;
  if( (p_unpack != NULL) && (!(nm_status_test(p_unpack, NM_STATUS_UNPACK_CANCELLED))) &&
      (p_unpack->flags & NM_REQ_FLAG_UNPACK_DATA_INFO) )
    {
      /* regular unpack with data posted- perform some sanity checks, update size, send ACK */
      NM_TRACEF("p_unpack = %p; p_pw = %p; p_injector = %p MATCHED\n",
                p_unpack, p_pw, p_injector);
      assert(nm_status_test(p_unpack, NM_STATUS_UNPACK_POSTED));
      assert(p_unpack->unpack.expected_len != NM_LEN_UNDEFINED);
      if(chunk_end > p_unpack->unpack.expected_len)
        {
          NM_WARN("received more data than expected-"
                  "   received: chunk offset = %lu; chunk len = %lu; chunk end = %lu\n"
                  "   matched request = %p; expected len = %lu; already received = %lu\n",
                  p_chunk->chunk_offset, p_chunk->chunk_len, chunk_end,
                  p_unpack, p_unpack->unpack.expected_len, p_unpack->unpack.cumulated_len);
          nm_core_unpack_set_error(p_core, p_unpack, -NM_ETRUNCATED);
          return 0;
        }
      assert(chunk_end <= p_unpack->unpack.expected_len);
      assert(p_unpack->unpack.cumulated_len + p_chunk->chunk_len <= p_unpack->unpack.expected_len);
      /* decode flags */
      if(!nm_cond_test(&p_unpack->status, NM_STATUS_UNPACK_DATA_SIZE))
        {
          int flag = 0;
          if(p_chunk->flags & NM_PROTO_FLAG_LASTCHUNK)
            {
              /* last chunk- update the real size to receive */
              p_unpack->unpack.expected_len = chunk_end;
              flag = 1;
            }
          else if((p_header != NULL) && (p_header->generic.proto_id == NM_PROTO_RDV))
            {
              /* rdv- update the real size to receive */
              p_unpack->unpack.expected_len = p_header->rdv.msg_len;
              flag = 1;
            }
          else if((p_header != NULL) && (p_header->generic.proto_id == NM_PROTO_MSG))
            {
              /* rdv- update the real size to receive */
              p_unpack->unpack.expected_len = p_header->msg.msg_len;
              flag = 1;
            }
          if(flag)
            {
              nm_status_add(p_unpack, NM_STATUS_UNPACK_DATA_SIZE);
              nm_trace_event(NM_TRACE_EVENT_CORE_UNPACK_MATCH_LAST, p_unpack, (p_pw ? p_pw->p_gate : NULL), (p_pw ? p_pw->p_drv : NULL), p_unpack->tag, p_unpack->seq, p_unpack->unpack.expected_len);
            }
        }
      if((p_unpack->unpack.cumulated_len == 0) && (p_chunk->flags & NM_PROTO_FLAG_ACKREQ))
        {
          nm_core_post_ack(p_unpack->p_gate, p_unpack->tag, p_unpack->seq);
        }
      return 1;
    }
  nm_unexpected_store(p_core, p_chunk, p_header, p_pw, p_injector);
  if((p_unpack != NULL) && !(p_unpack->flags & NM_REQ_FLAG_UNPACK_DATA_INFO))
    {
      /* unpack posted without data- update size & trigger events */
      NM_TRACEF("p_unpack = %p; p_pw = %p; p_injector = %p; no DATA_INFO; trigger DATA0 status event\n",
                p_unpack, p_pw, p_injector);
      nm_status_t status = NM_STATUS_NONE;
      if((p_chunk->chunk_offset == 0) && !nm_status_test(p_unpack, NM_STATUS_UNPACK_DATA0))
        {
          status |= NM_STATUS_UNPACK_DATA0;
        }
      if(!nm_cond_test(&p_unpack->status, NM_STATUS_UNPACK_DATA_SIZE))
        {
          int flag = 0;
          if(p_chunk->flags & NM_PROTO_FLAG_LASTCHUNK)
            {
              p_unpack->unpack.expected_len = chunk_end;
              flag = 1;
            }
          else if((p_header != NULL) && (p_header->generic.proto_id == NM_PROTO_RDV))
            {
              p_unpack->unpack.expected_len = p_header->rdv.msg_len;
              flag = 1;
            }
          if(flag)
            {
              status |= NM_STATUS_UNPACK_DATA_SIZE;
              nm_trace_event(NM_TRACE_EVENT_CORE_UNPACK_MATCH_LAST, p_unpack, (p_pw ? p_pw->p_gate : NULL), (p_pw ? p_pw->p_drv: NULL), p_unpack->tag, p_unpack->seq, p_unpack->unpack.expected_len);
            }
        }
      if(status != NM_STATUS_NONE)
        {
          /* data spec still undefined- fire data event */
          const struct nm_core_event_s event =
            {
             .status = status,
             .p_req = p_unpack
            };
          nm_core_status_event(p_core, &event, p_unpack);
        }
    }
  return 0;
}

/** Process a packed data request (NM_PROTO_PKT_DATA)- p_unpack may be NULL (unexpected)
 */
static void nm_pkt_data_handler(struct nm_core*p_core, nm_gate_t p_gate, struct nm_req_s**pp_unpack,
                                const struct nm_header_pkt_data_s*h, struct nm_pkt_wrap_s*p_pw)
{
  NM_TRACEF("p_pw = %p; p_unpack = %p\n", p_pw, *pp_unpack);
  const struct nm_in_chunk_s chunk =
    {
     .p_gate       = p_gate,
     .seq          = h->seq,
     .tag          = h->tag_id,
     .chunk_offset = h->chunk_offset,
     .chunk_len    = h->data_len,
     .flags        = h->proto_id & NM_PROTO_FLAG_MASK,
     .priority     = 0
    };
  struct nm_req_s*const p_unpack = *pp_unpack;
  if(nm_core_unpack_match_chunk(p_core, p_unpack, &chunk, (void*)h, p_pw, NULL))
    {
      if(chunk.chunk_offset + chunk.chunk_len > p_unpack->unpack.offset)
        {
          const nm_len_t offset = (chunk.chunk_offset < p_unpack->unpack.offset) ?
            (p_unpack->unpack.offset - chunk.chunk_offset) : 0;
          assert(offset < chunk.chunk_len);
          nm_data_pkt_unpack(&p_unpack->data, h, p_pw, chunk.chunk_offset + offset, chunk.chunk_len - offset);
        }
      nm_core_unpack_check_completion(p_core, p_pw, pp_unpack, chunk.chunk_offset, chunk.chunk_len);
    }
}

/** Process a short data request (NM_PROTO_SHORT_DATA)- p_unpack may be NULL (unexpected)
 */
static void nm_short_data_handler(struct nm_core*p_core, nm_gate_t p_gate, struct nm_req_s**pp_unpack,
                                  const struct nm_header_short_data_s*h, struct nm_pkt_wrap_s*p_pw)
{
  NM_TRACEF("p_pw = %p; p_unpack = %p\n", p_pw, *pp_unpack);
  const struct nm_in_chunk_s chunk =
    {
     .p_gate       = p_gate,
     .seq          = h->seq,
     .tag          = h->tag_id,
     .chunk_offset = 0,
     .chunk_len    = h->len,
     .flags        = h->proto_id & NM_PROTO_FLAG_MASK,
     .priority     = 0
    };
  struct nm_req_s*const p_unpack = *pp_unpack;
  assert(h->proto_id & NM_PROTO_FLAG_LASTCHUNK); /* short data is always single chunk */
  if(nm_core_unpack_match_chunk(p_core, p_unpack, &chunk, (void*)h, p_pw, NULL))
    {
      const void*ptr = ((void*)h) + NM_HEADER_SHORT_DATA_SIZE;
      if(chunk.chunk_offset + chunk.chunk_len > p_unpack->unpack.offset)
        {
          const nm_len_t offset = (chunk.chunk_offset < p_unpack->unpack.offset) ?
            (p_unpack->unpack.offset - chunk.chunk_offset) : 0;
          assert(offset < chunk.chunk_len);
          nm_data_copy_to(&p_unpack->data, chunk.chunk_offset + offset, chunk.chunk_len - offset, ptr + offset);
        }
      nm_core_unpack_check_completion(p_core, p_pw, pp_unpack, chunk.chunk_offset, chunk.chunk_len);
    }
}

/** Process a small data request (NM_PROTO_SMALL_DATA)- p_unpack may be NULL (unexpected)
 */
static void nm_small_data_handler(struct nm_core*p_core, nm_gate_t p_gate, struct nm_req_s**pp_unpack,
                                  const struct nm_header_data_s*h, struct nm_pkt_wrap_s*p_pw)
{
  NM_TRACEF("p_pw = %p; p_unpack = %p\n", p_pw, *pp_unpack);
  const struct nm_in_chunk_s chunk =
    {
     .p_gate       = p_gate,
     .seq          = h->seq,
     .tag          = h->tag_id,
     .chunk_offset = h->chunk_offset,
     .chunk_len    = h->len,
     .flags        = h->proto_id & NM_PROTO_FLAG_MASK,
     .priority     = 0
    };
  const void*ptr = (h->skip == 0xFFFF) ? (((void*)h) + NM_HEADER_DATA_SIZE) :
    p_pw->v[0].iov_base + (h->skip + nm_header_global_v0len(p_pw));
  assert(p_pw->v_nb == 1);
  struct nm_req_s*const p_unpack = *pp_unpack;
  if(nm_core_unpack_match_chunk(p_core, p_unpack, &chunk, (void*)h, p_pw, NULL))
    {
      if(chunk.chunk_offset + chunk.chunk_len > p_unpack->unpack.offset)
        {
          const nm_len_t offset = (chunk.chunk_offset < p_unpack->unpack.offset) ?
            (p_unpack->unpack.offset - chunk.chunk_offset) : 0;
          assert(offset < chunk.chunk_len);
          nm_data_copy_to(&p_unpack->data, chunk.chunk_offset + offset, chunk.chunk_len - offset, ptr + offset);
        }
      nm_core_unpack_check_completion(p_core, p_pw, pp_unpack, chunk.chunk_offset, chunk.chunk_len);
    }
}

static void nm_msg_handler(struct nm_core*p_core, nm_gate_t p_gate, struct nm_req_s**pp_unpack,
                           const struct nm_header_msg_s*h, struct nm_pkt_wrap_s*p_pw)
{
  NM_TRACEF("p_pw = %p; p_unpack = %p\n", p_pw, *pp_unpack);
  assert((h->proto_id & NM_PROTO_FLAG_MASK) == 0); /* no flag allow on this proto_id */
  const struct nm_in_chunk_s chunk =
    {
     .p_gate       = p_gate,
     .seq          = h->seq,
     .tag          = h->tag_id,
     .chunk_offset = 0,
     .chunk_len    = 0,
     .flags        = 0,
     .priority     = 0
    };
  struct nm_req_s*const p_unpack = *pp_unpack;
  if(nm_core_unpack_match_chunk(p_core, p_unpack, &chunk, (void*)h, p_pw, NULL))
    {
      nm_core_unpack_check_completion(p_core, p_pw, pp_unpack, chunk.chunk_offset, chunk.chunk_len);
    }
}

/** Process a received rendez-vous request (NM_PROTO_RDV)-
 * may be called when a rdv arrives or when an unpack matches an unexpected rdv
 */
static void nm_rdv_handler(struct nm_core*p_core, nm_gate_t p_gate, struct nm_req_s*p_unpack,
                           const struct nm_header_ctrl_rdv_s*h, struct nm_pkt_wrap_s*p_pw)
{
  NM_TRACEF("p_unpack = %p; p_gate = %p; container p_pw = %p; chunk_offset = %lu; chunk_len = %lu\n",
            p_unpack, p_gate, p_pw, h->chunk_offset, h->chunk_len);
  const struct nm_in_chunk_s chunk =
    {
     .p_gate       = p_gate,
     .seq          = h->seq,
     .tag          = h->tag_id,
     .chunk_offset = h->chunk_offset,
     .chunk_len    = h->chunk_len,
     .flags        = h->proto_id & NM_PROTO_FLAG_MASK,
     .priority     = h->priority
    };
  if(nm_core_unpack_match_chunk(p_core, p_unpack, &chunk, (void*)h, p_pw, NULL))
    {
      NM_TRACEF("p_unpack = %p; p_prefetch_pw = %p\n", p_unpack, p_unpack->unpack.p_prefetch_pw);
      nm_profile_inc(p_core->profiling.n_rdvs);
      if(chunk.chunk_offset < p_unpack->unpack.offset)
        {
          NM_FATAL("rendez-vous truncated by offset; not supported.\n");
        }
      assert(p_unpack->p_gate != NULL);
      assert(!nm_data_isnull(&p_unpack->data));
      struct nm_pkt_wrap_s*p_large_pw = p_unpack->unpack.p_prefetch_pw;
      if(p_large_pw == NULL)
        {
          /* not prefetched */
          p_large_pw = nm_pw_alloc_noheader(p_core);
          p_large_pw->p_unpack = p_unpack;
          p_large_pw->p_gate   = p_gate;
          nm_pw_set_data_raw(p_large_pw, &p_unpack->data, chunk.chunk_len, chunk.chunk_offset);
          NM_TRACEF("new pw in RDV handler; NOT PREFETCHED; p_large_pw = %p; p_unpack = %p\n", p_large_pw, p_unpack);
        }
      else
        {
          NM_TRACEF("found PREFETCHED pw; p_large_pw = %p; p_unpack = %p\n", p_large_pw, p_unpack);
          if( (p_large_pw->length != chunk.chunk_len) ||
              (p_large_pw->chunk_offset != chunk.chunk_offset) )
            {
              nm_pw_recv_unfetch(p_large_pw);
              nm_pw_set_data_raw(p_large_pw, &p_unpack->data, chunk.chunk_len, chunk.chunk_offset);
            }
        }
      p_large_pw->prio = h->priority;
      nm_pkt_wrap_list_push_back(&p_gate->pending_large_recv, p_large_pw);
      nm_strat_rdv_accept(p_core, p_gate);
    }
}


/** Process an injected packet request- p_unpack may be NULL (unexpected)
 */
static void nm_inject_data_handler(struct nm_core*p_core, struct nm_req_s**pp_unpack,
                                   const struct nm_in_chunk_s*p_chunk,
                                   const struct nm_chunk_injector_s*p_injector)
{
  nm_core_lock_assert(p_core);
  struct nm_req_s*p_unpack = *pp_unpack;
  NM_TRACEF("p_unpack = %p; unpack offset = %ld; p_prefetch_pw = %p\n",
            p_unpack, p_unpack ? p_unpack->unpack.offset : 0, p_unpack ? p_unpack->unpack.p_prefetch_pw : NULL);
  if(nm_core_unpack_match_chunk(p_core, p_unpack, p_chunk, NULL /* p_header */, NULL /* p_pw */, p_injector))
    {
      /* injection request and matching recv are both arrived- trigger data injection */
      assert(p_unpack != NULL);
      NM_TRACEF("p_chunk = %p; chunk_offset = %ld; chunk_len = %ld\n",
                p_chunk, p_chunk->chunk_offset, p_chunk->chunk_len);
      if( (p_chunk->chunk_offset + p_chunk->chunk_len > p_unpack->unpack.offset) ||
          ((p_chunk->chunk_len == 0) && (p_chunk->chunk_offset >= p_unpack->unpack.offset)) ) /* for injection, copy even 0-length chunks when they are in the right unpack window, since injection completion depends on it to happen */
        {
          NM_TRACEF("p_unpack = %p; MATCHING\n", p_unpack);
          const nm_len_t offset = (p_chunk->chunk_offset < p_unpack->unpack.offset) ?
            (p_unpack->unpack.offset - p_chunk->chunk_offset) : 0;
          assert((offset < p_chunk->chunk_len) || (p_chunk->chunk_len == 0));
          if(p_unpack->unpack.p_prefetch_pw)
            {
              /* data will not arrive through the expected path.
               * Unfetch in case we had a pw for the request */
              struct nm_pkt_wrap_s*p_large_pw = p_unpack->unpack.p_prefetch_pw;
              nm_pw_recv_unfetch(p_large_pw);
              assert(p_unpack->unpack.p_prefetch_pw == NULL);
              nm_pw_free(p_core, p_large_pw);
            }
          nm_core_unlock(p_core);
          (*p_injector->p_pull_data)(p_unpack, &p_unpack->data, p_chunk->chunk_offset + offset, p_chunk->chunk_len - offset, p_injector->p_ref);
          nm_core_lock(p_core);
        }
      else
        {
          NM_TRACEF("p_chunk = %p; NOT MATCHING\n", p_chunk);
        }
    }
  else
    {
      NM_TRACEF("p_unpack = %p; NOT MATCHING\n", p_unpack);
    }
  NM_TRACEF("done\n");
}

void nm_core_inject_chunk(struct nm_core*p_core, nm_gate_t p_gate, nm_core_tag_t tag, nm_seq_t seq,
                          nm_len_t chunk_offset, nm_len_t chunk_len, int is_last_chunk,
                          nm_injector_pull_data_t p_pull_data, void*p_ref)
{
  assert(p_gate != NULL);
  assert(seq != NM_SEQ_NONE);
  nm_core_lock(p_core);
  struct nm_req_s*p_unpack = nm_unpack_find_matching(p_core, p_gate, seq, tag);
  NM_TRACEF("injecting in request p_unpack = %p; chunk_offset = %lu; chunk_len = %lu; p_prefetch_pw = %p\n",
            p_unpack, chunk_offset, chunk_len, p_unpack ? p_unpack->unpack.p_prefetch_pw : NULL);
  const struct nm_in_chunk_s chunk =
    {
     .p_gate       = p_gate,
     .seq          = seq,
     .tag          = tag,
     .chunk_offset = chunk_offset,
     .chunk_len    = chunk_len,
     .flags        = is_last_chunk ? NM_PROTO_FLAG_LASTCHUNK : 0
    };
  nm_so_schedule_lazy_init();
  struct nm_chunk_injector_s injector =
    {
      .p_pull_data = p_pull_data,
      .p_ref       = p_ref
    };
  nm_inject_data_handler(p_core, &p_unpack, &chunk, &injector);
  nm_core_unlock(p_core);
  NM_TRACEF("done.\n");
}

void nm_core_inject_complete(struct nm_core*p_core, struct nm_req_s*p_req, nm_len_t chunk_offset, nm_len_t chunk_len)
{
  NM_TRACEF("p_req = %p\n", p_req);
  nm_core_lock(p_core);
  p_req->flags |= NM_REQ_FLAG_FINALIZE_LATER;
  nm_core_unpack_check_completion(p_core, NULL, &p_req, chunk_offset, chunk_len);
  nm_core_unlock(p_core);
}

void nm_core_inject_finalize(struct nm_core*p_core, struct nm_req_s*p_req)
{
  NM_TRACEF("p_req = %p\n", p_req);
  assert(p_req->flags & NM_REQ_FLAG_FINALIZE_LATER);
  /* Here NM_STATUS_UNPACK_COMPLETED may not be set yet, because it is set just
   * before executing an associated monitor if there is any, and not during
   * nm_core_unpack_check_completion() called in nm_core_inject_complete().
   * The above assert should be sufficient to avoid any bad use of the injection API. */
  assert(!nm_status_test(p_req, NM_STATUS_FINALIZED));

  nm_core_lock(p_core);
  const struct nm_core_event_s event =
    {
      .status = NM_STATUS_FINALIZED,
      .p_req = p_req
    };
  nm_core_status_event(p_core, &event, p_req);
  nm_core_unlock(p_core);
}

void nm_core_inject_complete_finalize(struct nm_core*p_core, struct nm_req_s*p_req, nm_len_t chunk_offset, nm_len_t chunk_len)
{
  NM_TRACEF("p_req = %p\n", p_req);
  nm_core_lock(p_core);
  assert(!(p_req->flags & NM_REQ_FLAG_FINALIZE_LATER));
  assert(!nm_status_test(p_req, NM_STATUS_UNPACK_COMPLETED));
  nm_core_unpack_check_completion(p_core, NULL, &p_req, chunk_offset, chunk_len);
  nm_core_unlock(p_core);
}

/** Process a complete rendez-vous ready-to-receive request.
 */
void nm_rtr_handler(struct nm_pkt_wrap_s*p_rtr_pw, const struct nm_header_ctrl_rtr_s*p_header)
{
  const nm_core_tag_t tag     = p_header->tag_id;
  const nm_seq_t seq          = p_header->seq;
  const nm_len_t chunk_offset = p_header->chunk_offset;
  const nm_len_t chunk_len    = p_header->chunk_len;
  nm_gate_t p_gate      = p_rtr_pw->p_gate;
  struct nm_core*p_core = p_gate->p_core;
  struct nm_pkt_wrap_s*p_large_pw = NULL, *p_pw_save;
  nm_core_lock_assert(p_core);
  puk_list_foreach_safe(nm_pkt_wrap, p_large_pw, p_pw_save, &p_gate->pending_large_send)
    {
      /* this is a large pw with rdv- it must contain a single req chunk */
      assert(nm_req_chunk_list_size(&p_large_pw->req_chunks) == 1);
      struct nm_req_chunk_s*p_req_chunk = nm_req_chunk_list_begin(&p_large_pw->req_chunks);
      struct nm_req_s*p_pack = p_req_chunk->p_req;
      NM_TRACEF("Searching the pw corresponding to the ack - cur_seq = %u - cur_offset = %zd\n",
                p_pack->seq, p_large_pw->chunk_offset);
      if( (p_pack->seq == seq) && nm_core_tag_eq(p_pack->tag, tag) &&
          (chunk_offset >= p_large_pw->chunk_offset) && (chunk_offset < p_large_pw->chunk_offset + p_large_pw->length) )
        {
          const nm_len_t chunk_end = chunk_offset + chunk_len;
          const nm_len_t pw_end = p_large_pw->chunk_offset + p_large_pw->length;
          if( (p_large_pw->flags & NM_PW_PREFETCHING) &&
              !(p_large_pw->flags & NM_PW_PREFETCHED) )
            {
              /* prefetching in progress; save state & return */
              p_large_pw->flags |= NM_PW_RDV_READY;
              struct nm_pw_pending_rtr_s*p_pending_rtr = nm_pw_pending_rtr_new();
              p_pending_rtr->p_rtr_pw = p_rtr_pw;
              p_pending_rtr->p_header = p_header;
              nm_pw_pending_rtr_list_push_back(&p_large_pw->pending_rtrs, p_pending_rtr);
              /* TODO- check pw refcounting */
              return;
            }
          nm_pkt_wrap_list_remove(&p_gate->pending_large_send, p_large_pw);
          if(chunk_len < p_large_pw->length)
            {
              /* ** partial RTR- split the packet  */
              NM_TRACEF("received partial RTR- chunk_offset = %lu; chunk_len = %lu; pw offset = %lu; pw len = %lu\n",
                        chunk_offset, chunk_len, p_large_pw->chunk_offset, p_large_pw->length);
              assert(chunk_len > 0 && chunk_len < p_large_pw->length);
              if(chunk_end < pw_end)
                {
                  /* trim pw end */
                  struct nm_pkt_wrap_s*p_pw2 = NULL;
                  const nm_len_t chunk2_len = pw_end - chunk_end;
                  nm_pw_split(p_core, p_large_pw, &p_pw2, p_large_pw->length - chunk2_len);
                  struct nm_req_chunk_s*p_req_chunk2 = nm_req_chunk_alloc(p_core);
                  nm_req_chunk_init(p_req_chunk2, p_req_chunk->p_req, chunk_end /* chunk_offset */, chunk2_len);
                  nm_req_chunk_list_push_back(&p_pw2->req_chunks, p_req_chunk2);
                  nm_pkt_wrap_list_push_front(&p_gate->pending_large_send, p_pw2);
                  p_req_chunk->chunk_len -= chunk2_len;
                }
              if(chunk_offset > p_large_pw->chunk_offset)
                {
                  /* trim pw begin */
                  struct nm_pkt_wrap_s*p_pw3 = NULL;
                  const nm_len_t split_offset = chunk_offset - p_large_pw->chunk_offset;
                  nm_pw_split(p_core, p_large_pw, &p_pw3, split_offset);
                  struct nm_req_chunk_s*p_req_chunk3 = nm_req_chunk_alloc(p_core);
                  nm_req_chunk_init(p_req_chunk3, p_req_chunk->p_req, p_pw3->chunk_offset /* chunk_offset */, p_pw3->length);
                  nm_req_chunk_list_push_back(&p_pw3->req_chunks, p_req_chunk3);
                  p_req_chunk->chunk_len -= p_pw3->length;
                  /* switch pws so that p_large_pw is the one to send (end), not the remainding (begin) */
                  struct nm_pkt_wrap_s*p_tmp_pw = p_large_pw;
                  p_large_pw = p_pw3;
                  p_pw3 = p_tmp_pw;
                  p_req_chunk = nm_req_chunk_list_begin(&p_large_pw->req_chunks);
                  nm_pkt_wrap_list_push_front(&p_gate->pending_large_send, p_pw3); /* set the new pw3 as pending */
                }
              assert(p_req_chunk->chunk_len == chunk_len); /* check trim */
              assert(p_req_chunk->chunk_len == p_large_pw->length);
            }
          /* send the data */
          struct nm_trk_s*p_trk = nm_trk_get_by_index(p_gate, p_header->trk_id);
          if(p_trk->kind == nm_trk_large)
            {
              memcpy(&p_large_pw->rdv_data[0], &p_header->rdv_data[0], NM_HEADER_RTR_DATA_SIZE);
              nm_core_post_send(p_large_pw, p_gate, p_header->trk_id);
            }
          else
            {
              /* rdv eventually accepted on trk#0- rollback and repack */
              assert(p_large_pw->p_data != NULL);
              if(p_large_pw->flags & NM_PW_PREFETCHING)
                {
                  /* cancel prefetching */
                  nm_pw_send_unfetch(p_large_pw);
                }
              nm_pw_free(p_core, p_large_pw);
              struct nm_req_chunk_s*p_req_chunk0 = nm_req_chunk_alloc(p_core);
              nm_req_chunk_init(p_req_chunk0, p_pack, chunk_offset, chunk_len);
              nm_strat_submit_req_chunk(p_core, p_gate, p_req_chunk0, 0);
            }
          return;
        }
    }
  NM_FATAL("FATAL- cannot find matching packet for received RTR: seq = %u; offset = %lu; len = %lu\n", seq, chunk_offset, chunk_len);
  abort();
}
/** Process an acknowledgement.
 */
static void nm_ack_handler(struct nm_pkt_wrap_s*p_ack_pw, const struct nm_header_ctrl_ack_s*p_header)
{
  struct nm_core*p_core = p_ack_pw->p_gate->p_core;
  nm_core_lock_assert(p_core);
  const nm_core_tag_t tag = p_header->tag_id;
  const nm_seq_t seq = p_header->seq;
  struct nm_req_s*p_pack = NULL;
  struct nm_gtag_s*p_gtag = nm_gtag_get(&p_ack_pw->p_gate->tags, tag);

  puk_list_foreach(nm_req, p_pack, &p_gtag->pending_packs)
    {
      assert(nm_core_tag_eq(p_pack->tag, tag));
      if(p_pack->seq == seq)
        {
          const int finalized = nm_status_test(p_pack, NM_STATUS_PACK_COMPLETED);
          nm_core_lock_assert(p_core);
          const struct nm_core_event_s event =
            {
              .status = NM_STATUS_ACK_RECEIVED | ( finalized ? NM_STATUS_FINALIZED : 0),
              .p_req = p_pack
            };
          if(finalized)
            {
              nm_req_list_remove(&p_gtag->pending_packs, p_pack);
              p_core->n_packs--;
              nm_trace_var(NM_TRACE_EVENT_VAR_N_PACKS, p_core->n_packs, NULL, NULL);
            }
          nm_core_status_event(p_core, &event, p_pack);
          nm_core_polling_level(p_core);
          return;
        }
    }
}

/** decode one chunk of headers.
 * @returns the number of processed bytes in global header,
 */
static int nm_decode_header_chunk(struct nm_core*p_core, const void*ptr, struct nm_pkt_wrap_s*p_pw, nm_gate_t p_gate)
{
  int rc = 0;
  const nm_proto_t proto_id   = (*(const nm_proto_t*)ptr) & NM_PROTO_ID_MASK;
  const nm_proto_t proto_flag = (*(const nm_proto_t*)ptr) & NM_PROTO_FLAG_MASK;
  NM_TRACEF("p_pw = %p; ptr = %p; proto_id = %x; proto_flag = %x\n", p_pw, ptr, proto_id, proto_flag);
  assert(proto_id != 0);
  switch(proto_id)
    {
    case NM_PROTO_PKT_DATA:
      {
        const struct nm_header_pkt_data_s*h = ptr;
        struct nm_req_s*p_unpack = nm_unpack_find_matching(p_core, p_gate, h->seq, h->tag_id);
        nm_pkt_data_handler(p_core, p_gate, &p_unpack, h, p_pw);
        rc = h->hlen;
      }
      break;

    case NM_PROTO_SHORT_DATA:
      {
        const struct nm_header_short_data_s*h = ptr;
        struct nm_req_s*p_unpack = nm_unpack_find_matching(p_core, p_gate, h->seq, h->tag_id);
        nm_short_data_handler(p_core, p_gate, &p_unpack, h, p_pw);
        rc = NM_HEADER_SHORT_DATA_SIZE + h->len;
      }
      break;

    case NM_PROTO_SMALL_DATA:
      {
        const struct nm_header_data_s*h = ptr;
        rc = NM_HEADER_DATA_SIZE;
        if(h->skip == 0xFFFF)
          {
            const nm_len_t size = (proto_flag & NM_PROTO_FLAG_ALIGNED) ? nm_aligned(h->len) : h->len;
            rc += size;
          }
        struct nm_req_s*p_unpack = nm_unpack_find_matching(p_core, p_gate, h->seq, h->tag_id);
        nm_small_data_handler(p_core, p_gate, &p_unpack, h, p_pw);
      }
      break;

    case NM_PROTO_MSG:
      {
        const struct nm_header_msg_s*h = ptr;
        struct nm_req_s*p_unpack = nm_unpack_find_matching(p_core, p_gate, h->seq, h->tag_id);
        nm_msg_handler(p_core, p_gate, &p_unpack, h, p_pw);
        rc = sizeof(struct nm_header_msg_s);
      }
      break;

    case NM_PROTO_RDV:
      {
        const struct nm_header_ctrl_rdv_s*h = ptr;
        rc = sizeof(struct nm_header_ctrl_rdv_s);
        struct nm_req_s*p_unpack = nm_unpack_find_matching(p_core, p_gate, h->seq, h->tag_id);
        if(p_unpack)
          {
            nm_trace_event(NM_TRACE_EVENT_CORE_UNPACK_RDV, p_unpack, p_pw->p_gate, p_pw->p_drv, p_unpack->tag, p_unpack->seq, p_unpack->unpack.expected_len);
          }
        nm_rdv_handler(p_core, p_gate, p_unpack, h, p_pw);
      }
      break;

    case NM_PROTO_RTR:
      {
        const struct nm_header_ctrl_rtr_s*h = ptr;
        rc = sizeof(struct nm_header_ctrl_rtr_s);
        nm_rtr_handler(p_pw, h);
      }
      break;

    case NM_PROTO_ACK:
      {
        const struct nm_header_ctrl_ack_s*h = ptr;
        rc = sizeof(struct nm_header_ctrl_ack_s);
        nm_ack_handler(p_pw, h);
      }
      break;

    case NM_PROTO_STRAT:
      {
        const struct puk_receptacle_NewMad_Strategy_s*strategy = &p_gate->strategy_receptacle;
        const struct nm_header_strat_s*h = ptr;
        if(strategy->driver->proto)
          {
            (*strategy->driver->proto)(strategy->_status, p_gate, p_pw, ptr, h->size);
          }
        else
          {
            NM_FATAL("strategy cannot process NM_PROTO_STRAT.\n");
          }
        rc = h->size;
      }
      break;

    default:
      NM_FATAL("received header with invalid proto_id 0x%02X\n", proto_id);
      break;
    }
  return rc;
}


/** Process a complete incoming request.
 */
void nm_pw_process_complete_recv(struct nm_core*p_core, struct nm_pkt_wrap_s*p_pw)
{
  NM_TRACEF("p_pw = %p; entering...\n", p_pw);
  nm_drv_t const p_drv = p_pw->p_drv;
  nm_core_lock_assert(p_core);
  nm_profile_inc(p_core->profiling.n_pw_in);
  nm_trace_event(NM_TRACE_EVENT_PW_COMPLETE_RECV, p_pw, p_pw->p_gate, p_drv, NM_CORE_TAG_NONE, NM_SEQ_NONE, p_pw->length);
  /* check error status */
  if(p_pw->flags & NM_PW_CANCELED)
    {
      nm_pw_ref_dec(p_pw);
      return;
    }
  /* clear the input request field */
  assert(p_pw->p_trk);
  assert(p_pw->p_trk->p_pw_recv == p_pw);
  p_pw->p_trk->p_pw_recv = NULL;
  assert((p_drv->p_pw_recv_any == p_pw) || (p_drv->p_pw_recv_any == NULL));
  p_drv->p_pw_recv_any = NULL;
  nm_gate_t const p_gate = p_pw->p_gate;
  assert(p_gate != NULL);
  if(p_pw->flags & NM_PW_CLOSED)
    {
      nm_pw_ref_dec(p_pw);
      nm_core_gate_disconnected(p_core, p_gate, p_drv);
      return;
    }

  if(p_pw->p_trk->kind == nm_trk_small)
    {
      /* ** Small packets - track #0 *********************** */
      struct iovec*const v0 = p_pw->v;
      const nm_len_t v0len = nm_header_global_v0len(p_pw);
      const void*ptr = v0->iov_base + sizeof(struct nm_header_global_s);
      do
        {
          /* Iterate over header chunks */
          NM_TRACEF("p_pw = %p; decoding header; ptr = %p.\n", p_pw, ptr);
          assert(ptr < v0->iov_base + v0->iov_len);
          nm_len_t done = nm_decode_header_chunk(p_core, ptr, p_pw, p_gate);
          ptr += done;
        }
      while(ptr < v0->iov_base + v0len);
      if(p_pw->flags & NM_PW_BUF_RECV)
        {
          const struct puk_receptacle_NewMad_minidriver_s*r = &p_pw->p_trk->receptacle;
          int rc = (*r->driver->recv_buf_release)(r->_status);
          if(rc != NM_ESUCCESS)
            {
              NM_WARN("drv->recv_buf_release() rc = %d (%s)\n", rc, nm_strerror(rc));
            }
          if(p_pw->flags & NM_PW_BUF_MIRROR)
            {
              p_pw->v[0].iov_base = p_pw->buf;
            }
          else
            {
#ifdef NMAD_DEBUG
              p_pw->v[0].iov_base = NULL;
              p_pw->v[0].iov_len = 0;
#endif /* NMAD_DEBUG*/
            }
        }
      /* refill recv on trk #0 */
      nm_drv_refill_recv(p_drv, p_gate);
    }
  else if(p_pw->p_trk->kind == nm_trk_large)
    {
      /* ** Large packet - track #1 ************************ */
      struct nm_req_s*p_unpack = p_pw->p_unpack;
      nm_core_unpack_check_completion(p_core, p_pw, &p_unpack, p_pw->chunk_offset, p_pw->length);
    }
  struct puk_receptacle_NewMad_Strategy_s*p_strategy = &p_gate->strategy_receptacle;
  if(p_strategy->driver->pw_recv_complete != NULL)
    {
      (*p_strategy->driver->pw_recv_complete)(p_strategy->_status, p_pw);
    }
  nm_pw_ref_dec(p_pw);
  NM_TRACEF("p_pw = %p; done\n", p_pw);
  /* Hum... Well... We're done guys! */
}

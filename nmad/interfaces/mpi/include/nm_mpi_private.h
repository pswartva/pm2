/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#ifndef NM_MPI_PRIVATE_H
#define NM_MPI_PRIVATE_H

/** @defgroup mpi_private_interface MadMPI Private Interface
 *
 * This is the MadMPI private interface
 *
 */

/** @ingroup mpi_private_interface
 * @file
 * @brief Internal data structures for MPI
 */

#include <stdint.h>
#include <limits.h>
#include <unistd.h>
#include <assert.h>
#include <complex.h>
#include <execinfo.h>
#include <sys/stat.h>

#include <Padico/Puk.h>
#include <nm_private_config.h>
#include <nm_public.h>
#include <nm_core_interface.h>
#include <nm_sendrecv_interface.h>
#include <nm_pack_interface.h>
#include <nm_launcher_interface.h>
#include <nm_coll_interface.h>

#include <nm_private.h>

#ifdef PIOMAN
#include <pioman.h>
#endif /* PIOMAN */

#include "nm_mpi.h"

/** @ingroup mpi_private_interface
 * @{
 */


#define nm_mpi_spinlock_t         nm_spinlock_t
#define nm_mpi_spin_init(LOCK)    nm_spin_init(LOCK)
#define nm_mpi_spin_destroy(LOCK) nm_spin_destroy(LOCK)
#define nm_mpi_spin_lock(LOCK)    nm_spin_lock(LOCK)
#define nm_mpi_spin_unlock(LOCK)  nm_spin_unlock(LOCK)

/* fixed-size floats */
#if SIZEOF_FLOAT==4
typedef float nm_mpi_float4_t;
#elif SIZEOF_DOUBLE==4
typedef double nm_mpi_float4_t;
#else
#  error "no C type for 4 bytes floats"
#endif

#if SIZEOF_FLOAT==8
typedef float nm_mpi_float8_t;
#elif SIZEOF_DOUBLE==8
typedef double nm_mpi_float8_t;
#else
#  error "no C type for 8 bytes floats"
#endif

/** error handler */
typedef struct nm_mpi_errhandler_s
{
  int id;
  union
  {
    MPI_Handler_function*legacy;
    MPI_Comm_errhandler_function*comm_func;
    MPI_Win_errhandler_function*win_func;
    MPI_File_errhandler_function*file_func;
    MPI_Session_errhandler_function*session_func;
  } function;
  enum nm_mpi_errhandler_kind_e
    {
     NM_MPI_ERRHANDLER_NONE = 0,
     NM_MPI_ERRHANDLER_COMM,
     NM_MPI_ERRHANDLER_WIN,
     NM_MPI_ERRHANDLER_FILE,
     NM_MPI_ERRHANDLER_SESSION
    } kind;
} nm_mpi_errhandler_t;

/** default errhandler for files, attached to MPI_FILE_NULL  */
extern struct nm_mpi_errhandler_s*nm_mpi_file_errhandler;

#define nm_mpi_errhandler_exec(P_ERRHANDLER, KIND, HANDLE, ERR)         \
  nm_mpi_errhandler_exec_internal(P_ERRHANDLER, KIND, HANDLE, ERR,      \
                                  __FUNCTION__, __FILE__, __LINE__)

/** marks an error as internal, not needing to call the handler */
#define NM_MPI_ERROR_INTERNAL(ERRORCODE) (ERRORCODE)

#define NM_MPI_ERROR_COMM(P_COMM, ERRORCODE)                            \
  ( ( ((ERRORCODE) == MPI_SUCCESS) ? MPI_SUCCESS :                      \
      nm_mpi_errhandler_exec((P_COMM)->p_errhandler, NM_MPI_ERRHANDLER_COMM, (P_COMM)->id, (ERRORCODE)) ) \
    , ERRORCODE )

/** raise an error on the given session */
#define NM_MPI_ERROR_SESSION(P_SESSION, ERRORCODE)                      \
  ( ( ((ERRORCODE) == MPI_SUCCESS) ? MPI_SUCCESS :                      \
      nm_mpi_errhandler_exec((P_SESSION)->p_errhandler, NM_MPI_ERRHANDLER_SESSION, (P_SESSION)->id, (ERRORCODE)) ) \
    , ERRORCODE )

/** raise an error on the given window */
#define NM_MPI_ERROR_WIN(P_WIN, ERRORCODE)                              \
  ( ( ((ERRORCODE) == MPI_SUCCESS) ? MPI_SUCCESS :                      \
      nm_mpi_errhandler_exec((P_WIN)->p_errhandler, NM_MPI_ERRHANDLER_WIN, (P_WIN)->id, (ERRORCODE)) ) \
    , ERRORCODE )

/** raise an error on the given file */
#define NM_MPI_ERROR_FILE(P_FILE, ERRORCODE)                            \
  ( ( ((ERRORCODE) == MPI_SUCCESS) ? MPI_SUCCESS :                      \
      nm_mpi_errhandler_exec((P_FILE)->p_errhandler, NM_MPI_ERRHANDLER_FILE, (P_FILE)->id, (ERRORCODE)) ) \
    , ERRORCODE )

/** raise an error on the default errhandler for file (attached to MPI_FILE_NULL) */
#define NM_MPI_ERROR_FILE_DEFAULT(ERRORCODE)                            \
  ( ( ((ERRORCODE) == MPI_SUCCESS) ? MPI_SUCCESS :                      \
      nm_mpi_errhandler_exec(nm_mpi_file_errhandler, NM_MPI_ERRHANDLER_FILE, MPI_FILE_NULL, (ERRORCODE)) ) \
    , ERRORCODE )

#define NM_MPI_ERROR_WORLD(ERRORCODE)                                   \
  ( ( ((ERRORCODE) == MPI_SUCCESS) ? MPI_SUCCESS :                      \
      nm_mpi_errhandler_exec(nm_mpi_communicator_get(MPI_COMM_WORLD)->p_errhandler, NM_MPI_ERRHANDLER_COMM, MPI_COMM_WORLD, (ERRORCODE)) ) \
    , ERRORCODE )

/** check that the given communicator is valid */
#define NM_MPI_COMMUNICATOR_CHECK(P_COMM)                               \
  PUK_CHECK_TYPE(struct nm_mpi_communicator_s*, P_COMM);                \
  do { if((P_COMM) == NULL) return NM_MPI_ERROR_WORLD(MPI_ERR_COMM); } while(0)

/** check that the given group is valid */
#define NM_MPI_GROUP_CHECK(P_GROUP)                                     \
  PUK_CHECK_TYPE(struct nm_mpi_group_s*, P_GROUP);                      \
  do { if((P_GROUP) == NULL) return NM_MPI_ERROR_WORLD(MPI_ERR_GROUP); } while(0)

/** check that the given session is valid */
#define NM_MPI_SESSION_CHECK(P_SESSION)                                 \
  PUK_CHECK_TYPE(struct nm_mpi_session_s*, P_SESSION);                  \
  do { if((P_SESSION) == NULL) return NM_MPI_ERROR_WORLD(MPI_ERR_SESSION); } while(0)

/** check that the given datatype is valid */
#define NM_MPI_DATATYPE_CHECK(P_DATATYPE)                               \
  PUK_CHECK_TYPE(struct nm_mpi_datatype_s*, P_DATATYPE);                \
  do { if((P_DATATYPE) == NULL) return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_TYPE); } while(0)

/** check that tag is a valid tag for a recv operation */
#define NM_MPI_TAG_CHECK_RECV(TAG)                                      \
  do { if(((TAG) != MPI_ANY_TAG) && ((TAG) < 0 || (TAG) > NM_MPI_TAG_MAX)) return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_TAG); } while(0)

/** check that tag is a valid tag for a send operation */
#define NM_MPI_TAG_CHECK_SEND(TAG)                                     \
  do { if( ((TAG) < 0) || ((TAG) > NM_MPI_TAG_MAX) || ((TAG) == MPI_ANY_TAG)) return NM_MPI_ERROR_COMM(p_comm, MPI_ERR_TAG); } while(0)

#define NM_MPI_WARNING(...) NM_WARN(__VA_ARGS__)

#define NM_MPI_FATAL_ERROR(...) NM_FATAL(__VA_ARGS__)

#define NM_MPI_TRACE(str, ...) NM_TRACEF(str, ## __VA_ARGS__)

#define FREE_AND_SET_NULL(p) do { padico_free(p); p = NULL; } while (0/*CONSTCOND*/)


/** elements for nm_mpi_refcount_s */
PUK_LIST_TYPE(nm_mpi_refcount_holder,
              const void*p_holder;
              const char*func;
              const char*file;
              int line;
              );

/** a reference-counter that keeps trace of who increments/decrements */
struct nm_mpi_refcount_s
{
  int refcount;
#ifdef NMAD_DEBUG
  const char*type;
  int id;
  nm_mpi_spinlock_t lock;
  struct nm_mpi_refcount_holder_list_s holders;
#endif /* NMAD_DEBUG */
};

#define NM_MPI_REFCOUNT_INVALID { .refcount = -1 }

/** initialize a new refcount object; get the type of object and its ID to make debugging easier */
static inline void nm_mpi_refcount_init(struct nm_mpi_refcount_s*p_refcount, const char*type __attribute__((unused)), int id)
{
  p_refcount->refcount = 1;
#ifdef NMAD_DEBUG
  p_refcount->type = type;
  p_refcount->id = id;
  nm_mpi_spin_init(&p_refcount->lock);
  nm_mpi_refcount_holder_list_init(&p_refcount->holders);
#endif
}

static inline void nm_mpi_refcount_dump(struct nm_mpi_refcount_s*p_refcount __attribute__((unused)))
{
#ifdef NMAD_DEBUG
  nm_mpi_refcount_holder_itor_t i;
  puk_list_foreach(nm_mpi_refcount_holder, i, &p_refcount->holders)
    {
      NM_MPI_WARNING("pending ref- type = %s; id = %d; p_holder = %p; %s:%d %s()\n",
                     p_refcount->type, p_refcount->id, i->p_holder, i->file, i->line, i->func);
    }
#endif /* NMAD_DEBUG */
}

static inline void nm_mpi_refcount_destroy(struct nm_mpi_refcount_s*p_refcount __attribute__((unused)))
{
#ifdef NMAD_DEBUG
  nm_mpi_refcount_dump(p_refcount);
  nm_mpi_spin_destroy(&p_refcount->lock);
  nm_mpi_refcount_holder_list_destroy(&p_refcount->holders);
#endif /* NMAD_DEBUG */
}

#define nm_mpi_refcount_inc(REFCOUNT, HOLDER)   \
  nm_mpi_refcount_inc_internal(REFCOUNT, HOLDER, __FUNCTION__, __FILE__, __LINE__)

static inline void nm_mpi_refcount_inc_internal(struct nm_mpi_refcount_s*p_refcount, const void*p_holder __attribute__((unused)),
                                                const char*func __attribute__((unused)),
                                                const char*file __attribute__((unused)),
                                                const int line __attribute__((unused)))
{
  assert(p_refcount->refcount >= 0);
  __sync_fetch_and_add(&p_refcount->refcount, 1);
#ifdef NMAD_DEBUG
  if(p_holder != NULL)
    {
      nm_mpi_spin_lock(&p_refcount->lock);
#if 0
      nm_mpi_refcount_holder_itor_t i;
      PUK_LIST_FIND(nm_mpi_refcount_holder, i, &p_refcount->holders, (i->p_holder == p_holder));
      if(i != NULL)
        {
          NM_MPI_WARNING("p_holder = %p already holding a ref to p_refcount = %p (type = %s; id = %d)\n"
                         " new holder: %s:%d %s()\n"
                         " previous holder: %s:%d %s()\n",
                         p_holder, p_refcount, p_refcount->type, p_refcount->id,
                         file, line, func, i->file, i->line, i->func);
        }
#endif /* 0 */
      struct nm_mpi_refcount_holder_s*h = nm_mpi_refcount_holder_new();
      h->p_holder = p_holder;
      h->func = func;
      h->file = file;
      h->line = line;
      nm_mpi_refcount_holder_list_push_back(&p_refcount->holders, h);
      nm_mpi_spin_unlock(&p_refcount->lock);
    }
#endif /* NMAD_DEBUG */
}

/** decrement refcount for holder; returns refcount (if 0, caller may free ref-counted resource) */
static inline int nm_mpi_refcount_dec(struct nm_mpi_refcount_s*p_refcount, const void*p_holder __attribute__((unused)))
{
  const int nb_ref = __sync_sub_and_fetch(&p_refcount->refcount, 1);
  assert(nb_ref >= 0);
#ifdef NMAD_DEBUG
  if(p_holder != NULL)
    {
      nm_mpi_spin_lock(&p_refcount->lock);
      nm_mpi_refcount_holder_itor_t i;
      PUK_LIST_FIND(nm_mpi_refcount_holder, i, &p_refcount->holders, (i->p_holder == p_holder));
      if(i == NULL)
        {
          NM_MPI_FATAL_ERROR("p_holder = %p not holding ref to p_refcount = %p (type = %s; id = %d); cannot release\n",
                             p_holder, p_refcount, p_refcount->type, p_refcount->id);
        }
      else
        {
          nm_mpi_refcount_holder_list_remove(&p_refcount->holders, i);
          nm_mpi_refcount_holder_delete(i);
        }
      nm_mpi_spin_unlock(&p_refcount->lock);
    }
#endif /* NMAD_DEBUG */
  return nb_ref;
}

/** Maximum value of the tag specified by the end-user */
#define NM_MPI_TAG_MAX                   ((nm_tag_t)(0x7FFFFFFF))
/** Mask for private tags */
#define NM_MPI_TAG_PRIVATE_MASK          ((nm_tag_t)(0x80000000))
/** Base value for private tags */
#define NM_MPI_TAG_PRIVATE_BASE          ((nm_tag_t)(0xF0000000))
#define NM_MPI_TAG_PRIVATE_BARRIER       ((nm_tag_t)(NM_MPI_TAG_PRIVATE_BASE | 0x11))
#define NM_MPI_TAG_PRIVATE_BCAST         ((nm_tag_t)(NM_MPI_TAG_PRIVATE_BASE | 0x12))
#define NM_MPI_TAG_PRIVATE_GATHER        ((nm_tag_t)(NM_MPI_TAG_PRIVATE_BASE | 0x13))
#define NM_MPI_TAG_PRIVATE_GATHERV       ((nm_tag_t)(NM_MPI_TAG_PRIVATE_BASE | 0x14))
#define NM_MPI_TAG_PRIVATE_SCATTER       ((nm_tag_t)(NM_MPI_TAG_PRIVATE_BASE | 0x15))
#define NM_MPI_TAG_PRIVATE_SCATTERV      ((nm_tag_t)(NM_MPI_TAG_PRIVATE_BASE | 0x16))
#define NM_MPI_TAG_PRIVATE_ALLTOALL      ((nm_tag_t)(NM_MPI_TAG_PRIVATE_BASE | 0x17))
#define NM_MPI_TAG_PRIVATE_ALLTOALLV     ((nm_tag_t)(NM_MPI_TAG_PRIVATE_BASE | 0x18))
#define NM_MPI_TAG_PRIVATE_REDUCE        ((nm_tag_t)(NM_MPI_TAG_PRIVATE_BASE | 0x19))
#define NM_MPI_TAG_PRIVATE_ALLREDUCE     ((nm_tag_t)(NM_MPI_TAG_PRIVATE_BASE | 0x1A))
#define NM_MPI_TAG_PRIVATE_SCAN          ((nm_tag_t)(NM_MPI_TAG_PRIVATE_BASE | 0x1B))
#define NM_MPI_TAG_PRIVATE_REDUCESCATTER ((nm_tag_t)(NM_MPI_TAG_PRIVATE_BASE | 0x1C))
#define NM_MPI_TAG_PRIVATE_ALLGATHER     ((nm_tag_t)(NM_MPI_TAG_PRIVATE_BASE | 0x1D))
#define NM_MPI_TAG_PRIVATE_TYPE_ADD      ((nm_tag_t)(NM_MPI_TAG_PRIVATE_BASE | 0x21)) /**< add a datatype; tag is 0xF0XXXX21 where XXXX is seq number (16 bits) */
#define NM_MPI_TAG_PRIVATE_TYPE_ADD_ACK  ((nm_tag_t)(NM_MPI_TAG_PRIVATE_BASE | 0x22)) /**< answer to add request */
#define NM_MPI_TAG_PRIVATE_WIN_INIT      ((nm_tag_t)(NM_MPI_TAG_PRIVATE_BASE | 0x23))
#define NM_MPI_TAG_PRIVATE_WIN_FENCE     ((nm_tag_t)(NM_MPI_TAG_PRIVATE_BASE | 0x24))
#define NM_MPI_TAG_PRIVATE_WIN_BARRIER   ((nm_tag_t)(NM_MPI_TAG_PRIVATE_BASE | 0x25))
#define NM_MPI_TAG_PRIVATE_COMM_SPLIT    ((nm_tag_t)(NM_MPI_TAG_PRIVATE_BASE | 0xF1))
#define NM_MPI_TAG_PRIVATE_COMM_CREATE   ((nm_tag_t)(NM_MPI_TAG_PRIVATE_BASE | 0xF2))
#define NM_MPI_TAG_PRIVATE_COMM_INFO     ((nm_tag_t)(NM_MPI_TAG_PRIVATE_BASE | 0xF3))
#define NM_MPI_TAG_PRIVATE_FILE_OPEN     ((nm_tag_t)(NM_MPI_TAG_PRIVATE_BASE | 0xF4))
/** Masks for RMA-communication tags */
#define NM_MPI_TAG_PRIVATE_RMA_BASE      ((nm_tag_t)(NM_MPI_TAG_PRIVATE_BASE | 0x08000000))
#define NM_MPI_TAG_PRIVATE_RMA_BASE_SYNC ((nm_tag_t)(NM_MPI_TAG_PRIVATE_BASE | 0x04000000))
#define NM_MPI_TAG_PRIVATE_RMA_MASK_OP   ((nm_tag_t)(NM_MPI_TAG_PRIVATE_RMA_BASE | 0x0F)) /**< rma operation mask */
#define NM_MPI_TAG_PRIVATE_RMA_MASK_AOP  ((nm_tag_t)(NM_MPI_TAG_PRIVATE_RMA_BASE | 0xF0)) /**< acc operation mask */
#define NM_MPI_TAG_PRIVATE_RMA_MASK_FOP  ((nm_tag_t)(NM_MPI_TAG_PRIVATE_RMA_MASK_OP    | \
                                                     NM_MPI_TAG_PRIVATE_RMA_MASK_AOP))    /**< full operation mask */
#define NM_MPI_TAG_PRIVATE_RMA_MASK_SEQ  ((nm_tag_t)(NM_MPI_TAG_PRIVATE_RMA_BASE | 0xFFFF00))
#define NM_MPI_TAG_PRIVATE_RMA_MASK_WIN  ((nm_tag_t)(0xFFFFFFFF00000000 | NM_MPI_TAG_PRIVATE_RMA_BASE))
#define NM_MPI_TAG_PRIVATE_RMA_MASK_USER ((nm_tag_t)(NM_MPI_TAG_PRIVATE_RMA_MASK_FOP   | \
                                                     NM_MPI_TAG_PRIVATE_RMA_MASK_SEQ))
#define NM_MPI_TAG_PRIVATE_RMA_MASK      ((nm_tag_t)(NM_MPI_TAG_PRIVATE_RMA_MASK_WIN   | \
                                                     NM_MPI_TAG_PRIVATE_RMA_MASK_USER))
#define NM_MPI_TAG_PRIVATE_RMA_MASK_SYNC ((nm_tag_t)(NM_MPI_TAG_PRIVATE_RMA_BASE_SYNC | 0xFFFFFFFF000000FF))
#define NM_MPI_TAG_PRIVATE_RMA_START     ((nm_tag_t)(NM_MPI_TAG_PRIVATE_RMA_BASE_SYNC | 0x01))
#define NM_MPI_TAG_PRIVATE_RMA_END       ((nm_tag_t)(NM_MPI_TAG_PRIVATE_RMA_BASE_SYNC | 0x02))
#define NM_MPI_TAG_PRIVATE_RMA_LOCK      ((nm_tag_t)(NM_MPI_TAG_PRIVATE_RMA_BASE_SYNC | 0x13)) /**< lock request */
#define NM_MPI_TAG_PRIVATE_RMA_LOCK_R    ((nm_tag_t)(NM_MPI_TAG_PRIVATE_RMA_BASE_SYNC | 0x04)) /**< lock ACK */
#define NM_MPI_TAG_PRIVATE_RMA_UNLOCK    ((nm_tag_t)(NM_MPI_TAG_PRIVATE_RMA_BASE_SYNC | 0x05)) /**< unlock request */
#define NM_MPI_TAG_PRIVATE_RMA_UNLOCK_R  ((nm_tag_t)(NM_MPI_TAG_PRIVATE_RMA_BASE_SYNC | 0x06)) /**< unlock ACK */
/** If dynamic window, tells if target address is valid */
#define NM_MPI_TAG_PRIVATE_RMA_OP_CHECK  ((nm_tag_t)(NM_MPI_TAG_PRIVATE_RMA_BASE_SYNC | 0x0F))
#define NM_MPI_TAG_PRIVATE_RMA_PUT       ((nm_tag_t)(NM_MPI_TAG_PRIVATE_RMA_BASE | 0x1))
#define NM_MPI_TAG_PRIVATE_RMA_GET_REQ   ((nm_tag_t)(NM_MPI_TAG_PRIVATE_RMA_BASE | 0x2))
#define NM_MPI_TAG_PRIVATE_RMA_ACC       ((nm_tag_t)(NM_MPI_TAG_PRIVATE_RMA_BASE | 0x3)) /**< accumulate */
#define NM_MPI_TAG_PRIVATE_RMA_GACC_REQ  ((nm_tag_t)(NM_MPI_TAG_PRIVATE_RMA_BASE | 0x4)) /**< get_accumulate */
#define NM_MPI_TAG_PRIVATE_RMA_FAO_REQ   ((nm_tag_t)(NM_MPI_TAG_PRIVATE_RMA_BASE | 0x5)) /**< fetch and op */
#define NM_MPI_TAG_PRIVATE_RMA_CAS_REQ   ((nm_tag_t)(NM_MPI_TAG_PRIVATE_RMA_BASE | 0x6)) /**< compare and swap */
#define NM_MPI_TAG_PRIVATE_RMA_REQ_RESP  ((nm_tag_t)(NM_MPI_TAG_PRIVATE_RMA_BASE | 0xF))


void nm_mpi_info_destructor(char*p_key, char*p_data);

/** hashtable type for info entries */
PUK_HASHTABLE_TYPE(nm_mpi_info, char*, char*,
                   &puk_hash_string_default_hash, &puk_hash_string_default_eq, &nm_mpi_info_destructor);

/** content for MPI_Info */
struct nm_mpi_info_s
{
  int id; /**< object ID */
  struct nm_mpi_info_hashtable_s content; /**< hashtable of <keys, values> */
};

/** @name keyval and attributes
 * @{ */

/** subroutine types for FORTRAN attributes binding */
typedef void (nm_mpi_copy_subroutine_t)(int*id, int*keyval, void*extra_state,
                                        void*attribute_val_in, void*attribute_val_out, int*flag, int*ierr);
typedef void (nm_mpi_delete_subroutine_t)(int*id, int*keyval, void*attribute_val, void*extra_state, int*ierr);

/** generic C type for comm/win/types attributes */
typedef int (nm_mpi_attr_copy_fn_t)(int object_id, int keyval_id, void*extra_state,
                                    void*attribute_val_in, void*attribute_val_out, int*flag);
typedef int (nm_mpi_attr_delete_fn_t)(int object_id, int keyval_id, void*attribute_val, void*extra_state);

/** a keyval used to index comm/types/win attributes */
struct nm_mpi_keyval_s
{
  int id;                                         /**< ID of this keyval */
  nm_mpi_attr_copy_fn_t*copy_fn;                  /**< copy callback function for C binding */
  nm_mpi_attr_delete_fn_t*delete_fn;              /**< delete callback function for C binding */
  nm_mpi_copy_subroutine_t*copy_subroutine;       /**< copy callback function for Fortran binding */
  nm_mpi_delete_subroutine_t*delete_subroutine;   /**< delete callback function for Fortran binding */
  void*extra_state;                               /**< user-supplied pointer given to callbacks */
  struct nm_mpi_refcount_s refcount;              /**< number of attributes indexed by this keyval */
};

void nm_mpi_attr_destructor(struct nm_mpi_keyval_s*p_keyval, void*p_value);
PUK_HASHTABLE_TYPE(nm_mpi_attr, struct nm_mpi_keyval_s*, void*,
                   &puk_hash_pointer_default_hash,
                   &puk_hash_pointer_default_eq,
                   &nm_mpi_attr_destructor);

/** an attribute table */
struct nm_mpi_attrs_s
{
  struct nm_mpi_attr_hashtable_s table;
  int id; /**< ID of the object this table is attached to (comm, datatype, win, ...) */
};

/** @} */

/** @name Communicators
 * @{ */

/** Internal group */
typedef struct nm_mpi_group_s
{
  int id;                 /**< ID of the group (handle) */
  nm_group_t p_nm_group;  /**< underlying nmad group */
} nm_mpi_group_t;

/** Internal communicator */
typedef struct nm_mpi_communicator_s
{
  int id;                            /**< id of the communicator */
  nm_comm_t p_nm_comm;               /**< underlying nmad intra-communicator (or overlay for inter-communicator) */
  struct nm_mpi_attrs_s attrs;       /**< communicator attributes, hashed by keyval descriptor */
  struct nm_mpi_info_s*p_info;       /**< info hints */
  nm_mpi_errhandler_t*p_errhandler;  /**< error handler attached to communicator */
  char*name;                         /**< communicator name */
  struct nm_mpi_refcount_s refcount; /**< number of references pointing to this type (active communications, handle) */
  enum nm_mpi_communicator_kind_e
    {
      NM_MPI_COMMUNICATOR_UNSPEC = 0,
      NM_MPI_COMMUNICATOR_INTRA  = 1,
      NM_MPI_COMMUNICATOR_INTER
    } kind;
  enum nm_mpi_topology_kind_e
    {
      NM_MPI_TOPOLOGY_NONE  = MPI_UNDEFINED,
      NM_MPI_TOPOLOGY_GRAPH = MPI_GRAPH,
      NM_MPI_TOPOLOGY_CART  = MPI_CART
    } topo;
  struct nm_mpi_cart_topology_s  /**< cartesian topology */
  {
    int ndims;    /**< number of dimensions */
    int*dims;     /**< number of procs in each dim. */
    int*periods;  /**< whether each dim. is periodic */
    int size;     /**< pre-computed size of cartesian topology */
  } cart_topology;
  struct nm_mpi_intercomm_s
  {
    nm_group_t p_local_group;
    nm_group_t p_remote_group;
    int rank;          /**< rank of self in local group */
    int remote_offset; /**< offset of remote group in global nm_comm */
  } intercomm;
} nm_mpi_communicator_t;
/** @} */

/** @name Session
 * @{ */
struct nm_mpi_session_s
{
  int id;
  nm_mpi_errhandler_t*p_errhandler;  /**< error handler attached to session */
  struct nm_mpi_info_s*p_info;
};
/** @} */

/** @name Window
 * @{ */
struct nm_mpi_window_s;
typedef struct nm_mpi_window_s nm_mpi_window_t;
struct nm_mpi_win_epoch_s;
typedef struct nm_mpi_win_epoch_s nm_mpi_win_epoch_t;
/** @} */

/** @name Requests
 * @{ */
/** Type of a communication request */
typedef int32_t nm_mpi_request_type_t;
#define NM_MPI_REQUEST_ZERO       ((nm_mpi_request_type_t)0x0000)
#define NM_MPI_REQUEST_RECV       ((nm_mpi_request_type_t)0x0001)
#define NM_MPI_REQUEST_PRECV      ((nm_mpi_request_type_t)0x0041)
#define NM_MPI_REQUEST_PROBE      ((nm_mpi_request_type_t)0x0002)
#define NM_MPI_REQUEST_SEND       ((nm_mpi_request_type_t)0x0004)
#define NM_MPI_REQUEST_RSEND      ((nm_mpi_request_type_t)0x0014)
#define NM_MPI_REQUEST_SSEND      ((nm_mpi_request_type_t)0x0024)
#define NM_MPI_REQUEST_PSEND      ((nm_mpi_request_type_t)0x0044)
#define NM_MPI_REQUEST_FILE       ((nm_mpi_request_type_t)0x0008)
#define NM_MPI_REQUEST_GREQUEST   ((nm_mpi_request_type_t)0x0080)
#define NM_MPI_REQUEST_IBARRIER   ((nm_mpi_request_type_t)0x1100)
#define NM_MPI_REQUEST_IBCAST     ((nm_mpi_request_type_t)0x2100)
#define NM_MPI_REQUEST_IREDUCE    ((nm_mpi_request_type_t)0x3100)
#define NM_MPI_REQUEST_IALLREDUCE ((nm_mpi_request_type_t)0x4100)
#define NM_MPI_REQUEST_IALLTOALL  ((nm_mpi_request_type_t)0x5100)
#define NM_MPI_REQUEST_IALLTOALLV ((nm_mpi_request_type_t)0x6100)
#define NM_MPI_REQUEST_IGATHER    ((nm_mpi_request_type_t)0x7100)
#define NM_MPI_REQUEST_IALLGATHER ((nm_mpi_request_type_t)0x8100)

/** bitmask for collective operations */
#define NM_MPI_REQUEST_COLLECTIVE  ((nm_mpi_request_type_t)0x0100)
/** bitmask for partitioned operations */
#define NM_MPI_REQUEST_PARTITIONED ((nm_mpi_request_type_t)0x0040)

typedef int8_t nm_mpi_status_t;
#define NM_MPI_STATUS_NONE        ((nm_mpi_status_t)0x00)
#define NM_MPI_REQUEST_CANCELLED  ((nm_mpi_status_t)0x04) /**< request has been cancelled */
#define NM_MPI_REQUEST_PERSISTENT ((nm_mpi_status_t)0x08) /**< request is persistent */

#define _NM_MPI_MAX_DATATYPE_SIZE 64

PUK_LIST_DECLARE_TYPE(nm_mpi_request);

/** Internal communication request */
typedef struct nm_mpi_request_s
{
  /** identifier of the request */
  MPI_Request id;
  /** nmad request for sendrecv interface */
  nm_sr_request_t request_nmad;
  /** type of the request */
  nm_mpi_request_type_t request_type;
  /** status of request */
  nm_mpi_status_t status;
  /** tag given by the user*/
  int user_tag;
  /** rank of the source node (used for incoming request) */
  int request_source;
  /** error status of the request, using MPI error codes */
  int request_error;
  /** type of the exchanged data */
  struct nm_mpi_datatype_s*p_datatype;
  /** second datatype for collectives that need different types for send & recv; used only for ref counting */
  struct nm_mpi_datatype_s*p_datatype2;
  /** gate of the destination or the source node */
  nm_gate_t gate;
  /** communicator used for communication */
  nm_mpi_communicator_t*p_comm;
  /** number of elements to be exchanged */
  nm_mpi_count_t count;
  /** pointer to the data to be exchanged */
  union
  {
    void*rbuf;       /**< pointer used for receiving */
    const void*sbuf; /**< pointer for sending */
    char static_buf[_NM_MPI_MAX_DATATYPE_SIZE]; /**< static buffer of max predefined datatype size */
    struct nm_mpi_file_op_s*p_file_op;          /**< MPI-IO operation */
    struct nm_mpi_grequest_s*p_grequest;        /**< generalized request */
  };
  /** variable fields depending on request type */
  union
  {
    struct
    {
      struct nm_coll_req_s*p_coll_req; /**< non-blocking nmad native collective */
    } collective;
    struct
    {
      int partitions;             /**< number of partitions */
    } partitioned;
    struct nm_mpi_grequest_s
    {
      MPI_Grequest_query_function*query_fn;
      MPI_Grequest_free_function*free_fn;
      MPI_Grequest_cancel_function*cancel_fn;
      void*extra_state;
      nm_cond_status_t completed; /**< set to NM_STATUS_FINALIZED when user signals completion */
    } grequest;
    struct
    {
      nm_mpi_win_epoch_t*p_epoch; /**< corresponding epoch management structure for rma operations */
      nm_mpi_window_t*p_win;      /**< corresponding rma window */
    } rma; /**< fields for send/recv requests created by the RMA operations */
  };
  /** Link for nm_mpi_reqlist_t lists */
  PUK_LIST_LINK(nm_mpi_request);
} __attribute__((__may_alias__)) nm_mpi_request_t;

PUK_LIST_CREATE_FUNCS(nm_mpi_request);

/** @} */

/** @name Reduce operators
 * @{ */

/** Internal reduce operators */
typedef struct nm_mpi_operator_s
{
  int id;
  MPI_User_function*function;
  MPI_User_function_c*function_c;
  int commute;
} nm_mpi_operator_t;
/** @} */

/** @name Datatypes
 * @{ */

/** a hash to identify a datatype across nodes */
typedef uint32_t nm_mpi_datatype_hash_t;

/** Serialized version of the internal datatype */
/** @note: each array in the types are inlined */
typedef struct nm_mpi_datatype_ser_s
{
  /** combiner of datatype elements */
  nm_mpi_type_combiner_t combiner;
  /** number of blocks in type map */
  nm_mpi_count_t count;
  /** size of the datatype once serialized */
  size_t ser_size;
  /** (probably unique) datatype hash */
  nm_mpi_datatype_hash_t hash;
  /** combiner specific parameters */
  union nm_mpi_datatype_ser_param_u
  {
    struct nm_mpi_datatype_ser_param_dup_s
    {
      nm_mpi_datatype_hash_t old_type;
    } DUP;
    struct nm_mpi_datatype_ser_param_resized_s
    {
      nm_mpi_datatype_hash_t old_type;
      nm_mpi_count_t lb;
      nm_mpi_count_t extent;
    } RESIZED;
    struct nm_mpi_datatype_ser_param_contiguous_s
    {
      nm_mpi_datatype_hash_t old_type;
    } CONTIGUOUS;
    struct nm_mpi_datatype_ser_param_vector_s
    {
      nm_mpi_datatype_hash_t old_type;
      nm_mpi_count_t stride;       /**< stride in multiple of datatype extent */
      nm_mpi_count_t blocklength;
    } VECTOR;
    struct nm_mpi_datatype_ser_param_hvector_s
    {
      nm_mpi_datatype_hash_t old_type;
      nm_mpi_count_t hstride; /**< stride in bytes */
      nm_mpi_count_t blocklength;
    } HVECTOR;
    struct nm_mpi_datatype_ser_param_indexed_s
    {
      nm_mpi_datatype_hash_t old_type;
      /**
       * The two following arrays inlined in the structure :
       * int*blocklengths;
       * int*displacements; < displacement in multiple of oldtype extent
       */
      void*DATA;
    } INDEXED;
    struct nm_mpi_datatype_ser_param_hindexed_s
    {
      nm_mpi_datatype_hash_t old_type;
      /**
       * The two following arrays inlined in the structure :
       * int*blocklengths;
       * MPI_Aint*displacements; < displacement in bytes
       */
      void*DATA;
    } HINDEXED;
    struct nm_mpi_datatype_ser_param_indexed_block_s
    {
      nm_mpi_datatype_hash_t old_type;
      int blocklength;
      /**
       * The following array inlined in the structure :
       * int*displacements;
       */
      void*DATA;
    } INDEXED_BLOCK;
    struct nm_mpi_datatype_ser_param_hindexed_block_s
    {
      nm_mpi_datatype_hash_t old_type;
      int blocklength;
      /**
       * The following array inlined in the structure :
       * MPI_Aint*displacements; < displacement in bytes
       */
      void*DATA;
    } HINDEXED_BLOCK;
    struct nm_mpi_datatype_ser_param_subarray_s
    {
      nm_mpi_datatype_hash_t old_type;
      int ndims;
      int order;
      /**
       * The three following arrays inlined in the structure :
       * int*sizes;
       * int*subsizes;
       * int*starts;
       */
      void*DATA;
    } SUBARRAY;
    struct nm_mpi_datatype_ser_param_struct_s
    {
      /**
       * The three following arrays inlined in the structure :
       * nm_mpi_datatype_hash_t*old_types;
       * int*blocklengths;
       * MPI_Aint*displacements;  < displacement in bytes
       */
      void*DATA;
    } STRUCT;
  } p;
} nm_mpi_datatype_ser_t;

/** Internal datatype */
typedef struct nm_mpi_datatype_s
{
  int id;
  /** combiner of datatype elements */
  nm_mpi_type_combiner_t combiner;
  /** number of blocks in type map */
  nm_mpi_count_t count;
  /** whether contiguous && lb == 0 && extent == size */
  int is_compact;
  /** whether entirely contiguous */
  int is_contig;
  /** number of base elements in the datatype */
  nm_mpi_count_t elements;
  /** lower bound of type */
  nm_mpi_count_t lb;
  /** extent of type; upper bound is lb + extent */
  nm_mpi_count_t extent;
  nm_mpi_count_t true_lb;
  nm_mpi_count_t true_extent;
  /** size of type */
  size_t size;
  /** pre-computed data properties */
  struct nm_data_properties_s props;
  /** number of references pointing to this type (active communications, handle) */
  struct nm_mpi_refcount_s refcount;
  /** whether committed or not */
  int committed;
  union
  {
    struct
    {
      struct nm_mpi_datatype_s*p_old_type;
    } DUP;
    struct
    {
      struct nm_mpi_datatype_s*p_old_type;
    } RESIZED;
    struct
    {
      struct nm_mpi_datatype_s*p_old_type;
    } CONTIGUOUS;
    struct
    {
      struct nm_mpi_datatype_s*p_old_type;
      nm_mpi_count_t blocklength;
      nm_mpi_aint_t stride;       /**< stride in multiple of datatype extent */
    } VECTOR;
    struct
    {
      struct nm_mpi_datatype_s*p_old_type;
      nm_mpi_count_t blocklength;
      nm_mpi_aint_t hstride; /**< stride in bytes */
    } HVECTOR;
    struct
    {
      struct nm_mpi_datatype_s*p_old_type;
      struct nm_mpi_type_indexed_map_s
      {
        nm_mpi_count_t blocklength;
        nm_mpi_aint_t displacement; /**< displacement in multiple of oldtype extent */
      } *p_map;
    } INDEXED;
    struct
    {
      struct nm_mpi_datatype_s*p_old_type;
      struct nm_mpi_type_hindexed_map_s
      {
        nm_mpi_count_t blocklength;
        nm_mpi_aint_t displacement; /**< displacement in bytes */
      } *p_map;
    } HINDEXED;
    struct
    {
      struct nm_mpi_datatype_s*p_old_type;
      nm_mpi_count_t blocklength;
      nm_mpi_count_t*array_of_displacements;
    } INDEXED_BLOCK;
    struct
    {
      struct nm_mpi_datatype_s*p_old_type;
      nm_mpi_count_t blocklength;
      nm_mpi_aint_t*array_of_displacements;
    } HINDEXED_BLOCK;
    struct
    {
      struct nm_mpi_datatype_s*p_old_type;
      int ndims;
      int order;
      struct nm_mpi_type_subarray_dim_s
      {
        int size;
        int subsize;
        int start;
      } *p_dims;
    } SUBARRAY;
    struct
    {
      struct nm_mpi_type_struct_map_s
      {
        struct nm_mpi_datatype_s*p_old_type;
        nm_mpi_count_t blocklength;
        nm_mpi_aint_t displacement; /**< displacement in bytes */
      } *p_map;
    } STRUCT;
  };
  char*name;
  struct nm_mpi_attrs_s attrs; /**< datatype attributes, hashed by keyval descriptor */
  size_t ser_size;       /**< size of the datatype once serialized */
  nm_mpi_datatype_ser_t*p_serialized; /**< serialized version of the type. */
  nm_mpi_datatype_hash_t hash;        /**< (probably unique) datatype hash */
} nm_mpi_datatype_t;

/** content for datatype traversal */
struct nm_data_mpi_datatype_s
{
  void*ptr;                             /**< pointer to base data */
  struct nm_mpi_datatype_s*p_datatype;  /**< datatype describing data layout */
  nm_mpi_count_t count;                 /**< number of elements */
};
__PUK_SYM_INTERNAL extern const struct nm_data_ops_s nm_mpi_datatype_ops;
NM_DATA_TYPE(mpi_datatype, struct nm_data_mpi_datatype_s, &nm_mpi_datatype_ops);

/** function apply to each datatype or sub-datatype upon traversal */
typedef void (*nm_mpi_datatype_apply_t)(void*ptr, nm_len_t len, void*_ref);

struct nm_data_mpi_datatype_header_s
{
  nm_mpi_aint_t offset;        /**< data offset */
  nm_mpi_datatype_hash_t datatype_hash; /**< datatype-to-be-send hash */
  nm_mpi_count_t count;   /**< number of elements */
};

/** content for datatype traversal and sending */
struct nm_data_mpi_datatype_wrapper_s
{
  struct nm_data_mpi_datatype_header_s header; /**< header for datatype */
  struct nm_data_mpi_datatype_s data;          /**< datatype describing data layout */
};
__PUK_SYM_INTERNAL extern const struct nm_data_ops_s nm_mpi_datatype_wrapper_ops;
NM_DATA_TYPE(mpi_datatype_wrapper, struct nm_data_mpi_datatype_wrapper_s, &nm_mpi_datatype_wrapper_ops);

__PUK_SYM_INTERNAL extern const struct nm_data_ops_s nm_mpi_datatype_serialize_ops;
NM_DATA_TYPE(mpi_datatype_serial, struct nm_mpi_datatype_s*, &nm_mpi_datatype_serialize_ops);

/** @} */

/** @name Window
 * @{ */

/** @name Requests list with its locks
 * @{ */

typedef struct nm_mpi_win_locklist_s
{
  nm_mpi_spinlock_t lock;       /**< lock for pending requests access */
  struct nm_mpi_request_list_s pending; /**< pending requests */
} nm_mpi_win_locklist_t;

/** @} */

/** @name Asynchronous management for incoming request
 * @{ */

struct nm_mpi_win_epoch_s
{
  volatile int mode;  /**< window mode for the given pair */
  uint64_t nmsg;      /**< number of messages exchanges during this epoch */
  uint64_t completed; /**< counter of completed requests */
};

/** @} */

/** @name Management structure for passive target mode
 * @{ */

typedef struct nm_mpi_win_pass_mngmt_s
{
  uint64_t excl_pending;   /**< whether there is an exclusive lock request pending */
  uint16_t lock_type;      /**< current lock type. 0 = NONE, 1 = EXCLUSIVE, 2 = SHARED */
  uint64_t naccess;        /**< number of procs that are currently accessing this window */
  nm_mpi_win_locklist_t q; /**< lock protected pending lock request list */
} nm_mpi_win_pass_mngmt_t;

/** @} */

/** Content for MPI_Win */
struct nm_mpi_window_s
{
  /** identifier of the window */
  int id;
  /** window's rank into the communicator */
  int myrank;
  /** data base address of the window */
  void*p_base;
  /** window's memory size */
  nm_mpi_aint_t*size;
  /** displacement unit */
  int*disp_unit;
  /** Distant windows id */
  int*win_ids;
  /** window communicator */
  nm_mpi_communicator_t*p_comm;
  /** group of processor that interact with this window */
  nm_group_t p_group;
  /** window core monitor for asynchronous call */
  struct nm_sr_monitor_s monitor;
  /** window core monitor for asynchronous lock */
  struct nm_sr_monitor_s monitor_sync;
  /**   WINDOW ATTRIBUTES   */
  /** infos about the window */
  struct nm_mpi_info_s*p_info;
  /** bit-vector window's flavor (the function used to allocate the window */
  int flavor;
  /** bit-vector OR of MPI_MODE_* constants (used in assert) */
  int mode;
  /** window memory model */
  int mem_model;
  /** error handler attached to window */
  nm_mpi_errhandler_t*p_errhandler;
  /** window name */
  char*name;
  /** window attributes, hashed by keyval descriptor */
  struct nm_mpi_attrs_s attrs;
  /**   SEQUENCE/EPOCH NUMBER MANAGEMENT   */
  /** sequence number for thread safety and message scheduling
   *  /!\ WARNING : As this number is on a 16 bits uint, if a lot of
   *  operations are issued (more than 65536 per epoch for one
   *  target), there is a risk for mismatching responses.
   */
  uint16_t next_seq;
  /** epoch management */
  nm_mpi_win_epoch_t*access;
  nm_mpi_win_epoch_t*exposure;
  /** pending closing exposing epoch requests */
  nm_mpi_request_t**end_reqs;
  /** counter to receive the amount of messages to be expected from a given distant window */
  uint64_t*msg_count;
  /** management queue for passive target */
  nm_mpi_win_pass_mngmt_t waiting_queue;
  /** pending passive RMA operation */
  nm_mpi_win_locklist_t*pending_ops;
  /** memory access protecting lock */
  nm_mpi_spinlock_t lock;
  /** shared memory file name */
  char shared_file_name[32];
};

/** @name Target communication modes
 * @{ */
#define NM_MPI_WIN_UNUSED                 0
#define NM_MPI_WIN_ACTIVE_TARGET          1
#define NM_MPI_WIN_PASSIVE_TARGET         2
#define NM_MPI_WIN_PASSIVE_TARGET_END     4
#define NM_MPI_WIN_PASSIVE_TARGET_PENDING 8
/** @} */

/** @name Lock types for private use
 * @{ */
#define NM_MPI_LOCK_NONE      ((uint16_t)0)
#define NM_MPI_LOCK_EXCLUSIVE ((uint16_t)MPI_LOCK_EXCLUSIVE)
#define NM_MPI_LOCK_SHARED    ((uint16_t)MPI_LOCK_SHARED)
#define _NM_MPI_LOCK_OFFSET   ((uint16_t)3)
/** @} */

/** @} */

#ifdef NMAD_PROFILE
/** performance analysis information */
struct nm_mpi_profiling_s
{
  unsigned long cur_req_send, max_req_send;
  unsigned long cur_req_recv, max_req_recv;
  unsigned long cur_req_total, max_req_total;
};
extern struct nm_mpi_profiling_s nm_mpi_profile;
#endif /* NMAD_PROFILE */

/* ********************************************************* */

#ifdef NMAD_DEBUG
#define NM_MPI_HANDLE_DEBUG 1
#else /* NMAD_DEBUG */
#define NM_MPI_HANDLE_DEBUG 0
#endif /* NMAD_DEBUG */


/** typed handle manager and object allocator for objects indexed by ID
 */
#define NM_MPI_HANDLE_TYPE(ENAME, TYPE, OFFSET, SLABSIZE)               \
  PUK_VECT_TYPE(nm_mpi_entry_ ## ENAME, TYPE*);                         \
  NM_ALLOCATOR_TYPE(ENAME, TYPE);                                       \
  struct nm_mpi_handle_##ENAME##_s                                      \
  {                                                                     \
    struct nm_mpi_entry_##ENAME##_vect_s table;                         \
    struct puk_int_vect_s pool; /**< pool of recently freed entries */  \
    int next_id;                /**< next id to allocate, if pool is empty */ \
    nm_mpi_spinlock_t lock;                                             \
    ENAME##_allocator_t allocator;                                      \
    char*(*display)(TYPE*);                                             \
  };                                                                    \
  __attribute__((unused))                                               \
  static int nm_mpi_handle_##ENAME##_initialized(struct nm_mpi_handle_##ENAME##_s*p_allocator) \
  {                                                                     \
    return (p_allocator->allocator != NULL);                            \
  }                                                                     \
  /** get the number of entries in the table */                         \
  __attribute__((unused))                                               \
  static int nm_mpi_handle_##ENAME##_load(struct nm_mpi_handle_##ENAME##_s*p_allocator) \
  {                                                                     \
    int load = 0;                                                       \
    nm_mpi_entry_##ENAME##_vect_itor_t i;                               \
    puk_vect_foreach(i, nm_mpi_entry_##ENAME, &p_allocator->table)      \
      {                                                                 \
        if(*i != NULL)                                                  \
          {                                                             \
            load++;                                                     \
          }                                                             \
      }                                                                 \
    return load;                                                        \
    }                                                                   \
  /** display the content of the table (dynamic entries only) */        \
  __attribute__((unused))                                               \
  static void nm_mpi_handle_##ENAME##_dump(struct nm_mpi_handle_##ENAME##_s*p_allocator) \
  {                                                                     \
    if( (p_allocator->allocator != NULL) &&                             \
        !nm_mpi_entry_##ENAME##_vect_empty(&p_allocator->table))        \
      {                                                                 \
        nm_mpi_entry_##ENAME##_vect_itor_t i;                           \
        puk_vect_foreach(i, nm_mpi_entry_##ENAME, &p_allocator->table)  \
          {                                                             \
            if(*i != NULL)                                              \
              {                                                         \
                const int index = nm_mpi_entry_##ENAME##_vect_rank(&p_allocator->table, i); \
                if(index != (*i)->id)                                   \
                  {                                                     \
                    NM_MPI_WARNING("inconsistency in " #ENAME " entry; index = %d; id = %d\n", index, (*i)->id); \
                  }                                                     \
                if(index >= (OFFSET))                                   \
                  {                                                     \
                    char*d = NULL;                                     \
                    if(p_allocator->display != NULL)                    \
                      {                                                 \
                        d = (*p_allocator->display)(*i);                \
                      }                                                 \
                    NM_MPI_WARNING("pending object; type = " #ENAME "; id = %d; ptr = %p; %s\n", (*i)->id, *i, \
                                   (d ? d : ""));                       \
                    if(d) padico_free(d);                               \
                  }                                                     \
              }                                                         \
          }                                                             \
      }                                                                 \
  }                                                                     \
  /** initialize the allocator */                                       \
  __attribute__((unused))                                               \
  static void nm_mpi_handle_##ENAME##_init(struct nm_mpi_handle_##ENAME##_s*p_allocator) \
  {                                                                     \
    nm_mpi_entry_##ENAME##_vect_init(&p_allocator->table);              \
    puk_int_vect_init(&p_allocator->pool);                              \
    p_allocator->next_id = OFFSET;                                      \
    nm_mpi_spin_init(&p_allocator->lock);                               \
    p_allocator->display = NULL;                                        \
    p_allocator->allocator = ENAME ## _allocator_new(SLABSIZE);         \
    int i;                                                              \
    for(i = 0; i < OFFSET; i++)                                         \
      {                                                                 \
        nm_mpi_entry_##ENAME##_vect_push_back(&p_allocator->table, NULL); \
      }                                                                 \
    nm_mpi_cleanup_register((void (*)(void*))&nm_mpi_handle_ ## ENAME ## _dump, p_allocator); \
  }                                                                     \
  __attribute__((unused))                                               \
  static void nm_mpi_handle_##ENAME##_finalize(struct nm_mpi_handle_##ENAME##_s*p_allocator, void(*destructor)(TYPE*)) \
  {                                                                     \
    nm_mpi_cleanup_unregister((void (*)(void*))&nm_mpi_handle_##ENAME##_dump, p_allocator); \
    if(NM_MPI_HANDLE_DEBUG)                                             \
      nm_mpi_handle_##ENAME##_dump(p_allocator);                        \
    if(destructor != NULL)                                              \
      {                                                                 \
        nm_mpi_entry_##ENAME##_vect_itor_t i;                           \
        puk_vect_foreach(i, nm_mpi_entry_##ENAME, &p_allocator->table)  \
          {                                                             \
            if(*i != NULL)                                              \
              {                                                         \
                (*destructor)(*i);                                      \
              }                                                         \
          }                                                             \
      }                                                                 \
    nm_mpi_entry_##ENAME##_vect_destroy(&p_allocator->table);           \
    puk_int_vect_destroy(&p_allocator->pool);                           \
    ENAME##_allocator_delete(p_allocator->allocator);                   \
    p_allocator->allocator = NULL;                                      \
  }                                                                     \
  /** store a builtin object (constant ID) */                           \
  __attribute__((unused))                                               \
  static TYPE*nm_mpi_handle_##ENAME##_store(struct nm_mpi_handle_##ENAME##_s*p_allocator, int id) \
  {                                                                     \
    assert(nm_mpi_handle_##ENAME##_initialized(p_allocator));           \
    TYPE*e = ENAME ## _malloc(p_allocator->allocator);                  \
    if((id <= 0) || (id > OFFSET))                                      \
      {                                                                 \
        NM_MPI_FATAL_ERROR("madmpi: cannot store invalid %s handle id %d\n", #ENAME, id); \
      }                                                                 \
    if(nm_mpi_entry_##ENAME##_vect_at(&p_allocator->table, id) != NULL) \
      {                                                                 \
        NM_MPI_FATAL_ERROR("madmpi: %s handle %d busy; cannot store.", #ENAME, id); \
      }                                                                 \
    nm_mpi_entry_##ENAME##_vect_put(&p_allocator->table, e, id);        \
    e->id = id;                                                         \
    return e;                                                           \
  }                                                                     \
  /** allocate a dynamic entry */                                       \
  __attribute__((unused))                                               \
  static TYPE*nm_mpi_handle_##ENAME##_alloc(struct nm_mpi_handle_##ENAME##_s*p_allocator) \
  {                                                                     \
    assert(nm_mpi_handle_##ENAME##_initialized(p_allocator));           \
    TYPE*e = ENAME ## _malloc(p_allocator->allocator);                  \
    int new_id = -1;                                                    \
    nm_mpi_spin_lock(&p_allocator->lock);                               \
    if(puk_int_vect_empty(&p_allocator->pool))                          \
      {                                                                 \
        new_id = p_allocator->next_id++;                                \
        nm_mpi_entry_##ENAME##_vect_resize(&p_allocator->table, new_id); \
      }                                                                 \
    else                                                                \
      {                                                                 \
        new_id = puk_int_vect_pop_back(&p_allocator->pool);             \
      }                                                                 \
    nm_mpi_entry_##ENAME##_vect_put(&p_allocator->table, e, new_id);    \
    nm_mpi_spin_unlock(&p_allocator->lock);                             \
    e->id = new_id;                                                     \
    return e;                                                           \
  }                                                                     \
  /** release the ID, but do not free block */                          \
  __attribute__((unused))                                               \
  static void nm_mpi_handle_##ENAME##_release(struct nm_mpi_handle_##ENAME##_s*p_allocator, TYPE*e) \
  {                                                                     \
    const int id = e->id;                                               \
    assert(id > 0);                                                     \
    nm_mpi_spin_lock(&p_allocator->lock);                               \
    assert(nm_mpi_entry_##ENAME##_vect_at(&p_allocator->table, id) == e); \
    nm_mpi_entry_##ENAME##_vect_put(&p_allocator->table, NULL, id);     \
    puk_int_vect_push_back(&p_allocator->pool, id);                     \
    e->id = -1;                                                         \
    nm_mpi_spin_unlock(&p_allocator->lock);                             \
  }                                                                     \
  /** free an entry  */                                                 \
  __attribute__((unused))                                               \
  static void nm_mpi_handle_##ENAME##_free(struct nm_mpi_handle_##ENAME##_s*p_allocator, TYPE*e) \
  {                                                                     \
    if(e->id > 0)                                                       \
      nm_mpi_handle_##ENAME##_release(p_allocator, e);                  \
    ENAME ## _free(p_allocator->allocator, e);                          \
  }                                                                     \
  /** get a pointer on an entry from its ID */                          \
  __attribute__((unused))                                               \
  static TYPE*nm_mpi_handle_##ENAME##_get(struct nm_mpi_handle_##ENAME##_s*p_allocator, int id) \
  {                                                                     \
    assert(nm_mpi_handle_##ENAME##_initialized(p_allocator));           \
    TYPE*e = NULL;                                                      \
    if((id > 0) && (id < nm_mpi_entry_##ENAME##_vect_size(&p_allocator->table))) \
      {                                                                 \
        nm_mpi_spin_lock(&p_allocator->lock);                           \
        e = nm_mpi_entry_##ENAME##_vect_at(&p_allocator->table, id);    \
        nm_mpi_spin_unlock(&p_allocator->lock);                         \
      }                                                                 \
    else                                                                \
      {                                                                 \
        return NULL;                                                    \
      }                                                                 \
    return e;                                                           \
  }                                                                     \
  __attribute__((unused))                                               \
  static void nm_mpi_handle_##ENAME##_set_display(struct nm_mpi_handle_##ENAME##_s*p_allocator, char*(*display)(TYPE*)) \
  {                                                                     \
    p_allocator->display = display;                                     \
  }

#define NM_MPI_HANDLE_NULL { .allocator = NULL, .next_id = -1 }


/* ********************************************************* */

/* ** submodules init/exit */

/** increment refcount to 'err' submodule */
void nm_mpi_err_lazy_init(void);
/** decrement refcount to 'err' submodule */
void nm_mpi_err_lazy_exit(void);

/** init request sub-system */
void nm_mpi_request_lazy_init(void);

void nm_mpi_request_lazy_exit(void);

/** init communicator sub-system */
void nm_mpi_comm_lazy_init(void);

void nm_mpi_comm_lazy_exit(void);

void nm_mpi_session_lazy_init(void);

void nm_mpi_session_lazy_exit(void);

/** increment refcount for 'group' sub-system */
void nm_mpi_group_lazy_init(void);

void nm_mpi_group_lazy_exit(void);

/** init datatype subsystem */
void nm_mpi_datatype_lazy_init(void);
void nm_mpi_datatype_lazy_exit(void);

void nm_mpi_datatype_exchange_init(void);
void nm_mpi_datatype_exchange_exit(void);

void nm_mpi_op_lazy_init(void);

void nm_mpi_op_lazy_exit(void);

/** init MPI I/O sub-system */
void nm_mpi_io_init(void);

void nm_mpi_io_exit(void);

void nm_mpi_attrs_lazy_init(void);

void nm_mpi_attrs_lazy_exit(void);

/** init window sub-system */
void nm_mpi_win_init(void);

void nm_mpi_win_exit(void);

/** init rma sub-system */
void nm_mpi_rma_init(void);

void nm_mpi_rma_exit(void);

void nm_mpi_info_lazy_init(void);
void nm_mpi_info_lazy_exit(void);

void nm_mpi_cleanup_register(void(*p_func)(void*), void*p_arg);
void nm_mpi_cleanup_unregister(void(*p_func)(void*), void*p_arg);


/* ** Communicators **************************************** */

/** Gets the in/out gate for the given node */
static inline nm_gate_t nm_mpi_communicator_get_gate(nm_mpi_communicator_t*p_comm, int node)
{
  assert(p_comm->kind != NM_MPI_COMMUNICATOR_UNSPEC);
  if(p_comm->kind == NM_MPI_COMMUNICATOR_INTRA)
    return nm_comm_get_gate(p_comm->p_nm_comm, node);
  else if(p_comm->kind == NM_MPI_COMMUNICATOR_INTER)
    return nm_group_get_gate(p_comm->intercomm.p_remote_group, node);
  else
    NM_MPI_FATAL_ERROR("wrong kind %d for communicator %d.\n", p_comm->kind, p_comm->id);
  return NULL;
}

/** Gets the node associated to the given gate */
static inline int nm_mpi_communicator_get_dest(nm_mpi_communicator_t*p_comm, nm_gate_t p_gate)
{
  int rank = nm_comm_get_dest(p_comm->p_nm_comm, p_gate);
  if(p_comm->kind == NM_MPI_COMMUNICATOR_INTER)
    {
      rank -= p_comm->intercomm.remote_offset;
      assert(rank < nm_group_size(p_comm->intercomm.p_remote_group));
      assert(rank >= 0);
    }
  return rank;
}

static inline nm_session_t nm_mpi_communicator_get_session(nm_mpi_communicator_t*p_comm)
{
  return nm_comm_get_session(p_comm->p_nm_comm);
}


/* ** Groups *********************************************** */

/**
 * Allocate a new internal representation of a group.
 */
nm_mpi_group_t*nm_mpi_group_alloc(void);

/**
 * Gets the internal representation of the given group.
 */
nm_mpi_group_t*nm_mpi_group_get(MPI_Group group);


/* ** Err handlers ***************************************** */

/**
 * Allocate a new internal representation of a errhandler.
 */
struct nm_mpi_errhandler_s*nm_mpi_errhandler_alloc(enum nm_mpi_errhandler_kind_e kind, MPI_Handler_function*func);

/**
 * Gets the internal representation of the given errhandler.
 */
struct nm_mpi_errhandler_s*nm_mpi_errhandler_get(int errhandler, enum nm_mpi_errhandler_kind_e kind);

void nm_mpi_errhandler_exec_internal(struct nm_mpi_errhandler_s*p_errhandler, enum nm_mpi_errhandler_kind_e kind, int handle, int err,
                                     const char*func, const char*file, const int line);

/** get the last used error class */
int nm_mpi_err_lastused(void);

/* ** Info Operations ************************************** */

/**
 * Allocates a new instance of the internal representation of an info.
 */
struct nm_mpi_info_s*nm_mpi_info_alloc(void);

/**
 * Frees the given instance of the internal representation of an info.
 */
void nm_mpi_info_free(struct nm_mpi_info_s*p_info);

/**
 * Gets the internal representation of the given info.
 */
struct nm_mpi_info_s*nm_mpi_info_get(MPI_Info info);

/** Define a value in the given infoset */
void nm_mpi_info_define(struct nm_mpi_info_s*p_info, const char*p_key, const char*p_value);

/**
 * Duplicates the internal representation of the given info.
 */
struct nm_mpi_info_s*nm_mpi_info_dup(struct nm_mpi_info_s*p_info);

/** Copy content o p_src_info into p_dest_info */
void nm_mpi_info_copy(struct nm_mpi_info_s*p_dest_info, struct nm_mpi_info_s*p_src_info);

/**
 * Update or add new entry in p_info_origin based on the entry from
 * p_info_up. This function only update already existing entries. It
 * does NOT create any new entry.
 */
void nm_mpi_info_update(struct nm_mpi_info_s*p_info_up, struct nm_mpi_info_s*p_info_origin);


/* ** Datatype functionalities ***************************** */

/**
 * Gets the size of the given datatype.
 */
size_t nm_mpi_datatype_size(nm_mpi_datatype_t*p_datatype);


/** get how many base elements from datatype fit into given size */
nm_mpi_count_t nm_mpi_datatype_get_elements(nm_mpi_datatype_t*p_datatype, nm_len_t size);

/**
 * Gets the internal representation of the given datatype.
 */
nm_mpi_datatype_t*nm_mpi_datatype_get(MPI_Datatype datatype);

/** Computes properties for datatype (LB, UB, extent, contig, etc.)
 * Called automatically upon commit or deserialize.
 */
void nm_mpi_datatype_properties_compute(nm_mpi_datatype_t*p_datatype);

void nm_mpi_datatype_traversal_apply(const void*_content, nm_data_apply_t apply, void*_context);

/**
 * Gets the internal representation of the given datatype from its
 * hash.
 */
nm_mpi_datatype_t*nm_mpi_datatype_hashtable_get(nm_mpi_datatype_hash_t datatype_hash);

/** increment refcount on given datatype */
#define nm_mpi_datatype_ref_inc(P_DATATYPE, P_HOLDER) nm_mpi_refcount_inc(&(P_DATATYPE)->refcount, (P_HOLDER))


/** decrements refcount on given datatype, free datatype if refcount reaches 0. */
int nm_mpi_datatype_ref_dec(nm_mpi_datatype_t*p_datatype, const void*p_holder);

void nm_mpi_datatype_pack(void*dest_ptr, const void*src_ptr, nm_mpi_datatype_t*p_datatype, nm_mpi_count_t count);

void nm_mpi_datatype_unpack(const void*src_ptr, void*dest_ptr, nm_mpi_datatype_t*p_datatype, nm_mpi_count_t count);

void nm_mpi_datatype_copy(const void*src_buf, nm_mpi_datatype_t*p_src_type, nm_mpi_count_t src_count,
                          void*dest_buf, nm_mpi_datatype_t*p_dest_type, nm_mpi_count_t dest_count);

/** get a pointer on the count'th data buffer */
static inline void*nm_mpi_datatype_get_ptr(void*buf, nm_mpi_count_t count, const nm_mpi_datatype_t*p_datatype)
{
  return buf + count * p_datatype->extent;
}

/** build a data descriptor for mpi data */
static inline void nm_mpi_data_build(struct nm_data_s*p_data, void*ptr, struct nm_mpi_datatype_s*p_datatype, nm_mpi_count_t count)
{
  nm_data_mpi_datatype_set(p_data, (struct nm_data_mpi_datatype_s){ .ptr = ptr, .p_datatype = p_datatype, .count = count });
}

/**
 * Deserialize a new datatype from the informations given in
 * p_datatype.
 */
void nm_mpi_datatype_deserialize(nm_mpi_datatype_ser_t*p_datatype, MPI_Datatype*p_newtype);

/**
 * Send a datatype to a distant node identified by its gate. This
 * function blocks until it recieve an acknowledgement from the
 * distant node that the type can now be used.
 */
int nm_mpi_datatype_send(nm_gate_t gate, nm_mpi_datatype_t*p_datatype);

void nm_mpi_datatype_hashtable_insert(nm_mpi_datatype_t*p_datatype);

static inline nm_mpi_datatype_hash_t nm_mpi_datatype_hash_common(const nm_mpi_datatype_t*p_datatype)
{
  nm_mpi_datatype_hash_t z = puk_hash_oneatatime((const void*)&p_datatype->combiner, sizeof(nm_mpi_type_combiner_t))
    + puk_hash_oneatatime((const void*)&p_datatype->count, sizeof(MPI_Count))
    + puk_hash_oneatatime((const void*)&p_datatype->lb, sizeof(MPI_Aint))
    + puk_hash_oneatatime((const void*)&p_datatype->extent, sizeof(MPI_Aint))
    + puk_hash_oneatatime((const void*)&p_datatype->size, sizeof(size_t));
  return z;
}

/* ** Requests functions *********************************** */

nm_mpi_request_t*nm_mpi_request_alloc(void);

nm_mpi_request_t*nm_mpi_request_alloc_send(nm_mpi_request_type_t type, nm_mpi_count_t count, const void*sbuf,
                                           struct nm_mpi_datatype_s*p_datatype, int tag, struct nm_mpi_communicator_s*p_comm);

nm_mpi_request_t*nm_mpi_request_alloc_recv(nm_mpi_count_t count, void*rbuf,
                                           struct nm_mpi_datatype_s*p_datatype, int tag, struct nm_mpi_communicator_s*p_comm);

nm_mpi_request_t*nm_mpi_request_alloc_icol(nm_mpi_request_type_t type, nm_mpi_count_t count, struct nm_mpi_datatype_s*p_datatype,
                                           struct nm_mpi_communicator_s*p_comm);

void nm_mpi_request_free(nm_mpi_request_t*req);

nm_mpi_request_t*nm_mpi_request_get(MPI_Request req_id);

int nm_mpi_request_test(nm_mpi_request_t*p_req);

int nm_mpi_request_wait(nm_mpi_request_t*p_req);

void nm_mpi_request_complete(nm_mpi_request_t*p_req, MPI_Request*request);

/** attach data to a request */
static inline void nm_mpi_request_set_datatype(nm_mpi_request_t*p_req, struct nm_mpi_datatype_s*p_datatype)
{
  p_req->p_datatype = p_datatype;
  if(p_datatype != NULL)
    nm_mpi_datatype_ref_inc(p_datatype, p_req);
}

/** attach a second datatype to this request */
static inline void nm_mpi_request_add_datatype2(nm_mpi_request_t*p_req, struct nm_mpi_datatype_s*p_datatype2)
{
  if(p_datatype2 != NULL)
    {
      assert(p_req != NULL);
      assert(p_req->p_datatype2 == NULL);
      p_req->p_datatype2 = p_datatype2;
      nm_mpi_datatype_ref_inc(p_datatype2, p_req);
    }
}

void nm_mpi_file_request_status_update(nm_mpi_request_t*p_req, struct nm_mpi_status_s*p_status);

int nm_mpi_file_request_test(nm_mpi_request_t*p_req);

int nm_mpi_file_request_wait(nm_mpi_request_t*p_req);


/* ** Send/recv/status functions *************************** */

/**
 * Initialises a sending request.
 */
int nm_mpi_isend_init(nm_mpi_request_t *p_req, int dest, nm_mpi_communicator_t *p_comm);

/**
 * Starts a sending request.
 */
int nm_mpi_isend_start(nm_mpi_request_t *p_req);

/**
 * Sends data.
 */
int nm_mpi_isend(nm_mpi_request_t *p_req, int dest, nm_mpi_communicator_t *p_comm);

/**
 * Initialises a receiving request.
 */
int nm_mpi_irecv_init(nm_mpi_request_t *p_req, int source, nm_mpi_communicator_t *p_comm);

/**
 * Starts a receiving request.
 */
int nm_mpi_irecv_start(nm_mpi_request_t *p_req);

/**
 * Receives data.
 */
int nm_mpi_irecv(nm_mpi_request_t *p_req, int source, nm_mpi_communicator_t *p_comm);


/* ** Collectives ****************************************** */

/**
 * Collective send over the communicator
 */
nm_mpi_request_t*nm_mpi_coll_isend(const void*buffer, nm_mpi_count_t count, nm_mpi_datatype_t*p_datatype, int dest, nm_tag_t tag, nm_mpi_communicator_t*p_comm);

/**
 * Receive data sent over the communicator via nm_mpi_coll_isend
 */
nm_mpi_request_t*nm_mpi_coll_irecv(void*buffer, nm_mpi_count_t count, nm_mpi_datatype_t*p_datatype, int source, nm_tag_t tag, nm_mpi_communicator_t*p_comm);

/**
 * Wait for the end of the p2p communication over communicator via nm_mpi_coll_isend
 */
void nm_mpi_coll_wait(nm_mpi_request_t*p_req);

/** internal implementation of collective Ireduce */
struct nm_coll_req_s*nm_mpi_coll_ireduce_init(nm_mpi_communicator_t*p_comm, int root,
                                              nm_mpi_count_t count, nm_mpi_datatype_t*p_datatype,
                                              const void*sendbuf, void*recvbuf,
                                              nm_tag_t tag, nm_mpi_operator_t*p_operator,
                                              nm_coll_req_notifier_t p_notify, void*ref);
/** start an already initialized ireduce
 * @note separate init/start so that even in case of immediate completion,
 * p_reduce will not be uninitialized in iallreduce */
void nm_mpi_coll_ireduce_start(struct nm_coll_req_s*p_coll_req);

/** blocking reduce */
int nm_mpi_coll_reduce(nm_mpi_communicator_t*p_comm, int root,
                       nm_mpi_count_t count, nm_mpi_datatype_t*p_datatype,
                       const void*sendbuf, void*recvbuf,
                       nm_tag_t tag, nm_mpi_operator_t*p_operator);

/* ** iallreduce */

struct nm_coll_req_s*nm_mpi_coll_iallreduce(nm_mpi_communicator_t*p_comm,
                                            nm_mpi_count_t count, nm_mpi_datatype_t*p_datatype,
                                            const void*sendbuf, void*recvbuf,
                                            nm_tag_t tag, nm_mpi_operator_t*p_operator);

/* ** ialltoall */

struct nm_coll_req_s*nm_mpi_coll_ialltoall(nm_mpi_communicator_t*p_comm,
                                           const void*sendbuf, nm_mpi_count_t sendcount, nm_mpi_datatype_t*p_send_datatype,
                                           void*recvbuf, nm_mpi_count_t recvcount, nm_mpi_datatype_t*p_recv_datatype);

/* ** ialltoallv */

struct nm_coll_req_s*nm_mpi_coll_ialltoallv(nm_mpi_communicator_t*p_comm,
                                            const void*sendbuf, const nm_mpi_count_t*sendcounts, const nm_mpi_aint_t*sdispls, nm_mpi_datatype_t*p_send_datatype,
                                            void*recvbuf,  const nm_mpi_count_t*recvcounts, const nm_mpi_aint_t*rdispls, nm_mpi_datatype_t*p_recv_datatype);

/* ** iallgather */

struct nm_coll_req_s*nm_mpi_coll_iallgather(nm_mpi_communicator_t*p_comm,
                                            const void*sendbuf, nm_mpi_count_t sendcount, nm_mpi_datatype_t*p_send_datatype,
                                            void*recvbuf, nm_mpi_count_t recvcount, nm_mpi_datatype_t*p_recv_datatype);


/* ** Operators ******************************************** */

/**
 * Gets the function associated to the given operator.
 */
nm_mpi_operator_t*nm_mpi_operator_get(MPI_Op op);

/**
 * Apply the operator to every chunk of data described by p_datatype
 * and pointed by outvec, with the result stored in outvec. The invec
 * parameter is an address of a pointer to a contiguous buffer of
 * predefined types, corresponding to each element pointed by outvec.
 */
void nm_mpi_operator_apply(void*invec, void*outvec, nm_mpi_count_t count, nm_mpi_datatype_t*p_datatype,
                           nm_mpi_operator_t*p_operator);

/* ** Communicator operations ****************************** */

nm_mpi_communicator_t*nm_mpi_communicator_alloc(nm_comm_t p_nm_comm, struct nm_mpi_errhandler_s*p_errhandler,
                                                enum nm_mpi_communicator_kind_e kind);

/**
 * Gets the internal representation of the given communicator.
 */
nm_mpi_communicator_t*nm_mpi_communicator_get(MPI_Comm comm);

/** increment refcount on given communicator */
#define nm_mpi_communicator_ref_inc(P_COMM, P_HOLDER) nm_mpi_refcount_inc(&(P_COMM)->refcount, (P_HOLDER));

void nm_mpi_communicator_ref_dec(nm_mpi_communicator_t*p_comm, const void*p_holder);


/* ** Attributes ******************************************* */

struct nm_mpi_keyval_s*nm_mpi_keyval_new(void);

void nm_mpi_keyval_delete(struct nm_mpi_keyval_s*p_keyval);

struct nm_mpi_keyval_s*nm_mpi_keyval_get(int id);

void nm_mpi_attrs_create(struct nm_mpi_attrs_s*p_attrs, int id);

int nm_mpi_attrs_copy(struct nm_mpi_attrs_s*p_old_attrs, struct nm_mpi_attrs_s*p_new_attrs);

void nm_mpi_attrs_destroy(struct nm_mpi_attrs_s*p_old_attrs);

int nm_mpi_attr_put(struct nm_mpi_attrs_s*p_attrs, struct nm_mpi_keyval_s*p_keyval, void*attr_value);

void nm_mpi_attr_get(struct nm_mpi_attrs_s*p_attrs, struct nm_mpi_keyval_s*p_keyval, void**p_attr_value, int*flag);

int nm_mpi_attr_delete(struct nm_mpi_attrs_s*p_attrs, struct nm_mpi_keyval_s*p_keyval);


/* ** Window operations *************************************/

/**
 * Gets the internal representation of the given window.
 */
nm_mpi_window_t*nm_mpi_window_get(MPI_Win win);

/**
 * Returns the next sequence number for the given window, and
 * increments it, lock free.
 */
static inline uint16_t nm_mpi_win_get_next_seq(nm_mpi_window_t*p_win)
{
  return __sync_fetch_and_add(&p_win->next_seq, 1);
}

/**
 * Returns whether the epoch requests are all completed
 */
static inline int nm_mpi_win_completed_epoch(nm_mpi_win_epoch_t*p_epoch)
{
  int ret = __sync_bool_compare_and_swap(&p_epoch->nmsg, p_epoch->completed, p_epoch->nmsg);
  return ret;
}

/**
 * Check whether the epoch (exposure or acces, passive or active) is
 * opened.
 */
static inline int nm_mpi_win_is_ready(nm_mpi_win_epoch_t*p_epoch)
{
  int ret = NM_MPI_WIN_UNUSED != p_epoch->mode;
  return ret;
}

/**
 * Check whether or not an assert if only a combination of defined
 * symbols.
 */
static inline int nm_mpi_win_valid_assert(int assert)
{
  int ret = assert >= 0 && assert <= (MPI_MODE_NOCHECK | MPI_MODE_NOSTORE | MPI_MODE_NOPUT
                                      | MPI_MODE_NOPRECEDE | MPI_MODE_NOSUCCEED);
  return ret;
}

/**
 * Check whether base is part of a memory segment attached to p_win,
 * and if the data fit, based on its extent.
 */
int nm_mpi_win_addr_is_valid(void*base, nm_mpi_aint_t extent, nm_mpi_window_t*p_win);

/**
 * Looks up in the hashtable if the datatype is already exchanged with
 * the target node. If it is, it returns immediately. Otherwhise, it
 * exchanges the datatype information with the target, and set the
 * proper entry in the hashtable.
 */
int nm_mpi_win_send_datatype(nm_mpi_datatype_t*p_datatype, int target_rank, nm_mpi_window_t*p_win);


/* ** RMA operations *************************************** */

/**
 * Decode the win_id from the tag.
 */
static inline int nm_mpi_rma_tag_to_win(nm_tag_t tag)
{
  return (tag & NM_MPI_TAG_PRIVATE_RMA_MASK_WIN) >> 32;
}

/**
 * Encode a window id inside the tag to handle the multiple windows.
 */
static inline nm_tag_t nm_mpi_rma_win_to_tag(int win_id)
{
  return ((nm_tag_t)win_id) << 32;
}

/**
 * Decode the sequence number from the tag.
 */
static inline uint16_t nm_mpi_rma_tag_to_seq(nm_tag_t tag)
{
  return (tag & NM_MPI_TAG_PRIVATE_RMA_MASK_SEQ) >> 8;
}

/**
 * Encode a sequence number inside the tag to handle THREAD_MULTIPLE.
 */
static inline nm_tag_t nm_mpi_rma_seq_to_tag(uint16_t seq)
{
  return (nm_tag_t)((((nm_tag_t)seq) << 8) & 0xFFFF00);
}

/**
 * Decode the operation id from the tag.
 */
static inline MPI_Op nm_mpi_rma_tag_to_mpi_op(nm_tag_t tag)
{
  return (tag & 0xF0) >> 4;
}

/**
 * Encode an operation id inside the tag for MPI_Accumulate and
 * MPI_Get_accumulate functions.
 */
static inline nm_tag_t nm_mpi_rma_mpi_op_to_tag(MPI_Op op)
{
  return (nm_tag_t)((((nm_tag_t)op) << 4) & 0xF0);
}

/**
 * Create the tag for private rma operations, without the window encoded
 * id.
 */
static inline nm_tag_t nm_mpi_rma_create_usertag(uint16_t seq, int user_tag)
{
  return nm_mpi_rma_seq_to_tag(seq) + (((nm_tag_t)user_tag) & 0xFC0000FF);
}

/**
 * Create the tag for private rma operations, with the first 32 bits used for window
 * id.
 */
static inline nm_tag_t nm_mpi_rma_create_tag(int win_id,  uint16_t seq_num, int user_tag)
{
  return nm_mpi_rma_win_to_tag(win_id) + nm_mpi_rma_create_usertag(seq_num, user_tag);
}

/* ** Keyval operations *************************************** */

int nm_mpi_comm_create_keyval_fort(nm_mpi_copy_subroutine_t*comm_copy_attr_fn,
                                   nm_mpi_delete_subroutine_t*comm_delete_attr_fn,
                                   int*comm_keyval, void*extra_state);

int nm_mpi_type_create_keyval_fort(nm_mpi_copy_subroutine_t*type_copy_attr_fn,
                                   nm_mpi_delete_subroutine_t*type_delete_attr_fn,
                                   int*type_keyval, void*extra_state);

int nm_mpi_win_create_keyval_fort(nm_mpi_copy_subroutine_t*win_copy_attr_fn,
                                  nm_mpi_delete_subroutine_t*win_delete_attr_fn,
                                  int*win_keyval, void*extra_state);

/* ** Threading ******************************************** */

int nm_mpi_thread_level_get(int required);


/* ** Large counts ***************************************** */

/** convert an array of int to an array of counts */
static inline MPI_Count*nm_mpi_array_count_from_int(nm_mpi_count_t n, const int*array_int)
{
  if(array_int == NULL)
    return NULL;
  MPI_Count*array_count = padico_malloc(sizeof(MPI_Count) * n);
  nm_mpi_count_t i;
  for(i = 0; i < n; i++)
    {
      array_count[i] = array_int[i];
    }
  return array_count;
}

/** convert an array of int to an array of aint */
static inline MPI_Aint*nm_mpi_array_aint_from_int(nm_mpi_count_t n, const int*array_int)
{
  if(array_int == NULL)
    return NULL;
  MPI_Aint*array_aint = padico_malloc(sizeof(MPI_Aint) * n);
  nm_mpi_count_t i;
  for(i = 0; i < n; i++)
    {
      array_aint[i] = array_int[i];
    }
  return array_aint;
}

/** convert an array of MPI_Aint to an array of counts */
static inline MPI_Count*nm_mpi_array_count_from_aint(nm_mpi_count_t n, const nm_mpi_aint_t*array_aint)
{
  if(array_aint == NULL)
    return NULL;
  MPI_Count*array_count = padico_malloc(sizeof(MPI_Count) * n);
  nm_mpi_count_t i;
  for(i = 0; i < n; i++)
    {
      array_count[i] = array_aint[i];
    }
  return array_count;
}

static inline void nm_mpi_array_count_free(MPI_Count*array_count)
{
  if(array_count != NULL)
    padico_free(array_count);
}

static inline void nm_mpi_array_aint_free(MPI_Aint*array_aint)
{
  if(array_aint != NULL)
    padico_free(array_aint);
}


/** @} */

#endif /* NM_MPI_PRIVATE_H */

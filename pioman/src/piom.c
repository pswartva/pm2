/*
 * Pioman: a Generic I/O Manager
 * Copyright (C) 2001-2023 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include "piom_private.h"

#include <errno.h>
#include <sys/stat.h>

#include <Padico/Module.h>

PADICO_MODULE_BUILTIN(pioman, NULL, NULL, NULL);

__PUK_SYM_INTERNAL struct piom_parameters_s piom_parameters =
  {
    .busy_wait_usec     = 10,
    .busy_wait_granularity = 100,
    .enable_progression = 1,
    .binding_level      = PIOM_TOPO_SOCKET,
    .idle_granularity   = 5,
    .idle_coef          = 10,
    .idle_distrib       = PIOM_BIND_DISTRIB_ALL,
    .timer_period       = 4000,
    .dedicated          = 0,
    .dedicated_distrib  = PIOM_BIND_DISTRIB_LAST,
    .dedicated_level    = PIOM_TOPO_SOCKET,
    .dedicated_wait     = 0,
    .spare_lwp          = 2,
    .mckernel           = 0
  };

static int piom_init_done = 0;


void piom_polling_force(void)
{
  piom_ltask_schedule(PIOM_POLL_POINT_FORCED);
}

void pioman_init(void)
{
  piom_init_done++;
  if(piom_init_done > 1)
    return;
  if(puk_opt_parse_bool(getenv("PIOM_VERBOSE")))
    {
      puk_trace_add_filter(PUK_TRACE_ACTION_SHOW, -1, "pioman", NULL, NULL, -1);
      PIOM_DISP("verbose mode.\n");
    }

  struct stat proc_mckernel;
  int rc = stat("/proc/mckernel", &proc_mckernel);
  if(rc == 0)
    {
      piom_parameters.mckernel = 1;
      PIOM_DISP("running on mckernel\n");
      piom_parameters.enable_progression = 0;
      PIOM_WARN("progression disabled by default on mckernel. Enable it with PIOM_ENABLE_PROGRESSION=1\n");
    }

  const char*s_busy_wait_usec        = getenv("PIOM_BUSY_WAIT_USEC");
  const char*s_busy_wait_granularity = getenv("PIOM_BUSY_WAIT_GRANULARITY");
  const char*s_enable_progression    = getenv("PIOM_ENABLE_PROGRESSION");
  const char*s_binding_level         = getenv("PIOM_BINDING_LEVEL");
  const char*s_idle_granularity      = getenv("PIOM_IDLE_GRANULARITY");
  const char*s_idle_coef             = getenv("PIOM_IDLE_COEF");
  const char*s_idle_distrib          = getenv("PIOM_IDLE_DISTRIB");
  const char*s_timer_period          = getenv("PIOM_TIMER_PERIOD");
  const char*s_dedicated             = getenv("PIOM_DEDICATED");
  const char*s_dedicated_distrib     = getenv("PIOM_DEDICATED_DISTRIB");
  const char*s_dedicated_level       = getenv("PIOM_DEDICATED_LEVEL");
  const char*s_dedicated_wait        = getenv("PIOM_DEDICATED_WAIT");
  const char*s_spare_lwp             = getenv("PIOM_SPARE_LWP");
  if(s_busy_wait_usec)
    {
      piom_parameters.busy_wait_usec = atoi(s_busy_wait_usec);
      PIOM_DISP("custom PIOM_BUSY_WAIT_USEC = %d\n", piom_parameters.busy_wait_usec);
    }
  if(s_busy_wait_granularity)
    {
      piom_parameters.busy_wait_granularity = atoi(s_busy_wait_granularity);
      PIOM_DISP("custom PIOM_BUSY_WAIT_GRANULARITY = %d\n", piom_parameters.busy_wait_granularity);
    }
  if(s_enable_progression)
    {
      piom_parameters.enable_progression = atoi(s_enable_progression);
      PIOM_DISP("custom PIOM_ENABLE_PROGRESSION = %d\n", piom_parameters.enable_progression);
    }
  if(s_idle_granularity)
    {
      piom_parameters.idle_granularity = atoi(s_idle_granularity);
      PIOM_DISP("custom PIOM_IDLE_GRANULARITY = %d\n", piom_parameters.idle_granularity);
    }
  if(s_idle_coef)
    {
      piom_parameters.idle_coef = atoi(s_idle_coef);
      PIOM_DISP("custom PIOM_IDLE_COEF = %d\n", piom_parameters.idle_coef);
    }
  if(s_binding_level)
    {
      enum piom_topo_level_e level =
        (strcmp(s_binding_level, "machine") == 0) ? PIOM_TOPO_MACHINE :
        (strcmp(s_binding_level, "node")    == 0) ? PIOM_TOPO_NODE :
        (strcmp(s_binding_level, "socket")  == 0) ? PIOM_TOPO_SOCKET :
        (strcmp(s_binding_level, "core")    == 0) ? PIOM_TOPO_CORE :
        (strcmp(s_binding_level, "pu")      == 0) ? PIOM_TOPO_PU :
        PIOM_TOPO_NONE;
      if(level != PIOM_TOPO_NONE)
        {
          piom_parameters.binding_level = level;
          PIOM_DISP("custom PIOM_BINDING_LEVEL = %s (%d)\n", s_binding_level, piom_parameters.binding_level);
        }
    }
  if(s_idle_distrib)
    {
      enum piom_bind_distrib_e distrib =
        (strcmp(s_idle_distrib, "all")   == 0) ? PIOM_BIND_DISTRIB_ALL :
        (strcmp(s_idle_distrib, "odd")   == 0) ? PIOM_BIND_DISTRIB_ODD :
        (strcmp(s_idle_distrib, "even")  == 0) ? PIOM_BIND_DISTRIB_EVEN :
        (strcmp(s_idle_distrib, "first") == 0) ? PIOM_BIND_DISTRIB_FIRST :
        PIOM_BIND_DISTRIB_NONE;
      if(distrib != PIOM_BIND_DISTRIB_NONE)
        {
          piom_parameters.idle_distrib = distrib;
          PIOM_DISP("custom PIOM_IDLE_DISTRIB = %s\n", s_idle_distrib);
        }
    }
  if(s_timer_period)
    {
      piom_parameters.timer_period = atoi(s_timer_period);
      PIOM_DISP("custom PIOM_TIMER_PERIOD = %d\n", piom_parameters.timer_period);
    }
  if(s_dedicated)
    {
      piom_parameters.dedicated = atoi(s_dedicated);
      PIOM_DISP("custom PIOM_DEDICATED = %d\n", piom_parameters.dedicated);
    }
  if(s_dedicated_distrib)
    {
      enum piom_bind_distrib_e distrib =
        (strcmp(s_dedicated_distrib, "all")   == 0) ? PIOM_BIND_DISTRIB_ALL :
        (strcmp(s_dedicated_distrib, "odd")   == 0) ? PIOM_BIND_DISTRIB_ODD :
        (strcmp(s_dedicated_distrib, "even")  == 0) ? PIOM_BIND_DISTRIB_EVEN :
        (strcmp(s_dedicated_distrib, "first") == 0) ? PIOM_BIND_DISTRIB_FIRST :
        PIOM_BIND_DISTRIB_NONE;
      if(distrib != PIOM_BIND_DISTRIB_NONE)
        {
          piom_parameters.dedicated_distrib = distrib;
          PIOM_DISP("custom PIOM_DEDICATED_DISTRIB = %s\n", s_dedicated_distrib);
        }
    }
  if(s_dedicated_level)
    {
      enum piom_topo_level_e level =
        (strcmp(s_dedicated_level, "machine") == 0) ? PIOM_TOPO_MACHINE :
        (strcmp(s_dedicated_level, "node")    == 0) ? PIOM_TOPO_NODE :
        (strcmp(s_dedicated_level, "socket")  == 0) ? PIOM_TOPO_SOCKET :
        (strcmp(s_dedicated_level, "core")    == 0) ? PIOM_TOPO_CORE :
        (strcmp(s_dedicated_level, "pu")      == 0) ? PIOM_TOPO_PU :
        PIOM_TOPO_NONE;
      if(level != PIOM_TOPO_NONE)
        {
          piom_parameters.dedicated_level = level;
          PIOM_DISP("custom PIOM_DEDICATED_LEVEL = %s (%d)\n", s_dedicated_level, piom_parameters.dedicated_level);
        }
    }
  if(s_dedicated_wait)
    {
      piom_parameters.dedicated_wait = atoi(s_dedicated_wait);
      PIOM_DISP("custom PIOM_DEDICATED_WAIT = %d\n", piom_parameters.dedicated_wait);
    }
  if(piom_parameters.dedicated)
    {
      piom_parameters.timer_period = -1;
      piom_parameters.idle_granularity = -1;
      piom_parameters.busy_wait_usec = -1;
    }
  if(piom_parameters.enable_progression == 0)
    {
      piom_parameters.timer_period = -1;
      piom_parameters.idle_granularity = -1;
      piom_parameters.busy_wait_usec = -1;
      piom_parameters.dedicated = 0;
    }
  if(s_spare_lwp)
    {
      piom_parameters.spare_lwp = atoi(s_spare_lwp);
      PIOM_DISP("custom PIOM_SPARE_LWP = %d\n", piom_parameters.spare_lwp);
    }

#ifdef PIOMAN_MARCEL
  int fake_argc = 1;
  char*fake_argv[2] = { "pioman", NULL };
  marcel_init(&fake_argc, fake_argv);
#endif /* PIOMAN_MARCEL */
  piom_ltasks_init();
  /*    piom_io_task_init(); */
}

void pioman_exit(void)
{
  assert(piom_init_done);
  piom_init_done--;
  if(piom_init_done == 0)
    {
      /*
        piom_io_task_stop();
      */
      piom_ltasks_exit();
    }
}

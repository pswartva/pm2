/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Internal structures definition for VLink_selector.
 * @ingroup VIO
 */

#ifndef VLINK_SELECTOR_INTERNALS_H
#define VLINK_SELECTOR_INTERNALS_H

#include <Padico/Topology.h>
#include <Padico/VLink-API.h>


/* ********************************************************* */
/* *** VLink-resolver ************************************** */

__PUK_SYM_INTERNAL
void           vlink_registry_register(padico_vnode_t, const struct sockaddr*, socklen_t, int);
__PUK_SYM_INTERNAL
void           vlink_registry_unregister(padico_vnode_t);
__PUK_SYM_INTERNAL
padico_vnode_t vlink_registry_lookup(const struct sockaddr*, socklen_t, int);

__PUK_SYM_INTERNAL
padico_topo_node_t vlink_resolver_resolve_node(const struct padico_vaddr_s*vaddr);
__PUK_SYM_INTERNAL
padico_topo_host_t vlink_resolver_resolve_host(const struct padico_vaddr_s*vaddr);

__PUK_SYM_INTERNAL
void padico_vlink_resolver_init(void);


#endif /* VLINK_SLEECTOR_INTERNALS_H */

dnl -*- mode: Autoconf;-*-

# ## add a makefile to out list
AC_DEFUN([AC_PADICO_OUT_MK],
         [
           padico_out_mk="${padico_out_mk} $1"
         ])

# ## add common makefiles to out list
AC_DEFUN([AC_PADICO_OUT_COMMON_MK],
         [
           AC_REQUIRE([AC_PADICO_BASE_TOOLS])
           AC_REQUIRE([AC_PADICO_CDEP])
           AC_REQUIRE([AC_PADICO_LINKER])
           AC_REQUIRE([AC_PADICO_OPTIMIZE])
           AC_REQUIRE([AC_PADICO_DEBUG])
           AC_REQUIRE([AC_PADICO_BUILDING_TOOLS])
           AC_PADICO_OUT_MK([common_vars.mk:${building_tools}/common_vars.mk.in])
           AC_PADICO_OUT_MK([common_rules.mk:${building_tools}/common_rules.mk.in])
          ])

# ## add root Makefile to out list
AC_DEFUN([AC_PADICO_OUT_ROOT],
         [
           AC_PADICO_OUT_MK([Makefile])
           if test -r ${srcdir}/doc/Doxyfile.in; then
             AC_PADICO_OUT_MK([doc/Doxyfile])
           fi
           if test -r ${srcdir}/${package_short_name}.pc.in; then
             AC_PADICO_OUT_MK([${package_short_name}.pc])
           fi
         ])

# ## add a shell script to out list
AC_DEFUN([AC_PADICO_OUT_SH],
         [
           PADICO_OUT_FILES="${PADICO_OUT_FILES} $1"
           AC_CONFIG_FILES( [ $1 ] , [ chmod +x $1 ])
         ])

# ## issue output commands from out_mk lists
AC_DEFUN([AC_PADICO_OUTPUT],
         [
           AC_CONFIG_FILES([ ${padico_out_mk} ])
           for f in ${padico_out_mk}; do
             case ${f} in
               *:*)
                 src=`echo ${f} | cut -f 1 -d ':'`
               ;;
               *)
                 src=${f}
               ;;
             esac
             PADICO_OUT_FILES="${PADICO_OUT_FILES} ${src} ${ac_config_headers}"
           done
           AC_SUBST([PADICO_OUT_FILES])
           AC_OUTPUT
         ])

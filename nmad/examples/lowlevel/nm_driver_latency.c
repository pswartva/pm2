/*
 * NewMadeleine
 * Copyright (C) 2011-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <sys/uio.h>
#include <assert.h>
#include <values.h>

#include <nm_private.h>
#include <nm_launcher_interface.h>
#include <nm_minidriver_interface.h>

const int roundtrips = 100000;

int main(int argc, char **argv)
{
  /* launch nmad session, get gate and driver */
  nm_launcher_init(&argc, argv);
  int rank = -1;
  nm_launcher_get_rank(&rank);
  const int is_server = !rank;
  const int peer = 1 - rank;
  nm_gate_t p_gate = NULL;
  nm_launcher_get_gate(peer, &p_gate);
  assert(p_gate != NULL);
  assert(p_gate->status == NM_GATE_STATUS_CONNECTED);

  /* take over the driver */
  nm_core_schedopt_disable(p_gate->p_core);

  /* hack here-
   * make sure the peer node has flushed its pending recv requests,
   * so that the pw we send are not processed by schedopt.
   */
  puk_usleep(500 * 1000);

  /* benchmark */
  struct nm_trk_s*p_trk_small = nm_trk_get_by_index(p_gate, NM_TRK_SMALL);
  struct puk_receptacle_NewMad_minidriver_s*r = &p_trk_small->receptacle;
  const struct nm_minidriver_properties_s*p_props = &p_trk_small->p_drv->props;
  char value = 42;
  void*buffer = &value;
  const nm_len_t len = sizeof(value);

  if(is_server)
    {
      double min = DBL_MAX;
      int i;
      for(i = 0; i < roundtrips; i++)
        {
          puk_tick_t t1, t2;
          PUK_GET_TICK(t1);
          nm_minidriver_send(r, p_props, buffer, len);
          nm_minidriver_recv(r, p_props, buffer, len);
          PUK_GET_TICK(t2);
          const double delay = PUK_TIMING_DELAY(t1, t2);
          const double t = delay / 2;
          if(t < min)
            min = t;
        }
      printf("driver latency:   %9.3f usec.\n", min);
    }
  else
    {
      int i;
      for(i = 0; i < roundtrips; i++)
        {
          nm_minidriver_recv(r, p_props, buffer, len);
          nm_minidriver_send(r, p_props, buffer, len);
        }
    }

  return 0;
}

/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file psp-ibv.c
 *  @brief PSP driver for Infiniband Verbs
 *
 *  @note This file contains excerpts from OpenIB.org examples
 *        released under the the terms of the GNU General Public
 *        License (GPL) Version 2.
 *        Copyright (c) 2005 Topspin Communications.
 */

#include <Padico/Puk.h>
#include <Padico/Module.h>
#include <Padico/PadicoTM.h>
#include <Padico/Topology.h>
#include <Padico/PSP.h>
#include <Padico/PadicoControl.h>
#include <Padico/Timing.h>

#include "InfinibandVerbs.h"

#include <infiniband/verbs.h>

#include <string.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/uio.h>
#include <sys/resource.h>
#include <time.h>
#include <stdarg.h>
#include <errno.h>
#include <unistd.h>

#if defined(MARCEL)
#define PSP_IBV_PIOMAN
#endif

#ifdef PSP_IBV_PIOMAN
#include <pioman.h>
#endif /*  PSP_IBV_PIOMAN */

static int padico_ibv_init(void);
static void padico_ibv_finalize(void);

PADICO_MODULE_DECLARE(PSP_InfinibandVerbs, padico_ibv_init, NULL, padico_ibv_finalize,
                      "PadicoControl", "PSP", "InfinibandVerbs");


/* Infiniband Verbs
 * pd:   protection domain
 * mr:   memory region
 * cq:   completion queue
 * qp:   queue pair
 * wr:   work request
 * wc:   work completion
 * sq:   send queue
 * rq:   receive queue
 * srq:  shared rq (pas support� par Mellanox InfiniHost MT23108)
 * zbva: zero based virtual addressing (pas support� par Mellanox InfiniHost MT23108)
 * imm:  immediate data
 */


/** @defgroup PSP-IBV component: PSP-IBV- Direct messaging over Infiniband verbs
 * @ingroup PadicoComponent */

/* ********************************************************* */
/* *** PadicoSimplePackets for Infiniband Verbs */

static void*psp_ibv_instantiate(puk_instance_t ai, puk_context_t context);
static void psp_ibv_destroy(void*_instance);
static void psp_ibv_init(void*_instance, uint32_t tag, const char*label, size_t*h_size,
                         padico_psp_handler_t handler, void*key);
static void psp_ibv_connect(void*_instance, padico_topo_node_t remote_node);
static padico_psp_connection_t psp_ibv_new_message(void*_instance, padico_topo_node_t node, void**_sbuf);
static void psp_ibv_pack(void*_instance, padico_psp_connection_t _conn,
                         const char*bytes, size_t size);
static void psp_ibv_end_message(void*_instance, padico_psp_connection_t _conn);

/** instanciation facet for PSP-IBV
 * @ingroup PSP-IBV
 */
static const struct puk_component_driver_s psp_ibv_component_driver =
  {
    .instantiate = &psp_ibv_instantiate,
    .destroy     = &psp_ibv_destroy
  };

/** 'SimplePackets' facet for PSP-IBV
* @ingroup PSP-IBV
*/
static const struct padico_psp_driver_s psp_ibv_driver =
  {
    .init        = &psp_ibv_init,
    .connect     = &psp_ibv_connect,
    .listen      = NULL,
    .new_message = &psp_ibv_new_message,
    .pack        = &psp_ibv_pack,
    .end_message = &psp_ibv_end_message
  };

/* ********************************************************* */


enum {
  PADICO_WRID_RECV = 1,
  PADICO_WRID_SEND = 2,
  PADICO_WRID_SP_CHUNK = 3,
  PADICO_WRID_SP_ACK = 4,
  PADICO_WRID_NUM = 5
};

struct padico_ibv_addr_s
{
  uint16_t lid;
  uint32_t qpn;
  uint32_t psn;
  uint64_t raddr;
  uint32_t rkey;
};

#define PADICO_IBV_TX_DEPTH   20 /* 2  */
#define PADICO_IBV_RX_DEPTH   2  /* 2  */
#define PADICO_IBV_RDMA_DEPTH 2  /* 8  */
#define PADICO_IBV_MAX_SG_SQ  2  /* 16 */
#define PADICO_IBV_MAX_SG_RQ  2
#define PADICO_IBV_MAX_INLINE 512 /* 512 */

#define PADICO_IBV_LZ_THRESHOLD     (32*1024)  /* 64 kB */
#define PADICO_IBV_LZ_BUFSIZE       (16*1024)  /* 32 kB */
#define PADICO_IBV_LZ_BUFNUM            48
#define PADICO_IBV_LZ_CREDITS_THRESHOLD 40
#define PADICO_IBV_LZ_CREDITS_GUARD     4
#define PADICO_IBV_LZ_STATUS_EMPTY   0x00 /**< no message in LZ */
#define PADICO_IBV_LZ_STATUS_DATA    0x01 /**< data in LZ (sent by copy) */
#define PADICO_IBV_LZ_STATUS_BYRDV   0x02 /**< data sent as attachment through rdma  */
#define PADICO_IBV_LZ_STATUS_RVREQ   0x04 /**< message is a r.-v. request */
#define PADICO_IBV_LZ_STATUS_CREDITS 0x08 /**< message contains credits */

#define PADICO_IBV_LR2_BUFSIZE (512*1024)
#define PADICO_IBV_LR2_NBUF 3
#define PADICO_IBV_LR2_BLOCKSIZE 4096
static const int lr2_steps[] =
  { 12*1024, 24*1024, 40*1024, 64*1024, 88*1024, 128*1024, /* 160*1024, 200*1024, 256*1024, */ 0};
static const int lr2_nsteps = sizeof(lr2_steps) / sizeof(int) - 1;

struct lr2_header_s
{
  volatile uint8_t busy; /* 'busy' has to be the last field in the struct */
} __attribute__((packed));

static const int lr2_hsize = sizeof(struct lr2_header_s);

struct padico_ibv_lz_cell_s
{
  char buffer[PADICO_IBV_LZ_BUFSIZE];
  struct padico_ibv_lz_header_s
  {
    volatile uint32_t offset;
    volatile uint32_t size;
    volatile uint8_t  ack;
    volatile uint8_t status;
  } header ;
} __attribute__((packed,aligned(__alignof__(unsigned int))));

PUK_LIST_TYPE(padico_ibv_connection,
              struct ibv_qp*qp;
              struct ibv_cq*of_cq;
              struct ibv_cq*if_cq;
              struct padico_ibv_addr_s local_addr;
              padico_topo_node_t remote_node;
              uint32_t max_inline; /**< max size of data for IBV inline */
              /* lz I/O */
              struct
              {
                uint64_t raddr;    /**< base absolute address of the remote LZ */
                uint32_t rkey;     /**< rkey of the remote MR */
                uint32_t next_out; /**< next sequence number for outgoing packet */
                uint32_t credits;  /**< remaining credits for sending */
                uint32_t next_in;  /**< cell number of next expected packet */
                uint32_t to_ack;   /**< credits not acked yet by the receiver */
                int pending[PADICO_WRID_NUM];
                struct
                {
                  const char*message;
                  int size;
                  int done;
                  void*rbuf;
                  void*sbuf;
                  int step;
                  int nbuffer;
                } send;
                struct
                {
                  char*message;
                  int size;
                  void*rbuf;
                  int done;
                  int step;
                  int nbuffer;
                } recv;

                struct             /**  zones registered as MR */
                {
                  struct padico_ibv_lz_cell_s cells[PADICO_IBV_LZ_BUFNUM];
                  struct padico_ibv_lz_cell_s out_cell[2];
                  struct padico_ibv_lz_header_s rv_buf; /**< prepare outgoing r.-v. (for incoming messages) */
                  struct padico_ibv_lz_header_s rv_box; /**< incoming r.-v. (for outgoing messages) */
                  char sbuf[PADICO_IBV_LR2_BUFSIZE * PADICO_IBV_LR2_NBUF];
                  char rbuf[PADICO_IBV_LR2_BUFSIZE * PADICO_IBV_LR2_NBUF];
                  volatile uint32_t rack;
                  volatile uint32_t sack;
                } zones;
                struct ibv_mr*mr;
              } lz;
              );

static struct padico_ibv_s
{
  struct padico_ibv_port_s*psp_ibv_port;     /**< IB port used by PSP */
  /* directory */
  struct padico_ibv_connection_list_s conns; /**< list of ibv connections */
  puk_hashtable_t connections;               /**< index for the above list [hash: node -> ibv_connection] */
  struct padico_psp_directory_s slots;       /**< PSP demultiplexing */
  struct ibv_pd*pd;                          /**< Protection domain used for PSP */
  /* Marcel & polling */
  padico_tm_bgthread_pool_t pool;
  marcel_mutex_t lock;
  marcel_t worker;
  int power_on;
#ifdef PSP_IBV_PIOMAN
  struct piom_ltask ltask;
#endif
} padico_ibv;

/* some forward declaration */
static inline int padico_ibv_poll_lz_all(void);
static inline void padico_ibv_wait_any(void);
static inline void padico_ibv_wait_one(struct padico_ibv_connection_s*conn);



/* ********************************************************* */

static inline int padico_ibv_min(const int a, const int b)
{
  if(b > a)
    return a;
  else
    return b;
}

static inline void padico_assert(int cond, const char*msg, ...)
{
#ifndef NDEBUG
  if(!cond)
    {
      va_list ap;
      va_start(ap, msg);
      vfprintf(stderr, msg, ap);
      abort();
      va_end(ap);
    }
#endif /* NDEBUG */
}

static inline void padico_ibv_lock(void)
{
#ifdef PSP_IBV_PIOMAN
  piom_ltask_mask(&padico_ibv.ltask);
#endif
  marcel_mutex_lock(&padico_ibv.lock);
}

static inline void padico_ibv_unlock(void)
{
  marcel_mutex_unlock(&padico_ibv.lock);
#ifdef PSP_IBV_PIOMAN
  piom_ltask_unmask(&padico_ibv.ltask);
#endif
}


/* ********************************************************* */

/** Create a (not-connected-yet) connection structure
 */
static struct padico_ibv_connection_s*padico_ibv_connection_init(padico_topo_node_t remote_node)
{
  struct padico_ibv_connection_s*conn = padico_malloc(sizeof(struct padico_ibv_connection_s));
  conn->of_cq = ibv_create_cq(padico_ibv.psp_ibv_port->context, PADICO_IBV_TX_DEPTH, NULL, NULL, 0);
  padico_assert(conn->of_cq != NULL, "Infiniband: couldn't create out CQ\n");
  conn->if_cq = ibv_create_cq(padico_ibv.psp_ibv_port->context, PADICO_IBV_RX_DEPTH, NULL, NULL, 0);
  padico_assert(conn->if_cq != NULL, "Infiniband: couldn't create in CQ\n");

  /* create QP */
  struct ibv_qp_init_attr qp_init_attr =
    {
      .send_cq = conn->of_cq,
      .recv_cq = conn->if_cq,
      .cap     = {
        .max_send_wr  = PADICO_IBV_TX_DEPTH,
        .max_recv_wr  = PADICO_IBV_RX_DEPTH,
        .max_send_sge = PADICO_IBV_MAX_SG_SQ,
        .max_recv_sge = PADICO_IBV_MAX_SG_RQ,
        .max_inline_data = PADICO_IBV_MAX_INLINE
      },
      .qp_type = IBV_QPT_RC
    };
  conn->qp = ibv_create_qp(padico_ibv.pd, &qp_init_attr);
  padico_assert(conn->qp != NULL, "Infiniband: couldn't create QP\n");
  conn->max_inline = qp_init_attr.cap.max_inline_data;

  /* init QP- step: INIT */
  struct ibv_qp_attr qp_attr =
    {
      .qp_state        = IBV_QPS_INIT,
      .pkey_index      = 0,
      .port_num        = padico_ibv.psp_ibv_port->port_num,
      .qp_access_flags = IBV_ACCESS_REMOTE_WRITE
    };
  int rc = ibv_modify_qp(conn->qp, &qp_attr,
                         IBV_QP_STATE              |
                         IBV_QP_PKEY_INDEX         |
                         IBV_QP_PORT               |
                         IBV_QP_ACCESS_FLAGS);
  padico_assert(!rc, "Infiniband: failed to modify QP to INIT\n");

  /* init LZ */
#ifdef RLIMIT_MEMLOCK
  struct rlimit lim;
  getrlimit(RLIMIT_MEMLOCK, &lim);
  if(lim.rlim_cur * 1024 < sizeof(conn->lz.zones))
    {
      padico_warning("RLIMIT_MEMLOCK is %d kB. Memory region registration is likely to fail (we need %d kB)\n", (int)lim.rlim_cur, (int)sizeof(conn->lz.zones)/1024);
    }
#endif /* RLIMIT_MEMLOCK */
  conn->lz.raddr = 0;
  conn->lz.rkey  = 0;
  conn->lz.mr = ibv_reg_mr(padico_ibv.pd, &conn->lz.zones,
                           sizeof(conn->lz.zones),
                           IBV_ACCESS_REMOTE_WRITE | IBV_ACCESS_LOCAL_WRITE);
  memset(&conn->lz.zones, 0, sizeof(conn->lz.zones));
  padico_assert(conn->lz.mr != NULL, "Infiniband: couldn't allocate LZ MR\n");
  conn->lz.next_in  = 1;
  conn->lz.to_ack   = 0;
  conn->lz.next_out = 1;
  conn->lz.credits  = PADICO_IBV_LZ_BUFNUM;
  int i;
  for(i = 0; i < PADICO_IBV_LZ_BUFNUM; i++)
    {
      conn->lz.zones.cells[i].header.status = 0;
    }
  conn->lz.zones.rv_box.status = 0;
  /* init local address */
  conn->local_addr = (struct padico_ibv_addr_s)
    {
      .lid   = padico_ibv.psp_ibv_port->port_lid,
      .qpn   = conn->qp->qp_num,
      .psn   = lrand48() & 0xffffff,
      .raddr = (unsigned long)&conn->lz.zones,
      .rkey  = conn->lz.mr->rkey
    };
  memset(conn->lz.pending, 0, sizeof(int)*PADICO_WRID_NUM);
  padico_out(40, "local address:  LID 0x%04x; QPN 0x%06x; PSN 0x%06x; raddr=%lX; rkey=%X\n",
             conn->local_addr.lid, conn->local_addr.qpn, conn->local_addr.psn,
             (unsigned long)conn->local_addr.raddr, (unsigned int)conn->local_addr.rkey);
  /* register connection in registry */
  conn->remote_node = remote_node;
  puk_hashtable_insert(padico_ibv.connections, conn->remote_node, conn);
  padico_ibv_connection_list_push_back(&padico_ibv.conns, conn);
  return conn;
}

/** Connect an (already created) connection to peer
 */
static void padico_ibv_connection_connect(struct padico_ibv_connection_s*conn,
                                          struct padico_ibv_addr_s*dest)
{
  padico_out(40, "remote address: LID 0x%04x; QPN 0x%06x; PSN 0x%06x; raddr=%lX; rkey=%X\n",
             dest->lid, dest->qpn, dest->psn, (unsigned long)dest->raddr, (unsigned int)dest->rkey);
  /* connect QP- step: RTR */
  struct ibv_qp_attr attr = {
    .qp_state           = IBV_QPS_RTR,
    .path_mtu           = IBV_MTU_1024, /* 1024 */
    .dest_qp_num        = dest->qpn,
    .rq_psn             = dest->psn,
    .max_dest_rd_atomic         = PADICO_IBV_RDMA_DEPTH,
    .min_rnr_timer      = 12, /* 12 */
    .ah_attr            = {
      .is_global        = 0,
      .dlid             = dest->lid,
      .sl               = 0,
      .src_path_bits    = 0,
      .port_num                 = padico_ibv.psp_ibv_port->port_num
    }
  };
  int rc = ibv_modify_qp(conn->qp, &attr,
                         IBV_QP_STATE              |
                         IBV_QP_AV                 |
                         IBV_QP_PATH_MTU           |
                         IBV_QP_DEST_QPN           |
                         IBV_QP_RQ_PSN             |
                         IBV_QP_MAX_DEST_RD_ATOMIC |
                         IBV_QP_MIN_RNR_TIMER);
  padico_assert(!rc, "Failed to modify QP to RTR\n");

  /* connect QP- step: RTS */
  attr.qp_state      = IBV_QPS_RTS;
  attr.timeout               = 14; /* 14 */
  attr.retry_cnt     = 7;  /* 7 */
  attr.rnr_retry     = 7;  /* 7 = infinity */
  attr.sq_psn        = conn->local_addr.psn;
  attr.max_rd_atomic = PADICO_IBV_RDMA_DEPTH; /* 1 */
  rc = ibv_modify_qp(conn->qp, &attr,
                     IBV_QP_STATE              |
                     IBV_QP_TIMEOUT            |
                     IBV_QP_RETRY_CNT          |
                     IBV_QP_RNR_RETRY          |
                     IBV_QP_SQ_PSN             |
                     IBV_QP_MAX_QP_RD_ATOMIC);
  padico_assert(!rc, "Failed to modify QP to RTS\n");

  /* connect LZ */
  conn->lz.rkey  = dest->rkey;
  conn->lz.raddr = dest->raddr;
}

/* ********************************************************* */
/* *** dynamic connection management */

static void padico_ibv_request_connect(struct padico_ibv_connection_s*conn, padico_topo_node_t remote_node)
{
  const struct padico_ibv_addr_s*addr = &conn->local_addr;
  padico_string_t msg = padico_string_new();
  padico_string_printf(msg, "<InfinibandVerbs:Connect from=\"%s\" lid=\"%hx\" qpn=\"%x\" psn=\"%x\" raddr=\"%lx\" rkey=\"%x\"/>",
                       padico_topo_nodename(), addr->lid, addr->qpn, addr->psn,
                       (unsigned long)addr->raddr, (unsigned)addr->rkey);
  padico_out(40, "req=%s\n", padico_string_get(msg));
  padico_control_send_oneway(remote_node, padico_string_get(msg));
  padico_string_delete(msg);
}

struct padico_ibv_connector_s
{
  struct padico_ibv_addr_s addr;
  padico_topo_node_t node;
};
/** asynchronous worker for connection establishment
 */
static void*padico_ibv_connector(void*_arg)
{
  struct padico_ibv_connector_s*c = _arg;
  padico_out(40, "connector- starting...\n");
  padico_ibv_lock();
  struct padico_ibv_connection_s*conn = puk_hashtable_lookup(padico_ibv.connections, c->node);
  if(!conn)
    {
      conn = padico_ibv_connection_init(c->node);
      padico_ibv_request_connect(conn, c->node);
      padico_ibv_connection_connect(conn, &c->addr);
    }
  else if(conn && !conn->lz.raddr)
    {
      padico_ibv_connection_connect(conn, &c->addr);
    }
  padico_ibv_unlock();
  padico_free(c);
  padico_out(40, "connector- done.\n");
  return NULL;
}
static void padico_ibv_connect_start_handler(puk_parse_entity_t e)
{
  struct padico_ibv_connector_s*c = padico_malloc(sizeof(struct padico_ibv_connector_s));
  c->node = padico_topo_getnodebyname(puk_parse_getattr(e, "from"));
  c->addr.lid = strtoul(puk_parse_getattr(e, "lid"), NULL, 16);
  c->addr.qpn = strtoul(puk_parse_getattr(e, "qpn"), NULL, 16);
  c->addr.psn = strtoul(puk_parse_getattr(e, "psn"), NULL, 16);
  c->addr.raddr = strtoull(puk_parse_getattr(e, "raddr"), NULL, 16);
  c->addr.rkey = strtoul(puk_parse_getattr(e, "rkey"), NULL, 16);
  padico_out(10, "connection request from node=%s\n", padico_topo_node_getname(c->node));
  padico_tm_bgthread_start(padico_ibv.pool, &padico_ibv_connector, c, "padico_ibv_connector");
}


/* ********************************************************* */
/* *** ibv I/O sysdep */

static inline void padico_ibv_do_rdma_send(struct padico_ibv_connection_s*conn,
                                           uint32_t lkey, void*buf, int size,
                                           uint32_t rkey, uintptr_t _raddr, uint64_t wrid)
{
  const uint64_t raddr = (uint64_t)_raddr;
  struct ibv_sge list = {
    .addr   = (uintptr_t)buf,
    .length = size,
    .lkey   = lkey
  };
  struct ibv_send_wr wr = {
    .wr_id      = wrid,
    .sg_list    = &list,
    .num_sge    = 1,
    .opcode     = IBV_WR_RDMA_WRITE,
    .send_flags = (size < conn->max_inline)? (IBV_SEND_INLINE | IBV_SEND_SIGNALED) : IBV_SEND_SIGNALED,
    .wr.rdma =
    {
      .remote_addr = raddr,
      .rkey        = rkey
    }
  };
  struct ibv_send_wr*bad_wr = NULL;
  int rc = ibv_post_send(conn->qp, &wr, &bad_wr);
  conn->lz.pending[wrid]++;
  padico_assert(!rc, "post send failed\n");
}

static inline void padico_ibv_do_send_wait(struct padico_ibv_connection_s*conn, uint64_t wrid)
{
  while(conn->lz.pending[wrid])
    {
      struct ibv_wc wc;
      int ne = 0;
      do
        {
          ne = ibv_poll_cq(conn->of_cq, 1, &wc);
          padico_assert(ne >= 0, "poll out CQ failed\n");
        }
      while(ne == 0);
      conn->lz.pending[wc.wr_id]--;
      padico_assert(ne == 1 && wc.status == IBV_WC_SUCCESS, "WC send failed (status=%d)\n", wc.status);
    }
}

static inline void padico_ibv_do_zone_send(struct padico_ibv_connection_s*conn,
                                           void*buf, int size, void*raddr, uint64_t wrid)
{
  const uintptr_t phys_raddr = (uintptr_t)conn->lz.raddr + ((uintptr_t)raddr - (uintptr_t)&conn->lz.zones);
  padico_out(40, "buf=%p; size=%d; (rel-)raddr=%p; (phys-)raddr=0x%X; lkey=%d; rkey=%d\n",
             buf, size, raddr, (unsigned)phys_raddr, conn->lz.mr->lkey, conn->lz.rkey);
  padico_ibv_do_rdma_send(conn, conn->lz.mr->lkey, buf, size,
                          conn->lz.rkey, phys_raddr, wrid);
}

static inline void padico_ibv_do_recv(struct padico_ibv_connection_s*conn, uint32_t lkey, void*buf, int size)
{
  struct ibv_sge list = {
    .addr   = (uintptr_t) buf,
    .length = size,
    .lkey   = lkey
  };
  struct ibv_recv_wr wr = {
    .wr_id   = PADICO_WRID_RECV,
    .sg_list = &list,
    .num_sge = 1,
  };
  struct ibv_recv_wr*bad_wr;
  int rc = ibv_post_recv(conn->qp, &wr, &bad_wr);
  padico_assert(!rc, "couldn't post recv\n");
}

static inline void padico_ibv_do_recv_wait(struct padico_ibv_connection_s*conn)
{
  struct ibv_wc wc;
  int ready = 0;
  do
    {
      ready = ibv_poll_cq(conn->if_cq, 1, &wc);
      padico_assert(ready >= 0, "poll CQ failed\n");
    }
  while(ready == 0);
  padico_assert(wc.status == IBV_WC_SUCCESS, "ibv_poll WC error.\n");
}


/* ********************************************************* */
/* *** send */

/* @pre assume other polling is disabled (lock is held)
 */
static int padico_ibv_lz_recover_credits(struct padico_ibv_connection_s*conn)
{
  /* harvest credits in LZ */
  int i;
  int recovered = 0;
  for(i = 0; i < PADICO_IBV_LZ_BUFNUM; i++)
    {
      struct padico_ibv_lz_cell_s*c = &conn->lz.zones.cells[i];
      if(c->header.status && c->header.ack)
        {
          conn->lz.credits += c->header.ack;
          recovered += c->header.ack;
          c->header.ack = 0;
        }
    }
  return recovered;
}

static inline void padico_ibv_idle(struct padico_ibv_connection_s*conn)
{
  padico_ibv_unlock();
  marcel_yield();
  padico_ibv_lock();
}

static inline void padico_ibv_lz_send(struct padico_ibv_connection_s*conn, struct iovec*buffers, int num)
{
  int current_cell = 0;
  int pending = 0;
  int remaining_size = 0;
  int i;
  for(i = 0; i < num; i++)
    {
      remaining_size += buffers[i].iov_len;
    }
  padico_ibv_lock();
  while(remaining_size)
    {
      current_cell ^= 1;
      struct padico_ibv_lz_cell_s*const cell = &conn->lz.zones.out_cell[current_cell];
      const size_t packet_size = remaining_size > PADICO_IBV_LZ_BUFSIZE ? PADICO_IBV_LZ_BUFSIZE : remaining_size;
      const size_t offset = PADICO_IBV_LZ_BUFSIZE - packet_size;
      size_t done = 0;
      padico_out(40, "building LZ- cell=%p\n", cell);
      while(done < packet_size)
        {
          if(done + buffers->iov_len <= packet_size)
            {
              /* take the whole fragment */
              padico_out(40, "whole frag- iov_len=%d; done=%d; packet_size=%d; remaining=%d\n",
                         (int)buffers->iov_len, (int)done, (int)packet_size, (int)remaining_size);
              memcpy(&cell->buffer[offset + done], buffers->iov_base, buffers->iov_len);
              done += buffers->iov_len;
              buffers++;
            }
          else
            {
              /* take a partial fragment */
              const size_t frag_size = packet_size - done;
              memcpy(&cell->buffer[offset + done], buffers->iov_base, frag_size);
              buffers->iov_len  -= frag_size;
              buffers->iov_base += frag_size;
              done += frag_size;
              padico_out(40, "partial frag_size=%d; done=%d; packet_size=%d; remaining=%d\n",
                         (int)frag_size, (int)done, (int)packet_size, (int)remaining_size);
            }
        }
      padico_out(40, "sending LZ- cell=%p\n", cell);
      cell->header = (struct padico_ibv_lz_header_s)
        {
          .offset = offset,
          .size   = packet_size,
          .ack    = conn->lz.to_ack,
          .status = PADICO_IBV_LZ_STATUS_DATA
        };
      conn->lz.to_ack = 0;
      int iter = 1;
      while(conn->lz.credits <= PADICO_IBV_LZ_CREDITS_GUARD)
      {
        const int recovered = padico_ibv_lz_recover_credits(conn);
        if(!recovered)
          {
            padico_warning("waiting for credits to send data...\n");
            padico_ibv_idle(conn);
          }
        if(iter++ % 1000 == 0)
          padico_warning("waiting credits to send data- suspecting deadlock...\n");
      }
      padico_out(40, "sending to cell #%d\n", conn->lz.next_out);
      padico_ibv_do_zone_send(conn, &cell->buffer[offset], sizeof(struct padico_ibv_lz_cell_s) - offset,
                              ((void*)&conn->lz.zones.cells[conn->lz.next_out]) + offset, PADICO_WRID_SEND);
      conn->lz.next_out = (conn->lz.next_out + 1) % PADICO_IBV_LZ_BUFNUM;
      conn->lz.credits--;
      pending++;
      if(pending > 1)
        {
          padico_ibv_do_send_wait(conn, PADICO_WRID_SEND);
          pending--;
        }
      remaining_size -= packet_size;
    }
  if(pending)
    {
      padico_ibv_do_send_wait(conn, PADICO_WRID_SEND);
      pending--;
    }
  padico_ibv_unlock();
  padico_out(50, "RDMA send ok.\n");
}

static void padico_ibv_sp_send(struct padico_ibv_connection_s*conn, const void*buf, int size)
{
  assert(conn->lz.send.message == NULL);
  conn->lz.send.message = buf;
  conn->lz.send.size    = size;
  conn->lz.send.done    = 0;
  conn->lz.send.rbuf    = conn->lz.zones.rbuf;
  conn->lz.send.sbuf    = conn->lz.zones.sbuf;
  conn->lz.send.step    = 0;
  conn->lz.send.nbuffer = 0;
  conn->lz.zones.rack  = 0;

  padico_ibv_lock();
  while(conn->lz.send.done < conn->lz.send.size)
    {
      const int chunk_size = lr2_steps[conn->lz.send.step];
      const int block_size = PADICO_IBV_LR2_BLOCKSIZE;
      const int block_max_payload = block_size - lr2_hsize;
      const int chunk_max_payload = chunk_size - lr2_hsize * chunk_size / block_size;
      const int chunk_payload = padico_ibv_min(conn->lz.send.size - conn->lz.send.done, chunk_max_payload);
      if((conn->lz.send.sbuf + chunk_size) > (((void*)conn->lz.zones.sbuf) + (conn->lz.send.nbuffer + 1) * PADICO_IBV_LR2_BUFSIZE))
        {
          /* flow control rationale- receiver may be:
           *   . reading buffer N -> ok for writing N + 1
           *   . already be ready for N + 1 -> ok
           *   . reading N - 1 (~= N + 2) -> wait
           * */
          const int n2 = (conn->lz.send.nbuffer + 2) % PADICO_IBV_LR2_NBUF;
          while(conn->lz.zones.rack == n2)
            {
            }
          /* swap buffers */
          conn->lz.send.nbuffer = (conn->lz.send.nbuffer + 1) % PADICO_IBV_LR2_NBUF;
          conn->lz.send.sbuf = ((void*)conn->lz.zones.sbuf) + conn->lz.send.nbuffer * PADICO_IBV_LR2_BUFSIZE;
          conn->lz.send.rbuf = ((void*)conn->lz.zones.rbuf) + conn->lz.send.nbuffer * PADICO_IBV_LR2_BUFSIZE;
        }
      int chunk_offset = 0; /**< offset in the sbuf/rbuf, i.e. payload + headers */
      int chunk_todo = chunk_payload;
      conn->lz.send.rbuf += (chunk_max_payload - chunk_payload) % block_max_payload;
      while(chunk_todo > 0)
        {
          const int block_payload = (chunk_todo % block_max_payload == 0) ?
            block_max_payload : (chunk_todo % block_max_payload);
          struct lr2_header_s*h = conn->lz.send.sbuf + chunk_offset + block_payload;
          memcpy(conn->lz.send.sbuf + chunk_offset,
                 conn->lz.send.message + conn->lz.send.done + (chunk_payload - chunk_todo), block_payload);
          h->busy = 1;
          chunk_todo   -= block_payload;
          chunk_offset += block_payload + lr2_hsize;
        }
      padico_ibv_do_send_wait(conn, PADICO_WRID_SP_CHUNK);
      padico_ibv_do_zone_send(conn, conn->lz.send.sbuf,
                              chunk_offset,
                              conn->lz.send.rbuf, PADICO_WRID_SP_CHUNK);
      conn->lz.send.done += chunk_payload;
      conn->lz.send.sbuf += chunk_offset;
      conn->lz.send.rbuf += chunk_offset;
      if(conn->lz.send.step < lr2_nsteps - 1)
        conn->lz.send.step++;
    }
  padico_ibv_do_send_wait(conn, PADICO_WRID_SP_CHUNK);
  conn->lz.send.message = NULL;
  padico_ibv_unlock();
}

static void padico_ibv_send(struct padico_ibv_connection_s*conn, const void*buf, int size)
{
  if(size <= PADICO_IBV_LZ_THRESHOLD)
    {
      struct iovec buffers[] = { { .iov_base = (void*)buf, .iov_len = size } };
      padico_ibv_lz_send(conn, buffers, 1);
    }
  else
    {
      padico_ibv_sp_send(conn, buf, size);
    }
}


/* ********************************************************* */
/* *** recv */

static void padico_ibv_lz_process_incoming(struct padico_ibv_connection_s*conn,
                                           struct padico_ibv_lz_header_s*h)
{
  conn->lz.credits += h->ack;
  h->ack = 0;
  if(conn->lz.to_ack > PADICO_IBV_LZ_CREDITS_THRESHOLD)
    {
      /* force credits ack */
      struct padico_ibv_lz_header_s*c = &conn->lz.zones.out_cell[0].header;
      *c = (struct padico_ibv_lz_header_s)
        {
          .offset = PADICO_IBV_LZ_BUFSIZE,
          .size   = 0,
          .ack    = conn->lz.to_ack,
          .status = PADICO_IBV_LZ_STATUS_CREDITS
        };
      int iter = 1;
      while(conn->lz.credits <= 2)
      {
        const int recovered = padico_ibv_lz_recover_credits(conn);
        if(!recovered)
          {
            padico_warning("waiting for credits to send ACK...\n");
            padico_ibv_idle(conn);
          }
        if(iter++ % 1000 == 0)
          padico_warning("waiting credits to send back credits...\n");
      }
      conn->lz.to_ack = 0;
      padico_assert(conn->lz.credits > 0, "Infiniband: remote buffer overrun while sending credits.\n");
      padico_ibv_do_zone_send(conn, c, sizeof(struct padico_ibv_lz_header_s),
                              &conn->lz.zones.cells[conn->lz.next_out].header, PADICO_WRID_SEND);
      conn->lz.next_out = (conn->lz.next_out + 1) % PADICO_IBV_LZ_BUFNUM;
      conn->lz.credits--;
      padico_ibv_do_send_wait(conn, PADICO_WRID_SEND);
    }
}

/** acknowledges the processing completion of an incoming LZ cell.
 */
static inline void padico_ibv_lz_cell_completed(struct padico_ibv_connection_s*conn,
                                                struct padico_ibv_lz_header_s*h)
{
  h->status = 0;
  conn->lz.to_ack++;
  conn->lz.next_in = (conn->lz.next_in + 1) % PADICO_IBV_LZ_BUFNUM;
}


static inline void padico_ibv_lz_recv(struct padico_ibv_connection_s*conn, void*buf, int size)
{
  int done = 0;
  padico_assert(conn->lz.zones.cells[(conn->lz.next_in+PADICO_IBV_LZ_BUFNUM-1) % PADICO_IBV_LZ_BUFNUM].header.status == 0,
                "Infiniband: LZ buffer overrun.\n");
  padico_ibv_lock();
  while(done < size)
    {
      struct padico_ibv_lz_cell_s*cell = NULL;
      /* find a cell with data */
      while(!cell)
        {
          cell = &conn->lz.zones.cells[conn->lz.next_in];
          padico_out(50, "LZ recv waiting on cell #%d (%p; base=%p)\n", conn->lz.next_in, cell, &conn->lz.zones);
          padico_ibv_wait_one(conn);
          padico_out(50, "processing cell #%d\n", conn->lz.next_in);
          padico_ibv_lz_process_incoming(conn, &cell->header);
          if(cell->header.size == 0)
            {
              padico_ibv_lz_cell_completed(conn, &cell->header);
              cell = NULL;
            }
        }
      /* take data */
      const int expected_size = size - done;
      const int packet_size = expected_size > cell->header.size ? cell->header.size : expected_size;
      padico_out(50, "cell contains data- size=%d; expected=%d; packet_size=%d\n",
                 cell->header.size, expected_size, packet_size);
      memcpy(buf+done, &cell->buffer[cell->header.offset], packet_size);
      done += packet_size;
      /* free cell (or not) */
      if(cell->header.size > packet_size)
        {
          cell->header.size   -= packet_size;
          cell->header.offset += packet_size;
        }
      else
        {
          padico_out(50, "cell #%d completed\n", conn->lz.next_in);
          padico_ibv_lz_cell_completed(conn, &cell->header);
        }
    }
  padico_ibv_unlock();
}

static void padico_ibv_sp_recv(struct padico_ibv_connection_s*conn, void*buf, int size)
{
  conn->lz.recv.done    = 0;
  conn->lz.recv.message = buf;
  conn->lz.recv.size    = size;
  conn->lz.recv.rbuf    = conn->lz.zones.rbuf;
  conn->lz.recv.step    = 0;
  conn->lz.recv.nbuffer = 0;

  padico_ibv_lock();
  while(conn->lz.recv.done < conn->lz.recv.size)
    {
      const int chunk_size = lr2_steps[conn->lz.recv.step];
      const int block_size = PADICO_IBV_LR2_BLOCKSIZE;
      const int block_max_payload = block_size - lr2_hsize;
      const int chunk_max_payload = chunk_size - lr2_hsize * chunk_size / block_size;
      const int chunk_payload = padico_ibv_min(conn->lz.recv.size - conn->lz.recv.done, chunk_max_payload);
      if((conn->lz.recv.rbuf + chunk_size) > (((void*)conn->lz.zones.rbuf) +
                                              (conn->lz.recv.nbuffer + 1) * PADICO_IBV_LR2_BUFSIZE))
        {
          /* swap buffers */
          conn->lz.recv.nbuffer = (conn->lz.recv.nbuffer + 1) % PADICO_IBV_LR2_NBUF;
          conn->lz.recv.rbuf = ((void*)conn->lz.zones.rbuf) + conn->lz.recv.nbuffer * PADICO_IBV_LR2_BUFSIZE;
          conn->lz.zones.sack = conn->lz.recv.nbuffer;
          padico_ibv_do_send_wait(conn, PADICO_WRID_SP_ACK);
          padico_ibv_do_zone_send(conn, (void*)&conn->lz.zones.sack, sizeof(uint32_t),
                                  (void*)&conn->lz.zones.rack, PADICO_WRID_SP_ACK);
        }
      int chunk_todo = chunk_payload;
      int chunk_offset = (chunk_max_payload - chunk_payload) % block_max_payload;
      while(chunk_todo > 0)
        {
          const int block_payload = (chunk_todo % block_max_payload == 0) ?
            block_max_payload : (chunk_todo % block_max_payload);
          struct lr2_header_s*h = conn->lz.recv.rbuf + block_size - lr2_hsize;
          while(!h->busy)
            {
            }
          memcpy(conn->lz.recv.message + conn->lz.recv.done,
                 conn->lz.recv.rbuf + chunk_offset,
                 block_payload);
          h->busy = 0;
          chunk_todo -= block_payload;
          conn->lz.recv.done += block_payload;
          conn->lz.recv.rbuf += chunk_offset + block_payload + lr2_hsize;
          chunk_offset = 0;
        }
      if(conn->lz.recv.step < lr2_nsteps - 1)
        conn->lz.recv.step++;
    }
  conn->lz.recv.message = NULL;
  padico_ibv_unlock();
}

static void padico_ibv_recv(struct padico_ibv_connection_s*conn, void*buf, int size)
{
  if(size <= PADICO_IBV_LZ_THRESHOLD)
    {
      padico_ibv_lz_recv(conn, buf, size);
    }
  else
    {
      padico_ibv_sp_recv(conn, buf, size);
    }
}

/* ********************************************************* */
/* *** polling */

static inline int padico_ibv_poll_lz_all(void)
{
  int ready = 0;
  padico_ibv_connection_itor_t i;
  for(i  = padico_ibv_connection_list_begin(&padico_ibv.conns);
      i != padico_ibv_connection_list_end(&padico_ibv.conns);
      i  = padico_ibv_connection_list_next(i))
    {
       struct padico_ibv_connection_s*const c = i;
      ready |= c->lz.zones.cells[c->lz.next_in].header.status;
    }
  return ready;
}

#ifdef PSP_IBV_PIOMAN
static int padico_ibv_fastpoll_lz(void*_dummy)
{
  const int ready = padico_ibv_poll_lz_all();
  if(ready)
    {
      padico_out(90, "LZ success!\n");
      piom_ltask_completed(&padico_ibv.ltask);
    }
  return 0;
}
#endif

#if 0
static int padico_ibv_fastpoll_cq(marcel_ev_server_t server,
                                  marcel_ev_op_t op,
                                  marcel_ev_req_t req,
                                  int nb_ev, int option)
{
  /* poll IB */
  struct ibv_wc wc;
  int ready = ibv_poll_cq(padico_ibv.if_cq, 1, &wc);
  padico_assert(ready >= 0, "poll CQ failed\n");
  if(ready > 0)
    {
      padico_assert(wc.status == IBV_WC_SUCCESS, "ibv_poll WC error.\n");
      padico_out(90, "IB poll success!\n");
      MARCEL_EV_REQ_SUCCESS(req);
    }
  return 0;
}
#endif /* 0 */

/** wait for any event in the rdma landing zone
 */
static inline void padico_ibv_wait_any(void)
{
#ifdef PSP_IBV_PIOMAN
  piom_ltask_wait(&padico_ibv.ltask);
  piom_ltask_create(&padico_ibv.ltask, &padico_ibv_fastpoll_lz, NULL, PIOM_LTASK_OPTION_REPEAT);
  piom_ltask_submit(&padico_ibv.ltask);
#else /* PSP_IBV_PIOMAN */
  int ready = 0;
  while(!ready)
    {
      padico_ibv_lock();
      ready = padico_ibv_poll_lz_all();
      padico_ibv_unlock();
    }
#endif /* PSP_IBV_PIOMAN */
}

/** wait for an event in a given rdma connection
 */
static inline void padico_ibv_wait_one(struct padico_ibv_connection_s*conn)
{
  struct padico_ibv_lz_cell_s*cell = &conn->lz.zones.cells[conn->lz.next_in];
  padico_ibv_unlock();
  while(!cell->header.status)
    {
#ifdef PSP_IBV_PIOMAN
      padico_ibv_wait_any();
#else /* PSP_IBV_PIOMAN */
      pthread_yield();
#endif /* PSP_IBV_PIOMAN */
    }
  padico_ibv_lock();
}


/* ********************************************************* */
/* *** PSP */

#define PSP_IBV_HEADER_SIZE 72
#define PSP_IBV_FRAG_SIZE ((size_t)(32*1024*1024)) /* 128 MB */

struct psp_ibv_instance_s
{
  padico_psp_slot_t slot;
  marcel_mutex_t lock;
  int header_sent;
  char sbuf[PADICO_IBV_LZ_BUFSIZE];
};

static void psp_ibv_pump(void*token, void*bytes, size_t size)
{
  struct padico_ibv_connection_s*const conn = token;
  padico_ibv_recv(conn, bytes, size);
}

static void*psp_ibv_worker(void*dummy)
{
  static char header[PSP_IBV_HEADER_SIZE];
  while(padico_ibv.power_on)
    {
      padico_ibv_wait_any();
      if(!padico_ibv.power_on)
        return NULL;
      padico_ibv_connection_itor_t i;
      for(i  = padico_ibv_connection_list_begin(&padico_ibv.conns);
          i != padico_ibv_connection_list_end(&padico_ibv.conns);
          i  = padico_ibv_connection_list_next(i))
        {
          struct padico_ibv_connection_s*const conn = i;
          if(conn->lz.raddr)
            {
              struct padico_ibv_lz_header_s*const h = &conn->lz.zones.cells[conn->lz.next_in].header;
              if(h->status && h->size)
                {
                  /* receive & process data */
                  padico_ibv_recv(conn, header, PSP_IBV_HEADER_SIZE);
                  const uint32_t*p_tag = (const uint32_t*)header;
                  const uint32_t tag = *p_tag;
                  const padico_psp_slot_t slot = padico_psp_slot_lookup(&padico_ibv.slots, tag);
                  (*(slot->handler))(((uint32_t*)header)+1, conn->remote_node, slot->key, &psp_ibv_pump, conn);
                }
              else if(h->status && h->size == 0)
                {
                  padico_out(50, "processing cell #%d in worker- status=0x%X\n", conn->lz.next_in, h->status);
                  padico_ibv_lock();
                  padico_ibv_lz_process_incoming(conn, h);
                  padico_ibv_lz_cell_completed(conn, h);
                  padico_ibv_unlock();
                }
            }
        }
    }
  return NULL;
}

static void*psp_ibv_instantiate(puk_instance_t ai, puk_context_t context)
{
  struct psp_ibv_instance_s*instance = padico_malloc(sizeof(struct psp_ibv_instance_s));
  marcel_mutex_init(&instance->lock, NULL);
  instance->slot = NULL;
  return instance;
}
static void psp_ibv_destroy(void*_instance)
{
  struct psp_ibv_instance_s*instance = (struct psp_ibv_instance_s*)_instance;
  if(instance->slot)
    padico_psp_slot_remove(&padico_ibv.slots, instance->slot);
  padico_free(instance);
}

static void psp_ibv_init(void*_instance, uint32_t tag, const char*label, size_t*h_size,
                         padico_psp_handler_t handler, void*key)
{
  struct psp_ibv_instance_s*instance = (struct psp_ibv_instance_s*)_instance;
  *h_size = PSP_IBV_HEADER_SIZE - sizeof(uint32_t);
  instance->slot = padico_psp_slot_insert(&padico_ibv.slots, handler, key, tag, *h_size);
  padico_out(10, "label = %s; h_size = %d\n", label, (int)*h_size);
}

static void psp_ibv_connect(void*_instance, padico_topo_node_t remote_node)
{
  padico_ibv_lock();
  struct padico_ibv_connection_s*conn = puk_hashtable_lookup(padico_ibv.connections, remote_node);
  if(!conn)
    {
      padico_out(10, "connecting to node=%s\n", padico_topo_node_getname(remote_node));
      conn = padico_ibv_connection_init(remote_node);
      padico_ibv_request_connect(conn, remote_node);
      /* TODO: wait for connection to appear */
      while(!conn->lz.raddr)
      {
        padico_ibv_unlock();
        padico_tm_msleep(200);
        padico_ibv_lock();
      }
      padico_out(10, "connected to node=%s\n", padico_topo_node_getname(remote_node));
    }
  else
    {
      padico_out(10, "instance already connected.\n,");
    }
  padico_ibv_unlock();
}

static padico_psp_connection_t psp_ibv_new_message(void*_instance, padico_topo_node_t node, void**_sbuf)
{
  struct psp_ibv_instance_s*const instance = (struct psp_ibv_instance_s*)_instance;
  padico_out(40, "entering\n");
  padico_ibv_lock();
  struct padico_ibv_connection_s*const conn = puk_hashtable_lookup(padico_ibv.connections, node);
  padico_ibv_unlock();
  marcel_mutex_lock(&instance->lock);
  instance->header_sent = 0;
  *_sbuf = ((uint32_t*)&instance->sbuf) + 1;
  uint32_t*p_tag = (uint32_t*)&instance->sbuf;
  *p_tag = instance->slot->tag;
  padico_out(40, "done.\n");
  return (padico_psp_connection_t)conn;
}

static void psp_ibv_pack(void*_instance, padico_psp_connection_t _conn,
                         const char*bytes, size_t size)
{
  struct psp_ibv_instance_s*const instance = (struct psp_ibv_instance_s*)_instance;
  struct padico_ibv_connection_s*const conn = (struct padico_ibv_connection_s*)_conn;
  int data_sent = 0;
  padico_out(40, "entering- header_sent=%d\n", instance->header_sent);
  if(!instance->header_sent)
    {
      if(size + PSP_IBV_HEADER_SIZE < PADICO_IBV_LZ_THRESHOLD)
        {
          padico_out(40, "agregating header and psp_ibv_pack()\n");
          struct iovec buffers[] = {
            { .iov_base = (void*)&instance->sbuf, .iov_len = PSP_IBV_HEADER_SIZE },
            { .iov_base = (void*)bytes, .iov_len = size }
          };
          padico_ibv_lz_send(conn, buffers, 2);
          instance->header_sent = 1;
          data_sent = 1;
        }
      else
        {
          padico_out(40, "sending header on psp_ibv_pack()\n");
          padico_ibv_send(conn, instance->sbuf, PSP_IBV_HEADER_SIZE);
          instance->header_sent = 1;
        }
    }
  if(!data_sent)
    {
      padico_ibv_send(conn, bytes, size);
    }
  padico_out(40, "done.\n");
}

static void psp_ibv_end_message(void*_instance, padico_psp_connection_t _conn)
{
  struct psp_ibv_instance_s*const instance = (struct psp_ibv_instance_s*)_instance;
  struct padico_ibv_connection_s*const conn = (struct padico_ibv_connection_s*)_conn;
  padico_out(40, "entering- header_sent=%d\n", instance->header_sent);
  if(!instance->header_sent)
    {
      padico_out(40, "sending header on psp_ibv_end_message()\n");
      padico_ibv_send(conn, instance->sbuf, PSP_IBV_HEADER_SIZE);
      instance->header_sent = 1;
    }
  marcel_mutex_unlock(&instance->lock);
  padico_out(40, "done.\n");
}


static int padico_ibv_init(void)
{
  srand48(getpid() * time(NULL));

  const struct padico_ibv_port_vect_s*ports = padico_ibv_get_ports();

  padico_ibv.psp_ibv_port = padico_ibv_port_vect_at(ports, 0);

  padico_ibv.connections = puk_hashtable_new_ptr();
  padico_ibv_connection_list_init(&padico_ibv.conns);
  puk_xml_add_action((struct puk_tag_action_s) {
    .xml_tag        = "InfinibandVerbs:Connect",
    .start_handler  = &padico_ibv_connect_start_handler,
    .end_handler    = NULL,
    .required_level = PUK_TRUST_CONTROL,
    .help           = "for internal use by InfinibandVerbs component"
  });

  /* memory registration */
  padico_ibv.pd = ibv_alloc_pd(padico_ibv.psp_ibv_port->context);
  padico_assert(padico_ibv.pd != NULL, "Couldn't allocate PD\n");


  /* *** polling */
  marcel_mutex_init(&padico_ibv.lock, NULL);
#ifdef PSP_IBV_PIOMAN
  piom_ltask_create(&padico_ibv.ltask, &padico_ibv_fastpoll_lz, NULL, PIOM_LTASK_OPTION_REPEAT);
  piom_ltask_submit(&padico_ibv.ltask);
#endif /* PSP_IBV_PIOMAN */

  padico_ibv.pool = padico_tm_bgthread_pool_create("InfinibandVerbs");

  /* *** PSP */
  padico_psp_directory_init(&padico_ibv.slots);


  return 0;

  puk_component_declare("InfinibandVerbs",
                        puk_component_provides("PadicoComponent", "component", &psp_ibv_component_driver),
                        puk_component_provides("PadicoSimplePackets", "psp", &psp_ibv_driver));
  padico_ibv.power_on = 1;
  padico_tm_rttask_start(&psp_ibv_worker, NULL, "psp_ibv_worker");
  return 0;
}


static void padico_ibv_finalize(void)
{
  padico_ibv.power_on = 0;
  padico_tm_bgthread_pool_wait(padico_ibv.pool);
}

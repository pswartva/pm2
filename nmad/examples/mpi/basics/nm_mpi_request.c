/*
 * NewMadeleine
 * Copyright (C) 2006-2023 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <mpi.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>


/* this test checks some corner cases with null requests
 */

#define ASSERT_FAILURE(ERR) if(err == MPI_SUCCESS) { fprintf(stderr, "MPI_SUCCESS when failure is expected.\n"); abort(); }
#define ASSERT_SUCCESS(ERR) if(err != MPI_SUCCESS) { fprintf(stderr, "unexpected error = %d.\n", err); abort(); }
#define CHECK_EMPTY_STATUS(STATUS) {                                    \
    int c = -1;                                                         \
    MPI_Get_count(&status, MPI_CHAR, &c);                               \
    if( (STATUS.MPI_TAG != MPI_ANY_TAG) ||                              \
        (STATUS.MPI_SOURCE != MPI_ANY_SOURCE) ||                        \
        (STATUS.MPI_ERROR != MPI_SUCCESS) ||                            \
        (c != 0))                                                       \
      {                                                                 \
        fprintf(stderr, "status not empty\n");                          \
        abort();                                                        \
      }                                                                 \
  }

int main(int argc, char**argv)
{
  int err, flag;
  int rank;
  MPI_Request request;
  MPI_Status status;

  MPI_Init(&argc,&argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  MPI_Errhandler_set(MPI_COMM_WORLD, MPI_ERRORS_RETURN);

  /* NULL requests  */
  err = MPI_Test(NULL, &flag, MPI_STATUS_IGNORE);
  ASSERT_FAILURE(err);
  err = MPI_Test(NULL, &flag, &status);
  ASSERT_FAILURE(err);
  err = MPI_Wait(NULL, MPI_STATUS_IGNORE);
  ASSERT_FAILURE(err);
  err = MPI_Wait(NULL, &status);
  ASSERT_FAILURE(err);

  /* null requests */
  request = MPI_REQUEST_NULL;
  err = MPI_Test(&request, &flag, MPI_STATUS_IGNORE);
  ASSERT_SUCCESS(err);

  memset(&status, 0, sizeof(status));
  err = MPI_Test(&request, &flag, &status);
  ASSERT_SUCCESS(err);
  CHECK_EMPTY_STATUS(status);

  err = MPI_Wait(&request, MPI_STATUS_IGNORE);
  ASSERT_SUCCESS(err);

  memset(&status, 0, sizeof(status));
  err = MPI_Wait(&request, &status);
  ASSERT_SUCCESS(err);
  CHECK_EMPTY_STATUS(status);

  /* TODO- add MPI_Waitsome/any/all & MPI_Testsome/any/all */

  printf("Success.\n");

  MPI_Finalize();
  exit(0);
}

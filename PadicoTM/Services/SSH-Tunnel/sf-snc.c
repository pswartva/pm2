/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/* @file SocketFactory for netcat over SSH
 */

#include <Padico/PadicoTM.h>
#include <Padico/VLink-internals.h>
#include <Padico/Module.h>
#include <Padico/SocketFactory.h>

static int sfsnc_module_init(void);
static void sfsnc_module_finalize(void);

PADICO_MODULE_DECLARE(SocketFactory_SSH_Netcat, sfsnc_module_init, NULL, sfsnc_module_finalize,
                      "SysIO", "Topology", "PadicoControl");


/** @defgroup SF_snc component: SocketFactory-snc- socket factory for communication through netcat over SSH console
 * @ingroup PadicoComponent
 */

/* ********************************************************* */

static int sfsnc_create(void*_instance, int family, int type, int protocol);
static int sfsnc_connect(void*_instance, const struct sockaddr*addr, socklen_t addrlen, padico_socketfactory_connector_t notifier, void*key);
static int sfsnc_close(void*_instance);

/** 'SocketFactory' facet for SocketFactory-snc
 * @ingroup SF_snc
 */
static const struct padico_socketfactory_s sfsnc_sf_driver =
  {
    .create  = &sfsnc_create,
    .bind    = NULL,
    .listen  = NULL,
    .connect = &sfsnc_connect,
    .close   = &sfsnc_close,
    .caps    = padico_vsock_caps_directconnect | padico_vsock_caps_clientonly
  };

static void*sfsnc_instantiate(puk_instance_t, puk_context_t);
static void sfsnc_destroy(void*);

/** instanciation facet for SocketFactory-snc
 * @ingroup SF_snc
 */
static const struct puk_component_driver_s sfsnc_component_driver =
  {
    .instantiate = &sfsnc_instantiate,
    .destroy     = &sfsnc_destroy
  };

/** instance of SocketFactory-snc
 * @ingroup SF_snc
 */
struct sfsnc_status_s
{
  int fd;
  padico_io_t io_connect;
  padico_socketfactory_connector_t connector;
  void*connector_key;
  pid_t pid;
  const char*ssh_path;
  const char*ssh_host;
  const char*ssh_cipher;
  const char*netcat_path;
};

static int sfsnc_callback_connector(int fd, void*key);

/* ********************************************************* */

static struct
{
  padico_tm_bgthread_pool_t pool;
} sfsnc;

static int sfsnc_module_init(void)
{
  sfsnc.pool = padico_tm_bgthread_pool_create("SocketFactory_SSH_Netcat");
  puk_component_declare("SocketFactory_SSH_Netcat",
                        puk_component_provides("PadicoComponent", "component", &sfsnc_component_driver),
                        puk_component_provides("SocketFactory", "sf", &sfsnc_sf_driver),
                        puk_component_attr("ssh_path",    "/usr/bin/ssh"),
                        puk_component_attr("ssh_cipher",  "blowfish-cbc"),
                        puk_component_attr("ssh_host",    NULL),
                        puk_component_attr("netcat_path", "nc"));
  return 0;
}

static void sfsnc_module_finalize(void)
{
  padico_tm_bgthread_pool_wait(sfsnc.pool);
}

/* ********************************************************* */
/* *** 'PadicoComponent' for SocketFactory_SSH_Netcat */

static void*sfsnc_instantiate(puk_instance_t instance, puk_context_t context)
{
  struct sfsnc_status_s*status = padico_malloc(sizeof(struct sfsnc_status_s));
  status->fd = -1;
  status->pid = -1;
  status->io_connect = NULL;
  status->connector  = NULL;
  status->ssh_path    = puk_context_getattr(context, "ssh_path");
  status->ssh_host    = puk_context_getattr(context, "ssh_host");
  status->ssh_cipher  = puk_context_getattr(context, "ssh_cipher");
  status->netcat_path = puk_context_getattr(context, "netcat_path");
  padico_out(40, "done.\n");
  return status;
}

static void sfsnc_destroy(void*_status)
{
  struct sfsnc_status_s*status = _status;
  if(status->io_connect)
    {
      padico_io_deactivate(status->io_connect);
      padico_io_unregister(status->io_connect);
      status->io_connect = NULL;
    }
  if(status->pid != -1)
    {
      int rc = -1, err = -1;
      padico_print("kill()- pid=%d\n", status->pid);
      puk_spinlock_acquire();
      rc  = kill(status->pid, SIGTERM);
      err = puk_abi_real_errno();
      puk_spinlock_release();
      if(rc)
        {
          padico_warning("kill()- errno=%d (%s).\n", err, strerror(err));
        }
      puk_spinlock_acquire();
      rc  = waitpid(status->pid, NULL, 0);
      puk_spinlock_release();
      status->pid = -1;
    }
  padico_free(status);
  padico_out(40, "done.\n");
}

/* ********************************************************* */
/* *** 'SocketFactory' for SocketFactory_SSH_Netcat */

static int sfsnc_create(void*_status, int family, int type, int protocol)
{
  struct sfsnc_status_s*status = _status;
  int err = 0, rc __attribute__((unused)) = -1;
  if(family != AF_INET)
    {
      err = EAFNOSUPPORT;
      goto exit;
    }
  if(type != SOCK_STREAM)
    {
      err = EPROTONOSUPPORT;
      goto exit;
    }
  if(status->ssh_host == NULL)
    {
      padico_warning("no hostname given- cannot create.\n");
      err = ENETUNREACH;
      goto exit;
    }
 exit:
  padico_out(40, "fd=%d; err=%d\n", rc, err);
  return -err;
}

static int sfsnc_connect(void*_status, const struct sockaddr*addr, socklen_t addrlen,
                          padico_socketfactory_connector_t notifier, void*key)
{
  struct sfsnc_status_s*status = _status;
  struct sockaddr_in*inaddr = (struct sockaddr_in*)addr;
  int rc = -1, err = 0;
  pid_t pid = -1;
  int sv[2] = { -1, -1};
  int keepalive = 1;

  padico_out(30, "entering\n");
  if(status->ssh_host == NULL)
    {
      padico_warning("no hostname given- cannot connect.\n");
      err = ENETUNREACH;
      goto exit;
    }
  rc  = PUK_ABI_FSYS_WRAP(socketpair)(AF_LOCAL, SOCK_STREAM, 0, sv);
  err = __puk_abi_wrap__errno;
  if(rc)
    goto exit;
  padico_out(30, "socketpair()- sv[0]=%d; sv[1]=%d\n", sv[0], sv[1]);
  rc  = PUK_ABI_FSYS_WRAP(setsockopt)(sv[0], SOL_SOCKET, SO_KEEPALIVE, &keepalive, sizeof(keepalive));
  err = __puk_abi_wrap__errno;
  if(rc)
    goto exit;
  rc  = PUK_ABI_FSYS_WRAP(setsockopt)(sv[1], SOL_SOCKET, SO_KEEPALIVE, &keepalive, sizeof(keepalive));
  err = __puk_abi_wrap__errno;
  if(rc)
    goto exit;
  puk_spinlock_acquire();
  pid = fork();
  err = puk_abi_real_errno();
  puk_spinlock_release();
  if(pid == -1)
    {
      padico_warning("fork() failed\n");
      goto exit;
    }
  if(pid == 0)
    {
      /* process: ssh */
      const char*host = inet_ntoa(inaddr->sin_addr);
      int port = ntohs(inaddr->sin_port);
      char s_port[8];
      snprintf(s_port, 7, "%d", (int)port);
      const char *argv[] =
        {
          status->ssh_path,         /* default: /usr/bin/ssh */
          "-x",                       /* disable X11 forwarding */
          "-a",                       /* disable agent forwarding */
          "-e", "none",               /* disable escape sequences for binary connection */
          "-c", status->ssh_cipher, /* default: blowfish-cbc */
          status->ssh_host,
          status->netcat_path,      /* default: nc */
          "-q", "0",                  /* disconnect immediately when stdin is closed */
          host, s_port,               /* nc args: <host> <port> */
          NULL                        /* NULL-terminated array */
        };
      padico_out(30, "connecting processes.\n");
      rc = 0;
      /* some plumbing- connect stdin / stdout to socket */
      rc = PUK_ABI_FSYS_WRAP(close)(sv[0]);
      if(rc)
        goto nc_error;
      rc = PUK_ABI_FSYS_WRAP(close)(0);
      if(rc)
        goto nc_error;
      rc = PUK_ABI_FSYS_WRAP(close)(1);
      if(rc)
        goto nc_error;
      rc = PUK_ABI_FSYS_WRAP(dup2)(sv[1], 0);
      if(rc == -1)
        goto nc_error;
      rc = PUK_ABI_FSYS_WRAP(dup2)(sv[1], 1);
      if(rc == -1)
        goto nc_error;
      rc = PUK_ABI_FSYS_WRAP(close)(sv[1]);
      if(rc)
        goto nc_error;

      /* remove padico library path */
      padico_out(50, "initial LD_LIBRARY_PATH=*%s*\n", getenv("LD_LIBRARY_PATH"));
      const char*padico_root = padico_getenv("PADICO_ROOT");
      padico_string_t new_lib = padico_string_new();
      padico_string_t padico_lib = padico_string_new();
      padico_string_t core_lib = padico_string_new();
      padico_string_catf(padico_lib, "%s/lib", padico_root);
      padico_string_catf(core_lib, "%s/var/mod/%s/lib", padico_root, padico_getattr("PADICO_CORE"));
      char*saveptr = NULL;
      char*lib = getenv("LD_LIBRARY_PATH");
      char*token = strtok_r(lib, ":", &saveptr);
      while(token != NULL)
        {
          if(strstr(token, padico_string_get(padico_lib)) == NULL &&
             strcmp(token, padico_string_get(core_lib)) != 0)
            {
              padico_string_catf(new_lib, "%s:", token);
            }
          token = strtok_r(NULL, ":", &saveptr);
        }
      setenv("LD_LIBRARY_PATH", padico_string_get(new_lib), 1);
      padico_string_delete(new_lib);
      padico_string_delete(padico_lib);
      padico_string_delete(core_lib);
      padico_out(50, "reduced LD_LIBRARY_PATH=*%s*\n", getenv("LD_LIBRARY_PATH"));

      padico_print("rc=%d; ssh_path=%s; ssh_host=%s; netcat_path=%s; addr=%s:%d\n",
                   rc, status->ssh_path, status->ssh_host, status->netcat_path, host, port);
      puk_spinlock_acquire();
      rc  = PUK_ABI_PROC_REAL(execv)(status->ssh_path, (char**)argv);
      err = puk_abi_real_errno();
      puk_spinlock_release();
    nc_error:
      err = __puk_abi_wrap__errno;
      padico_fatal("exec() failed for ssh- errno=%d (%s).\n", err, strerror(err));
    }
  else
    {
      /* process: padico */
      status->pid = pid;
      status->fd  = sv[0];
      status->connector = notifier;
      status->connector_key = key;
      status->io_connect = padico_io_register(status->fd, PADICO_IO_EVENT_WRITE,
                                                &sfsnc_callback_connector, status);
      padico_io_activate(status->io_connect);
      rc  = PUK_ABI_FSYS_WRAP(close)(sv[1]);
      err = __puk_abi_wrap__errno;
      if(rc)
        goto exit;
      err = EINPROGRESS;
    }
 exit:
  padico_out(30, "fd=%d; err=%d (%s)\n", status->fd, err, strerror(err));
  return -err;
}

static int sfsnc_close(void*_status)
{
  struct sfsnc_status_s*status = _status;
  int rc = -1;
  if(status->io_connect)
    {
      padico_io_deactivate(status->io_connect);
      padico_io_unregister(status->io_connect);
      status->io_connect = NULL;
    }
  if(status->fd != -1)
    {
      rc = PUK_ABI_FSYS_WRAP(close)(status->fd);
      int err __attribute__((unused)) = __puk_abi_wrap__errno;
      padico_out(50, "::close() rc=%d errno=%d (%s)\n", rc, err, strerror(err));
      status->fd = -1;
    }
  /* the ssh tunnel will be killed by destroy() */
  return rc;
}



/* ********************************************************* */

static void*sfsnc_connector_finalizer(void*_status)
{
  struct sfsnc_status_s*status = _status;
  (*status->connector)(status->connector_key, status->fd);
  return NULL;
}

static int sfsnc_callback_connector(int fd, void*key)
{
  int again = 0;
  struct sfsnc_status_s*status = key;
  padico_out(40, "fd=%d\n", fd);
  assert(fd == status->fd);
  padico_tm_bgthread_start(sfsnc.pool, &sfsnc_connector_finalizer, status,
                           "SocketFactory_SSH_Netcat:sfsnc_connector_finalizer");
  return again;
}

#! /bin/sh

echo "#args: $#"
echo "args: $@"

echo "Hello World!"
echo "I am $0 running on:"
uname -a
echo
echo "Current date is:"
/bin/date
echo
echo "My current working directory is `pwd`"
echo "Listing the directory"

echo "--- BEGIN ---"
/bin/ls
echo "--- END --- "
echo
echo "PATH=$PATH"
echo "LD_LIBRARY_PATH=$LD_LIBRARY_PATH"
echo
echo "Waiting 10 seconds..."
sleep 10

echo "Waking up!"
echo
echo "Leaving..."

/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file ns-basic-common.c
 * @brief Common functions of NS-basic module
 * @ingroup NetSelector-basic
 */

#include "Ns-basic-internals.h"
#include <fnmatch.h>

PADICO_MODULE_HOOK(NetSelector_basic);

__PUK_SYM_INTERNAL
struct padico_ns_database_s nsdb;

/* ********************************************************* */

static inline int ns_host_in_entity(padico_topo_host_t host, struct ns_entity_s*entity, void**cookie)
{
  switch(entity->kind)
    {
    case NS_ENTITY_NODE:
      {
        return 0;
      }
      break;

    case NS_ENTITY_HOST:
      {
        struct ns_host_s*const h = &entity->host;
        if(h->host)
          {
            /* regular host */
            return h->host == host;
          }
        else if( (fnmatch(h->name, padico_topo_host_getname(host), 0) == 0) &&
                 ((!cookie) ||
                  (cookie && ((!*cookie) || (host == *cookie)))))
          {
            /* wildcard host */
            if(cookie)
              *cookie = host;
            return 1;
          }
      }
      break;

    case NS_ENTITY_GROUP:
      {
        ns_entity_vect_itor_t e;
        puk_vect_foreach(e, ns_entity, &entity->group.leaves)
          {
            if(ns_host_in_entity(host, *e, NULL))
              return 1;
          }
      }
      break;

    default:
      break;
    }
  return 0;
}

/** @return 1 if 'node' is contained in given entity (rule matches) */
static inline int ns_node_in_entity(padico_topo_node_t node, struct ns_entity_s*entity, void**cookie)
{
  switch(entity->kind)
    {
    case NS_ENTITY_NODE:
      {
        const struct ns_node_s*const n = &entity->node;
        if(n->node)
          {
            /* regular node */
            return n->node == node;
          }
        else
          {
            /* wildcard node */
            if(cookie && (*cookie == NULL || node == *cookie))
              {
                *cookie = node;
                return 1;
              }
            else
              return 0;
          }
      }
      break;

    case NS_ENTITY_HOST:
      {
        const padico_topo_host_t host = padico_topo_node_gethost(node);
        return ns_host_in_entity(host, entity, cookie);
      }
      break;

    case NS_ENTITY_GROUP:
      {
        const char*const node_name = padico_topo_node_getname(node);
        const ns_entity_t self_node = puk_hashtable_lookup(nsdb.nodes_by_name, node_name);
        const int matches =
          (self_node && (ns_entity_vect_find(&self_node->contained, entity) != NULL));
        return matches || ns_host_in_entity(padico_topo_node_gethost(node), entity, cookie);
      }
      break;

    default:
      break;
    }
  return 0;
}

puk_component_t ns_basic_assembly(padico_topo_node_t n1, padico_topo_node_t n2,
                                  const char* profile, puk_iface_t iface)
{
  const struct ns_rule_bucket_s rulebucket =
    {
      .iface   = iface,
      .profile = profile?:PADICO_NS_DEFAULT_PROFILE
    };
  const ns_rule_list_t rules = puk_hashtable_lookup(nsdb.rulebuckets, &rulebucket);
  if(rules)
    {
      ns_rule_t rule;
      puk_list_foreach(ns_rule, rule, rules)
        {
          ns_target_vect_itor_t target;
          puk_vect_foreach(target, ns_target, &rule->targets)
            {
              switch(target->kind)
                {
                case NS_TARGET_CLIQUE:
                  {
                    ns_entity_t entity = target->clique.entity;
                    void*cookie = NULL;
                    if(ns_node_in_entity(n1, entity, &cookie) &&
                       ns_node_in_entity(n2, entity, &cookie))
                      return rule->assembly;
                  }
                  break;
                case NS_TARGET_ARROW:
                  {
                    if(ns_node_in_entity(n1, target->arrow.tail, NULL) &&
                       ns_node_in_entity(n2, target->arrow.head, NULL))
                      return rule->assembly;
                  }
                  break;
                case NS_TARGET_MULTIPARTITE:
                  {
                    ns_entity_t node1_part = NULL, node2_part = NULL;
                    void*cookie1 = NULL, *cookie2 = NULL;
                    ns_entity_t*_i;
                    puk_vect_foreach(_i, ns_entity, target->multipartite.subsets)
                      {
                        ns_entity_t i = *_i;
                        if(!node1_part && i != node2_part && ns_node_in_entity(n1, i, &cookie1)) node1_part = i;
                        if(!node2_part && i != node1_part && ns_node_in_entity(n2, i, &cookie2)) node2_part = i;
                      }
                    if(node1_part && node2_part && node1_part != node2_part)
                      return rule->assembly;
                  }
                  break;
                case NS_TARGET_NONE:
                  padico_fatal("target type=NONE. We shouldn't be here.\n");
                  break;
                case NS_TARGET_UNKNOWN:
                  break;
                }
            }
        }
#if 0
      padico_fatal("no assembly found for iface=%s; profile=%s on link %s -- %s\n",
                   iface->name,  rulebucket.profile,
                   padico_topo_node_getname(n1),
                   padico_topo_node_getname(n2));
#endif
      return NULL;
    }
  else
    {
      return NULL;
    }
}

puk_component_t ns_basic_host_assembly(padico_topo_host_t host,
                                       const char* profile, puk_iface_t iface)
{
  if(!host)
    {
      /* NetSelector-basic does not support (yet) NULL host */
      return NULL;
    }
  const padico_topo_node_t n1 = padico_topo_getlocalnode();
  const struct ns_rule_bucket_s rulebucket = {
    .iface = iface,
    .profile = profile?:PADICO_NS_DEFAULT_PROFILE
  };
  const ns_rule_list_t rules = puk_hashtable_lookup(nsdb.rulebuckets, &rulebucket);
  if(!rules)
    {
      return NULL;
    }
  ns_rule_t rule;
  puk_list_foreach(ns_rule, rule, rules)
    {
      ns_target_vect_itor_t target;
      puk_vect_foreach(target, ns_target, &rule->targets)
        {
          switch(target->kind)
            {
            case NS_TARGET_CLIQUE:
              {
                ns_entity_t entity = target->clique.entity;
                void*cookie = NULL;
                if(ns_node_in_entity(n1, entity, &cookie) &&
                   ns_host_in_entity(host, entity, &cookie))
                  return rule->assembly;
              }
              break;
            case NS_TARGET_ARROW:
              {
                if(ns_node_in_entity(n1, target->arrow.tail, NULL) &&
                   ns_host_in_entity(host, target->arrow.head, NULL))
                  return rule->assembly;
              }
              break;
            case NS_TARGET_MULTIPARTITE:
              {
                ns_entity_t node1_part = NULL, host_part = NULL;
                void*cookie1 = NULL, *cookie2 = NULL;
                ns_entity_t*_i;
                puk_vect_foreach(_i, ns_entity, target->multipartite.subsets)
                  {
                    ns_entity_t i = *_i;
                    if(!node1_part && i != host_part && ns_node_in_entity(n1, i, &cookie1)) node1_part = i;
                    if(!host_part && i != node1_part && ns_host_in_entity(host, i, &cookie2)) host_part = i;
                  }
                if(node1_part && host_part && node1_part != host_part)
                  return rule->assembly;
              }
              break;
            case NS_TARGET_NONE:
              padico_fatal("target type=NONE. We shouldn't be here.\n");
              break;
            case NS_TARGET_UNKNOWN:
              break;
            }
        }
    }
  return NULL;
}

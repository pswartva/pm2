/*
 * MadMPI benchmark
 * Copyright (C) 2015 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


#include <stdlib.h>
#include "mpi_bench_generic.h"

extern const struct mpi_bench_s mpi_bench_colloverlap_ibcast;
extern const struct mpi_bench_s mpi_bench_colloverlap_igather;
extern const struct mpi_bench_s mpi_bench_colloverlap_iallgather;
extern const struct mpi_bench_s mpi_bench_colloverlap_iallreduce;
extern const struct mpi_bench_s mpi_bench_colloverlap_ialltoall;

int main(int argc, char**argv)
{
  struct mpi_bench_param_s params =
    {
      .start_len   = MIN_DEFAULT,
      .end_len     = MAX_DEFAULT,
      .multiplier  = MULT_DEFAULT,
      .increment   = INCR_DEFAULT,
      .iterations  = LOOPS_DEFAULT,
      .param       = PARAM_DEFAULT
    };

  mpi_bench_init(&argc, &argv, 0);
  mpi_bench_params(argc, argv, &params);
  mpi_bench_run(&mpi_bench_colloverlap_ibcast, &params);
  mpi_bench_run(&mpi_bench_colloverlap_igather, &params);
  mpi_bench_run(&mpi_bench_colloverlap_iallgather, &params);
  mpi_bench_run(&mpi_bench_colloverlap_iallreduce, &params);
  mpi_bench_run(&mpi_bench_colloverlap_ialltoall, &params);
  mpi_bench_finalize();
  exit(0);
}

dnl -*- mode: Autoconf;-*-

AC_DEFUN([AC_PUK_OUT],
         [ dnl --- Puk/
           AC_PADICO_OUT_MK([Makefile Puk.pc Puk.gdbinit Examples/lfqueue/Makefile])
           dnl --- Puk/ModulesTools/
           AC_PADICO_OUT_MK([ModulesTools/Makefile])
           AC_PADICO_OUT_SH([ModulesTools/padico-mkmod])
          dnl --- simulation
          AC_PADICO_OUT_SH([simulation/puk-mult-run])
         ])

dnl -- Puk root path
dnl --
AC_DEFUN([AC_PUK_ROOT],
         [
           AC_REQUIRE([AC_PADICO_PACKAGE_ROOT])
           AC_MSG_CHECKING([for PUK_ROOT])
           puk_root=${prefix}
           AC_DEFINE_UNQUOTED(PUK_ROOT, "${puk_root}/", [Puk root install directory])
           AC_MSG_RESULT([${puk_root}])
         ])

# ## check how to disable std lib linkage
# used by padico-mkmod
AC_DEFUN([AC_PUK_NOSTDLIB],
         [ AC_REQUIRE([AC_PADICO_LINKER])
           AC_MSG_CHECKING([for nostdlib ld flag])
           case ${LD} in
             *c++*|*g++*|*gcc*|*clang*|*icc*) LDNOSTDLIB="";; # LDNOSTDLIB="-nostdlib";;
             'CC')  LDNOSTDLIB="-z muldefs";;
             *) AC_MSG_ERROR([Unknown C++ linker: ${LD}. Stop.])
           esac
           AC_MSG_RESULT([${LDNOSTDLIB}])
           AC_SUBST(LDNOSTDLIB)
         ])



# ## how to convert a static library into a shared object file
# used by padico-mkmod
AC_DEFUN([AC_PUK_ATOSO],
         [ AC_REQUIRE([AC_PADICO_COMPILER])
           AC_MSG_CHECKING([whether static libraries can be converted into dynamic libraries ])
           libm_a=`${CC} -print-file-name=libm.a`
           if test -r ${libm_a} ; then
             _LOCALLIBA=${libm_a}
           else
             _LOCALLIBA=`echo /usr/lib/lib*.a | cut -f 1 -d ' '`
             if test ! -r ${_LOCALLIBA} ; then
               AC_MSG_WARN([ Unable to find a lib.a in /usr/lib ])
             fi
           fi
           _LOCALLIBSO=test.so
           atoso_flags_gnu="-shared -export-dynamic -whole-archive -fPIC"
           atoso_flags_bsd="-G z allextract"
           ATOSO_OK="yes"
           if ( ld ${atoso_flags_gnu} ${_LOCALLIBA} -o ${_LOCALLIBSO} 1> /dev/null 2>&1 ); then
             ATOSO="ld ${atoso_flags_gnu}";
           elif ( gnu-ld ${atoso_flags_gnu} ${_LOCALLIBA} -o ${_LOCALLIBSO} 1> /dev/null 2>&1 ); then
             ATOSO="gnu-ld ${atoso_flags_gnu}";
           elif ( ld ${atoso_flags_bsd} ${_LOCALLIBA} -o ${_LOCALLIBSO}  1> /dev/null 2>&1 ); then
             ATOSO="ld ${atoso_flags_bsd}";
           else
             ATOSO="";
             ATOSO_OK="no";
           fi
           if test -r ${_LOCALLIBSO} ; then rm ${_LOCALLIBSO}; fi
           AC_MSG_RESULT([${ATOSO_OK}: ${ATOSO} ])
           AC_SUBST(ATOSO)
           AC_SUBST(ATOSO_OK)
         ])

# ## required and optional features
AC_DEFUN([AC_PUK_FEATURES],
         [ AC_REQUIRE([AC_PADICO_COMPILER])
           AC_REQUIRE([AC_PADICO_DEBUG])
           if test "x${padico_enable_debug}" = "xyes"; then
              AC_DEFINE([PUK_DEBUG], [1], [whether Puk is built in DEBUG])
           fi

           dnl - required headers
           AC_CHECK_HEADERS(dlfcn.h setjmp.h errno.h sys/stat.h sys/wait.h,
             [],
             AC_MSG_ERROR([Missing required header file]))

           AC_CHECK_HEADER(elf.h,
             [ HAVE_ELF_H="yes"
               AC_DEFINE(HAVE_ELF_H, 1, [Define if you have the <elf.h> header file.])],
             [ HAVE_ELF_H="no"])

           AC_CHECK_HEADER(stdint.h,
             [ HAVE_STDINT_H="yes"
               AC_DEFINE(HAVE_STDINT_H, 1, [Define if you have the <stdint.h> header file.])
               AC_DEFINE(PUK_HAVE_STDINT_H, 1, [Define if you have the <stdint.h> header file.])
             ],
             [ HAVE_STDINT_H="no"])

           dnl - network optional headers
           AC_CHECK_HEADER(ifaddrs.h,
             [ HAVE_IFADDRS_H="yes"
               AC_DEFINE(HAVE_IFADDRS_H, 1, [Define if you have the <ifaddrs.h> header file.])],
             [HAVE_IFADDRS_H="no"])
           AC_CHECK_HEADER(linux/netlink.h,
             [ HAVE_LINUX_NETLINK_H="yes"
               AC_DEFINE(HAVE_LINUX_NETLINK_H, 1, [Define if you have the <linux/netlink.h> header file.])],
             [HAVE_LINUX_NETLINK_H="no"])

           AC_CHECK_HEADER(linux/rtnetlink.h,
             [ HAVE_LINUX_RTNETLINK_H="yes"
               AC_DEFINE(HAVE_LINUX_RTNETLINK_H, 1, [Define if you have the <linux/rtnetlink.h> header file.])],
             [HAVE_LINUX_RTNETLINK_H="no"])

           AC_CHECK_HEADER(linux/sockios.h,
             [ HAVE_LINUX_SOCKIOS_H="yes"
               AC_DEFINE(HAVE_LINUX_SOCKIOS_H, 1, [Define if you have the <linux/sockios.h> header file.])],
             [HAVE_LINUX_SOCKIOS_H="no"])

           AC_CHECK_HEADER(linux/ethtool.h,
             [ HAVE_LINUX_ETHTOOL_H="yes"
               AC_DEFINE(HAVE_LINUX_ETHTOOL_H, 1, [Define if you have the <linux/ethtool.h> header file.])],
             [HAVE_LINUX_ETHTOOL_H="no"])

           AC_CHECK_HEADER(linux/if.h,
             [ HAVE_LINUX_IF_H="yes"
               AC_DEFINE(HAVE_LINUX_IF_H, 1, [Define if you have the <linux/if.h> header file.])],
             [HAVE_LINUX_IF_H="no"],
             [#include <netinet/in.h>])

           dnl - valgrind
           AC_CHECK_HEADER(valgrind/valgrind.h,
             [ HAVE_VALGRIND_H="yes"
               AC_DEFINE(HAVE_VALGRIND_H, 1, [Define if you have the <valgrind/valgrind.h> header file.])
               AC_DEFINE(PUK_HAVE_VALGRIND_H, 1, [Define if you have the <valgrind/valgrind.h> header file.])
             ],
             [ HAVE_VALGRIND_H="no" ])

           AC_CHECK_FUNCS([ setenv ])

           dnl - timing functions
           AC_SEARCH_LIBS(clock_gettime, [c rt],
             [ AC_DEFINE(HAVE_CLOCK_GETTIME, 1, [have clock_gettime])
               AC_DEFINE(PUK_HAVE_CLOCK_GETTIME, 1, [have clock_gettime]) ], [])
           AC_CHECK_DECLS(mach_absolute_time,
             [ AC_DEFINE(HAVE_MACH_ABSOLUTE_TIME, 1, [have mach_absolute_time declaration])
               AC_DEFINE(PUK_HAVE_MACH_ABSOLUTE_TIME, 1, [have mach_absolute_time]) ], [],
                [[
                        #include <mach/mach_time.h>
                ]])
         ])

AC_DEFUN([AC_PUK_GNU_TM],
         [
           AC_REQUIRE([AC_PADICO_COMPILER])
           AC_REQUIRE([AC_PADICO_LINKER])
           AC_MSG_CHECKING([whether we are building with gcc transactional memory support])
           AC_ARG_ENABLE(gnu-tm,
                         [  --enable-gnu-tm         Build with gcc transactional memory support ],
                         [ if test $enableval = yes; then
                             puk_enable_gnu_tm=yes
                           else
                             puk_enable_gnu_tm=no
                           fi],
                         [ PUK_ENABLE_GNU_TM=no ])
           AC_MSG_RESULT([ ${puk_enable_gnu_tm} ])
           if test x${puk_enable_gnu_tm} = xyes; then
             AX_CHECK_COMPILE_FLAG([-fgnu-tm],
                                   [has_compile_gnu_tm=yes],
                                   [has_compile_gnu_tm=no])
             AC_MSG_CHECKING([whether tool chain supports GNU TM])
             AC_LANG(C)
             save_CFLAGS="${CFLAGS}"
             save_LIBS="${LIBS}"
             CFLAGS="${CFLAGS} -fgnu-tm"
             LIBS="${LIBS} -litm"
             AC_COMPILE_IFELSE([
               AC_LANG_PROGRAM([],
               [
                   int i = 0;
                   __transaction_atomic { i++; }
                   return 0;
               ])
             ], [ has_compile_gnu_tm=yes ], [ has_compile_gnu_tm=no ] )
             if test ${has_compile_gnu_tm} = no; then
               AC_MSG_ERROR([compiler does not support GNU TM])
             fi
             AC_MSG_RESULT([yes])
             AC_DEFINE([PUK_ENABLE_GNU_TM], 1, [whether we use GNU TM])
             AC_DEFINE([PUK_USE_TRANSACTION], 1, [whether we use transactional memory])
             CFLAGS="${save_CFLAGS}"
             LIBS="${save_LIBS}"
             PUK_CFLAGS="${PUK_CFLAGS} -fgnu-tm"
             PUK_LIBS="${PUK_LIBS} -litm"
           fi
         ])

AC_DEFUN([AC_PUK_RTM],
         [
           AC_REQUIRE([AC_PADICO_COMPILER])
           AC_REQUIRE([AC_PADICO_LINKER])
           AC_MSG_CHECKING([whether we are building with Intel Restricted Transactional Memory (RTM) support])
           AC_ARG_ENABLE(rtm,
                         [  --enable-rtm            Build with Intel Restricted Transactional Memory (RTM) support ],
                         [ if test $enableval = yes; then
                             puk_enable_rtm=yes
                           else
                             puk_enable_rtm=no
                           fi],
                         [ PUK_ENABLE_RTM=no ])
           AC_MSG_RESULT([ ${puk_enable_rtm} ])
           if test x${puk_enable_rtm} = xyes; then
             AX_CHECK_COMPILE_FLAG([-march=native],
                                   [has_compile_march_native=yes],
                                   [has_compile_march_native=no])
             if test ${has_compile_march_native} = no; then
               AC_MSG_ERROR([compiler does not support -march=native needed by RTM support])
             fi
             AC_MSG_CHECKING([whether compiler supports RTM intrinsics])
             AC_LANG(C)
             save_CFLAGS="${CFLAGS}"
             CFLAGS="${CFLAGS} -march=native"
             AC_COMPILE_IFELSE([
               AC_LANG_PROGRAM([#include <x86intrin.h>],
               [
                  int i = 0;
                  unsigned status = _xbegin();
                  if(status == _XBEGIN_STARTED) { i++; }
                  _xend();
               ])
             ], [ has_compile_rtm=yes ], [ has_compile_rtm=no ] )
             if test ${has_compile_rtm} = no; then
               AC_MSG_ERROR([compiler does not support RTM intrinsics])
             fi
             AC_MSG_RESULT([yes])
             AC_DEFINE([PUK_ENABLE_RTM], 1, [whether we use Intel RTM])
             AC_DEFINE([PUK_USE_TRANSACTION], 1, [whether we use transactional memory])
             CFLAGS="${save_CFLAGS}"
             PUK_CFLAGS="${PUK_CFLAGS} -march=native"
           fi

         ])

AC_DEFUN([AC_PUK_PROFILE],
         [
           AC_MSG_CHECKING([whether we should enable memory profiling])
           AC_ARG_ENABLE(profile,
                         [  --enable-profile        Enable memory profiling ],
                         [ if test $enableval = yes; then
                             puk_enable_profile=yes
                           else
                             puk_enable_profile=no
                           fi],
                         [ puk_enable_profile=no ])
           AC_MSG_RESULT([ ${puk_enable_profile} ])
           if test x${puk_enable_profile} = xyes; then
             AC_DEFINE([PUK_ENABLE_PROFILE], 1, [enable memory profiling])
           fi
         ])


AC_DEFUN([AC_PUK_LFQUEUES],
        [
           AC_CANONICAL_TARGET
           dnl -- external queues
           AC_MSG_CHECKING([whether we should enable external lfqueues])
           AC_ARG_ENABLE(extqueues,
                         [  --enable-extqueues      Enable all external lfqueues ],
                         [ if test $enableval = yes; then
                             puk_enable_extqueues=yes
                           else
                             puk_enable_extqueues=no
                           fi],
                         [ puk_enable_extqueues=no ])
           AC_MSG_RESULT([ ${puk_enable_extqueues} ])
           dnl -- external ConcurrentFreaks
           AC_MSG_CHECKING([whether we should enable ConcurrentFreaks queues])
           AC_ARG_ENABLE(cfqueues,
                         [  --enable-cfqueues       Enable external ConcurrentFreaks lfqueues ],
                         [ if test $enableval = yes; then
                             puk_enable_cfqueues=yes
                           else
                             puk_enable_cfqueues=no
                           fi],
                         [ puk_enable_cfqueues=${puk_enable_extqueues} ])
           AC_MSG_RESULT([ ${puk_enable_cfqueues} ])
           AC_SUBST([puk_enable_cfqueues])
           if test x${puk_enable_cfqueues} = xyes; then
             PUK_LIBS="${PUK_LIBS} -lstdc++"
             AC_DEFINE([PUK_ENABLE_CFQUEUES], 1, [enable external ConcurrencyFreaks queues])
           fi
           dnl -- external looqueue
           AC_MSG_CHECKING([whether we should enable LOO queues])
           AC_ARG_ENABLE(looqueue,
                         [  --enable-looqueue       Enable external LOO queues ],
                         [ if test $enableval = yes; then
                             puk_enable_looqueue=yes
                           else
                             puk_enable_looqueue=no
                           fi],
                         [ puk_enable_looqueue=${puk_enable_extqueues} ])
           AC_MSG_RESULT([ ${puk_enable_looqueue} ])
           AC_SUBST([puk_enable_looqueue])
           if test x${puk_enable_looqueue} = xyes; then
             PUK_LIBS="${PUK_LIBS} -lstdc++ -latomic"
             AC_DEFINE([PUK_ENABLE_LOOQUEUE], 1, [enable external LOO queues])
           fi
           dnl -- external libconcurrent
           AC_MSG_CHECKING([whether we should enable libconcurrent lfqueues])
           AC_ARG_ENABLE(libconcurrent,
                         [  --enable-libconcurrent  Enable external libconcurrent lfqueues ],
                         [ if test $enableval = yes; then
                             puk_enable_libconcurrent=yes
                           else
                             puk_enable_libconcurrent=no
                           fi],
                         [ puk_enable_libconcurrent=${puk_enable_extqueues} ])
           AC_MSG_RESULT([ ${puk_enable_libconcurrent} ])
           AC_SUBST([puk_enable_libconcurrent])
           if test x${puk_enable_libconcurrent} = xyes; then
             AC_DEFINE([PUK_ENABLE_LIBCONCURRENT], 1, [enable external libconcurrent lfqueues])
           fi
           dnl -- external wfqueue
           AC_MSG_CHECKING([whether we should enable wfqueue])
           AC_ARG_ENABLE(wfqueue,
                         [  --enable-wfqueue        Enable external wfqueue lfqueues ],
                         [ if test $enableval = yes; then
                             puk_enable_wfqueue=yes
                           else
                             puk_enable_wfqueue=no
                           fi],
                         [ puk_enable_wfqueue=${puk_enable_extqueues} ])
           AC_MSG_RESULT([ ${puk_enable_wfqueue} ])
           AC_SUBST([puk_enable_wfqueue])
           if test x${puk_enable_wfqueue} = xyes; then
             AC_DEFINE([PUK_ENABLE_WFQUEUE], 1, [enable external wfqueue lfqueues])
           fi
           dnl -- external wcq
           AC_MSG_CHECKING([whether we should enable wcq queues])
           AC_ARG_ENABLE(wcq,
                         [  --enable-wcq            Enable external wcq lfqueues ],
                         [ if test $enableval = yes; then
                             puk_enable_wcq=yes
                           else
                             puk_enable_wcq=no
                           fi],
                         [ puk_enable_wcq=${puk_enable_extqueues} ])
           AC_MSG_RESULT([ ${puk_enable_wcq} ])
           AC_SUBST([puk_enable_wcq])
           if test x${puk_enable_wcq} = xyes; then
             AC_DEFINE([PUK_ENABLE_WCQ], 1, [enable external wcq lfqueues])
           fi
           dnl -- default queue
           AC_MSG_CHECKING([for default lfqueue kind])
           AC_ARG_ENABLE(lfqueue,
                         [  --enable-lfqueue=<q>    Enable <q> as the default lfqueue kind to use; accepted values: atomic | nblfq | nblfq2 | wcq | simqueue | koganpetrank | crturn | lcrq | msqueue | crdoublelink | wfqueue | ccqueue | dsmqueue | hqueue | scq2 | scqd | ncqd | looqueue | bitnext | lineararray; default = atomic ],
                         [ puk_default_lfqueue=$enableval ],
                         [ puk_default_lfqueue=no ])
           case ${puk_default_lfqueue} in
           atomic | nblfq | nblfq2 | wcq | simqueue | koganpetrank | crturn | lcrq | msqueue | crdoublelink | wfqueue | ccqueue | dsmqueue | hqueue | osciqueue | fcqueue | scq2 | scqd | ncqd | looqueue | bitnext | lineararray)
             ;;
           wflfq) # compatibility with legacy name
             puk_default_lfqueue=nblfq
             ;;
           wflfq2)
             puk_default_lfqueue=nblfq2
             ;;
           yes|default)
             case ${target_cpu} in
               x86_64*)
                 puk_default_lfqueue=atomic
                 ;;
               aarch64*)
                 puk_default_lfqueue=atomic
                 ;;
               *)
                 puk_default_lfqueue=nblfq
                 ;;
             esac
             ;;
           no)
             ;;
           *)
             AC_MSG_ERROR([accepted values for --enable-queue= atomic | nblfq | nblfq2 | wcq | simqueue | koganpetrank | crturn | lcrq | msqueue | crdoublelink | wfqueue | ccqueue | dsmqueue | hqueue | scq2 | scqd | ncqd | looqueue | bitnext | lineararray])
             ;;
           esac
           AC_MSG_RESULT([ ${puk_default_lfqueue} ])
           if test ${puk_default_lfqueue} != "no"; then
             if test ${puk_default_lfqueue} = "nblfq2"; then
               PUK_CFLAGS="${PUK_CLAGS} -mcx16" # enable CMPXCHG16B
             fi
             puk_lfqueue_type=`echo $puk_default_lfqueue | tr [a-z] [A-Z]`
             AC_DEFINE_UNQUOTED([PUK_LFQUEUE_DEFAULT_TYPE], PUK_LFQUEUE_${puk_lfqueue_type}_TYPE, [default lfqueue type])
             AC_DEFINE_UNQUOTED([PUK_LFQUEUE_DEFAULT_KIND], "$puk_default_lfqueue", [default lfqueue kind])
           fi
        ])

dnl -- tools needed for multiple instances per process for simulation
AC_DEFUN([AC_PUK_MULT],
         [
           dnl - check for dladdr()
           AC_SEARCH_LIBS(dladdr, [dl dlcompat c c++], [HAVE_DLADDR=yes], [HAVE_DLARR=no])
           dnl - objdump
           AC_PATH_PROGS(OBJDUMP, [ ${OBJDUMP} objdump ])
           AC_ARG_VAR(OBJDUMP, [ objdump, needed for sim launcher ])
           dnl - patchelf >= 0.18
           AC_PATH_PROGS(PATCHELF, [ ${PATCHELF} patchelf ])
           AC_ARG_VAR(PATCHELF, [ NIX patchelf, need >= 0.18 for sim launcher ])
           if test x${PATCHELF} != x; then
             AC_MSG_CHECKING([whether ${PATCHELF} supports --rename-dynamic-symbols])
             out=`patchelf 2>&1 | grep rename-dynamic-symbols`
             if test "x${out}" = "x"; then
               AC_MSG_RESULT([no])
             else
               patchelf_dynamic_symbols=yes
               AC_MSG_RESULT([yes])
             fi
           fi
           dnl - enable sim
           AC_MSG_CHECKING([whether to enable multiple instances])
           if test x${HAVE_DLADDR} = xyes -a x${OBJDUMP} != x -a x${PATCHELF} != x -a x${patchelf_dynamic_symbols} = xyes; then
             puk_enable_mult=yes
             AC_DEFINE([PUK_ENABLE_MULT], [1], [whether multiple instance is available])
             AC_MSG_RESULT([yes])
           else
             puk_enable_mult=no
             AC_MSG_RESULT([no])
           fi
           AC_SUBST([puk_enable_mult])
         ])

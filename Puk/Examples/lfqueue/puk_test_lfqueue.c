/* minimalistic example to test lfqueues
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include <Padico/Puk.h>

#define LFQUEUE_SIZE 256

#define LOOPS 300

PUK_LFQUEUE_TYPE(test, int*, NULL, LFQUEUE_SIZE);


int main(int argc, char**argv)
{

  int* buf = malloc(sizeof(int) * LFQUEUE_SIZE);
  int i;
  for(i = 0; i < LFQUEUE_SIZE; i++)
    {
      buf[i] = i;
    }
  fprintf(stderr, "# init\n");
  struct test_lfqueue_s q;
  test_lfqueue_init(&q);
  for(i = 0; i < LOOPS; i++)
    {
      fprintf(stderr, "# enqueue %d = %p\n", i, &buf[i]);
      int rc = test_lfqueue_enqueue(&q, &buf[i]);
      if(rc)
        {
          fprintf(stderr, "# queue full\n");
        }
    }
  for(i = 0; i < LOOPS; i++)
    {
      fprintf(stderr, "# dequeue %d\n", i);
      int*b = test_lfqueue_dequeue(&q);
      if(b)
        {
          fprintf(stderr, "# dequeue value = %d\n", *b);
        }
      else
        {
          fprintf(stderr, "# queue empty\n");
        }
    }

  puk_profile_dump();

  return 0;
}

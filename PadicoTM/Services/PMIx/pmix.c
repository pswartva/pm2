/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <Padico/Module.h>
#include <Padico/PadicoControl.h>
#include <Padico/Topology.h>
#include <Padico/Puk.h>
#include <Padico/PacketFilter.h>

#include "PMIx.h"

#include <pmix.h>

static int padico_pmix_init(void);
static void padico_pmix_finalize(void);

PADICO_MODULE_DECLARE(PMIx, padico_pmix_init, NULL, padico_pmix_finalize,
                      "Topology", "PadicoControl");

PADICO_MODULE_ATTR(enable_compression, "PADICO_PMIX_ENABLE_COMPRESSION", "enable compression in PMIx exchanges.", bool, 0);
PADICO_MODULE_ATTR(compression_algo, "PADICO_PMIX_COMPRESSION_ALGO", "use compression algorithm in PMIx exchanges.", string, "auto");


static struct
{
  pmix_proc_t proc;
  int rank;
  int size;
  puk_instance_t compression_instance;
  struct puk_receptacle_PacketFilter_s compression_receptacle;
} padico_pmix = { .rank = -1, .size = -1, .compression_instance = NULL };

/* ********************************************************* */

static inline char*padico_pmix_strerror(int rc)
{
  switch(rc)
    {
    case PMIX_SUCCESS: return "Success"; break;
    case PMIX_ERROR: return "General Error"; break;
    case PMIX_ERR_SILENT: return "Silent error"; break;
    case PMIX_ERR_DEBUGGER_RELEASE: return "Error in debugger release"; break;
    case PMIX_ERR_PROC_RESTART: return "Fault tolerance: Error in process restart"; break;
    case PMIX_ERR_PROC_CHECKPOINT: return "Fault tolerance: Error in process checkpoint"; break;
    case PMIX_ERR_PROC_MIGRATE: return "Fault tolerance: Error in process migration"; break;
    case PMIX_ERR_PROC_ABORTED: return "Process was aborted"; break;
    case PMIX_ERR_PROC_REQUESTED_ABORT: return "Process is already requested to abort"; break;
    case PMIX_ERR_PROC_ABORTING: return "Process is being aborted"; break;
    case PMIX_ERR_SERVER_FAILED_REQUEST: return "Failed to connect to the server"; break;
    case PMIX_EXISTS: return "Requested operation would overwrite an existing value"; break;
    case PMIX_ERR_INVALID_CRED: return "Invalid security credentials"; break;
    case PMIX_ERR_HANDSHAKE_FAILED: return "Connection handshake failed"; break;
    case PMIX_ERR_READY_FOR_HANDSHAKE: return "Ready for handshake"; break;
    case PMIX_ERR_WOULD_BLOCK: return "Operation would block"; break;
    case PMIX_ERR_UNKNOWN_DATA_TYPE: return "Unknown data type"; break;
    case PMIX_ERR_PROC_ENTRY_NOT_FOUND: return "Process not found"; break;
    case PMIX_ERR_TYPE_MISMATCH: return "Invalid type"; break;
    case PMIX_ERR_UNPACK_INADEQUATE_SPACE: return "Inadequate space to unpack data"; break;
    case PMIX_ERR_UNPACK_FAILURE: return "Unpack failed"; break;
    case PMIX_ERR_PACK_FAILURE: return "Pack failed"; break;
    case PMIX_ERR_PACK_MISMATCH: return "Pack mismatch"; break;
    case PMIX_ERR_NO_PERMISSIONS: return "No permissions"; break;
    case PMIX_ERR_TIMEOUT: return "Timeout expired"; break;
    case PMIX_ERR_UNREACH: return "Unreachable"; break;
    case PMIX_ERR_IN_ERRNO: return "Error defined in errno"; break;
    case PMIX_ERR_BAD_PARAM: return "Bad parameter"; break;
    case PMIX_ERR_RESOURCE_BUSY: return "Resource busy"; break;
    case PMIX_ERR_OUT_OF_RESOURCE: return "Resource exhausted"; break;
    case PMIX_ERR_DATA_VALUE_NOT_FOUND: return "Data value not found"; break;
    case PMIX_ERR_INIT: return "Error during initialization"; break;
    case PMIX_ERR_NOMEM: return "Out of memory"; break;
    case PMIX_ERR_INVALID_ARG: return "Invalid argument"; break;
    case PMIX_ERR_INVALID_KEY: return "Invalid key"; break;
    case PMIX_ERR_INVALID_KEY_LENGTH: return "Invalid key length"; break;
    case PMIX_ERR_INVALID_VAL: return "Invalid value"; break;
    case PMIX_ERR_INVALID_VAL_LENGTH: return "Invalid value length"; break;
    case PMIX_ERR_INVALID_LENGTH: return "Invalid argument length"; break;
    case PMIX_ERR_INVALID_NUM_ARGS: return "Invalid number of arguments"; break;
    case PMIX_ERR_INVALID_ARGS: return "Invalid arguments"; break;
    case PMIX_ERR_INVALID_NUM_PARSED: return "Invalid number parsed"; break;
    case PMIX_ERR_INVALID_KEYVALP: return "Invalid key/value pair"; break;
    case PMIX_ERR_INVALID_SIZE: return "Invalid size"; break;
    case PMIX_ERR_INVALID_NAMESPACE: return "Invalid namespace"; break;
    case PMIX_ERR_SERVER_NOT_AVAIL: return "Server is not available"; break;
    case PMIX_ERR_NOT_FOUND: return "Not found"; break;
    case PMIX_ERR_NOT_SUPPORTED: return "Not supported"; break;
    case PMIX_ERR_NOT_IMPLEMENTED: return "Not implemented"; break;
    case PMIX_ERR_COMM_FAILURE: return "Communication failure"; break;
    case PMIX_ERR_UNPACK_READ_PAST_END_OF_BUFFER: return "Unpacking past the end of the buffer provided"; break;
    }
  return "unknown";
};

/* ********************************************************* */

static int padico_pmix_init(void)
{
  /* Init; get rank & job size */
  pmix_status_t rc = PMIx_Init(&padico_pmix.proc, NULL, 0);
  if(rc != PMIX_SUCCESS)
    {
      padico_fatal("pmix initialization failed- rc = %d (%s).\n",
                   rc, padico_pmix_strerror(rc));
    }
  padico_pmix.rank = padico_pmix.proc.rank;
  pmix_proc_t wildcard_proc;
  PMIX_PROC_CONSTRUCT(&wildcard_proc);
  PMIX_LOAD_PROCID(&wildcard_proc, padico_pmix.proc.nspace, PMIX_RANK_WILDCARD);
  pmix_value_t*value;
  rc = PMIx_Get(&wildcard_proc, PMIX_JOB_SIZE, NULL, 0, &value);
  if(rc != PMIX_SUCCESS)
    {
      padico_fatal("cannot get job size- rc = %d (%s).\n",
                   rc, padico_pmix_strerror(rc));
    }
  padico_pmix.size = value->data.uint32;
  PMIX_VALUE_RELEASE(value);

  /* check consistency between PMIx numbering and padico-launch numbering */
  if(padico_topo_session() != NULL)
    {
      if(padico_pmix.rank != padico_na_rank())
        {
          PMIx_Finalize(NULL, 0);
          padico_fatal("inconsistency detected, PMIX_RANK=%d, PMIx_Init rank=%d. Suspecting missing srun parameters --mpi=pmix\n",
                       padico_na_rank(), padico_pmix.rank);
        }
    }
  /* init compression */
  if(padico_module_attr_enable_compression_getvalue())
    {
      const char*algo = "LZO"; /* fallback that should be always available since we have a builtin implementation */
      const char*default_algo = padico_module_attr_compression_algo_getvalue();
      if((default_algo == NULL) || (strcmp(default_algo, "") == 0) || (strcmp(default_algo, "auto") == 0) || (strcmp(default_algo, "default") == 0))
        {
#if defined(HAVE_BZLIB_H)
          algo = "BZ2";
#elif defined(HAVE_ZLIB_H)
          algo = "ZIP";
#elif defined(HAVE_LZ4_H)
          algo = "LZ4";
#endif
        }
      else
        {
          algo = default_algo;
        }
      padico_string_t s_compression = padico_string_new();
      padico_string_printf(s_compression, "PacketFilter-%s", algo);
      puk_component_t component = puk_component_resolve(padico_string_get(s_compression));
      if(component == NULL)
        {
          padico_fatal("cannot load component %s; compression method = %s\n", padico_string_get(s_compression), algo);
        }
      puk_component_t component2 = puk_compositize(component, "pmix-compression"); /* ensure component has a context */
      padico_string_delete(s_compression);
      padico_pmix.compression_instance = puk_component_instantiate(component2);
      puk_instance_indirect_PacketFilter(padico_pmix.compression_instance, NULL, &padico_pmix.compression_receptacle);
    }

  const char*version = PMIx_Get_version();
  padico_out(puk_verbose_notice, "using version = %s; process rank = %d; job size = %d; compression = %s\n",
             version, padico_pmix.rank, padico_pmix.size,
             padico_pmix.compression_instance ? padico_pmix.compression_instance->component->name : "(none)");
  return 0;
}

static void padico_pmix_finalize(void)
{
  if(PMIx_Initialized())
    PMIx_Finalize(NULL, 0);
}

int padico_pmix_getrank(void)
{
  return padico_pmix.rank;
}

int padico_pmix_getsize(void)
{
  return padico_pmix.size;
}

void padico_pmix_barrier(void)
{
  pmix_info_t*info;
  PMIX_INFO_CREATE(info, 1);
  int flag = 0;
  PMIX_INFO_LOAD(info, PMIX_COLLECT_DATA, &flag, PMIX_BOOL);
  pmix_status_t rc = PMIx_Fence(NULL, 0, info, 1);
  if(rc != PMIX_SUCCESS)
    {
      padico_fatal("error in PMIx_Fence- rc = %d (%s).\n",
                   rc, padico_pmix_strerror(rc));
    }
  PMIX_INFO_FREE(info, 1);
}

void padico_pmix_abort(const char*msg)
{
  PMIx_Abort(1, msg, NULL, 0);
}

/* ********************************************************* */

void padico_pmix_kvs_fence(void)
{
  pmix_info_t*info;
  PMIX_INFO_CREATE(info, 1);
  int flag = 1;
  PMIX_INFO_LOAD(info, PMIX_COLLECT_DATA, &flag, PMIX_BOOL);
  pmix_status_t rc = PMIx_Fence(NULL, 0, info, 1);
  if(rc != PMIX_SUCCESS)
    {
      padico_fatal("error in PMIx_Fence- rc = %d (%s).\n",
                   rc, padico_pmix_strerror(rc));
    }
  PMIX_INFO_FREE(info, 1);
}

int padico_pmix_kvs_publish(const char*key, const char*value)
{
  if(padico_pmix.compression_instance != NULL)
    {
      const size_t size = strlen(value);
      void*encoded_ptr = NULL;
      size_t encoded_size = 0;
      (*padico_pmix.compression_receptacle.driver->pf_encode)(padico_pmix.compression_receptacle._status,
                                                              value, size, &encoded_ptr, &encoded_size);
      size_t pkt_size = sizeof(uint64_t) + encoded_size;
      void*pkt_ptr = padico_malloc(pkt_size);
      uint64_t header = size;
      memcpy(pkt_ptr, &header, sizeof(header));
      memcpy(pkt_ptr + sizeof(uint64_t), encoded_ptr, encoded_size);
      pmix_value_t pmix_value;
      pmix_value.type = PMIX_BYTE_OBJECT;
      pmix_value.data.bo.bytes = pkt_ptr;
      pmix_value.data.bo.size = pkt_size;
      pmix_status_t rc = PMIx_Put(PMIX_GLOBAL, key, &pmix_value);
      if(rc != PMIX_SUCCESS)
        {
          padico_fatal("error in PMIx_Put- rc = %d (%s).\n",
                       rc, padico_pmix_strerror(rc));
        }
      rc = PMIx_Commit();
      if(rc != PMIX_SUCCESS)
        {
          padico_fatal("error in PMIx_Commit- rc = %d (%s).\n",
                       rc, padico_pmix_strerror(rc));
        }
      padico_free(encoded_ptr);
      padico_free(pkt_ptr);
    }
  else
    {
      pmix_value_t pmix_value;
      pmix_value.type = PMIX_STRING;
      pmix_value.data.string = (char*)value;
      pmix_status_t rc = PMIx_Put(PMIX_GLOBAL, key, &pmix_value);
      if(rc != PMIX_SUCCESS)
        {
          padico_fatal("error in PMIx_Put- rc = %d (%s).\n",
                       rc, padico_pmix_strerror(rc));
        }
      rc = PMIx_Commit();
      if(rc != PMIX_SUCCESS)
        {
          padico_fatal("error in PMIx_Commit- rc = %d (%s).\n",
                       rc, padico_pmix_strerror(rc));
        }
    }
  return 0;
}

char*padico_pmix_kvs_lookup(const char*key, int from)
{
  if(padico_pmix.compression_instance != NULL)
    {
      pmix_proc_t proc;
      PMIX_PROC_CONSTRUCT(&proc);
      PMIX_LOAD_PROCID(&proc, padico_pmix.proc.nspace, from);
      pmix_info_t*info;
      PMIX_INFO_CREATE(info, 1);
      pmix_scope_t scope = PMIX_GLOBAL;
      PMIX_INFO_LOAD(info, PMIX_DATA_SCOPE, &scope, PMIX_SCOPE);
      pmix_value_t*val = NULL;
      pmix_status_t rc = PMIx_Get(&proc, key, NULL, 0, &val);
      if(rc != PMIX_SUCCESS)
        {
          padico_fatal("error in PMIx_Get- key = %s; rc = %d (%s).\n",
                       key, rc, padico_pmix_strerror(rc));
        }
      void*pkt_ptr = val->data.bo.bytes;
      size_t pkt_size = val->data.bo.size;
      void*encoded_ptr = pkt_ptr + sizeof(uint64_t);
      size_t encoded_size = pkt_size - sizeof(uint64_t);
      size_t header;
      memcpy(&header, pkt_ptr, sizeof(header));
      size_t value_len = header;
      char*value = padico_malloc(value_len);
      (*padico_pmix.compression_receptacle.driver->pf_decode)(padico_pmix.compression_receptacle._status,
                                                              encoded_ptr, encoded_size, value, &value_len);
      PMIX_VALUE_RELEASE(val);
      PMIX_INFO_FREE(info, 1);
      return value;
    }
  else
    {
      pmix_proc_t proc;
      PMIX_PROC_CONSTRUCT(&proc);
      PMIX_LOAD_PROCID(&proc, padico_pmix.proc.nspace, from);
      pmix_info_t*info;
      PMIX_INFO_CREATE(info, 1);
      pmix_scope_t scope = PMIX_GLOBAL;
      PMIX_INFO_LOAD(info, PMIX_DATA_SCOPE, &scope, PMIX_SCOPE);
      pmix_value_t pmix_value, *val = &pmix_value;
      pmix_status_t rc = PMIx_Get(&proc, key, NULL, 0, &val);
      if(rc != PMIX_SUCCESS)
        {
          padico_fatal("error in PMIx_Get- key = %s; rc = %d (%s).\n",
                       key, rc, padico_pmix_strerror(rc));
        }
      char*value = padico_strdup(val->data.string);
      PMIX_VALUE_RELEASE(val);
      PMIX_INFO_FREE(info, 1);
      return value;
    }
}

/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Create internal description of Grid5000 topology
 * @author Sebastien Barascou
 */

#include <Padico/Puk.h>
#include <Padico/Module.h>
#include <Padico/PadicoTM.h>
#include <Padico/Topology.h>

#include <string.h>
#include <ctype.h>
#include <net/if_arp.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <signal.h>
#include <unistd.h>
#include <jansson.h>
#include <curl/curl.h>

#define SSHPORT (3443 + getpid())
#define APIADDR "https://localhost"
#define APIVERSION "2.0"
#define REQUEST_RETRY_MAX 10
#define URL_SIZE 512
#define SSH_CONNECT_TIMEOUT 1500

static int g5k_module_init(void);
static void g5k_module_finalize(void);

PADICO_MODULE_DECLARE(g5k, g5k_module_init, NULL, g5k_module_finalize);

static int ssh_pid = -1;
static puk_hashtable_t  netmask_table;

struct write_result
{
  char *data;
  size_t pos;
};

static int g5k_netmask_eq(const void*site1, const void*site2)
{
  int site1_len = strlen(site1);
  int site2_len = strlen(site2);
  int min_len = (site1_len > site2_len)?site2_len:site1_len;
  return  (site1_len == site2_len) && (strncmp(site1, site2, min_len) == 0);
}

static uint32_t g5k_netmask_hash(const void*site)
{
  return puk_hash_default(site, strlen(site));
}

/** init and fill netmask hashtable, with the site and corresponding cidr netmask
* this information is not available in Grid5000 API
* if Grid5000's admin change network configuration, netmask can become obsolete
*/
static void g5k_init_netmask()
{
  netmask_table  = puk_hashtable_new(&g5k_netmask_hash, &g5k_netmask_eq);
  puk_hashtable_insert(netmask_table, "bordeaux", "20");   //172.16.0.0
  puk_hashtable_insert(netmask_table, "grenoble", "24");   //172.16.16.0 || 172.16.18.0
  puk_hashtable_insert(netmask_table, "lille", "20");      //192.168.159.0
  puk_hashtable_insert(netmask_table, "luxembourg", "20"); //172.16.176.0
  puk_hashtable_insert(netmask_table, "lyon", "21");       //10.69.0.0
  puk_hashtable_insert(netmask_table, "nancy", "20");      //172.16.64.0
  puk_hashtable_insert(netmask_table, "orsay", "20");      //172.16.80.0
  puk_hashtable_insert(netmask_table, "reims", "20");      //172.16.160.0
  puk_hashtable_insert(netmask_table, "rennes", "20");     //172.16.96.0
  puk_hashtable_insert(netmask_table, "sophia", "22");     //138.96.20.0
  puk_hashtable_insert(netmask_table, "toulouse", "20");   //172.16.112.0
}

static int g5k_get_netmask(const char *site, struct in_addr *netmask_addr)
{
  char *cidr_str = puk_hashtable_lookup(netmask_table, site);
  if(cidr_str)
    {
      int cidr = atoi(cidr_str);
      assert(cidr > 0 && cidr < 32);
      uint32_t netmask = (0xffffffff << (32 - cidr));
      netmask = htonl(netmask);
      netmask_addr->s_addr = netmask;
      return cidr;
    }
  return -1;
}

/** function used by curl to write the server response in a buffer
 */
static size_t write_response(void *contents, size_t size, size_t nmemb, void *userp)
{
  size_t realsize = size * nmemb;
  struct write_result *mem = (struct write_result *)userp;
  mem->data = padico_realloc(mem->data, mem->pos + realsize + 1);
  if (mem->data == NULL)
    {
      padico_print("not enough memory (realloc returned NULL)\n");
      exit(EXIT_FAILURE);
    }
  memcpy(mem->data + mem->pos, contents, realsize);
  mem->pos += realsize;
  return realsize;
}

static char *request(const char *url)
{
  padico_print("request(%s)\n", url);
  CURL *curl;
  CURLcode status;
  long code;
  int retry_cpt = 0;
  curl = curl_easy_init();
  if(!curl)
    return NULL;
  struct write_result write_result = {
    .data = padico_malloc(1),
    .pos = 0,
  };
  struct curl_slist *requestHeader = NULL;
  requestHeader = curl_slist_append(requestHeader, "Accept: application/json");
  curl_easy_setopt(curl, CURLOPT_URL, url);
  curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_response);
  curl_easy_setopt(curl, CURLOPT_WRITEDATA, &write_result);
  curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
  curl_easy_setopt(curl, CURLOPT_HTTPHEADER, requestHeader);

 retry:
  if(retry_cpt > 0)
    {
       write_result.pos = 0;
       padico_tm_msleep(200);
    }
  status = curl_easy_perform(curl);
  if(status != 0)
    {
      fprintf(stderr, "curl error: %s\n", curl_easy_strerror(status));
      if(retry_cpt < REQUEST_RETRY_MAX)
        {
          retry_cpt++;
          goto retry;
        }
      padico_free(write_result.data);
      return NULL;
    }
  curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &code);
  if(code != 200)
    {
      padico_print("server responded with code %ld\n", code);
      if(retry_cpt < REQUEST_RETRY_MAX)
        {
          retry_cpt++;
          goto retry;
        }
      padico_free(write_result.data);
      return NULL;
    }
  curl_slist_free_all(requestHeader);
  curl_easy_cleanup(curl);
  curl_global_cleanup();
  write_result.data[write_result.pos] = '\0';
  return write_result.data;
}

static json_t* get_href_by_title(const json_t *array, const char* title_str)
{
  json_t *cell, *title, *href;
  if (!json_is_array(array))
    return NULL;
  int i = 0;
  for(i = 0; i < json_array_size(array); i++)
    {
      cell = json_array_get(array, i);
      if(json_is_object(cell))
        {
          title = json_object_get(cell, "title");
          if(json_is_string(title))
            {
              if(strncmp(title_str, json_string_value(title), strlen(title_str)) == 0)
                {
                  href = json_object_get(cell, "href");
                  if(json_is_string(href))
                    {
                      json_incref(href);
                      return href;
                    }
                }
            }
        }
    }
  return NULL;
}

static char *mac_address(const char *mac)
{
  int mac_size = strlen(mac);
  char *tmp = padico_malloc(mac_size);
  int i;
  int j = 0;
  for(i = 0; i < mac_size; i++)
    {
      if(mac[i] != ':' && mac[i] != '-' )
        {
          tmp[j] = toupper(mac[i]);
          j++;
        }
    }
  tmp[j] = 0;
  return tmp;
}

static padico_topo_network_t g5k_create_network(const struct in_addr *network_addr, int cidr)
{
  padico_string_t network_name = padico_string_new();
  padico_string_printf(network_name, "inet-[%s/%d]", inet_ntoa(*network_addr), cidr);
  padico_topo_network_t network = padico_topo_network_getbyid(padico_string_get(network_name));
  if(network == NULL)
    {
      network = padico_topo_network_create(padico_string_get(network_name), PADICO_TOPO_SCOPE_HOST, sizeof(struct in_addr), AF_INET);
    }
  return network;
}

static void g5k_compute_networks(const json_t *networks, const padico_topo_host_t host, const char* site)
{
  if(json_is_array(networks))
    {
      int j = 0;
      for(j = 0; j < json_array_size(networks); j++)
        {
          json_t *network_cell = json_array_get(networks, j);
          json_t *ip = json_object_get(network_cell, "ip");
          if(json_is_string(ip))
            {
              struct in_addr host_addr;
              if(inet_pton(AF_INET, json_string_value(ip), &host_addr)==1)
                {
                  json_t *device = json_object_get(network_cell, "device");
                  json_t *mac = json_object_get(network_cell, "mac");
                  json_t *network_address = json_object_get(network_cell, "network_address");
                  const char *network_address_str = NULL;
                  padico_topo_hwaddr_t hwaddr = NULL;
                  if(json_is_string(network_address))
                    {
                      network_address_str = json_string_value(network_address);
                      padico_topo_host_add_name(host, network_address_str, 0);
                    }
                  if(json_is_string(mac) && json_is_string(device))
                    {
                      char *mac_str = mac_address(json_string_value(mac));
                      size_t addr_len = strlen(mac_str);
                      char *addr = puk_hex_decode(mac_str, &addr_len, NULL);
                      hwaddr = padico_topo_hwaddr_create(json_string_value(device), 0, ARPHRD_ETHER,
                                                         addr_len, addr);
                      padico_free(mac_str);
                      padico_free(addr);
                      padico_topo_hwaddr_add(host, &hwaddr);
                    }
                  struct in_addr netmask_addr;
                  struct in_addr network_addr;
                  int cidr = g5k_get_netmask(site, &netmask_addr);
                  if(cidr > 0)
                    {
                      network_addr.s_addr = netmask_addr.s_addr & host_addr.s_addr;
                      padico_topo_network_t network = g5k_create_network(&network_addr, cidr);
                      padico_topo_addr_visibility_t v = padico_topo_inet_visibility(host_addr.s_addr);
                      if(v == PADICO_TOPO_ADDR_VISIBILITY_PUBLIC)
                        network = padico_topo_network_getbyid("inet");
                      padico_topo_binding_t priv_binding = padico_topo_host_bind(host, network, &host_addr);
                      if(hwaddr)
                        padico_topo_binding_add_hwaddr(priv_binding, hwaddr);
                      if(network_address_str)
                        padico_topo_binding_add_hostname(priv_binding, network_address_str);
                      if(v == PADICO_TOPO_ADDR_VISIBILITY_PUBLIC)
                        {
                          padico_topo_binding_add_filter(priv_binding, PADICO_TOPO_FILTER_BLOCK_IN,  PORT_ANY, IPPROTO_TCP);
                          padico_topo_binding_add_filter(priv_binding, PADICO_TOPO_FILTER_BLOCK_OUT, PORT_ANY, IPPROTO_TCP);
                          padico_topo_binding_add_filter(priv_binding, PADICO_TOPO_FILTER_BLOCK_IN,  PORT_ANY, IPPROTO_UDP);
                          padico_topo_binding_add_filter(priv_binding, PADICO_TOPO_FILTER_BLOCK_OUT, PORT_ANY, IPPROTO_UDP);
                        }
                    }
                }
            }
        }
    }
}

static void g5k_compute_nodes(const json_t *nodes, const char* site)
{
  if(json_is_array(nodes))
    {
      int i = 0;
      for(i = 0; i < json_array_size(nodes); i++)
        {
          json_t *cell = json_array_get(nodes, i);
          json_t *uid = json_object_get(cell, "uid");
          if(uid && json_is_string(uid))
            {
              padico_topo_host_t host = padico_topo_host_create(json_string_value(uid));
              json_t *networks = json_object_get(cell, "network_components");
              g5k_compute_networks(networks, host, site);
            }
        }
    }
}

static void g5k_describe_topology()
{
  int i = 0;
  char *request_url = padico_malloc(URL_SIZE);
  char *sites_str;
  json_t *root;
  json_error_t error;
  snprintf(request_url, URL_SIZE, "%s:%d/%s/grid5000/sites", APIADDR, SSHPORT, APIVERSION);
  sites_str = request(request_url);
  if(sites_str)
    {
      root = json_loads(sites_str, 0, &error);
      json_t *items = json_object_get(root, "items");
      for(i=0; i<json_array_size(items); i++)
        {
          json_t *site = json_array_get(items, i);
          json_t *uid = json_object_get(site, "uid");
          json_t *links = json_object_get(site, "links");
          json_t *href_clusters = get_href_by_title(links, "clusters");
          snprintf(request_url, URL_SIZE, "%s:%d/%s", APIADDR, SSHPORT, json_string_value(href_clusters));
          char * clusters_str = request(request_url);
          json_decref(href_clusters);
          if(clusters_str)
            {
              json_t *clusters_root = json_loads(clusters_str, 0, &error);
              padico_free(clusters_str);
              json_t *clusters_items = json_object_get(clusters_root, "items");
              int j = 0;
              for(j = 0; j<json_array_size(clusters_items); j++)
                {
                  json_t *clusters_cell = json_array_get(clusters_items, j);
                  json_t *clusters_links = json_object_get(clusters_cell, "links");
                  json_t *href_nodes = get_href_by_title(clusters_links, "nodes");
                  snprintf(request_url, URL_SIZE, "%s:%d/%s", APIADDR, SSHPORT, json_string_value(href_nodes));
                  char *nodes_str = request(request_url);
                  if(nodes_str)
                    {
                      json_decref(href_nodes);
                      json_t *nodes_root = json_loads(nodes_str, 0, &error);
                      padico_free(nodes_str);
                      json_t *nodes_items = json_object_get(nodes_root, "items");
                      g5k_compute_nodes(nodes_items, json_string_value(uid));
                      json_decref(nodes_root);
                    }
                }
              json_decref(clusters_root);
            }
        }
      padico_free(sites_str);
      json_decref(root);
    }
  padico_free(request_url);
}

static int g5k_connect_wait_ssh()
{
  int sock = socket(AF_INET, SOCK_STREAM, 0);
   if (sock == -1) {
     return 0;
   }

   struct sockaddr_in sin;
   sin.sin_family = AF_INET;
   sin.sin_port = htons(SSHPORT);
   sin.sin_addr.s_addr = (uint32_t)htonl(0x7F000001);
   struct timeval nowtime;
   struct timeval starttime;
   gettimeofday(&nowtime, NULL);
   starttime = nowtime;

   while (((nowtime.tv_sec * 1000) + (nowtime.tv_usec / 1000)) -
          ((starttime.tv_sec * 1000) + (starttime.tv_usec / 1000)) < SSH_CONNECT_TIMEOUT)
     {
       if (connect(sock, (struct sockaddr*)(&sin), sizeof(struct sockaddr_in)) == 0)
         {
           close(sock);
           return 1;
         }
       gettimeofday(&nowtime, NULL);
       padico_tm_msleep(200);
     }
   close(sock);
   return 0;
}

static int g5k_connect()
{
  int access_index = 0;
  const char *g5k_user_access = padico_getattr("G5K_ACCESS");
  const char *g5k_default_access[] =
    {
      "access.grid5000.fr",
      "access-south.grid5000.fr",
      "access-north.grid5000.fr"
    };
  const int g5k_default_access_size = sizeof(g5k_default_access) / sizeof(char *);
  char g5k_api_ssh_tun[URL_SIZE];
  snprintf(g5k_api_ssh_tun, URL_SIZE, "%d:api.grid5000.fr:443", SSHPORT);

  const char *argv[] =
    {
      "/usr/bin/ssh",
      "-o",
      "StrictHostKeyChecking=no",
      "-NL",
      g5k_api_ssh_tun,
      (g5k_user_access)?g5k_user_access:g5k_default_access[access_index],
      NULL
    };
  while(access_index < g5k_default_access_size)
    {
      puk_spinlock_acquire();
      ssh_pid = fork();
      puk_spinlock_release();
      if(ssh_pid == -1)
        {
          padico_warning("fork() failed!\n");
        }
      else if(ssh_pid == 0)
        {
          int rc = -1, err = -1;
          putenv("LD_LIBRARY_PATH=");
          puk_abi_real_errno() = 0;
          rc = PUK_ABI_PROC_REAL(execv)("/usr/bin/ssh", (char**)argv);
          err = puk_abi_real_errno();
          puk_spinlock_release();
          padico_warning("exec() failed for ssh- rc=%d; errno=%d (%s).\n", rc, err, strerror(err));
          return 0;
        }
      else
        {
          if(g5k_connect_wait_ssh())
            {
              return 1;
            }
          else
            {
              kill(ssh_pid, SIGTERM);
              ssh_pid = -1;
              padico_print("%s is unreachable with ssh\n",argv[5]);
              if(argv[5] != g5k_user_access)
                access_index++;
              argv[5] = g5k_default_access[access_index];
            }
        }
    }
  return 0;
}


static int g5k_module_init(void)
{
  if (g5k_connect())
    {
      g5k_init_netmask();
      g5k_describe_topology();
      if(ssh_pid > 0)
        {
          puk_spinlock_acquire();
          kill(ssh_pid, SIGTERM);
          puk_spinlock_release();
        }
    }
  return 0;
}

static void g5k_module_finalize(void)
{
  puk_hashtable_delete(netmask_table, NULL);
}

/** @file Red-Black Trees */

#ifndef PUK_TREES_H
#define PUK_TREES_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>


/* ** red-black tree *************************************** */

enum puk_rbtree_color_e { PUK_RBTREE_RED, PUK_RBTREE_BLACK };

/** a node in a red-black tree */
struct puk_rbtree_node_s
{
  int data;
  enum puk_rbtree_color_e color;
  struct puk_rbtree_node_s*parent;
  struct puk_rbtree_node_s*right;
  struct puk_rbtree_node_s*left;
};

/** a red-black tree */
struct puk_rbtree_s
{
  struct puk_rbtree_node_s*root; /**< global root for the entire tree */
  struct puk_rbtree_node_s*NIL;  /**< special node that serves as NIL value */
};


void puk_rbtree_show(struct puk_rbtree_s*t);

void puk_rbtree_insert(struct puk_rbtree_s*t, int data);

void puk_rbtree_traversal(struct puk_rbtree_s*tree);

void puk_rbtree_delete(struct puk_rbtree_s*t, struct puk_rbtree_node_s*z);

struct puk_rbtree_node_s*puk_rbtree_lookup(struct puk_rbtree_s*t, int data);

void puk_rbtree_init(struct puk_rbtree_s*t);

int puk_rbtree_height(struct puk_rbtree_s*t);



/* ** interval tree **************************************** */

enum puk_itree_color_e { PUK_ITREE_RED, PUK_ITREE_BLACK };

/** a node in an interval tree */
struct puk_itree_node_s
{
  uintptr_t base;                  /**< base of interval stored in this node */
  size_t len;                      /**< length of interval stored in this node */
  uintptr_t max;                   /**< max span of intervals in right subtree */
  enum puk_itree_color_e color;    /**< color for red-black tree */
  struct puk_itree_node_s*parent;
  struct puk_itree_node_s*right;
  struct puk_itree_node_s*left;
};

/** an interval tree: a red-black tree augmented to store intervals,
 * using algorithms from Cormen, Leiserson, Rivest. */
struct puk_itree_s
{
  struct puk_itree_node_s*root; /**< global root for the entire tree */
  struct puk_itree_node_s*NIL;  /**< special node that serves as NIL value */
  struct puk_itree_node_s nil;  /**< preallocated storage for NIL */
};

void puk_itree_init(struct puk_itree_s*t);

void puk_itree_destroy(struct puk_itree_s*t);

void puk_itree_show(struct puk_itree_s*t);

struct puk_itree_node_s*puk_itree_lookup(struct puk_itree_s*t, uintptr_t base);

/** find an exact matching with the same base & len */
struct puk_itree_node_s*puk_itree_exact_lookup(struct puk_itree_s*t, uintptr_t base, size_t len);

/** find an interval that intersect the given interval */
struct puk_itree_node_s*puk_itree_interval_lookup(struct puk_itree_s*t, uintptr_t base, size_t len);

void puk_itree_insert(struct puk_itree_s*t, struct puk_itree_node_s*z, uintptr_t base, size_t len);

void puk_itree_traversal(struct puk_itree_s*tree);

void puk_itree_delete(struct puk_itree_s*t, struct puk_itree_node_s*z);

int puk_itree_height(struct puk_itree_s*t);

int puk_itree_count(struct puk_itree_s*t);

#endif /* PUK_TREES_H */

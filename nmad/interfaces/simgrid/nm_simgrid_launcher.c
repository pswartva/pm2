/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

/** @file
 * Launcher for simgrid.
 */

#include <nm_public.h>
#include <nm_private.h>
#include <nm_launcher.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <Padico/Puk.h>
#include <Padico/Module.h>

#ifdef PUK_ENABLE_MULT

#include <Padico/Puk-mult.h>

/* ********************************************************* */

static void*nm_simgrid_launcher_instantiate(puk_instance_t i, puk_context_t c);
static void nm_simgrid_launcher_destroy(void*_status);

static const struct puk_component_driver_s nm_simgrid_launcher_component =
  {
    .instantiate = &nm_simgrid_launcher_instantiate,
    .destroy     = &nm_simgrid_launcher_destroy
  };

static void nm_simgrid_launcher_init(void*_status, int*argc, char**argv, const char*group_name);
static void nm_simgrid_launcher_barrier(void*_status);
static void nm_simgrid_launcher_get_gates(void*_status, nm_gate_t*gates);
static void nm_simgrid_launcher_abort(void*_status, int rc);

static const struct nm_launcher_driver_s nm_simgrid_launcher_driver =
  {
    .init         = &nm_simgrid_launcher_init,
    .barrier      = &nm_simgrid_launcher_barrier,
    .get_gates    = &nm_simgrid_launcher_get_gates,
    .abort        = &nm_simgrid_launcher_abort
  };

/* ********************************************************* */

PADICO_MODULE_COMPONENT(NewMad_Launcher_simgrid,
  puk_component_declare("NewMad_Launcher_simgrid",
                        puk_component_provides("PadicoComponent", "component",  &nm_simgrid_launcher_component),
                        puk_component_provides("NewMad_Launcher", "launcher", &nm_simgrid_launcher_driver ))
                        );

/* ********************************************************* */

struct nm_simgrid_launcher_status_s
{
  const char*local_url;
  struct nm_launcher_gates_s gates;
};

static void*nm_simgrid_launcher_instantiate(puk_instance_t i, puk_context_t c)
{
  struct nm_simgrid_launcher_status_s*status = padico_malloc(sizeof(struct nm_simgrid_launcher_status_s));
  const int size = puk_mult_getsize();
  const int rank = puk_mult_getnum();
  nm_launcher_gates_init(&status->gates, size, rank);
  return status;
}

static void nm_simgrid_launcher_destroy(void*_status)
{
  struct nm_simgrid_launcher_status_s*status = _status;
  nm_launcher_gates_destroy(&status->gates);
  padico_free(status);
}

static void nm_simgrid_launcher_get_gates(void*_status, nm_gate_t *gates)
{
  struct nm_simgrid_launcher_status_s*status = _status;
  int i;
  for(i = 0; i < status->gates.size; i++)
    {
      gates[i] = status->gates.p_gates[i].p_gate;
    }
}

static void nm_simgrid_launcher_abort(void*_status, int rc)
{
  exit(rc);
}

static void nm_simgrid_launcher_barrier(void*_status)
{
  puk_mult_barrier();
}

void nm_simgrid_launcher_init(void*_status, int*argc, char**argv, const char*group_name)
{
  struct nm_simgrid_launcher_status_s*status = _status;

  const int size = status->gates.size;
  const int rank = status->gates.rank;
  const struct nm_launcher_info_s info = { .size = size, .rank = rank, .wide_url_support = 1 };
  nm_launcher_set_info(&info);

  /* ** init drivers */
  int rc = nm_launcher_get_url(&status->local_url);
  if(rc != NM_ESUCCESS)
    {
      NM_FATAL("nm_launcher_get_url()- rc = %d\n", rc);
    }
  NM_DISPF("nm_launcher_get_url done- url = %s; size = %d; rank = %d\n",
           status->local_url, size, rank);

  /* ** publish local url */
  padico_string_t s_key = padico_string_new();
  padico_string_printf(s_key, "nm_launcher_url_%d", rank);
  puk_mult_publish(padico_string_get(s_key), status->local_url);
  puk_mult_barrier();

   /* ** resolve remote urls */
  int i;
  for(i = 0; i < size; i++)
    {
      if(i == rank)
        {
          status->gates.p_gates[i].p_url = padico_strdup(status->local_url);
        }
      else
        {
          padico_string_printf(s_key, "nm_launcher_url_%d", i);
          status->gates.p_gates[i].p_url = puk_mult_lookup(padico_string_get(s_key));
          assert(strcmp(status->gates.p_gates[i].p_url, status->local_url) != 0);
        }
    }

  /* connect gates */
  nm_launcher_gates_connect(&status->gates);
  padico_string_delete(s_key);

}

#endif /* PUK_ENABLE_MULT */

/*
 * NewMadeleine
 * Copyright (C) 2006-2021 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>

#include "../common/nm_examples_helper.h"

#include <Padico/Puk.h>

int main(int argc, char**argv)
{
  nm_examples_init(&argc, argv);

  nm_comm_t p_commw = nm_comm_world("nm_coll_comm");
  nm_comm_t p_comms = nm_comm_self("nm_coll_comm");

  printf("comm world size = %d; rank = %d\n", nm_comm_size(p_commw), nm_comm_rank(p_commw));
  nm_session_t p_sessionw = nm_comm_get_session(p_commw);
  printf("comm world session = %p\n", p_sessionw);
  printf("checking consistency of communicator session\n");
  if(nm_comm_get_by_session(p_sessionw) != p_commw)
    {
      NM_FATAL("inconsistency detected in communicator session.\n");
    }

  printf("comm self size = %d; rank = %d\n", nm_comm_size(p_comms), nm_comm_rank(p_comms));
  nm_session_t p_sessions = nm_comm_get_session(p_comms);
  printf("comm self session = %p\n", p_sessions);
  printf("checking consistency of communicator session\n");
  if(nm_comm_get_by_session(p_sessions) != p_comms)
    {
      NM_FATAL("inconsistency detected in communicator session.\n");
    }

  /* check session without communicator */
  printf("checking consistency of session without communicator\n");
  if(nm_comm_get_by_session(p_session) != NULL)
    {
      NM_FATAL("inconsistency detected in communicator session.\n");
    }

  nm_comm_destroy(p_comms);
  nm_comm_destroy(p_commw);

  printf("checking consistency of session for destroyed communicator\n");
  if(nm_comm_get_by_session(p_sessions) != NULL)
    {
      NM_FATAL("inconsistency detected in communicator session.\n");
    }

  nm_examples_exit();
  return 0;
}

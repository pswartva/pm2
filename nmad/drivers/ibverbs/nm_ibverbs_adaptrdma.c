/*
 * NewMadeleine
 * Copyright (C) 2006-2023 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include "nm_ibverbs.h"

#include <Padico/Module.h>

/* *** method: 'adaptrdma' ********************************* */

static const int nm_ibverbs_adaptrdma_block_granularity = 4096;
static const int nm_ibverbs_adaptrdma_step_base         = 16  * 1024;
static const int nm_ibverbs_adaptrdma_step_overrun      = 16  * 1024;

#define NM_IBVERBS_ADAPTRDMA_BUFSIZE (16 * 1024 * 1024)
#define NM_IBVERBS_ADAPTRDMA_HDRSIZE (sizeof(struct nm_ibverbs_adaptrdma_header_s))

#define NM_IBVERBS_ADAPTRDMA_FLAG_REGULAR      0x01
#define NM_IBVERBS_ADAPTRDMA_FLAG_BLOCKSIZE    0x02

/** on the wire header of minidriver 'adaptrdma' */
struct nm_ibverbs_adaptrdma_header_s
{
  uint32_t checksum;
  volatile uint32_t offset;
  volatile uint32_t busy; /* 'busy' has to be the last field in the struct */
} __attribute__((packed));

struct nm_ibverbs_adaptrdma_context_s
{
  struct nm_ibverbs_context_s*p_ibverbs_context;
};

/** Connection state for tracks sending with adaptive RDMA super-pipeline */
struct nm_ibverbs_adaptrdma_s
{
  struct ibv_mr*mr;
  struct nm_ibverbs_segment_s seg; /**< remote segment */
  struct nm_ibverbs_cnx_s*p_cnx;
  puk_context_t p_context;
  struct
  {
    char sbuf[NM_IBVERBS_ADAPTRDMA_BUFSIZE];
    char rbuf[NM_IBVERBS_ADAPTRDMA_BUFSIZE];
  } buffer;

  struct
  {
    const char*message;
    int todo;
    int done;
    void*rbuf;
    void*sbuf;
    int size_guard;
  } send;

  struct
  {
    char*message;
    int size;
    void*rbuf;
    int done;
    int block_size;
  } recv;
};

static void nm_ibverbs_adaptrdma_getprops(puk_context_t context, struct nm_minidriver_properties_s*p_props);
static void nm_ibverbs_adaptrdma_init(puk_context_t context, const void**p_url, size_t*p_url_size);
static void nm_ibverbs_adaptrdma_close(puk_context_t p_context);
static void nm_ibverbs_adaptrdma_connect(void*_status, const void*remote_url, size_t url_size);
static int  nm_ibverbs_adaptrdma_send_iov_post(void*_status, const struct iovec*v, int n);
static int  nm_ibverbs_adaptrdma_send_poll(void*_status);
static int  nm_ibverbs_adaptrdma_recv_init(void*_status, struct iovec*v, int n);
static int  nm_ibverbs_adaptrdma_poll_one(void*_status);

static const struct nm_minidriver_iface_s nm_ibverbs_adaptrdma_minidriver =
  {
    .getprops       = &nm_ibverbs_adaptrdma_getprops,
    .init           = &nm_ibverbs_adaptrdma_init,
    .close          = &nm_ibverbs_adaptrdma_close,
    .connect        = &nm_ibverbs_adaptrdma_connect,
    .send_iov_post  = &nm_ibverbs_adaptrdma_send_iov_post,
    .send_poll      = &nm_ibverbs_adaptrdma_send_poll,
    .recv_iov_post  = &nm_ibverbs_adaptrdma_recv_init,
    .recv_poll_one  = &nm_ibverbs_adaptrdma_poll_one,
    .recv_cancel    = NULL
  };

static void*nm_ibverbs_adaptrdma_instantiate(puk_instance_t instance, puk_context_t context);
static void nm_ibverbs_adaptrdma_destroy(void*);

static const struct puk_component_driver_s nm_ibverbs_adaptrdma_component =
  {
    .instantiate    = &nm_ibverbs_adaptrdma_instantiate,
    .destroy        = &nm_ibverbs_adaptrdma_destroy
  };


/* ********************************************************* */

PADICO_MODULE_COMPONENT(NewMad_ibverbs_adaptrdma,
  puk_component_declare("NewMad_ibverbs_adaptrdma",
                        puk_component_provides("PadicoComponent", "component", &nm_ibverbs_adaptrdma_component),
                        puk_component_provides("NewMad_minidriver", "minidriver", &nm_ibverbs_adaptrdma_minidriver),
                        puk_component_uses("NewMad_ibverbs", "ibverbs")));


/* ********************************************************* */

static void* nm_ibverbs_adaptrdma_instantiate(puk_instance_t instance, puk_context_t p_context)
{
  struct nm_ibverbs_adaptrdma_s*p_adaptrdma = padico_malloc(sizeof(struct nm_ibverbs_adaptrdma_s));
  memset(&p_adaptrdma->buffer, 0, sizeof(p_adaptrdma->buffer));
  p_adaptrdma->mr = NULL;
  p_adaptrdma->p_context = p_context;
  return p_adaptrdma;
}

static void nm_ibverbs_adaptrdma_destroy(void*_status)
{
  struct nm_ibverbs_adaptrdma_s*p_adaptrdma = _status;
  padico_free(p_adaptrdma);
}

/* ********************************************************* */

static void nm_ibverbs_adaptrdma_getprops(puk_context_t p_context, struct nm_minidriver_properties_s*p_props)
{
  struct nm_ibverbs_adaptrdma_context_s*p_adaptrdma_context = padico_malloc(sizeof(struct nm_ibverbs_adaptrdma_context_s));
  puk_context_set_status(p_context, p_adaptrdma_context);
  p_adaptrdma_context->p_ibverbs_context = nm_ibverbs_context_resolve(p_context);
  nm_ibverbs_context_get_profile(p_adaptrdma_context->p_ibverbs_context, &p_props->profile);
  p_props->capabilities.supports_iovec = 0;
  p_props->nickname = "ibverbs_adpatrdma";
}

static void nm_ibverbs_adaptrdma_init(puk_context_t p_context, const void**p_url, size_t*p_url_size)
{
  struct nm_ibverbs_adaptrdma_context_s*p_adaptrdma_context = puk_context_get_status(p_context);
  nm_ibverbs_context_get_url(p_adaptrdma_context->p_ibverbs_context, p_url, p_url_size);
}

static void nm_ibverbs_adaptrdma_close(puk_context_t p_context)
{
  struct nm_ibverbs_adaptrdma_context_s*p_adaptrdma_context = puk_context_get_status(p_context);
  puk_context_set_status(p_context, NULL);
  padico_free(p_adaptrdma_context);
}

static void nm_ibverbs_adaptrdma_connect(void*_status, const void*p_remote_url, size_t url_size)
{
  struct nm_ibverbs_adaptrdma_s*p_adaptrdma = _status;
  struct nm_ibverbs_adaptrdma_context_s*p_adaptrdma_context = puk_context_get_status(p_adaptrdma->p_context);
  struct nm_ibverbs_context_s*p_ibverbs_context = p_adaptrdma_context->p_ibverbs_context;
  struct nm_ibverbs_cnx_s*p_ibverbs_cnx = nm_ibverbs_cnx_create(p_ibverbs_context);
  p_adaptrdma->p_cnx = p_ibverbs_cnx;
  /* register Memory Region */
  p_adaptrdma->mr = nm_ibverbs_reg_mr(p_ibverbs_context,
                                      &p_adaptrdma->buffer, sizeof(p_adaptrdma->buffer),
                                      &p_ibverbs_cnx->local_addr.segment);
  nm_ibverbs_cnx_connect2(p_ibverbs_cnx, p_remote_url, url_size);
  p_adaptrdma->seg = p_ibverbs_cnx->remote_addr.segment;
}


/* ** adaptrdma I/O **************************************** */

static inline int nm_ibverbs_adaptrdma_block_size(int done)
{
  if(done < 16 * 1024)
    return 4096;
  else if(done < 128 * 1024)
    return  4096;
  else if(done < 512 * 1024)
    return  8192;
  else if(done < 1024 * 1024)
    return  16384;
  else
    return 16384;
}

/** calculates the position of the packet header, given the beginning of the packet and its size
 */
static inline struct nm_ibverbs_adaptrdma_header_s*nm_ibverbs_adaptrdma_get_header(void*buf, int packet_size)
{
  return buf + packet_size - sizeof(struct nm_ibverbs_adaptrdma_header_s);
}

static int nm_ibverbs_adaptrdma_send_iov_post(void*_status, const struct iovec*v, int n)
{
  struct nm_ibverbs_adaptrdma_s*p_adaptrdma = _status;
  assert(n == 1);
  p_adaptrdma->send.message = v[0].iov_base;
  p_adaptrdma->send.todo    = v[0].iov_len;
  p_adaptrdma->send.done    = 0;
  p_adaptrdma->send.rbuf    = p_adaptrdma->buffer.rbuf;
  p_adaptrdma->send.sbuf    = p_adaptrdma->buffer.sbuf;
  p_adaptrdma->send.size_guard = nm_ibverbs_adaptrdma_step_base;
  return NM_ESUCCESS;
}

static int nm_ibverbs_adaptrdma_send_poll(void*_status)
{
  struct nm_ibverbs_adaptrdma_s*p_adaptrdma = _status;
  int packet_size = 0;
  const int block_size = nm_ibverbs_adaptrdma_block_size(p_adaptrdma->send.done);
  const int available  = block_size - NM_IBVERBS_ADAPTRDMA_HDRSIZE;
  while((p_adaptrdma->send.todo > 0) &&
        ((p_adaptrdma->p_cnx->pending.wrids[NM_IBVERBS_WRID_PACKET] > 0) ||
         (packet_size < p_adaptrdma->send.size_guard) ||
         (p_adaptrdma->send.todo <= nm_ibverbs_adaptrdma_step_overrun)))
    {
      const int frag_size = (p_adaptrdma->send.todo > available) ? available : p_adaptrdma->send.todo;
      struct nm_ibverbs_adaptrdma_header_s*const p_header =
        nm_ibverbs_adaptrdma_get_header(p_adaptrdma->send.sbuf, packet_size + block_size);
      memcpy(p_adaptrdma->send.sbuf + packet_size,
             &p_adaptrdma->send.message[p_adaptrdma->send.done], frag_size);
      p_header->checksum = nm_ibverbs_checksum(&p_adaptrdma->send.message[p_adaptrdma->send.done], frag_size);
      p_header->busy   = NM_IBVERBS_ADAPTRDMA_FLAG_REGULAR;
      p_header->offset = frag_size;
      p_adaptrdma->send.todo -= frag_size;
      p_adaptrdma->send.done += frag_size;
      packet_size += block_size;
      if((p_adaptrdma->p_cnx->pending.wrids[NM_IBVERBS_WRID_PACKET] > 0) &&
         (p_adaptrdma->send.done > nm_ibverbs_adaptrdma_step_base))
        {
          int rc = nm_ibverbs_rdma_poll(p_adaptrdma->p_cnx);
          if(rc != NM_ESUCCESS)
            return rc;
        }
    }
  struct nm_ibverbs_adaptrdma_header_s*const p_h_last =
    nm_ibverbs_adaptrdma_get_header(p_adaptrdma->send.sbuf, packet_size);
  p_h_last->busy = NM_IBVERBS_ADAPTRDMA_FLAG_BLOCKSIZE;
  int rc = nm_ibverbs_rdma_send(p_adaptrdma->p_cnx, packet_size, p_adaptrdma->send.sbuf, p_adaptrdma->send.rbuf,
                                &p_adaptrdma->buffer,
                                &p_adaptrdma->seg,
                                p_adaptrdma->mr,
                                NM_IBVERBS_WRID_PACKET);
  if(rc != NM_ESUCCESS)
    return rc;
  p_adaptrdma->send.size_guard *= (p_adaptrdma->send.size_guard <= 8192) ? 2 : 1.5;
  rc = nm_ibverbs_rdma_poll(p_adaptrdma->p_cnx);
  if(rc != NM_ESUCCESS)
    return rc;
  p_adaptrdma->send.sbuf += packet_size;
  p_adaptrdma->send.rbuf += packet_size;
  if(p_adaptrdma->send.todo <= 0)
    {
      rc = nm_ibverbs_send_flush(p_adaptrdma->p_cnx, NM_IBVERBS_WRID_PACKET);
      if(rc != NM_ESUCCESS)
        return rc;
      else
        return NM_ESUCCESS;
    }
  else
    {
      return -NM_EAGAIN;
    }

}

static int nm_ibverbs_adaptrdma_recv_init(void*_status, struct iovec*v, int n)
{
  struct nm_ibverbs_adaptrdma_s*p_adaptrdma = _status;
  p_adaptrdma->recv.done       = 0;
  p_adaptrdma->recv.block_size = nm_ibverbs_adaptrdma_block_size(0);
  p_adaptrdma->recv.message    = v->iov_base;
  p_adaptrdma->recv.size       = v->iov_len;
  p_adaptrdma->recv.rbuf       = p_adaptrdma->buffer.rbuf;
  return NM_ESUCCESS;
}


static int nm_ibverbs_adaptrdma_poll_one(void*_status)
{
  struct nm_ibverbs_adaptrdma_s*p_adaptrdma = _status;
  do
    {
      struct nm_ibverbs_adaptrdma_header_s*p_header =
        nm_ibverbs_adaptrdma_get_header(p_adaptrdma->recv.rbuf, p_adaptrdma->recv.block_size);
      if(!p_header->busy)
        goto wouldblock;
      const int frag_size = p_header->offset;
      const int flag = p_header->busy;
      memcpy(&p_adaptrdma->recv.message[p_adaptrdma->recv.done], p_adaptrdma->recv.rbuf, frag_size);
      /* checksum */
      if(nm_ibverbs_checksum(&p_adaptrdma->recv.message[p_adaptrdma->recv.done], frag_size) != p_header->checksum)
        {
          NM_FATAL("checksum failed.\n");
        }
      /* clear blocks */
      void*_rbuf = p_adaptrdma->recv.rbuf;
      p_adaptrdma->recv.done += frag_size;
      p_adaptrdma->recv.rbuf += p_adaptrdma->recv.block_size;
      while(_rbuf < p_adaptrdma->recv.rbuf)
        {
          p_header = nm_ibverbs_adaptrdma_get_header(_rbuf, nm_ibverbs_adaptrdma_block_granularity);
          p_header->offset = 0;
          p_header->busy = 0;
          _rbuf += nm_ibverbs_adaptrdma_block_granularity;
        }
      switch(flag)
        {
        case NM_IBVERBS_ADAPTRDMA_FLAG_REGULAR:
          break;

        case NM_IBVERBS_ADAPTRDMA_FLAG_BLOCKSIZE:
          p_adaptrdma->recv.block_size = nm_ibverbs_adaptrdma_block_size(p_adaptrdma->recv.done);
          break;

        default:
          NM_FATAL("unexpected flag 0x%x in adaptrdma_recv()\n", flag);
          break;
        }
    }
  while(p_adaptrdma->recv.done < p_adaptrdma->recv.size);

  p_adaptrdma->recv.message = NULL;
  return NM_ESUCCESS;

 wouldblock:
  return -NM_EAGAIN;
}

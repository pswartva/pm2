/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <stdio.h>
#include <unistd.h>
#include <errno.h>

#include <Padico/Puk.h>

//char dir[] = "/udd/cperez/EADS";

int padico_module_run(int argc, char **argv) {  
  
  char *dir;
  
  if (argc!=2) {
    padico_warning("ERROR!!! Bad usage (argc=%d): chdir <dirname>\n", argc);
    return -1;
  }

  dir = argv[1];

  if ((chdir(dir)) == -1) {
    padico_warning("WARNING!!! chdir failed with errno %d\n", errno);
  } 

  {
    char buf[1024];
    if (getcwd(buf, 1024)!=NULL)
      padico_print("-----> CURRENT DIR IS %s\n",buf);   
  }

  return 0;
}

/*
 * NewMadeleine
 * Copyright (C) 2006-2023 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <stdint.h>
#include <assert.h>
#include <limits.h>

#include <nm_private.h>
#include <Padico/Module.h>
#include <Padico/Puk.h>

/* ********************************************************* */

static void strat_prio_init(puk_context_t p_context);
static void strat_prio_close(puk_context_t p_context);
static nm_drv_vect_t strat_prio_connect(void*_status, nm_gate_t p_gate, nm_drv_vect_t p_available_drvs);
static void strat_prio_submit_req_chunk(puk_context_t p_context, struct nm_req_chunk_s*p_req_chunk, int front);
static void strat_prio_submit_ctrl_chunk(puk_context_t p_context, struct nm_ctrl_chunk_s*p_ctrl_chunk);
static void strat_prio_schedule(puk_context_t p_context);
static void strat_prio_rdv_accept(void*_status, nm_gate_t p_gate);
static void strat_prio_pw_send_post(void*_status, struct nm_pkt_wrap_s*p_pw);
static void strat_prio_pw_send_complete(void*_status, struct nm_pkt_wrap_s*p_pw);
static void strat_prio_pw_recv_complete(void*_status, struct nm_pkt_wrap_s*p_pw);

static const struct nm_strategy_iface_s nm_strat_prio_driver =
  {
    .capabilities.supports_priorities = 1,
    .init               = &strat_prio_init,
    .close              = &strat_prio_close,
    .connect            = &strat_prio_connect,
    .submit_req_chunk   = &strat_prio_submit_req_chunk,
    .submit_ctrl_chunk  = &strat_prio_submit_ctrl_chunk,
    .schedule           = &strat_prio_schedule,
    .try_and_commit     = NULL,
    .rdv_accept         = &strat_prio_rdv_accept,
    .pw_send_post       = &strat_prio_pw_send_post,
    .pw_send_complete   = &strat_prio_pw_send_complete,
    .pw_recv_complete   = &strat_prio_pw_recv_complete
};

static void*strat_prio_instantiate(puk_instance_t, puk_context_t);
static void strat_prio_destroy(void*);

static const struct puk_component_driver_s nm_strat_prio_component_driver =
  {
    .instantiate = &strat_prio_instantiate,
    .destroy     = &strat_prio_destroy
  };

/* ********************************************************* */

/** Component declaration */
PADICO_MODULE_COMPONENT(NewMad_Strategy_prio,
                        puk_component_declare("NewMad_Strategy_prio",
                                              puk_component_provides("PadicoComponent", "component", &nm_strat_prio_component_driver),
                                              puk_component_provides("NewMad_Strategy", "strat", &nm_strat_prio_driver))
                        );

PADICO_MODULE_ATTR(max_pw, "NM_PRIO_MAX_PW", "total number of simultaneous outgoing packets; -1 for auto", int, -1);

PADICO_MODULE_ATTR(enable_pw_schedule, "NM_PRIO_ENABLE_PW_SCHEDULE", "whether to enable scheduling & throtling of pws (disabled by default)", bool, 0);

PADICO_MODULE_ATTR(enable_rdv_schedule, "NM_PRIO_ENABLE_RDV_SCHEDULE", "whether to enable scheduling of rendez-vous", bool, 1);

PADICO_MODULE_ATTR(copy_on_send_threshold, "NM_COPY_ON_SEND_THRESHOLD", "threshold to switch between copy and iovec send", int, 4096);

/* ********************************************************* */

/*
 * Rationale for strat prio.
 *
 * req_chunks are flushed from req_chunk_list in gates to strat prio
 * private data structures:
 *   -- a per-gate tag-indexed table, to maintain packet ordering
 *      inside a tag, even when changing packet schedule.
 *   -- a per-gate priority list of queues of chunks, to find the
 *      next chunk with highest priority.
 *   -- a per-gate hashtable of queues of chunks, used to find
 *      queues to store chunks of a given prio level
 *   -- a global (context) priority list of pw queues
 *   -- a global (context) hashtable of pw queues, hashed by priority
 */


/* ** types for chunks ************************************** */

PUK_LIST_DECLARE_TYPE2(nm_prio_tag_chunk, struct nm_prio_chunk_s)
PUK_LIST_DECLARE_TYPE2(nm_prio_gate_chunk, struct nm_prio_chunk_s);
PUK_LIST_DECLARE_TYPE2(nm_prio_context_chunk, struct nm_prio_chunk_s);

/** a req_chunk wrapper, to be inserted in both tag-indexed lists and priority queues */
struct nm_prio_chunk_s
{
  struct nm_req_chunk_s*p_req_chunk;
  struct nm_ctrl_chunk_s*p_ctrl_chunk;
  PUK_LIST_LINK(nm_prio_tag_chunk);     /**< link to store chunk in per-tag list */
  PUK_LIST_LINK(nm_prio_gate_chunk);    /**< link to store chunk in per-gate prio list */
  PUK_LIST_LINK(nm_prio_context_chunk); /**< link to store chunk in per-context prio queue */
};

PUK_LIST_CREATE_FUNCS(nm_prio_tag_chunk);
PUK_LIST_CREATE_FUNCS(nm_prio_gate_chunk);
PUK_LIST_CREATE_FUNCS(nm_prio_context_chunk);

NM_ALLOCATOR_TYPE(nm_prio_chunk, struct nm_prio_chunk_s);

/* ** types for global priority queue ********************** */

/** a queue for a given priority level */
PUK_PRIO_TABLE_TYPE(nm_prio_context,
                    struct nm_prio_context_chunk_list_s chunks; /**< list of chunks for a given priority level in context */
                    );

/** a queue for pw packets to send */
PUK_PRIO_TABLE_TYPE(nm_prio_pw,
                    struct nm_pkt_wrap_list_s pending_pw_list;  /**< pending outgoing pws for thie prio level */
                    );

/* ** types for per-gate priority queue ******************** */

PUK_PRIO_TABLE_TYPE(nm_prio_gate,
                    struct nm_prio_gate_chunk_list_s chunks; /**< list of chunks for a given priority level in gate */
                    );

/** a queue for large pw packets pending on recv before sending an RTR */
PUK_PRIO_TABLE_TYPE(nm_prio_pw_gate,
                    struct nm_pkt_wrap_list_s pending_large_recv;  /**< global pending large recv list */
                    );


/* ** types for per-tag queues ***************************** */

/** per-tag strat prio status */
struct nm_prio_tag_s
{
  struct nm_prio_tag_chunk_list_s chunks; /**< list of chunks submitted on the tag, ordered by submission date */
};
static inline void nm_prio_tag_ctor(struct nm_prio_tag_s*p_prio_tag, nm_core_tag_t tag)
{
  nm_prio_tag_chunk_list_init(&p_prio_tag->chunks);
}
static inline void nm_prio_tag_dtor(struct nm_prio_tag_s*p_prio_tag)
{
  nm_prio_tag_chunk_list_destroy(&p_prio_tag->chunks);
}
NM_TAG_TABLE_TYPE(nm_prio_tag, struct nm_prio_tag_s);


/* ** reverse gate lookup */

struct nm_strat_prio_s;

PUK_HASHTABLE_TYPE(nm_strat_prio_status, struct nm_gate_s*, struct nm_strat_prio_s*,
                   &puk_hash_pointer_default_hash, &puk_hash_pointer_default_eq, NULL);


/* ** global types ***************************************** */

/** shared context across all per-gate strat_prio instances */
struct nm_strat_prio_context_s
{
  struct nm_strat_prio_status_hashtable_s status_table;   /**< table of strat prio per-gate instances */
  struct nm_prio_chunk_allocator_s chunk_allocator;       /**< allocator for chunk cells */
  struct nm_prio_context_prio_table_s prio_queues;        /**< priority table for queues of chunks */
  int num_pw_send;                                        /**< number of pw beeing currently sent */
  int max_pw_send;                                        /**< maximum number of pw to send simultaneously */
  int env_max_pw;                                         /**< maximum number of pw value from environment */
  struct nm_core*p_core;                                  /**< keep shortcut to nmad core */
  struct nm_prio_pw_prio_table_s pw_prio_list;
};

/** Per-gate status for strat prio instances
 */
struct nm_strat_prio_s
{
  struct nm_strat_prio_context_s*p_prio_context;
  nm_gate_t p_gate;                                   /**< gate this instance is attached to */
  struct nm_prio_gate_prio_table_s gate_prio_queues;  /**< per-gate priority table for queues of chunks */
  struct nm_prio_pw_gate_prio_table_s gate_pending_large_recv;  /**< pending large recv list for this gate, by priority */
  struct nm_prio_tag_table_s tags;                    /**< list of chunks, indexed by tag */
  int nm_copy_on_send_threshold;
};

static void strat_prio_pw_send_try(struct nm_strat_prio_context_s*p_prio_context);
static void strat_prio_do_schedule(struct nm_strat_prio_context_s*p_prio_context);


/* ********************************************************* */

/** Initialize the gate storage for prio strategy.
 */
static void*strat_prio_instantiate(puk_instance_t p_instance, puk_context_t p_context)
{
  struct nm_strat_prio_s*p_status = padico_malloc(sizeof(struct nm_strat_prio_s));
  p_status->nm_copy_on_send_threshold = padico_module_attr_copy_on_send_threshold_getvalue();
  nm_prio_tag_table_init(&p_status->tags);
  nm_prio_gate_prio_table_init(&p_status->gate_prio_queues);
  nm_prio_pw_gate_prio_table_init(&p_status->gate_pending_large_recv);
  struct nm_strat_prio_context_s*p_prio_context = puk_context_get_status(p_context);
  assert(p_prio_context != NULL);
  /* adjust max pw cound with the number of nodes */
  if(p_prio_context->env_max_pw < 0)
    {
      p_prio_context->max_pw_send++;
    }
  p_status->p_prio_context = p_prio_context;
  p_status->p_gate = NULL;
  return p_status;
}

/** Cleanup the gate storage for prio strategy.
 */
static void strat_prio_destroy(void*_status)
{
  struct nm_strat_prio_s*p_status = _status;
  nm_prio_gate_prio_table_destroy(&p_status->gate_prio_queues);
  nm_prio_pw_gate_prio_table_destroy(&p_status->gate_pending_large_recv);
  nm_prio_tag_table_destroy(&p_status->tags);
  padico_free(_status);
}

/* ********************************************************* */

static void strat_prio_init(puk_context_t p_context)
{
  struct nm_strat_prio_context_s*p_prio_context = padico_malloc(sizeof(struct nm_strat_prio_context_s));
  puk_context_set_status(p_context, p_prio_context);
  /* init context */
  nm_strat_prio_status_hashtable_init(&p_prio_context->status_table);
  nm_prio_chunk_allocator_init(&p_prio_context->chunk_allocator, 16);
  nm_prio_context_prio_table_init(&p_prio_context->prio_queues);
  nm_prio_pw_prio_table_init(&p_prio_context->pw_prio_list);
  p_prio_context->env_max_pw = padico_module_attr_max_pw_getvalue();
  p_prio_context->num_pw_send = 0;
  p_prio_context->max_pw_send = 2;
  p_prio_context->p_core = NULL;
  if(p_prio_context->env_max_pw > 0)
    {
      p_prio_context->max_pw_send = p_prio_context->env_max_pw;
      NM_DISPF("max_pw_send = %d forced by environment.\n", p_prio_context->env_max_pw);
    }
  padico_out(puk_verbose_notice, "init- max = %d\n", p_prio_context->max_pw_send);
}

static void strat_prio_close(puk_context_t p_context)
{
  struct nm_strat_prio_context_s*p_prio_context = puk_context_get_status(p_context);
  puk_context_set_status(p_context, NULL);
  nm_strat_prio_status_hashtable_destroy(&p_prio_context->status_table);
  nm_prio_chunk_allocator_destroy(&p_prio_context->chunk_allocator);
  nm_prio_context_prio_table_destroy(&p_prio_context->prio_queues);
  nm_prio_pw_prio_table_destroy(&p_prio_context->pw_prio_list);
  padico_free(p_prio_context);
}

static void strat_prio_accept_large_pw(struct nm_strat_prio_s*p_status)
{
  nm_gate_t p_gate = p_status->p_gate;
  struct nm_trk_s*p_trk_large = &p_gate->trks[NM_TRK_LARGE];
  if(p_trk_large->p_pw_recv == NULL)
    {
      /* normalize prio table */
      struct nm_prio_pw_gate_prio_cell_s*p_prio_pw_cell =
        nm_prio_pw_gate_prio_table_top(&p_status->gate_pending_large_recv);
      while((p_prio_pw_cell != NULL) && nm_pkt_wrap_list_empty(&p_prio_pw_cell->pending_large_recv))
        {
          nm_prio_pw_gate_prio_table_pop(&p_status->gate_pending_large_recv);
          nm_pkt_wrap_list_destroy(&p_prio_pw_cell->pending_large_recv);
          nm_prio_pw_gate_prio_table_cell_free(&p_status->gate_pending_large_recv, p_prio_pw_cell);
          p_prio_pw_cell = nm_prio_pw_gate_prio_table_top(&p_status->gate_pending_large_recv);
        }
      if(p_prio_pw_cell != NULL)
        {
          struct nm_pkt_wrap_s*p_pw = nm_pkt_wrap_list_pop_front(&p_prio_pw_cell->pending_large_recv);
          /* The large-packet track is available- post recv and RTR for the top-most large pw */
          nm_tactic_rdv_accept(p_gate->p_core, p_pw);
        }
    }
}

static inline struct nm_prio_context_prio_cell_s*
strat_prio_context_get_cell(struct nm_strat_prio_context_s*p_prio_context, const nm_prio_t prio)
{
  struct nm_prio_context_prio_cell_s*p_prio_context_cell = nm_prio_context_prio_table_lookup(&p_prio_context->prio_queues, prio);
  if(p_prio_context_cell == NULL)
    {
      /* create new cell for the given prio level in context */
      p_prio_context_cell = nm_prio_context_prio_table_cell_alloc(&p_prio_context->prio_queues);
      nm_prio_context_chunk_list_init(&p_prio_context_cell->chunks);
      nm_prio_context_prio_table_insert(&p_prio_context->prio_queues, prio, p_prio_context_cell);
    }
  return p_prio_context_cell;
}

static inline struct nm_prio_gate_prio_cell_s*
strat_prio_gate_get_cell(struct nm_strat_prio_s*p_status, const nm_prio_t prio)
{
  struct nm_prio_gate_prio_cell_s*p_prio_gate_cell = nm_prio_gate_prio_table_lookup(&p_status->gate_prio_queues, prio);
  if(p_prio_gate_cell == NULL)
    {
      /* create new cell for the given prio level in gate */
      p_prio_gate_cell = nm_prio_gate_prio_table_cell_alloc(&p_status->gate_prio_queues);
      nm_prio_gate_chunk_list_init(&p_prio_gate_cell->chunks);
      nm_prio_gate_prio_table_insert(&p_status->gate_prio_queues, prio, p_prio_gate_cell);
    }
  return p_prio_gate_cell;
}

/** store a req chunk in the priority lists */
static void strat_prio_store_chunk(struct nm_strat_prio_context_s*p_prio_context, struct nm_strat_prio_s*p_status,
                                   struct nm_req_chunk_s*p_req_chunk, struct nm_ctrl_chunk_s*p_ctrl_chunk)
{
  assert((p_req_chunk == NULL) ^ (p_ctrl_chunk == NULL));
  /* ** alloc & fill chunk */
  struct nm_prio_chunk_s*p_prio_chunk = nm_prio_chunk_malloc(&p_prio_context->chunk_allocator);
  p_prio_chunk->p_req_chunk = p_req_chunk;
  p_prio_chunk->p_ctrl_chunk = p_ctrl_chunk;
  nm_prio_tag_chunk_list_cell_init(p_prio_chunk);
  nm_prio_gate_chunk_list_cell_init(p_prio_chunk);
  nm_prio_context_chunk_list_cell_init(p_prio_chunk);
  const nm_prio_t prio = (p_req_chunk != NULL) ? p_req_chunk->p_req->pack.priority : INT_MAX;

  /* ** store in global prio queue */
  struct nm_prio_context_prio_cell_s*p_prio_context_queue = strat_prio_context_get_cell(p_prio_context, prio);
  nm_prio_context_chunk_list_push_back(&p_prio_context_queue->chunks, p_prio_chunk);

  /* ** store in gate local prio queue */
  struct nm_prio_gate_prio_cell_s*p_prio_gate_queue = strat_prio_gate_get_cell(p_status, prio);
  nm_prio_gate_chunk_list_push_back(&p_prio_gate_queue->chunks, p_prio_chunk);

  if(p_req_chunk != NULL)
    {
      /* ** store req chunk in tag table */
      struct nm_prio_tag_s*p_prio_tag = nm_prio_tag_get(&p_status->tags, p_req_chunk->p_req->tag);
      nm_prio_tag_chunk_list_push_back(&p_prio_tag->chunks, p_prio_chunk);
    }
}

/** normalize prio list- purge empty lists; and return top priority queue */
static inline struct nm_prio_gate_prio_cell_s*strat_prio_queue_top(struct nm_prio_gate_prio_table_s*p_prio_table)
{
  struct nm_prio_gate_prio_cell_s*p_prio_queue = nm_prio_gate_prio_table_top(p_prio_table);
  while((p_prio_queue != NULL) && nm_prio_gate_chunk_list_empty(&p_prio_queue->chunks))
    {
      nm_prio_gate_prio_table_pop(p_prio_table);
      nm_prio_gate_chunk_list_destroy(&p_prio_queue->chunks);
      nm_prio_gate_prio_table_cell_free(p_prio_table, p_prio_queue);
      p_prio_queue = nm_prio_gate_prio_table_top(p_prio_table);
    }
  return p_prio_queue;
}

/** dequeue a prio chunk from all queues */
static inline void strat_prio_chunk_dequeue(struct nm_strat_prio_s*p_status,
                                            struct nm_prio_chunk_s*p_prio_chunk,
                                            struct nm_prio_context_prio_cell_s*p_prio_context_queue,
                                            struct nm_prio_gate_prio_cell_s*p_prio_gate_queue,
                                            struct nm_prio_tag_s*p_prio_tag)
{
  nm_prio_context_chunk_list_remove(&p_prio_context_queue->chunks, p_prio_chunk);
  nm_prio_gate_chunk_list_remove(&p_prio_gate_queue->chunks, p_prio_chunk);
  if(p_prio_tag != NULL)
    {
      nm_prio_tag_chunk_list_pop_front(&p_prio_tag->chunks);
      /* normalize tag-list */
      if(nm_prio_tag_chunk_list_empty(&p_prio_tag->chunks))
        {
          /* garbage-collect empty tags */
          nm_prio_tag_delete(&p_status->tags, p_prio_tag);
        }
    }
  nm_prio_chunk_free(&p_status->p_prio_context->chunk_allocator, p_prio_chunk);
}

/* ********************************************************* */
/* strat_prio interface */

/** connect a strat_prio instance with its gate */
static nm_drv_vect_t strat_prio_connect(void*_status, nm_gate_t p_gate, nm_drv_vect_t p_available_drvs)
{
  struct nm_strat_prio_s*p_status = _status;
  assert(p_status->p_gate == NULL);
  p_status->p_gate = p_gate;
  nm_strat_prio_status_hashtable_insert(&p_status->p_prio_context->status_table, p_gate, p_status);
  if(p_status->p_prio_context->p_core == NULL)
    {
      p_status->p_prio_context->p_core = p_gate->p_core;
    }
  return nm_drv_vect_copy(p_available_drvs);
}

static void strat_prio_submit_req_chunk(puk_context_t p_context, struct nm_req_chunk_s*p_req_chunk, int front)
{
  struct nm_gate_s*p_gate = p_req_chunk->p_req->p_gate;
  struct nm_core*p_core = p_gate->p_core;
  nm_core_lock_assert(p_core);
  struct nm_strat_prio_context_s*p_prio_context = puk_context_get_status(p_context);
  struct nm_strat_prio_s*p_status = nm_strat_prio_status_hashtable_lookup(&p_prio_context->status_table, p_gate);
  assert(p_status != NULL);
  strat_prio_store_chunk(p_prio_context, p_status, p_req_chunk, NULL);
}

static void strat_prio_submit_ctrl_chunk(puk_context_t p_context, struct nm_ctrl_chunk_s*p_ctrl_chunk)
{
  struct nm_gate_s*p_gate = p_ctrl_chunk->p_gate;
  struct nm_core*p_core = p_gate->p_core;
  nm_core_lock_assert(p_core);
  struct nm_strat_prio_context_s*p_prio_context = puk_context_get_status(p_context);
  struct nm_strat_prio_s*p_status = nm_strat_prio_status_hashtable_lookup(&p_prio_context->status_table, p_gate);
  assert(p_status != NULL);
  strat_prio_store_chunk(p_prio_context, p_status, NULL, p_ctrl_chunk);
}

/** main scheduling function for strat_prio */
static void strat_prio_schedule(puk_context_t p_context)
{
  struct nm_strat_prio_context_s*p_prio_context = puk_context_get_status(p_context);
  strat_prio_do_schedule(p_prio_context);
}

static void strat_prio_do_schedule(struct nm_strat_prio_context_s*p_prio_context)
{
  if(p_prio_context == NULL || p_prio_context->p_core == NULL)
    {
      /* strategy not connected yet; skip */
      return;
    }
  nm_core_lock_assert(p_prio_context->p_core);

  /* ** throttle on number of active pw */
  if( padico_module_attr_enable_pw_schedule_getvalue() &&
      (p_prio_context->num_pw_send >= p_prio_context->max_pw_send) )
    {
      if(p_prio_context->num_pw_send > p_prio_context->max_pw_send)
        {
          NM_FATAL("strat_prio: p_prio_context->num_pw_send = %d;  p_prio_context->max_pw_send = %d\n",
                   p_prio_context->num_pw_send, p_prio_context->max_pw_send);
        }
      return;
    }

  /* ** make progress on rdv */


  /* ** per-context prio scheduling */
  struct nm_prio_context_prio_cell_s*p_prio_context_queue = nm_prio_context_prio_table_top(&p_prio_context->prio_queues);
  while((p_prio_context_queue != NULL) && nm_prio_context_chunk_list_empty(&p_prio_context_queue->chunks))
    {
      nm_prio_context_prio_table_pop(&p_prio_context->prio_queues);
      nm_prio_context_chunk_list_destroy(&p_prio_context_queue->chunks);
      nm_prio_context_prio_table_cell_free(&p_prio_context->prio_queues, p_prio_context_queue);
      p_prio_context_queue = nm_prio_context_prio_table_top(&p_prio_context->prio_queues);
    }
  if(p_prio_context_queue != NULL)
    {
      /* there is at least a non-empty queue */
      struct nm_prio_chunk_s*p_prio_chunk = nm_prio_context_chunk_list_begin(&p_prio_context_queue->chunks);
      assert(!nm_prio_context_chunk_list_empty(&p_prio_context_queue->chunks));
      nm_gate_t p_gate = p_prio_chunk->p_req_chunk ? p_prio_chunk->p_req_chunk->p_req->p_gate : p_prio_chunk->p_ctrl_chunk->p_gate;
      assert(p_gate->status == NM_GATE_STATUS_CONNECTED);
      struct nm_trk_s*p_trk_small = &p_gate->trks[NM_TRK_SMALL];
      struct nm_core*p_core = p_gate->p_core;
      if(p_trk_small->p_pw_send == NULL)
        {
          /* trk is ready to send */
          struct nm_pkt_wrap_s*p_pw = nm_pw_alloc_global_header(p_core, p_trk_small);
          if(p_pw == NULL)
            {
              return;
            }
          p_pw->prio = nm_prio_context_prio_cell_get_prio(p_prio_context_queue);
          struct nm_strat_prio_s*p_status = nm_strat_prio_status_hashtable_lookup(&p_prio_context->status_table, p_gate);
          struct nm_prio_gate_prio_cell_s*p_prio_gate_queue = strat_prio_queue_top(&p_status->gate_prio_queues);
          /* check consistency */
          assert(!nm_prio_gate_chunk_list_empty(&p_prio_gate_queue->chunks));
          assert(nm_prio_gate_chunk_list_begin(&p_prio_gate_queue->chunks) == p_prio_chunk);

          /* ** aggregate chunks for the given gate */
          while(p_prio_gate_queue != NULL)
            {
              p_prio_chunk = nm_prio_gate_chunk_list_begin(&p_prio_gate_queue->chunks);
              if(p_prio_chunk->p_ctrl_chunk != NULL)
                {
                  /* ** pack ctrl chunk */
                  assert(p_prio_chunk->p_req_chunk == NULL);
                  struct nm_ctrl_chunk_s*p_ctrl_chunk = p_prio_chunk->p_ctrl_chunk;
                  if(NM_HEADER_CTRL_SIZE(&p_ctrl_chunk->ctrl) < nm_pw_remaining_buf(p_pw))
                    {
                      nm_pw_add_control(p_pw, &p_ctrl_chunk->ctrl);
                      nm_ctrl_chunk_free(&p_gate->p_core->ctrl_chunk_allocator, p_ctrl_chunk);
                      strat_prio_chunk_dequeue(p_status, p_prio_chunk, p_prio_context_queue, p_prio_gate_queue, NULL);
                    }
                  else
                    {
                      /* don't even try to aggregate data if pw cannot even contain any ctrl header */
                      goto post_send;
                    }
                }
              else
                {
                  /* ** pack data chunk gate */
                  assert(p_prio_chunk->p_ctrl_chunk == NULL);
                  struct nm_req_chunk_s*p_req_chunk = p_prio_chunk->p_req_chunk;
                  /* take the head of the lists of chunks in the tag of the highest priority chunk */
                  const nm_core_tag_t tag = p_req_chunk->p_req->tag;
                  struct nm_prio_tag_s*p_prio_tag = nm_prio_tag_get(&p_status->tags, tag);
                  struct nm_prio_chunk_s*p_prio_tag_chunk = nm_prio_tag_chunk_list_begin(&p_prio_tag->chunks);
                  if(p_prio_chunk != p_prio_tag_chunk)
                    {
                      /* there is an older req_chunk on the same tag as the max prio */
                      if(nm_prio_gate_prio_cell_get_prio(p_prio_gate_queue) !=
                         p_prio_tag_chunk->p_req_chunk->p_req->pack.priority)
                        {
                          /* different priority- find the right queue */
                          p_prio_gate_queue = nm_prio_gate_prio_table_lookup(&p_status->gate_prio_queues,
                                                                             p_prio_tag_chunk->p_req_chunk->p_req->pack.priority);
                        }
                      p_prio_chunk = p_prio_tag_chunk;
                      p_req_chunk = p_prio_tag_chunk->p_req_chunk;
                    }
                  /* find the corresponding context queue */
                  const nm_prio_t prio = nm_prio_gate_prio_cell_get_prio(p_prio_gate_queue);
                  p_prio_context_queue = nm_prio_context_prio_table_lookup(&p_prio_context->prio_queues, prio);

                  if(nm_tactic_req_is_short(p_req_chunk))
                    {
                      /* ** short data- post on trk #0 */
                      if(nm_tactic_req_short_size(p_req_chunk) <= nm_pw_remaining_buf(p_pw))
                        {
                          strat_prio_chunk_dequeue(p_status, p_prio_chunk, p_prio_context_queue, p_prio_gate_queue, p_prio_tag);
                          nm_pw_add_req_chunk(p_pw, p_req_chunk, NM_REQ_CHUNK_FLAG_SHORT);
                        }
                      else
                        {
                          goto post_send;
                        }
                    }
                  else if(nm_tactic_req_data_max_size(p_req_chunk) + sizeof(struct nm_header_global_s) < p_pw->max_len)
                    {
                      /* ** small data- post on trk #0 */
                      if(nm_tactic_req_data_max_size(p_req_chunk) < nm_pw_remaining_buf(p_pw))
                        {
                          strat_prio_chunk_dequeue(p_status, p_prio_chunk, p_prio_context_queue, p_prio_gate_queue, p_prio_tag);
                          nm_pw_add_req_chunk(p_pw, p_req_chunk, NM_REQ_CHUNK_FLAG_NONE);
                        }
                      else
                        {
                          goto post_send;
                        }
                    }
                  else
                    {
                      /* ** large data- post RDV */
                      struct nm_trk_s*p_trk_large = &p_gate->trks[NM_TRK_LARGE];
                      struct nm_drv_s*p_drv_large = p_trk_large->p_drv;
                      int rc = nm_tactic_pack_rdv(p_gate, p_drv_large, NULL, p_req_chunk, p_pw);
                      if(rc == NM_ESUCCESS)
                        {
                          strat_prio_chunk_dequeue(p_status, p_prio_chunk, p_prio_context_queue, p_prio_gate_queue, p_prio_tag);
                        }
                      else if(rc == -NM_EAGAIN)
                        {
                          strat_prio_chunk_dequeue(p_status, p_prio_chunk, p_prio_context_queue, p_prio_gate_queue, p_prio_tag);
                        }
                      else if(rc == -NM_EBUSY)
                        {
                          goto post_send;
                        }
                      else
                        {
                          NM_FATAL("strat_prio: unexpected error in nm_tactic_pack_rdv()- rc = %d", rc);
                        }
                    }
                }
              assert(p_pw->length <= p_pw->max_len);
              p_prio_gate_queue = strat_prio_queue_top(&p_status->gate_prio_queues);
            }
        post_send:
          assert(p_pw->length <= p_pw->max_len);
          assert(p_pw->length > sizeof(struct nm_header_global_s));
          nm_core_post_send(p_pw, p_gate, NM_TRK_SMALL);
        }
    }
  if(padico_module_attr_enable_pw_schedule_getvalue())
    {
      strat_prio_pw_send_try(p_prio_context);
    }
  else
    {
      assert(nm_prio_pw_prio_table_top(&p_prio_context->pw_prio_list) == NULL);
    }
}

/** Emit RTR for received RDV requests */
static void strat_prio_rdv_accept(void*_status, nm_gate_t p_gate)
{
  struct nm_strat_prio_s*p_status = _status;
  nm_core_lock_assert(p_status->p_prio_context->p_core);
  if(!nm_pkt_wrap_list_empty(&p_gate->pending_large_recv))
    {
      if(padico_module_attr_enable_rdv_schedule_getvalue())
        {
          /* get pw from pending_large_recv in gate */
          struct nm_pkt_wrap_s*p_pw = nm_pkt_wrap_list_pop_front(&p_gate->pending_large_recv);
          /* store pw in local prio table */
          struct nm_prio_pw_gate_prio_cell_s*p_prio_pw_cell =
            nm_prio_pw_gate_prio_table_lookup(&p_status->gate_pending_large_recv, p_pw->prio);
          if(p_prio_pw_cell == NULL)
            {
              /* create new queue for this prio level */
              p_prio_pw_cell = nm_prio_pw_gate_prio_table_cell_alloc(&p_status->gate_pending_large_recv);
              nm_pkt_wrap_list_init(&p_prio_pw_cell->pending_large_recv);
              nm_prio_pw_gate_prio_table_insert(&p_status->gate_pending_large_recv, p_pw->prio, p_prio_pw_cell);
            }
          nm_pkt_wrap_list_push_back(&p_prio_pw_cell->pending_large_recv, p_pw);
          /* try to schedule a pw if trk is ready */
          strat_prio_accept_large_pw(p_status);
        }
      else
        {
          struct nm_pkt_wrap_s*p_pw = nm_pkt_wrap_list_begin(&p_gate->pending_large_recv);
          struct nm_trk_s*p_trk_large = &p_gate->trks[NM_TRK_LARGE];
          if(p_trk_large->p_pw_recv == NULL)
            {
              /* The large-packet track is available- post recv and RTR */
              nm_pkt_wrap_list_remove(&p_gate->pending_large_recv, p_pw);
              nm_tactic_rdv_accept(p_gate->p_core, p_pw);
            }
        }
    }
}


/* ** pw scheduling **************************************** */

static void strat_prio_pw_send_try(struct nm_strat_prio_context_s*p_prio_context)
{
  nm_core_lock_assert(p_prio_context->p_core);
  if(p_prio_context->num_pw_send > p_prio_context->max_pw_send)
    NM_WARN("strat_prio_pw_send_try()- num_pw_send = %d\n", p_prio_context->num_pw_send);
  assert(p_prio_context->num_pw_send >= 0);
  if(p_prio_context->num_pw_send < p_prio_context->max_pw_send)
    {
      struct nm_prio_pw_prio_cell_s*p_cell = nm_prio_pw_prio_table_top(&p_prio_context->pw_prio_list);
      if(p_cell != NULL)
        {
          struct nm_pkt_wrap_s*p_pw = nm_pkt_wrap_list_begin(&p_cell->pending_pw_list);
          if(p_pw->p_trk->p_pw_send == NULL)
            {
              nm_pkt_wrap_list_pop_front(&p_cell->pending_pw_list);
              if(nm_pkt_wrap_list_empty(&p_cell->pending_pw_list))
                {
                  nm_prio_pw_prio_table_pop(&p_prio_context->pw_prio_list);
                  nm_prio_pw_prio_table_cell_free(&p_prio_context->pw_prio_list, p_cell);
                }
              p_prio_context->num_pw_send++;
              nm_pw_send_trigger(p_pw);
            }
        }
    }
}


static void strat_prio_pw_send_post(void*_status, struct nm_pkt_wrap_s*p_pw)
{
  if(padico_module_attr_enable_pw_schedule_getvalue())
    {
      struct nm_strat_prio_s*p_status = _status;
      struct nm_strat_prio_context_s*p_prio_context = p_status->p_prio_context;
      nm_core_lock_assert(p_prio_context->p_core);
      struct nm_trk_s*p_trk = p_pw->p_trk;
      if(p_trk->kind == nm_trk_large)
        {
          /* pw inherits req prio for large packs */
          assert(nm_req_chunk_list_size(&p_pw->req_chunks) == 1);
          struct nm_req_chunk_s*p_req_chunk = nm_req_chunk_list_begin(&p_pw->req_chunks);
          struct nm_req_s*p_pack = p_req_chunk->p_req;
          p_pw->prio = p_pack->pack.priority;
        }
      /* ** store pw in prio table */
      struct nm_prio_pw_prio_cell_s*p_cell = nm_prio_pw_prio_table_lookup(&p_prio_context->pw_prio_list, p_pw->prio);
      if(p_cell == NULL)
        {
          /* create new queue for this prio level */
          p_cell = nm_prio_pw_prio_table_cell_alloc(&p_prio_context->pw_prio_list);
          nm_pkt_wrap_list_init(&p_cell->pending_pw_list);
          nm_prio_pw_prio_table_insert(&p_prio_context->pw_prio_list, p_pw->prio, p_cell);
        }
      nm_pkt_wrap_list_push_back(&p_cell->pending_pw_list, p_pw);
      /* ** try to send the pw with highest prio, if trk is not busy */
      strat_prio_pw_send_try(p_prio_context);
    }
  else
    {
      nm_pw_send_post_default(p_pw);
    }
}

static void strat_prio_pw_send_complete(void*_status, struct nm_pkt_wrap_s*p_pw)
{
  if(padico_module_attr_enable_pw_schedule_getvalue())
    {
      struct nm_strat_prio_s*p_status = _status;
      struct nm_strat_prio_context_s*p_prio_context = p_status->p_prio_context;
      nm_core_lock_assert(p_prio_context->p_core);
      p_prio_context->num_pw_send--;
      assert(p_prio_context->num_pw_send >= 0);
      strat_prio_pw_send_try(p_prio_context);
      strat_prio_do_schedule(p_prio_context);
    }
  else
    {
      nm_pw_send_complete_default(p_pw);
    }
}

static void strat_prio_pw_recv_complete(void*_status, struct nm_pkt_wrap_s*p_pw)
{
  struct nm_strat_prio_s*p_status = _status;
  if(padico_module_attr_enable_rdv_schedule_getvalue())
    {
      struct nm_strat_prio_context_s*p_prio_context = p_status->p_prio_context;
      if(p_pw->trk_id == NM_TRK_LARGE)
        {
          strat_prio_accept_large_pw(p_status);
        }
      strat_prio_do_schedule(p_prio_context);
    }
  else
    {
      nm_gate_t p_gate = p_status->p_gate;
      strat_prio_rdv_accept(_status, p_gate);
    }
}

/*
 * NewMadeleine
 * Copyright (C) 2013-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#ifndef NM_MINIDRIVER_H
#define NM_MINIDRIVER_H

/** @ingroup nmad_private
 * @file
 * Definition of the driver interface.
 *
 * A driver with a context corresponds to a NIC. The same driver may be used
 * in multiple contexts in case of multi-rail. An instance is created for each
 * gate.
 *
 * A driver must define a subset of functions of struct nm_minidriver_iface_s.
 * ** Init **
 *   - getprops: the function is used to get properties of the driver. It is
 *     mandatory that all drivers define this function. In the properties,
 *     the field 'hints' contains hints given by upper layers for the driver;
 *     all other fields are initialized to 0 and are supposed to be filled-in
 *     by the driver. This function is always called first, before init.
 *     Additionnal hints are available as context attributes: "session_size",
 *     "rank", "wide_url_support".
 *   - init: the function is used to initialize the driver in the given context.
 *     It is mandatory that all drivers define this function. It returns an url
 *     that the launcher will provide to other nodes. The url is per-context.
 *     If a driver needs per instance urls, it is its responsibility to manage
 *     per-instance urls through the unique per-context url. In case
 *     wide_url_support is granted by the launcher, it may initialize a vector
 *     of urls, packed as a single wide per-context url. It must still support
 *     cases where non-scalable launcher (e.g. PMI2) does not support wide urls.
 *   - close: the function closes a driver in the given context. It is always
 *     called last, after all instances have been disconnected. This function
 *     may be left NULL.
 *
 * ** Connection establishment **
 *   A driver must define at least one method for connection establishment among
 *   synchronous or asynchronous.
 *   - For synchronous connection, connect() will be called on all
 *     nodes in sequence for each gate; it is assumed to establish the
 *     connection towards the node of the given url. This function is
 *     expected to be called on both nodes at the same time and is
 *     allowed to block. The remote node is known only through its url.
 *   - For asynchronous connection, connect_async() is called on all nodes
 *     for each gate, then connect_wait() is called for each gate. Multiple
 *     connection establishments may run at the same time. Function
 *     connect_async() may not block. A driver supporting fully asynchronous
 *     connection establishment is allowed to only define connect_async() and
 *     leave connect_wait() to NULL.
 *   If both methods arre available, a launcher that supports asynchronous
 *   connection will prefer asynchronous for better scalability.
 *   Function disconnect() is optionnal and may be left NULL. It is guaranteed
 *   that no communication will take place after disconnect.
 *
 * ** Sending **
 *   3 methods are available to initiate a send operation: buffer-based,
 *   iov-based, data-based. 2 methods are available to manage completion:
 *   polling and blocking wait. A driver must provide at least one method to
 *   initiate a send, must provide polling, and may optionnaly provide blocking
 *   wait.
 *   - buffer-based: the user calls send_buf_get() to get a buffer, fills it,
 *     then calls send_buf_post() to send it. The driver has thus the
 *     opportunity to allocate buffers directly in registered memory.
 *     This method is only possible for drivers for small packets.
 *   - iov-base: the user calls send_iov_post() whith an iovec that describes
 *     the packet to send. If the driver does not support iovecs (capability
 *     supports_iovec=0), then send_iov_post() will always be called with n=1.
 *   - data-based: the user calls send_data_post() with an nm_data_s that
 *     describes the data layout.
 *   The implementor should use the method that allows the lowest number of
 *   memory copies: buffer-based for small packets on network with memory
 *   registration or shared memory, data-based for large packets that need a
 *   memory copy, iov-based (with iovec support, as much as possible)
 *   otherwise.
 *   For completion, support of send_poll() is mandatory; support of send_wait()
 *   is optionnal. Even when using blocking wait, send_poll() will be called
 *   at least once before.
 *   It must be noted that these functions may be called from different threads,
 *   but not concurrently.
 *
 * ** Receiving **
 *   3 methods are available to initiate a recv operation: buffer-based,
 *   iov-base, data-based. 2 methods are available to manage completion:
 *   polling and blocking wait. A driver must provide at least one method to
 *   initiate a recv, must provide polling, and may optionnaly provide blocking
 *   wait.
 *   For scalability, before posting a recv request on a given instance, it is
 *   possible to ask globally (context-wide) for which instance has data
 *   available for receive. This may be done in a non-blocking way with
 *   recv_probe_any() or blocking with recv_wait_any(). After one if these
 *   functions returns, it is guaranteed that posting a receive on the returned
 *   instance will succeed immediately. This mechanism is reserved to drivers
 *   for small packet, without rendez-vous.
 *   - buffer-based: the user calls recv_buf_poll() to get a buffer with
 *     incoming data, then calls recv_buf_release() to give it back to the
 *     driver.
 *   - iov-based: the user calls recv_iov_post(), then polls its completion
 *     with recv_poll_one() or waits with recv_wait_one(). If the driver does
 *     not support iovecs (capability supports_iovec=0), then send_iov_post()
 *     will always be called with n=1. Please note that capability
 *     supports_iovec is global, for both sending and receiving.
 *   - data-based: the user calls recv_data_post(), then polls its completion
 *     with recv_poll_one() or waits with recv_wait_one().
 *   For completion, support of recv_poll_one() is mandatory for iov-based and
 *   data-based methods; blocking wait is optionnal.
 *   Upon termination, the user may want to cancel pending requests:
 *   - cancel_recv() cancels a posted recv_iov_post() or recv_data_post()
 *   - cancel_recv_any() cancels a recv_wait_any() that may be running at the
 *     same time in another thread.
 *
 * ** Prefetching **

 *   To optimize transfer on networks that require memory
 *   registration, it is possible to allow speculative registration on
 *   the sender or receiver side while a rendez-vous is still in
 *   progress. The user calls send_iov_prefetch() when it is likely
 *   that the message will be accepted on the given network, before
 *   having received the RTR, and recv_iov_prefetch() when it is
 *   likely that the message will be arriving through the given
 *   network, before having received rendez-vous request. In case the
 *   data arrives through another network or with a different layout
 *   (multiple chunks), functions send_iov_unfetch() and
 *   recv_iov_unfetch() are called to cancel prefetch. The driver may
 *   match calls to send_iov_prefetch() with the corresponding
 *   send_iov_post() through the iovec.
 *
 * ** Rendez-vous data **
 *   The driver may want to piggy-back some data with the RTR messages. In this
 *   case, the user calls get_rdv_data() on the receiver side after the receive
 *   has been posted, transports this data allongside with the RTR, and gives it
 *   to the driver on the sender side by calling set_rdv_data(). This may be
 *   usefull to driver implementors to transfer memory registration information
 *   or addresses for RDMA.
 */


#include <nm_public.h>
#include <Padico/Puk.h>
#ifdef NMAD_HWLOC
#include <hwloc.h>
#endif
#include <sys/uio.h>

/** Performance information for driver. This information is determined
 *  at init time and may depend on hardware and board number.
 */
struct nm_drv_profile_s
{
#ifdef NMAD_HWLOC
  hwloc_cpuset_t cpuset;  /**< cpu set close to the card (allocated by the driver, or NULL) */
#endif /* NMAD_HWLOC */
  /** Approximative performance of the board
   */
  int latency;   /**< in nanoseconds (10^-9 s) */
  int bandwidth; /**< in MB/s */
};

/** Static driver capabilities.
 */
struct nm_minidriver_capabilities_s
{
  nm_len_t max_msg_size; /**< maximum message size for the track */
  int supports_data;     /**< driver can send/recv direct nm_data_s */
  int supports_send_prefetch; /**< supports prefetch on the sender side */
  int supports_recv_prefetch; /**< supports prefetch on the receiver side */
  int supports_buf_send; /**< driver supported buffer-based send */
  int supports_buf_recv; /**< driver supported buffer-based recv */
  int supports_recv_any; /**< driver accepts receive from any source (recv_probe_any) */
  int supports_wait_any; /**< drivers supports passive wait_any */
  int supports_recv_wait; /**< drivers support blocking wait for a single receive */
  int prefers_wait_any;  /**< wait_any must be prefered for this driver */
  int prefers_send_wait; /**< send_wait must be prefered to send_poll */
  int prefers_recv_wait; /**< recv_wait_one must be prefered to recv_poll_one */
  int supports_iovec;    /**< driver can send iovecs; if set to 0 and send_iov_post is non-NULL, then driver supports only iov count = 1*/
  int max_iovecs;        /**< maximum size of iovecs; 0 for unlimited */
  int needs_rdv_data;    /**< driver needs rdv metadata to be transported out-of-band, using get_rdv_data/set_rdv_data */
  int min_period;        /**< minimum delay between poll (in microseconds) */
  int trk_rdv;           /**< trk needs a rdv for matched send/recv size; if 0, trk preserves boundaries */
  int no_send_poll;      /**< do not poll immediately after send post */
  int self;              /**< whether this driver communicates only with self */
};

/** some hints to help drivers tune themselves */
struct nm_minidriver_hints_s
{
  nm_trk_kind_t kind;
};

struct nm_minidriver_properties_s
{
  struct nm_drv_profile_s profile;
  struct nm_minidriver_capabilities_s capabilities;
  struct nm_minidriver_hints_s hints;
  const char*nickname;    /**< user-friendly name to display when user asks which drivers are used */
};

/** Interface driver for the 'NewMad_minidriver' component interface.
 * A driver interface simpler than NewMad_Driver, that do not rely
 * on nmad core structures, making driver fully encapsulated.
 */
struct nm_minidriver_iface_s
{
  /* ** driver init; applied to context before/after instantiation */

  /** get driver properties (profiling and capabilities) */
  void (*getprops)(puk_context_t context, struct nm_minidriver_properties_s*props);
  /** init the driver and get its url */
  void (*init)(puk_context_t context, const void**drv_url, size_t*url_size);
  /** close the driver */
  void (*close)(puk_context_t context);

  /* ** connection establishment */

  /** connect this instance to the remote url  */
  void (*connect)(void*_status, const void*remote_url, size_t url_size);
  /** asynchronously start the connect process */
  void (*connect_async)(void*_status, const void*remote_url, size_t url_size);
  /** wait for a previously posted connect_async to complete */
  void (*connect_wait)(void*_status);
  /** disconnect this instance */
  void (*disconnect)(void*_status);
  /* TODO- register error handler for fault-tolerance */

  /* ** sending primitives */

  /** post a send for data described by iovec; v is expected to remain valid up to send completion */
  int (*send_iov_post)(void*_status, const struct iovec*v, int n) __attribute__ ((warn_unused_result));
  /** post a send for data described by nm_data iterator; p_data is expected to remain valid up to send completion */
  int (*send_data_post)(void*_status, const struct nm_data_s*p_data, nm_len_t chunk_offset, nm_len_t chunk_len) __attribute__ ((warn_unused_result));
  /** get a buffer to prepare data for send_buf_post; may return -NM_ENOMEM in case no buffer is available  */
  int (*send_buf_get)(void*_status, void**p_buffer, nm_len_t*p_len) __attribute__ ((warn_unused_result));
  /** post a send after upper layers have filled-in the buffer */
  int (*send_buf_post)(void*_status, nm_len_t len) __attribute__ ((warn_unused_result));
  /** poll a send operation already posted through send_iov_post, send_data_post, or send_buf_post */
  int (*send_poll)(void*_status) __attribute__ ((warn_unused_result));
  /** wait for completion of a send operation already posted through send_iov_post, send_data_post, or send_buf_post */
  int (*send_wait)(void*_status) __attribute__ ((warn_unused_result));

  /* ** receiving primitives */

  /** poll the driver for a pending recv; returns buffer */
  int (*recv_buf_poll)(void*_status, void**p_buffer, nm_len_t*p_len) __attribute__ ((warn_unused_result));
  /** release a buffer returned by buf_recv_poll() or buf_recv_wait_any() */
  int (*recv_buf_release)(void*_status) __attribute__ ((warn_unused_result));
  /** post a recv for data described by iovec */
  int (*recv_iov_post)(void*_status, struct iovec*v, int n) __attribute__ ((warn_unused_result));
  /** post a recv for data described by nm_data iterator */
  int (*recv_data_post)(void*_status, const struct nm_data_s*p_data, nm_len_t chunk_offset, nm_len_t chunk_len) __attribute__ ((warn_unused_result));
  /** poll a recv operation already posted */
  int (*recv_poll_one)(void*_status) __attribute__ ((warn_unused_result));
  /** wait for completion of a recv operation already posted */
  int (*recv_wait_one)(void*_status) __attribute__ ((warn_unused_result));
  /** cancel a posted recv */
  int (*recv_cancel)(void*_status) __attribute__ ((warn_unused_result));
  /** poll the driver for a pending recv from any source; returns status of instance */
  int (*recv_probe_any)(puk_context_t p_context, void**_status) __attribute__ ((warn_unused_result));
  /** passively wait for a pending recv from any source; returns status of instance */
  int (*recv_wait_any)(puk_context_t p_context, void**_status) __attribute__ ((warn_unused_result));
  /** cancel a recv_wait_any */
  int (*recv_cancel_any)(puk_context_t p_context) __attribute__ ((warn_unused_result));

  /* ** prefetch */

  /** prefetch data in driver buffers before send_iov_post; calling it is _optional_ */
  void (*send_iov_prefetch)(void*_status, const struct iovec*v, int n);
  /** cancel a prefetch; a prefetch is assumed to _always_ be followed by either 'send' or 'unfetch' */
  void (*send_iov_unfetch)(void*_status, const struct iovec*v, int n);
  /** prefetch recv, i.e. begin memory registration even before rdv is received */
  void (*recv_iov_prefetch)(puk_context_t p_context, const struct iovec*v, int n);
  /** cancel a prefetch recv; a prefetch is assupmed to _always_ be followed either by 'recv' or 'unfetch' */
  void (*recv_iov_unfetch)(puk_context_t p_context, const struct iovec*v, int n);

  /* ** rdv data */

  /** retrieve the metadata associated with a rdv; to be called after a recv_*_post */
  int (*get_rdv_data)(void*_status, void*p_ptr, nm_len_t size);
  /** give rdv metadata before for a send; to be called before send_*_post */
  int (*set_rdv_data)(void*_status, void*p_ptr, nm_len_t size);

};
PUK_IFACE_TYPE(NewMad_minidriver, struct nm_minidriver_iface_s);

/** key to match prefetch entries with actual send_iov_post */
struct nm_prefetch_key_s
{
  const struct iovec*v;
  int n;
};

static inline int nm_prefetch_key_eq(const struct nm_prefetch_key_s*p_key1,
                                     const struct nm_prefetch_key_s*p_key2)
{
  /* compare only pointer, not the content, since we will not keep pointer longer than their lifetime */
  return ((p_key1->v == p_key2->v) && (p_key1->n == p_key2->n));
}

static inline uint32_t nm_prefetch_key_hash(const struct nm_prefetch_key_s*p_key)
{
  return puk_hash_pointer_default_hash(p_key->v);
}


#endif /* NM_MINIDRIVER_H */

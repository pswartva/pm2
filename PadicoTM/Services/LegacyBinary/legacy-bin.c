/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief "legacy" module driver for legacy binary codes
 * @defgroup LegacyBinary Service: LegacyBinary- support for legacy binary codes
 */

/*
 * LegacyBinary: support for legacy binaries in PadicoTM.
 *
 * Rationale:
 *
 * .1. padico-launch sets attribute PADICO_LEGACY=<binary>
 *
 * .2. padico-d sets a LD_PRELOAD=padico-legacy.so
 *
 * .3. library static initializer from padico-legacy.so bootstraps Puk and PadicoTM
 *
 * .4. the session script loads & starts a 'legacy' module
 *
 * .5. unit_legacy_start() (from this file) is called and starts a thread
 *
 * .7. the new thread calls padico_legacy_start() (from padico-legacy.c)
 *
 * .8. padico_legacy_start() calls the real main from the application.
 *
 */

#include <Padico/Module.h>
#include <Padico/Puk.h>
#include <Padico/PadicoTM.h>
#include <unistd.h>

static int legacy_binary_init(void);

PADICO_MODULE_DECLARE(LegacyBinary, legacy_binary_init, NULL, NULL);

/* ********************************************************* */

static padico_rc_t unit_legacy_start(puk_unit_t unit, puk_job_t job);
static padico_rc_t unit_legacy_unload(puk_unit_t unit);
static int  padico_launch_execve(const char*filename, char*const argv[], char*const arge[]) __attribute__((unused));

static const struct padico_loader_s legacy_driver =
{
  .name   = "legacy",
  .load   = NULL,
  .start  = &unit_legacy_start,
  .unload = &unit_legacy_unload
};

static puk_job_t legacy_binary_job = NULL;
static char**fake_argv = NULL;

/* ********************************************************* */

void padico_legacy_vexit(int rc)
{
  padico_print("binary invoked exit()- rc = %d\n", rc);
  puk_job_t job = legacy_binary_job;
  puk_job_notify(job);
  padico_tm_update_rc(rc);
  /* exit() cannot return- wait for a <kill/> in session script */
  puk_sleep(2);
  PUK_ABI_PROC_REAL(exit)(rc);
  padico_fatal("after exit()- we shouldn't be there.\n");
}

static padico_rc_t unit_legacy_start(puk_unit_t unit, puk_job_t job)
{
  const char*legacy_binary = padico_getenv("PADICO_LEGACY");
  fake_argv = padico_malloc((1+job->argc)*sizeof(char*));
  int i = 1;
  padico_print("starting main() for %s.\n", legacy_binary);
  for(i = 0; i < job->argc; i++)
    {
      fake_argv[i] = job->argv[i];
    }
  fake_argv[0] = padico_strdup(legacy_binary);
  fake_argv[job->argc] = NULL;

#ifdef PUKABI_ENABLE_PROC
  puk_abi_set_virtual(exit, &padico_legacy_vexit);
#endif
  legacy_binary_job = job;

  padico_tm_ready_signal();

  return NULL;
}

static padico_rc_t unit_legacy_unload(puk_unit_t unit)
{
  padico_free(fake_argv[0]);
  padico_free(fake_argv);
  fake_argv = NULL;
  padico_free(legacy_binary_job);
  legacy_binary_job = NULL;
  return NULL;
}

/* ********************************************************* */

int padico_launch_execve(const char*filename, char*const argv[], char*const arge[])
{
  char*padico_root = PADICOTM_ROOT;
  char padico_launch[1024];
  strcpy(padico_launch, padico_root);
  strcat(padico_launch, "/bin/padico-launch");
  char*new_argv[1024];
  int i = 0, j = 0;
  new_argv[i] = padico_strdup(padico_launch);
  i++;

  new_argv[i] = padico_strdup("-v");
  i++;
  new_argv[i] = padico_strdup(filename);
  i++;
  j = 1;
  while(argv[j])
    {
      new_argv[i] = argv[j];
      i++;
      j++;
    }
  new_argv[i] = NULL;
  marcel_sig_disable_interrupts();
  padico_print("execve: %s\n", filename);
  int rc = PUK_ABI_PROC_WRAP(execve)(padico_launch, new_argv, arge);
  int err = __puk_abi_wrap__errno;
  padico_fatal("execve rc = %d; error %d (%s).\n", rc, err, strerror(err));
  return -1;
}

/* ********************************************************* */

static int legacy_binary_init(void)
{
  puk_component_declare("PadicoLoader-legacy",
                        puk_component_provides("PadicoLoader", "loader", &legacy_driver));
#ifdef PUKABI_ENABLE_PROC
  puk_abi_set_virtual(execve, &padico_launch_execve);
#endif
  return 0;
}

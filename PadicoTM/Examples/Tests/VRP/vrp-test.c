/** @file Tests/VRP/vrp-test.c
 */

#include <Padico/VRP.h>
#include <Padico/Puk.h>
#include <Padico/NetAccess.h>
#include <Padico/Module.h>

int vrp_test_init(void);
int vrp_test_run(int argc, char**argv);
void vrp_test_finalize(void);

PADICO_MODULE_DECLARE(vrp_test, vrp_test_init, vrp_test_run, vrp_test_finalize);

static int port = 4579;
static int i_am_server = 0;
static vrp_incoming_t incoming;
static vrp_outgoing_t outgoing;

static void vrp_test_incoming_handler(vrp_in_buffer_t in_buffer);


int vrp_test_init(void)
{

  if(padico_na_rank() == 0)
    {
      i_am_server = 1;
      padico_print("Creating incoming...\n");
      incoming = vrp_incoming_construct(&port, &vrp_test_incoming_handler);
      padico_print("incoming ok -- port=%d\n", port);
    }
  else
    {
      char*server = padico_topo_host_getname(padico_topo_node_gethost(padico_topo_getnodebyrank(0)));
      i_am_server = 0;
      // hard-coded: server = node 0
      padico_print("Creating outgoing...\n");
      outgoing = vrp_outgoing_construct(server, port, 128, 1024);
      padico_print("outgoing ok...\n");
    }
  return 0;
}

int vrp_test_run(int argc, char**argv)
{
  const int count = 32;
  const int data_size = 10000000;
  const int max_cons_loss = 10000; /* maximum consecutive loss: 10000 bytes */
  const int max_percent_loss = 10; /* maximum loss per frame: 10% */
  char*data;

  if(!i_am_server)
    {
      int i;
      struct timeval tv1, tv2;
      double time;

      data = padico_malloc(data_size);
      padico_print("Starting transfer.\n");

      gettimeofday(&tv1, NULL);
      for(i=0; i<count; i++)
        {
          vrp_buffer_t buffer;
          padico_print("Constructing buffer i=%d size=%d\n", i, data_size);
          buffer = vrp_buffer_construct(data, data_size);
          padico_print("Set loss parameters: cons=%d bytes, max=%d%%\n",
                       max_cons_loss, max_percent_loss);
          vrp_buffer_set_loss_rate(buffer, 16, max_cons_loss);
          padico_print("Sending buffer.\n");
          vrp_outgoing_send_frame(outgoing, buffer);
          padico_print("Destroying buffer.\n");
          vrp_buffer_destroy(buffer);
        }
      gettimeofday(&tv2, NULL);
      time = (tv2.tv_usec-tv1.tv_usec) / 1000000.0;
      time+= tv2.tv_sec-tv1.tv_sec;
      padico_print("Total time: %f (%i bytes)\n", time, count * data_size);
      padico_print("  %f bytes/s\n", data_size*count/time);
      padico_print("  %f MB/s\n", data_size*count/(1000000*time));
      padico_free(data);
    }
  return 0;
}

void vrp_test_finalize(void)
{
  if(i_am_server)
    {
      vrp_incoming_close(incoming);
    }
  else
    {
      vrp_outgoing_close(outgoing);
    }
}

static void vrp_test_incoming_handler(vrp_in_buffer_t in_buffer)
{
  int i;
  padico_print("[vrp-test] Frame arrived at %p size=%d\n",
               in_buffer->data, in_buffer->size);
  padico_print("[vrp-test]   bytes lost=%d (%g%%)\n",
               in_buffer->loss_residual,
               (100.0*in_buffer->loss_residual)/in_buffer->size);
  padico_print("[vrp-test]   %d fragments lost, each %d bytes\n",
                in_buffer->loss_num, in_buffer->loss_granularity);
  for(i=0; i<in_buffer->loss_num; i++)
    {
      padico_print("[vrp-test]   missing fragment #%d %p:%p\n",
                   i, in_buffer->lost[i],
                   in_buffer->lost[i]+in_buffer->loss_granularity);
    }
}

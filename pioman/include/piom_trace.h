/*
 * Pioman: a Generic I/O Manager
 * Copyright (C) 2001-2018 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#ifndef PIOM_TRACE_H
#define PIOM_TRACE_H

enum piom_trace_event_e
  {
    PIOM_TRACE_EVENT_TIMER_POLL, PIOM_TRACE_EVENT_IDLE_POLL,
    PIOM_TRACE_EVENT_SUBMIT, PIOM_TRACE_EVENT_SUCCESS,
    PIOM_TRACE_STATE_INIT, PIOM_TRACE_STATE_POLL, PIOM_TRACE_STATE_NONE,
    PIOM_TRACE_VAR_LTASKS
  };

/* forward declaration for trace functions */
struct piom_ltask_locality_s;

#ifdef PIOMAN_TRACE
struct piom_trace_info_s
{
  enum piom_topo_level_e level;
  int rank;
  const char*cont_type;
  const char*cont_name;
  struct piom_trace_info_s*parent;
};

void piom_trace_flush(void);

__PUK_SYM_INTERNAL void piom_trace_local_new(struct piom_trace_info_s*trace_info);

__PUK_SYM_INTERNAL void piom_trace_local_event(enum piom_trace_event_e _event, void*_value);

__PUK_SYM_INTERNAL void piom_trace_remote_event(piom_topo_obj_t obj, enum piom_trace_event_e _event, void*_value);

#else /* PIOMAN_TRACE */

struct piom_trace_info_s
{ /* empty */ };

static inline void piom_trace_local_event(enum piom_trace_event_e _event __attribute__((unused)), void*_value __attribute__((unused)))
{ /* empty */ }
static inline void piom_trace_remote_event(piom_topo_obj_t obj __attribute__((unused)), enum piom_trace_event_e _event __attribute__((unused)), void* _value __attribute__((unused)))
{ /* empty */ }
#endif /* PIOMAN_TRACE */

static inline void piom_trace_local_state(enum piom_trace_event_e _event)
{
  piom_trace_local_event(_event, NULL);
}
static inline void piom_trace_remote_state(piom_topo_obj_t obj, enum piom_trace_event_e _event)
{
  piom_trace_remote_event(obj, _event, NULL);
}
static inline void piom_trace_remote_var(piom_topo_obj_t obj, enum piom_trace_event_e _event, int _value)
{
  void*value = (void*)((uintptr_t)_value);
  piom_trace_remote_event(obj, _event, value);
}

/** locality information, for a given hwloc obj */
struct piom_ltask_locality_s
{
  struct piom_ltask_queue*queue;       /**< local queue, NULL if none at this level */
  struct piom_trace_info_s trace_info; /**< context for trace subsystem */
  struct piom_ltask_locality_s*parent; /**< shortcut to parent local info */
};

#endif /* PIOM_TRACE_H */

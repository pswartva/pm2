/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 ** @brief   Public API header for Puk-ABI.
 *  @ingroup PukABI
 *  @author  Alexandre Denis
 *  @note    Only stuff defined in *this file* is part of the API, even
 *           if internals are #include'd for inlined functions.
 *  @warning Do not include this file in PukABI itself! MKSYMxX macros
 *           have different declarations.
 */


/** @defgroup PukABI API: Puk ABI -- dynamic ABI manager
 * PukABI is a dynamic ABI manager. It intercepts symbols using
 * LD_PRELOAD to allow for a variety of features: replace a libc
 * function with a user-supplied function; add hooks for locking with
 * another thread library than libc pthread; add hooks for memory
 * registration cache invalidation.
 */

#ifndef PUK_ABI_H
#define PUK_ABI_H

#define PUK_ABI

/* include internals for inlined and #define'd functions */
#include "Puk-ABI-internals.h"
#include <sys/types.h>
#include <errno.h>


/* ********************************************************* */

/** @internal
 * @brief Generates symbol types, structures, stubs, and wrappers.
 */

#define MKSYMFX(SYM, RETTYPE, ARGSDECL, ARGSUSE, PRE, POST,  ISREAL)     PUK_ABI_MKTYPE(SYM, RETTYPE, ARGSDECL)
#define MKSYMVX1(SYM, RETTYPE, ARGSDECL, ARG1, PROCESSING, ISREAL)       PUK_ABI_MKTYPE(SYM, RETTYPE, ARGSDECL)
#define MKSYMVX2(SYM, RETTYPE, ARGSDECL, ARG1, ARG2, PROCESSING, ISREAL) PUK_ABI_MKTYPE(SYM, RETTYPE, ARGSDECL)
#define MKSYMVX3(SYM, RETTYPE, ARGSDECL, ARG1, ARG2, ARG3, PROCESSING, ISREAL) PUK_ABI_MKTYPE(SYM, RETTYPE, ARGSDECL)
#define MKSYMPX(SYM, RETTYPE, ARGSDECL, ARGSUSE, PRE, POST,  ISREAL)    PUK_ABI_MKTYPE(SYM, RETTYPE, ARGSDECL)

#define MKSYMNOSUPPORT(SYM)   PUK_ABI_MKTYPE(SYM, void, (void))

#include "Puk-ABI-symbols.h"

/* ********************************************************* */


/* *** Methods to access symbols *************************** */

/** Call the *real* libc symbol (always REAL, no locking)- it is up to the
 * user to appropriately puk_spinlock_{acquire|release}().
 */
#define PUK_ABI_REAL(SYM)    (*((_PASTE(SYM, _func_t))(PUK_ABI_SYM_REAL(SYM))))

/** Call the *virtual* symbol (always virtual).
 * @note not really usefull, except for completeness of PukABI API.
 */
#define PUK_ABI_VIRTUAL(SYM) (*((_PASTE(SYM, _func_t))(PUK_ABI_SYM_VIRTUAL(SYM))))

/** Call the *stub* symbol (auto choice)
 */
#define PUK_ABI_STUB(SYM)   (SYM) /* PUK_ABI_SYM_STUB(SYM) */

/** Call the *wrap* symbol (real + puk_spinlock_{acquire|release}() )
 */
#define PUK_ABI_WRAP(SYM)   PUK_ABI_SYM_WRAP(SYM)

#ifdef PUKABI_ENABLE_FSYS
#define PUK_ABI_FSYS_REAL(SYM) PUK_ABI_REAL(SYM)
#define PUK_ABI_FSYS_WRAP(SYM) PUK_ABI_WRAP(SYM)
#define __puk_abi_wrap__errno    (*(puk_abi_errno_stub()))
#else /* PUKABI_ENABLE_FSYS */
#define PUK_ABI_FSYS_REAL(SYM) SYM
#define PUK_ABI_FSYS_WRAP(SYM) SYM
#define __puk_abi_wrap__errno    errno
#endif /* PUKABI_ENABLE_FSYS */

#ifdef PUKABI_ENABLE_PROC
#define PUK_ABI_PROC_REAL(SYM) PUK_ABI_REAL(SYM)
#define PUK_ABI_PROC_WRAP(SYM) PUK_ABI_WRAP(SYM)
#else /* PUKABI_ENABLE_PROC */
#define PUK_ABI_PROC_REAL(SYM) SYM
#define PUK_ABI_PROC_WRAP(SYM) SYM
#endif /* PUKABI_ENABLE_PROC */

#ifdef PUKABI_ENABLE_MEM
#define PUK_ABI_MEM_REAL(SYM) PUK_ABI_REAL(SYM)
#define PUK_ABI_MEM_WRAP(SYM) PUK_ABI_WRAP(SYM)
#else /* PUKABI_ENABLE_MEM */
#define PUK_ABI_MEM_REAL(SYM) SYM
#define PUK_ABI_MEM_WRAP(SYM) SYM
#endif /* PUKABI_ENABLE_MEM */

#ifdef PUKABI_ENABLE_RESOLV
#define PUK_ABI_RESOLV_REAL(SYM) PUK_ABI_REAL(SYM)
#define PUK_ABI_RESOLV_WRAP(SYM) PUK_ABI_WRAP(SYM)
#else /* PUKABI_ENABLE_RESOLV */
#define PUK_ABI_RESOLV_REAL(SYM) SYM
#define PUK_ABI_RESOLV_WRAP(SYM) SYM
#endif /* PUKABI_ENABLE_RESOLV */

/* *** errno management ************************************ */

#define puk_abi_seterrno(VAL)    ( (*(puk_abi_errno_stub())) = (VAL) )
#define puk_abi_geterrno()       (*(puk_abi_errno_stub()))
#define puk_abi_real_errno()     (*(puk_abi_errno_real()))

/* *** memory management *********************************** */

/** a registered memory entry */
struct puk_mem_reg_s
{
  void*context;
  const void*ptr;
  size_t len;
  void*key;
  int refcount;
};

/** hook to register memory- returned value is used as key */
typedef void*(*puk_mem_register_t)  (void*context, const void*ptr, size_t len);
/** hook to unregister memory */
typedef void (*puk_mem_unregister_t)(void*context, const void*ptr, void*key);
/** sets handlers to register memory */
void puk_mem_set_handlers(puk_mem_register_t reg, puk_mem_unregister_t unreg);
/** asks for memory registration- does nothing if registration has been cached */
const struct puk_mem_reg_s*puk_mem_reg(void*context, const void*ptr, size_t len);
/** marks registration entry as 'unused' */
void puk_mem_unreg(const struct puk_mem_reg_s*reg);

typedef void(*puk_mem_invalidate_hook_t)(void*ptr, size_t size);
void puk_abi_mem_set_hook(puk_mem_invalidate_hook_t hook);


/* *** FD management *************************************** */

extern int  puk_abi_vfd_alloc(void);
extern void puk_abi_vfd_free(int abi_fd);

/* *** PukABI public 'API' ********************************* */


/** disable LD_PRELOAD to prevent forked processes (xterm, gdb) from runnig with PukABI
 */
extern void puk_abi_disable_preload(void);

/** sets the handler for a virtual symbol */
#define puk_abi_set_virtual(SYM, FUNC) \
  { PUK_ABI_SYM_GETDESC(SYM)()->ptr_virtual = (FUNC); }

/** sets the main spinlock handlers. Set to NULL to cancel locks handlers.
 */
extern void puk_abi_set_spinlock_handlers(void (*acquire)(void),
                                          void (*release)(void),
                                          int (*trylock)(void));
/** sets the errno location function */
extern void puk_abi_set_errno_handler(int*(*errno_handler)(void));

/** remove the errno location function handler */
extern void puk_abi_unset_errno_handler(void);


#endif /* PUK_ABI_H */

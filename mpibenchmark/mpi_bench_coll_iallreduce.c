/*
 * MadMPI benchmark
 * Copyright (C) 2015-2020 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include "mpi_bench_generic.h"

static void*recv_buf = NULL;

static void mpi_bench_coll_iallreduce_init(void*buf, size_t len)
{
  recv_buf = malloc(len);
}

static void mpi_bench_coll_iallreduce_finalize(void)
{
  free(recv_buf);
  recv_buf = NULL;
}

static void mpi_bench_coll_iallreduce_func(void*buf, size_t len)
{
  MPI_Request req;
  MPI_Iallreduce(buf, recv_buf, len, MPI_BYTE, MPI_BAND, MPI_COMM_WORLD, &req);
  MPI_Wait(&req, MPI_STATUS_IGNORE);
}

const struct mpi_bench_s mpi_bench_coll_iallreduce =
  {
    .label      = "mpi_bench_coll_iallreduce",
    .name       = "MPI iallreduce",
    .rtt        = MPI_BENCH_RTT_COLLECTIVE,
    .collective = 1,
    .init       = &mpi_bench_coll_iallreduce_init,
    .finalize   = &mpi_bench_coll_iallreduce_finalize,
    .server     = &mpi_bench_coll_iallreduce_func,
    .client     = &mpi_bench_coll_iallreduce_func
  };

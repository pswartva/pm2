/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * starts multiple nodes in the same process for simulation
 */

#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <assert.h>
#include <dlfcn.h>

#include "Puk.h"
#include "Puk-mult.h"

static const char*sim_root = NULL;
static const char*sim_binary = NULL;
static int sim_N = -1;

static int sim_argc = -1;
static char**sim_argv = NULL;


/* ********************************************************* */
/* helper functions for simulation */

int puk_mult_getsize(void)
{
  return sim_N;
}

static inline void puk_mult_attr_destructor(const char*label, const char*value)
{
  padico_free((char*)label);
  padico_free((char*)value);
}

PUK_HASHTABLE_TYPE(puk_mult_attr, const char*, const char*,
                   &puk_hash_string_default_hash, &puk_hash_string_default_eq,
                   &puk_mult_attr_destructor);

static struct
{
  pthread_mutex_t lock;
  struct puk_mult_attr_hashtable_s attrs;
  int in_barrier;
  pthread_cond_t barrier_cond;
  pthread_mutex_t exclusive;
} puk_mult;

void puk_mult_exclusive_lock(void)
{
  pthread_mutex_lock(&puk_mult.exclusive);
}

void puk_mult_exclusive_unlock(void)
{
  pthread_mutex_unlock(&puk_mult.exclusive);
}

void puk_mult_publish(const char*label, const char*value)
{
  pthread_mutex_lock(&puk_mult.lock);
  puk_mult_attr_hashtable_insert(&puk_mult.attrs, padico_strdup(label), padico_strdup(value));
  pthread_mutex_unlock(&puk_mult.lock);
}

char*puk_mult_lookup(const char*label)
{
  pthread_mutex_lock(&puk_mult.lock);
  const char*value = puk_mult_attr_hashtable_lookup(&puk_mult.attrs, label);
  pthread_mutex_unlock(&puk_mult.lock);
  return padico_strdup(value);
}

void puk_mult_barrier(void)
{
  pthread_mutex_lock(&puk_mult.lock);
  puk_mult.in_barrier++;
  assert(puk_mult.in_barrier <= sim_N);
  if(puk_mult.in_barrier == sim_N)
    {
      puk_mult.in_barrier = 0;
      pthread_cond_broadcast(&puk_mult.barrier_cond);
    }
  else
    {
      pthread_cond_wait(&puk_mult.barrier_cond, &puk_mult.lock);
    }
  pthread_mutex_unlock(&puk_mult.lock);
}

static void puk_mult_init(void)
{
  pthread_mutex_init(&puk_mult.lock, NULL);
  puk_mult_attr_hashtable_init(&puk_mult.attrs);
  puk_mult.in_barrier = 0;
  pthread_cond_init(&puk_mult.barrier_cond, NULL);
  pthread_mutex_init(&puk_mult.exclusive, NULL);
}

/* ********************************************************* */

static void*sim_runner(void*arg)
{
  int n = (uintptr_t)arg;

  char*objname = malloc(256);
  snprintf(objname, 256, "%s/lib/sim%d_%s", sim_root, n, sim_binary);

  fprintf(stderr, "# puk-mult: loading %s\n", objname);
  void*handle = dlopen(objname, RTLD_NOW | RTLD_GLOBAL);
  if(handle == NULL)
    {
      fprintf(stderr, "# cannot open object file %s (%s).\n", objname, dlerror());
      abort();
    }
  char*main_name = malloc(256);
  snprintf(main_name, 256, "sim%d_main", n);
  void*sym = dlsym(handle, main_name);
  if(sym == NULL)
    {
      fprintf(stderr, "# cannot find symbol '%s' in object file %s.\n", main_name, objname);
      abort();
    }
  fprintf(stderr, "# puk-mult: starting %s...\n", main_name);

  void(*smain)(int, char**) = sym;

  (*smain)(sim_argc, sim_argv);

  free(objname);
  free(main_name);
  return NULL;
}

int main(int argc, char*argv[])
{
  fprintf(stderr, "# puk-mult: init.\n");
  sim_argc = argc;
  sim_argv = argv;

  const char*s_N = getenv("PUK_MULT_N");
  if(s_N == NULL)
    {
      fprintf(stderr, "# cannot find PUK_MULT_N in environment.\n");
      abort();
    }
  sim_N = atoi(s_N);

  sim_root = getenv("PUK_MULT_ROOT");
  if(sim_root == NULL)
    {
      fprintf(stderr, "# cannot find PUK_MULT_ROOT in environment.\n");
      abort();
    }

  sim_binary = getenv("PUK_MULT_BINARY");
  if(sim_binary == NULL)
    {
      fprintf(stderr, "# cannot find PUK_MULT_BINARY in environment.\n");
      abort();
    }

  puk_mult_init();

  fprintf(stderr, "# puk-mult: starting N = %d instances.\n", sim_N);
  pthread_t*t = malloc(sizeof(pthread_t) * sim_N);
  int n;
  for(n = 0; n < sim_N; n++)
    {
      pthread_create(&t[n], NULL, &sim_runner, (void*)(uintptr_t)n);
    }
  for(n = 0; n < sim_N; n++)
    {
      pthread_join(t[n], NULL);
    }
  fprintf(stderr, "# puk-mult: exit.\n");

  return 0;
}

/* bench-Control.c
 */

#include "bench-common.h"
#include <Padico/Topology.h>
#include <Padico/NetSelector.h>
#include <Padico/PadicoTM.h>
#include <Padico/Module.h>

int bench_control_init(void);
int bench_control_run(int argc, char**argv);

PADICO_MODULE_DECLARE(bench_Control, bench_control_init, bench_control_run, NULL);

static char*recv_buffer = NULL; /**< buffer for receipt */
static int  recv_lenbuf = 0;    /**< len of above buffer */
static marcel_t recv_tid = NULL;
static int  num_trips   = 0;    /**< number of messages received */

static marcel_mutex_t mutex;
static marcel_cond_t cond;
static padico_topo_node_t peer;

static void bench_control_post_recv(char*buffer, int lenbuf)
{
  padico_out(40, "post recv- len=%d\n", lenbuf);
  marcel_mutex_lock(&mutex);
  recv_buffer = buffer;
  recv_lenbuf = lenbuf;
  recv_tid    = marcel_self();
  marcel_cond_broadcast(&cond);
  marcel_mutex_unlock(&mutex);
}

static void bench_control_wait(void)
{
  padico_out(50, "entering trips=%d.\n", num_trips);
  marcel_mutex_lock(&mutex);
  while(recv_buffer)
    {
      padico_out(20, "waiting synchro trips=%d...\n", num_trips);
      marcel_cond_wait(&cond, &mutex);
      padico_out(20, "unlocked synchro trips=%d...\n", num_trips);
    }
  padico_out(40, "stop\n");
  recv_lenbuf = 0;
  recv_buffer = NULL;
  marcel_mutex_unlock(&mutex);
  padico_out(60, "done..\n");
}

static void bench_control_send(void*buffer, int lenbuf)
{
  padico_control_msg_t msg = padico_control_msg_new();
  padico_control_msg_add_cmd(msg, "<bench-Control:packet>");
  padico_control_msg_add_part(msg, buffer, lenbuf);
  padico_control_msg_add_cmd(msg, "</bench-Control:packet>");
  padico_control_send_msg(peer, msg);
}
static void bench_control_packet_handler(puk_parse_entity_t e)
{
  puk_parse_entity_vect_t c = puk_parse_get_contained(e);
  assert(puk_parse_entity_vect_size(c) == 1);
  struct iovec part = puk_parse_getpart(puk_parse_entity_vect_at(c, 0));
  const char*bytes = part.iov_base;
  const size_t len = part.iov_len;

  marcel_mutex_lock(&mutex);
  while((!recv_buffer) || (len != recv_lenbuf))
    {
      padico_out(10, "Ooops! Waiting in handler- recv_lenbuf=%d; len=%d;\n",
                 recv_lenbuf, len);
      marcel_cond_wait(&cond, &mutex);
    }
  memcpy(recv_buffer, bytes, len);
  const marcel_t tid = recv_tid;
  recv_tid = NULL;
  recv_buffer = NULL;
  num_trips++;
  marcel_cond_broadcast(&cond);
  marcel_mutex_unlock(&mutex);
  marcel_yield_to(tid);

}

int bench_control_init(void)
{
  marcel_mutex_init(&mutex, NULL);
  marcel_cond_init(&cond, NULL);

  peer = bench_common_param.is_server?
    bench_common_param.client_node:
    bench_common_param.server_node;

  puk_xml_add_action((struct puk_tag_action_s){
    .xml_tag        = "bench-Control:packet",
    .start_handler  = NULL,
    .end_handler    = &bench_control_packet_handler,
    .required_level = PUK_TRUST_CONTROL
  });

  padico_tm_msleep(500);
  return 0;
}

static void control_server(char*buffer, int lenbuf)
{
  padico_out(20, "starting- len=%d...\n", lenbuf);
  bench_control_post_recv(buffer, lenbuf);
  bench_control_wait();
  bench_control_send(buffer, lenbuf);
  padico_out(20, "done- len=%d...\n", lenbuf);
}

static void control_client(char*buffer, int lenbuf)
{
  padico_out(20, "starting- len=%d...\n", lenbuf);
  bench_control_post_recv(buffer, lenbuf);
  bench_control_send(buffer, lenbuf);
  bench_control_wait();
  padico_out(20, "done- len=%d...\n", lenbuf);
}

static const struct bench_desc_s control_bench =
{
  "Control",
  &control_server,
  &control_client
};

int bench_control_run(int argc, char**argv)
{
  the_bench(&control_bench);
  return 0;
}

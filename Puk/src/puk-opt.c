/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Management of configuration options.
 * @ingroup Puk
 */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#include "Puk-internals.h"

#include "Module.h"

PADICO_MODULE_HOOK(Puk);

PADICO_MODULE_ATTR(display_env, "PUK_DISPLAY_ENV", "display all attributes at startup; default: false", bool, 0);

struct puk_opt_early_value_s
{
  const char*label;
  const char*value;
};
PUK_VECT_TYPE(puk_opt_early_value, struct puk_opt_early_value_s);

PUK_VECT_TYPE(puk_opt, struct puk_opt_s*);

static struct
{
  puk_opt_vect_t opts;
  puk_opt_early_value_vect_t early_values; /**< early option values, set before the attribute was fully defined */
  int display_env;
} puk_opt = { .opts = NULL, .early_values = NULL, .display_env = 0 };

static void puk_opt_display(const struct puk_opt_s*o);

/* ********************************************************* */

__PUK_SYM_INTERNAL
void padico_puk_opt_init(void)
{
  if(padico_module_attr_display_env_getvalue())
    {
      /* opts */
      padico_out(puk_verbose_always, "dumping Puk opts (%d options).\n", puk_opt_vect_size(puk_opt.opts));
      if((puk_opt.opts != NULL) && (!puk_opt_vect_empty(puk_opt.opts)))
        {
          puk_opt_vect_itor_t o;
          puk_vect_foreach(o, puk_opt, puk_opt.opts)
            {
              puk_opt_display(*o);
            }
        }
      /* early values */
      if((puk_opt.early_values != NULL) && (!puk_opt_early_value_vect_empty(puk_opt.early_values)))
        {
          padico_out(puk_verbose_always, "dumping Puk opts early values; a value is set before attribute is defined.\n");
          puk_opt_early_value_vect_itor_t e;
          puk_vect_foreach(e, puk_opt_early_value, puk_opt.early_values)
            {
              padico_out(puk_verbose_always, "label= %s; value = %s\n", e->label, e->value);
            }
        }
      puk_opt.display_env = 1;
    }
}

static void puk_opt_display(const struct puk_opt_s*o)
{
  char*v = puk_opt_value_to_string(o);
  padico_out(puk_verbose_always, "%s = %s (%s.%s, %s, \"%s\")\n",
             o->env_var, v, o->mod_name, o->label, puk_opt_type_name(o->type), o->desc);
  padico_free(v);
}

/* ** Puk options ****************************************** */

int puk_opt_get_num(void)
{
  if(puk_opt.opts == NULL)
    return 0;
  else
    return puk_opt_vect_size(puk_opt.opts);
}

struct puk_opt_s*puk_opt_get_by_index(int index)
{
  if(puk_opt.opts == NULL)
    return NULL;
  if((index < 0) || (index >= puk_opt_vect_size(puk_opt.opts)))
    return NULL;
  return puk_opt_vect_at(puk_opt.opts, index);
}

struct puk_opt_s*puk_opt_get_by_name(const char*name)
{
  if(puk_opt.opts == NULL)
    return NULL;
  puk_opt_vect_itor_t i;
  puk_vect_foreach(i, puk_opt, puk_opt.opts)
    {
      if(strcmp(name, (*i)->label) == 0)
        {
          return *i;
        }
    }
  return NULL;
}

int puk_opt_get_index(const struct puk_opt_s*opt)
{
  if(puk_opt.opts == NULL)
    return -1;
  puk_opt_vect_itor_t i;
  puk_vect_foreach(i, puk_opt, puk_opt.opts)
    {
      if(*i == opt)
        {
          return puk_opt_vect_rank(puk_opt.opts, i);
        }
    }
  return -1;
}

void puk_opt_value_from_string(struct puk_opt_s*opt, const char*v)
{
  switch(opt->type)
    {
    case puk_opt_type_bool:
      opt->value.as_bool = puk_opt_parse_bool(v);
      break;
    case puk_opt_type_int:
      {
        long i = strtol(v, NULL, 0);
        if((i != LONG_MIN) && (i != LONG_MAX))
          {
            opt->value.as_int = i;
          }
        else
          {
            padico_warning("invalid value for option %s; cannot parse '%s' as integer.\n", opt->env_var, v);
          }
      }
      break;
    case puk_opt_type_unsigned:
      {
        long long u = strtoll(v, NULL, 0);
        if((u >= 0) && (u < UINT_MAX))
          {
            opt->value.as_unsigned = u;
          }
        else
          {
            padico_warning("invalid value for option %s; cannot parse '%s' as unsigned.\n", opt->env_var, v);
          }
      }
      break;
    case puk_opt_type_string:
      opt->value.as_string = padico_strdup(v);
      break;
    }
}

char*puk_opt_value_to_string(const struct puk_opt_s*opt)
{
  padico_string_t s = padico_string_new();
  switch(opt->type)
    {
    case puk_opt_type_bool:
      padico_string_printf(s, "%s", opt->value.as_bool ? "true" : "false");
      break;
    case puk_opt_type_int:
      {
        padico_string_printf(s, "%d", opt->value.as_int);
      }
      break;
    case puk_opt_type_unsigned:
      {
        padico_string_printf(s, "%u", opt->value.as_unsigned);
      }
      break;
    case puk_opt_type_string:
      padico_string_printf(s, "%s", opt->value.as_string);
      break;
    }
  char*v = padico_strdup(padico_string_get(s));
  padico_string_delete(s);
  return v;
}

struct puk_opt_s*puk_opt_find(const char*mod_name, const char*label)
{
  if(puk_opt.opts == NULL)
    return NULL;
  puk_opt_vect_itor_t i;
  puk_vect_foreach(i, puk_opt, puk_opt.opts)
    {
      if((strcmp((*i)->label, label) == 0) && ((*i)->mod_name != NULL) && (strcmp((*i)->mod_name, mod_name) == 0))
        {
          return *i;
        }
    }
  return NULL;
}

static struct puk_opt_early_value_s*puk_opt_early_value_find(const char*mod_name, const char*label)
{
  if(puk_opt.early_values == NULL)
    return NULL;
  padico_string_t s = padico_string_new();
  padico_string_printf(s, "%s.%s", mod_name, label);
  puk_opt_early_value_vect_itor_t i;
  puk_vect_foreach(i, puk_opt_early_value, puk_opt.early_values)
    {
      if(strcmp(i->label, padico_string_get(s)) == 0)
        {
          padico_string_delete(s);
          return i;
        }
    }
  padico_string_delete(s);
  return NULL;
}

void puk_opt_setvalue(const char*mod_name, const char*label, const char*value)
{
  assert(value != NULL);
  struct puk_opt_s*opt = puk_opt_find(mod_name, label);
  if(opt != NULL)
    {
      puk_opt_value_from_string(opt, value);
      if(puk_opt.display_env)
        {
          puk_opt_display(opt);
        }
    }
  else
    {
      /* set early value */
      padico_string_t s = padico_string_new();
      padico_string_printf(s, "%s.%s", mod_name, label);
      struct puk_opt_early_value_s e = { .label = padico_strdup(padico_string_get(s)), .value = padico_strdup(value) };
      puk_opt_early_value_vect_push_back(puk_opt.early_values, e);
      padico_string_delete(s);
    }
}

void puk_opt_declare(struct puk_opt_s*opt)
{
  if(puk_opt.opts == NULL)
    {
      puk_opt.opts = puk_opt_vect_new();
    }
  if(puk_opt.early_values == NULL)
    {
      puk_opt.early_values = puk_opt_early_value_vect_new();
    }
  struct puk_opt_early_value_s*e = puk_opt_early_value_find(opt->mod_name, opt->label);
  if(e)
    {
      padico_out(puk_verbose_notice, "option %s = %s explicitely set by user.\n", e->label, e->value);
    }
  if(opt->env_var != NULL)
    {
      const char*v = getenv(opt->env_var);
      if(v != NULL)
        {
          if(e != NULL)
            {
              padico_warning("ignoring value for %s from environment since option %s is explicitely set by user.\n",
                             opt->env_var, e->label);
            }
          else
            {
              padico_out(puk_verbose_notice, "using value %s from environment for %s.\n", v, opt->env_var);
              puk_opt_value_from_string(opt, v);
            }
        }
    }
  puk_opt_vect_push_back(puk_opt.opts, opt);
  if(puk_opt.display_env)
    {
      puk_opt_display(opt);
    }
}

void puk_opt_remove(struct puk_opt_s*opt)
{
  assert(puk_opt.opts != NULL);
  puk_opt_vect_itor_t i;
  puk_vect_foreach(i, puk_opt, puk_opt.opts)
    {
      if(i && (opt == *i))
        {
          puk_opt_vect_erase(puk_opt.opts, i);
          break; /* cannot continue since vect has been modified */
        }
    }
}

const char*puk_opt_type_name(puk_opt_type_t t)
{
  switch(t)
    {
    case puk_opt_type_bool:
      return "bool";
      break;
    case puk_opt_type_int:
      return "int";
      break;
    case puk_opt_type_unsigned:
      return "unsigned";
      break;
    case puk_opt_type_string:
      return "string";
      break;
    }
  return "invalid";
}

int puk_opt_parse_bool(const char*s)
{
  if(s == NULL)
    return 0;
  if( (strcmp(s, "1") == 0) || (strcmp(s, "yes") == 0) || (strcmp(s, "y") == 0)|| (strcmp(s, "on") == 0) || (strcmp(s, "true") == 0) || (strcmp(s, "enabled") == 0) )
    return 1;
  if( (strcmp(s, "0") == 0) || (strcmp(s, "no") == 0) || (strcmp(s, "n") == 0) || (strcmp(s, "off") == 0) || (strcmp(s, "false") == 0) || (strcmp(s, "disabled") == 0) )
    return 0;
  padico_warning("cannot parse option '%s' as boolean.\n", s);
  return -1;
}

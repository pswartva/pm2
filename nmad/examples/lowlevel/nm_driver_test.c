/*
 * NewMadeleine
 * Copyright (C) 2011-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <assert.h>
#include <values.h>
 #include <ctype.h>

#include <nm_private.h>
#include <nm_minidriver.h>

#include <nm_minidriver_interface.h>

#include <Padico/Module.h>
#include <Padico/Puk.h>
#include <Padico/AddrDB.h>
#include <Padico/Topology.h>

static int nm_driver_test_init(void);
static int nm_driver_test_run(int argc, char**argv);
static void nm_driver_test_finalize(void);

PADICO_MODULE_DECLARE(nm_driver_test, nm_driver_test_init, nm_driver_test_run, nm_driver_test_finalize);

/* hash minidriver recptacles by status (void*) */
PUK_HASHTABLE_TYPE(nm_minidriver, void*, struct puk_receptacle_NewMad_minidriver_s*,
                   &puk_hash_pointer_default_hash, &puk_hash_pointer_default_eq, NULL);

#define NM_DRIVER_TEST_CHECK_RC(OP, RC)                 \
  {                                                     \
    if(((RC) != NM_ESUCCESS) && ((RC) != -NM_EAGAIN))   \
      NM_FATAL("nm_driver_test: op %s returned rc = %d (%s)", (OP), rc, nm_strerror(rc)); \
  }

/** assembly for a minidriver always available, used for rdv */
static const char assembly_minidriver_control[] =
  "<puk:composite id=\"minidriver_control\">"
  "  <puk:component id=\"0\" name=\"PSP-Control\"/>"
  "  <puk:component id=\"1\" name=\"Minidriver_PSP\">"
  "    <puk:uses iface=\"PadicoSimplePackets\" port=\"psp\" provider-id=\"0\" />"
  "  </puk:component>"
  "  <puk:entry-point iface=\"NewMad_minidriver\" port=\"minidriver\" provider-id=\"1\" />"
  "</puk:composite>";

static int rank = -1, nprocs = -1;
static nm_len_t max_size = NM_LEN_UNDEFINED;

struct nm_driver_test_component_s
{
  const struct nm_minidriver_iface_s*driver;
  struct nm_minidriver_properties_s props;
  puk_context_t context;
  struct puk_receptacle_NewMad_minidriver_s*trks;
  puk_instance_t*instances;
  struct nm_minidriver_hashtable_s trks_hash;
};

static struct nm_driver_test_s
{
  /* main component tested */
  struct nm_driver_test_component_s main;
  /* auxiliary component used for rdvs, if needed */
  struct nm_driver_test_component_s rdv;
  struct
  {
    struct iovec v;
    struct nm_data_s data;
  } send;
  struct
  {
    struct iovec v;
    struct nm_data_s data;
  } recv;
} nm_driver_test;

static inline char nm_driver_test_buffer_content(nm_len_t offset, int round)
{
  return 'a' + ((offset + round) % 26);
}

static void nm_driver_test_buffer_fill(char*p_buffer, nm_len_t len, int round)
{
  nm_len_t i = 0;
  for(i = 0; i < len; i++)
    {
      p_buffer[i] = nm_driver_test_buffer_content(i, round);
    }
}
static void nm_driver_test_buffer_clear(char*p_buffer, nm_len_t len)
{
  memset(p_buffer, 0, len);
}
static void nm_driver_test_buffer_control(const char*p_buffer, nm_len_t len, int round)
{
  int i = 0;
  for(i = 0; i < len; i++)
    {
      const char expected = nm_driver_test_buffer_content(i, round);
      if(p_buffer[i] != expected)
        {
          fprintf(stderr, "Bad data at byte %d: expected 0x%x (%c), received 0x%x (%c)\n",
                  i, expected, expected, p_buffer[i], p_buffer[i]);
          abort();
        }
    }
}

/* ********************************************************* */


static inline void nm_driver_test_recv_probe_any(void**_status)
{
  int rc = (*nm_driver_test.main.driver->recv_probe_any)(nm_driver_test.main.context, _status);
  NM_DRIVER_TEST_CHECK_RC("recv_probe_any", rc);
}

static inline int nm_driver_test_recv_cancel(struct puk_receptacle_NewMad_minidriver_s*r)
{
  return (*r->driver->recv_cancel)(r->_status);
}

#ifdef PIOMAN_MULTITHREAD
struct nm_driver_test_wait_s
{
  struct puk_receptacle_NewMad_minidriver_s*r;
  char*buf;
  size_t len;
};
static void*nm_driver_test_wait_any_worker(void*_arg)
{
  void*status = NULL;
  int rc = (*nm_driver_test.main.driver->recv_wait_any)(nm_driver_test.main.context, &status);
  NM_DRIVER_TEST_CHECK_RC("recv_wait_any", rc);
  if(rc == NM_ESUCCESS)
    {
      struct nm_driver_test_wait_s*p_wait = _arg;
      nm_minidriver_recv(p_wait->r, &nm_driver_test.main.props, p_wait->buf, p_wait->len);
    }
  return NULL;
}
#endif /* PIOMAN_MULTITHREAD */

static void nm_driver_test_init_component(struct nm_driver_test_component_s*p_driver_component, puk_component_t component)
{
  assert(component != NULL);

  /* ** get component context and driver */
  p_driver_component->context = puk_component_get_context(component, puk_iface_NewMad_minidriver(), NULL);
  if(!p_driver_component->context)
    {
      NM_FATAL("nm_driver_test: cannot find context for Minidriver facet in component %s. Cannot instantiate.\n",
               component->name);
    }
  p_driver_component->driver = puk_component_get_driver_NewMad_minidriver(component, NULL);

  /* ** get driver properties */
  p_driver_component->props = (struct nm_minidriver_properties_s){ 0 };
#ifdef NMAD_HWLOC
  p_driver_component->props.profile.cpuset = hwloc_bitmap_alloc();
#endif
  fprintf(stderr, "# nm_driver_test: getting properties...\n");
  (*p_driver_component->driver->getprops)(p_driver_component->context, &p_driver_component->props);
  const struct nm_minidriver_properties_s*props = &p_driver_component->props;
  if(props->capabilities.max_msg_size > 0)
    {
      max_size = props->capabilities.max_msg_size;
      fprintf(stderr, "# nm_driver_test: max message size for driver %s = %lu\n", component->name, max_size);
    }
  fprintf(stderr, "#     max_msg_size = %lu; supports_data = %d; supports_buf_send = %d; supports_buf_recv = %d; supports_wait_any = %d\n",
          props->capabilities.max_msg_size, props->capabilities.supports_data,
          props->capabilities.supports_buf_send, props->capabilities.supports_buf_recv, props->capabilities.supports_wait_any);
  fprintf(stderr, "#     prefers_wait_any = %d; supports_iovec = %d; supports_recv_any = %d; min_period = %d; trk_rdv = %d\n",
          props->capabilities.prefers_wait_any, props->capabilities.supports_iovec, props->capabilities.supports_recv_any,
          props->capabilities.min_period, props->capabilities.trk_rdv);

  /* ** initialize driver & get url */
  fprintf(stderr, "# nm_driver_test: initializing driver...\n");
  const void*drv_url = NULL;
  size_t url_size = 0;
  (*p_driver_component->driver->init)(p_driver_component->context, &drv_url, &url_size);
  fprintf(stderr, "# nm_driver_test: init done; url_size = %lu\n", url_size);
  fprintf(stderr, "# nm_driver_test: local url = ");
  int i;
  for(i = 0; i < url_size; i++)
    {
      const unsigned char*u = drv_url;
      if(isprint(u[i]))
        fprintf(stderr, "%c ", u[i]);
      else
        fprintf(stderr, "[0x%x] ", u[i]);
    }
  fprintf(stderr, " (%d bytes)\n", (int)url_size);

  const char*session = padico_topo_node_getsession(padico_topo_getlocalnode());
  fprintf(stderr, "# nm_driver_test: connecting nodes in session %s\n", session);
  nprocs = padico_na_size();
  p_driver_component->trks = malloc(nprocs * sizeof(struct puk_receptacle_NewMad_minidriver_s));
  p_driver_component->instances = malloc(nprocs * sizeof(puk_instance_t));
  nm_minidriver_hashtable_init(&p_driver_component->trks_hash);
  int k;
  for(k = 0; k < nprocs; k++)
    {
      const padico_topo_node_t n = padico_topo_session_getnodebyrank(session, k);
      if(padico_topo_getlocalnode() != n)
        {
          /* instantiate & resolve minidriver interface */
          p_driver_component->instances[k] = puk_component_instantiate(component);
          puk_instance_indirect_NewMad_minidriver(p_driver_component->instances[k], "minidriver", &p_driver_component->trks[k]);
          nm_minidriver_hashtable_insert(&p_driver_component->trks_hash, p_driver_component->trks[k]._status, &p_driver_component->trks[k]);
          /* ** connect */
          const char addr_name[] = "nm_driver_test";
          fprintf(stderr, "# nm_driver_test: connecting to node #%d: %s [%s]\n", k,
                  padico_topo_node_getname(n), (const char*)padico_topo_node_getuuid(n));
          padico_addrdb_publish(n, addr_name, NULL, 0, drv_url, url_size);
          const int peer_url_size = 1024;
          char peer_url[peer_url_size];
          padico_req_t req = padico_tm_req_new(NULL, NULL);
          padico_addrdb_get(n, addr_name, NULL, 0, &peer_url[0], peer_url_size, req);
          padico_tm_req_wait(req);
          if(p_driver_component->trks[k].driver->connect_async != NULL)
            {
              fprintf(stderr, "# nm_driver_test: using async connect...\n");
              (*p_driver_component->trks[k].driver->connect_async)(p_driver_component->trks[k]._status, peer_url, url_size);
            }
          else
            {
              (*p_driver_component->trks[k].driver->connect)(p_driver_component->trks[k]._status, peer_url, url_size);
            }
        }
      else
        {
          p_driver_component->trks[k].driver = NULL;
          rank = k;
        }
    }
  for(k = 0; k < nprocs; k++)
    {
      const padico_topo_node_t n = padico_topo_session_getnodebyrank(session, k);
      if(padico_topo_getlocalnode() != n)
        {
          /* wait for async connects */
          if(p_driver_component->trks[k].driver->connect_async != NULL)
            {
              fprintf(stderr, "# nm_driver_test: waiting connection completion for node #%d: %s [%s]\n",
                      k, padico_topo_node_getname(n), (const char*)padico_topo_node_getuuid(n));
              if(p_driver_component->trks[k].driver->connect_wait != NULL)
                {
                  (*p_driver_component->trks[k].driver->connect_wait)(p_driver_component->trks[k]._status);
                }
            }
        }
    }
}

/** initial sync; ensures driver is ready to receive */
static void nm_driver_test_sync(struct nm_driver_test_component_s*p_driver_component)
{
  int k;
  const char*session = padico_topo_node_getsession(padico_topo_getlocalnode());
  for(k = 0; k < nprocs; k++)
    {
      const padico_topo_node_t n = padico_topo_session_getnodebyrank(session, k);
      if(padico_topo_getlocalnode() != n)
        {
          /* immediate send/recv to check that driver is really ready to accept send/recv */
          char dummy;
          nm_minidriver_isend(&p_driver_component->trks[k], &p_driver_component->props, &dummy, sizeof(dummy));
          int rcs = 1;
          int rcr = 1;
          int i;
          for(i = 0; i < 10000; i++)
            {
              if(rcs != NM_ESUCCESS)
                {
                  rcs = nm_minidriver_spoll(&p_driver_component->trks[k], &p_driver_component->props, &dummy, sizeof(dummy));
                }
            }
          nm_minidriver_irecv(&p_driver_component->trks[k], &p_driver_component->props, &dummy, sizeof(dummy));
          do
            {
              if(rcs != NM_ESUCCESS)
                {
                  rcs = nm_minidriver_spoll(&p_driver_component->trks[k], &p_driver_component->props, &dummy, sizeof(dummy));
                }
              if(rcr != NM_ESUCCESS)
                {
                  rcr = nm_minidriver_rpoll(&p_driver_component->trks[k], &p_driver_component->props, &dummy, sizeof(dummy));
                }
            }
          while(rcs != NM_ESUCCESS || rcr != NM_ESUCCESS);
        }
    }
}

static int nm_driver_test_init(void)
{
  const char*s_component = padico_getenv("NM_DRIVER_TEST");
  fprintf(stderr, "# nm_driver_test: using driver: NM_DRIVER_TEST = %s\n", s_component);
  if(s_component == NULL)
    {
      NM_FATAL("nm_driver_test: NM_DRIVER_TEST is not set. Please give the component name with -DNM_DRIVER_TEST<compoentn_name>, e.g. -DNM_DRIVER_TEST=Minidriver_tcp.\n");
    }
  /* ** resolve driver */
  puk_component_t p_component0 = puk_component_resolve(s_component);
  puk_component_t component = NULL;
  if(p_component0 == NULL)
    {
      NM_FATAL("nm_driver_test: cannot resolve component '%s'\n", s_component);
    }
  puk_context_t p_context0 = puk_component_get_context(p_component0, puk_iface_NewMad_minidriver(), NULL);
  if(p_context0 == NULL)
    {
      fprintf(stderr, "# nm_driver_test: no context in component.\n");
      puk_component_t p_composite = puk_composite_new("nm_driver_test");
      puk_context_t p_context0 = puk_context_new(p_component0, p_composite, NULL);

      puk_composite_add_entrypoint(p_composite, puk_iface_NewMad_minidriver(), "minidriver",
                                   NULL /* provider_port */, p_context0->id);

      /* plug ibverbs component if needed */
      puk_iface_t ibverbs_iface = puk_iface_register("NewMad_ibverbs");
      puk_component_conn_t p_ibverbs_conn = puk_context_conn_lookup(p_context0, ibverbs_iface, NULL);
      if(p_ibverbs_conn != NULL)
        {
          fprintf(stderr, "# nm_driver_test: component depends on NewMad_ibverbs common.\n");
          puk_component_t p_ibverbs_component = puk_component_resolve("NewMad_ibverbs_common");
          puk_context_t p_ibverbs_context = puk_context_new(p_ibverbs_component, p_composite, NULL);
          puk_context_conn_connect(p_context0, NULL, p_ibverbs_context->id, ibverbs_iface, "ibverbs");
        }
      /* plug a PSP component if needed */
      puk_iface_t psp_iface = puk_iface_register("PadicoSimplePackets");
      puk_component_conn_t p_psp_conn = puk_context_conn_lookup(p_context0, psp_iface, NULL);
      if(p_psp_conn != NULL)
        {
          fprintf(stderr, "# nm_driver_test: component depends on PSP interface- pluging PSP-Control.\n");
          puk_component_t p_psp_component = puk_component_resolve("PSP-Control");
          puk_context_t p_psp_context = puk_context_new(p_psp_component, p_composite, NULL);
          puk_context_conn_connect(p_context0, NULL, p_psp_context->id, psp_iface, "psp");
        }
      component = p_composite;
    }
  else
    {
      fprintf(stderr, "# nm_driver_test: component has a context.\n");
      component = puk_composite_duplicate(p_component0, "nm_driver_test");
    }
  fprintf(stderr, "# nm_driver_test: component loaded.\n");
  padico_string_t s0 = puk_component_serialize_id(p_component0, "nm_driver_test");
  fprintf(stderr, "# nm_driver_test: driver component: \n%s\n", padico_string_get(s0));
  padico_string_delete(s0);
  padico_string_t s = puk_component_serialize_id(component, "nm_driver_test");
  fprintf(stderr, "# nm_driver_test: contextualized component: \n%s\n", padico_string_get(s));
  padico_string_delete(s);

  nm_driver_test_init_component(&nm_driver_test.main, component);

  if(nm_driver_test.main.props.capabilities.trk_rdv)
    {
      fprintf(stderr, "# nm_driver_test: main driver needs rdv. Instantiating %s as a rdv support.\n", assembly_minidriver_control);
      puk_component_t rdv_component = puk_component_parse(assembly_minidriver_control);
      nm_driver_test_init_component(&nm_driver_test.rdv, rdv_component);
      nm_driver_test_sync(&nm_driver_test.rdv);
    }
  else
    {
      nm_driver_test_sync(&nm_driver_test.main);
    }

  return 0;
}

static int nm_driver_test_run(int argc, char**argv)
{
  fprintf(stderr, "# nm_driver_test: running....\n");
  const int default_max_size = 128 * 1024;
  int buf_size = ((max_size > 0) && (max_size < default_max_size)) ? max_size : default_max_size;
  const int next = (rank + 1) % nprocs;
  const int prev = (rank - 1 + nprocs) % nprocs;
  fprintf(stderr, "# nm_driver_test: rank = %d; prev = %d; next = %d\n", rank, prev, next);
  void*buf = malloc(buf_size);
  int i;

  /* blocking send/recv, all sizes */
  fprintf(stderr, "# ##\n");
  fprintf(stderr, "# nm_driver_test: testing blocking send/recv...\n");
  for(i = 1; i < buf_size; i++)
    {
      if((i == 1) || (i % 1024 == 0))
        {
          fprintf(stderr, "\n");
          fprintf(stderr, "# nm_driver_test: blocking send/recv; msg size = %d... ", i);
        }
      if(i % 32 == 0)
        {
          fprintf(stderr, ".");
          fflush(stderr);
        }
      if(rank % 2 == 0)
        {
          nm_driver_test_buffer_fill(buf, i, i);
          if(nm_driver_test.main.props.capabilities.trk_rdv)
            {
              nm_minidriver_send_rdv(&nm_driver_test.main.trks[prev], &nm_driver_test.main.props, buf, i,
                                     &nm_driver_test.rdv.trks[prev], &nm_driver_test.main.props);
            }
          else
            {
              nm_minidriver_send(&nm_driver_test.main.trks[prev], &nm_driver_test.main.props, buf, i);
            }
          nm_driver_test_buffer_clear(buf, i);
          if(nm_driver_test.main.props.capabilities.trk_rdv)
            {
              nm_minidriver_recv_rdv(&nm_driver_test.main.trks[next], &nm_driver_test.main.props, buf, i,
                                     &nm_driver_test.rdv.trks[next], &nm_driver_test.main.props);
            }
          else
            {
              nm_minidriver_recv(&nm_driver_test.main.trks[next], &nm_driver_test.main.props, buf, i);
            }
          nm_driver_test_buffer_control(buf, i, i);
        }
      else
        {
          nm_driver_test_buffer_clear(buf, i);
          if(nm_driver_test.main.props.capabilities.trk_rdv)
            {
              nm_minidriver_recv_rdv(&nm_driver_test.main.trks[next], &nm_driver_test.main.props, buf, i,
                                     &nm_driver_test.rdv.trks[next], &nm_driver_test.main.props);
            }
          else
            {
              nm_minidriver_recv(&nm_driver_test.main.trks[next], &nm_driver_test.main.props, buf, i);
            }
          nm_driver_test_buffer_control(buf, i, i);
          if(nm_driver_test.main.props.capabilities.trk_rdv)
            {
              nm_minidriver_send_rdv(&nm_driver_test.main.trks[prev], &nm_driver_test.main.props, buf, i,
                                     &nm_driver_test.rdv.trks[prev], &nm_driver_test.main.props);
            }
          else
            {
              nm_minidriver_send(&nm_driver_test.main.trks[prev], &nm_driver_test.main.props, buf, i);
            }
        }
    }

  /* blocking send/recv, stream */
  fprintf(stderr, "# ##\n");
  fprintf(stderr, "# nm_driver_test: testing streams of blocking send/recv...\n");
  for(i = 1; i < buf_size; i++)
    {
      if((i == 1) || (i % 1024 == 0))
        {
          fprintf(stderr, "\n");
          fprintf(stderr, "# nm_driver_test: streams of blocking send/recv; msg size = %d... ", i);
        }
      if(i % 32 == 0)
        {
          fprintf(stderr, ".");
          fflush(stderr);
        }
      if(rank % 2 == 0)
        {
          if(nm_driver_test.main.props.capabilities.trk_rdv)
            {
              nm_driver_test_buffer_fill(buf, i, 1);
              nm_minidriver_send_rdv(&nm_driver_test.main.trks[prev], &nm_driver_test.main.props, buf, i,
                                     &nm_driver_test.rdv.trks[prev], &nm_driver_test.main.props);
              nm_driver_test_buffer_fill(buf, i, 2);
              nm_minidriver_send_rdv(&nm_driver_test.main.trks[prev], &nm_driver_test.main.props, buf, i,
                                     &nm_driver_test.rdv.trks[prev], &nm_driver_test.main.props);
              nm_driver_test_buffer_fill(buf, i, 3);
              nm_minidriver_send_rdv(&nm_driver_test.main.trks[prev], &nm_driver_test.main.props, buf, i,
                                     &nm_driver_test.rdv.trks[prev], &nm_driver_test.main.props);
              nm_driver_test_buffer_fill(buf, i, 4);
              nm_minidriver_send_rdv(&nm_driver_test.main.trks[prev], &nm_driver_test.main.props, buf, i,
                                     &nm_driver_test.rdv.trks[prev], &nm_driver_test.main.props);
              nm_driver_test_buffer_clear(buf, i);
              nm_minidriver_recv_rdv(&nm_driver_test.main.trks[next], &nm_driver_test.main.props, buf, i,
                                     &nm_driver_test.rdv.trks[next], &nm_driver_test.main.props);
              nm_driver_test_buffer_control(buf, i, 5);
            }
          else
            {
              nm_driver_test_buffer_fill(buf, i, 1);
              nm_minidriver_send(&nm_driver_test.main.trks[prev], &nm_driver_test.main.props, buf, i);
              nm_driver_test_buffer_fill(buf, i, 2);
              nm_minidriver_send(&nm_driver_test.main.trks[prev], &nm_driver_test.main.props, buf, i);
              nm_driver_test_buffer_fill(buf, i, 3);
              nm_minidriver_send(&nm_driver_test.main.trks[prev], &nm_driver_test.main.props, buf, i);
              nm_driver_test_buffer_fill(buf, i, 4);
              nm_minidriver_send(&nm_driver_test.main.trks[prev], &nm_driver_test.main.props, buf, i);
              nm_driver_test_buffer_clear(buf, i);
              nm_minidriver_recv(&nm_driver_test.main.trks[next], &nm_driver_test.main.props, buf, i);
              nm_driver_test_buffer_control(buf, i, 5);
            }
        }
      else
        {
          if(nm_driver_test.main.props.capabilities.trk_rdv)
            {
              nm_driver_test_buffer_clear(buf, i);
              nm_minidriver_recv_rdv(&nm_driver_test.main.trks[next], &nm_driver_test.main.props, buf, i,
                                     &nm_driver_test.rdv.trks[next], &nm_driver_test.main.props);
              nm_driver_test_buffer_control(buf, i, 1);
              nm_minidriver_recv_rdv(&nm_driver_test.main.trks[next], &nm_driver_test.main.props, buf, i,
                                     &nm_driver_test.rdv.trks[next], &nm_driver_test.main.props);
              nm_driver_test_buffer_control(buf, i, 2);
              nm_minidriver_recv_rdv(&nm_driver_test.main.trks[next], &nm_driver_test.main.props, buf, i,
                                     &nm_driver_test.rdv.trks[next], &nm_driver_test.main.props);
              nm_driver_test_buffer_control(buf, i, 3);
              nm_minidriver_recv_rdv(&nm_driver_test.main.trks[next], &nm_driver_test.main.props, buf, i,
                                     &nm_driver_test.rdv.trks[next], &nm_driver_test.main.props);
              nm_driver_test_buffer_control(buf, i, 4);
              nm_driver_test_buffer_fill(buf, i, 5);
              nm_minidriver_send_rdv(&nm_driver_test.main.trks[prev], &nm_driver_test.main.props, buf, i,
                                     &nm_driver_test.rdv.trks[prev], &nm_driver_test.main.props);
            }
          else
            {
              nm_driver_test_buffer_clear(buf, i);
              nm_minidriver_recv(&nm_driver_test.main.trks[next], &nm_driver_test.main.props, buf, i);
              nm_driver_test_buffer_control(buf, i, 1);
              nm_minidriver_recv(&nm_driver_test.main.trks[next], &nm_driver_test.main.props, buf, i);
              nm_driver_test_buffer_control(buf, i, 2);
              nm_minidriver_recv(&nm_driver_test.main.trks[next], &nm_driver_test.main.props, buf, i);
              nm_driver_test_buffer_control(buf, i, 3);
              nm_minidriver_recv(&nm_driver_test.main.trks[next], &nm_driver_test.main.props, buf, i);
              nm_driver_test_buffer_control(buf, i, 4);
              nm_driver_test_buffer_fill(buf, i, 5);
              nm_minidriver_send(&nm_driver_test.main.trks[prev], &nm_driver_test.main.props, buf, i);
            }
        }
    }

  /* non-blocking send/recv */
  fprintf(stderr, "# ##\n");
  if(!nm_driver_test.main.props.capabilities.trk_rdv)
    {
      fprintf(stderr, "# nm_driver_test: testing non-blocking send/recv...\n");
      for(i = 1; i < buf_size; i++)
        {
          if((i == 1) || (i % 1024 == 0))
            {
              fprintf(stderr, "\n");
              fprintf(stderr, "# nm_driver_test: non-blocking send/recv; msg size = %d... ", i);
            }
          if(i % 32 == 0)
            {
              fprintf(stderr, ".");
              fflush(stderr);
            }
          nm_minidriver_irecv(&nm_driver_test.main.trks[next], &nm_driver_test.main.props, buf, i);
          nm_minidriver_isend(&nm_driver_test.main.trks[prev], &nm_driver_test.main.props, buf, i);
          int rrc = -1, src = -1;
          do
            {
              if(rrc != 0)
                rrc = nm_minidriver_rpoll(&nm_driver_test.main.trks[next], &nm_driver_test.main.props, buf, i);
              if(src != 0)
                src = nm_minidriver_spoll(&nm_driver_test.main.trks[prev], &nm_driver_test.main.props, buf, i);
            }
          while((rrc != 0) || (src != 0));
        }
    }

  /* recv any */
  fprintf(stderr, "# ##\n");
  fprintf(stderr, "# nm_driver_test: testing recv_probe_any...\n");
  if(nm_driver_test.main.props.capabilities.supports_recv_any && nm_driver_test.main.driver->recv_probe_any)
    {
      for(i = 1; i < buf_size; i = i * 1.1 + 1)
        {
          const nm_len_t size = 1;
          fprintf(stderr, "# nm_driver_test: testing recv_probe_any; size = %d\n", i);
          nm_minidriver_isend(&nm_driver_test.main.trks[prev], &nm_driver_test.main.props, buf, size);
          int rc = -1;
          void*status = NULL;
          do
            {
              if(status == NULL)
                nm_minidriver_recv_probe_any(nm_driver_test.main.driver, nm_driver_test.main.context, &status);
              if(rc != 0)
                rc = nm_minidriver_spoll(&nm_driver_test.main.trks[prev], &nm_driver_test.main.props, buf, size);
            }
          while((status == NULL) || (rc != 0));
          struct puk_receptacle_NewMad_minidriver_s*trk = nm_minidriver_hashtable_lookup(&nm_driver_test.main.trks_hash, status);
          assert(trk != NULL);
          nm_minidriver_recv(trk, &nm_driver_test.main.props, buf, size);
          fprintf(stderr, "# nm_driver_test: recv_probe_any ok.\n");
        }
    }
  else
    {
      fprintf(stderr, "# nm_driver_test: recv_probe_any not available.\n");
    }

#ifdef PIOMAN_MULTITHREAD
  /* wait any*/
  fprintf(stderr, "# ##\n");
  fprintf(stderr, "# nm_driver_test: testing recv_wait_any...\n");
  if(nm_driver_test.main.props.capabilities.supports_wait_any && nm_driver_test.main.driver->recv_wait_any)
    {
      for(i = 1; i < buf_size; i = i * 1.1 + 1)
        {
          const nm_len_t size = 1;
          fprintf(stderr, "# nm_driver_test: testing recv_wait_any; size = %d\n", i);
          nm_minidriver_isend(&nm_driver_test.main.trks[prev], &nm_driver_test.main.props, buf, size);
          struct nm_driver_test_wait_s wait =
            {
              .r = &nm_driver_test.main.trks[next],
              .buf = buf,
              .len = size
            };
          piom_thread_t tid;
          piom_thread_create(&tid, NULL, &nm_driver_test_wait_any_worker, &wait);
          nm_minidriver_send(&nm_driver_test.main.trks[prev], &nm_driver_test.main.props, buf, size);
          piom_thread_join(tid);
          fprintf(stderr, "# nm_driver_test: recv_wait_any ok.\n");
        }
    }
  else
    {
      fprintf(stderr, "# nm_driver_test: recv_wait_any not available.\n");
    }
#else /* PIOMAN_MULTITHREAD */
  fprintf(stderr, "# nm_driver_test: pioman disabled or non-threaded; not testing recv_wait_any.\n");
#endif /* PIOMAN_MULTITHREAD */

  /* cancel */
  fprintf(stderr, "# ##\n");
  fprintf(stderr, "# nm_driver_test: testing recv_cancel...\n");
  if(nm_driver_test.main.driver->recv_cancel)
    {
      const nm_len_t size = 4;
      nm_minidriver_irecv(&nm_driver_test.main.trks[next], &nm_driver_test.main.props, buf, size);
      int rc = nm_minidriver_recv_cancel(&nm_driver_test.main.trks[next], &nm_driver_test.main.props);
      fprintf(stderr, "# nm_driver_test: after recv- recv_cancel rc = %d (%s)\n", rc, nm_strerror(rc));
      rc = nm_minidriver_recv_cancel(&nm_driver_test.main.trks[next], &nm_driver_test.main.props);
      fprintf(stderr, "# nm_driver_test: not after recv- recv_cancel rc = %d (%s)\n", rc, nm_strerror(rc));
    }
  else
    {
      fprintf(stderr, "# nm_driver_test: recv_cancel not available; not tested.\n");
    }

  return 0;
}

static void nm_driver_test_finalize(void)
{
  int k;
  for(k = 0; k < nprocs; k++)
    {
      const char*session = padico_topo_node_getsession(padico_topo_getlocalnode());
      const padico_topo_node_t n = padico_topo_session_getnodebyrank(session, k);
      if(padico_topo_getlocalnode() != n)
        {
          fprintf(stderr, "# nm_driver_test: disconnect node #%d [%s]\n", k, (const char*)padico_topo_node_getuuid(n));
          if(nm_driver_test.main.trks[k].driver->disconnect)
            {
              (*nm_driver_test.main.trks[k].driver->disconnect)(nm_driver_test.main.trks[k]._status);
            }
          fprintf(stderr, "# nm_driver_test: destroy instance #%d\n", k);
          puk_instance_destroy(nm_driver_test.main.instances[k]);
        }
    }
  fprintf(stderr, "# nm_driver_test: close driver...\n");
  if(nm_driver_test.main.driver->close)
    {
      (*nm_driver_test.main.driver->close)(nm_driver_test.main.context);
    }
  fprintf(stderr, "# nm_driver_test: driver closed.\n");
}

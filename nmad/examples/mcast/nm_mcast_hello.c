/*
 * NewMadeleine
 * Copyright (C) 2021-2022 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <stdlib.h>
#include <stdio.h>

#include <nm_launcher_interface.h>
#include <nm_coll_interface.h>
#include <nm_mcast_interface.h>


enum prio_policy {
  NO_PRIO,
  PRIO,
  REVERSED_PRIO
};


static void usage(const char*prog_name)
{
  printf("Simple program to see if mcast works in a simple case.\n");
  printf("Usage: %s [prio|reversed-prio]\n", prog_name);
}


int main(int argc, char**argv)
{
  int worldsize = 0;
  int rank = -1;
  const char msg[] = "Hello, world!";
  const nm_len_t msg_size = strlen(msg) + 1;
  const nm_tag_t tag = 0x02;
  const int hlen = 0;
  enum prio_policy prio_policy = NO_PRIO;

  if(argc == 2)
    {
      if(strcmp(argv[1], "prio") == 0)
        {
          prio_policy = PRIO;
        }
      else if(strcmp(argv[1], "reversed-prio") == 0)
        {
          prio_policy = REVERSED_PRIO;
        }
      else
        {
          usage(argv[0]);
          return EXIT_FAILURE;
        }
    }

  /* generic nmad init */
  nm_launcher_init(&argc, argv);
  nm_launcher_get_size(&worldsize);
  nm_launcher_get_rank(&rank);

  if(worldsize == 1)
    {
      fprintf(stderr, "We need several nodes.\n");

      nm_launcher_exit();
      return EXIT_FAILURE;
    }

  nm_comm_t p_comm = nm_comm_world("nm_mcast_hello");
  nm_mcast_service_t mcast_service = nm_mcast_init(p_comm);

  if(rank == 0)
    {
      /* send mcast */
      int* dests = malloc(sizeof(int) * (worldsize - 1));
      int* prios = NULL;
      if(prio_policy != NO_PRIO)
        {
          prios = malloc(sizeof(int) * (worldsize - 1));
        }
      int i;
      for(i = 1; i < worldsize; i++)
        {
          dests[i-1] = i;
          if(prio_policy == PRIO)
            {
              prios[i-1] = i;
            }
          else if(prio_policy == REVERSED_PRIO)
            {
              prios[i-1] = worldsize - i;
            }
        }
      struct nm_data_s data;
      nm_data_contiguous_build(&data, (char*) msg, msg_size);
      nm_mcast_send(mcast_service, p_comm, dests, prios, worldsize-1, tag, &data, hlen, NM_COLL_TREE_DEFAULT);
      free(dests);
      if(prio_policy != NO_PRIO)
        {
          free(prios);
        }
    }
  else
    {
      /* receive data from an mcast with a point-to-point receive */
      nm_session_t p_session = nm_comm_get_session(p_comm);
      char* buffer = malloc(msg_size);
      nm_sr_request_t request;
#if 1 // detailled recv:
      nm_sr_recv_init(p_session, &request);
      nm_sr_recv_match(p_session, &request, NM_ANY_GATE, tag, NM_TAG_MASK_FULL);
      nm_sr_recv_post(p_session, &request);
      nm_sr_recv_data_wait(p_session, &request);
      struct nm_data_s data;
      nm_data_contiguous_build(&data, buffer, hlen);
      int rc = nm_sr_recv_peek(p_session, &request, &data);
      assert(rc == NM_ESUCCESS);
      nm_sr_recv_offset(p_session, &request, hlen);
      nm_sr_recv_unpack_contiguous(p_session, &request, buffer, msg_size);
#else // generic recv:
      nm_sr_irecv(p_session, 0, tag, buffer, msg_size, &request);
#endif
      nm_sr_rwait(p_session, &request);
      printf("Received message: %s\n", buffer);
      free(buffer);
    }

  /* generic nmad termination */
  nm_coll_barrier(p_comm, 0xF2);
  nm_mcast_finalize(mcast_service);
  nm_comm_destroy(p_comm);
  nm_launcher_exit();
  return EXIT_SUCCESS;
}

/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Padico control channel
 * @ingroup PadicoControl
 */

#include <Padico/Puk.h>
#include <Padico/Module.h>
#include <Padico/PadicoTM.h>
#include <Padico/Topology.h>

#include "PadicoControl-internals.h"

static int padico_control_init(void);
static void padico_control_finalize(void);

PADICO_MODULE_DECLARE(PadicoControl, padico_control_init, NULL, padico_control_finalize,
                      "Topology");

/* ********************************************************* */


PUK_VECT_TYPE(padico_string, padico_string_t);

/** A control message.
 * Message is built in vect 'content':
 *   first vect entry is XML message; following entries are binary attachments.
 * Ready to send on network is contained in iovec 'v'.
 */
struct padico_control_msg_s
{
  struct padico_string_vect_s content;
  struct iovec*v;
  size_t size;
  int offset; /**< cumulated size of parts */
};

PUK_LIST_TYPE(padico_pending_req,
              padico_topo_node_t node;
              padico_req_id_t req_id;
              );

static struct
{
  padico_topo_network_t network;     /**< the control channel as a network */
  padico_tasklet_t event_channel;  /**< tasklet used as event channel for control event */
  puk_hashtable_t controlers;      /**< key: puk_instance_t; value : padico_topo_node_vect_t */
  puk_component_t selector;          /**< selector for the control channel */
  struct padico_pending_req_list_s pending_reqs; /**< pending requests sent to a remote node */
  volatile int shutdown;
} na_control = { .shutdown = 0};


static void na_control_event_listener(void*arg);
static void na_control_ping_handler(puk_parse_entity_t e);
static void na_control_rc_handler(puk_parse_entity_t e);
static void na_control_request_handler(puk_parse_entity_t e);



/* ********************************************************* */

static int padico_control_init(void)
{
  puk_iface_register("PadicoControl");
  na_control.network = padico_topo_network_create("ControlNetwork", PADICO_TOPO_SCOPE_NODE, PADICO_TOPO_UUID_SIZE, AF_UNSPEC);
  padico_topo_node_bind(padico_topo_getlocalnode(), na_control.network, padico_topo_node_getuuid(padico_topo_getlocalnode()));
  na_control.controlers = puk_hashtable_new_ptr();
  na_control.selector = NULL;

  na_control.event_channel = padico_tasklet_new("PadicoControl");
  padico_tasklet_acquire(na_control.event_channel);
  padico_tasklet_subscribe(na_control.event_channel, &na_control_event_listener);
  padico_tasklet_release(na_control.event_channel);
  padico_pending_req_list_init(&na_control.pending_reqs);

  puk_xml_add_action((struct puk_tag_action_s)
                     {
                       .xml_tag        = "PadicoControl:request",
                       .start_handler  = NULL,
                       .end_handler    = &na_control_request_handler,
                       .required_level = PUK_TRUST_CONTROL,
                       .help           = "for internal use by PadicoControl"
                      });
  puk_xml_add_action((struct puk_tag_action_s)
                     {
                       .xml_tag        = "PadicoControl:rc",
                       .start_handler  = NULL,
                       .end_handler    = &na_control_rc_handler,
                       .required_level = PUK_TRUST_CONTROL,
                       .help           = "for internal use by PadicoControl"
                      });
  puk_xml_add_action((struct puk_tag_action_s)
                     {
                       .xml_tag        = "ping",
                       .start_handler  = NULL,
                       .end_handler    = &na_control_ping_handler,
                       .required_level = PUK_TRUST_CONTROL,
                       .help           = "ping node"
                      });

  padico_control_group_init();
  padico_control_multi_init();

  return 0;
}

static void na_control_controlers_destructor(void*_key, void*_data)
{
  padico_topo_node_vect_t peers = _data;
  padico_topo_node_vect_delete(peers);
}
static void padico_control_finalize(void)
{
  padico_control_group_finalize();
  padico_control_multi_finalize();
  padico_tasklet_acquire(na_control.event_channel);
  padico_tasklet_unsubscribe(na_control.event_channel, &na_control_event_listener);
  padico_tasklet_release(na_control.event_channel);
  padico_tasklet_destroy(na_control.event_channel);
  na_control.event_channel = NULL;
  na_control.selector = NULL;
  padico_pending_req_list_destroy(&na_control.pending_reqs);
  puk_xml_delete_action("PadicoControl:request");
  puk_xml_delete_action("PadicoControl:rc");
  puk_xml_delete_action("ping");
  puk_hashtable_delete(na_control.controlers, &na_control_controlers_destructor);
}

/* ********************************************************* */

padico_control_msg_t padico_control_msg_new(void)
{
  padico_control_msg_t msg = padico_malloc(sizeof(struct padico_control_msg_s));
  padico_string_vect_init(&msg->content);
  padico_string_t v0 = padico_string_new();
  padico_string_vect_push_back(&msg->content, v0);
  msg->v = NULL;
  msg->size = 0;
  msg->offset = 0;
  return msg;
}

void padico_control_msg_destroy(padico_control_msg_t msg)
{
  padico_string_vect_itor_t i;
  puk_vect_foreach(i, padico_string, &msg->content)
    {
      padico_string_delete(*i);
    }
  padico_string_vect_destroy(&msg->content);
  if(msg->v != NULL)
    {
      padico_free(msg->v);
      msg->v = NULL;
      }
  padico_free(msg);
}

void padico_control_msg_add_part(padico_control_msg_t msg, const void*text, size_t len)
{
  padico_string_t part = padico_string_new();
  padico_string_resize(part, len);
  memcpy(padico_string_get(part), text, len);
  padico_string_vect_push_back(&msg->content, part);
  padico_control_msg_add_cmd(msg, "<puk:part offset=\"%d\" len =\"%d\"/>", msg->offset, len);
  msg->offset += len;
}

int padico_control_msg_get_iovec_size(padico_control_msg_t msg)
{
  assert(msg->v != NULL);
  return padico_string_vect_size(&msg->content);
}

const struct iovec*padico_control_msg_get_iovec(padico_control_msg_t msg)
{
  const int n = padico_string_vect_size(&msg->content);
  struct iovec*v = padico_malloc(sizeof(struct iovec) * (n + 1));
  int i;
  for(i = 0; i < n; i++)
    {
      padico_string_t s = padico_string_vect_at(&msg->content, i);
      v[i].iov_base = padico_string_get(s);
      v[i].iov_len = padico_string_size(s);
      msg->size += padico_string_size(s);
    }
  v[n].iov_base = NULL;
  v[n].iov_len = -1;
  msg->v = v;
  return v;
}

size_t padico_control_msg_get_size(padico_control_msg_t msg)
{
  assert(msg->v != NULL);
  return msg->size;
}

struct iovec padico_control_msg_get_bytes(padico_control_msg_t msg)
{
  const struct iovec*v = padico_control_msg_get_iovec(msg);
  const size_t size = padico_control_msg_get_size(msg);
  char*bytes = padico_malloc(size);
  int offset = 0;
  while(v->iov_base != NULL)
    {
      memcpy(bytes + offset, v->iov_base, v->iov_len);
      offset += v->iov_len;
      v++;
    }
  return (struct iovec){ bytes, size };
}

padico_string_t padico_control_msg_get_cmd(padico_control_msg_t msg)
{
  return padico_string_vect_at(&msg->content, 0);
}


/* ***** Control API **************************************** */

int padico_control_send_msg(padico_topo_node_t node, padico_control_msg_t msg)
{
  puk_instance_t controler = NULL;
  struct puk_receptacle_PadicoControl_s r = { .driver = NULL };
  assert(node != NULL);
  padico_control_event_disable();
  padico_control_event_flush();
  /* resolve selector */
  if((!na_control.selector) && (!na_control.shutdown))
    {
      const char*name = padico_getattr("PADICO_CONTROL_SELECTOR");
      if(name && strlen(name) > 0)
        {
          padico_print("resolving control selector... %s\n", name);
          na_control.selector = puk_component_resolve(name);
        }
    }
  /* ask route to the selector */
  if(na_control.selector != NULL)
    {
      const struct padico_control_selector_driver_s*selector =
        puk_component_get_driver_ControlSelector(na_control.selector, NULL);
      if(selector == NULL || selector->router == NULL)
        {
          if(na_control.shutdown)
            {
              padico_control_event_enable();
              padico_control_msg_destroy(msg);
              return -1;
            }
          padico_fatal("component %s does not provide a ControlSelector facet.\n",
                       na_control.selector->name);
        }
      controler = (*selector->router)(node);
      if(controler != NULL)
        puk_instance_indirect_PadicoControl(controler, NULL, &r);
      if(controler == NULL || r.driver == NULL)
        {
          padico_control_event_enable();
          padico_control_msg_destroy(msg);
          return -1;
        }
    }
  else if(na_control.shutdown)
    {
      /* we can safely ignore oneway messages while shuting down */
      padico_control_event_enable();
      padico_control_msg_destroy(msg);
      return -1;
    }
  else
    {
      padico_fatal("no ControlSelector registered.\n");
    }
  /* send the request */
  padico_out(40, "sending to node=%s\n", padico_topo_node_getname(node));
  if(r.driver->send_common)
    {
      (*r.driver->send_common)(r._status, node, msg);
    }
  else
    {
      padico_fatal("control driver provides no 'send' function!\n");
    }
  padico_control_msg_destroy(msg);
  padico_control_event_enable();
  return 0;
}

void padico_control_send_ext(padico_topo_node_t node, const char*cmd, uint32_t req_id)
{
  if(na_control.shutdown && (req_id == PADICO_REQ_ID_NONE))
    {
      return;
    }
  padico_control_msg_t msg = padico_control_msg_new();
  if(req_id > PADICO_REQ_ID_NONE)
    {
      /* register the request as pending */
      padico_pending_req_t pending_req = padico_pending_req_new();
      pending_req->node = node;
      pending_req->req_id = req_id;
      padico_pending_req_list_push_back(&na_control.pending_reqs, pending_req);
      padico_control_msg_add_cmd(msg, "<PadicoControl:request req_id=\"%d\" from=\"%s\">",
                                 req_id, padico_topo_node_getuuid(padico_topo_getlocalnode()));
      padico_control_msg_add_cmd(msg, cmd);
      padico_control_msg_add_cmd(msg, "</PadicoControl:request>", req_id);
    }
  else
    {
      padico_control_msg_add_cmd(msg, cmd);
    }
  if(node == padico_topo_getlocalnode())
    {
      struct iovec v = padico_control_msg_get_bytes(msg);
      padico_control_deliver_message(NULL, padico_topo_getlocalnode(), v.iov_base, v.iov_len);
      padico_control_msg_destroy(msg);
    }
  else
    {
      int err = padico_control_send_msg(node, msg);
      if(err != 0 && req_id != PADICO_REQ_ID_NONE)
        {
          /* we can safely ignore oneway messages to disappeared nodes.
           * we cannot ignore messages that expect a reply.
           */
          padico_warning("node=%s is unreachable- request = %s\n", padico_topo_node_getname(node), cmd);
          /* node lost- immediate completion */
          padico_rc_t rc = padico_rc_error("node unreachable.\n");
          padico_tm_req_notify(req_id, rc);
        }
    }
}

static void na_control_notify(padico_topo_node_t node, uint32_t req_id, padico_rc_t rc)
{
  const char*msg = rc?(padico_rc_gettext(rc)):NULL;
  padico_string_t request = padico_string_new();
  if(msg == NULL)
    {
      padico_string_printf(request, "<PadicoControl:rc req_id=\"%i\" rc=\"%i\"/>\n", req_id, rc);
    }
  else
    {
      padico_string_printf(request, "<PadicoControl:rc req_id=\"%i\" rc=\"%i\">%s</PadicoControl:rc>\n", req_id, rc, msg);
    }
  padico_out(40, "req_id=%d- notifying to node=%s.\n", req_id, padico_topo_node_getname(node));
  padico_control_send_oneway(node, padico_string_get(request));
  padico_string_delete(request);
  padico_out(40, "req_id=%d- done.\n", req_id);
}


/** Deliver an incoming control message from an component to PadicoControl
 * @note this function takes ownership of data pointed to by 'msg'
 */
void padico_control_deliver_message(puk_instance_t controler, padico_topo_node_t from, char*cmd, size_t size)
{
  /* incoming request */
  struct padico_control_event_s*event = padico_malloc(sizeof(struct padico_control_event_s));
  event->kind = PADICO_CONTROL_EVENT_PROCESSING;
  assert(cmd != NULL);
  assert(size >= strlen(cmd));
  event->PROCESSING.node = from;
  event->PROCESSING.cmd  = cmd;
  event->PROCESSING.size = size;
  padico_tasklet_schedule(padico_control_get_event_channel(), event, "processing incoming message");
  padico_out(40, "done.\n");
}

padico_tasklet_t padico_control_get_event_channel(void)
{
  return na_control.event_channel;
}

/** Generates initial events for new subscribers to get the current state.
 * @pre event channel disabled
 */
void padico_control_gen_init_events(padico_event_listener_t l)
{
  puk_hashtable_enumerator_t e = puk_hashtable_enumerator_new(na_control.controlers);
  puk_instance_t controler = NULL;
  padico_topo_node_vect_t nodes = NULL;
  puk_hashtable_enumerator_next2(e, &controler, &nodes);
  while(controler != NULL)
    {
      struct padico_control_event_s event;
      event.kind = PADICO_CONTROL_EVENT_INIT_CONTROLER;
      event.INIT_CONTROLER.controler = controler;
      event.INIT_CONTROLER.nodes = nodes;
      (*l)(&event);
      puk_hashtable_enumerator_next2(e, &controler, &nodes);
    }
  puk_hashtable_enumerator_delete(e);
}

static void na_control_ping_handler(puk_parse_entity_t e)
{
  const char*name = puk_parse_get_text(e);
  padico_print("ping %s\n", name?:"(local)");
  if(name && (strlen(name) > 0))
    {
      padico_topo_node_t node = padico_topo_getnodebyname(name);
      if(node)
        {
          const char cmd[] = "<ping/>";
          padico_req_t req = padico_control_send(node, cmd);
          padico_rc_t rc = padico_tm_req_wait(req);
          puk_parse_set_rc(e, rc);

        }
      else
        {
          puk_parse_set_rc(e, padico_rc_error("unkonwn node"));
        }
    }
  else
    {
      puk_parse_set_rc(e, padico_rc_ok());
    }
}

static void na_control_request_handler(puk_parse_entity_t e)
{
  const char*s_req_id = puk_parse_getattr(e, "req_id");
  const char*s_from = puk_parse_getattr(e, "from");
  padico_topo_uuid_t uuid = (padico_topo_uuid_t)s_from;
  padico_topo_node_t from = padico_topo_getnodebyuuid(uuid);
  while(!from)
    {
       /* Node is unknown */
       /* Topology announcement must be in control event tasklet */
      padico_control_event_flush();
      from = padico_topo_getnodebyuuid(uuid);
    }
  const int req_id = atoi(s_req_id);
  puk_parse_entity_vect_t c = puk_parse_get_contained(e);
  assert(puk_parse_entity_vect_size(c) == 1);
  padico_rc_t rc = puk_parse_get_rc(puk_parse_entity_vect_at(c, 0));
  padico_out(30, "req_id=%d- notifying rc=%d (%s)\n",
             req_id, (rc==NULL)?0:rc->rc, (rc==NULL)?"null":padico_string_get(rc->msg));
  na_control_notify(from, req_id, rc);
}

static void na_control_rc_handler(puk_parse_entity_t e)
{
  const char*s_req_id = puk_parse_getattr(e, "req_id");
  const char*s_rc = puk_parse_getattr(e, "rc");
  const int req_id = atoi(s_req_id);
  const char*text = puk_parse_get_text(e);
  if((text != NULL) && (strlen(text) == 0))
    text = NULL;
  padico_rc_t rc = padico_rc_create(atoi(s_rc), text);
  padico_out(40, "req_id=%d; received RC (%s)\n", req_id, text?:"null");
  padico_pending_req_itor_t req_itor;
  PUK_LIST_FIND(padico_pending_req, req_itor, &na_control.pending_reqs, (req_itor->req_id == req_id));
  if(req_itor != NULL)
    {
      padico_pending_req_list_remove(&na_control.pending_reqs, req_itor);
      padico_pending_req_delete(req_itor);
    }
  else
    {
      padico_warning("received reply for non-pending request.\n");
    }
  padico_tm_req_notify(req_id, rc);
  puk_parse_set_content(e, rc);
}


/* ********************************************************* */

static void na_control_event_listener(void*arg)
{
  padico_control_event_t event = (struct padico_control_event_s*)arg;
  switch(event->kind)
    {
    case PADICO_CONTROL_EVENT_PLUG_CONTROLER:
      {
        puk_instance_t controler = event->PLUG_CONTROLER.controler;
        padico_out(20, "plug event- controler=%s\n", controler->component->name);
        padico_topo_node_vect_t peers = padico_topo_node_vect_new();
        puk_hashtable_insert(na_control.controlers, controler, peers);
      }
      break;

    case PADICO_CONTROL_EVENT_UNPLUG_CONTROLER:
      {
        puk_instance_t controler = event->UNPLUG_CONTROLER.controler;
        padico_out(20, "unplug event- controler=%p\n", controler);
        padico_topo_node_vect_t peers = puk_hashtable_lookup(na_control.controlers, controler);
        puk_hashtable_remove(na_control.controlers, controler);
        if(peers)
          {
            padico_topo_node_vect_delete(peers);
          }
      }
      break;

    case PADICO_CONTROL_EVENT_PLUG_SELECTOR:
      {
        puk_component_t selector = event->PLUG_SELECTOR.selector;
        padico_out(20, "plug event- selector=%s\n", selector->name);
        if(na_control.selector == NULL || na_control.selector == selector)
          {
            na_control.selector = selector;
          }
        else
          {
            padico_warning("new selector %s loaded while %s is already in use.\n",
                           selector->name, na_control.selector->name);
          }
      };
      break;

    case PADICO_CONTROL_EVENT_UNPLUG_SELECTOR:
      {
        puk_component_t selector = event->UNPLUG_SELECTOR.selector;
        padico_out(20, "unplug event- selector=%s\n", selector->name);
        if(na_control.selector == selector)
          {
            na_control.selector = NULL;
            padico_setattr("PADICO_CONTROL_SELECTOR", ""); /* prevent automatic reloading while shuting down */
            na_control.shutdown = 1;
          }
      };
      break;

    case PADICO_CONTROL_EVENT_NEW_NODE:
      {
        padico_topo_node_vect_t peers = puk_hashtable_lookup(na_control.controlers, event->NODE.controler);
        padico_topo_node_t node = event->NODE.node;
        padico_topo_uuid_t uuid = padico_topo_node_getuuid(node);
        padico_topo_node_bind(node, na_control.network, uuid);
        if(peers != NULL)
          {
            padico_out(20, "node event- new node *%s* served by %s\n",
                       padico_topo_node_getname(node),
                       event->NODE.controler->component->name);
            padico_topo_node_vect_push_back(peers, node);
          }
        else
          {
            padico_fatal("cannot add node *%s*- controler not found.\n",
                         padico_topo_node_getname(node));
          }
      }
      break;

    case PADICO_CONTROL_EVENT_DELETE_NODE:
      {
        padico_out(20, "node event- delete node *%s*\n", padico_topo_node_getname(event->NODE.node));
        padico_topo_node_disconnect(event->NODE.node);
        padico_pending_req_itor_t req_itor = padico_pending_req_list_begin(&na_control.pending_reqs);
        while(req_itor != NULL)
          {
            if(req_itor->node == event->NODE.node)
              {
                padico_print("deleting pending request 0x%04x to lost node.\n", req_itor->req_id);
                padico_rc_t rc = padico_rc_error("node disconnected.\n");
                padico_tm_req_notify(req_itor->req_id, rc);
                /*
                  padico_pending_req_list_remove(&na_control.pending_reqs, req_itor);
                  padico_pending_req_itor_t req2 = req_itor;
                  padico_pending_req_delete(req2);
                */
                req_itor = padico_pending_req_list_next(req_itor);
              }
            else
              {
                req_itor = padico_pending_req_list_next(req_itor);
              }
          }
      }
      break;

    case PADICO_CONTROL_EVENT_PROCESSING:
      {
        padico_out(30, "cmd=%s- starting\n",  event->PROCESSING.cmd);
        struct puk_parse_entity_s pe = puk_xml_parse_buffer(event->PROCESSING.cmd,  event->PROCESSING.size, PUK_TRUST_CLUSTER);
        padico_out(30, "parsing ok\n");
        padico_free(event->PROCESSING.cmd);
        event->PROCESSING.cmd = NULL;
        padico_rc_delete(pe.rc);
        padico_out(40, "exiting.\n");
      }
      break;

    case PADICO_CONTROL_EVENT_SHUTDOWN:
      na_control.shutdown = 1;
      break;

    default:
      padico_fatal("unexpected event in listener (kind=%d).\n", event->kind);
      break;
    }
}

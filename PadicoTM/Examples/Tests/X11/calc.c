#include <X11/Xlib.h>
#include <stdio.h>
#include <string.h>

/* ================================================= */
/* === Gestion de la calculatrice proprement dite == */
/* ================================================= */

#define MAX_DIGITS         8

static struct {
  char representation[MAX_DIGITS+1];
  char image[MAX_DIGITS+2];
  unsigned longueur;
  int negatif;
} nombre;

static char operateur,  /* par convention a '\0' quand absent */
  operation;            /* memorise l'operation a effectuer */
static long operande_gauche;

void calc_init()
{
  nombre.representation[0] = '\0';
  nombre.longueur = 0;
  nombre.negatif = 0;
  operateur = operation = '\0';
}

void calc_retour_arriere()
{
  if(operateur != '\0')
    operateur = '\0';
  else if(nombre.longueur > 0) {
    nombre.longueur--;
    nombre.representation[nombre.longueur] = '\0';
    if(nombre.longueur == 0)
      nombre.negatif = 0;
  }
}

void calc_changer_signe()
{
  if(operateur == '\0' && nombre.longueur)
    nombre.negatif ^= 1;
}

void calc_maj_image()
{
  if(nombre.longueur) {
    if(nombre.negatif)
      strcpy(nombre.image, "-");
    else
      strcpy(nombre.image, "");
    strcat(nombre.image, nombre.representation);
  } else
    strcpy(nombre.image, "0");
}

long calc_valeur()
{
  calc_maj_image();
  return atol(nombre.image);
}

void calc_calcule()
{
  long operande_droite;

  if(operation != '\0') {
    operande_droite = calc_valeur();
    switch(operation) {
    case '+' : {
      operande_gauche = operande_gauche + operande_droite;
      break;
    }
    case '-' : {
      operande_gauche = operande_gauche - operande_droite;
      break;
    }
    case '*' : {
      operande_gauche = operande_gauche * operande_droite;
      break;
    }
    case '/' : {
      operande_gauche = operande_gauche / operande_droite;
      break;
    }
    default : ;
    }
    operation = '\0';
    if(operande_gauche < 0) {
      sprintf(nombre.representation, "%ld", -operande_gauche);
      nombre.negatif = 1;
    } else {
      nombre.negatif = 0;
      sprintf(nombre.representation, "%ld", operande_gauche);
    }
    nombre.longueur = strlen(nombre.representation);
  }
}

void calc_operateur(char op)
{
  if(operateur == '\0') {
    calc_calcule();
    operateur = op;
  }
}

void calc_chiffre(char chiffre)
{
  if(operateur != '\0') {
    operande_gauche = calc_valeur();
    operation = operateur;
    operateur = '\0';
    nombre.representation[0] = '\0';
    nombre.longueur = 0;
    nombre.negatif = 0;
  }
  if(nombre.longueur < MAX_DIGITS && !(nombre.longueur == 0 && chiffre == '0')) {
    nombre.representation[nombre.longueur++] = chiffre;
    nombre.representation[nombre.longueur] = '\0';
  }
}

/* ================================ */
/* === Gestion de l'interface X === */
/* ================================ */

/* Variables (malheureusement) globales... */
static Display *dpy;
Window win;

/* Petites macros utiles */
#define min(a,b)           ((a) < (b) ? (a) : (b))
#define max(a,b)           ((a) > (b) ? (a) : (b))

/* Definition des dimensions */
#define DIGIT_WIDTH        8
#define FONT_HEIGHT        12
#define FONT_WIDTH         24
#define BORDER             10
#define DISPLAY_WIDTH      ((MAX_DIGITS+1)*DIGIT_WIDTH+10)
#define DISPLAY_HEIGHT     (FONT_HEIGHT+10)
#define KEYPAD_INTERSPC    8
#define KEYPAD_COLS        4
#define KEYPAD_ROWS        5
#define KEYPAD_HEIGHT      ((KEYPAD_ROWS*(FONT_HEIGHT+10+KEYPAD_INTERSPC))-KEYPAD_INTERSPC)
#define KEYPAD_WIDTH       ((KEYPAD_COLS*(FONT_WIDTH+10+KEYPAD_INTERSPC))-KEYPAD_INTERSPC)
#define CALC_HEIGHT        (BORDER*3+DISPLAY_HEIGHT+KEYPAD_HEIGHT)
#define CALC_WIDTH         (BORDER*2+max(DISPLAY_WIDTH,KEYPAD_WIDTH))
#define KEYPAD_X           ((DISPLAY_WIDTH > KEYPAD_WIDTH ? (DISPLAY_WIDTH-KEYPAD_WIDTH) : 0)/2+BORDER)
#define KEYPAD_Y           (2*BORDER+DISPLAY_HEIGHT)
#define DISPLAY_X          ((KEYPAD_WIDTH > DISPLAY_WIDTH ? (KEYPAD_WIDTH-DISPLAY_WIDTH) : 0)/2+BORDER)
#define DISPLAY_Y          BORDER

/* Couleurs utilisees */
typedef struct {
        unsigned char rouge, vert, bleu;
} couleur_t;

enum {
  BLANC,
  NOIR,
  GRIS,
  ROUGE,
  ORANGE,
  BLEU,
  BLEU_CLAIR,
  VERT,
  _NB_COULEURS
};

static couleur_t couleurs[_NB_COULEURS] = {
  { 255, 255, 255 }, /* blanc */
  { 0, 0, 0 },       /* noir */
  { 128, 128, 128 }, /* gris */
  { 255, 0, 0 },     /* rouge */
  { 255, 128, 0 },   /* orange */
  { 0, 0, 255 },     /* bleu */
  { 64, 128, 255 },   /* bleu clair */
  { 0, 255, 0 }    /* VERT */
};

/* Definition des touches */
enum {
  T_OFF, T_C, T_CE,
  T_1, T_2, T_3, T_MUL,
  T_4, T_5, T_6, T_DIV,
  T_7, T_8, T_9, T_SUB,
  T_0, T_INV, T_EQ, T_ADD,
  _NB_TOUCHES
};

static char *labels[_NB_TOUCHES] = {
  "OFF", "C", "CE",
  "1", "2", "3", "*",
  "4", "5", "6", "/",
  "7", "8", "9", "-",
  "0", "+/-", "=", "+"
};

/* Sous-fenetres */
static Window subwin[_NB_TOUCHES+1]; /* +1 pour l'ecran d'affichage */

static GC gcontexts[_NB_TOUCHES+1], gcontexts_inv[_NB_TOUCHES];

typedef struct {
  int foreground, background;
} aspect_t;

static aspect_t aspect[_NB_TOUCHES+1] = {
  { NOIR, ROUGE },        /* OFF */
  { ORANGE, GRIS },       /* C */
  { ROUGE, GRIS },        /* CE */
  { NOIR, BLANC },        /* 1 */
  { NOIR, BLANC },        /* 2 */
  { NOIR, BLANC },        /* 3 */
  { BLEU, GRIS },         /* x */
  { NOIR, BLANC },        /* 4 */
  { NOIR, BLANC },        /* 5 */
  { NOIR, BLANC },        /* 6 */
  { BLEU, GRIS },         /* / */
  { NOIR, BLANC },        /* 7 */
  { NOIR, BLANC },        /* 8 */
  { NOIR, BLANC },        /* 9 */
  { BLEU, GRIS },         /* - */
  { NOIR, BLANC },        /* 0 */
  { NOIR, BLANC },        /* +/- */
  { BLEU, GRIS },         /* = */
  { BLEU, GRIS },         /* + */
  { VERT, NOIR }          /* ecran */
};

typedef struct {
  unsigned x, y;
} coord_t;

static coord_t coord[_NB_TOUCHES] = {
  { 0, 0 },           { 2, 0 }, { 3, 0 },
  { 0, 1 }, { 1, 1 }, { 2, 1 }, { 3, 1 },
  { 0, 2 }, { 1, 2 }, { 2, 2 }, { 3, 2 },
  { 0, 3 }, { 1, 3 }, { 2, 3 }, { 3, 3 },
  { 0, 4 }, { 1, 4 }, { 2, 4 }, { 3, 4 }
};

static int touche_enfoncee = -1;

long code_couleur(int couleur)
{
  XColor color;

  color.red = couleurs[couleur].rouge * 255;
  color.green = couleurs[couleur].vert * 255;
  color.blue = couleurs[couleur].bleu * 255;
  XAllocColor(dpy, XDefaultColormap(dpy, DefaultScreen(dpy)), &color);

  return color.pixel;
}

GC contexte_graphique(Window win, int foreground, int background)
{
  XGCValues gcv;

  gcv.foreground = code_couleur(foreground);
  gcv.background = code_couleur(background);
  gcv.line_width = 1;
  gcv.font = XLoadFont(dpy, "fixed");
  gcv.function = GXcopy;

  return XCreateGC(dpy, win, GCForeground | GCBackground | GCLineWidth | GCFont | GCFunction, &gcv);
}

GC contexte_graphique_d_inversion(Window win)
{
  XGCValues gcv;

  gcv.function = GXinvert;

  return XCreateGC(dpy, win, GCFunction, &gcv);
}

void init_contextes_graphiques()
{
  int i;

  for(i=0; i<_NB_TOUCHES+1; i++) {
    gcontexts[i] = contexte_graphique(subwin[i],
                                      aspect[i].foreground,
                                      aspect[i].background);
    if(i != _NB_TOUCHES) /* uniquement pour le clavier */
      gcontexts_inv[i] = contexte_graphique_d_inversion(subwin[i]);
  }
}

void redessiner_touches()
{
  int i;

  for(i=0; i<_NB_TOUCHES; i++) {
    XClearWindow(dpy, subwin[i]);
    XDrawImageString(dpy, subwin[i], gcontexts[i],
                5, FONT_HEIGHT+5, labels[i], strlen(labels[i]));
    if(touche_enfoncee == i)
      XFillRectangle(dpy, subwin[i], gcontexts_inv[i],
                     0, 0, FONT_WIDTH+10, FONT_HEIGHT+10);
  }
  XFlush(dpy);
}

void rafraichir_ecran()
{
  int i;
  char buf[2] = { '\0', '\0' };

  calc_maj_image();
  XClearWindow(dpy, subwin[_NB_TOUCHES]);
  for(i=0; i<strlen(nombre.image); i++)
    XDrawImageString(dpy, subwin[_NB_TOUCHES], gcontexts[_NB_TOUCHES],
                DISPLAY_WIDTH-5-DIGIT_WIDTH*(strlen(nombre.image)-i),
                FONT_HEIGHT+5,
                nombre.image+i,
                1);
  XFlush(dpy);
}

void enfoncement_bouton(int touche)
{
  touche_enfoncee = touche;
  redessiner_touches();
}

void creer_fenetres()
{
  int i;
  GC gc;

  win = XCreateSimpleWindow(dpy, DefaultRootWindow(dpy), 0, 0, CALC_WIDTH, CALC_HEIGHT, 2, code_couleur(NOIR), code_couleur(BLEU_CLAIR));
  XStoreName(dpy, win, "Calculatrice");

  for(i=0; i<_NB_TOUCHES; i++) {
    subwin[i] = XCreateSimpleWindow(dpy, win,
                                    KEYPAD_X+coord[i].x*(FONT_WIDTH+10+KEYPAD_INTERSPC),
                                    KEYPAD_Y+coord[i].y*(FONT_HEIGHT+10+KEYPAD_INTERSPC),
                                    FONT_WIDTH+10,
                                    FONT_HEIGHT+10,
                                    1,
                                    code_couleur(ORANGE),
                                    code_couleur(aspect[i].background));
    XSelectInput(dpy, subwin[i], ExposureMask | ButtonPressMask | ButtonReleaseMask | LeaveWindowMask );
  }

  subwin[_NB_TOUCHES] = XCreateSimpleWindow(dpy, win,
                                            DISPLAY_X,
                                            DISPLAY_Y,
                                            DISPLAY_WIDTH,
                                            DISPLAY_HEIGHT,
                                            1,
                                            code_couleur(NOIR),
                                            code_couleur(aspect[_NB_TOUCHES].background));

  XSelectInput(dpy, subwin[_NB_TOUCHES], ExposureMask);

  XMapWindow(dpy, win);
  XMapSubwindows(dpy, win);

  init_contextes_graphiques();
}

void detruire_fenetres()
{
  XDestroyWindow(dpy, win);
}

int touche_correspondant_a(Window win)
{
  int i = 0;

  while(subwin[i] != win) i++;
  return i;
}

int main()
{
  XEvent evt;
  int touche;

  dpy = XOpenDisplay(0);

  calc_init();

  creer_fenetres();

  while(1) {
    XNextEvent(dpy, &evt);
    switch(evt.type) {
    case Expose : {
      while(XCheckTypedEvent(dpy, Expose, &evt)) ;
      /* Ici, on ne fait pas dans le d�tail, on redessine tout... */
      redessiner_touches();
      rafraichir_ecran();
      break;
    }
    case ButtonPress : {
      touche = touche_correspondant_a(evt.xany.window);
      enfoncement_bouton(touche);
      break;
    }
    case LeaveNotify :
      if(touche_enfoncee==touche_correspondant_a(evt.xany.window))
        {
          touche_enfoncee = -1;
          redessiner_touches();
        }
      break;

    case ButtonRelease : {
      switch(touche_enfoncee) {
      case T_OFF : {
        detruire_fenetres();
        exit(0);
      }
      case T_C : {
        calc_retour_arriere();
        rafraichir_ecran();
        break;
      }
      case T_CE : {
        calc_init();
        rafraichir_ecran();
        break;
      }
      case T_0 :
      case T_1 :
      case T_2 :
      case T_3 :
      case T_4 :
      case T_5 :
      case T_6 :
      case T_7 :
      case T_8 :
      case T_9 : {
        calc_chiffre(labels[touche_enfoncee][0]);
        rafraichir_ecran();
        break;
      }
      case T_ADD :
      case T_SUB :
      case T_MUL :
      case T_DIV : {
        calc_operateur(labels[touche_enfoncee][0]);
        rafraichir_ecran();
        break;
      }
      case T_INV : {
        calc_changer_signe();
        rafraichir_ecran();
        break;
      }
      case T_EQ : {
        calc_calcule();
        rafraichir_ecran();
        break;
      }
      default : ;
      }
      touche_enfoncee = -1;
      redessiner_touches();
    }
    default : ;
    }
  }
}

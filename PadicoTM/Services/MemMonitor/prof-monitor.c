/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Profiling monitor: dynamically display profiling counters
 * @ingroup ProfMonitor
 */

/** @defgroup ProfMonitor
 * @author Alexandre Denis
 */

#include <sys/time.h>

#include <Padico/Puk.h>
#include <Padico/PadicoTM.h>
#include <Padico/Module.h>

#define PADICO_PROF_MONITOR_PERIOD 10 /* in seconds */

static int padico_prof_monitor_init(void);
static void padico_prof_monitor_finalize(void);

PADICO_MODULE_DECLARE(ProfMonitor, padico_prof_monitor_init, NULL, padico_prof_monitor_finalize);

static struct
{
  int period;
  padico_tm_bgthread_pool_t pool;
  volatile int interrupt;
} padico_prof_monitor =
  {
    .period = PADICO_PROF_MONITOR_PERIOD,
    .interrupt = 0
  };

static void*padico_prof_monitor_worker(void*_arg)
{
  while(!padico_prof_monitor.interrupt)
    {
      fprintf(stderr, "# %s: ProfMonitor: dumping profiling info\n", padico_hostname());
      puk_profile_dump();
      puk_sleep(padico_prof_monitor.period);
    }
  return NULL;
}

static int padico_prof_monitor_init(void)
{
  const char*s_period = getenv("PADICO_PROF_MONITOR_PERIOD");
  if(s_period != NULL)
    {
      padico_prof_monitor.period = atoi(s_period);
      if(padico_prof_monitor.period <= 0)
        {
          padico_fatal("invalid period = %d\n", padico_prof_monitor.period);
        }
    }
  padico_out(puk_verbose_notice, "loading background profiling monitor; period = %d seconds.\n",
             padico_prof_monitor.period);
  padico_prof_monitor.pool = padico_tm_bgthread_pool_create("ProfMonitor");
  padico_tm_bgthread_start(padico_prof_monitor.pool, &padico_prof_monitor_worker, NULL, "ProfMonitor");
  return 0;
}

static void padico_prof_monitor_finalize(void)
{
  padico_prof_monitor.interrupt = 1;
  padico_tm_bgthread_pool_wait(padico_prof_monitor.pool);
}

#! /bin/bash

# run networks benchmarks for queue types given in args (or all if none given)

args="$*"

arch=$(uname -m)

qs="atomic nblfq looqueue wcq simqueue koganpetrank crturn lcrq msqueue crdoublelink wfqueue bitnext lineararray scq2"

if [ "x${args}" != "x" ]; then
    qs="${args}"
    echo "# using queue list from command line: ${qs}"
fi

export CXX="g++ -std=c++17"

TIMEOUT=40
MAXTHREADS=64 # ## william: 16; henri: 32; adastra: 192; pyxis: 64
MAXRETRY=10
ROUNDTRIPS=1000

HERE=$PWD

run_piom_bench_n() {
    q="${1}"
    n="${2}"
    nmroot=${HERE}/${arch}-${q}

    i=0
    rc=1
    while [ $rc != 0 ]; do
        ${nmroot}-piom/bin/padico-launch -mpi -n 2 ${NMAD_NODELIST} --timeout ${TIMEOUT} ${nmroot}-piom/bin/nm_piom_pingpong -S 1 -E 1 -N ${ROUNDTRIPS} -W 5 -t ${n} | timeout ${TIMEOUT} tee ${HERE}/${q}-piom-${n}.dat
        grep "TOTAL ${n}" ${HERE}/${q}-piom-${n}.dat
	rc=$?
	echo "# n = ${n}; i = ${i}; rc = ${rc}"
	i=$(( ${i} + 1 ))
	if [ ${i} -gt ${MAXRETRY} ]; then
	    echo "# giving up"
	    break
	fi
    done
    cat ${q}-piom-${n}.dat >> ${q}-piom.dat
    rm ${q}-piom-${n}.dat
}

run_piom_bench() {
    q="$1"
    N=1
    while [ ${N} -le ${MAXTHREADS} ]; do

        run_piom_bench_n ${q} ${N}

        run_piom_bench_n ${q} $(( ${N} * 3 / 2 )) #$(( ${N} * 99 / 70 )) # N * sqrt(2)
        
        N=$(( ${N} * 2 ))
    done

}

for q in ${qs}; do
    echo
    echo "# queue: ${q}"
    echo "# MAXTHREADS: ${MAXTHREADS}"
    echo

    nmroot=${HERE}/${arch}-${q}
    
    ( cd ../../../scripts ; ./pm2-build-packages --prefix=${nmroot}-mini --purge ./madmpi-mini.conf -- --enable-libconcurrent --enable-cfqueues --enable-wcq --enable-looqueue --enable-wfqueue --enable-lfqueue=${q} )

    rm -r ${nmroot}-mini/build
    
    ${nmroot}-mini/bin/padico-launch -mpi -n 2 ${NMAD_NODELIST} --timeout ${TIMEOUT} ${nmroot}-mini/bin/nm_bench_sendrecv -N 10000 | timeout ${TIMEOUT} tee ${HERE}/${q}-sendrecv.dat
    
    ( cd ../../../scripts ; ./pm2-build-packages --prefix=${nmroot}-piom --purge ./madmpi.conf -- --enable-libconcurrent --enable-cfqueues --enable-wcq --enable-looqueue --enable-wfqueue   --enable-lfqueue=${q} )

    rm -r ${nmroot}-piom/build

    run_piom_bench ${q}

done


/*
 * NewMadeleine
 * Copyright (C) 2016-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


/** @file basic example for the RPC interface
 */

#include <stdlib.h>
#include <stdio.h>

#include <nm_launcher_interface.h>
#include <nm_coll_interface.h>
#include <nm_rpc_interface.h>

struct len_buf_s {
  char* buf;
  nm_len_t len;
};

const char msg1[] = "Hello, world 1!";
const char msg2[] = "Hello, world 2!";
const nm_tag_t tag = 0x02;

/** handler called upon incoming RPC request */
static void rpc_hello_handler(nm_rpc_token_t p_token)
{
  struct len_buf_s* data = malloc(sizeof(struct len_buf_s));

  data->len = NM_LEN_UNDEFINED;
  nm_rpc_recv_header(p_token, &data->len, sizeof(nm_len_t));
  printf("rpc_hello_handler()- received header: len = %lu\n", data->len);

  data->buf = malloc(2*data->len);
  nm_rpc_token_set_ref(p_token, data);
  nm_rpc_irecv_body(p_token, data->buf, data->len);
  nm_rpc_irecv_body(p_token, data->buf+data->len, data->len);
}

/** handler called after all data for the incoming RPC request was received */
static void rpc_hello_finalizer(nm_rpc_token_t p_token)
{
  struct len_buf_s* data = nm_rpc_token_get_ref(p_token);
  printf("received bodies: '%s' and '%s'\n", data->buf, data->buf + data->len);
  free(data->buf);
  free(data);
}

int main(int argc, char**argv)
{
  /* generic nmad init */
  nm_launcher_init(&argc, argv);
  nm_session_t p_session = NULL;
  nm_session_open(&p_session, "nm_rpc_hello");
  int size = 0;
  nm_launcher_get_size(&size);
  int rank = -1;
  nm_launcher_get_rank(&rank);
  int peer = (size > 1) ? (1 - rank) : rank;
  nm_gate_t p_gate = NULL;
  nm_launcher_get_gate(peer, &p_gate);

  nm_comm_t p_comm = nm_comm_world("nm_rpc_hello");

  /* register the RPC service */
  nm_rpc_service_t p_service = nm_rpc_register(p_session, tag, NM_TAG_MASK_FULL,
                                               &rpc_hello_handler, &rpc_hello_finalizer, NULL);

  nm_len_t len = strlen(msg1) + 1; /* header */
  /* create rpc request */
  nm_rpc_req_t p_req = nm_rpc_req_init(p_service, p_gate, tag);
  nm_rpc_req_pack_header(p_req, &len, sizeof(len));
  nm_rpc_req_pack_body(p_req, msg1, len);
  nm_rpc_req_pack_body(p_req, msg2, len);
  nm_rpc_req_isend(p_req);
  nm_rpc_req_wait(p_req);

  /* generic nmad termination */
  nm_coll_barrier(p_comm, 0xF2);
  nm_rpc_unregister(p_service);
  nm_comm_destroy(p_comm);
  nm_session_close(p_session);
  nm_launcher_exit();
  return 0;

}

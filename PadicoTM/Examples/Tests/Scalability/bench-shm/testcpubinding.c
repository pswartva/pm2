#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#define BUFFMAX 5000

int main(int argc, char *argv[])
{
  if (argc!=2){
    printf("wrong\n");
    return 0;
  }
  else
    {
    FILE* fichier = NULL;

    fichier = fopen(argv[1], "r");

    char buff[BUFFMAX];

    int i=0;

    if (fichier != NULL)
      {
        while(fgets(buff,BUFFMAX,fichier)!=NULL&&strstr(buff,"PU")==NULL)
          {
            i++;
          }
        printf("%d\n",i);
      }
    fclose(fichier);
    }
  return 0;
}

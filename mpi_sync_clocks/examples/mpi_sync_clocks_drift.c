/*
 * MPI sync clocks
 * Copyright (C) 2006-2020 (see AUTHORS file)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

/** @file measure drift of our sync_clocks over a long period
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <mpi.h>
#include <mpi_sync_clocks.h>

#define ROUNDS_DEFAULT 100
#define ROUNDS_TIME_SECONDS 5
#define ROUNDTRIPS 1000

struct nm_clock_entry_s
{
  double t_master;
  double t_slave;
  long double skew;
};
struct nm_clock_drift_s
{
  struct nm_clock_entry_s*p_entries;
};

static int rounds = ROUNDS_DEFAULT;

int main(int argc, char**argv)
{
  if(argc == 2)
    {
      const char*s_rounds = argv[1];
      rounds = atoi(s_rounds);
      if(rounds <= 0)
        {
          fprintf(stderr, "usage: %s [<rounds>]\n", argv[0]);
          abort();
        }
    }

  MPI_Init(&argc, &argv);

  int rank = -1, worldsize = -1;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &worldsize);

  mpi_sync_clocks_t p_clocks = mpi_sync_clocks_init(MPI_COMM_WORLD);

  struct nm_clock_drift_s*p_drift = malloc(worldsize * sizeof(struct nm_clock_drift_s));
  int i;
  for(i = 0; i < worldsize; i++)
    {
      p_drift[i].p_entries = malloc(rounds * sizeof(struct nm_clock_entry_s));
    }

  int round;
  for(round = 0; round < rounds; round++)
    {
      fprintf(stderr, "# ## round = %d / %d; %d seconds\n", round, rounds, ROUNDS_TIME_SECONDS);
      MPI_Barrier(MPI_COMM_WORLD);
      if(rank == 0)
        {
          for(i = 1; i < worldsize; i++)
            {
              /* pingpong to get a common event on both master time and slave time */
              double min_lat = -1.0;
              int j;
              for(j = 0; j < ROUNDTRIPS; j++)
                {
                  double t_dummy = -1.0, t_mid = -1.0;
                  const double t_begin = mpi_sync_clocks_get_time_usec(p_clocks);
                  MPI_Send(&t_dummy, 1, MPI_DOUBLE, i, 42, MPI_COMM_WORLD);
                  MPI_Recv(&t_mid, 1, MPI_DOUBLE, i, 42, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                  const double t_end = mpi_sync_clocks_get_time_usec(p_clocks);
                  const double lat = (t_end - t_begin) / 2.0;
                  if(min_lat < 0 || lat < min_lat)
                    {
                      min_lat = lat;
                      p_drift[i].p_entries[round].t_master = t_begin;
                      p_drift[i].p_entries[round].t_slave = t_mid - min_lat;
                    }
                }
            }
        }
      else
        {
          double t_mid;
          int j;
          for(j = 0; j < ROUNDTRIPS; j++)
            {
              double t_dummy = -1.0;
              MPI_Recv(&t_dummy, 1, MPI_DOUBLE, 0, 42, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
              t_mid = mpi_sync_clocks_get_time_usec(p_clocks);
              MPI_Send(&t_mid, 1, MPI_DOUBLE, 0, 42, MPI_COMM_WORLD);
            }
        }
      /* measure time for synchronizing clocks */
      fprintf(stderr, "# synchronizing offsets...\n");
      const double t_sync_begin = mpi_sync_clocks_get_time_usec(p_clocks);
      mpi_sync_clocks_synchronize(p_clocks);
      const double t_sync_end = mpi_sync_clocks_get_time_usec(p_clocks);
      const double delay_sync = t_sync_end - t_sync_begin;
      fprintf(stderr, "# synchronizong done; time = %.2lf usec. (%.2lf usec./node)\n",
              delay_sync, delay_sync / (double)(worldsize - 1.0));
      if(rank == 0)
        {
          for(i = 0; i < worldsize; i++)
            {
              const long double skew = mpi_sync_clocks_get_skew(p_clocks, i);
              p_drift[i].p_entries[round].skew = skew;
            }
        }
      sleep(ROUNDS_TIME_SECONDS);
    }

  /* final sync */
  mpi_sync_clocks_synchronize(p_clocks);
  if(rank == 0)
    {
      printf("# round;\t dest;\t t_master;\t t_slave;\t drift to global;\t drift to local;\t skew\n");
      for(round = 0; round < rounds; round++)
        {
          for(i = 1; i < worldsize; i++)
            {
              const double t_master = p_drift[i].p_entries[round].t_master;
              const double t_slave_local = p_drift[i].p_entries[round].t_slave;
              const double t_slave = mpi_sync_clocks_remote_to_global(p_clocks, i, t_slave_local);
              const long double skew = p_drift[i].p_entries[round].skew;
              printf("%d \t %d \t %.3lf \t %.3lf \t %.3lf \t %.3lf \t %.16Lf\n",
                     round, i, t_master, t_slave, t_slave - t_master, t_slave_local - t_master, skew);
            }
        }
      for(i = 0; i < worldsize; i++)
        {
          fprintf(stderr, "# skew with rank %d = %.16Lg\n", i, mpi_sync_clocks_get_skew(p_clocks, i));
        }
    }

  mpi_sync_clocks_shutdown(p_clocks);
  MPI_Finalize();

  for(i = 0; i < worldsize; i++)
    {
      free(p_drift[i].p_entries);
    }
  free(p_drift);

  return 0;
}

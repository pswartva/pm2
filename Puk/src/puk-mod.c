/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Low-level module management
 * @note This low-level implementation is not thread-safe
 * @ingroup Puk
 */


#include "Puk-internals.h"
#include "Module.h"

PADICO_MODULE_BUILTIN(Puk, NULL, NULL, NULL);

static void defmod_start_handler  (puk_parse_entity_t);
static void defmod_end_handler    (puk_parse_entity_t);
static void requires_end_handler  (puk_parse_entity_t);
static void unit_end_handler      (puk_parse_entity_t);
static void attr_end_handler      (puk_parse_entity_t);

/******************************************************/

PUK_HASHTABLE_TYPE(puk_mod, const char*, puk_mod_t,
                   &puk_hash_string_default_hash, &puk_hash_string_default_eq, NULL);

static struct
{
  puk_mod_hashtable_t modl;      /**< modules hashtable- key: module name (string); data: puk_mod_t */
  padico_modID_vect_t mod_names; /**< vector of module names- updated only on request */
  puk_mod_t puk_mod;             /**< Puk itself as a module */
} puk_mod = { .modl = NULL, .puk_mod = NULL };


/******************************************************/



/* *** MOD ************************************************* */
/* actions:
 *   getbyname
 *   open
 *   resolve
 *   load
 *   run
 *   unload
 */

padico_modID_t puk_mod_getname(puk_mod_t mod)
{
  return mod ? mod->mod_name : "-";
}

/** a a dependancy from mod to dep */
void puk_mod_add_dep(puk_mod_t mod, puk_mod_t dep)
{
  puk_mod_vect_push_back(&mod->requires, dep);
  dep->used_by++;
  puk_mod_vect_push_back(&dep->rdeps, mod);
}

static void puk_mod_del_rdep(puk_mod_t mod, puk_mod_t dep)
{
  puk_mod_vect_itor_t r = puk_mod_vect_find(&dep->rdeps, mod);
  if(r != NULL)
    {
      dep->used_by--;
      puk_mod_vect_erase(&dep->rdeps, r);
    }
  else
    {
      padico_warning("cannot remove rdep; dependancy from %s on %s.\n", mod->mod_name, dep->mod_name);
    }
}

void puk_mod_set_ancestor(puk_mod_t mod, puk_mod_t ancestor)
{
  if(ancestor != NULL)
    {
      mod->inherits = ancestor;
      ancestor->used_by++;
      puk_mod_vect_push_back(&ancestor->rdeps, mod);
    }
}

void puk_mod_lock_use(puk_mod_t mod)
{
  mod->used_by++;
}

void puk_mod_unlock_use(puk_mod_t mod)
{
  mod->used_by--;
  assert(mod->used_by >= 0);
}

padico_modID_vect_t puk_mod_getmodIDs(void)
{
  if(puk_mod.modl)
    {
      puk_lock_acquire();
      puk_mod_hashtable_enumerator_t e = puk_mod_hashtable_enumerator_new(puk_mod.modl);
      const char*name;
      if(puk_mod.mod_names)
        padico_modID_vect_delete(puk_mod.mod_names);
      puk_mod.mod_names = padico_modID_vect_new();
      name = puk_mod_hashtable_enumerator_next_key(e);
      while(name)
        {
          padico_modID_vect_push_back(puk_mod.mod_names, name);
          name = puk_mod_hashtable_enumerator_next_key(e);
        }
      puk_mod_hashtable_enumerator_delete(e);
      puk_lock_release();
    }
  return puk_mod.mod_names;
}

puk_mod_t puk_mod_getbyname(const char*mod_name)
{
  puk_mod_t m = NULL;
  puk_lock_acquire();
  if(puk_mod.modl != NULL)
    m = puk_mod_hashtable_lookup(puk_mod.modl, mod_name);
  puk_lock_release();
  return m;
}

/* private function
 */
padico_rc_t puk_mod_open(puk_mod_t*mod, padico_string_t mod_path)
{
  padico_string_t s_meta_file = padico_string_new();
  padico_string_printf(s_meta_file, "%s/meta.xml", padico_string_get(mod_path));
  struct puk_parse_entity_s e = puk_xml_parse_file(padico_string_get(s_meta_file), PUK_TRUST_LOCAL);
  padico_rc_t rc = puk_parse_get_rc(&e);
  if(padico_rc_iserror(rc))
    {
      return rc;
    }
  if(!puk_parse_is(&e, "defmod"))
    {
      rc = padico_rc_error("Parse error: root entity is not \"defmod\" while opening module meta file %s",
                           padico_string_get(s_meta_file));
    }
  else
    {
      *mod = puk_parse_get_content(&e);
    }
  (*mod)->mod_path = padico_strdup(padico_string_get(mod_path));
  padico_string_delete(s_meta_file);
  return rc;

}

/** padico_puk_mod_open(mod, mod_name)
 *  open a module, loads its description from disk to memory.
 *  mod     : (out) the loaded mod
 *  mod_name: (in)  module name
 */
padico_rc_t padico_puk_mod_open(puk_mod_t*mod, const char*mod_name)
{
  if((mod_name[0] == '.') || (mod_name[0] == '/'))
    {
      /* mod name as directory path */
      padico_string_t s_meta_file = padico_string_new();
      padico_string_printf(s_meta_file, "%s/meta.xml", mod_name);
      int found = (access(padico_string_get(s_meta_file), F_OK) == 0);
      padico_string_delete(s_meta_file);
      if(found)
        {
          padico_string_t s_mod_path = padico_string_new();
          padico_string_printf(s_mod_path, "%s", mod_name);
          padico_rc_t rc = puk_mod_open(mod, s_mod_path);
          padico_string_delete(s_mod_path);
          return rc;
        }
    }
  else
    {
      /* lookup in path */
      puk_path_vect_t path_vect = puk_path_get();
      puk_path_vect_itor_t i;
      puk_vect_foreach(i, puk_path, path_vect)
        {
          padico_string_t s_meta_file = padico_string_new();
          padico_string_printf(s_meta_file, "%s/lib/padico/%s/meta.xml", *i, mod_name);
          const int found = (access(padico_string_get(s_meta_file), F_OK) == 0);
          padico_out(50, "trying %s; found = %d\n", padico_string_get(s_meta_file), found);
          padico_string_delete(s_meta_file);
          if(found)
            {
              padico_string_t mod_path = padico_string_new();
              padico_string_printf(mod_path, "%s/lib/padico/%s",*i, mod_name);
              padico_out(50, "module=*%s* path=*%s*\n", mod_name, padico_string_get(mod_path));
              padico_rc_t rc = puk_mod_open(mod, mod_path);
              padico_out(50, "module=*%s* done.\n", mod_name);
              padico_string_delete(mod_path);
              return rc;
            }
        }
    }
  padico_rc_t rc = padico_rc_error("Cannot find module %s in path.", mod_name);
  return rc;
}

/** padico_puk_mod_open_file(mod, file_name)
 *  open a module, loads its description from disk to memory.
 *  mod     : (out) the loaded mod
 *  mod_name: (in)  module file description
 */
padico_rc_t padico_puk_mod_open_file(puk_mod_t*mod, const char*file_name)
{
  padico_string_t full_path = padico_string_new();
  padico_string_cat(full_path, file_name);
  padico_out(50, "file=**%s**\n", file_name);
  padico_rc_t rc = puk_mod_open(mod, full_path);
  return rc;
}

/** Resolve a module by name from:
 *    - already loaded modules
 *    - builtin modules
 *    - modules on disk
 */
padico_rc_t padico_puk_mod_resolve(puk_mod_t*_mod, const char*mod_name)
{
  puk_mod_t mod = NULL;
  padico_rc_t rc = NULL;
  /* 1- already loaded */
  mod = puk_mod_getbyname(mod_name);
  /* 2- try built-in */
  if(mod == NULL)
    {
      puk_mod_builtin_tryload(&mod, mod_name);
    }
  /* 3- open from file */
  if(mod == NULL)
    {
      rc = padico_puk_mod_open(&mod, mod_name);
    }
  if(_mod)
    *_mod = mod;
  return rc;
}

/** padico_puk_mod_load(mod)
 *  mod: (in) mod descriptor for an *already opened* module
 */
padico_rc_t padico_puk_mod_load(puk_mod_t mod)
{
  padico_rc_t rc = NULL;

  puk_lock_acquire();
  mod->ref_count++;
  padico_out(40, "module=*%s*; ref_count=%d\n", mod->mod_name, mod->ref_count);
  if(mod->ref_count > 1)
    {
      puk_lock_release();
      return rc;
    }
  /* resolve all module references */
  while(padico_modID_vect_size(&mod->unresolved_deps) > 0)
    {
      char*id = (char*)padico_modID_vect_pop_back(&mod->unresolved_deps);
      padico_trace("resolving required mod **%s** for **%s**\n", id, mod->mod_name);
      puk_mod_t reqmod = puk_mod_getbyname(id);
      if(reqmod == NULL)
        {
          padico_rc_t urc = padico_puk_mod_resolve(&reqmod, id);
          if(!padico_rc_iserror(urc))
            {
              puk_mod_set_ancestor(reqmod, padico_module_self());
              padico_trace("loading required mod **%s** for **%s**\n", reqmod->mod_name, mod->mod_name);
              rc = padico_puk_mod_load(reqmod);
            }
          else
            {
              urc = padico_rc_error("MOD load error: required module **%s** not found", id);
              rc = padico_rc_cat(rc, urc);
              padico_rc_delete(urc);
              puk_lock_release();
              return rc;
            }
        }
      puk_mod_add_dep(mod, reqmod);
      padico_free(id);
    }
  /* add reverse reference to module containing the driver */
  assert(mod->driver->home != NULL);
  mod->driver->home->used_by++;
  puk_mod_vect_push_back(&mod->driver->home->rdeps, mod);
  puk_lock_release();

  /* load units */
  puk_unit_vect_itor_t u;
  puk_vect_foreach(u, puk_unit, &mod->units)
    {
      padico_rc_t urc = NULL;
      const struct padico_loader_s*driver = puk_component_get_driver_PadicoLoader(mod->driver, NULL);
      if(driver->load)
        urc = (driver->load)(u);
      if(padico_rc_iserror(urc))
      {
        rc = padico_rc_cat(rc, urc);
        padico_rc_delete(urc);
      }
    }
  return rc;
}

/** padico_puk_mod_start(mod, argc, argv)
 */
padico_rc_t padico_puk_mod_start(puk_mod_t mod, puk_job_t job)
{
  puk_unit_vect_itor_t u;
  padico_rc_t rc = NULL;
  padico_out(40, "module=**%s**\n", mod->mod_name);

  const char*setcwd = puk_mod_local_getattr(mod, "PADICO_SETCWD");
  if(setcwd)
    {
      int err = chdir(setcwd);
      if(err)
        {
          rc = padico_rc_error("Puk: cannot chdir to %s.", setcwd);
          return rc;
        }
      else
        {
          padico_print("set working directory: %s\n", setcwd);
        }
    }
  puk_vect_foreach(u, puk_unit, &mod->units)
    {
      const struct padico_loader_s*driver = puk_component_get_driver_PadicoLoader(mod->driver, NULL);
      if(driver->start)
        {
          job->rc = NULL;
          rc = (driver->start)(u, job);
          if(padico_rc_iserror(rc))
            {
              /* error => unit not runnable => continue */
              padico_rc_delete(rc);
              rc = NULL;
            }
          else
            {
              /* only *one* runnable unit allowed per module */
              return rc;
            }
        }
    }
  rc = padico_rc_error("Puk: no runnable unit found in module %s.", mod->mod_name);
  puk_job_notify(job);
  return rc;
}

void puk_job_notify(puk_job_t job)
{
  if(job->notify)
    {
      (*job->notify)(job->notify_key);
    }
  padico_free(job);
}

/** padico_puk_mod_unload(mod)
 */
padico_rc_t padico_puk_mod_unload(puk_mod_t mod)
{
  padico_rc_t rc = NULL;

  padico_trace("unload mod [%p] %s\n", mod, mod->mod_name);

  assert(mod->mod_name != NULL);

  padico_out(40, "MOD UNLOAD %s (now ref=%d)\n",  mod->mod_name, mod->ref_count);
  if(mod->used_by > 0)
    {
      /* ignore rdeps for which we are ancestor */
      int inherit = 0;
      puk_mod_vect_itor_t i;
      puk_vect_foreach(i, puk_mod, &mod->rdeps)
        {
          if((*i)->inherits == mod)
            {
              inherit++;
            }
        }
      if(mod->used_by - inherit > 0)
        {
          padico_warning("trying to unload module *%s* still in use (used_by = %d; rdeps = %d).\n",
                         mod->mod_name, mod->used_by, puk_mod_vect_size(&mod->rdeps));
          puk_vect_foreach(i, puk_mod, &mod->rdeps)
            {
              padico_warning("  module %s used by %s.\n", mod->mod_name, (*i)->mod_name);
            }
          rc = padico_rc_error("module in use (used_by = %d; rdeps = %d)",
                               mod->used_by, puk_mod_vect_size(&mod->rdeps));
          return rc;
        }
    }
  mod->ref_count--;
  if(mod->ref_count > 0)
    {
      rc = padico_rc_error("module in use (ref_count=%d)", mod->ref_count);
      return rc;
    }
  if(mod->ref_count < 0)
    {
      rc = padico_rc_error("module already unloaded (ref_count=%d)", mod->ref_count);
      return rc;
    }
  puk_unit_vect_itor_t u;
  puk_vect_foreach(u, puk_unit, &mod->units)
    {
      padico_rc_t urc = NULL;
      const struct padico_loader_s*driver = puk_component_get_driver_PadicoLoader(mod->driver, NULL);
      if(driver->unload)
        urc = (driver->unload)(u);
      if(padico_rc_iserror(urc))
        {
          rc = padico_rc_cat(rc, urc);
          padico_rc_delete(urc);
        }
    }
  if(mod->own_instances > 0)
    {
      padico_warning("module %s has %d instances after finalizing.\n", mod->mod_name, mod->own_instances);
    }
  puk_mod_vect_itor_t r;
  puk_vect_foreach(r, puk_mod, &mod->requires)
    {
      puk_mod_del_rdep(mod, *r);
    }
  puk_mod_del_rdep(mod, mod->driver->home);
  if(mod->inherits)
    {
      puk_mod_del_rdep(mod, mod->inherits);
    }
  padico_trace("Puk: MOD UNLOAD %s unloaded\n", mod->mod_name);
  puk_mod_destroy(mod);
  return rc;
}

/* *** ATTR ************************************************ */
/* attributes management
 *   getattr
 *   local_getattr
 *   setattr
 */

const char*puk_mod_getattr(puk_mod_t mod, const char*label)
{
  return puk_mod_global_getattr(mod, label);
}

const char*puk_mod_attr_get_value(struct puk_mod_attr_s*attr)
{
  if(attr->opt != NULL)
    {
      if(attr->value != NULL)
        padico_free(attr->value); /* flush cache */
      attr->value = puk_opt_value_to_string(attr->opt);
      return attr->value;
    }
  else if(attr->value != NULL)
    {
      return attr->value;
    }
  else
    {
      padico_warning("invalid state for attribute %s.\n", attr->label);
      return NULL;
    }
}

const char*puk_mod_local_getattr(puk_mod_t mod, const char*label)
{
  puk_mod_attr_vect_itor_t i;
  puk_vect_foreach(i, puk_mod_attr, mod->attrs)
    {
      if(strcmp(i->label, label) == 0)
        {
          return puk_mod_attr_get_value(i);
        }
    }
  return NULL;
}

const char*puk_mod_global_getattr(puk_mod_t mod, const char*label)
{
  if(!mod)
    mod = padico_module_self();
  const char*attr = puk_mod_local_getattr(mod, label);
  const char*value = (attr || !mod->inherits) ? attr : puk_mod_global_getattr(mod->inherits, label);
  if((mod == padico_module_self()) && !value)
    {
      value = padico_getenv(label);
    }
  return value;
}

void puk_mod_setattr(puk_mod_t mod, const char*label, const char*value)
{
  puk_mod_attr_vect_itor_t i;
  puk_vect_foreach(i, puk_mod_attr, mod->attrs)
    {
      if(strcmp(i->label, label) == 0)
        {
          if(i->value != NULL)
            {
              assert(i->opt == NULL);
              padico_free(i->value);
              i->value = padico_strdup(value);
              return;
            }
          else
            {
              assert(i->opt != NULL);
              puk_opt_value_from_string(i->opt, value);
              return;
            }
        }
    }
  struct puk_mod_attr_s attr;
  attr.label = padico_strdup(label);
  attr.opt = puk_opt_find(mod->mod_name, label);
  if(attr.opt)
    {
      attr.value = NULL;
      puk_opt_value_from_string(attr.opt, value);
    }
  else
    {
      attr.value = padico_strdup(value);
    }
  puk_mod_attr_vect_push_back(mod->attrs, attr);
}


/***************************************************/

/* private func
 * allocates a new "puk_mod_t" object with default values
 */
puk_mod_t puk_mod_new(const char*name, const char*driver_name)
{
  puk_mod_t m = padico_malloc(sizeof(struct puk_mod_s));
  /* resolve driver */
  padico_string_t drv_mod_name = padico_string_new();
  padico_string_printf(drv_mod_name, "PadicoLoader-%s", driver_name);
  puk_component_t driver = puk_component_resolve(padico_string_get(drv_mod_name));
  padico_string_delete(drv_mod_name);
  if(!driver)
    {
      padico_warning("driver **%s** not found for module **%s**\n", driver_name, name);
      return NULL;
    }
  m->driver      = driver;
  m->mod_name    = padico_strdup(name);
  m->mod_path    = NULL;
  m->ref_count   = 0;
  m->used_by     = 0;
  m->managed_instances = 0;
  m->managed_components = 0;
  m->own_instances = 0;
  puk_mod_vect_init(&m->requires);
  puk_mod_vect_init(&m->rdeps);
  padico_modID_vect_init(&m->unresolved_deps);
  m->inherits    = NULL;
  puk_unit_vect_init(&m->units);
  m->attrs       = puk_mod_attr_vect_new();
  puk_lock_acquire();
  if(puk_mod.modl == NULL)
    puk_mod.modl = puk_mod_hashtable_new();
  puk_mod_hashtable_insert(puk_mod.modl, m->mod_name, m);
  puk_lock_release();
  return m;
}

void puk_mod_destroy(puk_mod_t m)
{
  assert(m->used_by == 0);
  assert(m->ref_count == 0);
  assert(m->managed_instances == 0);
  puk_lock_acquire();
  puk_mod_hashtable_remove(puk_mod.modl, m->mod_name);
  puk_lock_release();
  if(m->attrs)
    {
      puk_mod_attr_vect_itor_t i;
      puk_vect_foreach(i, puk_mod_attr, m->attrs)
        {
          padico_free(i->label);
          if(i->value != NULL)
            padico_free(i->value);
        }
      puk_mod_attr_vect_delete(m->attrs);
      m->attrs = NULL;
    }
  puk_unit_vect_itor_t u;
  puk_vect_foreach(u, puk_unit, &m->units)
    {
      if(u->driver_specific)
        {
          padico_warning("driver specific data not de-allocated for unit %s (driver: %s).\n",
                         u->name, m->driver->name);
        }
      if(u->name)
        {
          padico_free(u->name);
          u->name = NULL;
        }
    }
  puk_unit_vect_destroy(&m->units);
  puk_mod_vect_destroy(&m->requires);
  puk_mod_vect_destroy(&m->rdeps);
  padico_modID_vect_destroy(&m->unresolved_deps);
  padico_free(m->mod_name);
  if(m->mod_path)
    {
      padico_free((void*)m->mod_path);
    }
  m->mod_name = NULL;
  padico_free(m);
}

/** dump modules list as graphviz file */
void puk_mod_graphviz(void)
{
  puk_mod_hashtable_enumerator_t e = puk_mod_hashtable_enumerator_new(puk_mod.modl);
  printf("\n\n");
  printf("digraph g {\n");
  puk_mod_t mod = puk_mod_hashtable_enumerator_next_data(e);
  while(mod != NULL)
    {
      printf("\t\"%s\" [shape = record,height=.2];\n", mod->mod_name);
      printf("\t\"%s\"[label = \"<name> %s |<depends> dep |<inherits> inherits \"];\n", mod->mod_name, mod->mod_name);
      puk_mod_vect_itor_t r;
      puk_vect_foreach(r, puk_mod, &mod->requires)
        {
          printf("\t\"%s\":depends -> \"%s\":name;  \n", mod->mod_name, (*r)->mod_name);
        }
      if(mod->inherits)
        {
          printf("\t\"%s\":inherits -> \"%s\":name; \n", mod->mod_name, mod->inherits->mod_name);
        }
      mod = puk_mod_hashtable_enumerator_next_data(e);
    }
  puk_mod_hashtable_enumerator_delete(e);
  printf("}\n");
}


/* *** parser funcs: defmod ******************************** */

static void defmod_start_handler(puk_parse_entity_t e)
{
  const char*mod_name = puk_parse_getattr(e, "name");
  const char*driver_name = puk_parse_getattr(e, "driver");
  puk_mod_t mod = NULL;
  if(!mod_name)
    {
      puk_parse_set_rc(e, padico_rc_error("no module name in <defmod/>\n"));
      return;
    }
  if(!driver_name)
    {
      puk_parse_set_rc(e, padico_rc_error("no driver name in <defmod/> for module %s.\n",
                                          mod_name));
      return;
    }
  mod = puk_mod_new(mod_name, driver_name);
  puk_parse_set_content(e, mod);
}
static void defmod_end_handler(puk_parse_entity_t e)
{
  if(puk_parse_get_content(e) == NULL)
    {
      puk_parse_set_rc(e, padico_rc_error("error while loading module"));
    }
}
static const struct puk_tag_action_s defmod_action =
{
  .xml_tag        = "defmod",
  .start_handler  = &defmod_start_handler,
  .end_handler    = &defmod_end_handler,
  .required_level = PUK_TRUST_CONTROL
};

/* *** parser funcs: requires ****************************** */

static void requires_end_handler(puk_parse_entity_t e)
{
  puk_parse_entity_t owner = puk_parse_parent(e);
  puk_mod_t mod = NULL;
  padico_modID_t req = padico_strdup(puk_parse_get_text(e));
  padico_out(50, "requires=%s\n", req);
  if(owner == NULL)
    {
      padico_warning("ignoring <requires> tag\n");
      return;
    }
  mod = puk_parse_get_content(owner);
  padico_modID_vect_push_back(&mod->unresolved_deps, req);
}
static const struct puk_tag_action_s requires_action =
{
  .xml_tag        = "requires",
  .start_handler  = NULL,
  .end_handler    = &requires_end_handler,
  .required_level = PUK_TRUST_OTHER
};

/* *** parser funcs: unit ********************************** */

static void unit_end_handler(puk_parse_entity_t e)
{
  puk_parse_entity_t owner = puk_parse_parent(e);
  puk_mod_t mod = owner?puk_parse_get_content(owner):NULL;
  struct puk_unit_s unit =
    {
      .name            = padico_strdup(puk_parse_get_text(e)),
      .mod             = mod,
      .driver_specific = NULL
    };
  padico_out(50, "unit=%s\n", unit.name);
  if(owner == NULL)
    {
      padico_warning("ignoring <unit> tag\n");
      return;
    }
  puk_unit_vect_push_back(&mod->units, unit);
}
static const struct puk_tag_action_s unit_action =
{
  .xml_tag        = "unit",
  .start_handler  = NULL,
  .end_handler    = &unit_end_handler,
  .required_level = PUK_TRUST_OTHER
};

/* *** parser funcs: file ********************************** */

static const struct puk_tag_action_s file_action =
{
  .xml_tag        = "file",
  .start_handler  = NULL,
  .end_handler    = NULL,
  .required_level = PUK_TRUST_OTHER
};

/* *** parser funcs: attr ********************************** */

static void attr_end_handler(puk_parse_entity_t e)
{
  const char*label = puk_parse_getattr(e, "label");
  if(label)
    {
      puk_parse_entity_t owner = puk_parse_parent(e);
      if(!owner)
        {
          puk_parse_set_rc(e, padico_rc_error("attribute in wrong context"));
        }
      else
        {
          const char*value = puk_parse_get_text(e);
          puk_mod_t mod = puk_parse_get_content(owner);
          puk_mod_setattr(mod, label, value);
        }
    }
  else
    {
      puk_parse_set_rc(e, padico_rc_error("<attr/> has no label.\n"));
      padico_warning("parsing error- <attr/> has no label.\n");
    }

}
static const struct puk_tag_action_s attr_action =
{
  .xml_tag        = "attr",
  .start_handler  = NULL,
  .end_handler    = &attr_end_handler,
  .required_level = PUK_TRUST_OTHER
};

/* *** parser funcs: setattr ******************************* */

static void setattr_end_handler(puk_parse_entity_t e)
{
  const char*label = puk_parse_getattr(e, "label");
  const char*mod_name = puk_parse_getattr(e, "mod")?:"Puk";
  if(label)
    {
      puk_mod_t mod = puk_mod_getbyname(mod_name);
      if(!mod)
        {
          puk_parse_set_rc(e, padico_rc_error("module %s not found", mod_name));
        }
      else
        {
          const char*value = puk_parse_get_text(e);
          puk_mod_setattr(mod, label, value);
        }
    }
  else
    {
      puk_parse_set_rc(e, padico_rc_error("<setattr/> requires 'label' attribute.\n"));
      padico_warning("parsing error- <setattr/> failed.\n");
    }

}
static const struct puk_tag_action_s setattr_action =
{
  .xml_tag        = "setattr",
  .start_handler  = NULL,
  .end_handler    = &setattr_end_handler,
  .required_level = PUK_TRUST_OTHER
};

/******************************************************/

/**
 * @pre padico_puk_findcore_init() already done
 * @pre padico_puk_xml_init() already done
 */
void padico_puk_mod_init(void)
{
  /* init module database */
  if(puk_mod.modl == NULL)
    puk_mod.modl = puk_mod_hashtable_new();
  puk_mod.mod_names = NULL;
  /* declare Puk itself as a builtin module. */
  puk_mod_builtin_tryload(&puk_mod.puk_mod, "Puk");
  if(puk_mod.puk_mod == NULL)
    {
      padico_out(puk_verbose_critical, "internal error while resolving builtin Puk module.\n");
    }
  puk_mod.puk_mod->driver->home = puk_mod.puk_mod;
  padico_puk_mod_load(puk_mod.puk_mod);
  assert(padico_module_self() != NULL);
  /* register parsing actions for module definition */
  puk_xml_add_action(defmod_action);
  puk_xml_add_action(requires_action);
  puk_xml_add_action(unit_action);
  puk_xml_add_action(file_action);
  puk_xml_add_action(attr_action);
  puk_xml_add_action(setattr_action);
}

void padico_puk_mod_finalize(void)
{
  if(getenv("PUK_MOD_DUMP") != NULL)
    {
      puk_mod_graphviz();
    }
  padico_print("finalizing- %d modules loaded\n", (int)puk_mod_hashtable_size(puk_mod.modl));
  while(puk_mod_hashtable_size(puk_mod.modl) > 0)
    {
      puk_lock_acquire();
      puk_mod_hashtable_enumerator_t e = puk_mod_hashtable_enumerator_new(puk_mod.modl);
      puk_mod_t m = puk_mod_hashtable_enumerator_next_data(e);
      while(m != NULL && (m->used_by > 0 || m->managed_instances > 0))
        m = puk_mod_hashtable_enumerator_next_data(e);
      puk_mod_hashtable_enumerator_delete(e);
      if(m == NULL)
        {
          padico_print("cyclic dependency detected while finalizing; listing modules:\n");
          e = puk_mod_hashtable_enumerator_new(puk_mod.modl);
          m = puk_mod_hashtable_enumerator_next_data(e);
          do
            {
              if(m != NULL)
                {
                  padico_print("   mod = %s; used_by = %d; manages = %d\n", m->mod_name, m->used_by, m->managed_instances);
                  puk_mod_vect_itor_t r;
                  puk_vect_foreach(r, puk_mod, &m->rdeps)
                    {
                      const char*kind = "-";
                      if((*r)->driver->home == m)
                        kind = "home";
                      else if((*r)->inherits == m)
                        kind = "inherits";
                      else if(puk_mod_vect_find(&(*r)->requires, m) != NULL)
                        kind = "requires";
                      padico_print("          used by %s [%s]\n", (*r)->mod_name, kind);
                    }
                }
              m = puk_mod_hashtable_enumerator_next_data(e);
            }
          while(m != NULL);
          padico_print(" %d modules remaining\n", (int)puk_mod_hashtable_size(puk_mod.modl));
          puk_mod_hashtable_enumerator_delete(e);
          break;
        }
      padico_out(12, "unloading %s\n", m->mod_name);
      puk_lock_release();
      /* quick fix: do not unload 'PadicoTM' mod */
      if(strcmp(m->mod_name, "PadicoTM") != 0 && strcmp(m->mod_name, "PadicoBootLib") != 0)
        {
          padico_puk_mod_unload(m);
        }
      else
        {
          puk_mod_hashtable_remove(puk_mod.modl, m->mod_name);
        }
    }
  /* destroy Puk module */
  puk_mod.puk_mod->used_by = 0;
  puk_mod.puk_mod->ref_count = 0;
  /*  puk_mod_destroy(puk_mod.puk_mod); */
  puk_mod.puk_mod = NULL;
  puk_mod_hashtable_delete(puk_mod.modl);
  puk_mod.modl = NULL;
}

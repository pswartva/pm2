dnl -*- mode: Autoconf;-*-

m4_include([ax_check_compile_flag.m4])
m4_include([ax_check_link_flag.m4])

AC_DEFUN([AC_PADICO_CXX],
  [
    MYCXXFLAGS="$CXXFLAGS"
    AC_PROG_CXX
    AC_PROG_CXXCPP
    case `uname -s` in
      SunOS)
        case ${CXX} in
          c++|g++*) CXXFLAGS="-g"
                    CXXPICFLAGS="-fPIC"
                    CXXDEPFLAGS="-MM";;
               CC)  CXXFLAGS="-g"
                    CXXPICFLAGS="-Kpic"
                    CXXDEPFLAGS="-xM1"
                    CXXSHFLAGS="-G";;
        esac
      ;;
      Linux|*BSD)
        BASE_CXXFLAGS=""
        DEBUG_CXXFLAGS="-g"
        OPT_CXXFLAGS="-O2"
        CXXPICFLAGS="-fPIC"
        CXXDEPFLAGS="-MM"
      ;;
      IRIX64)
        CXXFLAGS="-Wall -Dirix"
        CXXPICFLAGS="-fPIC"
        CXXDEPFLAGS="-MM"
        CXXDEBUGFLAGS="-g"
        CXXOPTFLAGS="-O2" dnl it's *really* -O2, not -O4. Else, it doesn't work with omniORB 3
      ;;
      Darwin)
        CXXFLAGS="-g -Wall -traditional-cpp"
        CXXPICFLAGS="-fPIC"
        CXXDEPFLAGS="-MM"
      ;;
      *) # What is this f$*k!@ OS? :-)
        AC_MSG_ERROR([Unknown OS/platform: `uname -s`])
      ;;
    esac
    BASE_CXXFLAGS="$MYCXXFLAGS $BASE_CXXFLAGS"
    AC_SUBST(CXXPICFLAGS)
    AC_SUBST(BASE_CXXFLAGS)
    AC_SUBST(DEBUG_CXXFLAGS)
    AC_SUBST(OPT_CXXFLAGS)
    HAVE_CXX=yes
    AC_SUBST([HAVE_CXX])
    AC_PADICO_GXX_VERSION
  ])

AC_DEFUN([AC_PADICO_SAVEFLAGS],
  [
    MYCPPFLAGS="$CPPFLAGS"
    MYCFLAGS="$CFLAGS"
    MYLDFLAGS="$LDFLAGS"
  ])

AC_DEFUN([AC_PADICO_COMPILER],
  [
    dnl - save current state
    AC_REQUIRE([AC_PADICO_SAVEFLAGS])
    AC_REQUIRE([AC_PROG_CC])
    AC_REQUIRE([AC_PROG_CPP])
    AC_REQUIRE([AC_PROG_CC_C99])
    dnl --- compute flags depending on the OS type
    case `uname -s` in
      SunOS)
        binary_format=elf
        dnl - dynamic libraries
        LDFLAGS=
        SYMBOL_PREFIX=""
        DYNLIB_PREFIX="lib"
        DYNLIB_EXT=".so"
        DYNLIB_FLAGS="-shared"
        DSO_EXT=".so"
        DSO_FLAGS="-shared"
        case ${CC} in
          gcc*) CFLAGS="-g -Wall"
                CPICFLAGS="-fPIC"
                CDEPFLAGS="-MM";;
          cc)   CFLAGS="-g"
                CPICFLAGS="-Kpic"
                CDEPFLAGS="-xM1";;
        esac
      ;;
      Linux|*BSD)
        binary_format=elf
        LDFLAGS="-rdynamic"
        SYMBOL_PREFIX=""
        DYNLIB_PREFIX="lib"
        DYNLIB_EXT=".so"
        DYNLIB_FLAGS="-shared -rdynamic"
        DSO_EXT=".so"
        DSO_FLAGS="-shared -rdynamic"
        AX_CHECK_LINK_FLAG([-Wl,-O3], [has_wlo3=yes], [has_wlo3=no])
        if test "x${has_wlo3}" != "xno"; then
          OPT_LDFLAGS="-Wl,-O3"
        else
          OPT_LDFLAGS="-Wl,-O1"
        fi
        dnl gcc
        BASE_CFLAGS="-Wall -pipe"
        AX_CHECK_COMPILE_FLAG([-g3], [CFLAGS_G3="-g3"], [])
        if test "x${CFLAGS_G3}" != "x"; then
           DEBUG_CFLAGS="${CFLAGS_G3}"
        else
           DEBUG_CFLAGS="-g"
        fi
        dnl Disable -Og for now, it causes to many <value optimized out>
        dnl AX_CHECK_COMPILE_FLAG([-Og], [has_og=yes], [has_og=no])
        dnl if test "x${has_og}" != "xno"; then
        dnl    DEBUG_CFLAGS="${DEBUG_CFLAGS} -Og"
        dnl fi
dnl        AX_CHECK_COMPILE_FLAG([-xHOST], [OPT_CFLAGS_NATIVE="-xHOST"], [])
        if test "x${OPT_CFLAGS_NATIVE}" = "x"; then
          AX_CHECK_COMPILE_FLAG([-mtune=native], [OPT_CFLAGS_NATIVE="-mtune=native"], [])
        fi
        AX_CHECK_COMPILE_FLAG([-ipo], [OPT_CFLAGS_LTO="-ipo"], [])
        if test "x${OPT_CFLAGS_LTO}" = "x"; then
          AX_CHECK_COMPILE_FLAG([-flto], [], [has_flto=no])
          AX_CHECK_LINK_FLAG([-flto], [], [has_flto=no])
          AX_CHECK_COMPILE_FLAG([-flto=auto], [], [has_flto_auto=no])
          AX_CHECK_LINK_FLAG([-flto=auto], [], [has_flto_auto=no])
          if test "x${has_flto_auto}" != "xno"; then
            OPT_CFLAGS_LTO="-flto=auto"
          elif test "x${has_flto}" != "xno"; then
            OPT_CFLAGS_LTO="-flto"
          fi
        fi
        OPT_CFLAGS="-O3"
        CPICFLAGS="-fPIC"
        CDEPFLAGS="-MM"
      ;;
      IRIX64)
        binary_format=elf
        dnl - dynamic libraries
        LDFLAGS="-rdynamic"
        SYMBOL_PREFIX=""
        DYNLIB_PREFIX="lib"
        DYNLIB_EXT=".so"
        DYNLIB_FLAGS="-shared"
        DSO_EXT=".so"
        DSO_FLAGS="-shared"
        dnl gcc-specific flags... but no other compiler supported
        CFLAGS="-Wall -Dirix"
        CPICFLAGS="-fPIC"
        CDEPFLAGS="-MM"
        CDEBUGFLAGS="-g"
        OPT_CFLAGS="-O4"
      ;;
      Darwin)
        binary_format="Mach-O"
        dnl - dynamic libraries
        LDFLAGS=""
        SYMBOL_PREFIX="_"
        DYNLIB_PREFIX="lib"
        DYNLIB_EXT=".dylib"
        DYNLIB_FLAGS="-flat_namespace -undefined suppress -fno-common -dynamiclib"
        DSO_EXT=""
        DSO_FLAGS="-flat_namespace -undefined suppress -bundle"
        dnl - gcc
        CFLAGS="-g -Wall -traditional-cpp -fno-common -Wno-long-double"
        CPICFLAGS="-fPIC"
        CDEPFLAGS="-MM"
      ;;
      *) # What is this f$*k!@ OS? :-)
        AC_MSG_ERROR([Unknown OS/platform: `uname -s`])
      ;;
    esac
    dnl -- check gcc version before computing final OPT_CFLAGS
    AC_PADICO_GCC_VERSION
    OPT_CFLAGS="$OPT_CFLAGS $OPT_CFLAGS_NATIVE $OPT_CFLAGS_LTO"
    dnl - user specified and standard flags
    CPPFLAGS="$MYCPPFGLAGS -D_REENTRANT -D_GNU_SOURCE"
    LDFLAGS="$MYLDFLAGS $LDFLAGS"
    BASE_CFLAGS="$MYCFLAGS $BASE_CFLAGS"
    AC_SUBST(CPPFLAGS)
    AC_SUBST(CDEPFLAGS)
    AC_SUBST(CPICFLAGS)
    AC_SUBST(DEBUG_CFLAGS)
    AC_SUBST(BASE_CFLAGS)
    AC_SUBST(OPT_CFLAGS)
    AC_SUBST(OPT_LDFLAGS)
    AC_SUBST(binary_format)
  ])


AC_DEFUN([AC_PADICO_GCC_VERSION],
         [ AC_REQUIRE([AC_PADICO_PACKAGE])
           AC_MSG_CHECKING([for gcc version])
           if test "x$GCC" != "xyes"; then
             AC_MSG_ERROR([ $PACKAGE_NAME requires GNU C compiler.])
           else
             gcc_version="`${CC} -dumpversion`"
             case $gcc_version in
               6.*|7.*|7|8.*|9.*|10|10.*|11|11.*|12|12.*)
                 AC_MSG_RESULT([ ${gcc_version} ok])
               ;;
               4.*|5.*)
                 OPT_CFLAGS_LTO=""
                 AC_MSG_RESULT([ ${gcc_version} ancient -- disable LTO])
               ;;
               2.*|3.*)
                 AC_MSG_RESULT([ ${gcc_version} not supported-- warning: gcc version not supported])
               ;;
               *)
                 AC_MSG_RESULT([ ${gcc_version} unknown-- report how it works!])
               ;;
             esac
           fi
          ])

AC_DEFUN([AC_PADICO_GXX_VERSION],
         [ AC_REQUIRE([AC_PADICO_PACKAGE])
           AC_MSG_CHECKING([for g++ version])
           if test "x$GXX" != "xyes"; then
             AC_MSG_ERROR([ $PACKAGE_NAME requires GNU C++ compiler.])
           else
             gxx_version="`${CXX} -dumpversion`"
             case $gxx_version in
               4.*|5.*|6.*|7.*|7|8.*|9.*|10|10.*|11|11.*|12|12.*)
                 AC_MSG_RESULT([ ${gxx_version} ok])
               ;;
               2.*|3.*)
                 AC_MSG_RESULT([ ${gxx_version} --warning: g++ version not supported. No CORBA support])
               ;;
               *)
                 AC_MSG_RESULT([ ${gxx_version} unkown --report how it works!])
               ;;
             esac
           fi
         ])

# ## dynamic objects
AC_DEFUN([AC_PADICO_DSO],
         [
           AC_REQUIRE([AC_PADICO_COMPILER])
           AC_MSG_CHECKING([for dynamic object (DSO) linker])
           DSO_LD="${CC} ${CPICFLAGS} ${DSO_FLAGS}"
           AC_MSG_RESULT([${DSO_LD}])
           AC_SUBST(DSO_LD)

           AC_MSG_CHECKING([for DSO extension])
           AC_MSG_RESULT([${DSO_EXT}])
           AC_SUBST(DSO_EXT)
           AC_DEFINE_UNQUOTED(DSO_EXT, "${DSO_EXT}",
             [Extension for bundles])

           AC_MSG_CHECKING([for symbol prefix])
           AC_MSG_RESULT([${SYMBOL_PREFIX}])
           AC_SUBST(SYMBOL_PREFIX)
           AC_DEFINE_UNQUOTED(SYMBOL_PREFIX, "${SYMBOL_PREFIX}",
             [prefix prepended to symbols by the ABI (usually "_")])
         ])

# ## address sanitizer
AC_DEFUN([AC_PADICO_ASAN],
         [
           AC_REQUIRE([AC_PADICO_COMPILER])
           AC_REQUIRE([AC_PADICO_LINKER])
           AC_MSG_CHECKING([whether we are building with Address Sanitizer (ASAN)])
           AC_ARG_ENABLE(asan,
                         [  --enable-asan           Build with Address Sanitizer (ASAN) ],
                         [ if test $enableval = yes; then
                             ENABLE_ASAN=yes
                           else
                             ENABLE_ASAN=no
                           fi],
                         [])
           AC_SUBST([ENABLE_ASAN])
           AC_MSG_RESULT([${ENABLE_ASAN}])
           if test x${enable_asan} = xyes; then
             AX_CHECK_COMPILE_FLAG([-fsanitize=address -fno-omit-frame-pointer],
                                   [has_compile_asan=yes],
                                   [has_compile_asan=no])
             AX_CHECK_LINK_FLAG([-fsanitize=address], [has_link_asan=yes], [has_link_asan=no])
             AC_MSG_CHECKING([whether tool chain supports ASAN])
             if test ${has_compile_asan} = no; then
               AC_MSG_ERROR([compiler does not support ASAN flags])
             fi
             if test ${has_link_asan} = no; then
               AC_MSG_ERROR([linker does not support ASAN flags])
             fi
             AC_MSG_RESULT([yes])
             BASE_CFLAGS="${BASE_CFLAGS} -fsanitize=address -fno-omit-frame-pointer"
             LDFLAGS="${LDFLAGS} -fsanitize=address"
             EXPORT_LIBS="${EXPORT_LIBS} -fsanitize=address"
           fi
         ])

# ## Fortify
AC_DEFUN([AC_PADICO_FORTIFY],
         [
           AC_REQUIRE([AC_PADICO_COMPILER])
           AC_REQUIRE([AC_PADICO_LINKER])
           AC_MSG_CHECKING([whether we are building with Fortify])
           AC_ARG_ENABLE(fortify,
                         [  --enable-fortify        Build with Fortify ],
                         [ if test $enableval = yes; then
                             BASE_CFLAGS="${BASE_CFLAGS} -D_FORTIFY_SOURCE=1"
                             enable_fortiy=yes
                           else
                             enable_fortify=no
                           fi],
                         [ enable_fortify=no ])
           AC_MSG_RESULT([${enable_fortify}])
         ])

#! /bin/bash 

PORT=8080

ENABLE_PADICO=yes

export BOOTSTRAP_PORT=1234
export BOOTSTRAP_HOST=flille.lille.grid5000.fr
export ALTO_HOST=gridtlse

export PADICO_OPTION="-i$PWD/topo-g5k.xml -i$PWD/topo-flille.xml -i$PWD/topo-kvm.xml -v"

config_padico() {
    if [ "x${ENABLE_PADICO}" = "xyes" ]; then
        #PADICO_OPTION="-i${PWD}/topo.xml -c -v -p -d"
	#PADICO_OPTION="-c -p -DNS_BASIC_CONF=${HOME}/controlssh.xml"
	export PADICO_LAUNCH="padico-launch ${PADICO_OPTION} -DPADICO_BOOTSTRAP_PORT=${BOOTSTRAP_PORT} -DPADICO_BOOTSTRAP_HOST=${BOOTSTRAP_HOST} -DPADICO_BOOTSTRAP_MODE=client"
	export PADICO_LAUNCH_RDV="padico-launch ${PADICO_OPTION} -DPADICO_BOOTSTRAP_LISTEN=${BOOTSTRAP_PORT}"
	PORT=8181
    fi
}

# ## variables used in this script
export TOMCAT_ROOT=/opt/jakarta-tomcat-5.0.28
export BASSO_ROOT=/home/gridtlse/TLSE_BASSO
export ALTO_ROOT=/home/gridtlse/TLSE_ALTO

# ## variables used by sub-programs
export TLSE_INSTALL=${HOME}/TLSE_BASSO
export TLSE_SERVICE_PATH=${TLSE_INSTALL}/service
export TLSE_SOLVER_PATH=${TLSE_INSTALL}/solver
export TLSE_CLIENT_PATH=${TLSE_INSTALL}/client
export TLSE_DAGDA_PATH=${TLSE_INSTALL}/dagda/
export PATH=${TLSE_CLIENT_PATH}/bin:${TLSE_SERVICE_PATH}/bin:${PATH}
export OMNIORB_CONFIG=${HOME}/omniORB.conf


start_omninames() {
    omninames_logdir="/tmp/omniNames_logdir_${RANDOM}"
    mkdir ${omninames_logdir}
    ${PADICO_LAUNCH} omniNames -start -always -logdir ${omninames_logdir} &
    if [ "x${ENABLE_PADICO}" = "xyes" ]; then
	${PADICO_LAUNCH} /usr/sbin/haproxy -f $HOME/config/haproxy_tlse.cfg &
    fi
}

config_omniorb() {
    cat > ${OMNIORB_CONFIG} <<EOF
InitRef = NameService=corbaname::${ALTO_HOST}
giopMaxMsgSize = 33554432
EOF
}


start_ALTO() {
    echo "start ALTO gridtlse side on ${ALTO_HOST}"
    mkdir -p /tmp/TLSE/log4j
    mkdir -p /tmp/tlse/supp_file
    psql -d tlse_db -c "update computer set full_name='${ALTO_HOST}' where web_server=1;"
    cat ${TOMCAT_ROOT}/webapps/weaver/WEB-INF/classes/tlse.properties.orig | sed s/localhost:8080/${ALTO_HOST}:${PORT}/ > ${TOMCAT_ROOT}/webapps/weaver/WEB-INF/classes/tlse.properties
    cat ${TOMCAT_ROOT}/webapps/weaver/grammar.xml.orig | sed s/localhost:8080/${ALTO_HOST}:${PORT}/  > ${TOMCAT_ROOT}/webapps/weaver/grammar.xml
    cat ${TOMCAT_ROOT}/webapps/websolve/WEB-INF/classes/tlse.properties.orig | sed s/localhost:8080/${ALTO_HOST}:${PORT}/ >  ${TOMCAT_ROOT}/webapps/websolve/WEB-INF/classes/tlse.properties
    cat ${ALTO_ROOT}/weaver/config/weaver.xml.orig | sed s/localhost:8080/${ALTO_HOST}:${PORT}/ > ${ALTO_ROOT}/weaver/config/weaver.xml
    ${TOMCAT_ROOT}/bin/startup.sh
    ${ALTO_ROOT}/start_tlse_servers.sh &
    ${PADICO_LAUNCH} dietAgent ${TLSE_SERVICE_PATH}/config/MA_TLSE.cfg >& ${TLSE_SERVICE_PATH}/log/MA_TLSE.trace & 
}

# ## start ALTO on kvm host

config_padico
export DEPLOYMENT="http://${ALTO_HOST}:${PORT}/websolve/Services?type=xml&object=deployment"
start_omninames
config_omniorb
start_ALTO

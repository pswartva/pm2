/** @file
 * Test sockets between a node inside a PadicoTM cluster and
 * another node outside the cluster. Client side (non-PadicoTM code).
 */

#include "io-test.h"
#include <sys/time.h>

struct client_s
{
  int fd;
  unsigned char*buffer;
};


static struct client_s* make_client(char*server_name)
{
  struct sockaddr_in addr;
  struct client_s*client = malloc(sizeof(struct client_s));
  int rc;

  struct hostent*he = gethostbyname(server_name);
  struct sin_addr*addr_sin;
  if(he != NULL)
    {
      addr_sin = (struct sin_addr*) he->h_addr_list[0];
    }
  /* suuport IP adress */
  else if(inet_aton(server_name, (struct in_addr*) addr_sin) != 0)
    {
      printf("[std-client] server=%s not founded!\n", server_name);
      exit(1);
    }

  /* socket */
  client->fd = make_socket();
  printf("[std-client] socket() fd_client=%d\n", client->fd);
  if(client->fd == -1)
    test_failed();

  /* connect */
  addr.sin_family = AF_INET;
  addr.sin_port = htons(SERVER_PORT_NUMBER);
  memcpy(&addr.sin_addr, addr_sin, sizeof(addr.sin_addr));
  rc = connect(client->fd, (struct sockaddr*)&addr, sizeof(addr));
  if(rc)
    {
      while(rc)
        {
          printf("[std-client] server not ready (%s) --retry...\n",
                 strerror(errno));
          sleep(2);
          rc = connect(client->fd, (struct sockaddr*)&addr, sizeof(addr));
        }
    }
  {
    /* set sock opt NODELAY */
    int opt = 1;
    int rc;
    rc = setsockopt(client->fd, SOL_TCP, TCP_NODELAY, &opt, sizeof(opt));
  }
  printf("[std-client] connect() server=%s fd_client=%d rc=%d\n",
         server_name, client->fd, rc);
  return client;
}


static void*client_loop(struct client_s*client)
{
  int i,s ;
  printf("CLIENT 0x%p fd=%d starting\n", client, client->fd);
  client->buffer = malloc(BUFFER_SIZE);
  for(s=4; s<BUFFER_SIZE; s*=2)
    {
      struct timeval t_begin, t_end;
      double t_usec;
      //      printf("[std-client] CLIENT 0x%p fd=%d sending size=%d\n", client, client->fd, s);
      fill_buffer(client->buffer, s, s);
      gettimeofday(&t_begin, NULL);
      for(i=0; i<config_num_roundtrip; i++)
        {
          if(config_check_buffer || config_reinit_buffer)
            {
              fill_buffer(client->buffer, s, s+i);
            }
          //send_msg1(client->fd, client->buffer, s, ALIGNED(s*drand48()));
          send_msg(client->fd, client->buffer, s);
          recv_msg(client->fd, client->buffer, s);
        }
      gettimeofday(&t_end, NULL);
      t_usec = 1000000.0 * (double)(t_end.tv_sec - t_begin.tv_sec)
        + (double)(t_end.tv_usec - t_begin.tv_usec);
      printf(" size=%7d time=%9.2f usec bw=%5.2f MB/s\n",
             s, t_usec/(config_num_roundtrip*2),
             (((double)s) * 2.0 * config_num_roundtrip) / t_usec );
    }
  free(client->buffer);
  printf("CLIENT 0x%p fd=%d closing...\n", client, client->fd);
  close(client->fd);
  printf("CLIENT 0x%p fd=%d completed\n", client, client->fd);
  client->fd = -1;
  return NULL;
}

void usage() {
  fprintf(stderr,"USAGE: client <server name>\n");
}

int main(int argc, char**argv)
{
   struct client_s*c;
   if (argc!=2) {
     usage();
     exit(1);
   }

   c = make_client(argv[1]);
   client_loop(c);
   return 0;
}

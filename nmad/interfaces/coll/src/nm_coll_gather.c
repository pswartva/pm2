/*
 * NewMadeleine
 * Copyright (C) 2014-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <nm_public.h>
#include <nm_private.h>
#include <nm_sendrecv_interface.h>
#include <nm_launcher_interface.h>
#include <Padico/Puk.h>
#include "nm_coll_private.h"
#include <Padico/Module.h>

PADICO_MODULE_HOOK(nm_coll);

PADICO_MODULE_ATTR(gather_tree_threshold, "NMAD_GATHER_TREE_THRESHOLD", "threshold between tree-based gather and linear gather", int, 4096);


/* ** gather *********************************************** */

static inline nm_len_t nm_coll_gather_data_size(int root, int self, struct nm_data_s*p_sdata, struct nm_data_s p_rdata[])
{
  if((p_sdata != NULL) && !nm_data_isnull(p_sdata))
    {
      /* non-NULL send type -> regular gather */
      return nm_data_size(p_sdata);
    }
  else if((p_rdata != NULL) && !nm_data_isnull(&p_rdata[0]))
    {
      /* non-NULL receive & NULL send -> in-place */
      return nm_data_size(&p_rdata[0]);
    }
  else
    {
      /* NULL send & NULL receive data -> size = 0 */
      return 0;
    }
}

static void nm_coll_gather_tree(nm_session_t p_session, nm_group_t p_group, int root, int self,
                                struct nm_data_s*p_sdata, struct nm_data_s p_rdata[], nm_tag_t tag)
{
  const int size = nm_group_size(p_group);
  const nm_len_t data_size = nm_coll_gather_data_size(root, self, p_sdata, p_rdata);
  const nm_coll_tree_kind_t kind = (data_size > padico_module_attr_gather_tree_threshold_getvalue()) ? NM_COLL_TREE_FLAT : NM_COLL_TREE_BINOMIAL;
  struct nm_coll_tree_info_s tree;
  nm_coll_tree_init(&tree, kind, size, self, root);
  void*gather_buf = padico_malloc(data_size * size); /** buffer containing all nodes contribution; uses virtual addressing so that all contributions are contiguous */
  int*children = padico_malloc(sizeof(int) * tree.max_arity);
  int s;
  for(s = tree.height - 1; s >= 0; s--)
    {
      int parent = -1;
      int n_children = 0;
      nm_coll_tree_step(&tree, s, &parent, children, &n_children);
      assert((parent == -1) || (n_children == 0) || (children[0] == -1));
      const int weight = nm_coll_tree_weight(&tree, s, tree.self);
      if(parent != -1)
        {
          /* send data to parent node */
          assert(weight >= 1);
          const nm_gate_t p_parent_gate = nm_group_get_gate(p_group, parent);
          if(weight == 1)
            {
              nm_sr_request_t sreq;
              nm_sr_isend_data(p_session, p_parent_gate, tag, p_sdata, &sreq);
              nm_sr_swait(p_session, &sreq);
            }
          else
            {
              const int vself = nm_coll_r2v(tree.self, &tree);
              if((p_sdata != NULL) && !nm_data_isnull(p_sdata))
                {
                  /* copy local data to gather buffer before send, unless it's already there (in-place) */
                  nm_data_copy_from(p_sdata, 0, data_size, gather_buf + data_size * vself);
                }
              nm_sr_send(p_session, p_parent_gate, tag, gather_buf + data_size * vself, data_size * weight);
            }
        }
      if(n_children > 0)
        {
          /* receive from children */
          struct nm_coll_gather_child_s
          {
            nm_sr_request_t rreq;
            struct nm_datav_s datav;
          };
          struct nm_coll_gather_child_s*reqs = padico_malloc(n_children * sizeof(struct nm_coll_gather_child_s));
          int i;
          for(i = 0; i < n_children; i++)
            {
              assert(children[i] != -1);
              nm_datav_init(&reqs[i].datav);
              nm_gate_t p_child_gate = nm_group_get_gate(p_group, children[i]);
              const int child_weight = nm_coll_tree_weight(&tree, s, children[i]);
              const int vchild = nm_coll_r2v(children[i], &tree);
              assert(child_weight >= 1);
              if(tree.self == tree.root)
                {
                  /* root: receive in place */
                  int k;
                  for(k = 0; k < child_weight; k++)
                    {
                      if(p_rdata != NULL)
                        {
                          nm_datav_add_chunk_data(&reqs[i].datav, &p_rdata[(children[i] + k) % tree.n]);
                        }
                    }
                  struct nm_data_s data;
                  nm_data_datav_build(&data, &reqs[i].datav);
                  nm_sr_irecv_data(p_session, p_child_gate, tag, &data, &reqs[i].rreq);
                }
              else
                {
                  /* not root: receive in gather buffer */
                  nm_sr_irecv(p_session, p_child_gate, tag, gather_buf + data_size * vchild, data_size * child_weight, &reqs[i].rreq);
                }
            }
          for(i = 0; i < n_children; i++)
            {
              nm_sr_rwait(p_session, &reqs[i].rreq);
              nm_datav_destroy(&reqs[i].datav);
            }
          padico_free(reqs);
        }
    }
  /* copy local contribution */
  if(self == root && p_rdata != NULL
     && p_sdata != NULL && !nm_data_isnull(p_sdata) /* null sdata used for MPI_IN_PLACE */)
    {
      nm_data_copy(&p_rdata[tree.self], p_sdata);
    }
  padico_free(gather_buf);
  padico_free(children);
}

#if 0
/* linear algo; now included in tree-based algo with kind=flat
 */

static void nm_coll_linear_gather(nm_session_t p_session, nm_group_t p_group, int root, int self,
                                  struct nm_data_s*p_sdata, struct nm_data_s p_rdata[], nm_tag_t tag)
{
  if(self == root)
    {
      const int size = nm_group_size(p_group);
      nm_sr_request_t*requests = padico_malloc(size * sizeof(nm_sr_request_t));
      int i;
      for(i = 0; i < size; i++)
        {
          if(i != self)
            {
              nm_gate_t p_gate = nm_group_get_gate(p_group, i);
              nm_sr_irecv_data(p_session, p_gate, tag, &p_rdata[i], &requests[i]);
            }
          else if(!nm_data_isnull(p_sdata))
            {
              nm_data_copy(&p_rdata[i], p_sdata);
            }
        }
      for(i = 0; i < size; i++)
        {
          if(i != self)
            {
              nm_sr_rwait(p_session, &requests[i]);
            }
        }
      padico_free(requests);
    }
  else
    {
      nm_gate_t p_root_gate = nm_group_get_gate(p_group, root);
      nm_sr_request_t request;
      nm_sr_isend_data(p_session, p_root_gate, tag, p_sdata, &request);
      nm_sr_swait(p_session, &request);
    }
}
#endif /* 0 */

void nm_coll_group_data_gather(nm_session_t p_session, nm_group_t p_group, int root, int self,
                               struct nm_data_s*p_sdata, struct nm_data_s p_rdata[], nm_tag_t tag)
{
  assert(nm_group_get_gate(p_group, self) == nm_launcher_self_gate());
  nm_coll_gather_tree(p_session, p_group, root, self, p_sdata, p_rdata, tag);
}

static void nm_coll_gather_data_from_contig(struct nm_data_s*p_sdata, const void*sbuf, nm_len_t slen,
                                            struct nm_data_s**pp_rdata, void*rbuf, nm_len_t rlen,
                                            int root, int self, nm_group_t p_group)
{
  if(sbuf != NULL)
    {
      nm_data_contiguous_build(p_sdata, (void*)sbuf, slen);
    }
  else
    {
      nm_data_null_build(p_sdata);
    }
  if(self == root)
    {
      const int size = nm_group_size(p_group);
      if(size < 0)
        {
          NM_FATAL("invalid group size %d.\n", size);
        }
      *pp_rdata = padico_malloc(size * sizeof(struct nm_data_s));
      int i;
      for(i = 0; i < size; i++)
        {
          nm_data_contiguous_build(&(*pp_rdata)[i], (void*)rbuf + i * rlen, rlen);
        }
    }
}

void nm_coll_group_gather(nm_session_t p_session, nm_group_t p_group, int root, int self,
                          const void*sbuf, nm_len_t slen, void*rbuf, nm_len_t rlen, nm_tag_t tag)
{
  struct nm_data_s send_data;
  struct nm_data_s*p_recv_data = NULL;
  nm_coll_gather_data_from_contig(&send_data, sbuf, slen, &p_recv_data, rbuf, rlen, root, self, p_group);
  nm_coll_group_data_gather(p_session, p_group, root, self, &send_data, p_recv_data, tag);
  if(p_recv_data)
    padico_free(p_recv_data);
}

void nm_coll_data_gather(nm_comm_t p_comm, int root, struct nm_data_s*p_sdata, struct nm_data_s p_rdata[], nm_tag_t tag)
{
  nm_coll_group_data_gather(nm_comm_get_session(p_comm), nm_comm_group(p_comm),
                            root, nm_comm_rank(p_comm), p_sdata, p_rdata, tag);
}

void nm_coll_gather(nm_comm_t p_comm, int root, const void*sbuf, nm_len_t slen, void*rbuf, nm_len_t rlen, nm_tag_t tag)
{
  nm_coll_group_gather(nm_comm_get_session(p_comm), nm_comm_group(p_comm),
                       root, nm_comm_rank(p_comm), sbuf, slen, rbuf, rlen, tag);
}


/* ** igather ********************************************** */

struct nm_coll_gather_s
{
  struct nm_coll_tree_status_s coll_tree;
  struct nm_data_s sdata;
  struct nm_data_s*p_rdata;
  int step;
  void*gather_buf;
  struct nm_datav_s*datavs;
};

static void nm_coll_igather_start(struct nm_coll_gather_s*p_gather);
static void nm_coll_igather_step(struct nm_coll_gather_s*p_gather);
static void nm_coll_igather_sreq_notifier(nm_sr_event_t event, const nm_sr_event_info_t*event_info, void*_ref);
static void nm_coll_igather_rreq_notifier(nm_sr_event_t event, const nm_sr_event_info_t*event_info, void*_ref);
static void nm_coll_igather_destroy(struct nm_coll_req_s*p_gather);

static void nm_coll_igather_sreq_notifier(nm_sr_event_t event, const nm_sr_event_info_t*event_info, void*_ref)
{
  struct nm_coll_tree_status_s*p_coll_tree = _ref;
  struct nm_coll_gather_s*p_gather = nm_container_of(p_coll_tree, struct nm_coll_gather_s, coll_tree);
  const int p = nm_atomic_dec(&p_gather->coll_tree.pending_reqs);
  if(p == 0)
    {
      nm_coll_igather_step(p_gather);
    }
}

static void nm_coll_igather_rreq_notifier(nm_sr_event_t event, const nm_sr_event_info_t*event_info, void*_ref)
{
  struct nm_coll_tree_status_s*p_coll_tree = _ref;
  struct nm_coll_gather_s*p_gather = nm_container_of(p_coll_tree, struct nm_coll_gather_s, coll_tree);
  if(p_coll_tree->tree.self == p_coll_tree->tree.root)
    {
      int i;
      for(i = 0; i < p_gather->coll_tree.n_children; i++)
        {
          nm_datav_destroy(&p_gather->datavs[i]);
        }
    }
  const int p = nm_atomic_dec(&p_gather->coll_tree.pending_reqs);
  if(p == 0)
    {
      nm_coll_igather_step(p_gather);
    }
}

static void nm_coll_igather_step(struct nm_coll_gather_s*p_gather)
{
  const nm_len_t data_size = nm_coll_gather_data_size(p_gather->coll_tree.tree.root, p_gather->coll_tree.tree.self,
                                                      &p_gather->sdata, p_gather->p_rdata);
 restart:
  if(p_gather->step > 0)
    {
      int parent = -1;
      p_gather->step--;
      nm_coll_tree_step(&p_gather->coll_tree.tree, p_gather->step, &parent,
                        p_gather->coll_tree.children, &p_gather->coll_tree.n_children);
      const int weight = nm_coll_tree_weight(&p_gather->coll_tree.tree, p_gather->step, p_gather->coll_tree.tree.self);
      if(parent != -1)
        {
          /* send data to parent node */
          assert(weight >= 1);
          if(weight == 1)
            {
              /* leaf: data from user buf */
              nm_coll_tree_send(&p_gather->coll_tree, parent, &p_gather->sdata, &p_gather->coll_tree.parent_request);
              nm_coll_tree_set_notifier(&p_gather->coll_tree, &p_gather->coll_tree.parent_request,
                                        &nm_coll_igather_sreq_notifier);
            }
          else
            {
              /* inner node: integrate local user data into gather_buf then send gather_buf */
              const int vself = nm_coll_r2v(p_gather->coll_tree.tree.self, &p_gather->coll_tree.tree);
              nm_data_copy_from(&p_gather->sdata, 0, data_size, p_gather->gather_buf + data_size * vself);
              struct nm_data_s data;
              nm_data_contiguous_build(&data, p_gather->gather_buf + data_size * vself, data_size * weight);
              nm_coll_tree_send(&p_gather->coll_tree, parent, &data, &p_gather->coll_tree.parent_request);
              nm_coll_tree_set_notifier(&p_gather->coll_tree, &p_gather->coll_tree.parent_request,
                                        &nm_coll_igather_sreq_notifier);
            }
        }
      else if(p_gather->coll_tree.n_children > 0)
        {
          /* receive from children */
          int i;
          for(i = 0; i < p_gather->coll_tree.n_children; i++)
            {
              assert(p_gather->coll_tree.children[i] != -1);
              const int child_weight = nm_coll_tree_weight(&p_gather->coll_tree.tree, p_gather->step,
                                                           p_gather->coll_tree.children[i]);
              const int vchild = nm_coll_r2v(p_gather->coll_tree.children[i], &p_gather->coll_tree.tree);
              assert(child_weight >= 1);
              if(p_gather->coll_tree.tree.self == p_gather->coll_tree.tree.root)
                {
                  /* root: build datav & receive in place */
                  nm_datav_init(&p_gather->datavs[i]);
                  int k;
                  for(k = 0; k < child_weight; k++)
                    {
                      if(p_gather->p_rdata != NULL)
                        {
                          const int index = (p_gather->coll_tree.children[i] + k) % p_gather->coll_tree.tree.n;
                          nm_datav_add_chunk_data(&p_gather->datavs[i], &p_gather->p_rdata[index]);
                        }
                    }
                  struct nm_data_s data;
                  nm_data_datav_build(&data, &p_gather->datavs[i]);
                  nm_coll_tree_recv(&p_gather->coll_tree, p_gather->coll_tree.children[i], &data,
                                    &p_gather->coll_tree.children_requests[i]);
                }
              else
                {
                  /* not root: receive in gather buffer */
                  struct nm_data_s data;
                  nm_data_contiguous_build(&data, p_gather->gather_buf + data_size * vchild, data_size * child_weight);
                  nm_coll_tree_recv(&p_gather->coll_tree, p_gather->coll_tree.children[i], &data,
                                    &p_gather->coll_tree.children_requests[i]);

                }
            }
          for(i = 0; i < p_gather->coll_tree.n_children; i++)
            {
              nm_coll_tree_set_notifier(&p_gather->coll_tree, &p_gather->coll_tree.children_requests[i],
                                        &nm_coll_igather_rreq_notifier);
            }
        }
      else
        {
          goto restart;
        }
    }
  else
    {
      /* notify completion */
      nm_coll_req_signal(nm_coll_req_container(p_gather));
    }

}


static struct nm_coll_req_s*nm_coll_igather_common_init(nm_session_t p_session, nm_group_t p_group, int root, int self,
                                                        nm_tag_t tag, nm_len_t data_size, nm_coll_req_notifier_t p_notify, void*p_ref)
{
  struct nm_coll_req_s*p_coll_req = nm_coll_req_alloc(sizeof(struct nm_coll_gather_s), NM_COLL_REQ_GATHER, &nm_coll_igather_destroy, p_notify, p_ref);
  struct nm_coll_gather_s*p_gather = nm_coll_req_payload(p_coll_req);
  const nm_coll_tree_kind_t kind = (data_size > 4096 /* 4 kB */) ? NM_COLL_TREE_FLAT : NM_COLL_TREE_BINOMIAL;
  nm_coll_tree_status_init(&p_gather->coll_tree, p_session, kind, p_group, root, self, tag);
  p_gather->step = p_gather->coll_tree.tree.height;
  p_gather->gather_buf = padico_malloc(data_size * nm_group_size(p_group));
  p_gather->p_rdata = NULL;
  if(root == self)
    {
      p_gather->datavs = padico_malloc(p_gather->coll_tree.tree.max_arity * sizeof(struct nm_datav_s));
    }
  else
    {
      p_gather->datavs = NULL;
    }
  return p_coll_req;
}

struct nm_coll_req_s*nm_coll_group_data_igather(nm_session_t p_session, nm_group_t p_group, int root, int self,
                                                struct nm_data_s*p_sdata, struct nm_data_s p_rdata[], nm_tag_t tag,
                                                nm_coll_req_notifier_t p_notify, void*p_ref)
{
  const nm_len_t data_size = nm_coll_gather_data_size(root, self, p_sdata, p_rdata);
  struct nm_coll_req_s*p_coll_req = nm_coll_igather_common_init(p_session, p_group, root, self, tag, data_size, p_notify, p_ref);
  struct nm_coll_gather_s*p_gather = nm_coll_req_payload(p_coll_req);
  p_gather->sdata = *p_sdata;
  if(root == self)
    {
      p_gather->p_rdata = padico_malloc(p_gather->coll_tree.tree.n * sizeof(struct nm_data_s));
      int i;
      for(i = 0; i < p_gather->coll_tree.tree.n; i++)
        {
          p_gather->p_rdata[i] = p_rdata[i];
        }
    }
  else
    {
      p_gather->p_rdata = NULL;
    }
  nm_coll_igather_start(p_gather);
  return p_coll_req;
}

struct nm_coll_req_s*nm_coll_igather(nm_comm_t p_comm, int root, const void*sbuf, nm_len_t slen,
                                     void*rbuf, nm_len_t rlen, nm_tag_t tag,
                                     nm_coll_req_notifier_t p_notify, void*p_ref)
{
  nm_session_t p_session = nm_comm_get_session(p_comm);
  nm_group_t p_group = nm_comm_group(p_comm);
  const int self = nm_comm_rank(p_comm);
  const nm_len_t data_size = slen;
  struct nm_coll_req_s*p_coll_req = nm_coll_igather_common_init(p_session, p_group, root, self, tag, data_size, p_notify, p_ref);
  struct nm_coll_gather_s*p_gather = nm_coll_req_payload(p_coll_req);
  nm_coll_gather_data_from_contig(&p_gather->sdata, sbuf, slen, &p_gather->p_rdata, rbuf, rlen, root, self, p_group);
  nm_coll_igather_start(p_gather);
  return p_coll_req;
}


static void nm_coll_igather_start(struct nm_coll_gather_s*p_gather)
{
  nm_coll_igather_step(p_gather);
  /* copy local contribution */
  if(p_gather->coll_tree.tree.self == p_gather->coll_tree.tree.root
     && p_gather->p_rdata != NULL
     && !nm_data_isnull(&p_gather->sdata) /* null sdata used for MPI_IN_PLACE */)
    {
      nm_data_copy(&p_gather->p_rdata[p_gather->coll_tree.tree.self], &p_gather->sdata);
    }
}

static void nm_coll_igather_destroy(struct nm_coll_req_s*p_coll_req)
{
  struct nm_coll_gather_s*p_gather = nm_coll_req_payload(p_coll_req);
  nm_coll_tree_status_destroy(&p_gather->coll_tree);
  if(p_gather->datavs)
    {
      padico_free(p_gather->datavs);
    }
  if(p_gather->p_rdata)
    {
      padico_free(p_gather->p_rdata);
    }
  if(p_gather->gather_buf)
    {
      padico_free(p_gather->gather_buf);
    }
}

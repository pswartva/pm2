/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief ID Packet Filter component- tester for VLink_PacketFilter
 */

#include <Padico/Module.h>
#include "PacketFilter.h"

/* ********************************************************* */
/* *** ID Packet Filter ************************************ */
/* ********************************************************* */


/** @defgroup PacketFilter_ID component: PacketFilter_ID- a packet filter that does nothing (identity function)
 * @note for testing purpose
 * @ingroup PadicoComponent
 */

static void* idpf_instantiate(puk_instance_t assembly,
                              puk_context_t context);
static void idpf_destroy(void*state);
static void idpf_init(void);
static void idpf_encode(void*, const void*, size_t, void**, size_t*);
static void idpf_decode(void*, const void*, size_t, void*, size_t*);

/** instanciation facet for PacketFilter_ID
 * @ingroup PacketFilter_ID
 */
static const struct puk_component_driver_s idpf_component_driver =
  {
    .instantiate    = &idpf_instantiate,
    .destroy        = &idpf_destroy,
    .component_init = &idpf_init
  };
/** 'PacketFilter' facet for PacketFilter_ID
 * @ingroup PacketFilter_ID
 */
static const struct packetfilter_driver_s idpf_driver =
  {
    .pf_encode = &idpf_encode,
    .pf_decode = &idpf_decode
  };

/* ********************************************************* */

static void idpf_init(void)
{
  padico_print("init\n");
}

PADICO_MODULE_COMPONENT(PacketFilter_ID,
                        puk_component_declare("PacketFilter_ID",
                                              puk_component_provides("PadicoComponent", "component", &idpf_component_driver),
                                              puk_component_provides("PacketFilter", "pf", &idpf_driver)));

/* ********************************************************* */

struct id_state_s
{
  int dummy;
};
typedef struct id_state_s* id_state_t;

static void*idpf_instantiate(puk_instance_t assembly, puk_context_t context)
{
  const id_state_t state = padico_malloc(sizeof(struct id_state_s));
  return state;
}

static void idpf_destroy(void*state)
{
  padico_free(state);
}


/* 'out' allocated by encode function
 */
static void idpf_encode(void*state,
                        const void*in, size_t in_size,
                        void**out, size_t* out_size)
{
  void*p = padico_malloc(in_size);
  memcpy(p, in, in_size);
  *out = p;
  *out_size = in_size;
}

/* 'out' allocated by caller
*/
static void idpf_decode(void*state,
                        const void*in, size_t in_size,
                        void*out, size_t* out_size)
{
  assert(*out_size >= in_size);
  memcpy(out, in, in_size);
  *out_size = in_size;
}

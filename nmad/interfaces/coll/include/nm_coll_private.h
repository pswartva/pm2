/*
 * NewMadeleine
 * Copyright (C) 2014-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#ifndef NM_COLL_PRIVATE_H
#define NM_COLL_PRIVATE_H

#include <nm_public.h>
#include <nm_sendrecv_interface.h>
#include <nm_private.h>
#include <Padico/Puk.h>

#include "nm_coll_interface.h"

/* ********************************************************* */

/** maximum tag usable by enduser for p2p */
#define NM_COLL_TAG_MASK_P2P      0x7FFFFFFF
/** base tag used for collectives */
#define NM_COLL_TAG_BASE          0xFF000000
/** comm_create phase 1 */
#define NM_COLL_TAG_COMM_CREATE_1 ( NM_COLL_TAG_BASE | 0x04 )
/** comm_create phase 2 */
#define NM_COLL_TAG_COMM_CREATE_2 ( NM_COLL_TAG_BASE | 0x05 )

enum nm_coll_req_kind_e
  {
    NM_COLL_REQ_NONE = 0,
    NM_COLL_REQ_BARRIER,
    NM_COLL_REQ_BCAST_TREE,
    NM_COLL_REQ_BCAST_PIPELINE,
    NM_COLL_REQ_BCAST_2TREES,
    NM_COLL_REQ_SCATTER,
    NM_COLL_REQ_GATHER,
    NM_COLL_REQ_OTHER,   /**< external use of nm_coll_req_e */
    _NM_COLL_REQ_KIND_MAX
  };

typedef enum nm_coll_req_kind_e nm_coll_req_kind_t;

typedef void (*nm_coll_req_destructor_t)(struct nm_coll_req_s*);

struct nm_coll_req_s
{
  nm_coll_req_kind_t kind;
  nm_coll_req_destructor_t p_destructor;
  nm_cond_status_t status;            /**< status for explicit wait- not signaled if p_notify is non-NULL */
  nm_coll_req_notifier_t p_notify;    /**< notification function for op termination */
  void*p_ref;                         /**< reference given to above p_notify */
#ifdef NMAD_PROFILE
  puk_tick_t start_time;
#endif /* NMAD_PROFILE */
  int _payload;                       /**< dummy placeholder; payload is actually allocated after this struct */
};

struct nm_coll_req_s*nm_coll_req_alloc(nm_len_t payload_size, nm_coll_req_kind_t kind,
                                       nm_coll_req_destructor_t p_destructor,
                                       nm_coll_req_notifier_t p_notify, void*p_ref);

static inline void*nm_coll_req_payload(struct nm_coll_req_s*p_coll_req)
{
  return (void*)&p_coll_req->_payload;
}

static inline struct nm_coll_req_s*nm_coll_req_container(void*p_payload)
{
  struct nm_coll_req_s*p_coll_req = nm_container_of(p_payload, struct nm_coll_req_s, _payload);
  return p_coll_req;
}

/** used internally to signal the completion of a coll req */
void nm_coll_req_signal(struct nm_coll_req_s*p_coll_req);


/* ** profiling ******************************************** */

/** profiling block for collective ops */
struct nm_coll_profile_s
{
  unsigned long long n;           /**< number of times the operation was called */
  unsigned long long total_bytes; /**< total number of bytes sent through this op */
  unsigned long long total_nodes; /**< total number of nodes involved for all ocurrences of this op */
  double total_time_usecs;
};

#ifdef NMAD_PROFILE
#define NM_COLL_PROFILE_ADD(PROF_LOCAL, PROF_GLOBAL, BYTES, NODES, USECS) \
  do {                                                                  \
    if((PROF_LOCAL) != NULL)                                            \
      {                                                                 \
        nm_profile_inc((PROF_LOCAL)->n);                                \
        nm_profile_add((PROF_LOCAL)->total_bytes, BYTES);               \
        nm_profile_add((PROF_LOCAL)->total_nodes, NODES);               \
        (PROF_LOCAL)->total_time_usecs += USECS;                        \
      }                                                                 \
    if((PROF_GLOBAL) != NULL)                                           \
      {                                                                 \
        nm_profile_inc((PROF_GLOBAL)->n);                               \
        nm_profile_add((PROF_GLOBAL)->total_bytes, BYTES);              \
        nm_profile_add((PROF_GLOBAL)->total_nodes, NODES);              \
        (PROF_GLOBAL)->total_time_usecs += USECS;                       \
      }                                                                 \
  } while(0)

static inline void nm_coll_profile_def(struct nm_coll_profile_s*p_profile, const char*name)
{
  puk_profile_var_defx(unsigned_long_long, counter, &p_profile->n, 0,
                       "nm_coll", "total number of this coll op",
                       "nm_coll.%s.n", name);
  puk_profile_var_defx(unsigned_long_long, aggregate, &p_profile->total_bytes, 0,
                       "nm_coll", "total number of bytes sent through this coll op",
                       "nm_coll.%s.total_bytes", name);
  puk_profile_var_defx(unsigned_long_long, aggregate, &p_profile->total_nodes, 0,
                       "nm_coll", "total number of nodes involved in this coll op",
                       "nm_coll.%s.total_nodes", name);
  puk_profile_var_defx(double, timer, &p_profile->total_time_usecs, 0.0,
                       "nm_coll", "total time spent in this coll op",
                       "nm_coll.%s.total_time_usecs", name);
}



#else /* NMAD_PROFILE */
#define NM_COLL_PROFILE_ADD(PROF_LOCAL, PROF_GLOBAL, BYTES, NODES)
#endif /* NMAD_PROFILE */

#endif /* NM_COLL_PRIVATE_H */

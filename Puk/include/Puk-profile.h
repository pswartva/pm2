/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Profiling tools.
 * @ingroup Puk
 */

#ifndef PUK_PROFILE_H
#define PUK_PROFILE_H

/* ** profiling vars *************************************** */

enum puk_profile_type_e
  {
    puk_profile_type_unsigned_long,
    puk_profile_type_unsigned_long_long,
    puk_profile_type_double
  };

/** a profile variable */
struct puk_profile_var_s
{
  enum puk_profile_type_e type;
  enum puk_profile_class_e /**< class of profiling variables; see MPI-4 spec for details; values must match values in nm_mpi_types.h  */
    {
      puk_profile_class_state         = 1, /**< discrete set of state */
      puk_profile_class_level         = 2,
      puk_profile_class_size          = 3,
      puk_profile_class_percentage    = 4,
      puk_profile_class_highwatermark = 5,
      puk_profile_class_lowwatermark  = 6,
      puk_profile_class_counter       = 7,
      puk_profile_class_aggregate     = 8,
      puk_profile_class_timer         = 9,
      puk_profile_class_generic       = 10
    } var_class;
  union
  {
    unsigned long*as_unsigned_long;
    unsigned long long*as_unsigned_long_long;
    double*as_double;
  } value;
  char*label;
  char*desc;
  char*category;
};

PUK_VECT_TYPE(puk_profile_var, struct puk_profile_var_s*);

/** get the list of profile variables */
struct puk_profile_var_vect_s*puk_profile_get_vars(void);

/** add a new profiling variable */
void puk_profile_var_add(struct puk_profile_var_s profile_var);

/** macro to allow for compact pvar declaration with type-checking.
 * parameter LABEL is copied so that it is valid to give an ephemeral value.
 * @warning DESC is _not_ copied.
 */
#define puk_profile_var_def(TYPE, CLASS, VALUE, CATEGORY, LABEL, DESC)  \
  puk_profile_var_add((struct puk_profile_var_s)                        \
                      {                                                 \
                        .type = puk_profile_type_##TYPE,                \
                        .var_class = puk_profile_class_##CLASS,         \
                        .value.as_##TYPE = (VALUE),                     \
                        .category = CATEGORY,                           \
                        .label = padico_strdup(LABEL),                  \
                        .desc = DESC                                    \
                      });

#define puk_profile_var_defx(TYPE, CLASS, VALUE, INITVALUE, CATEGORY, DESC, ...) \
  do                                                                    \
    {                                                                   \
      padico_string_t s = padico_string_new();                          \
      padico_string_printf(s, __VA_ARGS__);                             \
      *(VALUE) = (INITVALUE);                                           \
      puk_profile_var_def(TYPE, CLASS, VALUE, CATEGORY, padico_string_get(s), DESC); \
      padico_string_delete(s);                                          \
    }                                                                   \
  while(0)

/** dump all profiling fields */
void puk_profile_dump(void);

const char*puk_profile_type_name(enum puk_profile_type_e t);

#endif /* PUK_PROFILE_H */

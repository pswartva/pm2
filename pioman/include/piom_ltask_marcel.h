/*
 * Pioman: a Generic I/O Manager
 * Copyright (C) 2008-2018 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#ifndef PIOM_LTASK_MARCEL_H
#define PIOM_LTASK_MARCEL_H

#ifndef PIOM_CONFIG_H
#  error "Cannot include this file directly. Please include <piom_private.h>."
#endif /* PIOM_CONFIG_H */

#ifndef PIOMAN_MARCEL
#error "inconsistency detected: PIOMAN_MARCEL not defined in piom_ltask_marcel.h"
#endif /* PIOMAN_MARCEL */


__PUK_SYM_INTERNAL void piom_ltask_marcel_init(void);
__PUK_SYM_INTERNAL void piom_ltask_marcel_exit(void);

static inline void piom_ltask_hook_preinvoke(void)
{
  marcel_tasklet_disable();
}
static inline void piom_ltask_hook_postinvoke(void)
{
  marcel_tasklet_enable();
}
static inline void piom_ltask_check_wait(void)
{
}

#endif /* PIOM_LTASK_MARCEL_H */

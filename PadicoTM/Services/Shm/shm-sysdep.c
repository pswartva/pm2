/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 */

#include <Padico/Module.h>

#if defined(MARCEL)
#  warning "Shm: Marcel is enabled- not building module 'Shm'"
#else /* MARCEL */

PADICO_MODULE_DECLARE(Shm, NULL, NULL, NULL, "Topology");

#include "Shm.h"
#include <Padico/PadicoTM.h>
#include <Padico/Topology.h>

#include <unistd.h>
#include <signal.h>
#include <semaphore.h>
#ifdef HAVE_SYS_VFS_H
#include <sys/vfs.h>
#endif /* HAVE_SYS_VFS_H */


static void padico_shm_seg_init(struct padico_shm_seg_s*seg);
static void padico_shm_directory_clean(struct padico_shm_s*shm);
static void padico_shm_directory_register_self(struct padico_shm_s*shm);
static void padico_shm_directory_unregister_self(struct padico_shm_s*shm);


/* ********************************************************* */
/* ** segment creation */

#ifdef SHM_USE_POSIX

struct padico_shm_s*padico_shm_init(const char*name)
{
  static const int max_retry = 20;
  static const int size = sizeof(struct padico_shm_seg_s);
  int fd = -1;
  int count = 0;
  int err = 0;

  struct padico_shm_s*shm = padico_malloc(sizeof(struct padico_shm_s));
  shm->rank = -1;
  shm->self = NULL;
  shm->seg = NULL;
  shm->shm_path = NULL;
  shm->sem_path = NULL;

  padico_string_t p = padico_string_new();
  padico_string_printf(p, "/dev/shm/shm-%s-%s.tmp", name, padico_getenv("USER"));
  shm->shm_path = padico_strdup(padico_string_get(p));
  padico_string_delete(p);

#ifdef HAVE_SYS_VFS_H
  {
    struct statfs s;
    int rc = statfs("/dev/shm", &s);
    if(rc == -1)
      padico_fatal("statfs() failed for /dev/shm.\n");
    const long long free_size = s.f_bsize * s.f_bavail;
    const long long total_size = s.f_bsize * s.f_blocks;
    const long long need_size = sizeof(struct padico_shm_seg_s);
    padico_print("/dev/shm total size = %lld; free size = %lld; needed size = %lld\n",
                 total_size, free_size, need_size);
    if(free_size < need_size)
      {
        padico_fatal("not enough free space on /dev/shm; need at least %lld bytes.\n", need_size);
      }
  }
#endif /* HAVE_SYS_VFS_H */

  padico_out(20, "opening shared segment.\n");
  do
    {
      fd  = PUK_ABI_FSYS_WRAP(open)(shm->shm_path, O_RDWR|O_CREAT, 0600);
      err = __puk_abi_wrap__errno;
      count++;
    }
  while(count < max_retry && fd == -1 && err == EINTR);
  if(fd == -1)
    padico_fatal("cannot create shared segment: %s (%d)- count=%d\n", strerror(err), err, count);
  int rc = PUK_ABI_FSYS_WRAP(ftruncate)(fd, size);
  err = __puk_abi_wrap__errno;
  if(rc == -1)
    padico_fatal("%s (%d).\n", strerror(err), err);
  padico_out(20, "mapping shared segment.\n");
  struct padico_shm_seg_s*seg = PUK_ABI_FSYS_WRAP(mmap)(NULL, size, PROT_READ|PROT_WRITE, MAP_SHARED | MAP_POPULATE, fd, 0);
  err = __puk_abi_wrap__errno;
  if(seg == MAP_FAILED)
    padico_fatal("error while mapping shared segment: %s (%d)- fd = %d.\n", strerror(err), err, fd);
  PUK_ABI_FSYS_WRAP(close)(fd);
  shm->seg = seg;
  padico_out(20, "shared segment ok.\n");

  /* acquire semaphore */
  padico_string_t s_sem_path = padico_string_new();
  padico_string_printf(s_sem_path, "/shm-%s-%s.sem", name, padico_getenv("USER"));
  shm->sem_path = padico_strdup(padico_string_get(s_sem_path));
  padico_string_delete(s_sem_path);
  shm->sem = NULL;
 retry_open:
  errno = 0;
  shm->sem = sem_open(shm->sem_path, O_CREAT, 0600, 1);
  if(shm->sem == NULL)
    {
      padico_warning("error %d (%s) while opening semaphore.\n", errno, strerror(errno));
      puk_sleep(2);
      goto retry_open;
    }
  struct timespec t;
  clock_gettime(CLOCK_REALTIME, &t);
  t.tv_sec += 3;
 retry_wait:
  rc = sem_timedwait(shm->sem, &t);
  if(rc == -1)
    {
      if(errno == EINTR)
        goto retry_wait;
      else if(errno == ETIMEDOUT)
        {
          padico_warning("timeout on semaphore wait. Recovering shared segment.\n");
          sem_close(shm->sem);
          sem_unlink(shm->sem_path);
          goto retry_open;
        }
      else
        {
          padico_fatal("error %d while waiting for semaphore (%s).\n", errno, strerror(errno));
        }
    }
  rc = sem_post(shm->sem);
  assert(rc == 0);
  rc = sem_wait(shm->sem);
  if(rc != 0)
    {
      padico_fatal("failed to acquire semaphore; error %d (%s).\n", errno, strerror(errno));
    }

  int val = -1;
  sem_getvalue(shm->sem, &val);
  if(val > 0)
    {
      padico_fatal("semaphore %s in inconsistent state (val = %d).\n", shm->sem_path, val);
    }

  if( (strncmp(seg->build_string.date, __DATE__, strlen(__DATE__)) != 0 ) ||
      (strncmp(seg->build_string.time, __TIME__, strlen(__TIME__)) != 0) )
    {
      padico_shm_seg_init(seg);
    }
  else
    {
      padico_shm_directory_clean(shm);
    }

  /* register self node */
  padico_shm_directory_register_self(shm);

  /* release semaphore */
  sem_post(shm->sem);

  padico_print("%d x %dKB blocks- %d slots; directory = %luKB; buffer = %luKB\n",
               PADICO_SHM_NUMBLOCKS, PADICO_SHM_BLOCKSIZE/1024, PADICO_SHM_NUMNODES,
               sizeof(shm->seg->directory) / 1024,
               sizeof(shm->seg->buffer) / 1024);

  return shm;
}

void padico_shm_close(struct padico_shm_s*shm)
{
  if(shm->shm_path)
    {
      int rc = sem_wait(shm->sem);
      if(rc != 0)
        {
          padico_fatal("failed to acquire semaphore; error %d (%s).\n", errno, strerror(errno));
        }
      padico_shm_directory_unregister_self(shm);
      /* check whether we are last process */
      int numnodes = 0;
      int i;
      for(i = 0; i < PADICO_SHM_NUMNODES; i++)
        {
          struct padico_shm_node_s*n = &shm->seg->directory.nodes[i];
          if(n->enabled)
            {
              numnodes++;
            }
        }
      if(numnodes == 0)
        {
          padico_out(10, "last process; unlink shm path %s\n", shm->shm_path);
          unlink(shm->shm_path);
        }
      rc = sem_post(shm->sem);
      assert(rc == 0);
      padico_free(shm->shm_path);
    }
  sem_close(shm->sem);
  if(shm->sem_path)
    {
#warning TODO- find a way to unlink the semaphore without creating race condition
      /*
        sem_unlink(shm->sem_path);
      */
      padico_free(shm->sem_path);
    }
  padico_free(shm);
}

#elif SHM_USE_IPC
#  error "Shm IPC not implemented yet"
#else
#  error "No shm method defined"
#endif


/* ********************************************************* */
/* ** segment and directory management */

/** Initializes a newly created segment.
 * Asserts semaphore is held.
 */
static void padico_shm_seg_init(struct padico_shm_seg_s*seg)
{
  int i;
  strcpy(seg->build_string.date, __DATE__);
  strcpy(seg->build_string.time, __TIME__);
  padico_shm_block_lfqueue_init(&seg->buffer.free_blocks);
  for(i = 0; i < PADICO_SHM_NUMBLOCKS; i++)
    {
      padico_shm_block_lfqueue_enqueue(&seg->buffer.free_blocks, i);
    }
  padico_shm_queue_lfqueue_init(&seg->buffer.free_queues);
  for(i = 0; i < PADICO_SHM_NUMQUEUES; i++)
    {
      padico_shm_large_lfqueue_init(&seg->buffer.queues[i]);
      padico_shm_queue_lfqueue_enqueue(&seg->buffer.free_queues, i);
    }
  for(i = 0; i < PADICO_SHM_NUMNODES; i++)
    {
      struct padico_shm_node_s*node = &seg->directory.nodes[i];
      node->enabled = 0;
      padico_shm_block_lfqueue_init(&seg->buffer.pending_blocks[i]);
    }
}

/** Cleans directory.
 * Asserts semaphore is held.
 */
static void padico_shm_directory_clean(struct padico_shm_s*shm)
{
  int numnodes = 0;
  int i;
  for(i = 0; i < PADICO_SHM_NUMNODES; i++)
    {
      struct padico_shm_node_s*n = &shm->seg->directory.nodes[i];
      if(n->enabled)
        {
          int pid = n->pid;
          int rc = kill(pid, 0); /* check that the process is running */
          if(rc)
            {
              padico_print("cleaning node %s (pid=%d)\n", n->node_uuid, n->pid);
              n->enabled = 0;
              n->pid = -1;
            }
          else
            numnodes++;
        }
    }
  if(numnodes == 0)
    {
      /* no nodes in directory- reinit for safety (recover all blocks, etc.) */
      padico_shm_seg_init(shm->seg);
    }
}

struct padico_shm_node_s*padico_shm_directory_node_lookup(const struct padico_shm_s*shm, padico_topo_node_t node)
{
  int retry_count = 100;
  int i;
  padico_topo_uuid_t uuid = padico_topo_node_getuuid(node);
 retry:
  for(i = 0; i < PADICO_SHM_NUMNODES; i++)
    {
      struct padico_shm_node_s*n = &shm->seg->directory.nodes[i];
      if(n->enabled && memcmp(n->node_uuid, uuid, PADICO_TOPO_UUID_SIZE) == 0)
        {
          return n;
        }
    }
  if(retry_count-- > 0)
    {
      __sync_synchronize();
      puk_usleep(1000);
      goto retry;
    }
  return NULL;
}

/** Register local node into directory
 * Asserts semaphore is held.
 */
static void padico_shm_directory_register_self(struct padico_shm_s*shm)
{
  int i;
  /* find slot */
 retry:
  for(i = 0; i < PADICO_SHM_NUMNODES; i++)
    {
      struct padico_shm_node_s*node = &shm->seg->directory.nodes[i];
      if(!node->enabled)
        {
          int e = __sync_fetch_and_add(&node->enabled, 1);
          if(e != 0)
            {
              __sync_fetch_and_sub(&node->enabled, 1);
              padico_warning("race detected while registering self; retrying...\n");
              goto retry;
            }
          assert(node->enabled == 1);
          node->pid = getpid();
          node->rank = i;
          padico_shm_short_lfqueue_init(&node->mailbox.shared_queue);
          padico_shm_blockcache_lfqueue_init(&node->mailbox.block_cache);
          int j;
          for(j = 0; j < PADICO_SHM_NUMNODES; j++)
            {
              node->mailbox.large[j].pipeline = -1;
              padico_shm_short_lfqueue_init(&node->mailbox.short_queues[j]);
            }
          shm->rank = i;
          shm->self = node;
          /* fill uuid last so that other nodes don't use the entry before it's complete */
          memcpy(node->node_uuid, padico_topo_node_getuuid(padico_topo_getlocalnode()), PADICO_TOPO_UUID_SIZE + 1);
          node->node_uuid[PADICO_TOPO_UUID_SIZE] = '\0';
          padico_out(40, "I am node #%d\n", i);
          struct padico_shm_node_s*self = padico_shm_directory_node_lookup(shm, padico_topo_getlocalnode());
          if(self == NULL)
            {
              padico_fatal("inconsistency detected in directory.\n");
            }
          return;
        }
    }
  padico_fatal("cannot register self- directory full.\n");
}

/** Unregister local node into directory
 * Asserts semaphore is held.
 */
static void padico_shm_directory_unregister_self(struct padico_shm_s*shm)
{
  struct padico_shm_node_s*node = shm->self;
  assert(node->pid == getpid());
  node->enabled = 0;
  node->pid = -1;
  shm->rank = -1;
  shm->self = NULL;
}

void padico_shm_directory_dump(struct padico_shm_s*shm)
{
  int i;
  for(i = 0; i < PADICO_SHM_NUMNODES; i++)
    {
      struct padico_shm_node_s*node = &shm->seg->directory.nodes[i];
      fprintf(stderr, "# local = %s; i = %d; enabled = %d; rank = %d; uuid = %s \n",
              (const char*)padico_topo_node_getuuid(padico_topo_getlocalnode()), i, node->enabled, node->rank, node->node_uuid);
    }
}


/* ********************************************************* */
/* ** blocks */

int padico_shm_block_alloc(struct padico_shm_s*shm)
{
  int block = padico_shm_blockcache_lfqueue_dequeue_single_reader(&shm->self->mailbox.block_cache);
  if(block == -1)
    {
      block = padico_shm_block_lfqueue_dequeue(&shm->seg->buffer.free_blocks);
    }
  if(block == -1)
    {
      /*
        padico_warning("padico_shm_block_alloc()- out of space in shared memory segment.\n");
        puk_usleep(5);
      */
      return block;
    }
  assert(block >= 0);
  assert(block < PADICO_SHM_NUMBLOCKS);
  return block;
}


/* ********************************************************* */
/* ** large messages */

void padico_shm_large_send(struct padico_shm_s*shm, const void*bytes, size_t size, struct padico_shm_node_s*dest)
{
  int queue_num = -1;
  while(dest->mailbox.large[shm->rank].pipeline == -1)
    {
      /* wait the receiver to be ready to receive */
      __sync_synchronize();
    }
  queue_num = dest->mailbox.large[shm->rank].pipeline;
  dest->mailbox.large[shm->rank].pipeline = -1; /* consume RTR before send to avoid races */
  assert(queue_num != -1);

  int done = 0;
  do
    {
      int block_num = -1;
      do
        {
          block_num = padico_shm_block_alloc(shm);
        }
      while(block_num == -1);
      void*block = padico_shm_block_get_ptr(shm, block_num);
      padico_out(40, "copying block #%d\n", block_num);
      const int todo = size - done;
      const int b = (todo > PADICO_SHM_BLOCKSIZE) ? PADICO_SHM_BLOCKSIZE : todo;
      memcpy(block, bytes + done, b);
      done += b;
      padico_out(40, "commit block #%d\n", block_num);
      int rc = -1;
      do
        {
          rc = padico_shm_large_lfqueue_enqueue(&shm->seg->buffer.queues[queue_num], block_num);
        }
      while(rc != 0);
    }
  while(done < size);
  padico_out(40, "done.\n");
}

int padico_shm_large_recv_post(struct padico_shm_s*shm, int from)
{
  assert(shm->self->mailbox.large[from].pipeline == -1);
  /* allocate queue and signal RTR */
  int queue_num = -1;
  while(queue_num == -1)
    queue_num = padico_shm_queue_lfqueue_dequeue(&shm->seg->buffer.free_queues);
  shm->self->mailbox.large[from].pipeline = queue_num;
  __sync_synchronize();
  return queue_num;
}
int padico_shm_large_recv_poll(struct padico_shm_s*shm, void*bytes, size_t size, int queue_num)
{
  int done = 0;
  padico_out(40, "recv large- size=%d; done=%d\n", (int)size, done);
  while(done < size)
    {
      int block_num = -1;
      padico_out(40, "waiting chunk- size=%d; done=%d\n", (int)size, done);
      do
        {
          block_num = padico_shm_large_lfqueue_dequeue(&shm->seg->buffer.queues[queue_num]);
          if(block_num == -1 && done == 0)
            return -1;
        }
      while(block_num == -1);
      const int todo = size - done;
      int b = (todo > PADICO_SHM_BLOCKSIZE) ? PADICO_SHM_BLOCKSIZE : todo;
      void*block = padico_shm_block_get_ptr(shm, block_num);
      memcpy(bytes + done, block, b);
      done += b;
      padico_out(40, "copy done- size=%d; done=%d\n", (int)size, done);
      padico_shm_block_free(shm, block_num);
    }
  /* release queue */
  padico_shm_queue_lfqueue_enqueue(&shm->seg->buffer.free_queues, queue_num);
  return 0;
}

void padico_shm_large_recv(struct padico_shm_s*shm, void*bytes, size_t size, int from)
{
  int queue_num = padico_shm_large_recv_post(shm, from);
  int rc = -1;
  do
    {
      rc = padico_shm_large_recv_poll(shm, bytes, size, queue_num);
    }
  while(rc != 0);
}

#endif /* MARCEL */

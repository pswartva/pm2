/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


/* *********************************************************
 * Rationale of the newmad/ibverbs driver
 *
 * This is a multi-method driver. Four methods are available:
 *   -- bycopy: data is copied in a pre-allocated, pre-registered
 *      memory region, then sent through RDMA into a pre-registered
 *      memory region of the peer node.
 *   -- adaptrdma: data is copied in a pre-allocated, pre-registered
 *      memory region, with an adaptive super-pipeline. Memory blocks are
 *      copied as long as the previous RDMA send doesn't complete.
 *      A guard check ensures that block size progression is at least
 *      2-base exponential to prevent artifacts to kill performance.
 *   -- regrdma: memory is registered on the fly on both sides, using
 *      a pipeline with variable block size. For each block, the
 *      receiver sends an ACK with RDMA info (raddr, rkey), then
 *      zero-copy RDMA is performed.
 *   -- rcache: memory is registered on the fly on both sides,
 *      sequencially, using puk_mem_* functions from Puk-ABI that
 *      manage a cache.
 *
 * Method is chosen as follows:
 *   -- tracks for small messages always uses 'bycopy'
 *   -- tracks with rendez-vous use 'adaptrdma' for smaller messages
 *      and 'regrdma' for larger messages. Usually, the threshold
 *      is 224kB (from empirical results about registration/send overlap)
 *      on MT23108, and 2MB on ConnectX.
 *   -- tracks with rendez-vous use 'rcache' when ib_rcache is activated
 *      in the flavor.
 */


#include "nm_ibverbs.h"
#include <Padico/Module.h>
#include <Padico/Puk.h>
#include <nm_data.h>
#include <nm_private.h>
#ifdef NMAD_HWLOC
#include <hwloc.h>
#include <hwloc/openfabrics-verbs.h>
#endif /* NMAD_HWLOC */
#ifdef NMAD_PADICOTM
#include <Padico/SysIO.h>
#endif /* NMAD_PADICOTM*/

#include <unistd.h>
#include <fcntl.h>
#include <poll.h>
#include <sys/resource.h>

static void nm_ibverbs_common_init(void);
static void nm_ibverbs_common_finalize(void);

static const struct puk_component_driver_s nm_ibverbs_common_component =
  {
    .instantiate        = NULL,
    .destroy            = NULL,
    .component_init     = &nm_ibverbs_common_init,
    .component_finalize = &nm_ibverbs_common_finalize
  };

struct nm_ibverbs_context_s*nm_ibverbs_context_new(puk_context_t p_context, const struct nm_ibverbs_opts_s*p_opts);

static const struct nm_ibverbs_iface_s nm_ibverbs_common_iface =
  {
    .context_new = &nm_ibverbs_context_new
  };

PADICO_MODULE_COMPONENT(NewMad_ibverbs_common,
  puk_component_declare("NewMad_ibverbs_common",
                        puk_component_provides("PadicoComponent", "component", &nm_ibverbs_common_component),
                        puk_component_provides("NewMad_ibverbs", "ibverbs", &nm_ibverbs_common_iface),
                        puk_component_attr("ibv_device", "auto"),
                        puk_component_attr("ibv_port", "auto"),
                        puk_component_attr("subnet_prefix", "auto"),
                        puk_component_attr("network", "Infiniband-default")));

PADICO_MODULE_ATTR(lazy_connect, "NMAD_IBVERBS_LAZY_CONNECT", "enable lazy connection in ibverbs drivers (default: off)", bool, 0);

/** checksum algorithm to use with ibverbs */
static puk_checksum_t _nm_ibverbs_checksum = NULL;

/** ptr alignment enforced in every packets (through padding) */
int nm_ibverbs_alignment = 64;
/** ptr alignment enforced for buffer allocation */
int nm_ibverbs_memalign  = 4096;

PUK_VECT_TYPE(nm_ibverbs_hca, struct nm_ibverbs_hca_s*);

/** per-device descriptor */
struct nm_ibverbs_device_s
{
  struct ibv_device*ib_dev;
  struct nm_ibverbs_hca_vect_s contexts; /**< list of HCA contexts using the same device */
};

PUK_HASHTABLE_TYPE(nm_ibverbs_device, struct ibv_device*, struct nm_ibverbs_device_s*,
                   &puk_hash_pointer_default_hash, &puk_hash_pointer_default_eq, NULL);

static struct
{
  struct nm_ibverbs_device_hashtable_s device_table;
  nm_len_t total_mem_reg;  /**< total amount of memory registered */
  rlim_t memlock;          /**< maximum number of bytes of memory that can be locked, as returned by RLIMIT_MEMLOCK */
} nm_ibverbs_common = { .total_mem_reg = 0, .memlock = 0 };


/* ********************************************************* */

static void nm_ibverbs_cnx_qp_reset(struct nm_ibverbs_cnx_s*p_ibverbs_cnx);
static void nm_ibverbs_cnx_qp_init(struct nm_ibverbs_cnx_s*p_ibverbs_cnx);
static void nm_ibverbs_cnx_qp_rtr(struct nm_ibverbs_cnx_s*p_ibverbs_cnx);
static void nm_ibverbs_cnx_qp_rts(struct nm_ibverbs_cnx_s*p_ibverbs_cnx);
static void nm_ibverbs_cnx_qp_connect(struct nm_ibverbs_cnx_s*p_ibverbs_cnx);

/* ********************************************************* */

static void nm_ibverbs_common_init(void)
{
  static int init_done = 0;
  if(init_done)
    {
      NM_FATAL("init already done.\n");
    }
  init_done = 1;
  const char*checksum_env = getenv("NMAD_IBVERBS_CHECKSUM");
  if(_nm_ibverbs_checksum == NULL && checksum_env != NULL)
    {
      puk_checksum_t checksum = puk_checksum_get(checksum_env);
      if(checksum == NULL)
        NM_FATAL("checksum algorithm *%s* not available.\n", checksum_env);
      _nm_ibverbs_checksum = checksum;
      NM_DISPF("checksum enabled (%s).\n", checksum->name);
    }
  const char*align_env = getenv("NMAD_IBVERBS_ALIGN");
  if(align_env != NULL)
    {
      nm_ibverbs_alignment = atoi(align_env);
      NM_DISPF("alignment forced to %d\n", nm_ibverbs_alignment);
    }
  const char*memalign_env = getenv("NMAD_IBVERBS_MEMALIGN");
  if(memalign_env != NULL)
    {
      nm_ibverbs_memalign = atoi(memalign_env);
      NM_DISPF("memalign forced to %d\n", nm_ibverbs_memalign);
    }
  nm_ibverbs_device_hashtable_init(&nm_ibverbs_common.device_table);
  assert(nm_ibverbs_common.total_mem_reg == 0);
  /* get memlock rlimit */
  struct rlimit rlim;
  int rc = getrlimit(RLIMIT_MEMLOCK, &rlim);
  if(rc == 0)
    {
      if(rlim.rlim_cur == 0)
        {
          NM_FATAL("RLIMIT_MEMLOCK = %lu; cannot register memory.\n", rlim.rlim_cur);
        }
      else
        {
          NM_DISPF("RLIMIT_MEMLOCK = %lu\n", rlim.rlim_cur);
          nm_ibverbs_common.memlock = rlim.rlim_cur;
        }
    }
}

static void nm_ibverbs_common_finalize(void)
{
  if(nm_ibverbs_common.total_mem_reg != 0)
    {
      NM_WARN("remaining while finalizing: mem_reg = %lu\n", nm_ibverbs_common.total_mem_reg);
    }
}

/** get the ibverbs component from its parent component & create context */
struct nm_ibverbs_context_s*nm_ibverbs_context_resolve_opts(puk_context_t p_context, const struct nm_ibverbs_opts_s*p_opts)
{
  if(p_context == NULL)
    {
      NM_FATAL("parent context is NULL; cannot create ibverbs context.\n");
    }
  puk_component_conn_t p_ibverbs_conn = puk_context_conn_lookup(p_context, puk_iface_NewMad_ibverbs(), "ibverbs");
  if(p_ibverbs_conn == NULL)
    {
      NM_FATAL("'ibverbs' receptacle not found in component '%s'.\n", p_context->component->name);
    }
  if(p_ibverbs_conn->facet == NULL || p_ibverbs_conn->facet->driver == NULL)
    {
      NM_FATAL("'ibverbs' facet not connected in component '%s', cannot create ibverbs context.\n",
               p_context->component->name);
    }
  const struct nm_ibverbs_iface_s*p_ibverbs_iface = p_ibverbs_conn->facet->driver;
  return (p_ibverbs_iface->context_new)(p_ibverbs_conn->context, p_opts);
}

/** get in ibverbs context from a puk context */
struct nm_ibverbs_context_s*nm_ibverbs_context_new(puk_context_t p_context, const struct nm_ibverbs_opts_s*p_opts)
{
  struct nm_ibverbs_context_s*p_ibverbs_context = padico_malloc(sizeof(struct nm_ibverbs_context_s));
  memset(p_ibverbs_context, 0, sizeof(struct nm_ibverbs_context_s));
  const int use_comp_channel = p_opts->use_comp_channel;
  const int need_srq = p_opts->use_srq;
  const char*device = puk_context_getattr(p_context, "ibv_device");
  const char*s_port = puk_context_getattr(p_context, "ibv_port");
  p_ibverbs_context->ib_opts.mtu = p_opts->mtu;
  /* open HCA */
  if(device == NULL || s_port == NULL)
    NM_FATAL("cannot find ibv_device/ibv_port attributes.\n");
  const int port = (strcmp(s_port, "auto") == 0) ? 1 : atoi(s_port);
  nm_ibverbs_hca_open(&p_ibverbs_context->hca, device, port);
  /* open SRQ & comp channel if needed */
  p_ibverbs_context->srq.p_srq = NULL;
  p_ibverbs_context->srq.srq_cq = NULL;
  if(need_srq)
    {
      if(p_ibverbs_context->hca.ib_caps.max_srq > 0)
        {
          struct ibv_comp_channel*p_comp_channel = NULL;
          struct ibv_srq_init_attr srq_init_attr = {
            .srq_context  = p_ibverbs_context,
            .attr.max_wr  = NM_IBVERBS_RX_DEPTH,
            .attr.max_sge = NM_IBVERBS_MAX_SG_RQ };
          p_ibverbs_context->srq.p_srq = ibv_create_srq(p_ibverbs_context->hca.pd, &srq_init_attr);
          if(p_ibverbs_context->srq.p_srq == NULL)
            {
              NM_WARN("cannot allocate SRQ.\n");
              p_ibverbs_context->srq.p_srq = NULL;
            }
          if(use_comp_channel)
            {
              p_comp_channel = ibv_create_comp_channel(p_ibverbs_context->hca.context);
              if(p_comp_channel == NULL)
                {
                  NM_FATAL("cannot create comp_channel (%s).\n", strerror(errno));
                }
              p_ibverbs_context->srq.comp_channel = p_comp_channel;
              p_ibverbs_context->ib_opts.use_comp_channel = 1;
              NM_SYS(pipe)(p_ibverbs_context->srq.cancel_fd);
              p_ibverbs_context->srq.waiting = 0;
            }
          p_ibverbs_context->srq.srq_cq = ibv_create_cq(p_ibverbs_context->hca.context, NM_IBVERBS_SRQ_DEPTH,
                                                    p_ibverbs_context, p_comp_channel, 0);
          if(p_ibverbs_context->srq.srq_cq == NULL)
            {
              NM_FATAL("cannot create CQ (%s).\n", strerror(errno));
            }
          p_ibverbs_context->ib_opts.use_srq = 1;
          if(p_ibverbs_context->ib_opts.use_comp_channel)
            {
              int rc = ibv_req_notify_cq(p_ibverbs_context->srq.srq_cq, 0);
              if(rc)
                {
                  NM_FATAL("couldn't request CQ notification (%s)\n", strerror(errno));
                }
            }
        }
      else
        {
          NM_FATAL(" SRQ requested but not available on hardware.\n");
        }
    }
  NM_DISPF("  SRQ available = %d; enabled = %d; comp_channel = %d; MTU = %d\n",
           (p_ibverbs_context->hca.ib_caps.max_srq > 0),
           p_ibverbs_context->ib_opts.use_srq, p_ibverbs_context->ib_opts.use_comp_channel,
           nm_ibverbs_mtu_values[p_opts->mtu]);
  /* open UD endpoint & pre-post a recv */
  nm_ibverbs_ud_ep_open(&p_ibverbs_context->connect.udep, &p_ibverbs_context->hca);
  nm_ibverbs_cnx_addr_entry_hashtable_init(&p_ibverbs_context->connect.addr_hash);
  nm_ibverbs_cnx_qpn_hashtable_init(&p_ibverbs_context->connect.cnx_by_qpn);
  nm_ibverbs_cnx_addr_entry_list_init(&p_ibverbs_context->connect.pending_entries);
  memset(&p_ibverbs_context->connect.recv_packets, 0, NM_IBVERBS_RX_DEPTH * sizeof(struct nm_ibverbs_cnx_connect_packet_s));
  nm_ibverbs_ud_recv_pool_post(&p_ibverbs_context->connect.recv_pool, &p_ibverbs_context->connect.udep,
                               &p_ibverbs_context->connect.recv_packets[0], sizeof(struct nm_ibverbs_cnx_addr_msg_s),
                               NM_IBVERBS_RX_DEPTH - 1 );
  nm_spin_init(&p_ibverbs_context->connect.lock);
  /* init vectored connection */
  p_ibverbs_context->vector_connect.p_wide_url = NULL;
  p_ibverbs_context->vector_connect.session_size = -1;
  p_ibverbs_context->vector_connect.pp_cached_cnxs = NULL;
  return p_ibverbs_context;
}

void nm_ibverbs_context_set_srq_limit(struct nm_ibverbs_context_s*p_ibverbs_context, int srq_limit)
{
  struct ibv_srq_attr srq_attr =
    {
      .srq_limit = srq_limit
    };
  int rc = ibv_modify_srq(p_ibverbs_context->srq.p_srq, &srq_attr, IBV_SRQ_LIMIT);
  if(rc)
    {
      NM_WARN("cannot set SRQ limit.\n");
    }
}

void nm_ibverbs_context_get_url(struct nm_ibverbs_context_s*p_ibverbs_context, const void**pp_url, size_t*p_urlsize)
{
  *pp_url = &p_ibverbs_context->connect.udep.local_addr;
  *p_urlsize = sizeof(p_ibverbs_context->connect.udep.local_addr);
}


void nm_ibverbs_context_get_wide_url(struct nm_ibverbs_context_s*p_ibverbs_context, int session_size, int rank,
                                     const void**pp_url, size_t*p_urlsize)
{
  /* format of wide url:
   *   UD addr [ lid; qpn; psn ] | rank | cnx.qpn | cnx.qpn | cnx.qpn | ...
   */
  const size_t url_size = sizeof(struct nm_ibverbs_addr_s) + sizeof(uint32_t) + session_size * sizeof(uint32_t);
  void*p_wide_url = padico_malloc(url_size);
  p_ibverbs_context->vector_connect.pp_cached_cnxs = padico_malloc(session_size * sizeof(struct nm_ibverbs_cnx_s*));
  p_ibverbs_context->vector_connect.session_size = session_size;
  p_ibverbs_context->vector_connect.rank = rank;
  p_ibverbs_context->vector_connect.p_wide_url = p_wide_url;
  memcpy(p_wide_url, &p_ibverbs_context->connect.udep.local_addr, sizeof(struct nm_ibverbs_addr_s));
  memcpy(p_wide_url + sizeof(struct nm_ibverbs_addr_s),
         &p_ibverbs_context->vector_connect.rank, sizeof(uint32_t));
  int i;
  for(i = 0; i < session_size; i++)
    {
      struct nm_ibverbs_cnx_s*p_cnx = nm_ibverbs_cnx_create(p_ibverbs_context);
      if(p_cnx->local_addr.addr.qpn != p_cnx->local_addr.addr.psn)
        {
          NM_FATAL("inconsistency detected in addresses; psn != qpn.\n");
        }
      if(p_cnx->local_addr.addr.lid != p_ibverbs_context->connect.udep.local_addr.lid)
        {
          NM_FATAL("inconsistency detected in addresses; cnx.lid != UD.lid.\n");
        }
      memcpy(p_wide_url + sizeof(struct nm_ibverbs_addr_s) + sizeof(uint32_t) + i * sizeof(uint32_t),
             &p_cnx->local_addr.addr.qpn,
             sizeof(uint32_t));
      p_ibverbs_context->vector_connect.pp_cached_cnxs[i] = p_cnx;
    }

  *pp_url = p_wide_url;
  *p_urlsize = url_size;
}

void nm_ibverbs_context_delete(struct nm_ibverbs_context_s*p_ibverbs_context)
{
  if(p_ibverbs_context->srq.p_srq != NULL)
    ibv_destroy_srq(p_ibverbs_context->srq.p_srq);
  nm_ibverbs_hca_close(&p_ibverbs_context->hca);
  nm_ibverbs_cnx_qpn_hashtable_destroy(&p_ibverbs_context->connect.cnx_by_qpn);
  nm_ibverbs_cnx_addr_entry_hashtable_destroy(&p_ibverbs_context->connect.addr_hash);
  nm_ibverbs_cnx_addr_entry_list_destroy(&p_ibverbs_context->connect.pending_entries);
  padico_free(p_ibverbs_context);
}

int nm_ibverbs_context_wait_event(struct nm_ibverbs_context_s*p_ibverbs_context)
{
  p_ibverbs_context->srq.waiting = 1;
  if(p_ibverbs_context->ib_opts.use_comp_channel)
    {
      /* wait on fd set, to allow cancellation */
      struct pollfd fds[2];
      fds[0].fd      = p_ibverbs_context->srq.comp_channel->fd;
      fds[0].events  = POLLIN;
      fds[0].revents = 0;
      fds[1].fd      = p_ibverbs_context->srq.cancel_fd[0];
      fds[1].events  = POLLIN;
      fds[1].revents = 0;
      int rc = NM_SYS(poll)(fds, 2 /* nfds */, 0 /* timeout_ms */);
      if(rc > 0 && fds[1].revents & POLLIN)
        {
          char dummy;
          NM_SYS(read)(p_ibverbs_context->srq.cancel_fd[0], &dummy, 1);
        }
      else if(rc > 0 && fds[0].revents & POLLIN)
        {
          /* Wait for the completion event */
          struct ibv_cq*ev_cq = p_ibverbs_context->srq.srq_cq;
          void*ev_context = NULL;
          rc = ibv_get_cq_event(p_ibverbs_context->srq.comp_channel, &ev_cq, &ev_context);
          if(rc)
            {
              NM_WARN("failed to get cq_event\n");
              return -NM_EBROKEN;
            }
          assert(ev_cq == p_ibverbs_context->srq.srq_cq);
          /* Ack the event */
          ibv_ack_cq_events(p_ibverbs_context->srq.srq_cq, 1);

          /* Request notification upon the next completion event */
          rc = ibv_req_notify_cq(p_ibverbs_context->srq.srq_cq, 0);
          if(rc)
            {
              NM_WARN("couldn't request CQ notification\n");
              return -NM_EBROKEN;
            }
        }
      else if(rc < 0)
        {
          NM_WARN("error %d (%s) while polling completion channel.\n", errno, strerror(errno));
          return -NM_EBROKEN;
        }
    }
  else
    {
      NM_FATAL("ibv_comp_channel is disabled; cannot wait on comp_channel.\n");
    }
  p_ibverbs_context->srq.waiting = 0;
  return NM_ESUCCESS;
}

void nm_ibverbs_context_cancel_wait(struct nm_ibverbs_context_s*p_ibverbs_context)
{
  char dummy = 0;
  NM_SYS(write)(p_ibverbs_context->srq.cancel_fd[1], &dummy, 1);
  while(p_ibverbs_context->srq.waiting)
    {
      sched_yield();
    }
}

static int nm_ibverbs_hca_async_handler(int fd, void*_key)
{
  struct nm_ibverbs_hca_s*p_hca = _key;
  struct ibv_async_event event;
  int rc = ibv_get_async_event(p_hca->context, &event);
  if(rc == 0)
    {
      switch(event.event_type)
        {
        case IBV_EVENT_SRQ_LIMIT_REACHED:
          __sync_fetch_and_add(&p_hca->async_events.srq_limit_reached, 1);
          NM_DISPF("SRQ limit reached.\n");
          break;
        default:
          break;
        }
    }
  ibv_ack_async_event(&event);

  return 1;
}


void nm_ibverbs_hca_open(struct nm_ibverbs_hca_s*p_hca, const char*device, int port)
{
  if(port == -1)
    port = 1;
  const int autodetect = (strcmp(device, "auto") == 0);
  /* find IB device */
  int dev_amount = -1;
  int dev_auto = 0;
  struct ibv_device**dev_list = ibv_get_device_list(&dev_amount);
  if(!dev_list)
    {
      NM_FATAL("no device found.\n");
    }
 retry_autodetect:
  if(autodetect)
    {
      p_hca->ib_dev = dev_list[dev_auto];
    }
  else
    {
      struct ibv_device**dev_ptr = dev_list;
      p_hca->ib_dev = NULL;
      while((p_hca->ib_dev == NULL) && (*dev_ptr != NULL))
        {
          const char*ib_devname = ibv_get_device_name(*dev_ptr);
          if(strcmp(ib_devname, device) == 0)
            {
              p_hca->ib_dev = *dev_ptr;
              break;
            }
          dev_ptr++;
        }
    }
  if(p_hca->ib_dev == NULL)
    {
      NM_FATAL("cannot find required device %s.\n", device);
    }

  /* open IB context */
  p_hca->context = ibv_open_device(p_hca->ib_dev);
  if(p_hca->context == NULL)
    {
      NM_FATAL("cannot open IB context (%s).\n", strerror(errno));
    }

  /* get IB context attributes */
  struct ibv_device_attr device_attr;
  int rc = ibv_query_device(p_hca->context, &device_attr);
  if(rc != 0)
    {
      NM_FATAL("cannot get device capabilities (%s).\n", strerror(errno));
    }

  /* detect LID */
  struct ibv_port_attr port_attr;
  rc = ibv_query_port(p_hca->context, port, &port_attr);
  if(rc != 0)
    {
      NM_FATAL("dev = %s; port = %d; cannot get local port attributes (%s).\n",
               ibv_get_device_name(p_hca->ib_dev), port, strerror(rc));
    }
  if(port_attr.state != IBV_PORT_ACTIVE)
    {
      NM_WARN("dev = %s; port = %d; port is down.\n",
              ibv_get_device_name(p_hca->ib_dev), port);
      if(autodetect)
        {
          if(port < device_attr.phys_port_cnt)
            port++;
          else
            dev_auto++;
          if(dev_list[dev_auto] == NULL)
            {
              NM_FATAL("cannot find active IB device.\n");
            }
          goto retry_autodetect;
        }

    }
  p_hca->lid = port_attr.lid;
  if(p_hca->lid == 0)
    {
      NM_WARN("LID is null- subnet manager has probably crashed.\n");
    }

  /* IB capabilities */
  p_hca->ib_caps.max_qp        = device_attr.max_qp;
  p_hca->ib_caps.max_qp_wr     = device_attr.max_qp_wr;
  p_hca->ib_caps.max_cq        = device_attr.max_cq;
  p_hca->ib_caps.max_cqe       = device_attr.max_cqe;
  p_hca->ib_caps.max_mr        = device_attr.max_mr;
  p_hca->ib_caps.max_srq       = device_attr.max_srq;
  p_hca->ib_caps.max_mr_size   = device_attr.max_mr_size;
  p_hca->ib_caps.page_size_cap = device_attr.page_size_cap;
  p_hca->ib_caps.max_msg_size  = port_attr.max_msg_sz;

  int link_width = -1;
  switch(port_attr.active_width)
    {
    case 1: link_width = 1;  break;
    case 2: link_width = 4;  break;
    case 4: link_width = 8;  break;
    case 8: link_width = 12; break;
    case 16: link_width = 2; break;
    default:
      NM_WARN("unknown active_width = %d\n", port_attr.active_width);
      break;
    }
  int link_rate = -1;
  const char*s_link_rate = "unknown";
  switch(port_attr.active_speed)
    {
    case 0x01: link_rate = 2;  s_link_rate = "SDR";   break; /* 2.5 Gbps on the wire + 8/10 encoding */
    case 0x02: link_rate = 4;  s_link_rate = "DDR";   break; /* 5 Gbps on the wire + 8/10 encoding  */
    case 0x04: link_rate = 8;  s_link_rate = "QDR";   break; /* 8 Gbps on the wire + 8/10 encoding  */
    case 0x08: link_rate = 10; s_link_rate = "FDR10"; break; /* 10 Gbps + 64/66 encoding */
    case 0x10: link_rate = 14; s_link_rate = "FDR";   break; /* 14 Gbps + 64/66 encoding */
    case 0x20: link_rate = 25; s_link_rate = "EDR";   break; /* 25 Gbps + 64/66 encoding */
    case 0x40: link_rate = 50; s_link_rate = "HDR";   break; /* 50 Gbps + 64/66 encoding */
    case 0x80: link_rate = 100; s_link_rate = "NDR";  break;
    default:
      NM_WARN("unknown active_speed = %d\n", port_attr.active_speed);
      break;
    }
  p_hca->ib_caps.data_rate = link_width * link_rate;

  NM_DISPF("device '%s'- %dx %s (%d Gb/s); LID = 0x%02X\n",
           ibv_get_device_name(p_hca->ib_dev), link_width, s_link_rate, p_hca->ib_caps.data_rate, p_hca->lid);
#ifdef DEBUG
  NM_DISPF("  max_qp=%d; max_qp_wr=%d; max_cq=%d; max_cqe=%d; max_srq=%d\n",
           p_hca->ib_caps.max_qp, p_hca->ib_caps.max_qp_wr,
           p_hca->ib_caps.max_cq, p_hca->ib_caps.max_cqe,
           p_hca->ib_caps.max_srq);
  NM_DISPF("  max_mr=%d; max_mr_size=%llu; page_size_cap=%llu; max_msg_size=%llu\n",
           p_hca->ib_caps.max_mr,
           (unsigned long long) p_hca->ib_caps.max_mr_size,
           (unsigned long long) p_hca->ib_caps.page_size_cap,
           (unsigned long long) p_hca->ib_caps.max_msg_size);
#endif

  /* allocate Protection Domain */
  p_hca->pd = ibv_alloc_pd(p_hca->context);
  if(p_hca->pd == NULL)
    {
      NM_FATAL("cannot allocate IB protection domain (%s).\n", strerror(errno));
    }
  p_hca->device = padico_strdup(device);
  p_hca->port = port;
  p_hca->async_events.srq_limit_reached = 0;
#ifdef NMAD_PADICOTM
  int flags = fcntl(p_hca->context->async_fd, F_GETFL);
  rc = fcntl(p_hca->context->async_fd, F_SETFL, flags | O_NONBLOCK);
  p_hca->async_events.async_io = padico_io_register(p_hca->context->async_fd, PADICO_IO_EVENT_READ, &nm_ibverbs_hca_async_handler, p_hca);
  padico_io_activate(p_hca->async_events.async_io);
#endif /* NMAD_PADICOTM */
  ibv_free_device_list(dev_list);

  p_hca->size_mem_reg = 0;
  p_hca->n_mem_reg = 0;
  /* add HCA context to the list in device */
  p_hca->p_reg_back_pressure = NULL;
  struct nm_ibverbs_device_s*p_ibverbs_device = nm_ibverbs_device_hashtable_lookup(&nm_ibverbs_common.device_table, p_hca->ib_dev);
  if(p_ibverbs_device == NULL)
    {
      p_ibverbs_device = padico_malloc(sizeof(struct nm_ibverbs_device_s));
      p_ibverbs_device->ib_dev = p_hca->ib_dev;
      nm_ibverbs_hca_vect_init(&p_ibverbs_device->contexts);
      nm_ibverbs_device_hashtable_insert(&nm_ibverbs_common.device_table, p_hca->ib_dev, p_ibverbs_device);
    }
  nm_ibverbs_hca_vect_push_back(&p_ibverbs_device->contexts, p_hca);
}

void nm_ibverbs_hca_close(struct nm_ibverbs_hca_s*p_hca)
{
  struct nm_ibverbs_device_s*p_ibverbs_device = nm_ibverbs_device_hashtable_lookup(&nm_ibverbs_common.device_table, p_hca->ib_dev);
  assert(p_ibverbs_device != NULL);
  nm_ibverbs_hca_vect_itor_t it = nm_ibverbs_hca_vect_find(&p_ibverbs_device->contexts, p_hca);
  nm_ibverbs_hca_vect_erase(&p_ibverbs_device->contexts, it);
  if(nm_ibverbs_hca_vect_empty(&p_ibverbs_device->contexts))
    {
      nm_ibverbs_device_hashtable_remove(&nm_ibverbs_common.device_table, p_hca->ib_dev);
      nm_ibverbs_hca_vect_destroy(&p_ibverbs_device->contexts);
      padico_free(p_ibverbs_device);
    }
  padico_free((void*)p_hca->device);
  ibv_dealloc_pd(p_hca->pd);
  ibv_close_device(p_hca->context);
}

void nm_ibverbs_context_get_profile(struct nm_ibverbs_context_s*p_ibverbs_context, struct nm_drv_profile_s*p_profile)
{
  /* driver profile encoding */
#ifdef NMAD_HWLOC
  static hwloc_topology_t __topology = NULL;
  if(__topology == NULL)
    {
      hwloc_topology_init(&__topology);
      hwloc_topology_load(__topology);
    }
  p_profile->cpuset = hwloc_bitmap_alloc();
  int rc = hwloc_ibv_get_device_cpuset(__topology, p_ibverbs_context->hca.ib_dev, p_profile->cpuset);
  if(rc)
    {
      NM_WARN("error while detecting ibv device location.\n");
      hwloc_bitmap_copy(p_profile->cpuset, hwloc_topology_get_complete_cpuset(__topology));
    }
#endif /* NMAD_HWLOC */
  p_profile->latency = 4000; /* 4 usec.; 0-byte latency is 1-2 usec., but limited to inline data */
  p_profile->bandwidth = 1024 * (p_ibverbs_context->hca.ib_caps.data_rate / 8) * 0.75; /* empirical estimation of software+protocol overhead */
}

/** create and connect a connection, but does not synchronize it
 * The connection may not be ready immediately.
 */
struct nm_ibverbs_cnx_s*nm_ibverbs_cnx_vector_connect(struct nm_ibverbs_context_s*p_ibverbs_context,
                                                      const void*p_remote_url, size_t url_size)
{
  if(p_ibverbs_context->vector_connect.pp_cached_cnxs == NULL)
    {
      NM_FATAL("vector connect has not been initialized.\n");
    }
  struct nm_ibverbs_addr_s ud_addr;
  memcpy(&ud_addr, p_remote_url, sizeof(struct nm_ibverbs_addr_s));
  uint32_t remote_rank;
  memcpy(&remote_rank, p_remote_url + sizeof(struct nm_ibverbs_addr_s), sizeof(uint32_t));
  struct nm_ibverbs_cnx_s*p_cnx = p_ibverbs_context->vector_connect.pp_cached_cnxs[remote_rank];
  p_ibverbs_context->vector_connect.pp_cached_cnxs[remote_rank] = NULL;
  if(p_cnx == NULL)
    {
      NM_FATAL("remote already connected; inconsistency detected in vector connect.\n");
    }
  memcpy(&p_cnx->remote_addr.addr.qpn,
         p_remote_url + sizeof(struct nm_ibverbs_addr_s) + sizeof(uint32_t) + p_ibverbs_context->vector_connect.rank * sizeof(uint32_t),
         sizeof(uint32_t));
  p_cnx->remote_addr.addr.psn = p_cnx->remote_addr.addr.qpn;
  p_cnx->remote_addr.addr.lid = ud_addr.lid;
  p_cnx->vector_connect.remote_rank = remote_rank;
  p_cnx->p_addr_entry = NULL;
  nm_ibverbs_cnx_qp_connect(p_cnx);
  NM_TRACEF("vector connect; connected from rank = %d; to rank = %d\n", p_ibverbs_context->vector_connect.rank, remote_rank);
  return p_cnx;
}

/** create QP and both CQs */
struct nm_ibverbs_cnx_s*nm_ibverbs_cnx_create(struct nm_ibverbs_context_s*p_ibverbs_context)
{
  const int use_srq = p_ibverbs_context->ib_opts.use_srq;
  struct nm_ibverbs_cnx_s*p_ibverbs_cnx = padico_malloc(sizeof(struct nm_ibverbs_cnx_s));
  memset(p_ibverbs_cnx, 0, sizeof(struct nm_ibverbs_cnx_s));
  p_ibverbs_cnx->p_ibverbs_context = p_ibverbs_context;
  p_ibverbs_cnx->p_addr_entry = NULL;
  if(use_srq)
    {
      p_ibverbs_cnx->if_cq = NULL;
    }
  else
    {
      /* init inbound CQ */
      p_ibverbs_cnx->if_cq = ibv_create_cq(p_ibverbs_context->hca.context, NM_IBVERBS_RX_DEPTH, NULL, NULL, 0);
      if(p_ibverbs_cnx->if_cq == NULL)
        {
          NM_FATAL("ibverbs: cannot create in CQ (%s).\n", strerror(errno));
        }
    }
  /* init outbound CQ */
  p_ibverbs_cnx->of_cq = ibv_create_cq(p_ibverbs_context->hca.context, NM_IBVERBS_TX_DEPTH, NULL, NULL, 0);
  if(p_ibverbs_cnx->of_cq == NULL)
    {
      NM_FATAL("ibverbs: cannot create out CQ (%s).\n", strerror(errno));
    }
  /* create QP */
  struct ibv_qp_init_attr qp_init_attr = {
    .send_cq = p_ibverbs_cnx->of_cq,
    .recv_cq = use_srq ? p_ibverbs_context->srq.srq_cq : p_ibverbs_cnx->if_cq,
    .srq     = use_srq ? p_ibverbs_context->srq.p_srq : NULL,
    .cap     = {
      .max_send_wr     = NM_IBVERBS_TX_DEPTH,
      .max_recv_wr     = NM_IBVERBS_RX_DEPTH,
      .max_send_sge    = NM_IBVERBS_MAX_SG_SQ,
      .max_recv_sge    = NM_IBVERBS_MAX_SG_RQ,
      .max_inline_data = NM_IBVERBS_MAX_INLINE
    },
    .qp_type = IBV_QPT_RC
  };
  p_ibverbs_cnx->qp = ibv_create_qp(p_ibverbs_context->hca.pd, &qp_init_attr);
  if(p_ibverbs_cnx->qp == NULL)
    {
      NM_FATAL("ibverbs: couldn't create QP (%s)\n", strerror(errno));
    }
  p_ibverbs_cnx->max_inline = qp_init_attr.cap.max_inline_data;
  p_ibverbs_cnx->local_addr.addr.lid = p_ibverbs_context->hca.lid;
  p_ibverbs_cnx->local_addr.addr.qpn = p_ibverbs_cnx->qp->qp_num;
  p_ibverbs_cnx->local_addr.addr.psn = p_ibverbs_cnx->qp->qp_num; /* use QPN as PSN for more compact addresses */ /* lrand48() & 0xffffff; */
  return p_ibverbs_cnx;
}


/* ** address exchange ************************************* */

static inline struct nm_ibverbs_cnx_addr_entry_s*
nm_ibverbs_cnx_addr_new(struct nm_ibverbs_context_s*p_ibverbs_context)
{
  struct nm_ibverbs_cnx_addr_entry_s*p_addr_entry = padico_malloc(sizeof(struct nm_ibverbs_cnx_addr_entry_s));
  memset(&p_addr_entry->msg, 0, sizeof(struct nm_ibverbs_cnx_addr_msg_s));
  p_addr_entry->msg.state = NM_IBVERBS_CNX_ADDR_NONE;
  p_addr_entry->msg.timeout = 0;
  p_addr_entry->local_state = NM_IBVERBS_CNX_ADDR_NONE;
  p_addr_entry->local_timeout = 0;
  p_addr_entry->p_cnx = NULL;
  nm_ibverbs_cnx_addr_entry_list_cell_init(p_addr_entry);
  return p_addr_entry;
}

static inline struct nm_ibverbs_cnx_addr_entry_s*
nm_ibverbs_cnx_addr_lookup(struct nm_ibverbs_context_s*p_ibverbs_context, const struct nm_ibverbs_addr_s*p_remote_addr)
{
  struct nm_ibverbs_cnx_addr_entry_s*p_remote_entry =
        nm_ibverbs_cnx_addr_entry_hashtable_lookup(&p_ibverbs_context->connect.addr_hash, p_remote_addr);
  return p_remote_entry;
}

/** register new cnx and ud remote addr in addr hashtable */
static void nm_ibverbs_cnx_addr_register(struct nm_ibverbs_cnx_s*p_ibverbs_cnx, const struct nm_ibverbs_addr_s*p_remote_addr)
{
  struct nm_ibverbs_context_s*p_ibverbs_context = p_ibverbs_cnx->p_ibverbs_context;
  struct nm_ibverbs_cnx_addr_entry_s*p_new_entry = nm_ibverbs_cnx_addr_lookup(p_ibverbs_context, p_remote_addr);
  if(p_new_entry == NULL)
    {
      p_new_entry = nm_ibverbs_cnx_addr_new(p_ibverbs_context);
      p_new_entry->msg.addr = *p_remote_addr;
      nm_ibverbs_cnx_addr_entry_hashtable_insert(&p_ibverbs_context->connect.addr_hash, &p_new_entry->msg.addr, p_new_entry);
    }
  p_new_entry->p_cnx = p_ibverbs_cnx;
  p_ibverbs_cnx->p_addr_entry = p_new_entry;
}

static inline void nm_ibverbs_cnx_addr_send(struct nm_ibverbs_cnx_s*p_ibverbs_cnx, const struct nm_ibverbs_addr_s*p_remote_addr)
{
  struct nm_ibverbs_context_s*p_ibverbs_context = p_ibverbs_cnx->p_ibverbs_context;
  struct nm_ibverbs_cnx_addr_entry_s*p_addr_entry = p_ibverbs_cnx->p_addr_entry;
  struct nm_ibverbs_cnx_connect_packet_s send_packet;
  send_packet.addr_msg.addr     = p_ibverbs_context->connect.udep.local_addr;
  send_packet.addr_msg.cnx_addr = p_ibverbs_cnx->local_addr;
  send_packet.addr_msg.state    = p_addr_entry->local_state;
  send_packet.addr_msg.timeout  = p_addr_entry->local_timeout;
  nm_ibverbs_ud_send(&p_ibverbs_context->connect.udep, &send_packet, sizeof(struct nm_ibverbs_cnx_addr_msg_s), p_remote_addr);
}

static void nm_ibverbs_cnx_addr_state_transition(struct nm_ibverbs_cnx_s*p_ibverbs_cnx, enum nm_ibverbs_cnx_addr_state_e state)
{
  struct nm_ibverbs_cnx_addr_entry_s*p_addr_entry = p_ibverbs_cnx->p_addr_entry;
  assert(p_addr_entry != NULL);
  p_addr_entry->local_state = state;
  p_addr_entry->local_timeout = 0;
  nm_ibverbs_cnx_addr_send(p_ibverbs_cnx, &p_addr_entry->msg.addr);
  p_addr_entry->t_timeout = puk_timing_stamp() + NM_IBVERBS_TIMEOUT_CONNECT;
}

/** create a new cnx with addr entry and start address exchange */
static inline struct nm_ibverbs_cnx_s*
nm_ibverbs_cnx_start_addr(struct nm_ibverbs_context_s*p_ibverbs_context, const struct nm_ibverbs_addr_s*p_remote_addr)
{
  nm_spin_assert_locked(&p_ibverbs_context->connect.lock);
  struct nm_ibverbs_cnx_s*p_cnx = p_cnx = nm_ibverbs_cnx_create(p_ibverbs_context);
  /* register cnx */
  nm_ibverbs_cnx_addr_register(p_cnx, p_remote_addr);

  /* send local address */
  nm_ibverbs_cnx_addr_state_transition(p_cnx, NM_IBVERBS_CNX_ADDR_OK);

  nm_ibverbs_cnx_addr_entry_list_push_back(&p_ibverbs_context->connect.pending_entries, p_cnx->p_addr_entry);
  return p_cnx;
}

static void nm_ibverbs_cnx_addr_poll(struct nm_ibverbs_context_s*p_ibverbs_context)
{
  int rc = 0;
  do
    {
      rc = nm_ibverbs_ud_recv_pool_poll(&p_ibverbs_context->connect.recv_pool);
      if(rc >= 0)
        {
          nm_spin_lock(&p_ibverbs_context->connect.lock);
          assert(rc < NM_IBVERBS_RX_DEPTH);
          struct nm_ibverbs_cnx_addr_msg_s*p_remote_msg = &p_ibverbs_context->connect.recv_packets[rc].addr_msg;
          /* insert received packet into addr hashtable */
          struct nm_ibverbs_cnx_addr_entry_s*p_addr_entry =
            nm_ibverbs_cnx_addr_lookup(p_ibverbs_context, &p_remote_msg->addr);
          if(p_addr_entry == NULL)
            {
              /* create entry from message */
              p_addr_entry = nm_ibverbs_cnx_addr_new(p_ibverbs_context);
              p_addr_entry->msg = *p_remote_msg;
              nm_ibverbs_cnx_addr_entry_hashtable_insert(&p_ibverbs_context->connect.addr_hash, &p_addr_entry->msg.addr, p_addr_entry);
              if(p_ibverbs_context->ib_opts.lazy_connect)
                {
                  /* in lazy mode, initiate reverse cnx */
                  p_addr_entry->p_cnx = nm_ibverbs_cnx_start_addr(p_ibverbs_context, &p_addr_entry->msg.addr);
                }
            }
          else
            {
              /* update entry with message */
              p_addr_entry->msg = *p_remote_msg;
            }
          if(p_remote_msg->timeout)
            {
              /* send again in case of timeout */
              if((p_addr_entry != NULL) && (p_addr_entry->p_cnx != NULL))
                {
                  p_addr_entry->local_timeout = 0; /* avoid timeout ping-pong */
                  nm_ibverbs_cnx_addr_send(p_addr_entry->p_cnx, &p_addr_entry->msg.addr);
                }
            }
          if( p_ibverbs_context->ib_opts.lazy_connect &&
              (p_remote_msg->state >= NM_IBVERBS_CNX_ADDR_OK) &&  /* we got remote address */
              (p_addr_entry->local_state == NM_IBVERBS_CNX_ADDR_OK) ) /* local state still not CONNECTED */
            {
              /* got remote address- start QP connect sequence */
              assert(p_addr_entry->p_cnx != NULL);
              p_addr_entry->p_cnx->remote_addr = p_addr_entry->p_cnx->p_addr_entry->msg.cnx_addr;
              nm_ibverbs_cnx_qp_connect(p_addr_entry->p_cnx);
              nm_ibverbs_cnx_qpn_hashtable_insert(&p_ibverbs_context->connect.cnx_by_qpn,
                                                  &p_addr_entry->p_cnx->local_addr.addr.qpn, p_addr_entry->p_cnx);
              /* switch to state CONNECTED */
              nm_ibverbs_cnx_addr_state_transition(p_addr_entry->p_cnx, NM_IBVERBS_CNX_ADDR_CONNECTED);
            }
          nm_spin_unlock(&p_ibverbs_context->connect.lock);
          /* rearm receive */
          nm_ibverbs_ud_recv_pool_reload(&p_ibverbs_context->connect.recv_pool, rc);
        }
    }
  while(rc >= 0);

  /* manage timeouts */
  if(!nm_ibverbs_cnx_addr_entry_list_empty(&p_ibverbs_context->connect.pending_entries))
    {
      double now = puk_timing_stamp();
      nm_spin_lock(&p_ibverbs_context->connect.lock);
      nm_ibverbs_cnx_addr_entry_itor_t i, tmp;
      puk_list_foreach_safe(nm_ibverbs_cnx_addr_entry, i, tmp, &p_ibverbs_context->connect.pending_entries)
        {
          if(i->msg.state == NM_IBVERBS_CNX_ADDR_CONNECTED)
            {
              /* purge connected cnx from pending list */
              nm_ibverbs_cnx_addr_entry_list_remove(&p_ibverbs_context->connect.pending_entries, i);
              continue;
            }
          if(now > i->t_timeout)
            {
              NM_WARN("TIMEOUT expired p_addr_entry = %p; now = %f; timeout = %f\n",
                      i, now, i->t_timeout);
              i->local_timeout = 1;
              nm_ibverbs_cnx_addr_send(i->p_cnx, &i->msg.addr);
              i->local_timeout = 0;
              i->t_timeout = puk_timing_stamp() + NM_IBVERBS_TIMEOUT_CONNECT;
            }
        }
      nm_spin_unlock(&p_ibverbs_context->connect.lock);
    }
}

static void nm_ibverbs_cnx_addr_wait(struct nm_ibverbs_cnx_s*p_ibverbs_cnx,
                                     const struct nm_ibverbs_addr_s*p_remote_addr,
                                     enum nm_ibverbs_cnx_addr_state_e min_state)
{
  struct nm_ibverbs_context_s*p_ibverbs_context = p_ibverbs_cnx->p_ibverbs_context;
  struct nm_ibverbs_cnx_addr_entry_s*p_remote_entry = NULL;
  puk_tick_t t1, t2;
  PUK_GET_TICK(t1);
  puk_tick_t t_warn_origin = t1;
  double cur_timeout = NM_IBVERBS_TIMEOUT_CONNECT;
  do
    {
      nm_ibverbs_cnx_addr_poll(p_ibverbs_context);
      PUK_GET_TICK(t2);
      const double usec = PUK_TIMING_DELAY(t1, t2);
      if(usec > cur_timeout)
        {
          p_ibverbs_cnx->p_addr_entry->local_timeout = 1;
          nm_ibverbs_cnx_addr_send(p_ibverbs_cnx, p_remote_addr);
          if(PUK_TIMING_DELAY(t_warn_origin, t2) > NM_IBVERBS_TIMEOUT_WARN)
            {
              NM_WARN("timeout for address receive. Retrying...\n");
              t_warn_origin = t2;
            }
          PUK_GET_TICK(t1);
          if(cur_timeout < NM_IBVERBS_TIMEOUT_WARN)
            cur_timeout *= 2;
        }
      p_remote_entry = nm_ibverbs_cnx_addr_lookup(p_ibverbs_context, p_remote_addr);
    }
  while((!p_remote_entry) || (p_remote_entry->msg.state < min_state));
  assert(p_ibverbs_cnx->p_addr_entry != NULL);
}

static void nm_ibverbs_cnx_ready_wait(struct nm_ibverbs_cnx_s*p_ibverbs_cnx, const struct nm_ibverbs_addr_s*p_remote_addr)
{
  struct nm_ibverbs_context_s*p_ibverbs_context = p_ibverbs_cnx->p_ibverbs_context;
  struct nm_ibverbs_cnx_addr_entry_s*p_remote_entry = nm_ibverbs_cnx_addr_lookup(p_ibverbs_context, p_remote_addr);
  /* switch to state CONNECTED */
  p_ibverbs_cnx->p_addr_entry->local_state = NM_IBVERBS_CNX_ADDR_CONNECTED;
  nm_ibverbs_cnx_addr_send(p_ibverbs_cnx, p_remote_addr);

  /* wait for remote CONNECTED */
  puk_tick_t t0, t1, t2;
  PUK_GET_TICK(t0);
  PUK_GET_TICK(t1);
  do
    {
      nm_ibverbs_cnx_addr_poll(p_ibverbs_context);
      PUK_GET_TICK(t2);
      const double usec = PUK_TIMING_DELAY(t1, t2);
      if(usec > NM_IBVERBS_TIMEOUT_CONNECT)
        {
          /* send again in case DONE msg was lost */
          nm_ibverbs_cnx_addr_send(p_ibverbs_cnx, p_remote_addr);
          NM_WARN("timeout for ready notification. Retrying...\n");
          PUK_GET_TICK(t1);
        }
      const double usec0 = PUK_TIMING_DELAY(t0, t2);
      if(usec0 > NM_IBVERBS_TIMEOUT_READY)
        {
          NM_WARN("timeout for ready notification. Giving up.\n");
          break;
        }
    }
  while(p_remote_entry->msg.state != NM_IBVERBS_CNX_ADDR_CONNECTED);
}

void nm_ibverbs_cnx_connect2(struct nm_ibverbs_cnx_s*p_ibverbs_cnx, const void*p_url, size_t urlsize)
{
  nm_ibverbs_cnx_connect_async(p_ibverbs_cnx, p_url, urlsize);
  nm_ibverbs_cnx_connect_wait(p_ibverbs_cnx);
}

void nm_ibverbs_cnx_connect_async(struct nm_ibverbs_cnx_s*p_ibverbs_cnx, const void*p_url, size_t urlsize)
{
  struct nm_ibverbs_context_s*p_ibverbs_context = p_ibverbs_cnx->p_ibverbs_context;
  const struct nm_ibverbs_addr_s*p_remote_addr = p_url;
  assert(urlsize == sizeof(struct nm_ibverbs_addr_s));

  /* register cnx */
  nm_ibverbs_cnx_addr_register(p_ibverbs_cnx, p_remote_addr);

  /* send local address */
  nm_ibverbs_cnx_addr_state_transition(p_ibverbs_cnx, NM_IBVERBS_CNX_ADDR_OK);

  /* drain pending packets */
  nm_ibverbs_cnx_addr_poll(p_ibverbs_context);
}

void nm_ibverbs_cnx_connect_wait(struct nm_ibverbs_cnx_s*p_ibverbs_cnx)
{
  const struct nm_ibverbs_addr_s*p_remote_addr = &p_ibverbs_cnx->p_addr_entry->msg.addr;

  /* wait peer address */
  nm_ibverbs_cnx_addr_wait(p_ibverbs_cnx, p_remote_addr, NM_IBVERBS_CNX_ADDR_OK);
  assert(p_ibverbs_cnx->p_addr_entry != NULL);
  p_ibverbs_cnx->remote_addr = p_ibverbs_cnx->p_addr_entry->msg.cnx_addr;

  /* QP connect sequence */
  nm_ibverbs_cnx_qp_connect(p_ibverbs_cnx);

  /* wait for the connection to be up and ready */
  nm_ibverbs_cnx_ready_wait(p_ibverbs_cnx, p_remote_addr);
}

/** QP connect sequence */
static void nm_ibverbs_cnx_qp_connect(struct nm_ibverbs_cnx_s*p_ibverbs_cnx)
{
  nm_ibverbs_cnx_qp_reset(p_ibverbs_cnx);
  nm_ibverbs_cnx_qp_init(p_ibverbs_cnx);
  nm_ibverbs_cnx_qp_rtr(p_ibverbs_cnx);
  nm_ibverbs_cnx_qp_rts(p_ibverbs_cnx);
}

void nm_ibverbs_cnx_close(struct nm_ibverbs_cnx_s*p_ibverbs_cnx)
{
  ibv_destroy_qp(p_ibverbs_cnx->qp);
  if(p_ibverbs_cnx->if_cq != NULL)
    {
      ibv_destroy_cq(p_ibverbs_cnx->if_cq);
    }
  ibv_destroy_cq(p_ibverbs_cnx->of_cq);
  padico_free(p_ibverbs_cnx);
}


/* ** lazy connection ************************************** */

int nm_ibverbs_cnx_lazy_enabled(void)
{
  return padico_module_attr_lazy_connect_getvalue();
}

void nm_ibverbs_cnx_lazy_create(struct nm_ibverbs_context_s*p_ibverbs_context,
                                struct nm_ibverbs_cnx_s**pp_ibverbs_cnx,
                                const void*p_remote_url, size_t url_size)
{
  nm_spin_lock(&p_ibverbs_context->connect.lock);
  if(*pp_ibverbs_cnx == NULL)
    {
      struct nm_ibverbs_cnx_addr_entry_s*p_addr_entry = nm_ibverbs_cnx_addr_lookup(p_ibverbs_context, p_remote_url);
      struct nm_ibverbs_cnx_s*p_cnx = p_addr_entry ? p_addr_entry->p_cnx : NULL;
      if(p_cnx == NULL)
        {
          const struct nm_ibverbs_addr_s*p_remote_addr = p_remote_url;
          assert(url_size == sizeof(struct nm_ibverbs_addr_s));
          p_cnx = nm_ibverbs_cnx_start_addr(p_ibverbs_context, p_remote_addr);
        }
      *pp_ibverbs_cnx = p_cnx;
    }
  nm_spin_unlock(&p_ibverbs_context->connect.lock);
  nm_ibverbs_cnx_addr_poll(p_ibverbs_context);
}

void nm_ibverbs_cnx_lazy_progress(struct nm_ibverbs_context_s*p_ibverbs_context)
{
  nm_ibverbs_cnx_addr_poll(p_ibverbs_context);
}

static void nm_ibverbs_reg_back_pressure(const struct nm_ibverbs_context_s*p_ibverbs_context, nm_len_t size)
{
  struct nm_ibverbs_device_s*p_ibverbs_device = nm_ibverbs_device_hashtable_lookup(&nm_ibverbs_common.device_table, p_ibverbs_context->hca.ib_dev);
  assert(p_ibverbs_device != NULL);
  nm_ibverbs_hca_vect_itor_t i;
  puk_vect_foreach(i, nm_ibverbs_hca, &p_ibverbs_device->contexts)
    {
      struct nm_ibverbs_hca_s*p_hca = *i;
      if(p_hca->p_reg_back_pressure != NULL)
        {
          (*p_hca->p_reg_back_pressure)(size);
          break;
        }
    }
}


/** guess how much locked memory the given chunk of memory will be accounted for */
static inline nm_len_t nm_ibverbs_locked_mem_size(const void*p_ptr, nm_len_t len)
{
  const nm_len_t page_size = 4096; /* assume 4kB pages */
  const uintptr_t base = (uintptr_t)p_ptr;
  uintptr_t page_base = base - (base % page_size);
  assert(page_base % page_size == 0);
  uintptr_t page_end = base + len;
  if(page_end % page_size != 0)
    {
      page_end += (page_size - (page_end % page_size));
    }
  assert(page_end % page_size == 0);
  assert(page_end > page_base);
  assert((page_end - page_base) <= (len + 2 * page_size));
  return page_end - page_base;
}

struct ibv_mr*nm_ibverbs_reg_mr(struct nm_ibverbs_context_s*p_ibverbs_context, void*buf, size_t size,
                                struct nm_ibverbs_segment_s*p_segment)
{
 retry:
  if (p_ibverbs_context->hca.ib_caps.max_mr < p_ibverbs_context->hca.n_mem_reg + 1)
    {
      nm_ibverbs_reg_back_pressure(p_ibverbs_context, 1);
    }
  else if( (nm_ibverbs_common.memlock != RLIM_INFINITY) &&
           (nm_ibverbs_common.memlock < nm_ibverbs_common.total_mem_reg + size) )
    {
      if(nm_ibverbs_common.memlock < nm_ibverbs_common.total_mem_reg)
        {
          NM_WARN("memlock = %lu; mem_reg = %lu\n", nm_ibverbs_common.memlock, nm_ibverbs_common.total_mem_reg);
          nm_ibverbs_reg_back_pressure(p_ibverbs_context, size);
        }
      else if(size > (nm_ibverbs_common.memlock - nm_ibverbs_common.total_mem_reg))
        {
          nm_ibverbs_reg_back_pressure(p_ibverbs_context, size - (nm_ibverbs_common.memlock - nm_ibverbs_common.total_mem_reg));
        }
      else
        {
          nm_ibverbs_reg_back_pressure(p_ibverbs_context, size);
        }
    }
  struct ibv_mr*p_mr = ibv_reg_mr(p_ibverbs_context->hca.pd, buf, size,
                                  IBV_ACCESS_REMOTE_WRITE | IBV_ACCESS_REMOTE_READ | IBV_ACCESS_LOCAL_WRITE);
  if(p_mr == NULL)
    {
      const int err = errno;
      NM_WARN("cannot register MR (errno = %d; %s).\n", err, strerror(err));
      nm_ibverbs_reg_back_pressure(p_ibverbs_context, size);
      goto retry;
      return NULL;
    }
  assert(p_mr->length == size);
  __sync_fetch_and_add(&p_ibverbs_context->hca.n_mem_reg, 1);
  __sync_fetch_and_add(&p_ibverbs_context->hca.size_mem_reg, size);
  const nm_len_t locked_size = nm_ibverbs_locked_mem_size(buf, size);
  __sync_fetch_and_add(&nm_ibverbs_common.total_mem_reg, locked_size);
  if(p_segment)
    {
      p_segment->raddr = (uintptr_t)buf;
      p_segment->rkey = p_mr->rkey;
    }
  return p_mr;
}

int nm_ibverbs_dereg_mr(struct nm_ibverbs_context_s*p_ibverbs_context, struct ibv_mr*p_mr)
{
  __sync_fetch_and_sub(&p_ibverbs_context->hca.n_mem_reg, 1);
  __sync_fetch_and_sub(&p_ibverbs_context->hca.size_mem_reg, p_mr->length);
  const nm_len_t locked_size = nm_ibverbs_locked_mem_size(p_mr->addr, p_mr->length);
  __sync_fetch_and_sub(&nm_ibverbs_common.total_mem_reg, locked_size);
  int rc = ibv_dereg_mr(p_mr);
  if(rc != 0)
    {
      NM_WARN("error %d (%s) while deregistering memory.\n", rc, strerror(rc));
    }
  return rc;
}


/* ********************************************************* */
/* ** state transitions for QP finite-state automaton */

/** modify QP to state RESET */
static void nm_ibverbs_cnx_qp_reset(struct nm_ibverbs_cnx_s*p_ibverbs_cnx)
{
  /* modify QP- step: RTS */
  struct ibv_qp_attr attr =
    {
      .qp_state = IBV_QPS_RESET
    };
  int rc = ibv_modify_qp(p_ibverbs_cnx->qp, &attr, IBV_QP_STATE);
  if(rc != 0)
    {
      NM_FATAL("ibverbs: failed to modify QP to RESET (rc=%d; %s).\n", rc, strerror(rc));
    }
}

/** modify QP to state INIT */
static void nm_ibverbs_cnx_qp_init(struct nm_ibverbs_cnx_s*p_ibverbs_cnx)
{
  struct ibv_qp_attr attr =
    {
      .qp_state        = IBV_QPS_INIT,
      .pkey_index      = 0,
      .port_num        = p_ibverbs_cnx->p_ibverbs_context->hca.port,
      .qp_access_flags = IBV_ACCESS_REMOTE_WRITE | IBV_ACCESS_REMOTE_READ
    };
  int rc = ibv_modify_qp(p_ibverbs_cnx->qp, &attr,
                         IBV_QP_STATE              |
                         IBV_QP_PKEY_INDEX         |
                         IBV_QP_PORT               |
                         IBV_QP_ACCESS_FLAGS);
  if(rc != 0)
    {
      NM_FATAL("ibverbs: failed to modify QP to INIT (rc=%d; %s).\n", rc, strerror(rc));
    }
}

/** modify QP to state RTR */
static void nm_ibverbs_cnx_qp_rtr(struct nm_ibverbs_cnx_s*p_ibverbs_cnx)
{
  struct ibv_qp_attr attr =
    {
      .qp_state           = IBV_QPS_RTR,
      .path_mtu           = p_ibverbs_cnx->p_ibverbs_context->ib_opts.mtu,
      .dest_qp_num        = p_ibverbs_cnx->remote_addr.addr.qpn,
      .rq_psn             = p_ibverbs_cnx->remote_addr.addr.psn,
      .max_dest_rd_atomic = NM_IBVERBS_ATOMIC_DEPTH,
      .min_rnr_timer      = 12, /* 12 */
      .ah_attr            = {
        .is_global        = 0,
        .dlid             = p_ibverbs_cnx->remote_addr.addr.lid,
        .sl               = 0,
        .src_path_bits    = 0,
        .port_num         =  p_ibverbs_cnx->p_ibverbs_context->hca.port
      }
    };
  int rc = ibv_modify_qp(p_ibverbs_cnx->qp, &attr,
                         IBV_QP_STATE              |
                         IBV_QP_AV                 |
                         IBV_QP_PATH_MTU           |
                         IBV_QP_DEST_QPN           |
                         IBV_QP_RQ_PSN             |
                         IBV_QP_MAX_DEST_RD_ATOMIC |
                         IBV_QP_MIN_RNR_TIMER);
  if(rc != 0)
    {
      NM_FATAL("ibverbs: dev = %s; port = %d; failed to modify QP to RTR (%s)\n",
               ibv_get_device_name(p_ibverbs_cnx->p_ibverbs_context->hca.ib_dev),
               p_ibverbs_cnx->p_ibverbs_context->hca.port, strerror(rc));
    }
}

/** modify QP to state RTS */
static void nm_ibverbs_cnx_qp_rts(struct nm_ibverbs_cnx_s*p_ibverbs_cnx)
{
  struct ibv_qp_attr attr =
    {
      .qp_state      = IBV_QPS_RTS,
      .timeout       = 2, /* 14 */
      .retry_cnt     = 7,  /* 7 */
      .rnr_retry     = 7,  /* 7 = unlimited */
      .sq_psn        = p_ibverbs_cnx->local_addr.addr.psn,
      .max_rd_atomic = NM_IBVERBS_ATOMIC_DEPTH /* 1 */
    };
  int rc = ibv_modify_qp(p_ibverbs_cnx->qp, &attr,
                         IBV_QP_STATE              |
                         IBV_QP_TIMEOUT            |
                         IBV_QP_RETRY_CNT          |
                         IBV_QP_RNR_RETRY          |
                         IBV_QP_SQ_PSN             |
                         IBV_QP_MAX_QP_RD_ATOMIC);
  if(rc != 0)
    {
      NM_FATAL("ibverbs: failed to modify QP to RTS (rc=%d; %s)\n", rc, strerror(rc));
    }
}

/* ** RDMA ************************************************* */

int nm_ibverbs_do_rdma(struct nm_ibverbs_cnx_s*__restrict__ p_ibverbs_cnx,
                       const void*__restrict__ buf, int size, uint64_t raddr,
                       int opcode, int flags, uint32_t lkey, uint32_t rkey, uint64_t wrid)
{
  struct ibv_sge list = {
    .addr   = (uintptr_t)buf,
    .length = size,
    .lkey   = lkey
  };
  struct ibv_send_wr wr = {
    .wr_id      = wrid,
    .sg_list    = &list,
    .num_sge    = 1,
    .opcode     = opcode,
    .send_flags = flags,
    .next       = NULL,
    .imm_data   = 0,
    .wr.rdma =
    {
      .remote_addr = raddr,
      .rkey        = rkey
    }
  };
  struct ibv_send_wr*bad_wr = NULL;
  int rc = ibv_post_send(p_ibverbs_cnx->qp, &wr, &bad_wr);
  assert(wrid < _NM_IBVERBS_WRID_MAX);
  p_ibverbs_cnx->pending.wrids[wrid]++;
  p_ibverbs_cnx->pending.total++;
  if(rc)
    {
      NM_WARN("post RDMA write failed (rc=%d; %s).\n", rc, strerror(rc));
      return -NM_EBROKEN;
    }
  return NM_ESUCCESS;
}

int nm_ibverbs_rdma_send(struct nm_ibverbs_cnx_s*p_ibverbs_cnx, int size,
                         const void*__restrict__ ptr,
                         const void*__restrict__ _raddr,
                         const void*__restrict__ _lbase,
                         const struct nm_ibverbs_segment_s*p_seg,
                         const struct ibv_mr*__restrict__ mr,
                         int wrid)
{
  const uintptr_t _rbase = p_seg->raddr;
  const uint64_t raddr   = (uint64_t)(((uintptr_t)_raddr - (uintptr_t)_lbase) + _rbase);
  struct ibv_sge list = {
    .addr   = (uintptr_t)ptr,
    .length = size,
    .lkey   = mr->lkey
  };
  struct ibv_send_wr wr = {
    .wr_id      = wrid,
    .sg_list    = &list,
    .num_sge    = 1,
    .opcode     = IBV_WR_RDMA_WRITE,
    .send_flags = (size < p_ibverbs_cnx->max_inline) ? (IBV_SEND_INLINE | IBV_SEND_SIGNALED) : IBV_SEND_SIGNALED,
    .wr.rdma =
    {
      .remote_addr = raddr,
      .rkey        = p_seg->rkey
    }
  };
  struct ibv_send_wr*bad_wr = NULL;
  int rc = ibv_post_send(p_ibverbs_cnx->qp, &wr, &bad_wr);
  assert(wrid < _NM_IBVERBS_WRID_MAX);
  p_ibverbs_cnx->pending.wrids[wrid]++;
  p_ibverbs_cnx->pending.total++;
  if(rc)
    {
      NM_WARN("post RDMA send failed (rc=%d; %s).\n", rc, strerror(rc));
      return -NM_EBROKEN;
    }
  return NM_ESUCCESS;
}

int nm_ibverbs_rdma_poll(struct nm_ibverbs_cnx_s*__restrict__ p_ibverbs_cnx)
{
  int ne = 0;
  if(p_ibverbs_cnx->pending.total > 0)
    {
      do
        {
          struct ibv_wc wc;
          ne = ibv_poll_cq(p_ibverbs_cnx->of_cq, 1, &wc);
          if(ne > 0 && wc.status == IBV_WC_SUCCESS)
            {
              assert(wc.wr_id < _NM_IBVERBS_WRID_MAX);
              p_ibverbs_cnx->pending.wrids[wc.wr_id]--;
              p_ibverbs_cnx->pending.total--;
            }
          else if(ne > 0)
            {
              padico_out(puk_verbose_critical, "WC send failed- status=%d (%s)\n",
                      wc.status, nm_ibverbs_status_strings[wc.status]);
              switch(wc.status)
                {
                case IBV_WC_LOC_LEN_ERR:
                case IBV_WC_LOC_QP_OP_ERR:
                case IBV_WC_LOC_EEC_OP_ERR:
                case IBV_WC_LOC_PROT_ERR:
                case IBV_WC_LOC_ACCESS_ERR:
                case IBV_WC_LOC_RDD_VIOL_ERR:
                case IBV_WC_FATAL_ERR:
                case IBV_WC_GENERAL_ERR:
                  /* abort on local & fatal errors */
                  NM_FATAL("ibverbs fatal error.\n");
                default:
                  /* gracefully return non-local & non-ftal errors */
                  return -NM_EBROKEN;
                }
            }
          else if(ne < 0)
            {
              NM_FATAL("WC polling failed.\n");
              return -NM_ESCFAILD;
            }
        }
      while(ne > 0);
    }
  return NM_ESUCCESS;
}


/* ** checksum ********************************************* */


/** checksum algorithm. Set NMAD_IBVERBS_CHECKSUM to non-null to enable checksums.
 */
uint32_t nm_ibverbs_checksum(const char*data, nm_len_t len)
{
  if(_nm_ibverbs_checksum)
    return (*_nm_ibverbs_checksum->func)(data, len);
  else
    return 0;
}

int nm_ibverbs_checksum_enabled(void)
{
  return _nm_ibverbs_checksum != NULL;
}

uint32_t nm_ibverbs_memcpy_and_checksum(void*_dest, const void*_src, nm_len_t len)
{
  if(_nm_ibverbs_checksum)
    {
      if(_nm_ibverbs_checksum->checksum_and_copy)
        {
          return (*_nm_ibverbs_checksum->checksum_and_copy)(_dest, _src, len);
        }
      else
        {
          memcpy(_dest, _src, len);
          return nm_ibverbs_checksum(_dest, len);
        }
    }
  else
    {
      memcpy(_dest, _src, len);
    }
  return 1;
}

uint32_t nm_ibverbs_copy_from_and_checksum(void*dest, nm_data_slicer_t*p_slicer, const void*src, nm_len_t offset, nm_len_t len)
{
  assert(!(nm_data_slicer_isnull(p_slicer) && (src == NULL)));
  assert(!(!nm_data_slicer_isnull(p_slicer) && (src != NULL)));
  if(_nm_ibverbs_checksum && !nm_data_slicer_isnull(p_slicer))
    {
      NM_FATAL("ibverbs: checksums not supported yet with nm_data.\n");
    }
  if(!nm_data_slicer_isnull(p_slicer))
    {
      nm_data_slicer_copy_from(p_slicer, dest, len);
      return 1;
    }
  else
    {
      return nm_ibverbs_memcpy_and_checksum(dest, src + offset, len);
    }
}

uint32_t nm_ibverbs_copy_to_and_checksum(const void*src, nm_data_slicer_t*p_slicer, void*dest, nm_len_t offset, nm_len_t len)
{
  assert(!(nm_data_slicer_isnull(p_slicer) && (dest == NULL)));
  assert(!((!nm_data_slicer_isnull(p_slicer)) && (dest != NULL)));
  if(_nm_ibverbs_checksum && !nm_data_slicer_isnull(p_slicer))
    {
      NM_FATAL("ibverbs: checksums not supported yet with nm_data.\n");
    }
  if(!nm_data_slicer_isnull(p_slicer))
    {
      nm_data_slicer_copy_to(p_slicer, src, len);
      return 1;
    }
  else
    {
      return nm_ibverbs_memcpy_and_checksum(dest + offset, src, len);
    }
}


/* ** reg pool ********************************************* */

void nm_ibverbs_regpool_init(struct nm_ibverbs_regpool_s*p_regpool, struct nm_ibverbs_context_s*p_ibverbs_context, nm_len_t entry_size, nm_len_t slice_size)
{
  nm_ibverbs_regpool_slice_lfstack_init(&p_regpool->slices);
  nm_ibverbs_regpool_block_lfstack_init(&p_regpool->blocks);
  p_regpool->entry_size = entry_size;
  p_regpool->num_entries = slice_size;
  p_regpool->p_ibverbs_context = p_ibverbs_context;
#ifdef NMAD_PROFILE
  puk_profile_var_defx(unsigned_long_long, counter, &p_regpool->total_alloc, 0,
                       "nm_ibverbs", "total number of allocated entries",
                       "nm_ibverbs.regpool-%p-s%d.total_alloc", p_regpool, entry_size);
  puk_profile_var_defx(unsigned_long, highwatermark, &p_regpool->max_alloc, 0,
                       "nm_ibverbs", "maximum number of simultaneous allocations",
                       "nm_ibverbs.regpool-%p-s%d.max_alloc", p_regpool, entry_size);
  p_regpool->cur_alloc = 0;
#endif /* NMAD_PROFILE */
}

void nm_ibverbs_regpool_destroy(struct nm_ibverbs_regpool_s*p_regpool)
{
  struct nm_ibverbs_regpool_slice_lfstack_cell_s*p_slice = NULL;
  do
    {
      p_slice = nm_ibverbs_regpool_slice_lfstack_pop(&p_regpool->slices);
      if(p_slice)
        {
          nm_ibverbs_dereg_mr(p_regpool->p_ibverbs_context, p_slice->p_mr);
          padico_free(p_slice->p_ptr);
          padico_free(p_slice->p_cells);
          padico_free(p_slice);
        }
    }
  while(p_slice != NULL);
  p_regpool->p_ibverbs_context = NULL;
}

const struct nm_ibverbs_regpool_entry_s*nm_ibverbs_regpool_alloc_entry(struct nm_ibverbs_regpool_s*p_regpool)
{
  struct nm_ibverbs_regpool_block_lfstack_cell_s*p_block = NULL;
 retry:
  p_block = nm_ibverbs_regpool_block_lfstack_pop(&p_regpool->blocks);
  if(p_block == NULL)
    {
      /* alloc new slice */
      struct nm_ibverbs_regpool_slice_lfstack_cell_s*p_slice = padico_malloc(sizeof(struct nm_ibverbs_regpool_slice_lfstack_cell_s));
      p_regpool->num_entries *= 2;
      p_slice->p_ptr = padico_malloc(p_regpool->entry_size * p_regpool->num_entries);
      p_slice->p_cells = padico_malloc(p_regpool->num_entries * sizeof(struct nm_ibverbs_regpool_block_lfstack_cell_s));
      p_slice->p_mr = nm_ibverbs_reg_mr(p_regpool->p_ibverbs_context, p_slice->p_ptr, p_regpool->entry_size * p_regpool->num_entries, NULL);
      if(p_slice->p_mr == NULL)
        {
          const int err = errno;
          NM_FATAL("cannot register MR (errno = %d; %s).\n", err, strerror(err));
        }
      int i;
      for(i = 0; i < p_regpool->num_entries; i++)
        {
          struct nm_ibverbs_regpool_block_lfstack_cell_s*p_block = &p_slice->p_cells[i];
          p_block->entry.p_ptr = p_slice->p_ptr + i * p_regpool->entry_size;
          p_block->entry.p_mr = p_slice->p_mr;
          nm_ibverbs_regpool_block_lfstack_push(&p_regpool->blocks, p_block);
        }
      nm_ibverbs_regpool_slice_lfstack_push(&p_regpool->slices, p_slice);
      goto retry;
    }
#ifdef NMAD_PROFILE
  __sync_fetch_and_add(&p_regpool->cur_alloc, 1);
  __sync_fetch_and_add(&p_regpool->total_alloc, 1);
  if(p_regpool->cur_alloc > p_regpool->max_alloc)
    {
      p_regpool->max_alloc = p_regpool->cur_alloc;
    }
#endif /* NMAD_PROFILE */
  return &p_block->entry;
}

void nm_ibverbs_regpool_free_entry(struct nm_ibverbs_regpool_s*p_regpool, const struct nm_ibverbs_regpool_entry_s*p_entry)
{
  struct nm_ibverbs_regpool_block_lfstack_cell_s*p_block =
    nm_container_of(p_entry, struct nm_ibverbs_regpool_block_lfstack_cell_s, entry);
  nm_ibverbs_regpool_block_lfstack_push(&p_regpool->blocks, p_block);
#ifdef NMAD_PROFILE
  __sync_fetch_and_add(&p_regpool->cur_alloc, -1);
#endif /* NMAD_PROFILE */
}

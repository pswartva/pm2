/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


#ifndef NM_MPI_GROUP_H
#define NM_MPI_GROUP_H

/** \addtogroup mpi_interface */
/* @{ */

/** @name Functions: Groups */
/* @{ */


/**
 * Returns the size of a group
 * @param group group
 * @param size number of processes in the group
 * @return MPI status
 */
int MPI_Group_size(MPI_Group group,
                   int*size);
/**
 * Returns the rank of this process in the given group
 * @param group group
 * @param rank rank of the calling process in group, or MPI_UNDEFINED if the process is not a member
 * @return MPI status
 */
int MPI_Group_rank(MPI_Group group,
                   int *rank);

/**
 * Produces a group by combining two groups
 * @param group1 first group
 * @param group2 second group
 * @param newgroup union group
 * @return MPI status
 */
int MPI_Group_union(MPI_Group group1,
                    MPI_Group group2,
                    MPI_Group*newgroup);

/**
 * Produces a group as the intersection of two existing groups
 * @param group1 first group
 * @param group2 second group
 * @param newgroup intersection group
 * @return MPI status
 */
int MPI_Group_intersection(MPI_Group group1,
                           MPI_Group group2,
                           MPI_Group*newgroup);

/**
 * Produces a group from the difference of two groups
 * @param group1 first group
 * @param group2 second group
 * @param newgroup difference group
 * @return MPI status
 */
int MPI_Group_difference(MPI_Group group1,
                         MPI_Group group2,
                         MPI_Group*newgroup);

/**
 * Compares two groups
 * @param group1 first group
 * @param group2 second group
 * @param result nteger which is MPI_IDENT if the order and members of the two groups are the same, MPI_SIMILAR if only the members are the same, and MPI_UNEQUAL otherwise
 * @return MPI status
 */
int MPI_Group_compare(MPI_Group group1,
                      MPI_Group group2,
                      int*result);

/**
 * Produces a group by reordering an existing group and taking only listed members
 * @param group group
 * @param n number of elements in array ranks
 * @param ranks ranks of processes in group to appear in newgroup
 * @param newgroup new group derived from above, in the order defined by ranks
 * @return MPI status
 */
int MPI_Group_incl(MPI_Group group,
                   int n,
                   const int*ranks,
                   MPI_Group*newgroup);

/**
 * Creates a new group from ranges of ranks in an existing group
 * @param group group
 * @param n number of triplets in array ranges
 * @param ranges a one-dimensional array of integer triplets, of the form (first rank, last rank, stride) indicating ranks in group or processes to be included in newgroup.
 * @param newgroup new group derived from above, in the order defined by ranges
 * @return MPI status
 */
int MPI_Group_range_incl(MPI_Group group,
                         int n,
                         int ranges[][3],
                         MPI_Group*newgroup);

/**
 * Produces a group by reordering an existing group and taking only unlisted members
 * @param group group
 * @param n number of elements in array ranks
 * @param ranks ranks of processes in group to appear in newgroup
 * @param newgroup new group derived from above, in the order defined by ranks
 * @return MPI status
 */
int MPI_Group_excl(MPI_Group group,
                   int n,
                   const int*ranks,
                   MPI_Group*newgroup);

/**
 * Produces a group by excluding ranges of processes from an existing group
 * @param group group
 * @param n number of triplets in array ranges
 * @param ranges a one-dimensional array of integer triplets of the form (first rank, last rank, stride), indicating the ranks in group of processes to be excluded from the output group newgroup
 * @param newgroup new group derived from above, preserving the order in group
 * @return MPI status
 */
int MPI_Group_range_excl(MPI_Group group,
                         int n,
                         int ranges[][3],
                         MPI_Group*newgroup);

/**
 * Frees a group
 * @param group group to free
 * @return MPI status
 */
int MPI_Group_free(MPI_Group*group);

/**
 * Maps the rank of a set of processes in group1 to their rank in
 * group2.
 * @param group1 group
 * @param n number of ranks in ranks1 and ranks2 arrays
 * @param ranks1 array of zero or more valid ranks in group1
 * @param group2 group
 * @param ranks2 array of corresponding ranks in group2
 * @return MPI status
 */
int MPI_Group_translate_ranks(MPI_Group group1,
                              int n,
                              const int *ranks1,
                              MPI_Group group2,
                              int *ranks2);


/* @}*/
/* @}*/

#endif /* NM_MPI_GROUP_H */

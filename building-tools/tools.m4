dnl -*- mode: Autoconf;-*-

# ## basic tools
AC_DEFUN([AC_PADICO_BASE_TOOLS],
         [
           dnl --- true
           AC_PATH_PROG(BIN_TRUE, true)
           AC_SUBST(BIN_TRUE)

           dnl --- bash
           AC_PATH_PROG([BASH], [bash])
           AC_SUBST([BASH])

           dnl --- ps
           AC_PATH_PROG(PS, ps)
           AC_SUBST([PS])
           if test "x$PS" = "x"; then
             AC_MSG_ERROR([ps command not found.])
           fi
        ])

AC_DEFUN([AC_PADICO_CHECK_GNUPLOT],
         [
           AC_PATH_PROG(GNUPLOT, gnuplot)
           AC_SUBST(GNUPLOT)
         ])

AC_DEFUN([AC_PADICO_CHECK_GRAPHICSMAGICK],
         [
           AC_PATH_PROG(GRAPHICSMAGICK, gm)
           AC_SUBST(GRAPHICSMAGICK)
         ])

AC_DEFUN([AC_PADICO_CHECK_XTERM],
         [
           dnl --- xterm
           if test "x" = "x$XTERM" ; then
             AC_PATH_PROGS(xterm_path, [ xterm x-terminal-emulator rxvt Eterm konsole ])
             XTERM="$xterm_path"
           else
             XTERM="$XTERM"
           fi
           AC_ARG_VAR(XTERM, [ The X Window terminal emulator ])
           if test "x$XTERM" = "x"; then
             AC_MSG_WARN([xterm not found.])
           else
             AC_DEFINE_UNQUOTED(XTERM, "${XTERM}", [ X Window terminal emulator ])
           fi
        ])

AC_DEFUN([AC_PADICO_CHECK_DIALOG],
         [
           dnl --- dialog
           AC_PATH_PROGS(DIALOG, [ ${DIALOG} whiptail dialog cdialog gdialog ])
           AC_ARG_VAR(DIALOG, [ The program used to display dialogs (e.g. whiptail) ])
         ])

AC_DEFUN([AC_PADICO_CHECK_GDB],
         [
           dnl --- gdb
           AC_PATH_PROGS(GDB, [ ${GDB} gdb ])
           AC_ARG_VAR(GDB, [ The gdb debugger ])
           AC_DEFINE_UNQUOTED(GDB, "${GDB}", [ The gdb debugger ])
           if test "x$GDB" = "x"; then
             AC_MSG_WARN([gdb not found.])
           fi
         ])

# ## check for GNU make
AC_DEFUN([AC_PADICO_GNU_MAKE],
         [ AC_CACHE_CHECK( for GNU make,_cv_gnu_make_command,
                _cv_gnu_make_command='' ;
dnl Search all the common names for GNU make
                for a in "$MAKE" make gmake gnumake gnu-make ; do
                        if test -z "$a" ; then continue ; fi ;
                        if  ( sh -c "$a --version" 2> /dev/null | grep GNU  2>&1 > /dev/null ) ;  then
                                _cv_gnu_make_command=$a ;
                                break;
                        fi
                done ;
        ) ;
dnl If there was a GNU version, then set @ifGNUmake@ to the empty string, '#' otherwise
        if test  "x$_cv_gnu_make_command" != "x"  ; then
                MAKE=$_cv_gnu_make_command
                ifGNUmake=''
        else
                ifGNUmake='#'
                AC_MSG_ERROR([Not found])
        fi
        AC_ARG_VAR(MAKE, [ path for GNU make ])
        AC_SUBST(ifGNUmake)
         ] )

# ##  C/C++ dependency checker
AC_DEFUN([AC_PADICO_CDEP],
         [ AC_REQUIRE([AC_PADICO_COMPILER])
           AC_MSG_CHECKING([how to check C dependency])
           CC_DEP="${CC} ${CDEPFLAGS}"
           AC_MSG_RESULT([${CC_DEP}])
           AC_SUBST(CC_DEP)
         ])

AC_DEFUN([AC_PADICO_CXXDEP],
        [ AC_REQUIRE([AC_PADICO_CXX])
           AC_MSG_CHECKING([how to check C++ dependency])
           CXX_DEP="${CXX} ${CXXDEPFLAGS}"
           AC_MSG_RESULT([${CXX_DEP}])
           AC_SUBST(CXX_DEP)
         ])

# ## static linker
AC_DEFUN([AC_PADICO_LINKER],
         [ AC_REQUIRE([AC_PADICO_COMPILER])
           AC_MSG_CHECKING([for static linker])
           LD="${CC}"
           AC_MSG_RESULT([${LD} ${LDFLAGS}])
           AC_SUBST(LD)
           AC_SUBST(LDFLAGS)
         ])

# ## dynamic linker
AC_DEFUN([AC_PADICO_DYNLD],
         [ AC_REQUIRE([AC_PADICO_COMPILER])
           AC_MSG_CHECKING([for dynamic library linker])
           DYNLIB_LD="${CC} ${LDFLAGS} ${CPICFLAGS} ${DYNLIB_FLAGS}"
           AC_MSG_RESULT([${DYNLIB_LD}])
           AC_SUBST(DYNLIB_LD)

           AC_MSG_CHECKING([for dynamic library prefix])
           AC_MSG_RESULT([${DYNLIB_PREFIX}])
           AC_SUBST(DYNLIB_PREFIX)
           AC_DEFINE_UNQUOTED(DYNLIB_PREFIX, "${DYNLIB_PREFIX}",
             [prefix prepended to dynamic library names (usually "lib")])

           AC_MSG_CHECKING([for dynamic library extension])
           AC_MSG_RESULT([${DYNLIB_EXT}])
           AC_SUBST(DYNLIB_EXT)
           AC_DEFINE_UNQUOTED(DYNLIB_EXT, "${DYNLIB_EXT}",
             [suffix for dynamic library names (usually ".so" on Unix systems)])
           HAVE_DYNLIB=yes
           AC_SUBST([HAVE_DYNLIB])
         ])

AC_DEFUN([AC_PADICO_EXPAT],
         [
           AC_REQUIRE([PKG_PROG_PKG_CONFIG])
           AC_ARG_VAR(EXPAT_ROOT, "Specify Expat location in the system (default: use pkg-config)")
           if test "x$EXPAT_ROOT" != "x"; then
               BASE_CFLAGS="$BASE_CFLAGS -I$EXPAT_ROOT/include"
               CPPFLAGS="$CPPFLAGS -I$EXPAT_ROOT/include"
               LDFLAGS="$LDFLAGS -L$EXPAT_ROOT/lib"
               LIBS="$LIBS -lexpat"
           else
             PKG_CHECK_MODULES([expat], [ expat ])
             BASE_CFLAGS="$BASE_CFLAGS $expat_CFLAGS"
             CPPFLAGS="$CPPFLAGS $expat_CFLAGS"
             LIBS="$LIBS $expat_LIBS"
             requires_expat=expat
             AC_SUBST([requires_expat])
           fi
           AC_CHECK_HEADERS(expat.h,
                            [ HAVE_EXPAT_H=yes ],
                            [ HAVE_EXPAT_H=no  ])
           if test $HAVE_EXPAT_H = "no"; then
               AC_MSG_ERROR([ Expat XML parser not found ])
           fi
         ])


dnl -- detect Puk as external tool
AC_DEFUN([AC_PADICO_PUK_ROOT],
         [
             AC_MSG_CHECKING([for Puk root])
             puk_root="`pkg-config --variable=prefix Puk`"
             AC_SUBST([puk_root])
             AC_MSG_RESULT([${puk_root}])
             if test ! -f ${puk_root}/etc/padico/common_rules.mk; then
               AC_MSG_ERROR([required ${puk_root}/etc/padico/common_rules.mk not found])
             fi
         ])

/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief PukABI memory management
 * @ingroup PukABI
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <assert.h>

#include "Puk-ABI.h"

#ifdef PUKABI_ENABLE_MEM

#undef PUK_MEM_STAT

static char*puk_mem_strdup(const char*s);

static puk_mem_invalidate_hook_t puk_mem_invalidate_hook = NULL;

void puk_abi_mem_set_hook(puk_mem_invalidate_hook_t hook)
{
  assert(puk_mem_invalidate_hook == NULL || hook == NULL);
  puk_mem_invalidate_hook = hook;
}

void puk_abi_mem_free_invalidate(void*ptr)
{
  size_t*chunk_hdr = ptr;
  if(ptr != NULL)
    {
      chunk_hdr--;
      /* find chunk size in heap chunk metadata */
      const size_t chunk_size = *chunk_hdr & (size_t)(~((size_t)0x07)); /* clear out status bits (lower 3 bits) */
      puk_abi_mem_invalidate(ptr, chunk_size);
    }
}

void puk_abi_mem_invalidate(void*ptr, int size)
{
  if(puk_mem_invalidate_hook)
    (*puk_mem_invalidate_hook)(ptr, size);
}

/* ********************************************************* */

void puk_abi_mem_init()
{
  puk_abi_set_virtual(strdup, &puk_mem_strdup);
}

/* ********************************************************* */

#define PUK_MEM_CACHE_SIZE 64

static struct
{
  struct puk_mem_reg_s cache[PUK_MEM_CACHE_SIZE];
  puk_mem_register_t   reg;
  puk_mem_unregister_t unreg;
#ifdef PUK_MEM_STAT
  int cache_hit, cache_miss;
#endif
} puk_mem =
  {
    .reg   = NULL,
    .unreg = NULL,
#ifdef PUK_MEM_STAT
    .cache_hit = 0,
    .cache_miss = 0,
#endif
    .cache = { { .refcount = 0, .ptr = NULL, .len = 0 } }
  };

#ifdef PUK_MEM_STAT
void puk_abi_mem_stat(void)
{
  const int total = puk_mem.cache_hit + puk_mem.cache_miss;
  const int ratio = (total > 0) ? ((100 * puk_mem.cache_hit) / total) : 0;
  if(total > 0)
    fprintf(stderr, "PukABI: mem cache hit = %d; miss = %d (%d %%)\n",
            puk_mem.cache_hit, puk_mem.cache_miss, ratio);
}
#endif


static void puk_mem_cache_flush(struct puk_mem_reg_s*r)
{
  assert(r->refcount == 0);
  (*puk_mem.unreg)(r->context, r->ptr, r->key);
  r->context = NULL;
  r->ptr = NULL;
  r->len = 0;
  r->key = 0;
  r->refcount = 0;
}

static struct puk_mem_reg_s*puk_mem_slot_lookup(void)
{
  static unsigned int victim = 0;
  int i;
  for(i = 0; i < PUK_MEM_CACHE_SIZE; i++)
    {
      struct puk_mem_reg_s*r = &puk_mem.cache[i];
      if(r->ptr == NULL && r->len == 0)
        {
          return r;
        }
    }
  const int victim_first = victim;
  victim = (victim + 1) % PUK_MEM_CACHE_SIZE;
  struct puk_mem_reg_s*r = &puk_mem.cache[victim];
  while(r->refcount > 0 && victim != victim_first)
    {
      victim = (victim + 1) % PUK_MEM_CACHE_SIZE;
      r = &puk_mem.cache[victim];
    }
  if(r->refcount > 0)
    {
      return NULL;
    }
  puk_mem_cache_flush(r);
  return r;
}

extern void puk_mem_dump(void)
{
  int i;
  for(i = 0; i < PUK_MEM_CACHE_SIZE; i++)
    {
      struct puk_mem_reg_s*r = &puk_mem.cache[i];
      if(r->ptr != NULL)
        {
          puk_mem_cache_flush(r);
        }
    }
}

/** Invalidate memory registration before it is freed.
 */
extern void puk_mem_invalidate(void*ptr, size_t size)
{
  if(ptr)
    {
      int i;
      for(i = 0; i < PUK_MEM_CACHE_SIZE; i++)
        {
          struct puk_mem_reg_s*r = &puk_mem.cache[i];
          const void*rbegin = r->ptr;
          const void*rend   = r->ptr + r->len;
          const void*ibegin = ptr;
          const void*iend   = ptr + size;
          if( (r->ptr != NULL) &&
              ( ((ibegin >= rbegin) && (ibegin < rend)) ||
                ((iend >= rbegin) && (iend < rend)) ||
                ((ibegin >= rbegin) && (iend < rend)) ||
                ((rbegin >= ibegin) && (rend < iend)) ) )
            {
              if(r->refcount > 0)
                {
                  fprintf(stderr, "PukABI: trying to invalidate registered memory still in use.\n");
                  abort();
                }
              puk_mem_cache_flush(r);
            }
        }
    }
}

/* ********************************************************* */

void puk_mem_set_handlers(puk_mem_register_t reg, puk_mem_unregister_t unreg)
{
  puk_spinlock_acquire();
  puk_mem.reg = reg;
  puk_mem.unreg = unreg;
  puk_mem_invalidate_hook = &puk_mem_invalidate;
  puk_spinlock_release();
}
void puk_mem_unreg(const struct puk_mem_reg_s*_r)
{
  struct puk_mem_reg_s*r = (struct puk_mem_reg_s*)_r;
  puk_spinlock_acquire();
  r->refcount--;
  if(r->refcount < 0)
    {
      fprintf(stderr, "PukABI: unbalanced registration detected.\n");
      abort();
    }
  puk_spinlock_release();
}
const struct puk_mem_reg_s*puk_mem_reg(void*context, const void*ptr, size_t len)
{
  assert(ptr != NULL);
  puk_spinlock_acquire();
  int i;
  for(i = 0; i < PUK_MEM_CACHE_SIZE; i++)
    {
      struct puk_mem_reg_s*r = &puk_mem.cache[i];
      if(context == r->context && ptr >= r->ptr && (ptr + len <= r->ptr + r->len))
        {
          r->refcount++;
#ifdef PUK_MEM_STAT
          puk_mem.cache_hit++;
#endif
          puk_spinlock_release();
          return r;
        }
    }
  struct puk_mem_reg_s*r = puk_mem_slot_lookup();
  if(r)
    {
      r->context  = context;
      r->ptr      = ptr;
      r->len      = len;
      r->refcount = 1;
      r->key      = (*puk_mem.reg)(context, ptr, len);
    }
  else
    {
      puk_abi_fatal("PukABI: rcache buffer full.\n");
    }
#ifdef PUK_MEM_STAT
  puk_mem.cache_miss++;
#endif
  puk_spinlock_release();
  return r;
}

/* ********************************************************* */

static char*puk_mem_strdup(const char*s1)
{
  char*p = NULL;
  if(s1)
    {
      const int size = strlen(s1) + 1;
      p = __puk_abi_stub__malloc__(size);
      if(!p)
        return NULL;
      memcpy(p, s1, size);
    }
  return p;
}

#endif /* PUKABI_ENABLE_MEM */

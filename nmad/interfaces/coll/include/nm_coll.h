/*
 * NewMadeleine
 * Copyright (C) 2014-2023 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


/* @file
 * @ingroup coll_interface
 */

#include <nm_public.h>
#include <nm_sendrecv_interface.h>
#include <Padico/Puk.h>

/** @ingroup coll_interface
 * @defgroup coll_coll Collective operations
 */

/** @ingroup coll_coll
 * @{
 */

typedef struct nm_coll_req_s*nm_coll_req_t;

/** notification function for collective reqs */
typedef void (*nm_coll_req_notifier_t)(void*ref);

/* ** group-based collectives ****************************** */

extern void nm_coll_group_barrier(nm_session_t p_session, nm_group_t p_group, int self, nm_tag_t tag);

extern void nm_coll_group_bcast(nm_session_t p_session, nm_group_t p_group, int root, int self,
                                void*buffer, nm_len_t len, nm_tag_t tag);

extern void nm_coll_group_scatter(nm_session_t p_session, nm_group_t p_group, int root, int self,
                                  const void*sbuf, nm_len_t slen, void*rbuf, nm_len_t rlen, nm_tag_t tag);

extern void nm_coll_group_gather(nm_session_t p_session, nm_group_t p_group, int root, int self,
                                 const void*sbuf, nm_len_t slen, void*rbuf, nm_len_t rlen, nm_tag_t tag);

/* ** communicator-based collectives *********************** */

extern void nm_coll_barrier(nm_comm_t comm, nm_tag_t tag);

extern void nm_coll_bcast(nm_comm_t comm, int root, void*buffer, nm_len_t len, nm_tag_t tag);

extern void nm_coll_scatter(nm_comm_t comm, int root, const void*sbuf, nm_len_t slen, void*rbuf, nm_len_t rlen, nm_tag_t tag);

extern void nm_coll_gather(nm_comm_t comm, int root, const void*sbuf, nm_len_t slen, void*rbuf, nm_len_t rlen, nm_tag_t tag);

/* ** data-based collectives ******************************* */

extern void nm_coll_data_bcast(nm_comm_t comm, int root, struct nm_data_s*p_data, nm_tag_t tag);

extern void nm_coll_data_scatter(nm_comm_t p_comm, int root, struct nm_data_s p_sdata[], struct nm_data_s*p_rdata, nm_tag_t tag);

extern void nm_coll_data_gather(nm_comm_t p_comm, int root, struct nm_data_s*p_sdata, struct nm_data_s p_rdata[], nm_tag_t tag);

extern void nm_coll_group_data_bcast(nm_session_t p_session, nm_group_t p_group, int root, int self,
                                     struct nm_data_s*p_data, nm_tag_t tag);

/* ** non-blocking collectives *****************************  */

/* rationale of coll requests:
 *   - the coll_req is allocated by the primitive used to post the coll req
 *   - 3 completion modes are available: wait, test, or notify; they are
 *     mutually exclusive, a request with a notifier cannot be tested or
 *     waited for.
 *   - the request is automatically freed upon completion in all 3 cases.
 */

/* ** coll reqs */

extern void nm_coll_req_wait(struct nm_coll_req_s*p_coll_req);

extern int nm_coll_req_test(struct nm_coll_req_s*p_coll_req);


/** generic ibcast with automatic algorithm selection */
extern struct nm_coll_req_s*nm_coll_group_data_ibcast(nm_session_t p_session, nm_group_t p_group, int root, int self,
                                                      struct nm_data_s*p_data, nm_tag_t tag,
                                                      nm_coll_req_notifier_t p_notify, void*ref);

/** ibcast with pipelined algorithm */
extern struct nm_coll_req_s*nm_coll_ibcast_pipeline(nm_session_t p_session, nm_group_t p_group, int root, int self,
                                                    struct nm_data_s*p_data, nm_tag_t tag, nm_len_t block_size, nm_coll_tree_kind_t kind,
                                                    nm_coll_req_notifier_t p_notify, void*ref);
/** ibcast with tree algorithm */
extern struct nm_coll_req_s*nm_coll_bcast_tree(nm_session_t p_session, nm_group_t p_group, int root, int self,
                                               struct nm_data_s*p_data, nm_tag_t tag, nm_coll_tree_kind_t kind,
                                               nm_coll_req_notifier_t p_notify, void*ref);

/* ** ibarrier */

extern struct nm_coll_req_s*nm_coll_group_ibarrier(nm_session_t p_session, nm_group_t p_group,
                                                   int self, nm_tag_t tag);

/* ** igather */

extern struct nm_coll_req_s*nm_coll_group_data_igather(nm_session_t p_session, nm_group_t p_group, int root, int self,
                                                       struct nm_data_s*p_sdata, struct nm_data_s p_rdata[], nm_tag_t tag,
                                                       nm_coll_req_notifier_t p_notify, void*ref);

extern struct nm_coll_req_s*nm_coll_igather(nm_comm_t comm, int root, const void*sbuf, nm_len_t slen,
                                            void*rbuf, nm_len_t rlen, nm_tag_t tag,
                                            nm_coll_req_notifier_t p_notify, void*ref);

/* ** specific operations */

void nm_coll_bcast_2trees(nm_session_t p_session, nm_group_t p_group, int root, int self,
                          struct nm_data_s*p_data, nm_tag_t tag1, nm_tag_t tag2, nm_len_t block_size, nm_coll_tree_kind_t kind);


/** @} */

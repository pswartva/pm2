/*
 * NewMadeleine
 * Copyright (C) 2011-2023 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <infiniband/verbs.h>

#include "nm_connector.h"
#include "nm_ibverbs.h"
#include <nm_private.h>
#include <Padico/Module.h>

/* ********************************************************* */

/** timeout to receive connection ACK after sending connect address to peer (in usec.) */
#define NM_CONNECTOR_IBUD_TIMEOUT_ACK   (600 * 1000)

/** large enough to contain our nm_connector url  */
#define NM_CONNECTOR_IBUD_URL_SIZE sizeof(struct nm_ibverbs_addr_s)

/** a full address entry, with host id and associated address.
 */
struct nm_connector_ibud_entry_s
{
  char url[NM_CONNECTOR_IBUD_URL_SIZE];
  int ack;
  char _addr; /**< placeholder for variable-length field */
};

static int nm_connector_ibud_entry_eq(const struct nm_connector_ibud_entry_s*e1, const struct nm_connector_ibud_entry_s*e2)
{
  return (memcmp(e1->url, e2->url, NM_CONNECTOR_IBUD_URL_SIZE) == 0);
}
static uint32_t nm_connector_ibud_entry_hash(const struct nm_connector_ibud_entry_s*e)
{
  return puk_hash_oneatatime((const void*)e->url, NM_CONNECTOR_IBUD_URL_SIZE);
}
static void nm_connector_ibud_entry_destructor(struct nm_connector_ibud_entry_s*p_key, struct nm_connector_ibud_entry_s*p_data)
{
  padico_free(p_key);
}

PUK_HASHTABLE_TYPE(nm_connector_addr, struct nm_connector_ibud_entry_s*, struct nm_connector_ibud_entry_s*,
                   &nm_connector_ibud_entry_hash, &nm_connector_ibud_entry_eq, &nm_connector_ibud_entry_destructor);

/** connection manager, exchange per-connection url using per node url
 */
struct nm_connector_ibud_s
{
  struct nm_ibverbs_hca_s hca;
  struct nm_ibverbs_ud_ep_s ud_ep;
  nm_connector_addr_hashtable_t addrs; /**< already received addresses, hashed by node url */
  char*p_url;             /**< url for the connector itself */
  int addr_len;           /**< length of addresses */
  struct nm_ibverbs_ud_recv_s recv;
  void*recvbuf;
};


/* ********************************************************* */

static void*nm_connector_ibud_instantiate(puk_instance_t instance, puk_context_t context);
static void nm_connector_ibud_destroy(void*);

static const struct puk_component_driver_s nm_connector_ibud_component =
  {
    .instantiate = &nm_connector_ibud_instantiate,
    .destroy     = &nm_connector_ibud_destroy
  };

static void nm_connector_ibud_create(void*_status, int addr_len, const char**url);
static int nm_connector_ibud_exchange(void*_status,
                                     const char*local_connector_url, const char*remote_connector_url,
                                     const void*local_cnx_addr, void*remote_cnx_addr);

static const struct nm_connector_iface_s nm_connector_ibud =
  {
    .create   = &nm_connector_ibud_create,
    .exchange = &nm_connector_ibud_exchange
  };

PADICO_MODULE_COMPONENT(NewMad_Connector_ibud,
  puk_component_declare("NewMad_Connector_ibud",
                        puk_component_provides("PadicoComponent", "component", &nm_connector_ibud_component),
                        puk_component_provides("NewMad_Connector", "connector", &nm_connector_ibud)));

/* ********************************************************* */
/* ** connector */

static void*nm_connector_ibud_instantiate(puk_instance_t instance, puk_context_t context)
{
  /* allocate connector */
  struct nm_connector_ibud_s*p_connector = padico_malloc(sizeof(struct nm_connector_ibud_s));
  p_connector->addrs = nm_connector_addr_hashtable_new();
  p_connector->recvbuf = NULL;
  nm_ibverbs_hca_open(&p_connector->hca, "auto", -1);
  nm_ibverbs_ud_ep_open(&p_connector->ud_ep, &p_connector->hca);
  return p_connector;
}

static void nm_connector_ibud_destroy(void*_status)
{
  struct nm_connector_ibud_s*p_connector = _status;
  nm_ibverbs_ud_ep_close(&p_connector->ud_ep);
  nm_ibverbs_hca_close(&p_connector->hca);
  nm_connector_addr_hashtable_delete(p_connector->addrs);
  padico_free(p_connector);
}

void nm_connector_ibud_create(void*_status, int addr_len, const char**url)
{
  struct nm_connector_ibud_s*p_connector = _status;
  p_connector->addr_len = addr_len;

  fprintf(stderr, "# ## ## create()- addr_len = %d\n", addr_len);

  /* encode url */
  size_t urlsize = sizeof(p_connector->ud_ep.local_addr);
  p_connector->p_url = puk_hex_encode(&p_connector->ud_ep.local_addr, &urlsize, NULL);
  *url = p_connector->p_url;
}

static void nm_connector_ibud_send(struct nm_connector_ibud_s*p_connector, const char*remote_url,
                                   const void*local_addr, int ack)
{
  /* parse peer address */
  size_t urlsize = strlen(remote_url);
  struct nm_ibverbs_addr_s*p_remote_addr = puk_hex_decode(remote_url, &urlsize, NULL);
  assert(urlsize == sizeof(struct nm_ibverbs_addr_s));

  /* build address packet */
  const size_t size = sizeof(struct nm_connector_ibud_entry_s) + p_connector->addr_len;
  void*sendbuf = padico_malloc(GRH_OFFSET + size);
  struct nm_connector_ibud_entry_s*local_entry = sendbuf + GRH_OFFSET;
  memset(local_entry, 0, sizeof(struct nm_connector_ibud_entry_s));
  memcpy(local_entry->url, &p_connector->ud_ep.local_addr, NM_CONNECTOR_IBUD_URL_SIZE);
  memcpy(&local_entry->_addr, local_addr, p_connector->addr_len);
  local_entry->ack = ack;

  fprintf(stderr, "# ## send- remote lid = %d; qpn = %d; size = %ld.\n", p_remote_addr->lid, p_remote_addr->qpn, size);

  nm_ibverbs_ud_send(&p_connector->ud_ep, sendbuf, size, p_remote_addr);

  padico_free(p_remote_addr);
  padico_free(sendbuf);
}

static void nm_connector_ibud_rearm(struct nm_connector_ibud_s*p_connector)
{
  fprintf(stderr, "# ## rearm\n");

  if(p_connector->recvbuf == NULL)
    {
      const size_t size = sizeof(struct nm_connector_ibud_entry_s) + p_connector->addr_len;
      fprintf(stderr, "# ## rearm # post recv; size = %ld.\n", size);

      p_connector->recvbuf = padico_malloc(GRH_OFFSET + size);
      struct nm_connector_ibud_entry_s*recventry = p_connector->recvbuf + GRH_OFFSET;
      memset(recventry, 0, size);

      nm_ibverbs_ud_recv_post(&p_connector->recv, &p_connector->ud_ep, p_connector->recvbuf, size);
    }
}

/* poll network for peer address; return 0 for success; -1 for timeout */
static int nm_connector_ibud_poll(struct nm_connector_ibud_s*p_connector)
{
  fprintf(stderr, "# ## poll.\n");

  nm_connector_ibud_rearm(p_connector);
  puk_tick_t t1, t2;
  PUK_GET_TICK(t1);
  int rc = 0;
  do
    {
      rc = nm_ibverbs_ud_recv_poll(&p_connector->recv);
      PUK_GET_TICK(t2);
      const double usec = PUK_TIMING_DELAY(t1, t2);
      if(usec > NM_CONNECTOR_IBUD_TIMEOUT_ACK)
        return -1;
    }
  while(rc != 0);

  struct nm_connector_ibud_entry_s*recventry = p_connector->recvbuf + GRH_OFFSET;
  fprintf(stderr, "# ## recv packet - from = %s; ack = %d ------------------------------------------------\n",
          recventry->url, recventry->ack);
  struct nm_connector_ibud_entry_s*prev = nm_connector_addr_hashtable_lookup(p_connector->addrs, recventry);
  if(prev)
    {
      nm_connector_addr_hashtable_remove(p_connector->addrs, prev);
      padico_free(prev);
    }
  recventry = padico_malloc(sizeof(struct nm_connector_ibud_entry_s) + p_connector->addr_len);
  memcpy(recventry, p_connector->recvbuf + GRH_OFFSET, sizeof(struct nm_connector_ibud_entry_s) + p_connector->addr_len);
  nm_connector_addr_hashtable_insert(p_connector->addrs, recventry, recventry);
  padico_free(p_connector->recvbuf);
  p_connector->recvbuf = NULL;
  return 0;
}

static int nm_connector_ibud_recv(struct nm_connector_ibud_s*p_connector, const char*remote_url, void*remote_addr)
{
  size_t urlsize = strlen(remote_url);
  struct nm_ibverbs_addr_s*p_remote_addr = puk_hex_decode(remote_url, &urlsize, NULL);;
  struct nm_connector_ibud_entry_s key;
  memcpy(key.url, p_remote_addr, NM_CONNECTOR_IBUD_URL_SIZE);
  for(;;)
    {
      /* lookup in already received address */
      struct nm_connector_ibud_entry_s*remote_entry = nm_connector_addr_hashtable_lookup(p_connector->addrs, &key);
      if(remote_entry != NULL)
        {
          memcpy(remote_addr, &remote_entry->_addr, p_connector->addr_len);
          fprintf(stderr, "# ## recv addr SUCCESS! -------------------------------------------\n");
          return 0;
        }
      int rc = nm_connector_ibud_poll(p_connector);
      if(rc != 0)
        {
          NM_WARN("timeout while receiving peer address.\n");
          /* Timeout- we didn't receive peer address in time. We don't know
           * whether our packet was lost too, and we cannot try to establish
           * the connection to check. In doubt, return error. Caller will send again.
           */
          return -1;
        }
    }
  return 0;
}

static int nm_connector_ibud_wait_ack(struct nm_connector_ibud_s*p_connector, const char*remote_url)
{
  size_t urlsize = strlen(remote_url);
  struct nm_ibverbs_addr_s*p_remote_addr = puk_hex_decode(remote_url, &urlsize, NULL);;
  struct nm_connector_ibud_entry_s key;
  memcpy(key.url, p_remote_addr, NM_CONNECTOR_IBUD_URL_SIZE);
  for(;;)
    {
      struct nm_connector_ibud_entry_s*remote_entry = nm_connector_addr_hashtable_lookup(p_connector->addrs, &key);
      if(remote_entry != NULL && remote_entry->ack != 0)
        {
          fprintf(stderr, "# ## recv ACK SUCCESS! -------------------------------------------\n");
          return 0;
        }
      int rc = nm_connector_ibud_poll(p_connector);
      if(rc != 0)
        {
          NM_WARN("timeout while waiting for ACK.\n");
          return -1;
        }
    }
  return 0;
}

void nm_connector_display_addr(const unsigned char*addr, size_t len)
{
  size_t k;
  for(k = 0; k < len; k++)
    {
      fprintf(stderr, "%02x ", addr[k]);
    }
  fprintf(stderr, "\n");
}

static int nm_connector_ibud_exchange(void*_status,
                                     const char*local_url, const char*remote_url,
                                     const void*local_addr, void*remote_addr)
{
  struct nm_connector_ibud_s*p_connector = _status;
  fprintf(stderr, "# exchange: entering.\n");
  assert(p_connector != NULL);
  assert(strcmp(p_connector->p_url, local_url) == 0);
  nm_connector_ibud_rearm(p_connector);
  int rc = -1;
  do
    {
      nm_connector_ibud_send(p_connector, remote_url, local_addr, 0);
      rc = nm_connector_ibud_recv(p_connector, remote_url, remote_addr);
    }
  while(rc != 0);
  do
    {
      nm_connector_ibud_send(p_connector, remote_url, local_addr, 1);
      rc = nm_connector_ibud_wait_ack(p_connector, remote_url);
    }
  while(rc != 0);
  fprintf(stderr, "# exchange: done.\n");
  fprintf(stderr, "# local  addr: ");
  nm_connector_display_addr(local_addr, p_connector->addr_len);
  fprintf(stderr, "# remote addr: ");
  nm_connector_display_addr(remote_addr, p_connector->addr_len);

  return rc;
}

/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Puk string-related stuff
 * @ingroup Puk
 */

#include <string.h>
#include <stdlib.h>
#include <stdarg.h>

/* *** Puk automatic strings ******************************* */

struct padico_string_s
{
  int   _size;
  int   _capacity;
  char* _text;
};
typedef struct padico_string_s*padico_string_t;

static inline padico_string_t padico_string_new(void)
{
  padico_string_t s = (padico_string_t)padico_malloc(sizeof(struct padico_string_s));
#define PADICO_STRING_DEFAULT_CAPACITY 128
  s->_capacity = PADICO_STRING_DEFAULT_CAPACITY;
  s->_text = (char*)padico_malloc(s->_capacity);
  s->_text[0] = 0;
  s->_size = 0;
  return s;
}

static inline void __padico_string_delete(padico_string_t*_s)
{
  padico_string_t s = *_s;
  padico_free(s->_text);
  s->_size = 0;
  padico_free(s);
  *_s = NULL;
}
#define padico_string_delete(S) __padico_string_delete(&(S))

static inline void padico_string_grow(padico_string_t s, int size_inc)
{
  if(s->_size + size_inc >= s->_capacity)
    {
      s->_capacity *= 2;
      if(s->_size + size_inc >= s->_capacity)
        {
          s->_capacity += size_inc;
        }
      s->_text = (char*)padico_realloc(s->_text, s->_capacity);
    }
}

static inline void padico_string_printf(padico_string_t s, const char*fmt, ...)
{
  int rc;
  int restart;
  va_list ap;
  do {
    restart = 0;
    va_start(ap, fmt);
    rc = vsnprintf(s->_text, s->_capacity, fmt, ap);
    if((rc < 0) || (rc >= s->_capacity))
      {
        padico_string_grow(s, rc);
        restart = 1;
      }
    va_end(ap);
  }
  while(restart);
  s->_size = strlen(s->_text) + 1;
}

static inline void padico_string_cat(padico_string_t s, const char*s2)
{
  padico_string_grow(s, strlen(s2));
  strcat(s->_text, s2);
  s->_size = strlen(s->_text) + 1;
}

static inline void padico_string_catf(padico_string_t s, const char*fmt, ...)
{
  int rc;
  int restart;
  va_list ap;
  do {
    restart = 0;
    va_start(ap, fmt);
    rc = vsnprintf(s->_text + (s->_size?(s->_size - 1):0), s->_capacity - s->_size, fmt, ap);
    if((rc < 0) || (rc >= s->_capacity - s->_size))
      {
        restart = 1;
        padico_string_grow(s, rc);
      }
    va_end(ap);
  }
  while(restart);
  s->_size = strlen(s->_text) + 1;
}

static inline char*padico_string_get(padico_string_t s)
{
  return s->_text;
}
static inline int padico_string_size(padico_string_t s)
{
  return s->_size;
}
static inline void padico_string_resize(padico_string_t s, int size)
{
  padico_string_grow(s, size);
  s->_size = size;
}

static inline void padico_string_replace(padico_string_t s, char source, char target)
{
  char*c = s->_text;
  while(*c != '\0')
    {
      if(*c == source)
        *c = target;
      c++;
    }
}

/* *** Puk encoding **************************************** */

char*puk_escape_encode(const void*bytes, size_t*len, void*(*alloc)(unsigned long));
void*puk_escape_decode(const char*text,  size_t*len, void*(*alloc)(unsigned long));

char*puk_hex_encode(const void*bytes, size_t*len, void*(*alloc)(unsigned long));
void*puk_hex_decode(const char*text,  size_t*len, void*(*alloc)(unsigned long));

char*puk_base16_encode(const void*bytes, size_t*len, void*(*alloc)(unsigned long));
void*puk_base16_decode(const char*text,  size_t*len, void*(*alloc)(unsigned long));



/* *** Puk error codes ************************************* */

/** @addtogroup PukMessage
 * @{
 */
/** Type used for return codes */
struct padico_rc_s
{
  int rc;              /**< the code itself */
  padico_string_t msg; /**< message (error or info) */
};
typedef struct padico_rc_s*padico_rc_t;

static inline padico_rc_t padico_rc_new(void)
{
  padico_rc_t rc = (padico_rc_t)padico_malloc(sizeof(struct padico_rc_s));
  rc->rc = 0;
  rc->msg = NULL;
  return rc;
}
static inline void __padico_rc_delete(padico_rc_t*_rc)
{
  padico_rc_t rc = *_rc;
  if(rc && rc->msg)
    padico_string_delete(rc->msg);
  padico_free(rc);
  *_rc = NULL;
}
#define padico_rc_delete(RC) __padico_rc_delete(&(RC))

static inline padico_rc_t padico_rc_ok(void) { return NULL; }
static inline int padico_rc_iserror(padico_rc_t rc) { return ((rc != NULL) && (rc->rc)); }
padico_rc_t padico_rc_create(int _rc, const char*fmt, ...);
#define padico_rc_error(...) padico_rc_create(-1, __VA_ARGS__)
#define padico_rc_msg(...)   padico_rc_create(0, __VA_ARGS__)
padico_rc_t padico_rc_cat(padico_rc_t, padico_rc_t);
padico_rc_t padico_rc_add(padico_rc_t rc1 , padico_rc_t rc2);
void        padico_rc_show(padico_rc_t);
char*       padico_rc_gettext(padico_rc_t);


/** @} */

/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief optimized base64 encoder/decoder
 * @note uses code from public domain.
 */

#include "Puk-base64.h"
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

/*
  This is part of the libb64 project, and has been placed in the public domain.
  For details, see http://sourceforge.net/projects/libb64
*/

typedef enum
  {
    step_A, step_B, step_C
  } base64_encodestep;

typedef struct
{
  base64_encodestep step;
  char result;
  int stepcount;
} base64_encodestate;

static void base64_init_encodestate(base64_encodestate* state_in);

static char base64_encode_value(char value_in);

static int base64_encode_block(const char* plaintext_in, int length_in, char* code_out, base64_encodestate* state_in);

static int base64_encode_blockend(char* code_out, base64_encodestate* state_in);

typedef enum
  {
    step_a, step_b, step_c, step_d
  } base64_decodestep;

typedef struct
{
  base64_decodestep step;
  char plainchar;
} base64_decodestate;

static void base64_init_decodestate(base64_decodestate* state_in);

static int base64_decode_value(int value_in);

static int base64_decode_block(const char* code_in, const int length_in, char* plaintext_out, base64_decodestate* state_in);

/* ** Puk base 64 ****************************************** */

char*puk_base64_encode(const void*bytes, int*_len, void*(*alloc)(unsigned long))
{
  ssize_t out_len = 8 + ((*_len * 5) / 3) + (*_len / 72); /* bytes * 4/3 + bytes/72 + 4 rounding errors + 2*trailing '=' + trailing \n + trailing \0 */
  char*text = alloc?(*alloc)(out_len):malloc(out_len);
  if(text == NULL)
    {
      fprintf(stderr, "Puk: allocation failed.\n");
      abort();
    }
  base64_encodestate state;
  base64_init_encodestate(&state);
  ssize_t rc = base64_encode_block(bytes, *_len, text, &state);
  if(rc > out_len)
    {
      fprintf(stderr, "Puk: buffer overflow in base64 encoder.\n");
      abort();
    }
  rc += base64_encode_blockend(text + rc, &state);
  if(rc > out_len)
    {
      fprintf(stderr, "Puk: buffer overflow in base64 encoder.\n");
      abort();
    }
  *_len = rc - 1;
  return text;
}

void*puk_base64_decode(const char*text, int*len, void*(*alloc)(unsigned long))
{
  ssize_t out_len = *len + 3;
  char*bytes = alloc?(*alloc)(out_len):malloc(out_len);
  if(bytes == NULL)
    {
      fprintf(stderr, "Puk: allocation failed.\n");
      abort();
    }
  base64_decodestate state;
  base64_init_decodestate(&state);
  ssize_t rc = base64_decode_block(text, *len, bytes, &state);
  if(rc > out_len)
    {
      fprintf(stderr, "Puk: buffer overflow in base64 decoder.\n");
      abort();
    }
  *len = rc;
  return bytes;
}

/* ********************************************************* */

#define CHARS_PER_LINE 72
#undef BASE64_BREAK_LINES

static void base64_init_encodestate(base64_encodestate* state_in)
{
  state_in->step = step_A;
  state_in->result = 0;
  state_in->stepcount = 0;
}

static inline char base64_encode_value(char value_in)
{
  static const char* encoding = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
  if (value_in > 63) return '=';
  return encoding[(int)value_in];
}

static int base64_encode_block(const char* plaintext_in, int length_in, char* code_out, base64_encodestate* state_in)
{
  const char* plainchar = plaintext_in;
  const char* const plaintextend = plaintext_in + length_in;
  char* codechar = code_out;
  char result;
  char fragment;

  result = state_in->result;

  switch (state_in->step)
    {
      while (1)
        {
        case step_A:
          if (plainchar == plaintextend)
            {
              state_in->result = result;
              state_in->step = step_A;
              return codechar - code_out;
            }
          fragment = *plainchar++;
          result = (fragment & 0x0fc) >> 2;
          *codechar++ = base64_encode_value(result);
          result = (fragment & 0x003) << 4;
        case step_B:
          if (plainchar == plaintextend)
            {
              state_in->result = result;
              state_in->step = step_B;
              return codechar - code_out;
            }
          fragment = *plainchar++;
          result |= (fragment & 0x0f0) >> 4;
          *codechar++ = base64_encode_value(result);
          result = (fragment & 0x00f) << 2;
        case step_C:
          if (plainchar == plaintextend)
            {
              state_in->result = result;
              state_in->step = step_C;
              return codechar - code_out;
            }
          fragment = *plainchar++;
          result |= (fragment & 0x0c0) >> 6;
          *codechar++ = base64_encode_value(result);
          result  = (fragment & 0x03f) >> 0;
          *codechar++ = base64_encode_value(result);
          ++(state_in->stepcount);
#ifdef BASE64_BREAK_LINES
          if (state_in->stepcount == CHARS_PER_LINE/4)
            {
              *codechar++ = '\n';
              state_in->stepcount = 0;
            }
#endif /* BASE64_BREAK_LINES */
        }
    }
  /* control should not reach here */
  return codechar - code_out;
}

static int base64_encode_blockend(char* code_out, base64_encodestate* state_in)
{
  char* codechar = code_out;

  switch (state_in->step)
    {
    case step_B:
      *codechar++ = base64_encode_value(state_in->result);
      *codechar++ = '=';
      *codechar++ = '=';
      break;
    case step_C:
      *codechar++ = base64_encode_value(state_in->result);
      *codechar++ = '=';
      break;
    case step_A:
      break;
    }
  *codechar++ = '\n';
  *codechar++ = '\0';

  return codechar - code_out;
}


static inline int base64_decode_value(int value_in)
{
  static const int decoding[] = {62,-1,-1,-1,63,52,53,54,55,56,57,58,59,60,61,-1,-1,-1,-2,-1,-1,-1,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,-1,-1,-1,-1,-1,-1,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51};
  static const int decoding_size = sizeof(decoding);
  value_in -= 43;
  if (value_in < 0 || value_in > decoding_size) return -1;
  return decoding[value_in];
}

static void base64_init_decodestate(base64_decodestate* state_in)
{
  state_in->step = step_a;
  state_in->plainchar = 0;
}

static int base64_decode_block(const char* code_in, const int length_in, char* plaintext_out, base64_decodestate* state_in)
{
  const char* codechar = code_in;
  char* plainchar = plaintext_out;
  int fragment;

  *plainchar = state_in->plainchar;

  switch (state_in->step)
    {
      while (1)
        {
        case step_a:
          do {
            if (codechar == code_in+length_in)
              {
                state_in->step = step_a;
                state_in->plainchar = *plainchar;
                return plainchar - plaintext_out;
              }
            fragment = base64_decode_value(*codechar++);
          } while (fragment < 0);
          *plainchar    = (fragment & 0x03f) << 2;
        case step_b:
          do {
            if (codechar == code_in+length_in)
              {
                state_in->step = step_b;
                state_in->plainchar = *plainchar;
                return plainchar - plaintext_out;
              }
            fragment = base64_decode_value(*codechar++);
          } while (fragment < 0);
          *plainchar++ |= (fragment & 0x030) >> 4;
          *plainchar    = (fragment & 0x00f) << 4;
        case step_c:
          do {
            if (codechar == code_in+length_in)
              {
                state_in->step = step_c;
                state_in->plainchar = *plainchar;
                return plainchar - plaintext_out;
              }
            fragment = base64_decode_value(*codechar++);
          } while (fragment < 0);
          *plainchar++ |= (fragment & 0x03c) >> 2;
          *plainchar    = (fragment & 0x003) << 6;
        case step_d:
          do {
            if (codechar == code_in+length_in)
              {
                state_in->step = step_d;
                state_in->plainchar = *plainchar;
                return plainchar - plaintext_out;
              }
            fragment = base64_decode_value(*codechar++);
          } while (fragment < 0);
          *plainchar++   |= (fragment & 0x03f);
        }
    }
  /* control should not reach here */
  return plainchar - plaintext_out;
}

/*
 * NewMadeleine
 * Copyright (C) 2006-2018 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
int main(int argc, char **argv)
{
  int numtasks, rank;
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &numtasks);

  int i;
  for(i = 0; i < numtasks; i++)
    {
      if(rank == 0)
        {
          fprintf(stderr, "# sleeping on node #%d\n", i);
        }
      if(rank == i)
        {
          sleep(2);
        }
      MPI_Request req;
      MPI_Ibarrier(MPI_COMM_WORLD, &req);
      fprintf(stderr, "# node #%d waiting for round #%d\n", rank, i);
      MPI_Wait(&req, MPI_STATUS_IGNORE);
    }
  
  MPI_Finalize();
  return 0;
}

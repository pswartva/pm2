/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 *  @brief Various checksum algorithms.
 */


#ifndef PUK_CHECKSUM_H
#define PUK_CHECKSUM_H

#include <stdint.h>
#ifdef __SSE2__
#include <emmintrin.h>
#endif
#include "siphash.h"
#include "halfsiphash.h"

typedef uint32_t (*puk_checksum_func_t)(const void*_data, size_t _len);
typedef uint32_t (*puk_checksum_copy_func_t)(void*_dest, const void*_src, size_t _len);

/** a checksum algorithm */
struct puk_checksum_s
{
  const char*short_name;
  const char*name;
  puk_checksum_func_t func;
  puk_checksum_copy_func_t checksum_and_copy;
};
typedef const struct puk_checksum_s*puk_checksum_t;

extern puk_checksum_t puk_checksum_get(const char*short_name);

extern uint32_t puk_checksum_softcrc(const void*data, size_t len);

extern uint64_t puk_checksum_siphash(const void*data, size_t len);

extern uint32_t puk_checksum_halfsiphash(const void*data, size_t len);

#endif /* PUK_CHECKSUM_H */

#!/bin/bash

gcc testFIFO_lfqueue.c -o testFIFOlfqueue -l pthread -lrt `pkg-config --libs hwloc`

gcc testFIFO_lqueuemutex.c -o testFIFOlqueuemutex -l pthread -lrt `pkg-config --libs hwloc`

gcc testFIFO_lqueuespinlock.c -o testFIFOlqueuespinlock -l pthread -lrt `pkg-config --libs hwloc`
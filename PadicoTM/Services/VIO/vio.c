/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief I/O virtualizers- Implements padico_vio_*
 * @ingroup VIO
 */

#include "VIO-internals.h"
#include "VLink-API.h"
#include "VLink-debug.h"
#include "VLink-internals.h"
#include <Padico/Module.h>

#include <unistd.h>
#include <stdlib.h>
#include <sys/uio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>
#include <ulimit.h>
#include <stdarg.h>
#ifdef HAVE_INOTIFY_H
#include <sys/inotify.h>
#endif

PADICO_MODULE_HOOK(VIO);


/* ********************************************************* */

static const char vio_default_vsock[]   = "VLink_selector";
static const char vio_default_vfile[]   = "VFile_SysIO";
static const char vio_default_vfile64[] = "VFile64_SysIO";

/** a Padico file descriptor for virtualized I/O
 */
struct padico_vio_desc_s
{
  int desc_flag;        /**< flags attached to the descriptor */
  padico_vnode_t vnode; /**< the vnode associated with this descriptor */
};

#ifdef VIO_PUK_ABI
#  define PADICO_VIO_MAX_DESC PUK_ABI_FD_MAX
#else
#  define PADICO_VIO_MAX_DESC 256
#endif

/** internal state of the VIO personnality */
static struct
{
  struct padico_vio_desc_s vios[PADICO_VIO_MAX_DESC];
  marcel_mutex_t lock;
} vio;


struct padico_vio_info_s
{
  volatile int interrupt_req;
  int file_flag;  /**< flags associated with the *file*, not the descriptor
                   * @note F_GETFL/F_SETFL with fcntl()
                   * O_NONBLOCK/O_NDELAY only is supported
                   * @todo Maybe it shouldn't be in the vnode since it is only
                   * used at the 'VIO' level.
                   */
};

#define VIO_DATA(VNODE) (*((struct padico_vio_info_s*)((VNODE)->cookie)))

static inline padico_vnode_t vfd_to_vnode(int fd)
{
  return ( (fd < 0 || fd >= PADICO_VIO_MAX_DESC) ? NULL : vio.vios[fd].vnode);
}

/* *** VIO errno ******************************************* */

#ifdef VIO_PUK_ABI
/* errno managed by Puk-ABI */
extern int padico_vio_get_errno()
{
  return puk_abi_geterrno();
}
static inline void padico_vio_set_errno(int value)
{
  puk_abi_seterrno(value);
}
int*padico_vio_errno_location(void)
{
  return puk_abi_errno_stub();
}
#else /* VIO_PUK_ABI */
extern int padico_vio_get_errno()
{
  return errno;
}
static inline void padico_vio_set_errno(int value)
{
  errno = value;
}
#endif


/* *** VIO vfd management ********************************** */

static void vio_init_vnode(padico_vnode_t vnode)
{
  padico_out(40, "vnode=%p; locking\n", vnode);
  vnode_lock(vnode);
  padico_out(40, "vnode=%p; locked\n", vnode);
  vnode->cookie = padico_malloc(sizeof(struct padico_vio_info_s));
  VIO_DATA(vnode).file_flag = 0;
  VIO_DATA(vnode).interrupt_req = 0;
  vnode_unlock(vnode);
}

static void vio_release_vnode(padico_vnode_t vnode)
{
  if(vnode->cookie)
    {
      padico_free(vnode->cookie);
      vnode->cookie = NULL;
    }
}

static padico_vnode_t vio_pivot_vnode(padico_vnode_t vnode, int fd)
{
  if(vnode->content.pivot)
    {
      padico_vnode_t old_vnode = vnode;
      vnode = old_vnode->pivot.new_vnode;
      old_vnode->pivot.new_vnode = NULL;
      vnode->cookie = old_vnode->cookie;
      vnode_unlock(old_vnode);
      marcel_mutex_lock(&vio.lock);
      vio.vios[fd].vnode = vnode;
      marcel_mutex_unlock(&vio.lock);
      vnode_lock(old_vnode);
      padico_vsock_fullshutdown(old_vnode);
      vnode_unlock(old_vnode);
      vnode_lock(vnode);
    }
  return vnode;
}

static int vio_alloc_desc(padico_vnode_t vnode)
{
  marcel_mutex_lock(&vio.lock);
#ifdef VIO_PUK_ABI
  int i = puk_abi_vfd_alloc();
  if(i < 0)
    {
      padico_warning("no more file descriptors.\n");
      marcel_mutex_unlock(&vio.lock);
      return -EMFILE; /* file descriptor table full */
    }
#else /* VIO_PUK_ABI */
  int i = 3; /* 0, 1, 2 are reserved for standard I/O */
  while(vio.vios[i].vnode != NULL)
    {
      i++;
      if(i >= PADICO_VIO_MAX_DESC)
        {
          padico_warning("no more file descriptors\n");
          i = -1;
          padico_vio_set_errno(EMFILE); /* file descriptor table full */
          marcel_mutex_unlock(&vio.lock);
          return -1;
        }
    }
#endif /* VIO_PUK_ABI */
  vio.vios[i].vnode = vnode;
  vio.vios[i].desc_flag = 0;
  vio_init_vnode(vnode);
  marcel_mutex_unlock(&vio.lock);
  return i;
}

void vio_store_desc(padico_vnode_t vnode, int vfd)
{
  assert(vio.vios[vfd].vnode == NULL);
  vio.vios[vfd].vnode = vnode;
  vio.vios[vfd].desc_flag = 0;
  vio_init_vnode(vnode);
}

static void vio_release_desc(int vfd)
{
  marcel_mutex_lock(&vio.lock);
  vio_release_vnode(vio.vios[vfd].vnode);
#ifdef VIO_PUK_ABI
  puk_abi_vfd_free(vfd);
#endif
  vio.vios[vfd].vnode = NULL;
  marcel_mutex_unlock(&vio.lock);
}

padico_rc_t padico_vio_dump_vfd(int vfd)
{
  padico_rc_t rc = NULL;
  if(vfd >= 0 && vfd < PADICO_VIO_MAX_DESC)
    {
      padico_vnode_t vnode = vio.vios[vfd].vnode;
      if(vnode)
        {
          rc = padico_rc_msg("<VIO:vfd desc=\"%d\">\n<VIO:flags>%d</VIO:flags>",
                             vfd, vio.vios[vfd].desc_flag);
          rc = padico_rc_add(rc, padico_vio_dumpvnode(vnode));
          rc = padico_rc_add(rc, padico_rc_msg("</VIO:vfd>\n"));
        }
      else
        {
          rc = padico_rc_error("Bad file descriptor (%d)\n", vfd);
        }
    }
  else
    {
      rc = padico_rc_error("File descriptor out of bounds (%d)\n", vfd);
    }
  return rc;
}


/* ********************************************************* */

#ifdef VIO_PUK_ABI
static void vstdio_virtualize(int hfd, int mode)
{
  const puk_component_t component = puk_component_resolve(vio_default_vfile);
  padico_vnode_t vnode = padico_vnode_instantiate_entry(component, padico_module_self());
  vnode_hfd_create(vnode, hfd, mode);
  vnode->status.opened = 1;
  vio_store_desc(vnode, hfd);
}
#endif /* VIO_PUK_ABI */

void padico_vio_init(void)
{
  int i;
  marcel_mutex_init(&vio.lock, NULL);
  for(i = 0; i < PADICO_VIO_MAX_DESC; i++)
    {
      vio.vios[i].desc_flag = 0;
      vio.vios[i].vnode = NULL;
    }
#ifdef VIO_PUK_ABI
  /* initialize vio desc 0, 1 and 2 to stdin, stdout, stderr */
  vstdio_virtualize(0, O_RDONLY);
  vstdio_virtualize(1, O_WRONLY);
  vstdio_virtualize(2, O_WRONLY);
#endif
}


/* *** padico_vio_*: I/O virtualizers ********************** */

/* * generic vnode front-ends **************************** */

ssize_t padico_vio_read(int fd, void *buf, size_t count)
{
  ssize_t rc = 0;
  int err = 0;
  padico_vnode_t vnode = vfd_to_vnode(fd);
  if(!vnode)
    {
      padico_vio_set_errno(EBADF);
      return -1;
    }
  if(count == 0)
    {
      return 0;
    }
  vlink_profile_start(vlink_rprofiler);
  padico_out(40, "entering- vnode=%p; fd=%d; buf=%p; size=%ld; flags=%X (nonblock=%d)\n",
             vnode, fd, buf, puk_ulong(count), VIO_DATA(vnode).file_flag, !!(VIO_DATA(vnode).file_flag & O_NONBLOCK));
  vnode_lock(vnode);
  if((vnode->content.remote && vnode->remote.established && vnode->remote.can_recv) ||
     ((!vnode->content.remote) && vnode->status.opened))
    {
      VIO_DATA(vnode).interrupt_req = 0;
      vlink_profile_point(vlink_rprofiler, 1);
      rc = padico_vnode_read_post(vnode, buf, count);
      padico_out(60, "vnode=%p; read_post rc=%ld\n", vnode, puk_slong(rc));
      while(   /* test: not completed */
            ( (rc == 0) || (vnode->rbox.cant_interrupt_read) )
            && /* test: cancellation (connection lost, shutdown, or fd close) */
            ((vnode->content.remote && vnode->remote.can_recv) ||
             ((!vnode->content.remote) && vnode->status.opened))
            &&  /* test: EOF */
            (!vnode->status.end_of_file) )
        {
          padico_out(60, "vnode=%p fd=%d read_done=%ld\n", vnode, fd, puk_slong(vnode->rbox.read_done));
          if(VIO_DATA(vnode).file_flag & O_NONBLOCK)
            {
              padico_out(50, "vnode=%p O_NONBLOCK --don't wait, exit\n", vnode);
              err = EAGAIN;
              rc  = -1;
              break;
            }
          if(VIO_DATA(vnode).interrupt_req)
            {
              padico_out(50, "vnode=%p interrupted\n", vnode);
              err = EINTR;
              rc  = -1;
              VIO_DATA(vnode).interrupt_req = 0;
              break;
            }
          padico_out(40, "vnode=%p; waiting...\n", vnode);
          vlink_profile_point(vlink_rprofiler, 2);
          vobs_wait(vnode);
          vlink_profile_point(vlink_rprofiler, 10);
          rc = vnode->rbox.read_done;
          padico_out(40, "vnode=%p; unlocked- read_done=%ld\n", vnode, puk_slong(rc));
        }
      padico_vnode_read_clear(vnode);
      vlink_profile_point(vlink_rprofiler, 11);
    }
  else if (vnode->content.remote &&
           vnode->remote.established &&
           !vnode->remote.can_recv)
    {
      /* Connection has been closed -> post a 'read' to consume the remaining
       * data in the pipeline. Don't wait --no more data may arrive!
       * Trick: vnode_read_post() return rc=0 when no data is available.
       */
      rc = padico_vnode_read_post(vnode, buf, count);
      padico_out(30, "vnode=%p socket remotely closed- last rc=%ld\n", vnode, puk_slong(rc));
      padico_vnode_read_clear(vnode);
    }
  else
    {
      padico_out(20, "vnode=%p not connected- return rc=-1 errno=EBADF\n", vnode);
      rc = -1;
      err = EBADF;
    }
  vlink_profile_point(vlink_rprofiler, 12);
  vnode_unlock(vnode);
  if(rc == 0)
    {
      err = 0;
      assert(vnode->status.end_of_file || (vnode->remote.established && !vnode->remote.can_recv));
    }
  padico_out(40, "exit vnode=%p; fd=%d; buf=%p; size=%ld rc=%ld\n", vnode, fd, buf, puk_ulong(count), puk_slong(rc));
  vlink_profile_point(vlink_rprofiler, 14);
  padico_vio_set_errno(err);
  return rc;
}

ssize_t padico_vio_write(int fd, const void *buf, size_t count)
{
  ssize_t rc = 0;
  padico_vnode_t vnode = vfd_to_vnode(fd);
  int err = 0;
  if(!vnode)
    {
      padico_vio_set_errno(EBADF);
      return -1;
    }
  if((!vnode->content.remote) && (!vnode->status.opened))
    {
      padico_vio_set_errno(ECONNRESET);
      return -1;
    }
  if(count == 0)
    {
      return 0;
    }
  padico_out(50, "vnode=%p fd=%d buf=%p size=%ld flags=%d\n",
             vnode, fd, buf, puk_ulong(count), VIO_DATA(vnode).file_flag);
  if(VIO_DATA(vnode).file_flag & O_NONBLOCK)
    {
      padico_out(10, "trying to write in NONBLOCK mode- not implemented. fd=%d size=%ld\n",
                 fd, puk_ulong(count));
    }
  vnode_lock(vnode);
  vlink_profile_start(vlink_wprofiler);
  rc = padico_vnode_write_post(vnode, buf, count);
  while((rc < count) && (rc >= 0))
    {
      vobs_wait(vnode); /* wait for asynchronous send */
      rc = vnode->wbox.write_done;
    }
  if(vnode->content.wbox)
    {
      padico_vnode_write_clear(vnode);
    }
  if(rc == 0)
    {
      /* non-blocking */
      err = EAGAIN;
    }
  else if(rc == -1)
    {
      err = vnode_get_error(vnode);
    }
  vlink_profile_point(vlink_wprofiler, 14);
  vnode_unlock(vnode);
  padico_out(50, "vnode=%p write ok rc=%ld\n", vnode, puk_slong(rc));
  padico_vio_set_errno(err);
  return rc;
}

ssize_t padico_vio_readv(int fildes, const struct iovec *iov, int iovcnt)
{
  int i;
  ssize_t done = 0;
  padico_out(40, "entering- fd=%d; iovcnt=%d\n", fildes, iovcnt);
  for(i = 0; i < iovcnt; i++)
    {
      caddr_t current = iov[i].iov_base;
      ssize_t todo = iov[i].iov_len;
      while(todo > 0)
        {
          ssize_t rc = padico_vio_read(fildes, current, todo);
          int err = padico_vio_get_errno();
          if(rc < 0)
            {
              if(done && err == EAGAIN)
                {
                  return done;
                }
              else if(err != EINTR)
                {
                  padico_vio_set_errno(err);
                  return rc;
                }
            }
          todo -= rc;
          current += rc;
        }
      done += iov[i].iov_len;
    }
  padico_out(40, "exiting- fd=%d; iovcnt=%d\n", fildes, iovcnt);
  return done;
}

ssize_t padico_vio_writev(int fildes, const struct iovec *iov, int iovcnt)
{
  int i;
  ssize_t done = 0;
  for(i = 0; i < iovcnt; i++)
    {
      caddr_t current = iov[i].iov_base;
      ssize_t todo = iov[i].iov_len;
      while(todo > 0)
        {
          ssize_t rc = padico_vio_write(fildes, current, todo);
          int err = padico_vio_get_errno();
          if(rc < 0)
            {
              if(done && err == EAGAIN)
                {
                  return done;
                }
              else if(err != EINTR)
                {
                  padico_vio_set_errno(err);
                  return rc;
                }
            }
          todo -= rc;
          current += rc;
        }
      done += iov[i].iov_len;
    }
  return done;
}

int padico_vio_close(int fd)
{
  int rc = -1;
  padico_vnode_t vnode = vfd_to_vnode(fd);
  if(!vnode)
    {
      padico_vio_set_errno(EBADF);
      return -1;
    }
  padico_out(40, "vnode=%p fd=%d\n", vnode, fd);
  vnode_lock(vnode);
  rc = padico_vnode_close(vnode);
  vio_release_desc(fd);
  if(!vobs_secondary_ref_vect_empty(&vnode->observer.secondary))
    {
      vobs_secondary_ref_vect_itor_t s;
      puk_vect_foreach(s, vobs_secondary_ref, &vnode->observer.secondary)
        {
          marcel_mutex_lock(&s->obs->lock);
          s->obs->observers[s->index].vnode = NULL;
          marcel_cond_signal(&s->obs->waiting);
          marcel_mutex_unlock(&s->obs->lock);
        }
      vobs_secondary_ref_vect_destroy(&vnode->observer.secondary);
  }
  while(vnode->status.pending_op > 0)
    {
      padico_out(50, "vnode=%p; waiting %d pending operations to complete before destroy.\n",
                 vnode, vnode->status.pending_op);
      vobs_wait(vnode);
    }
  puk_instance_destroy(vnode->assembly_stuff.instance);
  padico_out(40, "vnode=%p fd=%d exiting\n", vnode, fd);
  return rc;
}

#ifdef HAVE_POLL_H
static void vobs_secondary_from_pollfd(struct vobs_secondary_s*obs, int nfds, struct pollfd fds[])
{
  int i;
  for(i = 0; i < nfds; i++)
    {
      const int fd = fds[i].fd;
      padico_vnode_t vnode = vio.vios[fd].vnode;
      if(!vnode)
        continue;
      vnode_lock(vnode);
      vnode = vio_pivot_vnode(vnode, fd);
      padico_out(60, "#%d; fds=%d; events=0x%X (r%d/w%d/e%d)\n", i, fds[i].fd, fds[i].events,
                       !!(fds[i].events & POLLIN), !!(fds[i].events & POLLOUT), !!(fds[i].events & POLLERR));
      obs->observers[obs->n_obs].oevents = fds[i].events;
      obs->observers[obs->n_obs].event = 0;
      if( (fds[i].events & POLLIN)
#ifdef POLLRDNORM
          || (fds[i].events & POLLRDNORM)
#endif
#ifdef POLLRDBAND
          || (fds[i].events & POLLRDBAND)
#endif
          )
        {
          obs->observers[obs->n_obs].event |= VNODE_EVENT_READ;
        }
      if( (fds[i].events & POLLOUT)
#ifdef POLLWRNORM
          || (fds[i].events & POLLWRNORM)
#endif
#ifdef POLLWRBAND
          || (fds[i].events & POLLWRBAND)
#endif
          )
        {
          obs->observers[obs->n_obs].event |= VNODE_EVENT_WRITE;
        }
      if(fds[i].events) /* always watch for errors when some other events is watched */
        obs->observers[obs->n_obs].event |= VNODE_EVENT_ERROR;
      obs->observers[obs->n_obs].vnode = vnode;
      struct vobs_secondary_ref_s ref = { .obs = obs, .index = obs->n_obs++ };
      vobs_secondary_ref_vect_push_back(&vnode->observer.secondary, ref);
      vnode_unlock(vnode);
    }
}

static int vnode_pollfd_from_observer(struct vobs_secondary_s*obs, int nfds, struct pollfd fds[])
{
  int n_ready = 0;
  int i;
  for(i = 0; i < nfds; i++)
    {
      padico_vnode_t vnode = vio.vios[fds[i].fd].vnode;
      int status = 0;
      if(!vnode){
        fds[i].revents = POLLNVAL;
        n_ready++;
        continue;
      }
      vobs_secondary_ref_vect_itor_t s;
      puk_vect_foreach(s, vobs_secondary_ref, &vnode->observer.secondary)
        {
          if(s->obs == obs)
            {
              if(obs->observers[s->index].oevents == fds[i].events)
                {
                  status = obs->observers[s->index].revent;
                  break;
                }
            }
        }
      fds[i].revents = 0;
      if( (status & VNODE_EVENT_READ ) && (fds[i].events & POLLIN) )
        fds[i].revents |= POLLIN;
      if( (status & VNODE_EVENT_WRITE) && (fds[i].events & POLLOUT) )
        fds[i].revents |= POLLOUT;
      if(  status & VNODE_EVENT_ERROR)
        fds[i].revents |= POLLERR;
      if(fds[i].revents)
        n_ready++;
      padico_out(70, "  fd=%d; events=0x%X; revents=0x%X\n", fds[i].fd, fds[i].events, fds[i].revents);
    }
  return n_ready;
}

/* timeout in miliseconds */
int padico_vio_poll(struct pollfd fds[], unsigned int nfds, int timeout)
{
  int n_ready = 0;
  int expired = 0;
  int rc = -1;
  int err = 0;
  struct vobs_secondary_s obs;

  padico_out(40, "nfds=%d timeout=%dms\n", nfds, timeout);
  {
    int k;
    for(k = 0; k < nfds; k++)
      {
        padico_out(40, "   fd[%d].fd=%d; events=0x%x\n", k, fds[k].fd, fds[k].events);
      }
  }
  vobs_secondary_init(&obs, nfds);
  padico_out(70, "initializing observer.\n");
  vobs_secondary_from_pollfd(&obs, nfds, fds);
  padico_out(70, "observer ok.\n");
  vobs_secondary_lock(&obs);
  while((!expired) && !(n_ready > 0))
    {
      padico_out(70, "updating observer...\n");
      n_ready = vobs_secondary_update(&obs);
      padico_out(70, "update ok- n_ready=%d\n", n_ready);
      if(n_ready == 0)
        {
          if(timeout == -1)
            {
              /* ** unlimited blocking wait */
              vobs_secondary_wait(&obs);
            }
          else if(timeout == 0)
            {
              /* ** non-blocking- don't wait */
              expired = 1;
              marcel_yield();  /* defeat active polling loops */
            }
          else if(timeout > 0)
            {
              /* ** blocking with timeout */
              struct timespec abstime;
              struct timeval  nowtime;
              gettimeofday(&nowtime, NULL);
              abstime.tv_sec =
                nowtime.tv_sec + ((nowtime.tv_usec + timeout * 1000) / 1000000);
              abstime.tv_nsec = ((nowtime.tv_usec + timeout * 1000) % 1000000) * 1000 ;
              int secondary_rc = vobs_secondary_timedwait(&obs, &abstime);
              n_ready = vobs_secondary_update(&obs);
              expired = (secondary_rc == ETIMEDOUT && n_ready == 0);
            }
        }
    }
  vobs_secondary_unlock(&obs);
  assert(n_ready >= 0);
  padico_out(70, "updating from observer...\n");
  n_ready = vnode_pollfd_from_observer(&obs, nfds, fds);
  padico_out(70, "ok- n_ready=%d\n", n_ready);
  rc  = n_ready;
  err = 0;
  vobs_secondary_destroy(&obs);
  padico_out(40, "nfds=%u timeout=%dms exit rc=%d n_ready=%d expired=%d\n",
             nfds, timeout, rc, n_ready, expired);
  {
    int k;
    for(k = 0; k < nfds; k++)
      {
        padico_out(40, "   fd[%d].fd=%d; events=0x%x; revents=0x%x\n", k, fds[k].fd, fds[k].events, fds[k].revents);
      }
  }
  assert(rc <= (int)nfds);
  padico_vio_set_errno(err);
  return rc;
}
#endif /* HAVE_POLL_H */

static void vobs_secondary_from_fdsets(struct vobs_secondary_s*vng, int nfds,
                                       fd_set*readfds, fd_set*writefds, fd_set*errorfds)
{
  int i;
  for(i = 0; i < nfds; i++)
    {
      padico_vnode_t vnode = vio.vios[i].vnode;
      if( (readfds  && (FD_ISSET(i, readfds))) ||
          (writefds && (FD_ISSET(i, writefds))) ||
          (errorfds && (FD_ISSET(i, errorfds))))
        {
          vnode_lock(vnode);
          vnode = vio_pivot_vnode(vnode, i);
          vng->observers[vng->n_obs].event = 0;
          if(readfds && (FD_ISSET(i, readfds)) )
            vng->observers[vng->n_obs].event |= VNODE_EVENT_READ;
          if(writefds && (FD_ISSET(i, writefds)))
            vng->observers[vng->n_obs].event |= VNODE_EVENT_WRITE;
          vng->observers[vng->n_obs].vnode = vnode;
          struct vobs_secondary_ref_s ref = { .obs = vng, .index = vng->n_obs++ };
          vobs_secondary_ref_vect_push_back(&vnode->observer.secondary, ref);
          vnode_unlock(vnode);
        }
    }
}

static int fdsets_from_vobs_secondary(struct vobs_secondary_s*vng, int nfds,
                                      fd_set*readfds, fd_set*writefds, fd_set*errorfds)
{
  int n_ready = 0;
  int i;
  for(i = 0; i < nfds; i++)
    {
      int ready = 0;
      padico_vnode_t vnode = vio.vios[i].vnode;
      int status = 0;
      if(vnode == NULL)
        continue;
      vobs_secondary_ref_vect_itor_t s;
      puk_vect_foreach(s, vobs_secondary_ref, &vnode->observer.secondary)
        {
          if(s->obs == vng)
            {
              status = vng->observers[s->index].revent;
              break;
            }
        }
      if(readfds && (FD_ISSET(i, readfds)))
        {
          if(!(status & VNODE_EVENT_READ))
            {
              padico_out(70, "vnode=%p not ready for read- fd=%d\n", vnode, i);
              FD_CLR(i, readfds);
            }
          else
            {
              ready = 1;
              padico_out(50, "vnode=%p ready for read- fd=%d\n", vnode, i);
            }
        }
      if(writefds && (FD_ISSET(i, writefds)))
        {
          if(!(status & VNODE_EVENT_WRITE))
            { FD_CLR(i, writefds); }
          else
            {
              ready = 1;
              padico_out(50, "vnode=%p ready for write fd=%d\n", vnode, i);
            }
        }
      if(errorfds && (FD_ISSET(i, errorfds)))
        {
          if(!(vnode_is_fatal_error(vnode)))
            { FD_CLR(i, errorfds); }
          else
            {
              ready = 1;
              padico_out(50, "vnode=%p ready for error fd=%d\n", vnode, i);
            }
        }
      n_ready+=ready;
    }
  return n_ready;
}


int padico_vio_select(int nfds, fd_set*readfds,
                      fd_set*writefds, fd_set*errorfds,
                      struct timeval *timeout)
{
  int i;
  int n_ready = 0;
  int expired = 0;
  struct timeval nowtime;
  struct timespec abstime;
  struct vobs_secondary_s obs;

  padico_out(50, "entering nfds=%d timeout=%lds:%ldus readfds=%p writefds=%p errorfds=%p\n",
             nfds, timeout?timeout->tv_sec:-1, timeout?timeout->tv_usec:-1,
             readfds, writefds, errorfds);

  vobs_secondary_init(&obs, nfds);
  vobs_secondary_from_fdsets(&obs, nfds, readfds, writefds, errorfds);

  if(timeout && (timeout->tv_sec != 0 || timeout->tv_usec !=0 ))
    {
      gettimeofday(&nowtime, NULL);
      abstime.tv_sec  = nowtime.tv_sec + timeout->tv_sec +
        (nowtime.tv_usec + timeout->tv_usec)/1000000;
      abstime.tv_nsec = ((nowtime.tv_usec + timeout->tv_usec) % 1000000) * 1000;
    }

  vobs_secondary_lock(&obs);
  do {
    n_ready = vobs_secondary_update(&obs);
    if(n_ready == 0)
      {
        if(timeout == NULL)
          {
            /* ** unlimited blocking wait */
            padico_out(20, "waiting...\n");
            vobs_secondary_wait(&obs);
            padico_out(20, "unlocked from cond_wait()\n");
          }
        else if(timeout->tv_sec == 0 && timeout->tv_usec == 0)
          {
            /* ** non-blocking- don't wait */
            expired = 1;
            marcel_yield();  /* defeat active polling loops */
          }
        else
          {
            /* ** blocking with timeout */
            int wait_rc = vobs_secondary_timedwait(&obs, &abstime);
            if(wait_rc == ETIMEDOUT)
              {
                /* timed out- no matter if fd are ready, continue. */
                padico_out(50, "timed out\n");
                expired = 1;
              }
            else if(wait_rc == 0)
              {
                /* we have been signaled- do nothing, just one loop */
                padico_out(50, "unlocked from timedwait()\n");
              }
            else
              {
                padico_warning("error in cond_timedwait()\n");
              }
          }
      }
  } while((!expired) && !(n_ready > 0));
  vobs_secondary_unlock(&obs);

  /* update fd sets */
  n_ready = fdsets_from_vobs_secondary(&obs, nfds, readfds, writefds, errorfds);

  if(n_ready > 0)
    {
      for(i=0; i<nfds; i++)
        {
          if(readfds && FD_ISSET(i, readfds))
            padico_out(60, " exit status fd=%d read=%d\n",
                       i, FD_ISSET(i, readfds));
          if(writefds && FD_ISSET(i, writefds))
            padico_out(60, " exit status fd=%d write=%d\n",
                       i, FD_ISSET(i, writefds));
          if(errorfds && FD_ISSET(i, errorfds))
            padico_out(60, " exit status fd=%d error=%d\n",
                       i, FD_ISSET(i, errorfds));
        }
    }
  else if(n_ready == 0)
    {
      padico_vio_set_errno(ETIMEDOUT);
    }
  else
    {
      padico_vio_set_errno(EINVAL);
    }
  padico_out(30, "exiting. rc=%d\n", n_ready);
  vobs_secondary_destroy(&obs);
  return n_ready;
}

int padico_vio_fcntl(int fd, int cmd, void* arg)
{
  int rc = 0;
  padico_vnode_t vnode = vfd_to_vnode(fd);
  padico_out(10, "fd=%d; cmd=%d; arg=%p\n", fd, cmd, arg);
  if(!vnode)
    {
      padico_vio_set_errno(EBADF);
      return -1;
    }
  vnode_lock(vnode);
  switch(cmd)
    {
    case F_GETFD:
      padico_out(10, "fd=%d; cmd=GETFD\n", fd);
      rc = vio.vios[fd].desc_flag;
      break;
    case F_SETFD:
      padico_out(10, "fd=%d; cmd=SETFD\n", fd);
      vio.vios[fd].desc_flag = puk_ulong(arg);
      break;
    case F_GETFL:
      padico_out(10, "fd=%d; cmd=GETFL\n", fd);
      rc = VIO_DATA(vnode).file_flag;
      break;
    case F_SETFL:
      padico_out(10, "fd=%d; cmd=SETFL\n", fd);
      VIO_DATA(vnode).file_flag = puk_ulong(arg);
      break;

    case F_SETLK:
    case F_GETLK:
    case F_SETLKW:
#if F_GETLK64 != F_GETLK
    case F_GETLK64:
    case F_SETLK64:
    case F_SETLKW64:
#endif
      {
        const struct flock*l __attribute__((unused)) = arg;
        padico_out(10, "flock[1] type=%d; start=%d; len=%d\n", (int)l->l_type, (int)l->l_start, (int)l->l_len);
        rc = vnode_hfd_fcntl(vnode, cmd, arg);
        padico_vio_set_errno(vnode_get_error(vnode));
        padico_out(10, "flock[2] type=%d; start=%d; len=%d\n", (int)l->l_type, (int)l->l_start, (int)l->l_len);
      }
      break;

    default:
      padico_warning("fcntl()- vnode=%p cmd=%d not implemented\n", vnode, cmd);
      rc = -1;
      padico_vio_set_errno(ENOSYS); /* not implemented */
      break;
    }
  vnode_unlock(vnode);
  padico_out(10, "rc=%d\n", rc);
  return rc;
}

int padico_vio_fsync(int fd)
{
  int rc = 0;
  padico_vnode_t vnode = vfd_to_vnode(fd);
  if(!vnode)
    {
      padico_vio_set_errno(EBADF);
      return -1;
    }
  vnode_lock(vnode);
  struct puk_receptacle_VFile_s vfile;
  struct puk_receptacle_VFile64_s vfile64;
  puk_instance_indirect_VFile(vnode->assembly_stuff.instance, NULL, &vfile);
  puk_instance_indirect_VFile64(vnode->assembly_stuff.instance, NULL, &vfile64);
  if(vfile.driver && vfile.driver->fsync)
    {
      rc = (*vfile.driver->fsync)(vnode);
    }
  else if(vfile64.driver && vfile64.driver->fsync)
    {
      rc = (*vfile64.driver->fsync)(vnode);
    }
  else
    {
      padico_warning("fd=%d; cannot fsync!\n", fd);
      rc = -1;
      padico_vio_set_errno(EINVAL);
    }
  vnode_unlock(vnode);
  return rc;
}

/* *** socket-specific front-ends ************************** */

ssize_t padico_vio_send(int s, const void*msg, size_t len, int flags)
{
  ssize_t rc = -1;
  if(flags != 0)
    {
      padico_warning("padico_vio_send() flags=0x%x not supported.\n", flags);
    }
  rc = padico_vio_write(s, msg, len);
  return rc;
}

ssize_t padico_vio_recv(int s, void*msg, size_t len, int flags)
{
#ifdef MSG_WAITALL
  const int waitall = (flags & MSG_WAITALL);
#else
  const int waitall = 0;
#endif
  if((flags != 0) && (!waitall))
    {
      padico_warning("padico_vio_recv() flags=0x%x not supported.\n", flags);
    }
  ssize_t done = 0;
  while((waitall && (done < len)) || (done == 0))
    {
      ssize_t rc = padico_vio_read(s, msg + done, len - done);
      if(rc > 0)
        done += rc;
      else if(rc == 0)
        return done;
      else
        return rc;
    }
  return done;
}

ssize_t padico_vio_sendto(int s, const void*buf, size_t len, int flags,
                          const struct sockaddr *to, int  tolen)
{
  ssize_t rc = 0;
  padico_vnode_t vnode = vfd_to_vnode(s);
  if(!vnode)
    {
      padico_vio_set_errno(EBADF);
      return -1;
    }
  vnode_lock(vnode);
  rc = padico_vsock_sendto(vnode, buf, len, flags, to, tolen); /* TODO- sendto is synchronous only */
  vnode_unlock(vnode);
  return rc;
}

#if 0 /* obsolete */
ssize_t padico_vio_recvfrom_madio(int s, void *buf, size_t len, int flags,
                                  struct sockaddr *from, int *fromlen)
{
  ssize_t rc = -1;
  padico_vnode_t vnode = vfd_to_vnode(s);
  if(!vnode)
    {
      padico_vio_set_errno(EBADF);
      return -1;
    }
  padico_out(40, "vnode=%p fd=%d buf=%p size=%ld flags=%d\n",
             vnode, s, buf, puk_ulong(len), VIO_DATA(vnode).file_flag);

  vnode_lock(vnode);
  rc = padico_vsock_recvfrom(vnode, buf, len, flags, from, fromlen); /* TODO- recvfrom is synchronous only */
  vnode_unlock(vnode);
  return rc;
}
#endif /* obsolete */

ssize_t padico_vio_recvfrom(int s, void *buf, size_t len, int flags,
                            struct sockaddr *from, socklen_t *fromlen)
{
  ssize_t rc = -1;
  padico_vnode_t vnode = vfd_to_vnode(s);
  if(!vnode)
    {
      padico_vio_set_errno(EBADF);
      return -1;
    }
  padico_out(40, "vnode=%p fd=%d buf=%p size=%ld flags=%d\n",
             vnode, s, buf, puk_ulong(len), VIO_DATA(vnode).file_flag);
  padico_warning("recvfrom()- fd=%d; len=%ld\n", s, puk_ulong(len));
  rc = PUK_ABI_FSYS_REAL(recvfrom)(vnode->hfd.fd, buf, len, flags, from, fromlen);
  padico_warning("recvfrom()- fd=%d; rc=%ld\n", s, puk_slong(rc));


  padico_out(50, "vnode=%p exit fd=%d buf=%p size=%ld rc=%ld\n",
             vnode, s, buf, puk_ulong(len), puk_slong(rc));
  return rc;
}

ssize_t padico_vio_sendmsg(int s, const struct msghdr *msg, int flags)
{
  ssize_t rc = -1;
  padico_vnode_t vnode = vfd_to_vnode(s);
  if(!vnode)
    {
      padico_vio_set_errno(EBADF);
      return -1;
    }
  if(vnode->content.remote)
    {
      rc = padico_vio_writev(s, msg->msg_iov, msg->msg_iovlen);
    }
  else
    {
      int i;
      int total = 0;
      int copy = 0;
      void*ptr = NULL;
      for(i=0; i<msg->msg_iovlen;i++)
        {
          total += msg->msg_iov[i].iov_len;
        }
      ptr = padico_malloc(total);
      for(i=0; i<msg->msg_iovlen;i++)
        {
          memcpy(ptr+copy, msg->msg_iov[i].iov_base, msg->msg_iov[i].iov_len);
          copy += msg->msg_iov[i].iov_len;
        }
      rc = padico_vio_sendto(s, ptr, total, flags,
                             msg->msg_name, msg->msg_namelen);
      padico_free(ptr);
    }
  return rc;
}

ssize_t padico_vio_recvmsg(int s, struct msghdr *msg, int flags)
{
  ssize_t rc = -1;
  padico_vnode_t vnode = vfd_to_vnode(s);
  if(!vnode)
    {
      padico_vio_set_errno(EBADF);
      return -1;
    }
  if(vnode->content.remote)
    {
      rc = padico_vio_readv(s, msg->msg_iov, msg->msg_iovlen);
    }
  else
    {
      int i;
      int total = 0;
      int copy = 0;
      void*ptr = NULL;
      for(i = 0; i < msg->msg_iovlen; i++)
        {
          total += msg->msg_iov[i].iov_len;
        }
      ptr = padico_malloc(total);
      rc = padico_vio_recvfrom(s, ptr, total, flags, msg->msg_name, &msg->msg_namelen);
      for(i = 0; i < msg->msg_iovlen; i++)
        {
          caddr_t current = msg->msg_iov[i].iov_base;
          ssize_t todo = msg->msg_iov[i].iov_len;
          if(todo <= total-copy)
            {
              memcpy(ptr+copy, current, todo);
            }
          else if(copy >= total)
            {
              break;
            }
          else
            {
              memcpy(ptr+copy, current, total-copy);
            }
          copy += todo;
        }
      padico_free(ptr);
    }
  return rc;
}

/* *** socket connection management ************************ */

int padico_vio_socket(int domain, int type, int protocol)
{
  int rc = -1;
  const puk_component_t component = puk_component_resolve(vio_default_vsock);
  const puk_instance_t instance = puk_component_instantiate(component);
  struct puk_receptacle_VLink_s r;
  puk_instance_indirect_VLink(instance, NULL, &r);
  padico_vnode_t vnode = r._status;
  rc = padico_vsock_create(vnode, domain, type, protocol);
  if(rc)
    {
      padico_vio_set_errno(-rc);
      return -1;
    }
  rc = vio_alloc_desc(vnode);
  if(rc < 0)
    {
      padico_vio_set_errno(-rc);
      return -1;
    }
  padico_out(40, "vnode=%p fd=%d domain=%d; type=%d; protocol=%d\n", vnode, rc, domain, type, protocol);
  padico_vio_set_errno(0);
  return rc;
}


int padico_vio_bind(int s, const struct sockaddr*name, socklen_t namelen)
{
  int rc = -1;
  padico_vnode_t vnode = vfd_to_vnode(s);
  if(!vnode)
    {
      padico_vio_set_errno(EBADF);
      return -1;
    }
  padico_out(40, "vfd=%d vnode=%p name: family=%d, len=%d\n",
             s, vnode, name->sa_family, namelen);
  vnode_lock(vnode);
  rc = padico_vsock_bind(vnode, name, namelen);
  padico_vio_set_errno(vnode_get_error(vnode));
  vnode_unlock(vnode);
  padico_out(40, "vfd=%d; vnode=%p; rc=%d\n", s, vnode, rc);
  return rc;
}

int padico_vio_listen(int s, int n)
{
  static const double fudge_factor = 1.5; /* standard BSD scaling factor */
  int backlog = n*fudge_factor + 1;
  int err = 0;
  int rc = -1;
  padico_vnode_t vnode = vfd_to_vnode(s);
  if(!vnode)
    {
      padico_vio_set_errno(EBADF);
      return -1;
    }
  padico_out(50, "vnode=%p vfd=%d backlog=%d\n", vnode, s, backlog);
  vnode_lock(vnode);
  if(!vnode->content.socket)
    {
      err = ENOTSOCK;
      goto error;
    }
  rc = padico_vsock_passive_connect(vnode, backlog, NULL);
  err = vnode_get_error(vnode);
 error:
  vnode_unlock(vnode);
  padico_out(50, "fd=%d exit; vnode=%p rc=%d; err=%d (%s)\n", s, vnode, rc, err, strerror(err));
  padico_vio_set_errno(err);
  return rc;
}

int padico_vio_accept(int s, struct sockaddr*raddr, socklen_t*namelen)
{
  int err = 0;
  int rc = -1;
  int pending = 0;
  padico_vnode_t vnode = vfd_to_vnode(s);
  if(!vnode)
    {
      padico_vio_set_errno(EBADF);
      return -1;
    }
  vnode_lock(vnode);
  padico_out(40, "vnode=%p vfd=%d entering\n", vnode, s);
  if(!vnode->content.socket)
    {
      err = ENOTSOCK;
      goto error;
    }
  if(!vnode->content.binding || !vnode->content.listener)
    {
      err = EINVAL;
      goto error;
    }
  VIO_DATA(vnode).interrupt_req = 0;
  pending = padico_vnode_queue_size(vnode->listener.backlog);
  padico_out(60, "vnode=%p first sync- pending=%d\n", vnode, pending);
  while((!pending) && !(VIO_DATA(vnode).file_flag & O_NONBLOCK))
    {
      if(VIO_DATA(vnode).interrupt_req)
        {
          VIO_DATA(vnode).interrupt_req = 0;
          err = EINTR;
          goto error;
        }
      padico_out(60, "vnode=%p waiting...\n", vnode);
      vobs_wait(vnode);
      pending = padico_vnode_queue_size(vnode->listener.backlog);
      padico_out(60, "vnode=%p sync- pending=%d\n", vnode, pending);
    }
  if(!pending)
    {
      err = EWOULDBLOCK;
      goto error;
    }
  padico_vnode_t newsock = padico_vnode_queue_retrieve(vnode->listener.backlog);
  vnode_check(newsock, socket);
  if((!newsock->content.remote) || (!newsock->remote.established))
    {
      err = ECONNABORTED;
      goto error;
    }
  vlink_address_trace(newsock->remote.remote_addr, newsock->remote.remote_addrlen);
  if(raddr != NULL && namelen != NULL)
    {
      if(*namelen < newsock->remote.remote_addrlen)
        {
          err = EFAULT;
          goto error;
        }
      padico_out(60, "filling return address\n");
      *namelen = newsock->remote.remote_addrlen;
      memcpy(raddr, newsock->remote.remote_addr, *namelen);
    }
  rc = vio_alloc_desc(newsock);
  if(rc < 0)
    {
      err = -rc;
      rc = -1;
    }
  padico_out(50, "alloc fd=%d new_fd=%d\n", s, rc);
 error:
  vnode_unlock(vnode);
  padico_out(60, "fd=%d exit; rc=%d; errno=%d (%s)\n", s, rc, err, strerror(err));
  padico_vio_set_errno(err);
  return rc;
}

int padico_vio_connect(int s, const struct sockaddr*name, socklen_t namelen)
{
  int err = 0;
  int rc = -1;
  padico_vnode_t vnode = vfd_to_vnode(s);
  if(!vnode)
    {
      padico_vio_set_errno(EBADF);
      return -1;
    }

  padico_out(40, " vnode=%p fd=%d\n", vnode, s);
  vlink_address_trace(name, namelen);
  vnode_lock(vnode);
  if(!vnode->content.socket)
    {
      err = ENOTSOCK;
      goto error;
    }
  if(vnode->content.remote)
    {
      err = EISCONN;
      goto error;
    }
  if(!(VIO_DATA(vnode).file_flag & O_NONBLOCK))
    {
      /* ** Blocking connect */
      rc = padico_vsock_active_connect(vnode, name, namelen);
      vnode = vio_pivot_vnode(vnode, s);
      err = vnode_get_error(vnode);
      padico_out(60, "vnode=%p blocking connect posted; rc=%d; errno=%d\n", vnode, rc, err);
      while(rc == -1 && err == EINPROGRESS)
        {
          /* connect() has NOT completed connection- in progress. */
          padico_out(60, "vnode=%p waiting- rc=%d; errno=%d; remote=%d; established=%d\n",
                     vnode, rc, err, vnode->content.remote, vnode->remote.established);
          vobs_wait(vnode);
          rc  = (vnode->content.remote && vnode->remote.established)?0:-1;
          err = rc?vnode_get_error(vnode):0;
          padico_out(60, "vnode=%p unlocked- rc=%d; errno=%d; remote=%d; established=%d\n",
                     vnode, rc, err, vnode->content.remote, vnode->remote.established);
        }
    }
  else
    {
      padico_out(60, "vnode=%p; non-blocking connect- connecting=%d; remote=%d; established=%d\n",
                 vnode, vnode->status.connecting, vnode->content.remote,
                 (vnode->content.remote && vnode->remote.established));
      /* ** Non-blocking connect */
      if(!vnode->content.remote && !vnode->status.connecting)
        {
          /* we are NOT connecting -- initialize connect */
          rc = padico_vsock_active_connect(vnode, name, namelen);
          vnode = vio_pivot_vnode(vnode, s);
          err = vnode_get_error(vnode);
          if(err == 0)
            rc = 0;
          padico_out(50, "non-blocking connect vnode=%p posted rc=%d err=%d established=%d\n",
                     vnode, rc, err, vnode->remote.established);
          assert((err != 0) || (vnode->remote.established));
        }
      else if(vnode->content.remote && vnode->remote.established)
        {
          /* connect done. */
          rc = 0;
          err = 0;
          padico_out(50, "non-blocking connect vnode=%p ok.\n", vnode);
        }
      else if(vnode->content.remote && vnode->status.connecting)
        {
          /* connect already posted -- update status */
          rc = -1;
          err = EALREADY;
          padico_out(50, "non-blocking connect vnode=%p already posted.\n", vnode);
          goto error;
        }
      else
        {
          padico_fatal("unexpected case in non-blocking connect\n");
        }
    }
  err = vnode_get_error(vnode); /* transfer error code from vnode to fd level (ignored when rc != -1) */
 error:
  vnode_unlock(vnode);
  padico_out(50, "exiting vnode=%p; remote=%d; established=%d; rc=%d; errno=%d (%s)\n",
             vnode, vnode->content.remote, vnode->remote.established, rc, err, strerror(err));
  padico_vio_set_errno(err);
  return rc;
}

int padico_vio_shutdown(int s, int how)
{
  /* how = 0: close-read; 1: close-write; 2: close-both */
  int err = 0;
  int rc = -1;
  padico_vnode_t vnode = vfd_to_vnode(s);
  if(!vnode)
    {
      padico_vio_set_errno(EBADF);
      return -1;
    }
  padico_out(40, "vnode=%p fd=%d how=%d\n", vnode, s, how);
  vnode_lock(vnode);
  if(!vnode->content.socket)
    {
      err = ENOTSOCK;
      goto error;
    }
  if(!vnode->content.remote)
    {
      err = ENOTCONN;
      goto error;
    }
  switch(how)
    {
    case 0: /* close read */
      vnode->remote.can_recv = 0;
      rc = 0;
      break;
    case 1: /* close write */
      vnode->remote.can_send = 0;
      rc = 0;
      break;
    case 2: /* close read-write */
      padico_vsock_fullshutdown(vnode);
      vnode->remote.can_send = 0;
      vnode->remote.can_recv = 0;
      rc = 0;
      break;
    default:
      padico_warning("padico_vio_shutdown() vnode=%p- bad parameter how=%d\n", vnode, how);
      err = EINVAL;
      goto error;
      break;
    }
 error:
  vnode_unlock(vnode);
  padico_vio_set_errno(err);
  return rc;
}

/* *** socket misc options ********************************* */

int padico_vio_setsockopt(int s, int level, int optname, const void*optval, socklen_t optlen)
{
  int err = 0;
  int rc = 0;
  padico_vnode_t vnode = vfd_to_vnode(s);
  padico_out(60, "s=%d; level=%d; optname=%d\n", s, level, optname);
  if(!vnode)
    {
      err = EBADF;
      rc = -1;
      goto error;
    }
  if(!vnode->content.socket)
    {
      err = ENOTSOCK;
      rc = -1;
      goto error;
    }
  switch(level)
    {
    case SOL_SOCKET:

      switch(optname)
        {
        case SO_DEBUG:
          break;
        case SO_REUSEADDR:
          /* @todo forward REUSEADDR to the hfd vnode */
          if(*(int*)optval)
            vnode->socket.so_options |= SO_REUSEADDR;
          else
            vnode->socket.so_options &= ~SO_REUSEADDR;
          vobs_commit(vnode, vobs_attr_socket);
          break;
        case SO_SNDBUF:
          padico_out(10, "SO_SNDBUF=%d\n", *(int*)optval);
          break;
        case SO_RCVBUF:
          padico_out(10, "SO_RECVBUF=%d\n", *(int*)optval);
          break;
        case SO_LINGER:
          padico_out(6, "ignore SO_LINGER\n");
          break;
        case SO_KEEPALIVE:
          padico_out(6, "ignore SO_KEEPALIVE\n");
          break;
        default:
          padico_warning("setsockopt() non-implemented option %d for SOL_SOCKET\n", optname);
          err = ENOSYS;
          rc = -1;
          break;
        }
      break;

    case SOL_TCP:
      switch(optname)
        {
        case TCP_NODELAY:
          if(vnode->content.hfd)
            {
              rc  = PUK_ABI_FSYS_WRAP(setsockopt)(vnode->hfd.fd, level, optname, optval, optlen);
              err = __puk_abi_wrap__errno;
            }
          else
            {
              padico_warning("setsockopt() option %d for SOL_TCP ignored for non-SysIO socket.\n", optname);
              rc = 0;
              err = 0;
              /*
                rc  = -1;
                err = ENOTSUP;
              */
            }
          break;

        default:
          padico_warning("setsockopt() non-implemented option %d for SOL_TCP- ignored\n", optname);
          rc = 0;
          err = 0;
        }
      break;

    default:
      padico_warning("setsockopt() non-implemented level %d\n", level);
      err = ENOSYS;
      rc = -1;
      break;
    }
 error:
  padico_vio_set_errno(err);
  return rc;
}

int padico_vio_getsockopt(int s, int level, int optname, void *optval, socklen_t *optlen)
{
  int err = 0;
  int rc = -1;
  padico_vnode_t vnode = vfd_to_vnode(s);
  if(!vnode)
    {
      padico_vio_set_errno(EBADF);
      return -1;
    }
  padico_out(40, "fd=%d; level=%d; option=%d\n", s, level, optname);
  if(!vnode->content.socket)
    {
      err = ENOTSOCK;
      goto error;
    }
  switch(optname)
    {
    case SO_TYPE:
      rc = 0;
      *(int*)optval = vnode->socket.so_type;
      break;
    case SO_ERROR:
      rc = 0;
      *(int*)optval = vnode_get_error(vnode);
      vnode_clear_error(vnode);
      break;
#ifdef SO_ACCEPTCONN
    case SO_ACCEPTCONN:
      rc = 0;
      *(int*)optval = vnode->content.listener;
      break;
#endif
    default:
      err = ENOSYS;
      break;
    }
 error:
  padico_vio_set_errno(err);
  return rc;
}

int padico_vio_getsockname(int s, struct sockaddr*name, socklen_t*namelen)
{
  int rc = -1;
  int err = 0;
  padico_vnode_t vnode = vfd_to_vnode(s);
  padico_out(60, "s=%d; vnode=%p\n", s, vnode);
  if(!vnode)
    {
      padico_out(20, "s=%d bad file descriptor\n", s);
      padico_vio_set_errno(EBADF);
      return -1;
    }
  vnode_lock(vnode);
  if(!vnode->content.socket)
    {
      padico_out(20, "s=%d not a socket\n", s);
      err = ENOTSOCK;
      goto error;
    }
  if(!vnode->content.binding)
    {
      padico_warning("getsockname() called with unbound socket!\n");
      padico_rc_show(padico_vio_dumpvnode(vnode));
      err = EINVAL;
      goto error;
    }
  if(*namelen < vnode->binding.primary_len)
    {
      padico_warning("getsockname()- given address not large enough (given: %d; required=%d).\n",
                     *namelen, vnode->binding.primary_len);
      err = ENOMEM;
      goto error;
    }
  memcpy(name, vnode->binding.primary, vnode->binding.primary_len);
  *namelen = vnode->binding.primary_len;
  rc = 0;
  vlink_address_trace(name, *namelen);
 error:
  vnode_unlock(vnode);
  padico_vio_set_errno(err);
  return rc;
}

int padico_vio_getpeername(int s, struct sockaddr*name, socklen_t*namelen)
{
  int rc = -1;
  int err = 0;
  padico_vnode_t vnode = vfd_to_vnode(s);
  if(!vnode)
    {
      padico_vio_set_errno(EBADF);
      return -1;
    }
  vnode_lock(vnode);
  if(!(vnode->content.socket))
    {
      err = ENOTSOCK;
      goto error;
    }
  if(!(vnode->content.remote))
    {
      err = ENOTCONN;
      goto error;
    }
  if(*namelen < vnode->remote.remote_addrlen)
    {
      padico_warning("getpeername()- given address not large enough (given: %d; required=%d).\n",
                     *namelen, vnode->remote.remote_addrlen);
      err = ENOMEM;
      goto error;
    }
  memcpy(name, vnode->remote.remote_addr, vnode->remote.remote_addrlen);
  *namelen = vnode->remote.remote_addrlen;
  rc = 0;
 error:
  vnode_unlock(vnode);
  padico_vio_set_errno(err);
  return rc;
}

int padico_vio_ulimit(int cmd, ...)
{
  int rc = -1;
  int err = 0;
  switch(cmd)
    {
#ifndef UL_GDESLIM   /* some systems do not define UL_GDESLIM. */
#define UL_GDESLIM 4 /* On those systems, it is hardcoded: 4 */
#endif /* UL_GDESLIM */
    case UL_GDESLIM:
      rc = PADICO_VIO_MAX_DESC;
      break;
    default:
      err = ENOSYS;
      padico_warning("ulimit() cmd=%d ignored\n", cmd);
      break;
    }
  padico_vio_set_errno(err);
  return rc;
}

int padico_vio_ioctl(int fd, int request, unsigned long arg)
{
  int rc = -1;
  int err = 0;
  padico_vnode_t vnode = vfd_to_vnode(fd);
  padico_out(40, "fd=%d; req=%X; arg=%lX\n", fd, request, arg);
  if(!vnode)
    {
      padico_vio_set_errno(EBADF);
      return -1;
    }
  if(vnode->content.hfd)
    {
      rc  = PUK_ABI_FSYS_WRAP(ioctl)(vnode->hfd.fd, request, arg);
      err = __puk_abi_wrap__errno;
    }
  else
    {
      rc  = -1;
      err = ENOTSUP;
    }
  padico_out(40, "fd=%d; rc=%d\n", fd, rc);
  padico_vio_set_errno(err);
  return rc;
}



/* *** pipe functions ************************************** */

int padico_vio_pipe(int fildes[2])
{
  static const char vpipe_component_name[] = "VPipe_Shm";
  const puk_component_t vpipe_factory = puk_component_resolve(vpipe_component_name);
  const struct padico_vpipe_driver_s*vpipe_driver = puk_component_get_driver_VPipe(vpipe_factory, NULL);
  int err = 0;
  padico_vnode_t vnodes[2];
  int rc = (*vpipe_driver->pipe)(vnodes);
  if(!rc)
    {
      int fd = vio_alloc_desc(vnodes[0]);
      if(fd < 0)
        {
          err = -fd;
          rc = -1;
          goto error;
        }
      fildes[0] = fd;
      fd = vio_alloc_desc(vnodes[1]);
      if(fd < 0)
        {
          err = -fd;
          rc = -1;
          goto error;
        }
      fildes[1] = fd;
    }
 error:
  padico_vio_set_errno(err);
  return rc;
}

/* *** file specific functions ***************************** */

int padico_vio_open(const char*_path, int oflag, ...)
{
  int rc = -1;
  const char*sep = strstr(_path, "://");
  const char*path = _path;
  puk_component_t component = NULL;
  if(sep)
    {
      path = sep + 3;
      char*component_name = strndup(_path, sep - _path);
      padico_warning("open() using component=%s; path=%s\n", component_name, path);
      component = puk_component_resolve(component_name);
      padico_free(component_name);
    }
  else
    {
      component = puk_component_resolve(vio_default_vfile);
    }
  const puk_instance_t instance = puk_component_instantiate(component);
  struct puk_receptacle_VFile_s r;
  puk_instance_indirect_VFile(instance, NULL, &r);
  padico_vnode_t vnode = r._status;
  assert(r._status != NULL && r.driver->open != NULL);
  padico_out(40, "path=%s; flags=%X\n", path, oflag);
  if(oflag & O_CREAT)
    {
      va_list ap;
      va_start(ap, oflag);
      unsigned mode = va_arg(ap, unsigned);
      rc = (*r.driver->open)(r._status, path, oflag, mode);
      va_end(ap);
    }
  else
    {
      rc = (*r.driver->open)(r._status, path, oflag, 0700);
    }
  padico_out(40, "driver->open- rc=%d\n", rc);
  if(rc)
    {
      padico_out(40, "exit- error path=%s- rc=%d (%s)\n", path, -rc, strerror(-rc));
      padico_vnode_close(vnode);
      puk_instance_destroy(vnode->assembly_stuff.instance);
      padico_vio_set_errno(-rc);
      rc = -1;
      goto error;
    }
  else
    {
      rc = vio_alloc_desc(vnode);
      padico_out(40, "exit- success rc=%d\n", rc);
      if(rc < 0)
        {
          padico_vio_set_errno(-rc);
          rc = -1;
          goto error;
        }
    }
 error:
  return rc;
}

int padico_vio_creat(const char*path, mode_t mode)
{
  return padico_vio_open(path, O_CREAT|O_WRONLY|O_TRUNC, mode);
}

int padico_vio_open64(const char*path, int oflag, ...)
{
  const puk_component_t component = puk_component_resolve(vio_default_vfile64);
  const puk_instance_t instance = puk_component_instantiate(component);
  struct puk_receptacle_VFile64_s r;
  puk_instance_indirect_VFile64(instance, NULL, &r);
  padico_vnode_t vnode = r._status;
  assert(r._status != NULL && r.driver->open != NULL);
  int rc = -1;
  padico_out(10, "path=%s; flags=%X\n", path, oflag);
  if(oflag & O_CREAT)
    {
      va_list ap;
      va_start(ap, oflag);
      unsigned mode = va_arg(ap, unsigned);
      rc = (*r.driver->open)(r._status, path, oflag, mode);
      va_end(ap);
    }
  else
    {
      rc = (*r.driver->open)(r._status, path, oflag, 0700);
    }
  if(rc)
    {
      padico_out(10, "path=%s; error=%d\n", path, -rc);
      padico_vnode_close(vnode);
      puk_instance_destroy(vnode->assembly_stuff.instance);
      padico_vio_set_errno(-rc);
      return -1;
    }
  else
    {
      rc = vio_alloc_desc(vnode);
      if(rc < 0)
        {
          padico_vio_set_errno(-rc);
          return -1;
        }
      else
        {
          padico_out(10, "path=%s; new fd=%d\n", path, rc);
          return rc;
        }
    }
}

int padico_vio_creat64(const char*path, mode_t mode)
{
  return padico_vio_open64(path, O_CREAT|O_WRONLY|O_TRUNC, mode);
}

off_t padico_vio_lseek(int fd, off_t offset, int whence)
{
  padico_vnode_t vnode = vfd_to_vnode(fd);
  if(!vnode)
    {
      padico_vio_set_errno(EBADF);
      return -1;
    }
  vnode_lock(vnode);
  struct puk_receptacle_VFile_s r;
  puk_instance_indirect_VFile(vnode->assembly_stuff.instance, NULL, &r);
  off_t rc = (*r.driver->lseek)(r._status, offset, whence);
  int err  = vnode_get_error(vnode);
  vnode->status.end_of_file = 0;
  vnode_unlock(vnode);
  padico_vio_set_errno(err);
  return rc;
}

off64_t padico_vio_lseek64(int fd, off64_t offset, int whence)
{
  padico_vnode_t vnode = vfd_to_vnode(fd);
  padico_out(10, "fd=%d; offset=%llu; whence=%d\n", fd, (unsigned long long)offset, whence);
  if(!vnode)
    {
      padico_vio_set_errno(EBADF);
      return -1;
    }
  vnode_lock(vnode);
  struct puk_receptacle_VFile64_s r;
  puk_instance_indirect_VFile64(vnode->assembly_stuff.instance, NULL, &r);
  off64_t rc = (*r.driver->lseek)(r._status, offset, whence);
  int err    = vnode_get_error(vnode);
  vnode->status.end_of_file = 0;
  vnode_unlock(vnode);
  padico_out(10, "fd=%d; rc=%llu; err=%d\n", fd, (unsigned long long)rc, err);
  padico_vio_set_errno(err);
  return rc;
}

int padico_vio_ftruncate(int fd, off_t length)
{
  padico_vnode_t vnode = vfd_to_vnode(fd);
  padico_out(10, "fd=%d; len=%lu\n", fd, (unsigned long)length);
  if(!vnode)
    {
      padico_vio_set_errno(EBADF);
      return -1;
    }
  vnode_lock(vnode);
  struct puk_receptacle_VFile_s r;
  puk_instance_indirect_VFile(vnode->assembly_stuff.instance, NULL, &r);
  int rc = (*r.driver->ftruncate)(r._status, length);
  int err = vnode_get_error(vnode);
  vnode_unlock(vnode);
  padico_out(10, "fd=%d; len=%lu; rc=%d; err=%d\n", fd, (unsigned long)length, rc, err);
  padico_vio_set_errno(err);
  return rc;
}
int padico_vio_ftruncate64(int fd, off64_t length)
{
  padico_vnode_t vnode = vfd_to_vnode(fd);
  padico_out(10, "fd=%d; len=%llu\n", fd, (unsigned long long)length);
  if(!vnode)
    {
      padico_vio_set_errno(EBADF);
      return -1;
    }
  vnode_lock(vnode);
  struct puk_receptacle_VFile64_s r;
  puk_instance_indirect_VFile64(vnode->assembly_stuff.instance, NULL, &r);
  int rc  = (r.driver->ftruncate)(r._status, length);
  int err = vnode_get_error(vnode);
  vnode_unlock(vnode);
  padico_out(10, "fd=%d; len=%llu; rc=%d; err=%d\n", fd, (unsigned long long)length, rc, err);
  padico_vio_set_errno(err);
  return rc;
}

int padico_vio___fxstat(int version, int fd, struct stat *buf)
{
  padico_vnode_t vnode = vfd_to_vnode(fd);
  if(!vnode)
    {
      padico_vio_set_errno(EBADF);
      return -1;
    }
  vnode_lock(vnode);
  struct puk_receptacle_VFile_s r;
  puk_instance_indirect_VFile(vnode->assembly_stuff.instance, NULL, &r);
  int rc = (r.driver->fxstat)(version, vnode, buf);
  int err = vnode_get_error(vnode);
  vnode_unlock(vnode);
  padico_vio_set_errno(err);
  return rc;
}
int padico_vio___fxstat64(int version, int fd, struct stat64*buf)
{
  padico_vnode_t vnode = vfd_to_vnode(fd);
  if(!vnode)
    {
      padico_vio_set_errno(EBADF);
      return -1;
    }
  vnode_lock(vnode);
  struct puk_receptacle_VFile64_s r;
  puk_instance_indirect_VFile64(vnode->assembly_stuff.instance, NULL, &r);
  int rc = (r.driver->fxstat)(version, vnode, buf);
  int err = vnode_get_error(vnode);
  vnode_unlock(vnode);
  padico_vio_set_errno(err);
  return rc;
}

void *padico_vio_mmap(void *addr, size_t len, int prot, int flags, int fd, off_t off)
{
  void*rc = NULL;
  int err = 0;
  if(fd < 0)
    {
#ifdef MAP_ANON /* anonymous mapping not POSIX, but widely supported */
      if(flags & MAP_ANON)
        {
          rc  = PUK_ABI_FSYS_WRAP(mmap)(addr, len, prot, flags, -1, off);
          err = __puk_abi_wrap__errno;
        }
      else
        {
          rc = NULL;
          err = EBADF;
        }
#else /* MAP_ANON */
      padico_fatal("vio_mmap() tries anonymous mmap() [fd=%d] -- not supported.", fd);
#endif
    }
  else /* fd >= 0 */
    {
      padico_vnode_t vnode = vfd_to_vnode(fd);
      if(!vnode)
        {
          padico_vio_set_errno(EBADF);
          return NULL;
        }
      vnode_lock(vnode);
      struct puk_receptacle_VFile_s r = { .driver = NULL, ._status = NULL};
      puk_instance_indirect_VFile(vnode->assembly_stuff.instance, NULL, &r);
      if(r.driver == NULL)
        {
          struct puk_receptacle_VFile64_s r = { .driver = NULL, ._status = NULL};
          puk_instance_indirect_VFile64(vnode->assembly_stuff.instance, NULL, &r);
          rc = (r.driver->mmap)(addr, len, prot, flags, vnode, off);
        }
      else
        {
          rc = (r.driver->mmap)(addr, len, prot, flags, vnode, off);
        }
      err = vnode_get_error(vnode);
      vnode_unlock(vnode);
    }
  padico_vio_set_errno(err);
  return rc;
}

void *padico_vio_mmap64(void *addr, size_t len, int prot, int flags, int fd, off64_t off)
{
  void*rc = NULL;
  int err = 0;
  if(fd < 0)
    {
#ifdef MAP_ANON /* anonymous mapping not POSIX, but widely supported */
      if(flags & MAP_ANON)
        {
          rc  = PUK_ABI_FSYS_WRAP(mmap)(addr, len, prot, flags, fd, off); /* TODO: should be mmap64 */
          err = __puk_abi_wrap__errno;
          padico_vio_set_errno(err);
        }
      else
        {
          rc = NULL;
          padico_vio_set_errno(EBADF);
        }
#else /* MAP_ANON */
      padico_fatal("vio_mmap64() tries anonymous mmap64() [fd=%d] -- not supported.", fd);
#endif
    }
  else /* fd >= 0 */
    {
      padico_vnode_t vnode = vfd_to_vnode(fd);
      if(!vnode)
        {
          padico_vio_set_errno(EBADF);
          return NULL;
        }
      vnode_lock(vnode);
      struct puk_receptacle_VFile64_s r;
      puk_instance_indirect_VFile64(vnode->assembly_stuff.instance, NULL, &r);
      rc = (r.driver->mmap)(addr, len, prot, flags, vnode, off);
      err = vnode_get_error(vnode);
      vnode_unlock(vnode);
    }
  padico_vio_set_errno(err);
  return rc;
}

/* *** Linux 'inotify' API ********************************* */

#ifdef HAVE_INOTIFY_H
int padico_vio_inotify_init(void)
{
  static const char inotify_component[] = "VStdIO";
  int hfd = PUK_ABI_FSYS_WRAP(inotify_init)();
  int err = __puk_abi_wrap__errno;
  int fd = -1;
  if(hfd >= 0)
    {
      puk_component_t component = puk_component_resolve(inotify_component);
      puk_instance_t instance = puk_component_instantiate(component);
      struct puk_receptacle_VLink_s r;
      puk_instance_indirect_VLink(instance, NULL, &r);
      padico_vnode_t vnode = r._status;
      vnode_hfd_create(vnode, hfd, O_RDWR);
      vnode->status.opened = 1;
      fd = vio_alloc_desc(vnode);
    }
  padico_vio_set_errno(err);
  return fd;
}
int padico_vio_inotify_add_watch(int fd, const char*pathname, uint32_t mask)
{
  int rc = 0;
  int err = 0;
  padico_vnode_t vnode = vfd_to_vnode(fd);
  padico_out(10, "fd=%d; path=%s; mask=%ud\n", fd, pathname, (unsigned)mask);
  if(!vnode)
    {
      padico_vio_set_errno(EBADF);
      return -1;
    }
  vnode_lock(vnode);
  rc  = PUK_ABI_FSYS_WRAP(inotify_add_watch)(vnode->hfd.fd, pathname, mask);
  err = __puk_abi_wrap__errno;
  vnode_unlock(vnode);
  padico_vio_set_errno(err);
  return rc;
}
int padico_vio_inotify_rm_watch(int fd, uint32_t wd)
{
  int rc = 0;
  int err = 0;
  padico_vnode_t vnode = vfd_to_vnode(fd);
  padico_out(10, "fd=%d; wd=%ud\n", fd, (unsigned)wd);
  if(!vnode)
    {
      padico_vio_set_errno(EBADF);
      return -1;
    }
  vnode_lock(vnode);
  rc  = PUK_ABI_FSYS_WRAP(inotify_rm_watch)(vnode->hfd.fd, wd);
  err = __puk_abi_wrap__errno;
  vnode_unlock(vnode);
  padico_vio_set_errno(err);
  return rc;
}

#endif /* HAVE_INOTIFY_H */

/* *** PadicoTM-VIO specific extensions ******************** */

/** asynchronous interrupt */
int padico_vio_interrupt(int fd)
{
  padico_vnode_t vnode = vfd_to_vnode(fd);
  if(!vnode)
    {
      padico_vio_set_errno(EBADF);
      return -1;
    }
  vnode_lock(vnode);
  if(VIO_DATA(vnode).interrupt_req)
    {
      padico_warning("interrupt already requested on vnode=%p\n", vnode);
    }
  else
    {
      VIO_DATA(vnode).interrupt_req = 1;
    }
  vnode_unlock(vnode);
  return 0;
}

puk_component_t padico_vio_component(int fd)
{
  padico_vnode_t vnode = vfd_to_vnode(fd);
  if(!vnode)
    {
      padico_vio_set_errno(EBADF);
      return NULL;
    }
  puk_component_t component = puk_instance_get_root_container(vnode->assembly_stuff.instance)->component;
  return component;
}

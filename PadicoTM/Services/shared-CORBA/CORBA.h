/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief CORBA header- public API for application using PadicoTM-enabled CORBA
 * @ingroup CORBA
 */

#ifndef PADICO_CORBA_H
#define PADICO_CORBA_H

#include <myCORBA.h>

#if defined(__cplusplus)

class Padico
{
public:
  static CORBA::ORB_ptr the_orb;
  class ORB
  {
  public:
    typedef char*ObjectId;
#if 0
    typedef string ObjectId;
    typedef sequence <ObjectId> ObjectIdList;
    exception InvalidName {};
    // Dynamic Invocation related operations
    void create_list ( in long count, out NVList new_list );
    void create_operation_list ( in OperationDef oper, out NVList new_list );
    void get_default_context ( out Context ctx );
    void send_multiple_requests_oneway( in RequestSeq req );
    void send_multiple_requests_deferred( in RequestSeq req );
    boolean poll_next_response();
    void get_next_response(out Request req );
    // Service information operations
    boolean get_service_information ( in ServiceType service_type, out ServiceInformation service_information );
    ObjectIdList list_initial_services ();
    // Type code creation operations
    TypeCode create_struct_tc ( in RepositoryId id, in Identifier name, in StructMemberSeq members );
    TypeCode create_union_tc ( in RepositoryId id, in Identifier name, in TypeCode discriminator_type, in UnionMemberSeq members );
    TypeCode create_enum_tc ( in RepositoryId id, in Identifier name, in EnumMemberSeq members );
    TypeCode create_alias_tc ( in RepositoryId id, in Identifier name, in TypeCode original_type );
    TypeCode create_exception_tc ( in RepositoryId id, in Identifier name, in StructMemberSeq members );
    TypeCode create_interface_tc ( in RepositoryId id, in Identifier name );
    TypeCode create_string_tc ( in unsigned long bound );
    TypeCode create_wstring_tc ( in unsigned long bound );
    TypeCode create_fixed_tc ( in unsigned short digits, in short scale );
    TypeCode create_sequence_tc ( in unsigned long bound, in TypeCode element type );
    TypeCode create_recursive_sequence_tc (// deprecated
                                           in unsigned long bound, in unsigned long offset );
    TypeCode create_array_tc ( in unsigned long length, in TypeCode element_type );
    TypeCode create_value_tc ( in RepositoryId id, in Identifier name, in ValueModifier type_modifier, in TypeCode concrete_base, in ValueMembersSeq members );
    TypeCode create_value_box_tc ( in RepositoryId id, in Identifier name, in TypeCode boxed_type );
    TypeCode create_native_tc ( in RepositoryId id, in Identifier name );
    TypeCode create_recursive_tc( in RepositoryId id );
    TypeCode create_abstract_interface_tc( in RepositoryId id, in Identifier name );
    TypeCode create_local_interface_tc( in RepositoryId id, in Identifier name );
    // Policy related operations
    Policy create_policy( in PolicyType type, in any val ) raises (PolicyError);
    // Dynamic Any related operations deprecated and removed
    // from primary list of ORB operations
    // Value factory operations
    ValueFactory register_value_factory( in RepositoryId id, in ValueFactory factory );
    void unregister_value_factory(in RepositoryId id);
    ValueFactory lookup_value_factory(in RepositoryId id);
#endif
    // Thread related operations
    CORBA::Boolean work_pending( )
    {
      return 1;
    }
    void perform_work()
    {
    }
    void run()
    {
    }
    void shutdown(CORBA::Boolean wait_for_completion )
    {
    }
    void destroy()
    {
    }
    char* object_to_string(CORBA::Object_ptr obj)
    {
      return Padico::the_orb->object_to_string(obj);
    }
    CORBA::Object_ptr string_to_object(const char* str )
    {
      return Padico::the_orb->string_to_object(str);
    }
    // Initial reference operation
    CORBA::Object_ptr resolve_initial_references(const Padico::ORB::ObjectId identifier)
    {
      return Padico::the_orb->resolve_initial_references(identifier);
    }
 };
  typedef Padico::ORB* ORB_ptr;
  static  Padico::ORB_ptr ORB_init()
  {
    return new Padico::ORB();
  }
  static  Padico::ORB_ptr ORB_init(int& argc, char** argv,
                                   const char* orb_identifier="")
  {
    return new Padico::ORB();
  }
};


#else
#error "Padico::ORB is C++ only!"
#endif

#endif /* PADICO_CORBA_H */

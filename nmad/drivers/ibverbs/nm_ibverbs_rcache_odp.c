/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

/** @file memory registration using explicit On-Demand Paging with prefetch
 */

#include "nm_ibverbs.h"
#include "nm_ibverbs_rcache.h"
#include <stdint.h>

#include <Padico/Module.h>

PADICO_MODULE_BUILTIN(nm_ibverbs_rcache_odp, NULL, NULL, NULL);

#if defined(NMAD_IBVERBS_ODP)

static int  nm_ibverbs_odp_query(struct nm_ibverbs_context_s*p_ibverbs_context);
static void nm_ibverbs_odp_init(struct nm_ibverbs_context_s*p_ibverbs_context);
static void nm_ibverbs_odp_close(struct nm_ibverbs_context_s*p_ibverbs_context);
static void nm_ibverbs_odp_mem_reg(struct nm_ibverbs_context_s*p_ibverbs_context, const void*ptr, size_t len,
                                    struct ibv_mr**pp_mr, void**pp_handle);
static void nm_ibverbs_odp_mem_unreg(struct nm_ibverbs_context_s*p_ibverbs_context, const void*ptr, size_t len,
                                      struct ibv_mr*p_mr, void*p_handle);

const struct nm_ibverbs_reg_driver_s nm_ibverbs_reg_driver_odp =
  {
   .query     = &nm_ibverbs_odp_query,
   .init      = &nm_ibverbs_odp_init,
   .close     = &nm_ibverbs_odp_close,
   .mem_reg   = &nm_ibverbs_odp_mem_reg,
   .mem_unreg = &nm_ibverbs_odp_mem_unreg
  };


/* ********************************************************* */
/* ** Implicit On-Demand Paging with prefetch */

static int nm_ibverbs_odp_query(struct nm_ibverbs_context_s*p_ibverbs_context)
{
  /* check device capabilities */
  struct ibv_device_attr_ex attrx;
  int rc = ibv_query_device_ex(p_ibverbs_context->hca.context, NULL, &attrx);
  if(rc)
    {
      NM_WARN("couldn't query device for its features\n");
      return 0;
    }
  if(!(attrx.odp_caps.general_caps & IBV_ODP_SUPPORT_WRITE))
    {
      NM_DISPF("the device doesn't support ODP for write\n");
      return 0;
    }
  return 1;
}

static void nm_ibverbs_odp_init(struct nm_ibverbs_context_s*p_ibverbs_context)
{
  assert(nm_ibverbs_odp_query(p_ibverbs_context));
  padico_out(puk_verbose_notice, "using explicit ODP.\n");
}

static void nm_ibverbs_odp_close(struct nm_ibverbs_context_s*p_ibverbs_context)
{
}

static void nm_ibverbs_odp_mem_reg(struct nm_ibverbs_context_s*p_ibverbs_context, const void*ptr, size_t len,
                                   struct ibv_mr**pp_mr, void**pp_handle)
{
  struct ibv_mr*p_mr = ibv_reg_mr(p_ibverbs_context->hca.pd, NULL, SIZE_MAX,
                                  IBV_ACCESS_REMOTE_WRITE | IBV_ACCESS_LOCAL_WRITE | IBV_ACCESS_ON_DEMAND);
  /* prefetch */
  struct ibv_sge sg_list;
  sg_list.lkey = p_mr->lkey;
  sg_list.addr = (uintptr_t)ptr;
  sg_list.length = len;
  int rc = ibv_advise_mr(p_ibverbs_context->hca.pd, IBV_ADVISE_MR_ADVICE_PREFETCH_WRITE,
                          IB_UVERBS_ADVISE_MR_FLAG_FLUSH,
                          &sg_list, 1);
  if(rc)
    {
      NM_WARN("couldn't prefetch MR; error %d (%s). Continue anyway\n", rc, strerror(rc));
    }
  *pp_mr = p_mr;
}

static void nm_ibverbs_odp_mem_unreg(struct nm_ibverbs_context_s*p_ibverbs_context, const void*ptr, size_t len,
                                     struct ibv_mr*p_mr, void*p_handle)
{
  ibv_dereg_mr(p_mr);
}

#endif /* NMAD_IBVERBS_ODP */

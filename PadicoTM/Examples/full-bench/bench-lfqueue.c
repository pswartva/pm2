/* bench-lfqueue.c
 * benchmark for shared memory transfer with Puk lock-free queue
 */


#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>

#include "bench-common.h"
#include <Padico/Module.h>
#include <Padico/Puk.h>
#ifdef PUKABI
#include <Padico/Puk-ABI.h>
#endif

int bench_lfqueue_init(void);
int bench_lfqueue_run(int argc, char**argv);
void bench_lfqueue_finalize(void);

PADICO_MODULE_DECLARE(bench_lfqueue, bench_lfqueue_init, bench_lfqueue_run, bench_lfqueue_finalize);

static char shm_path[256] = { '\0' };

#define MAXNODES 64
#define BUFSIZE (64 * 1024)
#define BUFNUM (MAXNODES * 8)

PUK_LFQUEUE_TYPE(bench_buf, int, -1, BUFNUM * 2);

struct bench_node_s
{
  struct bench_buf_lfqueue_s recv_queue;
  volatile int ready;
};
struct seg_s
{
  struct bench_node_s nodes[MAXNODES];
  struct bench_buf_lfqueue_s free_queue;
  char buffers[BUFNUM * BUFSIZE];
};

static struct bench_node_s*self = NULL;
static struct bench_node_s*peer = NULL;
static struct seg_s*seg = NULL;

int bench_lfqueue_init(void)
{
  snprintf(shm_path, 256, "/dev/shm/bench-lfqueue-%s", padico_getenv("USER"));
  int fd = PUK_ABI_FSYS_WRAP(open)(shm_path, O_RDWR|O_CREAT, 00700);
  if(fd == -1)
    {
      padico_fatal("BENCH: shm_open failed.\n");
    }
  ftruncate(fd, sizeof(struct seg_s));
  seg = PUK_ABI_FSYS_WRAP(mmap)(NULL, sizeof(struct seg_s), PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
  if(seg == MAP_FAILED)
    {
      padico_fatal("BENCH: mmap failed (%s) fd = %d.\n", strerror(errno), fd);
    }
  const int self_rank = padico_na_rank();
  padico_topo_node_t peer_node = bench_common_param.is_server ? bench_common_param.client_node : bench_common_param.server_node;
  int peer_rank = -1;
  const char*session = padico_topo_node_getsession(padico_topo_getlocalnode());
  int i;
  for(i = 0; peer_rank == -1; i++)
    {
      padico_topo_node_t n = padico_topo_session_getnodebyrank(session, i);
      if(n == NULL)
        abort();
      if(peer_node == n)
        peer_rank = i;
    }
  assert(peer_rank != -1);
  self = &seg->nodes[self_rank];
  peer = &seg->nodes[peer_rank];

  if(self_rank == 0)
    {
      bench_buf_lfqueue_init(&seg->free_queue);
      int i;
      for(i = 0 ; i < BUFNUM; i++)
        {
          bench_buf_lfqueue_enqueue(&seg->free_queue, i);
        }
    }
  bench_buf_lfqueue_init(&self->recv_queue);
  padico_print("BENCH: self = %d; peer = %d\n", self_rank, peer_rank);
  self->ready = 1;
  while(!peer->ready)
    {
      __sync_synchronize();
      self->ready = 1;
    }

  if(self_rank == 0)
    {
      sleep(3);
      PUK_ABI_FSYS_WRAP(close)(fd);
      unlink(shm_path);
    }
  padico_print("BENCH: connected.\n");
  return 0;
}

void bench_lfqueue_finalize(void)
{
}

static inline void my_shm_send(const char*ptr, int size)
{
  int block = -1;
  if(size > 0)
    {
      while(block == -1)
        {
          block = bench_buf_lfqueue_dequeue(&seg->free_queue);
        }
      void*blockptr = &seg->buffers[block * BUFSIZE];
      memcpy(blockptr, ptr, size);
    }
  else
    {
      block = -2;
    }
  bench_buf_lfqueue_enqueue(&peer->recv_queue, block);
}

static inline void my_shm_recv(char*ptr, int size)
{
  int block = -1;
  while(block == -1)
    {
      block = bench_buf_lfqueue_dequeue(&self->recv_queue);
    }
  if(size > 0)
    {
      void*blockptr = &seg->buffers[block * BUFSIZE];
      memcpy(ptr, blockptr, size);
      bench_buf_lfqueue_enqueue(&seg->free_queue, block);
    }
}

static void lfqueue_server(char*buffer, size_t lenbuf)
{
  my_shm_send(buffer, lenbuf);
  my_shm_recv(buffer, lenbuf);
}

static void lfqueue_client(char*buffer, size_t lenbuf)
{
  my_shm_recv(buffer, lenbuf);
  my_shm_send(buffer, lenbuf);
}

static struct bench_desc_s lfqueue_bench =
  {
    "lfqueue",
    &lfqueue_server,
    &lfqueue_client
  };

int bench_lfqueue_run(int argc, char**argv)
{
  the_bench(&lfqueue_bench);
  return 0;
}

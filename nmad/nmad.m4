dnl -*- mode: Autoconf;-*-

dnl -- NewMadeleine
dnl -- Copyright (C) 2006-2024 (see AUTHORS file)
dnl --
dnl -- This program is free software; you can redistribute it and/or modify
dnl -- it under the terms of the GNU General Public License as published by
dnl -- the Free Software Foundation; either version 2 of the License, or (at
dnl -- your option) any later version.
dnl --
dnl -- This program is distributed in the hope that it will be useful, but
dnl -- WITHOUT ANY WARRANTY; without even the implied warranty of
dnl -- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
dnl -- General Public License for more details.


dnl -- Fortran
dnl --
AC_DEFUN([AC_NMAD_FORTRAN_TARGET],
[
  dnl -- Fortran target
  nmad_fortran_target=unknown
  AC_PROG_FC([], [90])
  AC_PROG_F77
  dnl -- TODO- check what to do if we have F77 and no FC
  if test "x${FC}" != x; then
    dnl - GNU Fortran compiler?
    AC_MSG_CHECKING([whether Fortran compiler is GNU Fortran (gfortran)])
    if $FC -v 2>&1 | grep -i -E 'gnu|gcc|gfortran' >/dev/null 2>&1; then
      nmad_fortran_target=gfortran
      AC_DEFINE([NMAD_FORTRAN_TARGET_GFORTRAN], 0, [GNU gfortran Fortran support])
      AC_MSG_RESULT(yes)
    else
      AC_MSG_RESULT(no)
    fi
    dnl - Intel Fortran compiler?
    AC_MSG_CHECKING([whether Fortran compiler is Intel Fortran (ifort)])
    if $FC -v 2>&1 | grep -i -E 'intel|ifort' >/dev/null 2>&1; then
      nmad_fortran_target=ifort
      AC_DEFINE([NMAD_FORTRAN_TARGET_IFORT], 0, [Intel ifort Fortran support])
      AC_MSG_RESULT(yes)
    else
      AC_MSG_RESULT(no)
    fi
    dnl - Cray Fortran compiler?
    AC_MSG_CHECKING([whether Fortran compiler is Cray Fortran (ftn)])
    if $FC -v 2>&1 | grep -i -E 'Cray' >/dev/null 2>&1; then
      nmad_fortran_target=cray
      AC_DEFINE([NMAD_FORTRAN_TARGET_CRAY], 0, [Cray Fortran support])
      AC_MSG_RESULT(yes)
    else
      AC_MSG_RESULT(no)
    fi
  else
    nmad_fortran_target=none
    AC_DEFINE([NMAD_FORTRAN_TARGET_NONE], 0, [no Fortran support])
  fi
  AC_MSG_CHECKING([for Fortran support type])
  if test x${nmad_fortran_target} = xunknown; then
    AC_MSG_ERROR([unrecognized compiler type for ${FC}.])
  fi
  AC_MSG_RESULT([${nmad_fortran_target}])
  AC_SUBST([nmad_fortran_target])
])

AC_DEFUN([AC_NMAD_FORTRAN_INTEGER],
[
  dnl -- Fortran INTERGER size
  AC_LANG_PUSH([Fortran])
  nm_fortran_sizeof_integer=0
  dnl - try GNU sizeof == 4 bytes
  AC_MSG_CHECKING([whether Fortran sizeof INTEGER == 4])
  AC_RUN_IFELSE([
      implicit none
      integer :: i
      if ( sizeof(i) == 4 ) then
         call exit(0)
      else
         call exit(1)
      end if
      end
  ],
     [
       AC_DEFINE([NM_FORTRAN_SIZEOF_INTEGER_4], 1, [sizeof INTEGER == 4])
       nm_fortran_sizeof_integer=4
       AC_MSG_RESULT([yes])
     ],
     [
       AC_MSG_RESULT([no])
     ])
  dnl - try GNU sizeof == 8 bytes
  AC_MSG_CHECKING([whether Fortran sizeof INTEGER == 8])
  AC_RUN_IFELSE([
      implicit none
      integer :: i
      if ( sizeof(i) == 8 ) then
         call exit(0)
      else
         call exit(1)
      end if
      end
  ],
     [
       AC_DEFINE([NM_FORTRAN_SIZEOF_INTEGER_8], 1, [sizeof INTEGER == 8])
       nm_fortran_sizeof_integer=8
       AC_MSG_RESULT([yes])
     ],
     [
       AC_MSG_RESULT([no])
     ])
  dnl - try Fortran 2008 storage_size == 32 bits
  AC_MSG_CHECKING([whether Fortran INTEGER storage size is 32 bits])
  AC_RUN_IFELSE([
      implicit none
      integer :: i
      if ( storage_size(i) == 32 ) then
         call exit(0)
      else
         call exit(1)
      end if
      end
  ],
     [
       AC_DEFINE([NM_FORTRAN_SIZEOF_INTEGER_4], 1, [sizeof INTEGER == 4])
       nm_fortran_sizeof_integer=4
       AC_MSG_RESULT([yes])
     ],
     [
       AC_MSG_RESULT([no])
     ])
  dnl - try Fortran 2008 storage_size == 64 bits
  AC_MSG_CHECKING([whether Fortran INTEGER storage size is 64 bits])
  AC_RUN_IFELSE([
      implicit none
      integer :: i
      if ( storage_size(i) == 64 ) then
         call exit(0)
      else
         call exit(1)
      end if
      end
  ],
     [
       AC_DEFINE([NM_FORTRAN_SIZEOF_INTEGER_8], 1, [sizeof INTEGER == 8])
       nm_fortran_sizeof_integer=8
       AC_MSG_RESULT([yes])
     ],
     [
       AC_MSG_RESULT([no])
     ])
    AC_MSG_CHECKING([for Fortran INTEGER size])
     if test ${nm_fortran_sizeof_integer} = 0; then
       dnl - fallback to 4 bytes if auto-detection failed
       AC_DEFINE([NM_FORTRAN_SIZEOF_INTEGER_4], 1, [sizeof INTEGER == 4])
       nm_fortran_sizeof_integer=4
       AC_MSG_RESULT([4 (default)])
     else
       AC_MSG_RESULT([${nm_fortran_sizeof_integer} (auto-detected)])
     fi
  AC_LANG_POP([Fortran])
])


dnl -- PadicoTM
dnl --
AC_DEFUN([AC_NMAD_PADICOTM],
         [
           AC_ARG_WITH([padicotm],
             [AS_HELP_STRING([--with-padicotm], [use PadicoTM as launcher @<:@default=yes@:>@] )] )
           if test "x${with_padicotm}" != "xno"; then
             with_padicotm=yes
             AC_DEFINE([NMAD_PADICOTM], [1], [use PadicoTM as launcher])
             PKG_CHECK_MODULES([PadicoTM], [ PadicoTM ])
             requires_padicotm="PadicoTM"
             STATIC_LIBS="${STATIC_LIBS} `${PKG_CONFIG:-pkg-config} --static --libs PadicoTM`"
             AC_MSG_CHECKING([whether PadicoTM needs pioman])
             if pkg-config PadicoTM --print-requires | grep -q -- '^pioman$'; then
               need_pioman=yes
             else
               need_pioman=no
             fi
             AC_MSG_RESULT([$need_pioman])
           else
             requires_padicotm=
           fi
           AC_SUBST([requires_padicotm])
           AC_SUBST([with_padicotm])
           AC_MSG_CHECKING([for PadicoTM root])
           padicotm_root="`pkg-config --variable=prefix PadicoTM`"
           if test ! -r ${padicotm_root}/bin/padico-launch; then
             AC_MSG_ERROR([cannot find PadicoTM in ${padicotm_root}])
           fi
           AC_MSG_RESULT([${padicotm_root}])
           AC_SUBST([padicotm_root])
         ])

dnl -- PIOMan
dnl --
AC_DEFUN([AC_NMAD_PIOMAN],
         [
           AC_ARG_WITH([pioman],
             [AS_HELP_STRING([--with-pioman], [use pioman I/O manager @<:@default=no@:>@] )])
           AC_MSG_CHECKING([for PIOMan])
           if test "x${need_pioman}" = "xyes"; then
             with_pioman=yes
           fi
           if test "x${with_pioman}" = "xyes"; then
             AC_MSG_RESULT([yes])
             PKG_CHECK_MODULES([pioman], [ pioman ])
             requires_pioman="pioman"
             STATIC_LIBS="${STATIC_LIBS} `${PKG_CONFIG:-pkg-config} --static --libs pioman`"
             AC_DEFINE([NMAD_PIOMAN], [1], [use Pioman progression engine])
           else
             AC_MSG_RESULT([no])
             requires_pioman=
           fi
           AC_SUBST([requires_pioman])
           AC_SUBST(with_pioman)
         ])

dnl -- hwloc
dnl --
AC_DEFUN([AC_NMAD_HWLOC],
         [
           AC_PADICO_CHECK_HWLOC
           AC_MSG_CHECKING([whether to use hwloc])
           if test "x${with_hwloc}" = "xyes"; then
             AC_MSG_RESULT([yes])
             AC_DEFINE([NMAD_HWLOC], [1], [use hwloc])
           else
             AC_MSG_RESULT([no])
           fi
         ])

dnl -- PukABI
dnl --
AC_DEFUN([AC_NMAD_PUKABI],
         [
           AC_ARG_WITH([pukabi],
             [AS_HELP_STRING([--with-pukabi], [use PukABI ABI interceptor @<:@default=no@:>@] )])
           AC_MSG_CHECKING([for PukABI])
           if test "x${with_pukabi}" = "xyes"; then
             AC_MSG_RESULT([yes])
             PKG_CHECK_MODULES([PukABI], [ PukABI ])
             requires_pukabi="PukABI"
             AC_DEFINE([NMAD_PUKABI], [1], [interface with PukABI interposer])
           else
             AC_MSG_RESULT([no])
           fi
           AC_SUBST([requires_pukabi])
           AC_SUBST(with_pukabi)
         ])

dnl -- MX
dnl --
AC_DEFUN([AC_NMAD_CHECK_MX],
         [
           if test "x$MX_DIR" = "x" ; then
             if test "x${MX_ROOT}" = "x"; then
               if test -r /opt/mx/include/myriexpress.h ; then
                 MX_DIR=/opt/mx
               fi
             else
               MX_DIR=${MX_ROOT}
             fi
           fi
           HAVE_MX=no
           with_mx=no
           AC_ARG_WITH([mx],
             [AS_HELP_STRING([--with-mx], [use Myrinet MX @<:@default=no@:>@] )])
           if test "x${with_mx}" != "xno" ; then
             save_CPPFLAGS="${CPPFLAGS}"
             CPPFLAGS="${CPPFLAGS} -I${MX_DIR}/include"
             AC_CHECK_HEADER(myriexpress.h, [ HAVE_MX=yes ], [ HAVE_MX=no ])
             CPPFLAGS="${save_CPPFLAGS}"
             if test ${HAVE_MX} = yes; then
               if test "x${MX_DIR}" != "x" -a "x${MX_DIR}" != "x/usr"; then
                 mx_CPPFLAGS="-I${MX_DIR}/include"
                 mx_LIBS="-Wl,-rpath,${MX_DIR}/lib -L${MX_DIR}/lib"
               fi
               mx_LIBS="${mx_LIBS} -lmyriexpress"
               AC_SUBST([mx_CPPFLAGS])
               AC_SUBST([mx_LIBS])
             elif test "x${with_mx}" != "xcheck"; then
               AC_MSG_FAILURE([--with-mx was given, but myrinetexpress.h is not found])
             fi
           fi
           AC_SUBST(HAVE_MX)
         ])

dnl -- DCFA
dnl --
AC_DEFUN([AC_NMAD_CHECK_DCFA],
         [
           HAVE_DCFA=no
           AC_ARG_WITH([dcfa],
             [AS_HELP_STRING([--with-dcfa], [use Direct Communication Facility for Accelerators @<:@default=check@:>@])],
             [], [ with_dcfa=check ])
           if test "x${with_dcfa}" != "xno" ; then
             save_CPPFLAGS="${CPPFLAGS}"
             if test "x${with_dcfa}" != "xyes"; then
               DCFA_DIR="${with_dcfa}"
               CPPFLAGS="${CPPFLAGS} -I${DCFA_DIR}/include"
             fi
             AC_CHECK_HEADER(dcfa.h, [ HAVE_DCFA=yes ], [ HAVE_DCFA=no  ])
             CPPFLAGS="${save_CPPFLAGS}"
             if test ${HAVE_DCFA} = yes; then
               if test "x${DCFA_DIR}" != "x" -a "x${DCFA_DIR}" != "x/usr"; then
                 dcfa_CPPFLAGS="-I${DCFA_DIR}/include"
                 dcfa_LIBS="-Wl,-rpath,${DCFA_DIR}/lib -L${DCFA_DIR}/lib"
               fi
               dcfa_LIBS="${dcfa_LIBS} -ldcfa"
               AC_SUBST([dcfa_CPPFLAGS])
               AC_SUBST([dcfa_LIBS])
             elif test "x${with_dcfa}" != "xcheck"; then
               AC_MSG_FAILURE([--with-dcfa was given, but dcfa.h is not found])
             fi
           fi
           AC_SUBST(HAVE_DCFA)
         ])

dnl -- CCI
dnl --
AC_DEFUN([AC_NMAD_CHECK_CCI],
         [
           HAVE_CCI=no
           AC_ARG_WITH([cci],
             [AS_HELP_STRING([--with-cci], [use Common Communication Interface (CCI) @<:@default=check@:>@] )],
             [], [ with_cci=check ])
           if test "x${with_cci}" != "xno" ; then
             save_CPPFLAGS="${CPPFLAGS}"
             if test "x${with_cci}" != "xyes"; then
               CCI_DIR="${with_cci}"
               CPPFLAGS="${CPPFLAGS} -I${CCI_DIR}/include"
             fi
             AC_CHECK_HEADER(cci.h, [ HAVE_CCI=yes ], [ HAVE_CCI=no  ])
             CPPFLAGS="${save_CPPFLAGS}"
             if test ${HAVE_CCI} = yes; then
               if test "x${CCI_DIR}" != "x" -a "x${CCI_DIR}" != "x/usr"; then
                 cci_CPPFLAGS="-I${CCI_DIR}/include"
                 cci_LIBS="-Wl,-rpath,${CCI_DIR}/lib -L${CCI_DIR}/lib"
               fi
               cci_LIBS="${cci_LIBS} -lcci"
               AC_SUBST([cci_CPPFLAGS])
               AC_SUBST([cci_LIBS])
             elif test "x${with_cci}" != "xcheck"; then
               AC_MSG_FAILURE([--with-cci was given, but cci.h is not found])
             fi
           fi
           AC_SUBST(HAVE_CCI)
         ])

dnl -- UCX
dnl --
AC_DEFUN([AC_NMAD_CHECK_UCX],
         [
           AC_REQUIRE([PKG_PROG_PKG_CONFIG])
           HAVE_UCX=no
           AC_ARG_WITH([ucx],
             [AS_HELP_STRING([--with-ucx], [use Unified Communication X Library (UCX) @<:@default=check@:>@] )],
             [], [ with_ucx=check ])
           if test "x${with_ucx}" != "xno" ; then
             PKG_CHECK_MODULES([ucx], [ ucx ], [HAVE_UCX=yes], [HAVE_UCX=no])
             AC_MSG_CHECKING([for UCX version >= 1.10])
             if pkg-config --atleast-version 1.10 ucx; then
               AC_MSG_RESULT([yes])
             else
               HAVE_UCX=no
               AC_MSG_RESULT([no])
             fi
           fi
           AC_MSG_CHECKING([whether we are building the UCX driver])
           if test x${HAVE_UCX} = xyes; then
             AC_SUBST([ucx_CFLAGS])
             AC_SUBST([ucx_LIBS])
             AC_MSG_RESULT([yes])
             AC_DEFINE(HAVE_UCX, [ 1 ], [whether UCX is present ])
             STATIC_LIBS="${STATIC_LIBS} `${PKG_CONFIG:-pkg-config} --libs --static ucx`"
           else
             AC_MSG_RESULT([no])
           fi
           AC_SUBST([HAVE_UCX])
         ])

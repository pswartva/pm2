/* bench-vmsplice.c
 * benchmark for vmsplice on system FIFO (from OS, not VIO)
 */

/* for vmsplice */
#define _GNU_SOURCE

#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "bench-common.h"
#include <Padico/Module.h>
#ifdef PUKABI
#include <Padico/Puk-ABI.h>
#endif

int bench_vmsplice_init(void);
int bench_vmsplice_run(int argc, char**argv)
void bench_vmsplice_finalize(void);

PADICO_MODULE_DECLARE(bench_vmsplice, bench_vmsplice_init, bench_vmsplice_run, bench_vmsplice_finalize);

static int fifo1_fd = -1, fifo2_fd = -1;
static char fifo1_path[256] = { '\0' };
static char fifo2_path[256] = { '\0' };
static int use_vmsplice = 0;

int bench_vmsplice_init(void)
{
  snprintf(fifo1_path, 256, "/tmp/bench-vmsplice1-%s", padico_getenv("USER"));
  snprintf(fifo2_path, 256, "/tmp/bench-vmsplice2-%s", padico_getenv("USER"));
  mkfifo(fifo1_path, 0600);
  mkfifo(fifo2_path, 0600);
  fifo1_fd = PUK_ABI_FSYS_WRAP(open)(fifo1_path, O_RDWR);
  fifo2_fd = PUK_ABI_FSYS_WRAP(open)(fifo2_path, O_RDWR);
  padico_print("BENCH: fifo = %d / %d\n", fifo1_fd, fifo2_fd);
  if(padico_getattr("VMSPLICE") != NULL)
    {
      use_vmsplice = 1;
      padico_print("BENCH: using vmsplice.\n");
    }
  else
    {
      padico_warning("BENCH: not using vmsplice. Set VMSPLICE attribute to activate.\n");
    }
  return 0;
}

void bench_vmsplice_finalize(void)
{
  PUK_ABI_FSYS_WRAP(close)(fifo1_fd);
  PUK_ABI_FSYS_WRAP(close)(fifo2_fd);
}


static ssize_t socket_in(int fd, char *buf, size_t nbyte)
{
  size_t done = 0;
  while(done < nbyte)
    {
      int rc = PUK_ABI_FSYS_WRAP(read)(fd, buf + done, nbyte - done);
      if(rc > 0)
        done += rc;
      else
        {
          padico_warning("BENCH: read()- nbyte=%d; rc=%d\n", (int)nbyte, (int)done);
          break;
        }
    }
  return done;
}

static ssize_t socket_out(int fd, char *buf, size_t nbyte)
{
  size_t done = 0;
  while(done < nbyte)
    {
      int rc;
      if(use_vmsplice)
        {
          const struct iovec v = { .iov_base = buf + done, .iov_len = nbyte - done };
          rc = vmsplice(fd, &v, 1, 0);
        }
      else
        {
          rc = PUK_ABI_FSYS_WRAP(write)(fd, buf + done, nbyte - done);
        }
      if(rc > 0)
        done += rc;
      else
        {
          padico_warning("BENCH: write()- nbyte=%d; rc=%d\n", (int)nbyte, (int)done);
          break;
        }
    }
  return done;
}

static void vmsplice_server(char*buffer, size_t lenbuf)
{
  socket_out(fifo1_fd, buffer, lenbuf);
  socket_in(fifo2_fd, buffer, lenbuf);
}

static void vmsplice_client(char*buffer, size_t lenbuf)
{
  socket_in(fifo1_fd, buffer, lenbuf);
  socket_out(fifo2_fd, buffer, lenbuf);
}

static struct bench_desc_s vmsplice_bench =
  {
    "vmsplice",
    &vmsplice_server,
    &vmsplice_client
  };

int bench_vmsplice_run(int argc, char**argv)
{
  the_bench(&vmsplice_bench);
  return 0;
}

/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


#include <stdint.h>
#include <sys/uio.h>
#include <assert.h>

#include <nm_private.h>

PADICO_MODULE_HOOK(nmad);

static void nm_pw_recv_post(struct nm_pkt_wrap_s*p_pw);
static void nm_pw_recv_poll(struct nm_pkt_wrap_s*p_pw);
static void nm_pw_recv_poll_any(struct nm_pkt_wrap_s*p_pw);
static void nm_pw_recv_wait_any(struct nm_pkt_wrap_s*p_pw);
static void nm_pw_recv_wait_one(struct nm_pkt_wrap_s*p_pw);

/** Post a pw for recv on a driver.
 * either p_gate or p_drv may be NULL, but not both at the same time.
 */
void nm_core_post_recv(struct nm_pkt_wrap_s*p_pw, nm_gate_t p_gate,
                       nm_trk_id_t trk_id, nm_drv_t p_drv)
{
  struct nm_core*p_core = (p_gate != NULL) ? p_gate->p_core : p_drv->p_core;
  nm_core_lock_assert(p_core);
  nm_pw_assign(p_pw, trk_id, p_drv, p_gate);
  if(p_gate)
    {
      struct nm_trk_s*p_trk = p_pw->p_trk;
      assert(p_trk != NULL);
      assert(p_trk->p_pw_recv == NULL);
      p_trk->p_pw_recv = p_pw;
    }
  else
    {
      p_pw->flags |= NM_PW_POLL_ANY;
    }
  p_pw->flags |= NM_PW_RECV;
#ifdef PIOMAN
    if( (p_pw->p_unpack != NULL) && (p_pw->p_unpack->flags & NM_REQ_FLAG_UNPACK_PREFETCHING) &&
        !nm_status_test(p_pw->p_unpack, NM_STATUS_UNPACK_PREFETCHED) )
    {
      /* prefetching was requested for this pw but not completed yet; latsk will be submitted in prefetch completion task */
    }
  else
    {
      nm_ltask_submit_pw_recv(p_pw);
    }
#else /* PIOMAN */
  /* store pw in list of pw to poll; recv_post will be done on the next polling round */
  nm_pkt_wrap_list_push_back(&p_pw->p_drv->p_core->pending_recv_list, p_pw);
#endif /* PIOMAN */
}

/** Make progress on an active incoming request.
 */
void nm_pw_recv_progress(struct nm_pkt_wrap_s*p_pw)
{
  const nm_pw_flag_t flags = p_pw->flags;
  assert(flags != NM_PW_FLAG_NONE);
  assert( !( (p_pw->p_unpack != NULL) &&
             (p_pw->p_unpack->flags & NM_REQ_FLAG_UNPACK_PREFETCHING) &&
             !nm_status_test(p_pw->p_unpack, NM_STATUS_UNPACK_PREFETCHED)  ));
  if(flags & NM_PW_POLL_ANY)
    {
      /* polling on any before posting */
      nm_pw_recv_poll_any(p_pw);
    }
  else if(flags & NM_PW_POSTED)
    {
      /* already posted- poll */
      nm_pw_recv_poll(p_pw);
    }
  else if(flags & NM_PW_RECV)
    {
      /* post recv */
      nm_pw_recv_post(p_pw);
    }
  else
    {
      NM_FATAL("pw stats inconsistency detected; flags = 0x%x\n", flags);
    }
}

/** Blocking wait on an active incoming request.
 */
void nm_pw_recv_wait(struct nm_pkt_wrap_s*p_pw)
{
  assert(p_pw->flags & NM_PW_RECV);
  assert(p_pw->flags & NM_PW_BLOCKING_CALL);
  if(p_pw->flags & NM_PW_POLL_ANY)
    {
      /* wait on recv_any */
      nm_pw_recv_wait_any(p_pw);
    }
  else if(p_pw->p_drv->props.capabilities.prefers_recv_wait)
    {
      /* wait on single recv */
      nm_pw_recv_wait_one(p_pw);
    }
  else
    {
      /* recv_any was successfull just before switch from polling to
       * blocking wait; continue busy polling so as not to break semantics */
      while(!(p_pw->flags & NM_PW_COMPLETED))
        {
          nm_pw_recv_poll(p_pw);
        }
    }

}

static void nm_pw_recv_poll_any(struct nm_pkt_wrap_s*p_pw)
{
  struct nm_core*p_core = p_pw->p_drv->p_core;
  nm_core_nolock_assert(p_core);
  assert(p_pw->p_trk == NULL);
  const struct nm_minidriver_iface_s*p_driver = p_pw->p_drv->driver;
  assert(p_driver->recv_probe_any != NULL);
  void*_status = NULL;
  int rc = (*p_driver->recv_probe_any)(p_pw->p_drv->minidriver_context, &_status);
  if(rc == NM_ESUCCESS)
    {
      assert(_status != NULL);
      p_pw->flags &= ~NM_PW_POLL_ANY;
      /* reverse resolution status -> gate */
      struct nm_trk_s*p_trk = nm_trk_hashtable_lookup(&p_core->trk_table, _status);
      assert(p_trk != NULL);
      assert(p_trk->trk_id == p_pw->trk_id);
      nm_pw_assign(p_pw, p_pw->trk_id, p_trk->p_drv, p_trk->p_gate);
      assert(p_trk->p_pw_recv == NULL);
      p_trk->p_pw_recv = p_pw;
      assert(p_pw->p_gate != NULL);
      nm_pw_recv_post(p_pw);
    }
  else if(rc == -NM_ECANCELED)
    {
      p_pw->flags |= NM_PW_CANCELED;
#ifndef PIOMAN
      nm_pkt_wrap_list_remove(&p_core->pending_recv_list, p_pw);
#endif /* !PIOMAN */
      nm_pw_completed_enqueue(p_core, p_pw);
    }
  else if(rc == -NM_ECLOSED)
    {
      p_pw->flags |= NM_PW_CLOSED;
#ifndef PIOMAN
      nm_pkt_wrap_list_remove(&p_core->pending_recv_list, p_pw);
#endif /* !PIOMAN */
      nm_pw_completed_enqueue(p_core, p_pw);
    }
  if(PUK_VG_RUNNING_ON_VALGRIND)
    sched_yield();
}

static void nm_pw_recv_wait_any(struct nm_pkt_wrap_s*p_pw)
{
  struct nm_core*p_core = p_pw->p_drv->p_core;
  nm_core_nolock_assert(p_core);
  assert(p_pw->flags & NM_PW_POLL_ANY);
  assert(p_pw->p_trk == NULL);
  const struct nm_minidriver_iface_s*p_driver = p_pw->p_drv->driver;
  assert(p_driver->recv_wait_any != NULL);
  void*_status = NULL;
  int rc = (*p_driver->recv_wait_any)(p_pw->p_drv->minidriver_context, &_status);
  if(rc == NM_ESUCCESS)
    {
      assert(_status != NULL);
      assert(p_pw->p_drv->p_pw_recv_any == p_pw);
      p_pw->flags &= ~NM_PW_POLL_ANY;
      /* reverse resolution status -> gate */
      struct nm_trk_s*p_trk = nm_trk_hashtable_lookup(&p_core->trk_table, _status);
      assert(p_trk != NULL);
      nm_pw_assign(p_pw, p_pw->trk_id, p_trk->p_drv, p_trk->p_gate);
      assert(p_trk->p_pw_recv == NULL);
      p_trk->p_pw_recv = p_pw;
      assert(p_pw->p_gate != NULL);
      nm_pw_recv_post(p_pw);
      while(!(p_pw->flags & NM_PW_COMPLETED))
        {
          nm_pw_recv_poll(p_pw);
        }
    }
  else if((rc == -NM_ECLOSED) || (rc == -NM_ECANCELED))
    {
      if(rc == -NM_ECLOSED)
        {
          p_pw->flags |= NM_PW_CLOSED;
        }
      if(rc == -NM_ECANCELED)
        {
          p_pw->flags |= NM_PW_CANCELED;
        }
#ifndef PIOMAN
      nm_pkt_wrap_list_remove(&p_core->pending_recv_list, p_pw);
#endif /* !PIOMAN */
      nm_pw_completed_enqueue(p_core, p_pw);
    }
  else
    {
      NM_WARN("nm_pw_recv_wait_any()- rc = %d\n", rc);
    }
}

/** Poll an active incoming request.
 */
static void nm_pw_recv_poll(struct nm_pkt_wrap_s*p_pw)
{
  int err = NM_ESUCCESS;
  struct nm_core*p_core = p_pw->p_drv->p_core;
  static struct timespec next_poll = { .tv_sec = 0, .tv_nsec = 0 };
  nm_core_nolock_assert(p_core);
  NM_TRACEF("polling inbound request: gate = %p, drv = %s, trk = %d\n",
            p_pw->p_gate, p_pw->p_drv->driver_id, p_pw->trk_id);
#ifdef NMAD_PROFILE
  puk_tick_t t;
  PUK_GET_TICK(t);
  const double tdiff = PUK_TIMING_DELAY(p_pw->start_transfer_time, t);
  static double last_warn = 0.0;
  if(p_core->enable_pwsend_timeout && (p_pw->p_trk->kind == nm_trk_large))
    {
      if(tdiff > NM_PW_TIMEOUT)
        {
          const double time_warn = PUK_TIMING_DELAY(p_core->time_orig, t);
          if(time_warn > last_warn + NM_PW_TIMEOUT_PERIOD)
            {
              int dest = -1;
              nm_launcher_get_dest(p_pw->p_gate, &dest);
              last_warn = time_warn;
              NM_WARN("pw = %p recv timeout; trk_id = %d; drv = %s; source = %d; t = %g usec.\n",
                      p_pw, p_pw->trk_id, p_pw->p_drv->driver_id, dest, tdiff);
              puk_debug_gdb_backtrace();
            }
        }
    }
#endif /* NMAD_PROFILE */

  if(p_pw->p_drv->props.capabilities.min_period > 0)
    {
      struct timespec t;
#ifdef CLOCK_MONOTONIC_RAW
      const clockid_t clock = CLOCK_MONOTONIC_RAW;
#else
      const clockid_t clock = CLOCK_MONOTONIC;
#endif
      clock_gettime(clock, &t);
      if(t.tv_sec < next_poll.tv_sec ||
         (t.tv_sec == next_poll.tv_sec && t.tv_nsec < next_poll.tv_nsec))
        return;
      t.tv_nsec += 100 * 1000;
      if(t.tv_nsec > 1000000000)
        {
          t.tv_nsec -= 1000000000;
          t.tv_sec += 1;
        }
      next_poll = t;
      NM_WARN("nm_pw_recv_poll()- non-zero min_period.\n");
    }
  assert(p_pw->p_trk != NULL);
  assert(p_pw->p_trk == &p_pw->p_gate->trks[p_pw->trk_id]);
  const struct puk_receptacle_NewMad_minidriver_s*r = &p_pw->p_trk->receptacle;
  if(p_pw->flags & NM_PW_BUF_RECV)
    {
      void*buf = NULL;
      nm_len_t len = NM_LEN_UNDEFINED;
      err = (*r->driver->recv_buf_poll)(r->_status, &buf, &len);
      if(err == NM_ESUCCESS)
        {
          assert(len != NM_LEN_UNDEFINED);
          assert(len <= p_pw->max_len);
          p_pw->v[0].iov_base = buf;
          p_pw->v[0].iov_len = len;
          p_pw->length = len;
        }
    }
  else
    {
      err = (*r->driver->recv_poll_one)(r->_status);
    }
#ifdef NMAD_DEBUG
  if((err == NM_ESUCCESS) && (p_pw->p_data == NULL) &&
     (p_pw->p_drv->driver->recv_iov_post == NULL) &&
     (p_pw->p_drv->driver->recv_buf_poll == NULL) &&
     (p_pw->p_drv->props.capabilities.supports_data))
    {
      struct nm_data_s*p_data = &p_pw->p_trk->rdata;
      assert(!nm_data_isnull(p_data));
      nm_data_null_build(p_data);
    }
#endif /* NMAD_DEBUG */
#ifdef NMAD_PROFILE
  if(err == NM_ESUCCESS)
    {
      if(p_core->enable_pwsend_timeout && (p_pw->p_trk->kind == nm_trk_large))
        {
          if(tdiff > NM_PW_TIMEOUT)
            {
              NM_WARN("pw = %p recv COMPLETED after timeout; trk_id = %d; drv = %s; t = %g usec.\n",
                      p_pw, p_pw->trk_id, p_pw->p_drv->driver_id, tdiff);
            }
        }
    }
#endif /* NMAD_PROFILE */

  if((err == NM_ESUCCESS) && (p_pw->flags & NM_PW_DYNAMIC_V0))
    {
      /* dynamic v0- data was received in an intermediate buffer */
      struct nm_req_s*p_unpack = p_pw->p_unpack;
      assert(p_unpack != NULL);
      nm_data_copy_to(&p_unpack->data, p_pw->chunk_offset, p_pw->length, p_pw->v[0].iov_base);
    }

  if((err == NM_ESUCCESS) || (err == -NM_ECLOSED) || (err == -NM_ECANCELED))
    {
      if(err == -NM_ECLOSED)
        p_pw->flags |= NM_PW_CLOSED;
      if(err == -NM_ECANCELED)
        p_pw->flags |= NM_PW_CANCELED;
#ifdef NMAD_PROFILE
      {
        /* no lock needed since we are in the drv ltask */
        struct nm_drv_s*p_drv = p_pw->p_trk->p_drv;
        p_drv->profiling.n_recv_packets++;
        p_drv->profiling.n_recv_bytes += p_pw->length;
      }
#endif /* NMAD_PROFILE */
#ifndef PIOMAN
      nm_pkt_wrap_list_remove(&p_core->pending_recv_list, p_pw);
#endif /* !PIOMAN */
      nm_pw_completed_enqueue(p_core, p_pw);
    }
  else if (err != -NM_EAGAIN)
    {
      NM_WARN("drv->poll_recv returned %d\n", err);
    }
  if(PUK_VG_RUNNING_ON_VALGRIND)
    sched_yield();
}

/** Wait for an active incoming request.
 */
static void nm_pw_recv_wait_one(struct nm_pkt_wrap_s*p_pw)
{
  struct nm_core*p_core = p_pw->p_drv->p_core;
  nm_core_nolock_assert(p_core);
  NM_TRACEF("waiting inbound request: gate = %p, drv = %s, trk = %d\n",
            p_pw->p_gate, p_pw->p_drv->driver_id, p_pw->trk_id);

  assert(p_pw->p_trk != NULL);
  assert(p_pw->p_trk == &p_pw->p_gate->trks[p_pw->trk_id]);
  const struct puk_receptacle_NewMad_minidriver_s*r = &p_pw->p_trk->receptacle;
  assert(!(p_pw->flags & NM_PW_BUF_RECV));
  int err = (*r->driver->recv_wait_one)(r->_status);

#ifdef NMAD_DEBUG
  if((err == NM_ESUCCESS) && (p_pw->p_data == NULL) &&
     (p_pw->p_drv->driver->recv_iov_post == NULL) &&
     (p_pw->p_drv->driver->recv_buf_poll == NULL) &&
     (p_pw->p_drv->props.capabilities.supports_data))
    {
      struct nm_data_s*p_data = &p_pw->p_trk->rdata;
      assert(!nm_data_isnull(p_data));
      nm_data_null_build(p_data);
    }
#endif /* NMAD_DEBUG */

  if((err == NM_ESUCCESS) && (p_pw->flags & NM_PW_DYNAMIC_V0))
    {
      /* dynamic v0- data was received in an intermediate buffer */
      struct nm_req_s*p_unpack = p_pw->p_unpack;
      assert(p_unpack != NULL);
      nm_data_copy_to(&p_unpack->data, p_pw->chunk_offset, p_pw->length, p_pw->v[0].iov_base);
    }

  if((err == NM_ESUCCESS) || (err == -NM_ECLOSED) || (err == -NM_ECANCELED))
    {
      if(err == -NM_ECLOSED)
        p_pw->flags |= NM_PW_CLOSED;
      if(err == -NM_ECANCELED)
        p_pw->flags |= NM_PW_CANCELED;
#ifdef NMAD_PROFILE
      {
        /* no lock needed since we are in the drv ltask */
        puk_tick_t t;
        PUK_GET_TICK(t);
        const double tdiff = PUK_TIMING_DELAY(p_pw->start_transfer_time, t);
        struct nm_drv_s*p_drv = p_pw->p_trk->p_drv;
        p_drv->profiling.n_recv_packets++;
        p_drv->profiling.n_recv_bytes += p_pw->length;
      }
#endif /* NMAD_PROFILE */
#ifndef PIOMAN
      nm_pkt_wrap_list_remove(&p_core->pending_recv_list, p_pw);
#endif /* !PIOMAN */
      nm_pw_completed_enqueue(p_core, p_pw);
    }
  else
    {
      NM_WARN("drv->wait_recv_one returned %d (%s)\n", err, nm_strerror(err));
    }
}


/** Actually post a recv request to the driver
 */
static void nm_pw_recv_post(struct nm_pkt_wrap_s*p_pw)
{
  struct nm_core*p_core = p_pw->p_drv->p_core;
  /* no lock needed; only this ltask is allowed to touch the pw */
  nm_core_nolock_assert(p_core);
  NM_TRACEF("p_pw = %p; p_pw->p_unpack = %p; p_unpack->p_prefetch_pw = %p\n",
            p_pw, p_pw->p_unpack, p_pw->p_unpack ? p_pw->p_unpack->unpack.p_prefetch_pw : NULL);
#ifdef NMAD_PROFILE
  if(p_core->enable_pwsend_timeout && (p_pw->p_trk->kind == nm_trk_large))
    {
      PUK_GET_TICK(p_pw->start_transfer_time);
    }
#endif /* NMAD_PROFILE */
  nm_trace_event(NM_TRACE_EVENT_PW_POST_RECV, p_pw, p_pw->p_gate, p_pw->p_drv, NM_CORE_TAG_NONE, NM_SEQ_NONE, p_pw->length);

  assert(p_pw->p_gate);
  assert(p_pw->p_trk == &p_pw->p_gate->trks[p_pw->trk_id]);

  const struct nm_minidriver_capabilities_s*p_capabilities = &p_pw->p_trk->p_drv->props.capabilities;
  struct puk_receptacle_NewMad_minidriver_s*r = &p_pw->p_trk->receptacle;

  if(p_pw->p_data)
    {
      /* pw contains nm_data */
      if(p_capabilities->supports_data)
        {
          assert(r->driver->recv_data_post != NULL);
          int rc = (*r->driver->recv_data_post)(r->_status, p_pw->p_data, p_pw->chunk_offset, p_pw->length);
          if(rc != NM_ESUCCESS)
            {
              NM_WARN("drv->recv_data_post() rc = %d (%s)\n", rc, nm_strerror(rc));
            }
        }
      else
        {
          const int density = p_pw->data_props.size / p_pw->data_props.blocks;
          if( p_pw->data_props.is_contig ||
              (p_capabilities->supports_iovec && ((p_capabilities->max_iovecs == 0) || (p_capabilities->max_iovecs > p_pw->data_props.blocks)) && (density > NM_LARGE_MIN_DENSITY)) )
            {
              /* convert from nm_data to iovec */
              if( (p_pw->p_unpack == NULL) ||
                  !nm_status_test(p_pw->p_unpack, NM_STATUS_UNPACK_PREFETCHED) )
                {
                  nm_pw_data_to_iovec(p_pw);
                }
            }
          else
            {
              if( (p_pw->p_unpack != NULL) &&
                  (p_pw->p_unpack->unpack.p_prefetch_pw != NULL) )
                {
                  /* rules for prefetching are likely to not have prefetched
                   * such pw, but cancel prefetching just in case */
                  nm_pw_recv_unfetch(p_pw);
                }
              void*buf = padico_malloc(p_pw->length);
              if(buf == NULL)
                {
                  NM_FATAL("out of memory for recv.\n");
                }
              struct iovec*v0 = nm_pw_grow_iovec(p_pw);
              v0->iov_base = buf;
              v0->iov_len = p_pw->length;
              p_pw->flags |= NM_PW_DYNAMIC_V0;
            }
          NM_TRACEF("p_pw = %p; p_pw->v = %p; chunk_offset = %lu\n",
                    p_pw, p_pw->v, p_pw->chunk_offset);
          int rc = (*r->driver->recv_iov_post)(r->_status, p_pw->v, p_pw->v_nb);
          if(rc != NM_ESUCCESS)
            {
              NM_WARN("drv->recv_iov_post() rc = %d (%s)\n", rc, nm_strerror(rc));
            }
        }
    }
  else
    {
      /* no p_data, content is iovec */
      if(p_pw->p_drv->props.capabilities.supports_buf_recv)
        {
          assert(r->driver->recv_buf_poll != NULL);
          p_pw->flags |= NM_PW_BUF_RECV;
        }
      else if(r->driver->recv_iov_post)
        {
          int rc = (*r->driver->recv_iov_post)(r->_status, p_pw->v, p_pw->v_nb);
          if(rc != NM_ESUCCESS)
            {
              NM_WARN("drv->recv_iov_post() rc = %d (%s)\n", rc, nm_strerror(rc));
            }
        }
      else
        {
          assert(r->driver->recv_data_post);
          struct nm_data_s*p_data = &p_pw->p_trk->rdata;
          assert(nm_data_isnull(p_data));
          nm_data_iov_set(p_data, (struct nm_data_iov_s){ .v = p_pw->v, .n = p_pw->v_nb });
          int rc = (*r->driver->recv_data_post)(r->_status, p_data, 0 /* chunk_offset */, p_pw->length);
          if(rc != NM_ESUCCESS)
            {
              NM_WARN("drv->recv_data_post() rc = %d (%s)\n", rc, nm_strerror(rc));
            }
        }
    }
  p_pw->flags |= NM_PW_POSTED;
  if(p_pw->p_trk->kind == nm_trk_large)
    {
      if(p_pw->p_trk->p_drv->props.capabilities.needs_rdv_data)
        {
          int rc = (*r->driver->get_rdv_data)(r->_status, &p_pw->rdv_data[0], NM_HEADER_RTR_DATA_SIZE);
          if(rc != NM_ESUCCESS)
            {
              NM_WARN("drv->get_rdv_data() rc = %d (%s)\n", rc, nm_strerror(rc));
            }
        }
      p_pw->core_task.kind = NM_CORE_TASK_RTR_SEND;
      p_pw->core_task.content.rtr_send.p_pw = p_pw;
      nm_core_nolock_assert(p_core);
      nm_core_task_enqueue(p_core, 0, &p_pw->core_task);
    }
  else
    {
      nm_pw_recv_poll(p_pw);
    }
}

/** prefetch a packet-wrapper on the recv side
 * mostly used to register memory in advance while the rdv is still in progress
 */
void nm_pw_recv_prefetch(struct nm_pkt_wrap_s*p_pw)
{
  struct nm_core*p_core = p_pw->p_gate->p_core;
  nm_core_nolock_assert(p_core);
  assert(p_core->enable_recv_prefetch);
  struct nm_trk_s*p_trk = &p_pw->p_gate->trks[NM_TRK_LARGE];
  struct nm_drv_s*p_drv = p_trk->p_drv;
  assert(p_drv->props.capabilities.supports_recv_prefetch);
  assert(!p_drv->props.capabilities.supports_data); /* prefetching not supported with nm_data yet */
#ifdef NMAD_PROFILE
  puk_tick_t t1, t2;
  PUK_GET_TICK(t1);
  nm_profile_inc(p_drv->profiling.n_recv_prefetch);
  nm_profile_add(p_drv->profiling.n_recv_prefetch_bytes, p_pw->length);
#endif /* NMAD_PROFILE */
  nm_pw_data_to_iovec(p_pw);
  NM_TRACEF("p_pw = %p; p_pw->v = %p; chunk_offset = %lu\n", p_pw, p_pw->v, p_pw->chunk_offset);
  struct puk_receptacle_NewMad_minidriver_s*r = &p_trk->receptacle;
  (*r->driver->recv_iov_prefetch)(p_drv->minidriver_context, p_pw->v, p_pw->v_nb);
#ifdef NMAD_PROFILE
  PUK_GET_TICK(t2);
  const double tdiff = PUK_TIMING_DELAY(t1, t2);
  p_drv->profiling.total_recv_prefetch_usecs += tdiff;
  p_drv->profiling.average_recv_prefetch_bw_Mbps = (double)p_drv->profiling.n_recv_prefetch_bytes / (double)p_drv->profiling.total_recv_prefetch_usecs;
#endif /* NMAD_PROFILE */
  /* update unpack status with PREFETCHED only in core tasks to avoid race condition */
  /* issue a completion task to trigger normal processing of recv */
  p_pw->core_task.kind = NM_CORE_TASK_COMPLETED_PREFETCH;
  p_pw->core_task.content.completed_prefetch.p_pw = p_pw;
  nm_core_task_enqueue(p_core, 0, &p_pw->core_task);
}

/** cancel the recv prefetch of a pw, because it eventually got accepted
 * on another track or was split.
 */
void nm_pw_recv_unfetch(struct nm_pkt_wrap_s*p_pw)
{
  struct nm_core*p_core = p_pw->p_gate->p_core;
  assert(p_core->enable_recv_prefetch);
  assert(p_pw->p_unpack);
  assert(p_pw->p_unpack->flags & NM_REQ_FLAG_UNPACK_PREFETCHING);
#ifdef PIOMAN
  /* disable ltask, then execute unfetch here */
  piom_ltask_cancel_immediate(&p_pw->ltask);
#endif /* PIOMAN */
  if(nm_status_test(p_pw->p_unpack, NM_STATUS_UNPACK_PREFETCHED))
    {
      /* unfetch only if ltask for prefetching was already scheduled */
      assert(p_pw->v_nb > 0);
      struct nm_trk_s*p_trk = &p_pw->p_gate->trks[NM_TRK_LARGE];
      nm_profile_inc(p_trk->p_drv->profiling.n_recv_unfetch);
      struct puk_receptacle_NewMad_minidriver_s*r = &p_trk->receptacle;
      (*r->driver->recv_iov_unfetch)(p_trk->p_drv->minidriver_context, p_pw->v, p_pw->v_nb);
    }
  nm_status_unset(p_pw->p_unpack, NM_STATUS_UNPACK_PREFETCHED);
  p_pw->p_unpack->flags &= ~NM_REQ_FLAG_UNPACK_PREFETCHING;
  p_pw->p_unpack->unpack.p_prefetch_pw = NULL;
  p_pw->v_nb = 0;
  p_pw->p_data = NULL;
}

void nm_drv_refill_recv(nm_drv_t p_drv, nm_gate_t p_gate)
{
  struct nm_core*p_core = p_drv->p_core;
  nm_core_lock_assert(p_core);
  if(!p_core->enable_schedopt)
    return;
  if(p_drv->props.capabilities.supports_recv_any)
    {
      /* recv any available- single pw with no gate */
      if(p_drv->p_pw_recv_any == NULL)
        {
          struct nm_pkt_wrap_s*p_pw = nm_pw_alloc_buffer(p_core);
          nm_core_post_recv(p_pw, NM_GATE_NONE, NM_TRK_SMALL, p_drv);
        }
    }
  else
    {
      /* no recv any- post a pw per gate */
      assert(p_gate != NULL);
      assert(p_gate->status == NM_GATE_STATUS_CONNECTED);
#ifdef NMAD_DEBUG
      struct nm_trk_s*p_trk = nm_gate_trk_get(p_gate, p_drv);
      assert(p_trk != NULL && !p_trk->p_pw_recv);
      assert(p_trk->kind == nm_trk_small);
#endif /* NMAD_DEBUG */
      struct nm_pkt_wrap_s*p_pw = nm_pw_alloc_buffer(p_core);
      nm_core_post_recv(p_pw, p_gate, NM_TRK_SMALL, p_drv);
    }
}

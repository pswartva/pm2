/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

/** @file example for user-defined data iterators for an array of strings
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>

#include <nm_launcher_interface.h>
#include <nm_sendrecv_interface.h>


/** content for the user-defined data iterator
 */
struct user_data_s
{
  char**array;   /**< pointer to the array */
  int num;       /**< number of strings in array */
  nm_len_t len;  /**< length of strings */
};

static void user_data_traversal(const void*_content, nm_data_apply_t apply, void*_context)  __attribute__((unused));

/** set of operations that define the user-defined data type */
static const struct nm_data_ops_s user_data_ops =
  {
    .p_traversal      = &user_data_traversal,
    .p_generator_init = NULL, /**< use default generator */
    .p_generator_next = NULL
  };
/** create the type */
NM_DATA_TYPE(user_data, struct user_data_s, &user_data_ops);

/** data iterator given by user: call (*apply)() on every chunk of data */
static void user_data_traversal(const void*_content, nm_data_apply_t apply, void*_context)
{
  const struct user_data_s*content = _content;
  int i;
  for(i = 0; i < content->num; i++)
    {
      (*apply)(content->array[i], content->len, _context);
    }
}

int main(int argc, char**argv)
{
  static const nm_tag_t tag = 0x01;

  /* init ring topology */
  nm_launcher_init(&argc, argv);
  nm_session_t p_session;
  nm_session_open(&p_session, "nm_sr_custom_data");
  int rank, size;
  nm_launcher_get_rank(&rank);
  nm_launcher_get_size(&size);
  nm_gate_t p_gate_next, p_gate_prev;
  nm_launcher_get_gate((rank + 1) % size, &p_gate_next);
  nm_launcher_get_gate((rank - 1 + size) % size, &p_gate_prev);

  /* init array, filled only on rank #0 */
  int num = 200;
  int len = 100;
  char**array = malloc(sizeof(char*) * num);
  int i;
  for(i = 0; i < num; i++)
    {
      array[i] = malloc(len);
      if(rank == 0)
        {
          int j;
          for(j = 0; j < len - 1; j++)
            {
              array[i][j] = 'a' + ((i + j) % 26);
            }
          array[i][len - 1] = '\0';
        }
    }

  /* create data descriptor */
  struct nm_data_s data;
  nm_data_user_data_set(&data, (struct user_data_s){ .array = array, .num = num, .len = len });

  if(rank == 0)
    {
      /* first link in the ring */
      nm_sr_request_t request;
      nm_sr_isend_data(p_session, p_gate_next, tag, &data, &request);
      nm_sr_swait(p_session, &request);
    }
  else if(rank == size - 1)
    {
      /* last link in the ring */
      nm_sr_request_t request;
      nm_sr_irecv_data(p_session, p_gate_prev, tag, &data, &request);
      nm_sr_rwait(p_session, &request);
      printf("received data on node %d:\n", rank);
      for(i = 0; i < num; i++)
        {
          printf("%s\n", array[i]);
        }
    }
  else
    {
      /* forward data */
      nm_sr_request_t request;
      nm_sr_irecv_data(p_session, p_gate_prev, tag, &data, &request);
      nm_sr_rwait(p_session, &request);
      nm_sr_isend_data(p_session, p_gate_next, tag, &data, &request);
      nm_sr_swait(p_session, &request);
    }

  for(i = 0; i < num; i++)
    {
      free(array[i]);
    }
  free(array);

  nm_session_close(p_session);
  nm_launcher_exit();

  return 0;
}

/*
 * Pioman: a Generic I/O Manager
 * Copyright (C) 2008-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#ifndef PIOM_SIMGRID_H
#define PIOM_SIMGRID_H

#ifndef PIOM_CONFIG_H
#  error "Cannot include this file directly. Please include <pioman.h>."
#endif /* PIOM_CONFIG_H */

#ifndef PIOMAN_SIMGRID
#  error "inconsistency detected: PIOMAN_SIMGRID not defined in piom_lock_simgrid.h"
#endif /* PIOMAN_SIMGRID */

#include <errno.h>
#include <simgrid/host.h>

/* ** base pthread types *********************************** */

#define piom_thread_t        sg_actor_t
#define piom_thread_attr_t   void*
#define PIOM_THREAD_NULL     ((sg_actor_t)(-1)) /**< empty piom_thread_t */
#define PIOM_THREAD_SELF     sg_actor_self()
#define PIOM_THREAD_INVALID  ((sg_actor_t)(0))  /**< invalid piom_thread_t, to detect memory corruption */

/* ** spinlocks for simgrid ******************************** */

struct piom_spinlock_s
{
  sg_mutex_t mutex;
#ifdef PIOMAN_DEBUG
  sg_actor_t owner;
#endif
};
typedef struct piom_spinlock_s piom_spinlock_t;

static inline void piom_spin_init(piom_spinlock_t*lock)
{
  lock->mutex = sg_mutex_init();
#ifdef PIOMAN_DEBUG
  lock->owner = NULL;
#endif
}

static inline void piom_spin_destroy(piom_spinlock_t*lock)
{
  sg_mutex_destroy(lock->mutex);
#ifdef PIOMAN_DEBUG
  assert(lock->owner == NULL);
#endif
}

static inline void piom_spin_lock(piom_spinlock_t*lock)
{
  sg_mutex_lock(lock->mutex);
#ifdef PIOMAN_DEBUG
  assert(lock->owner == NULL);
  lock->owner = sg_actor_self();
#endif
}

static inline void piom_spin_unlock(piom_spinlock_t*lock)
{
#ifdef PIOMAN_DEBUG
  assert(lock->owner == sg_actor_self());
  lock->owner = NULL;
#endif
  sg_mutex_unlock(lock->mutex);
}
static inline int piom_spin_trylock(piom_spinlock_t*lock)
{
  int rc = sg_mutex_try_lock(lock->mutex);
#ifdef PIOMAN_DEBUG
  if(rc)
    {
      assert(lock->owner == NULL);
      lock->owner = sg_actor_self();
    }
#endif
  return rc;
}

static inline void piom_spin_assert_locked(piom_spinlock_t*lock __attribute__((unused)))
{
}

static inline void piom_spin_assert_notlocked(piom_spinlock_t*lock __attribute__((unused)))
{
}

/* ** semaphores ******************************************* */

struct piom_sem_s
{
  sg_sem_t sem;
};
typedef struct piom_sem_s piom_sem_t;

static inline void piom_sem_P(piom_sem_t*sem)
{
  sg_sem_acquire(sem->sem);
}

static inline void piom_sem_V(piom_sem_t*sem)
{
  sg_sem_release(sem->sem);
}

static inline void piom_sem_init(piom_sem_t*sem, int initial)
{
  sem->sem = sg_sem_init(initial);
}

static inline void piom_sem_destroy(piom_sem_t*sem)
{
  sg_sem_destroy(sem->sem);
}

/* ** Thread creation and join ***************************** */

struct piom_simgrid_thread_run_s
{
  void*(*func)(void*);
  void*arg;
  char*name;
  char*argv[2];
};

static inline void piom_simgrid_thread_runner(int argc, char**argv)
{
  assert(argc == 1);
  void*p = NULL;
  sscanf(argv[0], "%p", &p);
  assert(p != NULL);
  struct piom_simgrid_thread_run_s*p_run = (struct piom_simgrid_thread_run_s*)p;
  (*p_run->func)(p_run->arg);
  padico_free(p_run->name);
  padico_free(p_run->argv[0]);
  padico_free(p_run);
}

static inline int piom_thread_create(piom_thread_t*thread, piom_thread_attr_t*attr,
                                     void*(*thread_func)(void*), void*arg)
{


  struct piom_simgrid_thread_run_s*p_run = (struct piom_simgrid_thread_run_s*)padico_malloc(sizeof(struct piom_simgrid_thread_run_s));
  padico_string_t s = padico_string_new();
  padico_string_printf(s, "%p", p_run);
  p_run->argv[0] = padico_strdup(padico_string_get(s));
  p_run->argv[1] = NULL;
  p_run->func = thread_func;
  p_run->arg = arg;
  padico_string_printf(s, "pioman_runner-%p", p_run);
  p_run->name = padico_strdup(padico_string_get(s));
  padico_string_delete(s);
  *thread = sg_actor_create(p_run->name, sg_host_self(), &piom_simgrid_thread_runner, 1, p_run->argv);
  return 0;
}

static inline int piom_thread_join(piom_thread_t thread)
{
  sg_actor_join(thread, -1);
  return 0;
}

#define piom_thread_yield          sg_actor_yield

/* ** Mutex ************************************************ */

struct piom_simgrid_mutex_s
{
  sg_mutex_t mutex;
};
typedef struct piom_simgrid_mutex_s piom_thread_mutex_t;

static inline void piom_thread_mutex_init(piom_thread_mutex_t*m, void*attr)
{
  m->mutex = sg_mutex_init();
}

static inline void piom_thread_mutex_destroy(piom_thread_mutex_t*m)
{
  sg_mutex_destroy(m->mutex);
}

static inline void piom_thread_mutex_lock(piom_thread_mutex_t*m)
{
  sg_mutex_lock(m->mutex);
}
static inline void piom_thread_mutex_unlock(piom_thread_mutex_t*m)
{
  sg_mutex_unlock(m->mutex);
}

/* ** Condition ******************************************** */

struct piom_simgrid_cond_s
{
  sg_cond_t cond;
};
typedef struct piom_simgrid_cond_s piom_thread_cond_t;


static inline int piom_thread_cond_init(piom_thread_cond_t*c, void*attr)
{
  c->cond = sg_cond_init();
  return 0;
}

static inline int piom_thread_cond_signal(piom_thread_cond_t*c)
{
  sg_cond_notify_one(c->cond);
  return 0;
}

static inline int piom_thread_cond_broadcast(piom_thread_cond_t*c)
{
  sg_cond_notify_all(c->cond);
  return 0;
}

static inline int piom_thread_cond_wait(piom_thread_cond_t*c, piom_thread_mutex_t*m)
{
  sg_cond_wait(c->cond, m->mutex);
  return 0;
}

static inline int piom_thread_cond_destroy(piom_thread_cond_t*c)
{
  sg_cond_destroy(c->cond);
  return 0;
}


#endif /* PIOM_PTHREAD_H */

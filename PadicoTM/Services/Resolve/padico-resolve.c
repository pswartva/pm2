/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <Padico/Puk.h>
#include <Padico/PadicoTM.h>
#include <Padico/Module.h>
#include <Padico/Topology.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

static int padico_resolve_init(void);

PADICO_MODULE_DECLARE(Resolve, padico_resolve_init, NULL, NULL);

static int isaddr(const char *hostname)
{
  if(hostname)
    {
      struct sockaddr_in inaddr;
      if (inet_pton(AF_INET, hostname, &inaddr)==1)
        return 1;
      struct sockaddr_in6 in6addr;
      if (inet_pton(AF_INET6, hostname, &in6addr)==1)
        return 1;
      return 0;
    }
  return -1;
}

static char* nametoipv4(const char * name)
{
  char* ipstr= padico_malloc(INET_ADDRSTRLEN);
  padico_topo_host_t h=padico_topo_host_resolvebyname(name);
  const struct in_addr* iphost = padico_topo_host_getaddr(h);
  if(iphost)
    inet_ntop(AF_INET, iphost, ipstr, INET_ADDRSTRLEN);
  else
    {
      padico_free(ipstr);
      ipstr = NULL;
    }
  return ipstr;
}

int padico_getaddrinfo(const char *node, const char *service,
                       const struct addrinfo *hints, struct addrinfo **res)
{
  int rc =  isaddr(node);
  if(rc == 0)
    {
      char* ipstr = nametoipv4(node);
      if(ipstr)
        {
          rc =  PUK_ABI_RESOLV_WRAP(getaddrinfo)(ipstr, service, hints, res);
          padico_free(ipstr);
          return rc;
        }
    }
  return  PUK_ABI_RESOLV_WRAP(getaddrinfo)(node, service, hints, res);
}

struct hostent *padico_gethostbyname(const char *name)
{
  int rc =  isaddr(name);
  if(rc == 0)
    {
      char* ipstr = nametoipv4(name);
      if(ipstr)
        {
          struct hostent* tmp = PUK_ABI_RESOLV_WRAP(gethostbyname)(ipstr);
          padico_free(ipstr);
          return tmp;
        }
    }
  return PUK_ABI_RESOLV_WRAP(gethostbyname)(name);
}

int padico_gethostbyname_r(const char *name,
                           struct hostent *ret, char *buf, size_t buflen,
                           struct hostent **result, int *h_errnop)
{
  int rc =  isaddr(name);
  if(rc == 0)
    {
      char* ipstr = nametoipv4(name);
      if(ipstr)
        {
          rc = PUK_ABI_RESOLV_WRAP(gethostbyname_r)(ipstr, ret, buf, buflen, result, h_errnop);
          padico_free(ipstr);
          return rc;
        }
    }
  return PUK_ABI_RESOLV_WRAP(gethostbyname_r)(name, ret, buf, buflen, result, h_errnop);
}

int padico_gethostbyname2_r(const char *name, int af,
                            struct hostent *ret, char *buf, size_t buflen,
                            struct hostent **result, int *h_errnop)
{
  int rc =  isaddr(name);
  if(rc == 0 && af == AF_INET)
    {
      char* ipstr = nametoipv4(name);
      if(ipstr)
        {
          rc = PUK_ABI_RESOLV_WRAP(gethostbyname2_r)(ipstr, af, ret, buf, buflen, result, h_errnop);
          padico_free(ipstr);
          return rc;
        }
    }
  return PUK_ABI_RESOLV_WRAP(gethostbyname2_r)(name, af, ret, buf, buflen, result, h_errnop);
}

static int padico_resolve_init(void)
{
#ifdef PUKABI_ENABLE_RESOLV
  puk_abi_set_virtual(getaddrinfo, &padico_getaddrinfo);
  puk_abi_set_virtual(gethostbyname, &padico_gethostbyname);
  puk_abi_set_virtual(gethostbyname_r, &padico_gethostbyname_r);
  puk_abi_set_virtual(gethostbyname2_r, &padico_gethostbyname2_r);
#endif /* PUKABI_ENABLE_RESOLV */

  return 0;
}

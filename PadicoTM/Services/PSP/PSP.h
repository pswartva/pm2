/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Padico Simple Packets API
 * @ingroup PSP
 */

#ifndef PADICO_NA_PSP_H
#define PADICO_NA_PSP_H

#include <Padico/Puk.h>
#include <Padico/Topology.h>
#include <Padico/PadicoTM.h>

PUK_MOD_AUTO_DEP(PSP, PADICO_LAST_MODULE_NAME);
#undef PADICO_LAST_MODULE_NAME
#define PADICO_LAST_MODULE_NAME PSP

/** @defgroup PSP API: PSP- Padico Simple Packets.
 * A simple low-level packet-based messaging API, for networks with
 * implicit connection.
 */


/* ********************************************************* */
/* *** Interface: PadicoSimplePackets */

/** @addtogroup PSP
 * @{
 */


/* ** Values and intervals for PSP channels */


/* implicit channels */
#define PADICO_PSP_TAG_NONE      ((uint32_t)0x00000000)  /**< uninitialized tag */
#define PADICO_PSP_CONTROL       ((uint32_t)0x01000000)  /**< Control/PSP */
#define PADICO_PSP_CIRCUIT       ((uint32_t)0x02000000)  /**< Circuit/PSP */
#define PADICO_PSP_VOPW          ((uint32_t)0x03000000)  /**< VLink_Packet */
#define PADICO_PSP_NEWMADICO     ((uint32_t)0x04000000)  /**< Newmadico */
#define PADICO_PSP_MINIDRIVER    ((uint32_t)0x05000000)  /**< Minidriver_PSP */
/* reserved values for system use */
#define PADICO_PSP_RESERVED      ((uint32_t)0xF0000000)
#define PADICO_PSP_CLOSE_CHANNEL ((uint32_t)0xF1000000)  /**< [ reserved for internal use ] */
#define PADICO_PSP_SYSTEM        ((uint32_t)0xF2000000)  /**< [ reserved for internal use ] */

#define PADICO_PSP_ADAPTER_MASK  ((uint32_t)0xFF000000)
#define PADICO_PSP_INSTANCE_MASK ((uint32_t)0x00FFFFFF)

/** build a PSP tag from an component tag and an instance tag
 */
static inline uint32_t padico_psp_tag_build(uint32_t component_tag, uint32_t instance_tag)
{
  assert(!(component_tag & PADICO_PSP_INSTANCE_MASK));
  assert(!(instance_tag & PADICO_PSP_ADAPTER_MASK));
  return (component_tag | instance_tag);
}
/** get the 'component' (static) part from a PSP tag
 */
static inline uint32_t padico_psp_tag_get_component(uint32_t tag)
{
  return (tag & PADICO_PSP_ADAPTER_MASK);
}
/** get the 'instance' (dynamic) part from a PSP tag
 */
static inline uint32_t padico_psp_tag_get_instance(uint32_t tag)
{
  return (tag & PADICO_PSP_INSTANCE_MASK);
}

/** the function to pump data in PSP callbacks
 */
typedef void (*padico_psp_pump_t)(void*token, void*bytes, size_t size);

/** A callback for PSP
 * @note the callback must call '*pump' with the same number and
 * size of packets as the sender called 'pack'.
 */
typedef void (*padico_psp_handler_t)(const void*header, padico_topo_node_t from, void*key,
                                     padico_psp_pump_t pump, void*token);

/** An outgoing connection object, supposed to be opaque for the user.
 */
typedef void*padico_psp_connection_t;

/** driver descriptor for the PSP interface.
 */
struct padico_psp_driver_s
{
  /** @brief init a new PSP instance
   * @param _instance the PSP instance status (driver-dependant)
   * @param tag       the tag identifying the PSP channel; statically allocated
   * @param label     the name of the channel; used for debugging and dynamically allocated PSPs
   * @param h_size    size of the header on the given channel; inout parameter: in=requested size; out=real size (out>=in)
   * @param handler   the function that will be called upon message reception
   * @param key       the parameter that will be given to the callback function
   */
  void(*init)(void*_instance, uint32_t tag, const char*label, size_t*h_size, padico_psp_handler_t handler, void*key);

  /** @brief listen for connections from given node. Binds the instance to the given remote node.
   */
  void (*listen)(void*_instance, padico_topo_node_t node);

  /** @brief connects a remote node through PSP
   * @param _instance the PSP instance status
   * @param node      the remote node to connect to
   * @note PSP implementations _must_ support multiple calls to this
   *       function with the same 'node' parameter.
   */
  void(*connect)(void*_instance, padico_topo_node_t node);

  /** @brief begin a new outgoing message
   * @param _instance  the PSP instance status
   * @param node       the recipient of the to-be-sent message
   * @param sbuf       the sending buffer for headers ('out' parameters)
   * @return           an opaque driver-dependant connection
   */
  padico_psp_connection_t (*new_message)(void*_instance, padico_topo_node_t node, void**sbuf);

  /** @brief add a new segment of data to an outgoing message
   * @param _instance  the PSP instance status
   * @param conn       the connection object of the currently built message
   * @param bytes      pointer to the data to send
   * @param size       size of the data to send
   */
  void(*pack)(void*_instance, padico_psp_connection_t conn, const char*bytes, size_t size);

  /** @brief terminates an outgoing message
   * @param _instance  the PSP instance status
   * @param conn       the connection object of the message
   */
  void(*end_message)(void*_instance, padico_psp_connection_t conn);
};

/** Attach the iface name to its driver */
PUK_IFACE_TYPE(PadicoSimplePackets, struct padico_psp_driver_s);

/* ** helper functions */

static inline void padico_psp_init(const struct puk_receptacle_PadicoSimplePackets_s*psp, uint32_t tag, const char*label,
                                   size_t*h_size, padico_psp_handler_t handler, void*key)
{
  (*psp->driver->init)(psp->_status, tag, label, h_size, handler, key);
}

static inline void padico_psp_listen(const struct puk_receptacle_PadicoSimplePackets_s*psp, padico_topo_node_t node)
{
  if(psp->driver->listen != NULL)
    (*psp->driver->listen)(psp->_status, node);
}

static inline void padico_psp_connect(const struct puk_receptacle_PadicoSimplePackets_s*psp, padico_topo_node_t node)
{
  if(psp->driver->connect != NULL)
    (*psp->driver->connect)(psp->_status, node);
}

static inline padico_psp_connection_t padico_psp_new_message(const struct puk_receptacle_PadicoSimplePackets_s*psp,
                                                             padico_topo_node_t node, void**sbuf)
{
  return (*psp->driver->new_message)(psp->_status, node, sbuf);
}

static inline void padico_psp_pack(const struct puk_receptacle_PadicoSimplePackets_s*psp,
                                   padico_psp_connection_t conn, const char*bytes, size_t size)
{
  (*psp->driver->pack)(psp->_status, conn, bytes, size);
}

static inline void padico_psp_end_message(const struct puk_receptacle_PadicoSimplePackets_s*psp, padico_psp_connection_t conn)
{
  (*psp->driver->end_message)(psp->_status, conn);
}


/* *********************************************************
 * *** PSP directory ***************************************
 *
 * This directory is used for PSP demultiplexing of incoming
 * messages. This is a toolbox to be used by components
 * exhibiting the PSP interface.
 */

/** a PSP slot, i.e. a virtual channel */
struct padico_psp_slot_s
{
  padico_psp_handler_t handler;
  void*key;
  uint32_t tag;
  size_t h_size;
  int ref_count;
};
typedef struct padico_psp_slot_s*padico_psp_slot_t;

/** PSP slot directory used for multiplexing.
 * Helper for driver implementing PSP interface. */
struct padico_psp_directory_s
{
  pmarcel_spinlock_t lock;
  puk_hashtable_t table;
  padico_psp_slot_t cache;
};

void padico_psp_directory_init(struct padico_psp_directory_s*d);

padico_psp_slot_t padico_psp_slot_insert(struct padico_psp_directory_s*d,
                                         padico_psp_handler_t handler, void*key,
                                         uint32_t tag, size_t h_size);

void padico_psp_slot_remove(struct padico_psp_directory_s*d, padico_psp_slot_t slot);

padico_psp_slot_t padico_psp_slot_lookup(struct padico_psp_directory_s*d, uint32_t tag);



/** @} */


#endif /* PADICO_NA_PSP_H */

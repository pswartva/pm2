/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


#include "nm_mpi_private.h"
#include "nm_mpi_private_syms.h"

#include <Padico/Puk.h>
#include <Padico/Module.h>
PADICO_MODULE_HOOK(MadMPI);


NM_MPI_HANDLE_TYPE(session, struct nm_mpi_session_s, _NM_MPI_SESSION_OFFSET, 8);
static struct
{
  int refcount;
  struct nm_mpi_handle_session_s sessions;
} nm_mpi_session = { .refcount = 0, .sessions = NM_MPI_HANDLE_NULL };

/* ********************************************************* */
/* Aliases */

NM_MPI_ALIAS(MPI_Session_init,              mpi_session_init);
NM_MPI_ALIAS(MPI_Session_finalize,          mpi_session_finalize);
NM_MPI_ALIAS(MPI_Session_create_errhandler, mpi_session_create_errhandler);
NM_MPI_ALIAS(MPI_Session_set_errhandler,    mpi_session_set_errhandler);
NM_MPI_ALIAS(MPI_Session_get_errhandler,    mpi_session_get_errhandler);
NM_MPI_ALIAS(MPI_Session_call_errhandler,   mpi_session_call_errhandler);
NM_MPI_ALIAS(MPI_Session_get_info,          mpi_session_get_info);
NM_MPI_ALIAS(MPI_Session_get_pset_info,     mpi_session_get_pset_info);
NM_MPI_ALIAS(MPI_Session_get_num_psets,     mpi_session_get_num_psets);
NM_MPI_ALIAS(MPI_Session_get_nth_pset,      mpi_session_get_nth_pset);
NM_MPI_ALIAS(MPI_Group_from_session_pset,   mpi_group_from_session_pset);

/* ********************************************************* */

__PUK_SYM_INTERNAL
void nm_mpi_session_lazy_init(void)
{
  const int count = nm_atomic_inc(&nm_mpi_session.refcount);
  if(count == 0)
    {
      nm_mpi_handle_session_init(&nm_mpi_session.sessions);
    }
}

__PUK_SYM_INTERNAL
void nm_mpi_session_lazy_exit(void)
{
  const int count = nm_atomic_dec(&nm_mpi_session.refcount);
  if(count == 0)
    {
      nm_mpi_handle_session_finalize(&nm_mpi_session.sessions, NULL);
    }
}

/* ********************************************************* */

int mpi_session_init(MPI_Info info, MPI_Errhandler errhandler, MPI_Session*session)
{
  struct nm_mpi_errhandler_s*p_errhandler = nm_mpi_errhandler_get(errhandler, NM_MPI_ERRHANDLER_SESSION);
  if(p_errhandler == NULL)
    return NM_MPI_ERROR_WORLD(MPI_ERR_ARG);
  /* we cannot have argc/argv at this point; only cmdline launcher actually uses argc/argv */
  nm_launcher_init(NULL, NULL); /* lazy init, with ref count */
  nm_mpi_session_lazy_init();
  nm_mpi_group_lazy_init();
  nm_mpi_datatype_lazy_init();
  nm_mpi_comm_lazy_init();
  nm_mpi_op_lazy_init();
  struct nm_mpi_session_s*p_session = nm_mpi_handle_session_alloc(&nm_mpi_session.sessions);
  p_session->p_errhandler = p_errhandler;
  if(info == MPI_INFO_NULL)
    {
      p_session->p_info = nm_mpi_info_alloc();
    }
  else
    {
      struct nm_mpi_info_s*p_info = nm_mpi_info_get(info);
      p_session->p_info = nm_mpi_info_dup(p_info);
    }
  *session = p_session->id;
  return MPI_SUCCESS;
}

int mpi_session_finalize(MPI_Session*session)
{
  struct nm_mpi_session_s*p_session = nm_mpi_handle_session_get(&nm_mpi_session.sessions, *session);
  NM_MPI_SESSION_CHECK(p_session);
  p_session->p_errhandler = NULL;
  nm_mpi_info_free(p_session->p_info);
  nm_mpi_handle_session_free(&nm_mpi_session.sessions, p_session);
  nm_mpi_op_lazy_exit();
  nm_mpi_comm_lazy_exit();
  nm_mpi_datatype_lazy_exit();
  nm_mpi_group_lazy_exit();
  nm_mpi_session_lazy_exit();
  /*  nm_launcher_exit(); */ /* nm_launcher cannot be restarted yet. Do not finalize to allow further sessions */
  *session = MPI_SESSION_NULL;
  return MPI_SUCCESS;
}

int mpi_session_create_errhandler(MPI_Session_errhandler_function*function, MPI_Errhandler*errhandler)
{
  struct nm_mpi_errhandler_s*p_errhandler = nm_mpi_errhandler_alloc(NM_MPI_ERRHANDLER_SESSION, function);
  *errhandler = p_errhandler->id;
  return MPI_SUCCESS;
}

int mpi_session_set_errhandler(MPI_Session session, MPI_Errhandler errhandler)
{
  struct nm_mpi_errhandler_s*p_errhandler = nm_mpi_errhandler_get(errhandler, NM_MPI_ERRHANDLER_SESSION);
  if(p_errhandler == NULL)
    return NM_MPI_ERROR_WORLD(MPI_ERR_ARG);
  struct nm_mpi_session_s*p_session = nm_mpi_handle_session_get(&nm_mpi_session.sessions, session);
  NM_MPI_SESSION_CHECK(p_session);
  p_session->p_errhandler = p_errhandler;
  return MPI_SUCCESS;
}

int mpi_session_get_errhandler(MPI_Session session, MPI_Errhandler*errhandler)
{
  struct nm_mpi_session_s*p_session = nm_mpi_handle_session_get(&nm_mpi_session.sessions, session);
  NM_MPI_SESSION_CHECK(p_session);
  *errhandler = p_session->p_errhandler->id;
  return MPI_SUCCESS;
}

int mpi_session_call_errhandler(MPI_Session session, int errorcode)
{
  struct nm_mpi_session_s*p_session = nm_mpi_handle_session_get(&nm_mpi_session.sessions, session);
  NM_MPI_SESSION_CHECK(p_session);
  nm_mpi_errhandler_exec(p_session->p_errhandler, NM_MPI_ERRHANDLER_SESSION, session, errorcode);
  return MPI_SUCCESS;
}

int mpi_session_get_info(MPI_Session session, MPI_Info*info_used)
{
  struct nm_mpi_session_s*p_session = nm_mpi_handle_session_get(&nm_mpi_session.sessions, session);
  NM_MPI_SESSION_CHECK(p_session);
  struct nm_mpi_info_s*p_info_dup = nm_mpi_info_dup(p_session->p_info);
  *info_used = p_info_dup->id;
  return MPI_SUCCESS;
}

static const char*nm_mpi_session_psets[] = {
  "mpi://WORLD",
  "mpi://SELF",
  NULL
};

int mpi_session_get_num_psets(MPI_Session session, MPI_Info info, int*npset_names)
{
  struct nm_mpi_session_s*p_session = nm_mpi_handle_session_get(&nm_mpi_session.sessions, session);
  NM_MPI_SESSION_CHECK(p_session);
  *npset_names = sizeof(nm_mpi_session_psets) / sizeof(char*) - 1;
  return MPI_SUCCESS;
}

int mpi_session_get_nth_pset(MPI_Session session, MPI_Info info, int n, int*pset_len, char*pset_name)
{
  struct nm_mpi_session_s*p_session = nm_mpi_handle_session_get(&nm_mpi_session.sessions, session);
  NM_MPI_SESSION_CHECK(p_session);
  if(pset_name == NULL || pset_len == NULL)
    return NM_MPI_ERROR_SESSION(p_session, MPI_ERR_ARG);
  if((n < 0) || (n >= sizeof(nm_mpi_session_psets) - 1))
    return NM_MPI_ERROR_SESSION(p_session, MPI_ERR_ARG);
  if(*pset_len < 0)
    return NM_MPI_ERROR_SESSION(p_session, MPI_ERR_ARG);
  if(*pset_len > 0)
    {
      strncpy(pset_name, nm_mpi_session_psets[n], *pset_len);
      pset_name[*pset_len - 1] = '\0';
    }
  *pset_len = strlen(nm_mpi_session_psets[n]) + 1;
  return MPI_SUCCESS;
}

int mpi_session_get_pset_info(MPI_Session session, const char*pset_name, MPI_Info*info)
{
  struct nm_mpi_session_s*p_session = nm_mpi_handle_session_get(&nm_mpi_session.sessions, session);
  NM_MPI_SESSION_CHECK(p_session);
  if(strcmp(pset_name, "mpi://WORLD") == 0)
    {
      nm_group_t p_group_world = nm_group_world();
      struct nm_mpi_info_s*p_info = nm_mpi_info_alloc();
      char s[MPI_MAX_INFO_KEY];
      snprintf(s, MPI_MAX_INFO_KEY, "%d", nm_group_size(p_group_world));
      nm_group_free(p_group_world);
      nm_mpi_info_define(p_info, "mpi_size", s);
      *info = p_info->id;
      return MPI_SUCCESS;
    }
  else if(strcmp(pset_name, "mpi://SELF") == 0)
    {
      struct nm_mpi_info_s*p_info = nm_mpi_info_alloc();
      nm_mpi_info_define(p_info, "mpi_size", "1");
      *info = p_info->id;
      return MPI_SUCCESS;
    }
  else
    {
      *info = MPI_INFO_NULL;
      return NM_MPI_ERROR_SESSION(p_session, MPI_ERR_ARG);
    }
  return MPI_SUCCESS;
}

int mpi_group_from_session_pset(MPI_Session session, const char*pset_name, MPI_Group*newgroup)
{
  struct nm_mpi_session_s*p_session = nm_mpi_handle_session_get(&nm_mpi_session.sessions, session);
  NM_MPI_SESSION_CHECK(p_session);
  if(newgroup == NULL)
    return NM_MPI_ERROR_SESSION(p_session, MPI_ERR_ARG);
  /* hardwired for now */
  if(strcmp(pset_name, "mpi://WORLD") == 0)
    {
      nm_mpi_group_t*p_new_group = nm_mpi_group_alloc();
      p_new_group->p_nm_group = nm_group_world();
      *newgroup = p_new_group->id;
      return MPI_SUCCESS;
    }
  else if(strcmp(pset_name, "mpi://SELF") == 0)
    {
      nm_mpi_group_t*p_new_group = nm_mpi_group_alloc();
      p_new_group->p_nm_group = nm_group_self();
      *newgroup = p_new_group->id;
      return MPI_SUCCESS;
    }
  return NM_MPI_ERROR_SESSION(p_session, MPI_ERR_UNKNOWN);
}

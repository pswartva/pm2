/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>

#include "../common/nm_examples_helper.h"
#include <Padico/Puk.h>

#define MSG_SIZE (16 * 1024 * 1024L)
#define MSG_NUM 10


/** send a set of requests */
static void prio_send_set(void)
{
  void*buffer = malloc(MSG_NUM * MSG_SIZE);
  nm_sr_request_t*requests = malloc(MSG_NUM * sizeof(nm_sr_request_t));
  printf("# posting send requests...\n");
  int i;
  for(i = 0; i < MSG_NUM; i++)
    {
      const nm_tag_t tag = i;
      int*value = buffer + i * MSG_SIZE;
      const nm_prio_t prio = i;
      nm_sr_send_init(p_session, &requests[i]);
      nm_sr_send_pack_contiguous(p_session, &requests[i], value, MSG_SIZE);
      nm_sr_send_set_priority(p_session, &requests[i], prio);
      nm_sr_send_isend(p_session, &requests[i], p_gate, tag);
    }
  printf("# waiting for send completion...\n");
  for(i = 0; i < MSG_NUM; i++)
    {
      nm_sr_swait(p_session, &requests[i]);
    }
  printf("# send done.\n");
  free(buffer);
  free(requests);
}

/** send all requests in different packets */
static void prio_send_set_pause(void)
{
  void*buffer = malloc(MSG_NUM * MSG_SIZE);
  nm_sr_request_t*requests = malloc(MSG_NUM * sizeof(nm_sr_request_t));
  printf("# posting send requests with pause...\n");
  int i;
  for(i = 0; i < MSG_NUM; i++)
    {
      const nm_tag_t tag = i;
      int*value = buffer + i * MSG_SIZE;
      const nm_prio_t prio = i;
      nm_sr_send_init(p_session, &requests[i]);
      nm_sr_send_pack_contiguous(p_session, &requests[i], value, MSG_SIZE);
      nm_sr_send_set_priority(p_session, &requests[i], prio);
      nm_sr_send_isend(p_session, &requests[i], p_gate, tag);
      /* pause between each send, to be sure the rdv requests are sent separately */
      int k;
      for(k = 0; k < 10; k++)
        {
          nm_sr_progress(p_session);
        }
    }
  printf("# waiting for send completion...\n");
  for(i = 0; i < MSG_NUM; i++)
    {
      nm_sr_swait(p_session, &requests[i]);
    }
  printf("# send done.\n");
  free(buffer);
  free(requests);
}

/** send a set of requests in reverse order */
static void prio_send_set_reverse(void)
{
  void*buffer = malloc(MSG_NUM * MSG_SIZE);
  nm_sr_request_t*requests = malloc(MSG_NUM * sizeof(nm_sr_request_t));
  printf("# posting send requests...\n");
  int i;
  for(i = 0; i < MSG_NUM; i++)
    {
      const nm_tag_t tag = MSG_NUM - i - 1;
      int*value = buffer + i * MSG_SIZE;
      const nm_prio_t prio = MSG_NUM - i - 1;
      nm_sr_send_init(p_session, &requests[i]);
      nm_sr_send_pack_contiguous(p_session, &requests[i], value, MSG_SIZE);
      nm_sr_send_set_priority(p_session, &requests[i], prio);
      nm_sr_send_isend(p_session, &requests[i], p_gate, tag);
    }
  printf("# waiting for send completion...\n");
  for(i = 0; i < MSG_NUM; i++)
    {
      nm_sr_swait(p_session, &requests[i]);
    }
  printf("# send done.\n");
  free(buffer);
  free(requests);
}

/** receive messages as multiple requests listening on different tags */
static void prio_recv_multiple_tag(void)
{
  int i;
  void*buffer = malloc(MSG_NUM * MSG_SIZE);
  nm_sr_request_t*requests = malloc(MSG_NUM * sizeof(nm_sr_request_t));
  printf("# posting receive requests...\n");
  for(i = 0; i < MSG_NUM; i++)
    {
      int*value = buffer + i * MSG_SIZE;
      nm_tag_t tag = i;
      nm_sr_recv_init(p_session, &requests[i]);
      nm_sr_recv_unpack_contiguous(p_session, &requests[i], value, MSG_SIZE);
      nm_sr_recv_irecv(p_session, &requests[i], p_gate, tag, NM_TAG_MASK_FULL);
      nm_sr_request_set_ref(&requests[i], NULL);
    }
  printf("# waiting for receive completion...\n");
  /* busy wait on all requests to check which one completes first */
  int completed = 0;
  while(completed < MSG_NUM)
    {
      nm_sr_progress(p_session);
      for(i = 0; i < MSG_NUM; i++)
        {
          void*p_ref = nm_sr_request_get_ref(&requests[i]);
          if(p_ref == NULL)
            {
              /* request not counted yet as completed */
              int rc = nm_sr_rtest(p_session, &requests[i]);
              if(rc == NM_ESUCCESS)
                {
                  nm_tag_t tag = nm_sr_request_get_tag(&requests[i]);
                  printf("# recv completed: req #%d; prio = %d\n", i, (int)tag);
                  nm_sr_request_set_ref(&requests[i], (void*)0xFF); /* mark as processed */
                  completed++;
                }
            }
        }
    }
  free(buffer);
  free(requests);
}

/** receive messages as multiple requests listening on any tag */
static void prio_recv_multiple_anytag(void)
{
  int i;
  void*buffer = malloc(MSG_NUM * MSG_SIZE);
  nm_sr_request_t*requests = malloc(MSG_NUM * sizeof(nm_sr_request_t));
  printf("# posting receive requests...\n");
  for(i = 0; i < MSG_NUM; i++)
    {
      int*value = buffer + i * MSG_SIZE;
      nm_sr_recv_init(p_session, &requests[i]);
      nm_sr_recv_unpack_contiguous(p_session, &requests[i], value, MSG_SIZE);
      nm_sr_recv_irecv(p_session, &requests[i], p_gate, 0, NM_TAG_MASK_NONE); /* any tag */
      nm_sr_request_set_ref(&requests[i], NULL);
    }
  printf("# waiting for receive completion...\n");
  /* busy wait on all requests to check which one completes first */
  int completed = 0;
  while(completed < MSG_NUM)
    {
      nm_sr_progress(p_session);
      for(i = 0; i < MSG_NUM; i++)
        {
          void*p_ref = nm_sr_request_get_ref(&requests[i]);
          if(p_ref == NULL)
            {
              /* request not counted yet as completed */
              int rc = nm_sr_rtest(p_session, &requests[i]);
              if(rc == NM_ESUCCESS)
                {
                  nm_tag_t tag = nm_sr_request_get_tag(&requests[i]);
                  printf("# recv completed: req #%d; prio = %d\n", i, (int)tag);
                  nm_sr_request_set_ref(&requests[i], (void*)0xFF); /* mark as processed */
                  completed++;
                }
            }
        }
    }
  free(buffer);
  free(requests);
}

/** receive messages with a single wildcard request at a time */
static void prio_recv_single_wildcard(void)
{
  int i;
  void*buffer = malloc(MSG_SIZE);
  nm_sr_request_t request;
  printf("# posting receive request...\n");
  for(i = 0; i < MSG_NUM; i++)
    {
      nm_sr_recv_init(p_session, &request);
      nm_sr_recv_unpack_contiguous(p_session, &request, buffer, MSG_SIZE);
      nm_sr_recv_irecv(p_session, &request, NM_GATE_NONE, 0, NM_TAG_MASK_NONE); /* any gate, any tag */
      nm_sr_rwait(p_session, &request);
      nm_tag_t tag = nm_sr_request_get_tag(&request);
      printf("# recv completed: req #%d; prio = %d\n", i, (int)tag);
    }
  free(buffer);
}

int main(int argc, char**argv)
{
  nm_examples_init(&argc, argv);
  nm_examples_barrier(1);

  /* ** multiple receives, by tag */
  if(is_server)
    {
      printf("# ## test: multiple receives, multiple tags\n");
      prio_recv_multiple_tag();
    }
  else
    {
      puk_sleep(1);
      prio_send_set();
    }
  nm_examples_barrier(2);
  if(is_server)
    {
      printf("# ## test: multiple receives, multiple tags, send reversed\n");
      prio_recv_multiple_tag();
    }
  else
    {
      puk_sleep(1);
      prio_send_set_reverse();
    }
  nm_examples_barrier(3);
  if(is_server)
    {
      printf("# ## test: multiple receives, multiple tags, send pause\n");
      prio_recv_multiple_tag();
    }
  else
    {
      puk_sleep(1);
      prio_send_set_pause();
    }
  nm_examples_barrier(3);


  /* ** multiple receives, any tag */
  if(is_server)
    {
      printf("# ## test: multiple receives, multiple anytags\n");
      prio_recv_multiple_anytag();
    }
  else
    {
      puk_sleep(1);
      prio_send_set();
    }
  nm_examples_barrier(2);
  if(is_server)
    {
      printf("# ## test: multiple receives, multiple anytags, send reversed\n");
      prio_recv_multiple_anytag();
    }
  else
    {
      puk_sleep(1);
      prio_send_set_reverse();
    }
  nm_examples_barrier(3);
  if(is_server)
    {
      printf("# ## test: multiple receives, multiple anytags, send pause\n");
      prio_recv_multiple_anytag();
    }
  else
    {
      puk_sleep(1);
      prio_send_set_pause();
    }
  nm_examples_barrier(3);

  /* ** single wildcard receive at a time */
  if(is_server)
    {
      printf("# ## test: single wildcard receive, multiple tags\n");
      prio_recv_single_wildcard();
    }
  else
    {
      prio_send_set();
    }
  nm_examples_barrier(4);
  if(is_server)
    {
      printf("# ## test: single wildcard receive, multiple tags, send reversed\n");
      prio_recv_single_wildcard();
    }
  else
    {
      prio_send_set_reverse();
    }
  nm_examples_barrier(5);
  if(is_server)
    {
      printf("# ## test: single wildcard receive, multiple tags, send pause\n");
      prio_recv_single_wildcard();
    }
  else
    {
      prio_send_set_pause();
    }
  nm_examples_barrier(5);

  /* check order of unexpected matching */
  if(is_server)
    {
      printf("# ## test: multiple tags receives, multiple tags, unexpected\n");
      puk_sleep(3);
      prio_recv_multiple_tag();
    }
  else
    {
      prio_send_set();
    }
  nm_examples_barrier(5);
  if(is_server)
    {
      printf("# ## test: multiple anytag receives, multiple tags, unexpected\n");
      puk_sleep(3);
      prio_recv_multiple_anytag();
    }
  else
    {
      prio_send_set();
    }
  nm_examples_barrier(5);
  if(is_server)
    {
      printf("# ## test: multiple anytag receives, multiple tags, unexpected, send with pause\n");
      puk_sleep(3);
      prio_recv_multiple_anytag();
    }
  else
    {
      prio_send_set_pause();
    }
  nm_examples_barrier(5);
  if(is_server)
    {
      printf("# ## test: single wildcard receives, multiple tags, unexpected, send with pause\n");
      puk_sleep(3);
      prio_recv_single_wildcard();
    }
  else
    {
      prio_send_set_pause();
    }

  nm_examples_exit();
  exit(0);
}

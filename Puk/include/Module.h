/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Public Puk module API
 * @ingroup Puk
 */

/** @example Tutorial/01-HelloWorld/HelloWorld.c
 */

#ifndef PADICO_MODULE_H
#define PADICO_MODULE_H

#include "Puk.h"

#ifdef __cplusplus
#define EXTERN_C extern "C"
#else
#define EXTERN_C extern
#endif


/* ** internals ******************************************** */

/** @internal defines function that depends on module */
#define PUK_MOD_INTERNALS_FUNCTIONS(MODNAME)                            \
  __attribute__ ((__unused__))                                          \
  static inline const char*padico_getattr(const char*label)             \
  { return puk_mod_getattr(padico_module_self(), label); }              \
  __attribute__ ((__unused__))                                          \
  static inline const char*padico_getlocalattr(const char*label)        \
  { return puk_mod_local_getattr(padico_module_self(), label); }        \
  __attribute__ ((__unused__))                                          \
  static inline void padico_setattr(const char*label, const char*value) \
  { puk_mod_setattr(padico_module_self(), label, value); }

#define PUK_MOD_INTERNALS_STRUCTURES(MODNAME, INITFUNC, RUNFUNC, FINALIZEFUNC, ...) \
  extern struct puk_mod_binary_s padico_mod_binary__##MODNAME;          \
  static const struct puk_mod_funcs_s padico_mod_funcs__##MODNAME =     \
    {                                                                   \
      .init     = (INITFUNC),                                           \
      .run      = (RUNFUNC),                                            \
      .finalize = (FINALIZEFUNC)                                        \
    };                                                                  \
  struct puk_mod_binary_s padico_mod_binary__##MODNAME =                \
    {                                                                   \
      .name    = #MODNAME,                                              \
      .funcs   = &padico_mod_funcs__##MODNAME,                          \
      .mod     = NULL,                                                  \
      .profile = PUK_PROFILE_MEM_STATIC_INITIALIZER,                    \
      .auto_deps = PUK_MOD_AUTO_DEP_LIST,                               \
      .n_deps  = sizeof((char*[]){ 0, __VA_ARGS__ }) / sizeof(char*),   \
      .deps    = { NULL, __VA_ARGS__ }                                  \
    };

/** @internal defines type & declaration for module */
#define PUK_MOD_INTERNALS_CREATE(MODNAME, INITFUNC, RUNFUNC, FINALIZEFUNC, ...) \
  PUK_MOD_INTERNALS_STRUCTURES(MODNAME, INITFUNC, RUNFUNC, FINALIZEFUNC, __VA_ARGS__) \
  static const char*padico_mod_local_name = #MODNAME;                   \
  static void padico_module_create_initializer__##MODNAME(void)         \
    __attribute__((constructor(200),used));                             \
  static void padico_module_create_initializer__##MODNAME(void)         \
  {                                                                     \
    padico_mod_binary_local = &padico_mod_binary__##MODNAME;            \
  }

/** @internal import declaration for a module defined in another file */
#define PUK_MOD_INTERNALS_IMPORT(MODNAME)                               \
  static const char*padico_mod_local_name = #MODNAME;                   \
  extern struct puk_mod_binary_s padico_mod_binary__##MODNAME;          \
  static void padico_module_import_initializer__##MODNAME(void)         \
    __attribute__((constructor(200),used));                             \
  static void padico_module_import_initializer__##MODNAME(void)         \
  {                                                                     \
    padico_mod_binary_local = &padico_mod_binary__##MODNAME;            \
  }

/** define a binary module */
#define PUK_MOD_BINARY(MODNAME, INITFUNC, RUNFUNC, FINALIZEFUNC, ...)   \
  PUK_MOD_INTERNALS_CREATE(MODNAME, INITFUNC, RUNFUNC, FINALIZEFUNC, __VA_ARGS__); \
  PUK_MOD_INTERNALS_FUNCTIONS(MODNAME);

/* *** public macros *************************************** */

/** attach the current file to the given module, defined in another file */
#define PADICO_MODULE_HOOK(MODNAME)             \
  PUK_MOD_INTERNALS_IMPORT(MODNAME);            \
  PUK_MOD_INTERNALS_FUNCTIONS(MODNAME);

#if defined(PUK_BUILTIN_MODS)
#define PADICO_MODULE_DECLARE(MODNAME, INITFUNC, RUNFUNC, FINALIZEFUNC, ...) \
  PADICO_MODULE_BUILTIN(MODNAME, INITFUNC, RUNFUNC, FINALIZEFUNC, __VA_ARGS__)
#else /* PUK_BUILTIN_MODS */
/** define a module */
#define PADICO_MODULE_DECLARE(MODNAME, INITFUNC, RUNFUNC, FINALIZEFUNC, ...) \
  PUK_MOD_BINARY(MODNAME, INITFUNC, RUNFUNC, FINALIZEFUNC,  __VA_ARGS__)
#endif /* PUK_BUILTIN_MODS */

/* ** component modules ************************************ */

/** create a module as a container for a single component */
#define PADICO_MODULE_COMPONENT(MODNAME, COMPONENT)                     \
  EXTERN_C puk_component_t padico_module_component_##MODNAME;           \
  puk_component_t padico_module_component_##MODNAME = NULL;             \
  __attribute__ ((__unused__))                                          \
  static inline puk_component_t padico_module_component_self(void)      \
  { return padico_module_component_##MODNAME; }                         \
  EXTERN_C int padico_module_component__##MODNAME##_init(void)          \
  {                                                                     \
    puk_component_t c = (COMPONENT);                                    \
    if(strcmp(c->name, #MODNAME) != 0)                                  \
      padico_fatal("name inconsistency in module declaration.\n");      \
    padico_module_component_##MODNAME = c;                              \
    /* TODO- check driver */                                            \
    return 0;                                                           \
  }                                                                     \
  EXTERN_C void padico_module_component__##MODNAME##_finalize(void)     \
  {                                                                     \
    puk_component_destroy(padico_module_component_##MODNAME);           \
    padico_module_component_##MODNAME = NULL;                           \
  }                                                                     \
  PADICO_MODULE_DECLARE(MODNAME, padico_module_component__##MODNAME##_init, NULL, padico_module_component__##MODNAME##_finalize);


/* ** binary modules *************************************** */

/* #define here for a "soft link" to the binary-driver functions */
#define padico_module_myjob()  padico_tm_binary_myjob()
#define padico_module_exit(RC) padico_tm_binary_exit(RC)


/* ** builtin modules ************************************** */

/* these macros are used to define the content of a module of type 'builtin'
 * (contained in the main binary) */

/** @internal */
void puk_mod_builtin_declare(struct puk_mod_binary_s*builtin);

/** declare a builtin module */
#define PADICO_MODULE_BUILTIN(MODNAME, INITFUNC, RUNFUNC, FINALIZEFUNC, ...) \
  PUK_MOD_INTERNALS_STRUCTURES(MODNAME, INITFUNC, RUNFUNC, FINALIZEFUNC, __VA_ARGS__) \
  static void padico_module_builtin_initializer__##MODNAME(void)        \
    __attribute__((constructor(200),used));                             \
  static void padico_module_builtin_initializer__##MODNAME(void)        \
  {                                                                     \
    padico_mod_binary_local = &padico_mod_binary__##MODNAME;            \
    padico_mod_local_name = #MODNAME;                                   \
    puk_mod_builtin_declare(&padico_mod_binary__##MODNAME);             \
  }                                                                     \
  PUK_MOD_INTERNALS_FUNCTIONS(MODNAME);


/* ** Attributes ******************************************* */

/** declare a module attribute for a binary or builtin module
 * @param ATTRNAME name of the attribute
 * @param ENVVAR   environment variable that is synchronized with attribute
 * @param DESC     plain text description of the attribute
 * @param TYPE     type of the attribute, among: bool, int, unsigned, string
 * @param VALUE    initial value
 */
#define PADICO_MODULE_ATTR(ATTRNAME, ENVVAR, DESC, TYPE, VALUE)         \
  static struct puk_opt_s padico_module_opt_##ATTRNAME =                \
    {                                                                   \
      .label    = #ATTRNAME,                                            \
      .env_var  = (ENVVAR),                                             \
      .desc     = (DESC),                                               \
      .type     = puk_opt_type_##TYPE,                                  \
      .value.as_##TYPE = (VALUE)                                        \
    };                                                                  \
  static void padico_module_attr_initializer__##ATTRNAME(void)          \
    __attribute__((constructor(500),used));                             \
  static void padico_module_attr_initializer__##ATTRNAME(void)          \
  {                                                                     \
    padico_module_opt_##ATTRNAME.mod_name = padico_mod_local_name; /* dynamic value not allowed for static initialization */ \
    puk_opt_declare(&padico_module_opt_##ATTRNAME); /* 'opt' should remain allocated */ \
  }                                                                     \
  static void padico_module_attr_finalizer__##ATTRNAME(void)          \
    __attribute__((destructor(500),used));                              \
  static void padico_module_attr_finalizer__##ATTRNAME(void)            \
  {                                                                     \
    puk_opt_remove(&padico_module_opt_##ATTRNAME); /* remove opt from global table before unloading object */ \
  }                                                                     \
  static inline void padico_module_attr_##ATTRNAME##_setvalue(puk_opt_type_##TYPE##_t v) \
    __attribute__((unused));                                            \
  static inline void padico_module_attr_##ATTRNAME##_setvalue(puk_opt_type_##TYPE##_t v) \
  {                                                                     \
    padico_module_opt_##ATTRNAME.value.as_##TYPE = v;                   \
  }                                                                     \
  static inline puk_opt_type_##TYPE##_t padico_module_attr_##ATTRNAME##_getvalue(void) \
  {                                                                     \
    return puk_opt_getvalue_##TYPE(&padico_module_opt_##ATTRNAME);      \
  }

#endif /* PADICO_MODULE_H */

/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file ProxyFactory interface definition
 */

#ifndef PADICO_PROXYFACTORY_H
#define PADICO_PROXYFACTORY_H

#include <sys/types.h>
#include <sys/socket.h>

/** Driver for interface: 'ProxyFactory'.
 * @note ProxyFactory implementations _should_ not use instantiation.
 */
struct padico_proxyfactory_s
{
  /** create a proxy on-demand. The implementation _may_ use
   * a shared proxy for all instances.
   * @param addr return argument pointing to the proxy address
   * @param addrlen return argument giving the actual length of addr
   * @return 0 if success; -errno in case of error.
   */
  int  (*create)(const struct sockaddr**addr, socklen_t*addrlen);
  /** release the proxy, notifying that we disconnected.
   * The implementation _may_ kill the proxy if refcount reches 0.
   */
  void (*release)(void);
};

PUK_IFACE_TYPE(ProxyFactory, struct padico_proxyfactory_s);


#endif /* PADICO_PROXYFACTORY_H */


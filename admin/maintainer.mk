# ## maintainer makefile

mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
mkfile_dir  := $(dir $(mkfile_path))

PM2_ROOT    := $(mkfile_dir)/..

DATE                 := $(shell date +%Y-%m-%d )
PM2_VERSION          := $(shell cat $(PM2_ROOT)/building-tools/VERSION)
MPIBENCHMARK_VERSION := $(shell cat $(PM2_ROOT)/mpibenchmark/VERSION)

show:
	@echo "DATE                 = $(DATE)"
	@echo "PM2_VERSION          = $(PM2_VERSION)"
	@echo "MPIBENCHMARK_VERSION = $(MPIBENCHMARK_VERSION)"
	@echo
	@if [ "$(DATE_VERSION)" != "$(PM2_VERSION)" ]; then echo "inconsistency in version number"; exit 1; fi

# ## full PM2

release-pm2: show pm2-$(PM2_VERSION).tar.gz

pm2-$(PM2_VERSION).tar.gz: tmp/pm2-$(PM2_VERSION)
	( cd ./tmp ; tar czf pm2-$(PM2_VERSION).tar.gz ./pm2-$(PM2_VERSION) )
	@echo "# ## file ready: $@"

tmp/pm2-$(PM2_VERSION):
	-rm -r ./tmp/pm2-$(PM2_VERSION)
	-mkdir -p ./tmp/pm2-$(PM2_VERSION)
	git archive master | ( cd ./tmp/pm2-$(PM2_VERSION) ; tar x )
	( cd ./tmp/pm2-$(PM2_VERSION)/admin ; ./autogen.sh )

# ## MadMPI Benchmark

release-mpibenchmark: show mpibenchmark-$(MPIBENCHMARK_VERSION).tar.gz

mpibenchmark-$(MPIBENCHMARK_VERSION).tar.gz: tmp/mpibenchmark-$(MPIBENCHMARK_VERSION)
	( cd ./tmp ; tar czf mpibenchmark-$(MPIBENCHMARK_VERSION).tar.gz ./mpibenchmark-$(MPIBENCHMARK_VERSION) )
	@echo
	@echo "# ## file ready: $@"
	@echo

tmp/mpibenchmark-$(MPIBENCHMARK_VERSION):
	-rm -r ./tmp/mpibenchmark-$(MPIBENCHMARK_VERSION)
	-rm -r ./tmp/pm2-$(MPIBENCHMARK_VERSION)
	-mkdir -p ./tmp/pm2-$(MPIBENCHMARK_VERSION)
	git archive master | ( cd ./tmp/pm2-$(MPIBENCHMARK_VERSION) ; tar x )
	mv ./tmp/pm2-$(MPIBENCHMARK_VERSION)/mpibenchmark ./tmp/mpibenchmark-$(MPIBENCHMARK_VERSION)
	mv ./tmp/pm2-$(MPIBENCHMARK_VERSION)/building-tools ./tmp/mpibenchmark-$(MPIBENCHMARK_VERSION)/
	mv ./tmp/pm2-$(MPIBENCHMARK_VERSION)/mpi_sync_clocks ./tmp/mpibenchmark-$(MPIBENCHMARK_VERSION)/
	( cd ./tmp/mpibenchmark-$(MPIBENCHMARK_VERSION)/mpi_sync_clocks ; ./autogen.sh )
	( cd ./tmp/mpibenchmark-$(MPIBENCHMARK_VERSION) ; ./autogen.sh )
	-rm -r ./tmp/pm2-$(MPIBENCHMARK_VERSION)

# ## Bench NBC

release-bench_nbc: bench_nbc_$(VERSION).tar.gz

bench_nbc_$(VERSION).tar.gz: tmp/bench_nbc_$(VERSION)
	( cd tmp ; tar czf bench_nbc_$(VERSION).tar.gz ./bench_nbc )
	@echo "# ## file ready: $@"

tmp/bench_nbc_$(VERSION):
	-mkdir -p tmp
	-rm -r ./tmp/bench_nbc
	svn export svn+ssh://scm.gforge.inria.fr/svn/pm2/trunk/bench_nbc ./tmp/bench_nbc
	svn export svn+ssh://scm.gforge.inria.fr/svn/pm2/trunk/mpi_sync_clocks ./tmp/bench_nbc/mpi_sync_clocks
	svn export svn+ssh://scm.gforge.inria.fr/svn/padico/PadicoTM/trunk/PadicoTM/building-tools ./tmp/bench_nbc/building-tools
	( cd tmp/bench_nbc/mpi_sync_clocks ; ./autogen.sh )
	( cd tmp/bench_nbc ; ./autogen.sh )

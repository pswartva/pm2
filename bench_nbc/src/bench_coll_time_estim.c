/*
 * Bench NBC
 * Copyright (C) 2019-2020 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include "bench_coll_time_estim.h"

coll_time_info_t**estimate_coll_times(int flags,int coll_time, int data_size, int myrank,int nbprocs,double freq,int collective){

  if(coll_time > 0){
    coll_time_info_t**temp = malloc(sizeof(coll_time_info_t*));
    *temp = malloc(sizeof(coll_time_info_t));
    (*temp)->theorical_time = coll_time;
    (*temp)->data_size = data_size_from_time(*temp, (double)coll_time, myrank, nbprocs, freq, collective, flags);
    (*temp)->real_time = real_time_from_data_size(*temp, collective, freq, myrank, nbprocs, flags);
    return temp;
  }else if(data_size > 0){
    coll_time_info_t**temp = malloc(sizeof(coll_time_info_t*));
    *temp = malloc(sizeof(coll_time_info_t));
    (*temp)->data_size = data_size;
    (*temp)->real_time = real_time_from_data_size(*temp, collective, freq, myrank, nbprocs, flags);
    (*temp)->theorical_time = (*temp)->real_time->median[0];
    fprintf(stderr,"coll_time done\n");
    return temp;
  }
  coll_time_info_t**temp = malloc(sizeof(coll_time_info_t*)*NB_DATA_SIZE);
  int i;
  /*if(flags & USE_STATIC){

    for(i = 0; i < NB_DATA_SIZE; i++){
      temp[i] = malloc(sizeof(coll_time_info_t));
      temp[i]->theorical_time = 0;
      temp[i]->data_size = DATA_SIZES[i];
      temp[i]->real_time = real_time_from_data_size(temp[i],collective,freq,myrank,nbprocs, flags);
    }
    if(myrank == ROOT)
      fprintf(stderr,"Use static times\n");
    return temp;
  }*/
  if(flags & USE_COLL_TIME){
    int t = BASE_DATA_TIME;
    for(i = 0; i < NB_DATA_SIZE; i++){
      temp[i] = malloc(sizeof(coll_time_info_t));
      temp[i]->theorical_time = t;
      temp[i]->data_size = data_size_from_time(temp[i], (double)t, myrank, nbprocs, freq, collective, flags);
      temp[i]->real_time = real_time_from_data_size(temp[i], collective, freq, myrank, nbprocs, flags);
      MPI_Barrier(MPI_COMM_WORLD);
      fprintf(stderr,"rank%d done:%d(%lf)size:%d\n",myrank,temp[i]->theorical_time,temp[i]->real_time->median[0],temp[i]->data_size);
      MPI_Barrier(MPI_COMM_WORLD);

      t *= DATA_SIZE_STEP;
    }
    if(myrank == ROOT)
      fprintf(stderr,"Collectives sizes estimated\n");
    return temp;
  }
  int size = BASE_DATA_SIZE;
  for(i = 0; i < NB_DATA_SIZE; i++){
    temp[i] = malloc(sizeof(coll_time_info_t));
    temp[i]->theorical_time = 0;
    temp[i]->data_size = size;
    temp[i]->real_time = real_time_from_data_size(temp[i], collective, freq, myrank, nbprocs, flags);
    size *= DATA_SIZE_STEP;

  }
  if(myrank == ROOT)
    fprintf(stderr,"Collectives times estimated\n");
  return temp;
}

int data_size_from_time(coll_time_info_t*coll,double target,int myrank,int nbprocs,double freq,int collective, int flags)
{

  int size = 1 , old_size=0, temp_size=0, closer_size=0;
  bench_time_t time;
  double closer_delta = 10e9,tt=0, old_time;
  double*ranks_time = malloc(sizeof(double)*nbprocs);
  int multiplicator = 1000, max_iter = 0;

  do{

    /*    void*data = malloc(size*multiplicator);
          void*data2 = malloc(size*multiplicator);
          MPI_Request req;
          MPI_Status st;
          Process_Sync_Offset = 0;
          sync_mpi();//make all t0(i) equals
    //fprintf(stderr,"go\n");
    sync_start(myrank);
    Timer_tsc_ref(&t1);
    start_collective(data,data2,size*multiplicator,collective,&req,nbprocs);
    MPI_Wait(&req,&st);
    Timer_tsc_ref(&t2);
    //MPI_Barrier(MPI_COMM_WORLD);
    free(data);
    free(data2);*/

    old_time = tt;
    coll->data_size = size*multiplicator;
    time = *real_time_from_data_size(coll, collective, freq, myrank, nbprocs, flags);
    MPI_Gather(&(time.median[0]),1,MPI_DOUBLE,ranks_time,1,MPI_DOUBLE,ROOT,MPI_COMM_WORLD);
    //MPI_Barrier(MPI_COMM_WORLD);
    if(myrank == ROOT){
      tt = max(ranks_time,nbprocs);
      temp_size = interpolate(old_size,size,old_time,tt,target);
      if(fabs((target-tt)) < closer_delta){
        coll->real_time=&time;
        closer_size = size*multiplicator;
        closer_delta = fabs(target-tt);
      }
      fprintf(stderr,"[COLL tar:%f]:size:%d*%d,nbrep:%d,coll:%d,time:(%.2f-%.2f-%.2f;%.2f)%f(delta:%f)===>%d\n",target,size,multiplicator,get_nb_rep(size*multiplicator,0,NB_REP),collective,time.min[0],time.max[0],time.median[0],sqrt(time.var[0]),tt,closer_delta,closer_size);
      if((max_iter >= FINE_ESTIM_MAX_ITER && closer_delta <= target/100) || max_iter == ESTIM_MAX_ITER)
        temp_size = closer_size;
      else if(temp_size > 2000000)
        fprintf(stderr,"WARNING: Targeted collective duration is too high, closer: %d(delta:%f)\n",closer_size,closer_delta);

    }
    MPI_Bcast(&temp_size,1,MPI_INT,ROOT,MPI_COMM_WORLD);
    MPI_Bcast(&closer_delta,1,MPI_DOUBLE,ROOT,MPI_COMM_WORLD);
    if(closer_delta < target / 1000){
      free(ranks_time);
      return temp_size*multiplicator;
    }
    //MPI_Barrier(MPI_COMM_WORLD);
    if((max_iter >= FINE_ESTIM_MAX_ITER && closer_delta <= target/100) || max_iter == ESTIM_MAX_ITER){
      free(ranks_time);
      return temp_size;
    }
    if(temp_size == 0){
      if(multiplicator == 1){
        free(ranks_time);
        return 1;
      }
      size = 0;
      temp_size = 1;
      multiplicator /= 10;
    }
    else if(temp_size > 2000000){
      temp_size = 2000000;
    }
    if(temp_size == size && temp_size == 2000000){
      free(ranks_time);
      MPI_Bcast(&closer_size,1,MPI_INT,ROOT,MPI_COMM_WORLD);
      return closer_size;
    }
    old_size = size;
    size = temp_size;
    max_iter++;
  }while(1);
  return size;
}

bench_time_t*real_time_from_data_size(coll_time_info_t*t, int collective,double freq, int myrank, int nbprocs, int flags)
{
  int size = t->data_size;
  int nb_rep = get_nb_rep(size,0,NB_REP);
  //int nb_rep = 1;
  double*tab = malloc(sizeof(double)*nb_rep);
  MPI_Request req;
  MPI_Status st;
  int i;
#ifdef SYNC_1
  uint64_t t1,t2;
#endif
#ifdef SYNC_2
  double t1,t2;
  mpi_sync_clocks_t clock;
  clock = mpi_sync_clocks_init(MPI_COMM_WORLD);
#endif
  for(i = 0; i < nb_rep; i++)
    {
#ifndef GLOBAL_BUFFER
      void*data = malloc(size);
      void*data2 = malloc(size);
#endif
      //fprintf(stderr,"[%d/%d]:data:%p data2:%p,size:%d,coll:%d,req:%p\n",i,nb_rep,data,data2,size,collective,&req);
#ifdef SYNC_1
      Process_Sync_Offset = 0;
      sync_mpi();//make all t0(i) equals
      //fprintf(stderr,"go\n");
      sync_start(myrank);
      Timer_tsc_ref(&t1);
#endif
#ifdef SYNC_2
      mpi_sync_clocks_synchronize(clock);
      double barr;
      mpi_sync_clocks_barrier(clock,&barr);
      t1 = mpi_sync_clocks_get_time_usec(clock);
#endif

      if(flags & USE_BLOCKING)
	{
#ifdef GLOBAL_BUFFER
	  block_collective(size, collective, nbprocs);
#else
	  block_collective(data, data2, size, collective, nbprocs);
#endif
	}
      else
	{
#ifdef GLOBAL_BUFFER
	  start_collective(size,collective,&req,nbprocs);
#else
	  start_collective(data,data2,size,collective,&req,nbprocs);
#endif
	  MPI_Wait(&req, &st);
	}

#ifdef SYNC_1
      Timer_tsc_ref(&t2);
      tab[i] = (double)((t2 -t1)*freq);
#endif
#ifdef SYNC_2
      t2 = mpi_sync_clocks_get_time_usec(clock);
      tab[i] = mpi_sync_clocks_local_to_global(clock,t2) - mpi_sync_clocks_local_to_global(clock,t1);
#endif
      //if(myrank == ROOT)
      //fprintf(stderr,"[COLL_TIME:%f][%d/%d]:data:%p data2:%p,size:%d,coll:%d,req:%p\n",tab[i],i,nb_rep,data,data2,size,collective,&req);
#ifndef GLOBAL_BUFFER
      free(data);
      free(data2);
#endif
    }
  bench_time_t*times = malloc(sizeof(bench_time_t));//,b,c;
  times->median = malloc(sizeof(double));
  times->var = malloc(sizeof(double));
  times->max = malloc(sizeof(double));
  times->min = malloc(sizeof(double));
  times->median[0] = median(tab,nb_rep);
  times->var[0] = variance(tab,nb_rep);
  times->max[0] = max(tab,nb_rep);
  times->min[0] = min(tab,nb_rep);
#ifdef SYNC_2
  mpi_sync_clocks_shutdown(clock);
#endif
  free(tab);
  //fprintf(stderr,"size:%d,coll:%d,med:%f,max:%f,min:%f\n",size,collective,a,b,c);
  return times;
}

#ifdef GLOBAL_BUFFER
void start_collective(int size,int collective,MPI_Request*req,int nbprocs)
#else
void start_collective(void*data,void*data2,int size,int collective,MPI_Request*req,int nbprocs)
#endif
{
  switch(collective){

    case CALL_MPI_IBARRIER:
      MPI_Ibarrier(MPI_COMM_WORLD,req);
      break;
    case CALL_MPI_IBCAST:
      MPI_Ibcast(data,size,MPI_BYTE,ROOT,MPI_COMM_WORLD,req);
      //fprintf(stderr,"bcast\n");
      break;
    case CALL_MPI_IREDUCE:
      //fprintf(stderr,"reduce\n");
      MPI_Ireduce(data,data2,size,MPI_BYTE,MPI_BAND,ROOT,MPI_COMM_WORLD,req);
      break;
    case CALL_MPI_IALLTOALL:
      MPI_Ialltoall(data,size/nbprocs,MPI_BYTE,data2,size/nbprocs,MPI_BYTE,MPI_COMM_WORLD,req);
      break;
    case CALL_MPI_IALLREDUCE:
      MPI_Iallreduce(data,data2,size,MPI_BYTE,MPI_BAND,MPI_COMM_WORLD,req);
      break;
    case CALL_MPI_IGATHER:
      MPI_Igather(data,size/nbprocs,MPI_BYTE,data2,size/nbprocs,MPI_BYTE,ROOT,MPI_COMM_WORLD,req);
      break;
    case CALL_MPI_IALLGATHER:
      MPI_Iallgather(data,size/nbprocs,MPI_BYTE,data2,size/nbprocs,MPI_BYTE,MPI_COMM_WORLD,req);
      break;
    default:
      fprintf(stderr,"ERROR:Collective not implemented yet\n");
      exit(3);
      break;

  }
}

#ifdef GLOBAL_BUFFER
void block_collective(int size, int collective, int nbprocs)
#else
void block_collective(void*data, void*data2, int size, int collective, int nbprocs)
#endif
 {
  switch(collective)
    {
    case CALL_MPI_IBARRIER:
      MPI_Barrier(MPI_COMM_WORLD);
      break;
    case CALL_MPI_IBCAST:
      MPI_Bcast(data, size, MPI_BYTE, ROOT, MPI_COMM_WORLD);
      break;
    case CALL_MPI_IREDUCE:
      MPI_Reduce(data, data2, size, MPI_BYTE, MPI_BAND, ROOT, MPI_COMM_WORLD);
      break;
    case CALL_MPI_IALLTOALL:
      MPI_Alltoall(data, size/nbprocs, MPI_BYTE, data2, size/nbprocs, MPI_BYTE, MPI_COMM_WORLD);
      break;
    case CALL_MPI_IALLREDUCE:
      MPI_Allreduce(data, data2, size, MPI_BYTE, MPI_BAND, MPI_COMM_WORLD);
      break;
    case CALL_MPI_IGATHER:
      MPI_Gather(data, size/nbprocs, MPI_BYTE, data2, size/nbprocs, MPI_BYTE, ROOT, MPI_COMM_WORLD);
      break;
    case CALL_MPI_IALLGATHER:
      MPI_Allgather(data, size/nbprocs, MPI_BYTE, data2, size/nbprocs, MPI_BYTE,MPI_COMM_WORLD);
      break;
    default:
      fprintf(stderr,"ERROR:Collective not implemented yet\n");
      exit(3);
      break;

  }
}

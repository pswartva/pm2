/*
 * NewMadeleine
 * Copyright (C) 2006-2023 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include "nm_ibverbs.h"
#include "nm_ibverbs_rcache.h"

#include <Padico/Module.h>


/* *** method: 'rcache' ********************************* */

/** token given by the receiver to the sender alongside the rdv */
struct nm_ibverbs_rcache_rdv_token_s
{
  uint64_t raddr;
  uint64_t sigpkt_raddr;
  uint32_t rkey;
  uint32_t sigpkt_rkey;
  uint32_t seq;
};

/** Header sent to signal presence of rcache data */
struct nm_ibverbs_rcache_sighdr_s
{
  uint32_t head_offset;
#ifdef DEBUG
  uint32_t crc;
#endif /* DEBUG */
  uint32_t seq;
  volatile int busy;
};

#define NM_IBVERBS_RCACHE_ALIGN 64

#define NM_IBVERBS_RCACHE_SIGPAYLOAD 64

struct nm_ibverbs_rcache_sigpkt_s
{
  char head[NM_IBVERBS_RCACHE_SIGPAYLOAD];
  struct nm_ibverbs_rcache_sighdr_s sighdr;
};

struct nm_ibverbs_rcache_entry_s
{
  struct nm_prefetch_key_s key;
  struct ibv_mr*mr;
  void*p_reg_handle;
};

PUK_HASHTABLE_TYPE(nm_ibverbs_rcache_entry, struct nm_prefetch_key_s*,
                   struct nm_ibverbs_rcache_entry_s*,
                   &nm_prefetch_key_hash, &nm_prefetch_key_eq, NULL);

/** context for ibverbs 'rcache' */
struct nm_ibverbs_rcache_context_s
{
  struct nm_ibverbs_context_s*p_ibverbs_context;
  const struct nm_ibverbs_reg_driver_s*p_reg_driver;
  struct nm_ibverbs_rcache_entry_hashtable_s prefetched_mrs;
  struct nm_ibverbs_regpool_s sigpkt_regpool;      /**< pool of registered memory, used for sigpkt */
  int vector_connect;                              /**< whether we use vectored connection */
  int lazy_connect;                                /**< whether we use lazy connection establishment */
#ifdef DEBUG
  int do_crc;
  puk_checksum_func_t p_crc_func;
#endif
};

/** Connection state for 'rcache' tracks */
struct nm_ibverbs_rcache_s
{
  struct nm_ibverbs_cnx_s*p_cnx;
  struct nm_ibverbs_rcache_context_s*p_rcache_context;
  struct
  {
    char*message;
    nm_len_t size;
    void*p_reg_handle;
    struct ibv_mr*mr;
    const struct nm_ibverbs_regpool_entry_s*p_rsigpkt_entry;
    struct nm_ibverbs_rcache_sigpkt_s*p_rsigpkt;
    int seq;
  } recv;
  struct
  {
    const char*message;
    nm_len_t size;
    void*p_reg_handle;
    struct ibv_mr*mr;
    const struct nm_ibverbs_regpool_entry_s*p_ssigpkt_entry;
    struct nm_ibverbs_rcache_sigpkt_s*p_ssigpkt;
#ifdef DEBUG
    uint32_t crc;
#endif /* DEBUG */
    struct nm_ibverbs_rcache_rdv_token_s rdv_data;
    int seq;
    int posted;
  } send;
  struct
  {
    void*p_remote_url;
    size_t url_size;
  } lazy; /**< store remote url for later use in case of lazy connection */
};

static void nm_ibverbs_rcache_getprops(puk_context_t context, struct nm_minidriver_properties_s*p_props);
static void nm_ibverbs_rcache_init(puk_context_t context, const void**p_url, size_t*p_url_size);
static void nm_ibverbs_rcache_close(puk_context_t context);
static void nm_ibverbs_rcache_connect(void*_status, const void*remote_url, size_t url_size);
static void nm_ibverbs_rcache_connect_async(void*_status, const void*remote_url, size_t url_size);
static void nm_ibverbs_rcache_connect_wait(void*_status);
static void nm_ibverbs_rcache_send_iov_prefetch(void*_status, const struct iovec*v, int n);
static void nm_ibverbs_rcache_send_iov_unfetch(void*_status, const struct iovec*v, int n);
static int  nm_ibverbs_rcache_send_iov_post(void*_status, const struct iovec*v, int n);
static int  nm_ibverbs_rcache_send_poll(void*_status);
static void nm_ibverbs_rcache_recv_iov_prefetch(puk_context_t context, const struct iovec*v, int n);
static void nm_ibverbs_rcache_recv_iov_unfetch(puk_context_t context, const struct iovec*v, int n);
static int  nm_ibverbs_rcache_recv_iov_post(void*_status, struct iovec*v, int n);
static int  nm_ibverbs_rcache_poll_one(void*_status);
static int  nm_ibverbs_rcache_get_rdv_data(void*_status, void*p_ptr, nm_len_t size);
static int  nm_ibverbs_rcache_set_rdv_data(void*_status, void*p_ptr, nm_len_t size);

static const struct nm_minidriver_iface_s nm_ibverbs_rcache_minidriver =
  {
    .getprops          = &nm_ibverbs_rcache_getprops,
    .init              = &nm_ibverbs_rcache_init,
    .close             = &nm_ibverbs_rcache_close,
    .connect           = &nm_ibverbs_rcache_connect,
    .connect_async     = &nm_ibverbs_rcache_connect_async,
    .connect_wait      = &nm_ibverbs_rcache_connect_wait,
    .disconnect        = NULL,
    .send_data_post    = NULL,
    .send_iov_prefetch = &nm_ibverbs_rcache_send_iov_prefetch,
    .send_iov_unfetch  = &nm_ibverbs_rcache_send_iov_unfetch,
    .send_iov_post     = &nm_ibverbs_rcache_send_iov_post,
    .send_poll         = &nm_ibverbs_rcache_send_poll,
    .recv_iov_prefetch = &nm_ibverbs_rcache_recv_iov_prefetch,
    .recv_iov_unfetch  = &nm_ibverbs_rcache_recv_iov_unfetch,
    .recv_iov_post     = &nm_ibverbs_rcache_recv_iov_post,
    .recv_data_post    = NULL,
    .recv_poll_one     = &nm_ibverbs_rcache_poll_one,
    .recv_cancel       = NULL,
    .get_rdv_data      = &nm_ibverbs_rcache_get_rdv_data,
    .set_rdv_data      = &nm_ibverbs_rcache_set_rdv_data
  };

static void*nm_ibverbs_rcache_instantiate(puk_instance_t instance, puk_context_t context);
static void nm_ibverbs_rcache_destroy(void*);

static const struct puk_component_driver_s nm_ibverbs_rcache_component =
  {
    .instantiate = &nm_ibverbs_rcache_instantiate,
    .destroy = &nm_ibverbs_rcache_destroy
  };


PADICO_MODULE_COMPONENT(NewMad_ibverbs_rcache,
  puk_component_declare("NewMad_ibverbs_rcache",
                        puk_component_provides("PadicoComponent", "component", &nm_ibverbs_rcache_component),
                        puk_component_provides("NewMad_minidriver", "minidriver", &nm_ibverbs_rcache_minidriver),
                        puk_component_uses("NewMad_ibverbs", "ibverbs")));

PADICO_MODULE_ATTR(rcache_checksum, "NMAD_RCACHE_CHECKSUM", "enable checksums in ibrcache driver (disabled by default)", bool, 0);
PADICO_MODULE_ATTR(rcache_nocache, "NMAD_RCACHE_NOCACHE", "do not cache memory registration in ibrache, use only for debug (default: off)", bool, 0);
PADICO_MODULE_ATTR(rcache_odp, "NMAD_RCACHE_ODP", "enable On-Demand Paging for memory registration in ibrcache (default: off)", bool, 0);
PADICO_MODULE_ATTR(rcache_iodp, "NMAD_RCACHE_IODP", "enable Implicit On-Demand Paging for memory registration in ibrcache (default: off)", bool, 0);


/* ********************************************************* */

static inline int nm_ibverbs_rcache_ready(struct nm_ibverbs_rcache_s*p_rcache)
{
  return (!p_rcache->p_rcache_context->lazy_connect) || nm_ibverbs_cnx_lazy_ready(p_rcache->p_cnx);
}

static inline nm_len_t nm_ibverbs_rcache_head_offset(const void*ptr)
{
  const uintptr_t base = (uintptr_t)ptr;
  const nm_len_t head_offset = NM_IBVERBS_RCACHE_ALIGN - 1 - ((base - 1) % NM_IBVERBS_RCACHE_ALIGN);
  assert((base + head_offset) % NM_IBVERBS_RCACHE_ALIGN == 0);
  assert(head_offset >= 0 && head_offset < NM_IBVERBS_RCACHE_ALIGN);
  return head_offset;
}

static void* nm_ibverbs_rcache_instantiate(puk_instance_t instance, puk_context_t context)
{
  struct nm_ibverbs_rcache_s*p_rcache = padico_malloc(sizeof(struct nm_ibverbs_rcache_s));
  struct nm_ibverbs_rcache_context_s*p_rcache_context = puk_context_get_status(context);
  p_rcache->p_rcache_context = p_rcache_context;
  memset(&p_rcache->send, 0, sizeof(p_rcache->send));
  memset(&p_rcache->recv, 0, sizeof(p_rcache->recv));
  p_rcache->p_cnx = NULL;
  p_rcache->send.rdv_data.raddr = 0;
  return p_rcache;
}

static void nm_ibverbs_rcache_destroy(void*_status)
{
  struct nm_ibverbs_rcache_s*p_rcache = _status;
  if(p_rcache->p_rcache_context->lazy_connect)
    {
      padico_free(p_rcache->lazy.p_remote_url);
    }
  padico_free(p_rcache);
}

/* *** rcache connection *********************************** */

static void nm_ibverbs_rcache_getprops(puk_context_t p_context, struct nm_minidriver_properties_s*p_props)
{
  struct nm_ibverbs_rcache_context_s*p_rcache_context = padico_malloc(sizeof(struct nm_ibverbs_rcache_context_s));
  puk_context_set_status(p_context, p_rcache_context);
  nm_ibverbs_rcache_entry_hashtable_init(&p_rcache_context->prefetched_mrs);
#ifdef DEBUG
  p_rcache_context->do_crc = padico_module_attr_rcache_checksum_getvalue();
  if(p_rcache_context->do_crc)
    {
      puk_checksum_t p_checksum = puk_checksum_get("default");
      p_rcache_context->p_crc_func = p_checksum->func;
      NM_WARN("checksum enabled (%s).\n", p_checksum->name);
    }
#else
  if(padico_module_attr_rcache_checksum_getvalue())
    {
      NM_WARN("cannot use option NMAD_RCACHE_CHECKSUM in non-debug build.\n");
    }
#endif /* DEBUG */
  struct nm_ibverbs_opts_s ib_opts;
  nm_ibverbs_context_default_opts(&ib_opts); /* get default options, then tune MTU */
  ib_opts.mtu = IBV_MTU_4096; /* enforce large MTU for large messages */
  p_rcache_context->p_ibverbs_context = nm_ibverbs_context_resolve_opts(p_context, &ib_opts);
  nm_ibverbs_context_get_profile(p_rcache_context->p_ibverbs_context, &p_props->profile);
  p_props->capabilities.supports_data = 0;
  p_props->capabilities.supports_iovec = 0;
  p_props->capabilities.supports_send_prefetch = 1;
  p_props->capabilities.supports_recv_prefetch = 1;
  p_props->capabilities.trk_rdv = 1;
  p_props->capabilities.needs_rdv_data = 1;
  p_props->capabilities.max_msg_size = p_rcache_context->p_ibverbs_context->hca.ib_caps.max_msg_size;
  p_props->nickname = "ibverbs_rcache";
}

static void nm_ibverbs_rcache_init(puk_context_t context, const void**p_url, size_t*p_url_size)
{
  struct nm_ibverbs_rcache_context_s*p_rcache_context = puk_context_get_status(context);

  if(padico_module_attr_rcache_nocache_getvalue())
    {
      p_rcache_context->p_reg_driver = &nm_ibverbs_reg_driver_nocache;
    }
  else if(padico_module_attr_rcache_odp_getvalue())
    {
#if defined(NMAD_IBVERBS_ODP)
      p_rcache_context->p_reg_driver = &nm_ibverbs_reg_driver_odp;
#else
      NM_FATAL("odp not supported.\n");
#endif
    }
  else if(padico_module_attr_rcache_iodp_getvalue())
    {
#if defined(NMAD_IBVERBS_IODP)
      p_rcache_context->p_reg_driver = &nm_ibverbs_reg_driver_iodp;
#else
      NM_FATAL("iodp not supported.\n");
#endif
    }
  else
    {
      p_rcache_context->p_reg_driver = &nm_ibverbs_reg_driver_mini;
    }

  if(p_rcache_context->p_reg_driver->init)
    {
      (*p_rcache_context->p_reg_driver->init)(p_rcache_context->p_ibverbs_context);
    }
  nm_ibverbs_regpool_init(&p_rcache_context->sigpkt_regpool, p_rcache_context->p_ibverbs_context, sizeof(struct nm_ibverbs_rcache_sigpkt_s), 16);

  p_rcache_context->vector_connect = 0;
  p_rcache_context->lazy_connect = 0;
  const char*s_session_size = puk_context_getattr(context, "session_size");
  const char*s_rank = puk_context_getattr(context, "rank");
  const char*s_wide_url_support = puk_context_getattr(context, "wide_url_support");
  if((s_session_size != NULL) && (s_rank != NULL) && (s_wide_url_support != NULL))
    {
      const int wide_url_support = puk_opt_parse_bool(s_wide_url_support);
      if(wide_url_support)
        {
          p_rcache_context->vector_connect = 1;
        }
    }
  if(nm_ibverbs_cnx_lazy_enabled())
    {
      p_rcache_context->vector_connect = 0;
      NM_WARN("using lazy connect.\n");
      p_rcache_context->lazy_connect = 1;
      p_rcache_context->p_ibverbs_context->ib_opts.lazy_connect = 1;
    }
  if(p_rcache_context->vector_connect)
    {
      const int session_size = atoi(s_session_size);
      const int rank = atoi(s_rank);
      NM_DISPF("using vector connect; session_size = %d; rank = %d\n", session_size, rank);
      nm_ibverbs_context_get_wide_url(p_rcache_context->p_ibverbs_context, session_size, rank, p_url, p_url_size);
    }
  else
    {
      nm_ibverbs_context_get_url(p_rcache_context->p_ibverbs_context, p_url, p_url_size);
    }
}

static void nm_ibverbs_rcache_close(puk_context_t context)
{
  struct nm_ibverbs_rcache_context_s*p_rcache_context = puk_context_get_status(context);
  nm_ibverbs_rcache_entry_hashtable_destroy(&p_rcache_context->prefetched_mrs);
  if(p_rcache_context->p_reg_driver->close)
    {
      (*p_rcache_context->p_reg_driver->close)(p_rcache_context->p_ibverbs_context);
    }
  nm_ibverbs_regpool_destroy(&p_rcache_context->sigpkt_regpool);
}

static void nm_ibverbs_rcache_connect(void*_status, const void*p_remote_url, size_t url_size)
{
  nm_ibverbs_rcache_connect_async(_status, p_remote_url, url_size);
  nm_ibverbs_rcache_connect_wait(_status);
}

static void nm_ibverbs_rcache_connect_async(void*_status, const void*p_remote_url, size_t url_size)
{
  struct nm_ibverbs_rcache_s*p_rcache = _status;
  struct nm_ibverbs_rcache_context_s*p_rcache_context = p_rcache->p_rcache_context;
  if(p_rcache_context->lazy_connect)
    {
      p_rcache->p_cnx = NULL;
      p_rcache->lazy.url_size = url_size;
      p_rcache->lazy.p_remote_url = padico_malloc(url_size);
      memcpy(p_rcache->lazy.p_remote_url, p_remote_url, url_size);
    }
  else if(p_rcache_context->vector_connect)
    {
      p_rcache->p_cnx = nm_ibverbs_cnx_vector_connect(p_rcache_context->p_ibverbs_context, p_remote_url, url_size);
      /* no need to wait for connection to be ready since synchronization
       * is already ensured by the rdv */
    }
  else
    {
      p_rcache->p_cnx = nm_ibverbs_cnx_create(p_rcache_context->p_ibverbs_context);
      nm_ibverbs_cnx_connect_async(p_rcache->p_cnx, p_remote_url, url_size);
    }
}

static void nm_ibverbs_rcache_connect_wait(void*_status)
{
  struct nm_ibverbs_rcache_s*p_rcache = _status;
  struct nm_ibverbs_rcache_context_s*p_rcache_context = p_rcache->p_rcache_context;
  /* in case of vectored connection, no need to wait for addresses */
  if( (!p_rcache_context->vector_connect) && (!p_rcache_context->lazy_connect) )
    {
      nm_ibverbs_cnx_connect_wait(p_rcache->p_cnx);
    }
}


/* ** reg cache I/O **************************************** */

static void nm_ibverbs_rcache_send_register(struct nm_ibverbs_rcache_s*p_status, const struct iovec*v, int n,
                                            struct nm_ibverbs_rcache_entry_s*p_entry)
{
  char*message = v[0].iov_base;
  const nm_len_t size = v[0].iov_len;
  const nm_len_t head_offset = nm_ibverbs_rcache_head_offset(message);
  (*p_status->p_rcache_context->p_reg_driver->mem_reg)(p_status->p_rcache_context->p_ibverbs_context,
                                                       message + head_offset, size - head_offset,
                                                       &p_entry->mr, &p_entry->p_reg_handle);
}

static void nm_ibverbs_rcache_send_iov_prefetch(void*_status, const struct iovec*v, int n)
{
  NM_TRACEF("prefetch ptr = %p; len = %lu\n", v[0].iov_base, v[0].iov_len);
  struct nm_ibverbs_rcache_s*p_status = _status;
  struct nm_ibverbs_rcache_context_s*p_rcache_context = p_status->p_rcache_context;
  if(p_status->p_cnx == NULL)
    {
      nm_ibverbs_cnx_lazy_create(p_status->p_rcache_context->p_ibverbs_context, &p_status->p_cnx,
                                 p_status->lazy.p_remote_url, p_status->lazy.url_size);
    }
  struct nm_ibverbs_rcache_entry_s*p_entry = padico_malloc(sizeof(struct nm_ibverbs_rcache_entry_s));
  assert(n > 0);
  p_entry->key.v = v;
  p_entry->key.n = n;
  nm_ibverbs_rcache_send_register(p_status, v, n, p_entry);
  nm_ibverbs_rcache_entry_hashtable_insert(&p_rcache_context->prefetched_mrs, &p_entry->key, p_entry);
}

static void nm_ibverbs_rcache_send_iov_unfetch(void*_status, const struct iovec*v, int n)
{
  NM_TRACEF("unfetch ptr = %p; len = %lu\n", v[0].iov_base, v[0].iov_len);
  struct nm_ibverbs_rcache_s*p_status = _status;
  struct nm_ibverbs_rcache_context_s*p_rcache_context = p_status->p_rcache_context;
  struct nm_prefetch_key_s key = { .v = v, .n = n} ;
  struct nm_ibverbs_rcache_entry_s*p_entry =
    nm_ibverbs_rcache_entry_hashtable_lookup(&p_status->p_rcache_context->prefetched_mrs, &key);
  if(p_entry == NULL)
    {
      NM_FATAL("cannot find MR entry to unfetch.\n");
    }
  char*message = p_entry->key.v[0].iov_base;
  const nm_len_t size = p_entry->key.v[0].iov_len;
  const nm_len_t head_offset = nm_ibverbs_rcache_head_offset(message);
  (*p_status->p_rcache_context->p_reg_driver->mem_unreg)(p_rcache_context->p_ibverbs_context,
                                                         message + head_offset,
                                                         size - head_offset,
                                                         p_entry->mr, p_entry->p_reg_handle);
  nm_ibverbs_rcache_entry_hashtable_remove(&p_status->p_rcache_context->prefetched_mrs, &key);
  padico_free(p_entry);
}

static int nm_ibverbs_rcache_set_rdv_data(void*_status, void*p_ptr, nm_len_t size)
{
  struct nm_ibverbs_rcache_s*p_rcache = _status;
  assert(p_rcache->send.message == NULL);
  if(p_rcache->send.rdv_data.raddr != 0)
    return -NM_EINVAL;
  struct nm_ibverbs_rcache_rdv_token_s*p_rdv = p_ptr;
  p_rcache->send.rdv_data = *p_rdv;
  return NM_ESUCCESS;
}

static int nm_ibverbs_rcache_send_iov_post(void*_status, const struct iovec*v, int n)
{
  NM_TRACEF("post send ptr = %p; len = %lu\n", v[0].iov_base, v[0].iov_len);
  struct nm_ibverbs_rcache_s*p_rcache = _status;
  char*message = v[0].iov_base;
  const nm_len_t size = v[0].iov_len;
  assert(n == 1);
  if(size < 2 * NM_IBVERBS_RCACHE_ALIGN)
    return -NM_EINVAL;
  if(p_rcache->send.rdv_data.raddr == 0)
    return -NM_EINVAL;
  if(p_rcache->send.message != NULL)
    {
      NM_FATAL("rendez-vous already posted on sender side.\n");
    }
  p_rcache->send.seq++;
  assert(p_rcache->send.seq == p_rcache->send.rdv_data.seq);
  p_rcache->send.message = message;
  p_rcache->send.size = size;
#ifdef DEBUG
  if(p_rcache->p_rcache_context->do_crc)
    {
      p_rcache->send.crc = (*p_rcache->p_rcache_context->p_crc_func)(message, size);
    }
#endif /* DEBUG */
  NM_RCACHE_LOG("rcache = %p; seq = %d\n", p_rcache, p_rcache->send.seq);

  /* lazy connection */
  if(p_rcache->p_cnx == NULL)
    {
      nm_ibverbs_cnx_lazy_create(p_rcache->p_rcache_context->p_ibverbs_context, &p_rcache->p_cnx,
                                 p_rcache->lazy.p_remote_url, p_rcache->lazy.url_size);
    }

  /* allocate ssigpkt */
  assert(p_rcache->send.p_ssigpkt == NULL);
  assert(p_rcache->send.p_ssigpkt_entry == NULL);
  p_rcache->send.p_ssigpkt_entry = nm_ibverbs_regpool_alloc_entry(&p_rcache->p_rcache_context->sigpkt_regpool);
  p_rcache->send.p_ssigpkt = p_rcache->send.p_ssigpkt_entry->p_ptr;
  /* prefetch head */
  const nm_len_t head_offset = nm_ibverbs_rcache_head_offset(message);
  memcpy(&p_rcache->send.p_ssigpkt->head[0], message, head_offset);
  p_rcache->send.p_ssigpkt->sighdr.head_offset = head_offset;
  struct nm_prefetch_key_s key = { .v = v, .n = n} ;
  struct nm_ibverbs_rcache_entry_s*p_entry =
    nm_ibverbs_rcache_entry_hashtable_lookup(&p_rcache->p_rcache_context->prefetched_mrs, &key);
  if(p_entry == NULL)
    {
      struct nm_ibverbs_rcache_entry_s mr_entry;
      nm_ibverbs_rcache_send_register(p_rcache, v, n, &mr_entry);
      p_rcache->send.mr = mr_entry.mr;
      p_rcache->send.p_reg_handle = mr_entry.p_reg_handle;
    }
  else
    {
      nm_ibverbs_rcache_entry_hashtable_remove(&p_rcache->p_rcache_context->prefetched_mrs, &key);
      p_rcache->send.mr = p_entry->mr;
      p_rcache->send.p_reg_handle = p_entry->p_reg_handle;
      padico_free(p_entry);
    }
  /* send data */
  p_rcache->send.posted = 0;
  if(nm_ibverbs_rcache_ready(p_rcache))
    {
      int rc = nm_ibverbs_do_rdma(p_rcache->p_cnx,
                                  p_rcache->send.message + head_offset,
                                  p_rcache->send.size - head_offset,
                                  p_rcache->send.rdv_data.raddr + head_offset, IBV_WR_RDMA_WRITE, IBV_SEND_SIGNALED,
                                  p_rcache->send.mr->lkey, p_rcache->send.rdv_data.rkey, NM_IBVERBS_WRID_DATA);
      p_rcache->send.posted = 1;
      return rc;
    }
  else
    {
      nm_ibverbs_cnx_lazy_progress(p_rcache->p_rcache_context->p_ibverbs_context);
      return NM_ESUCCESS;
    }
}

static int nm_ibverbs_rcache_send_poll(void*_status)
{
  struct nm_ibverbs_rcache_s*p_rcache = _status;

  if(!nm_ibverbs_rcache_ready(p_rcache))
    {
      nm_ibverbs_cnx_lazy_progress(p_rcache->p_rcache_context->p_ibverbs_context);
      return -NM_EAGAIN;
    }
  if(!p_rcache->send.posted)
    {
      const nm_len_t head_offset = nm_ibverbs_rcache_head_offset(p_rcache->send.message);
      int rc = nm_ibverbs_do_rdma(p_rcache->p_cnx,
                                  p_rcache->send.message + head_offset,
                                  p_rcache->send.size - head_offset,
                                  p_rcache->send.rdv_data.raddr + head_offset, IBV_WR_RDMA_WRITE, IBV_SEND_SIGNALED,
                                  p_rcache->send.mr->lkey, p_rcache->send.rdv_data.rkey, NM_IBVERBS_WRID_DATA);
      p_rcache->send.posted = 1;
      if(rc != NM_ESUCCESS)
        return rc;
    }

  /* test rdma write completion */
  int rc = nm_ibverbs_send_poll(p_rcache->p_cnx, NM_IBVERBS_WRID_DATA);
  if(rc != NM_ESUCCESS)
    return rc;

  /* send signal header */
  p_rcache->send.p_ssigpkt->sighdr.seq = p_rcache->send.seq;
  p_rcache->send.p_ssigpkt->sighdr.busy = 1;
#ifdef DEBUG
  if(p_rcache->p_rcache_context->do_crc)
    {
      const uint32_t crc = (*p_rcache->p_rcache_context->p_crc_func)(p_rcache->send.message, p_rcache->send.size);
      if(crc != p_rcache->send.crc)
        {
          NM_FATAL("inconsistency detected in data between send_iov_post() and send_poll(); crc_post = 0x%x; crc_poll = 0x%x\n", p_rcache->send.crc, crc);
        }
      p_rcache->send.p_ssigpkt->sighdr.crc = crc;
    }
#endif /* DEBUG */
  rc = nm_ibverbs_do_rdma(p_rcache->p_cnx,
                          p_rcache->send.p_ssigpkt,
                          sizeof(struct nm_ibverbs_rcache_sigpkt_s),
                          p_rcache->send.rdv_data.sigpkt_raddr, IBV_WR_RDMA_WRITE, IBV_SEND_SIGNALED,
                          p_rcache->send.p_ssigpkt_entry->p_mr->lkey,
                          p_rcache->send.rdv_data.sigpkt_rkey,
                          NM_IBVERBS_WRID_HEADER);
  if(rc != NM_ESUCCESS)
    return rc;
  rc = nm_ibverbs_send_flush(p_rcache->p_cnx, NM_IBVERBS_WRID_HEADER);
  if(rc != NM_ESUCCESS)
    return rc;
  const nm_len_t head_offset = p_rcache->send.p_ssigpkt->sighdr.head_offset;
  (*p_rcache->p_rcache_context->p_reg_driver->mem_unreg)(p_rcache->p_rcache_context->p_ibverbs_context,
                                                         p_rcache->send.message + head_offset,
                                                         p_rcache->send.size - head_offset,
                                                         p_rcache->send.mr, p_rcache->send.p_reg_handle);
  NM_TRACEF("send SUCCESS ptr = %p; len = %lu\n", p_rcache->send.message, p_rcache->send.size);
  p_rcache->send.mr = NULL;
  p_rcache->send.p_reg_handle = NULL;
  p_rcache->send.message = NULL;
  p_rcache->send.size    = -1;
  p_rcache->send.rdv_data.raddr = 0;
  nm_ibverbs_regpool_free_entry(&p_rcache->p_rcache_context->sigpkt_regpool, p_rcache->send.p_ssigpkt_entry);
  p_rcache->send.p_ssigpkt = NULL;
  p_rcache->send.p_ssigpkt_entry = NULL;
  return NM_ESUCCESS;
}

static void nm_ibverbs_rcache_recv_register(struct nm_ibverbs_rcache_context_s*p_rcache_context, const struct iovec*v, int n,
                                            struct nm_ibverbs_rcache_entry_s*p_entry)
{
  char*message = v[0].iov_base;
  const nm_len_t size = v[0].iov_len;
  (*p_rcache_context->p_reg_driver->mem_reg)(p_rcache_context->p_ibverbs_context,
                                             message, size,
                                             &p_entry->mr, &p_entry->p_reg_handle);
}

static void nm_ibverbs_rcache_recv_iov_prefetch(puk_context_t context, const struct iovec*v, int n)
{
  NM_TRACEF("prefetch ptr = %p; len = %lu; v = %p\n", v[0].iov_base, v[0].iov_len, v);
  struct nm_ibverbs_rcache_context_s*p_rcache_context = puk_context_get_status(context);
  struct nm_ibverbs_rcache_entry_s*p_entry = padico_malloc(sizeof(struct nm_ibverbs_rcache_entry_s));
  assert(n > 0);
  p_entry->key.v = v;
  p_entry->key.n = n;
  nm_ibverbs_rcache_recv_register(p_rcache_context, v, n, p_entry);
  nm_ibverbs_rcache_entry_hashtable_insert(&p_rcache_context->prefetched_mrs, &p_entry->key, p_entry);
}

static void nm_ibverbs_rcache_recv_iov_unfetch(puk_context_t context, const struct iovec*v, int n)
{

  NM_TRACEF("unfetch ptr = %p; len = %lu; v = %p\n", v[0].iov_base, v[0].iov_len, v);
  struct nm_ibverbs_rcache_context_s*p_rcache_context = puk_context_get_status(context);
  struct nm_prefetch_key_s key = { .v = v, .n = n} ;
  struct nm_ibverbs_rcache_entry_s*p_entry =
    nm_ibverbs_rcache_entry_hashtable_lookup(&p_rcache_context->prefetched_mrs, &key);
  if(p_entry == NULL)
    {
      NM_FATAL("cannot find MR entry to unfetch.\n");
    }
  char*message = p_entry->key.v[0].iov_base;
  const nm_len_t size = p_entry->key.v[0].iov_len;
  (*p_rcache_context->p_reg_driver->mem_unreg)(p_rcache_context->p_ibverbs_context,
                                               message, size,
                                               p_entry->mr, p_entry->p_reg_handle);
  nm_ibverbs_rcache_entry_hashtable_remove(&p_rcache_context->prefetched_mrs, &key);
  padico_free(p_entry);
}

static int nm_ibverbs_rcache_recv_iov_post(void*_status, struct iovec*v, int n)
{
  NM_TRACEF("post receive ptr = %p; len = %lu; v = %p\n", v[0].iov_base, v[0].iov_len, v);
  struct nm_ibverbs_rcache_s*p_rcache = _status;
  if(p_rcache->p_cnx == NULL)
    {
      nm_ibverbs_cnx_lazy_create(p_rcache->p_rcache_context->p_ibverbs_context, &p_rcache->p_cnx,
                                 p_rcache->lazy.p_remote_url, p_rcache->lazy.url_size);
    }
  if(p_rcache->recv.message != NULL)
    {
      NM_FATAL("rendez-vous already posted on receiver side.\n");
    }
  p_rcache->recv.seq++;
  p_rcache->recv.message = v->iov_base;
  p_rcache->recv.size = v->iov_len;
  struct nm_prefetch_key_s key = { .v = v, .n = n} ;
  struct nm_ibverbs_rcache_entry_s*p_entry =
    nm_ibverbs_rcache_entry_hashtable_lookup(&p_rcache->p_rcache_context->prefetched_mrs, &key);

  if(p_entry == NULL)
    {
      struct nm_ibverbs_rcache_entry_s mr_entry;
      nm_ibverbs_rcache_recv_register(p_rcache->p_rcache_context, v, n, &mr_entry);
      p_rcache->recv.mr = mr_entry.mr;
      p_rcache->recv.p_reg_handle = mr_entry.p_reg_handle;
      NM_TRACEF("prefetch cache MISS ptr = %p; len = %lu\n", v[0].iov_base, v[0].iov_len);
    }
  else
    {
      nm_ibverbs_rcache_entry_hashtable_remove(&p_rcache->p_rcache_context->prefetched_mrs, &key);
      p_rcache->recv.mr = p_entry->mr;
      p_rcache->recv.p_reg_handle = p_entry->p_reg_handle;
      padico_free(p_entry);
      NM_TRACEF("prefetch cache HIT ptr = %p; len = %lu\n", v[0].iov_base, v[0].iov_len);
    }
  NM_RCACHE_LOG("rcache = %p; seq = %d; addr = %p; key = %d\n",
                p_rcache, p_rcache->recv.seq, p_rcache->recv.message, (int)p_rcache->recv.mr->rkey);
  return NM_ESUCCESS;
}

static int nm_ibverbs_rcache_get_rdv_data(void*_status, void*p_ptr, nm_len_t size)
{
  struct nm_ibverbs_rcache_s*p_rcache = _status;
  assert(p_rcache->recv.message != NULL);
  assert(p_rcache->recv.p_rsigpkt == NULL);
  p_rcache->recv.p_rsigpkt_entry = nm_ibverbs_regpool_alloc_entry(&p_rcache->p_rcache_context->sigpkt_regpool);
  p_rcache->recv.p_rsigpkt = p_rcache->recv.p_rsigpkt_entry->p_ptr;
  p_rcache->recv.p_rsigpkt->sighdr.busy = 0;
  struct nm_ibverbs_rcache_rdv_token_s*p_rdv = p_ptr;
  p_rdv->raddr = (uintptr_t)p_rcache->recv.message;
  p_rdv->rkey  = p_rcache->recv.mr->rkey;
  p_rdv->sigpkt_raddr = (uintptr_t)p_rcache->recv.p_rsigpkt;
  p_rdv->sigpkt_rkey  = p_rcache->recv.p_rsigpkt_entry->p_mr->rkey;
  p_rdv->seq   = p_rcache->recv.seq;
  return NM_ESUCCESS;
}

static int nm_ibverbs_rcache_poll_one(void*_status)
{
  struct nm_ibverbs_rcache_s*p_rcache = _status;
  /* lazy connect */
  if(!nm_ibverbs_rcache_ready(p_rcache))
    {
      nm_ibverbs_cnx_lazy_progress(p_rcache->p_rcache_context->p_ibverbs_context);
      return -NM_EAGAIN;
    }
  /* wait for sighdr */
  struct nm_ibverbs_rcache_sighdr_s*p_rsig = &p_rcache->recv.p_rsigpkt->sighdr;
  if(p_rsig->busy)
    {
      assert(p_rsig->busy == 1);
      const nm_len_t head_offset = p_rsig->head_offset;
      memcpy(p_rcache->recv.message, &p_rcache->recv.p_rsigpkt->head[0], head_offset);
#ifdef DEBUG
      if(p_rcache->p_rcache_context->do_crc)
        {
          const uint32_t crc = (*p_rcache->p_rcache_context->p_crc_func)(p_rcache->recv.message, p_rcache->recv.size);
          if(crc != p_rsig->crc)
            {
              NM_FATAL("inconsistency detected in data between sender and receiver; crc_send = 0x%x; recv_crc = 0x%x\n", p_rsig->crc, crc);
            }
        }
#endif /* DEBUG */
      assert(p_rsig->seq == p_rcache->recv.seq);
      /* unregister */
      (*p_rcache->p_rcache_context->p_reg_driver->mem_unreg)(p_rcache->p_rcache_context->p_ibverbs_context,
                                                             p_rcache->recv.message,
                                                             p_rcache->recv.size,
                                                             p_rcache->recv.mr, p_rcache->recv.p_reg_handle);
    }
  else
    {
      return -NM_EAGAIN;
    }
  NM_TRACEF("recv SUCCESS ptr = %p; len = %lu\n", p_rcache->recv.message, p_rcache->recv.size);
  /* cleanup */
  p_rcache->recv.mr = NULL;
  p_rcache->recv.p_reg_handle = NULL;
  p_rcache->recv.message = NULL;
  p_rcache->recv.size = -1;
  nm_ibverbs_regpool_free_entry(&p_rcache->p_rcache_context->sigpkt_regpool, p_rcache->recv.p_rsigpkt_entry);
  p_rcache->recv.p_rsigpkt = NULL;
  p_rcache->recv.p_rsigpkt_entry = NULL;
  return NM_ESUCCESS;
}

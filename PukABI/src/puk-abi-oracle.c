/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file Miscellaneous oracles for number of args of variadic functions
 * @ingroup PukABI
 */

#include <stdio.h>
#include <unistd.h>
#include <poll.h>
#include <fcntl.h>
#include <string.h>

#include "Puk-ABI.h"

/** return the number of arguments for fcntl */
int puk_abi_oracle__fcntl(int cmd)
{
  int o = -1;
  switch(cmd)
    {
    case F_DUPFD:
    case F_GETFD:
    case F_GETFL:
      o = 0;
      break;

    case F_SETFD:
    case F_SETFL:
      o = 1;
      break;

    case F_GETLK:
    case F_SETLK:
    case F_SETLKW:
#if defined F_GETLK64 && F_GETLK64 != F_GETLK
    case F_GETLK64:
    case F_SETLK64:
    case F_SETLKW64:
#endif
      o = 2;
      break;

    default:
      puk_abi_print("PukABI: warning- fcntl() called with unsupported option %d", cmd);
      o = 2;
      break;
    }
  return o;
}

static inline int strprefix(const char*s1, const char*s2)
{
  return strncmp(s1, s2, strlen(s2));
}

/** return the kind of fd to use according to filename */
int puk_abi_oracle__open_kind(const char*path)
{
  if( (strprefix(path, "/dev/") == 0) ||
      (strprefix(path, "/sys/") == 0) ||
      (strprefix(path, "/proc/") == 0) ||
      (strprefix(path, "/run/shm/") == 0) )
    {
      puk_abi_trace("PukABI: use **real** for path %s", path);
      return 1;
    }
  else
    {
      puk_abi_trace("PukABI: use default for path %s", path);
      return 0;
    }
}
/** return whether to use 3 parameters (creation mode) or 2 for open() */
int puk_abi_oracle__open_flags(int oflag)
{
  return(oflag & O_CREAT);
}

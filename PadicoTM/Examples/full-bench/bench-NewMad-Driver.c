/** @file bench-NewMad-Driver.c
 */

#include "bench-common.h"
#include <Padico/Module.h>
#include <Padico/Puk.h>
#include <nm_public.h>
#include <nm_sendrecv_interface.h>
#include <nm_session_interface.h>
#include <nm_launcher_interface.h>

#include <nm_private.h>

int bench_nmaddriver_init(void);
int bench_nmaddriver_run(int argc, char**argv);

PADICO_MODULE_DECLARE(bench_NewMad_Driver, bench_nmaddriver_init, bench_nmaddriver_run, NULL);

static nm_gate_t peer_gate = NULL;
static nm_session_t p_session = NULL;

static nm_gate_t p_gate = NULL;
static nm_drv_t p_drv = NULL;
static struct nm_gate_drv*p_gdrv = NULL;
static struct puk_receptacle_NewMad_Driver_s*driver = NULL;

int bench_nmaddriver_init(void)
{
  int fake_argc = 1;
  char*fake_argv[] = { "bench-NewMad", NULL };
  puk_component_t launcher = puk_component_resolve("NewMad_Launcher_newmadico");
  puk_instance_t instance = puk_component_instantiate(launcher);
  struct puk_receptacle_NewMad_Launcher_s r;
  puk_instance_indirect_NewMad_Launcher(instance, NULL, &r);
  (*r.driver->init)(r._status, &fake_argc, fake_argv, "bench-common");
  int rank = (*r.driver->get_rank)(r._status);
  int size = (*r.driver->get_size)(r._status);
  p_session = (*r.driver->get_session)(r._status);
  nm_gate_t*gates = padico_malloc(size * sizeof(nm_gate_t));
  (*r.driver->get_gates)(r._status, gates);
  int peer = (rank % 2 == 0) ? (rank + 1) : (rank - 1);
  assert(peer >= 0 && peer < size);
  peer_gate = gates[peer];

  padico_print("bench-NewMad-Driver: size = %d; rank = %d; peer_gate = %p\n", size, rank, peer_gate);


  /* take over the driver */
  p_gate = peer_gate;
  p_drv = nm_drv_default(p_gate);
  p_gdrv = nm_gate_drv_get(p_gate, p_drv);
  driver = &p_gdrv->receptacle;
  nm_core_schedopt_disable(p_drv->p_core);

  /* hack here-
   * make sure the peer node has flushed its pending recv requests,
   * so that the pw we send are not processed by schedopt.
   */
  usleep(500 * 1000);

  return 0;
}

void nmaddriver_server(char*buf, size_t len)
{
  struct nm_pkt_wrap_s*p_pw = NULL;
  nmad_lock();
  nm_so_pw_alloc(NM_PW_NOHEADER, &p_pw);
  nmad_unlock();
  nm_so_pw_add_raw(p_pw, buf, len, 0);
  nm_so_pw_assign(p_pw, NM_TRK_SMALL, p_drv, p_gate);

  int err = driver->driver->post_send_iov(driver->_status, p_pw);
  while(err == -NM_EAGAIN)
    {
      err = driver->driver->poll_send_iov(driver->_status, p_pw);
    }
  if(err != NM_ESUCCESS)
    abort();
  err = driver->driver->post_recv_iov(driver->_status, p_pw);
  while(err == -NM_EAGAIN)
    {
      err = driver->driver->poll_recv_iov(driver->_status, p_pw);
    }
  if(err != NM_ESUCCESS)
    abort();
  nm_so_pw_free(p_pw);
}

void nmaddriver_client(char*buf, size_t len)
{
  struct nm_pkt_wrap_s*p_pw = NULL;
  nmad_lock();
  nm_so_pw_alloc(NM_PW_NOHEADER, &p_pw);
  nmad_unlock();
  nm_so_pw_add_raw(p_pw, buf, len, 0);
  nm_so_pw_assign(p_pw, NM_TRK_SMALL, p_drv, p_gate);

  int err = driver->driver->post_recv_iov(driver->_status, p_pw);
  while(err == -NM_EAGAIN)
    {
      err = driver->driver->poll_recv_iov(driver->_status, p_pw);
    }
  if(err != NM_ESUCCESS)
    abort();
  err = driver->driver->post_send_iov(driver->_status, p_pw);
  while(err == -NM_EAGAIN)
    {
      err = driver->driver->poll_send_iov(driver->_status, p_pw);
    }
  if(err != NM_ESUCCESS)
    abort();
  nm_so_pw_free(p_pw);
}

static const struct bench_desc_s nmaddriver_bench =
{
  "NewMad-Driver",
  &nmaddriver_server,
  &nmaddriver_client
};

int bench_nmaddriver_run(int argc, char**argv)
{
  the_bench(&nmaddriver_bench);
  return 0;
}

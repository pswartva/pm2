/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include "nm_ibverbs.h"
#include "nm_ibverbs_rcache.h"
#include <malloc.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/resource.h>

#ifdef PUKABI
#include <Padico/Puk-ABI.h>
#endif

#include <Padico/Module.h>

PADICO_MODULE_BUILTIN(nm_ibverbs_rcache_mini, NULL, NULL, NULL);

#ifdef PIOMAN
#include <pioman.h>
#define nm_ibverbs_mini_spinlock_t   piom_spinlock_t
#define nm_ibverbs_mini_spin_init    piom_spin_init
#define nm_ibverbs_mini_spin_destroy piom_spin_destroy
#define nm_ibverbs_mini_spin_lock    piom_spin_lock
#define nm_ibverbs_mini_spin_unlock  piom_spin_unlock
#define nm_ibverbs_mini_spin_assert_locked piom_spin_assert_locked
#define nm_ibverbs_mini_spin_assert_notlocked piom_spin_assert_notlocked
#else /* PIOMAN */
#include <pthread.h>
/* minimal locking for MPI_THREAD_FUNNELED */
typedef pthread_spinlock_t nm_ibverbs_mini_spinlock_t;
static inline void nm_ibverbs_mini_spin_ignore(nm_ibverbs_mini_spinlock_t*p_spin)
{ }
static inline void nm_ibverbs_mini_spin_init(nm_ibverbs_mini_spinlock_t*p_lock)
{
  pthread_spin_init(p_lock, 0);
}
#define nm_ibverbs_mini_spin_destroy pthread_spin_destroy
#define nm_ibverbs_mini_spin_lock    pthread_spin_lock
#define nm_ibverbs_mini_spin_unlock  pthread_spin_unlock
#define nm_ibverbs_mini_spin_assert_locked    nm_ibverbs_mini_spin_ignore
#define nm_ibverbs_mini_spin_assert_notlocked nm_ibverbs_mini_spin_ignore

/* no locking at all, for MPI_THREAD_SINGLE */
/*
typedef int nm_ibverbs_mini_spinlock_t;
static inline void nm_ibverbs_mini_spin_ignore(nm_ibverbs_mini_spinlock_t*p_spin)
{ }
#define nm_ibverbs_mini_spin_init    nm_ibverbs_mini_spin_ignore
#define nm_ibverbs_mini_spin_destroy nm_ibverbs_mini_spin_ignore
#define nm_ibverbs_mini_spin_lock    nm_ibverbs_mini_spin_ignore
#define nm_ibverbs_mini_spin_unlock  nm_ibverbs_mini_spin_ignore
#define nm_ibverbs_mini_spin_assert_locked    nm_ibverbs_mini_spin_ignore
#define nm_ibverbs_mini_spin_assert_notlocked nm_ibverbs_mini_spin_ignore
*/
#endif /* PIOMAN */

static void nm_ibverbs_mini_mem_init(struct nm_ibverbs_context_s*p_ibverbs_context);
static void nm_ibverbs_mini_mem_close(struct nm_ibverbs_context_s*p_ibverbs_context);
static void nm_ibverbs_mini_mem_reg(struct nm_ibverbs_context_s*p_ibverbs_context, const void*ptr, size_t len,
                                    struct ibv_mr**pp_mr, void**pp_handle);
static void nm_ibverbs_mini_mem_unreg(struct nm_ibverbs_context_s*p_ibverbs_context, const void*ptr, size_t len,
                                      struct ibv_mr*p_mr, void*p_handle);

const struct nm_ibverbs_reg_driver_s nm_ibverbs_reg_driver_mini =
  {
    .init      = &nm_ibverbs_mini_mem_init,
    .close     = &nm_ibverbs_mini_mem_close,
    .mem_reg   = &nm_ibverbs_mini_mem_reg,
    .mem_unreg = &nm_ibverbs_mini_mem_unreg
  };


/* ********************************************************* */

PUK_LIST_DECLARE_TYPE(nm_ibverbs_mini_entry);

struct nm_ibverbs_mini_entry_s
{
  struct puk_itree_node_s itree;
  const void*ptr;     /**< pointer to the resgistered data; NULL for free slot */
  size_t len;         /**< length of registered data */
  int refcount;       /**< number of uses of this entry */
  struct ibv_mr*p_mr; /**< pointer to the ibverbs MR structure */
  struct nm_ibverbs_context_s*p_ibverbs_context; /**< the context this entry belong to */
  PUK_LIST_LINK(nm_ibverbs_mini_entry); /**< link for LRU list*/
};

PUK_LIST_CREATE_FUNCS(nm_ibverbs_mini_entry);

PUK_ALLOCATOR_TYPE_SINGLE(nm_ibverbs_mini_entries, struct nm_ibverbs_mini_entry_s);

static struct
{
  struct puk_itree_s entries; /**< interval tree containing rcache entries */
  struct nm_ibverbs_mini_entries_allocator_s allocator;
  struct nm_ibverbs_mini_entry_list_s lru_list; /**< list of entries not used anymore but kept in cache, in LRU order */
  nm_ibverbs_mini_spinlock_t lock;  /**< lock protecting access to this struct */
  size_t filling;                   /**< total amount of memory registered */
#ifdef NMAD_PROFILE
  struct
  {
    unsigned long long n_mem_reg;        /**< number of times mem_reg() was called */
    unsigned long long n_reg_mr;         /**< number of times ibv_reg_mr() was actually called */
    unsigned long long n_dereg_mr;       /**< number of times ibv_dereg_mr() was actually called */
    unsigned long long mem_reg_bytes;    /**< amount of registered memory */
    unsigned long long reg_mr_bytes;     /**< amount of registered memory in MR */
    unsigned long long n_cache_hits;     /**< number of cache hits */
    double total_reg_mr_usecs;           /**< total time spent in ibv_reg_mr() */
    double total_dereg_mr_usecs;         /**< total time spent in ibv_dereg_mr() */
    double cache_hit_rate;               /**< cache hit rate, in percent (0..100) */
    unsigned long long n_back_pressure;
    double average_reg_mr_bw_Mbps;
  } profile;
#endif /* NMAD_PROFILE */
} nm_ibverbs_mini;

/* ********************************************************* */

static void nm_ibverbs_mini_invalidate_hook(void*ptr, size_t size) __attribute__((unused));
static void nm_ibverbs_mini_reg_back_pressure(nm_len_t len);
static void nm_ibverbs_mini_invalidate(struct nm_ibverbs_mini_entry_s*p_entry);

static void nm_ibverbs_mini_mem_init(struct nm_ibverbs_context_s*p_ibverbs_context)
{
  puk_itree_init(&nm_ibverbs_mini.entries);
  nm_ibverbs_mini_entries_allocator_init(&nm_ibverbs_mini.allocator, 16);
  nm_ibverbs_mini_entry_list_init(&nm_ibverbs_mini.lru_list);
  nm_ibverbs_mini.filling = 0;
  nm_ibverbs_mini_spin_init(&nm_ibverbs_mini.lock);
#ifdef PUKABI
  /* invalidate mem reg upon free/realloc/munmap/mremap */
  puk_abi_mem_set_hook(&nm_ibverbs_mini_invalidate_hook);
#else
  NM_WARN("using rcache without PukABI.\n");
  mallopt(M_TRIM_THRESHOLD, -1);
  mallopt(M_MMAP_MAX, 0); /* XXX */
  mallopt(M_ARENA_MAX, 1);
  /*  malloc_stats(); */
#endif
  if(p_ibverbs_context->hca.p_reg_back_pressure == NULL)
    {
      p_ibverbs_context->hca.p_reg_back_pressure = &nm_ibverbs_mini_reg_back_pressure;
    }
  else
    {
      NM_FATAL("failed to register registration back pressure handler.\n");
    }
#ifdef NMAD_PROFILE
  puk_profile_var_defx(unsigned_long_long, counter, &nm_ibverbs_mini.profile.n_mem_reg, 0,
                       "nm_ibverbs_rcache_mini", "number of times mem_reg() was called",
                       "nm_ibverbs_rcache_mini.n_mem_reg");
  puk_profile_var_defx(unsigned_long_long, counter, &nm_ibverbs_mini.profile.n_reg_mr, 0,
                       "nm_ibverbs_rcache_mini", "number of times ibv_reg_mr() was actually called",
                       "nm_ibverbs_rcache_mini.n_reg_mr");
  puk_profile_var_defx(unsigned_long_long, aggregate, &nm_ibverbs_mini.profile.mem_reg_bytes, 0,
                       "nm_ibverbs_rcache_mini", "amount of registered memory",
                       "nm_ibverbs_rcache_mini.mem_reg_bytes");
  puk_profile_var_defx(unsigned_long_long, aggregate, &nm_ibverbs_mini.profile.reg_mr_bytes, 0,
                       "nm_ibverbs_rcache_mini", "amount of registered memory in MR",
                       "nm_ibverbs_rcache_mini.reg_mr_bytes");
  puk_profile_var_defx(unsigned_long_long, counter, &nm_ibverbs_mini.profile.n_cache_hits, 0,
                       "nm_ibverbs_rcache_mini", "number of cache hits",
                       "nm_ibverbs_rcache_mini.n_cache_hits");
  puk_profile_var_defx(unsigned_long_long, counter, &nm_ibverbs_mini.profile.n_back_pressure, 0,
                       "nm_ibverbs_rcache_mini", "number of times back pressure was called",
                       "nm_ibverbs_rcache_mini.n_back_pressure");
  puk_profile_var_defx(double, timer, &nm_ibverbs_mini.profile.total_reg_mr_usecs, 0.0,
                       "nm_ibverbs_rcache_mini", "total time spent in ibv_reg_mr()",
                       "nm_ibverbs_rcache_mini.total_reg_mr_usecs");
  puk_profile_var_defx(double, timer, &nm_ibverbs_mini.profile.total_dereg_mr_usecs, 0.0,
                       "nm_ibverbs_rcache_mini", "total time spent in ibv_dereg_mr()",
                       "nm_ibverbs_rcache_mini.total_dereg_mr_usecs");
  puk_profile_var_defx(double, percentage, &nm_ibverbs_mini.profile.cache_hit_rate, 0.0,
                       "nm_ibverbs_rcache_mini", "cache hit rate",
                       "nm_ibverbs_rcache_mini.cache_hit_rate");
  puk_profile_var_defx(double, timer, &nm_ibverbs_mini.profile.average_reg_mr_bw_Mbps, 0.0,
                       "nm_ibverbs_rcache_mini", "average bandwidth for MR registration, in Mbyte/s",
                       "nm_ibverbs_rcache_mini.average_reg_mr_bw_Mbps");
#endif /* NMAD_PROFILE */
}

static void nm_ibverbs_mini_mem_close(struct nm_ibverbs_context_s*p_ibverbs_context)
{
#ifdef PUKABI
  /* unregister memory hooks */
  puk_abi_mem_set_hook(NULL);
#endif /* PUKABI */
  /* invalidate all pending cache entries */
  nm_ibverbs_mini_spin_lock(&nm_ibverbs_mini.lock);
  struct nm_ibverbs_mini_entry_s*p_entry = nm_ibverbs_mini_entry_list_front(&nm_ibverbs_mini.lru_list);
  while(p_entry != NULL)
    {
      assert(p_entry->refcount == 0);
      nm_ibverbs_mini_invalidate(p_entry);
      p_entry = nm_ibverbs_mini_entry_list_front(&nm_ibverbs_mini.lru_list);
    }
  nm_ibverbs_mini_spin_unlock(&nm_ibverbs_mini.lock);
  /* all remaining entries at this points are erroneous */
  const int count = puk_itree_count(&nm_ibverbs_mini.entries);;
  if(count > 0)
    {
      NM_WARN("%d memory entries not unregistered before closing (%lu bytes).\n", count, nm_ibverbs_mini.filling);
    }
  nm_ibverbs_mini_entry_list_destroy(&nm_ibverbs_mini.lru_list);
  puk_itree_destroy(&nm_ibverbs_mini.entries);
  nm_ibverbs_mini_entries_allocator_destroy(&nm_ibverbs_mini.allocator);
}

/** remove the given entry from rcache and deregister memory  */
static void nm_ibverbs_mini_invalidate(struct nm_ibverbs_mini_entry_s*p_entry)
{
  nm_ibverbs_mini_spin_assert_locked(&nm_ibverbs_mini.lock);
  NM_RCACHE_LOG("evict cache slot %p; ptr = %p; len = %lu\n",
                p_entry, p_entry->ptr, p_entry->len);
  assert(p_entry->refcount == 0);
  assert(p_entry->ptr != NULL);
  assert(p_entry->p_mr != NULL);
  puk_itree_delete(&nm_ibverbs_mini.entries, &p_entry->itree);
  if(p_entry->refcount == 0)
    {
      nm_ibverbs_mini_entry_list_remove(&nm_ibverbs_mini.lru_list, p_entry);
    }
  nm_ibverbs_mini.filling -= p_entry->len;
  nm_ibverbs_mini_spin_unlock(&nm_ibverbs_mini.lock);
#ifdef NMAD_PROFILE
  puk_tick_t t1, t2;
  PUK_GET_TICK(t1);
#endif /* NMAD_PROFILE */
  int rc = nm_ibverbs_dereg_mr(p_entry->p_ibverbs_context, p_entry->p_mr);
  if(rc != 0)
    {
      NM_WARN("error %d (%s) while deregistering memory.\n", rc, strerror(rc));
    }
#ifdef NMAD_PROFILE
  PUK_GET_TICK(t2);
  const double tdiff = PUK_TIMING_DELAY(t1, t2);
  nm_ibverbs_mini.profile.total_dereg_mr_usecs += tdiff;
  nm_profile_inc(nm_ibverbs_mini.profile.n_dereg_mr);
#endif /* NMAD_PROFILE */
  p_entry->p_mr = NULL;
  p_entry->ptr = NULL;
  p_entry->refcount = 0;
  nm_ibverbs_mini_spin_lock(&nm_ibverbs_mini.lock);
  nm_ibverbs_mini_entries_free(&nm_ibverbs_mini.allocator, p_entry);
}

/** hook called from free/realloc/munmap/mremap to invalidate freed entries */
static void nm_ibverbs_mini_invalidate_hook(void*ptr, size_t len)
{
  if(ptr == NULL)
    return;
  nm_ibverbs_mini_spin_lock(&nm_ibverbs_mini.lock);
  struct puk_itree_node_s*p_node = puk_itree_interval_lookup(&nm_ibverbs_mini.entries, (uintptr_t)ptr, len);
  while(p_node != NULL)
    {
      struct nm_ibverbs_mini_entry_s*p_entry = puk_container_of(p_node, struct nm_ibverbs_mini_entry_s, itree);
      NM_TRACEF("invalidate ptr = %p; len = %lu\n", ptr, len);
      if(p_entry->refcount > 0)
        {
          NM_FATAL("cannot invalidate rcache entry still in use (refcount = %d); probably trying to free() a memory block still in use in a send/recv.\n", p_entry->refcount);
        }
      else
        {
          nm_ibverbs_mini_invalidate(p_entry);
        }
      p_node = puk_itree_interval_lookup(&nm_ibverbs_mini.entries, (uintptr_t)ptr, len);
    }
  nm_ibverbs_mini_spin_unlock(&nm_ibverbs_mini.lock);
}

static struct nm_ibverbs_mini_entry_s*nm_ibverbs_mini_lookup(const void*ptr, size_t len)
{
  nm_ibverbs_mini_spin_assert_locked(&nm_ibverbs_mini.lock);
  struct puk_itree_node_s*p_node = puk_itree_exact_lookup(&nm_ibverbs_mini.entries, (uintptr_t)ptr, len);
  if(p_node != NULL)
    {
      struct nm_ibverbs_mini_entry_s*p_entry = puk_container_of(p_node, struct nm_ibverbs_mini_entry_s, itree);
      NM_RCACHE_LOG("ptr = %p; len = %lu; matching entry; ptr = %p; len = %lu; refcount = %d\n",
                    ptr, len, p_entry->ptr, p_entry->len, p_entry->refcount);
      return p_entry;
    }
  return NULL;
}

/** evict the oldest unused cache entry; return -1 if eviction failed */
static int nm_ibverbs_mini_evict(void)
{
  nm_ibverbs_mini_spin_assert_locked(&nm_ibverbs_mini.lock);
  struct nm_ibverbs_mini_entry_s*p_entry = nm_ibverbs_mini_entry_list_front(&nm_ibverbs_mini.lru_list);
  if(p_entry == NULL)
    {
      NM_WARN("all %d rcache entries in use; cannot evict any entry.\n", puk_itree_count(&nm_ibverbs_mini.entries));
      return -1;
    }
  assert(p_entry->refcount == 0);
  /* evict cache entry */
  NM_TRACEF("evict ptr = %p; len = %lu\n", p_entry->ptr, p_entry->len);
  nm_ibverbs_mini_invalidate(p_entry);
  nm_ibverbs_mini_spin_assert_locked(&nm_ibverbs_mini.lock);
  return 0;
}

/** release pressure on memory registration by invalidating some cache entries (at least 'len' bytes) */
static void nm_ibverbs_mini_reg_back_pressure(nm_len_t len)
{
  NM_DISPF("len = %lu; filling = %ld\n", len, nm_ibverbs_mini.filling);
  nm_profile_inc(nm_ibverbs_mini.profile.n_back_pressure);
  nm_ibverbs_mini_spin_lock(&nm_ibverbs_mini.lock);
  nm_len_t start = nm_ibverbs_mini.filling;
  while(nm_ibverbs_mini.filling > start - len)
    {
      int rc = nm_ibverbs_mini_evict();
      if(rc < 0)
        {
          break;
        }
    }
  if(nm_ibverbs_mini.filling <= start - len)
    {
      NM_DISPF("len = %lu RECOVERED \n", len);
    }
  nm_ibverbs_mini_spin_unlock(&nm_ibverbs_mini.lock);
}

static void nm_ibverbs_mini_mem_reg(struct nm_ibverbs_context_s*p_ibverbs_context, const void*ptr, size_t len,
                                    struct ibv_mr**pp_mr, void**pp_handle)
{
  assert(ptr != NULL);
  nm_profile_inc(nm_ibverbs_mini.profile.n_mem_reg);
  nm_profile_add(nm_ibverbs_mini.profile.mem_reg_bytes, len);
  nm_ibverbs_mini_spin_lock(&nm_ibverbs_mini.lock);
  /* lookup */
  struct nm_ibverbs_mini_entry_s*p_entry = nm_ibverbs_mini_lookup(ptr, len);
  struct ibv_mr*p_mr = NULL;
  if(p_entry)
    {
      nm_profile_inc(nm_ibverbs_mini.profile.n_cache_hits);
#ifdef NMAD_PROFILE
      nm_ibverbs_mini.profile.cache_hit_rate =
        100.0 * nm_ibverbs_mini.profile.n_cache_hits / nm_ibverbs_mini.profile.n_mem_reg;
#endif /* NMAD_PROFILE */
      const int v = __sync_fetch_and_add(&p_entry->refcount, 1);
      p_mr = p_entry->p_mr;
      if(v == 0)
        {
          /* remove entry from LRU */
          nm_ibverbs_mini_entry_list_remove(&nm_ibverbs_mini.lru_list, p_entry);
        }
      nm_ibverbs_mini_spin_unlock(&nm_ibverbs_mini.lock);
    }
  else
    {
#ifdef NMAD_PROFILE
      puk_tick_t t1, t2;
      nm_ibverbs_mini.profile.cache_hit_rate =
        100.0 * nm_ibverbs_mini.profile.n_cache_hits / nm_ibverbs_mini.profile.n_mem_reg;
#endif /* NMAD_PROFILE */
      /* not found in cache- allocate a new entry */
      p_entry = nm_ibverbs_mini_entries_malloc(&nm_ibverbs_mini.allocator);
      p_entry->ptr = ptr;
      p_entry->len = len;
      p_entry->refcount = 1;
      p_entry->p_ibverbs_context = p_ibverbs_context;
      nm_ibverbs_mini_entry_list_cell_init(p_entry);
      nm_ibverbs_mini.filling += len;
      nm_ibverbs_mini_spin_unlock(&nm_ibverbs_mini.lock);
      nm_profile_inc(nm_ibverbs_mini.profile.n_reg_mr);
      nm_profile_add(nm_ibverbs_mini.profile.reg_mr_bytes, len);
    retry:
#ifdef NMAD_PROFILE
      PUK_GET_TICK(t1);
#endif /* NMAD_PROFILE */
      p_mr = nm_ibverbs_reg_mr(p_ibverbs_context, (void*)ptr, len, NULL);
#ifdef NMAD_PROFILE
      PUK_GET_TICK(t2);
      const double tdiff = PUK_TIMING_DELAY(t1, t2);
      nm_ibverbs_mini.profile.total_reg_mr_usecs += tdiff;
      nm_ibverbs_mini.profile.average_reg_mr_bw_Mbps =
        (double)nm_ibverbs_mini.profile.reg_mr_bytes / (double)nm_ibverbs_mini.profile.total_reg_mr_usecs;
#endif /* NMAD_PROFILE */
      p_entry->p_mr = p_mr;
      if(p_entry->p_mr == NULL)
        {
          if(nm_ibverbs_mini.filling == 0)
            {
              NM_WARN("memory registration failed.\n");
              struct rlimit rlim;
              int rc = getrlimit(RLIMIT_MEMLOCK, &rlim);
              if(rc == 0)
                {
                  if(rlim.rlim_cur == 0)
                    {
                      NM_FATAL("RLIMIT_MEMLOCK = %lu; cannot register memory.\n", rlim.rlim_cur);
                    }
                  else
                    {
                      NM_DISPF("RLIMIT_MEMLOCK = %lu\n", rlim.rlim_cur);
                    }
                }
              abort();
            }
          else
            {
              NM_RCACHE_LOG("ibv_reg_mr failed; evict entry and retry.\n");
              nm_ibverbs_mini_spin_lock(&nm_ibverbs_mini.lock);
              int evict = nm_ibverbs_mini_evict();
              nm_ibverbs_mini_spin_assert_locked(&nm_ibverbs_mini.lock);
              nm_ibverbs_mini_spin_unlock(&nm_ibverbs_mini.lock);
              if(evict == 0)
                goto retry;
              else
                {
                  NM_FATAL("failed to register memory- ptr = %p; len = %lu\n", ptr, len);
                }
            }
        }
      nm_ibverbs_mini_spin_lock(&nm_ibverbs_mini.lock);
      puk_itree_insert(&nm_ibverbs_mini.entries, &p_entry->itree, (uintptr_t)ptr, len);
      nm_ibverbs_mini_spin_unlock(&nm_ibverbs_mini.lock);
    }
  assert(p_entry->refcount >= 1);
  NM_RCACHE_LOG("ptr = %p; len = %lu; refcount = %d; filling = %lld; brk = %p\n",
                ptr, len, p_entry->refcount, (long long)nm_ibverbs_mini.filling, sbrk(0));
  *pp_mr = p_mr;
  *pp_handle = p_entry;
}

static void nm_ibverbs_mini_mem_unreg(struct nm_ibverbs_context_s*p_ibverbs_context, const void*ptr, size_t len,
                                      struct ibv_mr*p_mr, void*p_handle)
{
  struct nm_ibverbs_mini_entry_s*p_entry = p_handle;
  NM_TRACEF("p_entry = %p; ptr = %p; len = %lu; refcount = %d\n", p_entry, ptr, len, p_entry->refcount);
  assert(p_entry->ptr == ptr);
  assert(p_entry->len == len);
  assert(p_entry->refcount > 0);
  assert(p_entry->p_mr == p_mr);
  const int v = __sync_sub_and_fetch(&p_entry->refcount, 1);
  assert(p_entry->refcount >= 0);
  if(v == 0)
    {
      if(p_entry->refcount == 0) /* check again while holding the lock */
        {
          /* store entry in LRU cache */
          nm_ibverbs_mini_spin_lock(&nm_ibverbs_mini.lock);
          nm_ibverbs_mini_entry_list_push_front(&nm_ibverbs_mini.lru_list, p_entry);
          nm_ibverbs_mini_spin_unlock(&nm_ibverbs_mini.lock);
        }
    }
}

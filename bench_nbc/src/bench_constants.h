/*
 * Bench NBC
 * Copyright (C) 2019-2022 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#ifndef BENCH_CONSTANT_H
#define BENCH_CONSTANT_H

#define ROOT 0
#define NB_REP 100
#define DATA_THRESHOLD 1000000
#define COMPUTE_THRESHOLD 500
#define FINE_ESTIM_MAX_ITER 50
#define ESTIM_MAX_ITER 75
#define NB_REP_FREQ 25
#define NB_MATRIX_SIZE 12 // 100,1000,10 000, 100 000,1 000 000 default 11
#define NB_DATA_SIZE 12
#define BASE_COMPUTE_TIME 500
#define MAX_DATA_SIZE 1*1e8//usec default 1*1e8
#define MATRIX_COMP_STEP 2
#define DATA_SIZE_STEP 2
#define BASE_DATA_TIME 500
#define BASE_DATA_SIZE 50000
#define NB_OUTPUT_FILES 4
#define CALL_MPI_IBARRIER 0
#define CALL_MPI_IBCAST 1
#define CALL_MPI_IREDUCE 2
#define CALL_MPI_IALLTOALL 3
#define CALL_MPI_IALLREDUCE 4
#define CALL_MPI_IGATHER 5
#define CALL_MPI_IALLGATHER 6

#define ALL_RANK_PRINT 0x01
#define USE_COLL_TIME  0x02
#define USE_STATIC     0x04
#define USE_BLOCKING   0x08 /**< use blocking collective to calibrate collective time */

#define SYNC_2
#define GLOBAL_BUFFER


//static int DATA_SIZES[NB_DATA_SIZE] = {45000,89000,209000,429000,809000,1527000,2785000,4459000,8068000,16468000,32606000,56340000};

//static openmpi ibcast8
//static int DATA_SIZES[NB_DATA_SIZE] = {0,1000,10000,40000,367000,79000000,152700000,284500000,542800000};

//static openmpi ibcast8(manual)
//static int DATA_SIZES[NB_DATA_SIZE] = {700000,5000000,10000000,37000000,69000000,135000000,260000000,530000000,999900000};
//static int MATRIX_SIZES[NB_MATRIX_SIZE] = {102,122,163,205,253,335,419,522,634,775,979,1229};


//MPC

//static int DATA_SIZES[NB_DATA_SIZE] = {45000,135000,300000,666000,1442000,2158000,5367000,10293000,19884000,40487000,80919000,162106000};

//static int MATRIX_SIZES[NB_MATRIX_SIZE] = {96,126,162,204,265,334,421,518,613,769,969,1209};
#endif

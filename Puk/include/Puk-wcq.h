/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief lock-free FIFO based on wCQ algorithm . Only API adaptation
 * code is present here. Real wCQ implementation is in wcq.h.
 */


#ifndef PUK_WCQ_H
#define PUK_WCQ_H

#include <stddef.h>
#include <pthread.h>

#include "wfring_cas2.h"
//#include "lfring_cas2.h"
//#include "lfring_cas1.h"
#include "lfring_naive.h"
/* We cannot include the naive (NCQ), the cas1 (SCQD) and the cas2 (SCQ2)
 * versions at the same time, since some symbols share the same name between files.
 * Uncomment the right lfring_*.h include.
 */

#define NBITS2(n)  ((n&0x02)?1:0)
#define NBITS4(n)  ((n&0x0C)?(2+NBITS2(n>>2)):(NBITS2(n)))
#define NBITS8(n)  ((n&0xF0)?(4+NBITS4(n>>4)):(NBITS4(n)))
#define NBITS16(n) ((n&0xFF00)?(8+NBITS8(n>>8)):(NBITS8(n)))
#define NBITS32(n) ((n&0xFFFF0000)?(16+NBITS16(n>>16)):(NBITS16(n)))
#define NBITS(n)    (n==0?0:NBITS32((unsigned)n))

static inline int wcq_log2_floor(int x)
{
  assert(x > 0);
  return ((unsigned) (8*sizeof (unsigned long long) - __builtin_clzll((x)) - 1));
}

/** builds a lock-free FIFO type and functions.
 *  ENAME is base name for symbols
 *  TYPE is the type of elements in queue- assumed to be a pointer
 *  LFQUEUE_NULL is a nil value to fill and detect empty cells; assumed to be NULL
 *  SIZE is the size of the ring-buffer.
 *  Only 'init', 'enqueue', 'dequeue', and 'empty' operations are available.
 */
#define PUK_LFQUEUE_WCQ_TYPE(ENAME, TYPE, LFQUEUE_NULL, SIZE)           \
  /** a queue designed to contain indexes */                            \
  struct ENAME ## _wcq_idx_ring_s                                       \
  {                                                                     \
    char ring[WFRING_SIZE(NBITS(SIZE))] __attribute__((aligned(16)));   \
    _Atomic(struct wfring_state*) handle_tail;                          \
  };                                                                    \
  /** handle for local state of a thread */                             \
  struct ENAME ## _wcq_handle_s                                         \
  {                                                                     \
    struct wfring_state state_aq;       /**< state for aq */            \
    struct wfring_state state_fq;       /**< state for fq */            \
  };                                                                    \
  struct ENAME ## _lfqueue_s                                            \
  {                                                                     \
    TYPE array[SIZE];                   /**< array for actual data */   \
    struct ENAME ## _wcq_idx_ring_s aq; /**< allocated queue */         \
    struct ENAME ## _wcq_idx_ring_s fq; /**< free queue */              \
    pthread_key_t key;                                                  \
  };                                                                    \
  /** @internal register a new thread */                                \
  static inline void ENAME ## _wcq_register(struct ENAME ## _wcq_idx_ring_s*q, struct wfring_state*state) \
  {                                                                     \
    wfring_init_state((struct wfring*)q->ring, state);                  \
    struct wfring_state *tail = atomic_load(&q->handle_tail);           \
    if (tail == NULL) {                                                 \
      state->next = state;                                              \
      if (atomic_compare_exchange_strong(&q->handle_tail, &tail, state)) \
        return;                                                         \
    }                                                                   \
    struct wfring_state *next = atomic_load(&tail->next);               \
    do {                                                                \
      state->next = next;                                               \
    } while (!atomic_compare_exchange_weak(&tail->next, &next, state)); \
  }                                                                     \
  /** @internal resolve local thread state; create it lazily if needed */ \
  static inline struct ENAME ## _wcq_handle_s*ENAME ## _wcq_get_thread_local(struct ENAME ## _lfqueue_s*q) \
  {                                                                     \
    struct ENAME ## _wcq_handle_s*th = pthread_getspecific(q->key);     \
    if(th == NULL)                                                      \
      {                                                                 \
        th = malloc(sizeof(struct ENAME ## _wcq_handle_s));             \
        ENAME ## _wcq_register(&q->aq, &th->state_aq);                  \
        ENAME ## _wcq_register(&q->fq, &th->state_fq);                  \
        pthread_setspecific(q->key, th);                                \
      }                                                                 \
    return th;                                                          \
  }                                                                     \
  /** initialize a new lfqueue */                                       \
  static inline void ENAME ## _lfqueue_init(struct ENAME ## _lfqueue_s*q) \
  {                                                                     \
    const size_t order = NBITS(SIZE);                                   \
    assert((1 << order) == SIZE);                                       \
    wfring_init_empty((struct wfring *) q->aq.ring, order);             \
    wfring_init_empty((struct wfring *) q->fq.ring, order);             \
    q->aq.handle_tail = ATOMIC_VAR_INIT(NULL);                          \
    q->fq.handle_tail = ATOMIC_VAR_INIT(NULL);                          \
    pthread_key_create(&q->key, NULL /* TODO- missing destructor */);   \
    struct ENAME ## _wcq_handle_s*th = ENAME ## _wcq_get_thread_local(q); \
    int i;                                                              \
    for(i = 0; i < SIZE; i++)                                           \
      {                                                                 \
        q->array[i] = LFQUEUE_NULL;                                     \
        size_t eidx = i;                                                \
        wfring_enqueue((struct wfring*)q->fq.ring, order, eidx, false, &th->state_fq); \
      }                                                                 \
  }                                                                     \
  /** enqueue a new element in queue; return 0 in case of success, 1 of list is full */ \
  static inline int ENAME ## _lfqueue_enqueue(struct ENAME ## _lfqueue_s*q, TYPE val) \
  {                                                                     \
    struct ENAME ## _wcq_handle_s*th = ENAME ## _wcq_get_thread_local(q); \
    const size_t order = NBITS(SIZE);                                   \
    size_t eidx = wfring_dequeue((struct wfring*)q->fq.ring, order, false, &th->state_fq); \
    if(eidx == WFRING_EMPTY)                                            \
      return 1; /* free queue empty => queue full */                    \
    assert((eidx >= 0) && (eidx < SIZE));                               \
    assert(q->array[eidx] == LFQUEUE_NULL);                             \
    q->array[eidx] = val;                                               \
    wfring_enqueue((struct wfring*)q->aq.ring, order, eidx, false, &th->state_aq); \
    return 0;                                                           \
  }                                                                     \
  /** dequeue an element from the queue; returns NULL if queue is empty */ \
  static inline TYPE ENAME ## _lfqueue_dequeue(struct ENAME ## _lfqueue_s*q) \
  {                                                                     \
    struct ENAME ## _wcq_handle_s*th = ENAME ## _wcq_get_thread_local(q); \
    const size_t order = NBITS(SIZE);                                   \
    size_t eidx = wfring_dequeue((struct wfring*)q->aq.ring, order, false, &th->state_aq); \
    if(eidx == WFRING_EMPTY)                                            \
      {                                                                 \
        return LFQUEUE_NULL;                                            \
      }                                                                 \
    assert((eidx >= 0) && (eidx < SIZE));                               \
    TYPE val = q->array[eidx];                                          \
    assert(val != LFQUEUE_NULL);                                        \
    q->array[eidx] = LFQUEUE_NULL;                                      \
    wfring_enqueue((struct wfring*)q->fq.ring, order, eidx, false, &th->state_fq); \
    return val;                                                         \
  }                                                                     \
  static inline int ENAME ## _lfqueue_empty(struct ENAME ## _lfqueue_s*q) \
  {                                                                     \
    struct __wfring*_q = (struct __wfring*)&q->aq;                      \
    return (atomic_load(&_q->threshold) < 0);                           \
  }                                                                     \
  static inline int ENAME ## _lfqueue_enqueue_single_writer(struct ENAME ## _lfqueue_s*queue, TYPE e) \
  { return ENAME ## _lfqueue_enqueue(queue, e); }                       \
  static inline TYPE ENAME ## _lfqueue_dequeue_single_reader(struct ENAME ## _lfqueue_s*queue) \
  { return ENAME ## _lfqueue_dequeue(queue); }                          \
  static inline void ENAME ## _lfqueue_free(struct ENAME ## _lfqueue_s*q) \
  {                                                                     \
  }                                                                     \
  static inline int ENAME ## _lfqueue_load(struct ENAME ## _lfqueue_s*q) \
  {                                                                     \
    return 1; /* dummy value */                                         \
  }



#define PUK_LFQUEUE_SCQ2_TYPE(ENAME, TYPE, LFQUEUE_NULL, SIZE)          \
  struct ENAME ## _scq2_handle_s                                        \
  {                                                                     \
    lfatomic_t lhead;                                                   \
  } __attribute__((aligned(128)));                                      \
  struct ENAME ## _lfqueue_s                                            \
  {                                                                     \
    char ring[LFRING_PTR_SIZE(NBITS(SIZE))];                            \
    pthread_key_t key;                                                  \
  } __attribute__((aligned(128)));                                      \
  /** @internal resolve local thread state; create it lazily if needed */ \
  static inline struct ENAME ## _scq2_handle_s* ENAME ## _scq2_get_handle(struct ENAME ## _lfqueue_s*q) \
  {                                                                     \
    struct ENAME ## _scq2_handle_s*th = pthread_getspecific(q->key);    \
    if(th == NULL)                                                      \
      {                                                                 \
        th = malloc(sizeof(struct ENAME ## _scq2_handle_s));            \
        lfring_ptr_init_lhead(&th->lhead, NBITS(SIZE));                 \
        pthread_setspecific(q->key, th);                                \
      }                                                                 \
    return th;                                                          \
  }                                                                     \
  /** initialize a new lfqueue */                                       \
  static inline void ENAME ## _lfqueue_init(struct ENAME ## _lfqueue_s*q) \
  {                                                                     \
    lfring_ptr_init_empty((struct lfring_ptr *) q->ring, NBITS(SIZE));  \
    pthread_key_create(&q->key, NULL /* TODO- missing destructor */);   \
  }                                                                     \
  /** enqueue a new element in queue; return 0 in case of success, 1 of list is full */ \
  static inline int ENAME ## _lfqueue_enqueue(struct ENAME ## _lfqueue_s*q, TYPE val) \
  {                                                                     \
    struct ENAME ## _scq2_handle_s*th = ENAME ## _scq2_get_handle(q);   \
    bool f = lfring_ptr_enqueue((struct lfring_ptr *) q->ring, NBITS(SIZE), ((void*)val) + 1, false, false, &th->lhead); \
    return !f; /* TODO- never full ? */                                 \
  }                                                                     \
  /** dequeue an element from the queue; returns NULL if queue is empty */ \
  static inline TYPE ENAME ## _lfqueue_dequeue(struct ENAME ## _lfqueue_s*q) \
  {                                                                     \
    void *ptr;                                                          \
    if (!lfring_ptr_dequeue((struct lfring_ptr *) q->ring, NBITS(SIZE), &ptr, false)) \
      return NULL;                                                     \
    ptr--;                                                              \
    return ptr;                                                         \
  }                                                                     \
  static inline int ENAME ## _lfqueue_empty(struct ENAME ## _lfqueue_s*q) \
  {                                                                     \
    return 0;                                                           \
  }                                                                     \
  static inline int ENAME ## _lfqueue_enqueue_single_writer(struct ENAME ## _lfqueue_s*queue, TYPE e) \
  { return ENAME ## _lfqueue_enqueue(queue, e); }                       \
  static inline TYPE ENAME ## _lfqueue_dequeue_single_reader(struct ENAME ## _lfqueue_s*queue) \
  { return ENAME ## _lfqueue_dequeue(queue); }                          \
  static inline void ENAME ## _lfqueue_free(struct ENAME ## _lfqueue_s*q) \
  {                                                                     \
  }                                                                     \
  static inline int ENAME ## _lfqueue_load(struct ENAME ## _lfqueue_s*q) \
  {                                                                     \
    return 1; /* dummy value */                                         \
  }


#define PUK_LFQUEUE_SCQD_TYPE(ENAME, TYPE, LFQUEUE_NULL, SIZE)          \
  struct ENAME ## _lfqueue_s                                            \
  {                                                                     \
    char aq[LFRING_SIZE(NBITS(SIZE))];                                  \
    char fq[LFRING_SIZE(NBITS(SIZE))];                                  \
    void  *val[(1U << NBITS(SIZE))];                                    \
  } __attribute__((aligned(128)));                                      \
  static inline void ENAME ## _lfqueue_init(struct ENAME ## _lfqueue_s*q) \
  {                                                                     \
    lfring_init_empty((struct lfring *) q->aq, NBITS(SIZE));            \
    lfring_init_full((struct lfring *) q->fq, NBITS(SIZE));             \
  }                                                                     \
  /** enqueue a new element in queue; return 0 in case of success, 1 of list is full */ \
  static inline int ENAME ## _lfqueue_enqueue(struct ENAME ## _lfqueue_s*q, TYPE val) \
  {                                                                     \
    size_t eidx = lfring_dequeue((struct lfring *) q->fq, NBITS(SIZE), true); \
    if (eidx == LFRING_EMPTY) return -1;                                \
    q->val[eidx] = val;                                                 \
    lfring_enqueue((struct lfring *) q->aq, NBITS(SIZE), eidx, false);  \
    return 0;                                                           \
  }                                                                     \
  /** dequeue an element from the queue; returns NULL if queue is empty */ \
  static inline TYPE ENAME ## _lfqueue_dequeue(struct ENAME ## _lfqueue_s*q) \
  {                                                                     \
    void *val;                                                          \
    size_t eidx = lfring_dequeue((struct lfring *) q->aq, NBITS(SIZE), false); \
    if (eidx == LFRING_EMPTY) return NULL;                              \
    val = q->val[eidx];                                                 \
    lfring_enqueue((struct lfring *) q->fq, NBITS(SIZE), eidx, true);   \
    return val;                                                         \
  }                                                                     \
  static inline int ENAME ## _lfqueue_empty(struct ENAME ## _lfqueue_s*q) \
  {                                                                     \
    return 0;                                                           \
  }                                                                     \
  static inline int ENAME ## _lfqueue_enqueue_single_writer(struct ENAME ## _lfqueue_s*queue, TYPE e) \
  { return ENAME ## _lfqueue_enqueue(queue, e); }                       \
  static inline TYPE ENAME ## _lfqueue_dequeue_single_reader(struct ENAME ## _lfqueue_s*queue) \
  { return ENAME ## _lfqueue_dequeue(queue); }                          \
  static inline void ENAME ## _lfqueue_free(struct ENAME ## _lfqueue_s*q) \
  {                                                                     \
  }                                                                     \
  static inline int ENAME ## _lfqueue_load(struct ENAME ## _lfqueue_s*q) \
  {                                                                     \
    return 1; /* dummy value */                                         \
  }

#define PUK_LFQUEUE_NCQD_TYPE(ENAME, TYPE, LFQUEUE_NULL, SIZE)          \
  struct ENAME ## _lfqueue_s                                            \
  {                                                                     \
    char aq[LFRING_SIZE(NBITS(SIZE))];                                  \
    char fq[LFRING_SIZE(NBITS(SIZE))];                                  \
    void  *val[(1U << NBITS(SIZE))];                                    \
  } __attribute__((aligned(128)));                                      \
  static inline void ENAME ## _lfqueue_init(struct ENAME ## _lfqueue_s*q) \
  {                                                                     \
    lfring_init_empty((struct lfring *) q->aq, NBITS(SIZE));            \
    lfring_init_full((struct lfring *) q->fq, NBITS(SIZE));             \
  }                                                                     \
  /** enqueue a new element in queue; return 0 in case of success, 1 of list is full */ \
  static inline int ENAME ## _lfqueue_enqueue(struct ENAME ## _lfqueue_s*q, TYPE val) \
  {                                                                     \
    size_t eidx = lfring_dequeue((struct lfring *) q->fq, NBITS(SIZE), false); \
    if (eidx == LFRING_EMPTY) return -1;                                \
    q->val[eidx] = val;                                                 \
    lfring_enqueue((struct lfring *) q->aq, NBITS(SIZE), eidx, false);  \
    return 0;                                                           \
  }                                                                     \
  /** dequeue an element from the queue; returns NULL if queue is empty */ \
  static inline TYPE ENAME ## _lfqueue_dequeue(struct ENAME ## _lfqueue_s*q) \
  {                                                                     \
    void *val;                                                          \
    size_t eidx = lfring_dequeue((struct lfring *) q->aq, NBITS(SIZE), false); \
    if (eidx == LFRING_EMPTY) return NULL;                              \
    val = q->val[eidx];                                                 \
    lfring_enqueue((struct lfring *) q->fq, NBITS(SIZE), eidx, false);   \
    return val;                                                         \
  }                                                                     \
  static inline int ENAME ## _lfqueue_empty(struct ENAME ## _lfqueue_s*q) \
  {                                                                     \
    return 0;                                                           \
  }                                                                     \
  static inline int ENAME ## _lfqueue_enqueue_single_writer(struct ENAME ## _lfqueue_s*queue, TYPE e) \
  { return ENAME ## _lfqueue_enqueue(queue, e); }                       \
  static inline TYPE ENAME ## _lfqueue_dequeue_single_reader(struct ENAME ## _lfqueue_s*queue) \
  { return ENAME ## _lfqueue_dequeue(queue); }                          \
  static inline void ENAME ## _lfqueue_free(struct ENAME ## _lfqueue_s*q) \
  {                                                                     \
  }                                                                     \
  static inline int ENAME ## _lfqueue_load(struct ENAME ## _lfqueue_s*q) \
  {                                                                     \
    return 1; /* dummy value */                                         \
  }


#endif /* PUK_WCQ_H */

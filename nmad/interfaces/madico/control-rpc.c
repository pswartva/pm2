/** @file
 * @brief PadicoControl over nmad RPC
 */

#include <Padico/Puk.h>
#include <Padico/Module.h>
#include <Padico/PadicoTM.h>
#include <Padico/PadicoControl.h>
#include <Padico/Topology.h>

#include <nm_rpc_interface.h>
#include <nm_coll_interface.h>

/* ********************************************************* */

#define CONTROL_RPC_TAG 0x01

/** status of a control-rpc instance */
struct control_rpc_s
{
  padico_topo_network_t session_network;
  nm_rpc_service_t p_service;
  nm_comm_t p_comm;
};

/* *** PadicoComponent driver ******************************** */

static void*control_rpc_instantiate(puk_instance_t instance, puk_context_t context);
static void control_rpc_destroy(void*_status);

/** instanciation facet for control_rpc
 */
static const struct puk_component_driver_s control_rpc_component_driver =
{
  .instantiate = &control_rpc_instantiate,
  .destroy     = &control_rpc_destroy
};

/* *** PadicoControl driver ******************************** */

static void control_rpc_send_common(void*_status, padico_topo_node_t node, padico_control_msg_t msg);

/** 'Control' facet for Control/PSP
 */
static const struct padico_control_driver_s control_rpc_control_driver =
{
  .send_common = &control_rpc_send_common
};

/* ********************************************************* */

PADICO_MODULE_COMPONENT(Control_RPC,
  puk_component_declare("Control_RPC",
                        puk_component_provides("PadicoComponent", "component", &control_rpc_component_driver),
                        puk_component_provides("PadicoControl",   "control",   &control_rpc_control_driver)));

/* ********************************************************* */

static void control_rpc_handler(nm_rpc_token_t p_token);
static void control_rpc_finalizer(nm_rpc_token_t p_token);

struct control_rpc_header_s
{
  size_t len;
};

static void*control_rpc_instantiate(puk_instance_t instance, puk_context_t context)
{
  struct control_rpc_s*p_status = padico_malloc(sizeof(struct control_rpc_s));
  const char*session_id = padico_topo_node_getsession(padico_topo_getlocalnode());
  p_status->session_network = padico_topo_session_getnetwork(session_id);
  p_status->p_comm = nm_comm_world("Control_RPC");
  nm_rpc_register(nm_comm_get_session(p_status->p_comm), CONTROL_RPC_TAG, NM_TAG_MASK_FULL,
                  &control_rpc_handler, &control_rpc_finalizer, p_status);
  return p_status;
}

static void control_rpc_destroy(void*_status)
{
  struct control_rpc_s*p_status = _status;
  nm_rpc_unregister(p_status->p_service);
  padico_free(p_status);
}

static void control_rpc_send_common(void*_status, padico_topo_node_t node, padico_control_msg_t msg)
{
  struct control_rpc_s*p_status = _status;
  padico_topo_address_t address = padico_topo_node_getaddress(node, p_status->session_network);
  const void*addr_bytes = padico_topo_address_getbytes(address);
  const int*p_rank = addr_bytes;
  nm_gate_t p_gate = nm_comm_get_gate(p_status->p_comm, *p_rank);
  struct iovec v = padico_control_msg_get_bytes(msg);
  struct control_rpc_header_s header;
  header.len = v.iov_len;
  nm_rpc_req_t req = nm_rpc_req_init(p_status->p_service, p_gate, CONTROL_RPC_TAG);
  nm_rpc_req_pack_header(req, &header, sizeof(header));
  nm_rpc_req_pack_body(req, v.iov_base, v.iov_len);
  nm_rpc_req_isend(req);
  nm_rpc_req_wait(req);
  padico_free(v.iov_base);
}

static void control_rpc_handler(nm_rpc_token_t p_token)
{
  struct control_rpc_header_s*header = NULL;
  nm_rpc_recv_header(p_token, &header, sizeof(struct control_rpc_header_s));
  char*body = padico_malloc(header->len);
  nm_rpc_irecv_body(p_token, body, header->len);
  nm_rpc_token_set_ref(p_token, body);
}

static void control_rpc_finalizer(nm_rpc_token_t p_token)
{
  char*body = nm_rpc_token_get_ref(p_token);
  padico_control_deliver_message(NULL, NULL, body, strlen(body));
  /* PadicoControl takes ownership of buffer 'body'; don't free here. */
}

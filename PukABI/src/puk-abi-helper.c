/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file Helper functions for virtual implementation that need
 * other libc functions and thus cannot be inline in Puk-ABI-symbols.h
 * @ingroup PukABI
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "Puk-ABI.h"


void puk_abi_helper__perror(const char *s)
{
  fprintf(stderr, "%s: %s\n", s, strerror(*(puk_abi_errno_stub()))) ;
}

int puk_abi_helper__vfscanf(void *_stream, const char *format, va_list _args)
{
  FILE *stream = _stream ;
  int _result  = vfscanf(stream, format, _args) ;
  return _result ;
}

int puk_abi_helper__vfprintf(void*_stream, const char *format, va_list _args)
{
  FILE*stream = _stream;
  int _result = vfprintf(stream, format, _args);
  return _result;
}

int puk_abi_helper__vprintf(const char *format, va_list _args)
{
  int _result = vprintf(format, _args);
  return _result;
}

/* Initial number of arguments for vararg functions of the `execv'
   family.  */
#define INITIAL_ARGV_MAX 1024

/* Build an argv vector from a va_list.
 * Excerpt from 'execl' from the GNU C Library (LGPLv2+).
 */
const char**puk_abi_helper_build_argv(const char*arg, va_list args, int*need_free)
{
  size_t argv_max = INITIAL_ARGV_MAX;
  static const char*initial_argv[INITIAL_ARGV_MAX];
  const char**argv = initial_argv;

  argv[0] = arg;
  unsigned int i = 0;
  while (argv[i++] != NULL)
    {
      if (i == argv_max)
        {
          argv_max *= 2;
          const char **nptr = realloc (argv == initial_argv ? NULL : argv,
                                       argv_max * sizeof (const char *));
          if (nptr == NULL)
            {
              if (argv != initial_argv)
                free (argv);
              return NULL;
            }
          if (argv == initial_argv)
            /* We have to copy the already filled-in data ourselves.  */
            memcpy (nptr, argv, i * sizeof (const char *));

          argv = nptr;
        }

      argv[i] = va_arg (args, const char *);
    }
  if (argv != initial_argv)
    *need_free = 1;
  return argv;
}

void puk_abi_helper_free_argv(const char**argv)
{
  free(argv);
}

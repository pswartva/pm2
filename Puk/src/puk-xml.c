/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Common interface for using the XML parser
 * @ingroup Puk
 */

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include "expat.h"

#include "Puk-internals.h"
#include "Module.h"

PADICO_MODULE_HOOK(Puk);

static inline uint32_t puk_tag_key_hash(const char*tag)
{
  return puk_hash_string(tag);
}
static inline int puk_tag_key_eq(const char*tag1, const char*tag2)
{
  return !strcmp(tag1, tag2);
}
static void puk_tag_action_destructor(const char*key, struct puk_tag_action_s*a)
{
  padico_free(a);
}

PUK_HASHTABLE_TYPE(puk_tag, const char*, struct puk_tag_action_s*,
                   &puk_tag_key_hash, &puk_tag_key_eq, &puk_tag_action_destructor);

static struct
{
  puk_tag_hashtable_t index; /**< hashed index actions- key: label; value: action */
} puk_parser = { .index = NULL };

static void puk_xml_start_handler(void *data, const char *el, const char **attr);
static void puk_xml_end_handler  (void *data, const char *el);
static void puk_xml_char_handler (void *data, const char*s, int len);


/* ********************************************************* */

static inline char**puk_xml_xa_copy(const char**xa)
{
  const char**i = xa;
  while(*i)
    i += 2;
  char**c = padico_malloc(((i - xa) + 1) * sizeof(char*));
  char**j = c;
  for(i = xa, j = c; *i; i++, j++)
    {
      *j = padico_strdup(*i);
    }
  *j = NULL;
  return c;
}

static inline void puk_xml_xa_free(char**xa)
{
  char**c = xa;
  while(*c)
    {
      padico_free(*c);
      c++;
    }
  padico_free(xa);
}

const char*puk_xml_xa_getattr(const char*label, char**xa)
{
  while(*xa != NULL)
    {
      if(strcmp(*xa, label) == 0)
        return xa[1];
      else
        xa += 2;
    }
  return NULL;
}

static void puk_xml_start_handler(void*data, const char*tag, const char**xa)
{
  struct puk_parse_context_s*parse_context = (struct puk_parse_context_s*)data;
  if(parse_context->status == PUK_PARSE_OK)
    {
      puk_lock_acquire();
      const struct puk_tag_action_s*action = puk_tag_hashtable_lookup(puk_parser.index, tag);
      if(!action)
        {
          if(padico_puk_finalized())
            {
              padico_warning("unknown tag %s while shutting down; discard.\n", tag);
            }
          else if(strchr(tag, ':') != NULL)
            {
              char*mod_tag = padico_strdup(tag);
              char*end = strchr(mod_tag, ':');
              *end = '\0';
              puk_mod_t mod = NULL;
              padico_rc_t rc = padico_puk_mod_resolve(&mod, mod_tag);
              padico_print("XML tag %s not found. Trying auto-load: %s...\n", tag, mod_tag);
              if(!padico_rc_iserror(rc))
                {
                  puk_mod_set_ancestor(mod, padico_module_self());
                  rc = padico_puk_mod_load(mod);
                  if(padico_rc_iserror(rc))
                    padico_fatal("XML tag auto-load failed: %s.\n", tag);
                  else
                    padico_print("XML tag auto-load success.\n");
                }
              else
                padico_fatal("XML tag auto-load failed.\n");
              padico_free(mod_tag);
              action = puk_tag_hashtable_lookup(puk_parser.index, tag);
            }
        }
      puk_lock_release();
      puk_parse_entity_t pe = padico_malloc(sizeof(struct puk_parse_entity_s));
      if(action)
        {
          if(parse_context->trust_level >= action->required_level)
            {
              pe->tag     = action->xml_tag;
              pe->xa      = puk_xml_xa_copy(xa);
              pe->text    = NULL;
              pe->rc      = NULL;
              pe->context = parse_context;
              pe->content = NULL;
              pe->content_len = 0;
              pe->parent  = puk_parse_entity_vect_empty(&parse_context->parse_stack) ? NULL : *puk_parse_entity_vect_rbegin(&parse_context->parse_stack);
              puk_parse_entity_vect_init(&pe->contained_entities);
              puk_parse_entity_vect_push_back(&parse_context->parse_stack, pe);
              if(action->start_handler)
                {
                  (*action->start_handler)(*puk_parse_entity_vect_rbegin(&parse_context->parse_stack));
                }

              if(puk_parse_entity_vect_size(&parse_context->parse_stack) > 1)
                {
                  puk_parse_entity_t parent = puk_parse_parent(pe);
                  puk_parse_entity_vect_push_back(&parent->contained_entities, pe);
                }
            }
          else
            {
              pe->rc = padico_rc_error("[XML parser] tag <%s> not allowed"
                                      " (tag level %d, parsing level %d)",
                                      action->xml_tag,
                                      action->required_level,
                                      parse_context->trust_level);
              parse_context->status = PUK_PARSE_UNTRUSTED;
            }
        }
      else
        {
          pe->context = parse_context;
          pe->tag = "";
          pe->text = NULL;
          pe->rc = padico_rc_error("Unknown XML element: **%s**", tag);
          parse_context->status = PUK_PARSE_UNKNOWN_TAG;
          parse_context->root_entity = *pe;
          padico_free(pe);
          pe = NULL;
        }
      if(parse_context->status != PUK_PARSE_OK && (pe != NULL) && padico_rc_iserror(pe->rc))
        {
          padico_rc_show(pe->rc);
        }
    }
}
static void puk_xml_end_handler(void*data, const char*tag)
{
  struct puk_parse_context_s*parse_context = (struct puk_parse_context_s*)data;
  if(parse_context->status == PUK_PARSE_OK)
    {
      puk_parse_entity_t entity = *puk_parse_entity_vect_rbegin(&parse_context->parse_stack);
      const struct puk_tag_action_s*action = puk_tag_hashtable_lookup(puk_parser.index, tag);
      if(action == NULL && strcmp(tag, entity->tag) != 0)
        {
          parse_context->status = PUK_PARSE_ERROR;
        }
      else
        {
          if(action->end_handler != NULL)
            {
              (*action->end_handler)(entity);
            }
        }
      /* free contained entities */
      puk_parse_entity_vect_itor_t i;
      puk_vect_foreach(i, puk_parse_entity, puk_parse_get_contained(entity))
        {
          puk_parse_entity_t e = *i;
          padico_free(e);
        }
      puk_parse_entity_vect_destroy(&entity->contained_entities);
      /* free attributes */
      if(entity->xa)
        {
          puk_xml_xa_free(entity->xa);
          entity->xa = NULL;
        }
      /* free text */
      if(entity->text)
        {
          padico_free(entity->text);
          entity->text = NULL;
        }
      if(parse_context->status == PUK_PARSE_OK &&
         puk_parse_entity_vect_size(&parse_context->parse_stack) == 1)
        {
          parse_context->root_entity = *entity;
          padico_free(entity);
        }
      puk_parse_entity_vect_pop_back(&parse_context->parse_stack);
    }
}

static void puk_xml_char_handler(void *data, const char*s, int len)
{
  struct puk_parse_context_s*parse_context = (struct puk_parse_context_s*)data;
  if(parse_context->status == PUK_PARSE_OK)
    {
      puk_parse_entity_t e = *puk_parse_entity_vect_rbegin(&parse_context->parse_stack);
      if(e->text == NULL)
        {
          /* first fragment */
          char*text = padico_malloc(len+1);
          strncpy(text, s, len);
          text[len] = 0;
          e->text = text;
        }
      else
        {
          /* next fragment */
          char*t = e->text;
          const int old_len = strlen(t);
          e->text = padico_malloc(old_len+len+1);
          strncpy(e->text, t, old_len);
          strncpy(e->text + old_len, s, len);
          e->text[old_len + len] = 0;
          padico_free(t);
        }
    }
}

/* ********************************************************* */


struct puk_parse_entity_s puk_xml_parse_buffer(const char*buffer, int len, int trust_level)
{
  struct puk_parse_context_s parse_context;

  if(!puk_parser.index)
    {
      padico_fatal("cannot parse buffer:\n%s\nXML parser not initialized.\n", buffer);
    }
  puk_parse_entity_vect_init(&parse_context.parse_stack);
  parse_context.buffer = buffer;
  parse_context.size = len;
  parse_context.trust_level     = trust_level;
  parse_context.root_entity.tag = "";
  parse_context.status          = PUK_PARSE_OK;

  XML_Parser parser = XML_ParserCreate(NULL);
  if(!parser)
    {
      parse_context.root_entity.rc = padico_rc_error("[XML parser] Couldn't allocate memory.");
      return parse_context.root_entity;
    }
  XML_SetElementHandler(parser, &puk_xml_start_handler, &puk_xml_end_handler);
  XML_SetCharacterDataHandler(parser, &puk_xml_char_handler);
  XML_SetUserData(parser, &parse_context);

  int rc = XML_Parse(parser, buffer, strlen(buffer), 1);
  if(rc == XML_STATUS_ERROR)
    {
      parse_context.root_entity.rc = padico_rc_error("Parse error at line %d: %s\nDocument = %s",
                                                     XML_GetCurrentLineNumber(parser),
                                                     XML_ErrorString(XML_GetErrorCode(parser)), buffer);
    }
  else
    {
      if(!puk_parse_entity_vect_empty(&parse_context.parse_stack))
        {
          parse_context.root_entity.rc = padico_rc_error("Parse stack not empty (size=%d)",
                                                         puk_parse_entity_vect_size(&parse_context.parse_stack));
        }
      puk_parse_entity_vect_destroy(&parse_context.parse_stack);
      if(parse_context.status != PUK_PARSE_OK)
        {
          if(!padico_rc_iserror(parse_context.root_entity.rc))
            {
              parse_context.root_entity.rc = padico_rc_error("Parse error- status=%d\n", parse_context.status);
            }
        }
    }
  XML_ParserFree(parser);
  return parse_context.root_entity;
}

struct puk_parse_entity_s puk_xml_parse_file(const char*filename, int trust_level)
{
  struct puk_parse_entity_s e = { .rc = NULL };
  struct stat finfo;
  padico_out(50, "opening file *%s*\n", filename);
  int fd = open(filename, O_RDONLY, 0);
  if(fd == -1)
    {
      int retry = 10;
      while(fd == -1 && retry-- > 0)
        {
          puk_usleep(100000);
          fd = open(filename, O_RDONLY, 0);
        }
    }
  if(fd == -1)
    {
      int err = errno;
      puk_parse_set_rc(&e, padico_rc_error("cannot open file *%s* (%s)\n", filename, strerror(err)));
      return e;
    }
  fstat(fd, &finfo);
  int size = finfo.st_size;
  assert(size > 0);
  char*buffer = mmap(NULL, size, PROT_READ, MAP_PRIVATE, fd, 0);
  assert(buffer != NULL);
  e = puk_xml_parse_buffer(buffer, size, trust_level);
  munmap(buffer, size);
  close(fd);
  return e;
}

struct puk_parse_entity_s puk_xml_parse_init(const char*init_name)
{
  char*filename = NULL;
  padico_string_t s_fname = NULL;
  if(strchr(init_name, '/') == NULL)
    {
      puk_path_vect_t path = puk_path_get();
      puk_path_vect_itor_t i;
      puk_vect_foreach(i, puk_path, path)
        {
          if(s_fname)
            padico_string_delete(s_fname);
          s_fname = padico_string_new();
          padico_string_catf(s_fname, "%s/etc/padico/init.d/%s.xml", *i, init_name);
          if(access(padico_string_get(s_fname), F_OK) == 0)
            {
              filename = padico_strdup(padico_string_get(s_fname));
              break;
            }
        }
    }
  else
    {
      filename = padico_strdup(init_name);
    }
  if(s_fname)
    {
      padico_string_delete(s_fname);
    }
  if(filename)
    {
      padico_print("loading init file %s\n", filename);
      struct puk_parse_entity_s e = puk_xml_parse_file(filename, PUK_TRUST_CONTROL);
      padico_free(filename);
      return e;
    }
  else
    {
      padico_fatal("cannot find init file %s.\n", init_name);
    }
}

/* *** XML actions ***************************************** */

void puk_xml_add_action(struct puk_tag_action_s action)
{
  puk_lock_acquire();
  if(puk_parser.index == NULL)
    padico_puk_xml_init();
  if(action.help == NULL)
    action.help = "No help available";
  if(!puk_tag_hashtable_probe(puk_parser.index, action.xml_tag))
    {
      struct puk_tag_action_s* a = padico_malloc(sizeof(struct puk_tag_action_s));
      *a = action;
      puk_tag_hashtable_insert(puk_parser.index, action.xml_tag, a);
    }
  else
    {
      padico_warning("XML tag %s already registered.\n", action.xml_tag);
    }
  puk_lock_release();
}

void puk_xml_delete_action(const char*tag)
{
  puk_lock_acquire();
  struct puk_tag_action_s*a = puk_tag_hashtable_lookup(puk_parser.index, tag);
  if(a)
    {
      puk_tag_hashtable_remove(puk_parser.index, tag);
      padico_free(a);
    }
  puk_lock_release();
}

padico_rc_t puk_xml_lsaction()
{
  padico_rc_t rc = padico_rc_msg("<pukactions>\n");
  puk_tag_hashtable_enumerator_t e = puk_tag_hashtable_enumerator_new(puk_parser.index);
  struct puk_tag_action_s* a = puk_tag_hashtable_enumerator_next_data(e);
  while(a)
    {
      rc = padico_rc_add(rc, padico_rc_msg("  <tag>%s</tag>\n", a->xml_tag));
      a = puk_tag_hashtable_enumerator_next_data(e);
    }
  rc = padico_rc_add(rc, padico_rc_msg("</pukactions>\n"));
  puk_tag_hashtable_enumerator_delete(e);
  return rc;
}

/* ********************************************************* */

static void sequence_end_handler(puk_parse_entity_t e)
{
  const char*error = puk_parse_getattr(e, "error");
  puk_parse_entity_vect_t c = puk_parse_get_contained(e);
  puk_parse_entity_vect_itor_t i;
  puk_vect_foreach(i, puk_parse_entity, c)
    {
      padico_rc_t rc = puk_parse_get_rc(*i);
      if(padico_rc_iserror(rc))
        {
          puk_parse_set_rc(e, rc);
          if((error == NULL) || strcmp(error, "fatal") == 0)
            {
              padico_fatal("error while running 'sequence': %s\n", padico_rc_gettext(rc));
            }
          else if(strcmp(error, "warning") == 0)
            {
              padico_warning("error while running 'sequence': %s\n", padico_rc_gettext(rc));
            }
          else if(strcmp(error, "ignore") == 0)
            {
              /* do nothing */
            }
          break;
        }
    }
}
static const struct puk_tag_action_s sequence_action =
{
  .xml_tag        = "sequence",
  .start_handler  = NULL,
  .end_handler    = &sequence_end_handler,
  .required_level = PUK_TRUST_OTHER,
  .help           = "sequence of commands"
};

/* ********************************************************* */

static void puk_part_handler(puk_parse_entity_t e)
{
  const char*s_offset = puk_parse_getattr(e, "offset");
  const char*s_len = puk_parse_getattr(e, "len");
  const int offset = atoi(s_offset);
  const int len = atoi(s_len);
  struct puk_parse_context_s*context = e->context;
  struct iovec part = { .iov_base = (void*)context->buffer + strlen(context->buffer) + 1 + offset, .iov_len = len };
  assert(strlen(context->buffer) + 1 + offset + len <= context->size);
  e->content = part.iov_base;
  e->content_len = part.iov_len;
}
static const struct puk_tag_action_s puk_part_action =
  {
    .xml_tag = "puk:part",
    .start_handler = NULL,
    .end_handler = &puk_part_handler,
    .required_level = PUK_TRUST_OTHER,
    .help = "part of a multi-part message"
  };

/* ********************************************************* */

static void include_end_handler(puk_parse_entity_t e)
{
  const char*href = puk_parse_getattr(e, "href");
  const char*text = puk_parse_get_text(e);
  const char*file = (href != NULL) ? href : text;
  struct puk_parse_entity_s result = puk_xml_parse_file(file, PUK_TRUST_CONTROL);
  puk_parse_set_rc(e, result.rc);
}
static const struct puk_tag_action_s include_action =
{
  .xml_tag        = "include",
  .start_handler  = NULL,
  .end_handler    = &include_end_handler,
  .required_level = PUK_TRUST_OTHER,
  .help           = "include external XML file"
};

/* ********************************************************* */

static void help_end_handler(puk_parse_entity_t e)
{
  const char*text = puk_parse_get_text(e);
  padico_rc_t rc = NULL;
  if(text == NULL)
    {
      rc = puk_xml_lsaction();
    }
  else
    {
      const struct puk_tag_action_s*a = puk_tag_hashtable_lookup(puk_parser.index, text);
      if(a)
        {
          rc = padico_rc_msg("<tag trustlevel=\"%d\">\n"
                             "  <name>%s</name>\n"
                             "  <helptext>\n"
                             "<![CDATA[\n%s\n]]>\n"
                             "</helptext>\n"
                             "</tag>\n",
                             a->required_level, a->xml_tag, a->help);
        }
      else
        {
          rc = padico_rc_error("tag '%s' not found.", text);
        }
    }
  puk_parse_set_rc(e, rc);
}
static const struct puk_tag_action_s help_action =
{
  .xml_tag        = "help",
  .start_handler  = NULL,
  .end_handler    = &help_end_handler,
  .required_level = PUK_TRUST_OTHER,
  .help           = "Shows help on XML tags. Usage:\n"
                    "  <help/>: shows a list of tags\n"
                    "  <help>tag</help>: shows help on 'tag'"
};

/* ********************************************************* */

void padico_puk_xml_init(void)
{
  if(puk_parser.index != NULL)
    return;
  puk_parser.index = puk_tag_hashtable_new();
  puk_xml_add_action(help_action);
  puk_xml_add_action(sequence_action);
  puk_xml_add_action(puk_part_action);
  puk_xml_add_action(include_action);
}

void padico_puk_xml_finalize(void)
{
  puk_tag_hashtable_delete(puk_parser.index);
}

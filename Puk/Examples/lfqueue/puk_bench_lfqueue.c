/* simple benchmark for lfqueues
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <pthread.h>

#include <Padico/Puk.h>

#define LFQUEUE_SIZE (1024)

#define ARRAY_SIZE (1024*64) /* (1024*1024*16) */

static uint64_t array[ARRAY_SIZE];

static pthread_barrier_t b1, b2;

#ifdef QUEUE_TYPE
#define MKTYPE(Q) PUK_LFQUEUE_## Q ## _TYPE
#define LFQUEUE_BENCH_TYPE_(Q) MKTYPE(Q)
#define LFQUEUE_BENCH_TYPE(QUEUE_TYPE, ENAME, TYPE, SIZE) \
  LFQUEUE_BENCH_TYPE_(QUEUE_TYPE)(ENAME, TYPE, NULL, SIZE)
LFQUEUE_BENCH_TYPE(QUEUE_TYPE, bench, uint64_t*, LFQUEUE_SIZE);
#define MKKIND(Q) #Q
#define QUEUE_KIND_(Q) MKKIND(Q)
#define QUEUE_KIND QUEUE_KIND_(QUEUE_TYPE)
#else
PUK_LFQUEUE_TYPE(bench, uint64_t*, NULL, LFQUEUE_SIZE);
#define QUEUE_KIND PUK_LFQUEUE_KIND
#endif

static struct bench_lfqueue_s q;


/* ** producer/consumer ************************************ */

static void*producer(void*_d)
{
  pthread_barrier_wait(&b1);
  int d = (uintptr_t)_d;
  int i;
  for(i = 0; i < d; i++)
    {
      int rc = -1;
      do
        {
          rc = bench_lfqueue_enqueue(&q, &array[i % ARRAY_SIZE]);
        }
      while(rc != 0);
    }
  pthread_barrier_wait(&b2);
  return NULL;
}

static void*consumer(void*_d)
{
  pthread_barrier_wait(&b1);
  int d = (uintptr_t)_d;
  int i;
  for(i = 0; i < d; i++)
    {
      uint64_t*v = NULL;
      do
        {
          v = bench_lfqueue_dequeue(&q);
        }
      while(v == NULL);
    }
  pthread_barrier_wait(&b2);
  return NULL;
}

static void benchmark_producerconsumer(int np, int nc)
{
  assert((nc == 1) || (np == 1) || (nc == np));
  int dp = (np == 1) ? nc * ARRAY_SIZE : ARRAY_SIZE;
  int dc = (nc == 1) ? np * ARRAY_SIZE : ARRAY_SIZE;

  if(nc + np <= 2)
    {
      dp *= 8;
      dc *= 8;
    }
  else if(nc + np <= 4)
    {
      dp *= 2;
      dc *= 2;
    }

  bench_lfqueue_init(&q);

  fprintf(stderr, "# starting %d -> %d threads ... dp = %d; dc = %d\n", np, nc, dp, dc);
  puk_tick_t t1, t2;
  PUK_GET_TICK(t1);
  if(np == 0 && nc == 0)
    {
      /* no thread */
      int i;
      for(i = 0; i < dc; i++)
        {
          int rc __attribute__((unused)) = -1;
          rc = bench_lfqueue_enqueue(&q, &array[i % ARRAY_SIZE]);
          assert(rc == 0);
          uint64_t*v __attribute__((unused)) = bench_lfqueue_dequeue(&q);
          assert(v != NULL);
        }
    }
  else
    {
      pthread_t p[np], c[nc];
      pthread_barrier_init(&b1, NULL, np + nc);
      pthread_barrier_init(&b2, NULL, np + nc);
      int k;
      for(k = 0; k < nc; k++)
        {
          pthread_create(&c[k], NULL, &consumer, (void*)(uintptr_t)dc);
        }
      for(k = 0; k < np; k++)
        {
          pthread_create(&p[k], NULL, &producer, (void*)(uintptr_t)dp);
        }
      for(k = 0; k < nc; k++)
        {
          pthread_join(c[k], NULL);
        }
      for(k = 0; k < np; k++)
        {
          pthread_join(p[k], NULL);
        }
      pthread_barrier_destroy(&b1);
      pthread_barrier_destroy(&b2);
    }
  PUK_GET_TICK(t2);
  double d = padico_timing_diff_usec(&t1, &t2);
  printf("%d \t %d \t %f\n", np, nc, ((np?:1) * dp) / d);
}

/* ** pairwise benchmark *********************************** */

static void*pairwise_worker(void*_d)
{
  pthread_barrier_wait(&b1);
  int d = (uintptr_t)_d;
  int i;
  for(i = 0; i < d; i++)
    {
      int rc = -1;
      do
        {
          rc = bench_lfqueue_enqueue(&q, &array[i % ARRAY_SIZE]);
        }
      while(rc != 0);
      uint64_t*v = NULL;
      do
        {
          v = bench_lfqueue_dequeue(&q);
        }
      while(v == NULL);
    }
  pthread_barrier_wait(&b2);
  return NULL;
}

static void benchmark_pairwise(int n, int preload)
{
  const int s = ARRAY_SIZE;
  bench_lfqueue_init(&q);
  if(preload)
    {
      /* preload some data so that queue is never empty */
      int i;
      for(i = 0; i < preload; i++)
        {
          bench_lfqueue_enqueue(&q, &array[i % ARRAY_SIZE]);
        }
    }

  fprintf(stderr, "# starting pairwise %d threads ... s = %d\n", n, s);
  pthread_t t[n];
  pthread_barrier_init(&b1, NULL, n);
  pthread_barrier_init(&b2, NULL, n);
  puk_tick_t t1, t2;
  PUK_GET_TICK(t1);
  if(n == 0)
    {
      pairwise_worker((void*)(uintptr_t)s);
    }
  else
    {
      int k;
      for(k = 0; k < n; k++)
        {
          pthread_create(&t[k], NULL, &pairwise_worker, (void*)(uintptr_t)s);
        }
      for(k = 0; k < n; k++)
        {
          pthread_join(t[k], NULL);
        }
      fprintf(stderr, "# run end. \n");
    }
  PUK_GET_TICK(t2);
  double d = padico_timing_diff_usec(&t1, &t2);
  printf("%d \t %f\n", n, (n == 0) ? (2 * s / d) : ((2 * n * s) / d));

  pthread_barrier_destroy(&b1);
  pthread_barrier_destroy(&b2);
}

/* ** benchmark empty ************************************** */

static void*empty_worker(void*_d)
{
  pthread_barrier_wait(&b1);
  int d = (uintptr_t)_d;
  int i;
  for(i = 0; i < d; i++)
    {
      uint64_t*v __attribute__((unused)) = bench_lfqueue_dequeue(&q);
    }
  pthread_barrier_wait(&b2);
  return NULL;
}

static void benchmark_empty(int n)
{
  const int s = ARRAY_SIZE;
  bench_lfqueue_init(&q);

  fprintf(stderr, "# starting pairwise %d threads ... s = %d\n", n, s);
  pthread_t t[n];
  pthread_barrier_init(&b1, NULL, n);
  pthread_barrier_init(&b2, NULL, n);
  puk_tick_t t1, t2;
  PUK_GET_TICK(t1);
  if(n == 0)
    {
      empty_worker((void*)(uintptr_t)s);
    }
  else
    {
      int k;
      for(k = 0; k < n; k++)
        {
          pthread_create(&t[k], NULL, &empty_worker, (void*)(uintptr_t)s);
        }
      for(k = 0; k < n; k++)
        {
          pthread_join(t[k], NULL);
        }
      fprintf(stderr, "# run end. \n");
    }
  PUK_GET_TICK(t2);
  double d = padico_timing_diff_usec(&t1, &t2);
  printf("%d \t %f\n", n, (n == 0) ? (s / d) : ((n * s) / d));

  pthread_barrier_destroy(&b1);
  pthread_barrier_destroy(&b2);
}

int main(int argc, char**argv)
{
  const int max_threads = 16;
  padico_puk_init();

  int i;
  for(i = 0; i < ARRAY_SIZE; i++)
    {
      array[i] = i;
    }

  const char*op = NULL;
  int n = -1;
  if(argc == 3)
    {
      op = argv[1];
      n = atoi(argv[2]);
    }
  else if(argc == 2)
    {
      if(argv[1][0] == '-')
        {
          op = argv[1];
        }
      else
        {
          n = atoi(argv[1]);
        }
    }
  if(n > 0)
    {
      printf("# forced n = %d\n", n);
    }

  printf("# lfqueue kind = %s\n", QUEUE_KIND);
  printf("# N.prod\t| N.cons\t| throughput M.elements / s\n");

  if(op && strcmp(op, "-p") == 0)
    {
      printf("# benchmark type: pairwise\n");
      if(n > 0)
        {
          benchmark_pairwise(n, 0);
        }
      else
        {
          for(n = 1; n <= max_threads; n++)
            {
              benchmark_pairwise(n, 0);
            }
        }
    }
  else if(op && strcmp(op, "-pp") == 0)
    {
      printf("# benchmark type: pairwise with preload\n");
      if(n > 0)
        {
          benchmark_pairwise(n, 16);
        }
      else
        {
          for(n = 1; n <= max_threads; n++)
            {
              benchmark_pairwise(n, 16);
            }
        }
    }

  else if(op && strcmp(op, "-e") == 0)
    {
      printf("# benchmark type: empty queue\n");
      if(n > 0)
        {
          benchmark_empty(n);
        }
      else
        {
          for(n = 1; n <= max_threads; n++)
            {
              benchmark_empty(n);
            }
        }
    }
  else if((op == NULL) || (op && strcmp(op, "-pc") == 0))
    {
      printf("# benchmark type: producer/consumer\n");
      if(n > 0)
        {
          if(n == 1)
            {
              benchmark_producerconsumer(n, n);
            }
          else
            {
              benchmark_producerconsumer(1, n);
              benchmark_producerconsumer(n, 1);
              benchmark_producerconsumer(n, n);
            }
        }
      else
        {
          benchmark_producerconsumer(0, 0);
          for(n = 1; n <= max_threads; n++)
            {
              benchmark_producerconsumer(1, n);
              benchmark_producerconsumer(n, 1);
              benchmark_producerconsumer(n, n);
            }
        }
    }

  puk_profile_dump();
  padico_puk_shutdown();
  return 0;
}

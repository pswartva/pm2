/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Internal structures definition for VIO -- This is *not* the public API.
 * @ingroup VIO
 */

/** @defgroup VIO Service: VIO -- PadicoTM virtual I/O
 * @author Alexandre Denis
 */

#include <Padico/Puk.h>
#include <Padico/PadicoTM.h>
#include <Padico/PM2.h>
#include <Padico/Topology.h>

#include "VIO.h"
#include "VLink-API.h"

#ifndef VIO_INTERNALS_H
#define VIO_INTERNALS_H

#if defined(PUKABI) && defined(PUKABI_ENABLE_FSYS)
/** whether to plug VIO into PukABI */
#define VIO_PUK_ABI 1
#endif


/** Attach a VIO file descriptor to a vnode
 */
void vio_store_desc(padico_vnode_t vnode, int vfd);

#endif /* VIO_INTERNALS_H */

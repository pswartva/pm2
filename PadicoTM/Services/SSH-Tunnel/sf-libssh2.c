/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief SocketFactory using libssh2
 * @author Sebastien Barascou
 */

#include <Padico/PadicoTM.h>
#include <Padico/VLink-internals.h>
#include <Padico/Module.h>
#include <Padico/SocketFactory.h>
#include <Padico/SysIO.h>

#include <libssh2.h>


static int sflibssh2_module_init(void);
static void sflibssh2_module_finalize(void);

PADICO_MODULE_DECLARE(SocketFactory_LibSSH2, sflibssh2_module_init, NULL, sflibssh2_module_finalize,
                      "SysIO", "Topology", "PadicoControl");


#define SSH2_TMP_BUF_SIZE 16384

/** @defgroup SF_libssh2 component: SocketFactory-libssh2- socket factory for communication through SSH2 tunnels
 * @ingroup PadicoComponent
 */

/* ********************************************************* */

static int sflibssh2_create(void*_instance, int family, int type, int protocol);
static int sflibssh2_connect(void*_instance, const struct sockaddr*addr, socklen_t addrlen, padico_socketfactory_connector_t notifier, void*key);
static int sflibssh2_close(void*_instance);

/** 'SocketFactory' facet for SocketFactory-libssh2
 * @ingroup SF_libssh2
 */
static const struct padico_socketfactory_s sflibssh2_sf_driver =
  {
    .create  = &sflibssh2_create,
    .bind    = NULL,
    .listen  = NULL,
    .connect = &sflibssh2_connect,
    .close   = &sflibssh2_close,
    .caps    = padico_vsock_caps_directconnect | padico_vsock_caps_clientonly
  };

static void*sflibssh2_instantiate(puk_instance_t, puk_context_t);
static void sflibssh2_destroy(void*);

/** instanciation facet for SocketFactory-libssh2
 * @ingroup SF_libssh2
 */
static const struct puk_component_driver_s sflibssh2_component_driver =
  {
    .instantiate = &sflibssh2_instantiate,
    .destroy     = &sflibssh2_destroy
  };

/** instance of SocketFactory-libssh2
 * @ingroup SF_libssh2
 */
struct sflibssh2_status_s
{
  int fd;
  padico_io_t io_connect;
  padico_socketfactory_connector_t connector;
  void*connector_key;
  const char*ssh_host;
  const char*ssh_user;
  const char*ssh_cipher;
  LIBSSH2_SESSION *session;
  LIBSSH2_CHANNEL *channel;
  marcel_t thread;
  int ssh_server_fd;
  int local_listen_fd;
};

static int sflibssh2_callback_connector(int fd, void*key);

/* ********************************************************* */

static struct
{
  padico_tm_bgthread_pool_t pool;
} sflibssh2;

static int sflibssh2_module_init(void)
{
  char*username = getenv("USER");
  const char*default_cipher = "arcfour,aes128-cbc,blowfish-cbc,aes192-cbc,cast128-cbc,3des-cbc";
  sflibssh2.pool = padico_tm_bgthread_pool_create("SocketFactory_LibSSH2");
  puk_component_declare("SocketFactory_LibSSH2",
                        puk_component_provides("PadicoComponent", "component", &sflibssh2_component_driver),
                        puk_component_provides("SocketFactory", "sf", &sflibssh2_sf_driver),
                        puk_component_attr("ssh_host", "localhost"),
                        puk_component_attr("ssh_user", username),
                        puk_component_attr("ssh_cipher", default_cipher));
  return 0;
}

static void sflibssh2_module_finalize(void)
{
  padico_tm_bgthread_pool_wait(sflibssh2.pool);
}

/* ********************************************************* */
/* *** 'PadicoComponent' for SocketFactory_LibSSH2 */

static void*sflibssh2_instantiate(puk_instance_t instance, puk_context_t context)
{
  struct sflibssh2_status_s*status = padico_malloc(sizeof(struct sflibssh2_status_s));
  status->fd            = -1;
  status->ssh_server_fd = -1;
  status->io_connect    = NULL;
  status->connector     = NULL;
  status->ssh_host      = puk_context_getattr(context, "ssh_host");
  status->ssh_user      = puk_context_getattr(context, "ssh_user");
  status->ssh_cipher    = puk_context_getattr(context, "ssh_cipher");
  status->session       = NULL;
  status->channel       = NULL;
  padico_out(40, "done.\n");
  return status;
}

static void sflibssh2_destroy(void*_status)
{
  struct sflibssh2_status_s*status = _status;
  if(status->io_connect)
    {
      padico_io_deactivate(status->io_connect);
      padico_io_unregister(status->io_connect);
      status->io_connect = NULL;
    }
  padico_free(status);
  padico_out(40, "done.\n");
}

/* ********************************************************* */
/* *** helper functions for SocketFactory_LibSSH2 */

static void *sflibss2_start_thread(void* _status)
{
  /* struct sched_param p = { .sched_priority = 95 }; */
  /* pthread_setschedparam(pthread_self(), SCHED_RR, &p); */

  struct sflibssh2_status_s*status = _status;
  struct pollfd fds[2];
  char buf[ SSH2_TMP_BUF_SIZE];
  int rc, i;
  ssize_t len, done;
  while(1)
    {
      fds[0].fd = status->ssh_server_fd;
      fds[0].events = POLLIN;
      fds[0].revents = 0;
      fds[1].fd = status->local_listen_fd;
      fds[1].events = POLLIN;
      fds[1].revents = 0;

      rc = PUK_ABI_FSYS_WRAP(poll)(fds, 2, -1);
      if (rc)
        {
          if(fds[1].revents & POLLIN)
            {
              len =  PUK_ABI_FSYS_WRAP(recv)(status->local_listen_fd, buf, sizeof(buf), 0);
              if ( len <= 0)
                goto exit;
              done = 0;
              do
                {
                  i = libssh2_channel_write(status->channel, buf + done, len - done);
                  if(i < 0)
                    goto exit;
                  done += i;
                }
              while(i > 0 && done < len);
            }
          if(fds[0].revents & POLLIN)
            {
              while(1)
                {
                  len = libssh2_channel_read(status->channel, buf, sizeof(buf));
                  if(LIBSSH2_ERROR_EAGAIN == len)
                    break;
                  else if(len < 0)
                    goto exit;
                  rc = padico_sysio_out(status->local_listen_fd, buf, len);
                  if(rc < 0)
                    goto exit;
                  assert(rc == len);
                  if(libssh2_channel_eof(status->channel))
                    goto exit;
                }
            }
        }
    }
 exit:
  if(status->local_listen_fd > 0)
    {
      padico_sysio_close(status->local_listen_fd);
      status->local_listen_fd = -1;
    }
  padico_out(40, "SocketFactory-LIBSSH2 exit polling thread\n");
  marcel_exit(NULL);
}

static int sflibssh2_authenticate(struct sflibssh2_status_s*status)
{
  struct libssh2_agent_publickey *prev_identity = NULL, *identity = NULL;
  LIBSSH2_AGENT *agent = NULL;
  int rc = -1;
  const char *userauthlist = libssh2_userauth_list(status->session, status->ssh_user, strlen(status->ssh_user));
  if (strstr(userauthlist, "publickey") == NULL)
    {
      padico_warning("ssh server %s don't support \"publickey\" authentication\n", status->ssh_host);
      return -1;
    }
  agent = libssh2_agent_init(status->session);
  if (!agent)
    {
      padico_warning("Failure initializing ssh-agent support\n");
      return -1;
    }
  if (libssh2_agent_connect(agent))
    {
      padico_warning("Failure connecting to ssh-agent\n");
      return -1;
    }
  if (libssh2_agent_list_identities(agent))
    {
      padico_warning("Failure requesting identities to ssh-agent\n");
      return -1;
    }
  while (1)
    {
      rc = libssh2_agent_get_identity(agent, &identity, prev_identity);
      if (rc == 1)
        break;
      if (rc < 0)
        {
          padico_warning("Failure obtaining identity from ssh-agent support\n");
          return -1;
        }
      if (libssh2_agent_userauth(agent, status->ssh_user, identity) == 0)
        {
          break;
        }
      prev_identity = identity;
    }
  if (rc)
    {
      padico_warning("Couldn't find a good key in the ssh-agent identity collection\n");
      return -1;
    }
  if(agent)
    {
      libssh2_agent_disconnect(agent);
      libssh2_agent_free(agent);
    }
  return 0;
}

static int sflibssh2_socket_connect_ssh_server(struct sflibssh2_status_s*status)
{
  struct addrinfo *res;
  if(getaddrinfo(status->ssh_host, "ssh", NULL, &res))
    {
      padico_warning("Cannot find ssh server: %s\n", status->ssh_host);
      return -ENETUNREACH;
    }
  int fd = padico_sysio_connected_inet_socket_sockaddr(res[0].ai_addr);
  return fd;
}

/* ********************************************************* */
/* *** 'SocketFactory' for SocketFactory_LibSSH2 */

static int sflibssh2_create(void*_status, int family, int type, int protocol)
{
  struct sflibssh2_status_s*status = _status;
  int err = 0;
  if(family != AF_INET)
    {
      err = EAFNOSUPPORT;
      goto exit;
    }
  if(type != SOCK_STREAM)
    {
      err = EPROTONOSUPPORT;
      goto exit;
    }
  if(status->ssh_host == NULL)
    {
      padico_warning("SocketFactory-LIBSSH2: no hostname given- cannot create.\n");
      err = EINVAL;
      goto exit;
    }
  status->ssh_server_fd = sflibssh2_socket_connect_ssh_server(status);
  if(status->ssh_server_fd <= 0)
    {
      padico_warning("SocketFactory-LIBSSH2: Cannot connect to the ssh server %s.\n", status->ssh_host);
      err = EINVAL;
      goto exit;
    }
  status->session = libssh2_session_init();
  libssh2_session_method_pref(status->session, LIBSSH2_METHOD_CRYPT_CS, status->ssh_cipher);
  if(libssh2_session_startup(status->session, status->ssh_server_fd))
    {
      padico_warning("SocketFactory-LIBSSH2: Start or init libssh failed\n");
      padico_sysio_close(status->ssh_server_fd);
      status->ssh_server_fd = -1;
      libssh2_session_free(status->session);
      status->session = NULL;
      err = EINVAL;
      goto exit;
    }
  if(sflibssh2_authenticate(status))
    {
      padico_sysio_close(status->ssh_server_fd);
      status->ssh_server_fd = -1;
      libssh2_session_disconnect(status->session, "Authentication fail\n");
      libssh2_session_free(status->session);
      status->session = NULL;
      err = EINVAL;
      goto exit;
    }

 exit:
  padico_out(40, "err=%d\n", err);
  return -err;
}

static int sflibssh2_connect(void*_status, const struct sockaddr*addr, socklen_t addrlen,
                             padico_socketfactory_connector_t notifier, void*key)
{
  struct sflibssh2_status_s*status = _status;
  struct sockaddr_in*inaddr = (struct sockaddr_in*)addr;
  int sv[2];
  int rc = -1, err = 0;
  int sockopt = 1;
  if(status->ssh_host == NULL)
    {
      padico_warning("SocketFactory_LibSSH2: no hostname given- cannot connect.\n");
      err = ENETUNREACH;
      goto exit;
    }
  rc  = PUK_ABI_FSYS_WRAP(socketpair)(AF_UNIX, SOCK_STREAM, 0, sv);
  if (rc)
    {
      err = padico_sysio_errno();
      goto exit;
    }
  rc = PUK_ABI_FSYS_WRAP(setsockopt)(sv[0], SOL_SOCKET, SO_KEEPALIVE, &sockopt, sizeof(sockopt));
  if(rc)
    {
      err = padico_sysio_errno();
      goto exit;
    }
  rc = PUK_ABI_FSYS_WRAP(setsockopt)(sv[1], SOL_SOCKET, SO_KEEPALIVE, &sockopt, sizeof(sockopt));
  if(rc)
    {
      err = padico_sysio_errno();
      goto exit;
    }
  status->fd = sv[1];
  status->local_listen_fd = sv[0];
  const char * remote_desthost = inet_ntoa(inaddr->sin_addr);
  unsigned int remote_destport = ntohs(inaddr->sin_port);
  padico_out(40, "Forwarding connection to remote %s:%d\n",
             remote_desthost, remote_destport);
  status->channel = libssh2_channel_direct_tcpip(status->session, remote_desthost,
                                                 remote_destport);
  if (!status->channel)
    {
      padico_warning("SocketFactory-LIBSSH2: Could not connect to %s:%d with ssh server %s!\n",
                     remote_desthost, remote_destport, status->ssh_host);
      err = ENETUNREACH;
      goto exit;
    }
  libssh2_session_set_blocking(status->session, 0);
  marcel_create(&status->thread, NULL, sflibss2_start_thread, status);
  marcel_detach(status->thread);
  padico_out(40, "Connect with ssh server: %s to %s:%d done\n",
             status->ssh_host, remote_desthost, remote_destport);
  status->connector = notifier;
  status->connector_key = key;
  status->io_connect = padico_io_register(status->fd, PADICO_IO_EVENT_WRITE,
                                          &sflibssh2_callback_connector, status);
  padico_io_activate(status->io_connect);
  return 0;

 exit:
  padico_out(30, "fd=%d; err=%d (%s)\n", status->fd, err, strerror(err));
  return -err;
}

static int sflibssh2_close(void*_status)
{
  struct sflibssh2_status_s*status = _status;
  int rc = -1;
  if(status->io_connect)
    {
      padico_io_deactivate(status->io_connect);
      padico_io_unregister(status->io_connect);
      status->io_connect = NULL;
    }
  if(status->fd != -1)
    {
      rc = padico_sysio_close(status->fd);
      status->fd = -1;
    }
  if(status->local_listen_fd != -1)
    {
      rc = padico_sysio_close(status->fd);
      status->local_listen_fd = -1;
    }
  if(status->channel)
    {
      libssh2_channel_close(status->channel);
      libssh2_channel_free(status->channel);
    }
  if(status->session)
    {
      libssh2_session_disconnect(status->session,
                                 "Normal Shutdown, SocketFactory-LIBSSH2 close");
      libssh2_session_free(status->session);
    }
  if(status->ssh_server_fd != -1)
    {
      rc = padico_sysio_close(status->ssh_server_fd);
      status->ssh_server_fd = -1;
    }
  return rc;
}

/* ********************************************************* */

static void*sflibssh2_connector_finalizer(void*_status)
{
  struct sflibssh2_status_s*status = _status;
  (*status->connector)(status->connector_key, status->fd);
  return NULL;
}

static int sflibssh2_callback_connector(int fd, void*key)
{
  int again = 0;
  struct sflibssh2_status_s*status = key;
  padico_out(40, "fd=%d\n", fd);
  assert(fd == status->fd);
  padico_tm_bgthread_start(sflibssh2.pool, &sflibssh2_connector_finalizer, status,
                           "SocketFactory_LibSSH2:sflibssh2_connector_finalizer");
  return again;
}

/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <assert.h>

#include <nm_minidriver.h>
#include <nm_private.h>
#include <Padico/Module.h>

#include <simgrid/mailbox.h>
#include <simgrid/comm.h>
#include <simgrid/link.h>
#include <simgrid/actor.h>

/** cost of polling */
#define NM_SG_LARGE_TIME_POLL 0.00001

static void*nm_sg_large_instantiate(puk_instance_t instance, puk_context_t context);
static void nm_sg_large_destroy(void*status);

static const struct puk_component_driver_s nm_sg_large_component =
  {
    .instantiate = &nm_sg_large_instantiate,
    .destroy     = &nm_sg_large_destroy
  };

static void nm_sg_large_getprops(puk_context_t context, struct nm_minidriver_properties_s*props);
static void nm_sg_large_init(puk_context_t context, const void**drv_url, size_t*url_size);
static void nm_sg_large_close(puk_context_t context);
static void nm_sg_large_connect(void*_status, const void*remote_url, size_t url_size);
static void nm_sg_large_disconnect(void*_status);
static int  nm_sg_large_send_data_post(void*_status, const struct nm_data_s*p_data, nm_len_t chunk_offset, nm_len_t chunk_len);
static int  nm_sg_large_send_poll(void*_status);
static int  nm_sg_large_send_wait(void*_status);
static int  nm_sg_large_recv_data_post(void*_status, const struct nm_data_s*p_data, nm_len_t chunk_offset, nm_len_t chunk_len);
static int  nm_sg_large_recv_poll_one(void*_status);
static int  nm_sg_large_recv_wait_one(void*_status);

static const struct nm_minidriver_iface_s nm_sg_large_minidriver =
  {
    .getprops         = &nm_sg_large_getprops,
    .init             = &nm_sg_large_init,
    .close            = &nm_sg_large_close,
    .connect          = &nm_sg_large_connect,
    .disconnect       = &nm_sg_large_disconnect,
    .send_iov_post    = NULL,
    .send_data_post   = &nm_sg_large_send_data_post,
    .send_buf_get     = NULL,
    .send_buf_post    = NULL,
    .send_poll        = &nm_sg_large_send_poll,
    .send_wait        = &nm_sg_large_send_wait,
    .recv_iov_post    = NULL,
    .recv_data_post   = &nm_sg_large_recv_data_post,
    .recv_buf_poll    = NULL,
    .recv_buf_release = NULL,
    .recv_poll_one    = &nm_sg_large_recv_poll_one,
    .recv_wait_one    = &nm_sg_large_recv_wait_one,
    .recv_cancel      = NULL,
    .get_rdv_data     = NULL,
    .set_rdv_data     = NULL
  };

/* ********************************************************* */

PADICO_MODULE_COMPONENT(Minidriver_simgrid_large,
  puk_component_declare("Minidriver_simgrid_large",
                        puk_component_provides("PadicoComponent", "component", &nm_sg_large_component),
                        puk_component_provides("NewMad_minidriver", "minidriver", &nm_sg_large_minidriver)));

/* ********************************************************* */

/** 'simgrid' driver per-context data. */
struct nm_sg_large_context_s
{
  char*url;  /**< server url */
};

/** an on-the-wire packet */
struct nm_sg_large_pkt_s
{
  void*p_buffer;
  nm_len_t len;
};

/** 'simgrid' per-instance status. */
struct nm_sg_large_s
{
  struct nm_sg_large_context_s*p_simgrid_context;
  char*p_remote_url;
  sg_mailbox_t mailbox;
  sg_mailbox_t dest_mailbox;
  struct
  {
    sg_comm_t comm;
  } send;
  struct
  {
    sg_comm_t comm;
    struct nm_sg_large_pkt_s*p_incoming_pkt;
    const struct nm_data_s*p_data;
    nm_len_t chunk_offset;
    nm_len_t chunk_len;
  } recv;
};

/* ********************************************************* */

static void*nm_sg_large_instantiate(puk_instance_t instance, puk_context_t context)
{
  struct nm_sg_large_s*p_status = padico_malloc(sizeof(struct nm_sg_large_s));
  struct nm_sg_large_context_s*p_simgrid_context = puk_context_get_status(context);
  p_status->p_simgrid_context = p_simgrid_context;
  p_status->p_remote_url = NULL;
  p_status->mailbox = NULL;
  return p_status;
}

static void nm_sg_large_destroy(void*_status)
{
  struct nm_sg_large_s*p_status = _status;
  if(p_status->p_remote_url)
    padico_free(p_status->p_remote_url);
  padico_free(p_status);
}

/* ********************************************************* */

static void nm_sg_large_getprops(puk_context_t context, struct nm_minidriver_properties_s*props)
{
  props->profile.latency = 500; /* TODO- sg_link_get_latency() */
  props->profile.bandwidth = 8000; /* TODO- sg_link_get_bandiwdth() */
  props->capabilities.supports_buf_send = 0;
  props->capabilities.supports_buf_recv = 0;
  props->capabilities.supports_data = 1;
  props->capabilities.supports_iovec = 0;
  props->capabilities.supports_recv_any = 0;
  props->capabilities.supports_recv_wait = 1;
  props->capabilities.prefers_send_wait = 1;
  props->capabilities.prefers_recv_wait = 1;
  props->capabilities.trk_rdv = 1;
  props->nickname = "simgrid_large";
}

static void nm_sg_large_init(puk_context_t context, const void**drv_url, size_t*url_size)
{
  struct nm_sg_large_context_s*p_simgrid_context = padico_malloc(sizeof(struct nm_sg_large_context_s));

  sg_actor_t actor = sg_actor_self();
  const char*actor_name = sg_actor_get_name(actor);
  padico_string_t s = padico_string_new();
  padico_string_printf(s, "%s_%p", actor_name, context);
  p_simgrid_context->url = padico_strdup(padico_string_get(s));
  padico_string_delete(s);
  puk_context_set_status(context, p_simgrid_context);
  *drv_url = p_simgrid_context->url;
  *url_size = strlen(p_simgrid_context->url) + 1;
}

static void nm_sg_large_close(puk_context_t context)
{
  struct nm_sg_large_context_s*p_simgrid_context = puk_context_get_status(context);
  puk_context_set_status(context, NULL);
  padico_free(p_simgrid_context->url);
  padico_free(p_simgrid_context);
}

static void nm_sg_large_lazy_connect(struct nm_sg_large_s*p_status)
{
  if(p_status->mailbox == NULL)
    {
      struct nm_sg_large_context_s*p_simgrid_context = p_status->p_simgrid_context;
      padico_string_t s = padico_string_new();
      padico_string_printf(s, "from:%s--to:%s", p_status->p_remote_url, p_simgrid_context->url);
      p_status->mailbox = sg_mailbox_by_name(padico_string_get(s));
      padico_string_printf(s, "from:%s--to:%s", p_simgrid_context->url, p_status->p_remote_url);
      p_status->dest_mailbox = sg_mailbox_by_name(padico_string_get(s));
      padico_string_delete(s);
      sg_mailbox_set_receiver(sg_mailbox_get_name(p_status->mailbox));
      padico_out(puk_verbose_info, "local mailbox = %s (%p); dest mailbox = %s (%p)\n",
                 sg_mailbox_get_name(p_status->mailbox), p_status->mailbox,
                 sg_mailbox_get_name(p_status->dest_mailbox), p_status->dest_mailbox);
    }
}

static void nm_sg_large_connect(void*_status, const void*p_remote_url, size_t url_size)
{
  struct nm_sg_large_s*p_status = _status;
  p_status->p_remote_url = padico_strdup(p_remote_url);
}

static void nm_sg_large_disconnect(void*_status)
{
  struct nm_sg_large_s*p_status = _status;
}

static int nm_sg_large_send_data_post(void*_status, const struct nm_data_s*p_data, nm_len_t chunk_offset, nm_len_t chunk_len)
{
  struct nm_sg_large_s*p_status = _status;
  nm_sg_large_lazy_connect(p_status);
  struct nm_sg_large_pkt_s*p_pkt = padico_malloc(sizeof(struct nm_sg_large_pkt_s));
  p_pkt->p_buffer = padico_malloc(chunk_len);
  nm_data_copy_from(p_data, chunk_offset, chunk_len, p_pkt->p_buffer);
  p_pkt->len = chunk_len;
  p_status->send.comm = sg_mailbox_put_async(p_status->dest_mailbox, p_pkt, chunk_len);
  return NM_ESUCCESS;
}

static int nm_sg_large_send_poll(void*_status)
{
  struct nm_sg_large_s*p_status = _status;
  if(sg_comm_test(p_status->send.comm))
    {
      NM_TRACEF("send poll SUCCESS\n");
      return NM_ESUCCESS;
    }
  else
    {
      sg_actor_sleep_for(NM_SG_LARGE_TIME_POLL);
      return -NM_EAGAIN;
    }
}

static int nm_sg_large_send_wait(void*_status)
{
  struct nm_sg_large_s*p_status = _status;
  if(sg_comm_wait(p_status->send.comm) == SG_OK)
    {
      NM_TRACEF("send wait SUCCESS\n");
      return NM_ESUCCESS;
    }
  else
    {
      sg_actor_sleep_for(NM_SG_LARGE_TIME_POLL);
      return -NM_EAGAIN;
    }
}

static int nm_sg_large_recv_data_post(void*_status, const struct nm_data_s*p_data, nm_len_t chunk_offset, nm_len_t chunk_len)
{
  struct nm_sg_large_s*p_status = _status;
  nm_sg_large_lazy_connect(p_status);
  p_status->recv.p_incoming_pkt = NULL;
  p_status->recv.p_data = p_data;
  p_status->recv.chunk_offset = chunk_offset;
  p_status->recv.chunk_len = chunk_len;
  p_status->recv.comm = sg_mailbox_get_async(p_status->mailbox, (void**)&p_status->recv.p_incoming_pkt);
  return NM_ESUCCESS;
}

static int nm_sg_large_recv_poll_one(void*_status)
{
  struct nm_sg_large_s*p_status = _status;
  if(sg_comm_test(p_status->recv.comm))
    {
      NM_TRACEF("recv poll SUCCESS; ptr = %p\n", p_status->recv.p_incoming_pkt);
      if(p_status->recv.p_incoming_pkt->len > p_status->recv.chunk_len)
        {
          NM_FATAL("received more data than posted.\n");
        }
      nm_data_copy_to(p_status->recv.p_data, p_status->recv.chunk_offset, p_status->recv.p_incoming_pkt->len, p_status->recv.p_incoming_pkt->p_buffer);
      padico_free(p_status->recv.p_incoming_pkt);
      p_status->recv.p_incoming_pkt = NULL;
      return NM_ESUCCESS;
    }
  else
    {
      sg_actor_sleep_for(NM_SG_LARGE_TIME_POLL);
      return -NM_EAGAIN;
    }
}

static int nm_sg_large_recv_wait_one(void*_status)
{
  struct nm_sg_large_s*p_status = _status;
  if(sg_comm_wait(p_status->recv.comm) == SG_OK)
    {
      NM_TRACEF("recv wait SUCCESS; ptr = %p\n", p_status->recv.p_incoming_pkt);
      if(p_status->recv.p_incoming_pkt->len > p_status->recv.chunk_len)
        {
          NM_FATAL("received more data than posted.\n");
        }
      nm_data_copy_to(p_status->recv.p_data, p_status->recv.chunk_offset, p_status->recv.p_incoming_pkt->len, p_status->recv.p_incoming_pkt->p_buffer);
      padico_free(p_status->recv.p_incoming_pkt);
      p_status->recv.p_incoming_pkt = NULL;
      return NM_ESUCCESS;
    }
  else
    {
      sg_actor_sleep_for(NM_SG_LARGE_TIME_POLL);
      return -NM_EAGAIN;
    }
}

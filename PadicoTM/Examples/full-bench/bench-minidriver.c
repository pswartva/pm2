/** @file bench-minidriver.c
 */

#include "bench-common.h"
#include <Padico/Module.h>
#include <Padico/Puk.h>
#include <Padico/AddrDB.h>
#include <Padico/NetSelector.h>

#include <nm_minidriver.h>

int bench_minidriver_init(void);
int bench_minidriver_run(int argc, char**argv);

PADICO_MODULE_DECLARE(bench_minidriver, bench_minidriver_init, bench_minidriver_run, NULL);

static padico_topo_node_t peer = NULL;
static struct puk_receptacle_NewMad_minidriver_s trk_small, trk_large;
static size_t max_small = 64 * 1024;

static void bench_minidriver_connect(puk_component_t component, const char*trk_name, struct puk_receptacle_NewMad_minidriver_s*r)
{
  puk_context_t context = puk_component_get_context(component, puk_iface_NewMad_minidriver(), NULL);
  if(!context)
    {
      padico_fatal("bench-minidriver: cannot find context in minidriver component %s. Cannot instantiate.\n",
                   component->name);
    }
  /* init */
  const struct nm_minidriver_iface_s*driver = puk_component_get_driver_NewMad_minidriver(component, NULL);
  struct nm_minidriver_properties_s props;
#ifdef PIOMAN_TOPOLOGY_HWLOC
  props.profile.cpuset = hwloc_bitmap_alloc();
#endif
  (*driver->getprops)(context, &props);
  if(props.capabilities.max_msg_size > 0)
    {
      max_small = (props.capabilities.max_msg_size > 64 * 1024) ? 64 * 1024 : props.capabilities.max_msg_size ;
      padico_print("bench-minidriver: rendez-vous threshold for driver %s = %lu\n", component->name, max_small);
    }
  const void*drv_url = NULL;
  size_t url_size = 0;
  (*driver->init)(context, &drv_url, &url_size);
  /* instantiate */
  puk_instance_t instance = puk_component_instantiate(component);
  puk_instance_indirect_NewMad_minidriver(instance, "minidriver", r);
  /* connect */
  padico_addrdb_publish(peer, trk_name, NULL, 0, drv_url, url_size);
  char peer_url[url_size];
  padico_req_t req = padico_tm_req_new(NULL, NULL);
  padico_addrdb_get(peer, trk_name, NULL, 0, &peer_url[0], url_size, req);
  padico_tm_req_wait(req);
  (*r->driver->connect)(r->_status, peer_url, url_size);
  padico_print("bench-minidriver: connected using %s\n", component->name);
}

int bench_minidriver_init(void)
{
  /* find component */
  peer = bench_common_param.is_server ?
    bench_common_param.client_node :
    bench_common_param.server_node;
  const char*s_component = padico_getenv("BENCH_MINIDRIVER_ADAPTER");
  puk_component_t component_small = NULL, component_large = NULL;
  if(s_component != NULL)
    {
      if(puk_component_resolve(s_component) == NULL)
        padico_fatal("bench-minidriver: component %s not found.\n", s_component);
      char s_composite[512];
      snprintf(s_composite, 512,
               "<puk:composite id=\"bench_minidriver\">"
               "  <puk:component id=\"0\" name=\"%s\"/>"
               "  <puk:entry-point iface=\"NewMad_minidriver\" port=\"minidriver\" provider-id=\"0\" />"
               "</puk:composite>",
               s_component);
      component_small = puk_component_parse(s_composite);
      component_large = component_small;
      padico_print("bench-minidriver: using component %s set manually.\n", s_component);
      bench_minidriver_connect(component_small, "bench-minidriver", &trk_small);
      trk_large = trk_small;
    }
  else
    {
      component_small = padico_ns_serial_selector(peer, "trk_small", puk_iface_NewMad_minidriver());
      component_large = padico_ns_serial_selector(peer, "trk_large", puk_iface_NewMad_minidriver());
      padico_print("bench-minidriver: using components %s + %s from NetSelector.\n",
                   component_small->name, component_large->name);
      bench_minidriver_connect(component_small, "bench-minidriver:trk_small", &trk_small);
      bench_minidriver_connect(component_large, "bench-minidriver:trk_large", &trk_large);
    }


  return 0;
}

static inline void minidriver_send(struct puk_receptacle_NewMad_minidriver_s*r, char*buf, size_t len)
{
  int rc = -1;
  struct iovec v = { .iov_base = buf, .iov_len = len };
  struct nm_data_s data;
  if(r->driver->send_buf_get)
    {
      void*p_driver_buf = NULL;
      nm_len_t driver_len = NM_LEN_UNDEFINED;
      (*r->driver->send_buf_get)(r->_status, &p_driver_buf, &driver_len);
      memcpy(p_driver_buf, buf, len);
      (*r->driver->send_buf_post)(r->_status, len);
    }
  else if(r->driver->send_iov_post)
    {
      (*r->driver->send_iov_post)(r->_status, &v, 1);
    }
  else
    {
      nm_data_contiguous_build(&data, buf, len);
      (*r->driver->send_data_post)(r->_status, &data, 0, len);
    }
  do
    {
      rc = (*r->driver->send_poll)(r->_status);
    }
  while(rc != 0);
}

static inline void minidriver_recv(struct puk_receptacle_NewMad_minidriver_s*r, char*buf, size_t len)
{
  int rc = -1;
  struct iovec v = { .iov_base = buf, .iov_len = len };
  struct nm_data_s data;
  if(r->driver->recv_buf_poll)
    {
      void*p_driver_buf = NULL;
      nm_len_t driver_len = NM_LEN_UNDEFINED;
      do
        {
          rc = (*r->driver->recv_buf_poll)(r->_status, &p_driver_buf, &driver_len);
        }
      while(rc != 0);
      memcpy(buf, p_driver_buf, len);
      (*r->driver->recv_buf_release)(r->_status);
    }
 else
   {
     if(r->driver->recv_iov_post)
       {
         (*r->driver->recv_iov_post)(r->_status, &v, 1);
       }
     else
       {
         nm_data_contiguous_build(&data, buf, len);
         (*r->driver->recv_data_post)(r->_status, &data, 0, len);
       }
     do
       {
         rc = (*r->driver->recv_poll_one)(r->_status);
       }
     while(rc != 0);
   }
}

void minidriver_server(char*buf, size_t len)
{
  struct puk_receptacle_NewMad_minidriver_s*r = (len > max_small) ? &trk_large : &trk_small;
  minidriver_send(r, buf, len);
  minidriver_recv(r, buf, len);
}

void minidriver_client(char*buf, size_t len)
{
  struct puk_receptacle_NewMad_minidriver_s*r = (len > max_small) ? &trk_large : &trk_small;
  minidriver_recv(r, buf, len);
  minidriver_send(r, buf, len);
}

static const struct bench_desc_s minidriver_bench =
{
  "minidriver",
  &minidriver_server,
  &minidriver_client
};

int bench_minidriver_run(int argc, char**argv)
{
  the_bench(&minidriver_bench);
  return 0;
}

/*
 * NewMadeleine
 * Copyright (C) 2016-2022 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <math.h>

#include <nm_launcher_interface.h>
#include <nm_coll_interface.h>
#include <nm_coll_dynamic_interface.h>
#include <nm_sync_clocks_interface.h>

#include "../common/nm_examples_helper.h"

#define SERVER_PRINTF(fmt, ...) do { if(is_server) { printf(fmt, ## __VA_ARGS__); fflush(stdout); }} while(0)

typedef void (*algorithm_t)(int nb_dest_nodes, int size, float*msg, int rank, int nb_nodes_id, int size_id, int bench_id);

static void bcast(int nb_dest_nodes, int size, float*msg, int rank, int nb_nodes_id, int size_id, int bench_id);
static void ibcast(int nb_dest_nodes, int size, float*msg, int rank, int nb_nodes_id, int size_id, int bench_id);
static void dummy_loop(int nb_dest_nodes, int size, float* msg, int rank, int nb_nodes_id, int size_id, int bench_id);
static void binomial_tree(int nb_dest_nodes, int size, float*msg, int rank, int nb_nodes_id, int size_id, int bench_id);
static void binary_tree(int nb_dest_nodes, int size, float*msg, int rank, int nb_nodes_id, int size_id, int bench_id);

static algorithm_t algorithms[] = { dummy_loop, binomial_tree, binary_tree, ibcast, bcast };

#define NB_BENCH 10
#define NB_METHODS (sizeof(algorithms)/sizeof(algorithm_t))
#define NX_MAX 240196 // kB
#define NX_MIN 1 // kB
#define NX_STEP 1.4 // multiplication
#define NB_NODES_STEP 2 // addition
#define NB_NODES_START 3

struct statistics {
  double min;
  double med;
  double avg;
  double max;
};

int times_nb_nodes;
int times_size;
int worldsize;
double*times;
static nm_sync_clocks_t p_clocks = NULL;
static nm_comm_t clocks_comm = NULL;

static int comp_double(const void*_a, const void*_b)
{
  const double*a = _a;
  const double*b = _b;
  if(*a < *b)
    return -1;
  else if(*a > *b)
    return 1;
  else
    return 0;
}

double find_max(double*array, int size)
{
  double t_max = nm_sync_clocks_remote_to_global(p_clocks, 1, array[0]);
  double t_value;

  for (int i = 1; i < size; i++)
    {
      t_value = nm_sync_clocks_remote_to_global(p_clocks, i+1, array[i]);
      if (t_value > t_max)
        {
          t_max = t_value;
        }
    }

  return t_max;
}

struct statistics compute_statistics(double* array, int size)
{
  struct statistics stat;

  qsort(array, size, sizeof(double), &comp_double);

  double avg = 0;
  for (int i = 0; i < size; i++)
  {
    avg += array[i];
  }
  stat.avg = avg / size;

  stat.min = array[0];
  stat.med = array[(int) floor(size / 2)];
  stat.max = array[size - 1];

  return stat;
}

int time_index(int size, int bench, int node)
{
  assert(size < times_size);
  assert(bench < NB_BENCH);
  assert(node < worldsize);

  // Warning: if bench < 0, this function returns a result, the user has to check if it makes sense.

  return size * (NB_BENCH * (worldsize + 1))
          + bench * (worldsize + 1)
          + node;
}

static void check_data(float*recv, float*expected, int size)
{
  int i;
  for(i = 0; i < size; i++)
    {
      if(recv[i] != expected[i])
        {
          fprintf(stderr, "# data corrupted i = %d; recv = %f; expected = %f\n", i, recv[i], expected[i]);
          abort();
        }
    }
}

static void abstract_coll_dynamic(int nb_dest_nodes, int size, float*msg, int rank, int nb_nodes_id, int size_id, int bench_id)
{
  nm_gate_t gate;
  double t_end;

  if(is_server)
    {
      int* nodes = malloc(nb_dest_nodes * sizeof(int)); // freed by nm_coll_dynamic_send
      int* prios = calloc(nb_dest_nodes, sizeof(int)); // freed by nm_coll_dynamic_send
      int t_index = time_index(size_id, bench_id, 0);

      for(int i = 1; i <= nb_dest_nodes; i++)
        {
          nodes[i-1] = i;
        }

      if (bench_id >= 0)
        {
          times[t_index] = nm_sync_clocks_get_time_usec(p_clocks);
        }

      nm_coll_dynamic_send(nodes, prios, nb_dest_nodes, 1, (void*) msg, size * sizeof(float), NULL, NULL);

      for (int i = 1; i <= nb_dest_nodes; i++)
        {
          nm_launcher_get_gate(i, &gate);
          nm_sr_recv(p_session, gate, 2, &t_end, sizeof(t_end));

          if (bench_id >= 0)
            {
              times[t_index+i] = t_end;
            }
        }
    }
  else if (rank <= nb_dest_nodes)
    {
      nm_sr_request_t request;
      float* received_msg = malloc(size * sizeof(float));
      piom_cond_t cond_wait_recv;
      piom_cond_init(&cond_wait_recv, 0);

      nm_coll_dynamic_recv_register_recv(p_session, 0, 1, received_msg, size * sizeof(float),
                                         &request, &cond_wait_recv, NULL, NULL, NULL, NULL);
      piom_cond_wait(&cond_wait_recv, 1);

      t_end = nm_sync_clocks_get_time_usec(p_clocks);
      nm_launcher_get_gate(0, &gate);
      nm_sr_send(p_session, gate, 2, &t_end, sizeof(t_end));

      check_data(received_msg, msg, size);

      free(received_msg);
      piom_cond_destroy(&cond_wait_recv);
    }

}

static void binomial_tree(int nb_dest_nodes, int size, float*msg, int rank, int nb_nodes_id, int size_id, int bench_id)
{
  nm_coll_dynamic_set_tree_kind(NM_COLL_TREE_BINOMIAL);
  abstract_coll_dynamic(nb_dest_nodes, size, msg, rank, nb_nodes_id, size_id, bench_id);
}

static void binary_tree(int nb_dest_nodes, int size, float*msg, int rank, int nb_nodes_id, int size_id, int bench_id)
{
  nm_coll_dynamic_set_tree_kind(NM_COLL_TREE_BINARY);
  abstract_coll_dynamic(nb_dest_nodes, size, msg, rank, nb_nodes_id, size_id, bench_id);
}

static void dummy_loop(int nb_dest_nodes, int size, float* msg, int rank, int nb_nodes_id, int size_id, int bench_id)
{
  nm_gate_t gate;
  double t_end;

  if(is_server)
    {
      int t_index = time_index(size_id, bench_id, 0);
      if (bench_id >= 0)
        {
          times[t_index] = nm_sync_clocks_get_time_usec(p_clocks);
        }

      for(int i = 1; i <= nb_dest_nodes; i++)
        {
          nm_gate_t gate = NULL;
          nm_launcher_get_gate(i, &gate);
          nm_sr_send(p_session, gate, 0, msg, size * sizeof(float));
        }

      for (int i = 1; i <= nb_dest_nodes; i++)
        {
          nm_launcher_get_gate(i, &gate);
          nm_sr_recv(p_session, gate, 2, &t_end, sizeof(t_end));

          if (bench_id >= 0)
            {
              times[t_index+i] = t_end;
            }
        }
    }
  else // not server
    {
      nm_sr_request_t request;
      float* received_msg = malloc(size * sizeof(float));

      nm_sr_irecv(p_session, NM_ANY_GATE, 0, received_msg, size * sizeof(float), &request);
      nm_sr_rwait(p_session, &request);

      t_end = nm_sync_clocks_get_time_usec(p_clocks);
      nm_launcher_get_gate(0, &gate);
      nm_sr_send(p_session, gate, 2, &t_end, sizeof(t_end));

      check_data(received_msg, msg, size);

      free(received_msg);
    }
}

static void ibcast(int nb_dest_nodes, int size, float*msg, int rank, int nb_nodes_id, int size_id, int bench_id)
{
  nm_gate_t gate;
  double t_end;

  nm_group_t coop_nodes = nm_group_new();
  struct nm_data_s data;
  float* received_msg = NULL;

  for (int i = 0; i <= nb_dest_nodes; i++)
    {
      nm_launcher_get_gate(i, &gate);
      nm_group_add_node(coop_nodes, gate);
    }

  const int self = nm_group_rank(coop_nodes);
  int t_index = time_index(size_id, bench_id, 0);

  if(is_server)
    {
      nm_data_contiguous_build(&data, msg, size * sizeof(float));

      if (bench_id >= 0)
        {
          times[t_index] = nm_sync_clocks_get_time_usec(p_clocks);
        }
    }
  else
    {
      received_msg = malloc(size * sizeof(float));
      nm_data_contiguous_build(&data, received_msg, size * sizeof(float));
    }

  const nm_tag_t tag = 5;
  const int root = 0;
  struct nm_coll_req_s* p_bcast = nm_coll_group_data_ibcast(p_session, coop_nodes, root, self, &data, tag, NULL, NULL);
  nm_coll_req_wait(p_bcast);

  if (is_server)
    {
      for (int i = 1; i <= nb_dest_nodes; i++)
        {
          nm_launcher_get_gate(i, &gate);
          nm_sr_recv(p_session, gate, 2, &t_end, sizeof(t_end));

          if (bench_id >= 0)
            {
              times[t_index+i] = t_end;
            }
        }
    }
  else
    {
      t_end = nm_sync_clocks_get_time_usec(p_clocks);
      nm_launcher_get_gate(0, &gate);
      nm_sr_send(p_session, gate, 2, &t_end, sizeof(t_end));

      check_data(received_msg, msg, size);

      free(received_msg);
    }

  nm_group_free(coop_nodes);
}

static void bcast(int nb_dest_nodes, int size, float*msg, int rank, int nb_nodes_id, int size_id, int bench_id)
{
  nm_gate_t gate;
  double t_end;

  nm_group_t coop_nodes = nm_group_new();
  struct nm_data_s data;
  float* received_msg = NULL;

  for (int i = 0; i <= nb_dest_nodes; i++)
    {
      nm_launcher_get_gate(i, &gate);
      nm_group_add_node(coop_nodes, gate);
    }

  const int self = nm_group_rank(coop_nodes);
  int t_index = time_index(size_id, bench_id, 0);

  if(is_server)
    {
      nm_data_contiguous_build(&data, msg, size * sizeof(float));

      if (bench_id >= 0)
        {
          times[t_index] = nm_sync_clocks_get_time_usec(p_clocks);
        }
    }
  else
    {
      received_msg = malloc(size * sizeof(float));
      nm_data_contiguous_build(&data, received_msg, size * sizeof(float));
    }

  nm_coll_group_data_bcast(p_session, coop_nodes, 0, self, &data, 3);

  if (is_server)
    {
      for (int i = 1; i <= nb_dest_nodes; i++)
        {
          nm_launcher_get_gate(i, &gate);
          nm_sr_recv(p_session, gate, 2, &t_end, sizeof(t_end));

          if (bench_id >= 0)
            {
              times[t_index+i] = t_end;
            }
        }
    }
  else
    {
      t_end = nm_sync_clocks_get_time_usec(p_clocks);
      nm_launcher_get_gate(0, &gate);
      nm_sr_send(p_session, gate, 2, &t_end, sizeof(t_end));

      check_data(received_msg, msg, size);

      free(received_msg);
    }

  nm_group_free(coop_nodes);
}


void compute_display_times(const int method, const int nb_nodes_id, const int nb_dest_nodes)
{
  int size_id = 0;
  double times_bench[NB_BENCH];

  SERVER_PRINTF("Computing clock offsets... ");

  nm_sync_clocks_synchronize(p_clocks);

  if (is_server)
    {
      printf("done\n");

      /* Computing times */
      for (int s = NX_MIN; s < NX_MAX; s = (s * NX_STEP) + 1)
        {
          for(int b = 0; b < NB_BENCH; b++)
            {
              double t_begin = times[time_index(size_id, b, 0)];
              double t_end = find_max(times + time_index(size_id, b, 1), nb_dest_nodes);
              assert(t_begin < t_end);
              times_bench[b] = t_end - t_begin;
            }

          struct statistics stat_main_task = compute_statistics(times_bench, NB_BENCH);
          printf("   %d    |   %3d  \t| %5d\t\t| ", method, nb_dest_nodes+1, s);
          printf("%10.3lf\t%10.3lf\t%10.3lf\t%10.3lf\n", stat_main_task.min, stat_main_task.med, stat_main_task.avg, stat_main_task.max);
          fflush(stdout);

          size_id++;
        }
    }
}

void start_global_clock()
{
  SERVER_PRINTF("Starting global clock... ");
  p_clocks = nm_sync_clocks_init(clocks_comm);
  SERVER_PRINTF("done\n");
}

int main(int argc, char**argv)
{
  nm_examples_init_topo(&argc, argv, NM_EXAMPLES_TOPO_STAR);
  nm_coll_dynamic_init_multicast();

  nm_launcher_get_size(&worldsize);

  int rank;
  nm_launcher_get_rank(&rank);

  clocks_comm = nm_comm_world("nm_coll_dynamic_benchmark");

  times_nb_nodes = ((worldsize - NB_NODES_START) / NB_NODES_STEP) + 1;
  times_size = (int) (logf((float) NX_MAX / (float) NX_MIN) / logf(NX_STEP)) + 1;

  times = malloc(times_size * NB_BENCH * (worldsize + 1) * sizeof(double));

  int array_size = 0;

  if (is_server)
    {
      printf("#0: dummy loop\n");
      printf("#1: dynamic broadcast (binomial)\n");
      printf("#2: dynamic broadcast (binary)\n");
      printf("#3: ibcast\n");
      printf("#4: bcast\n");
      printf("        |  Nodes  \t|          \t| \tMain task lasted (us):\n");
      printf("  Algo  | in comm \t| Size (KB)\t| min\tmed\tavg\tmax\n");
      printf("-----------------------------------------------------------------------\n");
    }

  int nb_nodes_id = 0;
  int size_id = 0;

  for (int method = 0; method < NB_METHODS; method++)
    {
      nb_nodes_id = 0;

      for (int nb_dest_nodes = NB_NODES_START; nb_dest_nodes < worldsize; nb_dest_nodes += NB_NODES_STEP)
        {
          nm_examples_barrier(0x12);
          start_global_clock();

          size_id = 0;

          for (int s = NX_MIN; s < NX_MAX; s = (s * NX_STEP) + 1)
            {
              SERVER_PRINTF("   %d    |   %3d  \t| %5d\t\t| ", method, nb_dest_nodes+1, s);

              array_size = s * 1000 / sizeof(float);

              float* msg = malloc(array_size * sizeof(float));
              for(int i = 0; i < array_size; i++)
                {
                  *((float*) msg + i) = 3.14;
                }

              for(int b = -1; b < NB_BENCH; b++)
                {
                  if (rank <= nb_dest_nodes)
                    {
                      algorithms[method](nb_dest_nodes, array_size, msg, rank, nb_nodes_id, size_id, b);
                    }

                  SERVER_PRINTF(".");
                }

              SERVER_PRINTF("\n");

              free(msg);
              size_id++;
            }

          // flush clocks
          compute_display_times(method, nb_nodes_id, nb_dest_nodes);
          nm_sync_clocks_shutdown(p_clocks);

          nb_nodes_id++;
        }
    }

  nm_coll_dynamic_shutdown();
  nm_examples_exit();

  free(times);

  return 0;
}

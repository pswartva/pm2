/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Padico SOAP Gatekeeper
 * @ingroup SOAPGatekeeper
 */

/** @defgroup SOAPGatekeeper Service: SOAP-Gatekeeper -- server-side for SOAP remote control
 */

#include <Padico/Puk.h>
#include <Padico/PadicoTM.h>
#include <Padico/PM2.h>
#include <Padico/Topology.h>
#include <Padico/Module.h>

#include <stdsoap2.h>

#include "soapH.h"
#include "PadicoGatekeeper.nsmap"

#define SOAP_DEFAULT_PORT 18083
#define SOAP_URL_MAXSIZE 1024

static int  soap_gk_init(void);
static int  soap_gk_start(void);
static void*soap_gk_worker(void*);
static void soap_gk_stop(void);
static void copy_rc(padico_rc_t rc, struct padico_gatekeeper__rc_t *outrc);

static struct soap soap;

PADICO_MODULE_DECLARE(SOAP_Gatekeeper, soap_gk_init, NULL, soap_gk_stop);


/* ***** SOAP module/process methods *********************** */

int padico_gatekeeper__Ping(struct soap *soap, struct padico_gatekeeper__rc_t *outrc)
{
  padico_out(10, "Ping.\n");
  copy_rc(padico_rc_ok(), outrc);
  return 0;
}

int padico_gatekeeper__Kill(struct soap *soap, struct padico_gatekeeper__rc_t*outrc)
{
  padico_out(10, "Kill.\n");
  padico_tm_exit();
  copy_rc(padico_rc_ok(), outrc);
  return 0;
}

int padico_gatekeeper__LoadModule(struct soap *soap,
                                  padico_gatekeeper__modID_t modname,
                                  struct padico_gatekeeper__rc_t*outrc)
{
  padico_rc_t rc = NULL;
  padico_out(10, "LoadModule %s\n", modname);
  rc = padico_tm_mod_action(PADICO_TM_MOD_LOAD, modname);
  copy_rc(rc, outrc);
  return 0;
}

int padico_gatekeeper__UnloadModule(struct soap *soap,
                                    padico_gatekeeper__modID_t modname,
                                    struct padico_gatekeeper__rc_t*outrc)
{
  padico_rc_t rc = NULL;
  padico_out(10, "UnloadModule %s\n", modname);
  rc = padico_tm_mod_action(PADICO_TM_MOD_UNLOAD, modname);
  copy_rc(rc, outrc);
  return 0;
}

int padico_gatekeeper__RunModule(struct soap *soap,
                                 padico_gatekeeper__modID_t modname,
                                 struct padico_gatekeeper__job_args_s args,
                                 struct padico_gatekeeper__rc_t*outrc)
{
  padico_rc_t rc = NULL;
  int argc = args.__sizeargv + 1;
  char**argv;
  int i;
  argv = (char**)padico_malloc(sizeof(char*)*(argc+1));
  /** @bug memory leak: this malloc() is never freed
   */
  argv[0] = padico_strdup(modname);
  argv[argc] = NULL;
  for(i=1; i<argc; i++)
    argv[i] = padico_strdup(args.argv[i-1]);
  padico_out(10, "RunModule %s\n", modname);
  rc = padico_tm_mod_action_args(PADICO_TM_MOD_RUN, modname, argc, argv);
  copy_rc(rc, outrc);
  return 0;
}

int padico_gatekeeper__StartJob(struct soap *soap,
                                padico_gatekeeper__modID_t modname,
                                struct padico_gatekeeper__job_args_s args,
                                struct StartJobResponse *out)
{
   padico_rc_t rc = NULL;
  int argc = args.__sizeargv + 1;
  char**argv;
  int i;
  argv = (char**)padico_malloc(sizeof(char*)*(argc+1));
  /** @bug memory leak: this malloc() is never freed
   */
  argv[0] = padico_strdup(modname);
  argv[argc] = NULL;
  for(i=1; i<argc; i++)
    argv[i] = padico_strdup(args.argv[i-1]);
  padico_out(10, "StartJob %s\n", modname);
  rc = padico_tm_mod_action_args(PADICO_TM_MOD_RUN, modname, argc, argv);
  copy_rc(rc, out->outrc);
  return 0;
}


int padico_gatekeeper__ListModules(struct soap *soap,
                                   struct padico_gatekeeper__mod_list_s*outlist)
{
  padico_modID_vect_t modIDs = puk_mod_getmodIDs();

  padico_out(10, "ListModules\n");
  outlist->__sizemods = padico_modID_vect_size(modIDs);
  outlist->mods = padico_modID_vect_begin(modIDs);
  return 0;
}

/* ***** SOAP group/topology methods *********************** */

int padico_gatekeeper__CreateGroup(struct soap *soap,
                                   padico_gatekeeper__groupID_t groupID,
                                   struct padico_gatekeeper__nodeID_list_s nodes,
                                   struct padico_gatekeeper__rc_t*outrc)
{
  padico_rc_t rc = NULL;
  padico_print("creating group %s\n", groupID);
  if(NULL != padico_group_lookup(groupID))
    {
      rc = padico_rc_error("cannot create group: groupID=**%s** already exists.", groupID);
    }
  else
    {
      padico_group_t group = padico_group_init(groupID);
      int i;
      padico_print("adding nodes to group %s\n", groupID);
      for(i = 0; i < nodes.__sizenodes; i++)
        {
          char*nodeID = nodes.nodes[i];
          padico_topo_node_t node = padico_topo_getnodebyname(nodeID);
          padico_print("adding node %s to group %s\n", nodeID, groupID);
          if(node == NULL)
            {
#if 0 /* XXX: establish a bridge- not implemented yet */
              /* retry: establishes a bridge */
              if(strchr(nodeID, ':') == NULL)
                {
                  char server[SOAP_URL_MAXSIZE];
                  struct padico_gatekeeper__cluster_contact_s contact;
                  char*hostname = nodeID; /* trick: we assume that the nodeID may resolved into a hostID */
                  int soap_rc;
                  struct soap client_soap;
                  soap_init(&client_soap);
                  snprintf(server, SOAP_URL_MAXSIZE, "http://%s:%d/PadicoGatekeeper",
                           hostname, SOAP_DEFAULT_PORT);
                  padico_print("node %s not found-- trying %s\n",
                               nodeID, server);
                  /* get a bridge */
                  soap_rc = soap_call_padico_gatekeeper__BridgeInit(&client_soap, server, "", &contact);
                  if(soap_rc)
                    {
                      padico_print("%s failed\n", server);
                    }
                  else
                    {
                      /* connect the bridge */
                      padico_print("connecting bridge to %s:%d.\n",
                                   contact.hostID, contact.port);
                      padico_control_bridge_connect(contact.hostID, contact.port);
                      padico_tm_sleep(2);
                      node = padico_topo_getnodebyname(nodeID);
                      padico_print("nodeID=%s solved into %p.\n",
                                   nodeID, node);
                    }
                  soap_done(&client_soap);
                }
#endif /* XXX */
            }
          if(node != NULL)
            {
              padico_group_node_add(group, node);
            }
          else
            {
              rc = padico_rc_error("cannot add node **%s**: not recognized as a valid nodeID.", nodeID);
              break;
            }
        }
      if(!padico_rc_iserror(rc))
        {
          padico_group_publish(group);
        }
    }
  copy_rc(rc, outrc);
  return 0;
}
/*
int padico_gatekeeper__GetGroupInfo(struct soap *soap,
                                    padico_gatekeeper__groupID_t groupID,
                                    struct GetGroupInfoResponse*out)
{
  padico_rc_t rc = NULL;
  padico_group_t group = padico_group_lookup(groupID);
  if(group != NULL)
    {
      padico_topo_node_vect_t nodes = group->nodes;
      out->group.groupID = groupID;
      out->group.node_list.__sizenodes = padico_node_vect_size(nodes);
      out->group.node_list.nodes = padico_node_vect_begin(nodes);
    }
  else
    {
      rc = padico_rc_error("GetGroupInfo: group **%s** not found.", groupID);
    }
  copy_rc(rc, &out->rc);
  return 0;
}
*/
int padico_gatekeeper__GetNodeInfo(struct soap *soap,
                                   padico_gatekeeper__nodeID_t nodeID,
                                   struct GetNodeInfoResponse*out)
{
  padico_rc_t rc = NULL;
  padico_topo_node_t node = padico_topo_getnodebyname(nodeID);
  if(node != NULL)
    {
      out->node.canonical_nodename = padico_topo_node_getname(node);
#ifdef PADICO_MADIO
      out->node.rank = padico_na_madio_getrank(node);
#else
      out->node.rank = 0;
#endif
      out->node.hostID = padico_topo_host_getname(padico_topo_node_gethost(node));
    }
  else
    {
      rc = padico_rc_error("GetNodeInfo: node **%s** not found.", nodeID);
    }
  copy_rc(rc, &out->rc);
  return 0;
}

int padico_gatekeeper__GetLocalNetworkInfo(struct soap *soap,
                                           struct padico_gatekeeper__network_s*network)
{
  return 0;
}

int padico_gatekeeper__BridgeInit(struct soap *soap,
                                  struct padico_gatekeeper__cluster_contact_s*out)
{
  return -1; /* not implemented yet */
  /*
    out->port = padico_control_bridge_init();
    out->hostID = padico_topo_hostname();
    return 0;
  */
}

int padico_gatekeeper__BridgeConnect(struct soap *soap,
                                     struct padico_gatekeeper__cluster_contact_s contact,
                                     struct padico_gatekeeper__rc_t*out)
{
  return -1; /* not implemented yet */
  /*
    padico_rc_t rc = NULL;
    padico_control_bridge_connect(contact.hostID, contact.port);
    copy_rc(rc, out);
    return 0;
  */
}


int padico_gatekeeper__RawXMLCommand(struct soap *soap,
                                     char*xml_command,
                                     struct padico_gatekeeper__rc_t*out)
{
  struct puk_parse_entity_s pe;
  padico_print("parsing: %s\n", xml_command);
  pe = puk_xml_parse_buffer(xml_command, strlen(xml_command), PUK_TRUST_CONTROL);
  padico_rc_show(pe.rc);
  copy_rc(pe.rc, out);
  return 0;
}


/* ***** Helper functions ********************************** */

static void copy_rc(padico_rc_t rc, struct padico_gatekeeper__rc_t *outrc)
{
  if(rc)
    {
      outrc->rc  = rc->rc;
      outrc->msg = padico_string_get(rc->msg);
    }
  else
    {
      outrc->rc  = 0;
      outrc->msg = NULL;
    }
}

static int soap_gk_start(void)
{
  int rc = 0;
  int m;
  int soap_port = SOAP_DEFAULT_PORT;
  char* soap_port_string = padico_getattr("SOAP_PORT");

  if(soap_port_string != NULL)
    {
      soap_port = atoi(soap_port_string);
    }

  padico_trace("creating Gatekeeper on port %d\n", soap_port);
  soap_init(&soap);
  m = soap_bind(&soap, NULL, soap_port, 100);
  if (m < 0)
    {
      soap_print_fault(&soap, stderr);
      rc = -1;
    }
  else
    {
      padico_print("MyURL=http://%s:%d/PadicoGatekeeper\n",
                   padico_topo_host_getname(padico_topo_getlocalhost()), soap_port);
    }
  return rc;
}

static void* soap_gk_worker(void*dummy)
{
  int s;
  padico_trace("starting Gatekeeper\n");
  for ( ; ; )
    {
      s = soap_accept(&soap);
      padico_trace("Socket connection successful: slave socket = %d\n", s);
      if (s < 0)
        {
          padico_warning("SOAP fault: s=%d\n", s);
          soap_print_fault(&soap, stderr);
        }
      else
        {
          soap_serve(&soap);
          soap_end(&soap);
        }
    }
  return NULL;
}

/* **** SOAP-Gatekeeper module entry points **************** */


static marcel_t soap_gk_tid = 0;

static int soap_gk_init(void)
{
  int rc;
  rc = soap_gk_start();
  if(!rc)
    {
      marcel_create(&soap_gk_tid, NULL, &soap_gk_worker, NULL);
    }
  return rc;
}

static void soap_gk_stop(void)
{
  padico_out(10, "stop %s\n", padico_topo_nodename());
  if(soap_gk_tid)
    marcel_cancel(soap_gk_tid);
  /* TODO: actually kill the thread. */
}


#include <stdio.h>
#include <Padico/Puk.h>

/* make sure code still compiles when data type is a native type and not a pointer
 */

PUK_HASHTABLE_TYPE(test_ptr_int, void*, int,
                   &puk_hash_pointer_default_hash, &puk_hash_pointer_default_eq, NULL);

PUK_HASHTABLE_TYPE(test_ptr_double, void*, double,
                   &puk_hash_pointer_default_hash, &puk_hash_pointer_default_eq, NULL);

static uint32_t test_string_hfunc(const char*key)
{
  /* compute hash for given key; use puk_hash_default(void*, size_t) to hash
     blocks of bytes with the default hash function  */
  return puk_hash_string(key);
}
static int test_string_eqfunc(const char*key1, const char*key2)
{
  /* test for key equality; returns 1 if keys are equal, 0 otherwise */
  return (strcmp(key1, key2) == 0);
}

/* declare hashtable type */
PUK_HASHTABLE_TYPE(test_string, /* token used to build names */
                   const char*, /* type for keys */
                   const char*, /* type for data */
                   &test_string_hfunc, /* function to compute hash */
                   &test_string_eqfunc, /* function to test for equality */
                   NULL /* destructor function for entries, called upon hashtable deletion */);

int main(int argc, char**argv)
{
  /* init hashtable */
  test_string_hashtable_t h = test_string_hashtable_new();

  /* insert some data */
  test_string_hashtable_insert(h, "foo", "data for foo");
  test_string_hashtable_insert(h, "bar", "data for bar");

  /* perform some lookups */
  printf("h(foo) = %s\n", test_string_hashtable_lookup(h, "foo"));
  printf("h(bar) = %s\n", test_string_hashtable_lookup(h, "bar"));
  printf("h(bang) = %s\n", test_string_hashtable_lookup(h, "bang"));

  /* iterate over all entries */
  test_string_hashtable_enumerator_t e = test_string_hashtable_enumerator_new(h);
  const char*key = NULL, *value = NULL;
  test_string_hashtable_enumerator_next2(e, &key, &value);
  while(key != NULL)
    {
      printf("entry: key = %s; value = %s\n", key, value);
      test_string_hashtable_enumerator_next2(e, &key, &value);
    }
  test_string_hashtable_enumerator_delete(e);

  /* get size */
  printf("hashtable size = %d\n", (int)test_string_hashtable_size(h));

  /* remove an entry */
  test_string_hashtable_remove(h, "bar");
  test_string_hashtable_remove(h, "bang"); /* remove an entry that does not exist! */

  /* delete hashtable */
  test_string_hashtable_delete(h);

  return 0;
}

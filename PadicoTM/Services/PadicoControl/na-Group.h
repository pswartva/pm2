/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief API for group management in PadicoTM NetAccess
 * @note should be used through PadicoControl.h
 * @ingroup NetAccess
 */

#ifndef PADICO_NA_GROUP_H
#define PADICO_NA_GROUP_H

#include <Padico/Puk.h>

/* *** Group ****************************************************** */

typedef struct padico_group_s*padico_group_t;

padico_group_t   padico_group_init    (const char*id);
/** Publishes a group to its members. Should be called by the group leader. */
void             padico_group_publish (padico_group_t g);
int              padico_group_store   (padico_group_t g);
padico_group_t   padico_group_lookup  (const char*id);
void             padico_group_node_add(padico_group_t, padico_topo_node_t);
padico_group_t   padico_group_dup(padico_group_t g, const char*new_name);
void             padico_group_destroy (padico_group_t);
int              padico_group_size    (padico_group_t);
padico_topo_node_t padico_group_node    (padico_group_t g, int rank);
const char*      padico_group_name    (padico_group_t g);
/** Rank of the given node in the group */
int              padico_group_rank    (padico_group_t, padico_topo_node_t);
/** kill all nodes in group */
void             padico_group_kill    (padico_group_t g, int rc);
/** create a new group with nodes sorted according to given topology */
padico_group_t   padico_group_topology(padico_group_t g, const char*distrib);

/* ***** Group requests ************************************ */

typedef struct padico_group_req_s*padico_group_req_t;

padico_group_req_t padico_group_send_to_all(padico_group_t group, const char*cmd);
padico_rc_t        padico_group_wait_all   (padico_group_t group, padico_group_req_t greq);

void padico_group_barrier(padico_group_t group);

#endif /* PADICO_NA_GROUP_H */

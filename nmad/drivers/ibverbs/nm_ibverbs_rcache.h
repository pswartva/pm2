/*
 * NewMadeleine
 * Copyright (C) 2006-2021 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#ifndef NM_IBVERBS_RCACHE_H
#define NM_IBVERBS_RCACHE_H

#include <infiniband/verbs.h>

#include "nm_private_config.h"

#if defined(HAVE_IBV_QUERY_DEVICE_EX) && defined(HAVE_IBVERBS_ODP)
#  define NMAD_IBVERBS_ODP 1
#  if defined(HAVE_IBVERBS_IODP)
#    define NMAD_IBVERBS_IODP 1
#  endif
#endif

#ifdef PUKABI
#include <Padico/Puk-ABI.h>
#else
#define MINI_PUKABI
#endif

struct nm_ibverbs_reg_driver_s
{
  int  (*query)(struct nm_ibverbs_context_s*p_ibverbs_context);
  void (*init)(struct nm_ibverbs_context_s*p_ibverbs_context);
  void (*close)(struct nm_ibverbs_context_s*p_ibverbs_context);
  void (*mem_reg)(struct nm_ibverbs_context_s*p_ibverbs_context, const void*ptr, size_t len,
                  struct ibv_mr**pp_mr, void**pp_handle);
  void (*mem_unreg)(struct nm_ibverbs_context_s*p_ibverbs_context, const void*ptr, size_t len,
                    struct ibv_mr*p_mr, void*p_handle);
};

extern const struct nm_ibverbs_reg_driver_s nm_ibverbs_reg_driver_nocache;
extern const struct nm_ibverbs_reg_driver_s nm_ibverbs_reg_driver_mini;
#ifdef NMAD_IBVERBS_ODP
extern const struct nm_ibverbs_reg_driver_s nm_ibverbs_reg_driver_odp;
#endif
#ifdef NMAD_IBVERBS_IODP
extern const struct nm_ibverbs_reg_driver_s nm_ibverbs_reg_driver_iodp;
#endif

#define NM_RCACHE_LOG NM_TRACEF
//#define NM_RCACHE_LOG NM_DISPF

#endif /* NM_IBVERBS_RCACHE_H */

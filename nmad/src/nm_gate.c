/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <nm_private.h>

#include <Padico/Module.h>
PADICO_MODULE_HOOK(nmad);

nm_gate_t nm_core_gate_new(nm_core_t p_core, nm_drv_vect_t*p_drvs)
{
  nm_gate_t p_gate = nm_gate_new();

  p_gate->status = NM_GATE_STATUS_INIT;
  p_gate->p_core = p_core;
  p_gate->trks = NULL;
  p_gate->n_trks = 0;
  nm_gtag_table_init(&p_gate->tags);
  nm_matching_gsession_table_init(&p_gate->gsessions);

  nm_pkt_wrap_list_init(&p_gate->pending_large_recv);
  nm_pkt_wrap_list_init(&p_gate->pending_large_send);

  nm_req_chunk_list_init(&p_gate->req_chunk_list);
  nm_ctrl_chunk_list_init(&p_gate->ctrl_chunk_list);

  p_gate->strategy_instance = puk_context_instantiate(p_core->strategy_context);
  puk_instance_indirect_NewMad_Strategy(p_gate->strategy_instance, NULL,
                                        &p_gate->strategy_receptacle);
  p_gate->strat_todo = 0;
  nm_active_gate_list_cell_setnull(p_gate);
  nm_gate_list_push_back(&p_core->gate_list, p_gate);

  struct puk_receptacle_NewMad_Strategy_s*r = &p_gate->strategy_receptacle;
  if(r->driver->connect)
    {
      nm_drv_vect_t v = (*r->driver->connect)(r->_status, p_gate, *p_drvs);
      nm_drv_vect_delete(*p_drvs);
      *p_drvs = v;
    }
  p_gate->n_trks = nm_drv_vect_size(*p_drvs);
  p_gate->trks = padico_malloc(p_gate->n_trks * sizeof(struct nm_trk_s));
  assert(p_gate->n_trks > 0);
  int i;
  for(i = 0; i < p_gate->n_trks; i++)
    {
      struct nm_trk_s*p_trk = &p_gate->trks[i];
      p_trk->trk_id = i;
      struct nm_drv_s*p_drv = nm_drv_vect_at(*p_drvs, i);
      /* instantiate driver */
      p_trk->p_drv = p_drv;
      p_trk->p_gate = p_gate;
      p_trk->instance = puk_component_instantiate(p_drv->assembly);
      puk_instance_indirect_NewMad_minidriver(p_trk->instance, NULL, &p_trk->receptacle);
      nm_trk_hashtable_insert(&p_core->trk_table, p_trk->receptacle._status, p_trk);
      p_trk->p_pw_send = NULL;
      p_trk->p_pw_recv = NULL;
      nm_pkt_wrap_list_init(&p_trk->pending_pw_send);
      nm_data_null_build(&p_trk->sdata);
      nm_data_null_build(&p_trk->rdata);
      p_trk->kind = nm_trk_undefined;
      if(p_drv->props.capabilities.trk_rdv)
        p_trk->kind = nm_trk_large;
      else if(i %  2 == 0)
        p_trk->kind = nm_trk_small;
      else
        p_trk->kind = nm_trk_large;
      p_trk->binary_url = NULL;
      p_trk->binary_url_size = 0;
    }
  return p_gate;
}

void nm_core_gate_connect_async(struct nm_core*p_core, nm_gate_t p_gate, nm_drv_t p_drv, nm_trk_id_t trk_id, const char*url)
{
  assert(url != NULL);
  nm_core_lock(p_core);
  struct nm_trk_s*p_trk = nm_trk_get_by_index(p_gate, trk_id);
  assert(p_trk != NULL);
  assert(p_trk->p_drv == p_drv);
  p_gate->status = NM_GATE_STATUS_CONNECTING;
  size_t url_size = strlen(url);
  p_trk->binary_url = puk_hex_decode(url, &url_size, NULL);
  p_trk->binary_url_size = url_size;
  struct puk_receptacle_NewMad_minidriver_s*r = &p_trk->receptacle;
  if(r->driver->connect_async != NULL)
    {
      (*r->driver->connect_async)(r->_status, p_trk->binary_url, p_trk->binary_url_size);
    }
  nm_core_unlock(p_core);
}

void nm_core_gate_connect_wait(struct nm_core*p_core, struct nm_trk_s*p_trk)
{
  nm_gate_t p_gate = p_trk->p_gate;
  nm_drv_t p_drv = p_trk->p_drv;
  nm_core_lock(p_core);
#ifdef PIOMAN
  /* don't poll on recv_any while establishing connection,
   * since it would access the not yet connected cnx */
  int masked = 0;
  if(p_drv->props.capabilities.supports_recv_any &&
     (p_drv->p_pw_recv_any != NULL) &&
     !(p_drv->p_pw_recv_any->flags & NM_PW_BLOCKING_CALL))
    {
      piom_ltask_mask(&p_drv->p_pw_recv_any->ltask);
      masked = 1;
    }
#endif /* PIOMAN */
  struct puk_receptacle_NewMad_minidriver_s*r = &p_trk->receptacle;
  if(r->driver->connect_async != NULL)
    {
      if(r->driver->connect_wait != NULL)
        {
          (*r->driver->connect_wait)(r->_status);
        }
    }
  else
    {
      (*r->driver->connect)(r->_status, p_trk->binary_url, p_trk->binary_url_size);
    }
  padico_free(p_trk->binary_url);
  p_trk->binary_url = NULL;
  /* TODO- changing gate state while we connect a single track */
  p_gate->status = NM_GATE_STATUS_CONNECTED;
#ifdef PIOMAN
  if(masked)
    {
      piom_ltask_unmask(&p_drv->p_pw_recv_any->ltask);
    }
#endif /* PIOMAN */
  /* pre-fill recv on trk #0 */
  if(p_trk->kind == nm_trk_small)
    nm_drv_refill_recv(p_drv, p_gate);
  nm_trace_event(NM_TRACE_EVENT_CONNECT, p_gate, p_gate, NULL, NM_CORE_TAG_NONE, NM_SEQ_NONE, NM_LEN_UNDEFINED);
  nm_core_unlock(p_core);
}

void nm_core_gate_disconnect(struct nm_gate_s*p_gate)
{
  p_gate->status = NM_GATE_STATUS_DISCONNECTED;
  int i;
  for(i = 0; i < p_gate->n_trks; i++)
    {
      struct nm_trk_s*p_trk = &p_gate->trks[i];
      if(p_trk->instance != NULL)
        {
          struct puk_receptacle_NewMad_minidriver_s*r = &p_trk->receptacle;
          nm_trk_hashtable_remove(&p_gate->p_core->trk_table, p_trk->receptacle._status);
          if(r->driver->disconnect)
            {
              (*r->driver->disconnect)(r->_status);
            }
          puk_instance_destroy(p_trk->instance);
          p_trk->instance = NULL;
        }
    }
}

void nm_core_gate_destroy(struct nm_gate_s*p_gate)
{
  assert(p_gate->status != NM_GATE_STATUS_CONNECTED); /* disconnect before destroy */
  nm_gate_list_remove(&p_gate->p_core->gate_list, p_gate);
  nm_matching_gsession_table_destroy(&p_gate->gsessions);
  nm_gtag_table_destroy(&p_gate->tags);
  puk_instance_destroy(p_gate->strategy_instance);
  padico_free(p_gate->trks);
  nm_gate_delete(p_gate);
}

/** notify from driver p_drv that gate p_gate has been closed by peer.
 */
void nm_core_gate_disconnected(struct nm_core*p_core, nm_gate_t p_gate, nm_drv_t p_drv)
{
  nm_trace_event(NM_TRACE_EVENT_DISCONNECT, p_gate, p_gate, NULL, NM_CORE_TAG_NONE, NM_SEQ_NONE, NM_LEN_UNDEFINED);
  p_gate->status = NM_GATE_STATUS_DISCONNECTING;
  int connected = 0;
  NM_FOR_EACH_GATE(p_gate, p_core)
    {
      if(p_gate->status == NM_GATE_STATUS_CONNECTED)
        connected++;
    }
  if((connected > 0) && (p_drv->props.capabilities.supports_recv_any))
    {
      nm_drv_refill_recv(p_drv, NULL);
    }
}

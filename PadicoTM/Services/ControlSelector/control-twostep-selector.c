/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @ingroup Control
 */

#include <Padico/Puk.h>
#include <Padico/Module.h>
#include <Padico/PadicoTM.h>

#include <Padico/NetSelector.h>
#include <Padico/PadicoControl.h>
#include <Padico/AddrDB.h>

static int control_twostep_selector_init(void);
static void control_twostep_selector_finalize(void);

PADICO_MODULE_DECLARE(ControlTwoStepSelector, control_twostep_selector_init, NULL, control_twostep_selector_finalize);

/** @defgroup ControlTwoStepSelector component: ControlTwoStepSelector- selector setting up a direct
 * control channel to the remote node.
 * @ingroup PadicoComponent
 */

static struct
{
  puk_hashtable_t table; /* routing table- hashtable: node -> controler descriptor */
  puk_component_t selector;
} twostep_selector = { .selector = NULL };

static void control_twostep_selector_event_listener(void*arg);

struct twostep_selector_controler_s
{
  volatile enum
    {
      CONTROL_TWOSTEP_SELECTOR_BOOTSTRAP,
      CONTROL_TWOSTEP_SELECTOR_INPROGRESS,
      CONTROL_TWOSTEP_SELECTOR_DIRECT
    } step;
  union
    {
      puk_instance_t bootstrap_controler;
      puk_instance_t direct_controler;
    };
};

static puk_instance_t control_twostep_select_controler(padico_topo_node_t node);


/* ********************************************************* */

/** 'ControlSelector' facet for the ControlTwoStepSelector component
 * @ingroup ControlTwoStepSelector
 */
static const struct padico_control_selector_driver_s control_twostep_selector_driver =
  {
    .router  = &control_twostep_select_controler
  };

/* ********************************************************* */


static void control_twostep_connector_new_handler(puk_parse_entity_t e);

static int control_twostep_selector_init(void)
{
  padico_print("loading.\n");
  twostep_selector.selector =
    puk_component_declare("ControlTwoStepSelector",
                          puk_component_provides("ControlSelector", "selector", &control_twostep_selector_driver));
  twostep_selector.table = puk_hashtable_new_ptr();
  puk_xml_add_action((struct puk_tag_action_s){
                     .xml_tag        = "ControlTwoStepSelector:ConnectorNew",
                     .start_handler  = &control_twostep_connector_new_handler,
                     .end_handler    = NULL,
                     .required_level = PUK_TRUST_CONTROL
                     });
  padico_control_event_subscribe(&control_twostep_selector_event_listener);
  padico_control_plug_selector(twostep_selector.selector);
  return 0;
}

static void control_twostep_selector_finalize(void)
{
  padico_control_unplug_selector(twostep_selector.selector);
  padico_control_event_unsubscribe(&control_twostep_selector_event_listener);
}

/* ********************************************************** */
/* *** ControlTwoStepSelector ******************************* */

struct control_twostep_remote_node_s
{
  puk_instance_t controler;
  padico_topo_node_t node;
};

static void control_twostep_connector_notifier(padico_rc_t reply, void*key)
{
  struct control_twostep_remote_node_s*remote = key;
  if(padico_rc_iserror(reply))
    {
      padico_warning("advertise_new_node(): failed to setup the controler for node %s (%s).\n",
                     padico_topo_node_getname(remote->node), reply?padico_rc_gettext(reply):"unknown error");
    }
  else
    {
      padico_out(20, "Connection established.\n");
      padico_control_event_disable();
      struct twostep_selector_controler_s*step_controler = puk_hashtable_lookup(twostep_selector.table, remote->node);
      if(step_controler != NULL)
        {
          step_controler->step = CONTROL_TWOSTEP_SELECTOR_DIRECT;
          if(remote->controler != NULL)
            {
              step_controler->direct_controler = remote->controler;
              padico_print("node=%s is now served by assembly %s.\n",
                           padico_topo_node_getname(remote->node), remote->controler->component->name);
            }
          else
            {
              padico_print("node=%s fall back on boostrap channel.\n", padico_topo_node_getname(remote->node));
            }
        }
      padico_control_event_enable();
    }
  padico_rc_delete(reply);
  padico_free(remote);
}

static void control_twostep_connector_new_handler(puk_parse_entity_t e)
{
  const char*s_remote_node = puk_parse_getattr(e, "node");
  const padico_topo_node_t remote_node = padico_topo_getnodebyname(s_remote_node);
  padico_print("received request from node = %s\n", (char*)padico_topo_node_getuuid(remote_node));
  padico_rc_t rc = NULL;
  if(!remote_node)
    {
      rc = padico_rc_error("Connector: unknown node=%s.", s_remote_node);
      goto error;
    }
  struct twostep_selector_controler_s*step_controler = puk_hashtable_lookup(twostep_selector.table, remote_node);
  if(!step_controler)
    {
      rc = padico_rc_error("Connector: node=%s not in routing table.", s_remote_node);
      goto error;
    }
  if(step_controler->step != CONTROL_TWOSTEP_SELECTOR_BOOTSTRAP)
    {
      rc = padico_rc_error("Connector: node=%s already connected (or in progress).", s_remote_node);
      goto error;
    }
  puk_component_t component =
    padico_ns_serial_selector(remote_node, "default", puk_iface_PadicoConnector());
  if(component == NULL || puk_component_get_driver_PadicoControl(component, NULL) == NULL)
    {
      rc = padico_rc_error("Connector: node=%s- no suitable assembly found (PadicoControl, PadicoConnector).",
                           padico_topo_nodename());
      goto error;
    }
  puk_instance_t instance = puk_component_instantiate(component);
  struct puk_receptacle_PadicoConnector_s r;
  puk_instance_indirect_PadicoConnector(instance, NULL, &r);
  if(r.driver == NULL)
    {
      rc = padico_rc_error("Connector: node=%s- assembly=%s has no 'PadicoConnector' facet.",
                           padico_topo_nodename(), component->name);
      goto error;
    }
  struct control_twostep_remote_node_s*remote = padico_malloc(sizeof(struct control_twostep_remote_node_s));
  *remote = (struct control_twostep_remote_node_s) {
    .controler = instance,
    .node      = remote_node
  };
  padico_req_t req = padico_tm_req_new(&control_twostep_connector_notifier, remote);
#warning TODO- convert to new PadicoConnector with AddrDB
  /*
  int rc2 = (*r.driver->connect)(r._status, req, remote_node);
  if(rc2)
    {
      rc = padico_rc_error("Connector: node=%s- error while invoking connector->connect on %s.",
                           padico_topo_nodename(), component->name);
      goto error;
    }
  */

 error:
  if(padico_rc_iserror(rc))
    {
      padico_warning("connector handler error: %s.\n", padico_rc_gettext(rc));
    }
  puk_parse_set_rc(e, rc);
}


/* ********************************************************* */
/* ** 'ControlSelector' facet */

static puk_instance_t control_twostep_select_controler(padico_topo_node_t node)
{
  puk_instance_t control_instance = NULL;
  padico_control_event_disable();
  padico_out(50, "request for node=%s\n", padico_topo_node_getname(node));
  struct twostep_selector_controler_s*step_controler = puk_hashtable_lookup(twostep_selector.table, node);
  if(step_controler)
    {
      padico_out(60, "selector step=%d\n", step_controler->step);
      switch(step_controler->step)
        {
        case CONTROL_TWOSTEP_SELECTOR_BOOTSTRAP:
          {
            step_controler->step = CONTROL_TWOSTEP_SELECTOR_INPROGRESS;
            puk_component_t controler =
              padico_ns_serial_selector(node, "default", puk_iface_PadicoConnector());
            if(controler)
              padico_print("controler = %s\n", controler->name);
            else
              padico_print("controler = NULL\n");

            if((controler != NULL) && (controler != step_controler->bootstrap_controler->component))
              {
                puk_instance_t controler_instance = puk_component_instantiate(controler);
                padico_out(20, "init remote connector (remote node=%s)..\n", padico_topo_node_getname(node));
                struct control_twostep_remote_node_s*remote = padico_malloc(sizeof(struct control_twostep_remote_node_s));
                *remote = (struct control_twostep_remote_node_s) {
                  .controler = controler_instance,
                  .node      = node
                };
                padico_string_t str_new = padico_string_new();
                padico_string_catf(str_new, "<ControlTwoStepSelector:ConnectorNew node=\"%s\"/>", padico_topo_nodename());
                padico_control_send_async(node, padico_string_get(str_new), NULL, NULL);
                padico_string_delete(str_new);
                struct puk_receptacle_PadicoConnector_s r;
                puk_instance_indirect_PadicoConnector(remote->controler, NULL, &r);
                padico_out(20, "connecting to remote node=%s...\n", padico_topo_node_getname(node));
                padico_req_t req = padico_tm_req_new(&control_twostep_connector_notifier, remote);
                padico_print("sending request to remote node\n");
#warning TODO- convert to new PadicoConnector with AddrDB
  /*
                (*r.driver->connect)(r._status, req, node);
  */
                control_instance = step_controler->bootstrap_controler;
              }
            else
              {
                padico_print("no suitable assembly found (PadicoControl, PadicoConnector), stick on boostrap channel.\n");
                step_controler->step = CONTROL_TWOSTEP_SELECTOR_DIRECT;
                control_instance = step_controler->direct_controler;
              }
            padico_print("[%s] bootstrap = %s\n",
                         (char*)padico_topo_node_getuuid(node),
                         control_instance->component->name);
          }
        break;

        case CONTROL_TWOSTEP_SELECTOR_INPROGRESS:
          {
            control_instance = step_controler->bootstrap_controler;
            padico_print("[%s] inprogress = %s\n",
                         (char*)padico_topo_node_getuuid(node),
                         control_instance->component->name);
          }
        break;

        case CONTROL_TWOSTEP_SELECTOR_DIRECT:
          {
            control_instance = step_controler->direct_controler;
            padico_print("[%s] direct = %s\n",
                         (char*)padico_topo_node_getuuid(node),
                         control_instance->component->name);
          }
        break;

        default:
        break;
        }
      padico_out(50, "node served by assembly %s\n", control_instance->component->name);
    }
  else
    {
      padico_warning("no assembly found for control channel to node %s.\n",
                     padico_topo_node_getname(node));
    }
  padico_control_event_enable();

  return control_instance;
}

/* ********************************************************* */
/* ** Topology events handler to update routing table */

static void control_twostep_new_node(const puk_instance_t controler, const padico_topo_node_t node)
{
  assert(controler != NULL);

  struct twostep_selector_controler_s*step_controler = puk_hashtable_lookup(twostep_selector.table, node);
  if(step_controler == NULL)
    {
      step_controler = padico_malloc(sizeof(struct twostep_selector_controler_s));
      step_controler->step                = CONTROL_TWOSTEP_SELECTOR_BOOTSTRAP;
      step_controler->bootstrap_controler = controler;
      puk_hashtable_insert(twostep_selector.table, node, step_controler);
      padico_out(40, "init node event- new node *%s* served by %s\n",
                 padico_topo_node_getname(node), controler->component->name);
    }
}

static void control_twostep_selector_event_listener(void*arg)
{
  padico_control_event_t event = (struct padico_control_event_s*)arg;
  switch(event->kind)
    {
    case PADICO_CONTROL_EVENT_INIT_CONTROLER:
      {
        const padico_topo_node_vect_t nodes = event->INIT_CONTROLER.nodes;
        const puk_instance_t controler = event->INIT_CONTROLER.controler;
        padico_topo_node_vect_itor_t i;
        puk_vect_foreach(i, padico_topo_node, nodes)
          {
            control_twostep_new_node(controler, *i);
          }
      }
    break;

    case PADICO_CONTROL_EVENT_NEW_NODE:
      {
        const padico_topo_node_t node = event->NODE.node;
        const puk_instance_t controler = event->NODE.controler;
        control_twostep_new_node(controler, node);
      }
    break;

    case PADICO_CONTROL_EVENT_DELETE_NODE:
      {
        const padico_topo_node_t node = event->NODE.node;
        if(puk_hashtable_lookup(twostep_selector.table, node) != NULL)
          {
            puk_hashtable_remove(twostep_selector.table, node);
          }
      }
    break;

    default:
    break;
    }
}

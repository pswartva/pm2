/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief File-specific part for vnodes.
 * @ingroup VIO
 */

/** @todo VFile64 is not yet properly turned into using the new Puk-ABI
 * -> It should explicitly be used.
 */

#include "VLink-internals.h"
#include <Padico/Module.h>

#include <stdio.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/mman.h>


static int     vfile64_fsync(padico_vnode_t vnode);
static int     vfile_open64(padico_vnode_t vnode, const char *path, int oflag, mode_t mode);
static off64_t vfile_lseek64(padico_vnode_t vnode, off64_t offset, int whence);
static int     vfile_ftruncate64(padico_vnode_t vnode, off64_t length);
static int     vfile___fxstat64(int version, padico_vnode_t vnode, struct stat64*buf);
static void*   vfile_mmap64(void *addr, size_t len, int prot, int flags, padico_vnode_t vnode, off64_t off);

/** driver which incarnates the VFile64/SysIO component */
static const struct padico_vlink_driver_s vfile64_vlink_driver =
  {
    .create       = NULL,
    .bind         = NULL,
    .listen       = NULL,
    .connect      = NULL,
    .fullshutdown = NULL,
    .close        = vnode_hfd_close,
    .send_post    = vnode_hfd_write_post,
    .recv_post    = vnode_hfd_read_post,
    .dumpstat     = NULL
  };

static const struct padico_vfile64_driver_s vfile64_driver =
  {
    .open      = &vfile_open64,
    .lseek     = &vfile_lseek64,
    .ftruncate = &vfile_ftruncate64,
    .fxstat    = &vfile___fxstat64,
    .mmap      = &vfile_mmap64,
    .fcntl     = &vnode_hfd_fcntl,
    .fsync     = &vfile64_fsync
  };

PADICO_MODULE_COMPONENT(VFile64_SysIO,
  puk_component_declare("VFile64_SysIO",
                        puk_component_provides("PadicoComponent", "component", &padico_vnode_component_driver),
                        puk_component_provides("VLink", "vlink", &vfile64_vlink_driver),
                        puk_component_provides("VFile64", "vfile64", &vfile64_driver)));

/* ********************************************************* */


static int vfile_open64(padico_vnode_t vnode, const char*path, int oflag, mode_t mode)
{
  int fd;
  int err;
  padico_out(40, "path=%s; flags=%X; mode=%o\n", path, oflag, mode);
  if(oflag & O_CREAT)
    {
      fd  = PUK_ABI_FSYS_WRAP(open64)(path, oflag, mode);
      err = __puk_abi_wrap__errno;
    }
  else
    {
      fd  = PUK_ABI_FSYS_WRAP(open64)(path, oflag);
      err = __puk_abi_wrap__errno;
    }
  if(fd == -1)
    {
      return -err;
    }
  else
    {
      vnode_hfd_create(vnode, fd, mode);
      vnode->status.opened = 1;
      return 0;
    }
}

static off64_t vfile_lseek64(padico_vnode_t vnode, off64_t offset, int whence)
{
  vnode_check(vnode, hfd);
  off64_t rc = PUK_ABI_FSYS_WRAP(lseek64)(vnode->hfd.fd, offset, whence);
  int err = __puk_abi_wrap__errno;
  vnode_set_error(vnode, err);
  return rc;
}

static int vfile_ftruncate64(padico_vnode_t vnode, off64_t length)
{
  vnode_check(vnode, hfd);
  int rc  = PUK_ABI_FSYS_WRAP(ftruncate64)(vnode->hfd.fd, length);
  int err = __puk_abi_wrap__errno;
  vnode_set_error(vnode, err);
  return rc;
}

static int vfile___fxstat64(int version, padico_vnode_t vnode, struct stat64*buf)
{
  vnode_check(vnode, hfd);
  int rc  = PUK_ABI_FSYS_WRAP(__fxstat64)(version, vnode->hfd.fd, buf);
  int err = __puk_abi_wrap__errno;
  vnode_set_error(vnode, err);
  return rc;
}

static void *vfile_mmap64(void *addr, size_t len, int prot, int flags,
                          padico_vnode_t vnode, off64_t off)
{
  vnode_check(vnode, hfd);
  void*rc = mmap64(addr, len, prot, flags, vnode->hfd.fd, off);
  int err = __puk_abi_wrap__errno;
  if(rc == MAP_FAILED)
    {
      vnode_set_error(vnode, err);
    }
  return rc;
}

static int vfile64_fsync(padico_vnode_t vnode)
{
  int rc  = PUK_ABI_FSYS_WRAP(fsync)(vnode->hfd.fd);
  int err = __puk_abi_wrap__errno;
  vnode_set_error(vnode, err);
  return rc;
}

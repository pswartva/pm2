#
The NewMadeleine Documentation
==============================

Introduction
------------

NewMadeleine is a complete redesign and rewrite of the
communication library Madeleine. The new architecture aims at
enabling the use of a much wider range of communication flow
optimization techniques. It is entirely modular: The request
scheduler itself is interchangeable, allowing experimentations
with multiple approaches or on multiple issues with regard to
processing communication flows. We have implemented an
optimizing scheduler called SchedOpt. SchedOpt targets
applications with irregular, multi-flow communication schemes such
as found in the increasingly common application conglomerates made
of multiple programming environments and coupled pieces of code,
for instance. SchedOpt itself is easily extensible through the
concepts of optimization strategies  (what to optimize for, what
the optimization goal is) expressed in terms of tactics (how to
optimize to reach the optimization goal). Tactics themselves are
made of basic communication flows operations such as packet
merging or reordering.

More information on NewMadeleine can be found at
<http://pm2.gitlabpages.inria.fr/newmadeleine/>.


User APIs
---------

Several APIs are provided to NewMadeleine users:
  - the @ref mpi_interface known as MadMPI, which is a regular **MPI implementation**
    with full MPI-1 compliance, and parts of MPI-2 and MPI-3 (multi-threading and RMA)

or the **native** NewMadeleine APIs:
  - the @ref nm_public containing definitions common to all NewMadeleine interfaces.
  - the @ref sr_interface as general purpose messaging interface based on send/receive.
  - the @ref rpc_interface for asynchronous Remote Procedure Calls (RPC).
  - the @ref launcher_interface to initialize NewMadeleine and open sessions.
  - the @ref coll_interface for collective operations.
  - the @ref pack_interface for incremental packing/unpacking of messages.
  - the @ref onesided_interface for one-sided operations (RMA)
  - the @ref mcast_interface for dynamic multicasts.

the **internal** interfaces *not designed for end-users*:
  - the @ref core_interface for direct access to nmad packet scheduler. This is the interface upon all other interfaces rely. This is a private interface and may change at any time.
  - the @ref session_interface to explicitely interface with connection management, typically in case where user wants its own launcher rather than `padico-launch` or `mpirun`. This is normally not needed by regular users which should use @ref launcher_interface instead.
  - the @ref connector_interface used internally to exchange urls.
  - the @ref mpi_private_interface used internally in MadMPI.
  - the @ref nmad_private used internally by the nmad scheduler

Examples
--------

See @subpage examplereadme "README for examples" in `examples/` directory.

Getting the source code
-----------------------

NewMadeleine files are hosted by the PM2 project on the
Inria Gitlab at <https:/gitlab.inria.fr/pm2/>

The source code is managed by a *GIT* server hosted by
the Inria Gitlab. Anonymous GIT access is available, so that you
don't need to become a project member to access the repository
with the latest source. Alternatively, you can download a
released PM2 tarball.

Installation
------------

For up to date installation details and various configuration parameters see @subpage README

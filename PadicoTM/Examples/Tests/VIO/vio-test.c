/** @file
 * Test VIO sockets between 2 nodes of the same cluster.
 */

#include <Padico/Puk.h>
#include <Padico/VIO.h>
#include <Padico/NetAccess.h>
#include <Padico/PM2.h>
#include <Padico/Timing.h>
#include <Padico/Module.h>

#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

static int padico_module_init(void);
static int padico_module_run(int argc, char**argv);

PADICO_MODULE_DECLARE(vio_test, padico_module_init, padico_module_run, NULL);
#define MAX_CLIENTS 10

#define BUFFER_SIZE (4*1024*1024)

const int num_roundtrip    = 500;
const int check_buffer     = 0;
const int check_incomplete = 0;
const int split_msg        = 0;
const int reinit_buffer    = 0;

struct servant_s
{
  int fd;
  char*buffer;
};

struct server_s
{
  int fd;
  struct servant_s servants[MAX_CLIENTS];
};

struct client_s
{
  int fd;
  char*buffer;
};


#define BASE_PORT_NUMBER 4576
#define MY_PORT_NUMBER (BASE_PORT_NUMBER + padico_na_rank())

#define other_server_name(N) \
 (padico_topo_host_getname(padico_topo_node_gethost(padico_na_madio_getnodebyrank(N))))
#define other_server_addr(N) \
 (padico_topo_host_getaddr(padico_topo_node_gethost(padico_na_madio_getnodebyrank(N))))
#define other_server_port(N) \
 ((BASE_PORT_NUMBER) + (N))

void test_failed(void)
{
  padico_warning("[vio-test] halted.\n");
  /* TODO: we should unload(self) here */
  padico_fatal("Vio test failed.\n");
}

static struct server_s* make_server()
{
  int rc;
  struct sockaddr_in addr;
  struct server_s* server = padico_malloc(sizeof(struct server_s));

  /* socket */
  server->fd = padico_vio_socket(AF_INET, SOCK_STREAM, 0);
  padico_print("[vio-test] socket() fd_server=%d\n", server->fd);
  if(server->fd == -1)
    test_failed();

  /* bind */
  addr.sin_family = AF_INET;
  addr.sin_addr.s_addr = htonl(INADDR_ANY);
  addr.sin_port = htons(MY_PORT_NUMBER);
  rc = padico_vio_bind(server->fd, &addr, sizeof(addr));
  padico_print("[vio-test] bind() fd=%d rc=%d\n", server->fd, rc);
  if(rc)
    {
      padico_warning("[vio-test] error in bind()\n");
      test_failed();
    }

  /* listen */
  rc = padico_vio_listen(server->fd, 5);
  padico_print("[vio-test] listen() fd=%d rc=%d\n", server->fd, rc);
  if(rc)
    test_failed();

  return server;
}

static void make_servant(struct server_s*server, struct servant_s*servant)
{
  struct sockaddr_in addr;
  int addr_len = sizeof(addr);
  /* accept */
  servant->fd = padico_vio_accept(server->fd, &addr, &addr_len);
  padico_print("[vio-test] accept() fd_server=%d fd_servant=%d\n",
               server->fd, servant->fd);
  if(servant->fd == -1)
    test_failed();
}

static struct client_s* make_client()
{
  struct sockaddr_in addr;
  struct client_s*client = padico_malloc(sizeof(struct client_s));
  int server_rank = 1 - padico_na_rank();
  int rc;

  /* socket */
  client->fd = padico_vio_socket(AF_INET, SOCK_STREAM, 0);
  padico_print("[vio-test] socket() fd_client=%d\n", client->fd);
  if(client->fd == -1)
    test_failed();

  /* connect */
  addr.sin_family = AF_INET;
  addr.sin_port = htons(other_server_port(server_rank));
  memcpy(&addr.sin_addr, other_server_addr(server_rank), sizeof(addr.sin_addr));
  rc = padico_vio_connect(client->fd, &addr, sizeof(addr));
  if(rc)
    {
      while(rc)
        {
          padico_print("[vio-test] CLIENT 0x%X server not ready (%s) --retry...\n",
                       client, strerror(padico_vio_errno));
          padico_tm_sleep(4);
          rc = padico_vio_connect(client->fd, &addr, sizeof(addr));
        }
    }
  padico_print("[vio-test] connect() server=%s fd_client=%d rc=%d\n",
               other_server_name(server_rank), client->fd, rc);
  return client;
}
#define ALIGN_VAL  ((unsigned)sizeof(long))
#define ALIGN_MASK (~((ALIGN_VAL)-1))
#define ALIGNED(N) (((unsigned)(N)) & (ALIGN_MASK))

static void send_msg(int fd, char*buffer, int size, int size1, int i)
{
  int done = 0;
  if(size1 == 0) size1 = ALIGN_VAL;
  if(!split_msg)
    size1=size;
  while(done < size)
    {
      int rc;
      padico_trace("vio_write() i=%d size=%d size1=%d done=%d\n", i, size, size1, done);
      rc = padico_vio_write(fd, buffer+done, size1);
      padico_trace("vio_write() i=%d size=%d size1=%d rc=%d\n", i, size, size1, rc);
      if(rc >=0)
        {
          done += rc;
          size1 = size - done;
        }
      else
        {
          padico_print("[vio-test] write error rc=%d errno=%d (%s)\n", rc, padico_vio_errno, strerror(padico_vio_errno));
          test_failed();
        }
    }
}

static void print_sock_info(int fd)
{
  struct sockaddr_in addr;
  socklen_t addr_len = sizeof(addr);
  int rc;
  struct hostent*he = NULL;
  char*hostname;
  padico_print("[vio-test]     fd=%d\n", fd);
  rc = padico_vio_getsockname(fd, &addr, &addr_len);
  if(!rc)
    {
      if((int)addr.sin_addr.s_addr != 0)
        {
          he = gethostbyaddr(&addr.sin_addr, sizeof(addr.sin_addr), AF_INET);
          hostname = he->h_name;
        }
      else
        {
          hostname = "*ANY*";
        }
      padico_print("[vio-test]     local=%s:%d (%s)\n",
                   inet_ntoa(addr.sin_addr),
                   ntohs(addr.sin_port),
                   hostname);
    }
  rc = padico_vio_getpeername(fd, &addr, &addr_len);
  if(!rc)
    {
      he = gethostbyaddr(&addr.sin_addr, sizeof(addr.sin_addr), AF_INET);
      padico_print("[vio-test]     peer=%s:%d (%s)\n",
                   inet_ntoa(addr.sin_addr),
                   ntohs(addr.sin_port),
                   (he==NULL)?"-":he->h_name);
    }
}

static void fill_buffer(unsigned char*buffer, int size, int tid, int roundtrip)
{
  int i;
  for(i=0; i<size; i++)
    {
      if(i%2)
        buffer[i] = i;
      else
        if(i%4)
          buffer[i] = roundtrip;
        else
          buffer[i] = tid;
    }
}

static void*servant_loop(struct servant_s*servant)
{
  int i, s;
  int fd = servant->fd;
  padico_print("[vio-test] SERVANT 0x%X fd=%d starting\n", servant, fd);
  servant->buffer = padico_malloc(BUFFER_SIZE);
  for(s=4; s<BUFFER_SIZE; s*=2)
    {
      volatile char*buffer = servant->buffer;
      padico_print("[vio-test] SERVANT 0x%X fd=%d receiving size=%d\n", servant, fd, s);
      for(i=0; i<num_roundtrip; i++)
        {
          int j;
          int val = 0;//s+i;
          /* re-init buffer */
          if(reinit_buffer)
            {
              memset(buffer, 0, s);
            }
          /* receive message */
          {
            int done = 0;
            int size1 = ALIGNED(s*drand48());
            int size  = s;
            drand48();
            if(!split_msg)
              size1=size;
            if(size1 == 0) size1 = ALIGN_VAL;
            while(done < size)
              {
                int rc;
                padico_trace("vio_read() tid=%d i=%d size=%d size1=%d done=%d\n",
                             marcel_self(), i, size, size1, done);
                rc = padico_vio_read(fd, buffer+done, size1);
                padico_trace("vio_read() tid=%d i=%d size=%d size1=%d rc=%d\n",
                             marcel_self(), i, size, size1, rc);
                if(rc >0)
                  {
                    done += rc;
                    size1 = size - done;
                    {
                      if(check_incomplete)
                      for(j=0; j<done; j++)
                        {
                          unsigned char expected = (unsigned char)(val+j);
                          unsigned char received = (unsigned char)(buffer[j]);
                          if((j%2) && (received != expected))
                            {
                              padico_warning("[vio-test] check buffer failed at byte %d (done=%d, rc=%d)\n", j, done, rc);
                              padico_warning("[vio-test] expected=0x%X, received=0x%X roundtrip=%d fd=%d\n",
                                             (int)(expected), (int)(received), i, fd);
                              {
                                int k;
                                for(k=0; k<done; k++)
                                  {
                                    printf("%2X (%2X) ", (int)((unsigned char)(buffer[k])), (int)((unsigned char)(val+k)));
                                  }
                                printf("\n");
                              }
                              marcel_yield();
                              test_failed();
                            }

                          padico_trace("%d bytes checked ok\n", s);
                        }

                    }
                  }
                else if (rc == 0)
                  {
                    padico_print("[vio-test] end of file\n");
                    test_failed();
                  }
                else
                  {
                    padico_print("[vio-test] read error errno=%d\n", padico_vio_errno);
                    test_failed();
                  }
              }
          }
          /* check buffer */
          if(check_buffer)
          for(j=0; j<s; j++)
            {
              unsigned char expected = (unsigned char)(val+j);
              unsigned char received = (unsigned char)(buffer[j]);
              if((j%2) && (received != expected))
                {
                  padico_warning("[vio-test] check buffer failed at byte %d (size=%d)\n", j, s);
                  padico_warning("[vio-test] expected=0x%X, received=0x%X roundtrip=%d fd=%d\n",
                                 (int)(expected), (int)(received), i, fd);
                  {
                    int k;
                    for(k=0; k<s; k++)
                      {
                        printf("%X (%X) ", (int)((unsigned char)(buffer[k])), (int)((unsigned char)(val+k)));
                      }
                    printf("\n");
                  }
                  test_failed();
                }

              padico_trace("%d bytes checked ok\n", s);
            }
        }
    }
  padico_free(servant->buffer);
  padico_print("[vio-test] SERVANT 0x%X fd=%d closing...\n", servant, servant->fd);
  padico_vio_close(servant->fd);
  padico_print("[vio-test] SERVANT 0x%X fd=%d completed\n", servant, servant->fd);
  servant->fd = -1;
  return NULL;
}

static void*client_loop(struct client_s*client)
{
  int i,s ;
  padico_timing_t t1, t2;
  double t, bw;
  padico_print("[vio-test] CLIENT 0x%X fd=%d starting\n", client, client->fd);
  client->buffer = padico_malloc(BUFFER_SIZE);
  for(s=4; s<BUFFER_SIZE; s*=2)
    {
      padico_print("[vio-test] CLIENT 0x%X fd=%d sending size=%d\n", client, client->fd, s);
      padico_timing_get_tick(&t1);
      for(i=0; i<num_roundtrip; i++)
        {
          if(check_buffer)
            fill_buffer(client->buffer, s, (((unsigned)marcel_self())>>16), i);
          send_msg(client->fd, client->buffer, s, ALIGNED(s*drand48()), i);
        }
      padico_timing_get_tick(&t2);
      t = padico_timing_diff_usec(&t1, &t2) / (double)num_roundtrip;
      bw = (double)s/t;
      padico_print("[vio-test] CLIENT 0x%X fd=%d sent size=%d roundtrips=%d bw=%7.2fMB/s\n",
                   client, client->fd, s, num_roundtrip, bw);
    }
  padico_free(client->buffer);
  padico_print("[vio-test] CLIENT 0x%X fd=%d closing...\n", client, client->fd);
  padico_vio_close(client->fd);
  padico_print("[vio-test] CLIENT 0x%X fd=%d completed\n", client, client->fd);
  client->fd = -1;
  return NULL;
}

static void* server_handler(struct server_s*server)
{
  int n;
  for(n=0; n < MAX_CLIENTS; n++)
    {
      make_servant(server, &server->servants[n]);
      //  marcel_create(&server->servants[n].tid, NULL, &servant_loop, &server->servants[n]);
      padico_tm_bgthread_start(NULL, &servant_loop, &server->servants[n], "vio-test:servant_loop");
    }
  return NULL;
}

static void start_server(struct server_s*server)
{
  padico_tm_bgthread_start(NULL, &server_handler, server, "vio-test:server");
}

static void start_client(struct client_s*client)
{
  padico_tm_bgthread_start(NULL, &client_loop, client, "vio-test:client_loop");
}

static struct server_s*server1;

static int padico_module_init(void)
{
  padico_print("[vio-test] init\n");
  server1 = make_server();
  start_server(server1);
  return 0;
}

static int padico_module_run(int argc, char**argv)
{
  struct client_s*c1, *c2, *c3;
  padico_tm_thread_givename("vio-test:main");
  padico_print("[vio-test] starting mono-threaded test...\n");
  c1 = make_client();
  client_loop(c1);

  padico_print("[vio-test] starting multi-threaded test...\n");
  c2 = make_client();
  start_client(c2);

  c3 = make_client();
  start_client(c3);

  /*
    if(fd_client>0 && fd_servant>0 && fd_server>0)
    {
    padico_print("[vio-test] Connection success!\n");
    padico_print("[vio-test] --------------------------------\n");
    padico_print("[vio-test]   Server socket\n");
    print_sock_info(fd_server);
    padico_print("[vio-test]   Client socket\n");
    print_sock_info(fd_client);
    padico_print("[vio-test]   Servant socket\n");
    print_sock_info(fd_servant);
    padico_print("[vio-test] --------------------------------\n");
    padico_print("[vio-test] Message received: %s\n", inbox);
    }
    else
    {
    padico_print("[vio-test] Connection failed!\n");
    }
    padico_print("[vio-test] Test success!\n");
  */
  return 0;
}

/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


#ifndef NM_MPI_COMMUNICATOR_H
#define NM_MPI_COMMUNICATOR_H

/**
 * @ingroup mpi_interface
 * @file
 * declarations for MPI communicators
 * @{
 */

/** @name Functions: Communicators
 * @{ */


/**
 * This function indicates the number of processes involved in a an
 * intracommunicator.
 * @param comm communicator
 * @param size number of processes in the group of comm
 * @return MPI status
 */
int MPI_Comm_size(MPI_Comm comm,
                  int *size);

/**
 * This function gives the rank of the process in the particular
 * communicator's group.
 * @param comm communicator
 * @param rank rank of the calling process in group of comm
 * @return MPI status
 */
int MPI_Comm_rank(MPI_Comm comm,
                  int *rank);

int MPI_Keyval_create(MPI_Copy_function*copy_fn, MPI_Delete_function*delete_fn, int*keyval, void*extra_state);

int MPI_Keyval_free(int*keyval);

/**
 * This function returns attributes values from communicators.
 */
int MPI_Attr_get(MPI_Comm comm, int keyval, void*attr_value, int*flag);

/**
 * Stores attribute value associated with a key
 */
int MPI_Attr_put(MPI_Comm comm, int keyval, void*attr_value);

int MPI_Attr_delete(MPI_Comm comm, int keyval);


int MPI_Comm_create_keyval(MPI_Comm_copy_attr_function*comm_copy_attr_fn, MPI_Comm_delete_attr_function*comm_delete_attr_fn, int*comm_keyval, void*extra_state);

int MPI_Comm_free_keyval(int*comm_keyval);

int MPI_Comm_get_attr(MPI_Comm comm, int comm_keyval, void*attr_value, int*flag);

int MPI_Comm_set_attr(MPI_Comm comm, int comm_keyval, void*attr_value);

int MPI_Comm_delete_attr(MPI_Comm comm, int comm_keyval);


int MPI_Comm_set_info(MPI_Comm comm, MPI_Info info);

int MPI_Comm_get_info(MPI_Comm comm, MPI_Info*info_used);

/**
 * Creates a new communicator
 * @param comm communicator
 * @param group group, which is a subset of the group of comm
 * @param newcomm new communicator
 */
int MPI_Comm_create(MPI_Comm comm,
                    MPI_Group group,
                    MPI_Comm*newcomm);

int MPI_Comm_create_group(MPI_Comm comm, MPI_Group group, int tag, MPI_Comm*newcomm);

int MPI_Comm_create_from_group(MPI_Group group, const char*stringtag,
                               MPI_Info info, MPI_Errhandler errhandler, MPI_Comm*newcomm);

/**
 * Returns a handle to the group of the given communicator.
 * @param comm communicator
 * @param group group corresponding to comm
 * @return MPI status
 */
int MPI_Comm_group(MPI_Comm comm,
                   MPI_Group *group);

/**
 * Partitions the group associated to the communicator into disjoint
 * subgroups, one for each value of color.
 * @param comm communicator
 * @param color control of subset assignment
 * @param key control of rank assigment
 * @param newcomm new communicator
 * @return MPI status
 */
int MPI_Comm_split(MPI_Comm comm,
                   int color,
                   int key,
                   MPI_Comm *newcomm);

/**
 * Partitions the group associated to the communicator into disjoint
 * subgroups, based on the type specified by split_type. Each subgroup
 * contains all processes of the same type.
 * @param comm communicator
 * @param split_type control of subset assignment
 * @param key control of rank assigment
 * @param info info argument
 * @param newcomm new communicator
 * @return MPI status
 */
int MPI_Comm_split_type(MPI_Comm comm,
                        int split_type,
                        int key,
                        MPI_Info info,
                        MPI_Comm *newcomm);

/**
 * Creates a new intracommunicator with the same fixed attributes as
 * the input intracommunicator.
 * @param comm communicator
 * @param newcomm copy of comm
 * @return MPI status
 */
int MPI_Comm_dup(MPI_Comm comm, MPI_Comm*newcomm);

int MPI_Comm_dup_with_info(MPI_Comm comm, MPI_Info info, MPI_Comm *newcomm);


/**
 * Marks the communication object for deallocation.
 * @param comm communicator to be destroyed
 * @return MPI status
 */
int MPI_Comm_free(MPI_Comm *comm);

/**
 * Tests to see if a comm is an inter-communicator
 * @param comm communicator to test
 * @param flag true if this is an inter-communicator
 */
int MPI_Comm_test_inter(MPI_Comm comm, int*flag);

int MPI_Comm_set_name(MPI_Comm comm, char *comm_name);

int MPI_Comm_get_name(MPI_Comm comm, char *comm_name, int *resultlen);

int MPI_Comm_get_parent(MPI_Comm*parent);

int MPI_Comm_remote_group(MPI_Comm comm, MPI_Group*group);

int MPI_Comm_remote_size(MPI_Comm comm, int*size);

int MPI_Intercomm_create(MPI_Comm local_comm, int local_leader,
                         MPI_Comm peer_comm, int remote_leader,
                         int tag, MPI_Comm*newintercomm);

int MPI_Intercomm_merge(MPI_Comm intercomm, int high, MPI_Comm*newcomm);

/**
 * Compares two communicators
 */
int MPI_Comm_compare(MPI_Comm comm1, MPI_Comm comm2, int*result);


/** @}*/
/** @}*/

#endif /* NM_MPI_COMMUNICATOR_H */

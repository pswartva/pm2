#!/bin/bash

PADICO_HOSTS=$1
PADICO_NODE=$2
PADICO_HOST1=$( echo ${PADICO_HOSTS} | sed 's/,/\n/g' | head -n 1 )
PADICO_HOST2=$( echo ${PADICO_HOSTS} | sed 's/,/\n/g' | head -n 2 | tail -n 1 )
PADICO_PORT=$(( ${RANDOM} % 5000 + 20000 )) 

if [ "x1" = "x$3" ]; then 
    stcolor=$(tput sgr0) 
    errorcolor=$(tput setaf 1)
    passcolor=$(tput setaf 2)
fi

function success
{
    echo "${passcolor}TEST_SUCCESS${stcolor}"
}

function failed()
{
    if [ "x$1" == "x" ]; then
	echo "${errorcolor}TEST_FAILED${stcolor}"
    else
	echo "${errorcolor}TEST_FAILED${stcolor}: $1"
    fi
}

function testrc()
{
    if [ "x$1" != "x0" ]; then
	failed
	exit $1
    fi
}

function clean()
{
    killall padico-launch 2> /dev/null;
    killall sleep 2> /dev/null;
    echo
}
#! /bin/sh -e

echo "Initializing Puk..."

export M4PATH=../building-tools:./building-tools:${M4PATH}

echo "    generating ./configure [autoconf] ..."
${AUTOCONF:-autoconf} 
echo "    generating ./Puk/puk_config.h.in [autoheader] ..."
${AUTOHEADER:-autoheader} -f

echo "Done."

/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Front-end for PadicoConnector
 */

#ifndef PADICO_CONNECTOR_H
#define PADICO_CONNECTOR_H

#include <Padico/Puk.h>

PUK_MOD_AUTO_DEP(Connector, PADICO_LAST_MODULE_NAME);
#undef PADICO_LAST_MODULE_NAME
#define PADICO_LAST_MODULE_NAME Connector

#ifdef __cplusplus
extern "C" {
#endif

  padico_rc_t  padico_control_connector_new(const void**addr, size_t*addr_len);

  padico_rc_t  padico_control_connector_connect(const void*addr, size_t addr_len);

#ifdef __cplusplus
} /* extern "C" */
#endif


#endif /* PADICO_CONNECTOR_H */

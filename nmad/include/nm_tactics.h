/*
 * NewMadeleine
 * Copyright (C) 2006-2023 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#ifndef NM_TACTICS_H
#define NM_TACTICS_H

/** @ingroup nmad_private
 * @file
 * Tactics i.e. basic blocks used to build a strategy.
 */

/** checks whether req_chunk is a SHORT chunk for optimized protocol */
static inline int nm_tactic_req_is_short(const struct nm_req_chunk_s*p_req_chunk)
{
  return ( (p_req_chunk->chunk_offset == 0) &&
           (p_req_chunk->chunk_len < 255) &&
           (p_req_chunk->chunk_len == p_req_chunk->p_req->pack.len) &&
           (p_req_chunk->proto_flags == NM_PROTO_FLAG_LASTCHUNK) );
}

/** returns size in pw for the given short req_chunk */
static inline nm_len_t nm_tactic_req_short_size(const struct nm_req_chunk_s*p_req_chunk)
{
  assert(nm_tactic_req_is_short(p_req_chunk));
  return (NM_HEADER_SHORT_DATA_SIZE + p_req_chunk->chunk_len);
}

/** returns max size in pw for given data req_chunk-
 * value is a upper bound, actual size may be lower */
static inline nm_len_t nm_tactic_req_data_max_size(const struct nm_req_chunk_s*p_req_chunk)
{
  const nm_len_t chunk_len = p_req_chunk->chunk_len;
  /* compute maximum number of headers for this chunk;
   * only blocks larger than NM_DATA_IOV_THRESHOLD have their own header */
  const nm_len_t max_headers = p_req_chunk->chunk_props.size / NM_DATA_IOV_THRESHOLD;
  const nm_len_t max_header_blocks = (max_headers > p_req_chunk->chunk_props.blocks) ? p_req_chunk->chunk_props.blocks : max_headers;
  const nm_len_t max_header_len = NM_HEADER_DATA_SIZE + max_header_blocks * sizeof(struct nm_header_pkt_data_chunk_s) + NM_ALIGN_FRONTIER;
  return (max_header_len + chunk_len);
}

/** compute average block size */
static inline nm_len_t nm_tactic_req_data_density(const struct nm_req_chunk_s*p_req_chunk)
{
  const struct nm_req_s*p_pack = p_req_chunk->p_req;
  const struct nm_data_properties_s*p_props = nm_data_properties_get(&p_pack->data);
  const nm_len_t density = (p_props->blocks > 0) ? p_props->size / p_props->blocks : 0;
  return density;
}

/** Try to pack a control chunk in a pw.
 * @return NM_ESUCCESS for success, -NM_EBUSY if pw full
 * @note function takes ownership of p_ctrl_chunk
 */
static inline int nm_tactic_pack_ctrl(nm_gate_t p_gate, nm_drv_t p_drv  __attribute__((unused)),
                                      struct nm_ctrl_chunk_s*p_ctrl_chunk,
                                      struct nm_pkt_wrap_s*p_pw)
{
  if(NM_HEADER_CTRL_SIZE(&p_ctrl_chunk->ctrl) < nm_pw_remaining_buf(p_pw))
    {
      nm_pw_add_control(p_pw, &p_ctrl_chunk->ctrl);
      nm_ctrl_chunk_list_remove(&p_gate->ctrl_chunk_list, p_ctrl_chunk);
      nm_ctrl_chunk_free(&p_gate->p_core->ctrl_chunk_allocator, p_ctrl_chunk);
      return NM_ESUCCESS;
    }
  else
    {
      return -NM_EBUSY;
    }
}

int nm_tactic_pack_rdv(nm_gate_t p_gate, nm_drv_t p_drv,
                       struct nm_req_chunk_list_s*p_req_chunk_list,
                       struct nm_req_chunk_s*p_req_chunk,
                       struct nm_pkt_wrap_s*p_pw);

/** a chunk of rdv data.
 * Use one for regular rendezvous; use nb_driver chunks for multi-rail.
 */
struct nm_rdv_chunk_s
{
  nm_trk_id_t trk_id; /**< trk_id of track to send data on */
  nm_len_t len;       /**< length of data chunk */
};

void nm_tactic_rtr_pack(struct nm_core*p_core, struct nm_pkt_wrap_s*p_pw, int nb_chunks, const struct nm_rdv_chunk_s*p_chunks);

void nm_tactic_rdv_accept(struct nm_core*p_core, struct nm_pkt_wrap_s*p_pw);

#endif /* NM_TACTICS_H */

/* bench-CORBA-impl.cc
 */

#include <omniORB4/CORBA.h>
#include <omniORB4/Naming.hh>

#if 0
#undef MYCORBA_SHAREABLE

#ifdef MYCORBA_SHAREABLE
#define USE_PADICO_CORBA
#warning "Using Padico shared CORBA"
#else
#undef USE_PADICO_CORBA
#endif

#ifdef USE_PADICO_CORBA
#include <Padico/CORBA.h>
#include <Padico/COSNaming.h>
#else /* USE_PADICO_CORBA */
#include <myCORBA.h>
#include <myCOSNaming.h>
#endif

#endif

extern "C" {
#include "bench-common.h"
}
#include "bench-CORBA.h"
#ifdef MYCORBA_USE_SKEL
#include "bench-CORBA_skel.h"
#endif
#include <Padico/PadicoTM.h>
#include <Padico/Timing.h>
#include <Padico/Module.h>

#include <iostream>

int bench_corba_init(void);
int bench_corba_run(int argc, char**argv);
void bench_corba_finalize(void);

PADICO_MODULE_DECLARE(bench_CORBA, bench_corba_init, bench_corba_run, bench_corba_finalize);

static int corba_bench_finish = 0;

class bench_CORBA_impl : public POA_bench_CORBA
{
public:
  /* ** Send a sequence of octets */
  void SqO(const bench_CORBA::SeqOctet_t &msg)
  {
    static int i = 0;
    i++;
    /*
    if(msg.length() >= 512)
      {
        padico_print("SendSeqOctet() received size=%d i=%d\n", msg.length(), i);
      }
    */
  }
  /* ** Round-trip with a sequence of octets */
  void RO(bench_CORBA::SeqOctet_t &msg)
  {
    /*    static int i = 0;
          i++;
          if(msg.length() >= 512)
          {
          padico_print("RoundtripSeqOctet() received size=%d i=%d\n", msg.length(), i);
          }
    */
  }
  /* ** Latency test: empty method, no args */
  void LT(void)
  {
  }
  void Finish(void)
  {
    corba_bench_finish = 1;
  }
};

#ifdef USE_PADICO_CORBA
static Padico::ORB_ptr                orb;
#else
static CORBA::ORB_ptr                orb;
#endif
static bench_CORBA_impl*              bench_servant;
static bench_CORBA_var                bench_ref = NULL;
static PortableServer::POA_var        poa;
static PortableServer::POAManager_var poa_manager;
static CosNaming::NamingContext_var   naming_context;
static PortableServer::ObjectId_var   bench_server_oid;
static CosNaming::Name                bench_server_name;

int bench_corba_init(void)
{
  int argc = 1;
  padico_print("CORBA- ORB_init()\n");
#ifdef USE_PADICO_CORBA
  char*argv[] = { "bench-CORBA", NULL };
  orb = Padico::ORB_init(argc, argv);
#else  /* USE_PADICO_CORBA */
  const char*name_service = padico_getattr("NameService");
  if(!name_service)
    {
      padico_string_t nsref = padico_string_new();
      padico_string_printf(nsref, "corbaloc:iiop:%s/NameService", padico_na_host_getname(padico_na_node_gethost(bench_common_param.server_node)));
      name_service = padico_strdup(padico_string_get(nsref));
    }
  const char*default_argv[] = { "bench-CORBA", NULL };
  const char**argv = default_argv;
  padico_string_t name_ref = padico_string_new();
  argc = 3;
  argv = (const char**)padico_malloc((argc+1) * sizeof(const char*));
  padico_string_printf(name_ref, "NameService=%s", name_service);
  argv[0] = default_argv[0];
  argv[1] = "-ORBInitRef";
  argv[2] = padico_string_get(name_ref);
  argv[3] = NULL;
  char defaultsize[20];
  const char*giopMinMsgSize = "8192";
  sprintf(defaultsize, "%d", DEFAULT_MAX_LEN);
  const char*giopMaxMsgSize = padico_getattr("maxlen");
  if ( giopMaxMsgSize && atoi(giopMaxMsgSize) < 8192 ){
    giopMaxMsgSize=giopMinMsgSize;
  }
  const char* options[][2] = { { "giopMaxMsgSize", (giopMaxMsgSize)?giopMaxMsgSize:defaultsize}, { 0, 0 } };
  orb = CORBA::ORB_init(argc, argv, "omniORB4", options);

#endif /* USE_PADICO_CORBA */

  padico_print("CORBA- resolving NameService %s\n", name_service);
  try
    {
      CORBA::Object_var poa_ref = orb->resolve_initial_references ("RootPOA");
      poa = PortableServer::POA::_narrow (poa_ref);
      poa_manager = poa->the_POAManager();
      poa_manager->activate();

      CORBA::Object_var ns_ref =
        orb->resolve_initial_references ("NameService");
      naming_context = CosNaming::NamingContext::_narrow (ns_ref);
    }
  catch(...)
    {
      padico_warning("CORBA- error while resolving NameService.\n");
      return -1;
    }
  bench_server_name.length (1);
  bench_server_name[0].id = CORBA::string_dup("default");
  bench_server_name[0].kind = CORBA::string_dup("bench_CORBA");

  padico_print("CORBA- starting...\n");
  if(!bench_common_param.is_server)
    {
      /* *** servant */
      padico_print("CORBA- activating the servant...\n");
      padico_tm_sleep(2);
      try
        {
          bench_servant = new bench_CORBA_impl;
          bench_server_oid = poa->activate_object(bench_servant);
          naming_context->rebind(bench_server_name, bench_servant->_this());
          padico_print("CORBA- server ready [%s]\n", orb->object_to_string(bench_servant->_this()));
        }
      catch(...)
        {
          padico_warning("CORBA- error while registering server.\n");
          return -1;
        }
    }
  else
    {
      /* *** client */
      padico_print("CORBA- activating the client...\n");
      int retry = 0;
      const int maxretry = 10;
      for(retry = 0; retry < maxretry; retry++)
        {
          /* resolve */
          try
            {
              padico_tm_sleep(2); /* wait for the servant... */
              padico_print("CORBA- client resolving object (retry=%d)\n", retry);
              CORBA::Object_var server_obj = naming_context->resolve(bench_server_name);
              padico_print("CORBA- %s\n", orb->object_to_string(server_obj));
              bench_ref = bench_CORBA::_narrow(server_obj);
            }
          catch(...)
            {
              padico_warning("CORBA- cannot resolve object.\n");
              continue;
            }
          /* establish connection */
          try
            {
              padico_print("CORBA- client invoking request\n");
              bench_ref->LT(); /* first shot to establish the link-
                                * if it raises no exception, then the servant is ready */
              break; /* not pretty at all- it should definitely be a 'goto' :-) */
            }
          catch(CORBA::Exception&e)
            {
              padico_warning("CORBA- cannot establish connection to benchmark server. Retry.\n");
              continue;
            }
        }
      if(retry < maxretry)
        {
          try
            {
              const int latency_num_roundtrips = 1000;
              padico_print("CORBA- client performing latency benchmark (%d roundtrips), please wait...\n",
                           latency_num_roundtrips);
              padico_timing_t tick_start, tick_end;
              padico_timing_get_tick(&tick_start);
              for(int i=0; i<latency_num_roundtrips; i++)
                {
                  /*              padico_print("ping i=%d!\n", i);*/
                  bench_ref->LT();
                }
              padico_timing_get_tick(&tick_end);
              padico_print("CORBA- latency: %f microseconds\n",
                           padico_timing_diff_usec(&tick_start, &tick_end)/(2.0*latency_num_roundtrips));
              padico_print("CORBA- client ready to run\n");
            }
          catch(...)
            {
              padico_warning("CORBA- latency test failed.\n");
              return -1;
            }
        }
      else
        {
          padico_warning("CORBA- cannot establish connection to benchmark server. Giving up.\n");
          return -1;
        }
    }
  return 0;
}

void bench_corba_finalize(void)
{
  if(bench_common_param.is_server)
    {
      padico_print("CORBA- finalizing the servant...\n");
      try
        {
          poa->deactivate_object(bench_server_oid);
        }
      catch(const PortableServer::POA::ObjectNotActive &e)
        {
          padico_warning("CORBA- exception while deactivating object: object not active\n");
        }
      catch(const PortableServer::POA::WrongPolicy &e)
        {
          padico_warning("CORBA- exception while deactivating object: wrong policy\n");
        }
      catch(...)
        {
          padico_warning("CORBA- exception while deactivating object\n");
        }
      naming_context->unbind(bench_server_name);
      delete bench_servant;
    }

  if(!bench_common_param.is_server)
    {
      padico_print("CORBA- finalizing the client...\n");
      //   bench_ref->Finish();
    }
}

static void corba_server(char*buffer, size_t lenbuf)
{
  bench_CORBA::SeqOctet_t msg =
    bench_CORBA::SeqOctet_t(lenbuf, lenbuf, (CORBA::Octet*)buffer);
  try
    {
      bench_ref->RO(msg);
    }
  catch (CORBA::TRANSIENT const &)
    {
      padico_warning("### remote request failed for size=%d (TRANSIENT)\n",
                     lenbuf);
    }
  catch (CORBA::COMM_FAILURE const &e)
    {
      padico_warning("### remote request failed for size=%d (COMM_FAILURE COMPLETED=%d)\n",
                     lenbuf, e.completed());
    }
  catch (CORBA::SystemException const &e)
    {
      padico_warning("### remote request failed for size=%d (System Exception COMPLETED=%d)\n",
                     lenbuf, e.completed());
    }
  catch(...)
    {
      padico_warning("### remote request failed for size=%d (unknown exception)\n",
                     lenbuf);
    }
}

static void corba_client(char*buffer, size_t lenbuf)
{
  /*  orb->run(); */
}

struct bench_desc_s corba_bench =
{
  "CORBA",
  &corba_server,
  &corba_client
};

int bench_corba_run(int argc, char**argv)
{
  the_bench(&corba_bench);
  return 0;
}

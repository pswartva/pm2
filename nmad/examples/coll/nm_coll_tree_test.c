/*
 * NewMadeleine
 * Copyright (C) 2006-2022 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <math.h>
#include <stdio.h>
#include <nm_sendrecv_interface.h>
#include <nm_coll_interface.h>

static void nm_coll_tree_check(nm_coll_tree_kind_t kind, const char*name, int n0);
static void nm_coll_tree_draw(nm_coll_tree_kind_t kind, const char*name, int root, int n);


static void nm_coll_tree_display(nm_coll_tree_kind_t kind, const char*name, int root, int n, int with_weight)
{
  struct nm_coll_tree_info_s*trees = malloc(sizeof(struct nm_coll_tree_info_s) * n);
  int*children[n];
  int i;
  for(i = 0; i < n; i++)
    {
      nm_coll_tree_init(&trees[i], kind, n, i, root);
      children[i] = malloc(sizeof(int) * trees[i].max_arity);
    }
  printf("# %s (kind = %d); root = %d; n = %d; height = %d\n", name, kind, root, n, trees[0].height);
  int step;
  for(step = 0; step < trees[0].height; step++)
    {
      printf("step = %d / %d; kind = %s\n", step, trees[0].height, name);
      int parents[n];
      int n_children[n];
      for(i = 0; i < n; i++)
        {
          parents[i] = -1;
          nm_coll_tree_step(&trees[i], step, &parents[i], children[i], &n_children[i]);
        }
      for(i = 0; i < n; i++)
        {
          printf("  i = %3d; parent = %3d; ", i, parents[i]);
          int k;
          for(k = 0; k < n_children[i]; k++)
            {
              printf("child[%d] = %3d; ", k, children[i][k]);
              if(children[i][k] != -1)
                {
                  if(parents[children[i][k]] != i)
                    {
                      printf("\n\nERROR %d is child of %d, but parent of %d is %d\n", children[i][k], i, children[i][k], parents[children[i][k]]);
                      abort();
                    }
                }
            }
          if(parents[i] != -1)
            {
              int found_child = 0;
              for(k = 0; k < n_children[parents[i]]; k++)
                {
                  if(children[parents[i]][k] == i)
                    found_child = 1;
                }
              if(!found_child)
                {
                  printf("\n\nERROR %d is parent of %d, but %d is not child of %d\n", parents[i], i, i, parents[i]);
                }
            }
          printf("weight = %d\n", with_weight ? nm_coll_tree_weight(&trees[i], step, i) : -1);
        }
    }
  for(i = 0; i < n; i++)
    {
      free(children[i]);
    }
  printf("# %s; n = %d; done.\n\n\n", name, n);
  free(trees);

  nm_coll_tree_check(kind, name, n);
  nm_coll_tree_draw(kind, name, root, n);
}

static void nm_coll_tree_draw(nm_coll_tree_kind_t kind, const char*name, int root, int n)
{
  printf("# drawing tree = %s; root = %d; n = %d\n", name, root, n);
  struct nm_coll_tree_info_s*trees = malloc(sizeof(struct nm_coll_tree_info_s) * n);
  int*data = malloc(sizeof(int) * n);
  int i;
  for(i = 0; i < n; i++)
    {
      nm_coll_tree_init(&trees[i], kind, n, i, root);
      data[i] = 0;
    }
  data[root] = 1;
  int step;
  for(step = 0; step <= trees[0].height; step++)
    {
      printf("# step = %3d; ", step);
      for(i = 0; i < n; i++)
        {
          printf("%s", ((data[i] == 0) ? "." : "X"));
          if(step < trees[0].height)
            {
              int parent = -1;
              int children[trees[i].max_arity];
              int n_children = 0;
              nm_coll_tree_step(&trees[i], step, &parent, children, &n_children);
              if(parent != -1)
                {
                  data[i] = data[parent];
                }
            }
        }
      printf("\n");
    }
  free(data);
  free(trees);
}

static void nm_coll_tree_check(nm_coll_tree_kind_t kind, const char*name, int n0)
{
  struct nm_coll_tree_info_s*trees = malloc(sizeof(struct nm_coll_tree_info_s) * n0);
  int*data = malloc(sizeof(int) * n0);
  int n;
  for(n = 2; n <= n0; n++)
    {
      int root;
      for(root = 0; root < n; root++)
        {
          int i;
          for(i = 0; i < n; i++)
            {
              nm_coll_tree_init(&trees[i], kind, n, i, root);
              data[i] = 0;
            }
          data[root] = 1;
          int step;
          for(step = 0; step < trees[0].height; step++)
            {
              for(i = 0; i < n; i++)
                {
                  int children[trees[i].max_arity];
                  int parent;
                  int n_children;
                  parent = -1;
                  nm_coll_tree_step(&trees[i], step, &parent, children, &n_children);
                  if(parent != -1)
                    {
                      data[i] = data[parent];
                      if(data[parent] != 1)
                        {
                          printf("ERROR- tree = %s; n = %d; root = %d; step = %d; transmiting unitialized data from node %d to node %d\n",
                                 name, n, root, step, parent, i);
                        }
                    }
                }
            }
          for(i = 0; i < n; i++)
            {
              if(data[i] != 1)
                {
                  printf("ERROR- tree = %s; n = %d; root = %d; wrong data %d on node i = %d\n",
                         name, n, root, data[i], i);
                }
            }
        }
    }
  free(data);
  free(trees);
}

int main(int argc, char**argv)
{
  const int n = 63;
  const int root = 5;

  nm_coll_tree_display(NM_COLL_TREE_BINOMIAL, "binomial tree", root, n, 1);
  nm_coll_tree_display(NM_COLL_TREE_3NOMIAL, "3-nomial tree", root, n, 0);
  nm_coll_tree_display(NM_COLL_TREE_4NOMIAL, "4-nomial tree", root, n, 0);
  nm_coll_tree_display(NM_COLL_TREE_8NOMIAL, "8-nomial tree", root, n, 0);
  nm_coll_tree_display(NM_COLL_TREE_BINARY, "binary tree", root, n, 0);
  nm_coll_tree_display(NM_COLL_TREE_3ARY, "3-ary tree", root, n, 0);
  nm_coll_tree_display(NM_COLL_TREE_4ARY, "4-ary tree", root, n, 0);
  nm_coll_tree_display(NM_COLL_TREE_8ARY, "8-ary tree", root, n, 0);
  nm_coll_tree_display(NM_COLL_TREE_CHAIN, "chain", root, n, 0);
  nm_coll_tree_display(NM_COLL_TREE_CHAIN_MODIFIED, "chain-modified", root, n, 0);
  nm_coll_tree_display(NM_COLL_TREE_2CHAINS_MODIFIED, "2chains-modified", root, n, 0);
  nm_coll_tree_display(NM_COLL_TREE_LADDER, "ladder", root, n, 0);
  nm_coll_tree_display(NM_COLL_TREE_FLAT, "flat", root, n, 0);

  return 0;
}

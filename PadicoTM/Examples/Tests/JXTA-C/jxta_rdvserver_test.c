/*
 * Copyright (c) 2004 Sun Microsystems, Inc.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:
 *       "This product includes software developed by the
 *       Sun Microsystems, Inc. for Project JXTA."
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names "Sun", "Sun Microsystems, Inc.", "JXTA" and "Project JXTA" must
 *    not be used to endorse or promote products derived from this
 *    software without prior written permission. For written
 *    permission, please contact Project JXTA at http://www.jxta.org.
 *
 * 5. Products derived from this software may not be called "JXTA",
 *    nor may "JXTA" appear in their name, without prior written
 *    permission of Sun.
 *
 * THIS SOFTWARE IS PROVIDED AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL SUN MICROSYSTEMS OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of Project JXTA.  For more
 * information on Project JXTA, please see
 * <http://www.jxta.org/>.
 *
 * This license is based on the BSD license adopted by the Apache Foundation.
 *
 * $Id: jxta_rdvserver_test.c,v 1.5 2005/11/16 20:10:44 lankes Exp $
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include <apr_general.h>
#include <apr_time.h>

#include "jxta.h"
#include "jxta_debug.h"
#include "jxta_peer.h"
#include "jxta_peergroup.h"
#include "jxta_listener.h"
#include "jxta_endpoint_service.h"
#include "jxta_rdv_service.h"
#include "jxta_peerview.h"
#include "jxta_platformconfig.h"

#define DEBUG 1
#define LOCAL_RESULTS 1

static int sleep_time = 0;
static int max_nb_of_edges = 0;
static int current_nb_of_edges = 0;

static int current_nb_of_rdvs = 0;

#if DEBUG
static Jxta_log_file *log_file;
static Jxta_log_selector *log_selector;
#endif

static apr_time_t start_time;

static FILE *gnuplot_file = NULL;
static const char *RESULT_GNUPLOT_FILENAME = "results.rdv.dat";

static void JXTA_STDCALL rdvserver_peerview_listener(Jxta_object * obj, void *arg)
{
    JString *string = NULL;
    const char *string_char = NULL;
    Jxta_peerview_event *pv_event = (Jxta_peerview_event *) obj;

    apr_time_t current_time = apr_time_now();

    assert(jxta_id_to_jstring(pv_event->pid, &string) == JXTA_SUCCESS);
    string_char = jstring_get_string(string);

    switch (pv_event->event) {
    case JXTA_PEERVIEW_ADD:
      current_nb_of_rdvs++;
      fprintf(gnuplot_file,"PA\t%s\t%f\t%i\n", string_char, (float) (current_time - start_time) / 1000.0, current_nb_of_rdvs);
#if DEBUG
      printf("The following rdv peer has been added=%s. Currently %i are known to us\n", string_char, current_nb_of_rdvs);
#endif
      break;
    case JXTA_PEERVIEW_REMOVE:
      current_nb_of_rdvs--;
      fprintf(gnuplot_file,"PR\t%s\t%f\t%i\n", string_char, (float) (current_time - start_time) / 1000.0, current_nb_of_rdvs);
#if DEBUG
      printf("The following rdv peer has been removed=%s. Currently %i are known to us\n", string_char, current_nb_of_rdvs);
#endif
      break;
    }

    JXTA_OBJECT_RELEASE(string);
}

static void JXTA_STDCALL rdvserver_rdv_listener(Jxta_object * obj, void *arg)
{
    Jxta_status res = 0;
    JString *string = NULL;
    Jxta_rdv_event *rdv_event = (Jxta_rdv_event *) obj;

    apr_time_t current_time = apr_time_now();

    /** Get info from the event */
    int type = rdv_event->event;
    res = jxta_id_to_jstring(rdv_event->pid, &string);

    switch (type) {
    case JXTA_RDV_CLIENT_CONNECTED:
        current_nb_of_edges++;
        fprintf(gnuplot_file,"C\t%s\t%f\t%i\n", jstring_get_string(string), (float) (current_time - start_time) / 1000.0, current_nb_of_edges);
#if DEBUG
        printf("The following edge peer has just connected=%s. Currently %i are connected to us\n", jstring_get_string(string),
               current_nb_of_edges);
#endif
        break;
    case JXTA_RDV_CLIENT_RECONNECTED:
        fprintf(gnuplot_file,"R\t%s\t%f\t%i\n", jstring_get_string(string), (float) (current_time - start_time) / 1000.0, current_nb_of_edges);
#if DEBUG
        printf("The following edge peer has just reconnected=%s. Currently %i are connected to us\n", jstring_get_string(string),
               current_nb_of_edges);
#endif
        break;
    case JXTA_RDV_CLIENT_DISCONNECTED:
        current_nb_of_edges--;
        fprintf(gnuplot_file,"D\t%s\t%f\t%i\n", jstring_get_string(string), (float) (current_time - start_time) / 1000.0, current_nb_of_edges);
#if DEBUG
        printf("The following edge peer has just disconnected=%s. Currently %i are connected to us\n", jstring_get_string(string),
               current_nb_of_edges);
#endif
        break;
    case JXTA_RDV_BECAME_RDV:
        current_nb_of_edges = 0;
#if DEBUG
        printf("We are now a rdv peer=%s. Currently %i are connected to us\n", jstring_get_string(string), current_nb_of_edges);
#endif
        break;
#if DEBUG
    default:
        printf("Event not catched = %d\n", type);
#endif
    }

    if (current_nb_of_edges >= max_nb_of_edges) {
#if DEBUG
        printf("Wait %i seconds so that JXTA become idle\n", sleep_time);
#endif
        apr_sleep(sleep_time * 1000 * 1000);

        fprintf(gnuplot_file, "The test successfully ended. We had %i edge peers connected to us!\n", current_nb_of_edges);
        fclose(gnuplot_file);

        #if DEBUG
        printf("The test successfully ended. We had %i edge peers connected to us!\n", current_nb_of_edges);
        jxta_log_file_attach_selector(log_file, NULL, &log_selector);
        jxta_log_selector_delete(log_selector);
        jxta_log_file_close(log_file);
        #endif

        jxta_terminate();
    }
}

int jxta_rdvserver_run(int argc, char **argv)
{
    Jxta_PG *pg = NULL;
    Jxta_id *pid = NULL;
    JString *pid_jstring = NULL;
    Jxta_rdv_service *rdv = NULL;
    Jxta_peerview *peerview = NULL;
    Jxta_listener *rdv_listener = NULL;
    Jxta_listener *peerview_listener = NULL;
    Jxta_status res = JXTA_SUCCESS;
    int wait_before_start = 0;

    if (argc != 4) {
        printf("Syntax: %s [nb_of_edges] [wait_before_start in sec] [sleep_time in sec]\n", argv[0]);
        exit(0);
    }

    max_nb_of_edges = atoi(argv[1]);
    wait_before_start = atoi(argv[2]);
    sleep_time = atoi(argv[3]);

  /** Start log */
#if DEBUG
    log_selector = jxta_log_selector_new_and_set("*.*", &res);
    if (NULL == log_selector || res != JXTA_SUCCESS) {
        fprintf(stderr, "# Failed to init JXTA log selector.\n");
        return JXTA_FAILED;
    }
    jxta_log_file_open(&log_file, "jxta.log");
    jxta_log_using(jxta_log_file_append, log_file);
    jxta_log_file_attach_selector(log_file, log_selector, NULL);
#endif

#if LOCAL_RESULTS
    gnuplot_file = fopen(RESULT_GNUPLOT_FILENAME, "w");
#else
    {
      char *file_name = NULL;
      char *file_log_name = NULL;
      char *value_home = NULL;
      Jxta_PA * peer_adv = NULL;
      JString * peer_name_jstring = NULL;
      char * peer_name = NULL;

      value_home = getenv("HOME");
      /** filename set to the name of the peer */
      peer_adv = jxta_PlatformConfig_read("PlatformConfig");
      peer_name_jstring = jxta_PA_get_Name(peer_adv);
      peer_name = jstring_get_string(peer_name_jstring);
      file_name = calloc(strlen(value_home) + 15 + strlen(peer_name) + 1, sizeof(char));
      strcpy(file_name, value_home);
      strcat(file_name, "/adage_results/");
      strcat(file_name, peer_name);
      printf("%s\n", file_name);
      gnuplot_file = fopen(file_name, "w");
      assert(gnuplot_file != NULL);

      JXTA_OBJECT_RELEASE(peer_adv);
      JXTA_OBJECT_RELEASE(peer_name_jstring);

      file_log_name = calloc(strlen(file_name) + 5, sizeof(char));
      strcpy(file_log_name, file_name);
      strcat(file_log_name, ".log");
      free(file_name);

      /** Start log */
      #if DEBUG
      log_selector = jxta_log_selector_new_and_set("*.*", &res);
      if (NULL == log_selector || res != JXTA_SUCCESS) {
        fprintf(stderr, "# Failed to init JXTA log selector.\n");
        return -1;
      }
      jxta_log_file_open(&log_file, file_log_name);
      jxta_log_using(jxta_log_file_append, log_file);
      jxta_log_file_attach_selector(log_file, log_selector, NULL);
      #endif
    }
#endif

#if DEBUG
    printf("wait for %i sec before launching the peer\n", wait_before_start);
#endif
    if (wait_before_start > 0)
      apr_sleep(wait_before_start * 1000 * 1000);

    /** Start JXTA */
    start_time = apr_time_now();
    assert(jxta_PG_new_netpg(&pg) == JXTA_SUCCESS);
    jxta_PG_get_PID(pg, &pid);
    jxta_id_to_jstring(pid, &pid_jstring);
    JXTA_OBJECT_RELEASE(pid);
    fprintf(gnuplot_file, "S\t%s\t%f\n", jstring_get_string(pid_jstring), (float) start_time);
    JXTA_OBJECT_RELEASE(pid_jstring);

    /** Get useful services */
    jxta_PG_get_rendezvous_service(pg, &rdv);

    /** Add a listener to rdv events */
    rdv_listener = jxta_listener_new((Jxta_listener_func) rdvserver_rdv_listener, NULL, 1, 1);
    if (rdv_listener != NULL) {
        jxta_listener_start(rdv_listener);
        assert(jxta_rdv_service_add_event_listener(rdv, (char *) "test", (char *) "rdv", rdv_listener) == JXTA_SUCCESS);
    }

    /** Add a listener for peerview events */
    peerview_listener = jxta_listener_new((Jxta_listener_func) rdvserver_peerview_listener, NULL, 1, 1);
    if (peerview_listener != NULL) {
      jxta_listener_start(peerview_listener);
      peerview = jxta_rdv_service_get_peerview(rdv);
      assert(jxta_peerview_add_event_listener(peerview,
                                              (char *) "test",
                                              (char *) "peerview",
                                              peerview_listener) == JXTA_SUCCESS);
      JXTA_OBJECT_RELEASE(peerview);
    }

    JXTA_OBJECT_RELEASE(rdv);

#if DEBUG
    fprintf(stderr, "wait 1 hour for edge peers\n");
#endif
    apr_sleep(APR_USEC_PER_SEC * sleep_time);

    return 0;
}


int main(int argc, char **argv)
{
    int rv;

    jxta_initialize();
    rv = jxta_rdvserver_run(argc, argv);
    /** Never reached */
    return rv;
}

/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file ns-besteffort.c
 * @brief NetSelector with 'besteffort' strategy.
 *        Ought to manage most cases automatically from topology description.
 * @ingroup NetSelector-besteffort
 */

#include <stdlib.h>
#include <fnmatch.h>
#include <arpa/inet.h>

#include <Padico/Puk.h>
#include <Padico/Topology.h>
#include <Padico/NetSelector.h>
#include <Padico/PadicoTM.h>
#include <Padico/Module.h>

static int ns_besteffort_init(void);
static void ns_besteffort_finalize(void);

PADICO_MODULE_DECLARE(NetSelector_besteffort, ns_besteffort_init, NULL, ns_besteffort_finalize,
                      "Topology", "NetSelector");

PADICO_MODULE_ATTR(multinic_policy, "PADICO_MULTINIC_POLICY", "policy when multiple NICs are available: auto (let the driver choose), first, last, roundrobin (default)", string, "roundrobin");

/** default value for netselector list */
#define PADICO_NETSELECTOR_LIST_DEFAULT "self,shm,psm2,psm,ibverbs,portals4,cxi,tcp,control"

/** string that contains valid tokens for netselector list; used in help message */
#define PADICO_NETSELECTOR_LIST_VALID_TOKENS "self,shm,cma,psm2,psm,ibverbs,ibsrq,ibbuf,ibrcache,iblr2,portals4,cxi,tcp,local,control,defaults"

/** list of valid tokens for netselector list */
static const char*nsbe_valid_tokens[] = { "self", "shm", "cma", "psm2", "psm", "ibverbs", "ibsrq", "ibbuf", "ibrcache", "iblr2", "portals4", "cxi", "tcp", "local", "control" };

PADICO_MODULE_ATTR(netselector_list, "PADICO_NETSELECTOR_LIST", "list of networks the netselector will consider for driver selection; default = " PADICO_NETSELECTOR_LIST_DEFAULT, string, PADICO_NETSELECTOR_LIST_DEFAULT);

__PUK_SYM_INTERNAL
int ns_besteffort_selector_dijkstra(padico_string_t hosts, padico_string_t users,
                                    padico_topo_host_t dest, padico_topo_network_t*dest_n);


/* ********************************************************* */

/** a resolved rule for a given network; context may have been tuned for network */
struct nsbe_rule_resolved_s
{
  struct nsbe_rule_resolved_key_s
  {
    struct nsbe_rule_bucket_s*rule_bucket;
    padico_topo_network_t network;
  } key;
  puk_component_t component;
};

struct nsbe_rule_bucket_s
{
  const char*profile;         /**< NetSelector profile, NULL for any */
  const char*network_pattern; /**< regular expression for network name */
  const char*symbolic;        /**< component as symbolic expression */
};
PUK_VECT_TYPE(nsbe_rule, struct nsbe_rule_bucket_s);

static struct
{
  puk_hashtable_t rules; /**< hash: iface -> rule_vect  */
  puk_hashtable_t resolved; /**< hash: rule_bucket+network -> resolved component */
  marcel_mutex_t lock;
} nsbe;

/* ********************************************************* */
/* Assemblies library */

/* ** 'PadicoControl' */

__attribute__((unused))
 static const char assembly_control_sysio[] =
"<puk:composite id=\"nsbe:control_sysio\">"
"  <puk:component id=\"0\" name=\"SocketFactory_Plain\"/>"
"  <puk:component id=\"1\" name=\"Control_SysIO\">"
"    <puk:uses iface=\"SocketFactory\" provider-id=\"0\" />"
"  </puk:component>"
"  <puk:entry-point iface=\"PadicoControl\"   provider-id=\"1\" />"
"  <puk:entry-point iface=\"PadicoBootstrap\" provider-id=\"1\" />"
"  <puk:entry-point iface=\"PadicoConnector\" provider-id=\"1\" />"
"</puk:composite>";

/* ** 'PadicoConnector' */

__attribute__((unused))
static const char assembly_control_minidriver_ibverbs[] =
  "<puk:composite id=\"nsbe:control_minidriver\">"
  "  <puk:component id=\"0\" name=\"NewMad_ibverbs_common\"/>"
  "  <puk:component id=\"1\" name=\"NewMad_ibverbs_lr2\">"
  "    <puk:uses iface=\"NewMad_ibverbs\" provider-id=\"0\"/>"
  "  </puk:component>"
  "  <puk:component id=\"2\" name=\"Control_minidriver\">"
  "    <puk:uses iface=\"NewMad_minidriver\" provider-id=\"1\" />"
  "  </puk:component>"
  "  <puk:entry-point iface=\"PadicoControl\"   provider-id=\"2\" />"
  "  <puk:entry-point iface=\"PadicoConnector\" provider-id=\"2\" />"
  "</puk:composite>";

__attribute__((unused))
static const char assembly_control_minidriver_psm[] =
  "<puk:composite id=\"nsbe:control_minidriver\">"
  "  <puk:component id=\"1\" name=\"Minidriver_psm\" />"
  "  <puk:component id=\"2\" name=\"Control_minidriver\">"
  "    <puk:uses iface=\"NewMad_minidriver\" provider-id=\"1\" />"
  "  </puk:component>"
  "  <puk:entry-point iface=\"PadicoControl\"   provider-id=\"2\" />"
  "  <puk:entry-point iface=\"PadicoConnector\" provider-id=\"2\" />"
  "</puk:composite>";

__attribute__((unused))
static const char assembly_control_minidriver_psm2[] =
  "<puk:composite id=\"nsbe:control_minidriver\">"
  "  <puk:component id=\"1\" name=\"Minidriver_psm2\" />"
  "  <puk:component id=\"2\" name=\"Control_minidriver\">"
  "    <puk:uses iface=\"NewMad_minidriver\" provider-id=\"1\" />"
  "  </puk:component>"
  "  <puk:entry-point iface=\"PadicoControl\"   provider-id=\"2\" />"
  "  <puk:entry-point iface=\"PadicoConnector\" provider-id=\"2\" />"
  "</puk:composite>";

__attribute__((unused))
static const char assembly_connector_sysio[] =
"<puk:composite id=\"nsbe:connector_sysio\">"
"  <puk:component id=\"0\" name=\"SocketFactory_Plain\"/>"
"  <puk:component id=\"1\" name=\"Control_SysIO\">"
"    <puk:uses iface=\"SocketFactory\" provider-id=\"0\" />"
"  </puk:component>"
"  <puk:component id=\"2\" name=\"SecondaryConnector\">"
"    <puk:uses iface=\"PadicoBootstrap\" provider-id=\"1\" />"
"  </puk:component>"
"  <puk:entry-point iface=\"PadicoControl\"   provider-id=\"1\" />"
"  <puk:entry-point iface=\"PadicoConnector\" provider-id=\"2\" />"
"</puk:composite>";

/* ** 'VLink' */
__attribute__((unused))
static const char assembly_vlink_sfinet[] =
"<puk:composite id=\"nsbe:vlink_sfinet\">"
"  <puk:component id=\"0\" name=\"SocketFactory_Plain\"/>"
"  <puk:component id=\"1\" name=\"VLink_SocketFactory\">"
"    <puk:uses iface=\"SocketFactory\" provider-id=\"0\" />"
"  </puk:component>"
"  <puk:entry-point iface=\"VLink\" provider-id=\"1\"/>"
"</puk:composite>";
__attribute__((unused))
static const char assembly_vlink_infiniband[] =
"<puk:composite id=\"nsbe:vlink_infiniband\">"
"  <puk:component id=\"0\" name=\"InfinibandVerbs\"/>"
"  <puk:component id=\"1\" name=\"VLink_Packet\">"
"    <puk:uses iface=\"PadicoSimplePackets\" provider-id=\"0\" />"
"  </puk:component>"
"  <puk:entry-point iface=\"VLink\" provider-id=\"1\"/>"
"</puk:composite>";
__attribute__((unused))
static const char assembly_vlink_shm[] =
"<puk:composite id=\"nsbe:vlink_shm\">"
"  <puk:component id=\"0\" name=\"PSP_Shm\"/>"
"  <puk:component id=\"1\" name=\"VLink_Packet\">"
"    <puk:uses iface=\"PadicoSimplePackets\" provider-id=\"0\" />"
"  </puk:component>"
"  <puk:entry-point iface=\"VLink\" provider-id=\"1\"/>"
"</puk:composite>";
__attribute__((unused))
static const char assembly_vlink_control[] =
"<puk:composite id=\"nsbe:vlink_control\">"
"  <puk:component id=\"0\" name=\"PSP_Control\"/>"
"  <puk:component id=\"1\" name=\"VLink_Packet\">"
"    <puk:uses iface=\"PadicoSimplePackets\" provider-id=\"0\" />"
"  </puk:component>"
"  <puk:entry-point iface=\"VLink\" provider-id=\"1\"/>"
"</puk:composite>";
__attribute__((unused))
static const char assembly_vlink_packet_sfinet[] =
"<puk:composite id=\"nsbe:vlink_packet_sfinet\">"
"  <puk:component id=\"0\" name=\"SocketFactory_Plain\"/>"
"  <puk:component id=\"1\" name=\"PSP_SocketFactory\">"
"    <puk:uses iface=\"SocketFactory\" provider-id=\"0\" />"
"  </puk:component>"
"  <puk:component id=\"2\" name=\"VLink_Packet\">"
"    <puk:uses iface=\"PadicoSimplePackets\" provider-id=\"1\" />"
"  </puk:component>"
"  <puk:entry-point iface=\"VLink\" provider-id=\"2\"/>"
"</puk:composite>";

/* ** 'PadicoSimplePackets' */
__attribute__((unused))
static const char assembly_psp_shm[] =
"<puk:composite ref=\"PSP_Shm\"/>";
__attribute__((unused))
static const char assembly_psp_infiniband[] =
"<puk:composite ref=\"InfinibandVerbs\"/>";
__attribute__((unused))
static const char assembly_psp_sf[] =
"<puk:composite id=\"nsbe:psp_sf\">"
"  <puk:component id=\"0\" name=\"SocketFactory_Plain\"/>"
"  <puk:component id=\"1\" name=\"PSP_SocketFactory\">"
"    <puk:uses iface=\"SocketFactory\" provider-id=\"0\" />"
"  </puk:component>"
"  <puk:entry-point iface=\"PadicoSimplePackets\" provider-id=\"1\"/>"
"</puk:composite>";
__attribute__((unused))
static const char assembly_psp_control[] =
"<puk:composite ref=\"PSP_Control\"/>";

/* ** 'NewMad_Driver' */
__attribute__((unused))
static const char assembly_newmadico_shm[] =
"<puk:composite id=\"nsbe:nmad_shm\">"
"  <puk:component id=\"0\" name=\"PSP_Shm\"/>"
"  <puk:component id=\"1\" name=\"NewMadico\">"
"    <puk:uses iface=\"PadicoSimplePackets\" provider-id=\"0\" />"
"  </puk:component>"
"  <puk:entry-point iface=\"NewMad_Driver\" provider-id=\"1\"/>"
"</puk:composite>";
__attribute__((unused))
static const char assembly_newmadico_infiniband[] =
"<puk:composite id=\"nsbe:newmadico_infiniband\">"
"  <puk:component id=\"0\" name=\"InfinibandVerbs\"/>"
"  <puk:component id=\"1\" name=\"NewMadico\">"
"    <puk:uses iface=\"PadicoSimplePackets\" provider-id=\"0\" />"
"  </puk:component>"
"  <puk:entry-point iface=\"NewMad_Driver\" provider-id=\"1\" />"
"</puk:composite>";
__attribute__((unused))
static const char assembly_nmad_sf[] =
"<puk:composite id=\"nsbe:nmad_sf\">"
"  <puk:component id=\"0\" name=\"SocketFactory_Plain\"/>"
"  <puk:component id=\"1\" name=\"PSP_SocketFactory\">"
"    <puk:uses iface=\"SocketFactory\" provider-id=\"0\" />"
"  </puk:component>"
"  <puk:component id=\"2\" name=\"NewMadico\">"
"    <puk:uses iface=\"PadicoSimplePackets\" provider-id=\"1\" />"
"  </puk:component>"
"  <puk:entry-point iface=\"NewMad_Driver\" provider-id=\"2\" />"
"</puk:composite>";
__attribute__((unused))
static const char assembly_nmad_control[] =
"<puk:composite id=\"nsbe:nmad_control\">"
"  <puk:component id=\"0\" name=\"PSP_Control\"/>"
"  <puk:component id=\"1\" name=\"NewMadico\">"
"    <puk:uses iface=\"PadicoSimplePackets\" provider-id=\"0\" />"
"  </puk:component>"
"  <puk:entry-point iface=\"NewMad_Driver\" provider-id=\"1\" />"
"</puk:composite>";
__attribute__((unused))
static const char assembly_nmad_tcp[] =
  "<puk:composite id=\"nsbe:nmminitcp\">"
  "  <puk:component id=\"0\" name=\"Minidriver_tcp\"/>"
  "  <puk:component id=\"1\" name=\"Minidriver_tcp\"/>"
  "  <puk:component id=\"2\" name=\"NewMad_Driver_minidriver\">"
  "    <puk:uses iface=\"NewMad_minidriver\" port=\"trk0\" provider-id=\"0\" />"
  "    <puk:uses iface=\"NewMad_minidriver\" port=\"trk1\" provider-id=\"1\" />"
  "  </puk:component>"
  "  <puk:entry-point iface=\"NewMad_Driver\" provider-id=\"2\" />"
  "</puk:composite>";
__attribute__((unused))
static const char assembly_nmad_infiniband[] =
  "<puk:composite id=\"nsbe:nm-ib-lr2\">"
  "  <puk:component id=\"0\" name=\"NewMad_ibverbs_bycopy\"/>"
  "  <puk:component id=\"1\" name=\"NewMad_ibverbs_lr2\"/>"
  "  <puk:component id=\"2\" name=\"NewMad_Driver_minidriver\">"
  "    <puk:uses iface=\"NewMad_minidriver\" port=\"trk0\" provider-id=\"0\" />"
  "    <puk:uses iface=\"NewMad_minidriver\" port=\"trk1\" provider-id=\"1\" />"
  "  </puk:component>"
  "  <puk:entry-point iface=\"NewMad_Driver\" provider-id=\"2\" />"
  "</puk:composite>";

__attribute__((unused))
static const char assembly_nmad_mx[] =
"<puk:composite id=\"nsbe:nmad_mx\">"
"  <puk:component id=\"0\" name=\"NewMad_Driver_mx\"/>"
"  <puk:entry-point iface=\"NewMad_Driver\" provider-id=\"0\" />"
"</puk:composite>";
__attribute__((unused))
static const char assembly_nmad_minilocal[] =
  "<puk:composite id=\"nsbe:nmminilocal\">"
  "  <puk:component id=\"0\" name=\"Minidriver_local\"/>"
  "  <puk:component id=\"1\" name=\"Minidriver_local\"/>"
  "  <puk:component id=\"2\" name=\"NewMad_Driver_minidriver\">"
  "    <puk:uses iface=\"NewMad_minidriver\" port=\"trk0\" provider-id=\"0\" />"
  "    <puk:uses iface=\"NewMad_minidriver\" port=\"trk1\" provider-id=\"1\" />"
  "  </puk:component>"
  "  <puk:entry-point iface=\"NewMad_Driver\" provider-id=\"2\" />"
  "</puk:composite>";
__attribute__((unused))
static const char assembly_nmad_shm[] =
  "<puk:composite ref=\"NewMad_Driver_shm\"/>";
__attribute__((unused))
static const char assembly_nmad_minishmcma[] =
  "<puk:composite id=\"nsbe:nmminishm\">"
  "  <puk:component id=\"0\" name=\"Minidriver_shm\"/>"
  "  <puk:component id=\"1\" name=\"Minidriver_CMA\"/>"
  "  <puk:component id=\"2\" name=\"NewMad_Driver_minidriver\">"
  "    <puk:uses iface=\"NewMad_minidriver\" port=\"trk0\" provider-id=\"0\" />"
  "    <puk:uses iface=\"NewMad_minidriver\" port=\"trk1\" provider-id=\"1\" />"
  "  </puk:component>"
  "  <puk:entry-point iface=\"NewMad_Driver\" provider-id=\"2\" />"
  "</puk:composite>";
__attribute__((unused))
static const char assembly_nmad_minishm[] =
  "<puk:composite id=\"nsbe:nmminishm\">"
  "  <puk:component id=\"0\" name=\"Minidriver_shm\"/>"
  "  <puk:component id=\"1\" name=\"Minidriver_largeshm\"/>"
  "  <puk:component id=\"2\" name=\"NewMad_Driver_minidriver\">"
  "    <puk:uses iface=\"NewMad_minidriver\" port=\"trk0\" provider-id=\"0\" />"
  "    <puk:uses iface=\"NewMad_minidriver\" port=\"trk1\" provider-id=\"1\" />"
  "  </puk:component>"
  "  <puk:entry-point iface=\"NewMad_Driver\" provider-id=\"2\" />"
  "</puk:composite>";

__attribute__((unused))
static const char assembly_nmad_self[] =
  "<puk:composite id=\"nsbe:nmminiself\">"
  "  <puk:component id=\"0\" name=\"Minidriver_self\"/>"
  "  <puk:component id=\"1\" name=\"Minidriver_self\"/>"
  "  <puk:component id=\"2\" name=\"NewMad_Driver_minidriver\">"
  "    <puk:uses iface=\"NewMad_minidriver\" port=\"trk0\" provider-id=\"0\" />"
  "    <puk:uses iface=\"NewMad_minidriver\" port=\"trk1\" provider-id=\"1\" />"
  "  </puk:component>"
  "  <puk:entry-point iface=\"NewMad_Driver\" provider-id=\"2\" />"
  "</puk:composite>";

__attribute__((unused))
static const char assembly_minidriver_self[] =
  "<puk:composite id=\"nsbe:minidriver_self\">"
  "  <puk:component id=\"0\" name=\"Minidriver_self\"/>"
  "  <puk:entry-point iface=\"NewMad_minidriver\" port=\"minidriver\" provider-id=\"0\" />"
  "</puk:composite>";
__attribute__((unused))
static const char assembly_minidriver_selfbuf[] =
  "<puk:composite id=\"nsbe:minidriver_self_buf\">"
  "  <puk:component id=\"0\" name=\"Minidriver_self_buf\"/>"
  "  <puk:entry-point iface=\"NewMad_minidriver\" port=\"minidriver\" provider-id=\"0\" />"
  "</puk:composite>";

__attribute__((unused))
static const char assembly_minidriver_tcp[] =
  "<puk:composite id=\"nsbe:minidriver_tcp\">"
  "  <puk:component id=\"0\" name=\"Minidriver_tcp\"/>"
  "  <puk:entry-point iface=\"NewMad_minidriver\" port=\"minidriver\" provider-id=\"0\" />"
  "</puk:composite>";

__attribute__((unused))
static const char assembly_pspmini_infiniband[] =
  "<puk:composite id=\"nsbe:pspmini_infiniband\">"
  "  <puk:component id=\"0\" name=\"NewMad_ibverbs_bycopy\"/>"
  "  <puk:component id=\"1\" name=\"NewMad_ibverbs_lr2\"/>"
  "  <puk:component id=\"2\" name=\"PSP_minidriver\">"
  "    <puk:uses iface=\"NewMad_minidriver\" port=\"trk0\" provider-id=\"0\" />"
  "    <puk:uses iface=\"NewMad_minidriver\" port=\"trk1\" provider-id=\"1\" />"
  "  </puk:component>"
  "  <puk:entry-point iface=\"PadicoSimplePackets\" provider-id=\"2\" />"
  "</puk:composite>";

__attribute__((unused))
static const char assembly_vlink_miniib[] =
  "<puk:composite id=\"nsbe:vlink_miniib\">"
  "  <puk:component id=\"0\" name=\"NewMad_ibverbs_bycopy\"/>"
  "  <puk:component id=\"1\" name=\"NewMad_ibverbs_lr2\"/>"
  "  <puk:component id=\"2\" name=\"PSP_minidriver\">"
  "    <puk:uses iface=\"NewMad_minidriver\" port=\"trk0\" provider-id=\"0\" />"
  "    <puk:uses iface=\"NewMad_minidriver\" port=\"trk1\" provider-id=\"1\" />"
  "  </puk:component>"
  "  <puk:component id=\"3\" name=\"VLink_Packet\">"
  "    <puk:uses iface=\"PadicoSimplePackets\" provider-id=\"2\" />"
  "  </puk:component>"
  "  <puk:entry-point iface=\"VLink\" provider-id=\"3\"/>"
  "</puk:composite>";

__attribute__((unused))
static const char assembly_minidriver_ib_bybuf[] =
  "<puk:composite id=\"nsbe:minidriver_ibverbs_bybuf\">"
  "  <puk:component id=\"0\" name=\"NewMad_ibverbs_common\"/>"
  "  <puk:component id=\"1\" name=\"NewMad_ibverbs_bybuf\">"
  "    <puk:uses iface=\"NewMad_ibverbs\" provider-id=\"0\"/>"
  "  </puk:component>"
  "  <puk:entry-point iface=\"NewMad_minidriver\" port=\"minidriver\" provider-id=\"1\" />"
  "</puk:composite>";
__attribute__((unused))
static const char assembly_minidriver_ib_lr2[] =
  "<puk:composite id=\"nsbe:minidriver_ibverbs_lr2\">"
  "  <puk:component id=\"0\" name=\"NewMad_ibverbs_common\"/>"
  "  <puk:component id=\"1\" name=\"NewMad_ibverbs_lr2\">"
  "    <puk:uses iface=\"NewMad_ibverbs\" provider-id=\"0\"/>"
  "  </puk:component>"
  "  <puk:entry-point iface=\"NewMad_minidriver\" port=\"minidriver\" provider-id=\"1\" />"
  "</puk:composite>";
__attribute__((unused))
static const char assembly_minidriver_ib_rcache[] =
  "<puk:composite id=\"nsbe:minidriver_ibverbs_rcache\">"
  "  <puk:component id=\"0\" name=\"NewMad_ibverbs_common\"/>"
  "  <puk:component id=\"1\" name=\"NewMad_ibverbs_rcache\">"
  "    <puk:uses iface=\"NewMad_ibverbs\" provider-id=\"0\"/>"
  "  </puk:component>"
  "  <puk:entry-point iface=\"NewMad_minidriver\" port=\"minidriver\" provider-id=\"1\" />"
  "</puk:composite>";
__attribute__((unused))
const char assembly_minidriver_ib_srq[] =
  "<puk:composite id=\"nm:minidriver_ibverbs_srq\">"
  "  <puk:component id=\"0\" name=\"NewMad_ibverbs_common\"/>"
  "  <puk:component id=\"1\" name=\"NewMad_ibverbs_srq\">"
  "    <puk:uses iface=\"NewMad_ibverbs\" provider-id=\"0\"/>"
  "  </puk:component>"
  "  <puk:entry-point iface=\"NewMad_minidriver\" port=\"minidriver\" provider-id=\"1\" />"
  "</puk:composite>";

__attribute__((unused))
static const char assembly_minidriver_portals4_bybuf[] =
  "<puk:composite id=\"nsbe:minidriver_portals4_bybuf\">"
  "  <puk:component id=\"0\" name=\"Minidriver_portals4_bybuf\"/>"
  "  <puk:entry-point iface=\"NewMad_minidriver\" port=\"minidriver\" provider-id=\"0\" />"
  "</puk:composite>";
__attribute__((unused))
static const char assembly_minidriver_portals4_large[] =
  "<puk:composite id=\"nsbe:minidriver_portals4_large\">"
  "  <puk:component id=\"0\" name=\"Minidriver_portals4_large\"/>"
  "  <puk:entry-point iface=\"NewMad_minidriver\" port=\"minidriver\" provider-id=\"0\" />"
  "</puk:composite>";

__attribute__((unused))
static const char assembly_minidriver_ofi_rdm_buf[] =
  "<puk:composite id=\"nsbe:minidriver_ofi_rdm_buf\">"
  "  <puk:component id=\"0\" name=\"Minidriver_ofi_rdm_buf\">"
  "    <puk:attr label=\"provider\">cxi</puk:attr>"
  "  </puk:component>"
  "  <puk:entry-point iface=\"NewMad_minidriver\" port=\"minidriver\" provider-id=\"0\" />"
  "</puk:composite>";
__attribute__((unused))
static const char assembly_minidriver_ofi_rdm_large[] =
  "<puk:composite id=\"nsbe:minidriver_ofi_rdm_large\">"
  "  <puk:component id=\"0\" name=\"Minidriver_ofi_rdm_large\">"
  "    <puk:attr label=\"provider\">cxi</puk:attr>"
  "  </puk:component>"
  "  <puk:entry-point iface=\"NewMad_minidriver\" port=\"minidriver\" provider-id=\"0\" />"
  "</puk:composite>";

__attribute__((unused))
static const char assembly_minidriver_cma[] =
  "<puk:composite id=\"nsbe:minidriver_cma\">"
  "  <puk:component id=\"0\" name=\"Minidriver_CMA\"/>"
  "  <puk:entry-point iface=\"NewMad_minidriver\" port=\"minidriver\" provider-id=\"0\" />"
  "</puk:composite>";

__attribute__((unused))
static const char assembly_minidriver_shm[] =
  "<puk:composite id=\"nsbe:minidriver_shm\">"
  "  <puk:component id=\"0\" name=\"Minidriver_shm\"/>"
  "  <puk:entry-point iface=\"NewMad_minidriver\" port=\"minidriver\" provider-id=\"0\" />"
  "</puk:composite>";

__attribute__((unused))
static const char assembly_minidriver_largeshm[] =
  "<puk:composite id=\"nsbe:minidriver_largeshm\">"
  "  <puk:component id=\"0\" name=\"Minidriver_largeshm\"/>"
  "  <puk:entry-point iface=\"NewMad_minidriver\" port=\"minidriver\" provider-id=\"0\" />"
  "</puk:composite>";

__attribute__((unused))
static const char assembly_minidriver_control[] =
  "<puk:composite id=\"nsbe:minidriver_control\">"
  "  <puk:component id=\"0\" name=\"PSP_Control\"/>"
  "  <puk:component id=\"1\" name=\"Minidriver_PSP\">"
  "    <puk:uses iface=\"PadicoSimplePackets\" port=\"psp\" provider-id=\"0\" />"
  "  </puk:component>"
  "  <puk:entry-point iface=\"NewMad_minidriver\" port=\"minidriver\" provider-id=\"1\" />"
  "</puk:composite>";

__attribute__((unused))
static const char assembly_minidriver_psm_small[] =
  "<puk:composite id=\"nsbe:minidriver_psm-small\">"
  "  <puk:component id=\"0\" name=\"Minidriver_psm\"/>"
  "  <puk:entry-point iface=\"NewMad_minidriver\" port=\"minidriver\" provider-id=\"0\" />"
  "</puk:composite>";
__attribute__((unused))
static const char assembly_minidriver_psm_large[] =
  "<puk:composite id=\"nsbe:minidriver_psm-large\">"
  "  <puk:component id=\"0\" name=\"Minidriver_psm\"/>"
  "  <puk:entry-point iface=\"NewMad_minidriver\" port=\"minidriver\" provider-id=\"0\" />"
  "</puk:composite>";

__attribute__((unused))
static const char assembly_minidriver_psm2_small[] =
  "<puk:composite id=\"nsbe:minidriver_psm2-small\">"
  "  <puk:component id=\"0\" name=\"Minidriver_psm2\"/>"
  "  <puk:entry-point iface=\"NewMad_minidriver\" port=\"minidriver\" provider-id=\"0\" />"
  "</puk:composite>";
__attribute__((unused))
static const char assembly_minidriver_psm2_large[] =
  "<puk:composite id=\"nsbe:minidriver_psm2-large\">"
  "  <puk:component id=\"0\" name=\"Minidriver_psm2\"/>"
  "  <puk:entry-point iface=\"NewMad_minidriver\" port=\"minidriver\" provider-id=\"0\" />"
  "</puk:composite>";

__attribute__((unused))
static const char assembly_minidriver_local_small[] =
  "<puk:composite id=\"nsbe:minidriver_local-small\">"
  "  <puk:component id=\"0\" name=\"Minidriver_local\"/>"
  "  <puk:entry-point iface=\"NewMad_minidriver\" port=\"minidriver\" provider-id=\"0\" />"
  "</puk:composite>";
__attribute__((unused))
static const char assembly_minidriver_local_large[] =
  "<puk:composite id=\"nsbe:minidriver_local-large\">"
  "  <puk:component id=\"0\" name=\"Minidriver_local\"/>"
  "  <puk:entry-point iface=\"NewMad_minidriver\" port=\"minidriver\" provider-id=\"0\" />"
  "</puk:composite>";

/* ********************************************************* */

static puk_component_t
ns_besteffort_selector_serial(padico_topo_node_t node1, padico_topo_node_t node2,
                              const char* profile, puk_iface_t iface);
static puk_component_t
ns_besteffort_selector_host(padico_topo_host_t host, const char* profile,
                            puk_iface_t iface, uint16_t port, int protocol);

static const struct padico_netselector_driver_s ns_besteffort_default_driver =
  {
    .name    = "NetSelector-besteffort",
    .profile = "default",
    .serial  = &ns_besteffort_selector_serial,
    .host    = &ns_besteffort_selector_host
  };

static const struct padico_netselector_driver_s ns_besteffort_trk_small_driver =
  {
    .name    = "NetSelector-besteffort:trk_small",
    .profile = "trk_small",
    .serial  = &ns_besteffort_selector_serial,
    .host    = NULL
  };
static const struct padico_netselector_driver_s ns_besteffort_trk_large_driver =
  {
    .name    = "NetSelector-besteffort:trk_large",
    .profile = "trk_large",
    .serial  = &ns_besteffort_selector_serial,
    .host    = NULL
  };

/* *** Helper functions ************************************ */

static void nsbe_new_rule(const char*profile, const char*iface_name, const char*network, const char*assembly)
{
  struct nsbe_rule_bucket_s bucket =
    {
      .profile         = profile,
      .network_pattern = network,
      .symbolic        = assembly
    };
  puk_iface_t iface = puk_iface_register(iface_name);
  nsbe_rule_vect_t v = puk_hashtable_lookup(nsbe.rules, iface);
  if(!v)
    {
      v = nsbe_rule_vect_new();
      puk_hashtable_insert(nsbe.rules, iface, v);
    }
  nsbe_rule_vect_push_back(v, bucket);
}

/** checks whether the given entry is enabled in the current configuration */
static int nsbe_list_lookup(const char*entry)
{
  padico_string_t ne = padico_string_new();
  padico_string_printf(ne, "^%s", entry);
  const char*non_entry = padico_string_get(ne);
  int rc = 0;
  char*list = padico_strdup(padico_module_attr_netselector_list_getvalue());
  if((list == NULL) || (strcmp(list, "") == 0))
    {
      if(list)
        padico_free(list);
      list = padico_strdup("defaults");
    }
  char*defaults = strstr(list, "defaults");
  if(defaults != NULL)
    {
      memcpy(defaults, "\0,,,,,,,", 8); /* truncate string, then overwrite the remaining part */
      padico_string_t slist = padico_string_new();
      padico_string_catf(slist, "%s,%s,%s", list, PADICO_NETSELECTOR_LIST_DEFAULT, (defaults + 1));
      char*list2 = padico_strdup(padico_string_get(slist));
      padico_string_delete(slist);
      padico_free(list);
      list = list2;
    }
  char*saveptr = NULL;
  char*token = strtok_r(list, ",", &saveptr);
  while(token)
    {
      const char*ptok = (token != NULL && token[0] == '^') ? (token + 1) : token;
      int i;
      int found = 0;
      for(i = 0; i < sizeof(nsbe_valid_tokens) / sizeof(char*); i++)
        {
          if(strcmp(ptok, nsbe_valid_tokens[i]) == 0)
            {
              found = 1;
              break;
            }
        }
      if(!found)
        {
          padico_fatal("'%s' is not a valid token in PADICO_NETSELECTOR_LIST='%s'. Valid tokens are: " PADICO_NETSELECTOR_LIST_VALID_TOKENS,
                       token, padico_module_attr_netselector_list_getvalue());
        }
      if(strcmp(token, entry) == 0)
        {
          rc = 1;
          goto out;
        }
      if(strcmp(token, non_entry) == 0)
        {
          rc = 0;
          goto out;
        }
      token = strtok_r(NULL, ",", &saveptr);
    }
 out:
  padico_out(puk_verbose_info, "entry = %s; rc = %d\n", entry, rc);
  padico_string_delete(ne);
  padico_free(list);
  return rc;
}

static int comp_int(const void*_i1, const void*_i2)
{
  const int*i1 = _i1;
  const int*i2 = _i2;
  if(*i1 == *i2)
    return 0;
  else if(*i1 < *i2)
    return -1;
  else
    return 1;
}

/** apply multi-NIC policy in case multiple bindings are available */
static padico_topo_binding_t nsbe_binding_select(padico_topo_binding_vect_t bindings)
{
  padico_topo_binding_t b = NULL;
  const char*policy = padico_module_attr_multinic_policy_getvalue();
  static int policy_displayed = 0;
  if(!policy_displayed)
    {
      padico_print("using multi-NIC policy: %s.\n", policy);
      policy_displayed = 1;
    }
  if(strcmp(policy, "auto") == 0)
    {
      b = NULL;
    }
  else if(strcmp(policy, "first") == 0)
    {
      b = *padico_topo_binding_vect_begin(bindings);
    }
  else if(strcmp(policy, "last") == 0)
    {
      b = *padico_topo_binding_vect_rbegin(bindings);
    }
  else if(strcmp(policy, "roundrobin") == 0)
    {
      padico_topo_host_t localhost = padico_topo_getlocalhost();
      padico_topo_node_vect_t nodes = padico_topo_host_getnodes(localhost); /* local nodes in no particular order */
      const int n = padico_topo_node_vect_size(nodes);
      int*ranks = padico_malloc(n * sizeof(int));
      int i;
      for(i = 0; i < n; i++)
      {
        ranks[i] = padico_topo_session_getrank(padico_topo_session(), padico_topo_node_vect_at(nodes, i));
      }
      qsort(ranks, n, sizeof(int), &comp_int); /* sort ranks of local nodes */
      int local_rank = -1;
      for(i = 0; i < n; i++)
        {
          if(ranks[i] == padico_na_rank())
            {
              local_rank = i;
              break;
            }
        }
      if(local_rank == -1)
        padico_fatal("could not compute local rank for round-robin multi-NIC policy.\n");
      const size_t s = padico_topo_binding_vect_size(bindings);
      b = padico_topo_binding_vect_at(bindings, local_rank % s);
    }
  else
    {
      padico_fatal("unrecognized multi-NIC policy %s. Supported values: auto, first, last, roundrobin; default is roundrobin.\n", policy);
    }
  return b;
}

static puk_component_t nsbe_rule_bucket_resolve(struct nsbe_rule_bucket_s*rule_bucket, padico_topo_network_t network)
{
  struct nsbe_rule_resolved_key_s key = { .rule_bucket = rule_bucket, .network = network };
  struct nsbe_rule_resolved_s*resolved = puk_hashtable_lookup(nsbe.resolved, &key);
  if(!resolved)
    {
      puk_component_t component = puk_component_parse(rule_bucket->symbolic);
      if(network)
        {
          padico_string_t s_component_id = padico_string_new();
          padico_string_catf(s_component_id, "%s-{%s/%s}", component->name, padico_topo_network_getname(network), rule_bucket->profile);
          puk_component_t component2 = puk_compositize(component, padico_string_get(s_component_id));
          puk_component_destroy(component);
          component = component2;
          padico_string_delete(s_component_id);
          puk_component_setattr(component, "network", padico_topo_network_getname(network));
        }
      if((network != NULL) &&
         (fnmatch("Infiniband-*", padico_topo_network_getname(network), 0) == 0))
        {
          padico_topo_host_t host = padico_topo_getlocalhost();
          padico_topo_binding_vect_t bindings = padico_topo_host_getbindings(host, network);
          assert(padico_topo_binding_vect_size(bindings) > 0);
          padico_topo_binding_t b = nsbe_binding_select(bindings);
          padico_topo_binding_vect_delete(bindings);
          if(b != NULL)
            {
              int64_t index = -1;
              int rc = padico_topo_binding_get_index(b, &index);
              if(rc != 0)
                {
                  padico_warning("cannot get index number for InfiniBand board.\n");
                }
              char s_index[16];
              sprintf(s_index, "%lld", (long long int)index);
              padico_topo_hwaddr_t hwaddr = padico_topo_binding_gethwaddr(b);
              const char*name = NULL;
              padico_topo_hwaddr_get(hwaddr, &name, NULL, NULL, NULL, NULL);
              if(name != NULL && index >= 0)
                {
                  puk_component_setattr(component, "ibv_device", name);
                  puk_component_setattr(component, "ibv_port", s_index);
                }
            }
        }
      if((network != NULL) &&
         (fnmatch("PSM2-*", padico_topo_network_getname(network), 0) == 0))
        {
          padico_topo_host_t host = padico_topo_getlocalhost();
          padico_topo_binding_vect_t bindings = padico_topo_host_getbindings(host, network);
          assert(padico_topo_binding_vect_size(bindings) > 0);
          padico_topo_binding_t b = nsbe_binding_select(bindings);
          padico_topo_binding_vect_delete(bindings);
          if(b != NULL)
            {
              int64_t index = -1;
              int rc = padico_topo_binding_get_index(b, &index);
              if(rc != 0)
                {
                  padico_warning("cannot get index number for psm2 board.\n");
                }
              char s_index[16];
              sprintf(s_index, "%lld", (long long int)index);
              if(index >= 0)
                {
                  puk_component_setattr(component, "port", s_index);
                }
            }
        }
      resolved = padico_malloc(sizeof(struct nsbe_rule_resolved_s));
      resolved->key.rule_bucket = rule_bucket;
      resolved->key.network = network;
      resolved->component = component;
      puk_hashtable_insert(nsbe.resolved, &resolved->key, resolved);
    }
  return resolved->component;
}

static puk_component_t
ns_besteffort_selector_2networks(padico_topo_network_vect_t src_networks, padico_topo_network_vect_t dest_networks,
                                 const char*profile, puk_iface_t iface, padico_topo_host_t host, uint16_t port, int protocol)
{
  puk_component_t assembly = NULL;
  const nsbe_rule_vect_t rules = puk_hashtable_lookup(nsbe.rules, iface);
  if(rules != NULL)
    {
      nsbe_rule_vect_itor_t r;
      puk_vect_foreach(r, nsbe_rule, rules)
        {
          padico_out(40, "trying rule profile=%s; pattern=%s; symbolic=%s\n",
                     r->profile, r->network_pattern, r->symbolic);
          if((r->profile == NULL) || (strcmp(r->profile, profile) == 0))
            {
              padico_topo_network_vect_itor_t n;
              puk_vect_foreach(n, padico_topo_network, src_networks)
                {
                  padico_out(40, "  trying network=%s\n", padico_topo_network_getname(*n));
                  if(!padico_topo_host_can_output(padico_topo_getlocalhost(), *n, port, protocol))
                    continue;
                  if((padico_topo_network_vect_find(dest_networks, *n) != NULL) &&
                     (fnmatch(r->network_pattern, padico_topo_network_getname(*n), 0) == 0))
                    {
                      if(!padico_topo_host_can_input(host, *n, port, protocol))
                        continue;
                      padico_out(40, "** rule matches!\n");
                      assembly = nsbe_rule_bucket_resolve(r, *n);
                      assert(assembly != NULL);
                      if(puk_component_get_facet(assembly, iface, NULL) == NULL)
                        {
                          padico_fatal("assembly has no facet for iface %s. Assembly is: %s\n",
                                       iface->name, padico_string_get(puk_component_serialize(assembly)));
                        }
                      goto found;
                    }
                }
            }
        }
    }
  /* src_networks and dest_networks are disjoint */
  if(!strcmp(iface->name, "PadicoBootstrap") && host)
    {
      padico_string_t hosts = padico_string_new();
      padico_string_t users = padico_string_new();
      padico_topo_network_t dest_n = NULL;
      if(ns_besteffort_selector_dijkstra(hosts, users, host, &dest_n))
        {
          padico_string_t assembly_control_ssh2 = padico_string_new();
          const struct in_addr* dest = padico_topo_host_getaddr2(host, dest_n);
          char*dest_s = padico_malloc(INET_ADDRSTRLEN);
          inet_ntop(AF_INET, dest, dest_s, INET_ADDRSTRLEN);
          padico_string_catf(assembly_control_ssh2,
                             "<puk:composite id=\"nsbe:control_ssh2\">"
                             "  <puk:component id=\"0\" name=\"Control_SSH2\">"
                             "    <puk:attr label=\"ssh_host\">%s</puk:attr>"
                             "    <puk:attr label=\"ssh_user\">%s</puk:attr>"
                             "    <puk:attr label=\"ssh_dest\">%s</puk:attr>"
                             "  </puk:component>"
                             "  <puk:entry-point iface=\"PadicoControl\"   provider-id=\"0\" />"
                             "  <puk:entry-point iface=\"PadicoBootstrap\" provider-id=\"0\" />"
                             "</puk:composite>",
                             padico_string_get(hosts), padico_string_get(users), dest_s);
          if(padico_string_size(hosts) == 0)
            padico_fatal("invalid route to dest=%s\n", dest_s);
          assembly = puk_component_parse(padico_string_get(assembly_control_ssh2));
          padico_string_delete(assembly_control_ssh2);
          padico_free(dest_s);
          assert(assembly != NULL);
        }
      padico_string_delete(hosts);
      padico_string_delete(users);
    }
  padico_out(10, "NULL assembly for iface=%s\n", iface->name);
 found:
  return assembly;
}

static void ns_besteffort_getnetworks(padico_topo_node_t node, struct padico_topo_network_vect_s*networks)
{
  padico_topo_host_t host = padico_topo_node_gethost(node);
  padico_topo_network_vect_init(networks);
  padico_topo_node_getnetworks(node, networks);
  padico_topo_host_getnetworks(host, networks);
}

static puk_component_t
ns_besteffort_selector_2nodes(padico_topo_node_t localnode, padico_topo_node_t destnode,
                              const char*profile, puk_iface_t iface)
{
  struct padico_topo_network_vect_s src_networks, dest_networks;
  ns_besteffort_getnetworks(localnode, &src_networks);
  ns_besteffort_getnetworks(destnode, &dest_networks);
  puk_component_t assembly = ns_besteffort_selector_2networks(&src_networks, &dest_networks, profile, iface, padico_topo_node_gethost(destnode), 0, IPPROTO_TCP);
  padico_topo_network_vect_destroy(&src_networks);
  padico_topo_network_vect_destroy(&dest_networks);
  return assembly;
}

/* *** Selectors ******************************************* */

static puk_component_t
ns_besteffort_selector_serial(padico_topo_node_t localnode, padico_topo_node_t destnode,
                              const char*profile, puk_iface_t iface)
{
  marcel_mutex_lock(&nsbe.lock);
  const puk_component_t assembly = ns_besteffort_selector_2nodes(localnode, destnode, profile, iface);
  marcel_mutex_unlock(&nsbe.lock);
  return assembly;
}

static puk_component_t
ns_besteffort_selector_host(padico_topo_host_t host, const char*profile,
                            puk_iface_t iface, uint16_t port, int protocol)
{
  puk_component_t assembly = NULL;
  marcel_mutex_lock(&nsbe.lock);
  if(host == NULL)
    {
      /* take the first compatible rule */
      const nsbe_rule_vect_t rules = puk_hashtable_lookup(nsbe.rules, iface);
      if(rules != NULL)
        {
          struct nsbe_rule_bucket_s*r;
          if(!nsbe_rule_vect_empty(rules))
            {
              r = nsbe_rule_vect_ptr(rules, 0);
              padico_out(40, "Obtain rule pattern=%s; symbolic=%s\n", r->network_pattern, r->symbolic);
              assembly = nsbe_rule_bucket_resolve(r, NULL);
              assert(assembly != NULL);
              if(puk_component_get_facet(assembly, iface, NULL) == NULL)
                {
                  padico_fatal("assembly has no facet for iface %s.\n", iface->name);
                }
            }
        }
      if(!assembly)
        padico_out(10, "NULL assembly for iface=%s\n", iface->name);
    }
  else
    {
      struct padico_topo_network_vect_s src_networks, dest_networks;
      padico_topo_network_vect_init(&src_networks);
      padico_topo_host_getnetworks(padico_topo_getlocalhost(), &src_networks);
      padico_topo_network_vect_init(&dest_networks);
      padico_topo_host_getnetworks(host, &dest_networks);
      assembly = ns_besteffort_selector_2networks(&src_networks, &dest_networks, profile, iface, host, port, protocol);
      padico_topo_network_vect_destroy(&src_networks);
      padico_topo_network_vect_destroy(&dest_networks);
    }
  marcel_mutex_unlock(&nsbe.lock);
  return assembly;
}

/* *** Init ************************************************ */

uint32_t nsbe_rule_resolved_hash(const void*_key)
{
  return puk_hash_default(_key, sizeof(struct nsbe_rule_resolved_key_s));
}

int nsbe_rule_resolved_eq(const void*_key1, const void*_key2)
{
  const struct nsbe_rule_resolved_key_s*key1 = _key1;
  const struct nsbe_rule_resolved_key_s*key2 = _key2;
  return ((key1->rule_bucket == key2->rule_bucket) &&
          (key1->network == key2->network));
}

static int ns_besteffort_init(void)
{
  marcel_mutex_init(&nsbe.lock, NULL);
  nsbe.rules = puk_hashtable_new_ptr();
  nsbe.resolved = puk_hashtable_new(&nsbe_rule_resolved_hash, &nsbe_rule_resolved_eq);

  padico_out(40, "entering.\n");
  /* warning: order matters ! */
  /* ** default for PadicoTM */
  nsbe_new_rule("default",   "PadicoConnector",     "PSM2-*",         assembly_control_minidriver_psm2);
  nsbe_new_rule("default",   "PadicoConnector",     "PSM-*",          assembly_control_minidriver_psm);
  nsbe_new_rule("default",   "PadicoConnector",     "Infiniband-*",   assembly_control_minidriver_ibverbs);
  nsbe_new_rule("default",   "PadicoConnector",     "inet*",          assembly_control_sysio);
  nsbe_new_rule("default",   "PadicoBootstrap",     "inet*",          assembly_control_sysio);
  nsbe_new_rule("default",   "VLink",               "LoopbackHost-*", assembly_vlink_shm);
  nsbe_new_rule("default",   "VLink",               "Infiniband-*",   assembly_vlink_miniib);
  nsbe_new_rule("default",   "VLink",               "inet*",          assembly_vlink_sfinet);
  nsbe_new_rule("default",   "VLink",               "ControlNetwork", assembly_vlink_control);
  nsbe_new_rule("default",   "PadicoSimplePackets", "LoopbackHost-*", assembly_psp_shm);
  nsbe_new_rule("default",   "PadicoSimplePackets", "Infiniband-*",   assembly_pspmini_infiniband);
  nsbe_new_rule("default",   "PadicoSimplePackets", "inet*",          assembly_psp_sf);
  nsbe_new_rule("default",   "PadicoSimplePackets", "ControlNetwork", assembly_psp_control);
  /* ** nmlad legacy */
  nsbe_new_rule("default",   "NewMad_Driver",       "LoopbackNode-*", assembly_nmad_self);
  nsbe_new_rule("default",   "NewMad_Driver",       "LoopbackHost-*", assembly_nmad_minishm);
  nsbe_new_rule("default",   "NewMad_Driver",       "Infiniband-*",   assembly_nmad_infiniband);
  nsbe_new_rule("default",   "NewMad_Driver",       "MX-*",           assembly_nmad_mx);
  nsbe_new_rule("default",   "NewMad_Driver",       "inet*",          assembly_nmad_tcp);
  nsbe_new_rule("default",   "NewMad_Driver",       "ControlNetwork", assembly_nmad_control);
  /* ** nmad with minidriver */

  if(nsbe_list_lookup("self"))
    {
      nsbe_new_rule("trk_small", "NewMad_minidriver",   "LoopbackNode-*", assembly_minidriver_selfbuf);
      nsbe_new_rule("trk_large", "NewMad_minidriver",   "LoopbackNode-*", assembly_minidriver_self);
    }
  if(nsbe_list_lookup("cma") || puk_opt_parse_bool(getenv("NMAD_SHM_CMA")))
    {
      nsbe_new_rule("trk_small", "NewMad_minidriver",   "LoopbackHost-*", assembly_minidriver_shm);
      nsbe_new_rule("trk_large", "NewMad_minidriver",   "LoopbackHost-*", assembly_minidriver_cma);
    }
  if(nsbe_list_lookup("shm"))
    {
      nsbe_new_rule("trk_small", "NewMad_minidriver",   "LoopbackHost-*", assembly_minidriver_shm);
      nsbe_new_rule("trk_large", "NewMad_minidriver",   "LoopbackHost-*", assembly_minidriver_largeshm);
    }
  if(nsbe_list_lookup("local"))
    {
      nsbe_new_rule("trk_small", "NewMad_minidriver",   "LoopbackHost-*", assembly_minidriver_local_small);
      nsbe_new_rule("trk_large", "NewMad_minidriver",   "LoopbackHost-*", assembly_minidriver_local_large);
    }
  if(nsbe_list_lookup("psm2"))
    {
      nsbe_new_rule("trk_small", "NewMad_minidriver",   "PSM2-*",         assembly_minidriver_psm2_small);
      nsbe_new_rule("trk_large", "NewMad_minidriver",   "PSM2-*",         assembly_minidriver_psm2_large);
    }
  if(nsbe_list_lookup("psm"))
    {
      nsbe_new_rule("trk_small", "NewMad_minidriver",   "PSM-*",          assembly_minidriver_psm_small);
      nsbe_new_rule("trk_large", "NewMad_minidriver",   "PSM-*",          assembly_minidriver_psm_large);
    }

  if(nsbe_list_lookup("ibsrq") || puk_opt_parse_bool(getenv("NMAD_IBVERBS_SRQ")))
    {
      nsbe_new_rule("trk_small", "NewMad_minidriver",   "Infiniband-*",   assembly_minidriver_ib_srq);
    }
  if(nsbe_list_lookup("ibbuf"))
    {
      nsbe_new_rule("trk_small", "NewMad_minidriver",   "Infiniband-*",   assembly_minidriver_ib_bybuf);
    }
  if(nsbe_list_lookup("ibrcache") || puk_opt_parse_bool(getenv("NMAD_IBVERBS_RCACHE")))
    {
      nsbe_new_rule("trk_large", "NewMad_minidriver",   "Infiniband-*", assembly_minidriver_ib_rcache);
    }
  if(nsbe_list_lookup("iblr2"))
    {
      nsbe_new_rule("trk_large", "NewMad_minidriver",   "Infiniband-*", assembly_minidriver_ib_lr2);
    }
  if(nsbe_list_lookup("ibverbs"))
    {
      if(padico_na_size() > 16)
        {
          nsbe_new_rule("trk_small", "NewMad_minidriver",   "Infiniband-*",   assembly_minidriver_ib_srq);
        }
      else
        {
          nsbe_new_rule("trk_small", "NewMad_minidriver",   "Infiniband-*",   assembly_minidriver_ib_bybuf);
        }
      nsbe_new_rule("trk_large", "NewMad_minidriver",   "Infiniband-400G*", assembly_minidriver_ib_rcache);
      nsbe_new_rule("trk_large", "NewMad_minidriver",   "Infiniband-200G*", assembly_minidriver_ib_rcache);
      nsbe_new_rule("trk_large", "NewMad_minidriver",   "Infiniband-100G*", assembly_minidriver_ib_rcache);
      nsbe_new_rule("trk_large", "NewMad_minidriver",   "Infiniband-*",     assembly_minidriver_ib_lr2);
    }

  if(nsbe_list_lookup("portals4"))
    {
      nsbe_new_rule("trk_small", "NewMad_minidriver",   "BXI",            assembly_minidriver_portals4_bybuf);
      nsbe_new_rule("trk_large", "NewMad_minidriver",   "BXI",            assembly_minidriver_portals4_large);
    }

  if(nsbe_list_lookup("cxi"))
    {
      nsbe_new_rule("trk_small", "NewMad_minidriver",   "CXI",            assembly_minidriver_ofi_rdm_buf);
      nsbe_new_rule("trk_large", "NewMad_minidriver",   "CXI",            assembly_minidriver_ofi_rdm_large);
    }
  if(nsbe_list_lookup("tcp"))
    {
      nsbe_new_rule("trk_small", "NewMad_minidriver",   "inet*",          assembly_minidriver_tcp);
      nsbe_new_rule("trk_large", "NewMad_minidriver",   "inet*",          assembly_minidriver_tcp);
    }
  if(nsbe_list_lookup("control"))
    {
      nsbe_new_rule("trk_small", "NewMad_minidriver",   "ControlNetwork", assembly_minidriver_control);
      nsbe_new_rule("trk_large", "NewMad_minidriver",   "ControlNetwork", assembly_minidriver_control);
    }
  padico_ns_selector_register(&ns_besteffort_default_driver);
  padico_ns_selector_register(&ns_besteffort_trk_small_driver);
  padico_ns_selector_register(&ns_besteffort_trk_large_driver);
  padico_out(40, "done.\n");
  return 0;
}

static void nsbe_rule_vect_destructor(void*_key, void*_data)
{
  nsbe_rule_vect_t v = _data;
  nsbe_rule_vect_delete(v);
}

static void nsbe_resolved_destructor(void*_key, void*_data)
{
  struct nsbe_rule_resolved_s*r = _data;
  padico_free(r);
}

static void ns_besteffort_finalize(void)
{
  padico_ns_selector_unregister(&ns_besteffort_default_driver);
  padico_ns_selector_unregister(&ns_besteffort_trk_small_driver);
  padico_ns_selector_unregister(&ns_besteffort_trk_large_driver);
  puk_hashtable_delete(nsbe.rules, &nsbe_rule_vect_destructor);
  puk_hashtable_delete(nsbe.resolved, &nsbe_resolved_destructor);
}

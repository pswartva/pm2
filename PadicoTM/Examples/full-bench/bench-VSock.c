/* bench-VSock.c
 */

#include <netdb.h>
#include "bench-common.h"
#include <Padico/VSock.h>
#include <Padico/Module.h>

int bench_vsock_init(void);
void bench_vsock_run(int argc, char**argv);
void bench_vsock_finalize(void);

PADICO_MODULE_DECLARE(bench_VSock, bench_vsock_init, bench_vsock_run, bench_vsock_finalize);

#define DEFAULT_PORT       5050

static int port = DEFAULT_PORT;

static struct sockaddr_in local_addr;
static socklen_t          local_addr_size  = sizeof(local_addr);
static struct sockaddr_in remote_addr;
static socklen_t          remote_addr_size = sizeof(remote_addr);
static padico_vsock_t     bench_fd;
static padico_vsock_t     server_fd;

int bench_vsock_init(void)
{
  if(rank == 0)
    {
      int rc;
      server_fd = padico_socket(PF_INET, SOCK_STREAM, 0);
      local_addr.sin_family = AF_INET;
      local_addr.sin_port  = port;
      rc = padico_bind(server_fd, (struct sockaddr*)&local_addr, local_addr_size);
      if(rc != 0)
        padico_fatal("cannot bind on port %d\n", port);
      padico_listen(server_fd, 1);
      bench_fd = padico_accept(server_fd, (struct sockaddr*)&remote_addr, &remote_addr_size);
    }
  else
    {
      struct hostent*he;
      int rc;
      bench_fd = padico_socket(PF_INET, SOCK_STREAM, 0);
      he = gethostbyname(server_name);
      memcpy(&remote_addr.sin_addr, he->h_addr_list[0], sizeof(remote_addr.sin_addr));
      remote_addr.sin_family = AF_INET;
      remote_addr.sin_port  = port;
      do
        {
          rc = padico_connect(bench_fd, (struct sockaddr*)&remote_addr, remote_addr_size);
          if(rc != 0)
            {
              padico_warning("cannot connect fd %d. Retrying...\n", bench_fd);
              padico_tm_usleep(1000);
              /* TODO: c'est un peu bourrin d'attendre... */
            }
          else
            {
              padico_print("BENCH: connected.\n");
            }
        }
      while(rc != 0);
    }
  return 0;
}

void bench_vsock_finalize(void)
{
  if(rank == 0)
    {
      padico_close(server_fd);
    }
  padico_close(bench_fd);
}

void vsock_server(char*buffer, int lenbuf, int num_roundtrip)
{
  int i;
  for(i=0; i<num_roundtrip; i++)
    {
      padico_send(bench_fd, buffer, lenbuf, 0);
      padico_recv(bench_fd, buffer, lenbuf, 0);
    }
}

void vsock_client(char*buffer, int lenbuf, int num_roundtrip)
{
  int i;
  for(i=0; i<num_roundtrip; i++)
    {
      padico_recv(bench_fd, buffer, lenbuf, 0);
      padico_send(bench_fd, buffer, lenbuf, 0);
    }
}

struct bench_desc_s vsock_bench =
{
  "VSock",
  &vsock_server,
  &vsock_client
};

void bench_vsock_run(int argc, char**argv)
{
  the_bench(&vsock_bench);
}

/*
 * bench-common.c
 */

#include <stdlib.h>
#include <unistd.h>
#include <pwd.h>
#include <Padico/Puk.h>
#include <Padico/Topology.h>
#include <Padico/Module.h>

#include "bench-common.h"

int bench_common_init(void);
void bench_common_finalize(void);

PADICO_MODULE_DECLARE(bench_common, bench_common_init, NULL, bench_common_finalize);

#define DEFAULT_PATH       "/tmp"
#define DEFAULT_ROUNDTRIP  200

#define LOGSCALE_FACTOR_NUM   6
#define LOGSCALE_FACTOR_DENUM 5

static const char*path    = DEFAULT_PATH;
static long maxlen        = DEFAULT_MAX_LEN;
static int  num_roundtrip = DEFAULT_ROUNDTRIP;
static char *buffer       = NULL;
static size_t logscale_threshold = 192;
static size_t len_step  = 4;
static size_t len_start = 1;
static const size_t downramp_threshold = 128 * 1024;
static const int downramp_guard = 5;
static int wait_send = 0;

static const int dryrun_count = 50;

struct bench_common_param_s bench_common_param =
{
  .is_server   = 0,
  .server_node = NULL,
  .client_node = NULL
};

static int comp_double(const void*_t1, const void*_t2)
{
  const double*t1 = _t1;
  const double*t2 = _t2;
  if(*t1 == *t2)
    return 0;
  else if(*t1 < *t2)
    return -1;
  else
    return 1;
}

int bench_common_init(void)
{
  const char*c;
  int i;

  /* try the 'bench-common' group */
  padico_group_t group = padico_group_lookup("bench-common");
  if(group)
    {
      const int gsize = padico_group_size(group);
      if(group && gsize >= 2 && gsize % 2 == 0)
        {
          int k;
          for(k = 0; k < gsize; k += 2)
            {
              const padico_topo_node_t n1 = padico_group_node(group, k);
              const padico_topo_node_t n2 = padico_group_node(group, k+1);
              if(padico_topo_getlocalnode() == n1 || padico_topo_getlocalnode() == n2)
                {
                  bench_common_param.server_node = n1;
                  bench_common_param.client_node = n2;
                  break;
                }
            }
        }
    }
  /* try the 'BENCH_PEER' attribute */
  else if(padico_getattr("BENCH_PEER"))
    {
      const char*peer_name = padico_getattr("BENCH_PEER");
      const padico_topo_host_t peer_host = padico_topo_host_resolvebyname(peer_name);
      if(!peer_host)
        {
          padico_fatal("BENCH- unknown peer host %s\n", peer_name);
        }
      const padico_topo_node_vect_t nodes = padico_topo_host_getnodes(peer_host);
      padico_topo_node_t peer_node = NULL;
      padico_topo_node_vect_itor_t i;
      puk_vect_foreach(i, padico_topo_node, nodes)
        {
          if(*i != padico_topo_getlocalnode())
            {
              peer_node = *i;
              break;
            }
        }
      if(!peer_node)
        {
          padico_fatal("BENCH- no node on peer host %s\n", peer_name);
        }
      int reverse = 1;
      if (padico_getattr("BENCH_REVERSE"))
        {
          const char* str_reverse = padico_getattr("BENCH_REVERSE");
          if (strcmp(str_reverse, "0")!=0)
            reverse = -1;
        }
      const int is_server = reverse * strcmp(padico_topo_node_getname(peer_node), padico_topo_nodename()) > 0;
      padico_print("BENCH peer = %s [%d]\n", peer_name, is_server);
      if(is_server)
        {
          bench_common_param.server_node = padico_topo_getlocalnode();
          bench_common_param.client_node = peer_node;
        }
      else
        {
          bench_common_param.client_node = padico_topo_getlocalnode();
          bench_common_param.server_node = peer_node;
        }
    }
  /* try to use the session information */
  else if(padico_na_size() % 2 == 0)
    {
      const char*session = padico_topo_node_getsession(padico_topo_getlocalnode());
      int k;
      for(k = 0; k < padico_na_size(); k += 2)
        {
          const padico_topo_node_t n1 = padico_topo_session_getnodebyrank(session, k);
          const padico_topo_node_t n2 = padico_topo_session_getnodebyrank(session, k + 1);
          if(padico_topo_getlocalnode() == n1 || padico_topo_getlocalnode() == n2)
            {
              bench_common_param.server_node = n1;
              bench_common_param.client_node = n2;
              break;
            }
        }
    }
  bench_common_param.is_server = (bench_common_param.server_node == padico_topo_getlocalnode());
  if(bench_common_param.server_node == NULL ||
     bench_common_param.client_node == NULL)
    {
      padico_warning("BENCH- bad group definition.\nBenchmark expects a group named 'bench-common' with and even number of node, a BENCH_PEER attribute, or a session with 2 nodes.\n");
      return 0;
    }
  padico_print("*** self = %s\n", padico_module_self_name());

  path = padico_getattr("path")?:DEFAULT_PATH;
  c = padico_getattr("maxlen");
  if(c) maxlen = atoi(c);
  c = padico_getattr("startlen");
  if(c) len_start = atoi(c);
  c = padico_getattr("roundtrip");
  if(c) num_roundtrip = atoi(c);
  c = padico_getattr("waitsend");
  if(c) wait_send = atoi(c);
  padico_print("BENCH: path        = %s\n", path);
  padico_print("BENCH: startlen    = %ld\n", len_start);
  padico_print("BENCH: maxlen      = %ld\n", maxlen);
  padico_print("BENCH: roundtrip   = %d\n", num_roundtrip);
  padico_print("BENCH: waitsend    = %d\n", wait_send);
  padico_print("BENCH: server node = %s\n", padico_topo_node_getname(bench_common_param.server_node));
  padico_print("BENCH: server host = %s\n", padico_topo_host_getname(padico_topo_node_gethost(bench_common_param.server_node)));
  if ( (buffer = (char *) padico_malloc((unsigned) maxlen)) == (char *) NULL)
    {
      padico_fatal("bench-common: could not allocate buffer of size %ld.\n", maxlen);
      return 1;
    }
  for(i = 0; i < maxlen; i++)
    {
      buffer[i] = i;
    }
  return 0;
}

void bench_common_finalize(void)
{
  if(buffer)
    padico_free(buffer);
  padico_print("BENCH: finalizing\n");
}

void the_bench(const struct bench_desc_s*b)
{
  FILE*flog_time    = NULL;
  FILE*flog_bw      = NULL;
  FILE*flog_distrib = NULL;
  size_t lenbuf;
  padico_timing_t tick_start, tick_end;
  int current_rt = num_roundtrip;
  double*results = NULL;

  if(bench_common_param.server_node == NULL ||
     bench_common_param.client_node == NULL)
    {
      return;
    }

  if(bench_common_param.is_server)
    {
      padico_string_t name_time = padico_string_new();
      padico_string_t name_bw = padico_string_new();
      padico_string_t name_distrib = padico_string_new();
      /* const char*user = padico_strdup(getpwuid(getuid())->pw_name);*/
      const char*user = padico_strdup(padico_getenv("USER"));
      padico_string_printf(name_time, "%s/%s-%s.t",  path, b->name, user);
      padico_string_printf(name_bw,   "%s/%s-%s.bw", path, b->name, user);
      padico_string_printf(name_distrib,   "%s/%s-%s.dis", path, b->name, user);
      flog_time = fopen(padico_string_get(name_time), "w");
      flog_bw   = fopen(padico_string_get(name_bw), "w");
      flog_distrib   = fopen(padico_string_get(name_distrib), "w");
      padico_string_delete(name_time);
      padico_string_delete(name_bw);
      padico_string_delete(name_distrib);
      fprintf(flog_time, "# len. avg.   min.   max.   median\n");
    }
  lenbuf = len_start;

  /* dryrun */
  padico_print("%s bench- starting dryrun...\n", b->name);
  int i;
  for(i = 0; i < dryrun_count; i++)
    {
      if(bench_common_param.is_server)
        {
          b->server(buffer, lenbuf);
        }
      else
        {
          b->client(buffer, lenbuf);
        }
    }
  padico_print("%s bench- dryrun ok.\n", b->name);
  if(bench_common_param.is_server)
    {
      results = padico_malloc(sizeof(double) * num_roundtrip);
      padico_print("%s bench ----------------------------------------\n", b->name);
    }
  while (lenbuf <= maxlen)
    {
      if(lenbuf > downramp_threshold && current_rt > downramp_guard)
        current_rt = current_rt * LOGSCALE_FACTOR_DENUM / LOGSCALE_FACTOR_NUM;
      padico_out(40, "starting bench- len=%d\n", lenbuf);
      padico_timing_get_tick(&tick_start);
      if(bench_common_param.is_server)
        {
          for(i = 0; i < current_rt; i++)
            {
              if(wait_send)
                usleep(wait_send);
              padico_timing_get_tick(&tick_start);
              b->server(buffer, lenbuf);
              padico_timing_get_tick(&tick_end);
              const double time_usec = padico_timing_diff_usec(&tick_start, &tick_end) / 2.0;
              results[i] = time_usec;
            }
          qsort(results, current_rt, sizeof(double), &comp_double);
          double total_time_usec = 0.0;
          double min_time_usec = results[0];
          double max_time_usec = results[current_rt - 1];
          double med_time_usec = results[current_rt / 2];
          fprintf(flog_distrib, "#%d bytes", lenbuf);
          for(i = 0; i < current_rt; i++)
            {
              const double time_usec = results[i];
              fprintf(flog_distrib, " %.2f", time_usec);
              total_time_usec += time_usec;
            }
          fprintf(flog_distrib, "\n");
          double avg_time_usec = total_time_usec / (double)current_rt;
          double bw_MBps = 0.0;
          if (avg_time_usec > 0)
            bw_MBps = (double)(lenbuf / avg_time_usec);
          padico_print("%7d bytes, time=%8.3f usec, bw=%6.2f MB/s rt=%d\n",
                       lenbuf, avg_time_usec, bw_MBps, current_rt);
          fprintf(flog_time, "%d %f %f %f %f\n", lenbuf, avg_time_usec, min_time_usec, max_time_usec, med_time_usec);
          fprintf(flog_bw, "%d %f\n", lenbuf, bw_MBps);
        }
      else
        {
          for(i = 0; i < current_rt; i++)
            {
              b->client(buffer, lenbuf);
            }
        }
      padico_out(40, "done bench- len=%d\n", lenbuf);
      if(lenbuf == 0)
        {
          lenbuf = 1;
        }
      else if(lenbuf <= 16)
        {
          lenbuf *= 2;
        }
      else if(lenbuf >= logscale_threshold)
        {
          lenbuf = lenbuf * LOGSCALE_FACTOR_NUM / LOGSCALE_FACTOR_DENUM;
        }
      else
        {
          lenbuf += len_step;
        }
    }
  if(bench_common_param.is_server)
    {
      padico_print("%s bench ok -------------------------------------\n", b->name);
      padico_free(results);
      fclose(flog_time);
      fclose(flog_bw);
      fclose(flog_distrib);
    }
}

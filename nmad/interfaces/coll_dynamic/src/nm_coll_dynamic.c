/*
 * NewMadeleine
 * Copyright (C) 2019-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <nm_public.h>
#include <nm_launcher_interface.h>
#include <nm_session_interface.h>
#include <nm_coll_interface.h>
#include <Padico/Puk.h>

#ifdef PIOMAN

#include "nm_coll_dynamic_private.h"
#include "nm_coll_dynamic_interface.h"


static nm_session_t coll_dynamic_session = NULL;

static nm_multicast_req_allocator_t nm_multicast_req_allocator = NULL;
static nm_rpc_service_t rpc_service = NULL;
static piom_spinlock_t rpc_service_ref_lock;

static nm_coll_tree_kind_t diffusion_tree_type = NM_COLL_TREE_BINOMIAL;
static int copy_before_forward = 0;

struct _src_tag_req_key_helement_s {
  struct _src_tag_req_key_s
  {
    nm_tag_t tag;
    int src_node;
  } __attribute__((packed)) key;
  struct nm_multicast_req_s* p_req;
} __attribute__((packed));


static uint32_t src_tag_req_hfunc(const struct _src_tag_req_key_s* key)
{
  return puk_hash_default(key, sizeof(struct _src_tag_req_key_s));
}

static int src_tag_req_eqfunc(const struct _src_tag_req_key_s* key1, const struct _src_tag_req_key_s* key2)
{
  return (memcmp(key1, key2, sizeof(struct _src_tag_req_key_s)) == 0);
}

static void src_tag_req_delfunc(struct _src_tag_req_key_s* key, struct _src_tag_req_key_helement_s* element)
{
  padico_free(element);
}

PUK_HASHTABLE_TYPE(
  src_tag_req,
  struct _src_tag_req_key_s*,
  struct _src_tag_req_key_helement_s*,
  &src_tag_req_hfunc,
  &src_tag_req_eqfunc,
  &src_tag_req_delfunc
);

src_tag_req_hashtable_t unexpected_incoming_requests;
src_tag_req_hashtable_t recv_requests;
static piom_spinlock_t hashtables_lock;


static inline void return_recv_data_to_app(struct nm_multicast_req_s* initial_recv)
{
  if (initial_recv->cond_wait_recv != NULL)
    {
      piom_cond_signal(initial_recv->cond_wait_recv, 1);
    }

  if (initial_recv->end_recv_callback != NULL)
    {
      (*initial_recv->end_recv_callback)(initial_recv->end_recv_callback_arg);
    }
}


// Function called when all sending/forwarding communcations are over
inline void free_request_nodes_data(struct nm_multicast_req_s* p_req)
{
  assert(p_req->type == PENDING || p_req->type == FORWARD_RECV || p_req->type == FORWARD_INITIAL || p_req->type == INITIAL_SEND);

  padico_free(p_req->ref_nodes);
  padico_free(p_req->ref_prios);

  if (p_req->initial_recv != NULL) // receiver node
    {
      assert(p_req->initial_recv->type == USER_RECV);
      assert(p_req->type == FORWARD_RECV || p_req->type == PENDING);

      if (!(copy_before_forward && p_req->nb_nodes_in_tree > 0))
        {
          return_recv_data_to_app(p_req->initial_recv);
        }

      if (copy_before_forward && p_req->nb_nodes_in_tree > 0)
        {
          assert(p_req->initial_recv->copied_data_ptr != NULL);
          padico_free(p_req->initial_recv->copied_data_ptr);
        }

      nm_multicast_req_free(nm_multicast_req_allocator, p_req->initial_recv);
    }
  else // node which called nm_coll_dynamic_send*()
    {
      assert(p_req->type == FORWARD_INITIAL || p_req->type == INITIAL_SEND);
      if (p_req->end_send_callback != NULL)
        {
          (*p_req->end_send_callback)(p_req->end_send_callback_arg);
        }
    }
}

void monitor_send(nm_rpc_req_t rpc_req, void* _ref)
{
  struct nm_multicast_req_s* p_req = (struct nm_multicast_req_s*) _ref;

  assert(p_req->type == FORWARD_RECV || p_req->type == FORWARD_INITIAL || p_req->type == INITIAL_SEND);

  int src_node = p_req->ref_header->src_node;

  padico_free(p_req->ref_header);

  if(p_req->last)
    {
      free_request_nodes_data(p_req);
    }
  else
    {
      send_multicast(p_req->ref_nodes, p_req->ref_prios, p_req->nb_nodes_in_tree, p_req->tag,
                     &p_req->body_data, p_req->body_len, p_req->tree_step+1,
                     p_req->tree_additionnal_info, src_node, p_req->initial_recv,
                     p_req->end_send_callback, p_req->end_send_callback_arg);
    }

  nm_multicast_req_free(nm_multicast_req_allocator, p_req);
}


/**
 * Breadth-first search traversal of a binomial tree to reorder nodes according to priorities.
 * - Nodes in `nodes` are sorted in priority-descending order.
 * - Nodes in `ordered_nodes` will be ordered such as our execution of the binomial tree will send
 *   data to nodes respecting priorities (ie: nodes with higher priority will receive data first)
 * - `nb_nodes` is the number of nodes, *including* src node (so nb_dest+1)
 */
static inline void binomial_reorder_nodes(int* const nodes, int* const prios, const int nb_nodes, int* const ordered_nodes, int* const ordered_prios)
{
  int child = -1;
  int parent = -1;
  int n_children = 0;
  int ordered_nodes_index = 0;
  struct nm_coll_tree_info_s tree;

  nm_coll_tree_init(&tree, NM_COLL_TREE_BINOMIAL, nb_nodes, 0, 0);

  for (int step = 0; step < tree.height; step++)
    {
      for (int node = 0; node  < nb_nodes-1; node++)
        {
          nm_coll_tree_init(&tree, NM_COLL_TREE_BINOMIAL, nb_nodes, node, 0);
          nm_coll_tree_step(&tree, step, &parent, &child, &n_children);

          if (n_children > 0)
            {
              assert(n_children == 1); // it's a *bi*nomial tree

              ordered_nodes[child-1] = nodes[ordered_nodes_index];
              ordered_prios[child-1] = prios[ordered_nodes_index];
              ordered_nodes_index++;
            }
        }
    }
}

/**
 * Depth first search traversal of a binary tree to reorder nodes according to priorities.
 * - Nodes in `unordered_nodes` are sorted in priority-descending order.
 * - Nodes in `ordered_nodes` will be ordered such as our execution of the binary tree will send
 *   data to nodes respecting priorities (ie: nodes with higher priority will receive data first)
 * - `n` is the number of recipient nodes, so *excluding* src node
 */
static inline void binary_reorder_nodes(int* const unordered_nodes, int* const ordered_nodes, int* const unordered_prios, int* const ordered_prios, const int n, int* const ordered_i, const int unordered_i)
{
        if (unordered_i > 0)
        {
                ordered_nodes[*ordered_i] = unordered_nodes[unordered_i-1];
                ordered_prios[*ordered_i] = unordered_prios[unordered_i-1];
                (*ordered_i)++;
        }

        if ((2*unordered_i+1) <= n)
        {
                binary_reorder_nodes(unordered_nodes, ordered_nodes, unordered_prios, ordered_prios, n, ordered_i, (2*unordered_i+1));

                if ((2*unordered_i+2) <= n)
                {
                        binary_reorder_nodes(unordered_nodes, ordered_nodes, unordered_prios, ordered_prios, n, ordered_i, (2*unordered_i+2));
                }
        }
}



/**
 * @param nodes: destinatory nodes, excluding src node
 * @param nb_nodes: nb_nodes in multicast, including src node
 *
 * Works only with *BI*nomial trees and binary trees
 */
void send_multicast(int* nodes, int* prios, int nb_nodes, nm_tag_t tag, struct nm_data_s* body_data,
                    nm_len_t body_len, int tree_step, int tree_additionnal_info, int src_node,
                    struct nm_multicast_req_s* initial_recv, void (*end_send_callback)(void*), void* arg)
{
  assert(body_data != NULL);
  assert(nb_nodes > 1);

  int dest_id = -1;
  int nb_nodes_in_forward = -1;
  int is_local_last_send = -1;
  void* first_node_in_forward_list = NULL;
  void* first_prio_in_forward_list = NULL;
  int new_tree_additionnal_info = -1;
  struct nm_coll_tree_info_s tree;

  int rank = -1;
  nm_launcher_get_rank(&rank);

  if(src_node == rank && tree_step == 0) // First call to send_multicast, from src node
    {
      /** We reorder node list in order to nodes with higher priority will receive their data first.
        * The only requirement is that array `prios` has to be sorted in descending order (and array `nodes`
        * ordered such as prios[i] is the priority of the send to the node nodes[i]) */
      if (diffusion_tree_type == NM_COLL_TREE_BINARY || diffusion_tree_type == NM_COLL_TREE_BINOMIAL)
        {
          int* ordered_nodes = padico_malloc((nb_nodes-1) * sizeof(int));
          int* ordered_prios = padico_malloc((nb_nodes-1) * sizeof(int));
          if(diffusion_tree_type == NM_COLL_TREE_BINOMIAL)
            {
              binomial_reorder_nodes(nodes, prios, nb_nodes, ordered_nodes, ordered_prios);
            }
          else if (diffusion_tree_type == NM_COLL_TREE_BINARY)
            {
              int ordered_i = 0;
              binary_reorder_nodes(nodes, ordered_nodes, prios, ordered_prios, nb_nodes-1, &ordered_i, 0);
            }
          else
            {
              NM_FATAL("Impossible happened !\n");
            }
          padico_free(nodes);
          padico_free(prios);
          nodes = ordered_nodes;
          prios = ordered_prios;
        }
      else if (diffusion_tree_type != NM_COLL_TREE_CHAIN) // Chain tree doesn't need reordering
        {
          NM_FATAL("Unsupported type of tree.\n");
        }
    }

  if (diffusion_tree_type == NM_COLL_TREE_CHAIN)
    {
      dest_id = 0;
      nb_nodes_in_forward = nb_nodes-2; // nb_nodes includes src node
      first_node_in_forward_list = (void*) (nodes + 1);
      first_prio_in_forward_list = (void*) (prios + 1);
      is_local_last_send = 1;
      new_tree_additionnal_info = 0; // unused
    }
  else
    {
      nm_coll_tree_init(&tree, diffusion_tree_type, nb_nodes, 0, 0);

      if(diffusion_tree_type == NM_COLL_TREE_BINOMIAL)
        {
          assert(tree.max_arity <= 1);

          int parent = -1; // unused, but required by nm_coll_tree_step
          int n_children = 0;

          nm_coll_tree_step(&tree, tree_step, &parent, &dest_id, &n_children);

          assert(n_children == 1);

          dest_id--;
          // tree_additionnal_info is separator id in nodes array
          nb_nodes_in_forward = tree_additionnal_info - dest_id - 2;

          first_node_in_forward_list = (void*) (nodes + dest_id + 1);
          first_prio_in_forward_list = (void*) (prios + dest_id + 1);
          is_local_last_send = (tree_step == (tree.height-1));
          new_tree_additionnal_info = dest_id + 1; // not working when n_children > 1
        }
      else if (diffusion_tree_type == NM_COLL_TREE_BINARY)
        {
          assert(tree.max_arity == 2);

          if (tree_step == 0) // left subtree
            {
              dest_id = 0;
              first_node_in_forward_list = (void*) (nodes + 1);
              first_prio_in_forward_list = (void*) (prios + 1);
              int right_dest_id = nm_coll_binary_nb_nodes_in_left_subtree(&tree);
              nb_nodes_in_forward = right_dest_id - 1;
              is_local_last_send = (right_dest_id >= nb_nodes-1);
              new_tree_additionnal_info = right_dest_id;
            }
          else if (tree_step == 1) // right subtree
            {
              dest_id = tree_additionnal_info;
              nb_nodes_in_forward = nb_nodes - dest_id - 2;
              first_node_in_forward_list = (void*) (nodes + dest_id + 1);
              first_prio_in_forward_list = (void*) (prios + dest_id + 1);
              is_local_last_send = 1;
            }
          else
            {
              NM_FATAL("Binary tree cannot have more than two steps !\n");
            }
        }
      else
        {
          NM_FATAL("Unsupported type of tree\n");
        }
    }

  assert(dest_id > -1);
  assert(nb_nodes_in_forward > -1);
  assert(first_node_in_forward_list != NULL);
  assert(first_prio_in_forward_list != NULL);
  assert(is_local_last_send >= 0);
  assert(is_local_last_send == 1 || new_tree_additionnal_info >= 0);

  /*printf("[%d] sends to %d with prio %d\n", rank, nodes[dest_id], prios[dest_id]);*/

  struct nm_multicast_req_s* p_req = nm_multicast_req_malloc(nm_multicast_req_allocator);
  p_req->ref_header = padico_malloc(sizeof(struct multicast_header_s));
  p_req->ref_header->body_len = body_len;
  p_req->ref_header->nb_nodes = nb_nodes_in_forward;
  p_req->ref_header->src_node = src_node;
  p_req->p_session = NULL;
  p_req->nb_nodes_in_tree = nb_nodes;
  p_req->tree_step = tree_step;
  p_req->body_len = body_len;
  p_req->tag = tag;
  p_req->initial_recv = initial_recv;
  p_req->end_send_callback = end_send_callback;
  p_req->end_send_callback_arg = arg;
  p_req->request = NULL;
  p_req->copied_data_ptr = NULL;

  if(rank == src_node)
    {
      p_req->type = tree_step ? FORWARD_INITIAL : INITIAL_SEND;
    }
  else
    {
      p_req->type = FORWARD_RECV;
    }

  int comm_size;
  nm_launcher_get_size(&comm_size);

  assert(nodes[dest_id] < comm_size);
  assert(nodes[dest_id] != src_node);

  nm_gate_t gate = NULL;
  nm_launcher_get_gate(nodes[dest_id], &gate);

  p_req->body_data = *body_data;
  p_req->ref_nodes = nodes;
  p_req->ref_prios = prios;
  p_req->last = is_local_last_send;
  p_req->tree_additionnal_info = new_tree_additionnal_info;


  assert(rpc_service != NULL);
  nm_rpc_req_t rpc_req = nm_rpc_req_init(rpc_service, gate, tag);
  nm_rpc_req_pack_header(rpc_req, p_req->ref_header, sizeof(struct multicast_header_s));
  if (nb_nodes_in_forward > 0)
    {
      nm_rpc_req_pack_body(rpc_req, first_node_in_forward_list, nb_nodes_in_forward * sizeof(int));
      nm_rpc_req_pack_body(rpc_req, first_prio_in_forward_list, nb_nodes_in_forward * sizeof(int));
    }
  nm_rpc_req_pack_body_data(rpc_req, &p_req->body_data);
  nm_rpc_req_set_notifier(rpc_req, &monitor_send, p_req);
  nm_rpc_req_set_priority(rpc_req, prios[dest_id]);
  nm_rpc_req_isend(rpc_req);
}

void nm_coll_dynamic_send(int* nodes, int* prios, int nb_nodes, nm_tag_t tag, void* data, nm_len_t body_len,
                          void (*end_send_callback)(void*), void* arg)
{
  int rank = -1;
  nm_launcher_get_rank(&rank);

  struct nm_data_s body_data;
  nm_data_contiguous_build(&body_data, data, body_len);

  send_multicast(nodes, prios, nb_nodes+1, tag, &body_data, body_len, 0, nb_nodes+1, rank, NULL, end_send_callback, arg);
}

void nm_coll_dynamic_send_data(int* nodes, int* prios, int nb_nodes, nm_tag_t tag, struct nm_data_s* body_data, nm_len_t body_len,
                        void (*end_send_callback)(void*), void* arg)
{
  int rank = -1;
  nm_launcher_get_rank(&rank);

  send_multicast(nodes, prios, nb_nodes+1, tag, body_data, body_len, 0, nb_nodes+1, rank, NULL, end_send_callback, arg);
}

static void multicast_recv_handler_prepare_body(struct nm_multicast_req_s* p_req)
{
  assert(p_req->type == PENDING);
  assert(p_req->ref_header != NULL);
  assert(p_req->initial_recv != NULL);
  assert(p_req->initial_recv->type == USER_RECV);

  if (p_req->initial_recv->come_from_coop_callback != NULL)
    {
      (*p_req->initial_recv->come_from_coop_callback)(&(p_req->initial_recv->body_data), p_req->ref_header->body_len,
                                                      p_req->initial_recv->come_from_coop_callback_arg);
    }
  else
    {
      /*
       * In some use of coll_dynamic (ex: StarPU/nmad when datatype is unknown),
       * the come_from_coop_callback allocates memory to recv data, thus this
       * assert cannot be tested, because the size to allocate is unknown before
       * receiving the header.
       */
      assert(p_req->ref_header->body_len == p_req->initial_recv->body_len);
    }


  const int nb_nodes = p_req->ref_header->nb_nodes;

  assert(!nm_data_isnull(&p_req->initial_recv->body_data));

  p_req->ref_nodes = padico_malloc(nb_nodes * sizeof(int));
  p_req->ref_prios = padico_malloc(nb_nodes * sizeof(int));

  nm_rpc_irecv_body(p_req->rpc_token, p_req->ref_nodes, nb_nodes * sizeof(int));
  nm_rpc_irecv_body(p_req->rpc_token, p_req->ref_prios, nb_nodes * sizeof(int));
  nm_rpc_irecv_body_data(p_req->rpc_token, &p_req->initial_recv->body_data);
}


/* This function is called only as RPC handler, so no matching with user request is done yet. */
static void rpc_coll_dynamic_handler(nm_rpc_token_t p_token)
{
  struct nm_multicast_req_s* p_req = nm_multicast_req_malloc(nm_multicast_req_allocator);
  p_req->p_session = NULL;
  p_req->initial_recv = NULL;
  p_req->type = PENDING;
  p_req->ref_header = padico_malloc(sizeof(struct multicast_header_s));
  nm_data_contiguous_build(&p_req->header_data, p_req->ref_header, sizeof(struct multicast_header_s));
  nm_data_null_build(&p_req->body_data);
  p_req->request = NULL;
  p_req->copied_data_ptr = NULL;
  p_req->nb_nodes_in_tree = 0;

  p_req->rpc_token = p_token;
  nm_rpc_token_set_ref(p_token, p_req);

  assert(p_req->ref_header != NULL);
  nm_rpc_recv_header(p_token, p_req->ref_header, sizeof(struct multicast_header_s));

  nm_tag_t tag = nm_rpc_get_tag(p_token);

  struct _src_tag_req_key_s key = {
    .src_node = p_req->ref_header->src_node,
    .tag = tag
  };

  piom_spin_lock(&hashtables_lock);
  struct _src_tag_req_key_helement_s* helement = src_tag_req_hashtable_lookup(recv_requests, &key);

  if (helement == NULL) // recv request wasn't posted by the user yet
    {
      helement = padico_malloc(sizeof(struct _src_tag_req_key_helement_s));
      helement->key = key;
      helement->p_req = p_req;

      struct _src_tag_req_key_helement_s* unexpected_helement = src_tag_req_hashtable_lookup(unexpected_incoming_requests, &key);
      if (unexpected_helement != NULL)
        {
          NM_FATAL("A same unexpected request is already waiting a recv (from %d, tag %lx).", key.src_node, key.tag);
        }

      src_tag_req_hashtable_insert(unexpected_incoming_requests, &helement->key, helement);
      piom_spin_unlock(&hashtables_lock);

      nm_rpc_token_delay(p_token);
    }
  else // recv user was posted
    {
      src_tag_req_hashtable_remove(recv_requests, &key);
      struct _src_tag_req_key_helement_s* helement2 = src_tag_req_hashtable_lookup(recv_requests, &key);
      piom_spin_unlock(&hashtables_lock);
      assert(helement2 == NULL);

      assert(helement->p_req->type == USER_RECV);

      p_req->initial_recv = helement->p_req;

      int ret = nm_sr_rcancel(helement->p_req->p_session, helement->p_req->request);
      assert(ret == NM_ESUCCESS);

      padico_free(helement);

      multicast_recv_handler_prepare_body(p_req);
    }
}


void rpc_coll_dynamic_finalize(nm_rpc_token_t p_token)
{
  struct nm_multicast_req_s* p_req = (struct nm_multicast_req_s*) nm_rpc_token_get_ref(p_token);

  assert(p_req != NULL);
  assert(p_req->type == PENDING);
  assert(p_req->initial_recv != NULL);
  assert(p_req->initial_recv->type == USER_RECV);

  nm_tag_t tag = nm_rpc_get_tag(p_token);

  if(p_req->ref_header->nb_nodes > 0)
    {
      if (copy_before_forward)
        {
          // copy received data in our own buffer:
          nm_len_t body_data_size = nm_data_size(&p_req->initial_recv->body_data);
          /* body_len can be different of body_data_size: its value is provided by the application
           * and has a meaning only for the application. So we cannot use it here to know the size
           * of the data */
          p_req->initial_recv->copied_data_ptr = padico_malloc(body_data_size);
          struct nm_data_s body_data_origin = p_req->initial_recv->body_data;
          nm_data_contiguous_build(&p_req->initial_recv->body_data, p_req->initial_recv->copied_data_ptr, body_data_size);
          nm_data_copy(&p_req->initial_recv->body_data, &body_data_origin);

          // make received data available for the application:
          return_recv_data_to_app(p_req->initial_recv);
        }

      // start forwarding received data to other nodes:
      send_multicast(p_req->ref_nodes, p_req->ref_prios, p_req->ref_header->nb_nodes + 1,
          tag, &p_req->initial_recv->body_data,
          p_req->ref_header->body_len, 0, p_req->ref_header->nb_nodes + 1,
          p_req->ref_header->src_node, p_req->initial_recv,
          p_req->end_send_callback, p_req->end_send_callback_arg);
    }
  else
    {
      free_request_nodes_data(p_req);
    }

  padico_free(p_req->ref_header);
  nm_multicast_req_free(nm_multicast_req_allocator, p_req);
}


void nm_coll_dynamic_shutdown(void)
{
  piom_spin_lock(&rpc_service_ref_lock);

  nm_rpc_unregister(rpc_service);
  rpc_service = NULL;

  src_tag_req_hashtable_delete(unexpected_incoming_requests);
  src_tag_req_hashtable_delete(recv_requests);

  piom_spin_destroy(&hashtables_lock);
  piom_spin_unlock(&rpc_service_ref_lock);
  piom_spin_destroy(&rpc_service_ref_lock);

  nm_session_close(coll_dynamic_session);

  nm_multicast_req_allocator_delete(nm_multicast_req_allocator);
}

void nm_coll_dynamic_init_multicast()
{
  nm_multicast_req_allocator = nm_multicast_req_allocator_new(8);

  unexpected_incoming_requests = src_tag_req_hashtable_new();
  recv_requests = src_tag_req_hashtable_new();

  piom_spin_init(&hashtables_lock);
  piom_spin_init(&rpc_service_ref_lock);

  nm_session_open(&coll_dynamic_session, "nm_coll_dynamic");

  const char* s_coll_dynamic_tree = getenv("NMAD_COLL_DYNAMIC_TREE");
  if (s_coll_dynamic_tree)
    {
      if (strcmp(s_coll_dynamic_tree, "binary") == 0)
        {
          diffusion_tree_type = NM_COLL_TREE_BINARY;
        }
      else if (strcmp(s_coll_dynamic_tree, "chain") == 0)
        {
          diffusion_tree_type = NM_COLL_TREE_CHAIN;
        }
      else
        {
          if (strcmp(s_coll_dynamic_tree, "binomial") != 0)
            {
              NM_WARN("Only binary, binomial and chain trees are supported for coll_dynamic. Will use binomial instead.\n");
            }
          diffusion_tree_type = NM_COLL_TREE_BINOMIAL; // default
        }
  }

  const char* s_copy_before_forward = getenv("NMAD_COLL_DYNAMIC_COPY");
  if (s_copy_before_forward)
    {
      copy_before_forward = atoi(s_copy_before_forward);
    }

  piom_spin_lock(&rpc_service_ref_lock);
  rpc_service = nm_rpc_register(coll_dynamic_session, 0, NM_TAG_MASK_NONE, &rpc_coll_dynamic_handler, &rpc_coll_dynamic_finalize, NULL);
  piom_spin_unlock(&rpc_service_ref_lock);
}

void normal_receive_handler(nm_sr_event_t event, const nm_sr_event_info_t*p_info, void* _ref)
{
  nm_sr_request_t* p_request = p_info->req.p_request;

  assert(!((event & NM_SR_EVENT_FINALIZED) && (event & NM_SR_EVENT_RECV_DATA)));

  // If request was cancelled (ie nm_sr_cancel was called), it was cancelled by this interface
  // and thus all the following actions (remove, signal, call callbacks) are done elsewhere.
  // To cancel a request that registered with nm_coll_dynamic_register_recv*, call nm_coll_dynamic_remove_recv
  if (nm_sr_request_test(p_request, NM_STATUS_UNPACK_CANCELLED))
    {
      return;
    }

  struct nm_multicast_req_s* p_req = _ref;
  assert(p_req->type == USER_RECV);

  if(event == NM_SR_EVENT_RECV_DATA && p_req->normal_recv_recv_data_callback != NULL)
    {
      (*p_req->normal_recv_recv_data_callback)(p_request, p_req->normal_recv_recv_data_callback_arg);
    }

  if(event == NM_SR_EVENT_FINALIZED)
    {
      assert(nm_sr_request_test(p_request, NM_STATUS_FINALIZED));

      if (p_req->cond_wait_recv != NULL)
        {
          piom_cond_signal(p_req->cond_wait_recv, 1);
        }

      if (p_req->end_recv_callback != NULL)
        {
          (*p_req->end_recv_callback)(p_req->end_recv_callback_arg);
        }

      // remove recv:
      struct _src_tag_req_key_s key = {
        .src_node = p_req->src_node,
        .tag = p_req->tag
      };

      piom_spin_lock(&hashtables_lock);
      struct _src_tag_req_key_helement_s* helement = src_tag_req_hashtable_lookup(recv_requests, &key);
      assert(helement != NULL);
      assert(helement->p_req == p_req);

      src_tag_req_hashtable_remove(recv_requests, &key);
      piom_spin_unlock(&hashtables_lock);
      padico_free(helement);

      nm_multicast_req_free(nm_multicast_req_allocator, p_req);
    }
}

void register_recv_data(nm_session_t p_session, int src_node, nm_tag_t tag, struct nm_data_s* data,
                        nm_len_t len, nm_sr_request_t *p_request, piom_cond_t* cond_wait_recv,
                        callback_function_come_from_coop come_from_coop_callback, callback_function end_recv_callback,
                        callback_function_normal_recv normal_recv_callback, void* arg)
{
  nm_gate_t gate = NULL;
  nm_launcher_get_gate(src_node, &gate);

  struct nm_multicast_req_s* p_req = nm_multicast_req_malloc(nm_multicast_req_allocator);
  p_req->type = USER_RECV;
  p_req->initial_recv = NULL;
  p_req->body_len = len;
  if (data == NULL)
    {
      nm_data_null_build(&p_req->body_data);

      if (normal_recv_callback == NULL)
        {
          NM_WARN("You didn't provide nm_data for data neither callback to handle normal receive.\n");
          NM_WARN("NewMadeleine will not be able to store received data !\n");
        }
    }
  else
    {
      p_req->body_data = *data;
    }
  p_req->request = p_request;
  p_req->come_from_coop_callback = come_from_coop_callback;
  p_req->come_from_coop_callback_arg = arg;
  p_req->tag = tag;
  p_req->src_node = src_node;
  p_req->cond_wait_recv = cond_wait_recv;
  p_req->end_recv_callback = end_recv_callback;
  p_req->end_recv_callback_arg = arg;
  p_req->p_session = p_session;
  p_req->normal_recv_recv_data_callback = normal_recv_callback;
  p_req->normal_recv_recv_data_callback_arg = arg;
  p_req->copied_data_ptr = NULL;
  p_req->nb_nodes_in_tree = 0;

  struct _src_tag_req_key_s key = {
    .src_node = src_node,
    .tag = tag
  };

  int rank = -1;
  nm_launcher_get_rank(&rank);

  /*printf("[%d] nmad: handle recv src = %d, tag = %lx \n", rank, src_node, tag);*/
  piom_spin_lock(&hashtables_lock);
  struct _src_tag_req_key_helement_s* helement = src_tag_req_hashtable_lookup(unexpected_incoming_requests, &key);

  if (helement != NULL) // a collective request already arrived for this user recv
    {
      src_tag_req_hashtable_remove(unexpected_incoming_requests, &key);
      piom_spin_unlock(&hashtables_lock);

      assert(helement->p_req->type == PENDING);
      helement->p_req->initial_recv = p_req;

      multicast_recv_handler_prepare_body(helement->p_req);
      nm_rpc_token_complete(helement->p_req->rpc_token);

      padico_free(helement);
      /*printf("[%d] nmad: data for recv src = %d, tag = %lx already received\n", rank, src_node, tag);*/
    }
  else // data not yet arrived
    {
      struct _src_tag_req_key_helement_s* helement = src_tag_req_hashtable_lookup(recv_requests, &key);
      if (helement != NULL)
        {
          NM_FATAL("The same recv is already posted: src = %d, tag = %lx.", src_node, tag);
        }

      helement = padico_malloc(sizeof(struct _src_tag_req_key_helement_s));
      helement->key = key;
      helement->p_req = p_req;

      src_tag_req_hashtable_insert(recv_requests, &helement->key, helement);

      if(data == NULL)
        {
          nm_sr_recv_init(p_session, p_request);
          nm_sr_recv_irecv(p_session, p_request, gate, tag, NM_TAG_MASK_FULL);
        }
      else
        {
          nm_sr_irecv_data(p_session, gate, tag, data, p_request);
        }
      piom_spin_unlock(&hashtables_lock);
      nm_sr_request_set_ref(p_req->request, p_req);
      nm_sr_request_monitor(p_session, p_req->request, NM_SR_EVENT_RECV_DATA | NM_SR_EVENT_FINALIZED, &normal_receive_handler);
    }
}

void nm_coll_dynamic_recv_register_recv(nm_session_t p_session, int src_node, nm_tag_t tag, void *data,
                                        nm_len_t len, nm_sr_request_t *p_request, piom_cond_t* cond_wait_recv,
                                        callback_function_come_from_coop come_from_coop_callback,
                                        callback_function end_recv_callback, callback_function_normal_recv normal_recv_callback,
                                        void* arg)
{
  struct nm_data_s body_data;
  nm_data_contiguous_build(&body_data, data, len);

  register_recv_data(p_session, src_node, tag, &body_data, len, p_request, cond_wait_recv, come_from_coop_callback, end_recv_callback, normal_recv_callback, arg);
}


void nm_coll_dynamic_recv_register_recv_data(nm_session_t p_session, int src_node, nm_tag_t tag,
                                             struct nm_data_s* body_data, nm_len_t len,
                                             nm_sr_request_t *p_request, piom_cond_t* cond_wait_recv,
                                             callback_function_come_from_coop come_from_coop_callback,
                                             callback_function end_recv_callback,
                                             callback_function_normal_recv normal_recv_callback, void* arg)
{
  register_recv_data(p_session, src_node, tag, body_data, len, p_request, cond_wait_recv, come_from_coop_callback, end_recv_callback, normal_recv_callback, arg);
}

/**
 * return same as nm_sr_rcancel()
 */
int nm_coll_dynamic_remove_recv(int src_node, nm_tag_t tag)
{
  struct _src_tag_req_key_s key = {
    .src_node = src_node,
    .tag = tag
  };

  piom_spin_lock(&hashtables_lock);
  struct _src_tag_req_key_helement_s* helement = src_tag_req_hashtable_lookup(recv_requests, &key);

  if (helement == NULL)
    {
      piom_spin_unlock(&hashtables_lock);
      return -NM_ENOTPOSTED;
    }

  struct nm_multicast_req_s* p_req = helement->p_req;
  assert(p_req->type == USER_RECV);

  int ret = nm_sr_rcancel(p_req->p_session, p_req->request);

  src_tag_req_hashtable_remove(recv_requests, &key);
  piom_spin_unlock(&hashtables_lock);

  padico_free(helement);
  if (p_req->cond_wait_recv != NULL)
    {
      piom_cond_destroy(p_req->cond_wait_recv);
    }

  nm_multicast_req_free(nm_multicast_req_allocator, p_req);

  return ret;
}


void nm_coll_dynamic_set_tree_kind(nm_coll_tree_kind_t kind)
{
  if (kind != NM_COLL_TREE_BINARY && kind != NM_COLL_TREE_BINOMIAL && kind != NM_COLL_TREE_CHAIN)
    {
      NM_WARN("Only binary, binomial and chain trees are supported for coll_dynamic. Will use binomial instead.\n");
      diffusion_tree_type = NM_COLL_TREE_BINOMIAL;
    }
  else
    {
      diffusion_tree_type = kind;
    }
}

#endif /* PIOMAN */

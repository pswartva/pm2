/* full-bench.c
 */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <netdb.h>
#include <Padico/Puk.h>
#include <Padico/PadicoTM.h>
#include <Padico/NetAccess.h>
#include <Padico/Circuit.h>
#include <Padico/VSock.h>

#include "timing.h"

#include <Padico/Module.h>

#define DEFAULT_PATH       "/tmp/"
#define DEFAULT_MAX_LEN    8*1024*1024
#define DEFAULT_ROUNDTRIP  100
#define DEFAULT_PORT       5050

static char*path          = DEFAULT_PATH;
static long maxlen        = DEFAULT_MAX_LEN;
static int  num_roundtrip = DEFAULT_ROUNDTRIP;
static char *buffer = NULL;
static marcel_mutex_t mutex;
static marcel_cond_t cond;

static char*server_name = NULL;

static int port = DEFAULT_PORT;
static int rank = -1;
static int nproc = -1;

static padico_xchn_t xchn = NULL;

static padico_circuit_t circuit = NULL;
static padico_group_t   group = NULL;

static struct sockaddr_in local_addr;
static socklen_t          local_addr_size  = sizeof(local_addr);
static struct sockaddr_in remote_addr;
static socklen_t          remote_addr_size = sizeof(remote_addr);
static padico_vsock_t     bench_fd;

static timing_t tick1, tick2, tick3, tick4;
static double t1, t2, t3, t4, t5;

static void the_bench_xchn();
static void the_bench_circuit();
static void the_bench_vsock();
static void bench_init_xchn();
static void bench_init_circuit();
static void bench_init_vsock();

PADICO_DECLARE_INIT(
{
  char*c;
  int i;

  timing_init();

  rank   = padico_na_rank();
  nproc  = padico_na_size();
  server_name = padico_topo_nodename(0);
  padico_print("*** self = %s\n", padico_module_self_name());
  c = padico_getattr("path");
  if(c) path = c;
  c = padico_getattr("maxlen");
  if(c) maxlen = atoi(c);
  c = padico_getattr("roundtrip");
  if(c) num_roundtrip = atoi(c);
  c = padico_getattr("server");
  if(c) server_name = c;
  c = padico_getattr("port");
  if(c) port = atoi(c);
  padico_print("BENCH: [path]      = %s\n", path);
  padico_print("BENCH: [maxlen]    = %d\n", maxlen);
  padico_print("BENCH: [roundtrip] = %d\n", num_roundtrip);
  padico_print("BENCH: [server]    = %s\n", server_name);
  padico_print("BENCH: [port]      = %d\n", port);
  if ( (buffer = (char *) malloc((unsigned) maxlen)) == (char *) NULL)
    {
      padico_fatal("process %d could not allocate buffer of size %d\n",rank, maxlen);
      return;
    }
  for(i=0; i<maxlen; i++)
    buffer[i]=i;
  marcel_mutex_init(&mutex, NULL);
  marcel_cond_init(&cond, NULL);
  bench_init_xchn();
  bench_init_circuit();
  bench_init_vsock();
});

PADICO_DECLARE_RUN(argc, argv,
{
  vsock_disable(bench_fd);
  the_bench_xchn();
  padico_print("BENCH: XChn completed\n");
  the_bench_circuit();
  padico_print("BENCH: Circuit completed\n");
  vsock_enable(bench_fd);
  the_bench_vsock();
  padico_print("BENCH: VSock completed\n");
  padico_print("BENCH: completed!\n");
});

PADICO_DECLARE_FINALIZE(
{
  free(buffer);
  padico_print("BENCH: finalizing\n");
});

static void xchn_callback(padico_xchn_connection_t in_conn)
{
  int *size = padico_xchn_get_rbuf();
  int k;
  timing_get_tick(&tick3);
  padico_xchn_unpack(in_conn, buffer, *size, mad_send_CHEAPER, mad_receive_CHEAPER);
  padico_xchn_end_unpacking(in_conn);
  marcel_cond_signal(&cond);
  timing_get_tick(&tick4);
}

static void bench_init_xchn()
{
  xchn = padico_xchn_init("full-bench-xchn", &xchn_callback);
  padico_tm_usleep(200);
}

static void circuit_callback(padico_circuit_connection_t in_conn)
{
  int *size = padico_circuit_get_rbuf(in_conn);
  marcel_mutex_lock(&mutex);
  padico_circuit_unpack(in_conn, buffer, *size, mad_send_CHEAPER, mad_receive_CHEAPER);
  padico_circuit_end_unpacking(in_conn);
  marcel_mutex_unlock(&mutex);
  marcel_cond_signal(&cond);
}

static void bench_init_circuit()
{
  group = padico_group_init();
  padico_group_addnode(group, 0);
  padico_group_addnode(group, 1);
  padico_group_store(group, "full-bench-group");
  circuit = padico_circuit_init("full-bench-group", "full-bench-circuit");
  padico_circuit_start(circuit, &circuit_callback);
}

static void bench_init_vsock()
{
  if(rank == 0)
    {
      padico_vsock_t fd;
      int rc;
      fd = padico_socket(); /* TODO: changer -> (AF_INET, SOCK_STREAM, 0) */
      local_addr.sin_family = AF_INET;
      local_addr.sin_port  = port;
      rc = padico_bind(fd, (struct sockaddr*)&local_addr, local_addr_size);
      if(rc != 0)
        padico_fatal("cannot bind on port %d\n", port);
      /*      padico_listen(fd, 2); */
      bench_fd = padico_accept(fd, (struct sockaddr*)&remote_addr, &remote_addr_size);
    }
  else
    {
      struct hostent*he;
      int rc;
      bench_fd = padico_socket();
      he = gethostbyname(server_name);
      memcpy(&remote_addr.sin_addr, he->h_addr_list[0], sizeof(remote_addr.sin_addr));
      remote_addr.sin_family = AF_INET;
      remote_addr.sin_port  = port;
      rc = padico_connect(bench_fd, (struct sockaddr_in*)&remote_addr, remote_addr_size);
      if(rc != 0)
        padico_fatal("cannot connect fd %d\n", bench_fd);
    }
}


static void the_bench_xchn()
{
  FILE*flog_time;
  FILE*flog_bw;
  int lenbuf;
  double bw_MBps;
  timing_t tick_start, tick_end;
  double total_time_usec;
  double avg_time_usec;
  int i;

 if(rank == 0)
    {
      padico_print("XChn bench ----------------------------------------\n");
      chdir(path);
      flog_time = fopen("XChn.t", "w");
      flog_bw   = fopen("XChn.bw", "w");
    }
  lenbuf = 1;
  while (lenbuf <= maxlen)
    {
      total_time_usec = 0;
      t1 = t2 = t3 = t4 = t5 = 0;
      for(i = 0; i < num_roundtrip ; i++)
        {
          timing_get_tick(&tick_start);
          if (rank == 0)
            {
              padico_xchn_connection_t out_conn;
              *((int*)padico_xchn_get_sbuf(xchn)) = lenbuf;
              memcpy((int*)padico_xchn_get_sbuf(xchn)+4, buffer, 12);
              out_conn = padico_xchn_begin_packing(xchn, 1);
              timing_get_tick(&tick1);
              padico_xchn_pack(out_conn, buffer, lenbuf, mad_send_CHEAPER, mad_receive_CHEAPER);
              padico_xchn_end_packing(out_conn);
              timing_get_tick(&tick2);
              marcel_mutex_lock(&mutex);
              marcel_cond_wait(&cond, &mutex);
              marcel_mutex_unlock(&mutex);
            }
          else
            {
              padico_xchn_connection_t out_conn;
              marcel_mutex_lock(&mutex);
              marcel_cond_wait(&cond, &mutex);
              marcel_mutex_unlock(&mutex);
              *((int*)padico_xchn_get_sbuf(xchn)) = lenbuf;
              memcpy((int*)padico_xchn_get_sbuf(xchn)+4, buffer, 12);
              out_conn = padico_xchn_begin_packing(xchn, 0);
              padico_xchn_pack(out_conn, buffer, lenbuf, mad_send_CHEAPER, mad_receive_CHEAPER);
              padico_xchn_end_packing(out_conn);
            }
          timing_get_tick(&tick_end);
          total_time_usec += timing_usec(tick_start, tick_end);
          t1 += timing_usec(tick_start, tick1);
          t2 += timing_usec(tick1, tick2);
          t3 += timing_usec(tick2, tick3);
          t4 += timing_usec(tick3, tick4);
          t5 += timing_usec(tick4, tick_end);
        }
      avg_time_usec = total_time_usec / (double)(num_roundtrip * 2);
      /* avg_time_usec is the average time for a send in microseconds */
      if (avg_time_usec > 0)    /* rate is megabytes per second */
        bw_MBps = (double)(lenbuf / avg_time_usec);
      else
        bw_MBps = 0.0;
      if (rank == 0)
        {
          padico_print("len=%d bytes, time=%f usec., bw=%f MB/s\n",
                       lenbuf, avg_time_usec, bw_MBps);
          /*      padico_print(" t1=%f t2=%f t3=%f t4=%f t5=%f\n",
                       t1/ (double)(num_roundtrip),
                       t2/ (double)(num_roundtrip),
                       t3/ (double)(num_roundtrip),
                       t4/ (double)(num_roundtrip),
                       t5/ (double)(num_roundtrip));*/
          fprintf(flog_time, "%d %f\n", lenbuf, avg_time_usec);
          fprintf(flog_bw, "%d %f\n", lenbuf, bw_MBps);
        }
      lenbuf *= 2;
    }
  if (rank == 0)
    {
      fclose(flog_time);
      fclose(flog_bw);
      padico_print("XChn bench ok -------------------------------------\n");
    }
}

static void the_bench_circuit()
{
  FILE*flog_time;
  FILE*flog_bw;
  int lenbuf;
  double bw_MBps;
  timing_t tick_start, tick_end;
  double total_time_usec;
  double avg_time_usec;
  int i;

 if(rank == 0)
    {
      padico_print("Circuit bench ----------------------------------------\n");
      chdir(path);
      flog_time = fopen("Circuit.t", "w");
      flog_bw   = fopen("Circuit.bw", "w");
    }
  lenbuf = 1;
  while (lenbuf <= maxlen)
    {
      total_time_usec = 0;
      for(i = 0; i < num_roundtrip ; i++)
        {
          timing_get_tick(&tick_start);
          if (rank == 0)
            {
              padico_circuit_connection_t out_conn;
              marcel_mutex_lock(&mutex);
              *((int*)padico_circuit_get_sbuf(circuit)) = lenbuf;
              out_conn = padico_circuit_begin_packing(circuit, 1);
              padico_circuit_pack(out_conn, buffer, lenbuf, mad_send_CHEAPER, mad_receive_CHEAPER);
              padico_circuit_end_packing(out_conn);
              marcel_cond_wait(&cond, &mutex);
              marcel_mutex_unlock(&mutex);
            }
          else
            {
              padico_circuit_connection_t out_conn;
              marcel_mutex_lock(&mutex);
              marcel_cond_wait(&cond, &mutex);
              *((int*)padico_circuit_get_sbuf(circuit)) = lenbuf;
              out_conn = padico_circuit_begin_packing(circuit, 0);
              padico_circuit_pack(out_conn, buffer, lenbuf, mad_send_CHEAPER, mad_receive_CHEAPER);
              padico_circuit_end_packing(out_conn);
              marcel_mutex_unlock(&mutex);
            }
          timing_get_tick(&tick_end);
          total_time_usec += timing_usec(tick_start, tick_end);
        }
      avg_time_usec = total_time_usec / (double)(num_roundtrip * 2);
      /* avg_time_usec is the average time for a send in microseconds */
      if (avg_time_usec > 0)    /* rate is megabytes per second */
        bw_MBps = (double)(lenbuf / avg_time_usec);
      else
        bw_MBps = 0.0;
      if (rank == 0)
        {
          padico_print("len=%d bytes, time=%f usec., bw=%f MB/s\n",
                       lenbuf, avg_time_usec, bw_MBps);
          fprintf(flog_time, "%d %f\n", lenbuf, avg_time_usec);
          fprintf(flog_bw, "%d %f\n", lenbuf, bw_MBps);
        }
      lenbuf *= 2;
    }
  if (rank == 0)
    {
      fclose(flog_time);
      fclose(flog_bw);
      padico_print("Circuit bench ok -------------------------------------\n");
    }
}


static void the_bench_vsock()
{
  FILE*flog_time;
  FILE*flog_bw;
  int lenbuf;
  double bw_MBps;
  timing_t tick_start, tick_end;
  double total_time_usec;
  double avg_time_usec;
  int i;

 if(rank == 0)
    {
      padico_print("VSock bench ----------------------------------------\n");
      chdir(path);
      flog_time = fopen("VSock.t", "w");
      flog_bw   = fopen("VSock.bw", "w");
    }
  lenbuf = 1;
  while (lenbuf <= maxlen)
    {
      total_time_usec = 0;
      for(i = 0; i < num_roundtrip ; i++)
        {
          timing_get_tick(&tick_start);
          if (rank == 0)
            {
              /* TODO: loop for send/recv */
              padico_send(bench_fd, buffer, lenbuf, 0);
              padico_recv(bench_fd, buffer, lenbuf, 0);
            }
          else
            {
              padico_recv(bench_fd, buffer, lenbuf, 0);
              padico_send(bench_fd, buffer, lenbuf, 0);
            }
          timing_get_tick(&tick_end);
          total_time_usec += timing_usec(tick_start, tick_end);
        }
      avg_time_usec = total_time_usec / (double)(num_roundtrip * 2);
      if (avg_time_usec > 0)    /* rate is megabytes per second */
        bw_MBps = (double)(lenbuf / avg_time_usec);
      else
        bw_MBps = 0.0;
      if (rank == 0)
        {
          padico_print("len=%d bytes, time=%f usec., bw=%f MB/s\n",
                       lenbuf, avg_time_usec, bw_MBps);
          fprintf(flog_time, "%d %f\n", lenbuf, avg_time_usec);
          fprintf(flog_bw, "%d %f\n", lenbuf, bw_MBps);
        }
      lenbuf *= 2;
    }
  if (rank == 0)
    {
      fclose(flog_time);
      fclose(flog_bw);
      padico_print("VSock bench ok -------------------------------------\n");
    }
}

/*
 * NewMadeleine
 * Copyright (C) 2014-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include <errno.h>

#include "../common/nm_examples_helper.h"

#ifdef PIOMAN
#include <pioman.h>
#endif /* PIOMAN */

#define MAXTHREADS 36
#define LOOPS 5000
#define ARRAY_SIZE (1024*16)
#define NBOPS 9


static void*array_compute(void*_dummy)
{
  float*array = malloc(sizeof(float) * ARRAY_SIZE);
  int i;
  for(i = 0; i < ARRAY_SIZE; i++)
    {
      array[i] = drand48();
    }
  int k;
  for(k = 0; k < LOOPS; k++)
    {
      for(i = 0; i < ARRAY_SIZE; i++)
        {
          const float a1 = array[(i + 1) % ARRAY_SIZE];
          const float a2 = array[(i + 2) % ARRAY_SIZE];
          const float a3 = array[(i + 3) % ARRAY_SIZE];
          array[i] = ((4.0 - a1) * (a2 + 5.0) * (a3 + 18.5)) / (a1 + a2 + a3 * 2.0);
        }
    }
  float result = array[0];
  free(array);
  return (void*)(uintptr_t)result;
}

int main(int argc, char**argv)
{
  nm_examples_init(&argc, argv);
  int n;
  for(n = 1; n <= MAXTHREADS; n++)
    {
      puk_tick_t t1, t2;
      piom_thread_t tid[n];
      int t;
      PUK_GET_TICK(t1);
      for(t = 0; t < n; t++)
        {
          piom_thread_create(&tid[t], NULL, &array_compute, NULL);
        }
      for(t = 0; t < n; t++)
        {
          piom_thread_join(tid[t]);
        }
      PUK_GET_TICK(t2);
      double delay = PUK_TIMING_DELAY(t1,t2);
      fprintf(stderr, "# thread = %d; t = %.2f usec.; %.2f Mflop/s\n",
              n, delay, (double)((long long)NBOPS * (long long)ARRAY_SIZE * (long long)LOOPS) / delay);
    }
  return 0;
}

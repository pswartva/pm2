/*
 * NewMadeleine
 * Copyright (C) 2014-2022 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <nm_public.h>
#include <nm_private.h>
#include <nm_sendrecv_interface.h>
#include <nm_launcher_interface.h>
#include <Padico/Puk.h>
#include "nm_coll_private.h"
#include <Padico/Module.h>

PADICO_MODULE_HOOK(nm_coll);


/* ** barrier ********************************************** */

void nm_coll_group_barrier(nm_session_t p_session, nm_group_t p_group, int self, nm_tag_t tag)
{
  assert(nm_group_get_gate(p_group, self) == nm_launcher_self_gate());
  const int root = 0;
  nm_coll_group_gather(p_session, p_group, root, self, NULL, 0, NULL, 0, tag);
  nm_coll_group_bcast(p_session, p_group, root, self, NULL, 0, tag);
}

void nm_coll_barrier(nm_comm_t p_comm, nm_tag_t tag)
{
  nm_coll_group_barrier(nm_comm_get_session(p_comm), nm_comm_group(p_comm), nm_comm_rank(p_comm), tag);
}


/* ** ibarrier ********************************************* */

/** status of a barrier */
struct nm_coll_barrier_s
{
  struct nm_coll_tree_status_s coll_tree;
  struct nm_data_s data;
  int step;  /**< current step in tree  */
  enum
    {
      NM_COLL_IBARRIER_GATHERING,
      NM_COLL_IBARRIER_BROADCASTING,
      NM_COLL_IBARRIER_COMPLETED
    } state;
};

static void nm_coll_ibarrier_step(struct nm_coll_barrier_s*p_barrier);
static void nm_coll_ibarrier_req_notifier(nm_sr_event_t event, const nm_sr_event_info_t*event_info, void*_ref);
static void nm_coll_ibarrier_destroy(struct nm_coll_req_s*p_coll_req);

static void nm_coll_ibarrier_step(struct nm_coll_barrier_s*p_barrier)
{
 restart:
  assert(p_barrier->coll_tree.pending_reqs == 0);
  if(p_barrier->state == NM_COLL_IBARRIER_GATHERING)
    {
      if(p_barrier->step < p_barrier->coll_tree.tree.height)
        {
          int parent = -1;
          int n_children = 0;
          nm_coll_tree_step(&p_barrier->coll_tree.tree, p_barrier->coll_tree.tree.height - p_barrier->step - 1,
                            &parent, p_barrier->coll_tree.children, &n_children);
          p_barrier->step++;
          assert(!((parent != -1) && (n_children > 0)));
          if(parent != -1)
            {
              nm_coll_tree_send(&p_barrier->coll_tree, parent, &p_barrier->data,
                                &p_barrier->coll_tree.parent_request);
              nm_coll_tree_set_notifier(&p_barrier->coll_tree, &p_barrier->coll_tree.parent_request,
                                        &nm_coll_ibarrier_req_notifier);
            }
          else if(n_children > 0)
            {
              int i;
              for(i = 0; i < n_children; i++)
                {
                  nm_coll_tree_recv(&p_barrier->coll_tree, p_barrier->coll_tree.children[i], &p_barrier->data,
                                    &p_barrier->coll_tree.children_requests[i]);
                }
              for(i = 0; i < n_children; i++)
                {
                  nm_coll_tree_set_notifier(&p_barrier->coll_tree, &p_barrier->coll_tree.children_requests[i],
                                            &nm_coll_ibarrier_req_notifier);
                }
            }
          else
            {
              assert((parent == -1) && (n_children == 0));
              goto restart;
            }
        }
      else
        {
          p_barrier->state = NM_COLL_IBARRIER_BROADCASTING;
          p_barrier->step = 0;
          goto restart;
        }
    }
  else if(p_barrier->state == NM_COLL_IBARRIER_BROADCASTING)
    {
      assert(p_barrier->coll_tree.pending_reqs == 0);
      if(p_barrier->step < p_barrier->coll_tree.tree.height)
        {
          int parent = -1;
          int n_children = 0;
          nm_coll_tree_step(&p_barrier->coll_tree.tree, p_barrier->step, &parent,
                            p_barrier->coll_tree.children, &n_children);
          p_barrier->step++;
          assert(!((parent != -1) && (n_children > 0)));
          if(parent != -1)
            {
              nm_coll_tree_recv(&p_barrier->coll_tree, parent, &p_barrier->data,
                                &p_barrier->coll_tree.parent_request);
              nm_coll_tree_set_notifier(&p_barrier->coll_tree, &p_barrier->coll_tree.parent_request,
                                        &nm_coll_ibarrier_req_notifier);
            }
          else if(n_children > 0)
            {
              int i;
              for(i = 0; i < n_children; i++)
                {
                  nm_coll_tree_send(&p_barrier->coll_tree, p_barrier->coll_tree.children[i], &p_barrier->data,
                                    &p_barrier->coll_tree.children_requests[i]);
                }
              for(i = 0; i < n_children; i++)
                {
                  nm_coll_tree_set_notifier(&p_barrier->coll_tree, &p_barrier->coll_tree.children_requests[i],
                                           &nm_coll_ibarrier_req_notifier);
                }
            }
          else
            {
              assert((parent == -1) && (n_children == 0));
              goto restart;
            }
        }
      else
        {
          p_barrier->state = NM_COLL_IBARRIER_COMPLETED;
          nm_coll_req_signal(nm_coll_req_container(p_barrier));
        }
    }
}

static void nm_coll_ibarrier_req_notifier(nm_sr_event_t event, const nm_sr_event_info_t*event_info, void*_ref)
{
  struct nm_coll_barrier_s*p_barrier = _ref;
  const int p = nm_atomic_dec(&p_barrier->coll_tree.pending_reqs);
  if(p == 0)
    {
      nm_coll_ibarrier_step(p_barrier);
    }
}

struct nm_coll_req_s*nm_coll_group_ibarrier(nm_session_t p_session, nm_group_t p_group, int self, nm_tag_t tag)
{
  const int root = 0;
  const nm_coll_tree_kind_t kind = nm_coll_tree_heuristic(nm_group_size(p_group), 0);
  struct nm_coll_req_s*p_coll_req = nm_coll_req_alloc(sizeof(struct nm_coll_barrier_s), NM_COLL_REQ_BARRIER, &nm_coll_ibarrier_destroy, NULL, NULL);
  struct nm_coll_barrier_s*p_barrier = nm_coll_req_payload(p_coll_req);
  nm_coll_tree_status_init(&p_barrier->coll_tree, p_session, kind, p_group, root, self, tag);
  nm_data_null_build(&p_barrier->data);
  p_barrier->state = NM_COLL_IBARRIER_GATHERING;
  p_barrier->step = 0;
  nm_coll_ibarrier_step(p_barrier);
  return p_coll_req;
}

static void nm_coll_ibarrier_destroy(struct nm_coll_req_s*p_coll_req)
{
  struct nm_coll_barrier_s*p_barrier = nm_coll_req_payload(p_coll_req);
  nm_coll_tree_status_destroy(&p_barrier->coll_tree);
}

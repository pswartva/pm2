/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file InfinibandVerbs.h
 *  @brief Infiniband Verbs topology detection and description
 */

#ifndef PADICO_INFINIBAND_VERBS_H
#define PADICO_INFINIBAND_VERBS_H

#include <Padico/Puk.h>
#include <Padico/Topology.h>

#include <infiniband/verbs.h>

PUK_MOD_AUTO_DEP(InfinibandVerbs, PADICO_LAST_MODULE_NAME);
#undef PADICO_LAST_MODULE_NAME
#define PADICO_LAST_MODULE_NAME InfinibandVerbs

/** an active IB port */
struct padico_ibv_port_s
{
  struct ibv_context*context; /**< IB context of the device */
  const char*devname;         /**< name of the HCA device */
  int      port_num;          /**< IB port number */
  uint64_t guid;              /**< GUID of the HCA device */
  uint64_t subnet;            /**< GID prefix of the local subnet */
  uint16_t port_lid;          /**< LID for the local IB port */
  padico_topo_network_t network;
};

PUK_VECT_TYPE(padico_ibv_port, struct padico_ibv_port_s*);

const struct padico_ibv_port_vect_s*padico_ibv_get_ports(void);


#endif /* PADICO_INFINIBAND_VERBS_H */

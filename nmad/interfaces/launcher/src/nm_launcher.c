/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <nm_private.h>
#include <nm_launcher.h>
#include <nm_core_interface.h>
#include <nm_session_interface.h>
#include <nm_coll_interface.h>
#ifdef PIOMAN
#include <pioman.h>
#endif
#ifdef NMAD_PADICOTM
#include <Padico/PadicoTM.h>
#include <Padico/PadicoBootLib.h>
#endif /* NMAD_PADICOTM */

#ifdef PUKABI
#include <Padico/Puk-ABI.h>
#endif /* PUKABI */

#include <Padico/Module.h>

PADICO_MODULE_BUILTIN(nm_launcher, NULL, NULL, NULL);

PADICO_MODULE_ATTR(ibverbs_srq, "NMAD_IBVERBS_SRQ", "enables the use of Shared Requests Queues for scalability in number of nodes. The latency penalty is low. It is used by default for > 16 nodes", bool, 0);

PADICO_MODULE_ATTR(ibverbs_rcache, "NMAD_IBVERBS_RCACHE", "enables the registration cache; the default choice is to use the rcache-mini backend, best used with the PukABI module.", bool, 1);


static void nm_launcher_url_destructor(char*p_driver_name, char*p_driver_url);

/** hashtable: driver name -> driver url */
PUK_HASHTABLE_TYPE(nm_url, char*, char*,
                   &puk_hash_string_default_hash, &puk_hash_string_default_eq, &nm_launcher_url_destructor);

PUK_HASHTABLE_TYPE(nm_attrs, const char*, char*,
                   &puk_hash_string_default_hash, &puk_hash_string_default_eq, NULL);

static struct
{
  puk_instance_t instance;                   /**< instance of NewMad_Launcher component */
  struct puk_receptacle_NewMad_Launcher_s r; /**< receptacle for launcher component */
  nm_core_t p_core;                          /**< nmad core object */
  nm_gate_t*gates;                           /**< pointer to the gate array, filled by the launcher component */
  int rank;                                  /**< rank of local node; -1 if not set */
  int size;                                  /**< number of gates; -1 if not set */
  struct nm_gate_reverse_hashtable_s reverse; /**< reverse table: p_gate -> rank */
  puk_mod_t boot_mod;                        /**< the mod that was used for init */
  int puk_init;                              /**< whether we initialized Puk ourself */
  int init_level;                            /**< number of stacked init */
  const char*local_url;        /**< the local nmad driver url */
  padico_string_t url_string;  /**< local url as a string */
  nm_launcher_selector_t selector;
  void*selector_arg;
  nm_drv_t p_drv_self_small;   /**< driver to connect to self */
  nm_drv_t p_drv_self_large;   /**< driver to connect to self */
  int wide_url;                /**< whether the launcher supports wide urls */
  int homogeneous_network;     /**< whether the network is homogeneous across all nodes (same list of drivers) */

} launcher =
  {
    .instance = NULL, .gates = NULL, .rank = -1, .size = -1,
    .boot_mod = NULL, .puk_init = 0, .init_level = 0,
    .local_url = NULL,
    .url_string = NULL,
    .selector  = NULL,
    .p_drv_self_small = NULL,
    .p_drv_self_large = NULL,
    .wide_url = 0,
    .homogeneous_network = 1

  };

/* ********************************************************* */

static void nm_launcher_cleanup(void) __attribute__((destructor));

static void nm_launcher_cleanup(void)
{
  if(launcher.init_level > 0)
    {
      fprintf(stderr, "# nm_launcher: %d pending uses on exit. NewMadeleine did not exit cleanly.\n", launcher.init_level);
    }
}


/* ********************************************************* */

static inline void nm_launcher_check_init(void)
{
  if(launcher.instance == NULL)
    {
      NM_FATAL("launcher not initialized.\n");
    }
}

int nm_launcher_get_rank(int*rank)
{
  nm_launcher_check_init();
  *rank = launcher.rank;
  return NM_ESUCCESS;
}

int nm_launcher_get_size(int*size)
{
  nm_launcher_check_init();
  *size  = launcher.size;
  return NM_ESUCCESS;
}

int nm_launcher_get_gate(int dest, nm_gate_t*pp_gate)
{
  nm_launcher_check_init();
  assert(launcher.gates != NULL);
  assert(dest >= 0 && dest < launcher.size);
  nm_gate_t p_gate = launcher.gates[dest];
  *pp_gate = p_gate;
  return NM_ESUCCESS;
}

int nm_launcher_get_dest(nm_gate_t p_gate, int*dest)
{
  nm_launcher_check_init();
  assert(p_gate != NULL);
  if(nm_gate_reverse_hashtable_probe(&launcher.reverse, p_gate))
    {
      *dest = nm_gate_reverse_hashtable_lookup(&launcher.reverse, p_gate);
      return NM_ESUCCESS;
    }
  else
    {
      *dest = -1;
      return -NM_EINVAL;
    }
}

void nm_launcher_abort(int rc)
{
  nm_launcher_check_init();
  (*launcher.r.driver->abort)(launcher.r._status, rc);
}

static inline void nm_launcher_isend(struct nm_core*p_core, struct nm_req_s*p_req, struct nm_data_s*p_data,
                                     struct nm_gate_s*p_gate, nm_tag_t tag)
{
  const nm_core_tag_t core_tag = nm_core_tag_build(0 /* session */, tag);
  nm_core_pack_init(p_core, p_req);
  nm_core_pack_data(p_core, p_req, p_data);
  nm_core_pack_send(p_core, p_req, core_tag, p_gate, NM_REQ_FLAG_NONE);
  nm_core_pack_submit(p_core, p_req);
}

static inline void nm_launcher_send(struct nm_core*p_core, struct nm_data_s*p_data,
                                    struct nm_gate_s*p_gate, nm_tag_t tag)
{
  struct nm_req_s req;
  nm_launcher_isend(p_core, &req, p_data, p_gate, tag);
  nm_status_wait(&req, NM_STATUS_FINALIZED, p_core);
}

static inline void nm_launcher_irecv(struct nm_core*p_core, struct nm_req_s*p_req, struct nm_data_s*p_data,
                                     struct nm_gate_s*p_gate, nm_tag_t tag)
{
  const nm_core_tag_t core_tag = nm_core_tag_build(0 /* session */, tag);
  const nm_core_tag_t core_mask = nm_core_tag_build(NM_CORE_TAG_HASH_FULL, NM_TAG_MASK_FULL);
  nm_core_unpack_init(p_core, p_req);
  nm_core_unpack_data(p_core, p_req, p_data);
  nm_core_unpack_match_recv(p_core, p_req, p_gate, core_tag, core_mask);
  nm_core_unpack_submit(p_core, p_req, NM_REQ_FLAG_NONE);
}

static inline void nm_launcher_recv(struct nm_core*p_core, struct nm_data_s*p_data,
                                    struct nm_gate_s*p_gate, nm_tag_t tag)
{
  struct nm_req_s req;
  nm_launcher_irecv(p_core, &req, p_data, p_gate, tag);
  nm_status_wait(&req, NM_STATUS_FINALIZED, p_core);
}

void nm_launcher_barrier(void)
{
  (*launcher.r.driver->barrier)(launcher.r._status);
}

/** Checks that we are linked with a nmad library using the same ABI
 * that the one we were built against. This function is called automatically
 * by the launcher.
 */
static inline void nm_abi_config_check(const struct nm_abi_config_s*p_nm_abi_config)
{
  static struct nm_abi_config_s nm_abi_config = { 0 };
  nm_abi_config_capture(&nm_abi_config);
  if( (nm_abi_config.enable_debug   != p_nm_abi_config->enable_debug)   ||
      (nm_abi_config.enable_pioman  != p_nm_abi_config->enable_pioman)  ||
      (nm_abi_config.enable_pthread != p_nm_abi_config->enable_pthread) ||
      (nm_abi_config.enable_marcel  != p_nm_abi_config->enable_marcel)  ||
      (nm_abi_config.enable_pukabi  != p_nm_abi_config->enable_pukabi)  ||
      (nm_abi_config.enable_simgrid != p_nm_abi_config->enable_simgrid) )
    {
      fprintf(stderr, "# nmad: FATAL ERROR- ABI inconsistency detected between application and libnmad.\n"
              "#               libnmad / application\n"
              "# enable_debug     %2d   /   %2d\n"
              "# enable_pioman    %2d   /   %2d\n"
              "# enable_pthread   %2d   /   %2d\n"
              "# enable_marcel    %2d   /   %2d\n"
              "# enable_pukabi    %2d   /   %2d\n"
              "# enable_simgrid   %2d   /   %2d\n"
              "Please re-build your application with up-to-date nmad config.\n",
              nm_abi_config.enable_debug,   p_nm_abi_config->enable_debug,
              nm_abi_config.enable_pioman,  p_nm_abi_config->enable_pioman,
              nm_abi_config.enable_pthread, p_nm_abi_config->enable_pthread,
              nm_abi_config.enable_marcel,  p_nm_abi_config->enable_marcel,
              nm_abi_config.enable_pukabi,  p_nm_abi_config->enable_pukabi,
              nm_abi_config.enable_simgrid, p_nm_abi_config->enable_simgrid);
      abort();
    }
}

int nm_launcher_init_checked(int *argc, char**argv, const struct nm_abi_config_s*p_nm_abi_config)
{
  nm_abi_config_check(p_nm_abi_config);
  launcher.init_level++;
  if(launcher.init_level > 1)
    return NM_ESUCCESS;

  /*
   * Lazy Puk initialization (it may already have been initialized in PadicoTM)
   */
  if(!padico_puk_initialized())
    {
      padico_puk_init();
      launcher.puk_init = 1;
    }

  padico_puk_add_path(NMAD_ROOT);

#ifdef NMAD_PADICOTM
  launcher.boot_mod = padico_lib_init();
#endif /* NMAD_PADICOTM */

  /* ** resolve launcher */
  const char*launcher_env = getenv("NM_LAUNCHER");
  const char*launcher_name = NULL;
  if(launcher_env != NULL)
    {
      NM_DISPF("NM_LAUNCHER = %s forced by environment\n", launcher_env);
      padico_string_t s_launcher = padico_string_new();
      padico_string_printf(s_launcher, "NewMad_Launcher_%s", launcher_env);
      launcher_name = padico_strdup(padico_string_get(s_launcher));
      padico_string_delete(s_launcher);
    }
  else
    {
#ifdef NMAD_SIMGRID
      if(NMAD_SIMGRID)
        {
          /* launcher specialized for simgrid */
          launcher_name = "NewMad_Launcher_simgrid";
        }
      else
#endif /* NMAD_SIMGRID */
      if(getenv("PUK_MULT_ROOT") != NULL)
        {
          /* multiple node in the same process (simulation) */
          launcher_name = "NewMad_Launcher_mult";
        }
      else if((puk_mod_getbyname("PadicoTM") != NULL)
         && (padico_getenv("PADICO_BOOT_ID") != NULL)
         && (padico_getenv("PADICO_BOOTSTRAP_HOST") != NULL)
         )
        {
          /* PadicoTM loaded, launched through padico-launch & using regular bootstrap */
          launcher_name = "NewMad_Launcher_madico";
        }
      else if(padico_getenv("SLURM_JOB_ID") != NULL)
        {
          /* slurm job */
          if(getenv("PMIX_NAMESPACE") != NULL)
            {
              /* PMIx job */
#ifdef HAVE_PMIX
              launcher_name = "NewMad_Launcher_pmix";
#else /* HAVE_PMIX */
              NM_FATAL("PMIx job detected, but nmad was built without libpmix support.\n");
#endif /* HAVE_PMIX */
            }
          else if(getenv("PMI_JOBID") != NULL)
            {
              /* PMI2 job */
#ifdef HAVE_PMI2
              launcher_name = "NewMad_Launcher_pmi2";
#else /* HAVE_PMI2 */
              NM_FATAL("PMI2 job detected, but nmad was built without libpmi2 support.\n");
#endif /* HAVE_PMI2 */
            }
          else
            {
              NM_FATAL("detected slurm job, but no padico-launch environment nor PMI2 environment. Please launch your binary either with padico-launch or srun --mpi=pmi2.\n");
            }
        }
      else
        {
          /* last resort: single node */
          NM_DISPF("no launcher detected; default to single node.\n");
          launcher_name = "NewMad_Launcher_single";
        }
    }

  /* ** Initialize the global nmad core */
  int err = nm_core_init(&launcher.p_core);
  if(err != NM_ESUCCESS)
    {
      NM_FATAL("error %d while initializing nmad core.\n", err);
    }

  /* ** let the launcher establish connections */
  puk_component_t launcher_component = puk_component_resolve(launcher_name);
  if(launcher_component == NULL)
    {
      NM_FATAL("failed to load launcher %s.\n", launcher_name);
    }
  launcher.instance = puk_component_instantiate(launcher_component);
  puk_instance_indirect_NewMad_Launcher(launcher.instance, NULL, &launcher.r);
  (*launcher.r.driver->init)(launcher.r._status, argc, argv, "NewMadeleine");
  if(launcher.size == -1 || launcher.rank == -1)
    {
      NM_FATAL("launcher %s did not properly initialize job info.\n", launcher_name);
    }

  /* ** register gates */
  launcher.gates = padico_malloc(launcher.size * sizeof(nm_gate_t));
  (*launcher.r.driver->get_gates)(launcher.r._status, launcher.gates);
  nm_gate_reverse_hashtable_init(&launcher.reverse);
  int j;
  for(j = 0; j < launcher.size; j++)
    {
      nm_gate_t p_gate = launcher.gates[j];
      nm_gate_reverse_hashtable_insert(&launcher.reverse, p_gate, j);
    }
  padico_string_t s_rank = padico_string_new();
  padico_string_printf(s_rank, "[%3d] ", launcher.rank);
  puk_trace_setnameext(padico_string_get(s_rank));
  padico_string_delete(s_rank);

  /* ** barrier to synchronize drivers before any communication takes place */
  assert(launcher.r.driver->barrier != NULL);
  (*launcher.r.driver->barrier)(launcher.r._status);

  /* ** display drivers for debug */
  const char* s_display_drivers = getenv("NMAD_DISPLAY_DRIVERS");
  if(puk_opt_parse_bool(s_display_drivers))
    {
      /* sync to avoid mixed outputs on stderr */
      struct nm_data_s data;
      nm_data_contiguous_build(&data, NULL, 0);
      nm_tag_t tag = 2;
      if(launcher.rank > 0)
        {
          nm_launcher_recv(launcher.p_core, &data, launcher.gates[launcher.rank - 1], tag);
        }
      nm_launcher_print_drivers_strategy(launcher.p_core);
      if(launcher.rank < launcher.size - 1)
        {
          nm_launcher_send(launcher.p_core, &data, launcher.gates[launcher.rank + 1], tag);
        }
      nm_launcher_barrier();
    }

  /* ** start traces, if enabled */
#ifdef NMAD_TRACE
  nm_trace_init(launcher.p_core);
#endif /* NMAD_TRACE */

  return NM_ESUCCESS;
}

int nm_launcher_exit(void)
{
  launcher.init_level--;
  if(launcher.init_level > 0)
    return NM_ESUCCESS;
  if(launcher.init_level < 0)
    {
      NM_WARN("launcher already closed.\n");
      return -NM_EALREADY;
    }

#ifdef NMAD_TRACE
  nm_trace_exit(launcher.p_core);
#endif /* NMAD_TRACE */

#ifdef PIOMAN_TRACE
  piom_trace_flush();
#endif /* PIOMAN_TRACE */

  /* barrier before shutdown */
  nm_launcher_barrier();
  /* dump profiling info before shutdown, since profiling vars
   * may be destroyed once modules are unloaded */
  if(!puk_profile_var_vect_empty(puk_profile_get_vars()))
    {
      /* sync to avoid mixed outputs on stderr */
      struct nm_data_s data;
      nm_data_contiguous_build(&data, NULL, 0);
      nm_tag_t tag = 2;
      if(launcher.rank > 0)
        {
          nm_launcher_recv(launcher.p_core, &data, launcher.gates[launcher.rank - 1], tag);
        }
      puk_profile_dump();
      if(launcher.rank < launcher.size - 1)
        {
          nm_launcher_send(launcher.p_core, &data, launcher.gates[launcher.rank + 1], tag);
        }
      nm_launcher_barrier();
    }

  padico_free(launcher.gates);
  launcher.gates = NULL;
  nm_gate_reverse_hashtable_destroy(&launcher.reverse);
  puk_instance_destroy(launcher.instance);
  launcher.instance = NULL;

  /* close nmad core */
  nm_core_exit(launcher.p_core);
  launcher.p_core = NULL;
  padico_free((void*)launcher.local_url);
  launcher.local_url = NULL;
  padico_string_delete(launcher.url_string);

  if(launcher.boot_mod)
    {
#ifdef NMAD_PADICOTM
      padico_lib_finalize(launcher.boot_mod);
#endif
      launcher.boot_mod = NULL;
    }
  if(launcher.puk_init)
    {
      padico_puk_shutdown();
    }
  return NM_ESUCCESS;
}

PUK_VECT_TYPE(driver_string, char*);

void nm_launcher_print_drivers_strategy(nm_core_t p_core)
{
  nm_launcher_check_init();
  nm_gate_t p_gate = NULL;
  int i, j, found;

  struct driver_string_vect_s driver_str;
  driver_string_vect_init(&driver_str);

  int rank;
  nm_launcher_get_rank(&rank);

  NM_FOR_EACH_GATE(p_gate, p_core)
    {
      for(i = 0; i < p_gate->n_trks; i++)
        {
          const struct nm_trk_s*p_trk = &p_gate->trks[i];
          const struct nm_drv_s*p_drv = p_trk->p_drv;
          padico_string_t driver_string = padico_string_new();
          if(p_drv->props.nickname != NULL)
            padico_string_catf(driver_string, "%s", p_drv->props.nickname);
          else
            padico_string_catf(driver_string, "%s", p_trk->instance->component->name);
          const char*labels[50];
          const char*values[50];
          int n = 50;
          puk_component_listattrs(p_drv->assembly, labels, values, &n);
          int k;
          for(k = 0; k < n; k++)
            {
              padico_string_catf(driver_string, ":%s=%s", labels[k], values[k]);
            }
          found = 0;
          for(j = 0; j < driver_string_vect_size(&driver_str); j++)
            {
              if(strcmp(padico_string_get(driver_string), driver_string_vect_at(&driver_str, j)) == 0)
                {
                  found = 1;
                  break;
                }
            }
          if(!found)
            {
              driver_string_vect_push_back(&driver_str, padico_strdup(padico_string_get(driver_string)));
            }
          padico_string_delete(driver_string);
        }
    }

  padico_string_t s = padico_string_new();
  padico_string_catf(s, "# [%d %s] Strategy %s; used drivers: \n", rank, padico_hostname(), p_core->strategy_component->name);
  for(i = 0; i < driver_string_vect_size(&driver_str); i++)
    {
      padico_string_catf(s, "# [%d %s]    %s\n", rank, padico_hostname(), driver_string_vect_at(&driver_str, i));
      padico_free(driver_string_vect_at(&driver_str, i));
    }
  fprintf(stderr, "%s", padico_string_get(s));
  padico_string_delete(s);

  driver_string_vect_destroy(&driver_str);

}

/* ********************************************************* */
/* ** set of gates */

void nm_launcher_gates_init(struct nm_launcher_gates_s*p_gates, int size, int rank)
{
  p_gates->p_gates = padico_malloc(size * sizeof(struct nm_launcher_gate_s));
  p_gates->size = size;
  p_gates->rank = rank;
  int i;
  for(i = 0; i < p_gates->size; i++)
    {
      p_gates->p_gates[i].p_gate = NM_GATE_NONE;
      p_gates->p_gates[i].p_url = NULL;
      nm_drv_vect_init(&p_gates->p_gates[i].drvs);
    }
}

void nm_launcher_gates_destroy(struct nm_launcher_gates_s*p_gates)
{
  int i;
  for(i = 0; i < p_gates->size; i++)
    {
      if(p_gates->p_gates[i].p_url != NULL)
        padico_free(p_gates->p_gates[i].p_url);
      nm_drv_vect_destroy(&p_gates->p_gates[i].drvs);
    }
  padico_free(p_gates->p_gates);
  p_gates->p_gates = NULL;
}

void nm_launcher_gates_connect(struct nm_launcher_gates_s*p_gates)
{
#ifdef NMAD_DEBUG
  puk_hashtable_t table = puk_hashtable_new_string();
  int i;
  for(i = 0; i < p_gates->size; i++)
    {
      char*url = p_gates->p_gates[i].p_url;
      if(puk_hashtable_lookup(table, url) != NULL)
        {
          NM_FATAL("duplicate url detected; multiple nodes advertise the same url.\n");
        }
      puk_hashtable_insert(table, url, url);
    }
  puk_hashtable_delete(table, NULL);
#endif /* NMAD_DEBUG */
  /* ** generate permutations for connection order */
  int steps = 1; /* number of steps = size rounded to power of two */
  while(steps < p_gates->size)
    {
      steps *= 2;
    }
  int*p_dests = padico_malloc(sizeof(int) * steps);
  int n_dests = 0;
  int s;
  for(s = 0; s < steps; s++)
    {
      const int dest = p_gates->rank ^ s; /* pave the (step, rank) matrix with XOR pattern */
      assert(dest >= 0);
      assert(dest <= steps);
      if(dest >= p_gates->size)
        {
          /* dest may be larger than comm size if size is not a power of two */
          continue;
        }
      p_dests[n_dests] = dest;
      n_dests++;
    }
  /* connect gates asynchronously */
  const int max_async = 256;
  int a = 0, w = 0;
  while(w < n_dests)
    {
      while((a - w < max_async) && (a < n_dests))
        {
          const int dest = p_dests[a];
          int rc = nm_launcher_connect_async(&p_gates->p_gates[dest].p_gate, p_gates->p_gates[dest].p_url);
          if(rc != NM_ESUCCESS)
            {
              NM_WARN("nm_launcher_connect_async()- rc = %d\n", rc);
            }
          padico_out(50, "connection to node #%d done.\n", dest);
          a++;
        }
      if(w < a)
      {
        const int dest = p_dests[w];
        int rc = nm_launcher_connect_wait(p_gates->p_gates[dest].p_gate);
        if(rc != NM_ESUCCESS)
          {
            NM_WARN("nm_launcher_connect_wait()- rc = %d\n", rc);
          }
        w++;
      }
    }
  padico_free(p_dests);
}


/* ********************************************************* */
/* ** connection establishment */

static const char ibverbs_bybuf[] =
  "<puk:composite id=\"nm:minidriver_ibverbs_bybuf\">"
  "  <puk:component id=\"0\" name=\"NewMad_ibverbs_common\"/>"
  "  <puk:component id=\"1\" name=\"NewMad_ibverbs_bybuf\">"
  "    <puk:uses iface=\"NewMad_ibverbs\" provider-id=\"0\"/>"
  "  </puk:component>"
  "  <puk:entry-point iface=\"NewMad_minidriver\" port=\"minidriver\" provider-id=\"1\" />"
  "</puk:composite>";

static const char ibverbs_bycopy[] =
  "<puk:composite id=\"nm:minidriver_ibverbs_bycopy\">"
  "  <puk:component id=\"0\" name=\"NewMad_ibverbs_common\"/>"
  "  <puk:component id=\"1\" name=\"NewMad_ibverbs_bycopy\">"
  "    <puk:uses iface=\"NewMad_ibverbs\" provider-id=\"0\"/>"
  "  </puk:component>"
  "  <puk:entry-point iface=\"NewMad_minidriver\" port=\"minidriver\" provider-id=\"1\" />"
  "</puk:composite>";

static const char ibverbs_lr2[] =
  "<puk:composite id=\"nm:minidriver_ibverbs_lr2\">"
  "  <puk:component id=\"0\" name=\"NewMad_ibverbs_common\"/>"
  "  <puk:component id=\"1\" name=\"NewMad_ibverbs_lr2\">"
  "    <puk:uses iface=\"NewMad_ibverbs\" provider-id=\"0\"/>"
  "  </puk:component>"
  "  <puk:entry-point iface=\"NewMad_minidriver\" port=\"minidriver\" provider-id=\"1\" />"
  "</puk:composite>";

static const char ibverbs_rcache[] =
  "<puk:composite id=\"nm:minidriver_ibverbs_rcache\">"
  "  <puk:component id=\"0\" name=\"NewMad_ibverbs_common\"/>"
  "  <puk:component id=\"1\" name=\"NewMad_ibverbs_rcache\">"
  "    <puk:uses iface=\"NewMad_ibverbs\" provider-id=\"0\"/>"
  "  </puk:component>"
  "  <puk:entry-point iface=\"NewMad_minidriver\" port=\"minidriver\" provider-id=\"1\" />"
  "</puk:composite>";

static const char ibverbs_srq[] =
  "<puk:composite id=\"nm:minidriver_ibverbs_srq\">"
  "  <puk:component id=\"0\" name=\"NewMad_ibverbs_common\"/>"
  "  <puk:component id=\"1\" name=\"NewMad_ibverbs_srq\">"
  "    <puk:uses iface=\"NewMad_ibverbs\" provider-id=\"0\"/>"
  "  </puk:component>"
  "  <puk:entry-point iface=\"NewMad_minidriver\" port=\"minidriver\" provider-id=\"1\" />"
  "</puk:composite>";

/** Initialize the strategy */
static void nm_launcher_init_strategy(void)
{
  const char*strategy_name = getenv("NMAD_STRATEGY");
  if(!strategy_name)
    {
      strategy_name = "aggreg";
    }
  puk_component_t strategy = nm_core_component_load("Strategy", strategy_name);
  int err = nm_core_set_strategy(launcher.p_core, strategy);
  if(err != NM_ESUCCESS)
    {
      NM_FATAL("error %d while loading strategy '%s'.\n", err, strategy_name);
    }
}

static nm_drv_t nm_launcher_add_driver_attrs(puk_component_t component, nm_trk_kind_t kind, nm_attrs_hashtable_t attrs_table)
{
  /* ** set attributes */
  if(attrs_table != NULL && nm_attrs_hashtable_size(attrs_table) > 0)
    {
      const char*network = nm_attrs_hashtable_lookup(attrs_table, "network") ? : "default";
      padico_string_t driver_name = padico_string_new();
      padico_string_catf(driver_name, "%s-{%s}", component->name, network);
      component = puk_compositize(component, padico_string_get(driver_name));
      padico_string_delete(driver_name);
      nm_attrs_hashtable_enumerator_t e = nm_attrs_hashtable_enumerator_new(attrs_table);
      const char*label = NULL;
      char*value = NULL;
      nm_attrs_hashtable_enumerator_next2(e, &label, &value);
      while(label)
        {
          puk_component_setattr(component, label, value);
          nm_attrs_hashtable_enumerator_next2(e, &label, &value);
        }
      nm_attrs_hashtable_enumerator_delete(e);
    }
  return nm_launcher_add_driver(component, kind);
}

void nm_launcher_set_info(const struct nm_launcher_info_s*p_info)
{
  assert(launcher.size == -1);
  launcher.size = p_info->size;
  launcher.rank = p_info->rank;
  launcher.wide_url = p_info->wide_url_support;
}


/** add the given driver to the session */
nm_drv_t nm_launcher_add_driver(puk_component_t component, nm_trk_kind_t kind)
{
  assert(component != NULL);
  /* add size & rank as attribute */
  if(launcher.size > 0)
    {
      padico_string_t s_size = padico_string_new();
      padico_string_catf(s_size, "%d", launcher.size);
      puk_component_addattr(component, "session_size", padico_string_get(s_size));
      padico_string_delete(s_size);
    }
  if(launcher.rank >= 0)
    {
      padico_string_t s_rank = padico_string_new();
      padico_string_catf(s_rank, "%d", launcher.rank);
      puk_component_addattr(component, "rank", padico_string_get(s_rank));
      padico_string_delete(s_rank);
    }
  if(launcher.wide_url)
    {
      puk_component_addattr(component, "wide_url_support", "true");
    }
  const char*driver_url = NULL;
  nm_drv_t p_drv = NULL;
  int err = nm_core_driver_load_init(launcher.p_core, component, kind, &p_drv, &driver_url);
  if(err != NM_ESUCCESS)
    {
      NM_FATAL("error %d while loading driver '%s'.\n", err, component->name);
    }
  if(launcher.url_string == NULL)
    {
      launcher.url_string = padico_string_new();
    }
  else
    {
      padico_string_catf(launcher.url_string, "+");
    }
  padico_string_catf(launcher.url_string, "%s=%s", p_drv->driver_id, driver_url);
  return p_drv;
}

/** Initialize default drivers */
static void nm_launcher_init_drivers(void)
{
  const char*driver_env = getenv("NMAD_DRIVER");
  if(driver_env && (strlen(driver_env) == 0))
    driver_env = NULL;
  if(!driver_env)
    {
      driver_env = "tcp";
    }

  /* ** parse driver_string */
  int need_ibverbs  __attribute__((unused)) = 0;
  int need_psm      __attribute__((unused)) = 0;
  int need_psm2     __attribute__((unused)) = 0;
  int need_ucx      __attribute__((unused)) = 0;
  int need_portals4 __attribute__((unused)) = 0;
  char*driver_string = padico_strdup(driver_env); /* strtok writes into the string */
  char*saveptr = NULL;
  char*token = strtok_r(driver_string, "+", &saveptr);
  while(token)
    {
      const char*driver_trk_small = NULL, *driver_trk_large = NULL;
      const char*driver_name = token;
      char*attr_string = strchr(driver_name, ':');
      if(attr_string)
        {
          *attr_string = '\0';
          attr_string++;
        }
      nm_attrs_hashtable_t attrs_table = nm_attrs_hashtable_new();
      /* ** parse attributes */
      while(attr_string && *attr_string)
        {
          char*next_attr = strchr(attr_string, ':');
          if(next_attr)
            {
              *next_attr = '\0';
              next_attr++;
            }
          const char*label = attr_string;
          char*value = strchr(attr_string, '=');
          if(value)
            {
              *value = '\0';
              value++;
            }
          nm_attrs_hashtable_insert(attrs_table, label, value);
          attr_string = next_attr;
        }
      if(strcmp(driver_name, "tcp") == 0)
        {
          driver_trk_small =
            "<puk:composite id=\"nm:minidriver_tcp\">"
            "  <puk:component id=\"0\" name=\"Minidriver_tcp\"/>"
            "  <puk:entry-point iface=\"NewMad_minidriver\" port=\"minidriver\" provider-id=\"0\" />"
            "</puk:composite>";
          driver_trk_large =
            "<puk:composite id=\"nm:minidriver_tcp-large\">"
            "  <puk:component id=\"0\" name=\"Minidriver_tcp\"/>"
            "  <puk:entry-point iface=\"NewMad_minidriver\" port=\"minidriver\" provider-id=\"0\" />"
            "</puk:composite>";
        }
      else if(strcmp(driver_name, "local") == 0)
        {
          driver_trk_small =
            "<puk:composite id=\"nm:minidriver_local-small\">"
            "  <puk:component id=\"0\" name=\"Minidriver_local\"/>"
            "  <puk:entry-point iface=\"NewMad_minidriver\" port=\"minidriver\" provider-id=\"0\" />"
            "</puk:composite>";
          driver_trk_large =
            "<puk:composite id=\"nm:minidriver_local-large\">"
            "  <puk:component id=\"0\" name=\"Minidriver_local\"/>"
            "  <puk:entry-point iface=\"NewMad_minidriver\" port=\"minidriver\" provider-id=\"0\" />"
            "</puk:composite>";
        }
      else if(strcmp(driver_name, "simgrid") == 0)
        {
          driver_trk_small =
            "<puk:composite id=\"nm:minidriver_simgrid_small\">"
            "  <puk:component id=\"0\" name=\"Minidriver_simgrid_small\"/>"
            "  <puk:entry-point iface=\"NewMad_minidriver\" port=\"minidriver\" provider-id=\"0\" />"
            "</puk:composite>";
          driver_trk_large =
            "<puk:composite id=\"nm:minidriver_simgrid_large\">"
            "  <puk:component id=\"0\" name=\"Minidriver_simgrid_large\"/>"
            "  <puk:entry-point iface=\"NewMad_minidriver\" port=\"minidriver\" provider-id=\"0\" />"
            "</puk:composite>";
        }
      else if((strcmp(driver_name, "ib") == 0) || (strcmp(driver_name, "ibv") == 0) ||
              (strcmp(driver_name, "ibverbs") == 0))
        {
          /* ibverbs default; use some heuristic to choose the right components */
          need_ibverbs = 1;
          driver_trk_small = ibverbs_bybuf;
          if(padico_module_attr_ibverbs_srq_getvalue())
            {
              driver_trk_small = ibverbs_srq;
              NM_DISPF("ibverbs- SRQ forced by environment.\n");
            }
          if(launcher.size > 16)
            {
              driver_trk_small = ibverbs_srq;
              NM_DISPF("ibverbs- SRQ selected for > 16 nodes.\n");
            }
          driver_trk_large = ibverbs_rcache;
          if(!padico_module_attr_ibverbs_rcache_getvalue())
            {
              driver_trk_large = ibverbs_lr2;
              NM_DISPF("ibverbs- rcache disabled by environment.\n");
            }
        }
      else if(strcmp(driver_name, "iblr2") == 0)
        {
          need_ibverbs = 1;
          driver_trk_small = ibverbs_bybuf;
          driver_trk_large = ibverbs_lr2;
        }
      else if(strcmp(driver_name, "ibbuf") == 0)
        {
          need_ibverbs = 1;
          driver_trk_small = ibverbs_bybuf;
          driver_trk_large = ibverbs_lr2;
        }
      else if(strcmp(driver_name, "ibsrq") == 0)
        {
          need_ibverbs = 1;
          driver_trk_small = ibverbs_srq;
          driver_trk_large = ibverbs_lr2;
        }
      else if(strcmp(driver_name, "ibsrqrcache") == 0)
        {
          need_ibverbs = 1;
          driver_trk_small = ibverbs_srq;
          driver_trk_large = ibverbs_rcache;
        }
      else if(strcmp(driver_name, "ibsrqlr2") == 0)
        {
          need_ibverbs = 1;
          driver_trk_small = ibverbs_srq;
          driver_trk_large = ibverbs_lr2;
        }
      else if(strcmp(driver_name, "ibcopy") == 0)
        {
          need_ibverbs = 1;
          driver_trk_small = ibverbs_bycopy;
          driver_trk_large = ibverbs_lr2;
        }
      else if(strcmp(driver_name, "ibrcache") == 0)
        {
          need_ibverbs = 1;
          driver_trk_small = ibverbs_bybuf;
          driver_trk_large = ibverbs_rcache;
        }
      else if(strcmp(driver_name, "ibcopyrcache") == 0)
        {
          need_ibverbs = 1;
          driver_trk_small = ibverbs_bycopy;
          driver_trk_large = ibverbs_rcache;
        }
      else if(strcmp(driver_name, "shm") == 0)
        {
          driver_trk_small =
            "<puk:composite id=\"nm:minidriver_shm\">"
            "  <puk:component id=\"0\" name=\"Minidriver_shm\"/>"
            "  <puk:entry-point iface=\"NewMad_minidriver\" port=\"minidriver\" provider-id=\"0\" />"
            "</puk:composite>";
          driver_trk_large =
            "<puk:composite id=\"nm:minidriver_largeshm\">"
            "  <puk:component id=\"0\" name=\"Minidriver_largeshm\"/>"
            "  <puk:entry-point iface=\"NewMad_minidriver\" port=\"minidriver\" provider-id=\"0\" />"
            "</puk:composite>";
        }
      else if(strcmp(driver_name, "cma") == 0)
        {
          driver_trk_small =
            "<puk:composite id=\"nm:minidriver_shm\">"
            "  <puk:component id=\"0\" name=\"Minidriver_shm\"/>"
            "  <puk:entry-point iface=\"NewMad_minidriver\" port=\"minidriver\" provider-id=\"0\" />"
            "</puk:composite>";
          driver_trk_large =
            "<puk:composite id=\"nm:minidriver_cma\">"
            "  <puk:component id=\"0\" name=\"Minidriver_CMA\"/>"
            "  <puk:entry-point iface=\"NewMad_minidriver\" port=\"minidriver\" provider-id=\"0\" />"
            "</puk:composite>";
        }
      else if(strcmp(driver_name, "psm2") == 0)
        {
          need_psm2 = 1;
          driver_trk_small =
            "<puk:composite id=\"nm:minidriver_psm2-small\">"
            "  <puk:component id=\"0\" name=\"Minidriver_psm2\"/>"
            "  <puk:entry-point iface=\"NewMad_minidriver\" port=\"minidriver\" provider-id=\"0\" />"
            "</puk:composite>";
          driver_trk_large =
            "<puk:composite id=\"nm:minidriver_psm2-large\">"
            "  <puk:component id=\"0\" name=\"Minidriver_psm2\"/>"
            "  <puk:entry-point iface=\"NewMad_minidriver\" port=\"minidriver\" provider-id=\"0\" />"
            "</puk:composite>";
        }
      else if(strcmp(driver_name, "psm") == 0)
        {
          need_psm = 1;
          driver_trk_small =
            "<puk:composite id=\"nm:minidriver_psm-small\">"
            "  <puk:component id=\"0\" name=\"Minidriver_psm\"/>"
            "  <puk:entry-point iface=\"NewMad_minidriver\" port=\"minidriver\" provider-id=\"0\" />"
            "</puk:composite>";
          driver_trk_large =
            "<puk:composite id=\"nm:minidriver_psm-large\">"
            "  <puk:component id=\"0\" name=\"Minidriver_psm\"/>"
            "  <puk:entry-point iface=\"NewMad_minidriver\" port=\"minidriver\" provider-id=\"0\" />"
            "</puk:composite>";
        }
      else if(strcmp(driver_name, "ofi") == 0)
        {
          driver_trk_small =
            "<puk:composite id=\"nm:minidriver_ofi_rdm_buf\">"
            "  <puk:component id=\"0\" name=\"Minidriver_ofi_rdm_buf\"/>"
            "  <puk:entry-point iface=\"NewMad_minidriver\" port=\"minidriver\" provider-id=\"0\" />"
            "</puk:composite>";
          driver_trk_large =
            "<puk:composite id=\"nm:minidriver_ofi_rdm_large\">"
            "  <puk:component id=\"0\" name=\"Minidriver_ofi_rdm_large\"/>"
            "  <puk:entry-point iface=\"NewMad_minidriver\" port=\"minidriver\" provider-id=\"0\" />"
            "</puk:composite>";
        }
      else if(strcmp(driver_name, "ofi_msg") == 0)
        {
          driver_trk_small =
            "<puk:composite id=\"nm:minidriver_ofi_msg-small\">"
            "  <puk:component id=\"0\" name=\"Minidriver_ofi_msg\"/>"
            "  <puk:entry-point iface=\"NewMad_minidriver\" port=\"minidriver\" provider-id=\"0\" />"
            "</puk:composite>";
          driver_trk_large =
            "<puk:composite id=\"nm:minidriver_ofi_msg-large\">"
            "  <puk:component id=\"0\" name=\"Minidriver_ofi_msg\"/>"
            "  <puk:entry-point iface=\"NewMad_minidriver\" port=\"minidriver\" provider-id=\"0\" />"
            "</puk:composite>";
        }
      else if((strcmp(driver_name, "myri") == 0) || (strcmp(driver_name, "myrinet") == 0) ||
              (strcmp(driver_name, "mx") == 0))
        {
          NM_FATAL("support for mx driver is deprecated.\n");
        }
      else if(strcmp(driver_name, "ucx") == 0)
        {
          need_ucx = 1;
          driver_trk_small =
            "<puk:composite id=\"nm:minidriver_ucx-small\">"
            "  <puk:component id=\"0\" name=\"Minidriver_ucx\"/>"
            "  <puk:entry-point iface=\"NewMad_minidriver\" port=\"minidriver\" provider-id=\"0\" />"
            "</puk:composite>";
          driver_trk_large =
            "<puk:composite id=\"nm:minidriver_ucx-large\">"
            "  <puk:component id=\"0\" name=\"Minidriver_ucx\"/>"
            "  <puk:entry-point iface=\"NewMad_minidriver\" port=\"minidriver\" provider-id=\"0\" />"
            "</puk:composite>";
        }
      else if( (strcmp(driver_name, "portals4") == 0) ||
               (strcmp(driver_name, "bxi") == 0) )
        {
          need_portals4 = 1;
          driver_trk_small =
            "<puk:composite id=\"nm:minidriver_portals4-small\">"
            "  <puk:component id=\"0\" name=\"Minidriver_portals4_bybuf\"/>"
            "  <puk:entry-point iface=\"NewMad_minidriver\" port=\"minidriver\" provider-id=\"0\" />"
            "</puk:composite>";
          driver_trk_large =
            "<puk:composite id=\"nm:minidriver_portals4-large\">"
            "  <puk:component id=\"0\" name=\"Minidriver_portals4_large\"/>"
            "  <puk:entry-point iface=\"NewMad_minidriver\" port=\"minidriver\" provider-id=\"0\" />"
            "</puk:composite>";
        }
      else
        {
          NM_FATAL("invalid driver '%s' (NMAD_DRIVER = %s).\n", driver_name, driver_env);
        }
      /* ** sanity check */
#ifndef HAVE_IBVERBS
      if(need_ibverbs)
        {
          padico_fatal("selected driver %s needs ibverbs support, but ibverbs support is not compiled.\n", driver_name);
        }
#endif /* HAVE_IBVERBS */
#ifndef HAVE_PSM
      if(need_psm)
        {
          padico_fatal("selected driver %s needs psm support, but psm support is not compiled.\n", driver_name);
        }
#endif /* HAVE_PSM */
#ifndef HAVE_PSM2
      if(need_psm2)
        {
          padico_fatal("selected driver %s needs psm2 support, but psm2 support is not compiled.\n", driver_name);
        }
#endif /* HAVE_PSM2 */
#ifndef HAVE_UCX
      if(need_ucx)
        {
          padico_fatal("selected driver %s needs ucx support, but ucx support is not compiled.\n", driver_name);
        }
#endif /* HAVE_UCX */
#ifndef HAVE_PORTALS4
      if(need_portals4)
        {
          padico_fatal("selected driver %s needs portals4 support, but portals4 support is not compiled.\n", driver_name);
        }
#endif /* HAVE_PORTALS4 */

      /* ** parse components */
      assert(driver_trk_small);
      puk_component_t component_trk_small = puk_component_parse(driver_trk_small);
      if(component_trk_small == NULL)
        {
          NM_FATAL("failed to load component '%s'\n", driver_trk_small);
        }
      assert(driver_trk_large);
      puk_component_t component_trk_large = puk_component_parse(driver_trk_large);
      if(component_trk_large == NULL)
        {
          NM_FATAL("failed to load component '%s'\n", driver_trk_large);
        }

      /* ** load components */
      nm_launcher_add_driver_attrs(component_trk_small, nm_trk_small, attrs_table);
      nm_launcher_add_driver_attrs(component_trk_large, nm_trk_large, attrs_table);

      nm_attrs_hashtable_delete(attrs_table);
      token = strtok_r(NULL, "+", &saveptr);
    }
  padico_free(driver_string);

  /* load default driver */
  const char*driver_self_small =
    "<puk:composite id=\"nm:minidriver_self_buf-small\">"
    "  <puk:component id=\"0\" name=\"Minidriver_self_buf\"/>"
    "  <puk:entry-point iface=\"NewMad_minidriver\" port=\"minidriver\" provider-id=\"0\" />"
    "</puk:composite>";
  const char*driver_self_large =
    "<puk:composite id=\"nm:minidriver_self-large\">"
    "  <puk:component id=\"0\" name=\"Minidriver_self\"/>"
    "  <puk:entry-point iface=\"NewMad_minidriver\" port=\"minidriver\" provider-id=\"0\" />"
    "</puk:composite>";
  puk_component_t component_self_small = puk_component_parse(driver_self_small);
  puk_component_t component_self_large = puk_component_parse(driver_self_large);
  launcher.p_drv_self_small = nm_launcher_add_driver(component_self_small, nm_trk_small);
  launcher.p_drv_self_large = nm_launcher_add_driver(component_self_large, nm_trk_large);
}


int nm_launcher_get_url(const char**p_local_url)
{
  /* load strategy */
  nm_launcher_init_strategy();

  if(launcher.p_core->nb_drivers == 0)
    {
      /* load default drivers */
      nm_launcher_init_drivers();
    }
  if(launcher.local_url == NULL)
    launcher.local_url = padico_strdup(padico_string_get(launcher.url_string));
  *p_local_url = launcher.local_url;
  return NM_ESUCCESS;
}

void nm_launcher_set_selector(nm_launcher_selector_t selector, void*_arg)
{
  launcher.selector = selector;
  launcher.selector_arg = _arg;
}

/** default selector to choose drivers */
static nm_drv_vect_t nm_launcher_default_selector(const char*peer_url, void*_arg)
{
  nm_drv_vect_t v = nm_drv_vect_new();
  if(strcmp(peer_url, launcher.local_url) == 0)
    {
      /* ** loopback connect- use driver 'self' (hardwired) */
      nm_drv_vect_push_back(v, launcher.p_drv_self_small);
      nm_drv_vect_push_back(v, launcher.p_drv_self_large);
    }
  else
    {
      /* ** remote connection- find common drivers */
      nm_drv_t p_drv;
      NM_FOR_EACH_DRIVER(p_drv, launcher.p_core)
        {
          char driver_name[256];
          snprintf(driver_name, 256, "%s=", p_drv->assembly->name);
          if(p_drv == launcher.p_drv_self_small || p_drv == launcher.p_drv_self_large)
            {
              continue;
            }
          else if(strstr(peer_url, driver_name) != NULL)
            {
              nm_drv_vect_push_back(v, p_drv);
            }
          else
            {
              NM_WARN("peer node does not advertise any url for driver %s in url '%s'- skipping.\n",
                      driver_name, peer_url);
              continue;
            }
        }
    }
  return v;
}

static void nm_launcher_url_destructor(char*p_driver_name, char*p_driver_url)
{
  char*p_driver_string = p_driver_name; /* both strings points to the same buffer */
  padico_free(p_driver_string);
}

int nm_launcher_connect(nm_gate_t*pp_gate, const char*url)
{
  nm_launcher_connect_async(pp_gate, url);
  nm_launcher_connect_wait(*pp_gate);
  return NM_ESUCCESS;
}

int nm_launcher_connect_async(nm_gate_t*pp_gate, const char*url)
{
  assert(launcher.p_core != NULL);
  nm_drv_vect_t v = NULL;
  if(launcher.selector == NULL)
    {
      launcher.selector = &nm_launcher_default_selector;
    }

  v = (*launcher.selector)(url, launcher.selector_arg);
  if(nm_drv_vect_empty(v))
    {
      NM_FATAL("no common driver found for local (%s) and peer (%s) node. Abort.\n",
               launcher.local_url, url);
   }

  /* parse remote url, store in hashtable (driver_name -> driver_url) */
  struct nm_url_hashtable_s url_table;
  nm_url_hashtable_init(&url_table);
  char*parse_string = padico_strdup(url);
  char*saveptr = NULL;
  char*token = strtok_r(parse_string, "+", &saveptr);
  while(token != NULL)
    {
      char*driver_string = padico_strdup(token);
      char*driver_name = driver_string;
      char*driver_url = strchr(driver_name, '=');
      if(driver_url == NULL)
        {
          NM_FATAL("malformed url string '%s'\n", token);
        }
      *driver_url = '\0';
      driver_url++;
      if(nm_url_hashtable_lookup(&url_table, driver_name))
        {
          NM_FATAL("duplicate driver '%s' in url '%s'.\n", driver_name, url);
        }
      nm_url_hashtable_insert(&url_table, driver_name, driver_url);
      token = strtok_r(NULL, "+", &saveptr);
    }
  padico_free(parse_string);
  /* create gate */
  nm_gate_t p_gate = nm_core_gate_new(launcher.p_core, &v);
  /* connect all drivers */
  int trk_id = 0;
  nm_drv_vect_itor_t i;
  puk_vect_foreach(i, nm_drv, v)
    {
      nm_drv_t p_drv = *i;
      const char*driver_url = nm_url_hashtable_lookup(&url_table, p_drv->driver_id);
      if(driver_url == NULL)
        {
          NM_FATAL("cannot find url in table from driver %s; trk = %d\n", p_drv->driver_id, trk_id);
        }
      nm_core_gate_connect_async(launcher.p_core, p_gate, p_drv, trk_id, driver_url);
      trk_id++;
    }
  /* check whether all drivers are the same as local (homogeneous network) */
  const int n_drivers = nm_url_hashtable_size(&url_table);
  if(n_drivers == nm_drv_list_size(&launcher.p_core->driver_list))
    {
      nm_drv_t p_drv;
      NM_FOR_EACH_DRIVER(p_drv, launcher.p_core)
        {
          if( (p_drv != launcher.p_drv_self_small) &&
              (p_drv != launcher.p_drv_self_large) &&
              (!nm_url_hashtable_probe(&url_table,  p_drv->driver_id)) &&
              (!p_drv->props.capabilities.self) )
            {
              if(launcher.homogeneous_network)
                {
                  NM_DISPF("local driver %s not found in remote url; networks are not homogeneous.\n",
                           p_drv->driver_id);
                  launcher.homogeneous_network = 0;
                }
            }
        }
    }
  else
    {
      NM_DISPF("different number of drivers in local and remote url; networks are not homogeneous.\n");
      launcher.homogeneous_network = 0;
    }

  /* destroy the url hashtable */
  nm_url_hashtable_destroy(&url_table);
  nm_drv_vect_delete(v);
  *pp_gate = p_gate;
  return NM_ESUCCESS;
}

int nm_launcher_connect_wait(nm_gate_t p_gate)
{
  int t;
  for(t = 0; t < p_gate->n_trks; t++)
    {
      struct nm_trk_s*p_trk = &p_gate->trks[t];
      nm_core_gate_connect_wait(launcher.p_core, p_trk);
    }
  return NM_ESUCCESS;
}

int nm_launcher_homogeneous_network(void)
{
  return launcher.homogeneous_network;
}

/* Padico tutorial 2: MPI
 * PadicoTM/Examples/Tutorial/02-HelloMPI/HelloMPI.c
 * author: Alexandre DENIS
 */

#include <stdio.h>
#include <math.h>
#include <string.h>
#include <Padico/Puk.h>
#include <Padico/Module.h>

#include <mpi.h>

int hellompi_main(int argc, char**argv);

PADICO_MODULE_DECLARE(HelloMPI, NULL, hellompi_main, NULL);

#define BUFLEN 1024
char buffer[BUFLEN];
int  namelen = MPI_MAX_PROCESSOR_NAME;
char processor_name[MPI_MAX_PROCESSOR_NAME];

void hop()
{
  strcat(buffer, " * ");
  strcat(buffer, processor_name);
}

int hellompi_main(int argc, char**argv)
{
  int  myid, numprocs, next, prev;
  MPI_Status status;

  padico_print("[HelloMPI] strating- argc = %d\n", argc);
  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD,&numprocs);
  MPI_Comm_rank(MPI_COMM_WORLD,&myid);
  MPI_Get_processor_name(processor_name,&namelen);

  next = (myid + 1) % numprocs;
  prev = (myid - 1 + numprocs) % numprocs;
  buffer[0] = 0;

  MPI_Barrier(MPI_COMM_WORLD);
  padico_print("[HelloMPI] I am process %d on %s next=%d prev=%d\n",
               myid, processor_name, next, prev);

  padico_print("[HelloMPI] mode name = %s\n", padico_module_self_name());
  if (myid == 0)
    {
      if(argc > 1)
        {
          strcpy(buffer, argv[1]);
        }
      else
        {
          strcpy(buffer,"Hello world!");
        }
      hop();
      padico_print("[HelloMPI] %d sending '%s' (len=%d) \n",myid,buffer, strlen(buffer));
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, next, 99, MPI_COMM_WORLD);
      padico_print("[HelloMPI] %d receiving \n",myid);
      MPI_Recv(buffer, BUFLEN, MPI_CHAR, MPI_ANY_SOURCE, 99, MPI_COMM_WORLD,
               &status);
      padico_print("[HelloMPI] %d received '%s' \n",myid,buffer);

      hop();
      padico_print("[HelloMPI] %d sending '%s' \n",myid,buffer);
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, prev, 99, MPI_COMM_WORLD);
      padico_print("[HelloMPI] %d receiving \n",myid);
      MPI_Recv(buffer, BUFLEN, MPI_CHAR, MPI_ANY_SOURCE, 99, MPI_COMM_WORLD,
               &status);
      padico_print("[HelloMPI] %d received '%s' \n",myid,buffer);
      hop();

      padico_print("[HelloMPI] ##### %s #####\n", buffer);
    }
  else
    {
      padico_print("[HelloMPI] %d receiving  \n",myid);
      MPI_Recv(buffer, BUFLEN, MPI_CHAR, MPI_ANY_SOURCE, 99, MPI_COMM_WORLD,
               &status);
      padico_print("[HelloMPI] %d received '%s' \n",myid,buffer);
      hop();
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, next, 99, MPI_COMM_WORLD);
      padico_print("[HelloMPI] %d sent '%s' \n",myid,buffer);

      padico_print("[HelloMPI] %d receiving  \n",myid);
      MPI_Recv(buffer, BUFLEN, MPI_CHAR, MPI_ANY_SOURCE, 99, MPI_COMM_WORLD,
               &status);
      padico_print("[HelloMPI] %d received '%s' \n",myid,buffer);
      hop();
      MPI_Send(buffer, strlen(buffer)+1, MPI_CHAR, prev, 99, MPI_COMM_WORLD);
      padico_print("[HelloMPI] %d sent '%s' \n",myid,buffer);
    }
  padico_print("[HelloMPI] Synchronizing\n");
  MPI_Barrier(MPI_COMM_WORLD);
  padico_print("[HelloMPI] finalizing\n");
  MPI_Finalize();
  padico_print("[HelloMPI] ok\n");
  return 0;
}

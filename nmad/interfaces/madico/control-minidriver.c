/** @file
 * @brief PadicoControl over minidriver
 * @note usable only as secondary controler, cannot bootstrap
 */

#include <Padico/Puk.h>
#include <Padico/Module.h>
#include <Padico/PadicoTM.h>
#include <Padico/PadicoControl.h>
#include <Padico/Topology.h>


/* ********************************************************* */

#if defined(PIOMAN)

#include <nm_minidriver_interface.h>
#include <nm_minidriver.h>
#include <pioman.h>


/* *** PadicoComponent driver ******************************** */

static void*control_minidriver_instantiate(puk_instance_t instance, puk_context_t context);
static void control_minidriver_destroy(void*_status);

/** instanciation facet for Control_minidriver
 */
static const struct puk_component_driver_s control_minidriver_component_driver =
{
  .instantiate = &control_minidriver_instantiate,
  .destroy     = &control_minidriver_destroy
};

/* *** PadicoControl driver ******************************** */

static void control_minidriver_send_common(void*_status, padico_topo_node_t node, padico_control_msg_t msg);

/** 'Control' facet for Control/Minidriver
 */
static const struct padico_control_driver_s control_minidriver_control_driver =
{
  .send_common = &control_minidriver_send_common
};

/* *** PadicoConnector driver ****************************** */

static int control_minidriver_connector_init(puk_context_t context);
static int control_minidriver_connector_new(void*_status, const void**local_addr, size_t*addr_size);
static int control_minidriver_connector_connect(void*_status, const void*remote_addr, size_t addr_size);
static void control_minidriver_connector_close(puk_context_t context);

/** 'Connector' facet for Control/Minidriver
 */
static const struct padico_control_connector_driver_s control_minidriver_connector_driver =
{
  .connector_init    = &control_minidriver_connector_init,
  .connector_new     = &control_minidriver_connector_new,
  .connector_connect = &control_minidriver_connector_connect,
  .connector_close   = &control_minidriver_connector_close
};

/* ********************************************************* */

PADICO_MODULE_COMPONENT(Control_minidriver,
  puk_component_declare("Control_minidriver",
                        puk_component_provides("PadicoComponent", "component", &control_minidriver_component_driver),
                        puk_component_provides("PadicoControl", "control", &control_minidriver_control_driver),
                        puk_component_provides("PadicoConnector", "connector", &control_minidriver_connector_driver),
                        puk_component_uses("NewMad_minidriver", "minidriver")) );

/* ********************************************************* */

struct control_minidriver_status_s
{
  puk_instance_t instance;
  puk_context_t context;
  struct puk_receptacle_NewMad_minidriver_s minidriver;
  padico_topo_node_t peer;          /**< peer node */
  nm_len_t recv_msg_len;          /**< header of received message */
};

PUK_VECT_TYPE(control_minidriver_status, struct control_minidriver_status_s*);

/** context for control-minidriver */
struct control_minidriver_context_s
{
  struct control_minidriver_status_vect_s statuses;
  volatile int power_on;
  piom_cond_t dispatch_terminated;
  struct piom_ltask ltask;
  struct control_minidriver_status_s*out_status;   /**< status which triggered the success of the ltask */
  int out_rc;                                      /**< return code of the above operation completion */
  struct nm_minidriver_properties_s props;
  const void*drv_url;
  size_t url_size;
};


/* ********************************************************* */

static void*control_minidriver_instantiate(puk_instance_t instance, puk_context_t context)
{
  struct control_minidriver_context_s*control_context = puk_context_get_status(context);
  if(control_context == NULL)
    {
      padico_fatal("cannot instantiate; context has not been initialized.\n");
    }
  struct control_minidriver_status_s*status = padico_malloc(sizeof(struct control_minidriver_status_s));
  status->instance = instance;
  status->context  = context;
  status->peer     = NULL;
  puk_context_indirect_NewMad_minidriver(instance, "minidriver", &status->minidriver);
  return status;
}

static void control_minidriver_destroy(void*_status)
{
  struct control_minidriver_status_s*status = _status;
  struct control_minidriver_context_s*control_context = puk_context_get_status(status->context);
  padico_control_unplug_controler(status->instance);
  /* stop polling */
  piom_ltask_mask(&control_context->ltask);
  control_minidriver_status_vect_itor_t i = control_minidriver_status_vect_find(&control_context->statuses, status);
  assert(i != NULL);
  control_minidriver_status_vect_erase(&control_context->statuses, i);
  nm_minidriver_recv_cancel(&status->minidriver, &control_context->props);
  piom_ltask_unmask(&control_context->ltask);
  nm_minidriver_disconnect(&status->minidriver);
  padico_free(status);
}

/* ********************************************************* */

static void control_minidriver_send_common(void*_status, padico_topo_node_t node, padico_control_msg_t msg)
{
  struct control_minidriver_status_s*status = _status;
  struct control_minidriver_context_s*control_context = puk_context_get_status(status->context);
  struct iovec v = padico_control_msg_get_bytes(msg);
  nm_len_t len = v.iov_len;
  nm_minidriver_send(&status->minidriver, &control_context->props, &len, sizeof(len));
  nm_minidriver_send(&status->minidriver, &control_context->props, v.iov_base, len);
}

static int control_minidriver_task_poll(void*_context)
{
  puk_context_t context = _context;
  struct control_minidriver_context_s*control_context = puk_context_get_status(context);
  control_minidriver_status_vect_itor_t i;
  puk_vect_foreach(i, control_minidriver_status, &control_context->statuses)
    {
      struct control_minidriver_status_s*status = *i;
      int rc = nm_minidriver_rpoll(&status->minidriver, &control_context->props, &status->recv_msg_len, sizeof(status->recv_msg_len));
      if(rc == NM_ESUCCESS)
        {
          control_context->out_status = status;
          control_context->out_rc = rc;
          piom_ltask_completed(&control_context->ltask);
        }
      else if(rc != -NM_EAGAIN)
        {
          padico_warning("recv_poll_one()- rc = %d (%s)\n", rc, nm_strerror(rc));
          control_context->out_status = status;
          control_context->out_rc = rc;
          piom_ltask_completed(&control_context->ltask);
        }
    }
  if(!control_context->power_on)
    {
      control_context->out_status = NULL;
      control_context->out_rc = 0;
      piom_ltask_completed(&control_context->ltask);
    }
  return 0;
}

/** pre-post a recv for the given instance */
static void control_minidriver_reload_recv(struct control_minidriver_status_s*status)
{
  struct control_minidriver_context_s*control_context = puk_context_get_status(status->context);
  nm_minidriver_irecv(&status->minidriver, &control_context->props,
                      &status->recv_msg_len, sizeof(status->recv_msg_len));
}

static void*control_minidriver_dispatcher(void*_context)
{
  puk_context_t context = _context;
  struct control_minidriver_context_s*control_context = puk_context_get_status(context);
  while(control_context->power_on)
    {
      piom_ltask_create(&control_context->ltask, &control_minidriver_task_poll, context, PIOM_LTASK_OPTION_REPEAT);
      piom_ltask_submit(&control_context->ltask);
      piom_ltask_wait_success(&control_context->ltask);
      if(control_context->out_status != NULL && control_context->out_rc == NM_ESUCCESS)
        {
          struct control_minidriver_status_s*status = control_context->out_status;
          nm_len_t len = status->recv_msg_len;
          char*buf = padico_malloc(len);
          nm_minidriver_recv(&status->minidriver, &control_context->props, buf, len);
          padico_control_deliver_message(status->instance, status->peer, buf, len); /* takes ownership of buf */
          control_minidriver_reload_recv(status);
        }
    }
  piom_cond_signal(&control_context->dispatch_terminated, 1);
  return NULL;
}

/* ********************************************************* */

static int control_minidriver_connector_init(puk_context_t context)
{
  struct control_minidriver_context_s*control_context = puk_context_get_status(context);
  if(control_context != NULL)
    {
      /* already initialized */
      return 0;
    }
  assert(control_context == NULL);
  control_context = padico_malloc(sizeof(struct control_minidriver_context_s));
  control_minidriver_status_vect_init(&control_context->statuses);
  piom_cond_init(&control_context->dispatch_terminated, 0);
  puk_context_set_status(context, control_context);

  puk_context_t minidriver_context = puk_context_get_subcontext(context, puk_iface_NewMad_minidriver(), NULL);
  puk_component_t minidriver_component = minidriver_context->component;
  const struct nm_minidriver_iface_s*driver = puk_component_get_driver_NewMad_minidriver(minidriver_component, NULL);
  nm_minidriver_getprops(driver, minidriver_context, &control_context->props);
  nm_minidriver_init(driver, minidriver_context, &control_context->drv_url, &control_context->url_size);
  /* activate polling */
  control_context->power_on = 1;
  control_context->out_status = NULL;
  control_context->out_rc = 0;
  padico_tm_invoke_later(&control_minidriver_dispatcher, context);
  return 0;
}

static int control_minidriver_connector_new(void*_status, const void**local_addr, size_t*addr_size)
{
  struct control_minidriver_status_s*status = _status;
  struct control_minidriver_context_s*control_context = puk_context_get_status(status->context);
  if(control_context == NULL)
    {
      padico_fatal("connector_new()- context has not been initialized.\n");
    }
  *local_addr = control_context->drv_url;
  *addr_size = control_context->url_size;
  return 0;
}

static int control_minidriver_connector_connect(void*_status, const void*remote_addr, size_t addr_size)
{
  struct control_minidriver_status_s*status = _status;
  struct control_minidriver_context_s*control_context = puk_context_get_status(status->context);
  nm_minidriver_connect(&status->minidriver, remote_addr, addr_size);
  padico_control_plug_controler(status->instance, NULL);

  /* announce local node to peer node */
  padico_string_t self_node = padico_topo_node_serialize(padico_topo_getlocalnode());
  const char*msg = padico_string_get(self_node);
  nm_len_t len = strlen(msg) + 1;
  nm_minidriver_send(&status->minidriver, &control_context->props, &len, sizeof(len));
  nm_minidriver_send(&status->minidriver, &control_context->props, msg, len);
  padico_string_delete(self_node);
  nm_len_t recv_len = NM_LEN_UNDEFINED;
  nm_minidriver_recv(&status->minidriver, &control_context->props, &recv_len, sizeof(recv_len));
  char*recv_msg = padico_malloc(recv_len);
  nm_minidriver_recv(&status->minidriver, &control_context->props, recv_msg, recv_len);

  /* parse peer announce */
  padico_control_event_disable();
  struct puk_parse_entity_s e = puk_xml_parse_buffer(recv_msg, recv_len, PUK_TRUST_CLUSTER);
  padico_rc_t rc2 = puk_parse_get_rc(&e);
  if(padico_rc_iserror(rc2))
    {
      padico_rc_show(rc2);
      padico_fatal("parse error on initial handshake message.");
    }
  if(!puk_parse_is(&e, "Topology:node"))
    {
      padico_fatal("root entity is not \"Topology:node\" while initializing control channel.\n");
    }
  padico_topo_node_t peer = puk_parse_get_content(&e);
  assert(peer != NULL);
  assert(status->peer == NULL);
  status->peer = peer;
  padico_control_new_node(status->instance, peer);
  padico_control_event_enable();
  padico_free(recv_msg);

  /* start polling */
  piom_ltask_mask(&control_context->ltask);
  control_minidriver_status_vect_push_back(&control_context->statuses, status);
  control_minidriver_reload_recv(status);
  piom_ltask_unmask(&control_context->ltask);
  return -1;
}

static void control_minidriver_connector_close(puk_context_t context)
{
  struct control_minidriver_context_s*control_context = puk_context_get_status(context);
  control_context->power_on = 0;
  piom_cond_wait(&control_context->dispatch_terminated, 1);
  piom_ltask_cancel(&control_context->ltask);
  puk_context_t minidriver_context = puk_context_get_subcontext(context, puk_iface_NewMad_minidriver(), NULL);
  puk_component_t minidriver_component = minidriver_context->component;
  const struct nm_minidriver_iface_s*driver = puk_component_get_driver_NewMad_minidriver(minidriver_component, NULL);
  nm_minidriver_close(driver, minidriver_context);
  padico_free(control_context);
  piom_cond_destroy(&control_context->dispatch_terminated);
  puk_context_set_status(context, NULL);
}


#endif /* PIOMAN */

/*
 * NewMadeleine
 * Copyright (C) 2019-2022 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


#include <nm_coll_interface.h>
#include "nm_bench_coll_generic.h"

static const int root = 0;
static const nm_tag_t tag = 0x42;

static void nm_bench_coll_ibcast_run(struct nm_bench_coll_common_s*p_common, void*buf, nm_len_t len)
{
  struct nm_data_s data;
  nm_data_contiguous_build(&data, (void*)buf, len);
  nm_group_t p_group = nm_comm_group(p_common->p_comm);
  const int self = nm_comm_rank(p_common->p_comm);
  nm_session_t p_session = nm_comm_get_session(p_common->p_comm);
  struct nm_coll_req_s*p_bcast = nm_coll_group_data_ibcast(p_session, p_group, root, self, &data, tag, NULL, NULL);
  nm_coll_req_wait(p_bcast);
}

struct nm_bench_coll_s nm_bench_coll =
  {
    .label    = "nm_bench_coll_ibcast",
    .name     = "non-blocking broadcast",
    .init     = NULL,
    .run      = &nm_bench_coll_ibcast_run,
    .finalize = NULL
  };

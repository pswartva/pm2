/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Management of configuration options.
 * @ingroup Puk
 */

#ifndef PUK_OPT_H
#define PUK_OPT_H

#include <assert.h>

typedef enum {
  puk_opt_type_bool,
  puk_opt_type_int,
  puk_opt_type_unsigned,
  puk_opt_type_string
} puk_opt_type_t;

typedef int      puk_opt_type_bool_t;
typedef int      puk_opt_type_int_t;
typedef unsigned puk_opt_type_unsigned_t;
typedef const char*puk_opt_type_string_t;


struct puk_opt_s
{
  const char*label;
  const char*env_var;  /**< name of the environment variable synchronized with this opt; NULL for none */
  const char*desc;     /**< plain text description */
  const char*mod_name; /**< module this option belongs to */
  puk_opt_type_t type;
  union
  {
    int as_bool;
    int as_int;
    unsigned as_unsigned;
    const char*as_string;
  } value;
};

/** declare an option; it should remain allocated for the whole application lifetime.
 * Usually called from PADICO_MODULE_SETATTR()
 */
void puk_opt_declare(struct puk_opt_s*opt);

/** remove an option; to be called only from dynamically loadable objects, before unloading.
 * Options are not supposed to be removed.
 */
void puk_opt_remove(struct puk_opt_s*opt);

/** sets the value of the given option, from a string (value parsed if needed).
 * If no attribute for given mod_name.label exists, value is stored assuming
 * the module will be loaded later or an option be created later.
 */
void puk_opt_setvalue(const char*mod_name, const char*label, const char*value);

/** get the total number of options */
int puk_opt_get_num(void);

/** get an option from its index number */
struct puk_opt_s*puk_opt_get_by_index(int index);

/** get an option from its name */
struct puk_opt_s*puk_opt_get_by_name(const char*name);

/** get an option by modname/label */
struct puk_opt_s*puk_opt_find(const char*mod_name, const char*label);

/** get the index of a given option */
int puk_opt_get_index(const struct puk_opt_s*opt);

/** set the value of given option from a string, with parsing if needed */
void puk_opt_value_from_string(struct puk_opt_s*opt, const char*v);

/** get the value of parameter as a string. Ownership of the string is given to the caller.
 */
char*puk_opt_value_to_string(const struct puk_opt_s*opt);

/** get a plain text description of a type */
const char*puk_opt_type_name(puk_opt_type_t t);

/* ** typed accessors */

static inline int puk_opt_getvalue_bool(const struct puk_opt_s*opt)
{
  assert(opt->type == puk_opt_type_bool);
  return opt->value.as_bool;
}
static inline int puk_opt_getvalue_int(const struct puk_opt_s*opt)
{
  assert(opt->type == puk_opt_type_int);
  return opt->value.as_int;
}
static inline unsigned puk_opt_getvalue_unsigned(const struct puk_opt_s*opt)
{
  assert(opt->type == puk_opt_type_unsigned);
  return opt->value.as_unsigned;
}
static inline const char*puk_opt_getvalue_string(const struct puk_opt_s*opt)
{
  assert(opt->type == puk_opt_type_string);
  return opt->value.as_string;
}


static inline void puk_opt_setvalue_bool(struct puk_opt_s*opt, int v)
{
  assert(opt->type == puk_opt_type_bool);
  opt->value.as_bool = v;
}
static inline void puk_opt_setvalue_int(struct puk_opt_s*opt, int v)
{
  assert(opt->type == puk_opt_type_int);
  opt->value.as_int = v;
}
static inline void puk_opt_setvalue_unsigned(struct puk_opt_s*opt, unsigned v)
{
  assert(opt->type == puk_opt_type_unsigned);
  opt->value.as_unsigned = v;
}
static inline void puk_opt_setvalue_string(struct puk_opt_s*opt, const char*v)
{
  assert(opt->type == puk_opt_type_string);
  opt->value.as_string = padico_strdup(v);
}

/* *** Puk parse options *********************************** */

/** returns 0 if string is "0", "no", "disabled", or is NULL;
 * return 1 if string is "1", "yes", "enabled";
 * issue a warning and return -1 in any other case.
 */
int puk_opt_parse_bool(const char*s);

#endif /* PUK_OPT_H */

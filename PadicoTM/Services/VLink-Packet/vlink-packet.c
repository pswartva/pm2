/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief VLink_Packet for VIO (VLink-over-PSP-wrapper).
 * @ingroup VIO
 */

#include <Padico/Module.h>

#include "VLink-Packet.h"
#include <Padico/VLink-debug.h>


static int padico_vlink_packet_init(void);
static void padico_vlink_packet_finalize(void);

PADICO_MODULE_DECLARE(VLink_Packet, padico_vlink_packet_init, NULL, padico_vlink_packet_finalize,
                      "VIO", "PadicoControl");


static struct
{
  padico_vnode_t vnodes[VOPW_VNODES_MAX]; /**< indirection table to resolve tag->vnode */
  marcel_mutex_t lock;                    /**< lock for the vnode indirection table */
  struct vopw_dynres_s resolver;          /**< dynamic node resolution */
  padico_tm_bgthread_pool_t pool;
} vopw;

/* *** helper functions for locking.
 * Note: no vnode lock should be held before locking vopw.lock.
 */

/** safely acquire the vopw_lock while already holding a vnode lock
 */
static inline void vopw_lock_acquire(padico_vnode_t vnode)
{
  if(vnode)
    {
      vnode_unlock(vnode);
      marcel_mutex_lock(&vopw.lock);
      vnode_lock(vnode);
    }
  else
    {
      marcel_mutex_lock(&vopw.lock);
    }
}
static inline void vopw_lock_release(void)
{
  marcel_mutex_unlock(&vopw.lock);
}

/* ********************************************************* */

/** @defgroup VLink_Packet component: VLink_Packet- VLink over Padico Simple Packets (PSP)
 * @ingroup PadicoComponent
 */

/* *** 'build' functions */
static int         vsock_vopw_create      (padico_vnode_t);
static int         vsock_vopw_bind        (padico_vnode_t);
static int         vsock_vopw_listen      (padico_vnode_t);
static int         vsock_vopw_connect     (padico_vnode_t);
static void        vsock_vopw_fullshutdown(padico_vnode_t);
/* *** 'messaging' functions */
static void        vsock_vopw_send_post   (padico_vnode_t);
static void        vsock_vopw_recv_post   (padico_vnode_t);
static padico_rc_t vsock_vopw_dumpstat    (padico_vnode_t);


/** 'VLink' facet for the VLink_Packet component
 * @ingroup VLink_Packet
 */
static const struct padico_vlink_driver_s vlink_packet_driver =
  {
    .create       = vsock_vopw_create,
    .bind         = vsock_vopw_bind,
    .listen       = vsock_vopw_listen,
    .connect      = vsock_vopw_connect,
    .fullshutdown = vsock_vopw_fullshutdown,
    .close        = NULL,
    .send_post    = vsock_vopw_send_post,
    .recv_post    = vsock_vopw_recv_post,
    .dumpstat     = vsock_vopw_dumpstat
  };
/** 'VLinkMessaging' facet for the VLink_Packet component
 * @ingroup VLink_Packet
 */
static const struct padico_vlink_messaging_driver_s vlink_messaging_driver =
  {
    .sendto   = vsock_madio_sendto,
    .recvfrom = vsock_madio_recvfrom
  };

/** VLink/Packet component-specific per-VLink information
 * @ingroup VLink_Packet
 */
struct vlink_vopw_specific_s
{
  struct vopw_address_s raddr;        /**< address of remote node in vopw address system */
  struct vopw_address_s vopw_binding; /**< local binding */
  int is_bound;
  vopw_ibox_list_t   iboxes;          /**< incoming/indirect reception boxes */
  padico_vnode_t newsock;
  volatile struct
  {
    volatile marcel_t worker;    /**< worker thread for this rendez-vous */
    volatile ssize_t  received;  /**< received data size for rendez-vous (receiver side) */
    volatile size_t   sent;      /**< amount of data already sent (sender side) */
    volatile int      multipart; /**< number of fragments so far */
  } rendezvous;
  struct
  {
    size_t rendezvous_threshold;  /**< maximum size (in bytes) of data transfered without a rendez-vous (likely to cause a copy) */
    int rendezvous_multipart_max; /**< maximum number of parts a message can be splitted into before beeing autoritatively sent */
    size_t data_in_header;
    size_t packet_in_header;
  } config;
  struct puk_receptacle_PadicoSimplePackets_s psp_receptacle;
};

VLINK_CREATE_SPECIFIC_ACCESSORS(vopw);

static void* vopw_deferred_req_worker(void*arg);
static void  vopw_rendezvous_send(padico_vnode_t vnode);
static void  vopw_oktosend_send  (padico_vnode_t, size_t);
static void  vopw_accepted_send  (padico_vnode_t);
static void  vopw_connrefused_send(padico_topo_node_t node, uint16_t tag);
static int   vopw_data_check  (padico_vnode_t, const struct vopw_hdr_s*, padico_topo_node_t);
static void  vopw_data_process(padico_vnode_t, const struct vopw_hdr_s*, void*, padico_psp_pump_t);
static void  vopw_data_send(padico_vnode_t vnode, size_t len);
static void  vopw_acceptor(padico_vnode_t vnode, padico_topo_node_t node_from, int tag_from);
static padico_vnode_t vopw_vnode_lookup(uint16_t port);
static void  vopw_unregister_vnode(padico_vnode_t vnode);
static void  vopw_psp_handler(const void*_header, padico_topo_node_t from, void*key, padico_psp_pump_t pump, void*token);

static void vopw_dynres_register(uint32_t addr, uint32_t rank, uint16_t port, uint16_t tag);
static struct vopw_dynres_entry_s* vopw_dynres_lookup(const struct in_addr*addr, uint16_t port);
static struct vopw_dynres_entry_s* vopw_dynres_resolve(const struct in_addr*addr, uint16_t port);
static void vopw_dynres_reverse_lookup(padico_topo_node_t node, uint16_t tag, struct sockaddr*addr, int*addrlen);

static void vopw_control_event_listener(void*_event);


/* *** Helper functions */

static inline padico_psp_connection_t
vlink_packet_new_message(const struct vlink_vopw_specific_s*vvs, padico_topo_node_t node, struct vopw_hdr_s**hdr)
{
  return padico_psp_new_message(&vvs->psp_receptacle, node, (void**)hdr);
}

static inline int vlink_packet_send(const struct vlink_vopw_specific_s*vvs, padico_psp_connection_t conn,
                                    const void*bytes, size_t b_size)
{
  if(!vvs->is_bound)
    return -ESHUTDOWN;
  if(bytes)
    padico_psp_pack(&vvs->psp_receptacle, conn, bytes, b_size);
  padico_psp_end_message(&vvs->psp_receptacle, conn);
  return 0;
}

/* ********************************************************* */

/** Initialization function for the VLink_Packet driver for VIO
 */
static int padico_vlink_packet_init(void)
{
  int i;

  /* init indirection table */
  for(i = 0; i < VOPW_VNODES_MAX; i++)
    {
      vopw.vnodes[i] = NULL;
    }
  marcel_mutex_init(&vopw.lock, NULL);
  /* init dynres cache */
  vopw.resolver.cache = vopw_dynres_vect_new();
  marcel_mutex_init(&vopw.resolver.lock, NULL);
  marcel_cond_init(&vopw.resolver.cond, NULL);
  vopw.pool = padico_tm_bgthread_pool_create("VLink_Packet");

  /* monitor topology changes */
  padico_control_event_subscribe(&vopw_control_event_listener);

  /* register the component */
  puk_component_declare("VLink_Packet",
                        puk_component_provides("PadicoComponent", "component", &padico_vnode_component_driver),
                        puk_component_provides("VLink",           "vlink",     &vlink_packet_driver),
                        puk_component_provides("VLinkMessaging",  "vlink-msg", &vlink_messaging_driver),
                        puk_component_attr("rendezvous_threshold", "65536"),
                        puk_component_attr("rendezvous_multipart_max", "10"),
                        puk_component_uses("PadicoSimplePackets", "psp"));
  return 0;
}

static void padico_vlink_packet_finalize(void)
{
  padico_control_event_unsubscribe(&vopw_control_event_listener);
  padico_tm_bgthread_pool_wait(vopw.pool);
}

/* ********************************************************* */

static padico_rc_t vsock_vopw_dumpstat(padico_vnode_t vnode)
{
  padico_rc_t rc = NULL;
  if(vnode_is_vopw(vnode))
    {
      rc = padico_rc_msg("  <vopw>raddr=%s:%d iboxes=%d\n",
                         padico_topo_node_getname(vobs_vopw(vnode)->raddr.na_node),
                         vobs_vopw(vnode)->raddr.tag,
                         vopw_ibox_list_size(vobs_vopw(vnode)->iboxes));
      {
        vopw_ibox_itor_t i = vopw_ibox_list_begin(vobs_vopw(vnode)->iboxes);
        while(i != vopw_ibox_list_end(vobs_vopw(vnode)->iboxes))
          {
            rc = padico_rc_add(rc, padico_rc_msg("    <ibox what=%d>rendez_vous=%d\n",
                                                 i->what, i->rendezvous_size));
            rc = padico_rc_add(rc, padico_rc_msg("      <buffer data=%p total_size=%d buffer_offset=%l>%2X %2X %2X %2X ...</buffer>\n",
                                                 i->buffer.data, i->buffer.total_size, i->buffer.offset,
                                                 (int)(unsigned char)i->buffer.data[i->buffer.offset+0],
                                                 (int)(unsigned char)i->buffer.data[i->buffer.offset+1],
                                                 (int)(unsigned char)i->buffer.data[i->buffer.offset+2],
                                                 (int)(unsigned char)i->buffer.data[i->buffer.offset+3]));
            rc = padico_rc_add(rc, padico_rc_msg("    </ibox>\n"));
            i = vopw_ibox_list_next(i);
          }
      }
      if(vobs_vopw(vnode)->is_bound)
        {
          rc = padico_rc_add(rc, padico_rc_msg("\n    <vopw-binding tag=\"%d\"/>\n  </binding>\n",
                                               vobs_vopw(vnode)->vopw_binding.tag));
        }
      rc = padico_rc_add(rc, padico_rc_msg("  </vopw>\n"));
    }
  return rc;
}

static int vsock_vopw_create(padico_vnode_t vnode)
{
  vobs_vopw_t vo;
  padico_out(50, "vnode=%p- entering\n", vnode);
  vnode_check(vnode, socket);
  vobs_vopw_activate(vnode);
  vo = vobs_vopw(vnode);
  vo->iboxes               = vopw_ibox_list_new();
  vo->raddr.na_node        = NULL;
  vo->raddr.tag            = VOPW_TAG_NOTAG;
  vo->is_bound             = 0;
  vo->rendezvous.received  = -1;
  vo->rendezvous.sent      = -1;
  vo->rendezvous.multipart = 0;
  vo->rendezvous.worker    = (marcel_t)NULL;

  const puk_instance_t instance = padico_vnode_get_self_instance(vnode);
  puk_context_indirect_PadicoSimplePackets(instance, "psp", &vo->psp_receptacle);
  size_t h_size = sizeof(struct vopw_hdr_s);
  padico_psp_init(&vo->psp_receptacle, PADICO_PSP_VOPW, "VLink-over-PSP", &h_size, &vopw_psp_handler, NULL);
  vo->config.data_in_header   = h_size - 3 * sizeof(uint32_t);
  vo->config.packet_in_header = h_size - 3 * sizeof(uint32_t);
  vo->config.rendezvous_threshold = atoi(puk_instance_getattr(instance, "rendezvous_threshold"));
  vo->config.rendezvous_multipart_max = atoi(puk_instance_getattr(instance, "rendezvous_multipart_max"));
  padico_out(20, "vnode=%p; h_size=%lu; data_in_header=%lu\n", vnode, (unsigned long)h_size, (unsigned long)vo->config.data_in_header);
  vnode->socket.capabilities |= padico_vsock_caps_clientmustbind | padico_vsock_caps_secondaryonly;
  return 0;
}

/** Madeleine vnode recv_post function. Post a recv request.
 * Completes immediately if data is available in iboxes. Post an rbox
 * if no ibox is available. recv request is stored in vnode->rbox.
 * @note non-blocking call
 * @pre  vnode->mutex locked
 * @pre  vopw.lock unlocked
 * @post rbox posted if returns -1
 * @post vnode->mutex locked
 */
static void vsock_vopw_recv_post(padico_vnode_t vnode)
{
  ssize_t rlen = 0;
  vopw_ibox_t ibox = NULL;
  vnode_check(vnode, rbox);
  padico_out(50, "vnode=%p entering- rbox: buffer=%p posted=%ld done=%ld\n",
             vnode, vnode->rbox.read_buffer,
             puk_slong(vnode->rbox.read_posted), puk_slong(vnode->rbox.read_done));
  assert(vnode->rbox.read_done == 0);
  assert(vnode->rbox.read_posted > 0);
  assert(vnode->rbox.read_done <= vnode->rbox.read_posted);
  assert(vnode->rbox.read_buffer != NULL);
  vnode->rbox.tid = marcel_self();

  if(!vopw_ibox_list_empty(vobs_vopw(vnode)->iboxes))
    {
      /* there is something in iboxes -> immediate receive */
     ibox = vopw_ibox_list_front(vobs_vopw(vnode)->iboxes);
     switch(ibox->what)
       {
       case VOPW_IBOX_EOS:
         vnode_check(vnode, remote);
         vnode->status.end_of_file = 1;
         vobs_signal(vnode, vobs_attr_status);
         vnode->remote.can_recv = 0;
         vobs_signal(vnode, vobs_attr_remote);
         vopw_ibox_list_pop_front(vobs_vopw(vnode)->iboxes);
         vopw_ibox_delete(ibox);
         break;

       case VOPW_IBOX_DATA:
          if(VOPW_BUF_CURRENTSIZE(ibox->buffer) <= vnode->rbox.read_posted)
            {
              /* take the whole buffer in ibox */
              /** @todo check whether we can take more than one ibox at a time
               * according to the current protocol.
               */
              rlen = VOPW_BUF_CURRENTSIZE(ibox->buffer);
              assert(rlen > 0);
              VOPW_BUF_BUFFERTOMEM(vnode->rbox.read_buffer, ibox->buffer, rlen);
              VOPW_BUF_FREE(ibox->buffer);
              padico_out(40, "taking data in ibox [all buffer] (rlen=%ld, ibox=%p)\n",
                         puk_slong(rlen), ibox);
              vopw_ibox_list_pop_front(vobs_vopw(vnode)->iboxes);
              vopw_ibox_delete(ibox);
            }
          else
            {
              /* data in ibox is too large. Receive only 'posted' bytes */
              rlen = vnode->rbox.read_posted;
              assert(rlen > 0);
              VOPW_BUF_BUFFERTOMEM(vnode->rbox.read_buffer, ibox->buffer, rlen);
              padico_out(40, "taking data in ibox [multi-part] (rlen=%ld, ibox=%p)\n",
                         puk_slong(rlen), ibox);
            }
          vnode->rbox.read_done = rlen;
          assert(vnode->rbox.read_done > 0);
          assert(vnode->rbox.read_done <= vnode->rbox.read_posted);
          vobs_signal(vnode, vobs_attr_rbox);
         break;

       case VOPW_IBOX_RENDEZVOUS:
         {
           const size_t rv_size = ibox->rendezvous_size;
           padico_out(40, "taking rendezvous in ibox (rv_size=%ld, ibox=%p)\n",
                      puk_ulong(rv_size), ibox);
           vopw_ibox_list_pop_front(vobs_vopw(vnode)->iboxes);
           vopw_ibox_delete(ibox);
           assert(vnode->content.rbox && (vnode->rbox.read_posted != vnode->rbox.read_done));
           vopw_oktosend_send(vnode, rv_size);
           assert(vnode->rbox.read_done <= vnode->rbox.read_posted);
         }
         break;

       default:
         padico_fatal("vsock_vopw_recv_post() internal error (unexpected ibox type %d)\n", ibox->what);
         break;
       }
     vnode->status.readable = !vopw_ibox_list_empty(vobs_vopw(vnode)->iboxes);
    }
  else
    {
      /* nothing in iboxes -> wait */
      padico_out(40, "nothing in ibox -waiting\n");
    }
  padico_out(40, "vnode=%p ok rlen=%ld\n", vnode, puk_slong(rlen));
}


/**
 * @pre vnode->mutex locked
 * @pre vopw.lock unlocked
 */
static void vsock_vopw_send_post(padico_vnode_t vnode)
{
  padico_out(50, "vnode=%p entering\n", vnode);

  if(vnode->wbox.write_posted > vobs_vopw(vnode)->config.rendezvous_threshold)
    {
      if(vobs_vopw(vnode)->rendezvous.worker != (marcel_t)NULL)
        {
          padico_fatal("vsock_vopw_send_post()- cannot create rendezvous thread. Previous still running.\n");
        }
      /* do a rendezvous for zero-copy -send in background thread */
      padico_out(50, "vnode=%p starting worker thread\n", vnode);
      vnode->status.pending_op++;
      /* init the rendezvous */
      vnode_check(vnode, wbox);
      vnode_check(vnode, binding);
      assert(vnode->wbox.write_done == 0);
      assert(vobs_vopw(vnode)->rendezvous.sent == -1);
      assert(vobs_vopw(vnode)->rendezvous.received == -1);
      vobs_vopw(vnode)->rendezvous.received  = 0;
      vobs_vopw(vnode)->rendezvous.multipart = 0;
      vopw_rendezvous_send(vnode);
    }
  else
    {
      /* send without rendevous- zero-copy not guaranteed, but better latency */
      padico_out(50, "vnode=%p send without rendezvous- invoking data_send()\n", vnode);
      vlink_profile_point(vlink_wprofiler, 2);
      vopw_data_send(vnode, vnode->wbox.write_posted);
      vlink_profile_point(vlink_wprofiler, 12);
      vobs_signal(vnode, vobs_attr_wbox);
      vlink_profile_point(vlink_wprofiler, 13);
    }
  padico_out(40, "vnode=%p exit write_done=%ld\n", vnode, puk_slong(vnode->wbox.write_done));
}


/** generates a new tag
 */
static int vsock_vopw_gentag(padico_vnode_t vnode)
{
  int i, rc = -1;
  static int next_tag = 1;
  int previous_tag;
  padico_out(50, "vnode=%p entering\n", vnode);
  vopw_lock_acquire(vnode);
  vobs_vopw_check(vnode);
  vnode_check(vnode, socket);
  previous_tag = next_tag;
  next_tag = VOPW_TAG_NEXT(next_tag);
  for(i = next_tag;
      i != previous_tag;
      i = VOPW_TAG_NEXT(i))
    {
      if(vopw.vnodes[i] == NULL)
        {
          vobs_vopw(vnode)->vopw_binding.tag = i;
          vobs_vopw(vnode)->vopw_binding.na_node = padico_topo_getlocalnode();
          vobs_vopw(vnode)->is_bound = 1;
          vnode->rbox.tid = marcel_self();
          vopw.vnodes[i] = vnode;
          padico_out(6, "binding vnode=%p to tag=%d\n", vnode, i);
          rc = 0;
          break;
        }
    }
  vopw_lock_release();
  return rc;
}

/** generate an address from the current tag
 */
static void vsock_vopw_genaddr(padico_vnode_t vnode)
{
  struct sockaddr_pa*addr = padico_malloc(sizeof(struct sockaddr_pa));
  socklen_t addrlen = sizeof(struct sockaddr_pa);
  struct sockaddr_padico_vopw*addr_vopw = padico_sockaddr_payload(struct sockaddr_padico_vopw, addr);
  vobs_vopw_check(vnode);
  vnode_check(vnode, socket);
  assert(vobs_vopw(vnode)->is_bound);
  padico_sockaddr_init(addr, &addrlen, AF_PADICO_VOPW);
  addr_vopw->vopw_tag  = htons(vobs_vopw(vnode)->vopw_binding.tag);
  memcpy(addr_vopw->vopw_node_uuid,
         padico_topo_node_getuuid(vobs_vopw(vnode)->vopw_binding.na_node),
         PADICO_TOPO_UUID_SIZE);
  vnode->binding.primary     = (struct sockaddr*)addr;
  vnode->binding.primary_len = addrlen;
}

static int vsock_vopw_bind(padico_vnode_t vnode)
{
  int rc = 0;
  vnode_check(vnode, socket);
  vnode_check(vnode, binding);
  if(!vobs_vopw(vnode)->is_bound)
    {
      assert(vnode->binding.autobind);
      rc = vsock_vopw_gentag(vnode);
      if(rc)
        {
          padico_warning("no address available. Cannot bind.\n");
          vnode_set_error(vnode, EADDRNOTAVAIL);
        }
      vsock_vopw_genaddr(vnode);
      vnode->binding.autobind = 0;
    }
  return rc;
}

static int vsock_vopw_listen(padico_vnode_t vnode)
{
  int rc = 0;
  padico_print("listen.\n");
  vnode_check(vnode, listener);
  const struct padico_vaddr_s*src_vaddr = vnode->listener.src_addr;
  assert(src_vaddr != NULL);
  const struct sockaddr_pa*remote_paddr = (struct sockaddr_pa*)src_vaddr->addr;
  int check = padico_sockaddr_check((const struct sockaddr_pa*)remote_paddr, AF_PADICO_VOPW);
  if(check != 0)
    {
      vnode_set_error(vnode, check);
      rc = -1;
      goto error;
    }
  const struct sockaddr_padico_vopw*remote_vopw_addr = padico_sockaddr_payload(struct sockaddr_padico_vopw, remote_paddr);
  padico_topo_node_t remote_node = padico_topo_getnodebyuuid((padico_topo_uuid_t)remote_vopw_addr->vopw_node_uuid);
  if(remote_node == NULL)
    {
     /* node unreachable through VOPW */
      padico_warning("listen()- vnode=%p remote node=%s not reachable through VOPW\n",
                     vnode, padico_topo_node_getname(remote_node));
      vnode_set_error(vnode, EPROTONOSUPPORT);
      rc = -1;
      goto error;
    }
  /* create connected socket from server socket in advance */
  padico_vnode_t newsock = padico_vnode_component_acceptor(vnode, padico_module_self());
  rc = padico_vsock_create(newsock,
                           vnode->socket.so_family,
                           vnode->socket.so_type,
                           vnode->socket.so_protocol);
  vnode_lock(newsock);
  vobs_vopw(vnode)->newsock = newsock;

  /* bind the new socket */
  vnode_activate(newsock, binding);
  newsock->binding.autobind = 1;
  vsock_vopw_bind(newsock);
  vnode_unlock(newsock);

  /* establish reverse PSP path for newsock */
  padico_psp_listen(&vobs_vopw(newsock)->psp_receptacle, remote_node);

  padico_print("listen ## done.\n");
 error:
  return rc;
}

static int vsock_vopw_connect(padico_vnode_t vnode)
{
  int rc = -1;

  padico_out(50, "vnode=%p entering\n", vnode);
  vnode_check(vnode, remote);
  const struct sockaddr_pa*remote_paddr = (struct sockaddr_pa*)vnode->remote.remote_addr;
  const int check = padico_sockaddr_check(remote_paddr, AF_PADICO_VOPW);
  if(check)
    {
      vnode_set_error(vnode, check);
      rc = -1;
      goto error;
    }
  const struct sockaddr_padico_vopw*remote_vopw_addr = padico_sockaddr_payload(struct sockaddr_padico_vopw, remote_paddr);
  uint16_t remote_tag  = ntohs(remote_vopw_addr->vopw_tag);
  padico_topo_node_t remote_node = padico_topo_getnodebyuuid((padico_topo_uuid_t)remote_vopw_addr->vopw_node_uuid);

  if(remote_node == NULL)
    {
     /* node unreachable through VOPW */
      padico_warning("connect()- vnode=%p remote node=%s not reachable through VOPW\n",
                     vnode, padico_topo_node_getname(remote_node));
      vnode_set_error(vnode, EPROTONOSUPPORT);
      rc = -1;
      goto error;
    }
  /* node reachable through VOPW- connect */
  const struct vlink_vopw_specific_s*const vvs = vobs_vopw(vnode);
  padico_psp_listen(&vvs->psp_receptacle, remote_node);
  padico_psp_connect(&vvs->psp_receptacle, remote_node);

  if(!vvs->is_bound)
    {
      /* automatic primary binding */
      vnode_activate(vnode, binding);
      vsock_vopw_bind(vnode);
    }

  struct vopw_hdr_s*out_hdr = NULL;
  const padico_psp_connection_t conn = vlink_packet_new_message(vvs, remote_node, &out_hdr);
  padico_out(50, "vnode=%p trying VOPW for remote node=%s\n", vnode, padico_topo_node_getname(remote_node));
  out_hdr->op                     = htons(VOPW_OP_CONNECT);
  out_hdr->tag                    = htons(VOPW_TAG_ANY);
  out_hdr->info.CONNECT.your_tag  = htons(remote_tag);
  out_hdr->info.CONNECT.my_tag    = htons(vobs_vopw(vnode)->vopw_binding.tag);
  out_hdr->info.CONNECT.domain    = htonl(vnode->socket.so_family);
  out_hdr->info.CONNECT.type      = htonl(vnode->socket.so_type);

  /* send CONNECT */
  vlink_packet_send(vvs, conn, NULL, 0);

  vnode_set_error(vnode, EINPROGRESS);
  rc = -1;
  padico_out(60, "vnode=%p CONNECT request sent. status=EINPROGRESS\n", vnode);
 error:
  padico_out(60, "exiting vnode=%p; rc=%d; errno=%d\n", vnode, rc, vnode_get_error(vnode));
  return rc;
}

static void vopw_unregister_vnode(padico_vnode_t vnode)
{
  int i;
  padico_out(6, "unregister vnode=%p\n", vnode);
  vopw_lock_acquire(vnode);
  vobs_vopw_check(vnode);
  /* unbind */
  if(vobs_vopw(vnode)->is_bound)
    {
      padico_out(10, "vnode=%p was bound to tag=%d\n", vnode, vobs_vopw(vnode)->vopw_binding.tag);
      vobs_vopw(vnode)->vopw_binding.tag = VOPW_TAG_NOTAG;
      vobs_vopw(vnode)->is_bound = 0;
    }
  /* unregister the vnode from vopw_vnodes */
  for(i = 1; i < VOPW_VNODES_MAX; i++)
    {
      if(vopw.vnodes[i] == vnode)
        {
          padico_out(6, "vnode=%p unregistered i=%d\n", vnode, i);
          vopw.vnodes[i] = NULL;
        }
    }
  vopw_lock_release();
}

static void vlink_madio_rdshutdown(padico_vnode_t vnode)
{
  vnode_check(vnode, remote);
}

static void vlink_madio_wrshutdown(padico_vnode_t vnode)
{
  if(vobs_vopw(vnode)->raddr.na_node != NULL)
    {
      /* the socket is actually connected */
      struct vopw_deferred_req_s*req = padico_malloc(sizeof(struct vopw_deferred_req_s));
      vobs_vopw_check(vnode);
      vnode_check(vnode, remote);
      vnode_check(vnode, binding);
      padico_out(6, "vnode=%p sending EOS -- tag=%d\n", vnode, vobs_vopw(vnode)->raddr.tag);
      req->vnode = vnode;
      req->op = VOPW_OP_EOS;
      vnode->status.pending_op++;
      padico_tm_bgthread_start(vopw.pool, &vopw_deferred_req_worker, req, "deferred_req_worker:eos");
    }
  else
    {
      /* else: socket *IS* vopw, but not successfully connected */
      padico_out(20, " raddr.node = -1 -- don't send EOS.\n");
    }
}


static void vsock_vopw_fullshutdown(padico_vnode_t vnode)
{
  padico_out(50, "vnode=%p\n", vnode);
  if(vnode_is_vopw(vnode))
    {
      if(vnode->content.remote)
        {
          vlink_madio_wrshutdown(vnode);
          vlink_madio_rdshutdown(vnode);
        }
      vopw_unregister_vnode(vnode);
      vobs_vopw(vnode)->psp_receptacle = puk_receptacle_PadicoSimplePackets_null();
      vobs_vopw_deactivate(vnode);
    }
}


/* *** Packet connection-less communications *************** */

ssize_t vsock_madio_sendto(padico_vnode_t vnode, const void*buf, size_t len,
                           padico_topo_node_t node, int port)
{
  ssize_t rc = 0;
  padico_out(40, "vnode=%p; len=%ld\n", vnode, puk_ulong(len));
  if(padico_topo_getlocalnode() != node)
    {
      /* sending to a remote peer */
      struct vopw_dynres_entry_s*res_node =
        vopw_dynres_resolve(padico_topo_host_getaddr(padico_topo_node_gethost(node)), port);
      assert(res_node != NULL);
      int tag = res_node->tag;
      struct vopw_hdr_s*out_hdr;
      struct vlink_vopw_specific_s*const vvs = vobs_vopw(vnode);
      const padico_psp_connection_t conn = vlink_packet_new_message(vvs, node, &out_hdr);
      if(len <= vobs_vopw(vnode)->config.packet_in_header)
        {
          padico_out(50, "vnode=%p; len=%ld- short send\n", vnode, puk_ulong(len));
          /* SHORT_SEND- single packet */
          out_hdr->op            = htons(VOPW_OP_PACKET);
          out_hdr->tag           = htons(tag);
          out_hdr->info.PACKET.len = htonl(len);
          out_hdr->info.PACKET.port = ((struct sockaddr_in*)vnode->binding.primary)->sin_port;
          memcpy(out_hdr->info.PACKET.data, buf, len);
          vlink_packet_send(vvs, conn, NULL, 0);
        }
      else
        {
          /* LONG_SEND- multi-packet */
          padico_out(50, "vnode=%p; len=%ld- long send\n", vnode, puk_ulong(len));
          out_hdr->op            = htons(VOPW_OP_PACKET);
          out_hdr->tag           = htons(tag);
          out_hdr->info.PACKET.len = htonl(len);
          out_hdr->info.PACKET.port = ((struct sockaddr_in*)vnode->binding.primary)->sin_port;
          vlink_packet_send(vvs, conn, buf, len);
        }
      rc = len;
    }
  else
    {
      /* sending to self */
      padico_vnode_t recipient = vopw_vnode_lookup(port);
      padico_out(30, "vnode=%p sending to self- recipient=%p\n", vnode, recipient);
      if(recipient != NULL)
        {
          vnode_lock(recipient);
          if(recipient->content.rbox)
            {
              assert(recipient->rbox.read_posted >= len);
              memcpy(recipient->rbox.read_buffer, buf, len);
              recipient->rbox.read_done = len;
              vopw_dynres_reverse_lookup(padico_topo_getlocalnode(),
                                         ntohs(((struct sockaddr_in*)vnode->binding.primary)->sin_port),
                                         recipient->rbox.from,
                                         recipient->rbox.fromlen);
            }
          else
            {
              vopw_ibox_t ibox = vopw_ibox_new();
              ibox->what = VOPW_IBOX_PACKET;
              ibox->node_from = padico_topo_getlocalnode();
              ibox->port = ntohs(((struct sockaddr_in*)vnode->binding.primary)->sin_port);
              ibox->buffer.data = padico_malloc(len);
              ibox->buffer.total_size = len;
              ibox->buffer.size = len;
              ibox->buffer.offset = 0;
              vopw_ibox_list_push_back(vobs_vopw(recipient)->iboxes, ibox);
              memcpy(ibox->buffer.data, buf, len);
            }
          assert(vnode->rbox.read_done > 0);
          assert(vnode->rbox.read_done <= vnode->rbox.read_posted);
          vobs_signal(recipient, vobs_attr_rbox);
          vnode_unlock(recipient);
          rc = len;
        }
    }
  return rc;
}

ssize_t vsock_madio_recvfrom(padico_vnode_t vnode, void*buf, size_t len)
{
  ssize_t rc = 0;
  vopw_ibox_t ibox = NULL;
  vnode_check(vnode, rbox);
  if(!vopw_ibox_list_empty(vobs_vopw(vnode)->iboxes))
    {
      /* there is something in iboxes -> immediate receive */
     ibox = vopw_ibox_list_front(vobs_vopw(vnode)->iboxes);
     switch(ibox->what)
       {
       case VOPW_IBOX_PACKET:
         assert(ibox->buffer.total_size <= len);
         memcpy(buf, ibox->buffer.data, ibox->buffer.total_size);
         vnode->rbox.read_done = ibox->buffer.total_size;
         vopw_dynres_reverse_lookup(ibox->node_from, ibox->port, vnode->rbox.from, vnode->rbox.fromlen);
         vopw_ibox_list_pop_front(vobs_vopw(vnode)->iboxes);
         vopw_ibox_delete(ibox);
         break;
       default:
         padico_fatal("vsock_madio_recvfrom() internal error (unexpected ibox type %d)\n", ibox->what);
         break;
       }
     vnode->status.readable = !vopw_ibox_list_empty(vobs_vopw(vnode)->iboxes);
    }
  else
    {
      /* nothing in iboxes -> wait */
      padico_out(40, "nothing in ibox -waiting\n");
    }
  return rc;
}



/* *** private functions *********************************** */

static void*vopw_deferred_req_worker(void*arg)
{
  const struct vopw_deferred_req_s*req = (struct vopw_deferred_req_s*)arg;
  const padico_vnode_t vnode = req->vnode;
  vnode_lock(vnode);
  assert(vnode->status.pending_op > 0);
  if(vnode->status.writable && vnode->content.remote)
    {
      const struct vlink_vopw_specific_s*const vvs = vobs_vopw(vnode);
      struct vopw_hdr_s*out_hdr;
      const padico_psp_connection_t conn = vlink_packet_new_message(vvs, vvs->raddr.na_node, &out_hdr);
      if(req->op)
        {
          /* build a header */
          out_hdr->op  = htons(req->op);
          out_hdr->tag = htons(vvs->raddr.tag);
        }
      else
        {
          /* take the given header */
          memcpy(out_hdr, &req->hdr, sizeof(struct vopw_hdr_s));
        }
      padico_out(30," begin packing- tag=%d op=%d\n", ntohs(out_hdr->tag), ntohs(out_hdr->op));
      vlink_packet_send(vvs, conn, NULL, 0);
    }
  vnode->status.pending_op--;
  vobs_signal(vnode, vobs_attr_status);
  vnode_unlock(vnode);
  padico_free(arg);
  return NULL;
}

/** Create a new socket from a listening server socket
 */
static void vopw_acceptor(padico_vnode_t vnode, padico_topo_node_t node_from, int tag_from)
{
  struct sockaddr_in addr;
  vnode_lock(vnode);
  /** @todo: WARNING! There is a race condition in locking here!
   * vnode is *NOT LOCKED* when the tests whether it's listening are performed.
   * State may have changed! It *should* be fixed.
   * Anyway... we assume there is a vnode on this tag,
   * vnode is a listening socket with a non-full backlog,
   * even if it may have changed in an unlucky case.
   */
  padico_out(40, "vnode=%p; node_from=%s; tag_from=%d\n",
             vnode, padico_topo_node_getname(node_from), tag_from);
  padico_vnode_t newsock = vobs_vopw(vnode)->newsock;

  vnode_lock(newsock);
  vobs_vopw(newsock)->raddr.na_node = node_from;
  vobs_vopw(newsock)->raddr.tag  = tag_from;

  /* accept the connection */
  vopw_accepted_send(newsock);

  /* connect the new socket */
  newsock->status.writable = 1;
  vnode_unlock(newsock);
  addr.sin_family = AF_INET;
  addr.sin_port = htons(vobs_vopw(newsock)->vopw_binding.tag); /* XXX TODO */
  addr.sin_addr = *(padico_topo_host_getaddr(padico_topo_node_gethost(node_from)));
  padico_vsock_acceptor_finalize(newsock, vnode,
                                 (struct sockaddr*)&addr, sizeof(addr));

  vnode_unlock(vnode);
}

/** send a RENDEZVOUS request
 * @pre vnode->mutex locked
 */
static void vopw_rendezvous_send(padico_vnode_t vnode)
{
  padico_out(50, "vnode=%p sending rendezvous (done=%ld/%ld)\n", vnode,
             (long)vnode->wbox.write_done, (long)vnode->wbox.write_posted);
  const size_t tosend = vnode->wbox.write_posted - vnode->wbox.write_done;
  const struct vlink_vopw_specific_s*const vvs = vobs_vopw(vnode);
  struct vopw_hdr_s*out_hdr;
  const padico_psp_connection_t conn = vlink_packet_new_message(vvs, vvs->raddr.na_node, &out_hdr);
  padico_out(50, "vnode=%p sending rendezvous.\n", vnode);
  vobs_vopw(vnode)->rendezvous.received = 0;
  out_hdr->op  = htons(VOPW_OP_RENDEZVOUS);
  out_hdr->tag = htons(vvs->raddr.tag);
  out_hdr->info.RENDEZVOUS.proposed_len = htonl(tosend);
  vlink_packet_send(vvs, conn, NULL, 0);
  vobs_vopw(vnode)->rendezvous.sent = tosend;
}

/** send an OKTOSEND message to acknowledge a rendez-vous
 * @pre vnode->mutex locked
 * @pre vopw.lock unlocked
 */
static void vopw_oktosend_send(padico_vnode_t vnode, size_t rendezvous_size)
{
  /* receive posted- we must send an oktosend */
  vnode->rbox.cant_interrupt_read = 1; /* We have decided to perform a zero-copy receipt-
                                        * From now on, we cannot cancel, we cannot interrupt.
                                        * We MUST wait the rendezvous data.
                                        */
  const size_t rbox_avl = vnode->rbox.read_posted - vnode->rbox.read_done;
  const size_t accepted_len = (rbox_avl >= rendezvous_size) ?
    /* enough room available in rbox -> take it all */
    rendezvous_size :
    /* oops- we have to split the message */
    rbox_avl;
  padico_out(50, "vnode=%p send oktosend- rendezvous_size=%ld accepted_len=%ld read_posted=%ld read_done=%ld\n",
             vnode, puk_slong(rendezvous_size), puk_slong(accepted_len),
             puk_slong(vnode->rbox.read_posted), puk_slong(vnode->rbox.read_done));
  if(vnode->status.writable && vnode->content.remote)
    {
      const struct vlink_vopw_specific_s*const vvs = vobs_vopw(vnode);
      struct vopw_hdr_s*out_hdr;
      const padico_psp_connection_t conn = vlink_packet_new_message(vvs, vvs->raddr.na_node, &out_hdr);
      vnode_check(vnode, binding);
      out_hdr->op  = htons(VOPW_OP_OKTOSEND);
      out_hdr->tag = htons(vobs_vopw(vnode)->raddr.tag);
      out_hdr->info.OKTOSEND.accepted_len = htonl(accepted_len);
      vlink_packet_send(vvs, conn, NULL, 0);
      padico_out(50, "vnode=%p oktosend sent.\n", vnode);
    }
}


/** Send 'len' more bytes of data on 'vnode' from its current wbox
 * @pre vnode->mutex locked
 * @pre vopw.lock unlocked
 */
static void vopw_data_send(padico_vnode_t vnode, size_t len)
{
  const struct vlink_vopw_specific_s*const vvs = vobs_vopw(vnode);
  struct vopw_hdr_s*out_hdr;
  const padico_psp_connection_t conn = vlink_packet_new_message(vvs, vvs->raddr.na_node, &out_hdr);

  vnode_check(vnode, wbox);
  assert(len <= vnode->wbox.write_posted-vnode->wbox.write_done && len > 0); /* cannot send more than posted */

  if(len <= vvs->config.data_in_header)
    {
      /* SHORT_SEND- single packet */
      out_hdr->op             = htons(VOPW_OP_DATA);
      out_hdr->tag            = htons(vvs->raddr.tag);
      out_hdr->info.DATA.len  = htonl(len);
      out_hdr->info.DATA.tlen = htonl(0);
      memcpy(out_hdr->info.DATA.data, vnode->wbox.write_buffer + vnode->wbox.write_done, len);
      vlink_profile_point(vlink_wprofiler, 3);
      int rc = vlink_packet_send(vvs, conn, NULL, 0);
      vlink_profile_point(vlink_wprofiler, 10);
      padico_out(50, "vnode=%p data sent\n", vnode);
      vnode->wbox.write_done += len;
      if(rc)
        vnode_set_error(vnode, -rc);
    }
  else
    {
      /* LONG_SEND- multi-packet */
      assert(vnode->wbox.write_buffer != NULL);
      const size_t rlen = len;
      const size_t hlen = vvs->config.data_in_header;
      const size_t tlen = rlen - hlen;
      padico_out(50, "vnode=%p LONG_SEND rlen=%lu hlen=%lu tlen=%lu\n", vnode, (unsigned long)rlen, (unsigned long)hlen, (unsigned long)tlen);
      out_hdr->op             = htons(VOPW_OP_DATA);
      out_hdr->tag            = htons(vvs->raddr.tag);
      out_hdr->info.DATA.len  = htonl(len);
      out_hdr->info.DATA.tlen = htonl(tlen);
      memcpy(out_hdr->info.DATA.data, vnode->wbox.write_buffer+vnode->wbox.write_done, hlen);
      int rc = vlink_packet_send(vvs, conn, vnode->wbox.write_buffer+vnode->wbox.write_done + hlen, tlen);
      vnode->wbox.write_done += len;
      vobs_signal(vnode, vobs_attr_wbox);
      padico_out(50, "vnode=%p LONG_SEND ok\n", vnode);
      if(rc)
        vnode_set_error(vnode, -rc);
    }
}

/**
 * @return -1 if impossible to receive, 0 if ok
 */
static inline int vopw_data_check(padico_vnode_t vnode,
                                  const struct vopw_hdr_s*hdr, padico_topo_node_t node_from)
{
  size_t rlen = ntohl(hdr->info.DATA.len);
  int rc = 0;
  padico_out(60, "vnode=%p DATA rlen=%lu\n", vnode, (unsigned long)rlen);
  if( vnode->content.remote &&
      vnode_is_vopw(vnode) &&
      vnode->remote.established &&
      vobs_vopw(vnode)->raddr.na_node == node_from  &&
      vobs_vopw(vnode)->vopw_binding.tag  == ntohs(hdr->tag) )
    {
      /* packet ok */
      padico_out(60, "vnode=%p accepted rlen=%lu bytes\n", vnode, (unsigned long)rlen);
    }
  else
    {
      /* packet error:
       * wrong state, wrong node or wrong tag.
       * => generate warning, but... what should we do? (TODO)
       */
      padico_out(puk_verbose_critical, "I/O error on VLink_Packet socket (DATA)"
                 "\n### from=%s hdr_tag=%d size=%lu "
                 "\n### expected_node=%s expected_tag=%d expected_size=%lu"
                 "\n### data_in_header=%lu\n",
                 padico_topo_node_getname(node_from), ntohs(hdr->tag), (unsigned long)rlen,
                 padico_topo_node_getname(vobs_vopw(vnode)->raddr.na_node),
                 vobs_vopw(vnode)->vopw_binding.tag,
                 (unsigned long)vnode->rbox.read_posted,
                 (unsigned long)vobs_vopw(vnode)->config.data_in_header);
      if(vnode->rbox.read_posted > 0)
        vnode->rbox.read_done = 0;
      vnode_set_error(vnode, EIO);
      rc = -1;
    }
  return rc;
}


/**
 * @pre vnode->mutex locked
 */
static inline void vopw_data_process(padico_vnode_t vnode,
                                     const struct vopw_hdr_s*const hdr,
                                     void*token, padico_psp_pump_t pump)
{
  const size_t rlen = ntohl(hdr->info.DATA.len); /* length to receive */
  size_t hlen = rlen; /* head length */
  size_t tlen = ntohl(hdr->info.DATA.tlen);    /* tail length */
  const size_t h_size = vobs_vopw(vnode)->config.data_in_header;
  if(rlen > h_size)
    {
      hlen = h_size;
    }
  assert(tlen + hlen == rlen);
  vlink_profile_point(vlink_rprofiler, 1);

  assert((!vnode->content.rbox) || vnode->rbox.read_done <= vnode->rbox.read_posted);
  if( (!vnode->content.rbox) ||
      (vnode->content.rbox && (vnode->rbox.read_posted - vnode->rbox.read_done < rlen)))
    {
      /* we cannot receive in place
       * => recv in ibox */
      vopw_ibox_t ibox = vopw_ibox_new();
      padico_out(40, "vnode=%p cannot receive in-place rlen=%ld read_posted=%ld read_done=%ld\n",
                 vnode, puk_ulong(rlen), puk_slong(vnode->rbox.read_posted),
                 puk_slong(vnode->rbox.read_done));
      vlink_stat.num_recv_by_copy++;
      vlink_stat.bytes_recv_by_copy += rlen;
      ibox->what = VOPW_IBOX_DATA;
      VOPW_BUF_ALLOC(ibox->buffer, rlen);
      VOPW_BUF_MEMTOBUFFER(ibox->buffer, hdr->info.DATA.data, hlen);
      if(tlen)
        {
          VOPW_BUF_PUMPTOBUFFER(ibox->buffer, tlen, token, pump);
        }
      vopw_ibox_list_push_back(vobs_vopw(vnode)->iboxes, ibox);

      assert((!vnode->content.rbox) || vnode->rbox.read_done <= vnode->rbox.read_posted);
      vnode->status.readable = 1;
      if(vnode->content.rbox)
        {
          vlink_stat.rbox_too_small++;
          assert(vnode->rbox.read_done <= vnode->rbox.read_posted);
          if(vnode->rbox.read_posted - vnode->rbox.read_done > 0)
            {
              padico_out(50, "vnode=%p copy data size=%ld from ibox\n",
                         vnode, puk_slong(vnode->rbox.read_posted));
              /* copy requested size of data from ibox */
              VOPW_BUF_BUFFERTOMEM(vnode->rbox.read_buffer+vnode->rbox.read_done,
                                   ibox->buffer,
                                   vnode->rbox.read_posted - vnode->rbox.read_done);
              vnode->rbox.read_done = vnode->rbox.read_posted;
              vnode->rbox.read_buffer = NULL;
            }
          vnode->rbox.cant_interrupt_read = 0;
          assert(vnode->rbox.read_done > 0);
          assert(vnode->rbox.read_done <= vnode->rbox.read_posted);
          vobs_signal(vnode, vobs_attr_rbox);
        }
      else
        {
          vlink_stat.no_rbox++;
          vobs_signal(vnode, vobs_attr_status);
        }
      padico_out(50, "vnode=%p exiting after receive-by-copy rlen=%ld\n",
                 vnode, puk_ulong(rlen));
    }
  else
    {
      /* rbox available and is large enough -> direct receive */
      padico_out(40, "vnode=%p direct receive rlen=%ld head=%ld tail=%ld posted=%ld done=%ld\n",
                 vnode, puk_ulong(rlen), puk_ulong(hlen), puk_ulong(tlen),
                 puk_slong(vnode->rbox.read_posted), puk_slong(vnode->rbox.read_done));
      vlink_stat.num_recv_zero_copy++;
      vlink_stat.bytes_recv_zero_copy += rlen;
      memcpy(vnode->rbox.read_buffer+vnode->rbox.read_done, hdr->info.DATA.data, hlen);
      vlink_profile_point(vlink_rprofiler, 2);
      if(tlen)
        {
          padico_out(50, "vnode=%p waiting for remaining %ld bytes\n", vnode, puk_ulong(tlen));
          (*pump)(token, vnode->rbox.read_buffer + vnode->rbox.read_done + hlen, tlen);
          padico_out(50, "vnode=%p received %ld bytes in header, %ld bytes in body (total=%ld)\n",
                     vnode, puk_ulong(hlen), puk_ulong(tlen), puk_ulong(rlen));
        }
      vlink_profile_point(vlink_rprofiler, 3);
      vnode->rbox.cant_interrupt_read = 0; /* Zero-copy data received; operation is now
                                            * interruptible and cancelable.
                                            */
      vnode->rbox.read_done += rlen;
      assert(rlen == hlen+tlen);
      assert(vnode->rbox.read_done > 0);
      assert(vnode->rbox.read_done <= vnode->rbox.read_posted);
      vobs_signal(vnode, vobs_attr_rbox);
      padico_out(50, "vnode=%p exiting read_posted=%ld read_done=%ld\n",
                 vnode, puk_slong(vnode->rbox.read_posted), puk_slong(vnode->rbox.read_done));
      vlink_profile_point(vlink_rprofiler, 4);
    }
}


static void vopw_accepted_send(padico_vnode_t vnode)
{
  vobs_vopw_check(vnode);
  vnode_check(vnode, binding);
  padico_out(30, " vnode=%p incoming connection ACCEPTED!\n", vnode);
  struct vopw_hdr_s*hdr = padico_malloc(sizeof(struct vopw_hdr_s));
  const struct vlink_vopw_specific_s*const vvs = vobs_vopw(vnode);
  const padico_psp_connection_t conn = vlink_packet_new_message(vvs, vvs->raddr.na_node, &hdr);
  hdr->op = htons(VOPW_OP_ACCEPTED);
  hdr->tag = htons(vobs_vopw(vnode)->raddr.tag);
  hdr->info.ACCEPTED.my_tag = htons(vobs_vopw(vnode)->vopw_binding.tag);
  vlink_packet_send(vvs, conn, NULL, 0);
}

static void vopw_connrefused_send(padico_topo_node_t remote_node, uint16_t remote_tag)
{
  struct vopw_deferred_req_s*req = padico_malloc(sizeof(struct vopw_deferred_req_s));
  req->op = 0;
  req->dest_node = remote_node;
  req->vnode = NULL; /* TODO */
  req->hdr.op  = htons(VOPW_OP_CONNREFUSED);
  req->hdr.tag = htons(remote_tag);
  padico_out(40, " sending CONNREFUSED to node=%s tag=%d\n",
             padico_topo_node_getname(remote_node), remote_tag);
  padico_tm_bgthread_start(vopw.pool, &vopw_deferred_req_worker, req, "deferred_req_worker:connrefused");
}

static padico_vnode_t vopw_vnode_lookup(uint16_t port)
{
  padico_vnode_t vnode = NULL;
  int i;
  padico_out(30, "looking up port=%d\n", port);
  vopw_lock_acquire(NULL);
  for(i = 1; i < VOPW_VNODES_MAX; i++)
    {
      vnode = vopw.vnodes[i];
      if(vnode != NULL)
        {
          struct sockaddr_in*addr_in = (struct sockaddr_in*)vnode->binding.primary;
          vobs_vopw_check(vnode);
          vnode_check(vnode, binding);
          assert(vnode->binding.primary->sa_family == AF_INET);
          padico_out(20, "trying vnode=%p (port=%d tag=%d)\n",
                     vnode, ntohs(addr_in->sin_port), vobs_vopw(vnode)->vopw_binding.tag);
          if(ntohs(addr_in->sin_port) == port)
            {
              padico_out(20, "match: vnode=%p (port=%d tag=%d)\n",
                         vnode, ntohs(addr_in->sin_port), vobs_vopw(vnode)->vopw_binding.tag);
              break;
            }
        }
    }
  vopw_lock_release();
  return vnode;
}

static void vopw_dynres_register(uint32_t addr, uint32_t rank, uint16_t port, uint16_t tag)
{
  struct vopw_dynres_entry_s e;
  padico_out(60, "addr=%u; rank=%u; port=%d; tag=%d\n", addr, rank, port, tag);
  e.addr = addr;
  e.rank = rank;
  e.port = port;
  e.tag  = tag;
  marcel_mutex_lock(&vopw.resolver.lock);
  vopw_dynres_vect_push_back(vopw.resolver.cache, e);
  marcel_cond_broadcast(&vopw.resolver.cond);
  marcel_mutex_unlock(&vopw.resolver.lock);
}

static struct vopw_dynres_entry_s* vopw_dynres_lookup(const struct in_addr*addr, uint16_t port)
{
  struct vopw_dynres_entry_s*e = NULL;
  int i;
  padico_out(60, "looking for addr=%s; port=%d\n", inet_ntoa(*addr), port);
  marcel_mutex_lock(&vopw.resolver.lock);
  for(i = 0; i < vopw_dynres_vect_size(vopw.resolver.cache); i++)
    {
      e = vopw_dynres_vect_ptr(vopw.resolver.cache, i);
      if((e->port == port) && (e->addr == addr->s_addr))
        {
          padico_out(60, "found entry=%p: addr=%s; port=%d; rank=%d; tag=%d\n",
                     e, inet_ntoa(*((struct in_addr*)&e->addr)), e->port, e->rank, e->tag);
          marcel_mutex_unlock(&vopw.resolver.lock);
          return e;
        }
    }
  padico_out(60, "addr=%s; port=%d not found\n", inet_ntoa(*addr), port);
  marcel_mutex_unlock(&vopw.resolver.lock);
  return NULL;
}

static struct vopw_dynres_entry_s* vopw_dynres_resolve(const struct in_addr*addr, uint16_t port)
{
  struct vopw_dynres_entry_s*e = vopw_dynres_lookup(addr, port);

  padico_out(60, "addr=%s; port=%d\n", inet_ntoa(*addr), port);
  if(e == NULL)
    {
      struct vopw_deferred_req_s*req = padico_malloc(sizeof(struct vopw_deferred_req_s));
      padico_out(60, "addr=%s; port=%d not found in cache. Sending request.\n",
                 inet_ntoa(*addr), port);
      //      req->dest_rank = remote_node;
      req->vnode = NULL; /* TODO */
      req->op = 0; /* build our header */
      req->hdr.op  = htons(VOPW_OP_GET_TAG);
      req->hdr.tag = htons(VOPW_TAG_ANY);
      req->hdr.info.GETTAG.domain = AF_INET;
      req->hdr.info.GETTAG.type = SOCK_DGRAM;
      req->hdr.info.GETTAG.your_port = htons(port);
      /*  vnode->status.pending_op++; */
      padico_tm_bgthread_start(vopw.pool, &vopw_deferred_req_worker, req, "deferred_req_worker:gettag");

      while((e=vopw_dynres_lookup(addr, port)) == NULL)
        {
          padico_out(60, "waiting reply (cache size=%d)\n",
                     vopw_dynres_vect_size(vopw.resolver.cache));
          marcel_mutex_lock(&vopw.resolver.lock);
          marcel_cond_wait(&vopw.resolver.cond, &vopw.resolver.lock);
          marcel_mutex_unlock(&vopw.resolver.lock);
        }
  }
  return e;
}

static void vopw_dynres_reverse_lookup(padico_topo_node_t node, uint16_t port,
                                       struct sockaddr*addr, int*addrlen)
{
  /* convert: rankfrom/port -> from/fromlen */
  if(addr != NULL)
    {
      struct sockaddr_in*ai = (struct sockaddr_in*)addr;
      ai->sin_family = AF_INET;
      ai->sin_addr.s_addr = htonl(padico_topo_host_getaddr(padico_topo_node_gethost(node))->s_addr);
      ai->sin_port = htons(port);
    }
}

/** control events listener to invalidate vnodes when nodes disappear. */
static void vopw_control_event_listener(void*_event)
{
  const struct padico_control_event_s*event = _event;
  if(event->kind == PADICO_CONTROL_EVENT_DELETE_NODE )
    {
      const padico_topo_node_t node = event->NODE.node;
      int i;
      marcel_mutex_lock(&vopw.lock);
      for(i = 0; i < VOPW_VNODES_MAX; i++)
        {
          padico_vnode_t vnode = vopw.vnodes[i];
          if(vnode && vobs_vopw(vnode)->raddr.na_node == node)
            {
              vnode_lock(vnode);
              if(vnode->content.remote)
                {
                  vnode->remote.can_recv = 0;
                  vnode->remote.can_send = 0;
                  vnode->remote.established = 0;
                  vnode->content.remote = 0;
                  vnode_set_error(vnode, ECONNRESET);
                }
              vnode_unlock(vnode);
            }
        }
      marcel_mutex_unlock(&vopw.lock);
    }
}

/* *** Reception functions ********************************* */
/* ********************************************************* */


#define VOPW_CALLBACK_CASE(OPCODE) \
  case VOPW_OP_ ## OPCODE: \
    vopw_psp_callback_ ## OPCODE (hdr, token, pump, from, tag, op, vnode); \
    break

#define VOPW_CALLBACK_FUNC(OPCODE) \
  static inline void vopw_psp_callback_ ## OPCODE(const struct vopw_hdr_s*const hdr, \
                                                  void*token, \
                                                  padico_psp_pump_t pump, \
                                                  padico_topo_node_t node_from, \
                                                  uint32_t          tag, \
                                                  uint32_t           op, \
                                                  padico_vnode_t  vnode)

/* *** Data ************************************************ */

VOPW_CALLBACK_FUNC(DATA)
{
  if(vnode != NULL && vnode->content.remote && vnode->remote.established)
    {
      if(vopw_data_check(vnode, hdr, node_from) == 0)
        {
          /* packet ok: we receive */
          vopw_data_process(vnode, hdr, token, pump);
        }
      else
        {
          padico_warning("vopw_xchn_callback() data refused! -- VIO internal error\n");
        }
    }
  else
    {
      if(vnode !=NULL
         && vnode->status.connecting
         && vnode_is_vopw(vnode)
         && vobs_vopw(vnode)->vopw_binding.tag == ntohs(hdr->tag))
        {
          vopw_data_process(vnode, hdr, token, pump);
        }
      else
        {
          /* discard data */
          const int rlen = ntohl(hdr->info.DATA.len); /* length to receive */
          int tlen = ntohl(hdr->info.DATA.tlen);  /* tail length */
          padico_warning("discarding remaining data on closed socket- size=%d\n", rlen);
          if(tlen > 0)
            {
              void*tmp = padico_malloc(tlen);
              (*pump)(token, tmp, tlen);
              padico_free(tmp);
            }
        }
    }
  padico_out(60, "vnode=%p DATA processing completed\n", vnode);
}

VOPW_CALLBACK_FUNC(PACKET)
{
  if(vnode != NULL)
    {
      size_t len = ntohl(hdr->info.PACKET.len);
      uint16_t port_from = ntohs(hdr->info.PACKET.port);
      void*ptr = NULL;
      padico_out(40, "vnode=%p received PACKET- len=%lu\n", vnode, (unsigned long)len);
      if(vnode->content.rbox)
        {
          assert(vnode->rbox.read_posted >= len);
          ptr = vnode->rbox.read_buffer;
          vopw_dynres_reverse_lookup(node_from, port_from, vnode->rbox.from, vnode->rbox.fromlen);
          vnode->rbox.read_done = len;
        }
      else
        {
          vopw_ibox_t ibox = vopw_ibox_new();
          ibox->what = VOPW_IBOX_PACKET;
          ibox->node_from = node_from;
          ibox->port = port_from;
          ibox->buffer.data = padico_malloc(len);
          ibox->buffer.total_size = len;
          ibox->buffer.size = len;
          ibox->buffer.offset = 0;
          vopw_ibox_list_push_back(vobs_vopw(vnode)->iboxes, ibox);
          ptr = ibox->buffer.data;
        }
      if(len > vobs_vopw(vnode)->config.packet_in_header)
        {
          (*pump)(token, ptr, len);
        }
      else
        {
          memcpy(ptr, hdr->info.PACKET.data, len);
        }
      if(vnode->content.rbox)
        {
          assert(vnode->rbox.read_done > 0);
          assert(vnode->rbox.read_done <= vnode->rbox.read_posted);
          vobs_signal(vnode, vobs_attr_rbox);
        }
      else
        {
          vobs_signal(vnode, vobs_attr_status);
        }
    }
  else
    {
      padico_fatal("received packet on closed socket. We should discard- TODO\n");
    }
}

/** We have received a RENDEZVOUS- chose&do the appropriate reply
 * @pre vnode->mutex locked
 * @pre vsock_xchn unlocked
 */
VOPW_CALLBACK_FUNC(RENDEZVOUS)
{
  const size_t rendezvous_size = ntohl(hdr->info.RENDEZVOUS.proposed_len);
  padico_out(30, "vnode=%p received RENDEZVOUS - size=%ld.\n",
             vnode, puk_ulong(rendezvous_size));
  if(vnode->content.rbox
     && (vnode->rbox.read_posted != vnode->rbox.read_done)
     && (vopw_ibox_list_empty(vobs_vopw(vnode)->iboxes)) )
    {
      /* an rbox has been posted -the rendezvous is awaited */
      vopw_oktosend_send(vnode, rendezvous_size);
    }
  else
    {
      /* nobody is waiting for this rendezvous */
      vopw_ibox_t ibox = vopw_ibox_new();
      padico_out(50, "vnode=%p nobody is waiting for this RENDEZVOUS -- enqueue.\n", vnode);
      ibox->what = VOPW_IBOX_RENDEZVOUS;
      ibox->rendezvous_size = rendezvous_size;
      vopw_ibox_list_push_back(vobs_vopw(vnode)->iboxes, ibox);
      vnode->status.readable = 1;
      vobs_signal(vnode, vobs_attr_status);
    }
  padico_out(30, "vnode=%p RENDEZVOUS processed\n", vnode);
}


/** We have received an OKTOSEND- chose&do the appropriate reply
 * @pre vnode->mutex locked
 * @pre vsock_xchn unlocked
 */
VOPW_CALLBACK_FUNC(OKTOSEND)
{
  const size_t accepted_len = ntohl(hdr->info.OKTOSEND.accepted_len);
  padico_out(50, "vnode=%p received OKTOSEND - size=%lu\n", vnode, (unsigned long)accepted_len);
  vnode_check(vnode, wbox);
  assert(vobs_vopw(vnode)->rendezvous.received == 0);
  assert(accepted_len <= vnode->wbox.write_posted - vnode->wbox.write_done);
  padico_out(50, "vnode=%p entering accepted_len=%lu\n", vnode, (unsigned long)accepted_len);
  vobs_vopw(vnode)->rendezvous.received = accepted_len;

  vobs_vopw(vnode)->rendezvous.sent = 0;
  vobs_vopw(vnode)->rendezvous.multipart++;
  /* multi-part rendezvous */
  if(vobs_vopw(vnode)->rendezvous.multipart < vobs_vopw(vnode)->config.rendezvous_multipart_max)
    {
      padico_out(50, "vnode=%p sending size=%ld\n", vnode, puk_slong(vobs_vopw(vnode)->rendezvous.received));
      vopw_data_send(vnode, vobs_vopw(vnode)->rendezvous.received);
    }
  else
    {
      /* anyway, send it all! */
      padico_out(50, "vnode=%p sending more than requested (size=%ld)\n",
                 vnode, puk_slong(vnode->wbox.write_posted - vnode->wbox.write_done));
      vopw_data_send(vnode, vnode->wbox.write_posted - vnode->wbox.write_done);
    }
  if(vnode->wbox.write_done < vnode->wbox.write_posted)
    {
      vopw_rendezvous_send(vnode);
    }
  else
    {
      vobs_vopw(vnode)->rendezvous.received = -1;
      vobs_vopw(vnode)->rendezvous.sent     = -1;
      vobs_signal(vnode, vobs_attr_wbox);
      vnode->status.pending_op--;
      vobs_signal(vnode, vobs_attr_status);
    }
  padico_out(30, "vnode=%p OKTOSEND processed\n", vnode);
}

/* *** Connection establishment **************************** */

VOPW_CALLBACK_FUNC(CONNECT)
{
  int remote_tag = ntohs(hdr->info.CONNECT.my_tag);
  int local_tag  = ntohs(hdr->info.CONNECT.your_tag);
  padico_out(50, "received CONNECT to tag=%d\n", local_tag);
  if(vnode == NULL)
    {
      /* find a socket listening on this tag/domain/type */
      vnode =
        (local_tag < VOPW_VNODES_MAX && local_tag >= 0)?
        vopw.vnodes[local_tag]
        :NULL;
      if((vnode == NULL) ||             /* no vnode for this port */
         (!vnode->content.socket) ||    /* vnode is not a socket */
         (!vnode->content.listener) ||  /* socket not listening */
         (padico_vnode_queue_full(vnode->listener.backlog))) /* backlog is full */
        /* TODO: check socket type and domain. */
        {
          /* connection refused */
          padico_fatal("vnode=%p received CONNECT request tag=%d --"
                       "\n  nobody is listening on tag=%d domain=%d type=%d\n",
                       vnode, tag,
                       ntohs(hdr->info.CONNECT.your_tag),
                       ntohl(hdr->info.CONNECT.domain),
                       ntohl(hdr->info.CONNECT.type));
          vopw_connrefused_send(node_from, remote_tag);
        }
      else
        {
          padico_out(40, "vnode=%p recveived CONNECT (tag=%d)- send ACCEPT\n",
                     vnode, remote_tag);
          vopw_acceptor(vnode, node_from, remote_tag);
        }
    }
  else
    {
      padico_fatal("op==VOPW_OP_CONNECT && vnode!=NULL -- we shouldn't be here\n");
    }
}

VOPW_CALLBACK_FUNC(CONNREFUSED)
{
  padico_out(50, "vnode=%p received CONNREFUSED\n", vnode);
  vnode_set_error(vnode, ECONNREFUSED);
  vobs_signal(vnode, vobs_attr_remote);
}

VOPW_CALLBACK_FUNC(ACCEPTED)
{
  uint16_t remote_tag = ntohs(hdr->info.ACCEPTED.my_tag);
  padico_out(50, "vnode=%p received ACCEPTED remote_tag=%d\n",
             vnode, (int)remote_tag);
  vobs_vopw_check(vnode);
  vnode_check(vnode, binding);
  vobs_vopw(vnode)->raddr.na_node = node_from;
  vobs_vopw(vnode)->raddr.tag  = remote_tag;
  vnode->status.writable = 1;
  padico_vsock_connector_finalize(vnode);
}

VOPW_CALLBACK_FUNC(EOS)
{
  padico_out(6, "vnode=%p received EOS: end-of-stream from peer -- tag=%d.\n", vnode, tag);
  if(vnode != NULL)
    {
      vopw_ibox_t ibox = vopw_ibox_new();
      ibox->what = VOPW_IBOX_EOS;
      vopw_ibox_list_push_back(vobs_vopw(vnode)->iboxes, ibox);
    }
  else
    {
      padico_out(30, "concurrent close detected.\n");
    }
}

/* *** Address&tag resolution ****************************** */

VOPW_CALLBACK_FUNC(GET_TAG)
{
  uint16_t local_port = ntohs(hdr->info.GETTAG.your_port);
  vnode = vopw_vnode_lookup(local_port);
  if(vnode == NULL)  /* no vnode for this port */
    {
      padico_out(1, "received GET_TAG for port=%d- unknown\n",
                 local_port);
    }
  else
    {
      padico_out(40, "vnode=%p received GET_TAG for port=%d\n",
                 vnode, (unsigned)local_port);
      {
        struct vopw_deferred_req_s*req = padico_malloc(sizeof(struct vopw_deferred_req_s));
        uint32_t rank = padico_na_rank();
        uint16_t port = ntohs(((struct sockaddr_in*)vnode->binding.primary)->sin_port);
        uint16_t tag  = vobs_vopw(vnode)->vopw_binding.tag;
        //  uint32_t addr = ntohl(((struct sockaddr_in*)vnode->binding.primary)->sin_addr.s_addr);
        uint32_t addr = padico_topo_host_getaddr(padico_topo_node_gethost(padico_topo_getlocalnode()))->s_addr;
        // req->dest_rank = node_from;
        req->vnode = NULL;
        req->op = 0; /* build our header */
        req->hdr.op  = htons(VOPW_OP_GIVE_TAG);
        req->hdr.tag = htons(VOPW_TAG_ANY);
        req->hdr.info.GIVETAG.my_rank = htonl(rank);
        req->hdr.info.GIVETAG.my_addr = htonl(addr);
        req->hdr.info.GIVETAG.my_port = htons(port);
        req->hdr.info.GIVETAG.my_tag  = htons(tag);
        padico_out(50, " sending GIVE_TAG to dest=%s- port=%d; addr=%d; tag=%d\n",
                   padico_topo_node_getname(node_from), (unsigned int)port, addr, tag);
        vnode->status.pending_op++;
        padico_tm_bgthread_start(vopw.pool, &vopw_deferred_req_worker, req, "deferred_req_worker:givetag");
      }
    }
}

VOPW_CALLBACK_FUNC(GIVE_TAG)
{
  padico_out(40, "received GIVE_TAG\n");
  vopw_dynres_register(htonl(hdr->info.GIVETAG.my_addr),
                       htonl(hdr->info.GIVETAG.my_rank),
                       htons(hdr->info.GIVETAG.my_port),
                       htons(hdr->info.GIVETAG.my_tag));
}


/** the great callback function performing *all* receptions from SimplePackets in vopw
 */
static void vopw_psp_handler(const void*_header, padico_topo_node_t from, void*key, padico_psp_pump_t pump, void*token)
{
  const struct vopw_hdr_s*const hdr = _header;
  const uint32_t tag = ntohs(hdr->tag);
  const uint32_t  op = ntohs(hdr->op);
  marcel_t u_tid = (marcel_t)NULL;

  vlink_profile_start(vlink_rprofiler);
  if(tag != VOPW_TAG_ANY)
    {
      vopw_lock_acquire(NULL);
      const padico_vnode_t vnode = vopw.vnodes[tag];
      padico_out(50, "tag=%d resolved into vnode=%p\n", tag, vnode);
      if(vnode != NULL)
        {
          /* ** receive packet for open vnode */
          vnode_lock(vnode);
          vobs_vopw_check(vnode);
          vnode_check(vnode, binding);
          assert(vobs_vopw(vnode)->vopw_binding.tag == tag);
          vopw_lock_release();
          padico_out(40, "vnode=%p received op=%d tag=%d from node=%s\n", vnode, op, tag, padico_topo_node_getname(from));
          switch(op)
            {
              VOPW_CALLBACK_CASE(OKTOSEND);
              VOPW_CALLBACK_CASE(RENDEZVOUS);
              VOPW_CALLBACK_CASE(DATA);
              VOPW_CALLBACK_CASE(PACKET);
              VOPW_CALLBACK_CASE(CONNECT);
              VOPW_CALLBACK_CASE(CONNREFUSED);
              VOPW_CALLBACK_CASE(ACCEPTED);
              VOPW_CALLBACK_CASE(EOS);
            default:
              padico_fatal("vopw_psp_handler() invalid opcode received.\n"
                           " (unknown opcode: op=%d tag=%d from=%s vnode=%p)\n"
                           " -Suspecting network hardware transmission error.\n",
                           op, tag, padico_topo_node_getname(from), vnode);
              break;
            }
          if(vnode->rbox.tid)
            u_tid=vnode->rbox.tid;
          vnode_unlock(vnode);
          if(u_tid)
            marcel_yield_to(u_tid);
        }
      else
        {
          /* ** receive packet for an already closed vnode */
          vopw_lock_release();
          switch(op)
            {
              VOPW_CALLBACK_CASE(EOS);
              VOPW_CALLBACK_CASE(DATA);
              VOPW_CALLBACK_CASE(PACKET);
            default:
              padico_fatal("vopw_psp_handler() invalid opcode received.\n"
                           " (wrong opcode for NULL vnode: op=%d tag=%d from=%s)\n"
                           " -internal error or network hardware transmission error.\n",
                           op, tag, padico_topo_node_getname(from));
              break;
            }
        }
    }
  else
    {
      /* ** receive packet for no particular vnode */
      const padico_vnode_t vnode = NULL;
      switch(op)
        {
          VOPW_CALLBACK_CASE(CONNECT);
          VOPW_CALLBACK_CASE(GET_TAG);
          VOPW_CALLBACK_CASE(GIVE_TAG);
        default:
          padico_fatal("vopw_psp_handler() internal error.\n"
                       " (wrong opcode for null tag: op=%d tag=%d from=%s vnode=NULL)\n"
                       " -Suspecting network hardware transmission error.\n",
                       op, tag, padico_topo_node_getname(from));
          break;
        }
    }
  vlink_profile_point(vlink_rprofiler, 6);
}

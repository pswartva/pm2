/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file Socket factory through a SOCKS4 proxy
 */

#include <Padico/VLink-internals.h>
#include <Padico/Module.h>
#include <Padico/SocketFactory.h>
#include <Padico/Topology.h>


/* ********************************************************* */

static int socks4client_create(void*_status, int family, int type, int protocol);
static int socks4client_connect(void*_status, const struct sockaddr*addr, socklen_t addrlen,
                                padico_socketfactory_connector_t notifier, void*key);
static int socks4client_close(void*_status);

/** 'SocketFactory' facet for SocketFactory-plain
 * @ingroup SF_Plain
 */
static const struct padico_socketfactory_s socks4client_sf_driver =
  {
    .create     = &socks4client_create,
    .bind       = NULL,
    .listen     = NULL,
    .connect    = &socks4client_connect,
    .close      = &socks4client_close,
    .setsockopt = NULL,
    .caps       = padico_vsock_caps_directconnect | padico_vsock_caps_clientonly
  };

static void*socks4client_instantiate(puk_instance_t, puk_context_t);
static void socks4client_destroy(void*);

/** instanciation facet for SocketFactory-plain
 * @ingroup SF_Plain
 */
static const struct puk_component_driver_s socks4client_component_driver =
  {
    .instantiate = &socks4client_instantiate,
    .destroy     = &socks4client_destroy
  };

struct socks4client_status_s
{
  struct puk_receptacle_SocketFactory_s socket;
  const char*gateway;
  const char*port;
  volatile int fd;
  padico_sysio_t io;
  struct
  {
    struct sockaddr_in in_addr;
    padico_socketfactory_connector_t notifier;;
    void*key;
  } connector;
  struct
  {
    int proxy_connecting;
    int proxy_connected;
    int connecting;
    int request_sent;
    int connected;
  } state;
};

static void socks4client_proxy_connect_notifier(void*key, int fd);
static int  socks4client_proxy_reply_notifier(int fd, void*key);
static void socks4client_send_request(struct socks4client_status_s*status);

/* ********************************************************* */

struct socks4client_connect_request_s
{
  uint8_t vn;       /**< version number- should be 4 */
  uint8_t cd;       /**< command code- should be 1 for CONNECT  */
  uint16_t dstport; /**< destination port, in network byte order */
  uint32_t dstip;   /**< destination IP, in network byte order */
  uint8_t null;     /**< NULL byte */
}  __attribute__((packed));

struct socks4client_connect_reply_s
{
  uint8_t vn;       /**< version number- should be 0 */
  uint8_t cd;       /**< command code- should be 90 for success; 91-93 for failure */
  uint16_t dstport; /**< unused */
  uint32_t dstip;   /**< unused */
}  __attribute__((packed));


/* ********************************************************* */

PADICO_MODULE_COMPONENT(SOCKS4_client,
  puk_component_declare("SOCKS4_client",
                        puk_component_provides("PadicoComponent", "component", &socks4client_component_driver),
                        puk_component_provides("SocketFactory", "sf", &socks4client_sf_driver),
                        puk_component_uses("SocketFactory", "socket"),
                        puk_component_attr("gateway", "localhost"),
                        puk_component_attr("port", "1080")));

/* ********************************************************* */

static void*socks4client_instantiate(puk_instance_t instance, puk_context_t context)
{
  struct socks4client_status_s*status = padico_malloc(sizeof(struct socks4client_status_s));
  padico_print("instantiate().\n");
  status->fd = -1;
  status->io = NULL;
  status->connector.notifier = NULL;
  status->connector.key = NULL;
  memset(&status->state, 0, sizeof(status->state));
  puk_context_indirect_SocketFactory(instance, "socket", &status->socket);
  padico_print("indirect socket- status = %p\n", status->socket._status);
  status->gateway = puk_instance_getattr(instance, "gateway");
  status->port = puk_instance_getattr(instance, "port");
  padico_out(40, "done.\n");
  return status;
}

static void socks4client_destroy(void*_status)
{
  struct socks4client_status_s*status = _status;
  socks4client_close(_status);
  padico_free(status);
  padico_out(40, "done.\n");
}

/* ********************************************************* */

static int socks4client_create(void*_status, int family, int type, int protocol)
{
  struct socks4client_status_s*status = _status;
  assert(family == AF_INET);
  assert(type == SOCK_STREAM);
  padico_print("create()- gateway = %s; port = %s\n", status->gateway, status->port);
  /* create the socket */
  int rc = (*status->socket.driver->create)(status->socket._status, family, type, protocol);
  if(rc != 0)
    goto error;
  /* resolve proxy into address */
  padico_topo_host_t gateway = padico_topo_host_resolvebyname(status->gateway);
  if(!gateway)
    padico_fatal("cannot resolve gateway %s.\n", status->gateway);
  const uint16_t port = atoi(status->port);
  const struct in_addr*inaddr = padico_topo_host_getaddr(gateway);
  const struct sockaddr_in addr =
    {
      .sin_family = AF_INET,
      .sin_port = htons(port),
      .sin_addr = *inaddr
    };
  const socklen_t addrlen = sizeof(addr);
  padico_print("connecting to proxy %s:%d\n", status->gateway, port);
  /* connect to the proxy */
  rc = (*status->socket.driver->connect)(status->socket._status,
                                         (struct sockaddr*)&addr, addrlen,
                                         socks4client_proxy_connect_notifier, _status);
  status->state.proxy_connecting = 1;
  padico_out(40, "rc=%d\n", rc);
 error:
  return rc;
}

static int socks4client_connect(void*_status, const struct sockaddr*addr, socklen_t addrlen,
                                padico_socketfactory_connector_t notifier, void*key)
{
  struct socks4client_status_s*status = _status;
  int rc = -1, err = -1;
  struct sockaddr_in*addr_in = NULL;
  padico_print("connect().\n");
  switch(addr->sa_family)
    {
    case AF_INET:
      addr_in = (struct sockaddr_in*)addr;
      break;
    case AF_PADICO:
      if(padico_sockaddr_magic(addr) == AF_PADICO_INET)
        {
          addr_in = padico_sockaddr_payload(struct sockaddr_in, addr);
        }
      else
        {
          err = EAFNOSUPPORT;
          goto error;
        }
      break;
    default:
      padico_warning("address family not supported: %d\n", addr->sa_family);
      err = EAFNOSUPPORT;
      goto error;
    }
  status->connector.notifier = notifier;
  status->connector.key = key;
  status->connector.in_addr = *addr_in;
  status->state.connecting = 1;

  if(status->state.proxy_connected)
    {
      socks4client_send_request(status);

      if(rc == 0 || (rc == -1 && err == EINPROGRESS))
        {
          err = EINPROGRESS;
          padico_io_activate(status->io);
          padico_out(60, "callback_connector() registered\n");
        }
    }
  else
    {
      err = EINPROGRESS;
    }
 error:
  padico_out(40, "err=%d\n", err);
  return -err;
}

static int socks4client_close(void*_status)
{
  struct socks4client_status_s*status = _status;
  int rc = -1;
  status->fd = -1;
  (*status->socket.driver->close)(status->socket._status);
  /* TODO */

  return rc;
}

/** send a CONNECT request to SOCKS proxy when:
 *    -- socketfactory has issued a connect()
 *    -- proxy is connected
 */
static void socks4client_send_request(struct socks4client_status_s*status)
{
  padico_print("SOCKS4: sending request.\n");
  struct socks4client_connect_request_s req =
    {
      .vn = 0x04,
      .cd = 0x01,
      .dstport = htons(status->connector.in_addr.sin_port),
      .dstip = htonl(status->connector.in_addr.sin_addr.s_addr)
    };
  padico_sysio_out(status->fd, &req, sizeof(req));
  status->state.request_sent = 1;

}

/** notification handler when proxy is connected
 *  (connection started in create())
 */
static void socks4client_proxy_connect_notifier(void*key, int fd)
{
  struct socks4client_status_s*status = key;
  padico_print("connected to proxy.\n");
  status->state.proxy_connected = 1;
  status->io = padico_io_register(fd, PADICO_IO_EVENT_READ, &socks4client_proxy_reply_notifier, status);
  status->fd = fd;
  if(status->state.connecting)
    socks4client_send_request(status);
}

/** notification handler when reply from proxy is received.
 */
static int socks4client_proxy_reply_notifier(int fd, void*key)
{
  struct socks4client_status_s*status = key;
  struct socks4client_connect_reply_s reply;
  padico_sysio_in(status->fd, &reply, sizeof(reply));
  padico_print("got reply code = 0x%x\n", reply.cd);
  if(reply.cd == 0x90)
    {
      status->state.connected = 1;
      padico_io_unregister(status->io);
      status->io = NULL;
      (*status->connector.notifier)(status->connector.key, status->fd);
    }
  return 0;
}

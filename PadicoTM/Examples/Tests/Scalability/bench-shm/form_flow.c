#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#define BUFFMAX 5000

int main(int argc, char *argv[])
{
  if (argc!=3){
    printf("wrong\n");
    return 0;
  }
  else
    {
    FILE* fichier = NULL;

    fichier = fopen(argv[1], "r");

    char buff[BUFFMAX];
    int nbprocess=atoi(argv[2])/2;
    double pack;
    double minlat;
    double medlat;
    double maxlat;
    double minflow;
    double medflow;
    double maxflow;
    double minflowmbs;
    double medflowmbs;
    double maxflowmbs;
    int size;
    int i=0;int j=0;int k=0;
    if (fichier != NULL)
      {
        printf("# size |  latency min | latency med | latency max |  min 10^6 B/s |  med 10^6 B/s | max 10^6 B/s | min MB/s | med MB/s | max MB/s |\n");
        //trouver la taille
        while(fgets(buff,BUFFMAX,fichier)!=NULL)
          {
            if(strstr(buff,"//")!=NULL)
              {
                sscanf(buff,"// %d ",&size);
                //printf("%d\n",size);
                i++;
              }
          }
        //rangement
        rewind(fichier);
        double latmin[size][nbprocess+1];
        double latmed[size][nbprocess+1];
        double latmax[size][nbprocess+1];
        for(i=0;i<size;i++)
          {
            for(j=0;j<nbprocess+1;j++)
              {
                latmin[i][j]=0;
                latmed[i][j]=0;
                latmax[i][j]=0;
              }
          }

        while(fgets(buff,BUFFMAX,fichier)!=NULL)
          {
            if(strchr(buff,'#')==NULL && strstr(buff,"//")==NULL)
              {
                sscanf(buff," %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf ",&pack,&minlat,&medlat,&maxlat,&minflow,&minflowmbs,&medflow,&medflowmbs,&maxflow,&maxflowmbs);
                i=0;
                j=0;
                while(latmin[i][0]!=pack&&latmin[i][0]!=0)
                  {
                    i++;
                  }
                if(latmin[i][0]==0)
                  {
                    latmin[i][0]=pack;
                    latmed[i][0]=pack;
                    latmax[i][0]=pack;
                  }
                j=1;
                while(latmin[i][j]!=0&&j<nbprocess+1)
                  {
                    j++;
                  }
                if(j==nbprocess+1)
                  {
                    printf("error");
                  }
                else
                  {
                    latmin[i][j]=minlat;
                    latmax[i][j]=maxlat;
                    latmed[i][j]=medlat;
                  }

              }
          }
        for(k=0;k<size;k++)
          {
            minlat=1000000000000;
            maxlat=0;
            medlat=0;
            j=0;
            for(i=1;i<=nbprocess;i++)
              {
              if(minlat>latmin[k][i]&&latmin[k][i]!=0)
                {
                  minlat=latmin[k][i];
                }
              if(maxlat<latmax[k][i]&& latmax[k][i]!=0)
                {
                  maxlat=latmax[k][i];
                }
              if(latmed[k][i]!=0)
                {
                  medlat+=latmed[k][i];
                  j++;
                }
              }
            medlat/=j;
            minflow=latmin[k][0]/minlat;
            medflow=latmin[k][0]/medlat;
            maxflow=latmin[k][0]/maxlat;
            minflowmbs=minflow/1.048576;
            medflowmbs=medflow/1.048576;
            maxflowmbs=maxflow/1.048576;
            printf("%9.3f\t%9.3f\t%9.3f\t%9.3f\t%9.3f\t%9.3f\t%9.3f\t%9.3f\t%9.3f\t%9.3f\n",latmin[k][0],minlat,medlat,maxlat,minflow,medflow,maxflow,minflowmbs,medflowmbs,maxflowmbs);
          }
      }
    fflush(NULL);
    fclose(fichier);
    }
  return 0;
}

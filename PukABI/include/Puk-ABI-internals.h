/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @note This file should be "clean", i.e. it must *not* include
 * any other header.
 */

#ifndef PUK_ABI_INTERNALS_H
#define PUK_ABI_INTERNALS_H

#include "Puk-ABI-config.h"

#include <stdarg.h>

#ifdef __GNUC__
# define PUK_UNUSED  __attribute__ ((__unused__))
#endif

#ifndef NULL
#  define NULL ((void*)0)
#endif


/* *** String mangling ************************************* */

#define _TOSTRING(NAME) #NAME
#define _PASTE(N1, N2) N1 ## N2
#define _PASTE3(N1, N2, N3) N1 ## N2 ## N3


/* *** Linking ********************************************* */

#define __PUK_ABI_SYM_INTERNAL __attribute__ ((visibility("internal")))


/** PukABI should show warnings on bad fd automatic recovery */
#ifdef DEBUG
#  undef PUK_ABI_SHOW_RECOVERY
#endif /* DEBUG */
/** activate PukABI traces */
#undef  PUK_ABI_TRACE_ON
/** show pid in PukABI traces */
#undef  PUK_ABI_TRACE_PID
/** trace spinlocks done by PukABI */
#undef  PUK_ABI_TRACE_SPINLOCK
/** enable/disable assertion check */
#ifdef DEBUG
#define  PUK_ABI_CHECK_ASSERT
#endif /* DEBUG */
/** show stack top pointer in traces */
#undef  PUK_ABI_SHOW_STACKTOP

/** Maximum fd allocated by PukABI- @todo check getdtablesize(), RLIMIT_NOFILE, FD_SETSIZE or OPEN_MAX for a better value */
#define PUK_ABI_FD_MAX     1024


/* *** Puk ABI tools to deal without libc
 */
__PUK_ABI_SYM_INTERNAL void puk_abi_trace(char*msg, ...);
__PUK_ABI_SYM_INTERNAL void puk_abi_print(char*msg, ...);
__PUK_ABI_SYM_INTERNAL void puk_abi_fatal(char*msg, ...) __attribute__ ((noreturn));;
#ifdef PUK_ABI_CHECK_ASSERT
__PUK_ABI_SYM_INTERNAL void puk_abi_do_assert(int cond, const char*file, int line, const char*func);
#  define puk_abi_assert(COND) puk_abi_do_assert(COND, __FILE__, __LINE__, __FUNCTION__)
#else
#  define puk_abi_assert(COND)
#endif


/* *** Spinlock
 */
typedef void (*puk_spinlock_handler_t)(void);
struct puk_spinlock_s
{
  void (*acquire)(void);
  void (*release)(void);
  int  (*trylock)(void); /* 1: lock busy; 0: lock free */
};
#ifdef PUK_ABI_TRACE_SPINLOCK
#define puk_spinlock_acquire() \
  (puk_spinlock.acquire?puk_abi_print("A: %s", __FUNCTION__),(*puk_spinlock.acquire)():(void)0)
#define puk_spinlock_release() \
  (puk_spinlock.release?(*puk_spinlock.release)(),puk_abi_print("R: %s", __FUNCTION__):(void)0)
#define puk_spinlock_trylock() \
  (puk_spinlock.trylock?puk_abi_print("T: %s", __FUNCTION__),(*puk_spinlock.trylock)():0)
#else
#define puk_spinlock_acquire() (puk_spinlock.acquire?(*puk_spinlock.acquire)():(void)0)
#define puk_spinlock_release() (puk_spinlock.release?(*puk_spinlock.release)():(void)0)
#define puk_spinlock_trylock() (puk_spinlock.trylock?(*puk_spinlock.trylock)():0)
#endif

extern struct puk_spinlock_s puk_spinlock;

/* ** init
 */
__PUK_ABI_SYM_INTERNAL extern int puk_abi_init_done;
__PUK_ABI_SYM_INTERNAL extern void puk_abi_preinit(void);


/* *** File descriptors
 */

__PUK_ABI_SYM_INTERNAL int puk_fds_alloc(int kind, int fd, const char*caller);
__PUK_ABI_SYM_INTERNAL void puk_fds_free(int abi_fd);

/** checks whether the given fd is real or virtual */
__PUK_ABI_SYM_INTERNAL int  puk_fds_isreal(int abi_fd);
__PUK_ABI_SYM_INTERNAL int  puk_fdset_isreal(const void*fds1, const void*fds2, const void*fds3, int fdmax);
__PUK_ABI_SYM_INTERNAL int  puk_pollfd_isreal(void*ptr, int nfds);

/* *** oracles for variadic functions
 */
__PUK_ABI_SYM_INTERNAL int puk_abi_oracle__fcntl(int cmd);
__PUK_ABI_SYM_INTERNAL int puk_abi_oracle__open_flags(int oflag);
__PUK_ABI_SYM_INTERNAL int puk_abi_oracle__open_kind(const char*path);

/* *** helpers for libc-dependant functions
 */
__PUK_ABI_SYM_INTERNAL void puk_abi_helper__perror(const char *s);
__PUK_ABI_SYM_INTERNAL int puk_abi_helper__vfscanf(void *_stream, const char *format, va_list _args);
__PUK_ABI_SYM_INTERNAL int puk_abi_helper__vfprintf(void *_stream, const char *format, va_list _args);
__PUK_ABI_SYM_INTERNAL int puk_abi_helper__vprintf(const char *format, va_list _args);
__PUK_ABI_SYM_INTERNAL const char**puk_abi_helper_build_argv(const char*arg, va_list args, int*need_free);
__PUK_ABI_SYM_INTERNAL void puk_abi_helper_free_argv(const char**argv);


/* *** memory management
 */
#ifdef PUKABI_ENABLE_MEM
__PUK_ABI_SYM_INTERNAL void puk_abi_mem_init(void);

__PUK_ABI_SYM_INTERNAL void puk_abi_mem_invalidate(void*ptr, int size);

__PUK_ABI_SYM_INTERNAL void puk_abi_mem_free_invalidate(void*ptr);
#endif /* PUKABI_ENABLE_MEM */

/* *** Symbols
 */
struct puk_abi_sym_s
{
  const void*ptr_real;     /**< NULL means "not initialized" => lookup */
  const void*ptr_virtual;  /**< NULL means "not initialized" => use real  */
  const char name[];       /**< name of the symbol */
};

/** @internal don't use directly; use PUK_ABI_REAL. */
extern const void*puk_abi_real_symbol(struct puk_abi_sym_s*sym);

/* internal symbol manipulation- don't use outside of PukABI */

/** name of the pointer-to-function type defined by PukABI (e.g. foo_func_t) */
#define PUK_ABI_SYM_FUNCTYPE(SYM) _PASTE(SYM, _func_t)

/** name of the function that return the symbol descriptor (e.g. __puk_abi_getdesc__foo__) */
#define PUK_ABI_SYM_GETDESC(SYM)  _PASTE3(__puk_abi_getdesc__, SYM, __)

/** name of the symbol descriptor entry (e.g. __puk_abi_desc__foo__) */
#define PUK_ABI_SYM_DESCNAME(SYM) _PASTE3(__puk_abi_desc__, SYM, __)

/** name of the stub function defined by PukABI (e.g. __puk_abi_stub__foo__) */
#define PUK_ABI_SYM_STUB(SYM)     _PASTE3(__puk_abi_stub__, SYM, __)

/** name of the wrapper function defined by PukABI (e.g. __puk_abi_wrap__foo) */
#define PUK_ABI_SYM_WRAP(SYM)     _PASTE(__puk_abi_wrap__, SYM)

/** pointer to the "real" symbol (dynamically resolved, e.g. dlsym(RTLD_NEXT, "foo")) */
#define PUK_ABI_SYM_REAL(SYM)    (((PUK_ABI_SYM_GETDESC(SYM)())->ptr_real)?:    \
                              puk_abi_real_symbol(PUK_ABI_SYM_GETDESC(SYM)()))
/** pointer to the "virtual" symbol registered into PukABI */
#define PUK_ABI_SYM_VIRTUAL(SYM) ((PUK_ABI_SYM_GETDESC(SYM)()->ptr_virtual)?:   \
                              (puk_abi_fatal("# PukABI: virtual symbol not found: *%s*\n##### Abort.\n",\
                                             _TOSTRING(SYM)),NULL))

/** This macro declares all the types and forward functions declarations needed for a PukABI-managed symbol
 */
#define PUK_ABI_MKTYPE(SYM, RETTYPE, ARGSDECL)           \
  /** stub:   __puk_abi_stub__foo */                     \
  __PUK_ABI_SYM_INTERNAL RETTYPE PUK_ABI_SYM_STUB(SYM) ARGSDECL; \
  /** wrap:   __puk_abi_wrap__foo */                     \
  extern             RETTYPE PUK_ABI_SYM_WRAP(SYM) ARGSDECL; \
  /** struct: __puk_abi_desc__foo__ */                   \
  extern struct puk_abi_sym_s*PUK_ABI_SYM_GETDESC(SYM)(void); \
  /** func type: foo_func_t */                           \
  typedef            RETTYPE (*PUK_ABI_SYM_FUNCTYPE(SYM)) ARGSDECL;



/* *** errno
 * POSIX source in _REENTRANT mode requires 'errno' to be a macro
 * of the form: *((*func)())
 * We must guess the name of 'func', though :-)
 */

#if defined(PUKABI_SYMBOL_ERRNO)
#  define puk_abi_errno_sym PUKABI_SYMBOL_ERRNO
#else
#warning "Puk-ABI: PUKABI_SYMBOL_ERRNO not defined in autoconf. Trying to guess."
#  if defined(linux)
#    define puk_abi_errno_sym __errno_location
#  elif defined(sun)
#    define puk_abi_errno_sym ___errno
#  else
#    error "Puk-ABI: unsupported OS. Cannot guess pointer to errno."
#  endif
#endif

extern int*puk_abi_errno_stub(void); /* actually, an alias to PUK_ABI_SYM_STUB(__errno_location) */

#define puk_abi_errno_real() \
 (*((PUK_ABI_SYM_FUNCTYPE(puk_abi_errno_sym)) PUK_ABI_SYM_REAL(puk_abi_errno_sym)))()

#define PUK_ABI_ERRNO_INHERIT \
 { *(puk_abi_errno_stub()) = *(puk_abi_errno_real()); }


#endif /* PUK_ABI_INTERNALS_H */

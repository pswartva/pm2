/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief internal functions for timing
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>
#include "Puk-internals.h"

static struct
{
  int init_done;
  puk_tick_t time_orig;
} puk_timing = { .init_done = 0 };

/** converts a tick diff to microseconds, with offset compensation */
double puk_tick2usec(puk_tick_t t)
{
  static double clock_offset = -1.0;
  if(clock_offset < 0)
    {
      /* lazy clock offset init */
      int i;
      for(i = 0; i < 1000; i++)
        {
          puk_tick_t t1, t2;
          PUK_GET_TICK(t1);
          PUK_GET_TICK(t2);
          const double raw_delay = puk_tick2usec_raw(PUK_TICK_RAW_DIFF(t1, t2));
          if(raw_delay < clock_offset || clock_offset < 0)
            clock_offset = raw_delay;
        }
    }
  const double delay = puk_tick2usec_raw(t) - clock_offset;
  return (delay > 0.0) ? delay : 0.0; /* tolerate slight offset inacurracy */
}

/** computes a delay between ticks, in microseconds */
double puk_ticks2delay(const puk_tick_t*t1, const puk_tick_t *t2)
{
  puk_tick_t tick_diff = PUK_TICK_RAW_DIFF(*t1, *t2);
  return puk_tick2usec(tick_diff);
}

double puk_timing_stamp(void)
{
  if(!puk_timing.init_done)
    {
      PUK_GET_TICK(puk_timing.time_orig);
      puk_timing.init_done = 1;
    }
  puk_tick_t t;
  PUK_GET_TICK(t);
  return PUK_TIMING_DELAY(puk_timing.time_orig, t);
}

/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <rdma/fabric.h>
#include <rdma/fi_cm.h>
#include <rdma/fi_domain.h>
#include <rdma/fi_endpoint.h>
#include <rdma/fi_eq.h>
#include <rdma/fi_errno.h>

#define NM_FI_VERSION FI_VERSION(1, 5)

#if HAVE_DECL_FI_CONTEXT2
#define NM_OFI_USE_CONTEXT2 1
#define nm_ofi_context fi_context
#else
#define nm_ofi_context fi_context2
#endif


struct nm_ofi_common_context_s
{
  struct fi_info*fi;
  struct fid_fabric*fabric;
  struct fid_domain*domain;
  enum fi_ep_type ep_type;  /**< only FI_EP_RDM & FI_EP_MSG are supported */
  int need_mr;
  struct fid_eq*eq;         /**< event queue, for connection */
};

#define NM_OFI_CHECK_RC(RC, FUNC)                                       \
  {                                                                     \
    if((RC) != 0)                                                       \
      {                                                                 \
        NM_FATAL("# nm_ofi: %s()- error %d (%s).\n", #FUNC, (RC), fi_strerror(-(RC))); \
      }                                                                 \
  }


static inline void nm_ofi_common_init(struct nm_ofi_common_context_s*p_ofi_context, puk_context_t context,
                                      enum fi_ep_type ep_type, uint64_t caps)
{
  /* init libfabric */
  struct fi_info*hints = fi_allocinfo();
  /* capabilities we request from the provider*/
  hints->caps = FI_MSG | caps;
  /* modes we offer to the provider */
  /* do not offer FI_LOCAL_MR; rely on provider rcache for now; works with Cray CXI */
  hints->mode = /* FI_LOCAL_MR | */ FI_CONTEXT | FI_ASYNC_IOV;
#ifdef NM_OFI_USE_CONTEXT2
  hints->mode |= FI_CONTEXT2;
#endif
  /* enforce EP type */
  hints->ep_attr->type = ep_type; /* FI_EP_RDM or FI_EP_MSG */
  hints->domain_attr->mr_mode = FI_MR_PROV_KEY;
  /* detect provider */
  const char*p_provider = puk_context_getattr(context, "provider");
  if(strcmp(p_provider, "auto") != 0)
    {
      padico_out(puk_verbose_notice, "FI provider forced by attribute = %s\n", p_provider);
      hints->fabric_attr->prov_name = padico_strdup(p_provider);
    }
  /* detect fabric */
  const char*p_fabric = puk_context_getattr(context, "fabric");
  if(strcmp(p_fabric, "auto") != 0)
    {
      padico_out(puk_verbose_notice, "FI fabric forced by attribute = %s\n", p_fabric);
      hints->fabric_attr->name = padico_strdup(p_fabric);
    }
  /* detect domain */
  const char*p_domain = puk_context_getattr(context, "domain");
  if(strcmp(p_domain, "auto") != 0)
    {
      padico_out(puk_verbose_notice, "FI domain forced by attribute = %s\n", p_domain);
      hints->domain_attr->name = padico_strdup(p_domain);
    }
  /* get available providers */
  struct fi_info*fi = NULL;
  int rc = fi_getinfo(NM_FI_VERSION, NULL, NULL, 0, hints, &fi);
  NM_OFI_CHECK_RC(rc, "fi_getinfo");
  assert(fi != NULL);
  /* select provider */
  p_ofi_context->fi = fi;
  int i = 0;
  while(fi)
    {
      padico_out(puk_verbose_notice, "info[%d] provider = %s; domain = %s; fabric = %s; ep_type = %s; addr_format = %d; local_mr = %d; max_msg_size = %lu; inject_size = %lu\n",
                 i,
                 fi->fabric_attr->prov_name,
                 fi->domain_attr->name,
                 fi->fabric_attr->name,
                 ((fi->ep_attr->type == FI_EP_RDM) ? "RDM" : ((fi->ep_attr->type == FI_EP_MSG) ? "MSG" : "unknown" )),
                 fi->addr_format,
                 !!(fi->mode & FI_LOCAL_MR),
                 fi->ep_attr->max_msg_size,
                 fi->tx_attr->inject_size);
      fi = fi->next;
      i++;
    }
  if(p_ofi_context->fi == NULL)
    {
      NM_FATAL("no provider found with type RDM or MSG.\n");
    }
  p_ofi_context->need_mr = !!(p_ofi_context->fi->mode & FI_LOCAL_MR);
  p_ofi_context->ep_type = p_ofi_context->fi->ep_attr->type;
  padico_out(puk_verbose_notice, "selected provider = %s; domain = %s; fabric = %s; local MR = %d\n",
             p_ofi_context->fi->fabric_attr->prov_name, p_ofi_context->fi->domain_attr->name,
             p_ofi_context->fi->fabric_attr->name, p_ofi_context->need_mr);

}


static inline void nm_ofi_common_open(struct nm_ofi_common_context_s*p_ofi_context)
{
  /* open fabric */
  int rc = fi_fabric(p_ofi_context->fi->fabric_attr, &p_ofi_context->fabric, NULL);
  NM_OFI_CHECK_RC(rc, "fi_fabric");
  /* open event queue */
  struct fi_eq_attr eq_attr;
  memset(&eq_attr, 0, sizeof(eq_attr));
  eq_attr.size = p_ofi_context->fi->tx_attr->size;
  eq_attr.flags = 0;
  eq_attr.wait_obj = FI_WAIT_UNSPEC;
  rc = fi_eq_open(p_ofi_context->fabric, &eq_attr, &p_ofi_context->eq, NULL);
  NM_OFI_CHECK_RC(rc, "fi_eq_open");
  /* open domain */
  rc = fi_domain(p_ofi_context->fabric, p_ofi_context->fi, &p_ofi_context->domain, NULL);
  NM_OFI_CHECK_RC(rc, "fi_domain");

}

static inline void nm_ofi_common_close(struct nm_ofi_common_context_s*p_ofi_context)
{
  fi_close(&p_ofi_context->eq->fid);
  fi_close(&p_ofi_context->domain->fid);
  fi_close(&p_ofi_context->fabric->fid);
  fi_freeinfo(p_ofi_context->fi);
  puk_usleep(100000); /* HACK HERE- allow libfabric internal threads to exit before we unload the driver;
                       * fi_close() on domain & fabric is _not_ synchronous */
}

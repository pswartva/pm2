/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief lock-free FIFO queues implementations.
 * Multiple implementations are available:
 * - 'atomic' : lock-free queue as circular array, using atomics.
 *    This is the default. This implementation is not suitable for
 *    use in interrupt handlers.
 * - 'nblfq' : non-blocking queue, suitable for interrupt handlers.
 *    It uses tagged pointers and thus can only store pointers.
 * - 'nblfq2' : same as nblfq, but with DWCAS to store any 64-bit data.
 * - transaction-based implementations, using transactional memory.
 *    Not truly lock-free since it uses locks in case transaction fails.
 * - external implementations : requires the --enable-extqueues config flag
 *    before they can be used. Some restrictions may apply (arch support,
 *    C++ support, etc.)
 */

#include <assert.h>

#ifndef PUK_LFQUEUE_H
#define PUK_LFQUEUE_H

extern void puk_lfbackoff(int p);

#include "Puk-nblfq.h"

#if defined(PUK_ENABLE_LIBCONCURRENT) && !defined(__cplusplus)
#  include "Puk-libconcurrent.h"
#endif

#if defined(PUK_ENABLE_CFQUEUES)
#  include "Puk-cfqueues.h"
#endif

#if defined(PUK_ENABLE_LOOQUEUE)
#  include "Puk-looqueue.h"
#endif

#if defined(PUK_ENABLE_WCQ) && !defined(__cplusplus)
#  include "Puk-wcq.h"
#endif

#if defined(PUK_ENABLE_WFQUEUE)
#  include "Puk-wfqueue.h"
#endif

#if defined(PUK_LFQUEUE_DEFAULT_TYPE)
#  define PUK_LFQUEUE_TYPE(ENAME, TYPE, LFQUEUE_NULL, LFQUEUE_SIZE)   \
  PUK_LFQUEUE_DEFAULT_TYPE(ENAME, TYPE, LFQUEUE_NULL, LFQUEUE_SIZE)
#  define PUK_LFQUEUE_KIND PUK_LFQUEUE_DEFAULT_KIND
#elif defined(PUK_USE_TRANSACTION)
#  include "Puk-transaction.h"
#  if defined(PUK_ENABLE_GNU_TM)
#    define PUK_LFQUEUE_TYPE(ENAME, TYPE, LFQUEUE_NULL, LFQUEUE_SIZE)   \
  PUK_LFQUEUE_TM_TYPE(ENAME, TYPE, LFQUEUE_NULL, LFQUEUE_SIZE)
#  define PUK_LFQUEUE_KIND "gnutm"
#  elif defined(PUK_ENABLE_RTM)
#    define PUK_LFQUEUE_TYPE(ENAME, TYPE, LFQUEUE_NULL, LFQUEUE_SIZE)   \
  PUK_LFQUEUE_RTM_TYPE(ENAME, TYPE, LFQUEUE_NULL, LFQUEUE_SIZE)
#  define PUK_LFQUEUE_KIND "rtm"
#  else
#    error "no TM method"
#  endif
#else
/* by default, use atomic-based lock-free queues */
#  define PUK_LFQUEUE_TYPE(ENAME, TYPE, LFQUEUE_NULL, LFQUEUE_SIZE)     \
  PUK_LFQUEUE_ATOMIC_TYPE(ENAME, TYPE, LFQUEUE_NULL, LFQUEUE_SIZE)
#  define PUK_LFQUEUE_KIND "atomic"
#endif /* PUK_USE_TRANSACTION */


#if defined(PUK_ENABLE_PROFILE) && !defined(__cplusplus)
/* do not profile lfqueue in C++ since it is inlined
 * but Puk profiling does not compile in C++ */
#define PUK_LFQUEUE_PROFILE 1
#endif

#ifdef PUK_LFQUEUE_PROFILE
struct puk_lfqueue_profile_s
{
  unsigned long long n_enqueue;
  unsigned long long n_dequeue;
  unsigned long long n_enqueue_full;
  unsigned long long n_dequeue_empty;
  unsigned long long n_enqueue_race;
  unsigned long long n_dequeue_race;
};
static inline void puk_lfqueue_profile_init(struct puk_lfqueue_profile_s*p, const char*ename)
{
  padico_string_t s = padico_string_new();
  padico_string_printf(s, "puk_lfqueue.%s.%p.n_enqueue", ename, p);
  p->n_enqueue = 0;
  puk_profile_var_def(unsigned_long_long, counter, &p->n_enqueue,
                      "puk_lfqueue", padico_string_get(s),
                      "total number of enqueue operation");
  padico_string_printf(s, "puk_lfqueue.%s.%p.n_dequeue", ename, p);
  p->n_dequeue = 0;
  puk_profile_var_def(unsigned_long_long, counter, &p->n_dequeue,
                      "puk_lfqeue", padico_string_get(s),
                      "total number of dequeue operation");
  padico_string_printf(s, "puk_lfqueue.%s.%p.n_enqueue_full", ename, p);
  p->n_enqueue_full = 0;
  puk_profile_var_def(unsigned_long_long, counter, &p->n_enqueue_full,
                      "puk_lfqueue", padico_string_get(s),
                      "total number of enqueue attempted while queue was full");
  padico_string_printf(s, "puk_lfqueue.%s.%p.n_dequeue_empty", ename, p);
  p->n_dequeue_empty = 0;
  puk_profile_var_def(unsigned_long_long, counter, &p->n_dequeue_empty,
                      "puk_lfqueue", padico_string_get(s),
                      "total number of dequeue attempted while queue was empty");
  padico_string_printf(s, "puk_lfqueue.%s.%p.n_enqueue_race", ename, p);
  p->n_enqueue_race = 0;
  puk_profile_var_def(unsigned_long_long, counter, &p->n_enqueue_race,
                      "puk_lfqueue", padico_string_get(s),
                      "total number of race detected by CAS while enqueueing");
  padico_string_printf(s, "puk_lfqueue.%s.%p.n_dequeue_race", ename, p);
  p->n_dequeue_race = 0;
  puk_profile_var_def(unsigned_long_long, counter, &p->n_dequeue_race,
                      "puk_lfqueue", padico_string_get(s),
                      "total number of race detected by CAS while dequeueing");
}
#define puk_lfqueue_profile_inc(I) __sync_fetch_and_add(&I, 1);
#else
struct puk_lfqueue_profile_s
{ /* empty */ };
static inline void puk_lfqueue_profile_init(struct puk_lfqueue_profile_s*p, const char*ename)
{ };
#define puk_lfqueue_profile_inc(I)
#endif

/** builds a lock-free FIFO type and functions.
 *  ENAME is base name for symbols
 *  TYPE is the type of elements in queue- must be atomic (1, 2, 4, or 8 bytes long)
 *  LFQUEUE_NULL is a nil value to fill and detect empty cells
 *  LFQUEUE_SIZE is the array size
 *  Only 'init', 'enqueue', 'dequeue' operations are available.
 *  Others operations such as 'empty', 'size' are useless for lock-free
 *  Iterators are not possible.
 *  push_front/push_back/pop_front/pop_back is probably possible at a high
 *  cost in code size and performance. For simplicity and performance, only
 *  'enqueue' = push_back; 'dequeue' = pop_front are available (1 atomic op each).
 * Conventions:
 *   _head: next cell to enqueue
 *   _tail: next cell to dequeue
 *   _head == _tail     => queue empty
 *   _head == _tail - 1 => queue full
 */
#define PUK_LFQUEUE_ATOMIC_TYPE(ENAME, TYPE, LFQUEUE_NULL, LFQUEUE_SIZE) \
  typedef TYPE ENAME ## _lfqueue_elem_t;                                \
                                                                        \
  struct ENAME ## _lfqueue_s                                            \
  {                                                                     \
    volatile ENAME ## _lfqueue_elem_t _queue[(LFQUEUE_SIZE)];           \
    volatile unsigned _head;                                            \
    volatile unsigned _tail;                                            \
    struct puk_lfqueue_profile_s _profile;                              \
  };                                                                    \
                                                                        \
  __attribute__((unused))                                               \
  static inline void ENAME ## _lfqueue_init(struct ENAME ## _lfqueue_s*queue) \
  {                                                                     \
    int i;                                                              \
    for(i = 0; i < (LFQUEUE_SIZE); i++)                                 \
      {                                                                 \
        queue->_queue[i] = (LFQUEUE_NULL);                              \
      }                                                                 \
    queue->_head = 0;                                                   \
    queue->_tail = 0;                                                   \
    puk_lfqueue_profile_init(&queue->_profile, #ENAME);                 \
  }                                                                     \
                                                                        \
  __attribute__((unused))                                               \
  static inline int ENAME ## _lfqueue_enqueue_ext(struct ENAME ## _lfqueue_s*queue, ENAME ## _lfqueue_elem_t e, int single) \
  {                                                                     \
    puk_lfqueue_profile_inc(queue->_profile.n_enqueue);                 \
    int r = 0; /* allocate a slot for value */                          \
  retry:                                                                \
    ;                                                                   \
    const unsigned head = queue->_head;                                 \
    const unsigned tail = queue->_tail;                                 \
    const unsigned next = (head + 1) % (LFQUEUE_SIZE);                  \
    if(((head + (unsigned)(LFQUEUE_SIZE) - tail) % (LFQUEUE_SIZE)) >= (LFQUEUE_SIZE - 2)) \
      {                                                                 \
        /* Queue full, abort enqueue                                    \
         * Actually, we cannot distinguish whether queue is actually    \
         * full or we see dequeue before the corresponding enqueue.     \
         * (no memory fence in case of single reader)                   \
         */                                                             \
        puk_lfqueue_profile_inc(queue->_profile.n_enqueue_full);        \
        return -1;                                                      \
      }                                                                 \
    if(single)                                                          \
      {                                                                 \
        queue->_head = next;                                            \
      }                                                                 \
    else                                                                \
      if(!__sync_bool_compare_and_swap(&queue->_head, head, next))      \
        {                                                               \
          puk_lfqueue_profile_inc(queue->_profile.n_enqueue_race);      \
          puk_lfbackoff(r++); goto retry;                               \
        }                                                               \
    /* slot is still NULL for concurrent readers, already reserved if concurrent writers */ \
    while(queue->_queue[head] != (LFQUEUE_NULL))                        \
      { __sync_synchronize(); }                                         \
    /* store value in reserved slot */                                  \
    queue->_queue[head] = e;                                            \
    return 0;                                                           \
  }                                                                     \
                                                                        \
  __attribute__((unused))                                               \
  static inline int ENAME ## _lfqueue_enqueue(struct ENAME ## _lfqueue_s*queue, ENAME ## _lfqueue_elem_t e) \
  { return ENAME ## _lfqueue_enqueue_ext(queue, e, 0); }                \
                                                                        \
  __attribute__((unused))                                               \
  static inline int ENAME ## _lfqueue_enqueue_single_writer(struct ENAME ## _lfqueue_s*queue, ENAME ## _lfqueue_elem_t e) \
  { return ENAME ## _lfqueue_enqueue_ext(queue, e, 1); }                \
                                                                        \
  __attribute__((unused))                                               \
  static inline ENAME ## _lfqueue_elem_t ENAME ## _lfqueue_dequeue_ext(struct ENAME ## _lfqueue_s*queue, int single) \
  {                                                                     \
    puk_lfqueue_profile_inc(queue->_profile.n_dequeue);                 \
    ENAME ## _lfqueue_elem_t e = (LFQUEUE_NULL);                        \
    unsigned tail;                                                      \
    int r = 0; /* try to dequeue */                                     \
  retry:                                                                \
    ;                                                                   \
    tail = queue->_tail;                                                \
    const unsigned next = (tail + 1) % (LFQUEUE_SIZE);                  \
    if(tail == queue->_head)                                            \
      {                                                                 \
        /* queue was empty, abort dequeue */                            \
        puk_lfqueue_profile_inc(queue->_profile.n_dequeue_empty);       \
        return e;                                                       \
      }                                                                 \
    if(single)                                                          \
      {                                                                 \
        queue->_tail = next;                                            \
      }                                                                 \
    else                                                                \
      if(!__sync_bool_compare_and_swap(&queue->_tail, tail, next))      \
        {                                                               \
          puk_lfqueue_profile_inc(queue->_profile.n_dequeue_race);      \
          puk_lfbackoff(r++);                                           \
          goto retry;                                                   \
        }                                                               \
    e = queue->_queue[tail];                                            \
    while(e == (LFQUEUE_NULL))                                          \
      { __sync_synchronize(); e = queue->_queue[tail]; }                \
    queue->_queue[tail] = (LFQUEUE_NULL);                               \
    assert(e != (LFQUEUE_NULL));                                        \
    return e;                                                           \
  }                                                                     \
                                                                        \
  __attribute__((unused))                                               \
  static inline ENAME ## _lfqueue_elem_t ENAME ## _lfqueue_dequeue(struct ENAME ## _lfqueue_s*queue) \
  { return ENAME ## _lfqueue_dequeue_ext(queue, 0); }                   \
                                                                        \
  __attribute__((unused))                                               \
  static inline ENAME ## _lfqueue_elem_t ENAME ## _lfqueue_dequeue_single_reader(struct ENAME ## _lfqueue_s*queue) \
  { return ENAME ## _lfqueue_dequeue_ext(queue, 1); }                   \
                                                                        \
  /** hint whether the queue is empty.                                  \
   *  Do not rely on it: state may have changed before dequeue          \
   *  Usefull to reduce active polling, though */                       \
  __attribute__((unused))                                               \
  static inline int ENAME ## _lfqueue_empty(struct ENAME ## _lfqueue_s*queue) \
  {                                                                     \
    return(queue->_tail == queue->_head);                               \
  }                                                                     \
  /** hint about the number of elements in queue .                      \
   * Do not rely on it: state may have changed before function return.  \
   */                                                                   \
  __attribute__((unused))                                               \
  static inline int ENAME ## _lfqueue_load(struct ENAME ## _lfqueue_s*queue) \
  {                                                                     \
    return((LFQUEUE_SIZE + queue->_head - queue->_tail) % LFQUEUE_SIZE); \
  }                                                                     \
  /** get the allocated queue size */                                   \
  __attribute__((unused))                                               \
  static inline int ENAME ## _lfqueue_size(struct ENAME ## _lfqueue_s*queue __attribute__((unused))) \
  {                                                                     \
    return (LFQUEUE_SIZE);                                              \
  }


/** Build a dynamic lock-free queue. Compared to regular lfqueue, this one
 * is dynamically allocated, with a dynamic size.
 */
#define PUK_DLFQ_TYPE(ENAME, TYPE, DLFQ_NULL)                           \
  typedef TYPE ENAME ## _dlfq_elem_t;                                   \
                                                                        \
  struct ENAME ## _dlfq_s                                               \
  {                                                                     \
    ENAME ## _dlfq_elem_t*_queue;                                       \
    volatile unsigned _head;                                            \
    volatile unsigned _tail;                                            \
    unsigned _size;                                                     \
  };                                                                    \
                                                                        \
  __attribute__((unused))                                               \
  static inline void ENAME ## _dlfq_init(struct ENAME ## _dlfq_s*queue, unsigned size) \
  {                                                                     \
    queue->_queue = padico_malloc(size * sizeof(ENAME##_dlfq_elem_t));  \
    unsigned i;                                                         \
    for(i = 0; i < (size); i++)                                         \
      {                                                                 \
        queue->_queue[i] = (DLFQ_NULL);                                 \
      }                                                                 \
    queue->_size = size;                                                \
    queue->_head = 0;                                                   \
    queue->_tail = 0;                                                   \
  }                                                                     \
                                                                        \
  __attribute__((unused))                                               \
  static inline void ENAME ## _dlfq_destroy(struct ENAME ## _dlfq_s*queue) \
  {                                                                     \
    padico_free(queue->_queue);                                         \
    queue->_queue = NULL;                                               \
  }                                                                     \
                                                                        \
  __attribute__((unused))                                               \
  static inline int ENAME ## _dlfq_enqueue_ext(struct ENAME ## _dlfq_s*queue, ENAME ## _dlfq_elem_t e, int single) \
  {                                                                     \
    int r = 0; /* allocate a slot for value */                          \
  retry:                                                                \
    ;                                                                   \
    const unsigned head = queue->_head;                                 \
    const unsigned tail = queue->_tail;                                 \
    const unsigned size = queue->_size;                                 \
    const unsigned next = (head + 1) % (size);                          \
    if(((head + size - tail) % size) >= (size - 2))                     \
      {                                                                 \
        /* Queue full, abort enqueue                                    \
         * Actually, we cannot distinguish whether queue is actually    \
         * full or we see dequeue before the corresponding enqueue.     \
         * (no memory fence in case of single reader)                   \
         */                                                             \
        return -1;                                                      \
      }                                                                 \
    if(single)                                                          \
      {                                                                 \
        queue->_head = next;                                            \
      }                                                                 \
    else                                                                \
      if(!__sync_bool_compare_and_swap(&queue->_head, head, next))      \
        { puk_lfbackoff(r++); goto retry; }                             \
    /* slot is still NULL for concurrent readers, already reserved if concurrent writers */ \
    while(queue->_queue[head] != (DLFQ_NULL))                           \
      { __sync_synchronize(); }                                         \
    /* store value in reserved slot */                                  \
    queue->_queue[head] = e;                                            \
    return 0;                                                           \
  }                                                                     \
                                                                        \
  __attribute__((unused))                                               \
  static inline int ENAME ## _dlfq_enqueue(struct ENAME ## _dlfq_s*queue, ENAME ## _dlfq_elem_t e) \
  { return ENAME ## _dlfq_enqueue_ext(queue, e, 0); }                   \
                                                                        \
  __attribute__((unused))                                               \
  static inline int ENAME ## _dlfq_enqueue_single_writer(struct ENAME ## _dlfq_s*queue, ENAME ## _dlfq_elem_t e) \
  { return ENAME ## _dlfq_enqueue_ext(queue, e, 1); }                   \
                                                                        \
  __attribute__((unused))                                               \
  static inline ENAME ## _dlfq_elem_t ENAME ## _dlfq_dequeue_ext(struct ENAME ## _dlfq_s*queue, int single) \
  {                                                                     \
    ENAME ## _dlfq_elem_t e = (DLFQ_NULL);                              \
    unsigned tail;                                                      \
    int r = 0; /* try to dequeue */                                     \
  retry:                                                                \
    ;                                                                   \
    tail = queue->_tail;                                                \
    const unsigned next = (tail + 1) % queue->_size;                    \
    if(tail == queue->_head)                                            \
      {                                                                 \
        /* queue was empty, abort dequeue */                            \
        return e;                                                       \
      }                                                                 \
    if(single)                                                          \
      {                                                                 \
        queue->_tail = next;                                            \
      }                                                                 \
    else                                                                \
      if(!__sync_bool_compare_and_swap(&queue->_tail, tail, next))      \
        { puk_lfbackoff(r++); goto retry; }                             \
    e = queue->_queue[tail];                                            \
    while(e == (DLFQ_NULL))                                             \
      { __sync_synchronize(); e = queue->_queue[tail]; }                \
    queue->_queue[tail] = (DLFQ_NULL);                                  \
    assert(e != (DLFQ_NULL));                                           \
    return e;                                                           \
  }                                                                     \
                                                                        \
  __attribute__((unused))                                               \
  static inline ENAME ## _dlfq_elem_t ENAME ## _dlfq_dequeue(struct ENAME ## _dlfq_s*queue) \
  { return ENAME ## _dlfq_dequeue_ext(queue, 0); }                      \
                                                                        \
  __attribute__((unused))                                               \
  static inline ENAME ## _dlfq_elem_t ENAME ## _dlfq_dequeue_single_reader(struct ENAME ## _dlfq_s*queue) \
  { return ENAME ## _dlfq_dequeue_ext(queue, 1); }                      \
                                                                        \
  /** hint whether the queue is empty.                                  \
   *  Do not rely on it: state may have changed before dequeue          \
   *  Usefull to reduce active polling, though */                       \
  __attribute__((unused))                                               \
  static inline int ENAME ## _dlfq_empty(struct ENAME ## _dlfq_s*queue) \
  {                                                                     \
    return(queue->_tail == queue->_head);                               \
  }                                                                     \
  /** hint about the number of elements in queue .                      \
   * Do not rely on it: state may have changed before function return.  \
   */                                                                   \
  __attribute__((unused))                                               \
  static inline int ENAME ## _dlfq_load(struct ENAME ## _dlfq_s*queue)  \
  {                                                                     \
    return((queue->_size + queue->_head - queue->_tail) % queue->_size); \
  }                                                                     \
  /** get the allocated queue size */                                   \
  __attribute__((unused))                                               \
  static inline int ENAME ## _dlfq_size(struct ENAME ## _dlfq_s*queue)  \
  {                                                                     \
    return queue->_size;                                                \
  }                                                                     \
  /* Resize the DLFQ; needs external locking! */                        \
  __attribute__((unused))                                               \
  static inline void ENAME ## _dlfq_resize(struct ENAME ## _dlfq_s*queue, int newsize) \
  {                                                                     \
    const int load = ENAME ## _dlfq_load(queue);                        \
    if(newsize - 2 < load)                                              \
      {                                                                 \
        padico_fatal("cannot resize DLFQ smaller than actual content.\n"); \
      }                                                                 \
    ENAME##_dlfq_elem_t*newbuf = padico_malloc(newsize * sizeof(ENAME##_dlfq_elem_t)); \
    int i;                                                              \
    int head = 0;                                                       \
    for(i = 0; i < newsize; i++)                                        \
      {                                                                 \
        ENAME##_dlfq_elem_t e = ENAME##_dlfq_dequeue_single_reader(queue); \
        if(e != (DLFQ_NULL))                                            \
          {                                                             \
            newbuf[i] = e;                                              \
            head++;                                                     \
          }                                                             \
        else                                                            \
          {                                                             \
            newbuf[i] = (DLFQ_NULL);                                    \
          }                                                             \
      }                                                                 \
    padico_free(queue->_queue);                                         \
    queue->_size = newsize;                                             \
    queue->_queue = newbuf;                                             \
    queue->_head = head;                                                \
    queue->_tail = 0;                                                   \
  }


#endif /* PUK_LFQUEUE_H */

#include <stdio.h>
#include <strings.h>
#include <string.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
#include "helper.h"

#define MSGLEN 512

int main(int argc, char *argv[])
{
  usage(argc, argv);
  int port = atoi(argv[3]);
  int sock_fd = create_socket();
  char msg[MSGLEN];
  memset(msg, 0, MSGLEN);
  int rc = 0;

  if(strncmp("server",argv[1],6)==0)
    {
      bind_and_listen(sock_fd, port);
      struct sockaddr_in cli_addr;
      int clilen = sizeof(cli_addr);
      int clifd = accept(sock_fd,  (struct sockaddr *) &cli_addr,&clilen);
      if (clifd < 0)
        print_error("accept()");
      if((rc = recv(clifd, msg, MSGLEN, 0)) < 0)
          print_error("recv()");
      printf("Server: I receive \"%s\"\n", msg);
      char *tmp = strndup(msg, rc);
      sprintf(msg,"Hello %s", tmp);
      free(tmp);
      printf("Server: I send \"%s\"\n", msg);
      if(send(clifd, msg, strlen(msg), 0) < 0)
          print_error("send()");
      close2(clifd);
   }
  else if(strncmp("client",argv[1],6)==0)
    {
      connect2(sock_fd, port, argv[2]);
      printf("Client: I send \"client\"\n");
      if(send(sock_fd, "client\0", 7, 0) < 0)
          print_error("send()");
      if(recv(sock_fd, msg, MSGLEN, 0) < 0)
          print_error("recv()");
      printf("Client: I receive \"%s\"\n", msg);
    }
  close2(sock_fd);

  return 0;
}

/* bench-lfqueue1.c
 * benchmark for single process lfqueue
 */

#include <stdio.h>
#include <unistd.h>

#include "bench-common.h"
#include <Padico/Module.h>
#include <Padico/Puk.h>

int bench_lfqueue1_init(void);
int bench_lfqueue1_run(int argc, char**argv);
void bench_lfqueue1_finalize(void);

PADICO_MODULE_DECLARE(bench_lfqueue1, bench_lfqueue1_init, bench_lfqueue1_run, bench_lfqueue1_finalize);

/* ********************************************************* */

#define BUFNUM 512

PUK_LFQUEUE_TYPE(bench_buf, void*, NULL, BUFNUM);

static struct bench_buf_lfqueue_s q;

static struct bench_buf_lfqueue_s pool;

int bench_lfqueue1_init(void)
{
  bench_buf_lfqueue_init(&q);
  bench_buf_lfqueue_init(&pool);
  int i;
  for(i = 0; i < BUFNUM - 1; i++)
    {
      bench_buf_lfqueue_enqueue(&pool, (void*)(1 + i));
    }
  return 0;
}

void bench_lfqueue1_finalize(void)
{
}

static void bench_lfqueue1_server(char*buffer, size_t lenbuf)
{
  void*b;

  b = bench_buf_lfqueue_dequeue(&pool);
  bench_buf_lfqueue_enqueue(&q, b);
  b = bench_buf_lfqueue_dequeue(&q);
  bench_buf_lfqueue_enqueue(&pool, b);

  b = bench_buf_lfqueue_dequeue(&pool);
  bench_buf_lfqueue_enqueue(&q, b);
  buffer = bench_buf_lfqueue_dequeue(&q);
  bench_buf_lfqueue_enqueue(&pool, b);
}

static void bench_lfqueue1_client(char*buffer, size_t lenbuf)
{
  /* client does nothing */
}

static struct bench_desc_s bench_lfqueue1 =
  {
    "lfqueue1",
    &bench_lfqueue1_server,
    &bench_lfqueue1_client
  };

int bench_lfqueue1_run(int argc, char**argv)
{
  the_bench(&bench_lfqueue1);
  return 0;
}

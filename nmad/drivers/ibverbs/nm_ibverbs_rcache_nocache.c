/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include "nm_ibverbs.h"
#include "nm_ibverbs_rcache.h"

#include <Padico/Module.h>

PADICO_MODULE_BUILTIN(nm_ibverbs_rcache_nocache, NULL, NULL, NULL);

static void nm_ibverbs_nocache_init(struct nm_ibverbs_context_s*p_ibverbs_context);
static void nm_ibverbs_nocache_mem_reg(struct nm_ibverbs_context_s*p_ibverbs_context, const void*ptr, size_t len,
                                       struct ibv_mr**pp_mr, void**pp_handle);
static void nm_ibverbs_nocache_mem_unreg(struct nm_ibverbs_context_s*p_ibverbs_context, const void*ptr, size_t len,
                                         struct ibv_mr*p_mr, void*p_handle);

const struct nm_ibverbs_reg_driver_s nm_ibverbs_reg_driver_nocache =
  {
    .init      = &nm_ibverbs_nocache_init,
    .mem_reg   = &nm_ibverbs_nocache_mem_reg,
    .mem_unreg = &nm_ibverbs_nocache_mem_unreg
  };

/* ********************************************************* */
/* ** nocache */

static void nm_ibverbs_nocache_init(struct nm_ibverbs_context_s*p_ibverbs_context)
{
  padico_out(puk_verbose_notice, "using nocache policy in ibrcache.\n");
}

static void nm_ibverbs_nocache_mem_reg(struct nm_ibverbs_context_s*p_ibverbs_context, const void*ptr, size_t len,
                                       struct ibv_mr**pp_mr, void**pp_handle)
{
  *pp_mr = nm_ibverbs_reg_mr(p_ibverbs_context, (void*)ptr, len, NULL);
}

static void nm_ibverbs_nocache_mem_unreg(struct nm_ibverbs_context_s*p_ibverbs_context, const void*ptr, size_t len,
                                         struct ibv_mr*p_mr, void*p_handle)
{
  int rc = nm_ibverbs_dereg_mr(p_ibverbs_context, p_mr);
  if(rc != 0)
    {
      NM_FATAL("error %d (%s) while deregistering memory.\n", rc, strerror(rc));
    }
}

/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>

#include <portals4.h>

#include "nm_portals4_common.h"
#include <nm_minidriver.h>
#include <nm_private.h>
#include <Padico/Module.h>

/*
 * TODO-
 *   - manage the case where no more MD handles are available
 *   - factorize NI handle between bybuf & large
 *   - dynamically resize number of bufs
 *   - dropped events
 *   - cancel blocking calls
 *   - selective reload
 *   - array with direct access for status_table (gain: 50 usec.)
 *   - allow pw->max_len to be larger than NM_MAX_UNEXPECTED in case both send/recv support buf send
 *
 * notes:
 * - message dropped: count PTL_SR_DROP_COUNT
 */

/** size of the event queue used for send */
#define NM_PORTALS4_BYBUF_SENDQUEUE_LEN 128

/** size of the event queue used for recv */
#define NM_PORTALS4_BYBUF_RECVQUEUE_LEN 128

/** size of buffers */
#define NM_PORTALS4_BYBUF_SBUF_SIZE (64 * 1024) /* 64 kB */

/** size of buffer on the receiver side */
#define NM_PORTALS4_BYBUF_RBUF_SIZE (NM_PORTALS4_BYBUF_SBUF_SIZE * 64) /* 64 x 64 kB = 4 MB */

/** number of buffers on the receiving side */
#define NM_PORTALS4_BYBUF_RBUF_NUM 4


static void*nm_portals4_bybuf_instantiate(puk_instance_t instance, puk_context_t context);
static void nm_portals4_bybuf_destroy(void*status);

static const struct puk_component_driver_s nm_portals4_bybuf_component =
  {
    .instantiate = &nm_portals4_bybuf_instantiate,
    .destroy     = &nm_portals4_bybuf_destroy
  };

static void nm_portals4_bybuf_getprops(puk_context_t context, struct nm_minidriver_properties_s*props);
static void nm_portals4_bybuf_init(puk_context_t context, const void**drv_url, size_t*url_size);
static void nm_portals4_bybuf_close(puk_context_t context);
static void nm_portals4_bybuf_connect(void*_status, const void*remote_url, size_t url_size);
static void nm_portals4_bybuf_disconnect(void*_status);
static int  nm_portals4_bybuf_send_buf_get(void*_status, void**p_buffer, nm_len_t*p_len);
static int  nm_portals4_bybuf_send_buf_post(void*_status, nm_len_t len);
static int  nm_portals4_bybuf_send_poll(void*_status);
static int  nm_portals4_bybuf_recv_buf_poll(void*_status, void**p_buffer, nm_len_t*p_len);
static int  nm_portals4_bybuf_recv_buf_release(void*_status);
static int  nm_portals4_bybuf_recv_probe_any(puk_context_t p_context, void**_status);
static int  nm_portals4_bybuf_recv_wait_any(puk_context_t p_context, void**_status);
static int  nm_portals4_bybuf_recv_cancel(void*_status);
static int  nm_portals4_bybuf_recv_cancel_any(puk_context_t p_context);


static const struct nm_minidriver_iface_s nm_portals4_bybuf_minidriver =
  {
    .getprops         = &nm_portals4_bybuf_getprops,
    .init             = &nm_portals4_bybuf_init,
    .close            = &nm_portals4_bybuf_close,
    .connect          = &nm_portals4_bybuf_connect,
    .disconnect       = &nm_portals4_bybuf_disconnect,
    .send_iov_post    = NULL,
    .send_data_post   = NULL,
    .send_buf_get     = &nm_portals4_bybuf_send_buf_get,
    .send_buf_post    = &nm_portals4_bybuf_send_buf_post,
    .send_poll        = &nm_portals4_bybuf_send_poll,
    .recv_iov_post    = NULL,
    .recv_data_post   = NULL,
    .recv_buf_poll    = &nm_portals4_bybuf_recv_buf_poll,
    .recv_buf_release = &nm_portals4_bybuf_recv_buf_release,
    .recv_probe_any   = &nm_portals4_bybuf_recv_probe_any,
    .recv_poll_one    = NULL,
    .recv_wait_any    = &nm_portals4_bybuf_recv_wait_any,
    .recv_cancel      = &nm_portals4_bybuf_recv_cancel,
    .recv_cancel_any  = &nm_portals4_bybuf_recv_cancel_any
  };

/* ********************************************************* */

PADICO_MODULE_COMPONENT(Minidriver_portals4_bybuf,
  puk_component_declare("Minidriver_portals4_bybuf",
                        puk_component_provides("PadicoComponent", "component", &nm_portals4_bybuf_component),
                        puk_component_provides("NewMad_minidriver", "minidriver", &nm_portals4_bybuf_minidriver)));

/* ********************************************************* */

static inline int nm_portals4_process_eq(const ptl_process_t*p_process1, const ptl_process_t*p_process2)
{
  return ((p_process1->phys.nid == p_process2->phys.nid) && (p_process1->phys.pid == p_process2->phys.pid));
}
static inline uint32_t nm_portals4_process_hash(const ptl_process_t*p_process)
{
  return puk_hash_default(p_process, sizeof(ptl_process_t));
}

struct nm_portals4_bybuf_s;

PUK_HASHTABLE_TYPE(nm_portals4_status, ptl_process_t*, struct nm_portals4_bybuf_s*,
                   &nm_portals4_process_hash, &nm_portals4_process_eq, NULL);

struct nm_portals4_bybuf_sbuf_s
{
  char buffer[NM_PORTALS4_BYBUF_SBUF_SIZE]; /**< buffer containing data to send*/
  nm_len_t length;                          /**< length actually used of the buffer */
  ptl_handle_md_t md_handle;                /**< memory descriptor for the above buffer */
  struct nm_portals4_bybuf_s*p_status;      /**< instance currently using this sbuf for sending */
  int completed;
  int acked;
};

PUK_LFSTACK_TYPE(nm_portals4_bybuf_sbuf, struct nm_portals4_bybuf_sbuf_s sbuf;);

/** 'portals4' driver per-context data. */
struct nm_portals4_bybuf_context_s
{
  int threaded;                     /**< whether the driver is used in a threaded context */
  /* globals */
  ptl_ni_limits_t ni_limits;
  ptl_handle_ni_t ni_handle;        /**< handle to the network interface */
  struct nm_portals4_addr_s local;  /**< local address */
  struct nm_portals4_status_hashtable_s status_table; /**< status hashed by process_id */
  /* send */
  struct nm_portals4_bybuf_sbuf_lfstack_s sbuf_cache; /**< cache of prea-alloced, pre-registered send buffers */
  ptl_handle_eq_t send_eq_handle;                     /**< global event queue for send events */
  /* recv */
  char rbufs[NM_PORTALS4_BYBUF_RBUF_SIZE * NM_PORTALS4_BYBUF_RBUF_NUM]; /**< global receive buffers */
  int rbufs_posted[NM_PORTALS4_BYBUF_RBUF_NUM]; /**< whether N-th buffer is currently posted */
  int rbufs_pending[NM_PORTALS4_BYBUF_RBUF_NUM];
  ptl_handle_me_t me_handles[NM_PORTALS4_BYBUF_RBUF_NUM]; /**< list entry for receive */
  ptl_handle_eq_t global_eq_handle; /**< global event queue for recv */
  struct nm_portals4_bybuf_s*p_ready_status; /**< next ready status */
  void*ready_ptr;                   /**< pointer to received data */
  nm_len_t ready_len;               /**< length of the next ready packet */
#ifdef NMAD_PROFILE
  /* profiling */
  struct
  {
    unsigned long long n_outoforder_ack;
    unsigned long long n_pt_disabled;
  } profiling;
#endif /* NMAD_PROFILE */
};

/** 'portals4' per-instance status. */
struct nm_portals4_bybuf_s
{
  struct nm_portals4_bybuf_context_s*p_portals4_context;
  struct nm_portals4_addr_s peer;
  struct
  {
    struct nm_portals4_bybuf_sbuf_s*p_sbuf; /**< send buffer for the send currently in progress */
    int code; /**< status code of the send operation */
  } send;
  struct
  {
    int index;
  } recv;
};

static void nm_portals4_bybuf_recv_reload(struct nm_portals4_bybuf_context_s*p_portals4_context);
static void nm_portals4_bybuf_recv_poll_all(struct nm_portals4_bybuf_context_s*p_portals4_context);

/* ********************************************************* */

static void*nm_portals4_bybuf_instantiate(puk_instance_t instance, puk_context_t p_context)
{
  struct nm_portals4_bybuf_s*p_status = padico_malloc(sizeof(struct nm_portals4_bybuf_s));
  struct nm_portals4_bybuf_context_s*p_portals4_context = puk_context_get_status(p_context);
  p_status->p_portals4_context = p_portals4_context;
  p_status->send.p_sbuf = NULL;
  return p_status;
}

static void nm_portals4_bybuf_destroy(void*_status)
{
  struct nm_portals4_bybuf_s*p_status = _status;
  padico_free(p_status);
}

/* ********************************************************* */

static void nm_portals4_bybuf_getprops(puk_context_t context, struct nm_minidriver_properties_s*props)
{
  props->profile.latency = 2000;
  props->profile.bandwidth = 10000;
  props->capabilities.supports_iovec = 0;
  props->capabilities.max_msg_size = NM_PORTALS4_BYBUF_SBUF_SIZE;
  props->capabilities.supports_buf_send = 1;
  props->capabilities.supports_buf_recv = 1;
  props->capabilities.supports_wait_any = 1;
  props->capabilities.prefers_wait_any = 0;
  props->capabilities.supports_recv_any = 1;
  props->nickname = "portals4-bybuf";
}

static void nm_portals4_bybuf_init(puk_context_t context, const void**drv_url, size_t*url_size)
{
  struct nm_portals4_bybuf_context_s*p_portals4_context = padico_malloc(sizeof(struct nm_portals4_bybuf_context_s));
  puk_context_set_status(context, p_portals4_context);

#ifdef NMAD_PROFILE
  puk_profile_var_defx(unsigned_long_long, counter, &p_portals4_context->profiling.n_outoforder_ack, 0,
                       "Minidriver_portals4_bybuf", "number of out-of-order ACKs received"
                       "Minidriver_portals4_bybuf.context-%p.n_outoforder_ack", p_portals4_context);
  puk_profile_var_defx(unsigned_long_long, counter, &p_portals4_context->profiling.n_pt_disabled, 0,
                       "Minidriver_portals4_bybuf", "number of times PT was disabled by flow-control",
                       "Minidriver_portals4_bybuf.context-%p.n_pt_disabled", p_portals4_context);
#endif /* NMAD_PROFILE */

  int rc = PtlInit();
  nm_portals4_check_error(rc, "failed to initialise portals4 library");

  rc = PtlNIInit(PTL_IFACE_DEFAULT, PTL_NI_MATCHING | PTL_NI_PHYSICAL, PTL_PID_ANY, NULL,
                 &p_portals4_context->ni_limits, &p_portals4_context->ni_handle);
  nm_portals4_check_error(rc, "failed to initialise portals4 network interface");

  NM_DISPF("portals4_bybuf init- max_unexpected_headers = %d; max_volatile_size = %d; max_msg_size = %d\n",
           p_portals4_context->ni_limits.max_unexpected_headers, (int)p_portals4_context->ni_limits.max_volatile_size,
           (int)p_portals4_context->ni_limits.max_msg_size);
  PtlGetPhysId(p_portals4_context->ni_handle, &p_portals4_context->local.process_id);

  rc = PtlEQAlloc(p_portals4_context->ni_handle, NM_PORTALS4_BYBUF_RECVQUEUE_LEN, &p_portals4_context->global_eq_handle);
  nm_portals4_check_error(rc, "failed to allocate event queue");

  rc = PtlPTAlloc(p_portals4_context->ni_handle, /* options */ PTL_PT_FLOWCTRL,
                  p_portals4_context->global_eq_handle,
                  PTL_PT_ANY, &p_portals4_context->local.pt_index);
  nm_portals4_check_error(rc, "failed to allocate portal table");

  int i;
  for(i = 0; i < NM_PORTALS4_BYBUF_RBUF_NUM; i++)
    {
      p_portals4_context->rbufs_posted[i] = 0;
      p_portals4_context->rbufs_pending[i] = 0;
    }
  nm_portals4_status_hashtable_init(&p_portals4_context->status_table);
  /* event queue for send */
  rc = PtlEQAlloc(p_portals4_context->ni_handle, NM_PORTALS4_BYBUF_SENDQUEUE_LEN, &p_portals4_context->send_eq_handle);
  nm_portals4_check_error(rc, "failed to allocate event queue");
  nm_portals4_bybuf_sbuf_lfstack_init(&p_portals4_context->sbuf_cache);
  p_portals4_context->p_ready_status = NULL;
  p_portals4_context->threaded = NM_IS_THREADED;

  /* start polling immediately to ensure buffer are ready soon for recv */
  nm_portals4_bybuf_recv_reload(p_portals4_context);

  *drv_url = &p_portals4_context->local;
  *url_size = sizeof(p_portals4_context->local);
}

static void nm_portals4_bybuf_close(puk_context_t context)
{
  struct nm_portals4_bybuf_context_s*p_portals4_context = puk_context_get_status(context);
  /* flush sbufs */
  struct nm_portals4_bybuf_sbuf_lfstack_cell_s*p_cell = nm_portals4_bybuf_sbuf_lfstack_pop(&p_portals4_context->sbuf_cache);
  while(p_cell != NULL)
    {
      int rc = PtlMDRelease(p_cell->sbuf.md_handle);
      nm_portals4_check_error(rc, "failed to release memory descriptor");
      padico_free(p_cell);
      p_cell = nm_portals4_bybuf_sbuf_lfstack_pop(&p_portals4_context->sbuf_cache);
    }
  PtlEQFree(p_portals4_context->send_eq_handle);
  PtlNIFini(p_portals4_context->ni_handle);
  PtlFini();
  nm_portals4_bybuf_sbuf_lfstack_destroy(&p_portals4_context->sbuf_cache);
  puk_context_set_status(context, NULL);
  padico_free(p_portals4_context);
}

static void nm_portals4_bybuf_connect(void*_status, const void*remote_url, size_t url_size)
{
  struct nm_portals4_bybuf_s*p_status = _status;
  struct nm_portals4_bybuf_context_s*p_portals4_context = p_status->p_portals4_context;
  assert(url_size == sizeof(struct nm_portals4_addr_s));
  const struct nm_portals4_addr_s*p_peer = remote_url;
  p_status->peer = *p_peer;
  nm_portals4_status_hashtable_insert(&p_portals4_context->status_table, &p_status->peer.process_id, p_status);
}

static void nm_portals4_bybuf_disconnect(void*_status)
{
  struct nm_portals4_bybuf_s*p_status = _status;
  struct nm_portals4_bybuf_context_s*p_portals4_context = p_status->p_portals4_context;
  nm_portals4_status_hashtable_remove(&p_portals4_context->status_table, &p_status->peer.process_id);
}

static inline struct nm_portals4_bybuf_sbuf_s*nm_portals4_bybuf_sbuf_alloc(struct nm_portals4_bybuf_context_s*p_portals4_context)
{
  struct nm_portals4_bybuf_sbuf_lfstack_cell_s*p_cell = nm_portals4_bybuf_sbuf_lfstack_pop_ext(&p_portals4_context->sbuf_cache, !p_portals4_context->threaded);
  if(p_cell == NULL)
    {
      padico_memalign((void**)&p_cell, 16, sizeof(struct nm_portals4_bybuf_sbuf_lfstack_cell_s));
      p_cell->sbuf.p_status = NULL;
      p_cell->sbuf.length = NM_LEN_UNDEFINED;
      p_cell->sbuf.completed = 0;
      p_cell->sbuf.acked = 0;
      /* register MD for this block */
      const ptl_md_t md =
        {
          .start     = &p_cell->sbuf.buffer[0],
          .length    = NM_PORTALS4_BYBUF_SBUF_SIZE,
          .options   = PTL_MD_VOLATILE,
          .eq_handle = p_portals4_context->send_eq_handle,
          .ct_handle = PTL_CT_NONE
        };
      int rc = PtlMDBind(p_portals4_context->ni_handle, &md, &p_cell->sbuf.md_handle);
      nm_portals4_check_error(rc, "failed to bind memory descriptor");
    }
  return &p_cell->sbuf;
}

static inline void nm_portals4_bybuf_sbuf_free(struct nm_portals4_bybuf_context_s*p_portals4_context,
                                               struct nm_portals4_bybuf_sbuf_s*p_sbuf)
{
  struct nm_portals4_bybuf_sbuf_lfstack_cell_s*p_cell = nm_container_of(p_sbuf, struct nm_portals4_bybuf_sbuf_lfstack_cell_s, sbuf);
  p_cell->sbuf.p_status = NULL;
  p_cell->sbuf.length = NM_LEN_UNDEFINED;
  p_cell->sbuf.completed = 0;
  p_cell->sbuf.acked = 0;
  nm_portals4_bybuf_sbuf_lfstack_push_ext(&p_portals4_context->sbuf_cache, p_cell, !p_portals4_context->threaded);
}

static int nm_portals4_bybuf_send_buf_get(void*_status, void**p_buffer, nm_len_t*p_len)
{
  struct nm_portals4_bybuf_s*p_status = _status;
  struct nm_portals4_bybuf_sbuf_s*p_sbuf = nm_portals4_bybuf_sbuf_alloc(p_status->p_portals4_context);
  p_sbuf->p_status = p_status;
  assert(p_status->send.p_sbuf == NULL);
  p_status->send.p_sbuf = p_sbuf;
  *p_buffer = &p_sbuf->buffer[0];
  *p_len = NM_PORTALS4_BYBUF_SBUF_SIZE;
  return NM_ESUCCESS;
}

static inline void nm_portals4_bybuf_send(struct nm_portals4_bybuf_s*p_status,
                                          struct nm_portals4_bybuf_sbuf_s*p_sbuf)
{
  int rc = PtlPut(p_sbuf->md_handle,
                  0 /* local_offset */, p_sbuf->length,
                  PTL_ACK_REQ /* ack_req */,
                  p_status->peer.process_id,
                  p_status->peer.pt_index,
                  1 /* match_bits */,
                  0 /* remote_offset */,
                  p_sbuf /* user_ptr */,
                  0 /* hdr_data */);
  nm_portals4_check_error(rc, "failed to send data");
}

static int nm_portals4_bybuf_send_buf_post(void*_status, nm_len_t len)
{
  struct nm_portals4_bybuf_s*p_status = _status;
  p_status->send.p_sbuf->length = len;
  p_status->send.code = -NM_EAGAIN;
  nm_portals4_bybuf_send(p_status, p_status->send.p_sbuf);
  return NM_ESUCCESS;
}

static inline void nm_portals4_bybuf_send_poll_all(struct nm_portals4_bybuf_context_s*p_portals4_context)
{
  ptl_event_t event;
  int rc;
  do
    {
      rc = PtlEQGet(p_portals4_context->send_eq_handle, &event);
      if(rc == PTL_OK)
        {
          struct nm_portals4_bybuf_sbuf_s*p_sbuf = event.user_ptr;
          struct nm_portals4_bybuf_s*p_status = p_sbuf->p_status;
          assert(p_status != NULL);
          if(event.ni_fail_type == PTL_NI_OK)
            {
              if(event.type == PTL_EVENT_SEND)
                {
                  if(!p_sbuf->completed)
                    {
                      assert(p_status->send.p_sbuf == p_sbuf);
                      p_status->send.code = NM_ESUCCESS;
                      p_status->send.p_sbuf = NULL;
                      p_sbuf->completed = 1;
                    }
                  if(p_sbuf->acked)
                    {
                      NM_TRACEF("out of order event SEND & ACK\n");
                      nm_profile_inc(p_portals4_context->profiling.n_outoforder_ack);
                      nm_portals4_bybuf_sbuf_free(p_status->p_portals4_context, p_sbuf);
                    }
                }
              else if(event.type == PTL_EVENT_ACK)
                {
                  if(p_sbuf->completed)
                    {
                      nm_portals4_bybuf_sbuf_free(p_status->p_portals4_context, p_sbuf);
                    }
                  else
                    {
                      p_sbuf->acked = 1;
                    }
                }
            }
          else if(event.ni_fail_type == PTL_NI_PT_DISABLED)
            {
              /* flow control has disabled PT on remote side; try again */
              NM_WARN("send poll- NI failed with PTL_NI_PT_DISABLED; re-trying send.\n");
              nm_profile_inc(p_portals4_context->profiling.n_pt_disabled);
              puk_usleep(1000);
              nm_portals4_bybuf_send(p_status, p_sbuf);
            }
          else
            {
              NM_WARN("nm_portals4_bybuf_send_poll()- event NI fail type = %d (%s)\n",
                      event.ni_fail_type, nm_portals4_nifail_strerror(event.ni_fail_type));
            }
        }
      else if(rc == PTL_EQ_EMPTY)
        {
          /* do nothing */
        }
      else if(rc == PTL_EQ_DROPPED)
        {
          /* TODO- manage dropped events */
          NM_FATAL("detected dropped event\n");
        }
      else
        {
          nm_portals4_check_error(rc, "error while polling for events");
        }
    }
  while(rc != PTL_EQ_EMPTY);
}

static int nm_portals4_bybuf_send_poll(void*_status)
{
  struct nm_portals4_bybuf_s*p_status = _status;
  if(p_status->send.code == -NM_EAGAIN)
    nm_portals4_bybuf_send_poll_all(p_status->p_portals4_context);
  return p_status->send.code;
}

static void nm_portals4_bybuf_recv_reload(struct nm_portals4_bybuf_context_s*p_portals4_context)
{
  int i;
  for(i = 0; i < NM_PORTALS4_BYBUF_RBUF_NUM; i++)
    {
      if((!p_portals4_context->rbufs_posted[i]) && (p_portals4_context->rbufs_pending[i] == 0))
        {
          ptl_me_t me =
            {
              .start       = &p_portals4_context->rbufs[i * NM_PORTALS4_BYBUF_RBUF_SIZE],
              .length      = NM_PORTALS4_BYBUF_RBUF_SIZE,
              .uid         = PTL_UID_ANY,
              .match_id.phys.nid = PTL_NID_ANY,
              .match_id.phys.pid = PTL_PID_ANY,
              .match_bits  = 1,
              .ignore_bits = 0,
              .min_free    = NM_PORTALS4_BYBUF_SBUF_SIZE,
              .options     = PTL_ME_OP_PUT | PTL_ME_MANAGE_LOCAL | PTL_ME_MAY_ALIGN | PTL_ME_IS_ACCESSIBLE | PTL_ME_EVENT_LINK_DISABLE,
              .ct_handle   = PTL_CT_NONE
            };
          int rc = PtlMEAppend(p_portals4_context->ni_handle, p_portals4_context->local.pt_index,
                               &me, PTL_PRIORITY_LIST, (void*)((uintptr_t)i) /* user_ptr */, &p_portals4_context->me_handles[i]);
          nm_portals4_check_error(rc, "error while posting matching request");

          p_portals4_context->rbufs_posted[i] = 1;
        }
    }
}

static inline void nm_portals4_bybuf_recv_process_event(struct nm_portals4_bybuf_context_s*p_portals4_context,
                                                        const ptl_event_t*p_event)
{
  if(p_event->ni_fail_type == PTL_NI_PT_DISABLED)
    {
      NM_WARN("NI failed with PTL_NI_PT_DISABLED\n");
    }
  else if(p_event->ni_fail_type != PTL_NI_OK)
    {
      NM_WARN("recv poll- event NI fail type = %d (%s)\n",
              p_event->ni_fail_type, nm_portals4_nifail_strerror(p_event->ni_fail_type));
    }
  if(p_event->type == PTL_EVENT_PUT)
    {
      const uintptr_t index = (uintptr_t)p_event->user_ptr;
      const ptl_process_t source = p_event->initiator;
      const nm_len_t length = p_event->mlength;
      void*start = p_event->start;
      struct nm_portals4_bybuf_s*p_status = nm_portals4_status_hashtable_lookup(&p_portals4_context->status_table, &source);
      if(p_portals4_context->p_ready_status != NULL)
        {
          NM_FATAL("inconsistency detected in probe_any status.\n");
        }
      p_portals4_context->p_ready_status = p_status;
      p_portals4_context->ready_ptr = start;
      p_portals4_context->ready_len = length;
      p_status->recv.index = index;
      p_portals4_context->rbufs_pending[index]++;
    }
  else if(p_event->type == PTL_EVENT_AUTO_UNLINK)
    {
      const uintptr_t index = (uintptr_t)p_event->user_ptr;
      p_portals4_context->rbufs_posted[index] = 0;
      NM_TRACEF("event auto unlink index = %d.\n", (int)index);
    }
  else if(p_event->type == PTL_EVENT_PT_DISABLED)
    {
      NM_TRACEF("recv poll- got event PTL_EVENT_PT_DISABLED; re-enabling PT\n");
      PtlPTEnable(p_portals4_context->ni_handle, p_portals4_context->local.pt_index);
    }
}

static void nm_portals4_bybuf_recv_poll_all(struct nm_portals4_bybuf_context_s*p_portals4_context)
{
  ptl_event_t event;
  int rc = PtlEQGet(p_portals4_context->global_eq_handle, &event);
  if(rc == PTL_OK)
    {
      nm_portals4_bybuf_recv_process_event(p_portals4_context, &event);
    }
  else if(rc == PTL_EQ_EMPTY)
    {
      /* no event; do nothing */
    }
  else if(rc == PTL_EQ_DROPPED)
    {
      /* TODO- manage dropped events */
      NM_FATAL("detected dropped event\n");
    }
  else
    {
      nm_portals4_check_error(rc, "error while polling for events");
    }
}

static int nm_portals4_bybuf_recv_buf_poll(void*_status, void**p_buffer, nm_len_t*p_len)
{
  struct nm_portals4_bybuf_s*p_status = _status;
  struct nm_portals4_bybuf_context_s*p_portals4_context = p_status->p_portals4_context;
  if(p_portals4_context->p_ready_status == NULL)
    {
      nm_portals4_bybuf_recv_reload(p_portals4_context);
      nm_portals4_bybuf_recv_poll_all(p_portals4_context);
    }
  if(p_portals4_context->p_ready_status == p_status)
    {
      *p_buffer = p_portals4_context->ready_ptr;
      *p_len    = p_portals4_context->ready_len;
      p_portals4_context->p_ready_status = NULL;
      return NM_ESUCCESS;
    }
  return -NM_EAGAIN;
}

static int nm_portals4_bybuf_recv_buf_release(void*_status)
{
  struct nm_portals4_bybuf_s*p_status = _status;
  struct nm_portals4_bybuf_context_s*p_portals4_context = p_status->p_portals4_context;
  p_portals4_context->rbufs_pending[p_status->recv.index]--;
  return NM_ESUCCESS;
}

static int nm_portals4_bybuf_recv_probe_any(puk_context_t p_context, void**_status)
{
  struct nm_portals4_bybuf_context_s*p_portals4_context = puk_context_get_status(p_context);
  nm_portals4_bybuf_recv_reload(p_portals4_context);
  nm_portals4_bybuf_recv_poll_all(p_portals4_context);
  if(p_portals4_context->p_ready_status == NULL)
    {
      return -NM_EAGAIN;
    }
  else
    {
      struct nm_portals4_bybuf_s**pp_status = (struct nm_portals4_bybuf_s**)_status;
      *pp_status = p_portals4_context->p_ready_status;
      return NM_ESUCCESS;
    }
}

static int  nm_portals4_bybuf_recv_wait_any(puk_context_t p_context, void**_status)
{
  struct nm_portals4_bybuf_context_s*p_portals4_context = puk_context_get_status(p_context);
 retry:
  nm_portals4_bybuf_recv_reload(p_portals4_context);
  ptl_event_t event;
  int rc = PtlEQWait(p_portals4_context->global_eq_handle, &event);
  if(rc == PTL_OK)
    {
      nm_portals4_bybuf_recv_process_event(p_portals4_context, &event);
    }
  else if(rc == PTL_EQ_DROPPED)
    {
      /* TODO- manage dropped events */
      NM_FATAL("detected dropped event\n");
    }
  else
    {
      nm_portals4_check_error(rc, "error while polling for events");
    }
  if(p_portals4_context->p_ready_status == NULL)
    {
      goto retry;
      return -NM_EAGAIN;
    }
  else
    {
      struct nm_portals4_bybuf_s**pp_status = (struct nm_portals4_bybuf_s**)_status;
      *pp_status = p_portals4_context->p_ready_status;
      return NM_ESUCCESS;
    }
}


static int nm_portals4_bybuf_recv_cancel(void*_status)
{
  NM_WARN("portals4: cannot cancel single recv; only recv_cancel_any is available.\n");
  return -NM_ENOTIMPL;
}

static int  nm_portals4_bybuf_recv_cancel_any(puk_context_t p_context)
{
  struct nm_portals4_bybuf_context_s*p_portals4_context = puk_context_get_status(p_context);
  /* TODO- cancel PtlEQWait in progress, if any */
  int ok = 1;
  int i;
  for(i = 0; i < NM_PORTALS4_BYBUF_RBUF_NUM; i++)
    {
      if(p_portals4_context->rbufs_posted[i])
        {
          int rc = PtlMEUnlink(p_portals4_context->me_handles[i]);
          if(rc == PTL_IN_USE)
            {
              ok = 0;
            }
          else
            {
              p_portals4_context->rbufs_posted[i] = 0;
              nm_portals4_check_error(rc, "error while unlinking matching request");
            }
        }
    }
  if(ok)
    return NM_ESUCCESS;
  else
    return -NM_EINPROGRESS;
}

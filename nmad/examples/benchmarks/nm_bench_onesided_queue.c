/*
 * NewMadeleine
 * Copyright (C) 2019-2022 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include "nm_bench_generic.h"
#include <nm_onesided_interface.h>

static const nm_tag_t tag = 0x42;
static nm_onesided_t p_onesided = NULL;
static nm_onesided_queue_id_t queue = -1, rqueue = -1;

static void bench_onesided_queue_server(void*p_buf, nm_len_t len)
{
  char*p_ptr = NULL;
  while(p_ptr == NULL)
    {
      p_ptr = nm_onesided_queue_dequeue(p_onesided, queue);
    }
  memcpy(p_buf, p_ptr, len);
  padico_free(p_ptr);
  nm_onesided_request_t req;
  nm_onesided_queue_ienqueue(p_onesided, nm_bench_common.p_gate, rqueue, p_buf, len, &req);
  nm_onesided_req_wait(&req);
}

static void bench_onesided_queue_client(void*p_buf, nm_len_t len)
{
  nm_onesided_request_t req;
  nm_onesided_queue_ienqueue(p_onesided, nm_bench_common.p_gate, rqueue, p_buf, len, &req);
  nm_onesided_req_wait(&req);
  char*p_ptr = NULL;
  while(p_ptr == NULL)
    {
      p_ptr = nm_onesided_queue_dequeue(p_onesided, queue);
    }
  memcpy(p_buf, p_ptr, len);
  padico_free(p_ptr);
}

static void bench_onesided_queue_init(void*p_buf, nm_len_t len)
{
  if(p_onesided == NULL)
    {
      nm_onesided_init(nm_bench_common.p_session, &p_onesided);
      queue = nm_onesided_queue_create(p_onesided);
    }
  nm_sr_send(nm_bench_common.p_session, nm_bench_common.p_gate, tag, &queue, sizeof(queue));
  nm_sr_recv(nm_bench_common.p_session, nm_bench_common.p_gate, tag, &rqueue, sizeof(rqueue));
}

const struct nm_bench_s nm_bench =
  {
    .name   = "onesided interface - remote queues",
    .server = &bench_onesided_queue_server,
    .client = &bench_onesided_queue_client,
    .init   = &bench_onesided_queue_init
  };

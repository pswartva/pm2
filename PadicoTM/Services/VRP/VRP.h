/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Public API for the Variable Reliability Protocol
 * @ingroup VRP
 */

/** @example Tests/VRP/vrp-test.c */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <assert.h>
#include <arpa/inet.h>

/** @defgroup VRPAPI API: VRP
 * @ingroup VRP */

/** @addtogroup VRPAPI
 * @{ */

/** @name VRP buffers functions
 * @{ */

/** Buffer description for outgoing messages */
typedef struct vrp_buffer_s*vrp_buffer_t;


/** builds a buffer description
 * @param data data to transmit
 * @param size size of the data to transmit
 * @return a new buffer
 */
vrp_buffer_t
vrp_buffer_construct(char*data,
                     int size);

/** destroys a buffer description */
void
vrp_buffer_destroy(vrp_buffer_t buffer);

/** adds a critical region to a buffer
 * @param buffer an already constructed buffer description
 * @param data beginning of the critical region; should be inside the buffer!
 * @param size length of the critical region
 */
void
vrp_buffer_add_critical(vrp_buffer_t buffer,
                        char*data,
                        int size);

/** gives loss tolerance parameters for the buffer
 * @param buffer an already built buffer description
 * @param max maximum amount of data allowed to be lost
 *   (0=nothing lost, 100=100% may be lost)
 * @param cons maximum consecutive loss allowed (in bytes)
 */
void
vrp_buffer_set_loss_rate(vrp_buffer_t buffer,
                         int max,
                         int cons);

/** @} */

/** @name VRP outgoing functions
 * @{
 */

/** an outgoing descriptor (startpoint) */
typedef struct vrp_outgoing_s*vrp_outgoing_t;

/** constructs a VRP startpoint
 * @param hostname name of the remote host (on which
 *  vrp_incoming_construct() should have been called)
 * @param port port number on which the remote host is listenning
 * @param max_loss_rate default value for the maximum
 *  amount of data allowed to be lost
 *  (0=nothing lost, 100=100% may be lost)
 * @param max_cons_loss default value for maximum consecutive
 *  loss allowed (in bytes)
 * @note max_loss_rate and max_cons_loss are default values.
 *  They may be overriden on a frame per frame basis through
 *  vrp_buffer_set_loss_rate()
 */
vrp_outgoing_t
vrp_outgoing_construct(char*hostname, int port,
                       int max_loss_rate, int max_cons_loss);

void
vrp_outgoing_close(vrp_outgoing_t outgoing);

/** sends a frame over the network
 * @param outgoing an open outgoing descriptor
 * @param buffer a constructed VRP buffer descriptor
 */
int
vrp_outgoing_send_frame(vrp_outgoing_t outgoing,
                        vrp_buffer_t buffer);


/** @} */

/** @name VRP incoming functions
 * @{
 */

typedef struct vrp_incoming_s*vrp_incoming_t;

/** Buffer format for data received through VRP
 */
struct vrp_in_buffer_s
{
  /* data */
  size_t size;             /**< size of received data */
  void*  data;             /**< received data */
  /* loss description */
  void** lost;             /**< loss description array (size of array=loss_num) */
  int    loss_num;         /**< number of lost fragments */
  size_t loss_granularity; /**< granularity of loss, in bytes */
  size_t loss_residual;    /**< residual loss, in bytes */
};
typedef struct vrp_in_buffer_s*vrp_in_buffer_t;

/** handler function called uppon frame reception */
typedef void (*vrp_frame_handler_t)(vrp_in_buffer_t);

/** Constructs a VRP endpoint
 * @param port Port number on which VRP listens
 * @param handler Handler function called uppon frame receipt
 * @return a new vrp_incoming_t object
 */
vrp_incoming_t
vrp_incoming_construct(int*port, vrp_frame_handler_t handler);

void
vrp_incoming_close(vrp_incoming_t incoming);


/** @} */

enum vrp_error_e
{
  VRP_ERROR_OK                 = 0,
  VRP_ERROR_NOT_READY          = 1,
  VRP_ERROR_CONNECTION_REFUSED,
  VRP_ERROR_CONNECTION_TIMEOUT,
  VRP_ERROR_BAD_FRAME,
  VRP_ERROR_INTERNAL_ERROR
};

/** @} */ /* endgroup: VRPAPI */

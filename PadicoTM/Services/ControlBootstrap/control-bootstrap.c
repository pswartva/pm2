/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief ControlBootstrap Service: initialize a connection to a Padico node (the rendez vous node)
 * @ingroup ControlBootstrap
 */

/** @defgroup ControlBootstrap Service: initialize a connection to a Padico node (the rendez vous node)
 * @author
 */

/* ** rationale of ControlBootstrap configuration
 * ControlBootstrap connects an initial connection, and can perform
 * a barrier (init synchronization). Various schemes are available,
 * controlled through attributes:
 * -- PADICO_BOOTSTRAP_MODE={client|server|cluster|auto}
 *    . client: connect as a client to a rendez-vous node
 *    . server: accept connection as a rendez-vous node
 *    . cluster: node 0 is server, other nodes are client
 *    . auto: act as server, client, or both, depending whether
 *      attributes defining port number for client/server are defined.
 *    Default is: auto
 * -- PADICO_BOOTSTRAP_SYNC=<number>|full
 *    . full: performs a synchronization between all nodes of the session
 *    . <number>: wait the givne number of nodes (including self)
 *    Default is: full is mode is cluster, 1 else.
 * -- PADICO_BOOTSTRAP_HOST=<server>
 *    Name of the bootstrap server. Must be DNS-resolvable.
 * -- PADICO_BOOTSTRAP_PORT=<port>
 *    Port number of the bootstrap server for bootstrap clients.
 * -- PADICO_BOOTSTRAP_LISTEN=<port>
 *    Port number to listen to, for bootstrap server.
 */

#include <arpa/inet.h>

#include <Padico/Puk.h>
#include <Padico/PadicoTM.h>
#include <Padico/Module.h>

#include <Padico/NetSelector.h>
#include <Padico/Topology.h>
#include <Padico/PadicoControl.h>

static int control_bootstrap_init(void);
static void control_bootstrap_finalize(void);

PADICO_MODULE_DECLARE(ControlBootstrap, control_bootstrap_init, NULL, control_bootstrap_finalize);


/* ********************************************************* */

PUK_VECT_TYPE(control_bootstrap_conn, puk_instance_t);

static struct
{
  puk_component_t component;           /**< component used for listening */
  puk_instance_t instance;             /**< the listening bootstrap instance */
  control_bootstrap_conn_vect_t conns; /**< list of connections established by ControlBootstrap */
  struct
  {
    marcel_mutex_t lock;
    marcel_cond_t ready;
    enum { BOOTSTRAP_MODE_SESSION, BOOTSTRAP_MODE_TOTAL } mode;
    padico_topo_node_vect_t nodes;
    int nodes_ready;
  } sync;
} control_bootstrap =
  { .component = NULL, .instance = NULL };

/* ********************************************************* */

static void padico_bootstrap_indirect(padico_topo_host_t rdv_host,
                                      struct puk_receptacle_PadicoBootstrap_s*r)
{
  control_bootstrap.component =
    padico_ns_host_selector(rdv_host, "inet", puk_iface_PadicoBootstrap(), 0 /* port */, IPPROTO_TCP);
  if(control_bootstrap.component == NULL)
    {
      padico_fatal("no PadicoBootstrap assembly found.\n");
    }
  if(puk_component_get_driver_PadicoBootstrap(control_bootstrap.component, NULL) == NULL &&
     puk_component_get_driver_PadicoControl(control_bootstrap.component, NULL) == NULL)
    {
      padico_fatal("bootstrap component do not provide required interface (PadicoBootstrap, PadicoControl).\n");
    }
  control_bootstrap.instance = puk_component_instantiate(control_bootstrap.component);
  puk_instance_indirect_PadicoBootstrap(control_bootstrap.instance, NULL, r);
  padico_out(50, "init bootstrap served by assembly %s\n", control_bootstrap.instance->component->name);
}

static void control_bootstrap_ready_handler(puk_parse_entity_t e)
{
  marcel_mutex_lock(&control_bootstrap.sync.lock);
  control_bootstrap.sync.nodes_ready++;
  marcel_cond_signal(&control_bootstrap.sync.ready);
  marcel_mutex_unlock(&control_bootstrap.sync.lock);
}

static void control_bootstrap_event_listener(void*_arg)
{
  padico_control_event_t event = (struct padico_control_event_s*)_arg;
  switch(event->kind)
    {
    case PADICO_CONTROL_EVENT_NEW_NODE:
      {
        const padico_topo_node_t node = event->NODE.node;
        marcel_mutex_lock(&control_bootstrap.sync.lock);
        if(padico_topo_node_vect_find(control_bootstrap.sync.nodes, node) == NULL)
          {
            if((control_bootstrap.sync.mode == BOOTSTRAP_MODE_TOTAL) ||
               ((control_bootstrap.sync.mode ==  BOOTSTRAP_MODE_SESSION) &&
                (strcmp(padico_topo_node_getsession(event->NODE.node),
                        padico_topo_node_getsession(padico_topo_getlocalnode())) == 0)))
                  {
                    padico_topo_node_vect_push_back(control_bootstrap.sync.nodes, node);
                    marcel_cond_signal(&control_bootstrap.sync.ready);
                  }
          }
        marcel_mutex_unlock(&control_bootstrap.sync.lock);
      }
      break;

    case PADICO_CONTROL_EVENT_PLUG_CONTROLER:
      {
        const puk_instance_t controler = event->PLUG_CONTROLER.controler;
        const puk_instance_t server = event->PLUG_CONTROLER.server;
        const puk_instance_t server_root = server ? puk_instance_get_root_container(server) : NULL;
        const puk_instance_t controler_root =  puk_instance_get_root_container(controler);
        if(server && (server_root == control_bootstrap.instance))
          {
            control_bootstrap_conn_vect_push_back(control_bootstrap.conns, controler_root);
          }
      }
      break;

    default:
      break;
    }
}


static int control_bootstrap_init(void)
{
  puk_iface_register("PadicoBootstrap");
  int is_rdv_client = 0, is_rdv_server = 0;
  const char*c_rdv_host = padico_getattr("PADICO_BOOTSTRAP_HOST");
  const char*mode = padico_getattr("PADICO_BOOTSTRAP_MODE") ? : "auto";
  const char*sync = padico_getattr("PADICO_BOOTSTRAP_SYNC") ? : "full";
  const char*s_bootstrap_uuid = padico_getattr("PADICO_BOOTSTRAP_UUID");
  assert(s_bootstrap_uuid != NULL);
  padico_topo_uuid_t bootstrap_uuid = (padico_topo_uuid_t)s_bootstrap_uuid;
  const int size = atoi(padico_getattr("PADICO_BOOT_SIZE")?:"1");
  const int rank = atoi(padico_getattr("PADICO_BOOT_RANK")?:"0");
  struct puk_receptacle_PadicoBootstrap_s r;
  if(padico_topo_getnodebyuuid(bootstrap_uuid) == padico_topo_getlocalnode())
    {
      /* bootstrap master */
      is_rdv_server = 1;
    }
  else
    {
      is_rdv_client = 1;
    }

  control_bootstrap.conns = control_bootstrap_conn_vect_new();
  marcel_mutex_init(&control_bootstrap.sync.lock, NULL);
  marcel_cond_init(&control_bootstrap.sync.ready, NULL);
  control_bootstrap.sync.nodes = padico_topo_node_vect_new();
  control_bootstrap.sync.nodes_ready = 0;
  padico_control_event_subscribe(&control_bootstrap_event_listener);
  puk_xml_add_action((struct puk_tag_action_s){
                     .xml_tag        = "ControlBootstrap:ready",
                     .start_handler  = NULL,
                     .end_handler    = &control_bootstrap_ready_handler,
                     .required_level = PUK_TRUST_CONTROL
                     });

  if(strcmp(mode, "client") == 0)
    {
      is_rdv_client = 1;
    }
  else if(strcmp(mode, "server") == 0)
    {
      is_rdv_server = 1;
    }

  if(is_rdv_server)
    {
      /* rendez-vous server */
      padico_print("I am the rendez-vous node; listening...\n");
      padico_bootstrap_indirect(NULL, &r);
      (*r.driver->bootstrap_new)(r._status);
    }

  if(is_rdv_client)
    {
      /* rendez-vous client */
      if(!c_rdv_host)
        padico_fatal("configured as client. Cannot connect to the rendez-vous server because host is not configured. Please give a bootstrap server host with the PADICO_BOOTSTRAP_HOST attribute.\n");
      padico_print("connecting to rendez-vous host=%s; uuid =%s\n", c_rdv_host, s_bootstrap_uuid);
      const padico_topo_host_t host = padico_topo_host_resolvebyname(c_rdv_host);
      padico_bootstrap_indirect(host, &r);
      int sleep_msec = 10;
      const int sleep_msec_warning = 1000;
      const int sleep_msec_timeout = 20 * 1000; /* 20 seconds */
      int rc = 0;
      do
        {
          rc = (*r.driver->bootstrap_connect)(r._status, c_rdv_host, bootstrap_uuid);
          if(rc)
            {
              if(sleep_msec >= sleep_msec_warning)
                padico_warning("connection failed rc = %d (%s) with component %s- waiting %dmsec.\n",
                               rc, strerror(-rc), control_bootstrap.component->name, sleep_msec);
              padico_tm_msleep(sleep_msec);
              sleep_msec *= 2;
            }
        }
      while(rc && (sleep_msec < sleep_msec_timeout));
      if(rc)
        {
          padico_fatal("connection failed- cannot connect to session leader node.\n");
        }
      padico_print("connected.\n");
    }
  int wait_nodes = -1;
  if(sync && strcmp(sync, "full") == 0)
    {
      /* full session synchro: all nodes wait for all nodes */
      control_bootstrap.sync.mode = BOOTSTRAP_MODE_SESSION;
      wait_nodes = size - 1;
      padico_print("synchronizing session- waiting for %d nodes in session...\n", wait_nodes + 1);

    }
  else
    {
      control_bootstrap.sync.mode = BOOTSTRAP_MODE_TOTAL;
      if(sync && (atoi(sync) > 0))
        {
          wait_nodes = atoi(sync) - 1;
          padico_print("synchronizing session- waiting for %d nodes to join...\n", wait_nodes + 1);
        }
      else
        {
          wait_nodes = 0;
        }
    }

  marcel_mutex_lock(&control_bootstrap.sync.lock);
  while(padico_topo_node_vect_size(control_bootstrap.sync.nodes) < wait_nodes)
    {
      /* Note: this cond_wait purposely *blocks* the taskengine tasklet.
       * It prevents following tasks to be processed, but (hopefully)
       * allows recursive tasks to be processed so as to satisfy dependencies.
       */
      marcel_cond_wait(&control_bootstrap.sync.ready, &control_bootstrap.sync.lock);
    }
  if(control_bootstrap.sync.mode == BOOTSTRAP_MODE_SESSION)
    {
      padico_topo_network_t session = padico_topo_session_getnetwork(padico_topo_node_getsession(padico_topo_getlocalnode()));
      while(padico_topo_network_size(session) < size)
        {
          marcel_cond_wait(&control_bootstrap.sync.ready, &control_bootstrap.sync.lock);
        }
      if(rank == 0)
        {
          /* wait all nodes to acknowledge they are ready */
          padico_topo_network_t session = padico_topo_session_getnetwork(padico_topo_node_getsession(padico_topo_getlocalnode()));
          while((control_bootstrap.sync.nodes_ready < wait_nodes) ||
                padico_topo_network_size(session) < size)
            {
              marcel_cond_wait(&control_bootstrap.sync.ready, &control_bootstrap.sync.lock);
            }
        }
      else
        {
          padico_topo_node_t session_0 =
            padico_topo_session_getnodebyrank(padico_topo_node_getsession(padico_topo_getlocalnode()), 0);
          padico_string_t msg = padico_string_new();
          padico_string_t self_node = padico_topo_node_serialize(padico_topo_getlocalnode());
          padico_string_printf(msg, "<ControlBootstrap:ready>%s</ControlBootstrap:ready>\n", padico_string_get(self_node));
          padico_string_delete(self_node);
          padico_control_send_oneway(session_0, padico_string_get(msg));
          padico_string_delete(msg);
        }
    }
  marcel_mutex_unlock(&control_bootstrap.sync.lock);
  padico_control_event_sync();
  padico_print("session is ready.\n");

  return 0;
}

static void control_bootstrap_finalize(void)
{
  padico_control_event_disable();
  padico_control_event_unsubscribe(&control_bootstrap_event_listener);
  if(control_bootstrap.instance)
    {
      puk_instance_destroy(control_bootstrap.instance);
      control_bootstrap.instance = NULL;
      control_bootstrap.component = NULL;
    }
  while(!control_bootstrap_conn_vect_empty(control_bootstrap.conns))
    {
      puk_instance_t controler = control_bootstrap_conn_vect_pop_back(control_bootstrap.conns);
      puk_instance_destroy(controler);
    }
  control_bootstrap_conn_vect_delete(control_bootstrap.conns);
  padico_control_event_enable();
  padico_topo_node_vect_delete(control_bootstrap.sync.nodes);
  puk_xml_delete_action("ControlBootstrap:ready");
}

/* bench-Unix.c
 * benchmark for Unix sockets (from OS, not VIO)
 */

#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/un.h>

#include "bench-common.h"
#include <Padico/Module.h>
#ifdef PUKABI
#include <Padico/Puk-ABI.h>
#endif

int bench_unix_init(void);
int bench_unix_run(int argc, char**argv);
void bench_unix_finalize(void);

PADICO_MODULE_DECLARE(bench_unix, bench_unix_init, bench_unix_run, bench_unix_finalize);

static int bench_fd = -1;

static char socket_path[256];

int bench_unix_init(void)
{
  snprintf(socket_path, 256, "/tmp/bench-unix-%s", padico_getenv("USER"));
  int server_fd = PUK_ABI_FSYS_WRAP(socket)(PF_UNIX, SOCK_STREAM, 0);
  struct sockaddr_un sun;
  sun.sun_family = AF_UNIX;
  strcpy(sun.sun_path, socket_path);

  if(bench_common_param.is_server)
    {
      unlink(sun.sun_path);
      int rc = PUK_ABI_FSYS_WRAP(bind)(server_fd, (struct sockaddr*)&sun, sizeof(sun));
      if(rc != 0)
        {
          padico_fatal("BENCH: bind error- %s\n", strerror(errno));
        }
      rc = PUK_ABI_FSYS_WRAP(listen)(server_fd, 1);
      if(rc != 0)
        {
          padico_fatal("BENCH: listen error- %s\n", strerror(errno));
        }
      struct sockaddr_un sun2;
      socklen_t s2 = sizeof(sun2);
      sun2.sun_family = AF_UNIX;
      sun2.sun_path[0] = 0;
      bench_fd = PUK_ABI_FSYS_WRAP(accept)(server_fd, (struct sockaddr*)&sun2, &s2);
      padico_print("BENCH: client connected- fd = %d\n", bench_fd);
      PUK_ABI_FSYS_WRAP(close)(server_fd);
    }
  else
    {
      int rc = PUK_ABI_FSYS_WRAP(connect)(server_fd, (struct sockaddr*)&sun, sizeof(sun));
      while(rc < 0)
        {
          padico_print("BENCH: not connected\n");
          sleep(1);
          rc = PUK_ABI_FSYS_WRAP(connect)(server_fd, (struct sockaddr*)&sun, sizeof(sun));
        }
      bench_fd = server_fd;
      padico_print("BENCH: connected- fd = %d\n", bench_fd);
      unlink(socket_path);
    }
  return 0;
}

void bench_unix_finalize(void)
{
  PUK_ABI_FSYS_WRAP(close)(bench_fd);
}


static ssize_t socket_in(char *buf, size_t nbyte)
{
  size_t done = 0;
  while(done < nbyte)
    {
      int rc = PUK_ABI_FSYS_WRAP(read)(bench_fd, buf + done, nbyte - done);
      if(rc > 0)
        done += rc;
      else
        {
          padico_warning("BENCH: read()- nbyte=%d; rc=%d\n", (int)nbyte, (int)done);
          break;
        }
    }
  return done;
}

static ssize_t socket_out(char *buf, size_t nbyte)
{
  size_t done = 0;
  while(done < nbyte)
    {
      int rc = PUK_ABI_FSYS_WRAP(write)(bench_fd, buf + done, nbyte - done);
      if(rc > 0)
        done += rc;
      else
        {
          padico_warning("BENCH: write()- nbyte=%d; rc=%d\n", (int)nbyte, (int)done);
          break;
        }
    }
  return done;
}

static void unix_server(char*buffer, size_t lenbuf)
{
  socket_out(buffer, lenbuf);
  socket_in(buffer, lenbuf);
}

static void unix_client(char*buffer, size_t lenbuf)
{
  socket_in(buffer, lenbuf);
  socket_out(buffer, lenbuf);
}

static struct bench_desc_s unix_bench =
  {
    "Unix",
    &unix_server,
    &unix_client
  };

int bench_unix_run(int argc, char**argv)
{
  the_bench(&unix_bench);
  return 0;
}

/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Hooks VIO functions into Puk ABI
 */

#include <Padico/PadicoTM.h>
#include <Padico/Module.h>
#include "VIO-internals.h"

PADICO_MODULE_HOOK(VIO);


void padico_vio_abi_init(void)
{
#ifdef VIO_PUK_ABI
  padico_out(6, "registering VIO functions into Puk ABI.\n");
  puk_spinlock_acquire();
  puk_abi_set_virtual(read,        &padico_vio_read);
  puk_abi_set_virtual(write,       &padico_vio_write);
  puk_abi_set_virtual(readv,       &padico_vio_readv);
  puk_abi_set_virtual(writev,      &padico_vio_writev);
  puk_abi_set_virtual(close,       &padico_vio_close);
  puk_abi_set_virtual(select,      &padico_vio_select);
  puk_abi_set_virtual(fcntl,       &padico_vio_fcntl);
  puk_abi_set_virtual(poll,        &padico_vio_poll);
  puk_abi_set_virtual(ioctl,       &padico_vio_ioctl);
  puk_abi_set_virtual(pipe,        &padico_vio_pipe);
  puk_abi_set_virtual(open,        &padico_vio_open);
  puk_abi_set_virtual(creat,       &padico_vio_creat);
  puk_abi_set_virtual(lseek,       &padico_vio_lseek);
  puk_abi_set_virtual(ftruncate,   &padico_vio_ftruncate);
#ifdef HAVE___FXSTAT
  puk_abi_set_virtual(__fxstat,    &padico_vio___fxstat);
#else
  puk_abi_set_virtual(fstat,       &padico_vio_fstat);
#endif
  puk_abi_set_virtual(mmap,        &padico_vio_mmap);
  puk_abi_set_virtual(fsync,       &padico_vio_fsync);
  puk_abi_set_virtual(send,        &padico_vio_send);
  puk_abi_set_virtual(recv,        &padico_vio_recv);
  puk_abi_set_virtual(socket,      &padico_vio_socket);
  puk_abi_set_virtual(bind,        &padico_vio_bind);
  puk_abi_set_virtual(listen,      &padico_vio_listen);
  puk_abi_set_virtual(accept,      &padico_vio_accept);
  puk_abi_set_virtual(connect,     &padico_vio_connect);
  puk_abi_set_virtual(shutdown,    &padico_vio_shutdown);
  puk_abi_set_virtual(setsockopt,  &padico_vio_setsockopt);
  puk_abi_set_virtual(getsockopt,  &padico_vio_getsockopt);
  puk_abi_set_virtual(getsockname, &padico_vio_getsockname);
  puk_abi_set_virtual(getpeername, &padico_vio_getpeername);
  puk_abi_set_virtual(sendto,      &padico_vio_sendto);
  puk_abi_set_virtual(recvfrom,    &padico_vio_recvfrom);
  puk_abi_set_virtual(sendmsg,     &padico_vio_sendmsg);
  puk_abi_set_virtual(recvmsg,     &padico_vio_recvmsg);

#ifdef _LARGEFILE64_SOURCE
  /* large file extension (LFS) */
  if(sizeof(off_t) != sizeof(off64_t))
    {
      puk_abi_set_virtual(open64,      &padico_vio_open64);
      puk_abi_set_virtual(lseek64,     &padico_vio_lseek64);
      puk_abi_set_virtual(ftruncate64, &padico_vio_ftruncate64);
#ifdef HAVE___FXSTAT
      puk_abi_set_virtual(__fxstat64,  &padico_vio___fxstat64);
#endif
    }
  else
    {
      puk_abi_set_virtual(open64,      &padico_vio_open);
      puk_abi_set_virtual(lseek64,     &padico_vio_lseek);
      puk_abi_set_virtual(ftruncate64, &padico_vio_ftruncate);
#ifdef HAVE___FXSTAT
      puk_abi_set_virtual(__fxstat64,  &padico_vio___fxstat);
#endif
    }
#endif /* _LARGEFILE64_SOURCE */

  //   puk_abi_set_virtual(ulimit,      &padico_vio_ulimit); /** @todo */
  puk_spinlock_release();
#endif /* VIO_PUK_ABI */
}


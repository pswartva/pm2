#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <strings.h>
#include <string.h>
#include <netinet/in.h>
#include <netdb.h>
#include <time.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <poll.h>
#include <pthread.h>
#include <time.h>


void print_error(char *func)
{
  perror(func);
  exit(1);
}

int main(int argc, char *argv[])
{
  if(argc!=4)
    {
      printf("Usage: %s <client/server> <host> <port>\n", argv[0]);
      return 0;
    }
  int port = atoi(argv[3]);
  int sock_fd = socket(AF_INET, SOCK_STREAM, 0);
  if (sock_fd < 0)
    print_error("socket()");

  if(strncmp("server",argv[1],6)==0)
    {
      struct sockaddr_in serv_addr;
      bzero(&serv_addr,sizeof(serv_addr));
      serv_addr.sin_family = AF_INET;
      serv_addr.sin_port = htons(port);
      serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
      if (bind(sock_fd,(struct sockaddr *)&serv_addr,sizeof(serv_addr)) < 0)
        print_error("bind()");
      struct sockaddr_in cli_addr;
      if(listen(sock_fd, 5) < 0)
        print_error("accept()");
      int clilen = sizeof(cli_addr);
      int clifd = accept(sock_fd,  (struct sockaddr *) &cli_addr,&clilen);
      if (clifd < 0)
        print_error("accept()");

   }
  else if(strncmp("client",argv[1],6)==0)
    {

      struct hostent *info_serv;
      info_serv=gethostbyname(argv[2]);
      struct sockaddr_in serv_addr;
      bzero(&serv_addr,sizeof(serv_addr));
      serv_addr.sin_family = AF_INET;
      serv_addr.sin_port = htons(port);
      memcpy(&serv_addr.sin_addr, info_serv->h_addr_list[0], info_serv->h_length);

      if (connect(sock_fd, (struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0)
          print_error("connect()");
      printf("Client: I send \"client\"\n");

    }

  if ( close(sock_fd) < 0 )
      print_error("close()");
  return 0;
}

/*
 * NewMadeleine
 * Copyright (C) 2019-2021 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


#include <nm_mcast_interface.h>
#include "nm_bench_coll_generic.h"

static const int root = 0;
static const nm_tag_t tag = 0x42;
static nm_mcast_service_t mcast_service;

static void nm_bench_coll_mcast_init(struct nm_bench_coll_common_s*p_common, void*buf, nm_len_t len)
{
  mcast_service = nm_mcast_init(p_common->p_comm);
}

static void nm_bench_coll_mcast_run(struct nm_bench_coll_common_s*p_common, void*buf, nm_len_t len)
{
  struct nm_data_s data;
  nm_data_contiguous_build(&data, (void*)buf, len);
  if(nm_comm_rank(p_common->p_comm) == root)
    {
      const int worldsize = nm_comm_size(p_common->p_comm);
      int*dests = malloc(sizeof(int) * (worldsize - 1));
      int i;
      for (i = 1; i < worldsize; i++)
        {
          dests[i - 1] = i;
        }
      nm_mcast_send(mcast_service, p_common->p_comm, dests, NULL, worldsize - 1, tag, &data, 0 /* hlen */, NM_COLL_TREE_DEFAULT);
      free(dests);
    }
  else
    {
      nm_sr_recv(p_common->p_comm->p_session, NM_ANY_GATE, tag, buf, len);
    }
  /*  nm_coll_data_bcast(p_common->p_comm, root, &data, tag); */
}

static void nm_bench_coll_mcast_finalize(struct nm_bench_coll_common_s*p_common)
{
  nm_mcast_finalize(mcast_service);
}

struct nm_bench_coll_s nm_bench_coll =
  {
    .label    = "nm_bench_coll_mcast",
    .name     = "multicast",
    .init     = &nm_bench_coll_mcast_init,
    .run      = &nm_bench_coll_mcast_run,
    .finalize = &nm_bench_coll_mcast_finalize
  };

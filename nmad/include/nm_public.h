/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#ifndef NM_PUBLIC_H
#define NM_PUBLIC_H

/** @defgroup nm_public Public definitions.
 * Public definitions from nmad core and common to all nmad interfaces
 * It contains mainly:
 *   - definition of **gates**, the type to designate a peer: ::nm_gate_t. User is
 *     expected to get gates using @ref launcher_interface.
 *   - definition of **tags**: ::nm_tag_t. Tag size is usually at least 64 bits wide.
 *     Maximum available tag is #NM_TAG_MAX.
 *   - definition of **data descriptors**, the internal data representation in nmad:
 *     @ref nm_data
 *
 * **Sessions** are the basic multiplexing facility in NewMadeleine. Messages and tags
 * from different sessions are isolated from each other, allonwing multiple
 * libraries to use NewMadeleine at the same time, or a given code to use multiple
 * nmad interfaces at the same time. While they are used throughout all nmad
 * interfaces, sessions (::nm_session_t) are not defined in core but in
 * the internal @ref session_interface. User is expected to open and close sessions using
 * the @ref launcher_interface.
 */

/** @ingroup nm_public
 * @{
 * @file
 * This is the common public header for NewMad. It includes publics headers
 * wich define common types and constants, but no API by itself.
 *
 * The API for end-users are defined in the 'interfaces/' directory.
 * The low-level 'core' API is defined in nm_core_interface.h.
 * @}
 */

/** @ingroup core_interface
 * @{
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <nm_config.h>
#include <nm_errno.h>
#include <nm_types.h>
#include <nm_data.h>
#include <Padico/Puk.h>
#if defined(NMAD_PIOMAN)
#  include <pioman.h>
#endif

/* ** Config sanity checks ********************************* */

#if !defined(NMAD)
#  error "nmad CFLAGS not defined; please compile with CFLAGS returned by 'pkg-config --cflags nmad'"
#endif /* NMAD */

#if defined(NMAD_PIOMAN) && !defined(PIOMAN)
#  error "nmad was configured with pioman support; cannot build without pioman flags."
#endif

#if defined(PIOMAN) && !defined(NMAD_PIOMAN)
#  error "nmad was configured without pioman support; cannot build with pioman flags."
#endif

#if defined(NMAD_PUKABI) && !defined(PUKABI)
#  error "nmad was configured with PukABI support; cannot build without PukABI flags."
#endif

#if defined(PUKABI) && !defined(NMAD_PUKABI)
#  error "nmad was configured without PukABI support; cannot build with PukABI flags."
#endif

#if defined(PIOMAN_TOPOLOGY_HWLOC) && !defined(NMAD_HWLOC)
#  error "pioman was configured with hwloc, but nmad was configured without hwloc."
#endif

#if defined(NMAD_SIMGRID) && !defined(PUK_SIMGRID)
#  error "nmad was configured with simgrid, but Puk was configured without simgrid."
#endif

#if defined(PUK_SIMGRID) && !defined(NMAD_SIMGRID)
#  error "Puk was configured with simgrid, but nmad was configured without simgrid."
#endif

#if defined(NMAD_SIMGRID) && (defined(PIOMAN) && !defined(PIOMAN_SIMGRID))
#  error "nmad was configured with simgrid, but pioman was configured without simgrid."
#endif

/** config options that impact nmad ABI
 * (content of structures exposed in API or used in inline code)
 */
struct nm_abi_config_s
{
  int enable_debug;
  int enable_pioman;
  int enable_pthread;
  int enable_marcel;
  int enable_pukabi;
  int enable_simgrid;
};

/** Capture ABI config parameters in current context.
 */
static inline void nm_abi_config_capture(struct nm_abi_config_s*p_nm_abi_config)
{
#if defined(NMAD_DEBUG)
  p_nm_abi_config->enable_debug = 1;
#else
  p_nm_abi_config->enable_debug = 0;
#endif /* NMAD_DEBUG */
#if defined(PIOMAN)
  p_nm_abi_config->enable_pioman = 1;
#else
  p_nm_abi_config->enable_pioman = 0;
#endif /* PIOMAN */
#if defined(PIOMAN_PTHREAD)
  p_nm_abi_config->enable_pthread = 1;
#else
  p_nm_abi_config->enable_pthread = 0;
#endif /* PTHREAD */
#if defined(MARCEL)
  p_nm_abi_config->enable_marcel = 1;
#else
  p_nm_abi_config->enable_marcel = 0;
#endif /* MARCEL */
#if defined(PUKABI)
  p_nm_abi_config->enable_pukabi = 1;
#else
  p_nm_abi_config->enable_pukabi = 0;
#endif /* PUKABI */
#if defined(NMAD_SIMGRID)
  p_nm_abi_config->enable_simgrid = 1;
#else
  p_nm_abi_config->enable_simgrid = 0;
#endif /* PUKABI */
}


/** @} */


#endif /* NM_PUBLIC_H */

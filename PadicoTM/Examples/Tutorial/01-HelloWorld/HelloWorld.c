/* Padico tutorial 1: "Hello world"
 * available in: PadicoTM/Examples/Tutorial/01-HelloWorld/
 * author: Alexandre Denis
 */

#include <Padico/Puk.h>
#include <Padico/Module.h>

int hello_main(int argc, char**argv);

PADICO_MODULE_DECLARE(HelloWorld, NULL, hello_main, NULL);


static int hello_world_count = 0;

int hello_main(int argc, char**argv)
{
  printf("Hello world! [%d]\n", hello_world_count);
  hello_world_count++;
  return 0;
}

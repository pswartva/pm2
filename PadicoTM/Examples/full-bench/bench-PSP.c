/* bench-PSP.c
 */

#include "bench-common.h"
#include <Padico/PSP.h>
#include <Padico/NetSelector.h>
#include <Padico/PadicoTM.h>
#include <Padico/Module.h>

int bench_psp_init(void);
int bench_psp_run(int argc, char**argv);

PADICO_MODULE_DECLARE(bench_PSP, bench_psp_init, bench_psp_run, NULL);

static char*recv_buffer = NULL; /**< buffer for receipt */
static size_t  recv_lenbuf = 0;    /**< len of above buffer */
static size_t  len_offset  = 0;
static marcel_t recv_tid = NULL;
static int  num_trips   = 0;    /**< number of messages received */

static marcel_mutex_t mutex;
static marcel_cond_t cond;

static padico_topo_node_t peer;
static struct puk_receptacle_PadicoSimplePackets_s psp;

static inline void my_memcpy(void*_dest, const void*_source, size_t len)
{
  const int*source = _source;
  int*dest   = _dest;
  int chunks = len / sizeof(int);
  register int i;
  for(i = 0; i < chunks; i++)
    {
      dest[i] = source[i];
    }
}

/* init a new benchmark */
static void bench_psp_post_recv(char*buffer, size_t lenbuf)
{
  padico_out(40, "post recv- len=%d\n", lenbuf);
  marcel_mutex_lock(&mutex);
  recv_buffer = buffer;
  recv_lenbuf = lenbuf;
  recv_tid    = marcel_self();
  marcel_cond_broadcast(&cond);
  marcel_mutex_unlock(&mutex);
}

static void bench_psp_wait(void)
{
  padico_out(50, "entering trips=%d.\n", num_trips);
  marcel_mutex_lock(&mutex);
  while(recv_buffer)
    {
      padico_out(20, "waiting synchro trips=%d...\n", num_trips);
      marcel_cond_wait(&cond, &mutex);
      padico_out(20, "unlocked synchro trips=%d...\n", num_trips);
    }
  padico_out(40, "stop\n");
  recv_lenbuf = 0;
  recv_buffer = NULL;
  marcel_mutex_unlock(&mutex);
  padico_out(60, "done..\n");
}

static void bench_psp_send(void*buffer, size_t lenbuf)
{
  void*_header;
  padico_psp_connection_t conn = (*psp.driver->new_message)(psp._status, peer, &_header);
  uint32_t*header = (uint32_t*)(_header);
  header[0] = htonl(lenbuf);
  header[1] = htonl(num_trips);
  size_t blen = 0, hlen = 0;
  if(lenbuf > len_offset)
    {
      hlen = len_offset;
      blen = lenbuf - len_offset;
    }
  else
    {
      hlen = lenbuf;
      blen = 0;
    }
  memcpy(header + 2, buffer, hlen);
  if(blen > 0)
    {
      (*psp.driver->pack)(psp._status, conn, buffer + len_offset, blen);
    }
  (*psp.driver->end_message)(psp._status, conn);
}

static void bench_psp_handler(const void*_header,
                              padico_topo_node_t from,
                              void*key,
                              padico_psp_pump_t pump,
                              void*token)
{
  const uint32_t*header = _header;
  const uint32_t len   = ntohl(header[0]);
  const uint32_t trips = ntohl(header[1]);
  marcel_mutex_lock(&mutex);
  while((!recv_buffer) || (len != recv_lenbuf))
    {
      padico_out(10, "Ooops! Waiting in handler- recv_lenbuf=%d; len=%d;\n",
                 recv_lenbuf, len);
      marcel_cond_wait(&cond, &mutex);
    }
  size_t blen = 0, hlen = 0;
  if(len > len_offset)
    {
      hlen = len_offset;
      blen = len - len_offset;
    }
  else
    {
      hlen = len;
      blen = 0;
    }
  memcpy(recv_buffer, header + 2, hlen);
  if(blen > 0)
    {
      (*pump)(token, recv_buffer + len_offset, blen);
    }
  num_trips++;
  const marcel_t tid = recv_tid;
  recv_tid = NULL;
  recv_buffer = NULL;
  marcel_cond_broadcast(&cond);
  marcel_mutex_unlock(&mutex);
  marcel_yield_to(tid);
}

int bench_psp_init(void)
{
  marcel_mutex_init(&mutex, NULL);
  marcel_cond_init(&cond, NULL);

  peer = bench_common_param.is_server?
    bench_common_param.client_node:
    bench_common_param.server_node;
  const char*component_s = padico_getenv("BENCH_PSP_ADAPTER");
  puk_component_t component = component_s?puk_component_resolve(component_s) : padico_ns_serial_selector(peer, "default", puk_iface_PadicoSimplePackets());
  puk_instance_t instance = puk_component_instantiate(component);
  puk_instance_indirect_PadicoSimplePackets(instance, NULL, &psp);

  padico_string_t c = puk_component_serialize(component);
  padico_print("bench-PSP: using component %s\n", padico_string_get(c));
  padico_string_delete(c);

  size_t h_len = 2*sizeof(uint32_t);
  (*psp.driver->init)(psp._status, 2, "bench-PSP", &h_len, &bench_psp_handler, NULL);
  len_offset = h_len - 8;
  assert(len_offset < h_len);
  padico_print("bench-PSP: header = %d; offset = %d\n", h_len, len_offset);
  if(psp.driver->listen)
    (*psp.driver->listen)(psp._status, peer);
  if(psp.driver->connect)
    (*psp.driver->connect)(psp._status, peer);

  padico_tm_msleep(500);
  return 0;
}

static void psp_server(char*buffer, size_t lenbuf)
{
  padico_out(20, "starting- len=%d...\n", lenbuf);
  bench_psp_post_recv(buffer, lenbuf);
  bench_psp_wait();
  bench_psp_send(buffer, lenbuf);
  padico_out(20, "done- len=%d...\n", lenbuf);
}

static void psp_client(char*buffer, size_t lenbuf)
{
  padico_out(20, "starting- len=%d...\n", lenbuf);
  bench_psp_post_recv(buffer, lenbuf);
  bench_psp_send(buffer, lenbuf);
  bench_psp_wait();
  padico_out(20, "done- len=%d...\n", lenbuf);
}

static const struct bench_desc_s psp_bench =
{
  "PSP",
  &psp_server,
  &psp_client
};

int bench_psp_run(int argc, char**argv)
{
  the_bench(&psp_bench);
  return 0;
}

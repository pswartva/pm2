/*
 * NewMadeleine
 * Copyright (C) 2014-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <nm_public.h>
#include <nm_private.h>
#include <nm_sendrecv_interface.h>
#include <nm_launcher_interface.h>
#include <nm_session_interface.h>
#include <Padico/Puk.h>
#include "nm_coll_private.h"
#include <Padico/Module.h>

PADICO_MODULE_HOOK(nm_coll);

PADICO_MODULE_ATTR(bcast, "NM_COLL_BCAST", "Control the default algorithm for bcast; by default, use tree algorithm with default tree nm_coll.tree_kind; recognized values: chain, binomial, binary, etc., pipeline-chain, pipeline-bnomial, etc., 2trees-chain, 2trees-binomial, etc.",
                   string, "default");

PADICO_MODULE_ATTR(bcast_pipeline_threshold, "NM_COLL_BCAST_PIPELINE_THRESHOLD", "Control the threshold for pipeline-based bcast; pipelined-bcast will not be considered at all below this threshold; default is 48000",
                   unsigned, 48000);

PADICO_MODULE_ATTR(bcast_pipeline_blocksize, "NM_COLL_BCAST_PIPELINE_BLOCKSIZE", "Size of blocks used in pipelined-bcast; default is 24000",
                   unsigned, 24000);

#ifdef NMAD_PROFILE
static struct
{
  struct nm_coll_profile_s bcast_all, bcast_tree, bcast_pipeline;
  struct nm_coll_profile_s ibcast_all, ibcast_tree, ibcast_pipeline;
} nm_coll_bcast_profile;

static void nm_coll_bcast_profile_init(void)
{
  nm_coll_profile_def(&nm_coll_bcast_profile.bcast_all, "bcast_all");
  nm_coll_profile_def(&nm_coll_bcast_profile.bcast_tree, "bcast_tree");
  nm_coll_profile_def(&nm_coll_bcast_profile.bcast_pipeline, "bcast_pipeline");
  nm_coll_profile_def(&nm_coll_bcast_profile.ibcast_all, "ibcast_all");
  nm_coll_profile_def(&nm_coll_bcast_profile.ibcast_tree, "ibcast_tree");
  nm_coll_profile_def(&nm_coll_bcast_profile.ibcast_pipeline, "ibcast_pipeline");
}
static void nm_coll_bcast_profile_exit(void)
{
  /* purposely do not free profile vars */
}
NM_LAZY_INITIALIZER(nm_coll_bcast_profile, &nm_coll_bcast_profile_init, &nm_coll_bcast_profile_exit);
#endif /* NMAD_PROFILE */

static void nm_coll_bcast_by_name(const char*name, nm_coll_req_kind_t*p_coll_kind, nm_coll_tree_kind_t*p_tree_kind);

static void nm_coll_bcast_pipeline(nm_session_t p_session, nm_group_t p_group, int root, int self,
                                   struct nm_data_s*p_data, nm_tag_t tag, nm_len_t block_size, nm_coll_tree_kind_t kind);

/* ** algorithm selection ********************************* */

static void nm_coll_bcast_oracle(nm_group_t p_group, int root, int self, struct nm_data_s*p_data,
                                 nm_coll_req_kind_t*p_coll_kind, nm_len_t*p_block_size, nm_coll_tree_kind_t*p_tree_kind)
{
  const unsigned bcast_pipeline_threshold = padico_module_attr_bcast_pipeline_threshold_getvalue();
  const unsigned bcast_pipeline_blocksize = padico_module_attr_bcast_pipeline_blocksize_getvalue();
  const char*coll_bcast = padico_module_attr_bcast_getvalue();
  nm_coll_req_kind_t coll_kind = NM_COLL_REQ_NONE;
  nm_coll_tree_kind_t tree_kind = NM_COLL_TREE_NONE;
  nm_coll_bcast_by_name(coll_bcast, &coll_kind, &tree_kind);

  if(nm_data_size(p_data) < bcast_pipeline_threshold)
    {
      /* small messages- always tree */
      if(coll_kind == NM_COLL_REQ_BCAST_PIPELINE)
        {
          /* obey tree kind ifg specified by user */
          *p_coll_kind  = NM_COLL_REQ_BCAST_TREE;
          *p_block_size = 0;
          *p_tree_kind  = tree_kind;
        }
      else
        {
          /* default heuristic else */
          *p_coll_kind  = NM_COLL_REQ_BCAST_TREE;
          *p_block_size = 0;
          *p_tree_kind  = nm_coll_tree_heuristic(nm_group_size(p_group), nm_data_size(p_data));
        }
    }
  else
    {
      /* large messages- tree, pipeline, or 2trees */
      if(coll_kind == NM_COLL_REQ_BCAST_PIPELINE)
        {
          *p_coll_kind  = NM_COLL_REQ_BCAST_PIPELINE;
          *p_block_size = bcast_pipeline_blocksize;
          *p_tree_kind  = (tree_kind == NM_COLL_TREE_NONE) ? NM_COLL_TREE_BINOMIAL : tree_kind;
        }
      else if(coll_kind == NM_COLL_REQ_BCAST_2TREES)
        {
          *p_coll_kind  = NM_COLL_REQ_BCAST_2TREES;
          *p_block_size = 0;
          *p_tree_kind  = (tree_kind == NM_COLL_TREE_NONE) ? NM_COLL_TREE_CHAIN : tree_kind;
        }
      else
        {
          assert(coll_kind == NM_COLL_REQ_BCAST_TREE);
          if(tree_kind == NM_COLL_TREE_DEFAULT)
            {
              if(!nm_launcher_homogeneous_network())
                {
                  *p_coll_kind  = NM_COLL_REQ_BCAST_TREE;
                  *p_block_size = 0;
                  *p_tree_kind  = nm_coll_tree_heuristic(nm_group_size(p_group), nm_data_size(p_data));
                }
              else
                {
                  /* automatic selection for large messages */
                  assert(nm_launcher_homogeneous_network()); /* homogeneous network => we have reliable driver profiling information to model performance */
                  /* Performance models for broadcast:
                   *
                   * pipelined chain = (comm_size - 1 + (data_size / block_size)) * (latency + (block_size / bandwidth))
                   * pipelined binary = ( log2(comm_size) + (data_size / block_size) - 1) * (latency + (2 * block_size / bandwidth))
                   * binomial = ceil(log2(n)) * (lat + x / bw)
                   */
                  const int group_size = nm_group_size(p_group);
                  nm_gate_t p_gate = nm_group_get_gate(p_group, (self + 1) % group_size);
                  const struct nm_drv_s*p_drv_small = p_gate->trks[NM_TRK_SMALL].p_drv;
                  const struct nm_drv_s*p_drv_large = p_gate->trks[NM_TRK_LARGE].p_drv;
                  const nm_len_t data_size = nm_data_size(p_data);
                  const nm_len_t block_size = bcast_pipeline_blocksize;

                  const double pipelined_chain_usec = (group_size - 1 + (data_size / block_size)) *
                    ((p_drv_small->props.profile.latency / 1000.0) + (block_size / (double)p_drv_small->props.profile.bandwidth));
                  const double pipelined_binary_usec = (nm_coll_log2_ceil(group_size) + (data_size / block_size) - 1) *
                    ((p_drv_small->props.profile.latency / 1000.0) + (2.0 * block_size / (double)p_drv_small->props.profile.bandwidth));

                  const nm_len_t threshold = nm_drv_max_small(p_drv_small);
                  const struct nm_drv_s*p_drv = (data_size < threshold) ? p_drv_small : p_drv_large;
                  const double lat = (p_drv->props.profile.latency / 1000.0);
                  const double bw = p_drv->props.profile.bandwidth;
                  const double binomial_usec = nm_coll_log2_ceil(group_size) * (lat + data_size / bw);

                  if( (binomial_usec < pipelined_chain_usec) && (binomial_usec < pipelined_binary_usec) )
                    {
                      /* binomial, no pipeline */
                      *p_coll_kind  = NM_COLL_REQ_BCAST_TREE;
                      *p_block_size = 0;
                      *p_tree_kind  = NM_COLL_TREE_BINOMIAL;
                    }
                  else if( (pipelined_chain_usec < binomial_usec) && (pipelined_chain_usec < pipelined_binary_usec) )
                    {
                      /* pipelined chain */
                      *p_coll_kind  = NM_COLL_REQ_BCAST_PIPELINE;
                      *p_block_size = block_size;
                      *p_tree_kind  = NM_COLL_TREE_CHAIN;
                    }
                  else
                    {
                      /* pipelined binomial */
                      *p_coll_kind  = NM_COLL_REQ_BCAST_PIPELINE;
                      *p_block_size = block_size;
                      *p_tree_kind  = NM_COLL_TREE_BINARY;
                    }
                }
            }
          else
            {
              *p_coll_kind  = NM_COLL_REQ_BCAST_TREE;
              *p_block_size = 0;
              *p_tree_kind  = tree_kind;
            }
        }
    }
}

static void nm_coll_bcast_by_name(const char*name, nm_coll_req_kind_t*p_coll_kind, nm_coll_tree_kind_t*p_tree_kind)
{
  static const char*s_pipeline = "pipeline-";
  static const char*s_2trees = "2trees-";
  size_t len_pipeline = strlen(s_pipeline);
  size_t len_2trees = strlen(s_2trees);
  if(strncmp(s_pipeline, name, len_pipeline) == 0)
    {
      *p_coll_kind = NM_COLL_REQ_BCAST_PIPELINE;
      *p_tree_kind = nm_coll_tree_kind_by_name(name + len_pipeline);
    }
  else if(strncmp(s_2trees, name, len_2trees) == 0)
    {
      *p_coll_kind = NM_COLL_REQ_BCAST_2TREES;
      *p_tree_kind = nm_coll_tree_kind_by_name(name + len_2trees);
    }
  else if(strcmp(name, "2ring") == 0)
    {
      *p_coll_kind = NM_COLL_REQ_BCAST_2TREES;
      *p_tree_kind = NM_COLL_TREE_CHAIN;
    }
  else
    {
      *p_coll_kind = NM_COLL_REQ_BCAST_TREE;
      *p_tree_kind = nm_coll_tree_kind_by_name(name);
    }
}


/* ** ibcast tree ****************************************** */

/** status of a tree-based ibcast */
struct nm_coll_bcast_tree_s
{
  struct nm_coll_tree_status_s coll_tree;
  struct nm_data_s data;
  int step;  /**< current step in tree  */
};

static void nm_coll_ibcast_tree_step(struct nm_coll_bcast_tree_s*p_bcast);
static void nm_coll_ibcast_tree_req_notifier(nm_sr_event_t event, const nm_sr_event_info_t*event_info, void*_ref);

static void nm_coll_ibcast_tree_step(struct nm_coll_bcast_tree_s*p_bcast)
{
 restart:
  assert(p_bcast->coll_tree.pending_reqs == 0);
  if(p_bcast->step < p_bcast->coll_tree.tree.height)
    {
      int parent = -1;
      int n_children = 0;
      nm_coll_tree_step(&p_bcast->coll_tree.tree, p_bcast->step, &parent,
                        p_bcast->coll_tree.children, &n_children);
      p_bcast->step++;
      assert(!((parent != -1) && (n_children > 0)));
      if(parent != -1)
        {
          nm_coll_tree_recv(&p_bcast->coll_tree, parent, &p_bcast->data,
                            &p_bcast->coll_tree.parent_request);
          nm_coll_tree_set_notifier(&p_bcast->coll_tree, &p_bcast->coll_tree.parent_request,
                                    &nm_coll_ibcast_tree_req_notifier);
        }
      else if(n_children > 0)
        {
          int i;
          for(i = 0; i < n_children; i++)
            {
              nm_coll_tree_send(&p_bcast->coll_tree, p_bcast->coll_tree.children[i], &p_bcast->data,
                                &p_bcast->coll_tree.children_requests[i]);
            }
          for(i = 0; i < n_children; i++)
            {
              nm_coll_tree_set_notifier(&p_bcast->coll_tree, &p_bcast->coll_tree.children_requests[i],
                                        &nm_coll_ibcast_tree_req_notifier);
            }
        }
      else
        {
          assert((parent == -1) && (n_children == 0));
          goto restart;
        }
    }
  else
    {
      /* notify completion */
      assert(p_bcast->step == p_bcast->coll_tree.tree.height);
      nm_coll_req_signal(nm_coll_req_container(p_bcast));
    }
}

static void nm_coll_ibcast_tree_req_notifier(nm_sr_event_t event, const nm_sr_event_info_t*event_info, void*_ref)
{
  struct nm_coll_tree_status_s*p_coll_tree = _ref;
  struct nm_coll_bcast_tree_s*p_bcast = nm_container_of(p_coll_tree, struct nm_coll_bcast_tree_s, coll_tree);
  const int p = nm_atomic_dec(&p_bcast->coll_tree.pending_reqs);
  if(p == 0)
    {
      nm_coll_ibcast_tree_step(p_bcast);
    }
}

static void nm_coll_ibcast_tree_destroy(struct nm_coll_req_s*p_coll_req)
{
  struct nm_coll_bcast_tree_s*p_bcast = nm_coll_req_payload(p_coll_req);
#ifdef NMAD_PROFILE
  puk_tick_t t;
  PUK_GET_TICK(t);
  const double tdiff = PUK_TIMING_DELAY(p_coll_req->start_time, t);
  nm_coll_bcast_profile_lazy_init();
  NM_COLL_PROFILE_ADD(&nm_coll_bcast_profile.ibcast_tree, &nm_coll_bcast_profile.ibcast_all,
                      nm_data_size(&p_bcast->data), nm_group_size(p_bcast->coll_tree.p_group), tdiff);
#endif /* NMAD_PROFILE */
  nm_coll_tree_status_destroy(&p_bcast->coll_tree);
}

struct nm_coll_req_s*nm_coll_ibcast_tree(nm_session_t p_session, nm_group_t p_group, int root, int self,
                                         struct nm_data_s*p_data, nm_tag_t tag, nm_coll_tree_kind_t kind,
                                         nm_coll_req_notifier_t p_notify, void*p_ref)
{
  struct nm_coll_req_s*p_coll_req = nm_coll_req_alloc(sizeof(struct nm_coll_bcast_tree_s), NM_COLL_REQ_BCAST_TREE, &nm_coll_ibcast_tree_destroy,
                                                      p_notify, p_ref);
  struct nm_coll_bcast_tree_s*p_bcast = nm_coll_req_payload(p_coll_req);
  p_bcast->data = *p_data;
  p_bcast->step = 0;
  nm_coll_tree_status_init(&p_bcast->coll_tree, p_session, kind, p_group, root, self, tag);
  nm_coll_ibcast_tree_step(p_bcast);
  return p_coll_req;
}

struct nm_coll_req_s*nm_coll_group_data_ibcast(nm_session_t p_session, nm_group_t p_group, int root, int self,
                                               struct nm_data_s*p_data, nm_tag_t tag,
                                               nm_coll_req_notifier_t p_notify, void*p_ref)
{
  nm_len_t block_size = 0;
  nm_coll_req_kind_t coll_kind = NM_COLL_REQ_NONE;
  nm_coll_tree_kind_t tree_kind = NM_COLL_TREE_NONE;
  nm_coll_bcast_oracle(p_group, root, self, p_data, &coll_kind, &block_size, &tree_kind);
  struct nm_coll_req_s*p_coll_req = NULL;
  switch(coll_kind)
    {
    case NM_COLL_REQ_BCAST_TREE:
      p_coll_req = nm_coll_ibcast_tree(p_session, p_group, root, self, p_data, tag, tree_kind, p_notify, p_ref);
      break;
    case NM_COLL_REQ_BCAST_PIPELINE:
      p_coll_req = nm_coll_ibcast_pipeline(p_session, p_group, root, self, p_data, tag, block_size, tree_kind, p_notify, p_ref);
      break;
    case NM_COLL_REQ_BCAST_2TREES:
      /* todo- 2trees not supported for ibcast */
      p_coll_req = nm_coll_ibcast_tree(p_session, p_group, root, self, p_data, tag, tree_kind, p_notify, p_ref);
      break;
    default:
      NM_FATAL("wrong coll kind = %d\n", coll_kind);
      break;
    }
  return p_coll_req;
}

/* ** bcast tree ******************************************* */

/** tree-based bcast */
void nm_coll_group_data_bcast_tree(nm_session_t p_session, nm_group_t p_group, int root, int self,
                                   struct nm_data_s*p_data, nm_tag_t tag, nm_coll_tree_kind_t kind)
{
#ifdef NMAD_PROFILE
  puk_tick_t start_time;
  PUK_GET_TICK(start_time);
#endif /* NMAD_PROFILE */
  assert(nm_group_get_gate(p_group, self) == nm_launcher_self_gate());
  const int size = nm_group_size(p_group);
  struct nm_coll_tree_info_s tree;
  nm_coll_tree_init(&tree, kind, size, self, root);
  nm_sr_request_t*requests = padico_malloc(tree.max_arity * sizeof(nm_sr_request_t));
  int*children = padico_malloc(sizeof(int) * tree.max_arity);
  int s;
  for(s = 0; s < tree.height; s++)
    {
      int parent = -1;
      int n_children = 0;
      nm_coll_tree_step(&tree, s, &parent, children, &n_children);
      assert((parent == -1) || (n_children == 0) || (children[0] == -1));
      if(parent != -1)
        {
          nm_gate_t p_parent_gate = nm_group_get_gate(p_group, parent);
          nm_sr_request_t request;
          nm_sr_irecv_data(p_session, p_parent_gate, tag, p_data, &request);
          nm_sr_rwait(p_session, &request);
        }
      if(n_children > 0)
        {
          int i;
          for(i = 0; i < n_children; i++)
            {
              assert(children[i] != -1);
              nm_gate_t p_gate = nm_group_get_gate(p_group, children[i]);
              nm_sr_isend_data(p_session, p_gate, tag, p_data, &requests[i]);
            }
          for(i = 0; i < n_children; i++)
            {
              nm_sr_swait(p_session, &requests[i]);
            }
        }
    }
  padico_free(children);
  padico_free(requests);
#ifdef NMAD_PROFILE
  puk_tick_t end_time;
  PUK_GET_TICK(end_time);
  const double tdiff = PUK_TIMING_DELAY(start_time, end_time);
  nm_coll_bcast_profile_lazy_init();
  NM_COLL_PROFILE_ADD(&nm_coll_bcast_profile.bcast_tree, &nm_coll_bcast_profile.bcast_all,
                      nm_data_size(p_data), nm_group_size(p_group), tdiff);
#endif /* NMAD_PROFILE */
}


/* ** ibcast pipeline ************************************** */

struct nm_coll_bcast_pipeline_s
{
  struct nm_coll_tree_status_s tree_status;
  struct nm_data_s data;
  nm_len_t block_size;
  int pipeline_stages;
  nm_sr_request_t*p_requests;
  int cs;
};

static void nm_coll_ibcast_pipeline_step(struct nm_coll_bcast_pipeline_s*p_bcast);
static void nm_coll_ibcast_pipeline_req_notifier(nm_sr_event_t event, const nm_sr_event_info_t*event_info, void*_ref);
static void nm_coll_ibcast_pipeline_destroy(struct nm_coll_req_s*p_coll_req);

struct nm_coll_req_s*nm_coll_ibcast_pipeline(nm_session_t p_session, nm_group_t p_group, int root, int self,
                                             struct nm_data_s*p_data, nm_tag_t tag, nm_len_t block_size, nm_coll_tree_kind_t kind,
                                             nm_coll_req_notifier_t p_notify, void*p_ref)
{
  struct nm_coll_req_s*p_coll_req = nm_coll_req_alloc(sizeof(struct nm_coll_bcast_pipeline_s), NM_COLL_REQ_BCAST_PIPELINE, &nm_coll_ibcast_pipeline_destroy,
                                                      p_notify, p_ref);
  struct nm_coll_bcast_pipeline_s*p_bcast = nm_coll_req_payload(p_coll_req);
  nm_coll_tree_status_init(&p_bcast->tree_status, p_session, kind, p_group, root, self, tag);
  assert(nm_group_get_gate(p_group, self) == nm_launcher_self_gate());
  const nm_len_t data_size = nm_data_size(p_data);
  p_bcast->data = *p_data;
  p_bcast->block_size = block_size;
  p_bcast->pipeline_stages = (data_size + (block_size - 1) ) / block_size; /* rounding instead of truncating */
  p_bcast->p_requests = padico_malloc(p_bcast->pipeline_stages * (1 + p_bcast->tree_status.tree.max_arity) * sizeof(nm_sr_request_t));
  p_bcast->cs = 0;
  NM_TRACEF("size = %zd; block_size = %zd; stages = %d\n", data_size, block_size, p_bcast->pipeline_stages);
  nm_coll_ibcast_pipeline_step(p_bcast);
  return p_coll_req;
}

static void nm_coll_ibcast_pipeline_destroy(struct nm_coll_req_s*p_coll_req)
{
  struct nm_coll_bcast_pipeline_s*p_bcast = nm_coll_req_payload(p_coll_req);
#ifdef NMAD_PROFILE
  puk_tick_t t;
  PUK_GET_TICK(t);
  const double tdiff = PUK_TIMING_DELAY(p_coll_req->start_time, t);
  nm_coll_bcast_profile_lazy_init();
  NM_COLL_PROFILE_ADD(&nm_coll_bcast_profile.ibcast_pipeline, &nm_coll_bcast_profile.ibcast_all,
                      nm_data_size(&p_bcast->data), nm_group_size(p_bcast->tree_status.p_group), tdiff);
#endif /* NMAD_PROFILE */
  nm_coll_tree_status_destroy(&p_bcast->tree_status);
  padico_free(p_bcast->p_requests);
}

static void nm_coll_ibcast_pipeline_step(struct nm_coll_bcast_pipeline_s*p_bcast)
{
 restart:
  assert(p_bcast->tree_status.pending_reqs == 0);
  if(p_bcast->cs < p_bcast->tree_status.tree.height + p_bcast->pipeline_stages)
    {
      int nreqs = 0;
      int p;
      for(p = 0; p < p_bcast->pipeline_stages; p++)
        {
          /* make progress on all pipeline stages */
          const int step = p_bcast->cs - p;
          if((step >= 0) && (step < p_bcast->tree_status.tree.height))
            {
              int parent = -1;
              nm_coll_tree_step(&p_bcast->tree_status.tree, step, &parent,
                                p_bcast->tree_status.children, &p_bcast->tree_status.n_children);
              assert((parent == -1) || (p_bcast->tree_status.n_children == 0) || (p_bcast->tree_status.children[0] == -1));
              NM_TRACEF("step = %d; p = %d; parent = %d; n_children = %d\n", step, p, parent, p_bcast->tree_status.n_children);
              /* build data chunk descriptor */
              const nm_len_t chunk_len = (p == p_bcast->pipeline_stages - 1) ?
                (nm_data_size(&p_bcast->data) - (p_bcast->pipeline_stages - 1 ) * p_bcast->block_size) : p_bcast->block_size;
              const nm_len_t chunk_offset = p * p_bcast->block_size;
              struct nm_data_s data;
              nm_data_excerpt_build(&data, &p_bcast->data, chunk_offset, chunk_len);
              /* recv from parent */
              if(parent != -1)
                {
                  nm_coll_tree_recv(&p_bcast->tree_status, parent, &data, &p_bcast->p_requests[nreqs]);
                  nm_coll_tree_set_notifier(&p_bcast->tree_status, &p_bcast->p_requests[nreqs],
                                            &nm_coll_ibcast_pipeline_req_notifier);
                  nreqs++;
                }
              /* send to children */
              if(p_bcast->tree_status.n_children > 0)
                {
                  int i;
                  for(i = 0; i < p_bcast->tree_status.n_children; i++)
                    {
                      if((p_bcast->pipeline_stages > 1) && (p == 0))
                        {
                          /* send first block in synchronous mode to allow overflooding
                           * with lots of unexpected small packets below rdv threshold */
                          nm_coll_tree_issend(&p_bcast->tree_status, p_bcast->tree_status.children[i], &data, &p_bcast->p_requests[nreqs]);
                        }
                      else
                        {
                          nm_coll_tree_send(&p_bcast->tree_status, p_bcast->tree_status.children[i], &data, &p_bcast->p_requests[nreqs]);
                        }
                      nm_coll_tree_set_notifier(&p_bcast->tree_status, &p_bcast->p_requests[nreqs],
                                                &nm_coll_ibcast_pipeline_req_notifier);
                      nreqs++;
                    }
                }
            }
        }
      assert(nreqs <= p_bcast->pipeline_stages * (1 + p_bcast->tree_status.tree.max_arity));
      if(nreqs == 0)
        {
          /* no request posted */
          p_bcast->cs++;
          goto restart;
        }
    }
  else
    {
      /* notify completion */
      nm_coll_req_signal(nm_coll_req_container(p_bcast));
    }
}

static void nm_coll_ibcast_pipeline_req_notifier(nm_sr_event_t event, const nm_sr_event_info_t*event_info, void*_ref)
{
  struct nm_coll_tree_status_s*p_coll_tree = _ref;
  struct nm_coll_bcast_pipeline_s*p_bcast = nm_container_of(p_coll_tree, struct nm_coll_bcast_pipeline_s, tree_status);
  const int p = nm_atomic_dec(&p_bcast->tree_status.pending_reqs);
  if(p == 0)
    {
      p_bcast->cs++;
      nm_coll_ibcast_pipeline_step(p_bcast);
    }
}

/** pipelined bcast */
static void nm_coll_bcast_pipeline(nm_session_t p_session, nm_group_t p_group, int root, int self,
                                   struct nm_data_s*p_data, nm_tag_t tag, nm_len_t block_size, nm_coll_tree_kind_t kind)
{
#ifdef NMAD_PROFILE
  puk_tick_t start_time;
  PUK_GET_TICK(start_time);
#endif /* NMAD_PROFILE */
  assert(nm_group_get_gate(p_group, self) == nm_launcher_self_gate());
  struct nm_coll_tree_info_s tree;
  nm_coll_tree_init(&tree, kind, nm_group_size(p_group), self, root);
  const nm_len_t data_size = nm_data_size(p_data);
  int pipeline_stages = (data_size + (block_size - 1) ) / block_size; /* rounding instead of truncating */
  nm_sr_request_t*requests = padico_malloc(pipeline_stages * (1 + tree.max_arity) * sizeof(nm_sr_request_t));
  int*children = padico_malloc(sizeof(int) * tree.max_arity);
  NM_TRACEF("size = %zd; block_size = %zd; stages = %d\n", data_size, block_size, pipeline_stages);
  int cs; /* compound step */
  for(cs = 0; cs < tree.height + pipeline_stages - 1; cs++)
    {
      int nreqs = 0;
      int p;
      for(p = 0; p < pipeline_stages; p++)
        {
          /* make progress on all pipeline stages */
          const int step = cs - p;
          if((step >= 0) && (step < tree.height))
            {
              int parent = -1;
              int n_children = 0;
              nm_coll_tree_step(&tree, step, &parent, children, &n_children);
              assert((parent == -1) || (n_children == 0) || (children[0] == -1));
              NM_TRACEF("step = %d; p = %d; parent = %d; n_children = %d\n", step, p, parent, n_children);
              /* build data chunk descriptor */
              const nm_len_t chunk_len = (p == pipeline_stages - 1) ?
                (data_size - (pipeline_stages - 1 ) * block_size) : block_size;
              const nm_len_t chunk_offset = p * block_size;
              struct nm_data_s data;
              nm_data_excerpt_build(&data, p_data, chunk_offset, chunk_len);
              /* recv from parent */
              if(parent != -1)
                {
                  nm_gate_t p_parent_gate = nm_group_get_gate(p_group, parent);
                  nm_sr_request_t*p_req = &requests[nreqs];
                  nreqs++;
                  nm_sr_irecv_data(p_session, p_parent_gate, tag, &data, p_req);
                }
              /* send to children */
              if(n_children > 0)
                {
                  int i;
                  for(i = 0; i < n_children; i++)
                    {
                      assert(children[i] != -1);
                      nm_gate_t p_gate = nm_group_get_gate(p_group, children[i]);
                      nm_sr_request_t*p_req = &requests[nreqs];
                      nreqs++;
                      nm_sr_send_init(p_session, p_req);
                      nm_sr_send_pack_data(p_session, p_req, &data);
                      if((pipeline_stages > 1) && (p == 0))
                        {
                          /* send first block in synchronous mode to allow overflooding
                           * with lots of unexpected small packets below rdv threshold */
                          nm_sr_send_issend(p_session, p_req, p_gate, tag);
                        }
                      else
                        {
                          nm_sr_send_isend(p_session, p_req, p_gate, tag);
                        }
                    }
                }
            }
        }
      assert(nreqs <= pipeline_stages * (1 + tree.max_arity));
      int i;
      for(i = 0; i < nreqs; i++)
        {
          nm_sr_request_wait(&requests[i]);
        }
    }
  padico_free(children);
  padico_free(requests);
#ifdef NMAD_PROFILE
  puk_tick_t end_time;
  PUK_GET_TICK(end_time);
  const double tdiff = PUK_TIMING_DELAY(start_time, end_time);
  nm_coll_bcast_profile_lazy_init();
  NM_COLL_PROFILE_ADD(&nm_coll_bcast_profile.bcast_pipeline, &nm_coll_bcast_profile.bcast_all,
                      nm_data_size(p_data), nm_group_size(p_group), tdiff);
#endif /* NMAD_PROFILE */
}


/* ** 2trees *********************************************** */

/** split data, then use 2 different trees for each part */
void nm_coll_bcast_2trees(nm_session_t p_session, nm_group_t p_group, int root, int self,
                          struct nm_data_s*p_data, nm_tag_t tag1, nm_tag_t tag2, nm_len_t block_size, nm_coll_tree_kind_t kind)
{
  /* split data */
  const nm_len_t data_size = nm_data_size(p_data);
  assert(data_size > 2);
  const nm_len_t split_point = data_size / 2;
  assert(split_point > 0);
  struct nm_data_s data1, data2;
  nm_data_excerpt_build(&data1, p_data, 0, split_point - 1);
  nm_data_excerpt_build(&data2, p_data, split_point, data_size - split_point);
  /* build 2 groups in opposite order */
  nm_group_t p_group1 = p_group, p_group2 = nm_group_new();
  int k;
  for(k = 0; k < nm_group_size(p_group); k++)
    {
      nm_gate_t p_gate = nm_group_get_gate(p_group, nm_group_size(p_group) - k - 1);
      nm_group_add_node(p_group2, p_gate);
    }
  const int root1 = root, root2 = nm_group_size(p_group) - root - 1;
  assert(nm_group_get_gate(p_group2, root2) == nm_group_get_gate(p_group1, root1));
  const int self1 = self, self2 = nm_group_size(p_group) - self - 1;
  assert(nm_group_get_gate(p_group2, self2) == nm_group_get_gate(p_group1, self1));
  /* start both ibcasts */
  struct nm_coll_req_s*p_bcast1 = nm_coll_group_data_ibcast(p_session, p_group1, root1, self1, &data1, tag1, NULL, NULL);
  struct nm_coll_req_s*p_bcast2 = nm_coll_group_data_ibcast(p_session, p_group2, root2, self2, &data2, tag2, NULL, NULL);
  nm_coll_req_wait(p_bcast1);
  nm_coll_req_wait(p_bcast2);
}


/* ** blocking bcast frontends ***************************** */

/** frontend to chose automatically the best bcast algorithm */
void nm_coll_group_data_bcast(nm_session_t p_session, nm_group_t p_group, int root, int self,
                              struct nm_data_s*p_data, nm_tag_t tag)
{
  assert((self >= 0) && (self < nm_group_size(p_group)));
  assert((root >= 0) && (root < nm_group_size(p_group)));
  assert(nm_group_get_gate(p_group, self) == nm_launcher_self_gate());
  nm_len_t block_size = 0;
  nm_coll_req_kind_t coll_kind = NM_COLL_REQ_NONE;
  nm_coll_tree_kind_t tree_kind = NM_COLL_TREE_NONE;
  nm_coll_bcast_oracle(p_group, root, self, p_data, &coll_kind, &block_size, &tree_kind);
  switch(coll_kind)
    {
    case NM_COLL_REQ_BCAST_TREE:
      nm_coll_group_data_bcast_tree(p_session, p_group, root, self, p_data, tag, tree_kind);
      break;
    case NM_COLL_REQ_BCAST_PIPELINE:
      assert(block_size > 0);
      nm_coll_bcast_pipeline(p_session, p_group, root, self, p_data, tag, block_size, tree_kind);
      break;
    case NM_COLL_REQ_BCAST_2TREES:
      nm_coll_bcast_2trees(p_session, p_group, root, self, p_data, tag, tag, block_size, tree_kind);
      break;
    default:
      NM_FATAL("wrong coll kind = %d\n", coll_kind);
      break;
    }
}

void nm_coll_group_bcast(nm_session_t p_session, nm_group_t p_group, int root, int self,
                         void*buffer, nm_len_t len, nm_tag_t tag)
{
  struct nm_data_s data;
  nm_data_contiguous_build(&data, (void*)buffer, len);
  nm_coll_group_data_bcast(p_session, p_group, root, self, &data, tag);
}

void nm_coll_data_bcast(nm_comm_t p_comm, int root, struct nm_data_s*p_data, nm_tag_t tag)
{
  nm_coll_group_data_bcast(nm_comm_get_session(p_comm), nm_comm_group(p_comm),
                           root, nm_comm_rank(p_comm), p_data, tag);
}

void nm_coll_bcast(nm_comm_t p_comm, int root, void*buffer, nm_len_t len, nm_tag_t tag)
{
  nm_coll_group_bcast(nm_comm_get_session(p_comm), nm_comm_group(p_comm),
                      root, nm_comm_rank(p_comm), buffer, len, tag);
}

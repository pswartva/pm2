/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <Padico/Puk.h>
#include <nm_public.h>
#include <nm_private.h>
#include <nm_launcher.h>

#include <string.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netdb.h>
#include <ifaddrs.h>
#include <sys/uio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <netpacket/packet.h>

#include <Padico/Module.h>


static int nm_cmdline_launcher_declare(void);
static void nm_cmdline_launcher_finalize(void);

PADICO_MODULE_BUILTIN(NewMad_Launcher_cmdline, &nm_cmdline_launcher_declare, NULL, &nm_cmdline_launcher_finalize);

/* ** Cmd line launcher ************************************ */

static void*nm_cmdline_launcher_instantiate(puk_instance_t i, puk_context_t c);
static void nm_cmdline_launcher_destroy(void*_status);

static const struct puk_component_driver_s nm_cmdline_launcher_component_driver =
  {
    .instantiate = &nm_cmdline_launcher_instantiate,
    .destroy     = &nm_cmdline_launcher_destroy
  };

static void nm_cmdline_launcher_init(void*_status, int*argc, char**argv, const char*group_name);
static void nm_cmdline_launcher_barrier(void*_status);
static void nm_cmdline_launcher_get_gates(void*_status, nm_gate_t*gates);
static void nm_cmdline_launcher_abort(void*_status, int rc);

static const struct nm_launcher_driver_s nm_cmdline_launcher_driver =
  {
    .init         = &nm_cmdline_launcher_init,
    .barrier      = &nm_cmdline_launcher_barrier,
    .get_gates    = &nm_cmdline_launcher_get_gates,
    .abort        = &nm_cmdline_launcher_abort
  };

static struct
{
  puk_component_t component;
} nm_cmdline_launcher = { .component = NULL };

static int nm_cmdline_launcher_declare(void)
{
  nm_cmdline_launcher.component =
    puk_component_declare("NewMad_Launcher_cmdline",
                          puk_component_provides("PadicoComponent", "component", &nm_cmdline_launcher_component_driver),
                          puk_component_provides("NewMad_Launcher", "launcher", &nm_cmdline_launcher_driver ));
  return 0;
}

static void nm_cmdline_launcher_finalize(void)
{
  puk_component_destroy(nm_cmdline_launcher.component);
}


struct nm_cmdline_launcher_status_s
{
  nm_gate_t p_gate;
  nm_gate_t p_self_gate;
  int is_server;
  int sock;
};

static void*nm_cmdline_launcher_instantiate(puk_instance_t i, puk_context_t c)
{
  struct nm_cmdline_launcher_status_s*status = padico_malloc(sizeof(struct nm_cmdline_launcher_status_s));
  status->p_gate = NM_GATE_NONE;
  status->is_server = 0;
  status->sock = -1;
  return status;
}

static void nm_cmdline_launcher_destroy(void*_status)
{
  struct nm_cmdline_launcher_status_s*status = _status;
  if(status->sock > 0)
    NM_SYS(close)(status->sock);
  padico_free(status);
}

static void nm_cmdline_launcher_get_gates(void*_status, nm_gate_t *_gates)
{
  struct nm_cmdline_launcher_status_s*status = _status;
  int self = status->is_server ? 0 : 1;
  int peer = 1 - self;
  _gates[self] = status->p_self_gate;
  _gates[peer] = status->p_gate;
}

static void nm_cmdline_launcher_abort(void*_status, int rc)
{
  exit(rc);
}

static void nm_cmdline_launcher_addr_send(int sock, const char*url)
{
  int len = strlen(url) + 1;
  int rc = NM_SYS(send)(sock, &len, sizeof(len), 0);
  if(rc != sizeof(len))
    {
      NM_FATAL("cannot send address to peer.\n");
    }
  rc = NM_SYS(send)(sock, url, len, 0);
  if(rc != len)
    {
      NM_FATAL("cannot send address to peer.\n");
    }
}

static void nm_cmdline_launcher_addr_recv(int sock, char**p_url)
{
  int len = -1;
  int rc = -1;
 retry_recv:
  rc = NM_SYS(recv)(sock, &len, sizeof(len), MSG_WAITALL);
  int err = errno;
  if(rc == -1 && err == EINTR)
    goto retry_recv;
  if(rc != sizeof(len))
    {
      NM_FATAL("cannot get address from peer (%s).\n", strerror(err));
    }
  char*url = padico_malloc(len);
 retry_recv2:
  rc = NM_SYS(recv)(sock, url, len, MSG_WAITALL);
  err = errno;
  if(rc == -1 && err == EINTR)
    goto retry_recv2;
  if(rc != len)
    {
      NM_FATAL("cannot get address from peer (%s).\n", strerror(err));
    }
  *p_url = url;
}

static void nm_cmdline_launcher_barrier(void*_status)
{
  struct nm_cmdline_launcher_status_s*status = _status;
  const char*text = "barrier";
  nm_cmdline_launcher_addr_send(status->sock, text);
  char*dummy = NULL;
  nm_cmdline_launcher_addr_recv(status->sock, &dummy);
  padico_free(dummy);
}

void nm_cmdline_launcher_init(void*_status, int*argc, char**argv, const char*_label)
{
  struct nm_cmdline_launcher_status_s*status = _status;
  const char*local_session_url = NULL;
  char*remote_session_url = NULL;
  const char*remote_launcher_url = NULL;

  if(argc == NULL || argv == NULL)
    {
      NM_FATAL("argc=NULL or argv=NULL; cmdline launcher needs real argc & argv.\n");
    }
  int i;
  for(i = 1; i < *argc; i++)
    {
      if(strcmp(argv[i], "-R") == 0)
        {
          /* parse -R for rails */
          if(*argc <= i+1)
            {
              NM_FATAL("# launcher: missing railstring.\n");
            }
          const char*railstring = argv[i+1];
          padico_setenv("NMAD_DRIVER", railstring); /* TODO- convert this into an nm_session_set_param() */
          NM_DISPF("railstring = %s\n", railstring);
          *argc = *argc - 2;
          int j;
          for(j = i; j < *argc; j++)
            {
              argv[j] = argv[j + 2];
            }
          i--;
          continue;
        }
      else if(argv[i][0] != '-')
        {
          remote_launcher_url = argv[i];
          NM_DISPF("remote url = %s\n", remote_launcher_url);
          /* update command line  */
          *argc = *argc - 1;
          int j;
          for(j = i; j < *argc; j++)
            {
              argv[j] = argv[j+1];
            }
          break;
        }
      else if(strcmp(argv[i], "--") == 0)
        {
          /* app args follow; update command line  */
          *argc = *argc - 1;
          int j;
          for(j = i; j < *argc; j++)
            {
              argv[j] = argv[j+1];
            }
          break;
        }
    }
  argv[*argc] = NULL;
  status->is_server = (!remote_launcher_url);

  const struct nm_launcher_info_s info = { .rank = status->is_server ? 0 : 1, .size = 2, .wide_url_support = 1 };
  nm_launcher_set_info(&info);

  int err = nm_launcher_get_url(&local_session_url);
  if (err != NM_ESUCCESS)
    {
      NM_FATAL("nm_launcher_get_url returned err = %d\n", err);
    }


  /* address exchange */

  if(!remote_launcher_url)
    {
      /* server */
      char local_launcher_url[16] = { 0 };
      int server_sock = socket(AF_INET, SOCK_STREAM, 0);
      assert(server_sock > -1);
      int val = 1;
      int rc = NM_SYS(setsockopt)(server_sock, SOL_SOCKET, SO_REUSEADDR, &val, sizeof(val));
      struct sockaddr_in addr;
      unsigned addr_len = sizeof addr;
      addr.sin_family = AF_INET;
      addr.sin_port = htons(8657 + getuid());
      addr.sin_addr.s_addr = INADDR_ANY;
      rc = NM_SYS(bind)(server_sock, (struct sockaddr*)&addr, addr_len);
      int err = errno;
      if(rc == -1 && err == EADDRINUSE)
        {
          /* try again without default port */
          addr.sin_family = AF_INET;
          addr.sin_port = htons(0);
          addr.sin_addr.s_addr = INADDR_ANY;
          rc = NM_SYS(bind)(server_sock, (struct sockaddr*)&addr, addr_len);
        }
      if(rc)
        {
          NM_FATAL("bind error (%s)\n", strerror(err));
        }
      rc = NM_SYS(getsockname)(server_sock, (struct sockaddr*)&addr, &addr_len);
      NM_SYS(listen)(server_sock, 255);
      struct in_addr inaddr = puk_inet_getaddr();
      snprintf(local_launcher_url, 16, "%08x%04x", htonl(inaddr.s_addr), addr.sin_port);
      if(local_launcher_url[0] == '\0')
        {
          NM_FATAL("cannot get local address\n");
        }
      fprintf(stderr, "# launcher: local url = '%s'\n", local_launcher_url);
      fprintf(stderr, "# launcher: cmdline launcher waiting for other node to connect manually. This is probably not what you want. Please launch binary through padico-launch, mpirun, or srun.\n");
      status->sock = -1;
    retry_accept:
      status->sock = NM_SYS(accept)(server_sock, (struct sockaddr*)&addr, &addr_len);
      err = errno;
      if(status->sock < 0)
        {
          if(err == EINTR)
            goto retry_accept;
          NM_FATAL("accept() failed- err = %d (%s)\n", err, strerror(err));
        }
      NM_SYS(close)(server_sock);
      nm_cmdline_launcher_addr_send(status->sock, local_session_url);
      nm_cmdline_launcher_addr_recv(status->sock, &remote_session_url);
    }
  else
    {
      /* client */
      status->sock = NM_SYS(socket)(AF_INET, SOCK_STREAM, 0);
      assert(status->sock > -1);
      assert(strlen(remote_launcher_url) == 12);
      in_addr_t peer_addr;
      int peer_port;
      sscanf(remote_launcher_url, "%08x%04x", &peer_addr, &peer_port);
      struct sockaddr_in inaddr = {
        .sin_family = AF_INET,
        .sin_port   = peer_port,
        .sin_addr   = (struct in_addr){ .s_addr = ntohl(peer_addr) }
      };
      int rc = -1;
    retry_connect:
      rc = NM_SYS(connect)(status->sock, (struct sockaddr*)&inaddr, sizeof(struct sockaddr_in));
      int err = errno;
      if(rc)
        {
          if(err == EINTR)
            goto retry_connect;
          NM_FATAL("cannot connect to %s:%d\n", inet_ntoa(inaddr.sin_addr), peer_port);
        }
      nm_cmdline_launcher_addr_recv(status->sock, &remote_session_url);
      nm_cmdline_launcher_addr_send(status->sock, local_session_url);
    }

  /* connect remote */
  err = nm_launcher_connect(&status->p_gate, remote_session_url);
  if (err != NM_ESUCCESS)
    {
      NM_FATAL("nm_launcher_connect returned err = %d\n", err);
    }

  /* connect to self */
  err = nm_launcher_connect(&status->p_self_gate, local_session_url);
  if (err != NM_ESUCCESS)
    {
      NM_FATAL("self nm_launcher_connect returned err = %d\n", err);
    }

  if(remote_session_url != NULL)
    padico_free(remote_session_url);
}

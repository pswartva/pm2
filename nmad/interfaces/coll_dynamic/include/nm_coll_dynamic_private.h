/*
 * NewMadeleine
 * Copyright (C) 2019-2020 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#ifndef NM_COLL_PRIVATE_H
#define NM_COLL_PRIVATE_H

#include <nm_sendrecv_interface.h>
#include <nm_rpc_interface.h>

#ifdef PIOMAN

typedef void (*callback_function)(void*);
typedef void (*callback_function_come_from_coop)(struct nm_data_s*, nm_len_t, void*);
typedef void (*callback_function_normal_recv)(nm_sr_request_t*, void*);

/** header of mutlicast requests */
struct multicast_header_s
{
  nm_len_t body_len; // See (struct nm_multicast_req_s).body_len
  int nb_nodes;
  int src_node;
}  __attribute__((packed));


enum nm_multicast_req_type_e {
  UNSPECIFIED, // Error !
  PENDING, // I'm the pending recv: the recv always posted to receive broadcast messages which have not matching user recv
  USER_RECV, // I'm the recv posted by the user
  INITIAL_SEND, // I'm the node which called nm_coll_dynamic_send*() and this is my first send
  FORWARD_INITIAL, // I'm the node which called nm_coll_dynamic_send*(), but it's not my first send
  FORWARD_RECV // I received data from a broadcast, and I'm forwarding it to other nodes
};


/** a multicast request (outgoing or ingoing) */
struct nm_multicast_req_s
{
  /* Common attributes for send and recv requests: */
  nm_sr_request_t* request;                /**< the sendrecv request, actually allocated here */
  nm_session_t p_session;                 /**< session the incoming request belongs to */
  struct nm_data_s body_data;             /**< the user-supplied body data descriptor */
  int* ref_nodes;                         /**< ref for nodes to still send data */
  int* ref_prios;                         /** < send to ref_nodes[i] has priority ref_prios[i] */
  struct multicast_header_s* ref_header;  /**< header of the multicast request */
  nm_tag_t tag;

  /* Attributes for recv requests: */
  int src_node;
  void (*come_from_coop_callback)(struct nm_data_s*, nm_len_t, void*);
  void* come_from_coop_callback_arg;
  void (*end_recv_callback)(void*);
  void* end_recv_callback_arg;
  void (*normal_recv_recv_data_callback)(nm_sr_request_t*, void*);
  void* normal_recv_recv_data_callback_arg;
  piom_cond_t* cond_wait_recv;
  struct nm_data_s header_data;           /**< pre-built type for header recv */
  struct nm_data_s multicast_data;        /**< data descriptor for multicast request */
  nm_rpc_token_t rpc_token;
  void* copied_data_ptr;                  /**< ptr to a copy of data received; in recv if a forward is needed,
                                            received data are copied to this memory, in order to return data
                                            to application, be still be able to forward data to other nodes */

  /* Attributes for send requests: */
  int last;                               /**< boolean to 1 if it'is the last request of the multicast,
                                               to free resources */
  int nb_nodes_in_tree;
  int tree_step;
  int tree_additionnal_info;
  nm_len_t body_len;                       /**< size of the data, used only by nm_coll_dynamic_send() and
                                             nm_coll_dynamic_recv_register() to build the nm_data.
                                             Otherwise it has a meaning only for the application, which can
                                             need it if the receive buffer is defined by the application only
                                             in the callback com_from_coop */
  struct nm_multicast_req_s* initial_recv; /**< contains a request describing the recv posted by the user
                                             (registered with nm_coll_dynamic_register_recv*() */
  void (*end_send_callback)(void*);
  void* end_send_callback_arg;

  enum nm_multicast_req_type_e type;
};

PUK_ALLOCATOR_TYPE(nm_multicast_req, struct nm_multicast_req_s);

void free_request_nodes_data(struct nm_multicast_req_s* p_req);
void send_multicast(int* nodes, int* prios, int nb_nodes, nm_tag_t tag, struct nm_data_s* data,
                    nm_len_t body_len, int tree_step, int separator_id, int src_node, 
                    struct nm_multicast_req_s* initial_recv, callback_function end_send_callback, void* arg);

void monitor_send(nm_rpc_req_t rpc_req, void* _ref);
void normal_receive_handler(nm_sr_event_t event, const nm_sr_event_info_t*p_info, void* _ref);
void register_recv_data(nm_session_t p_session, int src_node, nm_tag_t tag, struct nm_data_s* data, 
                        nm_len_t len, nm_sr_request_t *p_request, piom_cond_t* cond_wait_recv, 
                        callback_function_come_from_coop come_from_coop_callback, callback_function end_recv_callback,
                        callback_function_normal_recv normal_recv_callback, void* arg);


#else /* PIOMAN */
#  error "cannot use interface coll_dynamic without pioman"
#endif /* PIOMAN */

#endif /* NM_COLL_PRIVATE_H */


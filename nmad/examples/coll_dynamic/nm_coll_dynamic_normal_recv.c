/*
 * NewMadeleine
 * Copyright (C) 2016-2019 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <mpi.h>

#include <nm_launcher_interface.h>
#include <nm_coll_dynamic_interface.h>

#include "../common/nm_examples_helper.h"


const char msg[] = "Hello, world!";


int main(int argc, char**argv)
{
  nm_examples_init_topo(&argc, argv, NM_EXAMPLES_TOPO_STAR);
  nm_coll_dynamic_init_multicast();

  if(is_server)
    {
      int n;
      nm_launcher_get_size(&n);

      for(int i = 1; i < n; i++)
        {
          nm_gate_t gate = NULL;
          nm_launcher_get_gate(i, &gate);
          nm_sr_send(p_session, gate, 4, msg, sizeof(msg));
        }
    }
  else
    {
      nm_sr_request_t request;
      char* received_msg = malloc(sizeof(msg));

      piom_cond_t cond_wait_recv;
      piom_cond_init(&cond_wait_recv, 0);

      nm_coll_dynamic_recv_register_recv(p_session, 0, 4, received_msg, sizeof(msg), &request, 
                                          &cond_wait_recv, NULL, NULL, NULL, NULL);

      piom_cond_wait(&cond_wait_recv, 1);
      piom_cond_destroy(&cond_wait_recv);

      assert(memcmp(received_msg, msg, sizeof(msg)) == 0);

      free(received_msg);
    }

  printf("success\n");

  nm_coll_dynamic_shutdown();
  nm_examples_exit();
  return 0;
}

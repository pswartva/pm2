/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Early types definitions for Puk.
 * We define here types that do not have dependancies.
 * @ingroup Puk
 */

#ifndef PUK_TYPES_H
#define PUK_TYPES_H

/** forward declaration for modules */
struct puk_mod_s;

/** base module type */
typedef struct puk_mod_s*puk_mod_t;


/* ** profiling ******************************************** */

/** a block of memory profiling info */
struct puk_profile_mem_s
{
  unsigned long long total_mem;  /**< total amount of memory allocated for this entry */
  unsigned long long max_mem;    /**< maximum amount of memory allocated for this entry */
  unsigned long long num_malloc; /**< number of malloc() performed for this entry */
  unsigned long long num_free;   /**< number of free() performed for this entry */
};
#define PUK_PROFILE_MEM_STATIC_INITIALIZER \
  { .total_mem = 0, .max_mem = 0, .num_malloc = 0, .num_free = 0 }



#endif /* PUK_TYPES_H */

/*
 * NewMadeleine
 * Copyright (C) 2014-2022 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <nm_coll_interface.h>

#include <Padico/Module.h>
PADICO_MODULE_HOOK(nm_coll);

PADICO_MODULE_ATTR(tree_kind, "NM_COLL_TREE_KIND", "Control the default kind of tree used for collectives; by default, use 4-nomial for packets < 32kB, binomial else.",
                   string, "default");


/* **********************************************************/
/* @file
 * trees description for collective operations
 */


/** helper to fill children/parent */
static inline void nm_coll_tree_rule_send(const struct nm_coll_tree_info_s*p_tree, int*p_parent, int*p_children, int*n_children, int vself, int from, int dest)
{
  assert((from >= 0) && (from < p_tree->n));
  assert((dest >= 0) && (dest < p_tree->n));
  if(vself == from)
    {
      p_children[0] = nm_coll_v2r(dest, p_tree);
      *n_children = 1;
      *p_parent = -1;
    }
  else if(vself == dest)
    {
      *n_children = 0;
      *p_parent = nm_coll_v2r(from, p_tree);
    }
  else
    {
      *n_children = 0;
      *p_parent = -1;
    }
}


/* ** binomial tree **************************************** */

static inline void nm_coll_binomial_init(struct nm_coll_tree_info_s*p_tree)
{
  p_tree->max_arity = 1;
  p_tree->height = nm_coll_log2_ceil(p_tree->n);
}

/** walk to the leafs of the tree
 * @param i rank of local node
 * @param n number of leafs
 * @param level level in tree, numbered from the leafs
 */
static inline int nm_coll_binomial_child(int i, int n, int level)
{
  assert(level >= 0);
  const int skip = (1 << level);
  assert(skip >= 0);
  if((i % (2*skip) == 0) && (i + skip < n))
    return (i + skip);
  else
    return -1;
}

/** walk to the root of the tree, level numbered from the leafs */
static inline int nm_coll_binomial_parent(int i, int n __attribute__((unused)), int level)
{
  const int skip = (1 << level);
  if(i % (2*skip) == skip)
    return (i - skip);
  else
    return -1;
}

/** number of leafs below the vertex */
static inline int nm_coll_binomial_weight(int i, int n, int level)
{
  const int skip = (1 << (level));
  const int parent = nm_coll_binomial_parent(i, n, level);
  if(parent == -1)
    return -1;
  else if(i + skip > n)
    return (n - i);
  else
    return skip;
}

static inline void nm_coll_binomial_step(const struct nm_coll_tree_info_s*p_tree, int step, int*p_parent, int*p_children, int*n_children)
{
  const int child = nm_coll_binomial_child(nm_coll_r2v(p_tree->self, p_tree), p_tree->n, p_tree->height - step - 1);
  const int parent = nm_coll_binomial_parent(nm_coll_r2v(p_tree->self, p_tree), p_tree->n, p_tree->height - step - 1);
  if(parent != -1)
    {
      *p_parent = nm_coll_v2r(parent, p_tree);
    }
  else
    {
      *p_parent = -1;
    }
  if(child != -1)
    {
      p_children[0] = nm_coll_v2r(child, p_tree);
      *n_children = 1;
    }
  else
    {
      p_children[0] = -1;
      *n_children = 0;
    }
}

/* ** k-nomial tree **************************************** */

static inline void nm_coll_k_nomial_init(const int K, struct nm_coll_tree_info_s*p_tree)
{
  p_tree->max_arity = K - 1;
  p_tree->height = nm_coll_log_n_ceil(p_tree->n, K);
}

static inline int nm_coll_k_nomial_child(const int K, int i, int n, int level)
{
  const int skip = nm_coll_ipow(K, level);
  if((i % (K * skip) == 0) && (i + skip < n))
    return (i + skip);
  else
    return -1;
}

static inline void nm_coll_k_nomial_step(const int K, const struct nm_coll_tree_info_s*p_tree, int step, int*p_parent, int*p_children, int*n_children)
{
  const int level = p_tree->height - step - 1;
  const int i = nm_coll_r2v(p_tree->self, p_tree);
  const int skip = nm_coll_ipow(K, level);
  /* parent */
  *p_parent = -1;
  int j;
  for(j = 0; j < K - 1; j++)
    {
      if(i % (K * skip) == skip + j * skip)
        {
          *p_parent = nm_coll_v2r(i - skip - j * skip, p_tree);
        }
    }
  /* children */
  if((i % (K * skip) == 0) && (i + skip < p_tree->n))
    {
      int c = 0;
      for(j = 0; j < K - 1; j++)
        {
          if(i + skip + skip * j < p_tree->n)
            {
              p_children[c] = nm_coll_v2r(i + skip + skip * j, p_tree);
              c++;
            }
        }
      for(j = c; j < p_tree->max_arity; j++)
        {
          p_children[j] = -1;
        }
      *n_children = c;
    }
  else
    {
      *n_children = 0;
      for(j = 0; j < p_tree->max_arity; j++)
        {
          p_children[j] = -1;
        }
    }
}

/* ** 4-nomial tree **************************************** */

static inline void nm_coll_4_nomial_init(struct nm_coll_tree_info_s*p_tree)
{
  nm_coll_k_nomial_init(4, p_tree);
}

static inline int nm_coll_4_nomial_child(int i, int n, int level)
{
  return nm_coll_k_nomial_child(4, i, n, level);
}

static inline void nm_coll_4_nomial_step(const struct nm_coll_tree_info_s*p_tree, int step, int*p_parent, int*p_children, int*n_children)
{
  nm_coll_k_nomial_step(4, p_tree, step, p_parent, p_children, n_children);
}

/* ** 3-nomial tree **************************************** */

static inline void nm_coll_3_nomial_init(struct nm_coll_tree_info_s*p_tree)
{
  nm_coll_k_nomial_init(3, p_tree);
}

static inline int nm_coll_3_nomial_child(int i, int n, int level)
{
  return nm_coll_k_nomial_child(3, i, n, level);
}

static inline void nm_coll_3_nomial_step(const struct nm_coll_tree_info_s*p_tree, int step, int*p_parent, int*p_children, int*n_children)
{
  nm_coll_k_nomial_step(3, p_tree, step, p_parent, p_children, n_children);
}

/* ** 8-nomial tree **************************************** */

static inline void nm_coll_8_nomial_init(struct nm_coll_tree_info_s*p_tree)
{
  nm_coll_k_nomial_init(8, p_tree);
}

static inline int nm_coll_8_nomial_child(int i, int n, int level)
{
  return nm_coll_k_nomial_child(8, i, n, level);
}

static inline void nm_coll_8_nomial_step(const struct nm_coll_tree_info_s*p_tree, int step, int*p_parent, int*p_children, int*n_children)
{
  nm_coll_k_nomial_step(8, p_tree, step, p_parent, p_children, n_children);
}

/* ** flat tree ******************************************** */

static inline void nm_coll_flat_init(struct nm_coll_tree_info_s*p_tree)
{
  p_tree->max_arity = (p_tree->self == p_tree->root) ? p_tree->n - 1 : 0;
  p_tree->height = 1;
}

static inline int nm_coll_flat_weight(const struct nm_coll_tree_info_s*p_tree, int step, int self)
{
  assert(self >= 0 && self < p_tree->n);
  if(step == 0)
    {
      if(self == p_tree->root)
        {
          return p_tree->n - 1;
        }
      else
        {
          return 1;
        }
    }
  else
    {
      return -1;
    }
}

static inline void nm_coll_flat_step(const struct nm_coll_tree_info_s*p_tree, int step __attribute__((unused)), int*p_parent, int*p_children, int*n_children)
{
  if(p_tree->self == p_tree->root)
    {
      int j, k;
      for(k = 0, j = 0; k < p_tree->n; k++)
        {
          if(k != p_tree->root)
            {
              p_children[j] = k;
              j++;
            }
        }
      *n_children = p_tree->n - 1;
      *p_parent = -1;
    }
  else
    {
      *n_children = 0;
      *p_parent = p_tree->root;
    }
}

/* ** chain ************************************************ */

static inline void nm_coll_chain_init(struct nm_coll_tree_info_s*p_tree)
{
  p_tree->max_arity = 1;
  p_tree->height = p_tree->n - 1;
}

static inline void nm_coll_chain_step(const struct nm_coll_tree_info_s*p_tree, int step, int*p_parent, int*p_children, int*n_children)
{
  const int vself = nm_coll_r2v(p_tree->self, p_tree);
  if((vself == step) && (step < p_tree->n - 1))
    {
      p_children[0] = nm_coll_v2r(vself + 1, p_tree);
      *n_children = 1;
    }
  else
    {
      p_children[0] = -1;
      *n_children = 0;
    }
  if((vself - 1 == step) && (vself > 0))
    {
      *p_parent = nm_coll_v2r(vself - 1, p_tree);
    }
  else
    {
      *p_parent = -1;
    }
}

/* ** chain-modified *************************************** */

/* HPL ring-modified algorithm.
 */

static inline void nm_coll_chain_modified_init(struct nm_coll_tree_info_s*p_tree)
{
  p_tree->max_arity = 1;
  p_tree->height = p_tree->n - 1;
}

static inline void nm_coll_chain_modified_step(const struct nm_coll_tree_info_s*p_tree, int step, int*p_parent, int*p_children, int*n_children)
{
  const int vself = nm_coll_r2v(p_tree->self, p_tree);
  if(step == 1)
    {
      if(vself == 0)
        {
          p_children[0] = nm_coll_v2r(2, p_tree);
          *n_children = 1;
          *p_parent = -1;
        }
      else if(vself == 2)
        {
          *n_children = 0;
          *p_parent = nm_coll_v2r(0, p_tree);
        }
      else
        {
          *n_children = 0;
          *p_parent = -1;
        }
    }
  else
    {
      if((vself == step) && (step < p_tree->n - 1))
        {
          p_children[0] = nm_coll_v2r(vself + 1, p_tree);
          *n_children = 1;
        }
      else
        {
          p_children[0] = -1;
          *n_children = 0;
        }
      if((vself - 1 == step) && (vself > 0))
        {
          *p_parent = nm_coll_v2r(vself - 1, p_tree);
        }
      else
        {
          *p_parent = -1;
        }
    }
}

/* ** 2-chain-modified ************************************* */

/* HPL 2-rings modified algorithm (but it's really 2-chains, not rings).
 */

static inline void nm_coll_2chains_modified_init(struct nm_coll_tree_info_s*p_tree)
{
  p_tree->max_arity = 1;
  if(p_tree->n == 1)
    p_tree->height = 0;
  else if(p_tree->n == 2)
    p_tree->height = 1;
  else
    p_tree->height = 1 + p_tree->n / 2;
}

static inline void nm_coll_2chains_modified_step(const struct nm_coll_tree_info_s*p_tree, int step, int*p_parent, int*p_children, int*n_children)
{
  const int vself = nm_coll_r2v(p_tree->self, p_tree);
  const int vpivot = 1 + p_tree->n / 2;
  if(step == 0)
    {
      /* 0 sends to 1 */
      nm_coll_tree_rule_send(p_tree, p_parent, p_children, n_children, vself, 0, 1);
    }
  else if(step == 1)
    {
      /* 0 sends to pivot (root of 2nd chain) */
      nm_coll_tree_rule_send(p_tree, p_parent, p_children, n_children, vself, 0, vpivot);
    }
  else if(vself < vpivot)
    {
      /* chains rooted at 0 */
      if(step == 2)
        {
          nm_coll_tree_rule_send(p_tree, p_parent, p_children, n_children, vself, 0, 2);
        }
      else
        {
          nm_coll_tree_rule_send(p_tree, p_parent, p_children, n_children, vself, step - 1, step);
        }
    }
  else if(vpivot + step - 1 < p_tree->n)
    {
      /* chains rooted at pivot */
      assert(step >= 2);
      nm_coll_tree_rule_send(p_tree, p_parent, p_children, n_children, vself, vpivot + step - 2, vpivot + step - 1);
    }
  else
    {
      *n_children = 0;
      *p_parent = -1;
    }
}


/* ** ladder ************************************************ */
/* Like the flat tree, but there is only one child per step. */

static inline void nm_coll_ladder_init(struct nm_coll_tree_info_s*p_tree)
{
  p_tree->max_arity = (p_tree->self == p_tree->root) ? 1 : 0;
  p_tree->height = p_tree->n - 1;
}

static inline void nm_coll_ladder_step(const struct nm_coll_tree_info_s*p_tree, int step __attribute__((unused)), int*p_parent, int*p_children, int*n_children)
{
  if((p_tree->self == p_tree->root) && (step < p_tree->n - 1))
    {
      p_children[0] = nm_coll_v2r(step + 1, p_tree);
      *n_children = 1;
    }
  else
    {
      /* do not write p_children[0] since max_arity = 0 here */
      *n_children = 0;
    }

  if((p_tree->self != p_tree->root) && (p_tree->self == nm_coll_v2r(step + 1, p_tree)))
    {
      *p_parent = p_tree->root;
    }
  else
    {
      *p_parent = -1;
    }
}

/* ** binary tree ****************************************** */

static inline int int_min(const int a, const int b)
{
  return (a <= b) ? a : b;
}

int nm_coll_binary_nb_nodes_in_left_subtree(const struct nm_coll_tree_info_s* const p_tree)
{
  int height = p_tree->height + 1; // we need that the height of a tree with only a root equals 1

  // Number of nodes, including root node, in a full binary tree with height height-1:
  int nb_nodes_tree_previous_height = (1 << (height-1)) - 1;

  // Maximum theoretical number of nodes (here, leafs) in last stage:
  int max_nb_nodes_last_height = 1 << (height-1);

  /*
   * The number of nodes in the left subtree is:
   * * the number of nodes in the full left subtree of height h-1
   * * + the minimum between the real number of nodes in the last stage
   *     and the maximal number of nodes in the last stage of the subtree
   * * (in other words, we need to know if nodes in the last stage overflow to right subtree)
   */
  return (nb_nodes_tree_previous_height >> 1) + int_min(p_tree->n - (1 << (height-1)) + 1, max_nb_nodes_last_height >> 1);
}

static inline void nm_coll_binary_init(struct nm_coll_tree_info_s*p_tree)
{
  p_tree->max_arity = 2;
  p_tree->height = nm_coll_log2_floor(p_tree->n);
}

static inline void nm_coll_binary_step(const struct nm_coll_tree_info_s*p_tree, int step, int*p_parent, int*p_children, int*n_children)
{
  const int vself = nm_coll_r2v(p_tree->self, p_tree);
  const int vparent = (vself - 1) / 2;
  const int vchild1 = 2 * vself + 1;
  const int vchild2 = 2 * vself + 2;
  /* parent */
  if(nm_coll_log2_floor(vself + 1) - 1 == step)
    {
      *p_parent = nm_coll_v2r(vparent, p_tree);
    }
  else
    {
      *p_parent = -1;
    }
  /* children */
  *n_children = 0;
  p_children[0] = -1;
  p_children[1] = -1;
  if(nm_coll_log2_floor(vself + 1) == step)
    {
      if(p_tree->n > vchild1)
        {
          p_children[0] = nm_coll_v2r(vchild1, p_tree);
          (*n_children)++;
        }
      if(p_tree->n > vchild2)
        {
          p_children[1] = nm_coll_v2r(vchild2, p_tree);
          (*n_children)++;
        }
    }
}

/* ** k-ary tree ******************************************* */

static inline void nm_coll_k_ary_init(const int K, struct nm_coll_tree_info_s*p_tree)
{
  p_tree->max_arity = K;
  p_tree->height = nm_coll_log_n_floor((K - 1) * p_tree->n, K);
}

static inline void nm_coll_k_ary_step(const int K, const struct nm_coll_tree_info_s*p_tree, int step, int*p_parent, int*p_children, int*n_children)
{
  const int vself = nm_coll_r2v(p_tree->self, p_tree);
  /* parent */
  if(nm_coll_log_n_floor((K - 1) * (vself + 1), K) - 1 == step)
    {
      *p_parent = nm_coll_v2r((vself - 1) / K, p_tree);
    }
  else
    {
      *p_parent = -1;
    }
  /* children */
  *n_children = 0;
  if(nm_coll_log_n_floor((K - 1) * (vself + 1), K) == step)
    {
      int i;
      for(i = 0; i < K; i++)
        {
          const int vchild = K * vself + 1 + i;
          if(p_tree->n > vchild)
            {
              p_children[i] = nm_coll_v2r(vchild, p_tree);
              (*n_children)++;
            }
          else
            {
              p_children[i] = -1;
            }
        }
    }
}

/* ** 4-ary tree ******************************************* */

static inline void nm_coll_4ary_init(struct nm_coll_tree_info_s*p_tree)
{
  nm_coll_k_ary_init(4, p_tree);
}

static inline void nm_coll_4ary_step(const struct nm_coll_tree_info_s*p_tree, int step, int*p_parent, int*p_children, int*n_children)
{
  nm_coll_k_ary_step(4, p_tree, step, p_parent, p_children, n_children);
}

/* ** generic trees **************************************** */

/** initialize a generic tree descriptor
 */
void nm_coll_tree_init(struct nm_coll_tree_info_s*p_tree, nm_coll_tree_kind_t kind, int n, int self, int root)
{
  p_tree->kind = kind;
  p_tree->root = root;
  p_tree->n = n;
  p_tree->self = self;
  switch(kind)
    {
    case NM_COLL_TREE_FLAT:
      nm_coll_flat_init(p_tree);
      break;
    case NM_COLL_TREE_CHAIN:
      nm_coll_chain_init(p_tree);
      break;
    case NM_COLL_TREE_CHAIN_MODIFIED:
      nm_coll_chain_modified_init(p_tree);
      break;
    case NM_COLL_TREE_2CHAINS_MODIFIED:
      nm_coll_2chains_modified_init(p_tree);
      break;
    case NM_COLL_TREE_LADDER:
      nm_coll_ladder_init(p_tree);
      break;
    case NM_COLL_TREE_BINOMIAL:
      nm_coll_binomial_init(p_tree);
      break;
    case NM_COLL_TREE_3NOMIAL:
      nm_coll_3_nomial_init(p_tree);
      break;
    case NM_COLL_TREE_4NOMIAL:
      nm_coll_4_nomial_init(p_tree);
      break;
    case NM_COLL_TREE_8NOMIAL:
      nm_coll_8_nomial_init(p_tree);
      break;
    case NM_COLL_TREE_BINARY:
      nm_coll_binary_init(p_tree);
      break;
    case NM_COLL_TREE_3ARY:
      nm_coll_k_ary_init(3, p_tree);
      break;
    case NM_COLL_TREE_4ARY:
      nm_coll_4ary_init(p_tree);
      break;
    case NM_COLL_TREE_8ARY:
      nm_coll_k_ary_init(8, p_tree);
      break;
    default:
      NM_FATAL("unexpected kind of tree %d.\n", kind);
      break;
    }
}

void nm_coll_tree_step(const struct nm_coll_tree_info_s*p_tree, int step, int*p_parent, int*p_children, int*n_children)
{
  switch(p_tree->kind)
    {
    case NM_COLL_TREE_FLAT:
      nm_coll_flat_step(p_tree, step, p_parent, p_children, n_children);
      break;
    case NM_COLL_TREE_CHAIN:
      nm_coll_chain_step(p_tree, step, p_parent, p_children, n_children);
      break;
    case NM_COLL_TREE_CHAIN_MODIFIED:
      nm_coll_chain_modified_step(p_tree, step, p_parent, p_children, n_children);
      break;
    case NM_COLL_TREE_2CHAINS_MODIFIED:
      nm_coll_2chains_modified_step(p_tree, step, p_parent, p_children, n_children);
      break;
    case NM_COLL_TREE_LADDER:
      nm_coll_ladder_step(p_tree, step, p_parent, p_children, n_children);
      break;
    case NM_COLL_TREE_BINOMIAL:
      nm_coll_binomial_step(p_tree, step, p_parent, p_children, n_children);
      break;
    case NM_COLL_TREE_3NOMIAL:
      nm_coll_3_nomial_step(p_tree, step, p_parent, p_children, n_children);
      break;
    case NM_COLL_TREE_4NOMIAL:
      nm_coll_4_nomial_step(p_tree, step, p_parent, p_children, n_children);
      break;
    case NM_COLL_TREE_8NOMIAL:
      nm_coll_8_nomial_step(p_tree, step, p_parent, p_children, n_children);
      break;
    case NM_COLL_TREE_BINARY:
      nm_coll_binary_step(p_tree, step, p_parent, p_children, n_children);
      break;
    case NM_COLL_TREE_3ARY:
      nm_coll_k_ary_step(3, p_tree, step, p_parent, p_children, n_children);
      break;
    case NM_COLL_TREE_4ARY:
      nm_coll_4ary_step(p_tree, step, p_parent, p_children, n_children);
      break;
    case NM_COLL_TREE_8ARY:
      nm_coll_k_ary_step(8, p_tree, step, p_parent, p_children, n_children);
      break;
    default:
      NM_FATAL("unexpected kind of tree %d.\n", p_tree->kind);
      break;
    }
}

int nm_coll_tree_weight(const struct nm_coll_tree_info_s*p_tree, int step, int self)
{
  switch(p_tree->kind)
    {
    case NM_COLL_TREE_FLAT:
      return nm_coll_flat_weight(p_tree, step, self);
      break;
    case NM_COLL_TREE_BINOMIAL:
      {
        const int vself = nm_coll_r2v(self, p_tree);
        return nm_coll_binomial_weight(vself, p_tree->n, p_tree->height - step - 1);
      }
      break;
    default:
      NM_FATAL("unexpected kind of tree %d.\n", p_tree->kind);
      break;
    }
 return -1;
}

nm_coll_tree_kind_t nm_coll_tree_kind_by_name(const char*s_kind)
{
  if((s_kind == NULL) || (strcmp("", s_kind) == 0) || (strcmp("default", s_kind) == 0))
    {
      return NM_COLL_TREE_DEFAULT;
    }
  else if(strcmp("flat", s_kind) == 0)
    {
      return NM_COLL_TREE_FLAT;
    }
  else if(strcmp("chain", s_kind) == 0)
    {
      return NM_COLL_TREE_CHAIN;
    }
  else if(strcmp("chain-modified", s_kind) == 0)
    {
      return NM_COLL_TREE_CHAIN_MODIFIED;
    }
  else if(strcmp("2chains-modified", s_kind) == 0)
    {
      return NM_COLL_TREE_2CHAINS_MODIFIED;
    }
  else if(strcmp("ladder", s_kind) == 0)
    {
      return NM_COLL_TREE_LADDER;
    }
  else if(strcmp("binomial", s_kind) == 0)
    {
      return NM_COLL_TREE_BINOMIAL;
    }
  else if((strcmp("3nomial", s_kind) == 0) || (strcmp("trinomial", s_kind) == 0))
    {
      return NM_COLL_TREE_3NOMIAL;
    }
  else if((strcmp("4nomial", s_kind) == 0) || (strcmp("quadrinomial", s_kind) == 0))
    {
      return NM_COLL_TREE_4NOMIAL;
    }
  else if(strcmp("8nomial", s_kind) == 0)
    {
      return NM_COLL_TREE_8NOMIAL;
    }
  else if(strcmp("binary", s_kind) == 0)
    {
      return NM_COLL_TREE_BINARY;
    }
  else if(strcmp("3ary", s_kind) == 0)
    {
      return NM_COLL_TREE_3ARY;
    }
  else if(strcmp("4ary", s_kind) == 0)
    {
      return NM_COLL_TREE_4ARY;
    }
  else if(strcmp("8ary", s_kind) == 0)
    {
      return NM_COLL_TREE_8ARY;
    }
  else
    {
      enum nm_coll_tree_kind_e k = NM_COLL_TREE_NONE;
      k++;
      padico_string_t s = padico_string_new();
      while(k < _NM_COLL_TREE_MAX)
        {
          padico_string_catf(s, "%s ", nm_coll_tree_kind_name(k));
          k++;
        }
      NM_FATAL("cannot parse tree kind '%s'; available tree kinds: %s\n", s_kind, padico_string_get(s));
    }
}

nm_coll_tree_kind_t nm_coll_tree_env(void)
{
  static enum nm_coll_tree_kind_e default_kind = NM_COLL_TREE_NONE;
  if(default_kind == NM_COLL_TREE_NONE)
    {
      const char*s_kind = padico_module_attr_tree_kind_getvalue();
      default_kind = nm_coll_tree_kind_by_name(s_kind);
      if(default_kind != NM_COLL_TREE_DEFAULT)
        {
          NM_DISPF("tree kind = '%s' forced by environment.\n", s_kind);
        }
    }
  return default_kind;
}

nm_coll_tree_kind_t nm_coll_tree_heuristic(int comm_size __attribute__((unused)), nm_len_t data_size)
{
  enum nm_coll_tree_kind_e default_kind = nm_coll_tree_env();
  if(default_kind == NM_COLL_TREE_DEFAULT)
    {
      if(data_size > 32 * 1024)
        {
          return NM_COLL_TREE_BINOMIAL;
        }
      else
        {
          return NM_COLL_TREE_4NOMIAL;
        }
    }
  else
    {
      return default_kind;
    }
}

/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Control using ssh2 protocol
 * @author Sebastien Barascou
 */
#include <Padico/Puk.h>
#include <Padico/Module.h>
#include <Padico/PadicoTM.h>
#include <Padico/SysIO.h>
#include <Padico/PadicoControl.h>
#include <Padico/SocketFactory.h>

#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <poll.h>
#include <unistd.h>

#ifdef HAVE_LIBSSH2_H
#include <libssh2.h>
#endif

static int control_ssh2_init(void);
static void control_ssh2_finalize(void);

PADICO_MODULE_DECLARE(Control_SSH2, control_ssh2_init, NULL, control_ssh2_finalize);

#define OPENSSH_MIN_VERSION 5.4

/* *** PadicoControl driver ******************************** */

static void ssh2_control_send_common(void*_status, padico_topo_node_t node, padico_control_msg_t msg);

static const struct padico_control_driver_s ssh2_control_driver =
  {
    .send_common = &ssh2_control_send_common
  };


/* *** PadicoBootstrap driver ******************************** */

static int ssh2_control_bootstrap_connect(void*_status, const char*hostname, padico_topo_uuid_t uuid);

static const struct padico_bootstrap_driver_s ssh2_bootstrap_driver =
  {
    .bootstrap_new     = NULL,
    .bootstrap_connect = &ssh2_control_bootstrap_connect
  };

/* *** PadicoComponent driver ******************************** */

static void*ssh2_control_instantiate(puk_instance_t, puk_context_t);
static void ssh2_control_destroy(void*);

static const struct puk_component_driver_s ssh2_control_component_driver =
  {
    .instantiate = &ssh2_control_instantiate,
    .destroy     = &ssh2_control_destroy
  };

/* ********************************************************* */
enum ssh2_control_state_e
  {
    SSH2_CONTROL_STATE_INIT = 0,    /**< connection is initializing */
    SSH2_CONTROL_STATE_CONNECTED,   /**< connection is established */
    SSH2_CONTROL_STATE_DISCONNECTED /**< connection has been disconnected */
  };

struct ssh2_openssh_s
{
  pid_t pid;
  float version;
};

struct ssh2_control_status_s
{
  puk_instance_t instance;        /**< self as an instance (used for interactions with padico_control_*) */
  puk_context_t context;
  marcel_mutex_t lock;
  marcel_cond_t connection_event; /**< condition signaled upon connection event */
  enum ssh2_control_state_e state;  /**< whether the channel is connected */
  padico_topo_node_t peer;          /**< peer node */
  padico_io_t in_io;              /**< IO handler (used for receipt only) */
  padico_req_t req;               /**< request object used for asynchronous connection */
  int ssh_server_fd;
  const char *ssh_host;
  const char *ssh_user;
  const char *ssh_cipher;
  const char *ssh_dest;
  const char*ssh_path;
  struct ssh2_openssh_s  openssh;
#ifdef HAVE_LIBSSH2_H
  LIBSSH2_SESSION *session;
  LIBSSH2_CHANNEL *channel;
#endif /* HAVE_LIBSSH2_H */
};

/* ********************************************************* */
/* ***  OpenSSH specific functions */

static float ssh2_openssh_version(const char*ssh_path)
{
  float version = 0;
  int fd[2];
  PUK_ABI_FSYS_REAL(pipe)(fd);
  pid_t child = fork();
  if(!child)
    {
      PUK_ABI_FSYS_REAL(close)(fd[0]);
      PUK_ABI_FSYS_REAL(dup2)(fd[1], STDERR_FILENO);
      unsetenv("LD_PRELOAD");
      padico_cleanenv();
      const char *argv[] =
        {
          "ssh",
          "-V",
          NULL
        };
      puk_spinlock_acquire();
      PUK_ABI_PROC_REAL(execv)(ssh_path, (char**)argv);
      puk_spinlock_release();
      PUK_ABI_FSYS_REAL(close)(fd[1]);
      exit(1);
    }
  else if(child > 0)
    {
      PUK_ABI_FSYS_REAL(close)(fd[1]);
      char buf[512];
      memset(buf, 0, 512);
      PUK_ABI_FSYS_REAL(read)(fd[0], buf, 512);
      sscanf(buf, "OpenSSH_%f*s", &version);
      padico_out(40, "OpenSSH version on localhost: %g\n", version);
      PUK_ABI_FSYS_REAL(close)(fd[0]);
    }
  else
    {
      version = 0;
    }
  return version;
}

static void ssh2_openssh_config(struct ssh2_control_status_s*status, const char*filename,
                           char**nodes, char**users, int n)
{
  FILE*config_file = fopen(filename, "w");
  fprintf(config_file,
          "Host *\n"
          "StrictHostKeyChecking no\n"
          "ForwardAgent yes\n"
          "Ciphers %s\n\n", status->ssh_cipher);
  int i=0;
  fprintf(config_file,
          "Host %s\n"
          "User %s\n\n",
          nodes[i], users[i]);
  for(i=1; i < n; i++)
    {
      fprintf(config_file,
              "Host %s\n"
              "User %s\n"
              "ProxyCommand ssh -x -e none -F %s -W %%h:%%p %s\n\n",
              nodes[i], users[i], filename, nodes[i-1]);
    }
  fclose(config_file);
}

static int ssh2_openssh_init_ssh_connection(struct ssh2_control_status_s*status,
                                            char**nodes, char**users, int n, char*destination)
{
  int sv[2] = { -1, -1};
  int keepalive = 1;
  padico_string_t ssh_config = padico_string_new();
  int rc = 0;
  int err = 0;
  padico_string_catf(ssh_config, "/tmp/padico_ssh_%s", padico_topo_node_getuuid(padico_topo_getlocalnode()));
  ssh2_openssh_config(status, padico_string_get(ssh_config), nodes, users, n);
  rc  = PUK_ABI_FSYS_REAL(socketpair)(AF_LOCAL, SOCK_STREAM, 0, sv);
  err = __puk_abi_wrap__errno;
  if(rc)
    return -err;
  rc  = PUK_ABI_FSYS_REAL(setsockopt)(sv[0], SOL_SOCKET, SO_KEEPALIVE, &keepalive, sizeof(keepalive));
  err = __puk_abi_wrap__errno;
  if(rc)
    return -err;
  rc  = PUK_ABI_FSYS_REAL(setsockopt)(sv[1], SOL_SOCKET, SO_KEEPALIVE, &keepalive, sizeof(keepalive));
  err = __puk_abi_wrap__errno;
  if(rc)
    return -err;
  puk_spinlock_acquire();
  status->openssh.pid = fork();
  puk_spinlock_release();
  if(status->openssh.pid > 0)
    {
      rc = PUK_ABI_FSYS_REAL(close)(sv[1]);
      status->ssh_server_fd = sv[0];
    }
  else if(status->openssh.pid == 0)
    {
      rc = PUK_ABI_FSYS_REAL(close)(sv[0]);
      if(rc)
        goto ossh_error;
      rc = PUK_ABI_FSYS_REAL(dup2)(sv[1], 0);
      if(rc == -1)
        goto ossh_error;
      rc = PUK_ABI_FSYS_REAL(dup2)(sv[1], 1);
      if(rc == -1)
        goto ossh_error;
      rc = PUK_ABI_FSYS_REAL(close)(sv[1]);
      if(rc)
        goto ossh_error;
      unsetenv("LD_PRELOAD");
      padico_cleanenv();
      const char *argv[] =
        {
          "ssh",
          "-W",
          destination,
          "-F",
          padico_string_get(ssh_config),
          "-x",
          "-e",
          "none",
          nodes[n-1],
          NULL
        };
      padico_print("ssh tunnel to %s through %s\n", destination, nodes[n-1]);
      puk_spinlock_acquire();
      rc  = PUK_ABI_PROC_REAL(execv)(status->ssh_path, (char**)argv);
      puk_spinlock_release();
    ossh_error:
      rc  = -__puk_abi_wrap__errno;
      return rc;
    }
  else
    {
       err = __puk_abi_wrap__errno;
       return err;
    }
  padico_string_delete(ssh_config);
  return 0;
}

/* ********************************************************* */
/* ***  LibSSH2 specific functions */

#ifdef HAVE_LIBSSH2_H

static int ssh2_libssh2_connect_server(struct ssh2_control_status_s*status)
{
  struct addrinfo *res;
  if(getaddrinfo(status->ssh_host, "ssh", NULL, &res))
    {
      padico_warning("Cannot resolve ssh server: %s\n", status->ssh_host);
      return -ENETUNREACH;
    }
  int fd = padico_sysio_connected_inet_socket_sockaddr(res[0].ai_addr);
  return fd;
}

static int ssh2_libssh2_authenticate(struct ssh2_control_status_s*status)
{
  struct libssh2_agent_publickey *prev_identity = NULL, *identity = NULL;
  LIBSSH2_AGENT *agent = NULL;
  int rc = -1;
  const char *userauthlist = libssh2_userauth_list(status->session, status->ssh_user, strlen(status->ssh_user));
  if (strstr(userauthlist, "publickey") == NULL)
    {
      padico_warning("ssh server %s don't support \"publickey\" authentication\n", status->ssh_host);
      return -1;
    }
  agent = libssh2_agent_init(status->session);
  if (!agent)
    {
      padico_warning("Failure initializing ssh-agent support\n");
      goto noagent;
    }
  if (libssh2_agent_connect(agent))
    {
      padico_warning("Failure connecting to ssh-agent\n");
      goto noagent;
    }
  if (libssh2_agent_list_identities(agent))
    {
      padico_warning("Failure requesting identities to ssh-agent\n");
      goto noagent;
    }
  while (1)
    {
      rc = libssh2_agent_get_identity(agent, &identity, prev_identity);
      if (rc == 1)
        break;
      if (rc < 0)
        {
          padico_warning("Failure obtaining identity from ssh-agent support\n");
          return -1;
        }
      if ((rc = libssh2_agent_userauth(agent, status->ssh_user, identity)) == 0)
        {
          break;
        }
      prev_identity = identity;
    }
 noagent:
  if (rc)
    {
      padico_warning("Couldn't find a good key in the ssh-agent identity collection\n");
      const char*home = padico_getenv("HOME");
      padico_string_t priv = padico_string_new();
      padico_string_catf(priv, "%s/.ssh/id_rsa", home);
      padico_string_t pub = padico_string_new();
      padico_string_catf(pub, "%s/.ssh/id_rsa.pub", home);
      if(access(padico_string_get(pub), F_OK) == 0)
        {
          padico_out(40,"Try %s without passphrase\n", padico_string_get(priv));
          rc = libssh2_userauth_publickey_fromfile(status->session, status->ssh_user,
                                                   padico_string_get(pub),
                                                   padico_string_get(priv), "");
        }
    }
  if(agent)
    {
      libssh2_agent_disconnect(agent);
      libssh2_agent_free(agent);
    }
  return rc;
}

static int ssh2_libssh2_init_ssh_connection(struct ssh2_control_status_s*_status)
{
  struct ssh2_control_status_s*status = _status;
  if(!status->session)
    {
      status->ssh_server_fd = ssh2_libssh2_connect_server(status);
      if(status->ssh_server_fd < 0)
        {
          return -ENETUNREACH;
        }
      status->session = libssh2_session_init();
      if(status->session)
        {
          libssh2_session_method_pref(status->session, LIBSSH2_METHOD_CRYPT_CS, status->ssh_cipher);
          if(libssh2_session_handshake(status->session, status->ssh_server_fd))
            {
              padico_warning("Start or init libssh failed\n");
              padico_sysio_close(status->ssh_server_fd);
              status->ssh_server_fd = -1;
              libssh2_session_free(status->session);
              status->session = NULL;
              return -ECONNREFUSED;
            }
          if(ssh2_libssh2_authenticate(status))
            {
              padico_sysio_close(status->ssh_server_fd);
              status->ssh_server_fd = -1;
              libssh2_session_disconnect(status->session, "Authentication fail\n");
              libssh2_session_free(status->session);
              status->session = NULL;
              return -ECONNREFUSED;
            }
        }
      else
        return -ENETUNREACH;
    }
  return 0;
}

#endif /* HAVE_LIBSSH2_H */

/* ********************************************************* */
/* *** SSH2 helper function */

static ssize_t ssh2_control_in(struct ssh2_control_status_s*status, void* buf, size_t count)
{
  ssize_t done = 0;
#ifdef HAVE_LIBSSH2_H
  if(status->openssh.pid == 0 && status->session)
    {
      if(count > 0)
        {
          while(done < count)
            {
              int rc = libssh2_channel_read(status->channel, buf + done, count - done);
              if(rc > 0)
                done += rc;
              else if(rc == 0)
                return done;
              else if(rc == LIBSSH2_ERROR_EAGAIN)
                continue;
              else
                return rc;
            }
        }
    }
  else
    {
#endif /* HAVE_LIBSSH2_H */
      done = padico_sysio_in(status->ssh_server_fd, buf, count);
#ifdef HAVE_LIBSSH2_H
    }
#endif /* HAVE_LIBSSH2_H */
  return done;
}

static ssize_t ssh2_control_out(struct ssh2_control_status_s*status, const void* buf, size_t count)
{
  ssize_t done = 0;
#ifdef HAVE_LIBSSH2_H
  if(status->openssh.pid == 0 && status->session)
    {
      padico_out(40,"%s\n",(char*)buf);
      if(count > 0)
        {
          while(done < count)
            {
              int rc = libssh2_channel_write(status->channel, buf + done, count - done);
              if(rc > 0)
                done += rc;
              else if(rc == 0)
                {
                  return done;
                }
              else if(rc == LIBSSH2_ERROR_EAGAIN)
                continue;
              else
                return rc;
            }
        }
    }
  else
    {
#endif /* HAVE_LIBSSH2_H */
      done = padico_sysio_out(status->ssh_server_fd, buf, count);
#ifdef HAVE_LIBSSH2_H
    }
#endif /* HAVE_LIBSSH2_H */
  return done;
}

static ssize_t ssh2_control_outv(struct ssh2_control_status_s*status, const struct iovec *iov, int iovcnt)
{
  ssize_t done = 0;
#ifdef HAVE_LIBSSH2_H
  if(status->openssh.pid == 0 && status->session)
    {
      int i = 0;
      for(i = 0; i < iovcnt; i++)
        {
          int rc = ssh2_control_out(status, iov[i].iov_base, iov[i].iov_len);
          if(rc > 0)
            {
              assert(rc == iov[i].iov_len);
              done += rc;
            }
          else if(rc == 0)
            return done;
          else
            return rc;
        }
    }
  else
    {
#endif /* HAVE_LIBSSH2_H */
      done = padico_sysio_outv(status->ssh_server_fd, iov, iovcnt);
#ifdef HAVE_LIBSSH2_H
    }
#endif /* HAVE_LIBSSH2_H */
  return done;
}

/* ********************************************************* */
/* *** PadicoControl */
static void ssh2_control_connector_notify(struct ssh2_control_status_s*status, padico_rc_t rc);

static void ssh2_control_connection_lost(struct ssh2_control_status_s*status)
{
  status->state = SSH2_CONTROL_STATE_DISCONNECTED;
  padico_control_delete_node(status->instance, status->peer);
  ssh2_control_connector_notify(status, padico_rc_error("Control_SSH2: connection lost"));
}

static void ssh2_control_send_common(void*_status, padico_topo_node_t node, padico_control_msg_t msg)
{
  struct ssh2_control_status_s*status = _status;
  const struct iovec*v = padico_control_msg_get_iovec(msg);
  const int iovcnt = padico_control_msg_get_iovec_size(msg);
  const size_t size = padico_control_msg_get_size(msg);
  struct iovec*v2 = padico_malloc(sizeof(struct iovec) * (iovcnt + 1));
  int i;
  for(i=0; i<iovcnt; i++)
    {
      v2[i+1].iov_base = v[i].iov_base;
      v2[i+1].iov_len = v[i].iov_len;
    }
  marcel_mutex_lock(&status->lock);
  while(status->state == SSH2_CONTROL_STATE_INIT)
    {
      padico_out(80, "wait for the establishment of the control channel.\n");
      marcel_cond_wait(&status->connection_event, &status->lock);
    }
  assert(status->ssh_server_fd > 0);
  assert(node == status->peer);
  uint32_t n_size = htonl(size);
  v2[0].iov_base = &n_size;
  v2[0].iov_len = sizeof(n_size);
  int sysrc = ssh2_control_outv(status, v2, iovcnt + 1);
  marcel_mutex_unlock(&status->lock);
  if(sysrc < 0)
    {
      ssh2_control_connection_lost(status);
    }
  padico_free(v2);
  assert(sysrc == size + sizeof(n_size));
  padico_out(30, "send ok.\n");
}


static int ssh2_control_read_callback(int fd, void*key)
{
  int again = 1;
  struct ssh2_control_status_s*status = (struct ssh2_control_status_s*)key;
  size_t size;
  char*bytes;
  marcel_mutex_lock(&status->lock);
#ifdef HAVE_LIBSSH2_H
  if(status->openssh.pid == 0 && status->session)
    {
      libssh2_channel_read(status->channel, NULL, 0);
      while(libssh2_poll_channel_read(status->channel, 0))
        {
          uint32_t n_size = 0;
          assert(status->ssh_server_fd == fd);
          padico_out(40, "entering...\n");
          int sysrc = ssh2_control_in(status, &n_size, sizeof(uint32_t));
          if(sysrc <= 0)
            {
              again = 0;
              ssh2_control_connection_lost(status);
              goto exit;
            }
          size = ntohl(n_size);
          bytes = NULL;
          if(size > 0)
            {
              bytes = padico_malloc(size);
              sysrc = ssh2_control_in(status, bytes, size);
              if(sysrc < size)
                {
                  again = 0;
                  ssh2_control_connection_lost(status);
                  goto exit;
                }
            }
          padico_out(40, "received message- len=%ld; msg=*%s*\n", puk_ulong(size), bytes);
          padico_control_deliver_message(status->instance, status->peer, bytes, size);
        }
    exit:
      if(libssh2_channel_eof(status->channel))
        {
          again = 0;
          ssh2_control_connection_lost(status);
        }
    }
  else
    {
#endif /* HAVE_LIBSSH2_H */
      assert(status->ssh_server_fd == fd);
      padico_out(40, "entering...\n");
      uint32_t n_size = 0;
      int sysrc = padico_sysio_in(fd, &n_size, sizeof(uint32_t));
      if(sysrc <= 0)
        {
          again = 0;
          ssh2_control_connection_lost(status);
          return again;
        }
      size = ntohl(n_size);
      bytes = NULL;
      if(size > 0)
        {
          bytes = padico_malloc(size);
          sysrc = padico_sysio_in(fd, bytes, size);
          if(sysrc < size)
            {
              again = 0;
              ssh2_control_connection_lost(status);
              return again;
            }
        }
      padico_out(40, "received message- len=%ld; msg=*%s*\n", puk_ulong(size), bytes);
      padico_control_deliver_message(status->instance, status->peer, bytes, size);
#ifdef HAVE_LIBSSH2_H
    }
#endif /* HAVE_LIBSSH2_H */
  marcel_mutex_unlock(&status->lock);
  return again;
}

/* ********************************************************* */
/* *** PadicoConnector */
static void ssh2_control_connector_notify(struct ssh2_control_status_s*status, padico_rc_t rc)
{
  if(status->req)
    {
      padico_tm_req_notify(status->req->id, rc);
      status->req = NULL;
    }
  else padico_rc_delete(rc);
}

static void*ssh2_control_start(void*_status)
{
  struct ssh2_control_status_s*status = _status;
  /* announce local node to peer node */
  padico_string_t self_node = padico_topo_node_serialize(padico_topo_getlocalnode());
  const char*msg = padico_string_get(self_node);
  marcel_mutex_lock(&status->lock);
  assert(status->ssh_server_fd > 0);
  assert(status->state == SSH2_CONTROL_STATE_INIT);
  uint32_t size = strlen(msg) + 1;
  int sysrc = ssh2_control_out(status, &size, sizeof(size));
  if(sysrc > 0)
    {
      sysrc = ssh2_control_out(status, msg, size);
    }
  if(sysrc < 0)
    {
      ssh2_control_connection_lost(status);
    }
  marcel_mutex_unlock(&status->lock);
  msg = NULL;
  padico_string_delete(self_node);
  /* receive peer announce */
  sysrc = ssh2_control_in(status, &size, sizeof(size));
  assert(sysrc == sizeof(size));
  if(sysrc <= 0)
    {
      ssh2_control_connection_lost(status);
      return NULL;
    }
  char*recv_msg = padico_malloc(size);
  sysrc = ssh2_control_in(status, recv_msg, size);
  if(sysrc < size)
    {
      ssh2_control_connection_lost(status);
      return NULL;
    }
  /* parse peer announce */
  padico_control_event_disable();
  struct puk_parse_entity_s e = puk_xml_parse_buffer(recv_msg, size - 1, PUK_TRUST_CLUSTER);
  padico_rc_t rc = puk_parse_get_rc(&e);
  if(padico_rc_iserror(rc))
    {
      padico_rc_show(rc);
      padico_fatal("parse error on initial handshake message.");
    }
  if(!puk_parse_is(&e, "Topology:node"))
    {
      padico_fatal("root entity is not \"Topology:node\" while initializing control channel.\n");
    }
  padico_topo_node_t peer = puk_parse_get_content(&e);
  assert(peer != NULL);
  assert(status->peer == NULL);
  status->peer = peer;
  padico_control_new_node(status->instance, peer);
  padico_control_event_enable();

  /* authorize to send */
  marcel_mutex_lock(&status->lock);
  status->state = SSH2_CONTROL_STATE_CONNECTED;
  status->in_io = padico_io_register(status->ssh_server_fd, PADICO_IO_EVENT_READ,
                                     &ssh2_control_read_callback, status);
  marcel_cond_broadcast(&status->connection_event);
  marcel_mutex_unlock(&status->lock);
  /* activate polling */
  padico_io_activate(status->in_io);

  /* notify connection establishment */
  padico_out(20, "the connection to node %s is up\n", padico_topo_node_getname(peer));
  ssh2_control_connector_notify(status, padico_rc_ok());
  padico_free(recv_msg);

  padico_control_event_disable();
  padico_control_event_flush();
  padico_control_event_enable();

  return NULL;
}


/* ********************************************************* */
/* *** PadicoBootstrap */

/** get a bootstrap port from an UUID */
static uint16_t ssh2_control_bootstrap_auto_port(padico_topo_node_t node)
{
  const unsigned int port_base  = 20000;
  const unsigned int port_end   = 32000;
  const unsigned int port_range = port_end - port_base;
  const char*session_id = padico_topo_node_getsession(padico_topo_getlocalnode());
  const unsigned int port_offset = puk_hash_default((void*)session_id, strlen(session_id));
  const uint16_t port = port_base + (port_offset % port_range);
  return port;
}

static int ssh2_control_bootstrap_connect(void*_status, const char*hostname, padico_topo_uuid_t uuid)
{
  struct ssh2_control_status_s*status = (struct ssh2_control_status_s*) _status;

  const uint16_t port = htons(ssh2_control_bootstrap_auto_port(NULL));
  const padico_topo_host_t host = padico_topo_host_resolvebyname(hostname);
  const struct in_addr*ip = padico_topo_host_getaddr(host);
  if(!ip)
    {
      padico_fatal("cannot find host '%s': host has no IP address.\n", hostname);
    }
  int n = 0, i = 0;
  int rc = -1;
  char*ssh_host = padico_strdup(status->ssh_host);
  char*ssh_user = padico_strdup(status->ssh_user);
  char**nodes = padico_malloc(sizeof(char*));
  char**users = padico_malloc(sizeof(char*));
  char*save_node = NULL, *save_user = NULL;
  char*node_token = strtok_r((char*)ssh_host, ",", &save_node);
  char*user_token = strtok_r((char*)ssh_user, ",", &save_user);
  while(node_token != NULL)
    {
      nodes[n] = node_token;
      users[n] = user_token;
      n++;
      nodes = padico_realloc(nodes, (n+1)*sizeof(char*));
      users = padico_realloc(users, (n+1)*sizeof(char*));
      node_token = strtok_r(NULL, ",", &save_node);
      user_token = strtok_r(NULL, ",", &save_user);
    }

  const char * remote_desthost;
  if(status->ssh_dest)
    remote_desthost = status->ssh_dest;
  else
    remote_desthost = padico_strdup(inet_ntoa(*ip));
  unsigned int remote_destport = ntohs(port);
#ifdef HAVE_LIBSSH2_H
  if(n == 1)
    {
      rc = ssh2_libssh2_init_ssh_connection(status);
      if(rc == 0)
        {
          padico_out(10,"connecting to %s:%d with ssh server %s@%s\n", remote_desthost, remote_destport, status->ssh_user, status->ssh_host);
          libssh2_session_set_blocking(status->session, 1);
          status->channel = libssh2_channel_direct_tcpip(status->session, remote_desthost, remote_destport);
          if (!status->channel)
            {
              padico_out(30, "Could not open the direct-tcpip channel\n");
              return -ENETUNREACH;
            }
          libssh2_session_set_blocking(status->session, 0);
        }
    }
  else
    {
#endif /* HAVE_LIBSSH2_H */
      if(status->openssh.version >= OPENSSH_MIN_VERSION)
        {
          padico_string_t destination = padico_string_new();
          padico_string_printf(destination, "%s:%d", remote_desthost, remote_destport);
          rc = ssh2_openssh_init_ssh_connection(status, nodes, users, n, padico_string_get(destination));
          padico_string_delete(destination);
        }
      else
        {
          return -ENETUNREACH;
        }
#ifdef HAVE_LIBSSH2_H
    }
#endif /* HAVE_LIBSSH2_H */

  padico_control_plug_controler(status->instance, NULL);
  ssh2_control_start(status);
  marcel_mutex_lock(&status->lock);
  while(status->state == SSH2_CONTROL_STATE_INIT)
    {
      padico_out(80, "wait for the establishment of the control channel.\n");
      marcel_cond_wait(&status->connection_event, &status->lock);
    }
  if(status->state != SSH2_CONTROL_STATE_CONNECTED)
    {
      rc = -ENETUNREACH;
    }
  marcel_mutex_unlock(&status->lock);
  if(!rc)
    {
#ifdef HAVE_LIBSSH2_H
      if(status->session)
        padico_print("(LibSSH2) ssh server=%s; cipher=%s, addr=%s:%d\n",
                     status->ssh_host, libssh2_session_methods(status->session, LIBSSH2_METHOD_CRYPT_CS),
                     remote_desthost, remote_destport);
#endif /* HAVE_LIBSSH2_H */
      if(status->openssh.pid > 0)
        {
          padico_string_t msg_ossh = padico_string_new();
          padico_string_catf(msg_ossh, "(OpenSSH_%g) addr=%s:%d [ ",
                             status->openssh.version, remote_desthost, remote_destport);
          for(i = 0; i < n-1; i++)
            {
              padico_string_catf(msg_ossh, "%s@%s <-> ", users[i], nodes[i]);
            }
          padico_string_catf(msg_ossh, "%s@%s ]\n", users[n-1], nodes[n-1]);
          padico_print("%s", padico_string_get(msg_ossh));
          padico_string_delete(msg_ossh);
        }
    }
  padico_free(users);
  padico_free(nodes);

  return rc;
}

/* ********************************************************* */
/* *** PadicoComponent */

static void*ssh2_control_instantiate(puk_instance_t instance, puk_context_t context)
{
  struct ssh2_control_status_s*status = padico_malloc(sizeof(struct ssh2_control_status_s));
  *status = (struct ssh2_control_status_s)
    {
      .peer           = NULL,
      .ssh_server_fd  = -1,
      .in_io          = NULL,
      .state          = SSH2_CONTROL_STATE_INIT,
      .instance       = instance,
      .context        = context,
      .req            = NULL,
#ifdef HAVE_LIBSSH2_H
      .session        = NULL,
      .channel        = NULL,
#endif
    };
  marcel_mutex_init(&status->lock, 0);
  marcel_cond_init(&status->connection_event, 0);
  status->ssh_host = puk_context_getattr(context, "ssh_host");
  status->ssh_user = puk_context_getattr(context, "ssh_user");
  status->ssh_cipher = puk_context_getattr(context, "ssh_cipher");
  status->ssh_dest = puk_context_getattr(context, "ssh_dest");
  status->ssh_path = puk_context_getattr(context, "ssh_path");
  status->openssh.pid = 0;
  status->openssh.version = ssh2_openssh_version(status->ssh_path);
  return (void*)status;
}

static void ssh2_control_destroy(void*_status)
{
  if(_status)
    {
      struct ssh2_control_status_s*status = _status;
      if(status->ssh_server_fd != -1)
        padico_sysio_close(status->ssh_server_fd);
#ifdef HAVE_LIBSSH2_H
      if(status->channel)
        libssh2_channel_free(status->channel);
      if(status->session)
        {
          libssh2_session_disconnect(status->session, "Normal Shutdown, Control_SSH2 destroy");
          libssh2_session_free(status->session);
        }
#endif
      padico_free(_status);
    }
}

/* ********************************************************* */

static puk_component_t control_ssh2_component = NULL;

static int control_ssh2_init(void)
{
  char *username = getenv("USER");
  const char*default_cipher = "arcfour,aes128-cbc,blowfish-cbc,aes192-cbc,cast128-cbc,3des-cbc";
  control_ssh2_component =
    puk_component_declare("Control_SSH2",
                          puk_component_provides("PadicoComponent",   "component",   &ssh2_control_component_driver),
                          puk_component_provides("PadicoControl",   "control",   &ssh2_control_driver),
                          puk_component_provides("PadicoBootstrap", "bootstrap", &ssh2_bootstrap_driver),
                          puk_component_attr("ssh_host", "localhost"),
                          puk_component_attr("ssh_user", username),
                          puk_component_attr("ssh_dest", NULL),
                        puk_component_attr("ssh_cipher", default_cipher),
                          puk_component_attr("ssh_path", "/usr/bin/ssh"));
  padico_control_event_sync();
  return 0;
}

static void control_ssh2_finalize(void)
{
  puk_component_destroy(control_ssh2_component);
}

/*
 * NewMadeleine
 * Copyright (C) 2016-2022 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

#include <nm_launcher_interface.h>
#include <nm_coll_interface.h>
#include <nm_log.h>
#include <Padico/Puk.h>

#include "../common/nm_examples_helper.h"


const int msg_len = 1024 * 1024; /* 1 MB */

int recv_finalized = 0;

/** header of mutlicast requests */
struct custom_type_header_s
{
  nm_len_t body_len;
}  __attribute__((packed));

struct nm_custom_type_req_s
{
  nm_sr_request_t request;                  /**< the sendrecv request, actually allocated here */
  struct nm_data_s body_data;               /**< the user-supplied body data descriptor, copied to
                                                 allow asynchronicity */
  struct nm_data_s header_data;             /**< pre-built type for header recv */
  struct nm_data_s custom_type_data;        /**< data descriptor for custom_type request */
  void* ref_body;                           /**< user ref for sent data */
  struct custom_type_header_s* ref_header;  /**< header of the custom_type request */
};

PUK_ALLOCATOR_TYPE(nm_custom_type_req, struct nm_custom_type_req_s);

static nm_custom_type_req_allocator_t nm_custom_type_req_allocator = NULL;


struct nm_custom_type_content_s
{
  void* header_ptr;                /**< pointer to a header buffer */
  const struct nm_data_s* p_body;  /**< user-supplied body */
};

static void nm_custom_type_traversal(const void*_content, nm_data_apply_t apply, void*_context);

const struct nm_data_ops_s nm_custom_type_ops =
  {
    .p_traversal = &nm_custom_type_traversal
  };

NM_DATA_TYPE(custom_type, struct nm_custom_type_content_s, &nm_custom_type_ops);

static void nm_custom_type_traversal(const void*_content, nm_data_apply_t apply, void*_context)
{
  const struct nm_custom_type_content_s*p_content = _content;

  (*apply)(p_content->header_ptr, sizeof(struct custom_type_header_s), _context);

  nm_data_traversal_apply(p_content->p_body, apply, _context);
}

void nm_custom_type_data_build(struct nm_data_s* p_custom_type_data, void* hptr,
                             const struct nm_data_s* p_body)
{
  nm_data_custom_type_set(p_custom_type_data, (struct nm_custom_type_content_s)
      {
        .header_ptr = hptr,
        .p_body     = p_body
      });
}

static void monitor_send(nm_sr_event_t event, const nm_sr_event_info_t*p_info, void*_ref)
{
  struct nm_custom_type_req_s* p_req = (struct nm_custom_type_req_s*) _ref;

  free(p_req->ref_header);
  free(p_req->ref_body);

  nm_custom_type_req_free(nm_custom_type_req_allocator, p_req);
}

static void monitor_recv(nm_sr_event_t event, const nm_sr_event_info_t*p_info, void*_ref)
{
  struct nm_custom_type_req_s* p_req = _ref;

  assert(!((event & NM_SR_EVENT_FINALIZED) && (event & NM_SR_EVENT_RECV_DATA)));

  int rank = -1;
  nm_launcher_get_rank(&rank);

  printf("[%d] monitor_recv\n", rank);

  if(event & NM_SR_EVENT_FINALIZED)
    {
      printf("[%d] EVENT_FINALIZED, body_len = %ld\n", rank, p_req->ref_header->body_len);

      if(p_req->ref_header->body_len != msg_len)
        {
          NM_FATAL("body_len is different in finalized\n");
        }

      control_buffer(p_req->ref_body, msg_len);

      free(p_req->ref_header);
      free(p_req->ref_body);
      nm_custom_type_req_free(nm_custom_type_req_allocator, p_req);

      recv_finalized = 1;
    }

  if(event & NM_SR_EVENT_RECV_DATA)
    {
      printf("[%d] EVENT_RECV_DATA\n", rank);
      nm_data_null_build(&p_req->body_data);
      int rc = nm_sr_recv_peek(p_session, &p_req->request, &p_req->header_data);

      if(rc != NM_ESUCCESS)
        {
          NM_FATAL("# nm_custom_type: rc = %d in nm_sr_recv_peek() [ %s ]\n", rc, nm_strerror(rc));
        }

      const nm_len_t body_len = p_req->ref_header->body_len;

      printf("[%d] body_len = %ld\n", rank, body_len);

      p_req->ref_body = malloc(body_len);
      nm_data_contiguous_build(&p_req->body_data, p_req->ref_body, body_len);

      nm_custom_type_data_build(&p_req->custom_type_data, p_req->ref_header, &p_req->body_data);
      nm_sr_recv_unpack_data(p_session, &p_req->request, &p_req->custom_type_data);

      if(p_req->ref_header->body_len != msg_len)
        {
          NM_FATAL("body_len is different in recv_data\n");
        }
    }
}

int main(int argc, char**argv)
{
  nm_examples_init(&argc, argv);

  nm_custom_type_req_allocator = nm_custom_type_req_allocator_new(8);
  struct nm_custom_type_req_s* p_req = nm_custom_type_req_malloc(nm_custom_type_req_allocator);
  p_req->ref_header = malloc(sizeof(struct custom_type_header_s));

  int rank = -1;
  nm_launcher_get_rank(&rank);

  struct nm_data_s body_data;
  struct nm_data_s custom_type_data;

  if(rank == 0)
    {
      p_req->ref_body = malloc(msg_len * sizeof(char));
      fill_buffer(p_req->ref_body, msg_len);

      p_req->ref_header->body_len = msg_len;

      nm_gate_t gate = NULL;
      nm_launcher_get_gate(1, &gate);

      nm_data_contiguous_build(&body_data, p_req->ref_body, msg_len);

      nm_custom_type_data_build(&custom_type_data, p_req->ref_header, &body_data);
      nm_sr_send_init(p_session, &p_req->request);
      nm_sr_send_pack_data(p_session, &p_req->request, &custom_type_data);
      nm_sr_request_set_ref(&p_req->request, p_req);
      nm_sr_request_monitor(p_session, &p_req->request, NM_SR_EVENT_FINALIZED, &monitor_send);
      nm_sr_send_dest(p_session, &p_req->request, gate, 1);

      printf("[%d] sending to 1, body_len = %d\n", rank, msg_len);

      nm_sr_send_header(p_session, &p_req->request, sizeof(struct custom_type_header_s));
      nm_sr_send_submit(p_session, &p_req->request);
      /* from now on, request will be executed asynchronously. Data needs to
      * remain valid until completion, thus body_data and custom_type_data
      * scope is ont limited to this block. */
    }
  else if (rank == 1)
    {
      p_req->ref_body = NULL;

      nm_data_contiguous_build(&p_req->header_data, p_req->ref_header, sizeof(struct custom_type_header_s));
      nm_sr_recv_init(p_session, &p_req->request);
      nm_sr_request_set_ref(&p_req->request, p_req);
      nm_sr_request_monitor(p_session, &p_req->request, NM_SR_EVENT_FINALIZED | NM_SR_EVENT_RECV_DATA,
                            &monitor_recv);
      nm_sr_recv_irecv(p_session, &p_req->request, NM_ANY_GATE, 1, NM_TAG_MASK_FULL);

      while(recv_finalized == 0)
        {
          nm_sr_progress(p_session);
        }
    }
  nm_examples_exit();
  nm_custom_type_req_allocator_delete(nm_custom_type_req_allocator);
  return 0;

}

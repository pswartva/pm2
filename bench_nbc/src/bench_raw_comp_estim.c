/*
 * Bench NBC
 * Copyright (C) 2019-2020 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <time.h>
#include <stdio.h>
#include <stdlib.h>

#include "misc.h"
#include "bench_constants.h"

#define DEFAULT_FILENAME "bench_raw_estim.out"

long int init_raw_compute(double time){
  int old_size = 1;
  int size,i,closer_size=0, count=0;
  //size of matrices

  // double *a,*b,*res;
  //times
  //uint64_t t,t1,old_t,t_cpu_exploit = 0;
  double old_t = -1,t_cpu_exploit = 0,closer_delta=10e9,*tab;
  struct timespec t,t1;
  size = 100;
  //fprintf(stderr,"approximate matrix size:");
#ifdef GLOBAL_BUFFER
  matrix_compute(size);
#else
  double *a,*b,*res;
  a=malloc(size*size*sizeof(double));
  b=malloc(size*size*sizeof(double));
  res=calloc(size*size,sizeof(double));
  matrix_compute(a,b,res,size);
  free(a);
  free(b);
  free(res);
#endif
  size = 2;
  do{


    //    else if(t_cpu_exploit > time){
    //      over=1;
    //      size = (old_size + size) / 2;
    //    }


    int nb_rep = get_nb_rep(0,size,NB_REP);
    tab=malloc(sizeof(double)*nb_rep);
    for(i = 0; i < nb_rep; i++){
#ifdef GLOBAL_BUFFER
      clock_gettime(CLOCK_MONOTONIC,&t);
      matrix_compute(size);
      clock_gettime(CLOCK_MONOTONIC,&t1);
      tab[i] = ((double)t1.tv_sec*1000000.0 + (double) t1.tv_nsec /1000.0) - ((double)t.tv_sec*1000000.0 + (double) t.tv_nsec /1000.0);
#else
      a=malloc(size*size*sizeof(double));
      b=malloc(size*size*sizeof(double));
      res=calloc(size*size,sizeof(double));

      //test if we have a matrix size which have a "time" compute time
      /*get_time_r(&t);
        matrix_compute(a,b,res,size);
      //get_time_r(&t1);
      old_t = t_cpu_exploit;
      t_cpu_exploit = t1 - t;*/
      clock_gettime(CLOCK_MONOTONIC,&t);
      matrix_compute(a,b,res,size);
      clock_gettime(CLOCK_MONOTONIC,&t1);
      tab[i] = ((double)t1.tv_sec*1000000.0 + (double) t1.tv_nsec /1000.0) - ((double)t.tv_sec*1000000.0 + (double) t.tv_nsec /1000.0);
      //fprintf(stderr,"size=%ld time=%lf t_cpu_exploit=%lf\n",size,time,t_cpu_exploit);

      free(a);
      free(b);
      free(res);
#endif
    }
    old_t = t_cpu_exploit;
    t_cpu_exploit = median(tab,nb_rep);
    free(tab);
    int temp = interpolate(old_size,size,(double)old_t,(double)t_cpu_exploit,(double)time);
    if(fabs((time-t_cpu_exploit)) < closer_delta){
      closer_size = size;
      closer_delta = fabs(time-t_cpu_exploit);
    }
    if(closer_delta < time/100 || count > 100){
      return closer_size;
    }
    old_size = size;
    size = temp;
    count++;

  }
  while(1);
  return size;
}

static void usage(void)
{
  printf("Usage: bench_raw_estim [<outfile>]\n");
  printf("  estimates computation speed before running bench_nbc\n");
  printf("  <outfile>   the file to write to (default: %s)\n", DEFAULT_FILENAME);
}

int main(int argc, char**argv)
{
#ifdef GLOBAL_BUFFER
  a=malloc(5000*5000*sizeof(double));
  b=malloc(5000*5000*sizeof(double));
  res=calloc(5000*5000,sizeof(double));
#endif

  if((argc == 2) && ((strcmp(argv[1], "-h") == 0) || (strcmp(argv[1], "--help") == 0)))
    {
      usage();
      exit(0);
    }
  else if((argc > 2) || ((argc == 2) && (argv[1][0] == '-')))
    {
      usage();
      exit(-1);
    }

  char*filename = DEFAULT_FILENAME;
  if(argc == 2)
    {
      filename = argv[1];
    }
  fprintf(stderr, "# writing data to file: %s\n", filename);
  FILE*output = fopen(filename,"w");
  if(output == NULL)
    {
      fprintf(stderr, "# cannot open file %s for writing.\n", filename);
      abort();
    }
  comp_time_info_t** temp = NULL;
  temp =  malloc(sizeof(comp_time_info_t*)*NB_MATRIX_SIZE);
  int j, temp_time = BASE_COMPUTE_TIME;
  for(j = 0; j < NB_MATRIX_SIZE; j++){
    temp[j] = malloc(sizeof(comp_time_info_t));
    temp[j]->theorical_time = temp_time;
    temp[j]->matrix_size = init_raw_compute((double)(temp_time));
    temp[j]->real_time = estimate_comp_time(temp[j]->matrix_size);
    uint64_t n = temp[j]->matrix_size;
    uint64_t nb_op = n*n*(n+(n-1));
    fprintf(output,"%d %lf %ld %ld\n",temp_time,temp[j]->real_time->median[0],n,nb_op);
    fprintf(stderr, "# round = %d / %d; matrix size = %lu; target time = %d usec.; real time = %lf usec.\n",
	    j, NB_MATRIX_SIZE, n, temp_time,temp[j]->real_time->median[0]);
    temp_time *= MATRIX_COMP_STEP;
  }
  return 0;
}

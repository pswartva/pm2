/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief VIO auto-diagnosis statistics
 * @ingroup VIO
 */


#include "VIO-internals.h"
#include "VLink-internals.h"
#include <Padico/Topology.h>
#include <Padico/Module.h>

#include <netinet/in.h>
#include <arpa/inet.h>

PADICO_MODULE_HOOK(VIO);

/* ********************************************************* */

struct vlink_statistics_s vlink_stat;
#ifdef VLINK_PROFILE
#warning "******************* VLink-profiling activated. ***********************"
padico_timing_profiler_t vlink_rprofiler = NULL; /* AD:profile */
padico_timing_profiler_t vlink_wprofiler = NULL; /* AD:profile */

void padico_vlink_profile_flush(void)
{
  padico_timing_profile_flush(vlink_rprofiler);
  padico_timing_profile_flush(vlink_wprofiler);
}
static void vio_flushprofile_end_handler(puk_parse_entity_t e)
{
  padico_vlink_profile_flush();
}
#endif /* VLINK_PROFILE */

/* ********************************************************* */

static padico_rc_t padico_vio_dumpstat(void)
{
  padico_rc_t rc = padico_rc_msg("\n");
  padico_rc_t line = padico_rc_msg("<viostatus>num_fd=%d\n"
                                   "open_sockets=%d\n"
                                   "num_recv_by_copy=%d\n"
                                   "num_recv_zero_copy=%d\n"
                                   "bytes_recv_by_copy=%lld\n"
                                   "bytes_recv_zero_copy=%lld\n"
                                   "num_hfd_force_synchro=%d\n"
                                   "num_immediate_write=%d\n"
                                   "num_immediate_read=%d\n"
                                   "num_write=%d\n"
                                   "num_read=%d\n"
                                   "misaligned_receive=%d\n"
                                   "misaligned_send=%d\n"
                                   "no_rbox=%d\n"
                                   "rbox_too_small=%d\n"
                                   "rendezvous_zero_copy=%d\n"
                                   "</viostatus>",
                                   vlink_stat.num_fd,
                                   vlink_stat.open_sockets,
                                   vlink_stat.num_recv_by_copy,
                                   vlink_stat.num_recv_zero_copy,
                                   vlink_stat.bytes_recv_by_copy,
                                   vlink_stat.bytes_recv_zero_copy,
                                   vlink_stat.num_hfd_force_synchro,
                                   vlink_stat.num_immediate_write,
                                   vlink_stat.num_immediate_read,
                                   vlink_stat.num_write,
                                   vlink_stat.num_read,
                                   vlink_stat.misaligned_receive,
                                   vlink_stat.misaligned_send,
                                   vlink_stat.no_rbox,
                                   vlink_stat.rbox_too_small,
                                   vlink_stat.rendezvous_zero_copy);
  rc = padico_rc_cat(rc, line);
  if(line)
    padico_rc_delete(line);
  return rc;
}

static padico_rc_t vio_rc_sockaddr(struct sockaddr*addr, int addrlen)
{
  padico_rc_t rc;
  switch(addr->sa_family)
    {
    case AF_INET:
      rc = padico_rc_msg("<sockaddr len=%d family=\"AF_INET\">%s:%d</sockaddr>",
                         addrlen,
                         inet_ntoa(((struct sockaddr_in*)addr)->sin_addr),
                         ntohs(((struct sockaddr_in*)addr)->sin_port));
      break;
    case AF_UNIX:
      rc = padico_rc_msg("<sockaddr len=%d family=\"AF_UNIX\">%s</sockaddr>",
                         addrlen,
                         ((struct sockaddr_un*)addr)->sun_path);
      break;
    default:
      rc = padico_rc_msg("<sockaddr len=%d family=\"unknown\"/>", addrlen);
    }
  return rc;
}

padico_rc_t padico_vio_dumpvnode(padico_vnode_t vnode)
{
  const struct padico_vlink_driver_s*driver =
    padico_vnode_get_vlink_driver(vnode);
  padico_rc_t rc = padico_rc_msg("\n<VIO:vlink vnode=%p>\n", vnode);
  rc = padico_rc_add(rc, padico_rc_msg("  <status readable=\"%d\" writable=\"%d\" opened=\"%d\"/>\n",
                                       vnode->status.readable,
                                       vnode->status.writable,
                                       vnode->status.opened));
  if(padico_vnode_component_name(vnode))
    rc = padico_rc_add(rc, padico_rc_msg("  <component>component=%s</component>\n",
                                         padico_vnode_component_name(vnode)));
  if(vnode->content.hfd)
    rc = padico_rc_add(rc, padico_rc_msg("  <hfd fd=\"%d\">io_read=%p io_write=%p</hfd>\n",
                                         vnode->hfd.fd,
                                         vnode->hfd.io_read,
                                         vnode->hfd.io_write));
  if(vnode->content.socket)
    rc = padico_rc_add(rc, padico_rc_msg("  <socket so_options=%u so_family=%d so_type=%d so_protocol=%d/>\n",
                                         vnode->socket.so_options,
                                         vnode->socket.so_family,
                                         vnode->socket.so_type,
                                         vnode->socket.so_protocol));

  if(vnode->content.binding)
    {
      rc = padico_rc_add(rc, padico_rc_msg("  <binding>\n    "));
      rc = padico_rc_add(rc, vio_rc_sockaddr(vnode->binding.primary, vnode->binding.primary_len));
      rc = padico_rc_add(rc, padico_rc_msg("\n  </binding>\n"));
    }

  if(vnode->content.remote)
    {
      rc = padico_rc_add(rc, padico_rc_msg("  <remote established=%d can_send=%d can_recv=%d>\n    ",
                                           vnode->remote.established,
                                           vnode->remote.can_send,
                                           vnode->remote.can_recv));
      rc = padico_rc_add(rc, vio_rc_sockaddr(vnode->remote.remote_addr, vnode->remote.remote_addrlen));
      rc = padico_rc_add(rc, padico_rc_msg("\n  </remote>\n"));
    }
  if(vnode->content.listener)
    rc = padico_rc_add(rc, padico_rc_msg("  <listener>backlog_queue=%p queue_size=%d backlog_max_size=%d</listener>\n",
                                         vnode->listener.backlog,
                                         padico_vnode_queue_size(vnode->listener.backlog),
                                         vnode->listener.backlog_size));
  if(driver && driver->dumpstat)
    {
      rc = padico_rc_add(rc, (*(driver->dumpstat))(vnode));
    }
  if(vnode->content.rbox)
    rc = padico_rc_add(rc, padico_rc_msg("  <rbox>read_posted=%d read_done=%d read_buffer=%p</rbox>\n",
                                         vnode->rbox.read_posted,
                                         vnode->rbox.read_done,
                                         vnode->rbox.read_buffer));
  if(vnode->content.wbox)
    rc = padico_rc_add(rc, padico_rc_msg("  <wbox>write_posted=%d write_done=%d write_buffer=%p is_owner=%d</wbox>\n",
                                         vnode->wbox.write_posted,
                                         vnode->wbox.write_done,
                                         vnode->wbox.write_buffer,
                                         vnode->wbox.is_owner));
  rc = padico_rc_add(rc, padico_rc_msg("</VIO:vlink>\n"));
  return rc;
}

static void viostat_end_handler(puk_parse_entity_t e)
{
  puk_parse_set_rc(e, padico_vio_dumpstat());
}
static const struct puk_tag_action_s viostat_action =
{
  .xml_tag        = "VIO:dumpstat",
  .start_handler  = NULL,
  .end_handler    = &viostat_end_handler,
  .required_level = PUK_TRUST_OUTSIDE,
  .help           = "Dumps debug/statistics information on VIO."
};

/* debug assembly */
#ifdef PADICO_TRACE_ON
void vnode_show_assembly(puk_component_t assembly, int way, const char*text)
{
  if(vlink_stat.debug.show_vlink_assembly)
    {
      puk_component_enumerator_t e =
        puk_component_enumerator_new(assembly, way);
      puk_context_t i = puk_component_enumerator_next(e);
      padico_print(" %s- assembly=%p\n", text, assembly);
      while(i)
        {
          padico_print("      * component=%s; context=%p\n",
                       i->component->name, i);
          i = puk_component_enumerator_next(e);
        }
      puk_component_enumerator_delete(e);
    }
}

void vnode_show_vlink(padico_vnode_t vnode, int way, const char*text)
{
  if(vlink_stat.debug.show_vlink_assembly)
    {
      const puk_component_t assembly = vnode->assembly_stuff.instance->component;
      vnode_show_assembly(assembly, way, text);
    }
}
#endif /* PADICO_TRACE_ON */


/* ***** VIO:dumpvfd *************************************** */

static void viodump_vfd_end_handler(puk_parse_entity_t e)
{
  if(puk_parse_get_text(e))
    {
      int fd = atoi(puk_parse_get_text(e));
      puk_parse_set_rc(e, padico_vio_dump_vfd(fd));
    }
  else
    {
      puk_parse_set_rc(e, padico_rc_error("No file descriptor given\n"));
    }
}
static const struct puk_tag_action_s viodumpvfd_action =
{
  .xml_tag        = "VIO:dumpvfd",
  .start_handler  = NULL,
  .end_handler    = &viodump_vfd_end_handler,
  .required_level = PUK_TRUST_OUTSIDE,
  .help = "Dumps debug information about a given VIO file descriptor.\n"
  "Example: <VIO:dumpvfd>14</VIO:dumpvfd> dumps information on fd 14."
};

void padico_viostat_init(void)
{
  vlink_stat.num_fd = 0;
  vlink_stat.open_sockets = 0;
  vlink_stat.num_recv_by_copy = 0;
  vlink_stat.num_recv_zero_copy = 0;
  vlink_stat.num_hfd_force_synchro = 0;
  vlink_stat.num_immediate_write = 0;
  vlink_stat.num_immediate_read = 0;
  vlink_stat.num_write = 0;
  vlink_stat.num_read = 0;
  vlink_stat.bytes_recv_by_copy = 0;
  vlink_stat.bytes_recv_zero_copy = 0;
  vlink_stat.misaligned_receive = 0;
  vlink_stat.misaligned_send = 0;
  vlink_stat.no_rbox = 0;
  vlink_stat.rbox_too_small = 0;
  vlink_stat.rendezvous_zero_copy = 0;
  vlink_stat.debug.show_vlink_assembly = 0;

  {
    /* debug level */
    const char*debug = padico_getattr("VLINK_DEBUG");
    const int show_all = debug?(strstr(debug, "all") != NULL):0;
    if(debug)
      {
        padico_print("debug VLINK_DEBUG=%s\n", debug);
      }
    vlink_stat.debug.show_vlink_assembly =
      show_all || (debug && strstr(debug, "assembly"));
  }
  puk_xml_add_action(viostat_action);
  puk_xml_add_action(viodumpvfd_action);

#ifdef VLINK_PROFILE
  vlink_rprofiler = padico_timing_profiler_new(15, 100, 20, "/tmp/VIO-r-profile.log"); /* AD:profile */
  vlink_wprofiler = padico_timing_profiler_new(15, 100, 20, "/tmp/VIO-w-profile.log"); /* AD:profile */
  puk_xml_add_action((struct puk_tag_action_s){
    .xml_tag = "VIO-profile:flush",
    .end_handler = vio_flushprofile_end_handler,
    .required_level = PUK_TRUST_OUTSIDE}
  );
  padico_print("********** VIO profiling activated. ********* \n");
  padico_print("send command \"<VIO-profile:flush/>\" to flush profiling data.\n");
#endif
}

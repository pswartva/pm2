#! /bin/bash
######################################################################################################
# distribution.sh
#
# NOM
#        distribution.sh - Cr�e une courbe de distribution
#
# SYNOPSIS
#       ./distribution.sh [OPTION] ENTR�E
#
# DESCRIPTION
#       Cr�e une courbe de la distribution de chaque fichier en entr�e.
#
#       -h
#		afficher les r�gles d'usage
#
#       -o fichier
#		sp�cifier le fichier de sortie (par d�faut out.ps)
#
#       -min N
#		d�finir le d�but de l'intervale d'affichage (par d�faut 0)
#		
#       -max N
#		d�finir la fin de l'intervale d'affichage (par d�faut 120)
#
#       Les fichiers de donn�es sont constitu�s de suites de nombres :
#	   81.7
#	   79.9
#	   80.5
#	   83.4
#	   81.3
#	   79.1
#	   80.2
#
# EXEMPLE D'UTILISATION
#
#       $> grep "#4 bytes" /tmp/VIO-login.dis |awk '{print$3}'> 4_bytes
#       $> grep "#8 bytes" /tmp/VIO-login.dis |awk '{print$3}'> 8_bytes
#       $> ./distribution.sh -min 20 -max 50 -o distribution.ps 8_bytes 4_bytes 
######################################################################################################

usage() {
    echo "USAGE: $0 [-b <bytes>] [-o fichier.ps] [-min <xmin>] [-max <xmax>] <file.dis>"
}

ps_file=out.ps
input_file=
tmp_file=
width=1
bytes=1

until [ -z "$1" ]  # Until all parameters used up...
do
  case $1 in
      -o)
	  shift
	  ps_file=$1
	  ;;
      -b)
	  shift
	  bytes="$1"
	  ;;
      -min)
	  shift
	  xmin="$1"
	  ;;
      -max)
	  shift
	  xmax="$1"
	  ;;
      -width)
	  shift
	  width="$1"
	  ;;
      -h|--help)
	  usage
	  exit 0
	  ;;
      *)
	  input_file="$1"
	  ;;
  esac
  shift
done

if [ -z "${input_file}" ]; then
    usage
    exit 1;
fi

tmp_file=$( tempfile )
grep "#${bytes} bytes" ${input_file} | tr ' ' '\n' > ${tmp_file}


gnuplot <<EOF
set terminal postscript color
set output "${ps_file}"
set ylabel "count"
set xlabel "latency (usec.)"
set boxwidth 0.6*${width}
binwidth=${width}
bin(x,width)=width*floor(x/width)
plot [${xmin}:${xmax}] "${tmp_file}" using (bin(\$1,binwidth)):(${width}) smooth freq with boxes fs solid 0.6 title "distribution for ${bytes} bytes messages"
EOF

rm ${tmp_file}

echo "# output file: ${ps_file}"

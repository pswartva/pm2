/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief LZO Packet Filter component
 */

#include <Padico/Module.h>
#include "PacketFilter.h"
#include "minilzo.h"


/* ********************************************************* */
/* *** LZO Packet Filter *********************************** */
/* ********************************************************* */


/** @defgroup PacketFilter_LZO component: PacketFilter_LZO- a packet filter that performs realtime LZO compression
 * @ingroup PadicoComponent
 */

static void* lzopf_instantiate(puk_instance_t assembly, puk_context_t context);
static void lzopf_destroy(void*state);
static void lzopf_init(void);
static void lzopf_encode(void*, const void*, size_t, void**, size_t*);
static void lzopf_decode(void*, const void*, size_t, void*, size_t*);

/** instanciation facet for PacketFilter_LZO
 * @ingroup PacketFilter_LZO
 */
static const struct puk_component_driver_s lzo_component_driver =
  {
    .instantiate    = &lzopf_instantiate,
    .destroy        = &lzopf_destroy,
    .component_init = &lzopf_init
  };
/** 'PacketFilter' facet for PacketFilter_LZO
 * @ingroup PacketFilter_LZO
 */
static const struct packetfilter_driver_s lzopf_driver =
  {
    .pf_encode = &lzopf_encode,
    .pf_decode = &lzopf_decode
  };

/* ********************************************************* */

static void lzopf_init(void)
{
  lzo_init();
  padico_out(puk_verbose_notice, "using miniLZO version %s (%s)\n",
               lzo_version_string(), lzo_version_date());
}

PADICO_MODULE_COMPONENT(PacketFilter_LZO,
                        puk_component_declare("PacketFilter_LZO",
                                              puk_component_provides("PadicoComponent", "component", &lzo_component_driver),
                                              puk_component_provides("PacketFilter", "pf", &lzopf_driver)));

/* ********************************************************* */

struct lzo_state_s
{
  void*wrkmem;
};

static void* lzopf_instantiate(puk_instance_t assembly, puk_context_t context)
{
  struct lzo_state_s*state = padico_malloc(sizeof(struct lzo_state_s));
  state->wrkmem = padico_malloc(LZO1X_MEM_COMPRESS);
  return state;
}

static void lzopf_destroy(void*w)
{
  struct lzo_state_s*state  = w;
  assert(state != NULL);
  padico_free(state->wrkmem);
  padico_free(state);
}


/* 'out' allocated by encode function
 */
static void lzopf_encode(void*w,
                         const void*in, size_t in_size,
                         void**out, size_t* out_size)
{
  struct lzo_state_s*state = w;
  lzo_uint s = in_size + in_size / 16 + 64;
  void*p = padico_malloc(s);
  int rc = lzo1x_1_compress(in, in_size, p, &s, state->wrkmem);
  if(rc != LZO_E_OK)
    {
      /* compression failed: don't compress */
      padico_fatal("compression failed.\n");
      memcpy(p, in, in_size);
      s = in_size;
    }
  *out = p;
  *out_size = s;
}

/* 'out' allocated by caller
*/
static void lzopf_decode(void*w,
                         const void*in, size_t in_size,
                         void*out, size_t* out_size)
{
  lzo_uint s = *out_size;
  int rc = lzo1x_decompress(in, in_size, out, &s, NULL);
  if(rc != LZO_E_OK)
    {
      padico_fatal("CRC error while uncompressing- in_size=%lu; out_size=%lu; rc=%d\n",
                   (unsigned long)in_size, (unsigned long)*out_size, rc);
    }
  *out_size = s;
}

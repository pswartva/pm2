/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief PadicoTM core module public interface
 * @todo should be documented...
 * @ingroup TaskManager
 */

#ifndef PADICO_TM_H
#define PADICO_TM_H

#include <Padico/Puk.h>
#include "PM2.h"
#ifdef PUKABI
#include <Padico/Puk-ABI.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif

#ifndef PUKABI
#include <errno.h>

  static inline void puk_spinlock_acquire(void)
  {  }
  static inline void puk_spinlock_release(void)
  {  }
  static inline void puk_abi_disable_preload(void)
  { }

#define PUK_ABI_FSYS_WRAP(SYM)   SYM
#define PUK_ABI_FSYS_REAL(SYM)   SYM
#define PUK_ABI_PROC_WRAP(SYM)   SYM
#define PUK_ABI_PROC_REAL(SYM)   SYM
#define PUK_ABI_MEM_WRAP(SYM)    SYM
#define PUK_ABI_MEM_REAL(SYM)    SYM
#define PUK_ABI_RESOLV_WRAP(SYM) SYM
#define PUK_ABI_RESOLV_REAL(SYM) SYM
#define PUK_ABI_WRAP(SYM)        SYM
#define PUK_ABI_REAL(SYM)        SYM

#define puk_abi_set_virtual(F, PTR) { }

#define __puk_abi_wrap__errno errno
  static inline void puk_abi_seterrno(int e)
  {
    errno = e;
  }
  static inline void puk_abi_real_seterrno(int e)
  {
    errno = e;
  }
#define puk_abi_real_errno() errno

#endif /* PUKABI */

  /* ******************************************************* */
  /* ** main entry points */

  void padico_tm_init(void);

  void padico_tm_main(void);

  void padico_tm_finalize(void);

  void padico_tm_ready_wait(void);

  void padico_tm_ready_signal(void);

#include "tm-Tasks.h"

  /* ******************************************************* */

/** @addtogroup PadicoTasks
 * @{
 */

  /* *** padico_req_* */

  typedef void (*padico_task_notifier_t)(padico_rc_t, void*);
  typedef uint32_t padico_req_id_t;
#define PADICO_REQ_ID_NONE ((padico_req_id_t)0)

  PUK_LIST_TYPE(padico_req,
                padico_req_id_t id;
                int         completed;
                padico_rc_t rc;
                padico_task_notifier_t notify;
                void*                  notify_key;
                );
  PUK_VECT_TYPE(padico_reqs, padico_req_t);

  padico_rc_t  padico_tm_req_wait(padico_req_t req);
  padico_req_t padico_tm_req_new(padico_task_notifier_t notifier, void*key);
  void         padico_tm_req_notify(padico_req_id_t req_id, padico_rc_t rc);


  /* ** RTTASK ********************************************* */

  typedef void*(*padico_thread_fun_t)(void*);

  void padico_tm_rttask_start(padico_thread_fun_t f, void*arg, const char*name);

  /** a thread running in background, optionnaly member of a bgthread pool */
  PUK_LIST_TYPE(padico_tm_bgthread,
                marcel_t tid;
                padico_thread_fun_t f;
                void*arg;
                char*name;
                struct padico_tm_bgthread_pool_s*pool;
                );

  /** a thread pool, to control and wait running threads */
  struct padico_tm_bgthread_pool_s
  {
    const char*name;
    marcel_mutex_t lock;
    struct padico_tm_bgthread_list_s running;
    struct padico_tm_bgthread_list_s done;
  };
  typedef struct padico_tm_bgthread_pool_s*padico_tm_bgthread_pool_t;
  /** create a new thread pool */
  padico_tm_bgthread_pool_t padico_tm_bgthread_pool_create(const char*name);

  /** finalize a pool of threads: waits for all thread to complete */
  void padico_tm_bgthread_pool_wait(padico_tm_bgthread_pool_t pool);

  void padico_tm_bgthread_start(padico_tm_bgthread_pool_t pool, padico_thread_fun_t f, void*arg, const char*name);

#define padico_tm_invoke_later(FUNC, KEY) { padico_tm_invoke_later_named((FUNC), (KEY), #FUNC); }

  static inline void padico_tm_invoke_later_named(padico_thread_fun_t f, void*arg, const char*name)
  {
    padico_tm_bgthread_start(NULL, f, arg, name);
  }

  /* ** TASK */

  enum padico_mtask_kind_e
  {
    PADICO_TM_MOD_LOAD   = 1,
    PADICO_TM_MOD_UNLOAD = 2,
    PADICO_TM_MOD_RUN    = 3,
    PADICO_TM_MOD_START  = 4
  };

  struct padico_task_s
  {
    padico_req_t task_req;
    puk_mod_t parent;            /**< parent mod that issued the task */
    enum
      {
        PADICO_TM_TASK_EXIT = 1, /**< exit PadicoTM */
        PADICO_TM_TASK_MOD  = 2, /**< operation on modules */
        PADICO_TM_TASK_CMD  = 3, /**< execute an XML command */
        PADICO_TM_TASK_FILE = 4, /**< load a XML file and execute its content */
        PADICO_TM_TASK_MAIN = 5  /**< execute the given function in the main thread */
      } kind;
    union
    {
      /* task MOD */
      struct
      {
        enum padico_mtask_kind_e kind;
        char* mod_name;
        int   argc;
        char**argv;
      } mtask;
      /* task CMD */
      struct
      {
        char*cmd;
        int  trust_level;
      } cmdtask;
      /* task FILE */
      struct
      {
        char*file;
      } ftask;
      /* task MAIN */
      struct
      {
        int(*func)(int argc, char**argv);
        int argc;
        char**argv;
      } mainthread;
    } tinfo;
  };
  typedef struct padico_task_s*padico_task_t;

  padico_rc_t padico_tm_mod_action_args(enum padico_mtask_kind_e kind,
                                        const char*mod_name,
                                        int argc, char**argv);

  static inline padico_rc_t padico_tm_mod_action(enum padico_mtask_kind_e kind, const char*mod_name)
  {
    assert(kind != PADICO_TM_MOD_RUN && kind != PADICO_TM_MOD_START);
    padico_rc_t rc = padico_tm_mod_action_args(kind, mod_name, 0, NULL);
    return rc;
  }

  void padico_tm_task_enqueue_from(padico_task_t task, puk_mod_t parent, const char*name);
#define padico_tm_task_enqueue(TASK, NAME) padico_tm_task_enqueue_from((TASK), padico_module_self(), (NAME))

  void padico_tm_exit(void);

  /** @} */

  /* thread-friendly sleeping */

  int  padico_tm_nanosleep(const struct timespec*req, struct timespec*rem);
  void padico_tm_usleep(long usec);
  void padico_tm_msleep(long msec);
  unsigned int padico_tm_sleep(unsigned long seconds);
  void padico_tm_yield(void);

  /* helper function to change return code */

  void padico_tm_update_rc(int rc);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* PADICO_TM_H */

/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#ifndef NM_LAUNCHER_H
#define NM_LAUNCHER_H

/** @file
 * Internal definitions and functions for launchers. This file is
 * not part of the public interface; it is supposed to be used only
 * by launchers. The interface for endusers is nm_launcher_interface.h
 */

#include <nm_launcher_interface.h>
#include <Padico/Puk.h>

/** block of info given by the launcher */
struct nm_launcher_info_s
{
  int size; /**< the number of nodes in the job */
  int rank; /**< rank of the local process */
  int wide_url_support; /**< whether the launcher supports wide urls */
};

/** @internal Component interface definition: 'NewMad_Launcher' */
struct nm_launcher_driver_s
{
  /** initialize nmad, establishe connections */
  void (*init)(void*_status, int*argc, char**argv, const char*label);
  /** barrier accross all processes, implemented out-of-band of nmad */
  void (*barrier)(void*_status);
  /** get the list of gates */
  void (*get_gates)(void*_status, nm_gate_t*gates);
  /** abort the full session */
  void (*abort)(void*_status, int rc);
};

/** @internal */
PUK_IFACE_TYPE(NewMad_Launcher, struct nm_launcher_driver_s);

/** an entry for a remote node */
struct nm_launcher_gate_s
{
  nm_gate_t p_gate;
  char*p_url;
  struct nm_drv_vect_s drvs;
};

/** a set of gates to manage connection establishment by launcher */
struct nm_launcher_gates_s
{
  int size;
  int rank;
  struct nm_launcher_gate_s*p_gates;
};

/** hash nm drivers by component */
PUK_HASHTABLE_TYPE(nm_component, puk_component_t, nm_drv_t,
                   &puk_hash_pointer_default_hash, &puk_hash_pointer_default_eq, NULL);

void nm_launcher_gates_init(struct nm_launcher_gates_s*p_gates, int size, int rank);

void nm_launcher_gates_destroy(struct nm_launcher_gates_s*p_gates);

/** connects the given set of gates */
void nm_launcher_gates_connect(struct nm_launcher_gates_s*p_gates);


/* ********************************************************* */
/* ** interface for launchers to interact with nmad */

/** Declare the rank, size & properties of job/launcher
 */
void nm_launcher_set_info(const struct nm_launcher_info_s*p_info);

/** Add a driver to the session.
 * @note call to this function is optional. Default drivers will be loaded
 * if no driver is loaded manually.
 */
nm_drv_t nm_launcher_add_driver(puk_component_t component, nm_trk_kind_t kind);

/** Type for 'selector'. Returns drivers to connect to given gate.
 */
typedef nm_drv_vect_t (*nm_launcher_selector_t)(const char*url, void*_arg);
/** Declare the selector to use to establish connections
 */
void nm_launcher_set_selector(nm_launcher_selector_t selector, void*_arg);

/** Finalizes process of driver loading & return local url.
 * 'p_local_url' is a return parameter, pointing to an object allocated in the launcher- do not free nor modify!
 */
int nm_launcher_get_url(const char**p_local_url);

/** Connect the given gate.
 * 'remote_url' is the url returned by the nm_launcher_get_url() on the given peer.
 * @note this function is synchronous; the remote node must call this function
 * at the same time with our url
 */
int nm_launcher_connect(nm_gate_t*pp_gate, const char*remote_url);

/** Asynchronously start connection establishment.
 * @note it is expected that, if async connection establishment is used,
 * both nm_launcher_connect_wait() _must_ be called.
 */
int nm_launcher_connect_async(nm_gate_t*pp_gate, const char*remote_url);

/** Wait for a given async connection to get ready.
 */
int nm_launcher_connect_wait(nm_gate_t p_gate);


#endif /* NM_LAUNCHER_H */

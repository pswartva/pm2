/*
 * NewMadeleine
 * Copyright (C) 2014-2022 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <assert.h>
#include <nm_public.h>
#include <nm_private.h>

#include "nm_coll_private.h"
#include <Padico/Module.h>

PADICO_MODULE_HOOK(nm_coll);


static void nm_coll_req_destroy(struct nm_coll_req_s*p_coll_req);

/* ** coll request ***************************************** */

struct nm_coll_req_s*nm_coll_req_alloc(nm_len_t payload_size, nm_coll_req_kind_t kind,
                                       nm_coll_req_destructor_t p_destructor,
                                       nm_coll_req_notifier_t p_notify, void*p_ref)
{
  struct nm_coll_req_s*p_coll_req = padico_malloc(sizeof(struct nm_coll_req_s) + payload_size);
  p_coll_req->kind = kind;
  p_coll_req->p_destructor = p_destructor;
  p_coll_req->p_notify = p_notify;
  if(p_notify != NULL)
    {
      p_coll_req->p_ref = p_ref;
    }
  else
    {
      nm_cond_init(&p_coll_req->status, 0);
    }
#ifdef NMAD_PROFILE
  PUK_GET_TICK(p_coll_req->start_time);
#endif /* NMAD_PROFILE */
  return p_coll_req;
}

void nm_coll_req_wait(struct nm_coll_req_s*p_coll_req)
{
  nm_core_t p_core = nm_core_get_singleton();
  nm_cond_wait(&p_coll_req->status, 1, p_core);
  nm_coll_req_destroy(p_coll_req);
}

int nm_coll_req_test(struct nm_coll_req_s*p_coll_req)
{
  if(nm_cond_test(&p_coll_req->status, 1))
    {
      nm_coll_req_destroy(p_coll_req);
      return NM_ESUCCESS;
    }
  else
    {
      nm_core_t p_core = nm_core_get_singleton();
      nm_schedule(p_core);
      return -NM_EAGAIN;
    }
}

void nm_coll_req_signal(struct nm_coll_req_s*p_coll_req)
{
  if(p_coll_req->p_notify == NULL)
    {
      nm_cond_signal(&p_coll_req->status, 1);
    }
  else
    {
      nm_coll_req_notifier_t p_notify = p_coll_req->p_notify;
      void*p_ref = p_coll_req->p_ref;
      (*p_notify)(p_ref);
      nm_coll_req_destroy(p_coll_req);
    }
}


static void nm_coll_req_destroy(struct nm_coll_req_s*p_coll_req)
{
  if(p_coll_req->p_destructor != NULL)
    {
      (*p_coll_req->p_destructor)(p_coll_req);
    }
  if(p_coll_req->p_notify == NULL)
    {
      nm_cond_destroy(&p_coll_req->status);
    }
  padico_free(p_coll_req);
}

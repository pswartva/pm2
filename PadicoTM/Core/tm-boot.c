/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief PadicoTM Boot
 * @ingroup TaskManager
 */

#include <Padico/Puk.h>
#include <Padico/Module.h>
#include "PM2.h"
#include "TaskManager-internals.h"

#include <unistd.h>

static int padico_taskmanager_init(void);
static void padico_taskmanager_finalize(void);
static int padico_taskmanager_run(int argc, char**argv);

PADICO_MODULE_BUILTIN(PadicoTM, &padico_taskmanager_init, &padico_taskmanager_run, &padico_taskmanager_finalize);

extern void padico_tm_init(void);
extern void padico_tm_main(void);

static void* padico_tm_thread_self(void);
static void tm_puk_acquire_lock(void);
static void tm_puk_release_lock(void);

#ifdef PADICO_MARCEL
static void padico_tm_main_lock_acquire(void);
static void padico_tm_main_lock_release(void);
static int  padico_tm_main_lock_trylock(void);
#endif

static struct
{
  pmarcel_mutex_t puk_lock; /**< lock for component base */
  marcel_t puk_lock_owner;
  int puk_lock_level;
  marcel_mutex_t lock;
  marcel_cond_t event;
  int ready;                /**< ready flag for loader that needs synch */
} tm_boot = { .ready = 0 };

/* ********************************************************* */

void padico_tm_init(void)
{
  puk_mod_t tm_mod = NULL;
  marcel_mutex_init(&tm_boot.lock, NULL);
  marcel_cond_init(&tm_boot.event, NULL);
  padico_puk_mod_resolve(&tm_mod, "PadicoTM");
  assert(tm_mod == padico_module_self());
  padico_puk_mod_load(tm_mod);
}

void padico_tm_main(void)
{
  puk_mod_t tm_mod = padico_module_self();
  struct puk_job_s job;
  padico_tm_thread_givename("PadicoTM_main");
  padico_puk_mod_start(tm_mod, &job);
}

void padico_tm_finalize(void)
{
  padico_tm_task_engine_finalize();
  /* disable for now- let puk shutdown unload all modules */
  /*
    puk_mod_t tm_mod = padico_module_self();
    padico_puk_mod_unload(tm_mod);
  */
#ifdef PIOMAN
  /* pioman_exit(); */
#endif /* PIOMAN */
}

void padico_tm_ready_wait(void)
{
  marcel_mutex_lock(&tm_boot.lock);
  while(!tm_boot.ready)
    {
      marcel_cond_wait(&tm_boot.event, &tm_boot.lock);
    }
  marcel_mutex_unlock(&tm_boot.lock);
}

void padico_tm_ready_signal(void)
{
  marcel_mutex_lock(&tm_boot.lock);
  tm_boot.ready = 1;
  marcel_cond_signal(&tm_boot.event);
  marcel_mutex_unlock(&tm_boot.lock);
}


static void padico_taskmanager_finalize(void)
{
  padico_print("taskmanager finalize.\n");
  puk_lock_set_handlers(NULL, NULL);
}

static int padico_taskmanager_init(void)
{
  puk_trace_breakpoint();
  const puk_mod_t puk = puk_mod_getbyname("Puk");
  if(puk == NULL)
    padico_fatal("cannot find Puk module.\n");
  puk_mod_set_ancestor(padico_module_self(), puk);
  padico_puk_add_path(PADICOTM_ROOT);
  const char*node_dir = padico_getenv("PADICO_NODE_DIR");
  if(node_dir)
    {
      padico_string_t s_session_dir = padico_string_new();
      padico_string_printf(s_session_dir, "%s/session", node_dir);
      padico_puk_add_path(padico_string_get(s_session_dir));
      padico_string_delete(s_session_dir);
    }
  padico_out(40, "*** BOOT *************************\n");

  /* init PM2 */
  {
#if (defined(PADICO_MARCEL) || defined(PIOMAN))
#ifdef PADICO_MARCEL
    int fake_argc = 1;
    char*_fake_argv[] = { "padico-d.bin", NULL };
    char**fake_argv = _fake_argv;
    marcel_init(&fake_argc, fake_argv);
#endif /* PADICO_MARCEL */
#ifdef PIOMAN
    pioman_init();
#endif /* PIOMAN */
    padico_out(50, "PM2 init- ok.\n");
#endif /* PADICO_MARCEL || PIOMAN */
  }
  /* ** Init ABI and locking */
#ifdef PADICO_MARCEL
#if 0
  padico_tm_main_lock_acquire();
  puk_abi_set_spinlock_handlers(&padico_tm_main_lock_acquire,
                                &padico_tm_main_lock_release,
                                &padico_tm_main_lock_trylock);
#ifdef marcel_errno
  puk_abi_set_errno_handler(&marcel___errno_location);
#else
  puk_abi_set_errno_handler(&padico_tm_geterrno);
#endif /* marcel_errno */
  padico_tm_main_lock_release();
#endif
  puk_abi_set_virtual(sleep,     pmarcel_sleep);
  puk_abi_set_virtual(usleep,    pmarcel_usleep);
  puk_abi_set_virtual(nanosleep, pmarcel_nanosleep);
#endif /* PADICO_MARCEL */
  puk_trace_setplugin(&padico_tm_thread_self);
  puk_abi_seterrno(0);
  pmarcel_mutexattr_t attrs;
  pmarcel_mutexattr_init(&attrs);
  pmarcel_mutexattr_settype(&attrs, PMARCEL_MUTEX_RECURSIVE_NP);
  pmarcel_mutex_init(&tm_boot.puk_lock, &attrs);
  tm_boot.puk_lock_owner = (marcel_t)NULL;
  tm_boot.puk_lock_level = 0;
  puk_lock_set_handlers(&tm_puk_acquire_lock, &tm_puk_release_lock);
  padico_out(50, "spinlock handlers init ok.\n");

  /* Init PadicoTM subsystems */
  padico_tm_xml_init();         /* XML command steering */
  padico_tm_task_engine_init(); /* init & start TaskEngine */

  /* enqueue startup scripts from etc/padico/init.d/ */
  const char*s_startup_seq = padico_getenv("PADICO_STARTUP");
  if((s_startup_seq != NULL) && (strlen(s_startup_seq) > 0))
    {
      char*startup = padico_strdup(s_startup_seq);
      const char*init_script = strtok(startup, ":");
      while(init_script)
        {
          padico_task_t t = padico_malloc(sizeof(struct padico_task_s));
          t->task_req = NULL;
          t->kind = PADICO_TM_TASK_FILE;
          t->tinfo.ftask.file = padico_strdup(init_script);
          padico_tm_task_enqueue(t, init_script);
          init_script = strtok(NULL, ":");
        }
      padico_free(startup);
    }
  else
    {
      padico_out(puk_verbose_notice, "empty startup sequence. Immediate ready signal.\n");
      padico_tm_ready_signal();
    }
  return 0;
}

/* *** PukABI plugin *************************************** */

#ifdef PADICO_MARCEL
#ifndef marcel_errno
static int* padico_tm_geterrno(void)
{
  return &(marcel_self()->__errno);
}
#endif
static volatile int mainlock_count = 0;
static void padico_tm_main_lock_acquire(void)
{
  marcel_extlib_protect();
  const int oldcount = __sync_fetch_and_add(&mainlock_count, 1);
  if(oldcount < 0)
    {
      PUK_ABI_REAL(fprintf)(stderr, "PadicoTM: unbalanced main lock usage detected while locking: %d\n", mainlock_count);
      abort();
    }
}
static void padico_tm_main_lock_release(void)
{
  const int oldcount = __sync_fetch_and_sub(&mainlock_count, 1);
  if(oldcount <= 0)
    {
      PUK_ABI_REAL(fprintf)(stderr, "PadicoTM: unbalanced main lock usage detected while unlocking: %d\n", mainlock_count);
      abort();
    }
  marcel_extlib_unprotect();
}
static int padico_tm_main_lock_trylock(void)
{
  marcel_extlib_protect();
  __sync_fetch_and_add(&mainlock_count, 1);
  return 1;
}
#endif /* PADICO_MARCEL */

/* *** Puk traces plugin *********************************** */

static void* padico_tm_thread_self(void)
{
  return (void*)(((unsigned long)marcel_self())>>16);
}

/* *** PadicoTM components ********************************* */

static void tm_puk_acquire_lock(void)
{
  pmarcel_mutex_lock(&tm_boot.puk_lock);
  tm_boot.puk_lock_level++;
  if(tm_boot.puk_lock_level == 1)
    {
      assert(tm_boot.puk_lock_owner == (marcel_t)NULL);
      tm_boot.puk_lock_owner = marcel_self();
    }
  else
    {
      assert(tm_boot.puk_lock_owner == marcel_self());
    }
}

static void tm_puk_release_lock(void)
{
  tm_boot.puk_lock_level--;
  if(tm_boot.puk_lock_level == 0)
    {
      assert(tm_boot.puk_lock_owner == marcel_self());
      tm_boot.puk_lock_owner = (marcel_t)NULL;
    }
  else
    {
      assert(tm_boot.puk_lock_owner == marcel_self());
    }
  pmarcel_mutex_unlock(&tm_boot.puk_lock);
}


/* *** PadicoTM boot *************************************** */

static int padico_taskmanager_run(int argc __attribute__((unused)), char**argv __attribute__((unused)))
{
  padico_out(50, "starting...\n");

  /* this is the main loop */
  padico_tm_task_engine_run();

  padico_print("exit.\n");
#if 0
  padico_puk_shutdown();
  padico_print("switching off multi-threading scheduler...\n");
  common_exit(&pm2_attr);
#endif
  padico_print("ok\n");
  exit(0);

  padico_fatal("after taskmanager_main()- We should NEVER reach here.\n");
  return -1; /* if we reach there, it's an error */
}


/* *** multi-threading ************************************* */


int padico_tm_nanosleep(const struct timespec*req, struct timespec*rem)
{
#ifdef PADICO_MARCEL
  const unsigned long msec = req->tv_sec * 1000L + req->tv_nsec / 1000000;
  marcel_delay(msec);
  if(rem)
    {
      padico_warning("nanosleep remaining time ignored.\n");
    }
  return 0;
#else
  return puk_nanosleep(req, rem);
#endif
}

void padico_tm_usleep(long usec)
{
#ifdef PADICO_MARCEL
  const unsigned long msec = usec / 1000;
  marcel_delay(msec);
#else
  puk_usleep(usec);
#endif
}

void padico_tm_msleep(long msec)
{
  padico_tm_usleep(1000 * msec);
}

unsigned int padico_tm_sleep(unsigned long seconds)
{
  const unsigned long msec = seconds * 1000L;
  padico_tm_msleep(msec);
  return 0;
}

void padico_tm_yield(void)
{
#ifdef PADICO_MARCEL
  marcel_yield();
#else
  sched_yield();
#endif
}

void padico_tm_thread_givename_tid(const char*name, marcel_t tid)
{
#ifdef PADICO_PTHREAD
#if defined(__GLIBC__) && defined(__GLIBC_MINOR__) && ((__GLIBC__ == 2 && __GLIBC_MINOR__ >= 12) || (__GLIBC__ > 2))
  int rc = pthread_setname_np(tid, name);
  if(rc)
    padico_print("cannot set thread name: *%s* rc = %d\n", name, rc);
#else
#warning pthread_setname_np not available
#endif
#endif
#ifdef PADICO_MARCEL
  marcel_setname(tid, name);
#endif
}

void padico_tm_thread_givename(const char*name)
{
  padico_tm_thread_givename_tid(name, marcel_self());
}

void padico_tm_thread_top(void)
{
#ifdef PADICO_MARCEL
#if (MARCEL_VERSION >= 0x020000)
  marcel_init_top("|xterm -geometry 110x30 -title marcel:top -S//0");
#endif
#endif
}

/*
 * NewMadeleine
 * Copyright (C) 2016-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


/** @file example that mimics the communication scheme of coll_dynamic.
 * Used only to ensure no regression occurs for this precise communication pattern.
 */

// Buggy example
// mpirun -d -c -p -DNMAD_DRIVER=tcp -np 2 ./nm_rpc_coll_dynamic_data
#include <stdlib.h>
#include <stdio.h>

#include <nm_launcher_interface.h>
#include <nm_coll_interface.h>
#include <nm_rpc_interface.h>

#define NX 100
#define FAKE_DEST_NODE 24
#define FAKE_SRC_NODE 12


struct len_buf_s {
  int* buf;
  int* nodes;
  nm_len_t len;
};

const nm_tag_t tag = 0x02;

struct multicast_header_s
{
  nm_len_t body_len;
  int nb_nodes;
  int src_node;
  int seq;
};


/** handler called upon incoming RPC request */
static void rpc_hello_handler(nm_rpc_token_t p_token)
{
  struct len_buf_s* data = malloc(sizeof(struct len_buf_s));

  struct multicast_header_s header;

  nm_rpc_recv_header(p_token, &header, sizeof(struct multicast_header_s));
  printf("header.nb_nodes: %d\n", header.nb_nodes);
  printf("header.src_node: %d\n", header.src_node);
  printf("header.body_len: %lu\n", header.body_len);

  assert(header.src_node == FAKE_SRC_NODE);
  assert(header.nb_nodes == 1);
  assert(header.body_len == NX * sizeof(int));

  data->buf = malloc(header.body_len);
  data->nodes = malloc(header.nb_nodes * sizeof(int));
  nm_rpc_token_set_ref(p_token, data);
  nm_rpc_irecv_body(p_token, data->nodes, header.nb_nodes * sizeof(int));
  nm_rpc_irecv_body(p_token, data->buf, header.body_len);
}

/** handler called after all data for the incoming RPC request was received */
static void rpc_hello_finalizer(nm_rpc_token_t p_token)
{
  struct len_buf_s* data = nm_rpc_token_get_ref(p_token);

  assert(data->nodes[0] == FAKE_DEST_NODE);

  for(int i = 0; i < NX; i++)
    {
      assert(data->buf[i] == i);
    }
  free(data->buf);
  free(data->nodes);
  free(data);
}

int main(int argc, char**argv)
{
  /* generic nmad init */
  nm_launcher_init(&argc, argv);
  nm_session_t p_session = NULL;
  nm_session_open(&p_session, "nm_rpc_hello");
  int size = 0;
  nm_launcher_get_size(&size);
  int rank = -1;
  nm_launcher_get_rank(&rank);
  int peer = (size > 1) ? (rank + size - 1) % size : rank;
  nm_gate_t p_gate = NULL;
  nm_launcher_get_gate(peer, &p_gate);

  nm_comm_t p_comm = nm_comm_world("nm_rpc_hello");

  /* register the RPC service */
  nm_rpc_service_t p_service = nm_rpc_register(p_session, tag, NM_TAG_MASK_FULL,
                                               &rpc_hello_handler, &rpc_hello_finalizer, NULL);

  struct multicast_header_s header;
  header.body_len = NX * sizeof(int);
  header.src_node = FAKE_SRC_NODE;
  header.nb_nodes = 1;

  int* nodes = malloc(sizeof(int));
  nodes[0] = 24;

  int* data = malloc(NX * sizeof(int));
  for(int i = 0; i < NX; i++)
    {
      data[i] = i;
    }

  /* create rpc request */
  nm_rpc_req_t p_req = nm_rpc_req_init(p_service, p_gate, tag);
  nm_rpc_req_pack_header(p_req, &header, sizeof(struct multicast_header_s));
  nm_rpc_req_pack_body(p_req, nodes, sizeof(int)); //body_data ?
  nm_rpc_req_pack_body(p_req, data, NX * sizeof(int));
  nm_rpc_req_isend(p_req);
  nm_rpc_req_wait(p_req);

  /* generic nmad termination */
  nm_coll_barrier(p_comm, 0xF2);

  free(nodes);
  free(data);
  nm_rpc_unregister(p_service);
  nm_comm_destroy(p_comm);
  nm_session_close(p_session);
  nm_launcher_exit();
  return 0;

}

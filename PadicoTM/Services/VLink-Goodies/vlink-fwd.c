/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <Padico/Puk.h>
#include <Padico/VLink-internals.h>
#include <Padico/Module.h>
#include <Padico/Topology.h>

static int vlink_fwd_module_init(void);
static void vlink_fwd_module_finalize(void);

PADICO_MODULE_DECLARE(VLink_fwd, vlink_fwd_module_init, NULL, vlink_fwd_module_finalize);

/* VLink_fwd Vlink-driver functions */
static void vlink_fwd_init        (padico_vnode_t);

static int  vlink_fwd_create      (padico_vnode_t);
static int  vlink_fwd_bind        (padico_vnode_t);
static int  vlink_fwd_listen      (padico_vnode_t);
static int  vlink_fwd_connect     (padico_vnode_t);
static void vlink_fwd_fullshutdown(padico_vnode_t);
static void vlink_fwd_send_post   (padico_vnode_t);
static void vlink_fwd_recv_post   (padico_vnode_t);

/** @defgroup VLinkfwd component: VLink_fwd- VLink over VLink "through-a-node" component
 * @ingroup PadicoComponent
 */

static puk_component_t vlink_fwd_component = NULL;

#define AF_PADICO_VLFW AF_PADICO_MAGIC('V', 'L', 'F', 'W')

static const struct padico_vlink_driver_s vlink_fwd_driver =
{
  .create       = &vlink_fwd_create,
  .bind         = &vlink_fwd_bind,
  .listen       = &vlink_fwd_listen,
  .connect      = &vlink_fwd_connect,
  .fullshutdown = &vlink_fwd_fullshutdown,
  .close        = NULL,
  .send_post    = &vlink_fwd_send_post,
  .recv_post    = &vlink_fwd_recv_post,
  .dumpstat     = NULL
};

struct vlink_lookup_s
{
  int id;
  char uuid[PADICO_TOPO_UUID_SIZE+1];
};

struct vlink_fwd_specific_s
{
  int id;
  char uuid[PADICO_TOPO_UUID_SIZE+1];
  padico_vnode_t vnode;
  padico_topo_node_t gw;
};

struct gw_half_s
{
  padico_vnode_t vnode;
  padico_topo_node_t node;
  int*condition;
  marcel_mutex_t*mutex;
  marcel_cond_t*cond;
  marcel_t thread;
};

struct gw_status_s
{
  int id;
  char uuid[PADICO_TOPO_UUID_SIZE+1];
  struct gw_half_s*to_gw;
  struct gw_half_s*from_gw;
};

static puk_hashtable_t status_table = NULL;
static marcel_mutex_t status_mutex;
static marcel_cond_t status_cond;

VLINK_CREATE_SPECIFIC_ACCESSORS(fwd);

static void vlink_fwd_gw_notifier(padico_vnode_t, vobs_attr_t, void*);
static void vlink_fwd_notifier(padico_vnode_t, vobs_attr_t, void*);
static void vlink_fwd_commit(padico_vnode_t, vobs_attr_t);

static void vlink_fwd_connect_handler(puk_parse_entity_t);
static void vlink_fwd_gwlisten_handler(puk_parse_entity_t);
static void vlink_fwd_listen_handler(puk_parse_entity_t);

static void vlink_status_register(void*);
static void*vlink_status_lookup(const char*, int);
static uint32_t vlink_hash(const void*);
static int vlink_eq(const void*, const void*);

/* ********************************************************* */

static int vlink_fwd_module_init(void)
{
  padico_out(30,"VLink-Fwd initialisation\n");

  marcel_mutex_init(&status_mutex, NULL);
  marcel_cond_init(&status_cond, NULL);
  status_table = puk_hashtable_new(&vlink_hash, &vlink_eq);

  vlink_fwd_component =
    puk_component_declare("VLink_fwd",
                          puk_component_provides("VLink", "vlink", &vlink_fwd_driver),
                          puk_component_provides("PadicoComponent", "component", &padico_vnode_component_driver),
                          puk_component_attr("gateway_nodename", NULL));
  puk_xml_add_action((struct puk_tag_action_s){
       .xml_tag        = "VLink_fwd:Connect",
       .start_handler  = NULL,
       .end_handler    = &vlink_fwd_connect_handler,
       .required_level = PUK_TRUST_CONTROL
  });
  puk_xml_add_action((struct puk_tag_action_s){
       .xml_tag        = "VLink_fwd:GwListen",
       .start_handler  = NULL,
       .end_handler    = &vlink_fwd_gwlisten_handler,
       .required_level = PUK_TRUST_CONTROL
  });
  puk_xml_add_action((struct puk_tag_action_s){
       .xml_tag        = "VLink_fwd:Listen",
       .start_handler  = NULL,
       .end_handler    = &vlink_fwd_listen_handler,
       .required_level = PUK_TRUST_CONTROL
  });
  return 0;
}

static void vlink_fwd_module_finalize(void)
{
  puk_component_destroy(vlink_fwd_component);
}

/* ********************************************************* */

static uint32_t vlink_hash(const void*key)
{
  return puk_hash_default(key, sizeof(int)+sizeof(char)*(PADICO_TOPO_UUID_SIZE+1));
}

static int vlink_eq(const void*key1, const void*key2)
{
  return ((*((int*)key1)==*((int*)key2)) &&
          !(strcmp((char*)(key1+sizeof(int)),(char*)(key2+sizeof(int)))));
}

static void vlink_status_register(void*status)
{
  marcel_mutex_lock(&status_mutex);
  puk_hashtable_insert(status_table, status, (void*)status);
  marcel_mutex_unlock(&status_mutex);
}

static void*vlink_status_lookup(const char*uuid, int id)
{
  void*status = NULL;
  struct vlink_lookup_s tmp;
  tmp.id = id;
  strcpy((tmp.uuid), uuid);
  marcel_mutex_lock(&status_mutex);
  status = puk_hashtable_lookup(status_table, &tmp);
  marcel_mutex_unlock(&status_mutex);
  return status;
}

static int status_num_count = 0;

static int generate_status_num()
{
  /* Generate a local status number */
  int ret = -1;
  marcel_mutex_lock(&status_mutex);
  ret = status_num_count++;
  marcel_mutex_unlock(&status_mutex);
  return ret;
}

/* ********************************************************** */

static void vlink_fwd_commit(padico_vnode_t vnode, vobs_attr_t attr)
{
  /* Tracks changes to our attributes done by the user */
  struct vlink_fwd_specific_s*status = vobs_fwd(vnode);
  padico_out(30,"Commit:%d\n", attr);
  switch(attr)
    {
    case vobs_attr_rbox:
      padico_print("RECV-COMMIT !\n");
      status->vnode->content.rbox = vnode->content.rbox;
      status->vnode->rbox = vnode->rbox;
      vobs_commit(status->vnode, attr);
      break;
    case vobs_attr_wbox:
      padico_print("SEND-COMMIT !\n");
      status->vnode->content.rbox = vnode->content.rbox;
      status->vnode->wbox = vnode->wbox;
      vobs_commit(status->vnode, attr);
      break;
    default:
      padico_fatal("Unexpected attribute (%d) in vlink_fwd_commit\n", attr);
      break;
    }
}

static void vlink_fwd_notifier(padico_vnode_t sub, vobs_attr_t attr, void*key)
{
  /* Tracks changes in sub-vnode's attributes */
  // We're on dest or source node
  padico_vnode_t vnode = (padico_vnode_t)key;
  struct vlink_fwd_specific_s*status = vobs_fwd(vnode);

  padico_print("Notifier:%d\n", attr);

  switch(attr)
    {
    case vobs_attr_status:
      vnode->status = status->vnode->status;
      vobs_signal(vnode, attr);
      break;
    case vobs_attr_remote:
      // i'm connected to Gw, warn the upper Vnode
      vnode->status          = sub->status;
      vnode->content.remote  = sub->content.remote;
      vnode->remote          = sub->remote;
      vnode->content.binding = sub->content.binding;
      vnode->binding         = sub->binding;
      padico_out(30, "vnode=%p; sub=%p vnode.established=%d sub.established=%d\n",
                 vnode, sub, vnode->remote.established, sub->remote.established);
      vobs_signal(vnode, attr);
      break;
    case vobs_attr_listener:
      // Gw connected to us
      vnode->status = sub->status;
      if(!padico_vnode_queue_empty(sub->listener.backlog))
        {
          padico_vnode_t new_vnode = padico_vnode_queue_retrieve(sub->listener.backlog);
          status->vnode = new_vnode;
          padico_vsock_init(new_vnode,
                            sub->socket.so_family,
                            sub->socket.so_type,
                            sub->socket.so_protocol);
          vnode_lock(new_vnode);
          vnode_activate(new_vnode, binding);
          vnode_activate(new_vnode, remote);
          vnode_set_error(new_vnode, 0);
          new_vnode->status.connecting     = 0;
          new_vnode->remote.established    = 1;
          new_vnode->remote.can_recv       = 1;
          new_vnode->remote.can_send       = 1;
          padico_vnode_queue_append(vnode->listener.backlog, new_vnode);
          vobs_subscribe(new_vnode, &vlink_fwd_notifier, vnode);
          vnode_unlock(new_vnode);
          vobs_signal(vnode, vobs_attr_listener);
        }
      else
        {
          padico_warning("Empty backlog in VLink_fwd notifier\n");
        }
      break;
    case vobs_attr_rbox:
      padico_print("RECEIVED !\n");
      vnode->rbox = sub->rbox;
      vobs_signal(vnode, attr);
      break;
    case vobs_attr_wbox:
      padico_print("SENT !\n");
      vnode->wbox = sub->wbox;
      //      vobs_signal(vnode, attr);
      break;
    default:
      padico_fatal("Unexpected attribute (%d) in vlink_fwd_notifier\n", attr);
      break;
    }
}

struct thread_arg
{
  struct gw_half_s*read;
  struct gw_half_s*write;
};

static void*comm_thread(void*arg)
{
  struct thread_arg*args = arg;
  int read = 0;

  padico_out(30,"Communication thread started.\n");

  // Wait until connexion is ready
  marcel_mutex_lock(args->read->mutex);
  while(!*(args->read->condition))
    {
      marcel_cond_wait(args->read->cond, args->read->mutex);
    }
  *(args->read->condition) = 0;
  marcel_mutex_unlock(args->read->mutex);

  // Make comms progress
  while(1)
    {
      char buffer[1024];
      read = padico_vnode_read_post(args->read->vnode, buffer, 1024*sizeof(char));
      if( read > 0)
        {
          padico_print("Read done:%d\n", read);
          size_t rc = 0;
          while(read)
            {
              rc = padico_vnode_write_post(args->write->vnode, buffer+rc, read);
              read -= rc;
            }
          padico_print("Transmitted.\n");
          padico_vnode_read_clear(args->read->vnode);
        } else {
          marcel_cond_wait(args->read->cond, args->read->mutex);
        }
    }
  return NULL;
}

static void begin_comm_thread(struct gw_half_s*read, struct gw_half_s*write)
{
  struct thread_arg*args = padico_malloc(sizeof(struct thread_arg));
  assert(read && write);
  args->read = read;
  args->write = write;
  marcel_create(&read->thread, NULL, &comm_thread, args);

  return;
}

static void vlink_fwd_gw_notifier(padico_vnode_t vnode, vobs_attr_t attr, void*key)
{
  /* Tracks changes in to_gw_vnode's or from_gw_vnode's attributes */
  // We're on Gateway
  struct gw_status_s*gw_status = (struct gw_status_s*)key;

  padico_print("GwNotifier:%d\n", attr);

  switch(attr)
    {
    case vobs_attr_status:
      // Do nothing: change of status in a underlying VLink
      if(vnode->status.readable)
        {
          if(vnode == gw_status->to_gw->vnode)
            {
              marcel_mutex_lock(gw_status->to_gw->mutex);
              *gw_status->to_gw->condition = 1;
              marcel_mutex_unlock(gw_status->to_gw->mutex);
              marcel_cond_signal(gw_status->to_gw->cond);
            } else {
              assert(vnode == gw_status->from_gw->vnode);
              marcel_mutex_lock(gw_status->from_gw->mutex);
              *gw_status->from_gw->condition = 1;
              marcel_mutex_unlock(gw_status->from_gw->mutex);
              marcel_cond_signal(gw_status->from_gw->cond);
            }
        }
      break;
    case vobs_attr_remote:
      assert(vnode == gw_status->from_gw->vnode);
      // Do nothing: i'm connected to dest node
      break;
    case vobs_attr_listener:
      if(!padico_vnode_queue_empty(vnode->listener.backlog))
        {
          padico_vnode_t serv_vnode = gw_status->to_gw->vnode;
          padico_vnode_t new_vnode = padico_vnode_queue_retrieve(vnode->listener.backlog);
          gw_status->to_gw->vnode = new_vnode;
          padico_vsock_init(new_vnode,
                            vnode->socket.so_family,
                            vnode->socket.so_type,
                            vnode->socket.so_protocol);
          vnode_lock(new_vnode);
          vnode_activate(new_vnode, binding);
          vnode_activate(new_vnode, remote);
          vnode_set_error(new_vnode, 0);
          new_vnode->status.connecting     = 0;
          new_vnode->remote.established    = 1;
          new_vnode->remote.can_recv       = 1;
          new_vnode->remote.can_send       = 1;
          padico_vnode_queue_append(serv_vnode->listener.backlog, new_vnode);
          vobs_subscribe(new_vnode, &vlink_fwd_gw_notifier, gw_status);
          vnode_unlock(new_vnode);
        }
      else
        {
          padico_warning("Empty backlog in VLink_fwd notifier\n");
        }
      break;
    case vobs_attr_rbox:
      assert(gw_status->to_gw->vnode != NULL && gw_status->from_gw->vnode != NULL);
      // We already posted a recv.
      if(vnode == gw_status->to_gw->vnode)
        {
          marcel_cond_signal(gw_status->to_gw->cond);
        } else {
          marcel_cond_signal(gw_status->from_gw->cond);
        }
      break;
    case vobs_attr_wbox:
      assert(gw_status->to_gw->vnode != NULL && gw_status->from_gw->vnode != NULL);
      // We already posted a send.
      if(vnode == gw_status->to_gw->vnode)
        {
          marcel_cond_signal(gw_status->to_gw->cond);
        } else {
          marcel_cond_signal(gw_status->from_gw->cond);
        }
      break;
    default:
      padico_fatal("Unexpected attribute (%d) in vlink_fwd_gw_notifier\n", attr);
      break;
    }
}

/* ********************************************************** */

static void vlink_fwd_init(padico_vnode_t vnode)
{
  vobs_fwd_activate(vnode);
  struct vlink_fwd_specific_s*status = vobs_fwd(vnode);

  // Get a local ID
  status->id = generate_status_num();
  padico_topo_uuid_t uuid = padico_topo_node_getuuid(padico_topo_getlocalnode());
  strcpy(status->uuid, (char*)uuid);

  // Get a reference on Gateway
  const puk_context_t context = padico_vnode_get_context(vnode);
  const char*gateway_nodename = puk_context_getattr(context, "gateway_nodename");
  status->gw = padico_topo_getnodebyname(gateway_nodename);

  // Create the underlying vnode
  puk_component_t gw_component =
    padico_ns_serial_selector(status->gw, "default", puk_iface_VLink());
  status->vnode = padico_vnode_instantiate_entry(gw_component, padico_module_self());
  vobs_subscribe(status->vnode, &vlink_fwd_notifier, vnode);
  padico_out(30,"Vnode created: %s\n", padico_vnode_component_name(status->vnode));

  // Set commit function
  vobs_set_commit(vnode, &vlink_fwd_commit);
}

static int vlink_fwd_create(padico_vnode_t vnode)
{
  return 0;
}

struct sockaddr_vlink_fwd
{
  uint16_t idl;
  char uuid[PADICO_TOPO_UUID_SIZE];
};

static int vlink_fwd_bind(padico_vnode_t vnode)
{
  vlink_fwd_init(vnode);
  // Prepare the vnode
  struct vlink_fwd_specific_s*status = vobs_fwd(vnode);
  vlink_status_register(status);

  // Prepare the AF_PADICO Address
  struct sockaddr_pa*addr = padico_malloc(sizeof(struct sockaddr_pa));
  socklen_t addrlen = sizeof(struct sockaddr_pa);
  struct sockaddr_vlink_fwd*addr_fwd =
    padico_sockaddr_payload(struct sockaddr_vlink_fwd, addr);
  padico_sockaddr_init(addr, &addrlen, AF_PADICO_VLFW);

  addr_fwd->idl = status->id;
  padico_topo_uuid_t uuid = padico_topo_node_getuuid(padico_topo_getlocalnode());
  strcpy(addr_fwd->uuid, (char*)uuid);

  vnode->binding.primary     = (struct sockaddr*)addr;
  vnode->binding.primary_len = addrlen;

  // Do bind underlying node
  // Create
  int rc = padico_vsock_create(status->vnode, AF_INET, SOCK_STREAM, 0);
  if(rc >= 0)
    return padico_vsock_autobind(status->vnode);
  else return rc;

  padico_free(addr);

  //dead end
  return -1;
}

static int vlink_fwd_connect(padico_vnode_t vnode)
{
  /* Dest node don't connect */
  // First time initialisation
  vlink_fwd_init(vnode);
  struct vlink_fwd_specific_s*status = vobs_fwd(vnode);

  // Check arriving address
  vnode_check(vnode, remote);
  const struct sockaddr_pa*paddr = (struct sockaddr_pa*)vnode->remote.remote_addr;
  const struct sockaddr_vlink_fwd *addr =
    padico_sockaddr_payload(struct sockaddr_vlink_fwd, paddr);

  status->id = addr->idl;
  strcpy(status->uuid, addr->uuid);
  vlink_status_register(status);

  // Create
  padico_vsock_create(status->vnode, AF_INET, SOCK_STREAM, 0);

  padico_string_t s_launch = padico_string_new();
  padico_topo_uuid_t uuid = padico_topo_node_getuuid(padico_topo_getlocalnode());
  // Switch depending on interoperability
  if(((struct sockaddr*)paddr)->sa_family == AF_PADICO)
    {
      /* AF_PADICO case. Dest is a PADICO node */
      padico_string_catf(s_launch, "<VLink_fwd:Connect Uuid=\"%s\" Idl=\"%d\" MyUuid=\"%s\" />",
                         addr->uuid, addr->idl, (char*)uuid);
    } else {
      /* AF_INET case. Dest ask for socket interoperability */
      assert(((struct sockaddr*)paddr)->sa_family == AF_INET);
      const uint16_t port = ((struct sockaddr_in*) (vnode->binding).primary)->sin_port;
      padico_string_catf(s_launch, "<VLink_fwd:Connect Uuid=\"%s\" Idl=\"%d\" MyUuid=\"%s\" Dport=\"%d\" />",
                         addr->uuid, addr->idl, (char*)uuid, port);
    }

  // Send the message to Gw
  padico_rc_t padico_rc;
  padico_req_t req = padico_control_send(status->gw, padico_string_get(s_launch));
  padico_rc = padico_tm_req_wait(req);
  padico_string_delete(s_launch);

  int err = padico_rc_iserror(padico_rc);
  if(err != 0)
    {
      padico_warning("Error: Message \"Connect\" not arrived: %s.\n",
                     padico_rc_gettext(padico_rc));
      err = -1;
    }
  padico_rc_delete(padico_rc);

  return err;
}

struct gw_status_s*gw_status_new(char*s_uuid, char*s_srcuuid, int id)
{
  // Init
  struct gw_status_s*status = padico_malloc(sizeof(struct gw_status_s));
  status->id = id;
  strcpy(status->uuid, (char*)s_uuid);

  status->to_gw = padico_malloc(sizeof(struct gw_half_s));
  status->from_gw = padico_malloc(sizeof(struct gw_half_s));
  status->to_gw->mutex = padico_malloc(sizeof(marcel_mutex_t));
  status->from_gw->mutex = padico_malloc(sizeof(marcel_mutex_t));
  marcel_mutex_init(status->to_gw->mutex, NULL);
  marcel_mutex_init(status->from_gw->mutex, NULL);
  status->to_gw->cond = padico_malloc(sizeof(marcel_cond_t));
  status->from_gw->cond = padico_malloc(sizeof(marcel_cond_t));
  marcel_cond_init(status->to_gw->cond, NULL);
  marcel_cond_init(status->from_gw->cond, NULL);
  status->to_gw->vnode = NULL;
  status->from_gw->vnode = NULL;
  status->to_gw->condition = padico_malloc(sizeof(int));
  status->from_gw->condition = padico_malloc(sizeof(int));
  *status->to_gw->condition = 0;
  *status->from_gw->condition = 0;

  // Prepare dest_node
  padico_topo_node_t dest_node = padico_topo_getnodebyuuid((padico_topo_uuid_t)s_srcuuid);
  status->from_gw->node = dest_node;
  assert(status->from_gw->node);
  vlink_status_register(status);

  // Instanciation (source side)
  if(s_srcuuid)
    {
      padico_topo_node_t src_node = padico_topo_getnodebyuuid((padico_topo_uuid_t)s_srcuuid);
      status->to_gw->node = src_node;
      puk_component_t src_component =
        padico_ns_serial_selector(src_node, "default", puk_iface_VLink());
      status->to_gw->vnode = padico_vnode_instantiate_entry(src_component, padico_module_self());
      vobs_subscribe(status->to_gw->vnode, &vlink_fwd_gw_notifier, status);
    }

  // Return
  return status;
}

static void vlink_fwd_connect_handler(puk_parse_entity_t e)
{
  const char*s_uuid = puk_parse_getattr(e, "Uuid");
  const char*s_srcuuid = puk_parse_getattr(e, "MyUuid");
  const char*s_idl = puk_parse_getattr(e, "Idl");
  const char*s_port = puk_parse_getattr(e, "Dport");
  assert(s_idl && s_uuid && s_srcuuid);

  // Prepare structures
  struct gw_status_s*gw_status = vlink_status_lookup(s_uuid, atoi(s_idl));
  if(gw_status == NULL)
    gw_status = gw_status_new((char*)s_uuid, (char*)s_srcuuid, atoi(s_idl));
  else { // Instanciate source side...
    padico_topo_node_t src_node = padico_topo_getnodebyuuid((padico_topo_uuid_t)s_srcuuid);
    gw_status->to_gw->node = src_node;
    puk_component_t src_component =
      padico_ns_serial_selector(src_node, "default", puk_iface_VLink());
    gw_status->to_gw->vnode = padico_vnode_instantiate_entry(src_component, padico_module_self());
    vobs_subscribe(gw_status->to_gw->vnode, &vlink_fwd_gw_notifier, gw_status);
  }

  // Start comm threads
  begin_comm_thread(gw_status->to_gw, gw_status->from_gw);
  begin_comm_thread(gw_status->from_gw, gw_status->to_gw);

  // Listen
  int rc = padico_vsock_create(gw_status->to_gw->vnode, AF_INET, SOCK_STREAM, 0);
  if(rc != 0)
    { padico_warning("Create error on Gw\n"); }
  rc = padico_vsock_autobind(gw_status->to_gw->vnode);
  if(rc >= 0)
    {
      struct sockaddr*addr;
      addr = padico_malloc(sizeof(struct sockaddr));
      struct sockaddr_in*addr_in = (struct sockaddr_in*)addr;
      addr_in->sin_family = AF_INET;
      addr_in->sin_port = ((struct sockaddr_in*)gw_status->to_gw->vnode->binding.primary)->sin_port;
      addr_in->sin_addr = *padico_topo_host_getaddr(padico_topo_node_gethost(gw_status->to_gw->node));

      const padico_vaddr_t src_addr =
        padico_vaddr_new(addr, sizeof(struct sockaddr), 0);
      padico_out(30,"Listen to src_node...\n");
      padico_vsock_passive_connect(gw_status->to_gw->vnode, 2, src_addr);
      padico_free(addr);
      padico_out(30,"Connected.\n");
    }

  // Send a message to src_node
  padico_rc_t padico_rc;
  padico_string_t s_launch = padico_string_new();
  padico_string_catf(s_launch,"<VLink_fwd:GwListen Uuid=\"%s\" Idl=\"%d\" Port=\"%d\" />",
                     gw_status->uuid, gw_status->id,
                     ((struct sockaddr_in*)gw_status->to_gw->vnode->binding.primary)->sin_port);
  padico_req_t req = padico_control_send(gw_status->to_gw->node, padico_string_get(s_launch));
  padico_rc = padico_tm_req_wait(req);
  padico_string_delete(s_launch);

  int err = padico_rc_iserror(padico_rc);
  if(err != 0)
    { padico_warning("Message \"GwListen\" not arrived: %s\n", padico_rc_gettext(padico_rc)); }
  padico_rc_delete(padico_rc);

  // Connect the third node
  if(!s_port)
    {
      /* Dest is a PADICO node */
      padico_out(30,"Dest is a Padico Node. Waiting for \"Listen\" message.\n");
      // Do nothing. He will contact us
    } else {
      /* AF_INET case. Dest ask for socket interoperability */
      // Do the connect to designated port
      padico_out(30,"Dest is not a Padico Node. Connecting...\n");

      struct sockaddr*addr;
      addr = padico_malloc(sizeof(struct sockaddr));
      struct sockaddr_in*addr_in = (struct sockaddr_in*)addr;
      addr_in->sin_family = AF_INET;
      addr_in->sin_port = atoi(s_port);
      addr_in->sin_addr = *padico_topo_host_getaddr(padico_topo_node_gethost(gw_status->from_gw->node));

      padico_vsock_active_connect(gw_status->from_gw->vnode, addr, sizeof(struct sockaddr));
      padico_free(addr);
    }
}

static int vlink_fwd_listen(padico_vnode_t vnode)
{
  /* Source node don't listen */
  struct vlink_fwd_specific_s*status = vobs_fwd(vnode);

  // Send message to Gw to connect
  padico_rc_t padico_rc;
  padico_string_t s_launch = padico_string_new();
  padico_string_catf(s_launch,"<VLink_fwd:Listen Uuid=\"%s\" Idl=\"%d\" Port=\"%d\" />",
                     status->uuid, status->id,
                     ((struct sockaddr_in*)status->vnode->binding.primary)->sin_port);
  padico_req_t req = padico_control_send(status->gw, padico_string_get(s_launch));

  // Do listen
  struct sockaddr*addr;
  addr = padico_malloc(sizeof(struct sockaddr));
  struct sockaddr_in*addr_in = (struct sockaddr_in*)addr;
  addr_in->sin_family = AF_INET;
  addr_in->sin_port = ((struct sockaddr_in*)status->vnode->binding.primary)->sin_port;
  addr_in->sin_addr = *padico_topo_host_getaddr(padico_topo_node_gethost(status->gw));

  const padico_vaddr_t src_addr =
    padico_vaddr_new(addr, sizeof(struct sockaddr), 0);

  padico_out(30,"Listen for src_node(%p)...\n", addr);
  int rc = padico_vsock_passive_connect(status->vnode, 2, src_addr);
  if(rc < 0)
    { padico_warning("Listen error on dest node.\n"); }
  padico_out(30,"Connected.\n");
  padico_free(addr);

  padico_rc = padico_tm_req_wait(req);
  padico_string_delete(s_launch);
  int err = padico_rc_iserror(padico_rc);
  if(err != 0)
    { padico_warning("Message \"Listen\" not arrived: %s\n", padico_rc_gettext(padico_rc)); }
  padico_rc_delete(padico_rc);

  return rc;
}

static void vlink_fwd_gwlisten_handler(puk_parse_entity_t e)
{
  const char*s_uuid = puk_parse_getattr(e, "Uuid");
  const char*s_idl = puk_parse_getattr(e, "Idl");
  const char*s_port = puk_parse_getattr(e, "Port");
  assert(s_idl && s_uuid && s_port);

  // Get Status
  int idl = atoi(s_idl);
  int port = atoi(s_port);
  struct vlink_fwd_specific_s*status = vlink_status_lookup(s_uuid, idl);

  // Prepare adress
  struct sockaddr*addr;
  addr = padico_malloc(sizeof(struct sockaddr));
  struct sockaddr_in*addr_in = (struct sockaddr_in*)addr;
  addr_in->sin_family = AF_INET;
  addr_in->sin_port = port;
  addr_in->sin_addr = *padico_topo_host_getaddr(padico_topo_node_gethost(status->gw));

  // Connect
  padico_out(30,"Connecting to Gateway(%p)...\n", addr);
  padico_vsock_active_connect(status->vnode, addr, sizeof(struct sockaddr));
  padico_free(addr);
  padico_out(30,"Connected.\n");
}

static void vlink_fwd_listen_handler(puk_parse_entity_t e)
{
  const char*s_uuid = puk_parse_getattr(e, "Uuid");
  const char*s_idl = puk_parse_getattr(e, "Idl");
  const char*s_port = puk_parse_getattr(e, "Port");
  assert(s_idl && s_uuid && s_port);

  // Get status
  struct gw_status_s*gw_status = vlink_status_lookup(s_uuid, atoi(s_idl));
  if(gw_status == NULL)
    gw_status = gw_status_new((char*)s_uuid, NULL, atoi(s_idl));

  // Instanciate the way to dest node
  puk_component_t dest_component =
    padico_ns_serial_selector(gw_status->from_gw->node, "default", puk_iface_VLink());
  gw_status->from_gw->vnode = padico_vnode_instantiate_entry(dest_component, padico_module_self());
  vobs_subscribe(gw_status->from_gw->vnode, &vlink_fwd_gw_notifier, gw_status);

  int rc = padico_vsock_create(gw_status->from_gw->vnode, AF_INET, SOCK_STREAM, 0);
  if(rc != 0)
    padico_warning("Create error.\n");

  struct sockaddr*addr;
  addr = padico_malloc(sizeof(struct sockaddr));
  struct sockaddr_in*addr_in = (struct sockaddr_in*)addr;
  addr_in->sin_family = AF_INET;
  addr_in->sin_port = atoi(s_port);
  addr_in->sin_addr = *padico_topo_host_getaddr(padico_topo_node_gethost(gw_status->from_gw->node));

  padico_out(30,"Connecting to dest_node(%p)...\n", addr);
  padico_vsock_active_connect(gw_status->from_gw->vnode, addr, sizeof(struct sockaddr));
  padico_free(addr);
  padico_out(30,"Connected.\n");
}

static void vlink_fwd_send_post(padico_vnode_t vnode)
{
  struct vlink_fwd_specific_s*status = vobs_fwd(vnode);
  padico_print("SEND !\n");
  padico_vnode_write_post(status->vnode,
                          vnode->wbox.write_buffer,
                          vnode->wbox.write_posted);
  vnode->status = status->vnode->status;
}

static void vlink_fwd_recv_post(padico_vnode_t vnode)
{
  struct vlink_fwd_specific_s*status = vobs_fwd(vnode);
  padico_print("RECV !\n");
  padico_vnode_read_post(status->vnode,
                         vnode->rbox.read_buffer,
                         vnode->rbox.read_posted);
  vnode->status = status->vnode->status;
}

static void vlink_fwd_fullshutdown(padico_vnode_t vnode)
{
  struct vlink_fwd_specific_s*status = vobs_fwd(vnode);

  padico_vsock_fullshutdown(status->vnode);
}

/*
 * NewMadeleine
 * Copyright (C) 2016-2019 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 *
 * This example is a draft showing how to build and use diffusion trees for
 * multicast.
 * The data to send is wrapped within a structure containing:
 * - the data itself
 * - ids of the nodes to which the data needs to be forwarded
 * - a header containing the size of the data and the number of nodes
 */


#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <mpi.h>

#include <nm_launcher_interface.h>
#include <nm_coll_dynamic_interface.h>

#include "../common/nm_examples_helper.h"

#define NB_LOOPS 10


const char msg[] = "Hello, world!";


int main(int argc, char**argv)
{
  MPI_Init(&argc, &argv); // Just for eztrace

  nm_examples_init_topo(&argc, argv, NM_EXAMPLES_TOPO_STAR);
  nm_coll_dynamic_init_multicast();

  for (int j = 0; j < NB_LOOPS; j++)
  {
    if(is_server)
      {
        int n;
        nm_launcher_get_size(&n);

        int nb_dest_nodes = n - 1;

        int* nodes = malloc(nb_dest_nodes * sizeof(int)); // freed by nm_coll_dynamic_send
        int* prios = malloc(nb_dest_nodes * sizeof(int)); // freed by nm_coll_dynamic_send

        for(int i = 1; i < n; i++)
          {
            nodes[i-1] = i;
            prios[i-1] = n-i;
          }

        nm_coll_dynamic_send(nodes, prios, nb_dest_nodes, 0, (void*) msg, sizeof(msg), NULL, NULL);

        /*struct nm_data_s body_data;*/
        /*nm_data_contiguous_build(&body_data, msg, sizeof(msg));*/
        /*nm_coll_dynamic_send_data(p_session, nodes, nb_dest_nodes, 0, &body_data, sizeof(msg), NULL, NULL);*/
      }
    else
      {
        nm_sr_request_t request;
        char* received_msg = malloc(sizeof(msg));

        piom_cond_t cond_wait_recv;
        piom_cond_init(&cond_wait_recv, 0);

        nm_coll_dynamic_recv_register_recv(p_session, 0, 0, received_msg, sizeof(msg), &request, 
                                           &cond_wait_recv, NULL, NULL, NULL, NULL);

        /*struct nm_data_s body_data;*/
        /*nm_data_contiguous_build(&body_data, received_msg, sizeof(msg));*/
        /*nm_coll_dynamic_recv_register_recv_data(p_session, 0, 0, &body_data, sizeof(msg), &request, &cond_wait_recv, NULL, NULL, NULL);*/


        piom_cond_wait(&cond_wait_recv, 1);
        piom_cond_destroy(&cond_wait_recv);

        assert(memcmp(received_msg, msg, sizeof(msg)) == 0);

        free(received_msg);
      }


    nm_coll_barrier(p_comm, 123);
  }

  printf("success\n");

  nm_coll_dynamic_shutdown();
  nm_examples_exit();
  return 0;

}


/*
 * NewMadeleine
 * Copyright (C) 2014-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <nm_public.h>
#include <nm_private.h>
#include <nm_sendrecv_interface.h>
#include <nm_launcher_interface.h>
#include <Padico/Puk.h>
#include "nm_coll_private.h"
#include "nm_mpi_private.h"
#include "nm_mpi_private_syms.h"

#include <Padico/Module.h>
PADICO_MODULE_HOOK(MadMPI);

PADICO_MODULE_ATTR(alltoall_nports, "NM_MPI_ALLTOALL_NPORTS", "number of output ports for MPI_Alltoall; default is 4",
                   int, 4);

/* ** reduce *********************************************** */

__PUK_SYM_INTERNAL
int nm_mpi_coll_reduce(nm_mpi_communicator_t*p_comm, int root,
                       nm_mpi_count_t count, nm_mpi_datatype_t*p_datatype,
                       const void*sendbuf, void*recvbuf,
                       nm_tag_t tag, nm_mpi_operator_t*p_operator)
{
  const int size = nm_comm_size(p_comm->p_nm_comm);
  const int self = nm_comm_rank(p_comm->p_nm_comm);
  const nm_len_t slot_size = count * p_datatype->true_extent;
  const nm_coll_tree_kind_t kind = nm_coll_tree_heuristic(nm_comm_size(p_comm->p_nm_comm), slot_size);
  struct nm_coll_tree_info_s tree;
  nm_coll_tree_init(&tree, kind, size, self, root);
  int*children = padico_malloc(sizeof(int) * tree.max_arity);
  nm_mpi_request_t**requests = padico_malloc(size * sizeof(nm_mpi_request_t*));
  void*gather_buf = padico_malloc(tree.max_arity * slot_size);
  void*local_buf = NULL;
  const void*next_sbuf = (sendbuf == MPI_IN_PLACE) ? recvbuf : sendbuf; /* next data to send */
  /* input buffer from self */
  if(self == root)
    {
      assert(recvbuf != NULL);
      local_buf = recvbuf;
      if(sendbuf != MPI_IN_PLACE)
        {
          memcpy(recvbuf, sendbuf, slot_size);
        }
    }
  else
    {
      if(sendbuf == MPI_IN_PLACE)
        {
          local_buf = recvbuf;
        }
      else
        {
          local_buf = padico_malloc(slot_size);
          memcpy(local_buf, sendbuf, slot_size);
        }
    }
  int s;
  for(s = tree.height - 1; s >= 0; s--)
    {
      int parent = -1;
      int n_children = 0;
      nm_coll_tree_step(&tree, s, &parent, children, &n_children);
      assert((parent == -1) || (n_children == 0) || (children[0] == -1));
      if(n_children > 0)
        {
          /* receive from children */
          int i;
          for(i = 0; i < n_children; i++)
            {
              assert(children[i] != -1);
              requests[i] = nm_mpi_coll_irecv(gather_buf + i * slot_size, count, p_datatype, children[i], tag, p_comm);
            }
          for(i = 0; i < n_children; i++)
            {
              nm_mpi_coll_wait(requests[i]);
            }
          /* Do the reduction operation */
          for(i = n_children - 1; i >= 0; i--)
            {
              void*inbuf = gather_buf + i * slot_size;
              void*inoutbuf = (i > 0) ? (gather_buf + (i - 1) * slot_size) : local_buf;
              nm_mpi_operator_apply(&inbuf, inoutbuf, count, p_datatype, p_operator);
              next_sbuf = inoutbuf;
            }
        }
      /* send to parent */
      if(parent != -1)
        {
          nm_mpi_request_t*p_req = nm_mpi_coll_isend(next_sbuf, count, p_datatype, parent, tag, p_comm);
          nm_mpi_coll_wait(p_req);
        }
    }
  FREE_AND_SET_NULL(gather_buf);
  if(local_buf != recvbuf)
    {
      FREE_AND_SET_NULL(local_buf);
    }
  FREE_AND_SET_NULL(requests);
  FREE_AND_SET_NULL(children);
  return MPI_SUCCESS;

}

/* ** ireduce *********************************************** */

/** status of a reduce */
struct nm_mpi_coll_reduce_s
{
  struct nm_coll_tree_status_s coll_tree;
  const void*sendbuf; /**< send buffer given by user; may be NULL in case of MPI_IN_PLACE */
  void*recvbuf;       /**< recv buffer given by use; may be NULL if self != root */
  void*local_buf;     /**< local accumulator buffer; either malloced or a pointer to recvbuf if available */
  void*gather_buf;    /**< buffers used to receive data from toher nodes */
  void*next_sbuf;     /**< next buffer to send; either sendbuf or local_buf (or recvbuf if in-place) */
  nm_mpi_datatype_t*p_datatype;
  nm_mpi_count_t count;
  int self;
  int root;
  nm_mpi_operator_t*p_operator;
  int step;           /**< current step in tree */
  int n_children;     /**< number of children in this step */
};

static void nm_mpi_coll_ireduce_step(struct nm_mpi_coll_reduce_s*p_reduce);
static void nm_mpi_coll_ireduce_send_notifier(nm_sr_event_t event, const nm_sr_event_info_t*event_info, void*_ref);
static void nm_mpi_coll_ireduce_recv_notifier(nm_sr_event_t event, const nm_sr_event_info_t*event_info, void*_ref);

static void nm_mpi_coll_ireduce_destroy(struct nm_coll_req_s*p_coll_req)
{
  struct nm_mpi_coll_reduce_s*p_reduce = nm_coll_req_payload(p_coll_req);
  nm_coll_tree_status_destroy(&p_reduce->coll_tree);
  if(p_reduce->local_buf != p_reduce->recvbuf)
    padico_free(p_reduce->local_buf);
  padico_free(p_reduce->gather_buf);
}

static void nm_mpi_coll_ireduce_step(struct nm_mpi_coll_reduce_s*p_reduce)
{
 restart:
  assert(p_reduce->coll_tree.pending_reqs == 0);
  if(p_reduce->step >= 0)
    {
      int parent = -1;
      nm_coll_tree_step(&p_reduce->coll_tree.tree, p_reduce->step, &parent,
                        p_reduce->coll_tree.children, &p_reduce->n_children);
      p_reduce->step--;
      assert(!((parent != -1) && (p_reduce->n_children > 0)));
      if(parent != -1)
        {
          /* send to parent */
          struct nm_data_s send_data;
          nm_mpi_data_build(&send_data, p_reduce->next_sbuf, p_reduce->p_datatype, p_reduce->count);
          nm_coll_tree_send(&p_reduce->coll_tree, parent, &send_data, &p_reduce->coll_tree.parent_request);
          nm_coll_tree_set_notifier(&p_reduce->coll_tree, &p_reduce->coll_tree.parent_request,
                                    &nm_mpi_coll_ireduce_send_notifier);
        }
      else if(p_reduce->n_children > 0)
        {
          /* receive from children */
          const size_t slot_size = p_reduce->count * p_reduce->p_datatype->true_extent;
          int i;
          for(i = 0; i < p_reduce->n_children; i++)
            {
              assert(p_reduce->coll_tree.children[i] != -1);
              struct nm_data_s recv_data;
              nm_mpi_data_build(&recv_data, (void*)p_reduce->gather_buf + i * slot_size, p_reduce->p_datatype, p_reduce->count);
              nm_coll_tree_recv(&p_reduce->coll_tree, p_reduce->coll_tree.children[i], &recv_data,
                                &p_reduce->coll_tree.children_requests[i]);
            }
          for(i = 0; i < p_reduce->n_children; i++)
            {
              nm_coll_tree_set_notifier(&p_reduce->coll_tree, &p_reduce->coll_tree.children_requests[i],
                                        &nm_mpi_coll_ireduce_recv_notifier);
            }
        }
      else
        {
          assert((parent == -1) && (p_reduce->n_children == 0));
          goto restart; /* no parent nor children; go to next step */
        }
    }
  else
    {
      /* notify completion */
      nm_coll_req_signal(nm_coll_req_container(p_reduce));
    }
}

__PUK_SYM_INTERNAL
struct nm_coll_req_s*
nm_mpi_coll_ireduce_init(nm_mpi_communicator_t*p_comm, int root,
                         nm_mpi_count_t count, nm_mpi_datatype_t*p_datatype,
                         const void*sendbuf, void*recvbuf,
                         nm_tag_t tag, nm_mpi_operator_t*p_operator,
                         nm_coll_req_notifier_t p_notify, void*p_ref)
{
  nm_group_t p_group = nm_comm_group(p_comm->p_nm_comm);
  const int self = nm_comm_rank(p_comm->p_nm_comm);
  nm_session_t p_session = nm_comm_get_session(p_comm->p_nm_comm);
  const nm_len_t slot_size = count * p_datatype->true_extent;

  struct nm_coll_req_s*p_coll_req = nm_coll_req_alloc(sizeof(struct nm_mpi_coll_reduce_s), NM_COLL_REQ_OTHER,
                                                      &nm_mpi_coll_ireduce_destroy, p_notify, p_ref);
  struct nm_mpi_coll_reduce_s*p_reduce = nm_coll_req_payload(p_coll_req);
  const nm_coll_tree_kind_t kind = nm_coll_tree_heuristic(nm_group_size(p_group), slot_size);
  nm_coll_tree_status_init(&p_reduce->coll_tree, p_session, kind, p_group, root, self, tag);
  p_reduce->step = p_reduce->coll_tree.tree.height - 1;
  p_reduce->p_operator = p_operator;
  p_reduce->sendbuf = sendbuf;
  p_reduce->recvbuf = recvbuf;
  p_reduce->gather_buf = padico_malloc(p_reduce->coll_tree.tree.max_arity * slot_size);
  p_reduce->local_buf = NULL;
  p_reduce->next_sbuf = (sendbuf == MPI_IN_PLACE) ? recvbuf : (void*)sendbuf;
  p_reduce->p_datatype = p_datatype;
  p_reduce->count = count;
  p_reduce->self = self;
  p_reduce->root = root;
  p_reduce->n_children = 0;
  return p_coll_req;
}

__PUK_SYM_INTERNAL
void nm_mpi_coll_ireduce_start(struct nm_coll_req_s*p_coll_req)
{
  struct nm_mpi_coll_reduce_s*p_reduce = nm_coll_req_payload(p_coll_req);
  const nm_len_t slot_size = p_reduce->count * p_reduce->p_datatype->true_extent;
  if(p_reduce->gather_buf == NULL)
    {
      NM_FATAL("malloc failed in ireduce.\n");
    }
  if(p_reduce->self == p_reduce->root)
    {
      /* input buffer from self */
      assert(p_reduce->recvbuf != NULL);
      p_reduce->local_buf = p_reduce->recvbuf;
      if(p_reduce->sendbuf != MPI_IN_PLACE)
        {
          memcpy(p_reduce->recvbuf, p_reduce->sendbuf, slot_size);
        }
    }
  else
    {
      if(p_reduce->sendbuf == MPI_IN_PLACE)
        {
          p_reduce->local_buf = p_reduce->recvbuf;
        }
      else
        {
          p_reduce->local_buf = padico_malloc(slot_size);
          if(p_reduce->local_buf == NULL)
            {
              NM_FATAL("malloc failed in ireduce.\n");
            }
          memcpy(p_reduce->local_buf, p_reduce->sendbuf, slot_size);
        }
    }
  nm_mpi_coll_ireduce_step(p_reduce);
}

static void nm_mpi_coll_ireduce_recv_notifier(nm_sr_event_t event, const nm_sr_event_info_t*p_event_info, void* _ref)
{
  struct nm_coll_tree_status_s*p_coll_tree = _ref;
  struct nm_mpi_coll_reduce_s*p_reduce = nm_container_of(p_coll_tree, struct nm_mpi_coll_reduce_s, coll_tree);
  nm_atomic_dec(&p_reduce->coll_tree.pending_reqs);
  if(p_reduce->coll_tree.pending_reqs == 0)
    {
      const nm_len_t slot_size = p_reduce->count * nm_mpi_datatype_size(p_reduce->p_datatype);
      int i;
      for(i = p_reduce->n_children - 1; i >= 0; i--)
        {
          void*inbuf = p_reduce->gather_buf + i * slot_size;
          void*inoutbuf = (i > 0) ? (p_reduce->gather_buf + (i - 1) * slot_size) : p_reduce->local_buf;
          nm_mpi_operator_apply(&inbuf, inoutbuf, p_reduce->count, p_reduce->p_datatype, p_reduce->p_operator);
          p_reduce->next_sbuf = inoutbuf;
        }
      nm_mpi_coll_ireduce_step(p_reduce);
    }
}

static void nm_mpi_coll_ireduce_send_notifier(nm_sr_event_t event, const nm_sr_event_info_t*p_event_info, void* _ref)
{
  struct nm_coll_tree_status_s*p_coll_tree = _ref;
  struct nm_mpi_coll_reduce_s*p_reduce = nm_container_of(p_coll_tree, struct nm_mpi_coll_reduce_s, coll_tree);
  const int p = nm_atomic_dec(&p_reduce->coll_tree.pending_reqs);
  if(p == 0)
    {
      nm_mpi_coll_ireduce_step(p_reduce);
    }
}


/* ** iallreduce ******************************************* */

struct nm_mpi_coll_allreduce_s
{
  struct nm_coll_req_s*p_reduce;
  struct nm_coll_req_s*p_bcast;
  nm_mpi_communicator_t*p_comm;
  enum
    {
     NM_MPI_COLL_IALLREDUCE_REDUCING,
     NM_MPI_COLL_IALLREDUCE_BROADCASTING,
     NM_MPI_COLL_IALLREDUCE_COMPLETED
    } state;
};

static void nm_mpi_coll_iallreduce_bcast_notifier(void*ref)
{
  struct nm_mpi_coll_allreduce_s*p_allreduce = ref;
  nm_coll_req_signal(nm_coll_req_container(p_allreduce));
}

static void nm_mpi_coll_iallreduce_reduce_notifier(void*ref)
{
  struct nm_mpi_coll_allreduce_s*p_allreduce = ref;
  const int root = 0;
  const nm_tag_t tag =  NM_MPI_TAG_PRIVATE_ALLREDUCE;
  assert(p_allreduce->state == NM_MPI_COLL_IALLREDUCE_REDUCING);
  p_allreduce->state = NM_MPI_COLL_IALLREDUCE_BROADCASTING;
  struct nm_mpi_coll_reduce_s*p_reduce = nm_coll_req_payload(p_allreduce->p_reduce);
  struct nm_data_s data;
  nm_mpi_data_build(&data, p_reduce->recvbuf, p_reduce->p_datatype, p_reduce->count);
  nm_comm_t p_comm = p_allreduce->p_comm->p_nm_comm;
  nm_group_t p_group = nm_comm_group(p_comm);
  const int self = nm_comm_rank(p_comm);
  nm_session_t p_session = nm_comm_get_session(p_comm);
  p_allreduce->p_bcast = nm_coll_group_data_ibcast(p_session, p_group, root, self, &data, tag,
                                                   &nm_mpi_coll_iallreduce_bcast_notifier, p_allreduce);
}

__PUK_SYM_INTERNAL
struct nm_coll_req_s*nm_mpi_coll_iallreduce(nm_mpi_communicator_t*p_comm,
                                            nm_mpi_count_t count, nm_mpi_datatype_t*p_datatype,
                                            const void*sendbuf, void*recvbuf,
                                            nm_tag_t tag, nm_mpi_operator_t*p_operator)
{
  const int root = 0;
  struct nm_coll_req_s*p_coll_req = nm_coll_req_alloc(sizeof(struct nm_mpi_coll_allreduce_s), NM_COLL_REQ_OTHER,
                                                      /* destructor */ NULL, NULL, NULL);
  struct nm_mpi_coll_allreduce_s*p_allreduce = nm_coll_req_payload(p_coll_req);
  p_allreduce->state = NM_MPI_COLL_IALLREDUCE_REDUCING;
  p_allreduce->p_comm = p_comm;
  p_allreduce->p_bcast = NULL;
  p_allreduce->p_reduce = nm_mpi_coll_ireduce_init(p_comm, root, count, p_datatype, sendbuf, recvbuf,
                                                   tag, p_operator, &nm_mpi_coll_iallreduce_reduce_notifier, p_allreduce);
  nm_mpi_coll_ireduce_start(p_allreduce->p_reduce);
  return p_coll_req;
}

/* ** ialltoall ******************************************** */

/** status of an alltoall operation (blocking/non-blocking) */
struct nm_mpi_coll_alltoall_s
{
  nm_mpi_communicator_t*p_comm;  /**< communicator for the alltoall */
  int seq;                       /**< next request to send */
  int pending;                   /**< number of pendings requests (send+recv) */
  nm_sr_request_t*send_requests;
  nm_sr_request_t*recv_requests;
  const void*sendbuf;
  nm_mpi_count_t sendcount;
  nm_mpi_datatype_t*p_send_datatype;
  int in_place;
};

static void nm_mpi_coll_ialltoall_destroy(struct nm_coll_req_s*p_coll_req);
static void nm_mpi_coll_ialltoall_step(struct nm_mpi_coll_alltoall_s*p_alltoall);
static void nm_mpi_coll_ialltoall_send_notifier(nm_sr_event_t event, const nm_sr_event_info_t*p_info, void*_ref);
static void nm_mpi_coll_ialltoall_recv_notifier(nm_sr_event_t event, const nm_sr_event_info_t*p_info, void*_ref);

__PUK_SYM_INTERNAL
struct nm_coll_req_s*nm_mpi_coll_ialltoall(nm_mpi_communicator_t*p_comm,
                                           const void*p_sendbuf, nm_mpi_count_t sendcount, nm_mpi_datatype_t*p_send_datatype,
                                           void*p_recvbuf, nm_mpi_count_t recvcount, nm_mpi_datatype_t*p_recv_datatype)
{
  const int nports = padico_module_attr_alltoall_nports_getvalue();
  if(nports < 1)
    NM_MPI_FATAL_ERROR("invalid nports = %d for MPI_Alltoall().\n", nports);
  const nm_tag_t tag = NM_MPI_TAG_PRIVATE_ALLTOALL;
  nm_session_t p_session = nm_mpi_communicator_get_session(p_comm);
  const int self = nm_comm_rank(p_comm->p_nm_comm);
  struct nm_coll_req_s*p_coll_req = nm_coll_req_alloc(sizeof(struct nm_mpi_coll_alltoall_s), NM_COLL_REQ_OTHER,
                                                      &nm_mpi_coll_ialltoall_destroy, NULL, NULL);
  struct nm_mpi_coll_alltoall_s*p_alltoall = nm_coll_req_payload(p_coll_req);
  p_alltoall->p_comm = p_comm;
  p_alltoall->seq = 0;
  p_alltoall->pending = 2 * (nm_comm_size(p_comm->p_nm_comm) - 1);
  p_alltoall->send_requests = padico_malloc(nm_comm_size(p_comm->p_nm_comm) * sizeof(nm_sr_request_t));
  p_alltoall->recv_requests = padico_malloc(nm_comm_size(p_comm->p_nm_comm) * sizeof(nm_sr_request_t));
  if(p_sendbuf != MPI_IN_PLACE)
    {
      p_alltoall->sendbuf = p_sendbuf;
      p_alltoall->sendcount = sendcount;
      p_alltoall->p_send_datatype = p_send_datatype;
      p_alltoall->in_place = 0;
    }
  else
    {
      const int size = nm_comm_size(p_comm->p_nm_comm);
      const nm_len_t slot_size = recvcount * nm_mpi_datatype_size(p_recv_datatype);
      void*buf = padico_malloc(size * slot_size);
      nm_mpi_datatype_pack(buf, p_recvbuf, p_recv_datatype, recvcount * size);
      p_alltoall->sendbuf = buf;
      p_alltoall->sendcount = slot_size;
      p_alltoall->p_send_datatype = nm_mpi_datatype_get(MPI_BYTE);
      p_alltoall->in_place = 1;
    }
  /* pre-post all receive requests */
  int i;
  for(i = 0; i < nm_comm_size(p_comm->p_nm_comm); i++)
    {
      if(i != self)
        {
          nm_gate_t p_gate = nm_comm_get_gate(p_comm->p_nm_comm, i);
          struct nm_data_s data;
          nm_mpi_data_build(&data, (void*)nm_mpi_datatype_get_ptr(p_recvbuf, i * recvcount, p_recv_datatype),
                            p_recv_datatype, recvcount);
          nm_sr_irecv_data(p_session, p_gate, tag, &data, &p_alltoall->recv_requests[i]);
          nm_sr_request_set_ref(&p_alltoall->recv_requests[i], p_alltoall);
          nm_sr_request_monitor(p_session, &p_alltoall->recv_requests[i], NM_SR_EVENT_FINALIZED,
                                &nm_mpi_coll_ialltoall_recv_notifier);
        }
    }
  /* pre-load nports (=4) send requests */
  for(i = 0; (i < nports) && (i < nm_comm_size(p_comm->p_nm_comm)); i++)
    {
      nm_mpi_coll_ialltoall_step(p_alltoall);
    }
  if(!p_alltoall->in_place)
    {
      nm_mpi_datatype_copy(nm_mpi_datatype_get_ptr((void*)p_sendbuf, (nm_mpi_count_t)self * sendcount, p_send_datatype), p_send_datatype, sendcount,
                           nm_mpi_datatype_get_ptr(p_recvbuf, (nm_mpi_count_t)self * recvcount, p_recv_datatype), p_recv_datatype, recvcount);
    }
  if(nm_comm_size(p_comm->p_nm_comm) == 1)
    {
      /* do not expect send/recv completion with a single node, there will be none */
      nm_coll_req_signal(nm_coll_req_container(p_alltoall));
    }
  return p_coll_req;
}

static void nm_mpi_coll_ialltoall_destroy(struct nm_coll_req_s*p_coll_req)
{
  struct nm_mpi_coll_alltoall_s*p_alltoall = nm_coll_req_payload(p_coll_req);
  padico_free(p_alltoall->send_requests);
  padico_free(p_alltoall->recv_requests);
  if(p_alltoall->in_place)
    {
      padico_free((void*)p_alltoall->sendbuf);
    }
}

/** try to start a new send request on the current alltoall */
static void nm_mpi_coll_ialltoall_step(struct nm_mpi_coll_alltoall_s*p_alltoall)
{
  const nm_tag_t tag = NM_MPI_TAG_PRIVATE_ALLTOALL;
  nm_session_t p_session = nm_mpi_communicator_get_session(p_alltoall->p_comm);
  int s = nm_atomic_inc(&p_alltoall->seq);
  if(s == 0)
    {
      /* skip self */
      s = nm_atomic_inc(&p_alltoall->seq);
    }
  if(s < nm_comm_size(p_alltoall->p_comm->p_nm_comm))
    {
      /* send to rank = seq + self */
      const int dest = (s + nm_comm_rank(p_alltoall->p_comm->p_nm_comm)) % nm_comm_size(p_alltoall->p_comm->p_nm_comm);
      nm_gate_t p_gate = nm_comm_get_gate(p_alltoall->p_comm->p_nm_comm, dest);
      struct nm_data_s data;
      nm_mpi_data_build(&data, (void*)nm_mpi_datatype_get_ptr((void*)p_alltoall->sendbuf, dest * p_alltoall->sendcount, p_alltoall->p_send_datatype),
                        p_alltoall->p_send_datatype, p_alltoall->sendcount);

      nm_sr_isend_data(p_session, p_gate, tag, &data, &p_alltoall->send_requests[dest]);
      nm_sr_request_set_ref(&p_alltoall->send_requests[dest], p_alltoall);
      nm_sr_request_monitor(p_session, &p_alltoall->send_requests[dest], NM_SR_EVENT_FINALIZED,
                            &nm_mpi_coll_ialltoall_send_notifier);
    }
}

/** handler for complete send requests in alltoall */
static void nm_mpi_coll_ialltoall_send_notifier(nm_sr_event_t event, const nm_sr_event_info_t*p_info, void*_ref)
{
  struct nm_mpi_coll_alltoall_s*p_alltoall = _ref;
  const int p = nm_atomic_dec(&p_alltoall->pending);
  if(p == 0)
    {
      nm_coll_req_signal(nm_coll_req_container(p_alltoall));
    }
  else
    {
      nm_mpi_coll_ialltoall_step(p_alltoall);
    }
}

/** handler for complete receive requests in alltoall */
static void nm_mpi_coll_ialltoall_recv_notifier(nm_sr_event_t event, const nm_sr_event_info_t*p_info, void*_ref)
{
  struct nm_mpi_coll_alltoall_s*p_alltoall = _ref;
  const int p = nm_atomic_dec(&p_alltoall->pending);
  if(p == 0)
    {
      nm_coll_req_signal(nm_coll_req_container(p_alltoall));
    }
}

/* ** ialltoallv ******************************************* */

/** status of an alltoallv operation (blocking/non-blocking) */
struct nm_mpi_coll_alltoallv_s
{
  nm_mpi_communicator_t*p_comm;  /**< communicator for the alltoall */
  int seq;                       /**< next request to send */
  int pending;                   /**< number of pendings requests (send+recv) */
  nm_sr_request_t*send_requests;
  nm_sr_request_t*recv_requests;
  const void*sendbuf;
  int*p_sendcounts;
  int*p_sdispls;
  nm_mpi_datatype_t*p_send_datatype;
  int in_place;
};

static void nm_mpi_coll_ialltoallv_destroy(struct nm_coll_req_s*p_coll_req);
static void nm_mpi_coll_ialltoallv_step(struct nm_mpi_coll_alltoallv_s*p_alltoallv);
static void nm_mpi_coll_ialltoallv_send_notifier(nm_sr_event_t event, const nm_sr_event_info_t*p_info, void*_ref);
static void nm_mpi_coll_ialltoallv_recv_notifier(nm_sr_event_t event, const nm_sr_event_info_t*p_info, void*_ref);

__PUK_SYM_INTERNAL
struct nm_coll_req_s*nm_mpi_coll_ialltoallv(nm_mpi_communicator_t*p_comm,
                                            const void*p_sendbuf, const nm_mpi_count_t*p_sendcounts, const nm_mpi_aint_t*p_sdispls, nm_mpi_datatype_t*p_send_datatype,
                                            void*p_recvbuf,  const nm_mpi_count_t*p_recvcounts, const nm_mpi_aint_t*p_rdispls, nm_mpi_datatype_t*p_recv_datatype)
{
  const int nports = padico_module_attr_alltoall_nports_getvalue();
  if(nports < 1)
    NM_MPI_FATAL_ERROR("invalid nports = %d for MPI_Alltoallv().\n", nports);
  const nm_tag_t tag = NM_MPI_TAG_PRIVATE_ALLTOALLV;
  nm_session_t p_session = nm_mpi_communicator_get_session(p_comm);
  const int self = nm_comm_rank(p_comm->p_nm_comm);
  const int size = nm_comm_size(p_comm->p_nm_comm);
  struct nm_coll_req_s*p_coll_req = nm_coll_req_alloc(sizeof(struct nm_mpi_coll_alltoallv_s), NM_COLL_REQ_OTHER,
                                                      &nm_mpi_coll_ialltoallv_destroy, NULL, NULL);
  struct nm_mpi_coll_alltoallv_s*p_alltoallv = nm_coll_req_payload(p_coll_req);
  p_alltoallv->p_comm = p_comm;
  p_alltoallv->seq = 0;
  p_alltoallv->pending = 2 * (nm_comm_size(p_comm->p_nm_comm) - 1);
  p_alltoallv->send_requests = padico_malloc(nm_comm_size(p_comm->p_nm_comm) * sizeof(nm_sr_request_t));
  p_alltoallv->recv_requests = padico_malloc(nm_comm_size(p_comm->p_nm_comm) * sizeof(nm_sr_request_t));
  p_alltoallv->p_sendcounts = padico_malloc(nm_comm_size(p_comm->p_nm_comm) * sizeof(nm_mpi_count_t));
  p_alltoallv->p_sdispls = padico_malloc(nm_comm_size(p_comm->p_nm_comm) * sizeof(nm_mpi_aint_t));
  if(p_sendbuf != MPI_IN_PLACE)
    {
      int i;
      for(i = 0; i < size; i++)
        {
          /* store send info */
          p_alltoallv->p_sendcounts[i] = p_sendcounts[i];
          p_alltoallv->p_sdispls[i] = p_sdispls[i];
        }
      p_alltoallv->sendbuf = p_sendbuf;
      p_alltoallv->p_send_datatype = p_send_datatype;
      p_alltoallv->in_place = 0;
    }
  else
    {
      nm_len_t total_size = 0;
      int i;
      for(i = 0; i < size; i++)
        {
          total_size += p_recvcounts[i] * nm_mpi_datatype_size(p_recv_datatype);
        }
      void*buf = padico_malloc(total_size);
      nm_mpi_aint_t offset = 0;
      for(i = 0; i < size; i++)
        {
          p_alltoallv->p_sendcounts[i] = p_recvcounts[i] * nm_mpi_datatype_size(p_recv_datatype);
          p_alltoallv->p_sdispls[i] = offset;
          nm_mpi_datatype_pack(buf + offset, nm_mpi_datatype_get_ptr(p_recvbuf, p_rdispls[i], p_recv_datatype), p_recv_datatype, p_recvcounts[i]);
          offset += p_recvcounts[i] * nm_mpi_datatype_size(p_recv_datatype);
        }
      p_alltoallv->p_send_datatype = nm_mpi_datatype_get(MPI_BYTE);
      p_alltoallv->sendbuf = buf;
      p_alltoallv->in_place = 1;
    }
  int i;
  for(i = 0; i < nm_comm_size(p_comm->p_nm_comm); i++)
    {
      /* pre-post all receive requests */
      if(i != self)
        {
          nm_gate_t p_gate = nm_comm_get_gate(p_comm->p_nm_comm, i);
          struct nm_data_s data;
          nm_mpi_data_build(&data, (void*)nm_mpi_datatype_get_ptr(p_recvbuf, p_rdispls[i], p_recv_datatype),
                            p_recv_datatype, p_recvcounts[i]);
          nm_sr_irecv_data(p_session, p_gate, tag, &data, &p_alltoallv->recv_requests[i]);
          nm_sr_request_set_ref(&p_alltoallv->recv_requests[i], p_alltoallv);
          nm_sr_request_monitor(p_session, &p_alltoallv->recv_requests[i], NM_SR_EVENT_FINALIZED,
                                &nm_mpi_coll_ialltoallv_recv_notifier);
        }
    }
  /* pre-load nports (=4) send requests */
  for(i = 0; (i < nports) && (i < nm_comm_size(p_comm->p_nm_comm)); i++)
    {
      nm_mpi_coll_ialltoallv_step(p_alltoallv);
    }
  if(!p_alltoallv->in_place)
    {
      nm_mpi_datatype_copy(nm_mpi_datatype_get_ptr((void*)p_sendbuf, p_sdispls[self], p_send_datatype), p_send_datatype, p_sendcounts[self],
                           nm_mpi_datatype_get_ptr(p_recvbuf, p_rdispls[self], p_recv_datatype), p_recv_datatype, p_recvcounts[self]);
    }
  if(nm_comm_size(p_comm->p_nm_comm) == 1)
    {
      /* do not expect send/recv completion with a single node, there will be none */
      nm_coll_req_signal(nm_coll_req_container(p_alltoallv));
    }
  return p_coll_req;
}

static void nm_mpi_coll_ialltoallv_destroy(struct nm_coll_req_s*p_coll_req)
{
  struct nm_mpi_coll_alltoallv_s*p_alltoallv = nm_coll_req_payload(p_coll_req);
  padico_free(p_alltoallv->send_requests);
  padico_free(p_alltoallv->recv_requests);
  padico_free(p_alltoallv->p_sendcounts);
  padico_free(p_alltoallv->p_sdispls);
  if(p_alltoallv->in_place)
    {
      padico_free((void*)p_alltoallv->sendbuf);
    }
}

/** try to start a new send request on the current alltoall */
static void nm_mpi_coll_ialltoallv_step(struct nm_mpi_coll_alltoallv_s*p_alltoallv)
{
  const nm_tag_t tag = NM_MPI_TAG_PRIVATE_ALLTOALLV;
  nm_session_t p_session = nm_mpi_communicator_get_session(p_alltoallv->p_comm);
  int s = nm_atomic_inc(&p_alltoallv->seq);
  if(s == 0)
    {
      /* skip self */
      s = nm_atomic_inc(&p_alltoallv->seq);
    }
  if(s < nm_comm_size(p_alltoallv->p_comm->p_nm_comm))
    {
      /* send to rank = seq + self */
      const int dest = (s + nm_comm_rank(p_alltoallv->p_comm->p_nm_comm)) % nm_comm_size(p_alltoallv->p_comm->p_nm_comm);
      nm_gate_t p_gate = nm_comm_get_gate(p_alltoallv->p_comm->p_nm_comm, dest);
      struct nm_data_s data;
      nm_mpi_data_build(&data, (void*)nm_mpi_datatype_get_ptr((void*)p_alltoallv->sendbuf, p_alltoallv->p_sdispls[dest], p_alltoallv->p_send_datatype),
                        p_alltoallv->p_send_datatype, p_alltoallv->p_sendcounts[dest]);

      nm_sr_isend_data(p_session, p_gate, tag, &data, &p_alltoallv->send_requests[dest]);
      nm_sr_request_set_ref(&p_alltoallv->send_requests[dest], p_alltoallv);
      nm_sr_request_monitor(p_session, &p_alltoallv->send_requests[dest], NM_SR_EVENT_FINALIZED,
                            &nm_mpi_coll_ialltoallv_send_notifier);
    }
}

/** handler for complete send requests in alltoall */
static void nm_mpi_coll_ialltoallv_send_notifier(nm_sr_event_t event, const nm_sr_event_info_t*p_info, void*_ref)
{
  struct nm_mpi_coll_alltoallv_s*p_alltoallv = _ref;
  const int p = nm_atomic_dec(&p_alltoallv->pending);
  if(p == 0)
    {
      nm_coll_req_signal(nm_coll_req_container(p_alltoallv));
    }
  else
    {
      nm_mpi_coll_ialltoallv_step(p_alltoallv);
    }
}

/** handler for complete receive requests in alltoall */
static void nm_mpi_coll_ialltoallv_recv_notifier(nm_sr_event_t event, const nm_sr_event_info_t*p_info, void*_ref)
{
  struct nm_mpi_coll_alltoallv_s*p_alltoallv = _ref;
  const int p = nm_atomic_dec(&p_alltoallv->pending);
  if(p == 0)
    {
      nm_coll_req_signal(nm_coll_req_container(p_alltoallv));
    }
}

/* ** iallgather ******************************************* */

struct nm_mpi_coll_allgather_s
{
  struct nm_coll_req_s*p_gather;
  struct nm_coll_req_s*p_bcast;
  nm_mpi_communicator_t*p_comm;
  enum
    {
     NM_MPI_COLL_IALLGATHER_GATHERING,
     NM_MPI_COLL_IALLGATHER_BROADCASTING,
     NM_MPI_COLL_IALLGATHER_COMPLETED
    } state;
  struct nm_data_s bcast_data;
  struct nm_data_s*p_recv_data;
};

static void nm_mpi_coll_iallgather_destroy(struct nm_coll_req_s*p_coll_req)
{
  struct nm_mpi_coll_allgather_s*p_allgather = nm_coll_req_payload(p_coll_req);
  if(p_allgather->p_recv_data != NULL)
    padico_free(p_allgather->p_recv_data);
}

static void nm_mpi_coll_iallgather_bcast_notifier(void*ref)
{
  struct nm_mpi_coll_allgather_s*p_allgather = ref;
  nm_coll_req_signal(nm_coll_req_container(p_allgather));
}

static void nm_mpi_coll_iallgather_gather_notifier(void*ref)
{
  struct nm_mpi_coll_allgather_s*p_allgather = ref;
  assert(p_allgather->state == NM_MPI_COLL_IALLGATHER_GATHERING);
  p_allgather->state = NM_MPI_COLL_IALLGATHER_BROADCASTING;

  const int root = 0;
  const nm_tag_t tag = NM_MPI_TAG_PRIVATE_ALLGATHER;
  nm_comm_t p_comm = p_allgather->p_comm->p_nm_comm;
  nm_group_t p_group = nm_comm_group(p_comm);
  const int self = nm_comm_rank(p_comm);
  nm_session_t p_session = nm_comm_get_session(p_comm);
  p_allgather->p_bcast =
    nm_coll_group_data_ibcast(p_session, p_group, root, self, &p_allgather->bcast_data, tag,
                              &nm_mpi_coll_iallgather_bcast_notifier, p_allgather);
}

__PUK_SYM_INTERNAL
struct nm_coll_req_s*nm_mpi_coll_iallgather(nm_mpi_communicator_t*p_comm,
                                            const void*sendbuf, nm_mpi_count_t sendcount, nm_mpi_datatype_t*p_send_datatype,
                                            void*recvbuf, nm_mpi_count_t recvcount, nm_mpi_datatype_t*p_recv_datatype)
{
  const int root = 0;
  const int self = nm_comm_rank(p_comm->p_nm_comm);
  const nm_tag_t tag = NM_MPI_TAG_PRIVATE_ALLGATHER;
  struct nm_coll_req_s*p_coll_req = nm_coll_req_alloc(sizeof(struct nm_mpi_coll_allgather_s), NM_COLL_REQ_OTHER,
                                                      &nm_mpi_coll_iallgather_destroy, NULL, NULL);
  struct nm_mpi_coll_allgather_s*p_allgather = nm_coll_req_payload(p_coll_req);
  p_allgather->state = NM_MPI_COLL_IALLGATHER_GATHERING;
  p_allgather->p_comm = p_comm;
  p_allgather->p_bcast = NULL;

  /* build send & recv data descriptor for igather from iallgather info.
   * There is no direct matching because of MPI_IN_PLACE.
   */
  struct nm_data_s send_data;
  p_allgather->p_recv_data = NULL;
  if(sendbuf == MPI_IN_PLACE)
    {
      if(root == self)
        {
          nm_data_null_build(&send_data);
        }
      else
        {
          nm_mpi_data_build(&send_data,
                            nm_mpi_datatype_get_ptr(recvbuf, (self * recvcount), p_recv_datatype),
                            p_recv_datatype, recvcount);
        }
    }
  else
    {
      nm_mpi_data_build(&send_data, (void*)sendbuf, p_send_datatype, sendcount);
    }
  if(root == self)
    {
      const int size = nm_comm_size(p_comm->p_nm_comm);
      p_allgather->p_recv_data = padico_malloc(size * sizeof(struct nm_data_s));
      int i;
      for(i = 0; i < size; i++)
        {
          nm_mpi_data_build(&p_allgather->p_recv_data[i],
                            nm_mpi_datatype_get_ptr(recvbuf, (i * recvcount), p_recv_datatype),
                            p_recv_datatype, recvcount);
        }
    }
  nm_mpi_data_build(&p_allgather->bcast_data, recvbuf, p_recv_datatype, recvcount * nm_comm_size(p_comm->p_nm_comm));

  p_allgather->p_gather = nm_coll_group_data_igather(nm_comm_get_session(p_comm->p_nm_comm),
                                                     nm_comm_group(p_comm->p_nm_comm),
                                                     root, self, &send_data, p_allgather->p_recv_data, tag,
                                                     &nm_mpi_coll_iallgather_gather_notifier, p_allgather);
  return p_coll_req;
}

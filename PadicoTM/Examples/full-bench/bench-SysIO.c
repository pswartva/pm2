/* bench-XChn.c
 */

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "bench-common.h"
#include <Padico/Topology.h>
#include <Padico/PadicoTM.h>
#include <Padico/PM2.h>
#include <Padico/SysIO.h>
#include <Padico/Module.h>
#ifdef PUKABI
#include <Padico/Puk-ABI.h>
#endif

int bench_sysio_init(void);
int bench_sysio_run(int argc, char**argv);

PADICO_MODULE_DECLARE(bench_SysIO, bench_sysio_init, bench_sysio_run, NULL);

#define DEFAULT_PORT       5040

static int port = DEFAULT_PORT;
static int bench_fd;
static int server_fd;
static padico_sysio_t read_io = NULL;
static padico_sysio_t write_io = NULL;
static struct sockaddr_in remote_addr;
static socklen_t          remote_addr_size = sizeof(remote_addr);

static char* recv_buffer = NULL;
static volatile int recv_lenbuf  = 0;
static          int len_offset   = 0;
static          int trips_posted = 0;    /* num of message we will received */
static          int num_received = 0;
static          int num_sent     = 0;
static marcel_mutex_t mutex;
static marcel_cond_t cond;
static marcel_t bsio_thread = NULL;

static void bsio_signal(void);
static void bsio_start(char*buffer, int lenbuf);
static void bsio_stop(void);
static void bsio_send(void*dummy);

/* a message has arrived */
static void bsio_signal(void)
{
  if(num_sent < trips_posted)
    {
      padico_out(60, "deviate sent=%d/%d...\n", num_sent, trips_posted);
      bsio_send(NULL);
    }
  if(num_received >= trips_posted)
    {
      padico_out(60, "broadcasting sent=%d; recv=%d\n", num_sent, num_received);
      marcel_cond_broadcast(&cond);
    }
  padico_out(60, " <- unlocked.\n");
}

/* init a new benchmark */
static void bsio_start(char*buffer, int lenbuf)
{
  padico_out(40, "starting len=%d\n", lenbuf);
  marcel_mutex_lock(&mutex);
  bsio_thread = marcel_self();
  trips_posted = 1;
  recv_buffer = buffer;
  recv_lenbuf = lenbuf;
  len_offset  = 0;
  num_sent = 0;
  num_received = 0;
  marcel_mutex_unlock(&mutex);
  padico_io_activate(read_io);
}

static void bsio_stop(void)
{
  padico_out(40, "stop\n");
  padico_io_deactivate(read_io);
  recv_lenbuf = 0;
  recv_buffer = NULL;
  trips_posted = 0;
}

static void bsio_wait(void)
{
  marcel_mutex_lock(&mutex);
  while(num_received < trips_posted)
    {
      padico_out(20, "waiting synchro...\n");
      marcel_cond_wait(&cond, &mutex);
      padico_out(20, "unlocked synchro.\n");
    }
  marcel_mutex_unlock(&mutex);
}

static void bsio_send(void*dummy)
{
  char*buffer = recv_buffer;
  int lenbuf  = recv_lenbuf;
  int rc = 0;
  num_sent++;
  padico_out(40, "sending... sent=%d, recv=%d\n", num_sent, num_received);
  rc = padico_sysio_out(bench_fd, buffer, lenbuf);
  if(rc != lenbuf)
    {
      padico_warning("sent %d bytes (len=%d; num=%d)\n", rc, lenbuf, num_received);
    }
}

static int bench_sysio_reader(int fd, void*key)
{
  int rc = 0;
  num_received ++;
  padico_out(60, "in callback sent=%d, recv=%d\n", num_sent, num_received);
  while(num_received > trips_posted)
    {
      padico_warning("buffer not ready!\n");
      sched_yield();
    }
  rc = padico_sysio_in(fd, recv_buffer, recv_lenbuf);
  if(rc != recv_lenbuf)
    {
      padico_warning("received %d bytes (len=%d; num=%d)\n", rc, recv_lenbuf, num_received);
    }
  bsio_signal();
  if(num_received < trips_posted)
    {
      return 1;
    }
  else
    {
      return 0;
    }
}

int bench_sysio_init(void)
{
  marcel_mutex_init(&mutex, NULL);
  marcel_cond_init(&cond, NULL);

  if(bench_common_param.is_server)
    {
      server_fd = padico_sysio_bound_inet_socket(&port);
      if(server_fd == -1)
        {
          padico_warning("BENCH: cannot create server socket on port %d.\n", port);
          return -1;
        }
      padico_print("BENCH: listening on port %d\n", port);
      bench_fd  = padico_sysio_accept(server_fd,
                                      (struct sockaddr*)&remote_addr,
                                      &remote_addr_size);
      if(bench_fd == -1)
        {
          padico_warning("BENCH: error while accepting incoming connection.\n");
          return -1;
        }
      padico_print("BENCH: received connection.\n");
      padico_sysio_close(server_fd);
    }
  else
    {
      do
        {
          padico_topo_host_t peer_host = padico_topo_node_gethost(bench_common_param.server_node);
          const struct in_addr*peer_addr = padico_topo_host_getaddr(peer_host);
          padico_print("BENCH: connecting to server (%s:%d).\n", inet_ntoa(*peer_addr), port);
          bench_fd = padico_sysio_connected_inet_socket(peer_addr, port);
          if(bench_fd < 0)
            {
              padico_warning("cannot connect fd %d. Retrying...\n", bench_fd);
              padico_tm_sleep(2);
            }
          else
            {
              padico_print("BENCH: client ready.\n");
            }
        }
      while(bench_fd < 0);
    }
  //  fcntl(bench_fd, F_SETFL, O_NONBLOCK);
  {
    int opt = 1;
    int rc = 0;
    rc = PUK_ABI_FSYS_WRAP(setsockopt)(bench_fd, SOL_TCP, TCP_NODELAY, &opt, sizeof(opt));
    if(rc)
      {
        padico_warning("setsockopt failed\n");
      }
  }
  read_io = padico_io_register(bench_fd, PADICO_IO_EVENT_READ,
                               &bench_sysio_reader, NULL);
  padico_tm_msleep(500);
  return 0;
}

static void sysio_server(char*buffer, size_t lenbuf)
{
  padico_out(20, "starting...\n");
  bsio_start(buffer, lenbuf);
  bsio_wait();
  bsio_stop();
}

static void sysio_client(char*buffer, size_t lenbuf)
{
  padico_out(20, "starting...\n");
  bsio_start(buffer, lenbuf);
  bsio_send(NULL);
  bsio_wait();
}

struct bench_desc_s sysio_bench =
{
  "SysIO",
  &sysio_server,
  &sysio_client
};

int bench_sysio_run(int argc, char**argv)
{
  padico_print("BENCH: Sysio start.\n");
  the_bench(&sysio_bench);
  return 0;
}

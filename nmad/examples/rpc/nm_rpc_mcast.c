/*
 * NewMadeleine
 * Copyright (C) 2016-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


/** @file example of a dynamic mcast built using RPC.
 * Used as a testbed for interface @ref mcast_interface.
 */

#include <stdlib.h>
#include <stdio.h>

#include <nm_launcher_interface.h>
#include <nm_coll_interface.h>
#include <nm_rpc_interface.h>

#define NLOOPS 10

/** @file
 * This file is an example of a multicast implemented through RPC, using
 * nm_core_inject_chunk() to inject data in the receiver code path as if
 * it had been received from the network.
 */

/* ********************************************************* */

/** internal status of mcast module */
struct nm_mcast_s
{
  nm_comm_t p_comm;
  nm_rpc_service_t p_service;
};

/** header of RPC requests used for mcast */
struct nm_mcast_header_s
{
  nm_len_t hlen;
  nm_len_t len;
  nm_tag_t tag;
  nm_seq_t seq;
  nm_session_hash_t session;
  int rank_from;   /**< rank of root sender, in comm_world communicator */
};

/** state of a received mcast packet, to share state between callbacks */
struct nm_mcast_pkt_s
{
  struct nm_mcast_header_s header;
  nm_rpc_token_t p_rpc_token;
  struct nm_req_s*p_req;
};

/* ********************************************************* */

/** This function is called when an injected chunk is matched with a user request, thus
 * we can actually post the receive to receive data in place.
 * This function can also by called during peeking (nm_sr_recv_peek()), so it
 * can be called several times for the same request.
 */
static void nm_mcast_notify_req(struct nm_req_s*p_req, const struct nm_data_s*p_data0, nm_len_t chunk_offset, nm_len_t chunk_len, void*p_ref)
{
  struct nm_mcast_pkt_s*p_pkt = p_ref;
  fprintf(stderr, "# nm_mcast_notify_req()- p_req = %p; chunk_offset = %ld; chunk_len = %ld (data size = %ld)\n",
          p_req, chunk_offset, chunk_len, nm_data_size(p_data0));
  p_pkt->p_req = p_req;
  struct nm_data_s data;
  const struct nm_data_s*p_data = NULL;
  if((chunk_offset > 0) || (chunk_len < nm_data_size(p_data0)))
    {
      /* adjust data to the actual chunk length & offset */
      nm_data_excerpt_build(&data, (struct nm_data_s*)p_data0, chunk_offset, chunk_len);
      p_data = &data;
    }
  else
    {
      p_data = p_data0;
    }
  if(chunk_offset + chunk_len <= p_pkt->header.hlen)
    {
      /* receive the user header (specified by nm_rpc_req_set_hlen(), different
       * from nm_rpc_req_pack_header() !). Data is available immediately. */
      fprintf(stderr, "# nm_mcast_notify_req()- receiving as header\n");
      nm_rpc_recv_header_data(p_pkt->p_rpc_token, (struct nm_data_s*)p_data);
    }
  else
    {
      /* receive data body. Data will be available after the finalizer has been called. */
      fprintf(stderr, "# nm_mcast_notify_req()- receiving as body\n");
      nm_rpc_irecv_body_data(p_pkt->p_rpc_token, (struct nm_data_s*)p_data);
    }
  if(chunk_offset + chunk_len >= p_pkt->header.len)
    {
      /* notify that all receive requests have been posted for this RPC incoming request.
      * (needed because we called nm_rpc_token_delay()) */
      fprintf(stderr, "# nm_mcast_notify_req()- signaling token complete\n");
      nm_rpc_token_complete(p_pkt->p_rpc_token);
    }
  fprintf(stderr, "# nm_mcast_notify_req()- done.\n");
}

/** RPC handler: this function is called once for each RPC message received.
 */
static void nm_mcast_rpc_handler(nm_rpc_token_t p_token)
{
  nm_comm_t p_comm = nm_rpc_service_get_ref(nm_rpc_get_service(p_token));
  struct nm_core*p_core = nm_core_get_singleton();
  /* initialize & fill a mcast packet descriptor */
  struct nm_mcast_pkt_s*p_pkt = malloc(sizeof(struct nm_mcast_pkt_s));
  nm_rpc_token_set_ref(p_token, p_pkt);
  p_pkt->header.len = NM_LEN_UNDEFINED;
  p_pkt->p_rpc_token = p_token;
  p_pkt->p_req = NULL;

  /* receive mcast header (set by nm_rpc_req_pack_header()) */
  nm_rpc_recv_header(p_token, &p_pkt->header, sizeof(p_pkt->header));
  fprintf(stderr, "# nm_mcast_rpc_handler()- received header: msg len = %lu; seq = %d; hlen = %ld; from = %d\n",
         p_pkt->header.len, p_pkt->header.seq, p_pkt->header.hlen, p_pkt->header.rank_from);

  /* Signal this RPC handler didn't post any recv yet (and maybe won't
   * post any recv in the handler at all).
   * Warning- it needs to be before nm_core_inject_chunk(), since they
   * may trigger (or not) immediate completion and unlock the token.
   * Since we used nm_rpc_token_delay(), we will have to explicitely
   * call nm_rpc_token_complete() to notify the RPC service that we
   * posted all receives for this token.
   */
  nm_rpc_token_delay(p_token);

  nm_gate_t p_gate = nm_comm_get_gate(p_comm, p_pkt->header.rank_from);
  nm_core_tag_t core_tag = nm_core_tag_build(p_pkt->header.session, p_pkt->header.tag);
  if(p_pkt->header.hlen > 0)
    {
      /* user asked for a non-empty header; inject the header separately, to make it
      * immediately available, without waiting for the body.
      * Here, is_last_chunk is equivalent to "the whole message contains only a
      * header" */
      const int is_last_chunk = p_pkt->header.hlen == p_pkt->header.len;
      nm_core_inject_chunk(p_core, p_gate, core_tag, p_pkt->header.seq,
                           0 /* chunk_offset */, p_pkt->header.hlen, is_last_chunk,
                           &nm_mcast_notify_req, p_pkt);
    }
  if(p_pkt->header.len > p_pkt->header.hlen)
    {
      /* inject the data body in the core, if present. The injected chunk is
         only a placeholder, data won't be available before the rpc finalizer
         is called */
      nm_core_inject_chunk(p_core, p_gate, core_tag, p_pkt->header.seq,
                           p_pkt->header.hlen, p_pkt->header.len -  p_pkt->header.hlen, 1 /* last chunk */,
                           &nm_mcast_notify_req, p_pkt);
    }
  fprintf(stderr, "# nm_mcast_rpc_handler()- done.\n");
}

/** RPC finalizer: this function is called when the RPC is fully received */
static void nm_mcast_rpc_finalizer(nm_rpc_token_t p_token)
{
  nm_comm_t p_comm = nm_rpc_service_get_ref(nm_rpc_get_service(p_token));
  struct nm_core*p_core = nm_core_get_singleton();
  struct nm_mcast_pkt_s*p_pkt = nm_rpc_token_get_ref(p_token);
  fprintf(stderr, "# nm_mcast_rpc_finalize()\n");
  /* notify the core injector that data for this request has arrived */
  nm_core_inject_complete_finalize(p_core, p_pkt->p_req, 0, p_pkt->header.len);
  free(p_pkt);
}

/* ********************************************************* */

const nm_tag_t mcast_tag = 0x42;
static struct nm_mcast_s mcast = { .p_comm = NULL, .p_service = NULL };

/** initialize the mcast interface */
static void nm_mcast_init(void)
{
  mcast.p_comm = nm_comm_world("nm_mcast_private"); /* private communicator used for RPCs */
  mcast.p_service = nm_rpc_register(nm_comm_get_session(mcast.p_comm), mcast_tag, NM_TAG_MASK_FULL,
                                    &nm_mcast_rpc_handler, &nm_mcast_rpc_finalizer, mcast.p_comm);
}

/** destroys mcast interface */
static void nm_mcast_finalize(void)
{
  nm_rpc_unregister(mcast.p_service);
  nm_comm_destroy(mcast.p_comm);
}

/** send an mcast message on the given communicator, to the list of given ranks.
 * @note only comm/ranks addressing is available instead of gates, since addresses
 * need to be serializable on the network.
 * @param hlen header length, i.e. part of length of p_data that needs to be
 * sent eagerly so as to be immediately available in the receiver handler. Set
 * to 0 if not needed.
 */
static void nm_mcast_send(nm_comm_t p_comm, int*ranks, int n_ranks, nm_tag_t tag, struct nm_data_s*p_data, nm_len_t hlen)
{
  struct nm_core*p_core = nm_core_get_singleton();
  int rank = -1;
  nm_launcher_get_rank(&rank);

  int i;
  for(i = 0; i < n_ranks; i++)
    {
      /* This code is only a proof of concept. Data is sent to all nodes
         with a loop instead of a tree. */
      if(ranks[i] != rank)
        {
          /* get packet information from local nmad core: core_tag, seq, and session hash_code,
             so as to be able to inject it in the core at destination */
          nm_core_tag_t core_tag = nm_core_tag_build(nm_comm_get_session(p_comm)->hash_code, tag);
          nm_gate_t p_gate = nm_comm_get_gate(p_comm, ranks[i]);
          const nm_seq_t seq = nm_core_send_seq_get(p_core, p_gate, core_tag);
          struct nm_mcast_header_s header =
            {
             .hlen      = hlen,
             .len       = nm_data_size(p_data),
             .tag       = tag,
             .seq       = seq,
             .session   = nm_comm_get_session(p_comm)->hash_code,
             .rank_from = nm_comm_get_dest(mcast.p_comm, nm_comm_get_gate(p_comm, rank))
            };
          fprintf(stderr, "# sending to rank = %d; tag = %lu; seq = %d\n", ranks[i], tag, seq);
          nm_rpc_req_t p_req = nm_rpc_req_init(mcast.p_service, p_gate, mcast_tag);
          nm_rpc_req_pack_header(p_req, &header, sizeof(header));
          nm_rpc_req_pack_body_data(p_req, p_data);
          nm_rpc_req_set_hlen(p_req, hlen);
          nm_rpc_req_isend(p_req);
          nm_rpc_req_wait(p_req);
        }
    }
}

/* ********************************************************* */

int main(int argc, char**argv)
{
  /* generic nmad init */
  nm_launcher_init(&argc, argv);
  nm_session_t p_session = NULL;
  nm_session_open(&p_session, "nm_rpc_mcast");
  int size = 0;
  nm_launcher_get_size(&size);
  int rank = -1;
  nm_launcher_get_rank(&rank);

  int*dests = malloc(sizeof(int) * (size - 1));
  int n_dests = 0;
  int i;
  for(i = 0; i < size; i++)
    {
      if(i != rank)
        {
          dests[n_dests] = i;
          n_dests++;
        }
    }
  nm_comm_t p_comm = nm_comm_world("nm_rpc_mcast");
  const char msg[] = "Hello, world!";
  const nm_len_t msg_size = strlen(msg) + 1;
  const nm_tag_t tag = 0x02;

  nm_mcast_init();

  nm_sr_request_t requests[NLOOPS];
  int k;
  for(k = 0; k < NLOOPS; k++)
    {

      const int hlen = 8;
      if(rank == 0)
        {
          /* send mcast, with an explicit user header of 8 bytes */
          struct nm_data_s data;
          nm_data_contiguous_build(&data, (char*)msg, msg_size);
          nm_mcast_send(p_comm, dests, n_dests, tag, &data, hlen);
        }
      else
        {
          nm_session_t p_session = nm_comm_get_session(p_comm);
          char*buffer = malloc(msg_size);

          /* post a receive request without attached data */

          /** Method 1: detailed recv **/
          nm_sr_recv_init(p_session, &requests[k]);
          nm_sr_recv_match(p_session, &requests[k], NM_ANY_GATE, tag, NM_TAG_MASK_FULL);
          nm_sr_recv_post(p_session, &requests[k]);
          /* wait for _some_ data to be available (actually: at least the user header) */
          nm_sr_recv_data_wait(p_session, &requests[k]);
          /* peek to get the user header */
          struct nm_data_s data;
          nm_data_contiguous_build(&data, buffer, hlen);
          int rc = nm_sr_recv_peek(p_session, &requests[k], &data);
          assert(rc == NM_ESUCCESS);
          /* discard the user header (peek didn't consume data, but injected
             data cannot be read twice) */
          nm_sr_recv_offset(p_session, &requests[k], hlen);
          /* post the full receive (the first hlen bytes of this requests[k] won't be
             received here, since we already received them in the peek and explicitely
             discarded them) */
          nm_sr_recv_unpack_contiguous(p_session, &requests[k], buffer, msg_size);

          /** Method 2: classic, easy recv **/
          /*nm_sr_irecv(p_session, 0, tag, buffer, msg_size, &requests[k]);*/

          nm_sr_rwait(p_session, &requests[k]);
          fprintf(stderr, "Received message: %s\n", buffer);
          free(buffer);
        }
    }

  /* generic nmad termination */
  nm_coll_barrier(p_comm, 0xF2);
  nm_mcast_finalize();
  nm_comm_destroy(p_comm);
  nm_session_close(p_session);
  nm_launcher_exit();
  return 0;
}

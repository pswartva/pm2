/*
 * NewMadeleine
 * Copyright (C) 2006-2022 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#ifndef NM_ONESIDED_INTERFACE_H
#define NM_ONESIDED_INTERFACE_H

#include <nm_public.h>
#include <nm_sendrecv_interface.h>
#include <nm_onesided_private.h>

#include <stdint.h>

/** @defgroup onesided_interface One-sided interface.
 * This is the onesided interface, for Remote Memory Access such as get/put/fence.
 * @example nm_onesided_simple.c
 */

/** @file
 * @ingroup onesided_interface
 */

/* ********************************************************* */

/** @defgroup onesided_init Initialization functions for onesided
 * @ingroup onesided_interface
 * @{
 */

/** an instance of the 'onesided' interface */
typedef struct nm_onesided_s*nm_onesided_t;

/** initialize the interface, using the given session.
 * The call is not strictly collective, but a node needs to call
 * this function to be able to use onesided interface as an
 * initiator or as a target.
 * May be called on multiple sessions for multiple independant channels.
 */
void nm_onesided_init(nm_session_t p_session, nm_onesided_t*pp_onesided);

void nm_onesided_finalize(nm_onesided_t p_onesided);

/** @} */


/* ** basic ops ******************************************** */

/** @defgroup onesided_ops Basic operations for onesided.
 * The basic RMA operations: put/get/fence.
 * @ingroup onesided_interface
 * @{
 */

/** request type for 'onesided' interface */
typedef struct nm_onesided_request_s nm_onesided_request_t;

/** remotely write to destination memory; local data is p_ptr with length len; dest_addr is
 * the address in the remote memory space
 */
void nm_onesided_iput(nm_onesided_t p_onesided, struct nm_gate_s*p_gate,
                      const void*p_ptr, nm_len_t len, uintptr_t dest_addr, nm_onesided_request_t*p_req);

/** remotely write to destination memory; local data described by p_data; dest_addr is
 * the address in the remote memory space
 * (same as above with data described by an nm_data)
 */
void nm_onesided_iput_data(nm_onesided_t p_onesided, struct nm_gate_s*p_gate,
                           struct nm_data_s*p_data, uintptr_t dest_addr, nm_onesided_request_t*p_req);

/** remotely read data from target memory. */
void nm_onesided_iget(nm_onesided_t p_onesided, struct nm_gate_s*p_gate,
                      void*p_ptr, nm_len_t len, uintptr_t dest_addr, nm_onesided_request_t*p_req);

/** remotely read data from target memory (nm_data flavor). */
void nm_onesided_iget_data(nm_onesided_t p_onesided, struct nm_gate_s*p_gate,
                           struct nm_data_s*p_data, uintptr_t dest_addr, nm_onesided_request_t*p_req);

/** ensures all put operations to the remote node are completed */
void nm_onesided_ifence(nm_onesided_t p_onesided, struct nm_gate_s*p_gate, nm_onesided_request_t*p_req);

/** wait for this request completion */
void nm_onesided_req_wait(nm_onesided_request_t*p_req);

/** wait for completion of a vector of requests */
void nm_onesided_req_wait_all(nm_onesided_request_t**p_reqs, int n);

/** test for completion of a request */
int nm_onesided_req_test(nm_onesided_request_t*p_req);

/** @} */

/* ** remote queues **************************************** */

/** @defgroup onesided_queue Remote queues.
 * Enqueue messages remotely in receiver queue.
 * Queue creation is a local operation. Queue id is then transmited
 * by the user. Remote enqueue has a local completion; it doesn't
 * wait for remote completion. Upon error, messages are discarded.
 *
 * @ingroup onesided_interface
 * @{
 * @example nm_onesided_queues.c
 */

/** local id for a queue */
typedef int nm_onesided_queue_id_t;

/** create a local queue, accessible remotely */
nm_onesided_queue_id_t nm_onesided_queue_create(nm_onesided_t p_onesided);

/** enqueue a message in the given remote queue, non-blocking version */
void nm_onesided_queue_ienqueue(nm_onesided_t p_onesided, struct nm_gate_s*p_gate, nm_onesided_queue_id_t queue,
                                const void*p_ptr, nm_len_t len, nm_onesided_request_t*p_req);

/** enqueue a message in the given remote queue, blocking version */
void nm_onesided_queue_enqueue(nm_onesided_t p_onesided, struct nm_gate_s*p_gate, nm_onesided_queue_id_t queue,
                               const void*p_ptr, nm_len_t len);

/** try to dequeue an entry from local queue; return NULL if queue is empty.
 * The caller takes ownership of data and must free it after use.
 */
void*nm_onesided_queue_dequeue(nm_onesided_t p_onesided, nm_onesided_queue_id_t queue);

/** @} */

/* ** target-side completions ****************************** */

/** @defgroup onesided_target Target-side completion for onesided.
 * Explicitely wait for one-sided operation completions on target side.
 * Additionnal data may be attached to the completion request.
 * @ingroup onesided_interface
 * @{
 */

/** request type to receive target-side completions */
typedef struct nm_onesided_target_request_s nm_onesided_target_request_t;

/** send a put request with target-side completion.
 * p_ptr & len is data for the onesided operation
 * p_target_ptr & target_len will be delivered to the target-side completion.
 * p_target_ptr may be NULL to only notify completion without attached data.
 * maximum for target_len is 64 bytes.
 */
void nm_onesided_iput_with_target(nm_onesided_t p_onesided, struct nm_gate_s*p_gate,
                                  void*p_ptr, nm_len_t len, uintptr_t dest_addr,
                                  void*p_target_ptr, nm_len_t target_len,
                                  nm_onesided_request_t*p_req);

/** send a get request with target-side completion*/
void nm_onesided_iget_with_target(nm_onesided_t p_onesided, struct nm_gate_s*p_gate,
                                  void*p_ptr, nm_len_t len, uintptr_t dest_addr,
                                  void*p_target_ptr, nm_len_t target_len,
                                  nm_onesided_request_t*p_req);

/** posts a recv request for target-side completion */
void nm_onesided_target_irecv(nm_onesided_t p_onesided, nm_gate_t p_gate, void*p_ptr, nm_len_t len,
                              nm_onesided_target_request_t*p_req);

/** wait completion of a request on target side */
void nm_onesided_target_wait(nm_onesided_target_request_t*p_req);

/** get the source of a request */
nm_gate_t nm_onesided_target_get_source(nm_onesided_target_request_t*p_req);

/** get the address of a request */
uintptr_t nm_onesided_target_get_addr(nm_onesided_target_request_t*p_req);

/** get the size of a request (size of the onesided operation, not the target-side completion data) */
nm_len_t nm_onesided_target_get_size(nm_onesided_target_request_t*p_req);

/** @} */


/* ** blocking versions ************************************ */

/** @defgroup onesided_blocking Blocking operations
 * Basic one-sided operations in their blocking flavor
 * @ingroup onesided_interface
 * @{
 */

/** blocking version of iput */
void nm_onesided_put(nm_onesided_t p_onesided, struct nm_gate_s*p_gate,
                     void*p_ptr, nm_len_t len, uintptr_t dest_addr);

/** blocking version of iget */
void nm_onesided_get(nm_onesided_t p_onesided, struct nm_gate_s*p_gate,
                     void*p_ptr, nm_len_t len, uintptr_t dest_addr);

/** blocking version of ifence */
void nm_onesided_fence(nm_onesided_t p_onesided, struct nm_gate_s*p_gate);

/** @} */


#endif /* NM_ONESIDED_INTERFACE_H */

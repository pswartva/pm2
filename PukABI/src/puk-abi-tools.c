/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @ingroup PukABI
 */

#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>
#include <stdarg.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/mman.h>
#include <sys/un.h>
#include <unistd.h>
#include <string.h>
#include <poll.h>
#include <fcntl.h>

#include "Puk-ABI.h"

static const void*puk_abi_sym_resolve_next(const char*symbol);

extern char **environ;

extern int puk_abi_init_done;

void puk_abi_preinit(void) __attribute__((constructor));
void puk_abi_preinit(void)
{
  if(puk_abi_init_done)
    return;
  puk_abi_init_done = 1;
  /* ** pre-init symbol table with bare minimum for dlsym() */
  /* pre-heat malloc/free before virtual symbols are hooked */
  void*tmp_malloc = malloc(1);
  free(tmp_malloc);
  tmp_malloc = calloc(1, 1);
  free(tmp_malloc);
  /* resolve next errno and plug it as *real* */
  struct puk_abi_sym_s*errno_desc = PUK_ABI_SYM_GETDESC(puk_abi_errno_sym)();
  const void*next_errno = puk_abi_sym_resolve_next(errno_desc->name);
  errno_desc->ptr_real = next_errno;
  /* hook memory wrappers */
#ifdef PUKABI_ENABLE_MEM
  puk_abi_mem_init();
#endif /* PUKABI_ENABLE_MEM */

  int verbose = 0;
  if(environ != NULL)
    {
      if(getenv("PUKABI_VERBOSE"))
        {
          verbose = 1;
        }
    }
  else
    {
      /* libc not initialized yet. Ask the kernel for our own environment.
       * @note Linux only.
       */
      const int envsize = 32768;
      char*self_env = malloc(envsize);
      memset(self_env, 0, envsize);
      int fd = PUK_ABI_FSYS_WRAP(open)("/proc/self/environ", O_RDONLY);
      if(fd > 0)
        {
          PUK_ABI_FSYS_WRAP(read)(fd, self_env, envsize);
          PUK_ABI_FSYS_WRAP(close)(fd);
        }
      const char*ptr = self_env;
      while(*ptr != '\0')
        {
          const char*found = strstr(ptr, "PUKABI_VERBOSE=yes");
          if(found)
            verbose = 1;
          ptr += strlen(ptr) + 1;
        }
      free(self_env);
    }
  if(verbose)
    {
      fprintf(stderr, "PukABI: preinit.\n");
    }
}


#define MAXMSG 1024
static ssize_t (*write_func)(int fildes, const void *buf, size_t nbyte) = NULL;

#ifdef PUK_ABI_TRACE_ON
static int puk_abi_sym_debug = 1;
#else
static int puk_abi_sym_debug = 0;
#endif

#ifdef PUK_ABI_CHECK_ASSERT
extern void puk_abi_do_assert(int cond, const char*file, int line, const char*func)
{
  if(!cond)
    {
      puk_abi_fatal("# PukABI: assertion failed in %s %s:%d\n", func, file, line);
    }
}
#endif

/** resolve next entry in symbol table (supposedly 'real' symbol from libc)
 */
static const void*puk_abi_sym_resolve_next(const char*symbol)
{
  const void*fun = dlsym(RTLD_NEXT, symbol);
  if(fun == NULL)
    {
      puk_abi_trace("# PukABI: WARNING- symbol %s not found!", symbol);
    }
  return fun;
}

/** resolve next symbol and registers it as 'real' in PukABI table
 */
extern const void*puk_abi_real_symbol(struct puk_abi_sym_s*sym)
{
  const void*fun = puk_abi_sym_resolve_next(sym->name);
  if(!fun)
    {
      puk_abi_fatal("# PukABI: REAL symbol %s not found.\n", sym->name);
    }
  puk_abi_trace("# PukABI: resolving REAL symbol '%s'.", sym->name);
  sym->ptr_real = fun;
  return fun;
}

void puk_abi_trace(char*msg, ...)
{
#ifdef PUK_ABI_SHOW_STACKTOP
  int stack_top = 0;
#endif
  char debugmsg[MAXMSG];
  if(puk_abi_sym_debug)
    {
      int s = 0;
      int done = 0;
      va_list ap;
      va_start(ap, msg);
#ifdef PUK_ABI_TRACE_PID
      s = snprintf(debugmsg, MAXMSG, "# PukABI: [%d] ", getpid());
#endif
#ifdef PUK_ABI_SHOW_STACKTOP
      s = snprintf(debugmsg, MAXMSG, "%lX: ", ((unsigned long)&stack_top)>>12);
#endif
      s += vsnprintf(debugmsg + s, MAXMSG, msg, ap);
      s += snprintf(debugmsg + s, MAXMSG, "\n");
      if(!write_func)
        {
          write_func = puk_abi_sym_resolve_next("write");
        }
      do
        {
          done += (*write_func)(2, debugmsg, s-done);
        }
      while(done < s);
      va_end(ap);
    }
}

void puk_abi_print(char*msg, ...)
{
#ifdef PUK_ABI_SHOW_STACKTOP
  int stack_top = 0;
#endif
  char debugmsg[MAXMSG];
  int s = 0;
  int done = 0;
  va_list ap;
  va_start(ap, msg);
  memset(debugmsg, 0, MAXMSG);
  s += vsnprintf(debugmsg + s, MAXMSG, msg, ap);
  puk_abi_assert(s > 0);
#ifdef PUK_ABI_SHOW_STACKTOP
  s +=  snprintf(debugmsg + s, MAXMSG, " (stack top: %p)", (void*)(((unsigned long)&stack_top)>>12));
#endif
  s +=  snprintf(debugmsg + s, MAXMSG, "\n");
  if(!write_func)
    {
      write_func = puk_abi_sym_resolve_next("write");
    }
  do
    {
      done += (*write_func)(2, debugmsg, s-done);
    }
  while(done < s);
  va_end(ap);
}

void puk_abi_fatal(char*msg, ...)
{
  char debugmsg[MAXMSG];
  int s;
  va_list ap;
  va_start(ap, msg);
  s = vsnprintf(debugmsg, MAXMSG, msg, ap);
  if(!write_func)
    {
      write_func = puk_abi_sym_resolve_next("write");
    }
  (*write_func)(2, debugmsg, s);
  va_end(ap);
  abort();
}

/* ********************************************************* */

extern void puk_abi_set_spinlock_handlers(void (*acquire)(void),
                                          void (*release)(void),
                                          int (*trylock)(void))
{
  puk_spinlock.acquire = acquire;
  puk_spinlock.release = release;
  puk_spinlock.trylock = trylock;
}

extern void puk_abi_disable_preload(void)
{
  putenv("LD_PRELOAD=");
}

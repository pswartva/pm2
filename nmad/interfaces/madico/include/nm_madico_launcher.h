/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

/** @file
 * init code using madico shared accross madico, pmi2 & pmix launchers
 */

#ifndef NM_MADICO_LAUNCHER_H
#define NM_MADICO_LAUNCHER_H

#include <Padico/Topology.h>
#include <Padico/NetSelector.h>

static nm_drv_vect_t nm_madico_launcher_default_selector(const char*url, void*_arg)
{
  struct nm_launcher_gates_s*p_gates = _arg;
  const int size = p_gates->size;
  int i = 0;
  while(i < size)
    {
      if((p_gates->p_gates[i].p_url != NULL) && (strcmp(p_gates->p_gates[i].p_url, url) == 0))
        {
          nm_drv_vect_t p_drvs = nm_drv_vect_copy(&p_gates->p_gates[i].drvs);
          return p_drvs;
        }
      i++;
    }
  return NULL;
}


/** use PadicoTM NetSelector to populate gates with driver.
 */
static inline void nm_launcher_gates_populate_ns(struct nm_launcher_gates_s*p_gates, padico_topo_node_t*p_peers)
{
  const char*nmad_driver = padico_getenv("NMAD_DRIVER");
  if(!nmad_driver)
    {
      struct nm_component_hashtable_s loaded_components;
      nm_component_hashtable_init(&loaded_components);
      int i;
      for(i = 0; i < p_gates->size; i++)
        {
          nm_drv_vect_init(&p_gates->p_gates[i].drvs);
          const puk_component_t component_1 =
            padico_ns_serial_selector(p_peers[i], "trk_small", puk_iface_NewMad_minidriver());
          const puk_component_t component_2 =
            padico_ns_serial_selector(p_peers[i], "trk_large", puk_iface_NewMad_minidriver());
          if(component_1 == NULL)
            NM_FATAL("no suitable component to connect small trk to node %s\n",
                     padico_topo_node_getname(p_peers[i]));
          if(component_2 == NULL)
            NM_FATAL("no suitable component to connect large trk to node %s\n",
                     padico_topo_node_getname(p_peers[i]));

          nm_drv_t p_drv1 = nm_component_hashtable_lookup(&loaded_components, component_1);
          if(p_drv1 == NULL)
            {
              p_drv1 = nm_launcher_add_driver(component_1, nm_trk_small);
              nm_component_hashtable_insert(&loaded_components, component_1, p_drv1);
            }
          nm_drv_vect_push_back(&p_gates->p_gates[i].drvs, p_drv1);
          nm_drv_t p_drv2 = nm_component_hashtable_lookup(&loaded_components, component_2);
          if(p_drv2 == NULL)
            {
              p_drv2 = nm_launcher_add_driver(component_2, nm_trk_large);
              nm_component_hashtable_insert(&loaded_components, component_2, p_drv2);
            }
          nm_drv_vect_push_back(&p_gates->p_gates[i].drvs, p_drv2);
        }
      nm_launcher_set_selector(&nm_madico_launcher_default_selector, p_gates);
      nm_component_hashtable_destroy(&loaded_components);
    }
}


#endif /* NM_MADICO_LAUNCHER_H */

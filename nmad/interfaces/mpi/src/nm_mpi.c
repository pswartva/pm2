/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


#include "nm_mpi_private.h"
#include "nm_mpi_private_syms.h"
#include <assert.h>
#include <errno.h>

#include <Padico/Puk.h>
#include <Padico/Module.h>
PADICO_MODULE_BUILTIN(MadMPI, NULL, NULL, NULL);

/** an entry of handler-based objects, used to check cleanup in debug mode */
struct nm_mpi_cleanup_entry_s
{
  void (*p_func)(void*);
  void*p_arg;
};
PUK_VECT_TYPE(nm_mpi_cleanup_entry, struct nm_mpi_cleanup_entry_s);

static struct
{
  int init_done;
  int finalize_done;
  int thread_level;
  void*buffer;         /**< buffer given by MPI_Buffer_attach() */
  ssize_t buffer_size;
  struct nm_mpi_cleanup_entry_vect_s cleanup;
} nm_mpi =
  {
    .init_done = 0, .finalize_done = 0, .thread_level = 0,
    .buffer = NULL, .buffer_size = MPI_UNDEFINED,
    .cleanup = PUK_VECT_STATIC_INITIALIZER
  };
#ifdef NMAD_PROFILE
__PUK_SYM_INTERNAL struct nm_mpi_profiling_s nm_mpi_profile = { 0 };
#endif /* NMAD_PROFILE */


/* ********************************************************* */
/* Aliases */

NM_MPI_ALIAS(MPI_Init, mpi_init);
NM_MPI_ALIAS(MPI_Init_thread, mpi_init_thread);
NM_MPI_ALIAS(MPI_Initialized, mpi_initialized);
NM_MPI_ALIAS(MPI_Query_thread, mpi_query_thread);
NM_MPI_ALIAS(MPI_Finalize, mpi_finalize);
NM_MPI_ALIAS(MPI_Finalized, mpi_finalized);
NM_MPI_ALIAS(MPI_Abort, mpi_abort);
NM_MPI_ALIAS(MPI_Get_processor_name, mpi_get_processor_name);
NM_MPI_ALIAS(MPI_Wtime, mpi_wtime);
NM_MPI_ALIAS(MPI_Wtick, mpi_wtick);
NM_MPI_ALIAS(MPI_Get_version, mpi_get_version);
NM_MPI_ALIAS(MPI_Get_library_version, mpi_get_library_version);
NM_MPI_ALIAS(MPI_Get_address, mpi_get_address);
NM_MPI_ALIAS(MPI_Address, mpi_address);
NM_MPI_ALIAS(MPI_Alloc_mem, mpi_alloc_mem);
NM_MPI_ALIAS(MPI_Free_mem, mpi_free_mem);
NM_MPI_ALIAS(MPI_Buffer_attach, mpi_buffer_attach);
NM_MPI_ALIAS(MPI_Buffer_detach, mpi_buffer_detach);

/** stub implementation to be overriden by profiling library */
int MPI_Pcontrol(const int level, ...) __attribute__((weak));
NM_MPI_ALIAS(MPI_Pcontrol, mpi_pcontrol);

/* ********************************************************* */

static const char* const nm_mpi_thread_labels[4] =
  {
    [ MPI_THREAD_SINGLE ]     = "single",
    [ MPI_THREAD_FUNNELED ]   = "funneled",
    [ MPI_THREAD_SERIALIZED ] = "serialized",
    [ MPI_THREAD_MULTIPLE ]   = "multiple"
  };

__PUK_SYM_INTERNAL
int nm_mpi_thread_level_get(int required)
{
  /* runtime check for consistency */
  assert(MPI_THREAD_SINGLE     == NM_THREAD_SINGLE);
  assert(MPI_THREAD_FUNNELED   == NM_THREAD_FUNNELED);
  assert(MPI_THREAD_SERIALIZED == NM_THREAD_SERIALIZED);
  assert(MPI_THREAD_MULTIPLE   == NM_THREAD_MULTIPLE);
  int thread_level = -1;
#if defined(PIOMAN) && defined(PIOMAN_MULTITHREAD)
  thread_level = MPI_THREAD_MULTIPLE;
#else
  if(required == MPI_THREAD_MULTIPLE)
    thread_level = MPI_THREAD_SERIALIZED;
  else
    thread_level = required;
#endif
  return thread_level;
}

int mpi_init(int*argc, char***argv)
{
  int provided = 0;
  int err = mpi_init_thread(argc, argv, MPI_THREAD_SINGLE, &provided);
  return err;
}

int mpi_init_thread(int*argc, char***argv, int required, int*provided)
{
  padico_puk_init();
  /* load MadMPI builtin module */
  puk_mod_t mod = NULL;
  padico_puk_mod_resolve(&mod, "MadMPI");
  if(mod == NULL)
    {
      NM_FATAL("failed to load MadMPI builtin module.\n");
    }
  padico_puk_mod_load(mod);

  nm_mpi.thread_level = nm_mpi_thread_level_get(required);
  nm_core_set_thread_level(nm_mpi.thread_level);
  *provided = (nm_mpi.thread_level > required) ? required : nm_mpi.thread_level;
  if(required > *provided)
    {
      padico_out(puk_verbose_critical, "required thread level = %s; provided = %s\n",
                 nm_mpi_thread_labels[required], nm_mpi_thread_labels[*provided]);
    }

  nm_launcher_init(argc, argv ? *argv : NULL);

  nm_mpi_err_lazy_init();
  nm_mpi_info_lazy_init();
  nm_mpi_attrs_lazy_init();
  nm_mpi_group_lazy_init();
  nm_mpi_comm_lazy_init();
  nm_mpi_datatype_lazy_init();
  nm_mpi_op_lazy_init();
  nm_mpi_request_lazy_init();
  nm_mpi_io_init();
  nm_mpi_win_init();
  nm_mpi_rma_init();
#ifdef NMAD_PROFILE
  puk_profile_var_def(unsigned_long, level, &nm_mpi_profile.cur_req_send,
                      "nm_mpi", "nm_mpi.cur_req_send", "current number of simultaneous send requests");
  puk_profile_var_def(unsigned_long, level, &nm_mpi_profile.cur_req_recv,
                      "nm_mpi", "nm_mpi.cur_req_recv", "current number of simultaneous recv requests");
  puk_profile_var_def(unsigned_long, level, &nm_mpi_profile.cur_req_total,
                      "nm_mpi", "nm_mpi.cur_req_total", "current number of simultaneous requests (send+recxv)");
  puk_profile_var_def(unsigned_long, highwatermark, &nm_mpi_profile.max_req_send,
                      "nm_mpi", "nm_mpi.max_req_send", "maximum number of simultaneous send requests");
  puk_profile_var_def(unsigned_long, highwatermark, &nm_mpi_profile.max_req_recv,
                      "nm_mpi", "nm_mpi.max_req_recv", "maximum number of simultaneous recv requests");
  puk_profile_var_def(unsigned_long, highwatermark, &nm_mpi_profile.max_req_total,
                      "nm_mpi", "nm_mpi.max_req_total", "maximum number of simultaneous requests (send+recxv)");
#endif /* NMAD_PROFILE */
  nm_mpi.init_done = 1;
  return MPI_SUCCESS;
}

int mpi_query_thread(int*provided)
{
  if(!nm_mpi.init_done)
    {
      *provided = MPI_UNDEFINED;
      return NM_MPI_ERROR_WORLD(MPI_ERR_OTHER);
    }
  *provided = nm_mpi.thread_level;
  return MPI_SUCCESS;
}

int mpi_initialized(int*flag)
{
  *flag = nm_mpi.init_done;
  return MPI_SUCCESS;
}

int mpi_finalized(int*flag)
{
  *flag = nm_mpi.finalize_done;
  return MPI_SUCCESS;
}

int mpi_finalize(void)
{
  if(nm_mpi.finalize_done)
    {
      NM_MPI_WARNING("MPI_Finalize() called multiple time.\n");
      return NM_MPI_ERROR_WORLD(MPI_ERR_OTHER);
    }
  mpi_barrier(MPI_COMM_WORLD);
  nm_mpi_rma_exit();
  nm_mpi_win_exit();
  nm_mpi_io_exit();
  nm_mpi_request_lazy_exit();
  nm_mpi_datatype_lazy_exit();
  nm_mpi_op_lazy_exit();
  nm_mpi_comm_lazy_exit();
  nm_mpi_group_lazy_exit();
  nm_mpi_attrs_lazy_exit();
  nm_mpi_info_lazy_exit();
  nm_mpi_err_lazy_exit();
  nm_mpi.init_done = 0;
  nm_mpi.finalize_done = 1;
  nm_launcher_exit();
  padico_puk_shutdown();
  return MPI_SUCCESS;
}

int mpi_abort(MPI_Comm comm __attribute__((unused)), int errorcode)
{
  fprintf(stderr, "# madmpi: FATAL- MPI_Abort err = %d\n", errorcode);
  nm_launcher_abort(errorcode);
  abort();
  return errorcode;
}


int mpi_get_processor_name(char*name, int*resultlen)
{
  int err;
  err = gethostname(name, MPI_MAX_PROCESSOR_NAME);
  if (!err)
    {
      *resultlen = strlen(name);
      return MPI_SUCCESS;
    }
  else
    {
      return MPI_ERR_UNKNOWN;
    }
}

double mpi_wtime(void)
{
  static puk_tick_t orig_time;
  static int orig_done = 0;
  if(!orig_done)
    {
      PUK_GET_TICK(orig_time);
      orig_done = 1;
    }
  puk_tick_t time;
  PUK_GET_TICK(time);
  const double usec = PUK_TIMING_DELAY(orig_time, time);
  return usec / 1000000.0;
}

double mpi_wtick(void) {
  return 1e-7;
}

int mpi_get_version(int*version, int*subversion)
{
  *version = MPI_VERSION;
  *subversion = MPI_SUBVERSION;
  return MPI_SUCCESS;
}

int mpi_get_library_version(char*version, int*resultlen)
{
  sprintf(version, "%s", NMAD_VERSION_STRING);
  *resultlen = strlen(version);

  return MPI_SUCCESS;
}

int mpi_get_address(void *location, MPI_Aint *address)
{
  /* This is the "portable" way to generate an address.
     The difference of two pointers is the number of elements
     between them, so this gives the number of chars between location
     and ptr.  As long as sizeof(char) == 1, this will be the number
     of bytes from 0 to location */
  *address = (uintptr_t)location;
  return MPI_SUCCESS;
}

int mpi_address(void *location, MPI_Aint *address)
{
  return MPI_Get_address(location, address);
}

int mpi_alloc_mem(MPI_Aint size, MPI_Info info, void*_ptr)
{
  void**ptr = _ptr;
  *ptr = padico_malloc(size);
  return MPI_SUCCESS;
}

int mpi_free_mem(void*ptr)
{
  padico_free(ptr);
  return MPI_SUCCESS;
}

int mpi_buffer_attach(void*buffer, int size)
{
  nm_mpi.buffer = buffer;
  nm_mpi.buffer_size = size;
  return MPI_SUCCESS;
}
int mpi_buffer_detach(void*buffer_addr, int*size)
{
  void**buffer = buffer_addr;
  *buffer = nm_mpi.buffer;
  *size = nm_mpi.buffer_size;
  nm_mpi.buffer = NULL;
  nm_mpi.buffer_size = MPI_UNDEFINED;
  return MPI_SUCCESS;
}

int mpi_pcontrol(const int level, ...)
{
  return MPI_SUCCESS;
}

__PUK_SYM_INTERNAL
void nm_mpi_cleanup_register(void(*p_func)(void*), void*p_arg)
{
  struct nm_mpi_cleanup_entry_s e = { . p_func = p_func, .p_arg = p_arg };
  nm_mpi_cleanup_entry_vect_push_back(&nm_mpi.cleanup, e);
}

__PUK_SYM_INTERNAL
void nm_mpi_cleanup_unregister(void(*p_func)(void*), void*p_arg)
{
  nm_mpi_cleanup_entry_vect_itor_t i;
  puk_vect_foreach(i, nm_mpi_cleanup_entry, &nm_mpi.cleanup)
    {
      if( (i->p_func == p_func) && (i->p_arg == p_arg) )
        {
          nm_mpi_cleanup_entry_vect_erase(&nm_mpi.cleanup, i);
          break;
        }
    }
}

static void nm_mpi_fini(void) __attribute__((destructor));
static void nm_mpi_fini(void)
{
#ifdef NMAD_DEBUG
  nm_mpi_cleanup_entry_vect_itor_t i;
  puk_vect_foreach(i, nm_mpi_cleanup_entry, &nm_mpi.cleanup)
    {
      (*i->p_func)(i->p_arg);
    }
#endif /* NMAD_DEBUG */
  nm_mpi_cleanup_entry_vect_destroy(&nm_mpi.cleanup);
}

#! /bin/sh

export M4PATH=./building-tools:../building-tools:${M4PATH}

${AUTOCONF:-autoconf}
${AUTOHEADER:-autoheader} -f

if [ ! -r ./mpi_sync_clocks ]; then
    if [ -r ../mpi_sync_clocks ]; then
	ln -s ../mpi_sync_clocks .
    else
	echo "Cannot find mpi_sync_clocks sub-module"
	exit 1
    fi
fi
( cd mpi_sync_clocks ; ./autogen.sh )


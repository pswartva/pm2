/*
 * NewMadeleine
 * Copyright (C) 2006-2021 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

/** @file example & tests for builtin data iterators
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>

#include "../common/nm_examples_helper.h"

int main(int argc, char**argv)
{
  nm_examples_init_topo(&argc, argv, NM_EXAMPLES_TOPO_PAIRS);

  uint64_t len = 4096;
  char*buf = malloc((size_t)len);

  if(is_server)
    {
      /* server side */
      nm_sr_request_t request;
      clear_buffer(buf, len);
      struct nm_data_s data;

      /* data as a contiguous block */
      nm_data_contiguous_build(&data, buf, len);
      nm_sr_irecv_data(p_session, NM_ANY_GATE, 0, &data, &request);
      nm_sr_rwait(p_session, &request);
      control_buffer(buf, len);

      /* each byte as an iovec entry */
      struct iovec*v = malloc(sizeof(struct iovec) * len);
      int i;
      for(i = 0; i < len ; i++)
        {
          /* fill iovec in reverse order, to really send it as iovec (otherwise it would be recognized as contiguous) */
          v[len - i - 1].iov_base = buf + i;
          v[len - i - 1].iov_len = 1;
        }
      nm_data_iov_build(&data, v, len); /* warning: v must remain allocated while p_data is in use */
      nm_sr_irecv_data(p_session, NM_ANY_GATE, 0, &data, &request);
      nm_sr_rwait(p_session, &request);
      control_buffer(buf, len);
      free(v);

      /* datav: vector of nm_data */
      struct nm_datav_s datav;
      nm_datav_init(&datav);
      for(i = 0; i < len ; i++)
        {
          struct nm_data_s d;
          nm_data_contiguous_build(&d, buf + len - i - 1, 1);
          nm_datav_add_chunk_data(&datav, &d); /* nm_data is _copied_ */
        }
      nm_data_datav_build(&data, &datav);
      nm_sr_irecv_data(p_session, NM_ANY_GATE, 0, &data, &request);
      nm_sr_rwait(p_session, &request);
      control_buffer(buf, len);
      nm_datav_destroy(&datav);

    }
  else
    {
      /* client side */
      nm_sr_request_t request;
      fill_buffer(buf, len);
      struct nm_data_s data;

      nm_data_contiguous_build(&data, buf, len);
      nm_sr_isend_data(p_session, p_gate, 0, &data, &request);
      nm_sr_swait(p_session, &request);

      struct iovec*v = malloc(sizeof(struct iovec) * len);
      int i;
      for(i = 0; i < len ; i++)
        {
          v[len - i - 1].iov_base = buf + i;
          v[len - i - 1].iov_len = 1;
        }
      nm_data_iov_build(&data, v, len);
      nm_sr_isend_data(p_session, p_gate, 0, &data, &request);
      nm_sr_swait(p_session, &request);
      free(v);

      struct nm_datav_s datav;
      nm_datav_init(&datav);
      for(i = 0; i < len ; i++)
        {
          struct nm_data_s d;
          nm_data_contiguous_build(&d, buf + len - i - 1, 1);
          nm_datav_add_chunk_data(&datav, &d); /* nm_data is _copied_ */
        }
      nm_data_datav_build(&data, &datav);
      nm_sr_isend_data(p_session, p_gate, 0, &data, &request);
      nm_sr_swait(p_session, &request);
      nm_datav_destroy(&datav);
    }

  free(buf);
  nm_examples_exit();
  return 0;
}

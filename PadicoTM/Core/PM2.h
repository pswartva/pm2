/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief PM2 (Marcel & Madeleine) interface
 * @ingroup TaskManager
 */

#ifndef PADICO_PM2_H
#define PADICO_PM2_H

#include <Padico/Puk.h>
#include <sched.h>
#include "PadicoTM_config.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef PIOMAN
#include <pioman.h>
#endif
#ifdef PADICO_MARCEL
#include <marcel.h>
#define MARCEL_CREATE_DETACHED 1
#endif

  /* clean-up namespace- yes, it's a hack, but it works! */
#ifdef BEGIN
#  undef BEGIN
#endif
#ifdef END
#  undef END
#endif
#ifdef EXCEPTION
#  undef EXCEPTION
#endif
#ifdef RAISE
#  undef RAISE
#endif

  /* *** pthread-less *************************************** */

#ifdef PADICO_PTHREAD
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <pthread.h>

  /* types */
#define marcel_t              pthread_t
#define marcel_attr_t         pthread_attr_t
#define marcel_mutex_t        pthread_mutex_t
#define marcel_cond_t         pthread_cond_t
#define marcel_key_t          pthread_key_t
  /* create */
#define marcel_create         pthread_create
#define marcel_join           pthread_join
#define marcel_detach         pthread_detach
#define marcel_exit           pthread_exit
#define marcel_self           pthread_self
#define marcel_yield          pthread_yield
#define marcel_yield_to(TID)  pthread_yield()
#define marcel_attr_init      pthread_attr_init
#define marcel_attr_setdetachstate pthread_attr_setdetachstate
#define MARCEL_CREATE_DETACHED PTHREAD_CREATE_DETACHED
#define marcel_attr_setrealtime(ATTR, CLASS) ((void)0)
  /* lock */
#define marcel_mutex_init     pthread_mutex_init
#define marcel_mutex_destroy  pthread_mutex_destroy
#define marcel_mutex_lock     pthread_mutex_lock
#define marcel_mutex_trylock(M) (!(pthread_mutex_trylock(M)))
#define marcel_mutex_unlock   pthread_mutex_unlock
  /* cond */
#define marcel_cond_init      pthread_cond_init
#define marcel_cond_destroy   pthread_cond_destroy
#define marcel_cond_wait      pthread_cond_wait
#define marcel_cond_timedwait pthread_cond_timedwait
#define marcel_cond_signal    pthread_cond_signal
#define marcel_cond_broadcast pthread_cond_broadcast
  /* spinlock */
#define pmarcel_spinlock_t    pthread_spinlock_t
#define pmarcel_spin_init     pthread_spin_init
#define pmarcel_spin_destroy  pthread_spin_destroy
#define pmarcel_spin_lock     pthread_spin_lock
#define pmarcel_spin_unlock   pthread_spin_unlock
#define pmarcel_spin_trylock  pthread_spin_trylock
  /* specific */
#define marcel_key_create     pthread_key_create
#define marcel_key_delete     pthread_key_delete
#define marcel_getspecific    pthread_getspecific
#define marcel_setspecific    pthread_setspecific
  /* pmarcel locking */
#define pmarcel_mutex_t            pthread_mutex_t
#define pmarcel_mutexattr_t        pthread_mutexattr_t
#define pmarcel_mutexattr_init     pthread_mutexattr_init
#define pmarcel_mutexattr_settype  pthread_mutexattr_settype
#define pmarcel_mutex_init         pthread_mutex_init
#define pmarcel_mutex_lock         pthread_mutex_lock
#define pmarcel_mutex_unlock       pthread_mutex_unlock
#define PMARCEL_MUTEX_RECURSIVE_NP PTHREAD_MUTEX_RECURSIVE_NP

  /* misc (unsupported) */
#define marcel_nbthreads() (1)
#define marcel_sig_disable_interrupts() ((void)0)

#endif /* PADICO_PTHREAD */

#ifdef PADICO_ABT
#include <abt.h>


#endif /* PADICO_ABT */


  /** Give a symbolic name to a Marcel thread */
  void              padico_tm_thread_givename(const char*name);

  void              padico_tm_thread_givename_tid(const char*name, marcel_t tid);

  /** Show statistics about Marcel scheduler internal state */
  void              padico_tm_thread_top(void);


#ifdef __cplusplus
} /* extern "C" */
#endif


#endif /* PADICO_PM2_H */

/*
 * NewMadeleine
 * Copyright (C) 2017-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <stdint.h>
#include <unistd.h>
#include <fcntl.h>
#include <poll.h>
#include <limits.h>
#include <sys/uio.h>
#include <sys/poll.h>
#include <string.h>
#include <errno.h>
#include <assert.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <sys/utsname.h>


#include <nm_minidriver.h>
#include <nm_private.h>
#include <Padico/Module.h>

static void*nm_tcp_instantiate(puk_instance_t instance, puk_context_t context);
static void nm_tcp_destroy(void*status);

static const struct puk_component_driver_s nm_tcp_component =
  {
    .instantiate = &nm_tcp_instantiate,
    .destroy     = &nm_tcp_destroy
  };

static void nm_tcp_getprops(puk_context_t context, struct nm_minidriver_properties_s*props);
static void nm_tcp_init(puk_context_t context, const void**drv_url, size_t*url_size);
static void nm_tcp_close(puk_context_t context);
static void nm_tcp_connect(void*_status, const void*remote_url, size_t url_size);
static void nm_tcp_disconnect(void*_status);
static int  nm_tcp_send_iov_post(void*_status, const struct iovec*v, int n);
static int  nm_tcp_send_poll(void*_status);
static int  nm_tcp_recv_iov_post(void*_status,  struct iovec*v, int n);
static int  nm_tcp_recv_poll_one(void*_status);
static int  nm_tcp_recv_probe_any(puk_context_t p_context, void**_status);
static int  nm_tcp_recv_wait_any(puk_context_t p_context, void**_status);
static int  nm_tcp_recv_cancel_any(puk_context_t p_context);

static const struct nm_minidriver_iface_s nm_tcp_minidriver =
  {
    .getprops         = &nm_tcp_getprops,
    .init             = &nm_tcp_init,
    .close            = &nm_tcp_close,
    .connect          = &nm_tcp_connect,
    .disconnect       = &nm_tcp_disconnect,
    .send_iov_post    = &nm_tcp_send_iov_post,
    .send_data_post   = NULL,
    .send_buf_get     = NULL,
    .send_buf_post    = NULL,
    .send_poll        = &nm_tcp_send_poll,
    .recv_iov_post    = &nm_tcp_recv_iov_post,
    .recv_data_post   = NULL,
    .recv_buf_poll    = NULL,
    .recv_buf_release = NULL,
    .recv_poll_one    = &nm_tcp_recv_poll_one,
    .recv_probe_any   = &nm_tcp_recv_probe_any,
    .recv_wait_any    = &nm_tcp_recv_wait_any,
    .recv_cancel      = NULL,
    .recv_cancel_any  = &nm_tcp_recv_cancel_any
  };

/* ********************************************************* */

PADICO_MODULE_COMPONENT(Minidriver_tcp,
  puk_component_declare("Minidriver_tcp",
                        puk_component_provides("PadicoComponent", "component", &nm_tcp_component),
                        puk_component_provides("NewMad_minidriver", "minidriver", &nm_tcp_minidriver),
                        puk_component_attr("wait_any", "auto")));

/* ********************************************************* */

#define NM_TCP_URL_SIZE_MAX 64
/** identity of peer node */
struct nm_tcp_peer_id_s
{
  char url[NM_TCP_URL_SIZE_MAX];
};
/** pending connection */
struct nm_tcp_pending_s
{
  int fd;                       /**< socket of the pending connection */
  struct nm_tcp_peer_id_s peer; /**< url of the peer node */
};
PUK_VECT_TYPE(nm_tcp_pending, struct nm_tcp_pending_s);

PUK_VECT_TYPE(nm_tcp_status, struct nm_tcp_s*);

/** 'tcp' driver per-context data. */
struct nm_tcp_context_s
{
  struct nm_tcp_status_vect_s p_statuses;
  struct pollfd*fds;        /**< pollfd used for recv_poll|wait_any */
  int nfds;                 /**< size of above fds */
  int round_robin;          /**< first fd to poll for fairness */
  int cancel_fd[2];         /**< pipe used to cancel a recv_wait */
  int rebuild;              /**< ask to rebuild fds before polling */
  struct sockaddr_in addr;  /**< server socket address */
  int server_fd;            /**< server socket fd */
  char*url;                 /**< server url */
  nm_tcp_pending_vect_t pending_fds;
};

/** 'tcp' per-instance status. */
struct nm_tcp_s
{
  int fd;                   /**< tcp socket */
  int error;
  struct nm_tcp_context_s*p_tcp_context;
  struct
  {
    nm_len_t size;
    struct iovec*v;
    int n;
    nm_len_t todo;
    struct iovec*full_v;
  } recv;
  struct
  {
    nm_len_t size;
    struct iovec*v;
    int n;
    nm_len_t todo;
    struct iovec*full_v;
  } send;
};


/* ********************************************************* */

static void*nm_tcp_instantiate(puk_instance_t instance, puk_context_t context)
{
  struct nm_tcp_s*p_status = padico_malloc(sizeof(struct nm_tcp_s));
  struct nm_tcp_context_s*p_tcp_context = puk_context_get_status(context);
  if(p_tcp_context == NULL)
    {
      NM_FATAL("trying to instantiate before context init.");
    }
  p_status->fd = -1;
  p_status->error = 0;
  p_status->p_tcp_context = p_tcp_context;
  p_status->send.v = NULL;
  p_status->send.full_v = NULL;
  p_status->recv.v = NULL;
  p_status->recv.full_v = NULL;
  nm_tcp_status_vect_push_back(&p_tcp_context->p_statuses, p_status);
  return p_status;
}

static void nm_tcp_destroy(void*_status)
{
  struct nm_tcp_s*p_status = _status;
  if(p_status->send.full_v != NULL)
    padico_free(p_status->send.full_v);
  if(p_status->recv.full_v != NULL)
    padico_free(p_status->recv.full_v);
  padico_free(p_status);
}

/* ********************************************************* */

static void nm_tcp_getprops(puk_context_t context, struct nm_minidriver_properties_s*p_props)
{
  const char*s_wait_any = puk_context_getattr(context, "wait_any");
  int wait_any = 1;
  if(strcmp(s_wait_any, "auto") != 0)
    {
      wait_any = atoi(s_wait_any);
      NM_DISPF("wait_any = %d; forced by user.\n", wait_any);
    }
  p_props->capabilities.supports_recv_any = 1;
  p_props->capabilities.supports_wait_any = 1;
  p_props->capabilities.prefers_wait_any = wait_any;
  p_props->capabilities.supports_iovec = 1;
  p_props->profile.latency = 15000; /* 15 usec.*/
  p_props->profile.bandwidth = 1150; /* assume 10G-Ethernet for now */
  p_props->nickname = "tcp";
  /* update bandwidth with detected value */
  struct in_addr in_addr = puk_inet_getaddr(); /* default IP address*/
  char*ifname = puk_inet_get_ifname_byaddr(&in_addr);
  assert(ifname != NULL);
  const int link_speed = puk_inet_get_link_speed(ifname);
  if(link_speed > 0)
    {
      padico_out(puk_verbose_info, "detected link speed = %d MB/s; iface = %s\n", link_speed, ifname);
      p_props->profile.bandwidth = link_speed * 0.92; /* assume 8% of protocol overhead */
    }
  else
    {
      padico_out(puk_verbose_info, "failed to detect link speed for iface = %s\n", ifname);
    }
  padico_free(ifname);
}

static void nm_tcp_init(puk_context_t context, const void**drv_url, size_t*url_size)
{
  uint16_t port = 0;
  struct nm_tcp_context_s*p_tcp_context = padico_malloc(sizeof(struct nm_tcp_context_s));
  p_tcp_context->addr.sin_family = AF_INET;
  p_tcp_context->addr.sin_addr.s_addr = htonl(INADDR_ANY);
  p_tcp_context->addr.sin_port = htons(port);
  p_tcp_context->server_fd = NM_SYS(socket)(AF_INET, SOCK_STREAM, 0);
  int rc = NM_SYS(bind)(p_tcp_context->server_fd, (struct sockaddr*)&p_tcp_context->addr, sizeof(p_tcp_context->addr));
  if(rc != 0)
    {
      NM_FATAL("bind() error: %s\n", strerror(errno));
    }
  socklen_t addr_len = sizeof(p_tcp_context->addr);
  rc = NM_SYS(getsockname)(p_tcp_context->server_fd, (struct sockaddr*)&p_tcp_context->addr, &addr_len);
  if(rc != 0)
    {
      NM_FATAL("getsockname() error: %s\n", strerror(errno));
    }
  port = ntohs(p_tcp_context->addr.sin_port);
  char hostname[1024];
  gethostname(hostname, 1024);
  padico_string_t s_url = padico_string_new();
  padico_string_catf(s_url, "%s:%d", hostname, port);
  p_tcp_context->url = padico_strdup(padico_string_get(s_url));
  padico_string_delete(s_url);
  rc = NM_SYS(listen)(p_tcp_context->server_fd, 128);
  if(rc != 0)
    {
      NM_FATAL("listen() error: %s\n", strerror(errno));
    }
  nm_tcp_status_vect_init(&p_tcp_context->p_statuses);
  p_tcp_context->pending_fds = nm_tcp_pending_vect_new();
  p_tcp_context->fds = NULL;
  p_tcp_context->nfds = 0;
  p_tcp_context->rebuild = 1;
  p_tcp_context->round_robin = 0;
  NM_SYS(pipe)(p_tcp_context->cancel_fd);
  puk_context_set_status(context, p_tcp_context);
  *drv_url = p_tcp_context->url;
  *url_size = strlen(p_tcp_context->url) + 1;
  assert(*url_size <= NM_TCP_URL_SIZE_MAX);
}

static void nm_tcp_close(puk_context_t context)
{
  struct nm_tcp_context_s*p_tcp_context = puk_context_get_status(context);
  NM_SYS(close)(p_tcp_context->server_fd);
  puk_context_set_status(context, NULL);
  nm_tcp_status_vect_destroy(&p_tcp_context->p_statuses);
  nm_tcp_pending_vect_delete(p_tcp_context->pending_fds);
  if(p_tcp_context->fds)
    padico_free(p_tcp_context->fds);
  padico_free(p_tcp_context->url);
  padico_free(p_tcp_context);
}

static void nm_tcp_connect(void*_status, const void*remote_url, size_t url_size)
{
  struct nm_tcp_s*p_status = _status;
  struct nm_tcp_context_s*p_tcp_context = p_status->p_tcp_context;

  if(strcmp(p_tcp_context->url, remote_url) > 0)
    {
      /* ** connect */
      char*url2 = padico_strdup(remote_url);
      const char*remote_hostname = url2;
      char*remote_port = strchr(url2, ':');
      *remote_port = '\0';
      remote_port++;
      const uint16_t port = atoi(remote_port);
      struct hostent*host_entry = gethostbyname(remote_hostname);
      if(host_entry == NULL)
        {
          NM_FATAL("cannot resolve remote host: %s.\n", remote_hostname);
        }
      struct sockaddr_in addr;
      addr.sin_family = AF_INET;
      addr.sin_port = htons(port);
      memcpy(&addr.sin_addr.s_addr, host_entry->h_addr, host_entry->h_length);
      int fd = NM_SYS(socket)(AF_INET, SOCK_STREAM, 0);
      int rc = NM_SYS(connect)(fd, (struct sockaddr*)&addr, sizeof(addr));
      if(rc != 0)
        {
          NM_FATAL("cannot connect to %s:%d (%s)\n", remote_hostname, port, strerror(errno));
        }
      struct nm_tcp_peer_id_s id;
      memset(&id.url, 0, sizeof(id.url));
      strncpy(id.url, p_tcp_context->url, sizeof(id.url));
      rc = NM_SYS(send)(fd, &id, sizeof(id), MSG_WAITALL);
      if(rc != sizeof(id))
        {
          NM_FATAL("error while sending id to peer node.\n");
        }
      p_status->fd = fd;
      padico_free(url2);
    }
  else
    {
      int fd = -1;
      nm_tcp_pending_vect_itor_t i;
      puk_vect_foreach(i, nm_tcp_pending, p_tcp_context->pending_fds)
        {
          if(strcmp(i->peer.url, remote_url) == 0)
            {
              fd = i->fd;
              nm_tcp_pending_vect_erase(p_tcp_context->pending_fds, i);
              break;
            }
        }
      while(fd == -1)
        {
          fd = NM_SYS(accept)(p_tcp_context->server_fd, NULL, NULL);
          if(fd < 0)
            {
              NM_FATAL("error while accepting\n");
            }
          struct nm_tcp_peer_id_s id;
          int rc = NM_SYS(recv)(fd, &id, sizeof(id), MSG_WAITALL);
          if(rc != sizeof(id))
            {
              NM_FATAL("error while receiving peer node id.\n");
            }
          if(strcmp(id.url, remote_url) != 0)
            {
              struct nm_tcp_pending_s pending =
                {
                  .fd = fd,
                  .peer = id
                };
              nm_tcp_pending_vect_push_back(p_tcp_context->pending_fds, pending);
              fd = -1;
            }
        }
      p_status->fd = fd;
    }
  int val = 1;
  socklen_t len = sizeof(val);
  NM_SYS(setsockopt)(p_status->fd, IPPROTO_TCP, TCP_NODELAY, (void*)&val, len);
  p_tcp_context->rebuild = 1;
}

static void nm_tcp_disconnect(void*_status)
{
  struct nm_tcp_s*p_status = _status;
  struct nm_tcp_context_s*p_tcp_context = p_status->p_tcp_context;
  /* remove instance from global polling */
  nm_tcp_status_vect_itor_t itor = nm_tcp_status_vect_find(&p_tcp_context->p_statuses, p_status);
  nm_tcp_status_vect_erase(&p_tcp_context->p_statuses, itor);
  /* half-close for sending */
  NM_SYS(shutdown)(p_status->fd, SHUT_WR);
  /* flush (and throw away) remaining bytes in the pipe up to the EOS */
  int ret = -1;
  do
    {
      char dummy = 0;
      ret = NM_SYS(read)(p_status->fd, &dummy, sizeof(dummy));
    }
  while (ret > 0 || ((ret == -1 && errno == EAGAIN)));
  /* safely close fd when EOS is reached */
  NM_SYS(close)(p_status->fd);
  p_status->fd = -1;
}

static int nm_tcp_send_iov_post(void*_status, const struct iovec*v, int n)
{
  struct nm_tcp_s*p_status = _status;
  p_status->send.v = padico_malloc(sizeof(struct iovec) * (n + 2));
  p_status->send.size = 0;
  int i;
  for(i = 0; i < n; i++)
    {
      p_status->send.v[i+1] = (struct iovec){ .iov_base = v[i].iov_base, .iov_len = v[i].iov_len };
      p_status->send.size += v[i].iov_len;
    }
  p_status->send.v[0] = (struct iovec){ .iov_base = &p_status->send.size, .iov_len = sizeof(p_status->send.size) };
  p_status->send.v[n + 1].iov_base = NULL;
  p_status->send.n = n + 1;
  p_status->send.todo = p_status->send.size + sizeof(p_status->send.size);
  p_status->send.full_v = p_status->send.v;
  return NM_ESUCCESS;
}

static int nm_tcp_send_poll(void*_status)
{
  struct nm_tcp_s*p_status = _status;
  struct msghdr msg =
    {
      .msg_name       = NULL,
      .msg_namelen    = 0,
      .msg_iov        = p_status->send.v,
      .msg_iovlen     = p_status->send.n,
      .msg_controllen = 0
    };
  int rc = NM_SYS(sendmsg)(p_status->fd, &msg, MSG_DONTWAIT);
  const int err = errno;

  if(rc < 0 && err == EAGAIN)
    {
      return -NM_EAGAIN;
    }
  else if(rc < 0 && err == EPIPE)
    {
      NM_WARN("socket closed while sending message.\n");
      return -NM_ECLOSED;
    }
  else if(rc < 0 && err == EFAULT)
    {
      NM_FATAL("error %d while sending message (%s): invalid user pointer.\n", err, strerror(err));
    }
  else if(rc < 0)
    {
      NM_FATAL("error %d while sending message (%s).\n", err, strerror(err));
    }
  else if(rc < p_status->send.todo)
    {
      p_status->send.todo -= rc;
      while(rc > 0)
        {
          if(rc < p_status->send.v[0].iov_len)
            {
              const nm_len_t c = rc;
              p_status->send.v[0].iov_base += rc;
              p_status->send.v[0].iov_len -= rc;
              rc -= c;
            }
          else
            {
              const nm_len_t c = p_status->send.v[0].iov_len;
              p_status->send.v++;
              p_status->send.n--;
              rc -= c;
            }
        }
      return -NM_EAGAIN;
    }
  padico_free(p_status->send.full_v);
  p_status->send.full_v = NULL;
  p_status->send.v = NULL;
  return NM_ESUCCESS;
}

static int nm_tcp_recv_iov_post(void*_status, struct iovec*v, int n)
{
  struct nm_tcp_s*p_status = _status;
  p_status->recv.size = NM_LEN_UNDEFINED;
  p_status->recv.v = padico_malloc(sizeof(struct iovec) * (n + 1));
  /* @todo check whether we really need to copy v */
  int i;
  for(i = 0; i < n; i++)
    {
      p_status->recv.v[i] = (struct iovec){ .iov_base = v[i].iov_base, .iov_len = v[i].iov_len };
    }
  p_status->recv.v[n] = (struct iovec){ .iov_base = NULL, .iov_len = 0 };
  p_status->recv.n = n;
  p_status->recv.full_v = p_status->recv.v;
  return NM_ESUCCESS;
}

static int nm_tcp_recv_poll_one(void*_status)
{
  struct nm_tcp_s*p_status = _status;
  if(p_status->recv.size == NM_LEN_UNDEFINED)
    {
      /* receive message size */
      size_t hdr_todo = sizeof(p_status->recv.size);
      void*hdr_ptr = &p_status->recv.size;
      int rc = -1;
    hdr_retry:
      rc = NM_SYS(recv)(p_status->fd, hdr_ptr, hdr_todo, MSG_DONTWAIT);
      const int err = errno;
      if(rc > 0)
        {
          assert(rc <= hdr_todo);
          hdr_todo -= rc;
          hdr_ptr += rc;
          if(hdr_todo > 0)
            {
              /* truncated header- retry immediately */
              goto hdr_retry;
            }
        }
      else if(rc == 0)
        {
          p_status->error = 1;
          p_status->p_tcp_context->rebuild = 1;
          return -NM_ECLOSED;
        }
      else if(rc < 0 && err == EAGAIN)
        {
          if(hdr_todo == sizeof(p_status->recv.size))
            {
              return -NM_EAGAIN;
            }
          else
            {
              /* if we alreay got beginning of header, retry immediately */
              goto hdr_retry;
            }
        }
      else if(rc < 0)
        {
          NM_FATAL("error %d while reading header (%s).\n", err, strerror(err));
        }
      assert(hdr_todo == 0);
      /* truncate iovec to actual message size */
      p_status->recv.todo = 0;
      int i;
      for(i = 0; i < p_status->recv.n; i++)
        {
          if(p_status->recv.todo == p_status->recv.size)
            {
              p_status->recv.v[i].iov_len = 0;
            }
          else if(p_status->recv.v[i].iov_len + p_status->recv.todo > p_status->recv.size)
            {
              p_status->recv.v[i].iov_len = p_status->recv.size - p_status->recv.todo;
              p_status->recv.todo = p_status->recv.size;
            }
          else
            {
              p_status->recv.todo += p_status->recv.v[i].iov_len;
            }
        }
      if(p_status->recv.size > p_status->recv.todo)
        {
          NM_FATAL("received more data than expected (received = %lu; expected = %lu).\n",
                   p_status->recv.size, p_status->recv.todo);
        }
    }
  /* receive data */
  assert(p_status->recv.todo > 0);
  assert(p_status->recv.n > 0);
  struct msghdr msg =
    {
      .msg_name       = NULL,
      .msg_namelen    = 0,
      .msg_iov        = p_status->recv.v,
      .msg_iovlen     = p_status->recv.n,
      .msg_controllen = 0
    };
  int rc = NM_SYS(recvmsg)(p_status->fd, &msg, MSG_DONTWAIT);
  const int err = errno;
  if(rc < 0 && err == EAGAIN)
    {
      return -NM_EAGAIN;
    }
  else if((rc < 0 && err == EPIPE) || (rc == 0))
    {
      p_status->error = 1;
      p_status->p_tcp_context->rebuild = 1;
      return -NM_ECLOSED;
    }
  else if(rc < 0)
    {
      NM_FATAL("error %d while reading data (%s); rc = %d; size = %lu).\n",
               err, strerror(err), rc, p_status->recv.size);
    }
  else if(rc < p_status->recv.todo)
    {
      /* truncate iovec for incomplete recv */
      p_status->recv.todo -= rc;
      while(rc > 0)
        {
          if(rc < p_status->recv.v[0].iov_len)
            {
              const nm_len_t c = rc;
              p_status->recv.v[0].iov_base += rc;
              p_status->recv.v[0].iov_len -= rc;
              rc -= c;
            }
          else
            {
              const nm_len_t c = p_status->recv.v[0].iov_len;
              p_status->recv.v++;
              p_status->recv.n--;
              rc -= c;
            }
        }
      return -NM_EAGAIN;
    }
  else
    {
      assert(rc > 0);
      assert(p_status->recv.todo == rc);
      padico_free(p_status->recv.full_v);
      p_status->recv.full_v = NULL;
      p_status->recv.v = NULL;
      return NM_ESUCCESS;
    }
}

static int nm_tcp_recv_any_common(puk_context_t p_context, void**_status, int timeout)
{
  struct nm_tcp_s*p_status = NULL;
  struct nm_tcp_context_s*p_tcp_context = puk_context_get_status(p_context);
  int error = 0;
 retry:
  if(p_tcp_context->rebuild)
    {
      if(p_tcp_context->nfds != nm_tcp_status_vect_size(&p_tcp_context->p_statuses))
        {
          p_tcp_context->nfds = nm_tcp_status_vect_size(&p_tcp_context->p_statuses);
          p_tcp_context->fds = padico_realloc(p_tcp_context->fds, sizeof(struct pollfd) * (p_tcp_context->nfds + 1));
        }
      int i;
      for(i = 0; i < p_tcp_context->nfds; i++)
        {
          const struct nm_tcp_s*p_status = nm_tcp_status_vect_at(&p_tcp_context->p_statuses, i);
          if(!p_status->error)
            p_tcp_context->fds[i].fd = p_status->fd;
          else
            p_tcp_context->fds[i].fd = -p_status->fd;
          p_tcp_context->fds[i].events = POLLIN;
        }
      p_tcp_context->round_robin = 0;
      p_tcp_context->fds[p_tcp_context->nfds].fd     = p_tcp_context->cancel_fd[0];
      p_tcp_context->fds[p_tcp_context->nfds].events = POLLIN;
      p_tcp_context->rebuild = 0;
    }
  int timeout_ms;
  if(timeout < 0)
    timeout_ms = 100;
  else
    timeout_ms = timeout;
  int rc = NM_SYS(poll)(p_tcp_context->fds, p_tcp_context->nfds + 1, timeout_ms);
  if(rc == 0 && timeout < 0)
    {
      goto retry;
    }
  p_tcp_context->round_robin = (p_tcp_context->nfds > 0) ? ((p_tcp_context->round_robin + 1) % p_tcp_context->nfds) : 0;
  if(rc > 0)
    {
      int i;
      for(i = 0; i < p_tcp_context->nfds; i++)
        {
          const int k = (p_tcp_context->round_robin + i) % p_tcp_context->nfds;
          const short e = p_tcp_context->fds[k].revents;
          if(e)
            {
              if(e & POLLIN)
                {
                  assert(nm_tcp_status_vect_at(&p_tcp_context->p_statuses, k)->fd == p_tcp_context->fds[k].fd);
                  if(p_status == NULL)
                    {
                      p_status = nm_tcp_status_vect_at(&p_tcp_context->p_statuses, k);
                      break;
                    }
                }
              else if(e & (POLLERR | POLLHUP))
                {
                  NM_WARN("error for fd = %d\n", p_tcp_context->fds[k].fd);
                  nm_tcp_status_vect_at(&p_tcp_context->p_statuses, k)->error = 1;
                  p_tcp_context->fds[k].fd = -p_tcp_context->fds[k].fd;
                  p_tcp_context->rebuild = 1;
                  error = 1;
                }
              else if(e & POLLNVAL)
                {
                  NM_FATAL("invalid fd = %d in poll.\n", p_tcp_context->fds[k].fd);
                }
              else
                {
                  NM_FATAL("unexpected revent %d in poll.\n", e);
                }
            }
        }
      if(p_status)
        {
          *_status = p_status;
          return NM_ESUCCESS;
        }
      if(p_tcp_context->fds[p_tcp_context->nfds].revents & POLLIN)
        {
          char dummy;
          NM_SYS(read)(p_tcp_context->cancel_fd[0], &dummy, 1);
          *_status = NULL;
          return -NM_ECANCELED;
        }
      if(error)
        {
          *_status = NULL;
          return -NM_ECLOSED;
        }
      NM_WARN("unexpected return value from poll().\n");
      return -NM_EUNKNOWN;
    }
  else
    {
      *_status = NULL;
      return -NM_EAGAIN;
    }
}

static int nm_tcp_recv_probe_any(puk_context_t p_context, void**_status)
{
  return nm_tcp_recv_any_common(p_context, _status, 0);
}

static int nm_tcp_recv_wait_any(puk_context_t p_context, void**_status)
{
  return nm_tcp_recv_any_common(p_context, _status, -1);
}

static int nm_tcp_recv_cancel_any(puk_context_t p_context)
{
  struct nm_tcp_context_s*p_tcp_context = puk_context_get_status(p_context);
  char dummy = 0;
  NM_SYS(write)(p_tcp_context->cancel_fd[1], &dummy, 1);
  return NM_ESUCCESS;
}

/*
 * NewMadeleine
 * Copyright (C) 2006-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


#ifndef NM_CORE_H
#define NM_CORE_H

#ifndef NM_PRIVATE_H
#  error "Cannot include this file directly. Please include <nm_private.h>"
#endif

#ifndef PIOMAN
#include <pthread.h>
#endif

/** @ingroup nmad_private
 * @file
 * Definition for nmad core.
 */

/** lfqueue for pending tasks */
PUK_LFQUEUE_TYPE(nm_core_task, struct nm_core_task_s*, NULL, 1024);

/** Core NewMadeleine structure.
 */
struct nm_core
{
  nm_spinlock_t lock;                           /**< lock to protect req lists in core */
  nm_thread_level_t thread_level;

#ifdef PIOMAN
  struct piom_ltask ltask;                      /**< task used for main core progress */
#else /* PIOMAN */
  /* if PIOMan is enabled, it already manages a list of pw to poll */
  struct nm_pkt_wrap_list_s pending_send_list;  /**< active pw for send to poll */
  struct nm_pkt_wrap_list_s pending_recv_list;  /**< active pw for recv to poll */
#endif /* PIOMAN */
  struct nm_pkt_wrap_list_s prefetch_large_send; /**< large packets to prefetch */

#ifdef NMAD_HWLOC
  hwloc_topology_t topology;
#endif /* NMAD_HWLOC */

  puk_component_t strategy_component;              /**< component for the selected strategy */
  puk_context_t strategy_context;                  /**< global context of the strategy */
  const struct nm_strategy_iface_s*strategy_iface; /**< interface of the selected strategy */

  struct nm_gate_list_s gate_list;              /**< list of gates. */
  struct nm_drv_list_s driver_list;             /**< list of drivers. */
  int nb_drivers;                               /**< number of drivers in drivers list */

  struct nm_core_task_lfqueue_s pending_tasks;     /**< core tasks to execute asynchronously; enqueued from non-locked sections; read from locked sections */

  struct nm_req_chunk_allocator_s req_chunk_allocator;   /**< allocator for req_chunk elements */
  struct nm_ctrl_chunk_allocator_s ctrl_chunk_allocator; /**< allocator for control chunks */
  struct nm_pw_nohd_allocator_s pw_nohd_allocator; /**< allocator for header-less pw*/
  struct nm_pw_buf_allocator_s pw_buf_allocator;   /**< allocator for pw with contiguous buffer */
  uint64_t unpack_seq;                             /**< next sequence number for unpacks */
  int n_packs, n_unpacks;                          /**< number of pending packs & unpacks */
  struct nm_matching_tag_table_s tag_table;        /**< matching infor for requests with tag (and any gate) */
  struct nm_matching_wildcard_table_s wildcard_table; /**< matching info for wildcard requests */
  struct nm_core_monitor_vect_s monitors;          /**< monitors for upper layers to track events in nmad core */
  struct nm_active_gate_list_s active_gates;       /**< list of gates with active requests */

  struct nm_trk_hashtable_s trk_table;             /**< trk table hashed by component status; used for reverse lookup */

  struct nm_core_dispatching_event_lfqueue_s dispatching_events; /**< queue for events to dispatch */
  struct nm_core_dispatching_event_allocator_s dispatching_event_allocator; /**< allocator for elements of above list */
  nm_spinlock_t dispatching_lock;                  /**< lock for dispatching of events */

  int enable_schedopt;                          /**< whether schedopt is enabled atop drivers */
  int enable_auto_flush;                        /**< automatic flush after each pack_submit */
  int enable_isend_csum;                        /**< check message integrity between isend submission & completion */
  int enable_pwsend_timeout;
  int enable_send_prefetch;                     /**< whether to prefetch large messages for send while rdv is in progress */
  int enable_recv_prefetch;                     /**< whether to prefetch large messages for recv while rdv is in progress */

#ifdef NMAD_PROFILE
  puk_tick_t time_orig;
  struct
  {
    unsigned long long n_locks;
    unsigned long long n_schedule;
    unsigned long long n_packs;
    unsigned long long n_unpacks;
    unsigned long long n_unexpected;
    unsigned long long n_rdvs;
    unsigned long long n_rdvs_accept_small;
    unsigned long long n_rdvs_accept_single;
    unsigned long long n_rdvs_accept_split;
    unsigned long long n_pw_split;
    unsigned long long n_pw_out;
    unsigned long long n_pw_in;
    unsigned long long n_try_and_commit;
    unsigned long long n_strat_apply;
    unsigned long long n_outoforder_event;
    unsigned long long n_event_queue_full;
    unsigned long long n_packs_partitioned;
    unsigned long max_unpacks;
    unsigned long max_packs;
  } profiling;
#endif /* NMAD_PROFILE */
};

#endif /* NM_CORE_H */

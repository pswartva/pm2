/*
 * NewMadeleine
 * Copyright (C) 2006-2023 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

/** @file memory registration using Implicit On-Demand Paging with prefetch
 */

#include "nm_ibverbs.h"
#include "nm_ibverbs_rcache.h"
#include <stdint.h>

#include <Padico/Module.h>

PADICO_MODULE_BUILTIN(nm_ibverbs_rcache_iodp, NULL, NULL, NULL);

#if defined(NMAD_IBVERBS_IODP)

static int  nm_ibverbs_iodp_query(struct nm_ibverbs_context_s*p_ibverbs_context);
static void nm_ibverbs_iodp_init(struct nm_ibverbs_context_s*p_ibverbs_context);
static void nm_ibverbs_iodp_close(struct nm_ibverbs_context_s*p_ibverbs_context);
static void nm_ibverbs_iodp_mem_reg(struct nm_ibverbs_context_s*p_ibverbs_context, const void*ptr, size_t len,
                                    struct ibv_mr**pp_mr, void**pp_handle);
static void nm_ibverbs_iodp_mem_unreg(struct nm_ibverbs_context_s*p_ibverbs_context, const void*ptr, size_t len,
                                      struct ibv_mr*p_mr, void*p_handle);

const struct nm_ibverbs_reg_driver_s nm_ibverbs_reg_driver_iodp =
  {
   .query     = &nm_ibverbs_iodp_query,
   .init      = &nm_ibverbs_iodp_init,
   .close     = &nm_ibverbs_iodp_close,
   .mem_reg   = &nm_ibverbs_iodp_mem_reg,
   .mem_unreg = &nm_ibverbs_iodp_mem_unreg
  };

static struct nm_ibverbs_iodp_s
{
  struct ibv_mr*p_mr;
} nm_ibverbs_iodp = { .p_mr = NULL };

/* ********************************************************* */
/* ** Implicit On-Demand Paging with prefetch */

static int nm_ibverbs_iodp_query(struct nm_ibverbs_context_s*p_ibverbs_context)
{
  /* check device capabilities */
  struct ibv_device_attr_ex attrx;
  int rc = ibv_query_device_ex(p_ibverbs_context->hca.context, NULL, &attrx);
  if(rc)
    {
      NM_WARN("couldn't query device for its features\n");
      return 0;
    }
  if(!(attrx.odp_caps.general_caps & IBV_ODP_SUPPORT_IMPLICIT))
    {
      NM_DISPF("the device doesn't support implicit ODP\n");
      return 0;
    }
  return 1;
}

static void nm_ibverbs_iodp_init(struct nm_ibverbs_context_s*p_ibverbs_context)
{
  assert(nm_ibverbs_iodp_query(p_ibverbs_context));
  padico_out(puk_verbose_notice, "using implicit ODP.\n");
  /* register full memory for on-demand paging */
  assert(nm_ibverbs_iodp.p_mr == NULL);
  nm_ibverbs_iodp.p_mr = ibv_reg_mr(p_ibverbs_context->hca.pd, NULL, SIZE_MAX,
                                    IBV_ACCESS_REMOTE_WRITE | IBV_ACCESS_LOCAL_WRITE | IBV_ACCESS_ON_DEMAND);
  if(nm_ibverbs_iodp.p_mr == NULL)
    {
      NM_FATAL("error while registering the full memory with on-demand paging.\n");
    }
}

static void nm_ibverbs_iodp_close(struct nm_ibverbs_context_s*p_ibverbs_context)
{
  ibv_dereg_mr(nm_ibverbs_iodp.p_mr);
  nm_ibverbs_iodp.p_mr = NULL;
}

static void nm_ibverbs_iodp_mem_reg(struct nm_ibverbs_context_s*p_ibverbs_context, const void*ptr, size_t len,
                                    struct ibv_mr**pp_mr, void**pp_handle)
{
  /* prefetch */
  struct ibv_sge sg_list;
  sg_list.lkey = nm_ibverbs_iodp.p_mr->lkey;
  sg_list.addr = (uintptr_t)ptr;
  sg_list.length = len;
  int rc = ibv_advise_mr(p_ibverbs_context->hca.pd, IBV_ADVISE_MR_ADVICE_PREFETCH_WRITE,
                          IBV_ADVISE_MR_FLAG_FLUSH,
                          &sg_list, 1);
  if(rc != 0 && rc != ENOTSUP && rc != EOPNOTSUPP && rc != ENOSYS && rc != EPROTONOSUPPORT)
    {
      NM_WARN("couldn't prefetch MR; error %d (%s). Continue anyway\n", rc, strerror(rc));
    }
  *pp_mr = nm_ibverbs_iodp.p_mr;
}

static void nm_ibverbs_iodp_mem_unreg(struct nm_ibverbs_context_s*p_ibverbs_context, const void*ptr, size_t len,
                                         struct ibv_mr*p_mr, void*p_handle)
{
  /* empty */
}

#endif /* NMAD_IBVERBS_IODP */

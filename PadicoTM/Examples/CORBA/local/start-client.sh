#! /bin/sh

if [ "x${PADICO_CORE}" = "x" ]; then
    export PADICO_CORE=pthread
fi

padico-launch -v -DPADICO_BOOTSTRAP_MODE=client -DPADICO_BOOTSTRAP_HOST=localhost ../bench-CORBA-client


/*
 * NewMadeleine
 * Copyright (C) 2021-2022 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

/* Basic example / test using mcast interface to do a broadcast, received by a
 * regular recv. */

#include <stdlib.h>
#include <stdio.h>

#include <nm_launcher_interface.h>
#include <nm_coll_interface.h>
#include <nm_mcast_interface.h>

#include "../common/nm_examples_helper.h"

#define NB_EXCHANGES 10

int main(int argc, char** argv)
{
  const nm_tag_t tag = 0xbeef;
  const char msg[] = "Hello, world!";
  const nm_len_t msg_size = strlen(msg) + 1;
  int worldsize;

  nm_examples_init_topo(&argc, argv, NM_EXAMPLES_TOPO_STAR);

  nm_launcher_get_size(&worldsize);
  if(worldsize == 1)
    {
      fprintf(stderr, "We need several nodes.\n");
      nm_examples_exit();
      return EXIT_FAILURE;
    }

  nm_mcast_service_t mcast_service = nm_mcast_init(p_comm);

  fprintf(stderr, "# With barrier, different tags...\n");
  for (int k = 0; k < NB_EXCHANGES; k++)
    {
      if (is_server)
        {
          int* dests = malloc(sizeof(int) * (worldsize - 1));
          int i;
          for (i = 1; i < worldsize; i++)
            {
              dests[i-1] = i;
            }

          struct nm_data_s data;
          nm_data_contiguous_build(&data, (char*) msg, msg_size);
          nm_mcast_send(mcast_service, p_comm, dests, NULL, worldsize-1, tag+k, &data, 0, NM_COLL_TREE_DEFAULT);

          free(dests);
        }
      else
        {
          char* buffer = malloc(msg_size);

          nm_sr_recv(nm_comm_get_session(p_comm), 0, tag+k, buffer, msg_size);

          if (strcmp(buffer, msg))
            {
              NM_FATAL("Received msg: '%s' (should be '%s')\n", buffer, msg);
            }

          free(buffer);
        }

      nm_coll_barrier(p_comm, 0xF2);
    }

  nm_coll_barrier(p_comm, 0xF3);

  fprintf(stderr, "# With barrier, same tags...\n");
  for (int k = 0; k < NB_EXCHANGES; k++)
    {
      if (is_server)
        {
          int* dests = malloc(sizeof(int) * (worldsize - 1));
          int i;
          for (i = 1; i < worldsize; i++)
            {
              dests[i-1] = i;
            }

          struct nm_data_s data;
          nm_data_contiguous_build(&data, (char*) msg, msg_size);
          nm_mcast_send(mcast_service, p_comm, dests, NULL, worldsize-1, tag, &data, 0, NM_COLL_TREE_DEFAULT);

          free(dests);
        }
      else
        {
          char* buffer = malloc(msg_size);

          nm_sr_recv(nm_comm_get_session(p_comm), 0, tag, buffer, msg_size);

          if (strcmp(buffer, msg))
            {
              NM_FATAL("Received msg: '%s' (should be '%s')\n", buffer, msg);
            }

          free(buffer);
        }

      nm_coll_barrier(p_comm, 0xF4);
    }

  nm_coll_barrier(p_comm, 0xF5);

  fprintf(stderr, "# Without barrier, different tags...\n");
  for (int k = 0; k < NB_EXCHANGES; k++)
    {
      if (is_server)
        {
          int* dests = malloc(sizeof(int) * (worldsize - 1));
          int i;
          for (i = 1; i < worldsize; i++)
            {
              dests[i-1] = i;
            }

          struct nm_data_s data;
          nm_data_contiguous_build(&data, (char*) msg, msg_size);
          nm_mcast_send(mcast_service, p_comm, dests, NULL, worldsize-1, tag+k, &data, 0, NM_COLL_TREE_DEFAULT);

          free(dests);
        }
      else
        {
          char* buffer = malloc(msg_size);

          nm_sr_recv(nm_comm_get_session(p_comm), 0, tag+k, buffer, msg_size);

          if (strcmp(buffer, msg))
            {
              NM_FATAL("Received msg: '%s' (should be '%s')\n", buffer, msg);
            }

          free(buffer);
        }
    }

  nm_coll_barrier(p_comm, 0xF6);

  fprintf(stderr, "# Without barrier, same tags...\n");
  for (int k = 0; k < NB_EXCHANGES; k++)
    {
      if (is_server)
        {
          int* dests = malloc(sizeof(int) * (worldsize - 1));
          int i;
          for (i = 1; i < worldsize; i++)
            {
              dests[i-1] = i;
            }

          struct nm_data_s data;
          nm_data_contiguous_build(&data, (char*) msg, msg_size);
          nm_mcast_send(mcast_service, p_comm, dests, NULL, worldsize-1, tag, &data, 0, NM_COLL_TREE_DEFAULT);

          free(dests);
        }
      else
        {
          char* buffer = malloc(msg_size);

          nm_sr_recv(nm_comm_get_session(p_comm), 0, tag, buffer, msg_size);

          if (strcmp(buffer, msg))
            {
              NM_FATAL("Received msg: '%s' (should be '%s')\n", buffer, msg);
            }

          free(buffer);
        }
    }

  nm_coll_barrier(p_comm, 0xF7);

  fprintf(stderr, "# Without barrier, same tags, binary tree...\n");
  for (int k = 0; k < NB_EXCHANGES; k++)
    {
      if (is_server)
        {
          int* dests = malloc(sizeof(int) * (worldsize - 1));
          int i;
          for (i = 1; i < worldsize; i++)
            {
              dests[i-1] = i;
            }

          struct nm_data_s data;
          nm_data_contiguous_build(&data, (char*) msg, msg_size);
          nm_mcast_send(mcast_service, p_comm, dests, NULL, worldsize-1, tag, &data, 0, NM_COLL_TREE_BINARY);

          free(dests);
        }
      else
        {
          char* buffer = malloc(msg_size);

          nm_sr_recv(nm_comm_get_session(p_comm), 0, tag, buffer, msg_size);

          if (strcmp(buffer, msg))
            {
              NM_FATAL("Received msg: '%s' (should be '%s')\n", buffer, msg);
            }

          free(buffer);
        }
    }

  nm_coll_barrier(p_comm, 0xF8);

  fprintf(stderr, "# Without barrier, same tags, chain tree...\n");
  for (int k = 0; k < NB_EXCHANGES; k++)
    {
      if (is_server)
        {
          int* dests = malloc(sizeof(int) * (worldsize - 1));
          int i;
          for (i = 1; i < worldsize; i++)
            {
              dests[i-1] = i;
            }

          struct nm_data_s data;
          nm_data_contiguous_build(&data, (char*) msg, msg_size);
          nm_mcast_send(mcast_service, p_comm, dests, NULL, worldsize-1, tag, &data, 0, NM_COLL_TREE_CHAIN);

          free(dests);
        }
      else
        {
          char* buffer = malloc(msg_size);

          nm_sr_recv(nm_comm_get_session(p_comm), 0, tag, buffer, msg_size);

          if (strcmp(buffer, msg))
            {
              NM_FATAL("Received msg: '%s' (should be '%s')\n", buffer, msg);
            }

          free(buffer);
        }
    }

  nm_coll_barrier(p_comm, 0xF9);

  printf("success\n");

  nm_mcast_finalize(mcast_service);
  nm_examples_exit();
  return EXIT_SUCCESS;
}

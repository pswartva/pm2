/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Automatic strings and various encoding functions
 * @ingroup Puk
 */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

#include "Puk-internals.h"

#include "Module.h"

PADICO_MODULE_HOOK(Puk);

/* ** Puk strings ****************************************** */


/* ********************************************************* */
/* *** Encoding ******************************************** */

/* ** Hexadecimal encoding ********************************* */

static const char _hex_table[] =
"0123456789ABCDEF";

static const char _hex_reverse[] =
  {
    /*  32 */ -1, -1, -1, -1,  -1, -1, -1, -1,  -1, -1, -1, -1,  -1, -1, -1, -1,
    /*  48 */  0,  1,  2,  3,   4,  5,  6,  7,   8,  9, -1, -1,  -1, -1, -1, -1,
    /*  64 */ -1, 10, 11, 12,  13, 14, 15, -1,  -1, -1, -1, -1,  -1, -1, -1, -1,
    /*  80 */ -1, -1, -1, -1,  -1, -1, -1, -1,  -1, -1, -1, -1,  -1, -1, -1, -1,
    /*  96 */ -1, 10, 11, 12,  13, 14, 15, -1,  -1, -1, -1, -1,  -1, -1, -1, -1,
    /* 112 */ -1, -1, -1, -1,  -1, -1, -1, -1,  -1, -1, -1, -1,  -1, -1, -1, -1
  };

/** @internal */
static inline char reverse_hex(char c)
{
  return ( (c > 32 && c < 127) ? _hex_reverse[c - 32] : -1);
}
static inline char encode_hex(unsigned char c)
{
  int i =(int)c;
  assert(i < 16);
  return _hex_table[i];
}
char*puk_hex_encode(const void*_bytes, size_t*len, void*(*alloc)(unsigned long))
{
  size_t i;
  const size_t out_len = *len*2+1;
  char*text = alloc?(*alloc)(out_len):padico_malloc(out_len);
  char*p = text;
  const unsigned char*bytes = _bytes;
  memset(text, 0, out_len);
  for(i = 0; i < *len; i++)
    {
      unsigned char c = bytes[i];
      *(p++) = encode_hex((unsigned char)((c & 0xF0) >> 4));
      *(p++) = encode_hex((unsigned char)(c & 0x0F));
    }
  *p = 0;
  *len = p - text;
  assert(*len == strlen(text));
  return text;
}

void*puk_hex_decode(const char*text, size_t*len, void*(*alloc)(unsigned long))
{
  const size_t out_len = ((*len + 1) / 2) + 1;
  char*bytes = alloc?(*alloc)(out_len):padico_malloc(out_len);
  char*p = bytes;
  assert(bytes != NULL && *len == strlen(text));
  while(*text)
    {
      *(p++) = (reverse_hex(*text) << 4) | reverse_hex(*(text+1));
      text += 2;
    }
  *len = p-bytes;
  return bytes;
}

/* ** base16 encoding ************************************** */

/* 0-9 -> A-P = ASCII 65-80 */

static inline char base16_encode(unsigned char c)
{
  int i = (int)c;
  return 'A' + (i & 0x0F);
}
static inline char base16_decode(char c1, char c2)
{
  return (((c1 - 'A') & 0x0F) << 4) | ((c2 - 'A') & 0x0F);
}
char*puk_base16_encode(const void*_bytes, size_t*len, void*(*alloc)(unsigned long))
{
  size_t i;
  const size_t out_len = *len * 2 + 1;
  const size_t in_len = *len;
  char*text = alloc?(*alloc)(out_len):padico_malloc(out_len);
  char*p = text;
  const unsigned char*bytes = _bytes;
  for(i = 0; i < in_len; i++)
    {
      const unsigned char c = bytes[i];
      p[0] = base16_encode(c >> 4);
      p[1] = base16_encode(c);
      p += 2;
    }
  *p = 0;
  *len = p - text;
  assert(*len == strlen(text));
  return text;
}

void*puk_base16_decode(const char*text, size_t*len, void*(*alloc)(unsigned long))
{
  const size_t out_len = ((*len + 1) / 2) + 1;
  char*bytes = alloc?(*alloc)(out_len):padico_malloc(out_len);
  char*p = bytes;
  assert(bytes != NULL && *len == strlen(text));
  while(*text)
    {
      *p = base16_decode(text[0], text[1]);
      text += 2;
      p++;
    }
  *len = p-bytes;
  return bytes;
}



/* ** escaped encoding ************************************* */

char*puk_escape_encode(const void*bytes, size_t*_len, void*(*alloc)(unsigned long))
{
  size_t i;
  const size_t len = *_len;
  const size_t out_len = len * 3 + 2;
  char*text = alloc?(*alloc)(out_len):padico_malloc(out_len);
  char*p = text;
  memset(text, 0, out_len);
  for(i = 0; i < len; i++)
    {
      unsigned char c = ((unsigned char*)bytes)[i];
      if( ('a' <= c && c <= 'z') ||
          ('A' <= c && c <= 'Z') ||
          ('0' <= c && c <= '9'))
        {
          *(p++) = c;
        }
      else
        {
          *(p++) = '%';
          *(p++) = _hex_table[(c & 0xF0) >> 4];
          *(p++) = _hex_table[(c & 0x0F)];
        }
      *p = 0;
    }
  *_len = p - text;
  assert(*_len == strlen(text));
  return text;
}

void*puk_escape_decode(const char*text, size_t*len, void*(*alloc)(unsigned long))
{
  const size_t out_len = *len + 1;
  char*bytes = alloc?(*alloc)(out_len):padico_malloc(out_len);
  char*p = bytes;
  assert(bytes != NULL && *len == strlen(text));
  while(*text)
    {
      char c = *(text++);
      if(c == '%')
        {
          *(p++) = (reverse_hex(*text) << 4) | reverse_hex(*(text+1));
          text += 2;
        }
      else
        {
          *(p++) = c;
        }
    }
  *len = p-bytes;
  return bytes;
}


/* ** Puk error codes *************************************** */


padico_rc_t padico_rc_create(int _rc, const char*fmt, ...)
{
  padico_rc_t rc = NULL;
  int restart;
  va_list ap;
  if(fmt && fmt[0])
    {
      rc = padico_rc_new();
      rc->rc = _rc;
      rc->msg = padico_string_new();
      do
        {
          restart = 0;
          va_start(ap, fmt);
          int err = vsnprintf(rc->msg->_text, rc->msg->_capacity, fmt, ap);
          if((err < 0) || (err >= rc->msg->_capacity))
            {
              padico_string_grow(rc->msg, err);
              restart = 1;
            }
          va_end(ap);
        }
      while(restart);
      rc->msg->_size = strlen(rc->msg->_text);
    }
  else if(_rc)
    {
      rc = padico_rc_new();
      rc->rc = _rc;
      rc->msg = NULL;
    }
  return rc;
}

padico_rc_t padico_rc_cat(padico_rc_t rc1 , padico_rc_t rc2)
{
  if(rc2 == NULL)
    {
      return rc1;
    }
  if(rc1 == NULL)
    {
      rc1 = padico_rc_new();
    }
  if(rc1->rc || rc2->rc)
    {
      rc1->rc = -1;
    }
  if(rc1->msg == NULL)
    {
      rc1->msg = padico_string_new();
    }
  if(rc2->msg != NULL)
    {
      padico_string_cat(rc1->msg, rc2->msg->_text);
    }
 return rc1;
}

padico_rc_t padico_rc_add(padico_rc_t rc1 , padico_rc_t rc2)
{
  padico_rc_t rc = padico_rc_cat(rc1, rc2);
  if(rc2)
    padico_rc_delete(rc2);
  return rc;
}

void padico_rc_show(padico_rc_t rc)
{
  if(rc == NULL)
    {
      padico_print("ok/0\n");
    }
  else if(rc->rc)
    {
      padico_print("err/%s\n", padico_string_get(rc->msg));
    }
  else if(rc->msg)
    {
      padico_print("ok/%s\n", padico_string_get(rc->msg));
    }
  else
    {
      padico_print("err/incorrect code\n");
    }
}

char*padico_rc_gettext(padico_rc_t rc)
{
  if(rc == NULL)
    {
      return "";
    }
  else if(rc->rc)
    {
      return padico_string_get(rc->msg);
    }
  else if(rc->msg)
    {
      return padico_string_get(rc->msg);
    }
  else
    {
      padico_fatal("err/incorrect code\n");
    }
  return NULL;
}

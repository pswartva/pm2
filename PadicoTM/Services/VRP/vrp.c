/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Variable Reliability Protocol module- common parts
 * @ingroup VRP
 */

#include <stdio.h>

#ifdef PADICO

#include <Padico/Module.h>

static int vrp_init(void);
static void vrp_shutdown(void)

PADICO_MODULE_DECLARE(VRP, vrp_init, NULL, vrp_shutdown);

#define PADICO_VRP_DEFINED
FILE* vrp_log_fd = NULL;

#endif /* PADICO */

#include "VRP-internals.h"

/*
 * Only one thread is allowed to be in the vrp code (and thus
 * mucking with data structures) at a time.
 */

struct fd_finder_t fd_finder;
vrp_mutex_t vrp_mutex;

/*
 * vrp_init()
 *
 * Initialize the VRP protocol.
 */
static int vrp_init(void)
{
  int i;

  vrp_print("*** VRP init\n");
  vrp_print("  max_packet_size      = %d bytes\n",   VRP_MAX_PACKET_SIZE);
  vrp_print("  data_per_packet      = %d bytes\n",   VRP_DATA_PER_PACKET);
  vrp_print("  outgoing_window_size = %d bytes\n",   VRP_OUTGOING_WINDOW_SIZE_BYTES);
  vrp_print("  outgoing_window_size = %d packets\n", VRP_OUTGOING_WINDOW_SIZE);
  //  vrp_print("  outgoing_num_bufs    = %d packets\n", VRP_OUTGOING_NUM_BUFS);
  //  vrp_print("  incoming_window_size = %d packets\n", VRP_INCOMING_WINDOW_SIZE);
  vrp_mutex_init(vrp_mutex);
  /* Initialize the fd_table_* tables */
  fd_finder.fd_tablesize = 256; /* TODO */
  fd_finder.fd_table = (fd_table_t*)vrp_malloc(sizeof(fd_table_t) * fd_finder.fd_tablesize);
  for (i = 0; i < fd_finder.fd_tablesize; i++)
    {
      fd_finder.fd_table[i].type = FD_TABLE_TYPE_NONE;
      fd_finder.fd_table[i].value = NULL;
    }
  vrp_log_fd = vrp_log_init();
  vrp_log(vrp_log_fd, "#  max_packet_size      = %d bytes\n",   VRP_MAX_PACKET_SIZE);
  vrp_log(vrp_log_fd, "#  data_per_packet      = %d bytes\n",   VRP_DATA_PER_PACKET);
  vrp_log(vrp_log_fd, "#  outgoing_window_size = %d bytes\n",   VRP_OUTGOING_WINDOW_SIZE_BYTES);
  vrp_log(vrp_log_fd, "#  outgoing_window_size = %d packets\n", VRP_OUTGOING_WINDOW_SIZE);
  vrp_log(vrp_log_fd, "# TIMEOUTDATA: actual_RTT RTT (smoothed) timeout average_deviation\n");
  vrp_log(vrp_log_fd, "#\n");
  return 0;
} /* vrp_init() */


/*
 * vrp_shutdown()
 *
 * This routine is called during normal shutdown of a process.
 *
 * Shutdown the udp protocol module by closing all off of the
 * outgoing's that have open fd's.  This will result in an orderly
 * shutdown of fd's, so that other contexts will not detect an
 * abnormal eof on the other ends of these fd's.
 *
 * It is ok if this procedure takes a little while to complete -- for
 * example to wait for another process to read off its fd and
 * free up some socket buffer space.
 */
static void vrp_shutdown(void)
{
    int i;
    vrp_outgoing_t outgoing;
    vrp_incoming_t incoming;

    vrp_enter();

    vrp_print("*** vrp_shutdown()\n");

    vrp_show(1, ("vrp_shutdown(): inside\n"));

    for (i = 0; i < fd_finder.fd_tablesize; i++)
    {
        switch(fd_finder.fd_table[i].type)
        {
        case FD_TABLE_TYPE_OUTGOING:
          vrp_show(1, ("vrp_shutdown(): outgoing\n"));
          outgoing = (vrp_outgoing_t) fd_finder.fd_table[i].value;
          /*  if (outgoing->state != VRP_OUTGOING_CLOSED)
              {
              vrp_outgoing_close(outgoing);
              vrp_outgoing_free(outgoing);
              } */
          break;
        case FD_TABLE_TYPE_INCOMING:
          vrp_show(1, ("vrp_shutdown(): incoming\n"));
          incoming = (vrp_incoming_t) fd_finder.fd_table[i].value;
          vrp_incoming_close(incoming);
          break;
        default:
          /* Nothing registered for this fd */
          break;
        }
    }
    vrp_exit();
    vrp_log_end();
} /* vrp_shutdown() */

#! /bin/sh -x

nameserver=10.69.1.17
rdvhost=sagittaire-17.lyon.grid5000.fr

padico-launch -v -nodefault -DNS_BASIC_CONF=$PWD/../../NetSelector/lyon.xml -DPADICO_BOOTSTRAP_PORT=30201 -DPADICO_BOOTSTRAP_HOST=${rdvhost} -iNetSelector-preset-vanilla -iControler-preset-router ../bench-CORBA-client -ORBInitRef NameService=corbaloc:iiop:${nameserver}:2809/NameService




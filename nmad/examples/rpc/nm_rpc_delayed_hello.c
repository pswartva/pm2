/*
 * NewMadeleine
 * Copyright (C) 2016-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


/** @file RPC example with delayed body receive (not posted in handler)
 * using nm_rpc_token_delay()
 */

#include <stdlib.h>
#include <stdio.h>

#include <nm_launcher_interface.h>
#include <nm_coll_interface.h>
#include <nm_rpc_interface.h>

const char msg[] = "Hello, world!";
const nm_tag_t tag = 0x02;

static volatile int handler_called = 0;
static volatile int finalized = 0;
nm_rpc_service_t rpc_service;

struct rpc_delayed {
  nm_len_t rlen;
  nm_rpc_token_t token;
};

/** handler called upon incoming RPC request */
static void rpc_hello_handler(nm_rpc_token_t p_token)
{
  nm_len_t rlen = NM_LEN_UNDEFINED;
  nm_rpc_recv_header(p_token, &rlen, sizeof(rlen));
  printf("rpc_hello_handler()- received header: len = %lu\n", rlen);

  struct rpc_delayed* rpc_delay = nm_rpc_service_get_ref(rpc_service);
  rpc_delay->rlen = rlen;
  rpc_delay->token = p_token;
  /*char*buf = malloc(rlen);*/
  /*nm_rpc_token_set_ref(p_token, buf);*/
  nm_rpc_token_delay(p_token);
  /*nm_rpc_irecv_body(p_token, buf, rlen);*/
  handler_called = 1;
}

/** handler called after all data for the incoming RPC request was received */
static void rpc_hello_finalizer(nm_rpc_token_t p_token)
{
  printf("received body\n");
  finalized = 1;
}

int main(int argc, char**argv)
{
  /* generic nmad init */
  nm_launcher_init(&argc, argv);
  nm_session_t p_session = NULL;
  nm_session_open(&p_session, "nm_rpc_hello");
  int size = 0;
  nm_launcher_get_size(&size);
  int rank = -1;
  nm_launcher_get_rank(&rank);
  int peer = (size > 1) ? (1 - rank) : rank;
  nm_gate_t p_gate = NULL;
  nm_launcher_get_gate(peer, &p_gate);

  nm_comm_t p_comm = nm_comm_world("nm_rpc_hello");

  struct rpc_delayed* rpc_delay = malloc(sizeof(struct rpc_delayed));

  /* register the RPC service */
  rpc_service = nm_rpc_register(p_session, tag, NM_TAG_MASK_FULL,
                                               &rpc_hello_handler, &rpc_hello_finalizer, rpc_delay);

  nm_len_t len = strlen(msg) + 1; /* header */
  /* create rpc request */
  nm_rpc_req_t p_req = nm_rpc_req_init(rpc_service, p_gate, tag);
  nm_rpc_req_pack_header(p_req, &len, sizeof(len));
  nm_rpc_req_pack_body(p_req, msg, len);
  nm_rpc_req_isend(p_req);
  nm_rpc_req_wait(p_req);

  while(!handler_called)
    {
      nm_sr_progress(p_session);
    }

  puk_sleep(2);

  assert(rpc_delay->rlen > 0);
  char*buf = malloc(rpc_delay->rlen);
  nm_rpc_irecv_body(rpc_delay->token, buf, rpc_delay->rlen);


  nm_rpc_token_complete(rpc_delay->token);


  while(!finalized)
    {
      nm_sr_progress(p_session);
    }

  assert(memcmp(buf, msg, len) == 0);

  /* generic nmad termination */
  nm_coll_barrier(p_comm, 0xF2);
  free(rpc_delay);
  free(buf);
  nm_rpc_unregister(rpc_service);
  nm_comm_destroy(p_comm);
  nm_session_close(p_session);
  nm_launcher_exit();
  return 0;

}

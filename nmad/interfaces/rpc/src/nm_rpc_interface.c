/*
 * NewMadeleine
 * Copyright (C) 2016-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <stdint.h>

#include <Padico/Puk.h>
#include <nm_private.h>
#include <nm_sendrecv_interface.h>
#include <nm_rpc_interface.h>
#include <nm_core_interface.h>

PADICO_MODULE_HOOK(nmad);


/* ********************************************************* */

NM_ALLOCATOR_TYPE(nm_rpc_req, struct nm_rpc_req_s);

static nm_rpc_req_allocator_t nm_rpc_req_allocator = NULL;

static inline nm_rpc_req_t nm_rpc_req_new(void)
{
  nm_rpc_req_t p_rpc_req = nm_rpc_req_malloc(nm_rpc_req_allocator);
  return p_rpc_req;
}

void nm_rpc_req_delete(nm_rpc_req_t p_rpc_req)
{
  nm_rpc_req_free(nm_rpc_req_allocator, p_rpc_req);
}


NM_ALLOCATOR_TYPE(nm_rpc_token, struct nm_rpc_token_s);

static nm_rpc_token_allocator_t nm_rpc_token_allocator = NULL;

static inline void nm_rpc_token_destroy(struct nm_rpc_token_s*p_token)
{
  nm_spin_lock(&p_token->p_service->token_list_lock);
  nm_rpc_token_list_remove(&p_token->p_service->token_list, p_token);
  nm_spin_unlock(&p_token->p_service->token_list_lock);
  nm_cond_destroy(&p_token->done);
  nm_rpc_token_free(nm_rpc_token_allocator, p_token);
}

/* ********************************************************* */

static void nm_rpc_init(void)
{
  assert(nm_rpc_req_allocator == NULL);
  assert(nm_rpc_token_allocator == NULL);
  nm_rpc_req_allocator = nm_rpc_req_allocator_new(8);
  nm_rpc_token_allocator = nm_rpc_token_allocator_new(8);
}

static void nm_rpc_exit(void)
{
  if(nm_rpc_token_allocator != NULL)
    nm_rpc_token_allocator_delete(nm_rpc_token_allocator);
  if(nm_rpc_req_allocator != NULL)
    nm_rpc_req_allocator_delete(nm_rpc_req_allocator);
}

NM_LAZY_INITIALIZER(nm_rpc, &nm_rpc_init, &nm_rpc_exit);

/* ********************************************************* */

static void nm_rpc_handler(nm_sr_event_t event, const nm_sr_event_info_t*p_info, void*_ref);


/* ********************************************************* */

/** data content for an RPC request (header + body) */
struct nm_rpc_req_data_content_s
{
  struct nm_datav_s*p_headerv;
  struct nm_datav_s*p_bodyv;
};

static void nm_rpc_req_data_traversal(const void*_content, nm_data_apply_t p_apply, void*_context);
const struct nm_data_ops_s nm_rpc_req_data_ops =
  {
    .p_traversal = &nm_rpc_req_data_traversal
  };
NM_DATA_TYPE(rpc_req, struct nm_rpc_req_data_content_s, &nm_rpc_req_data_ops);

static void nm_rpc_req_data_traversal(const void*_content, nm_data_apply_t p_apply, void*_context)
{
  const struct nm_rpc_req_data_content_s*p_content = _content;
  int i;
  for(i = 0; i < p_content->p_headerv->n_data; i++)
    {
      nm_data_traversal_apply(&p_content->p_headerv->p_data[i], p_apply, _context);
    }
  for(i = 0; i < p_content->p_bodyv->n_data; i++)
    {
      nm_data_traversal_apply(&p_content->p_bodyv->p_data[i], p_apply, _context);
    }
}

/** @internal data constructor for the compound rpc data type (header + body) */
static void nm_rpc_req_data_build(struct nm_data_s*p_rpc_req_data, struct nm_datav_s*p_headerv, struct nm_datav_s*p_bodyv)
{
  nm_data_rpc_req_set(p_rpc_req_data, (struct nm_rpc_req_data_content_s)
                      {
                        .p_headerv = p_headerv,
                        .p_bodyv   = p_bodyv
                      });
}

/** data content for an RPC body (server-side).
 * On server side, headers are received using peek(), thus
 * the real receive for body discards headers (offset on receive
 * request, placeholder in data type).
 */
struct nm_rpc_body_data_content_s
{
  nm_len_t header_len;
  struct nm_datav_s*p_bodyv;
};
static void nm_rpc_body_data_traversal(const void*_content, nm_data_apply_t p_apply, void*_context);
const struct nm_data_ops_s nm_rpc_body_data_ops =
  {
    .p_traversal = &nm_rpc_body_data_traversal
  };
NM_DATA_TYPE(rpc_body, struct nm_rpc_body_data_content_s, &nm_rpc_body_data_ops);

static void nm_rpc_body_data_traversal(const void*_content, nm_data_apply_t p_apply, void*_context)
{
  const struct nm_rpc_body_data_content_s*p_content = _content;
  (*p_apply)(NULL, p_content->header_len, _context); /* place-holder for headers */
  int i;
  for(i = 0; i < p_content->p_bodyv->n_data; i++)
    {
      nm_data_traversal_apply(&p_content->p_bodyv->p_data[i], p_apply, _context);
    }
}

static void nm_rpc_body_data_build(struct nm_data_s*p_rpc_body, nm_len_t header_len, struct nm_datav_s*p_bodyv)
{
  nm_data_rpc_body_set(p_rpc_body, (struct nm_rpc_body_data_content_s)
                       {
                         .header_len = header_len,
                         .p_bodyv = p_bodyv
                       });
}

/* ********************************************************* */

nm_rpc_req_t nm_rpc_req_init(nm_rpc_service_t p_service,  nm_gate_t p_gate, nm_tag_t tag)
{
  nm_session_t p_session = p_service->p_session;
  nm_rpc_req_t p_req = nm_rpc_req_new();
  p_req->extra_hlen = 0;
  p_req->p_service = p_service;
  p_req->priority = 0;
  nm_datav_init(&p_req->headerv);
  nm_datav_init(&p_req->bodyv);
  nm_sr_send_init(p_session, &p_req->request);
  nm_sr_send_dest(p_session, &p_req->request, p_gate, tag);
  return p_req;
}

void nm_rpc_req_pack_header(nm_rpc_req_t p_rpc_req, const void*ptr, nm_len_t len)
{
  assert(nm_datav_size(&p_rpc_req->bodyv) == 0); /* all headers must be packed before body chunks */
  nm_datav_add_chunk(&p_rpc_req->headerv, ptr, len);
}

void nm_rpc_req_pack_body_data(nm_rpc_req_t p_rpc_req, const struct nm_data_s*p_data)
{
  nm_datav_add_chunk_data(&p_rpc_req->bodyv, p_data);
}

void nm_rpc_req_pack_body(nm_rpc_req_t p_rpc_req, const void*ptr, nm_len_t len)
{
  nm_datav_add_chunk(&p_rpc_req->bodyv, ptr, len);
}

void nm_rpc_req_set_hlen(nm_rpc_req_t p_rpc_req, nm_len_t hlen)
{
  p_rpc_req->extra_hlen = hlen;
}

void nm_rpc_req_isend(nm_rpc_req_t p_req)
{
  struct nm_data_s rpc_req_data; /* safe as temp var; will be copied by nm_sr_send_pack_data() */
  nm_rpc_req_data_build(&rpc_req_data, &p_req->headerv, &p_req->bodyv);
  nm_sr_send_pack_data(p_req->p_service->p_session, &p_req->request, &rpc_req_data);
  const nm_len_t header_size = nm_datav_size(&p_req->headerv) + p_req->extra_hlen;
  nm_sr_send_set_priority(p_req->p_service->p_session, &p_req->request, p_req->priority);
  nm_sr_send_header(p_req->p_service->p_session, &p_req->request, header_size);
  nm_sr_send_submit(p_req->p_service->p_session, &p_req->request);
}

nm_rpc_req_t nm_rpc_isend(nm_rpc_service_t p_service,  nm_gate_t p_gate, nm_tag_t tag,
                          void*hptr, nm_len_t hlen, struct nm_data_s*p_body)
{
  nm_rpc_req_t p_req = nm_rpc_req_init(p_service, p_gate, tag);
  nm_rpc_req_pack_header(p_req, hptr, hlen);
  nm_rpc_req_pack_body_data(p_req, p_body);
  nm_rpc_req_isend(p_req);
  return p_req;
}

static void nm_rpc_req_notifier(nm_sr_event_t event, const nm_sr_event_info_t*p_info, void*_ref)
{
  nm_rpc_req_t p_req = _ref;
  (*p_req->p_notifier)(p_req, p_req->p_notifier_ref);
  nm_rpc_req_delete(p_req);
}

void nm_rpc_req_set_notifier(nm_rpc_req_t p_req, nm_rpc_req_notifier_t p_notifier, void*ref)
{
  p_req->p_notifier = p_notifier;
  p_req->p_notifier_ref = ref;
  nm_sr_request_set_ref(&p_req->request, p_req);
  nm_sr_request_monitor(p_req->request.p_session, &p_req->request, NM_SR_EVENT_FINALIZED, &nm_rpc_req_notifier);
}

static inline void nm_rpc_req_reload(nm_rpc_service_t p_service)
{
  struct nm_rpc_token_s* p_token = nm_rpc_token_malloc(nm_rpc_token_allocator);
  p_token->p_service = p_service;
  nm_rpc_token_list_cell_init(p_token);
  p_token->delayed = 0;
  nm_cond_init(&p_token->done, 0);
  nm_sr_recv_init(p_service->p_session, &p_token->request);
  nm_sr_request_set_ref(&p_token->request, p_token);
  nm_sr_request_monitor(p_service->p_session, &p_token->request,
      NM_SR_EVENT_FINALIZED | NM_SR_EVENT_RECV_DATA | NM_SR_EVENT_RECV_CANCELLED, &nm_rpc_handler);
  nm_spin_lock(&p_service->token_list_lock);
  nm_rpc_token_list_push_front(&p_service->token_list, p_token);
  nm_spin_unlock(&p_service->token_list_lock);
  nm_sr_recv_irecv(p_service->p_session, &p_token->request, NM_ANY_GATE, p_service->tag, p_service->tag_mask);
}

void nm_rpc_token_complete(struct nm_rpc_token_s*p_token)
{
  const nm_len_t header_len = nm_datav_size(&p_token->headerv);
  struct nm_data_s rpc_body_data;
  NM_TRACEF("p_token = %p; request = %p\n", p_token, &p_token->request);
  nm_rpc_body_data_build(&rpc_body_data, header_len, &p_token->bodyv);
  nm_sr_recv_unpack_data(p_token->p_service->p_session, &p_token->request, &rpc_body_data);
}

static void nm_rpc_handler(nm_sr_event_t event, const nm_sr_event_info_t*p_info, void*_ref)
{
  struct nm_rpc_token_s*p_token = _ref;
  nm_rpc_service_t p_service = p_token->p_service;
  assert(! ((event & NM_SR_EVENT_FINALIZED) &&
            (event & NM_SR_EVENT_RECV_DATA)));
  if(!(event & NM_SR_EVENT_RECV_CANCELLED))
    {
      if(event & NM_SR_EVENT_FINALIZED)
        {
          NM_TRACEF("event FINALIZED; p_token = %p; request = %p\n", p_token, &p_token->request);
          if(p_service->p_finalizer != NULL)
            {
              (*p_service->p_finalizer)(p_token);
            }
          if(p_service->running)
            {
              nm_rpc_token_destroy(p_token);
            }
          else
            {
              nm_cond_signal(&p_token->done, 1);
            }
        }
      if(event & NM_SR_EVENT_RECV_DATA)
        {
          NM_TRACEF("event RECV_DATA; p_token = %p; request = %p\n", p_token, &p_token->request);
          assert(!nm_sr_request_test(p_info->req.p_request, NM_STATUS_FINALIZED));
          assert(p_token->delayed == 0);
          nm_datav_init(&p_token->headerv);
          nm_datav_init(&p_token->bodyv);
          p_token->ref = NULL;
          (*p_service->p_handler)(p_token);
          if(!p_token->delayed)
            {
              nm_rpc_token_complete(p_token);
            }
          if(p_service->running)
            {
              nm_rpc_req_reload(p_service);
            }
        }
    }
  else if(p_service->running)
    {
      nm_rpc_token_destroy(p_token);
    }
  else // request cancelled while unregistering service
    {
      nm_cond_signal(&p_token->done, 1);
    }
}

nm_rpc_service_t nm_rpc_register(nm_session_t p_session, nm_tag_t tag, nm_tag_t tag_mask,
                                 nm_rpc_handler_t p_handler, nm_rpc_finalizer_t p_finalizer,
                                 void*ref)
{
  struct nm_rpc_service_s*p_service = padico_malloc(sizeof(struct nm_rpc_service_s));
  p_service->p_session   = p_session;
  p_service->p_handler   = p_handler;
  p_service->p_finalizer = p_finalizer;
  p_service->ref         = ref;
  p_service->tag         = tag;
  p_service->tag_mask    = tag_mask;
  p_service->running     = 1;
  nm_rpc_token_list_init(&p_service->token_list);
  nm_spin_init(&p_service->token_list_lock);
  nm_rpc_lazy_init();
  nm_rpc_req_reload(p_service);
  return p_service;
}

void nm_rpc_unregister(nm_rpc_service_t p_service)
{
  nm_sr_flush(p_service->p_session);
  nm_atomic_dec(&p_service->running); /* disallow recv reload */

  struct nm_rpc_token_s*p_token_itor = NULL;
  struct nm_rpc_token_s*p_token_itor2 = NULL;
  int rc;
  nm_spin_lock(&p_service->token_list_lock);
  puk_list_foreach_safe(nm_rpc_token, p_token_itor, p_token_itor2, &p_service->token_list)
    {
      rc = nm_sr_rcancel(p_service->p_session, &p_token_itor->request);
      if(rc != NM_ESUCCESS)
        {
          /* concurrent handler detected; wait for completion */
          if(p_token_itor->delayed)
            {
              NM_WARN("waiting on token = %p in delayed state; nm_rpc_unregister() is likely to deadlock.\n", p_token_itor);
            }
          nm_cond_wait(&p_token_itor->done, 1, nm_core_get_singleton());
          nm_rpc_token_list_remove(&p_service->token_list, p_token_itor);
          nm_rpc_token_free(nm_rpc_token_allocator, p_token_itor);
        }
    }
  nm_rpc_token_list_destroy(&p_service->token_list);
  nm_spin_unlock(&p_service->token_list_lock);
  nm_spin_destroy(&p_service->token_list_lock);
  padico_free(p_service);
}

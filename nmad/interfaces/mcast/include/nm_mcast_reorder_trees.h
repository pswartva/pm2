/*
 * NewMadeleine
 * Copyright (C) 2021-2022 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


/* Routines to reorder nodes in a tree.
 *
 * These functions reorder nodes in array according to an array of their
 * respective priority.
 *
 * One major constraint is that nodes in a subtree are contiguous in the array
 * (to easily send these subtrees through the network: it requires only a
 * pointer and a size, without extra copy).
 */

#ifndef NM_MCAST_REORDER_TREE_H
#define NM_MCAST_REORDER_TREE_H

/** reverse comparison function for (prio; dest) pairs
 * (reverse to get descending order)
 */
static int nm_mcast_pairs_compare(const void*_p1, const void*_p2)
{
  const int*p_pair1 = _p1;
  const int*p_pair2 = _p2;
  if(p_pair1[0] < p_pair2[0])
    return 1;
  else if(p_pair1[0] > p_pair2[0])
    return -1;
  else return 0;
}

/** sort p_dests & p_prios by prio descending order */
static inline void nm_mcast_generic_reorder_nodes(const int*p_dests, const int*p_prios,
                                                  int*p_ordered_dests, int*p_ordered_prios, int n)
{
  int*p_pairs = padico_malloc(sizeof(int) * n * 2);
  int i;
  for(i = 0; i < n; i++)
    {
      p_pairs[2 * i] = p_prios[i];
      p_pairs[2 * i + 1] = p_dests[i];
    }
  qsort(p_pairs, n, sizeof(int) * 2, &nm_mcast_pairs_compare);
  for(i = 0; i < n; i++)
    {
      p_ordered_prios[i] = p_pairs[2 * i];
      p_ordered_dests[i] = p_pairs[2 * i + 1];
    }
  padico_free(p_pairs);
}

#endif /* NM_MCAST_REORDER_TREE_H */

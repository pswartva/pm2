/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file SocketFactory for socket over SSH tunnel
 */

#include <Padico/PadicoTM.h>
#include <Padico/VLink-internals.h>
#include <Padico/Module.h>
#include <Padico/SocketFactory.h>

static int sfstun_module_init(void);
static void sfstun_module_finalize(void);

PADICO_MODULE_DECLARE(SocketFactory_SSH_Tunnel, sfstun_module_init, NULL, sfstun_module_finalize,
                      "SysIO", "Topology", "PadicoControl");


/** @defgroup SF_stun component: SocketFactory-stun- socket factory for SSH tunnels
 * @ingroup PadicoComponent
 */

/* ********************************************************* */

static int sfstun_create(void*_instance, int family, int type, int protocol);
static int sfstun_connect(void*_instance, const struct sockaddr*addr, socklen_t addrlen, padico_socketfactory_connector_t notifier, void*key);
static int sfstun_close(void*_instance);

/** 'SocketFactory' facet for SocketFactory-stun
 * @ingroup SF_stun
 */
static const struct padico_socketfactory_s sfstun_sf_driver =
  {
    .create  = &sfstun_create,
    .bind    = NULL,
    .listen  = NULL,
    .connect = &sfstun_connect,
    .close   = &sfstun_close,
    .caps    = padico_vsock_caps_directconnect | padico_vsock_caps_clientonly
  };

static void*sfstun_instantiate(puk_instance_t, puk_context_t);
static void sfstun_destroy(void*);

/** instanciation facet for SocketFactory-stun
 * @ingroup SF_stun
 */
static const struct puk_component_driver_s sfstun_component_driver =
  {
    .instantiate = &sfstun_instantiate,
    .destroy     = &sfstun_destroy
  };

/** instance of SocketFactory-stun
 * @ingroup SF_stun
 */
struct sfstun_instance_s
{
  int fd;
  padico_io_t io_connect;
  padico_socketfactory_connector_t connector;
  void*connector_key;
  pid_t pid;
  const char*ssh_path;
  const char*ssh_host;
  int ssh_port;
  const char*ssh_cipher;
};

static struct
{
  int port_offset;
  padico_tm_bgthread_pool_t pool;
} sfstun;

static int sfstun_callback_connector(int fd, void*key);

/* ********************************************************* */

static int sfstun_module_init(void)
{
  sfstun.port_offset = 0;
  sfstun.pool = padico_tm_bgthread_pool_create("SocketFactory_SSH_Tunnel");
  puk_component_declare("SocketFactory_SSH_Tunnel",
                        puk_component_provides("PadicoComponent", "component", &sfstun_component_driver),
                        puk_component_provides("SocketFactory", "sf",&sfstun_sf_driver),
                        puk_component_attr("ssh_host", NULL),
                        puk_component_attr("ssh_port", NULL),
                        puk_component_attr("ssh_path", "/usr/bin/ssh"),
                        puk_component_attr("ssh_cipher", "blowfish"));
  return 0;
}

static void sfstun_module_finalize(void)
{
  padico_tm_bgthread_pool_wait(sfstun.pool);
}

/* ********************************************************* */
/* *** 'PadicoComponent' for SocketFactory_SSH_Tunnel */

static void*sfstun_instantiate(puk_instance_t assembly, puk_context_t context)
{
  struct sfstun_instance_s*instance = padico_malloc(sizeof(struct sfstun_instance_s));
  instance->fd = -1;
  instance->pid = -1;
  instance->io_connect = NULL;
  instance->connector  = NULL;
  instance->ssh_path   = puk_context_getattr(context, "ssh_path");
  instance->ssh_cipher = puk_context_getattr(context, "ssh_cipher");
  instance->ssh_host   = puk_context_getattr(context, "ssh_host");
  const char*ssh_port  = puk_context_getattr(context, "ssh_port");
  instance->ssh_port   = atoi(ssh_port) + sfstun.port_offset++;
  padico_out(40, "path=%s; host=%s; port=%d; cipher=%s- done.\n",
             instance->ssh_path, instance->ssh_host, instance->ssh_port, instance->ssh_cipher);
  return instance;
}

static void sfstun_destroy(void*_instance)
{
  struct sfstun_instance_s*instance = _instance;

  sfstun_close(_instance);
  if(instance->pid != -1)
    {
      int rc = -1, err = -1;
      puk_spinlock_acquire();
      rc = kill(instance->pid, SIGTERM);
      err = puk_abi_real_errno();
      puk_spinlock_release();
      instance->pid = -1;
      if(rc)
        {
          padico_warning("kill()- errno=%d (%s).\n", err, strerror(err));
        }
    }
  padico_free(instance);
  padico_out(40, "done.\n");
}


/* ********************************************************* */
/* *** 'SocketFactory' for SocketFactory_SSH_Tunnel */

static int sfstun_create(void*_instance, int family, int type, int protocol)
{
  struct sfstun_instance_s*instance = _instance;
  int fd  = padico_sysio_socket(family, type, protocol);
  int err = padico_sysio_errno();
  assert(family == AF_INET);
  assert(type == SOCK_STREAM);
  padico_out(10, "family=%d\n", family);
  if(fd > -1)
    {
      err = 0;
      instance->fd = fd;
      instance->io_connect = padico_io_register(fd, PADICO_IO_EVENT_WRITE, &sfstun_callback_connector, instance);
    }
  padico_out(40, "fd=%d; err=%d\n", fd, err);
  return -err;
}

static void sfstun_build_tunnel(struct sfstun_instance_s*instance, struct sockaddr_in*inaddr)
{
  pid_t pid = -1;
  int rc = -1, err = -1;
  if(instance->pid == -1)
    {
      puk_spinlock_acquire();
      pid = fork();
      err = puk_abi_real_errno();
      puk_spinlock_release();
      if(pid == -1)
        {
          padico_warning("fork() failed!\n");
        }
      else if(pid == 0)
        {
          /* build ssh tunnel string */
          const char*ssh_tunnel_host = padico_strdup(inet_ntoa(inaddr->sin_addr));
          const int ssh_tunnel_rport = ntohs(inaddr->sin_port);
          padico_string_t s_ssh_tunnel = padico_string_new();
          padico_string_printf(s_ssh_tunnel, "%d:%s:%d", instance->ssh_port, ssh_tunnel_host, ssh_tunnel_rport);
          const char*ssh_tunnel = padico_string_get(s_ssh_tunnel);
          const char*argv[] =
            {
              instance->ssh_path,         /* path to ssh- default: /usr/bin/ssh */
              "-x",                       /* no X11 */
              "-n",                       /* no stdin */
              "-T",                       /* no pseudo-tty */
              "-N",                       /* no remote command */
              "-e", "none",               /* disable escape sequences */
              "-L", ssh_tunnel,           /* the tunnel itself */
              "-c", instance->ssh_cipher, /* cipher- default: blowfish (much faster than 3des) */
              instance->ssh_host,         /* our gateway host */
              NULL                        /* NULL-terminated array */
            };

          padico_print("ssh_path=%s; ssh_tunnel=%s; ssh_host=%s; cipher=%s\n",
                       instance->ssh_path, ssh_tunnel, instance->ssh_host, instance->ssh_cipher);
          putenv("LD_LIBRARY_PATH=");
          puk_spinlock_acquire();
          puk_abi_real_errno() = 0;
          rc = PUK_ABI_PROC_REAL(execv)(instance->ssh_path, (char**)argv);
          err = puk_abi_real_errno();
          puk_spinlock_release();
          padico_fatal("exec() failed for ssh- rc=%d; errno=%d (%s).\n", rc, err, strerror(err));
        }
      else
        {
          /* father: PadicoTM process */
          instance->pid = pid;
        }
    }
}

static int sfstun_connect(void*_instance, const struct sockaddr*addr, socklen_t addrlen,
                          padico_socketfactory_connector_t notifier, void*key)
{
  int rc = -1, err = -1;
  struct sfstun_instance_s*instance = _instance;
  struct sockaddr_in inaddr;
  int inaddrlen = sizeof(inaddr);
  int nodelay = 1;
  int retry_count = 10;
  int retry_delay = 50;
  const int max_delay = 1000;
  memcpy(&inaddr, addr, sizeof(struct sockaddr_in));
  inaddr.sin_port = htons(instance->ssh_port);
  inet_aton("127.0.0.1", &inaddr.sin_addr);

  sfstun_build_tunnel(instance, (struct sockaddr_in*)addr);
  rc  = PUK_ABI_FSYS_WRAP(setsockopt)(instance->fd, SOL_TCP, TCP_NODELAY, &nodelay, sizeof(nodelay));
  err = __puk_abi_wrap__errno;
  if(rc)
    goto exit;
 retry:
  rc  = PUK_ABI_FSYS_WRAP(connect)(instance->fd, &inaddr, inaddrlen);
  err = __puk_abi_wrap__errno;
  padico_out(50, "::connect() rc=%d errno=%d (%s)\n", rc, err, strerror(err));
  if(rc == 0 || (rc == -1 && err == EINPROGRESS))
    {
      err = EINPROGRESS;
      instance->connector     = notifier;
      instance->connector_key = key;
      padico_io_activate(instance->io_connect);
      padico_out(60, "callback_connector() registered\n");
    }
  else if(rc == -1 && (err == ECONNREFUSED || err == EINTR))
    {
      int status = -1;
      rc = waitpid(instance->pid, &status, WNOHANG);
      if(rc == instance->pid)
        {
          padico_warning("ssh error- status=%d.\n", status);
          instance->pid = -1;
          err = ENETUNREACH; /** @todo check what is the best errno when ssh fails. */
          goto exit;
        }
      else
        {
          if(retry_delay >= max_delay)
            padico_warning("connect()- tunnel not ready (%s)- retrying...\n", strerror(err));
          padico_tm_msleep(retry_delay);
          retry_delay = (retry_delay > max_delay) ? max_delay : (retry_delay * 2);
          retry_count++;
          if(retry_count > 0)
            goto retry;
        }
    }
  else
    {
      padico_warning("connect() error while connecting to ssh- errno=%d (%s)\n", err, strerror(err));
    }
  padico_out(40, "err=%d\n", err);
 exit:
  return -err;
}

static int sfstun_close(void*_instance)
{
  struct sfstun_instance_s*instance = _instance;
  int rc = -1;
  if(instance->io_connect)
    {
      padico_io_deactivate(instance->io_connect);
      padico_io_unregister(instance->io_connect);
      instance->io_connect = NULL;
    }
  if(instance->fd != -1)
    {
      rc = PUK_ABI_FSYS_WRAP(close)(instance->fd);
      int err __attribute__((unused)) = __puk_abi_wrap__errno;
      padico_out(50, "::close() rc=%d errno=%d (%s)\n", rc, err, strerror(err));
      instance->fd = -1;
    }
  /* the ssh tunnel will be killed by destroy() */
  return rc;
}

/* ********************************************************* */

static void*sfstun_connector_finalizer(void*_instance)
{
  struct sfstun_instance_s*instance = _instance;
  (*instance->connector)(instance->connector_key, instance->fd);
  return NULL;
}

static int sfstun_callback_connector(int fd, void*key)
{
  int again = 0;
  struct sfstun_instance_s*instance = key;
  padico_out(40, "fd=%d\n", fd);
  assert(fd == instance->fd);
  padico_tm_bgthread_start(sfstun.pool, &sfstun_connector_finalizer, instance,
                           "SocketFactory_SSH_Tunnel:sfstun_connector_finalizer");
  return again;
}

#include <stdio.h>
#include <strings.h>
#include <string.h>
#include <netinet/in.h>
#include <netdb.h>
#include <time.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <poll.h>
#include <pthread.h>
#include <time.h>
#include "helper.h"

#define MSGLEN 512

void print_pollfd(struct pollfd* fds, int nfds)
{
  int i;
  for(i=0; i<nfds; i++)
    {
      printf("n:%d; POLLIN:%d; POLLPRI:%d; POLLOUT:%d; POLLERR:%d; POLLHUP:%d; POLLNVAL:%d\n",
             i, (fds[i].revents & POLLIN) != 0,
             (fds[i].revents & POLLPRI) != 0,
             (fds[i].revents & POLLOUT) != 0,
             (fds[i].revents & POLLERR) != 0,
             (fds[i].revents & POLLHUP) != 0,
             (fds[i].revents & POLLNVAL) != 0);
    }
  printf("=========================================================================\n");
}

int main(int argc, char *argv[])
{
  usage(argc, argv);
  int i;
  int port = atoi(argv[3]);
  int sock_fd = create_socket();
  char *msg[MSGLEN];

  if(strncmp("server",argv[1],6)==0)
    {
      bind_and_listen(sock_fd, port);
      struct sockaddr_in cli_addr;
      int clilen = sizeof(cli_addr);

      struct pollfd pollfdtab[4];
      pollfdtab[0].fd = sock_fd;
      pollfdtab[0].events = POLLIN|POLLPRI;
      for(i=1; i<4; i++)
        {
          if(poll(pollfdtab, 1, -1) < 0)
            print_error("poll()");
          print_pollfd(pollfdtab, 1);
          if(pollfdtab[0].revents & POLLIN)
            {
              int clifd = accept(sock_fd,  (struct sockaddr *) &cli_addr,&clilen);
              if (clifd < 0 )
                print_error("accept()");
              pollfdtab[i].fd = clifd;
            }
        }
      if(poll(pollfdtab, 1, 200) < 0)
            print_error("poll()");
      print_pollfd(pollfdtab, 1);

      pollfdtab[1].events = POLLIN;
      pollfdtab[2].events = POLLIN;
      pollfdtab[3].events = POLLIN;
      if(poll(pollfdtab, 4, -1) < 0)
        print_error("poll()");
      print_pollfd(pollfdtab, 4);

      for(i=1; i<4; i++)
        {
          if(pollfdtab[i].revents & POLLIN)
            if(read(pollfdtab[i].fd, msg, MSGLEN) < 0)
              print_error("read()");
        }

      pollfdtab[1].events = POLLIN|POLLOUT;
      pollfdtab[2].events = POLLIN|POLLOUT;
      pollfdtab[3].events = POLLIN|POLLOUT;

      if(poll(pollfdtab, 4, -1) < 0)
        print_error("poll()");
      print_pollfd(pollfdtab, 4);

      for(i=1; i<4; i++)
        close2(pollfdtab[i].fd);
    }
  else if(strncmp("client",argv[1],6) == 0)
    {

      int sock_fd1 = create_socket();
      int sock_fd2 = create_socket();
      connect2(sock_fd, port, argv[2]);
      connect2(sock_fd1, port, argv[2]);
      connect2(sock_fd2, port, argv[2]);

      sleep(1);
      if(send(sock_fd, "a\0", 2, 0) < 0)
        print_error("send()");
      sleep(1);

      close2(sock_fd1);
      close2(sock_fd2);

    }
  close2(sock_fd);

  return 0;
}

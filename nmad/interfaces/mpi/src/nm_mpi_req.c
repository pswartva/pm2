/*
 * NewMadeleine
 * Copyright (C) 2014-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include "nm_mpi_private.h"
#include "nm_mpi_private_syms.h"
#include <assert.h>
#include <errno.h>

#include <Padico/Module.h>
PADICO_MODULE_HOOK(MadMPI);

NM_MPI_HANDLE_TYPE(request, nm_mpi_request_t, _NM_MPI_REQUEST_OFFSET, 256);

static struct
{
  int refcount;
  struct nm_mpi_handle_request_s requests;
} nm_mpi_req = { .refcount = 0, .requests = NM_MPI_HANDLE_NULL };

/* ********************************************************* */

NM_MPI_ALIAS(MPI_Request_free,          mpi_request_free);
NM_MPI_ALIAS(MPI_Wait,                  mpi_wait);
NM_MPI_ALIAS(MPI_Waitall,               mpi_waitall);
NM_MPI_ALIAS(MPI_Waitany,               mpi_waitany);
NM_MPI_ALIAS(MPI_Waitsome,              mpi_waitsome);
NM_MPI_ALIAS(MPI_Test,                  mpi_test);
NM_MPI_ALIAS(MPI_Testall,               mpi_testall);
NM_MPI_ALIAS(MPI_Testany,               mpi_testany);
NM_MPI_ALIAS(MPI_Testsome,              mpi_testsome);
NM_MPI_ALIAS(MPI_Test_cancelled,        mpi_test_cancelled);
NM_MPI_ALIAS(MPI_Cancel,                mpi_cancel);
NM_MPI_ALIAS(MPI_Start,                 mpi_start);
NM_MPI_ALIAS(MPI_Startall,              mpi_startall);
NM_MPI_ALIAS(MPI_Grequest_start,        mpi_grequest_start);
NM_MPI_ALIAS(MPI_Grequest_complete,     mpi_grequest_complete);
NM_MPI_ALIAS(MPI_Status_set_elements,   mpi_status_set_elements);
NM_MPI_ALIAS(MPI_Status_set_elements_x, mpi_status_set_elements_x);
NM_MPI_ALIAS(MPI_Status_set_elements_c, mpi_status_set_elements_c);
NM_MPI_ALIAS(MPI_Status_set_cancelled,  mpi_status_set_cancelled);
NM_MPI_ALIAS(MPI_Get_count,             mpi_get_count);
NM_MPI_ALIAS(MPI_Get_count_c,           mpi_get_count_c);
NM_MPI_ALIAS(MPI_Get_elements,          mpi_get_elements);
NM_MPI_ALIAS(MPI_Get_elements_c,        mpi_get_elements_c);
NM_MPI_ALIAS(MPI_Get_elements_x,        mpi_get_elements_x);

/* ********************************************************* */

static char*nm_mpi_request_display(nm_mpi_request_t*p_req)
{
  padico_string_t s = padico_string_new();
  padico_string_catf(s, " type = %x (", p_req->request_type);
  if(p_req->request_type & NM_MPI_REQUEST_RECV)
    {
      padico_string_catf(s, "recv");
    }
  if(p_req->request_type & NM_MPI_REQUEST_SEND)
    {
      padico_string_catf(s, "send");
    }
  if(p_req->request_type & NM_MPI_REQUEST_PROBE)
    {
      padico_string_catf(s, "probe");
    }
  if(p_req->request_type & NM_MPI_REQUEST_FILE)
    {
      padico_string_catf(s, "file");
    }
  if(p_req->request_type & NM_MPI_REQUEST_GREQUEST)
    {
      padico_string_catf(s, "grequest");
    }
  if(p_req->request_type & NM_MPI_REQUEST_COLLECTIVE)
    {
      padico_string_catf(s, "collective");
    }
  if(p_req->request_type & NM_MPI_REQUEST_PARTITIONED)
    {
      padico_string_catf(s, "partitioned");
    }
  padico_string_catf(s, ");");
  if(p_req->status & NM_MPI_REQUEST_CANCELLED)
    padico_string_catf(s, " status = cancelled;");
  if(p_req->status & NM_MPI_REQUEST_PERSISTENT)
    padico_string_catf(s, " status = persistent;");
  if(p_req->request_type & NM_MPI_REQUEST_RECV)
    {
      nm_len_t len;
      nm_sr_request_get_expected_size(&p_req->request_nmad, &len);
      padico_string_catf(s, " expected size = %lld;", len);
    }
  if(p_req->request_type & NM_MPI_REQUEST_SEND)
    {
      nm_len_t len;
      nm_sr_request_get_size(&p_req->request_nmad, &len);
      padico_string_catf(s, " size = %lld;", len);
    }
  padico_string_catf(s, " tag = %d;", p_req->user_tag);
  padico_string_catf(s, " count = %lld;", p_req->count);
  padico_string_catf(s, " communicator = %d", p_req->p_comm->id);
  padico_string_catf(s, " datatype = %d", p_req->p_datatype->id);
  char*r = padico_strdup(padico_string_get(s));
  padico_string_delete(s);
  return r;
}

__PUK_SYM_INTERNAL
void nm_mpi_request_lazy_init(void)
{
  const int count = nm_atomic_inc(&nm_mpi_req.refcount);
  if(count == 0)
    {
      nm_mpi_handle_request_init(&nm_mpi_req.requests);
      nm_mpi_handle_request_set_display(&nm_mpi_req.requests, &nm_mpi_request_display);
    }
}

__PUK_SYM_INTERNAL
void nm_mpi_request_lazy_exit(void)
{
  const int count = nm_atomic_dec(&nm_mpi_req.refcount);
  if(count == 0)
    {
      nm_mpi_handle_request_finalize(&nm_mpi_req.requests, &nm_mpi_request_free);
    }
}

/* ********************************************************* */

__PUK_SYM_INTERNAL
nm_mpi_request_t*nm_mpi_request_alloc(void)
{
  nm_mpi_request_lazy_init();
  nm_mpi_request_t*p_req = nm_mpi_handle_request_alloc(&nm_mpi_req.requests);
  nm_mpi_request_list_cell_init(p_req);
  p_req->status = NM_MPI_STATUS_NONE;
  p_req->p_datatype = NULL;
  p_req->p_datatype2 = NULL;
  p_req->p_comm = NULL;
  p_req->gate = NM_GATE_NONE;
#ifdef NMAD_PROFILE
  __sync_fetch_and_add(&nm_mpi_profile.cur_req_total, 1);
  if(nm_mpi_profile.cur_req_total > nm_mpi_profile.max_req_total)
    {
      nm_mpi_profile.max_req_total = nm_mpi_profile.cur_req_total;
    }
#endif /* NMAD_PROFILE */
  return p_req;
}

/** allocate a request for a non-blocking collective */
__PUK_SYM_INTERNAL
nm_mpi_request_t*nm_mpi_request_alloc_icol(nm_mpi_request_type_t type, nm_mpi_count_t count, struct nm_mpi_datatype_s*p_datatype,
                                           struct nm_mpi_communicator_s*p_comm)
{
  assert(type & NM_MPI_REQUEST_COLLECTIVE);
  struct nm_mpi_request_s*p_req = nm_mpi_request_alloc();
  p_req->status = NM_MPI_STATUS_NONE;
  p_req->request_type = type;
  p_req->count = count;
  p_req->p_comm = p_comm;
  nm_mpi_request_set_datatype(p_req, p_datatype);
  nm_mpi_communicator_ref_inc(p_comm, p_req);
  return p_req;
}

/** allocate a send request and fill all fields with default values */
__PUK_SYM_INTERNAL
nm_mpi_request_t*nm_mpi_request_alloc_send(nm_mpi_request_type_t type, nm_mpi_count_t count, const void*sbuf,
                                           struct nm_mpi_datatype_s*p_datatype, int tag, struct nm_mpi_communicator_s*p_comm)
{
  assert(type & NM_MPI_REQUEST_SEND);
  struct nm_mpi_request_s*p_req = nm_mpi_request_alloc();
  p_req->request_type       = type;
  p_req->count              = count;
  p_req->sbuf               = sbuf;
  p_req->user_tag           = tag;
  p_req->p_comm             = p_comm;
  p_req->status             = NM_MPI_STATUS_NONE;
  nm_mpi_request_set_datatype(p_req, p_datatype);
  nm_mpi_communicator_ref_inc(p_comm, p_req);
#ifdef NMAD_PROFILE
  __sync_fetch_and_add(&nm_mpi_profile.cur_req_send, 1);
  if(nm_mpi_profile.cur_req_send > nm_mpi_profile.max_req_send)
    {
      nm_mpi_profile.max_req_send = nm_mpi_profile.cur_req_send;
    }
#endif /* NMAD_PROFILE */
  return p_req;
}

/** allocate a recv request and fill all fields with default values */
__PUK_SYM_INTERNAL
nm_mpi_request_t*nm_mpi_request_alloc_recv(nm_mpi_count_t count, void*rbuf,
                                           struct nm_mpi_datatype_s*p_datatype, int tag, struct nm_mpi_communicator_s*p_comm)
{
  struct nm_mpi_request_s*p_req = nm_mpi_request_alloc();
  p_req->request_type       = NM_MPI_REQUEST_RECV;
  p_req->count              = count;
  p_req->rbuf               = rbuf;
  p_req->user_tag           = tag;
  p_req->p_comm             = p_comm;
  p_req->status             = NM_MPI_STATUS_NONE;
  nm_mpi_request_set_datatype(p_req, p_datatype);
  nm_mpi_communicator_ref_inc(p_comm, p_req);
#ifdef NMAD_PROFILE
  __sync_fetch_and_add(&nm_mpi_profile.cur_req_recv, 1);
  if(nm_mpi_profile.cur_req_recv > nm_mpi_profile.max_req_recv)
    {
      nm_mpi_profile.max_req_recv = nm_mpi_profile.cur_req_recv;
    }
#endif /* NMAD_PROFILE */
  return p_req;
}

__PUK_SYM_INTERNAL
void nm_mpi_request_free(nm_mpi_request_t*p_req)
{
#ifdef NMAD_PROFILE
  __sync_fetch_and_sub(&nm_mpi_profile.cur_req_total, 1);
  if(p_req->request_type & NM_MPI_REQUEST_RECV)
    {
      __sync_fetch_and_sub(&nm_mpi_profile.cur_req_recv, 1);
    }
  else if(p_req->request_type & NM_MPI_REQUEST_SEND)
    {
      __sync_fetch_and_sub(&nm_mpi_profile.cur_req_send, 1);
    }
  else if(p_req->request_type & NM_MPI_REQUEST_COLLECTIVE)
    {
      /* nothing to do */
    }
  else if(p_req->request_type & NM_MPI_REQUEST_GREQUEST)
    {
      /* nothing to do */
    }
  else
    {
      NM_MPI_WARNING("# unexpected request type = 0x%x\n", p_req->request_type);
    }
#endif /* NMAD_PROFILE */
  if(p_req->p_comm != NULL)
    {
      nm_mpi_communicator_ref_dec(p_req->p_comm, p_req);
    }
  if(p_req->p_datatype != NULL)
    {
      nm_mpi_datatype_ref_dec(p_req->p_datatype, p_req);
    }
  if(p_req->p_datatype2 != NULL)
    {
      nm_mpi_datatype_ref_dec(p_req->p_datatype2, p_req);
    }
  if(p_req->request_type & NM_MPI_REQUEST_GREQUEST)
    {
      (*p_req->p_grequest->free_fn)(p_req->p_grequest->extra_state);
      nm_cond_destroy(&p_req->p_grequest->completed);
      p_req->p_grequest = NULL;
    }
  if((p_req->request_type & NM_MPI_REQUEST_PARTITIONED) &&
     (p_req->request_type & NM_MPI_REQUEST_RECV))
    {
      if(p_req->request_nmad.req.unpack.partition.p_pchunks != NULL)
        {
          NM_MPI_WARNING("partitioned request %d not properly completed before free\n", p_req->id);
        }
    }
  /* set request to zero to help debug */
  p_req->request_type = NM_MPI_REQUEST_ZERO;
  nm_mpi_handle_request_free(&nm_mpi_req.requests, p_req);
  nm_mpi_request_lazy_exit();
}

__PUK_SYM_INTERNAL
nm_mpi_request_t*nm_mpi_request_get(MPI_Request req_id)
{
  const int id = (int)req_id;
  if(id == MPI_REQUEST_NULL)
    return NULL;
  assert(id >= 0);
  nm_mpi_request_t*p_req = nm_mpi_handle_request_get(&nm_mpi_req.requests, req_id);
  assert(p_req != NULL);
  assert(p_req->id == id);
  return p_req;
}

static int nm_mpi_set_status(nm_mpi_request_t*p_req, struct nm_mpi_status_s*p_status)
{
  int err = MPI_SUCCESS;
  if(p_req == NULL)
    {
      /* empty status */
      p_status->MPI_TAG = MPI_ANY_TAG;
      p_status->MPI_SOURCE = MPI_ANY_SOURCE;
      p_status->MPI_ERROR = MPI_SUCCESS;
      p_status->size = 0;
      return MPI_SUCCESS;
    }
  p_status->MPI_TAG = p_req->user_tag;
  p_status->MPI_ERROR = p_req->request_error;

  if(p_req->request_type == NM_MPI_REQUEST_RECV)
    {
      if(p_req->request_source == MPI_ANY_SOURCE)
        {
          nm_gate_t p_gate;
          err = nm_sr_recv_source(nm_mpi_communicator_get_session(p_req->p_comm), &p_req->request_nmad, &p_gate);
          p_status->MPI_SOURCE = nm_mpi_communicator_get_dest(p_req->p_comm, p_gate);
        }
      else
        {
          p_status->MPI_SOURCE = p_req->request_source;
        }
      if(p_req->user_tag == MPI_ANY_TAG)
        {
          nm_tag_t nm_tag = nm_sr_request_get_tag(&p_req->request_nmad);
          p_status->MPI_TAG = (int)nm_tag;
        }
    }
  if(p_req->request_type & NM_MPI_REQUEST_FILE)
    {
      nm_mpi_file_request_status_update(p_req, p_status);
    }
  else if(p_req->request_type & NM_MPI_REQUEST_GREQUEST)
    {
      err = (*p_req->p_grequest->query_fn)(p_req->p_grequest->extra_state, p_status);
    }
  else
    {
      nm_len_t _size = 0;
      nm_sr_request_get_size(&(p_req->request_nmad), &_size);
      p_status->size = _size;
      p_status->cancelled = p_req->status & NM_MPI_REQUEST_CANCELLED;
    }
  return NM_MPI_ERROR_INTERNAL(err);
}


/* ********************************************************* */

static void nm_mpi_request_finalizer(nm_sr_event_t event, const nm_sr_event_info_t*p_info, void*_ref)
{
  nm_mpi_request_t*p_req = _ref;
  assert(event & NM_STATUS_FINALIZED);
  nm_mpi_request_free(p_req);
}

int mpi_request_free(MPI_Request*request)
{
  nm_mpi_request_t*p_req = nm_mpi_request_get(*request);
  if(p_req == NULL)
    {
      return NM_MPI_ERROR_WORLD(MPI_ERR_REQUEST);
    }
  if(p_req->request_type & NM_MPI_REQUEST_FILE)
    {
      NM_MPI_WARNING("MPI_Request_free() on MPI_File. Not supported yet.\n");
      return NM_MPI_ERROR_FILE_DEFAULT(MPI_ERR_UNKNOWN);
    }
  else if(p_req->request_type & NM_MPI_REQUEST_COLLECTIVE)
    {
      NM_MPI_WARNING("MPI_Request_free() on non-blocking collective. Not supported yet.\n");
      return NM_MPI_ERROR_WORLD(MPI_ERR_UNKNOWN);
    }
  else if(p_req->request_type & (NM_MPI_REQUEST_RECV | NM_MPI_REQUEST_SEND))
    {
      /* poiont-to-point request */
      nm_session_t p_session = nm_sr_request_get_session(&p_req->request_nmad);
      nm_sr_request_set_ref(&p_req->request_nmad, p_req);
      nm_sr_request_monitor(p_session, &p_req->request_nmad, NM_STATUS_FINALIZED, &nm_mpi_request_finalizer);
      *request = MPI_REQUEST_NULL;
    }
  else
    {
      NM_MPI_WARNING("MPI_Request_free() on unknown type of request.\n");
      return NM_MPI_ERROR_WORLD(MPI_ERR_UNKNOWN);
    }
  return MPI_SUCCESS;
}

int mpi_waitsome(int incount, MPI_Request*array_of_requests, int*outcount, int*array_of_indices, MPI_Status*array_of_statuses)
{
  nm_mpi_count_t count = 0;
  if(incount <= 0)
    {
      *outcount = MPI_UNDEFINED;
      return MPI_SUCCESS;
    }
  while(count == 0)
    {
      int i;
      nm_mpi_count_t count_null = 0;
      for(i = 0; i < incount; i++)
        {
          int flag;
          nm_mpi_request_t*p_req = nm_mpi_request_get(array_of_requests[i]);
          if(p_req == NULL || p_req->request_type == NM_MPI_REQUEST_ZERO)
            {
              count_null++;
              continue;
            }
          int err;
          if(array_of_statuses == MPI_STATUSES_IGNORE)
            {
              err = mpi_test(&array_of_requests[i], &flag, MPI_STATUS_IGNORE);
            }
          else
            {
              err = mpi_test(&array_of_requests[i], &flag, &array_of_statuses[count]);
            }
          if(err != MPI_SUCCESS)
            {
              *outcount = MPI_UNDEFINED;
              return NM_MPI_ERROR_WORLD(err);
            }
          if(flag)
            {
              array_of_indices[count] = i;
              count++;
            }
        }
      if(count_null == incount)
        {
          *outcount = MPI_UNDEFINED;
          return MPI_SUCCESS;
        }
    }
  *outcount = count;
  return MPI_SUCCESS;
}

int mpi_wait(MPI_Request*request, MPI_Status*status)
{
  if(request == NULL)
    {
      return NM_MPI_ERROR_WORLD(MPI_ERR_REQUEST);
    }
  if(*request == MPI_REQUEST_NULL)
    {
      if(status != MPI_STATUS_IGNORE)
        {
          nm_mpi_set_status(NULL, status);
        }
      return MPI_SUCCESS;
    }
  nm_mpi_request_t*p_req = nm_mpi_request_get(*request);
  if(p_req == NULL)
    {
      return NM_MPI_ERROR_WORLD(MPI_ERR_REQUEST);
    }
  int err = nm_mpi_request_wait(p_req);
  if(status != MPI_STATUS_IGNORE)
    {
      err = nm_mpi_set_status(p_req, status);
    }
  nm_mpi_request_complete(p_req, request);
  return NM_MPI_ERROR_WORLD(err);
}

int mpi_waitall(int count, MPI_Request*array_of_requests, MPI_Status*array_of_statuses)
{
  if(count < 0)
    return NM_MPI_ERROR_WORLD(MPI_ERR_COUNT);
  else if(count == 0)
    return MPI_SUCCESS;
  nm_sr_request_t**p_nm_requests = padico_malloc(count * sizeof(nm_sr_request_t*));
  int err = NM_ESUCCESS;
  /* sort-out nmad sr requests */
  int nm_count = 0;
  int i;
  for(i = 0; i < count; i++)
    {
      if(array_of_requests[i] != MPI_REQUEST_NULL)
        {
          nm_mpi_request_t*p_req = nm_mpi_request_get(array_of_requests[i]);
          if(p_req == NULL)
            {
              padico_free(p_nm_requests);
              return NM_MPI_ERROR_WORLD(MPI_ERR_REQUEST);
            }
          if((p_req->request_type & (NM_MPI_REQUEST_COLLECTIVE | NM_MPI_REQUEST_FILE)) == 0)
            {
              /* request is neither collective nor MPI-IO */
              p_nm_requests[nm_count] = &p_req->request_nmad;
              nm_count++;
            }
        }
    }
  if(nm_count > 0)
    {
      nm_sr_request_wait_all(p_nm_requests, nm_count);
    }
  padico_free(p_nm_requests);
  /* wait non-sr requests separately */
  for(i = 0; i < count; i++)
    {
      nm_mpi_request_t*p_req = nm_mpi_request_get(array_of_requests[i]);
      if(p_req != NULL)
        {
          if((p_req->request_type & (NM_MPI_REQUEST_COLLECTIVE | NM_MPI_REQUEST_FILE)) != 0)
            {
              /* request is either collective or MPI-IO */
              err = nm_mpi_request_wait(p_req);
              if(err != MPI_SUCCESS)
                break;
            }
        }
    }
  /* upodate status of all requests */
  for(i = 0; i < count; i++)
    {
      nm_mpi_request_t*p_req = nm_mpi_request_get(array_of_requests[i]);
      if(p_req != NULL)
        {
          if(array_of_statuses != MPI_STATUSES_IGNORE)
            {
              err = nm_mpi_set_status(p_req, &array_of_statuses[i]);
            }
          nm_mpi_request_complete(p_req, &array_of_requests[i]);
          if(err != NM_ESUCCESS)
            {
              break;
            }
        }
      else if(array_of_requests[i] == MPI_REQUEST_NULL && array_of_statuses != MPI_STATUSES_IGNORE)
        {
          array_of_statuses[i].MPI_SOURCE = MPI_ANY_SOURCE;
          array_of_statuses[i].MPI_TAG = MPI_ANY_TAG;
          array_of_statuses[i].size = 0;
        }
    }
  return NM_MPI_ERROR_WORLD(err);
}

int mpi_waitany(int count, MPI_Request*array_of_requests, int*rqindex, MPI_Status*status)
{
  int err = MPI_SUCCESS;
  for(;;)
    {
      int i;
      nm_mpi_count_t count_null = 0;
      for(i = 0; i < count; i++)
        {
          int flag;
          nm_mpi_request_t*p_req = nm_mpi_request_get(array_of_requests[i]);
          if(p_req == NULL || p_req->request_type == NM_MPI_REQUEST_ZERO)
            {
              count_null++;
              continue;
            }
          err = mpi_test(&(array_of_requests[i]), &flag, status);
          if(flag)
            {
              *rqindex = i;
              return NM_MPI_ERROR_WORLD(err);
            }
        }
      if(count_null == count)
        {
          *rqindex = MPI_UNDEFINED;
          return MPI_SUCCESS;
        }
    }
}

int mpi_test(MPI_Request*request, int*flag, MPI_Status*status)
{
  if(request == NULL)
    {
      return NM_MPI_ERROR_WORLD(MPI_ERR_REQUEST);
    }
  if(*request == MPI_REQUEST_NULL)
    {
      *flag = 1;
      if(status != MPI_STATUS_IGNORE)
        {
          nm_mpi_set_status(NULL, status);
        }
      return MPI_SUCCESS;
    }
  nm_mpi_request_t *p_req = nm_mpi_request_get(*request);
  if(p_req == NULL)
    {
      return NM_MPI_ERROR_WORLD(MPI_ERR_REQUEST);
    }
  int err = nm_mpi_request_test(p_req);
  if(err == NM_ESUCCESS)
    {
      *flag = 1;
      if(status != MPI_STATUS_IGNORE)
        {
          err = nm_mpi_set_status(p_req, status);
        }
      nm_mpi_request_complete(p_req, request);
    }
  else if(err == -NM_EAGAIN)
    {
      *flag = 0;
      return MPI_SUCCESS;
    }
  else
    {
      *flag = 1;
      return NM_MPI_ERROR_WORLD(err);
    }
  return MPI_SUCCESS;
}

int mpi_testany(int count, MPI_Request*array_of_requests, int*rqindex, int*flag, MPI_Status*status)
{
  int err = MPI_SUCCESS;
  nm_mpi_count_t count_inactive = 0;
  int i;
  for(i = 0; i < count; i++)
    {
      nm_mpi_request_t*p_req = nm_mpi_request_get(array_of_requests[i]);
      if(p_req == NULL || p_req->request_type == NM_MPI_REQUEST_ZERO)
        {
          count_inactive++;
          continue;
        }
      err = mpi_test(&(array_of_requests[i]), flag, status);
      if((err != MPI_SUCCESS) || (*flag == 1))
        {
          *rqindex = i;
          return NM_MPI_ERROR_WORLD(err);
        }
    }
  *rqindex = MPI_UNDEFINED;
  if(count_inactive == count)
    {
      *flag = 1;
    }
  return NM_MPI_ERROR_WORLD(err);
}

int mpi_testall(int count, MPI_Request*array_of_requests, int*flag, MPI_Status*array_of_statuses)
{
  int i, err =  MPI_SUCCESS;
  /* flag is true only if all requests have completed. Otherwise, flag is
   * false and neither the array_of_requests nor the array_of_statuses is
   * modified.
   */
  for(i = 0; i < count; i++)
    {
      nm_mpi_request_t*p_req = nm_mpi_request_get(array_of_requests[i]);
      if((array_of_requests[i] != MPI_REQUEST_NULL) && (p_req == NULL))
        {
          *flag = 0;
          return NM_MPI_ERROR_WORLD(MPI_ERR_REQUEST);
        }
      else if(p_req == NULL || p_req->request_type == NM_MPI_REQUEST_ZERO)
        {
          /* inactive request */
          continue;
        }
      else
        {
          int rc = nm_mpi_request_test(p_req);
          if(rc == -NM_EAGAIN)
            {
              /* at least one request is not completed */
              *flag = 0;
              return MPI_SUCCESS;
            }
        }
    }
  /* all the requests are valid and completed */
  for(i = 0; i < count; i++)
    {
      nm_mpi_request_t*p_req = nm_mpi_request_get(array_of_requests[i]);
      if((p_req == NULL) || (p_req->request_type == NM_MPI_REQUEST_ZERO))
        {
          /* inactive request */
          continue;
        }
      if(array_of_statuses == MPI_STATUSES_IGNORE)
        {
          err = mpi_test(&array_of_requests[i], flag, MPI_STATUS_IGNORE);
        }
      else
        {
          err = mpi_test(&array_of_requests[i], flag, &array_of_statuses[i]);
        }
      if(*flag != 1)
        {
          /* at least one request is not completed */
          NM_MPI_FATAL_ERROR("Error during MPI_Testall: request #%d should be completed, but it is not!", i);
          return NM_MPI_ERROR_WORLD(MPI_ERR_INTERN);
        }
    }
  *flag = 1;
  return NM_MPI_ERROR_WORLD(err);
}

int mpi_testsome(int count, MPI_Request*array_of_requests, int*outcount, int*indices, MPI_Status*array_of_statuses)
{
  int i, flag, err = MPI_SUCCESS;
  nm_mpi_count_t count_inactive = 0;
  *outcount = 0;
  for(i = 0; i < count; i++)
    {
      nm_mpi_request_t *p_req = nm_mpi_request_get(array_of_requests[i]);
      if(p_req == NULL || p_req->request_type == NM_MPI_REQUEST_ZERO)
        {
          count_inactive++;
          continue;
        }
      if(array_of_statuses == MPI_STATUSES_IGNORE)
        {
          err = mpi_test(&array_of_requests[i], &flag, MPI_STATUS_IGNORE);
        }
      else
        {
          err = mpi_test(&array_of_requests[i], &flag, &array_of_statuses[i]);
        }
      if(flag == 1)
        {
          indices[*outcount] = i;
          (*outcount)++;
        }
    }
  if(count_inactive == count)
    *outcount = MPI_UNDEFINED;
  return NM_MPI_ERROR_WORLD(err);
}

int mpi_cancel(MPI_Request*request)
{
  int err = MPI_SUCCESS;
  nm_mpi_request_t*p_req = nm_mpi_request_get(*request);
  if(!p_req)
    return NM_MPI_ERROR_WORLD(MPI_ERR_REQUEST);
  if(p_req->request_type == NM_MPI_REQUEST_RECV)
    {
      int rc = nm_sr_rcancel(nm_mpi_communicator_get_session(p_req->p_comm), &p_req->request_nmad);
      if(rc == NM_ESUCCESS || rc == -NM_ECANCELED)
        {
          /* successfully cancelled or already cancelled */
          p_req->status |= NM_MPI_REQUEST_CANCELLED;
          err = MPI_SUCCESS;
        }
      else if(rc == -NM_EINPROGRESS || rc == -NM_EALREADY)
        {
          /* too late for cancel, recv will be successfull: return succes, do not mark as cancelled */
          err = MPI_SUCCESS;
        }
      else
        {
          /* cannot cancel for another reason */
          NM_MPI_WARNING("MPI_Cancel unexpected error %d. Cannot cancel.\n", rc);
          err = MPI_ERR_UNKNOWN;
        }
    }
  else if(p_req->request_type & NM_MPI_REQUEST_SEND)
    {
      if(nm_sr_stest(nm_mpi_communicator_get_session(p_req->p_comm), &p_req->request_nmad) == NM_ESUCCESS)
        {
          /* already successfull: return success */
          err = MPI_SUCCESS;
        }
      else
        {
          /* cancel for send requests is not implemented in nmad.
           * 1. issue a warning
           * 2. mark as cancelled so as to unblock the next wait to avoid deadlock in case of rdv
           * 3. return an error code
           * cancel of a send request is deprecated in MPI-4
           */
          NM_MPI_WARNING("MPI_Cancel called for send- not supported.\n");
          p_req->status |= NM_MPI_REQUEST_CANCELLED;
          err = MPI_ERR_UNSUPPORTED_OPERATION;
        }
    }
  else if(p_req->request_type & NM_MPI_REQUEST_GREQUEST)
    {
      err = (*p_req->p_grequest->cancel_fn)(p_req->p_grequest->extra_state, nm_cond_test_locked(&p_req->p_grequest->completed, 1));
    }
  else
    {
      NM_MPI_FATAL_ERROR("Request type %d incorrect\n", p_req->request_type);
    }
  return NM_MPI_ERROR_WORLD(err);
}

int mpi_test_cancelled(const MPI_Status*status, int*flag)
{
  *flag = status->cancelled;
  return MPI_SUCCESS;
}

int mpi_start(MPI_Request*request)
{
  nm_mpi_request_t *p_req = nm_mpi_request_get(*request);
  int err = MPI_SUCCESS;
  assert(p_req != NULL);
  assert(p_req->request_type != NM_MPI_REQUEST_ZERO);
  assert(p_req->status & NM_MPI_REQUEST_PERSISTENT);
  if(p_req->request_type & NM_MPI_REQUEST_SEND)
    {
      err = nm_mpi_isend_start(p_req);
    }
  else if(p_req->request_type & NM_MPI_REQUEST_RECV)
    {
      err = nm_mpi_irecv_start(p_req);
    }
  else
    {
      NM_MPI_FATAL_ERROR("Unknown persistent request type: %d\n", p_req->request_type);
      err = MPI_ERR_INTERN;
    }
  return NM_MPI_ERROR_WORLD(err);
}

int mpi_startall(int count, MPI_Request *array_of_requests)
{
  int err = MPI_SUCCESS;
  int i;
  for(i = 0; i < count; i++)
    {
      err = mpi_start(&(array_of_requests[i]));
      if(err != MPI_SUCCESS)
        {
          return NM_MPI_ERROR_WORLD(err);
        }
    }
  return NM_MPI_ERROR_WORLD(err);
}

/* ********************************************************* */
/* ** core test/wait/complete functions used to build MPI level primitives */

/** free request after completion */
__PUK_SYM_INTERNAL
void nm_mpi_request_complete(nm_mpi_request_t*p_req, MPI_Request*request)
{
  if((p_req->request_type & NM_MPI_REQUEST_PARTITIONED) &&
      (p_req->request_type & NM_MPI_REQUEST_RECV))
    {
      nm_core_unpack_partition_free(&p_req->request_nmad.req);
    }
  if(!(p_req->status & NM_MPI_REQUEST_PERSISTENT))
    {
      if(request)
        {
          /* clear the reference the user has on request */
          assert(*request == p_req->id);
          *request = MPI_REQUEST_NULL;
        }
      nm_mpi_request_free(p_req);
    }
}

__PUK_SYM_INTERNAL
/** test status of the given request and set error state.
 * returns an MPI eror code (>0) or an nmad error code (<0, mostly
 * -NM_EAGAIN, which has no equivalent in MPI) */
int nm_mpi_request_test(nm_mpi_request_t*p_req)
 {
  int err = NM_ESUCCESS;
  if(p_req->request_error != MPI_SUCCESS)
    {
      /* request already in error state before testing completion */
      err = p_req->request_error;
    }
  else if(p_req->status & NM_MPI_REQUEST_CANCELLED)
    {
      err = MPI_SUCCESS;
    }
  else if(p_req->request_type & NM_MPI_REQUEST_RECV)
    {
      if(p_req->request_source != MPI_PROC_NULL)
        {
          int rc = nm_sr_rtest(nm_mpi_communicator_get_session(p_req->p_comm), &p_req->request_nmad);
          if(rc == -NM_ETRUNCATED)
            {
              err = MPI_ERR_TRUNCATE;
            }
          else
            {
              err = rc;
            }
        }
      else
        {
          err = MPI_SUCCESS;
        }
    }
  else if(p_req->request_type & NM_MPI_REQUEST_SEND)
    {
      if(p_req->gate)
        {
          int rc = nm_sr_stest(nm_mpi_communicator_get_session(p_req->p_comm), &p_req->request_nmad);
          err = rc;
        }
      else
        err = MPI_SUCCESS;
    }
  else if(p_req->request_type & NM_MPI_REQUEST_FILE)
    {
      err = nm_mpi_file_request_test(p_req);
    }
  else if(p_req->request_type & NM_MPI_REQUEST_COLLECTIVE)
    {
      err = nm_coll_req_test(p_req->collective.p_coll_req);
      if(err == NM_ESUCCESS)
        {
          p_req->collective.p_coll_req = NULL;
        }
    }
  else if(p_req->request_type & NM_MPI_REQUEST_GREQUEST)
    {
      if(nm_cond_test_locked(&p_req->p_grequest->completed, 1))
        return MPI_SUCCESS;
      else
        return -NM_EAGAIN;
    }
  else
    {
      NM_MPI_FATAL_ERROR("Request type 0x%x invalid for test\n", p_req->request_type);
    }
  /* save request error state before leaving */
  if(err != -NM_EAGAIN)
    {
      p_req->request_error = err;
    }
  return NM_MPI_ERROR_INTERNAL(err);
}

__PUK_SYM_INTERNAL
int nm_mpi_request_wait(nm_mpi_request_t*p_req)
{
  int err = MPI_SUCCESS;
  if(p_req->status & NM_MPI_REQUEST_CANCELLED)
    {
      err = MPI_SUCCESS;
    }
  else if(p_req->request_type & NM_MPI_REQUEST_RECV)
    {
      if(p_req->request_source != MPI_PROC_NULL)
        {
          nm_sr_request_wait(&p_req->request_nmad);
        }
      err = MPI_SUCCESS;
    }
  else if(p_req->request_type & NM_MPI_REQUEST_SEND)
    {
      if(p_req->gate != NULL)
        {
          nm_sr_request_wait(&p_req->request_nmad);
        }
      err = MPI_SUCCESS;
    }
  else if(p_req->request_type & NM_MPI_REQUEST_FILE)
    {
      err = nm_mpi_file_request_wait(p_req);
    }
  else if(p_req->request_type & NM_MPI_REQUEST_COLLECTIVE)
    {
      nm_coll_req_wait(p_req->collective.p_coll_req);
      err = MPI_SUCCESS;
    }
  else if(p_req->request_type & NM_MPI_REQUEST_GREQUEST)
    {
      /* get nm_core through MPI_COMM_WORLD */
      nm_mpi_communicator_t*p_comm_world = nm_mpi_communicator_get(MPI_COMM_WORLD);
      struct nm_core*p_core = nm_core_get_singleton();
      nm_cond_wait(&p_req->p_grequest->completed, 1, p_core);
      err = MPI_SUCCESS;
    }
   else
    {
      NM_MPI_FATAL_ERROR("Waiting operation invalid for request type %d\n", p_req->request_type);
    }
  return NM_MPI_ERROR_INTERNAL(err);
}


/* ** Generalized requests ********************************* */


int mpi_grequest_start(MPI_Grequest_query_function*query_fn,
                       MPI_Grequest_free_function*free_fn,
                       MPI_Grequest_cancel_function*cancel_fn, void*extra_state,
                       MPI_Request*request)
{
  struct nm_mpi_request_s*p_req = nm_mpi_request_alloc();
  struct nm_mpi_grequest_s*p_grequest = &p_req->grequest;
  p_grequest->query_fn    = query_fn;
  p_grequest->free_fn     = free_fn;
  p_grequest->cancel_fn   = cancel_fn;
  p_grequest->extra_state = extra_state;
  nm_cond_init(&p_grequest->completed, 0);
  p_req->request_type = NM_MPI_REQUEST_GREQUEST;
  p_req->p_grequest = p_grequest;
  *request = p_req->id;
  return MPI_SUCCESS;
}

int mpi_grequest_complete(MPI_Request request)
{
  nm_mpi_request_t*p_request = nm_mpi_request_get(request);
  nm_cond_signal(&p_request->p_grequest->completed, 1);
  return MPI_SUCCESS;
}


/* ** Status *********************************************** */

int mpi_get_count_c(MPI_Status*status, MPI_Datatype datatype, MPI_Count*count)
{
  nm_mpi_datatype_t*p_datatype = nm_mpi_datatype_get(datatype);
  if(p_datatype == NULL)
    {
      *count = MPI_UNDEFINED;
    }
  else
    {
      const size_t size = nm_mpi_datatype_size(p_datatype);
      if(size == 0)
        {
          *count = 0;
        }
      else if(status->size % size != 0)
        {
          *count = MPI_UNDEFINED;
        }
      else
        {
          *count = status->size / size;
        }
    }
  return MPI_SUCCESS;
}

int mpi_get_count(MPI_Status*status, MPI_Datatype datatype, int*count)
{
  MPI_Count large_count;
  int err = mpi_get_count_c(status, datatype, &large_count);
  if(err == MPI_SUCCESS)
    {
      if(large_count < INT_MAX || large_count == MPI_UNDEFINED)
        {
          *count = large_count;
          return NM_MPI_ERROR_WORLD(err);
        }
      else
        {
          *count = MPI_UNDEFINED;
          return NM_MPI_ERROR_WORLD(MPI_ERR_OTHER);
        }
    }
  else
    {
      *count = MPI_UNDEFINED;
      return NM_MPI_ERROR_WORLD(err);
    }

  return err;
}

int mpi_get_elements_c(const MPI_Status*status, MPI_Datatype datatype, MPI_Count*count)
{
  nm_mpi_datatype_t*p_datatype = nm_mpi_datatype_get(datatype);
  if(p_datatype == NULL)
    return NM_MPI_ERROR_WORLD(MPI_ERR_TYPE);
  const size_t datatype_size = nm_mpi_datatype_size(p_datatype);
  if(datatype_size == 0)
    {
      *count = 0;
    }
  else
    {
      *count = nm_mpi_datatype_get_elements(p_datatype, status->size);
    }
  return MPI_SUCCESS;
}

int mpi_get_elements_x(const MPI_Status*status, MPI_Datatype datatype, MPI_Count*count)
{
  return mpi_get_elements_c(status, datatype, count);
}

int mpi_get_elements(const MPI_Status*status, MPI_Datatype datatype, int*count)
{
  MPI_Count large_count;
  int err = mpi_get_elements_c(status, datatype, &large_count);
  if(err == MPI_SUCCESS)
    {
      if(large_count < INT_MAX || large_count == MPI_UNDEFINED)
        {
          *count = large_count;
          return NM_MPI_ERROR_WORLD(err);
        }
      else
        {
          *count = MPI_UNDEFINED;
          return NM_MPI_ERROR_WORLD(MPI_ERR_OTHER);
        }
    }
  else
    {
      *count = MPI_UNDEFINED;
      return NM_MPI_ERROR_WORLD(err);
    }
}

int mpi_status_set_elements(MPI_Status*status, MPI_Datatype datatype, int count)
{
  return mpi_status_set_elements_c(status, datatype, count);
}

int mpi_status_set_elements_x(MPI_Status*status, MPI_Datatype datatype, MPI_Count count)
{
  return mpi_status_set_elements_c(status, datatype, count);
}

int mpi_status_set_elements_c(MPI_Status*status, MPI_Datatype datatype, MPI_Count count)
{
  nm_mpi_datatype_t*p_datatype = nm_mpi_datatype_get(datatype);
  if(p_datatype == NULL)
    {
      return NM_MPI_ERROR_WORLD(MPI_ERR_TYPE);
    }
  status->size = count * nm_mpi_datatype_size(p_datatype);
  return MPI_SUCCESS;
}

int mpi_status_set_cancelled(MPI_Status*status, int flag)
{
  status->cancelled = flag;
  return MPI_SUCCESS;
}

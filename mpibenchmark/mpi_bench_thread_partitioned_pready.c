/*
 * MadMPI benchmark
 * Copyright (C) 2014-2023 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <mpi.h>
#include <pthread.h>
#include "mpi_bench_generic.h"

#if MPI_VERSION >= 4

static int threads = 0;

static struct mpi_bench_param_bounds_s param_bounds =
  {
    .min  = 1,
    .max  = THREADS_DEFAULT,
    .mult = 1,
    .incr = 1
  };

static const struct mpi_bench_param_bounds_s*mpi_bench_thread_partitioned_pready_getparams(void)
{
  param_bounds.max = mpi_bench_get_threads();
  return &param_bounds;
}

static void mpi_bench_thread_partitioned_pready_setparam(int param)
{
  threads = param;
}

static void mpi_bench_thread_partitioned_pready_endparam(void)
{
  threads = 0;
}

static void partitioned_pready_send(void*buf, size_t len)
{
  MPI_Request request;
  MPI_Psend_init(buf, threads, len / threads, MPI_CHAR, mpi_bench_common.peer, TAG,
                 MPI_COMM_WORLD, MPI_INFO_NULL, &request);
  MPI_Start(&request);
  int i;
#pragma omp parallel for  shared(request) schedule(static, 1)
  for(i = 0; i < threads; i++)
    {
      MPI_Pready(i, request);
    }
  MPI_Wait(&request, MPI_STATUS_IGNORE);
}

static void partitioned_pready_recv(void*buf, size_t len)
{
  MPI_Recv(buf, len, MPI_CHAR, mpi_bench_common.peer, TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
}

static void mpi_bench_thread_partitioned_pready_server(void*buf, size_t len)
{
  partitioned_pready_send(buf, len);
  partitioned_pready_recv(buf, len);
}

static void mpi_bench_thread_partitioned_pready_client(void*buf, size_t len)
{
  partitioned_pready_recv(buf, len);
  partitioned_pready_send(buf, len);
}

const struct mpi_bench_s mpi_bench_thread_partitioned_pready =
  {
    .label      = "mpi_bench_thread_partitioned_pready",
    .name       = "MPI threaded partitioned communication (pready only)",
    .rtt        = 0,
    .threads    = 1,
    .server     = &mpi_bench_thread_partitioned_pready_server,
    .client     = &mpi_bench_thread_partitioned_pready_client,
    .getparams  = &mpi_bench_thread_partitioned_pready_getparams,
    .setparam   = &mpi_bench_thread_partitioned_pready_setparam,
    .endparam   = &mpi_bench_thread_partitioned_pready_endparam,
    .init       = NULL,
    .finalize   = NULL
  };

#else /* MPI_VERSION >= 4*/

const struct mpi_bench_s mpi_bench_thread_partitioned_pready =
  {
    .label      = "mpi_bench_thread_partitioned_pready",
    .name       = "MPI threaded partitioned (pready only) communication (not supported)",
    .rtt        = 0,
    .threads    = 0,
    .server     = NULL,
    .client     = NULL,
    .getparams  = NULL,
    .setparam   = NULL,
    .endparam   = NULL,
    .init       = NULL,
    .finalize   = NULL
  };

#endif

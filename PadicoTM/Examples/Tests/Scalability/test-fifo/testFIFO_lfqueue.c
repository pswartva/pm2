#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <hwloc.h>
#include <time.h>
#include "Puk-lfqueue.h"
#define CONSUMER 1
#define PRODUCER 2

#define NBLOOP 1000000

/*
 * mode 0: 1 producer 1 consumer
 * mode 1: 1 producer N consumers
 * mode 2: N producers 1 consumer
 * mode 3: N producers N consumers
*/

//thread parameters
struct thread_param{
  struct test_lfqueue_s* queue;
  double*  tab_nbiteration;
  int num;
  int mode;
  int CPUnumber;
  int nbloop;
};

//creation and initialization of thread parameters
struct thread_param* thread_param_create(struct test_lfqueue_s* queue, double*  tab_nbiteration, int num, int mode, int CPUnumber, int nbloop){
  struct thread_param* thread_param=malloc(sizeof(struct thread_param));
  thread_param->queue=queue;
  thread_param->tab_nbiteration=tab_nbiteration;
  thread_param->num=num;
  thread_param->mode=mode;
  thread_param->CPUnumber=CPUnumber;
  thread_param->nbloop=nbloop;
  return thread_param;
}

//sort algorithm
void fusion(double* tableau, int debut1, int fin1, int fin2)
{

   int debut2 = fin1+1;
   int compteur1 = debut1;
   int compteur2 = debut2;
   int i;

   double* tableau2 = malloc((fin1-debut1+1)*sizeof(double));

   // copy of the element of the beginning of the array
   for(i=debut1; i<=fin1; i++)
      tableau2[i-debut1] = tableau[i];

   // fusion of the two arrays
   for(i=debut1; i<=fin2; i++)
   {
      if(compteur1==debut2) // every element of the first array used
         break; // every element ranked
      else if(compteur2==(fin2+1)) //every element of the second array used
      { // copy of the element of the first under-array at the end of the array
         tableau[i] = tableau2[compteur1-debut1];
         compteur1++;
      }
      else if(tableau2[compteur1-debut1]<tableau[compteur2])
      { // adding of 1 element of the first under-array
         tableau[i] = tableau2[compteur1-debut1];
         compteur1++; // going the next item of the first under-array
      }
      else
      { // copy of the element at the end of the array
         tableau[i] = tableau[compteur2];
         compteur2++; // going the next item of the second under-array
      }
   }
   free(tableau2);
}

void triFusionAux(double* tableau, int debut, int fin)
{
   if(debut!=fin) // stop condition
   {
      int milieu = (debut+fin)/2;
      triFusionAux(tableau, debut, milieu); // sort part1
      triFusionAux(tableau, milieu+1, fin); // sort part2
      fusion(tableau, debut, milieu, fin); // fusion of the two parts
   }
}

void triFusion(double* tableau, int longueur)
{
   if(longueur>0)
      triFusionAux(tableau, 0, longueur-1);
}


//end sort algorithm

//begin median

double median(double* tab, int length){
  if(length%2==0){
    return (tab[length/2-1]+tab[length/2])/2;
  }
  else{
    return tab[length/2];
  }
}

//end median

PUK_LFQUEUE_TYPE(test,int,-1,16777216)

int thread_CPU_assignment(int CPUnb){

  hwloc_topology_t topology;
  hwloc_topology_init(&topology);
  hwloc_topology_load(topology);
  hwloc_cpuset_t cpuset;
  hwloc_obj_t obj;
  int depth;


  depth = hwloc_get_type_or_below_depth(topology, HWLOC_OBJ_CORE);

  obj = hwloc_get_obj_by_depth(topology, depth, CPUnb);
  if (obj) {
    /* Get a copy of its cpuset that we may modify. */
    cpuset = hwloc_bitmap_dup(obj->cpuset);

    /* Get only one logical processor (in case the core is
       SMT/hyperthreaded). */
    hwloc_bitmap_singlify(cpuset);

    /* And try to bind ourself there. */
    if (hwloc_set_cpubind(topology, cpuset, HWLOC_CPUBIND_THREAD)) {
      char *str;
      int error = errno;
      hwloc_bitmap_asprintf(&str, obj->cpuset);
      printf("Couldn't bind to cpuset %s: %s\n", str, strerror(error));
      free(str);
    }
  }
  /* Free our cpuset copy */
  hwloc_bitmap_free(cpuset);
  hwloc_topology_destroy(topology);

  return 1;
}

//Time calculation withe the timespecs of the beginning and the end of the experiement
struct timespec* time_calculation(struct timespec* time_start,struct timespec* time_end){
  struct timespec* time_result=malloc(sizeof(struct timespec));
  if((time_end->tv_nsec-time_start->tv_nsec)<0)
      {
        time_result->tv_sec = time_end->tv_sec-time_start->tv_sec-1 ;
        time_result->tv_nsec = 1000000000 + time_end->tv_nsec-time_start->tv_nsec ;
      }
    else
      {
        time_result->tv_sec = time_end->tv_sec-time_start->tv_sec ;
        time_result->tv_nsec = time_end->tv_nsec-time_start->tv_nsec ;
      }
  return time_result;
}


double rate_calculation(int req,struct timespec* duration){
  return ((double)req/((double) duration->tv_sec+((double) duration->tv_nsec/1000000000)));
}

//Function pointer of the rate experiment
void *my_thread_process ( void* thread_param )
{
  struct thread_param* param=(struct thread_param*)thread_param;

  thread_CPU_assignment(param->CPUnumber);
  int i;
  int counter=0;
  struct timespec* time_start=malloc(sizeof(struct timespec));
  struct timespec* time_end=malloc(sizeof(struct timespec));
  if(param->mode==2){

    for(i=0;i<param->nbloop;i++){
      while(test_lfqueue_enqueue_ext(param->queue,4,0)==-1){}
    }

    clock_gettime(CLOCK_MONOTONIC , time_start);
    for(i=0;i<param->nbloop;i++){
      while(test_lfqueue_enqueue_ext(param->queue,4,0)==-1){}
      counter++;
    }
    clock_gettime(CLOCK_MONOTONIC , time_end);

    for(i=0;i<param->nbloop;i++){
      while(test_lfqueue_enqueue_ext(param->queue,4,0)==-1){}
    }

  }
  else if(param->mode==1){

    for(i=0;i<param->nbloop;i++){
      while(test_lfqueue_dequeue_ext(param->queue,0)==-1){}
    }

    clock_gettime(CLOCK_MONOTONIC , time_start);
    for(i=0;i<param->nbloop;i++){
      while(test_lfqueue_dequeue_ext(param->queue,0)==-1){}
      counter++;
    }

    clock_gettime(CLOCK_MONOTONIC , time_end);

    for(i=0;i<param->nbloop;i++){
      while(test_lfqueue_dequeue_ext(param->queue,0)==-1){}
    }

  }
  else{
    fprintf(stderr,"Wrong mode (consumer or producer)\n");
    return (void*)-1;
  }
  struct timespec* time_result=time_calculation(time_start, time_end);
  param->tab_nbiteration[param->num]= rate_calculation(counter,time_result);
  free(time_start);
  free(time_end);
  free(time_result);
  free(param);
  return (void*) time_result;
}

/*test1 sequence*/
void test1(int maxnbproducer, int maxnbconsumer, int nbcore){
  void *ret;
  struct test_lfqueue_s* test=malloc(sizeof(struct test_lfqueue_s));
  test_lfqueue_init(test);
  int counter=0;
  int i=0;
  int k;
  int nbconsumer=0;
  int nbproducer=0;
  printf("#Nbproducer Nbconsumer ProducerRateMin ProducerRateMedian ProducerRateMax ConsumerRateMin ConsumerRateMedian ConsumerRateMax\n");
  for(nbconsumer=1;nbconsumer<=maxnbconsumer;nbconsumer++){
    for(nbproducer=1;nbproducer<=maxnbproducer;nbproducer++){
      pthread_t thread_consumer[nbconsumer];
      pthread_t thread_producer[nbproducer];

      double rateconsumer[nbconsumer];
      double rateproducer[nbproducer];

      for(i=0;i<nbproducer;i++){
        k=counter%nbcore;
        if (pthread_create (&thread_producer[i], NULL, my_thread_process, (void*)thread_param_create(test, rateproducer, i, PRODUCER,k,NBLOOP)) < 0) {
          fprintf (stderr, "pthread_create error for thread producer\n");
          exit (1);
        }
        counter++;
      }
      for(i=0;i<nbconsumer;i++){
        k=counter%nbcore;
        if (pthread_create (&thread_consumer[i], NULL, my_thread_process, (void*)thread_param_create(test, rateconsumer, i, CONSUMER,k,(int)(((double)nbproducer/(double)nbconsumer)*(double)NBLOOP))) < 0) {
          fprintf (stderr, "pthread_create error for thread consumer\n");
          exit (1);
        }
        counter++;
      }

      for(i=0;i<nbproducer;i++){
        (void)pthread_join (thread_producer[i], &ret);
      }
      for(i=0;i<nbconsumer;i++){
        (void)pthread_join (thread_consumer[i], &ret);
      }
      triFusion(rateproducer,nbproducer);
      double producermin=rateproducer[0];
      double producermax=rateproducer[nbproducer-1];
      double producermedian=median(rateproducer,nbproducer);
      triFusion(rateconsumer,nbconsumer);
      double consumermin=rateconsumer[0];
      double consumermax=rateconsumer[nbconsumer-1];
      double consumermedian=median(rateconsumer,nbconsumer);
      printf("%9d\t%9d\t%9.3f\t%9.3f\t%9.3f\t%9.3f\t%9.3f\t%9.3f\t\n",nbproducer, nbconsumer,producermin,producermedian,producermax,consumermin,consumermedian,consumermax);
    }
  }
  free(test);
}
/*sequence test1 end*/

/*test2 sequence*/
void test2(int maxnbproducer, int maxnbconsumer,int nbcore){
  void *ret;
  struct test_lfqueue_s* test=malloc(sizeof(struct test_lfqueue_s));
  test_lfqueue_init(test);
  int counter=0;
  int i=0;
  int k;
  int nbconsumer;
  int nbproducer;
  printf("#Nbproducer Nbconsumer ProducerRateMin ProducerRateMedian ProducerRateMax ConsumerRateMin ConsumerRateMedian ConsumerRateMax\n");
  for(nbconsumer=1;nbconsumer<=maxnbconsumer;nbconsumer++){
    nbproducer=nbconsumer;
    pthread_t thread_consumer[nbconsumer];
    pthread_t thread_producer[nbproducer];

    double rateconsumer[nbconsumer];
    double rateproducer[nbproducer];

    for(i=0;i<nbproducer;i++){
      k=counter%nbcore;
      if (pthread_create (&thread_producer[i], NULL, my_thread_process, (void*)thread_param_create(test, rateproducer, i, PRODUCER,k,NBLOOP)) < 0) {
        fprintf (stderr, "pthread_create error for thread producer\n");
        exit (1);
      }
      counter++;
    }
    for(i=0;i<nbconsumer;i++){
      k=counter%nbcore;
      if (pthread_create (&thread_consumer[i], NULL, my_thread_process, (void*)thread_param_create(test, rateconsumer, i, CONSUMER,k,NBLOOP)) < 0) {
        fprintf (stderr, "pthread_create error for thread consumer\n");
        exit (1);
      }
      counter++;
    }

    for(i=0;i<nbproducer;i++){
      (void)pthread_join (thread_producer[i], &ret);
    }
    for(i=0;i<nbconsumer;i++){
      (void)pthread_join (thread_consumer[i], &ret);
    }
    triFusion(rateproducer,nbproducer);
    double producermin=rateproducer[0];
    double producermax=rateproducer[nbproducer-1];
    double producermedian=median(rateproducer,nbproducer);
    triFusion(rateconsumer,nbconsumer);
    double consumermin=rateconsumer[0];
    double consumermax=rateconsumer[nbconsumer-1];
    double consumermedian=median(rateconsumer,nbconsumer);
    printf("%9d\t%9d\t%9.3f\t%9.3f\t%9.3f\t%9.3f\t%9.3f\t%9.3f\t\n",nbproducer, nbconsumer,producermin,producermedian,producermax,consumermin,consumermedian,consumermax);

  }
  free(test);
}
/*sequence test2 end*/

int main (int argc, char **argv)
{
  if(argc==2){
    printf("#Rate test lock-free queue\n");
    thread_CPU_assignment(0);
    hwloc_topology_t topology;
    hwloc_topology_init(&topology);
    hwloc_topology_load(topology);
    int depth = hwloc_get_type_or_below_depth(topology, HWLOC_OBJ_CORE);
    int nbcore=  hwloc_get_nbobjs_by_depth(topology, depth);
    hwloc_topology_destroy(topology);

    printf("#Number of cores: %d\n", nbcore);

    int maxnbconsumer;
    int maxnbproducer;

    if(0==atoi(argv[1])){
      maxnbconsumer=1;
      maxnbproducer=1;
      printf("#Mode: 1 producer 1 consumer\n");
      test1(maxnbproducer,maxnbconsumer,nbcore);
    }
    else if(1==atoi(argv[1])){
      maxnbproducer=1;
      maxnbconsumer=nbcore-1;
      printf("#Mode: 1 producer N consumers\n");
      test1(maxnbproducer,maxnbconsumer,nbcore);
    }
    else if(2==atoi(argv[1])){
      maxnbproducer=nbcore-1;
      maxnbconsumer=1;
      printf("#Mode: N producers 1 consumer\n");
      test1(maxnbproducer,maxnbconsumer,nbcore);
    }
    else if(3==atoi(argv[1])){
      maxnbproducer=nbcore/2;
      maxnbconsumer=nbcore/2;
      printf("#Mode: N producers N consumers\n");
      test2(maxnbproducer,maxnbconsumer,nbcore);
    }
    else{
      fprintf (stderr,"Wrong mode(0 or 1 or 2 or 3\n");
    }

    return EXIT_SUCCESS;
  }
  else{
    fprintf(stderr,"./exec mode\n");
    return EXIT_SUCCESS;
  }
}

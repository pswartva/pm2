import java.lang.Thread;
import mpi.*;
import java.io.*;

public class JSimple {
    static public void main(String[] args) throws MPIException {

        java.io.PrintStream out = System.out;

        System.out.println("JSimple: in main !!!!\n");

        if (args.length>=1) {
            System.out.println("JSimple: before mpi init\n");
            MPI.Init(args);
            System.out.println("JSimple: after mpi init\n");
        }

        int me=MPI.COMM_WORLD.Rank();
        int size=MPI.COMM_WORLD.Size();
        System.out.println("Here is process "+me+" of "+size);

//      try {
//        FileOutputStream f = new FileOutputStream("/tmp/simple"+me);
//        //  out = new java.io.PrintStream(f);


//      }
//      catch(FileNotFoundException e) {
            System.out.println("name: "+MPI.Get_processor_name());
//      }
//      try {
//          Thread.sleep(1000);
//      }
//      catch(InterruptedException e) {}
            
        MPI.COMM_WORLD.Barrier();

        if(me == 0)  System.out.println("simple TEST COMPLETE\n");
        
        MPI.Finalize();
    }
    
}

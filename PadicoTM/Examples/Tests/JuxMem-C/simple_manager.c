/**
 * A simple JuxMem client (do nothing)
 * $Id: simple_provider.c,v 1.4 2005/06/29 11:56:58 mjan Exp $
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <apr_time.h>

#include <juxmem.h>

#define DEBUG 1

int main(int argc, char **argv)
{
  JuxMem_peer *juxmem_peer = NULL;
  int uptime = 0;
  
  if (argc != 2) {
    printf("Syntax: simple_manager [uptime in seconds]\n");
    exit(0);
  }
  
  uptime = atoi(argv[1]);
  
  juxmem_peer = juxmem_initialize(NULL, (int) DEBUG, "jxta.log");
  assert(juxmem_peer != NULL);
  
  apr_sleep(APR_USEC_PER_SEC * uptime);

  juxmem_terminate(juxmem_peer);
  
  return 0;
}

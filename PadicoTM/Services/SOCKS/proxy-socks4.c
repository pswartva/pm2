/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file Hardcoded connection to a SOCKS4 proxy
 */

#include <Padico/Module.h>
#include <Padico/Puk.h>
#include "ProxyFactory.h"

static int proxy_socks4_init(void);

PADICO_MODULE_DECLARE(Proxy_SOCKS4, proxy_socks4_init, NULL, NULL);


static int  proxy_socks4_create(const struct sockaddr**addr, socklen_t*addrlen);
static void proxy_socks4_release(void);

static const struct padico_proxyfactory_s proxy_socks4_driver =
  {
    .create  = &proxy_socks4_create,
    .release = &proxy_socks4_release
  };

static int proxy_socks4_init(void)
{
  puk_component_declare("Proxy_SOCKS4",
                        puk_component_provides("ProxyFactory", "proxy", &proxy_socks4_driver),
                        puk_component_attr("gateway", "localhost"));
  return 0;
}

static int  proxy_socks4_create(const struct sockaddr**addr, socklen_t*addrlen)
{

  return 0;
}

static void proxy_socks4_release(void)
{
}

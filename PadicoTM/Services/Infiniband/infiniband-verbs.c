/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file infiniband-verbs.c
 *  @brief Infiniband Verbs topology detection and description
 */

#include <endian.h>
#include <net/if_arp.h>

#include <Padico/Puk.h>
#include <Padico/Module.h>

#include "InfinibandVerbs.h"

static int padico_ibv_init(void);
static void padico_ibv_finalize(void);

PADICO_MODULE_DECLARE(InfinibandVerbs, padico_ibv_init, NULL, padico_ibv_finalize,
                      "Topology");


/* ********************************************************* */

static struct padico_ibv_s
{
  struct padico_ibv_port_vect_s ports;
} padico_ibv;

/* ********************************************************* */

static void padico_ibv_create_port(struct ibv_device*ib_dev, int port_num)
{
  struct padico_ibv_port_s*ibv_port = padico_malloc(sizeof(struct padico_ibv_port_s));
  const char*ib_devname = ibv_get_device_name(ib_dev);
  ibv_port->devname = padico_strdup(ib_devname);
  ibv_port->context = ibv_open_device(ib_dev);

  /* detect HCA GUID */
  struct ibv_device_attr device_attr;
  ibv_query_device(ibv_port->context, &device_attr);
  ibv_port->guid = be64toh(device_attr.node_guid);
  ibv_port->port_num = port_num;
  assert(port_num <= device_attr.phys_port_cnt);
  /* detect subnet GID */
  union ibv_gid gid;
  ibv_query_gid(ibv_port->context, ibv_port->port_num, 0, &gid);
  ibv_port->subnet = be64toh(gid.global.subnet_prefix);
  /* Get port LID */
  struct ibv_port_attr port_attr;
  int rc = ibv_query_port(ibv_port->context, port_num, &port_attr);
  if(rc)
    {
      padico_fatal("couldn't get port attributes for device = %s; port = %d; rc = %d (%s).\n", ib_devname, port_num, rc, strerror(rc));
    }
  if(port_attr.state != IBV_PORT_ACTIVE)
    {
      padico_out(puk_verbose_notice, "device = %s; port %d is not active.\n", ib_devname, ibv_port->port_num);
      padico_free((void*)ibv_port->devname);
      padico_free(ibv_port);
      return;
    }
  ibv_port->port_lid = port_attr.lid;
  int link_width = -1;
  switch(port_attr.active_width)
    {
    case 1: link_width = 1;  break;
    case 2: link_width = 4;  break;
    case 4: link_width = 8;  break;
    case 8: link_width = 12; break;
    case 16: link_width = 2; break;
    }
  int link_rate = -1;
  switch(port_attr.active_speed)
    {
    case 0x01: link_rate = 2;  break;
    case 0x02: link_rate = 4;  break;
    case 0x04: link_rate = 8;  break;
    case 0x08: link_rate = 10; break;
    case 0x10: link_rate = 14; break;
    case 0x20: link_rate = 25; break;
    case 0x40: link_rate = 50; break;
    default: padico_out(puk_verbose_notice, "unknown active_speed = %x\n", port_attr.active_speed);
    }
  const int data_rate = link_width * link_rate;

  padico_print("device: '%s'; subnet GID: %04X:%04X:%04X:%04X; hca GUID: %04X:%04X:%04X:%04X; link width: %d; rate: %d (%dGb/s)\n",
               ib_devname,
               (unsigned)((ibv_port->subnet >> 48) & 0xFFFF), (unsigned)((ibv_port->subnet >> 32) & 0xFFFF),
               (unsigned)((ibv_port->subnet >> 16) & 0xFFFF), (unsigned)(ibv_port->subnet & 0xFFFF),
               (unsigned)((ibv_port->guid >> 48) & 0xFFFF),   (unsigned)((ibv_port->guid >> 32) & 0xFFFF),
               (unsigned)((ibv_port->guid >> 16) & 0xFFFF),   (unsigned)(ibv_port->guid & 0xFFFF),
               link_width, link_rate, data_rate);
  if(port_attr.lid == 0)
    {
      padico_out(puk_verbose_notice, "device = %s; port %d; ignoring port with LID = 0.\n", ib_devname, ibv_port->port_num);
      padico_free(ibv_port);
      return;
    }
  const uint64_t default_subnet = 0xFE80000000000000ULL;
  if(ibv_port->subnet == default_subnet)
    {
      padico_out(puk_verbose_notice, "device '%s' subnet has factory default GID prefix %04X:%04X:%04X:%04X. NetSelector is likely to detect wrong routes. Please customize subnet GID in opensm configuration.\n",
                 ib_devname,
                 (unsigned)((ibv_port->subnet >> 48) & 0xFFFF), (unsigned)((ibv_port->subnet >> 32) & 0xFFFF),
                 (unsigned)((ibv_port->subnet >> 16) & 0xFFFF), (unsigned)(ibv_port->subnet & 0xFFFF));
    }

  /* *** Topology */
  padico_string_t s_net = padico_string_new();
  padico_string_printf(s_net, "Infiniband-%dGbps-(w%d-s0x%02X)-[%04X:%04X:%04X:%04X]",
                       data_rate, port_attr.active_width, port_attr.active_speed,
                       ((ibv_port->subnet >> 48) & 0xFFFF), ((ibv_port->subnet >> 32) & 0xFFFF),
                       ((ibv_port->subnet >> 16) & 0xFFFF), (ibv_port->subnet & 0xFFFF));
  padico_topo_network_t network = padico_topo_network_getbyid(padico_string_get(s_net));
  if(network)
    {
      ibv_port->network = network;
    }
  else
    {
      ibv_port->network = padico_topo_network_create(padico_string_get(s_net), PADICO_TOPO_SCOPE_HOST, sizeof(ibv_port->port_lid), AF_UNSPEC);
    }
  padico_string_delete(s_net);
  padico_topo_network_set_perf(ibv_port->network, 2000, 1024 * 1024 * (data_rate / 8) * 0.75);
  padico_topo_binding_t binding = padico_topo_host_bind(padico_topo_getlocalhost(), ibv_port->network, &ibv_port->port_lid);
  padico_topo_hwaddr_t hwaddr = padico_topo_hwaddr_create(ib_devname, -1 /* no rtnetlink index */, ARPHRD_INFINIBAND, sizeof(ibv_port->guid), &ibv_port->guid);
  padico_topo_binding_add_hwaddr(binding, hwaddr);
  padico_topo_hwaddr_add(padico_topo_getlocalhost(), &hwaddr);
  padico_topo_binding_add_index(binding, port_num);

  padico_ibv_port_vect_push_back(&padico_ibv.ports, ibv_port);

}

static void padico_ibv_discover(void)
{
  /* scan device list */
  int num_devices = -1;
  struct ibv_device**dev_list = ibv_get_device_list(&num_devices);
  if(!dev_list)
    {
      padico_warning("no device found.\n");
    }
  else
    {
      padico_print("detected %d devices.\n", num_devices);
    }
  if(*dev_list == NULL)
    {
      padico_warning("no device found.\n");
    }
  int d = 0;
  while(dev_list[d] != NULL)
    {
      struct ibv_device*ib_dev = dev_list[d];
      const char*ib_devname = ibv_get_device_name(ib_dev);
      struct ibv_context*context = ibv_open_device(ib_dev);
      if(context == NULL)
        {
          padico_fatal("couldn't get context for IB device %s (%s)\n", ib_devname, strerror(errno));
        }
      struct ibv_device_attr device_attr;
      ibv_query_device(context, &device_attr);
      int port_count = device_attr.phys_port_cnt;
      ibv_close_device(context);
      int p;
      for(p = 1; p <= port_count; p++)
        {
          padico_ibv_create_port(ib_dev, p);
        }
      d++;
    }

  ibv_free_device_list(dev_list);


}



const struct padico_ibv_port_vect_s*padico_ibv_get_ports(void)
{
  return &padico_ibv.ports;
}

static int padico_ibv_init(void)
{
  padico_ibv_port_vect_init(&padico_ibv.ports);
  padico_ibv_discover();
  return 0;
}

static void padico_ibv_finalize(void)
{
  padico_ibv_port_vect_itor_t i;
  puk_vect_foreach(i, padico_ibv_port, &padico_ibv.ports)
    {
      struct padico_ibv_port_s*ibv_port = *i;
      ibv_close_device(ibv_port->context);
      padico_free((void*)ibv_port->devname);
      padico_free(ibv_port);
    }
}

#!/bin/bash

if [ "x" != "x$1" ]
then
    echo "# inserting new host $1"
    psql -d tlse_db -c "INSERT INTO computer (full_name, etat, active, prefix_location, place_id, grid_id) VALUES ('$1', 'decontamine', true, '/home/gridtlse/Websolve/matrices', 1, 1);"
    id=$(psql -d tlse_db -c "SELECT computer_id from computer where full_name='$1'" | head -n 3 | tail -n 1)
    psql -d tlse_db -c "INSERT INTO service (computer_id, tool_id, binary_name) VALUES (${id}, 105, 'mainMUMPS492');"
    psql -d tlse_db -c "INSERT INTO service (computer_id, tool_id, binary_name) VALUES (${id}, 106, 'mainMUMPS492');"
    psql -d tlse_db -c "INSERT INTO service (computer_id, tool_id, binary_name) VALUES (${id}, 72, 'nomain');"
    psql -d tlse_db -c "INSERT INTO service (computer_id, tool_id, binary_name) VALUES (${id}, 109, 'validMat');"
    psql -d tlse_db -c "INSERT INTO service (computer_id, tool_id, binary_name) VALUES (${id}, 123, 'mainSolve');"
    psql -d tlse_db -c "INSERT INTO service (computer_id, tool_id, binary_name) VALUES (${id}, 122, 'mainFacto');"
fi


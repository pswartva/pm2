#! /bin/bash

iptables -F
iptables -X

# allow: loopback, DNS, ssh
iptables -A INPUT -i lo -j ACCEPT
iptables -A INPUT -p tcp --dport 53 -j ACCEPT
iptables -A INPUT -p udp --dport 53 -j ACCEPT
iptables -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A INPUT -p tcp --dport ssh -j ACCEPT
# Lille frontend
iptables -A INPUT --source 192.168.159.228 -j ACCEPT
iptables -A INPUT --source 192.168.160.228 -j ACCEPT
iptables -A INPUT --source frontend -j ACCEPT
# log & drop other packets
iptables -A INPUT -p tcp -j LOG --log-prefix "TCP LOGDROP"
iptables -A INPUT -p udp -j LOG --log-prefix "UDP LOGDROP"
iptables -A INPUT -j DROP


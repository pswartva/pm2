#
README for Puk
==============

This document decribes Puk installation and configuration.

for any question, mailto: Alexandre.Denis@inria.fr

for information on what Puk is, see http://pm2.gforge.inria.fr/PadicoTM/

Quick Start
-----------

A quick cheat sheet for the impatient:

    ./autogen.sh
    mkdir build
    cd build
    ../configure --prefix=/prefix
    make
    make install

Requirements
------------
  - autoconf and autoheader (v 2.50 or later)
  - pkg-config
  - Expat 2.0 or higer
    see http://www.libexpat.org/ to download.
  - gcc 3.2 or higher


Documentation
-------------

- Web site:
    http://pm2.gforge.inria.fr/PadicoTM/

- git users:
  Please initialize autoconf-generated files with the following command:
  `./autogen.sh`


- Warning:
  + Puk purposely cannot be built in its source tree. Please create a build
    directory anywhere.

- To generate doxygen documentation:

    cd ${prefix}/build/Puk
    make docs

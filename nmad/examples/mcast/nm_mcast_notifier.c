/*
 * NewMadeleine
 * Copyright (C) 2021-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

/* Example / test of the mcast interface with mcast_send notifier */

#include <stdlib.h>
#include <stdio.h>

#include <nm_launcher_interface.h>
#include <nm_coll_interface.h>
#include <nm_mcast_interface.h>

#include "../common/nm_examples_helper.h"

void mcast_end_callback(void* ref)
{
  nm_cond_signal(ref, 1);
}

int main(int argc, char** argv)
{
  int i, worldsize;
  const nm_tag_t tag = 0xbeef;
  const char msg[] = "Hello, world!";
  const nm_len_t msg_size = strlen(msg) + 1;

  nm_examples_init_topo(&argc, argv, NM_EXAMPLES_TOPO_STAR);

  nm_launcher_get_size(&worldsize);
  if(worldsize == 1)
    {
      fprintf(stderr, "We need several nodes.\n");
      nm_examples_exit();
      return EXIT_FAILURE;
    }

  nm_mcast_service_t mcast_service = nm_mcast_init(p_comm);

  if (is_server)
    {
      int* dests = malloc(sizeof(int) * (worldsize - 1));
      for (i = 1; i < worldsize; i++)
        {
          dests[i-1] = i;
        }

      struct nm_data_s data;
      nm_data_contiguous_build(&data, (char*) msg, msg_size);

      nm_cond_status_t cond_mcast_ended;
      nm_cond_init(&cond_mcast_ended, 0);

      nm_mcast_t mcast;
      nm_mcast_send_init(mcast_service, &mcast);
      nm_mcast_send_set_notifier(&mcast, mcast_end_callback, &cond_mcast_ended);
      nm_mcast_isend(&mcast, p_comm, dests, NULL, worldsize-1, tag, &data, 0, NM_COLL_TREE_DEFAULT);

      nm_cond_wait(&cond_mcast_ended, 1, nm_core_get_singleton());
      nm_cond_destroy(&cond_mcast_ended);

      nm_mcast_send_destroy(&mcast);

      free(dests);
    }
  else
    {
      char* buffer = malloc(msg_size);

      nm_sr_recv(nm_comm_get_session(p_comm), 0, tag, buffer, msg_size);

      if (strcmp(buffer, msg))
        {
          NM_FATAL("Received msg: '%s' (should be '%s')\n", buffer, msg);
        }

      free(buffer);
    }

  nm_coll_barrier(p_comm, 0xF3);

  printf("success\n");

  nm_mcast_finalize(mcast_service);
  nm_examples_exit();
  return EXIT_SUCCESS;
}

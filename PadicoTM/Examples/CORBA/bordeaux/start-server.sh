#! /bin/sh 

if [ "x$1" = "x" ]; then
    echo "usage: $0 <host>" 1>&2
    echo "  with <host> hostname of the omniNames server" 1>&2
    exit 1
fi

nameserver="$1"

nsconfig=${PWD}/../../NetSelector/bordeaux.xml


set -x

padico-launch -v -c -DNS_BASIC_CONF=${nsconfig} -DPADICO_BOOTSTRAP_MODE=server ../bench-CORBA-server -ORBInitRef NameService=corbaloc:iiop:${nameserver}:2809/NameService


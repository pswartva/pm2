/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2024 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 */

#include "Puk-internals.h"
#include "Module.h"
#include <stdlib.h>

PADICO_MODULE_HOOK(Puk);


/** destructor for hashtable */
static inline void puk_iface_destructor(const char*name, puk_iface_t iface)
{
  padico_free((char*)iface->name);
  padico_free(iface);
}

PUK_HASHTABLE_TYPE(puk_iface, const char*, puk_iface_t,
                   &puk_hash_string_default_hash, &puk_hash_string_default_eq, &puk_iface_destructor);

static inline void puk_component_destructor(const char*name, puk_component_t c)
{
  puk_component_destroy(c);
}

PUK_HASHTABLE_TYPE(puk_component, const char*, puk_component_t,
                   &puk_hash_string_default_hash, &puk_hash_string_default_eq, &puk_component_destructor);

/** Puk components manager
 */
static struct
{
  puk_iface_hashtable_t ifaces;         /**< table of known interfaces- hashed by name */
  puk_component_hashtable_t components; /**< table of registered components- hashed by name */
} puk_components = { .ifaces = NULL, .components = NULL };


/* ********************************************************* */

static void puk_composite_start_handler(puk_parse_entity_t e);
static void puk_composite_end_handler(puk_parse_entity_t e);
static void puk_component_start_handler(puk_parse_entity_t e);
static void puk_component_end_handler(puk_parse_entity_t e);
static void puk_attr_end_handler(puk_parse_entity_t e);
static void puk_uses_start_handler(puk_parse_entity_t e);
static void puk_entrypoint_start_handler(puk_parse_entity_t e);
static void puk_lscomponents_handler(puk_parse_entity_t e);

static void*puk_composite_instantiate(puk_instance_t instance, puk_context_t in_context);
static void puk_composite_destroy(void*_status);

static const struct puk_component_driver_s composite_component_driver =
  {
    .instantiate = &puk_composite_instantiate,
    .destroy     = &puk_composite_destroy
  };


/* ********************************************************* */
/* local shortcuts */


/** @internal Gets the composite content from a component (supposed to be a composite) */
static inline
struct puk_composite_content_s*puk_composite_get(puk_component_t component)
{
  const struct puk_composite_driver_s*const driver = component ?
    puk_component_get_driver_PadicoComposite(component, NULL) : NULL;
  if(!driver)
    {
      padico_fatal("component *%s* has no 'PadicoComposite' facet.\n", component->name);
    }
  return driver->content;
}


/* ********************************************************* */

void padico_puk_components_init(void)
{
  if(puk_components.ifaces != NULL)
    return;
  puk_components.ifaces = puk_iface_hashtable_new();
  puk_components.components = puk_component_hashtable_new();
  puk_iface_register("PadicoComponent");
  puk_iface_register("PadicoComposite");

  /* parsing */
  puk_xml_add_action((struct puk_tag_action_s)
  {
    .xml_tag        = "puk:composite", /* puk:composite */
    .start_handler  = &puk_composite_start_handler,
    .end_handler    = &puk_composite_end_handler,
    .required_level = PUK_TRUST_CONTROL
  });
  puk_xml_add_action((struct puk_tag_action_s)
  {
    .xml_tag        = "puk:component", /* puk:component */
    .start_handler  = &puk_component_start_handler,
    .end_handler    = &puk_component_end_handler,
    .required_level = PUK_TRUST_CONTROL
  });
  puk_xml_add_action((struct puk_tag_action_s)
  {
    .xml_tag        = "puk:attr", /* puk:attr */
    .start_handler  = NULL,
    .end_handler    = &puk_attr_end_handler,
    .required_level = PUK_TRUST_CONTROL
  });
  puk_xml_add_action((struct puk_tag_action_s)
  {
    .xml_tag        = "puk:uses", /* puk:uses */
    .start_handler  = &puk_uses_start_handler,
    .end_handler    = NULL,
    .required_level = PUK_TRUST_CONTROL
  });
  puk_xml_add_action((struct puk_tag_action_s)
  {
    .xml_tag        = "puk:entry-point", /* puk:entry-point */
    .start_handler  = &puk_entrypoint_start_handler,
    .end_handler    = NULL,
    .required_level = PUK_TRUST_CONTROL
  });
  puk_xml_add_action((struct puk_tag_action_s)
  {
    .xml_tag        = "lscomponents",
    .start_handler  = &puk_lscomponents_handler,
    .end_handler    = NULL,
    .required_level = PUK_TRUST_CONTROL
  });
}

void padico_puk_components_finalize(void)
{
  /* purge components */
  puk_component_hashtable_delete(puk_components.components);
  /* purge ifaces */
  puk_iface_hashtable_delete(puk_components.ifaces);
}


/* ********************************************************* */
/* *** PadicoComponent interface *************************** */

puk_iface_t puk_iface_register(const char*name)
{
  puk_lock_acquire();
  puk_iface_t iface = puk_iface_hashtable_lookup(puk_components.ifaces, name);
  if(!iface)
    {
      iface = padico_malloc(sizeof(struct puk_iface_s));
      padico_out(40, "registering iface %s\n", name);
      iface->name = padico_strdup(name);
      puk_iface_hashtable_insert(puk_components.ifaces, iface->name, iface);
    }
  puk_lock_release();
  return iface;
}

puk_iface_t puk_iface_lookup(const char*name)
{
  puk_lock_acquire();
  puk_iface_t iface = puk_iface_hashtable_lookup(puk_components.ifaces, name);
  if(!iface)
    {
      padico_out(6, "PadicoComponent: interface %s not found.\n", name);
    }
  puk_lock_release();
  return iface;
}

/* ********************************************************* */
/* ***** PadicoComponent component ************************* */

static puk_component_t puk_component_new(const char*name, puk_mod_t home_mod)
{
  puk_component_t c = padico_malloc(sizeof(struct puk_component_s));
  padico_out(40, "building component %s\n", name);
  c->name = padico_strdup(name);
  c->attrs = NULL;
  puk_uses_vect_init(&c->uses);
  puk_facet_vect_init(&c->provides);
  c->home = home_mod;
  if(home_mod)
    home_mod->managed_components++;
  puk_lock_acquire();
  puk_component_hashtable_insert(puk_components.components, c->name, c);
  puk_lock_release();
  return c;
}

puk_component_t puk_component_declare2(const char component_name[], puk_mod_t home_mod,
                                       int n_ports, const struct puk_component_port_s ports[])
{
  int i;
  puk_lock_acquire();
  puk_component_t a = puk_component_hashtable_lookup(puk_components.components, component_name);
  puk_lock_release();
  if(a != NULL)
    return a;
  a = puk_component_new(component_name, home_mod);
  for(i = 0; i < n_ports; i++)
    {
      struct puk_component_port_s r = ports[i];
      switch(r.kind)
        {
        case PUK_COMPONENT_PROVIDES:
          {
            puk_iface_t iface = puk_iface_register(r.content.provides.iface_name);
            padico_out(40, "  component provides facet %s\n", iface->name);
            const struct puk_facet_s _facet =
              {
                .iface  = iface,
                .label  = r.label?padico_strdup(r.label):NULL,
                .driver = r.content.provides.driver
              };
            puk_facet_vect_push_back(&a->provides, _facet);
          }
          break;
        case PUK_COMPONENT_USES:
          {
            puk_iface_t iface = puk_iface_register(r.content.uses.iface_name);
            padico_out(40, "  component uses iface %s\n", iface->name);
            const struct puk_uses_s _uses =
              {
                .iface = iface,
                .label = r.label
              };
            puk_uses_vect_push_back(&a->uses, _uses);
          }
          break;
        case PUK_COMPONENT_ATTR:
          {
            char*label = padico_strdup(r.label);
            char*value = r.content.attr.value ? padico_strdup(r.content.attr.value) : NULL;
            if(!a->attrs)
              a->attrs = puk_attr_hashtable_new();
            puk_attr_hashtable_insert(a->attrs, label, value);
          }
          break;
        }
    }
  const struct puk_component_driver_s*driver = puk_component_get_driver_PadicoComponent(a, NULL);
  if(driver && driver->component_init)
    {
      (*driver->component_init)();
    }
  padico_out(40, "registering component %s.\n", a->name);
  return a;
}

puk_facet_t puk_component_get_facet(puk_component_t a, puk_iface_t iface, const char*label)
{
  puk_facet_vect_itor_t i;
  /* @note linear search better than anything else when #facet < 5! */
  puk_lock_acquire();
  puk_vect_foreach(i, puk_facet, &a->provides)
    {
      if((i->iface == iface) &&
         ((label == NULL) || (i->label == NULL) || (strcmp(label, i->label) == 0)) )
        goto out;
    }
  i = NULL;
 out:
  puk_lock_release();
  return i;
}

const char*puk_component_getattr(puk_component_t c, const char*label)
{
  const char*value = NULL;
  if(c->attrs != NULL)
    value = puk_attr_hashtable_lookup(c->attrs, label);
  if(!value)
    {
      puk_context_vect_t contexts = puk_component_get_contexts(c);
      puk_context_vect_itor_t i;
      puk_vect_foreach(i, puk_context, contexts)
        {
          value = puk_context_getattr(*i, label);
          if(value)
            break;
        }
      puk_context_vect_delete(contexts);
    }
  return value;
}

void puk_component_setattr(puk_component_t c, const char*label, const char*value)
{
  puk_context_vect_t contexts = puk_component_get_contexts(c);
  puk_context_vect_itor_t i;
  puk_vect_foreach(i, puk_context, contexts)
    {
      if((*i)->attrs && puk_attr_hashtable_probe((*i)->attrs, label))
        {
          puk_context_putattr(*i, label, value);
        }
    }
  puk_context_vect_delete(contexts);
}

void puk_component_addattr(puk_component_t c, const char*label, const char*value)
{
  puk_context_vect_t contexts = puk_component_get_contexts(c);
  puk_context_vect_itor_t i;
  puk_vect_foreach(i, puk_context, contexts)
    {
      puk_context_putattr(*i, label, value);
    }
  puk_context_vect_delete(contexts);
}

void puk_component_listattrs(puk_component_t c, const char**labels, const char**values, int*n)
{
  puk_context_vect_t contexts = puk_component_get_contexts(c);
  puk_context_vect_itor_t i;
  int k = 0;
  puk_vect_foreach(i, puk_context, contexts)
    {
      if((*i)->attrs)
        {
          puk_attr_hashtable_enumerator_t e = puk_attr_hashtable_enumerator_new((*i)->attrs);
          const char*l = puk_attr_hashtable_enumerator_next_key(e);
          while(l != NULL)
            {
              const char*v = puk_component_getattr(c, l);
              labels[k] = l;
              values[k] = v;
              k++;
              if(k >= *n)
                break;
              l = puk_attr_hashtable_enumerator_next_key(e);
            }
          puk_attr_hashtable_enumerator_delete(e);
        }
    }
  *n = k;
}

/** Component lookup+auto-load if lookup fails.
 */
puk_component_t puk_component_resolve_internal(const char*component_name, puk_mod_t owner)
{
  puk_lock_acquire();
  puk_component_t component = puk_component_hashtable_lookup(puk_components.components, component_name);
  puk_lock_release();
  if(!component)
    {
      padico_out(20, "component '%s' not found. Auto-load...\n", component_name);
      puk_mod_t mod = NULL;
      padico_rc_t rc = padico_puk_mod_resolve(&mod, component_name);
      if(!padico_rc_iserror(rc))
        {
          padico_out(20, "loading component %s ...\n", component_name);
          puk_mod_set_ancestor(mod, owner);
          rc = padico_puk_mod_load(mod);
          puk_lock_acquire();
          component = puk_component_hashtable_lookup(puk_components.components, component_name);
          puk_lock_release();
          if(component == NULL)
            {
              padico_warning("module %s does not provide component %s.\n",
                             mod->mod_name, component_name);
            }
        }
      if(!padico_rc_iserror(rc))
        {
          padico_out(20, "component '%s' successfully loaded.\n", component_name);
        }
      else
        {
          padico_warning("component '%s' auto-load failed (%s).\n",
                         component_name, padico_rc_gettext(rc));
        }
    }
  else
    {
      if(component->home)
        {
          /*
            component->home->used_by++;
          */
          component->home->managed_components++;
        }
    }
  return component;
}

void puk_attr_destructor(char*label, char*value)
{
  padico_free(label);
  if(value != NULL)
    padico_free(value);
}

static void puk_context_destructor(void*key, void*data)
{
  puk_context_t context = data;
  puk_context_destroy(context);
}

void puk_component_destroy(puk_component_t c)
{
  assert(c->name != NULL);
  const struct puk_component_driver_s*driver = puk_component_get_driver_PadicoComponent(c, NULL);
  if(driver && driver->component_finalize)
    {
      (*driver->component_finalize)();
    }
  puk_component_hashtable_remove(puk_components.components, c->name);
  const struct puk_composite_driver_s*const composite_driver =
    puk_component_get_driver_PadicoComposite(c, NULL);
  if(composite_driver)
    {
      struct puk_composite_content_s*content = composite_driver->content;
      puk_component_conn_vect_itor_t ep;
      puk_vect_foreach(ep, puk_component_conn, &content->entry_points)
        {
          struct puk_uses_s*receptacle = ep->receptacle;
          padico_free((char*)receptacle->label);
          padico_free(receptacle);
        }
      puk_component_conn_vect_destroy(&content->entry_points);
      puk_hashtable_delete(content->contexts, &puk_context_destructor);
      padico_free(content);
    }
  padico_free((char*)c->name);
  c->name = NULL;
  puk_uses_vect_destroy(&c->uses);
  puk_facet_vect_itor_t f;
  puk_vect_foreach(f, puk_facet, &c->provides)
    {
      if(f->label)
        padico_free((char*)f->label);
    }
  puk_facet_vect_destroy(&c->provides);
  if(c->attrs)
    {
      puk_attr_hashtable_delete(c->attrs);
    }
  padico_free(c);
}

static void puk_lscomponents_handler(puk_parse_entity_t e)
{
  puk_component_hashtable_enumerator_t n = puk_component_hashtable_enumerator_new(puk_components.components);
  padico_string_t s = padico_string_new();
  puk_component_t c = puk_component_hashtable_enumerator_next_data(n);
  while(c)
    {
      padico_string_t sz = puk_component_serialize(c);
      padico_string_catf(s, "%s\n", padico_string_get(sz));
      padico_string_delete(sz);
      c = puk_component_hashtable_enumerator_next_data(n);
    }
  puk_component_hashtable_enumerator_delete(n);
  puk_parse_set_rc(e, padico_rc_msg(padico_string_get(s)));
  padico_string_delete(s);
}

void puk_component_home_flush(puk_mod_t mod)
{
  puk_component_vect_t todo = puk_component_vect_new();
  puk_component_hashtable_enumerator_t n = puk_component_hashtable_enumerator_new(puk_components.components);
  puk_component_t c = puk_component_hashtable_enumerator_next_data(n);
  while(c)
    {
      if(c->home == mod)
        {
#ifdef PUK_DEBUG
          padico_out(10, "mod = %s; flush component = %s\n", mod->mod_name, c->name);
#endif /* PUK_DEBUG */
          puk_component_vect_push_back(todo, c);
        }
      c = puk_component_hashtable_enumerator_next_data(n);
    }
  puk_component_hashtable_enumerator_delete(n);
  /* do not destroy component while the enumerator is using the hashtable */
  puk_component_vect_itor_t i;
  puk_vect_foreach(i, puk_component, todo)
    {
      puk_component_destroy(*i);
    }
  puk_component_vect_delete(todo);
}

/* ********************************************************* */
/* *** Contexts & composites ******************************* */

void puk_context_putattr(puk_context_t context, const char*label, const char*value)
{
  if(!context->attrs)
    context->attrs = puk_attr_hashtable_new();
  char*outlabel = NULL, *outvalue = NULL;
  puk_attr_hashtable_lookup2(context->attrs, label, &outlabel, &outvalue);
  char*v = (value != NULL) ? padico_strdup(value) : NULL;
  if(outlabel)
    {
      if(outvalue)
        padico_free((void*)outvalue);
      puk_attr_hashtable_insert(context->attrs, outlabel, v);
    }
  else
    {
      puk_attr_hashtable_insert(context->attrs, padico_strdup(label), v);
    }
}

const char*puk_context_getattr(puk_context_t context, const char*label)
{
  const char*value = (context && context->attrs) ? puk_attr_hashtable_lookup(context->attrs, label) : NULL;
  return value;
}

/** get a receptacle of a component context */
puk_component_conn_t puk_context_conn_lookup(puk_context_t context, puk_iface_t iface, const char*label)
{
  if(context == NULL)
    return NULL;
  puk_component_conn_vect_itor_t i;
  puk_vect_foreach(i, puk_component_conn, &context->conns)
    {
      if(((i->receptacle->iface == iface) || (iface == NULL)) &&
         ((label == NULL) ||
          (strcmp(i->receptacle->label, label) == 0) ||
          (strcmp(i->receptacle->iface->name, label) == 0)))
        return i;
    }
  return NULL;
}

/** connect a receptacle in the given context */
void puk_context_conn_connect(puk_context_t user, const char*port_name, const char*provider_id, puk_iface_t iface, const char*provider_port)
{
  const puk_component_t composite = user->assembly;
  if(!composite)
    padico_fatal("invalid component context- not inside a composite.\n");
  struct puk_composite_content_s*content = puk_composite_get(composite);
  assert(content != NULL);
  puk_context_t provider = puk_hashtable_lookup(content->contexts, provider_id);
  if(provider)
    {
      const puk_facet_t facet = puk_component_get_facet(provider->component, iface, provider_port);
      assert(facet != NULL);
      puk_component_conn_t conn = puk_context_conn_lookup(user, iface, port_name);
      assert(conn != NULL);
      conn->context = provider;
      conn->facet = facet;
    }
  else
    {
      padico_warning("invalid 'uses'- context '%s' does not exist.\n", provider_id);
    }
}

puk_context_t puk_context_getbyid(puk_component_t component, const char*id)
{
  const struct puk_composite_driver_s*const composite_driver =
    puk_component_get_driver_PadicoComposite(component, NULL);
  if(composite_driver)
    {
      puk_context_t context = puk_hashtable_lookup(composite_driver->content->contexts, id);
      return context;
    }
  else
    {
      return NULL;
    }
}

puk_context_t puk_component_get_context(puk_component_t component, puk_iface_t iface, const char*label)
{
  puk_context_t context = NULL;
  const struct puk_composite_driver_s*composite_driver =
    puk_component_get_driver_PadicoComposite(component, NULL);
  if(composite_driver)
    {
      struct puk_composite_content_s*content = composite_driver->content;
      puk_component_conn_vect_itor_t conn;
      puk_vect_foreach(conn, puk_component_conn, &content->entry_points)
        {
          if( ((iface == NULL) || (conn->facet->iface == iface)) &&
              ((label == NULL) || (strcmp(conn->facet->label, label) == 0)))
            {
              context = conn->context;
              break;
            }
        }
    }
  return context;
}

puk_context_t puk_context_get_subcontext(puk_context_t context, puk_iface_t iface, const char*label)
{
  puk_component_conn_t conn = puk_context_conn_lookup(context, iface, label);
  if(conn)
    {
      return conn->context;
    }
  else
    {
      padico_warning("cannot find subcontext iface = %p; label = %s\n", iface, label);
      return NULL;
    }
}

puk_context_vect_t puk_component_get_contexts(puk_component_t assembly)
{
  /* convert hashtable into unsorted vector */
  struct puk_composite_content_s*content = puk_composite_get(assembly);
  puk_hashtable_enumerator_t h = puk_hashtable_enumerator_new(content->contexts);
  puk_context_vect_t contexts = puk_context_vect_new();
  puk_context_t c = puk_hashtable_enumerator_next_data(h);
  while(c)
    {
      assert(c->assembly == assembly);
      padico_out(60, "adding context id=%s; component=%s; assembly=%s\n", c->id, c->component->name, c->assembly->name);
      puk_context_vect_push_back(contexts, c);
      c = puk_hashtable_enumerator_next_data(h);
    }
  puk_hashtable_enumerator_delete(h);
  return contexts;
}


void puk_context_indirect(puk_instance_t instance,
                          puk_iface_t iface,
                          const char*label,
                          void*_driver, void *_status)
{
  const puk_component_conn_t entry_point = puk_context_conn_lookup(instance->context, iface, label);
  assert(entry_point != NULL);
  if(_driver)
    {
      *(const void**)_driver = entry_point->facet->driver;
    }
  if(_status)
    {
      assert(puk_composite_get(instance->container->component) != NULL);
      const struct puk_composite_status_s*composite_status = instance->container->status;
      puk_instance_t sub_instance = puk_hashtable_lookup(composite_status->instance_table, entry_point->context);
      puk_instance_entry_point(sub_instance, iface, entry_point->facet->label, NULL, _status);
    }
}

/** create a new empty context attached to a component */
puk_context_t puk_context_new(puk_component_t component, puk_component_t assembly, const char*context_id)
{
  puk_context_t context = padico_malloc(sizeof(struct puk_component_context_s));
  context->component = component;
  context->attrs     = NULL;
  context->assembly  = assembly;
  context->status    = NULL;
  /* compute context ID */
  if(context_id == NULL)
    {
      padico_string_t sid = padico_string_new();
      padico_string_printf(sid, "_puk:context-%s-(%p)", component->name, context);
      context->id = padico_strdup(padico_string_get(sid));
      padico_string_delete(sid);
    }
  else
    {
      context->id = padico_strdup(context_id);
    }
  puk_component_conn_vect_init(&context->conns);
  /* instantiate receptacles */
  puk_uses_vect_itor_t i;
  puk_vect_foreach(i, puk_uses, &component->uses)
    {
      struct puk_component_conn_s uses =
        { .receptacle = i, .context = NULL, .facet = NULL };
      puk_component_conn_vect_push_back(&context->conns, uses);
    }
  /* instanciate attributes to default value */
  if(component->attrs)
    {
      puk_attr_hashtable_enumerator_t e = puk_attr_hashtable_enumerator_new(context->component->attrs);
      char*label = NULL;
      char*value = NULL;
      puk_attr_hashtable_enumerator_next2(e, &label, &value);
      while(label)
        {
          puk_context_putattr(context, label, value);
          puk_attr_hashtable_enumerator_next2(e, &label, &value);
        }
      puk_attr_hashtable_enumerator_delete(e);
    }
  /* insert context in assembly */
  if(assembly != NULL)
    {
      puk_hashtable_insert(puk_composite_get(assembly)->contexts, context->id, context);
    }
  return context;
}

void puk_context_destroy(puk_context_t context)
{
  if(context->attrs)
    {
      puk_attr_hashtable_delete(context->attrs);
    }
  puk_component_conn_vect_destroy(&context->conns);
  padico_free((char*)context->id);
  padico_free(context);
}

puk_component_t puk_composite_new(const char*id)
{
  puk_component_t composite = puk_component_new(id, NULL);
  const struct puk_facet_s component_facet =
    {
      .iface  = puk_iface_PadicoComponent(),
      .driver = &composite_component_driver,
      .label  = NULL
    };
  puk_facet_vect_push_back(&composite->provides, component_facet);
  struct puk_composite_content_s*composite_content = padico_malloc(sizeof(struct puk_composite_content_s));
  composite_content->driver.content = composite_content;
  const struct puk_facet_s composite_facet =
    {
      .iface  = puk_iface_PadicoComposite(),
      .driver = &composite_content->driver,
      .label  = NULL
    };
  puk_facet_vect_push_back(&composite->provides, composite_facet);
  puk_component_conn_vect_init(&composite_content->entry_points);
  composite_content->contexts = puk_hashtable_new_string();
  composite_content->checked = 0;
  padico_out(40, "new composite- id=%s\n", id);
  return composite;
}

void puk_composite_add_entrypoint(puk_component_t composite, puk_iface_t iface, const char*port_name,
                                  const char*provider_port, const char*provider_id)
{
  struct puk_composite_content_s*content = puk_composite_get(composite);
  if(composite)
    {
      puk_context_t context = puk_hashtable_lookup(content->contexts, provider_id);
      if(context)
        {
          const puk_facet_t provider_facet = puk_component_get_facet(context->component, iface, provider_port);
          if(provider_facet == NULL || context == NULL)
            {
              padico_fatal("cannot find entry-point with type=%s; name=%s in composite %s\n",
                           iface->name, port_name, composite->name);
            }
          struct puk_uses_s*receptacle = padico_malloc(sizeof(struct puk_uses_s));
          receptacle->iface = iface;
          receptacle->label = padico_strdup(port_name?:iface->name);
          const struct puk_component_conn_s entry_point =
            {
              .receptacle = receptacle,
              .context    = context,
              .facet      = provider_facet
            };
          puk_component_conn_vect_push_back(&content->entry_points, entry_point);
          const struct puk_facet_s facet =
            {
              .iface  = iface,
              .driver = provider_facet->driver,
              .label  = padico_strdup(port_name?:iface->name)
            };
          puk_facet_vect_push_back(&composite->provides, facet);
        }
      else
        {
          padico_warning("no context for provider '%s' in composite '%s'\n",
                         provider_id, composite->name);
          abort();
        }
    }
  else
    {
      padico_warning("trying to add an entry point '%s' to component '%s' which is not a composite.\n",
                     port_name, composite->name);
    }
}

puk_component_t puk_component_encapsulate(puk_component_t component, const char*id)
{
  puk_component_t composite = puk_composite_new(id);
  puk_context_t context = puk_context_new(component, composite, NULL);
  puk_facet_vect_itor_t f;
  puk_vect_foreach(f, puk_facet, &component->provides)
    {
      if(f->iface != puk_iface_PadicoComponent() &&
         f->iface != puk_iface_PadicoComposite())
        {
          puk_composite_add_entrypoint(composite, f->iface, f->label, f->label, context->id);
        }
    }
  return composite;
}

puk_component_t puk_composite_duplicate(puk_component_t component, const char*id)
{
  padico_string_t s_component = puk_component_serialize_id(component, id);
  puk_component_t composite = puk_component_parse(padico_string_get(s_component));
  padico_string_delete(s_component);
  return composite;
}

puk_component_t puk_compositize(puk_component_t component, const char*id)
{
  const struct puk_composite_driver_s*const driver =
    puk_component_get_driver_PadicoComposite(component, NULL);
  if(driver)
    {
      return puk_composite_duplicate(component, id);
    }
  else
    {
      return puk_component_encapsulate(component, id);
    }
}


/* ********************************************************* */
/* *** Composite ******************************************* */

static void*puk_composite_instantiate(puk_instance_t instance, puk_context_t in_context)
{
  struct puk_composite_status_s*status = padico_malloc(sizeof(struct puk_composite_status_s));
  instance->status = status;
  if(in_context)
    {
      padico_warning("instances inside an instance.\n");
    }
  status->instance_table = puk_hashtable_new_ptr();
  status->instance = instance;
  puk_context_t context = NULL;
  puk_component_enumerator_t e = puk_component_enumerator_new(instance->component, BOTTOM_UP);
  for(context  = puk_component_enumerator_next(e);
      context != NULL;
      context  = puk_component_enumerator_next(e))
    {
      puk_instance_t sub_instance = puk_component_instantiate_internal(context->component, instance, context, instance->owner);
      puk_hashtable_insert(status->instance_table, context, sub_instance);
    }
  puk_component_enumerator_delete(e);
  return status;
}

static void puk_composite_destroy(void*_status)
{
  struct puk_composite_status_s*composite_status = _status;
  puk_component_enumerator_t component_enum =
    puk_component_enumerator_new(composite_status->instance->component, TOP_DOWN);
  puk_context_t context = puk_component_enumerator_next(component_enum);
  while(context)
    {
      puk_hashtable_enumerator_t e = puk_hashtable_enumerator_new(composite_status->instance_table);
      puk_instance_t sub_instance = NULL;
      puk_context_t sub_context = NULL;
      puk_hashtable_enumerator_next2(e, &sub_context, &sub_instance);
      while(sub_instance)
        {
          if(sub_context == context)
            {
              puk_instance_destroy(sub_instance);
              break;
            }
          puk_hashtable_enumerator_next2(e, &sub_context, &sub_instance);
        }
      puk_hashtable_enumerator_delete(e);
      context = puk_component_enumerator_next(component_enum);
    }
  puk_component_enumerator_delete(component_enum);
  puk_hashtable_delete(composite_status->instance_table, NULL);
  padico_free(composite_status);
}

/* ********************************************************* */
/* *** Component instance ********************************** */

puk_instance_t puk_component_instantiate_internal(puk_component_t component, puk_instance_t container,
                                                  puk_context_t context, puk_mod_t owner)
{
  const struct puk_component_driver_s*driver = puk_component_get_driver_PadicoComponent(component, NULL);
  puk_instance_t instance = padico_malloc(sizeof(struct puk_instance_s));
  assert((context == NULL) || (context->component == component));
  if(component->home)
    {
      /* TODO- manage own_instances count by component, not by home */
      if(component->home == owner)
        component->home->own_instances++;
      else
        component->home->managed_instances++;
    }
  instance->component = component;
  instance->status    = NULL;
  instance->container = container;
  instance->context   = context;
  instance->owner     = owner;
  if(driver && driver->instantiate)
    {
      instance->status = (*driver->instantiate)(instance, context);
    }
  return instance;
}

void puk_instance_destroy(puk_instance_t instance)
{
  if(instance)
    {
      const struct puk_component_driver_s*driver =
        puk_component_get_driver_PadicoComponent(instance->component, NULL);
      if(driver && driver->destroy)
        {
          (*driver->destroy)(instance->status);
        }
      if(instance->component->home)
        {
          if(instance->component->home == instance->owner)
            instance->component->home->own_instances--;
          else
            instance->component->home->managed_instances--;
        }
      padico_free(instance);
    }
}

void puk_instance_entry_point(puk_instance_t instance,
                              puk_iface_t iface,
                              const char*label,
                              void*_driver, void*_status)
{
  const puk_component_t component = instance->component;
  puk_facet_t f = puk_component_get_facet(component, iface, label);
  if(f != NULL)
    {
      if(_driver)
        {
          *((const void**)_driver) = f->driver;
        }
      if(_status)
        {
          const struct puk_composite_driver_s*composite_driver =
            puk_component_get_driver_PadicoComposite(component, NULL);
          if(composite_driver)
            {
              struct puk_composite_content_s*content = composite_driver->content;
              const struct puk_composite_status_s*composite_status = instance->status;
              puk_component_conn_vect_itor_t i;
              puk_vect_foreach(i, puk_component_conn, &content->entry_points)
                {
                  puk_instance_t sub_instance =
                    puk_hashtable_lookup(composite_status->instance_table, i->context);
                  puk_instance_entry_point(sub_instance, iface, label, NULL, _status);
                  if(*(void**)_status)
                    break;
                }
            }
          else
            {
              *((void**)_status) = instance->status;
            }
        }
    }
  else
    {
      if(_driver)
        *((const void**)_driver) = NULL;
      if(_status)
        *((void**)_status) = NULL;
    }
}

puk_instance_t puk_instance_find_context(puk_instance_t container, puk_instance_t local)
{
  if((container->component == local->component) && (container->context == local->context))
    return container;
  else
    {
      const struct puk_composite_driver_s*const composite_driver =
        puk_component_get_driver_PadicoComposite(container->component, NULL);
      if(composite_driver)
        {
          struct puk_composite_content_s*content = composite_driver->content;
          const struct puk_composite_status_s*composite_status = container->status;
          puk_component_conn_vect_itor_t i;
          puk_vect_foreach(i, puk_component_conn, &content->entry_points)
            {
              puk_instance_t sub_instance =
                puk_hashtable_lookup(composite_status->instance_table, i->context);
              puk_instance_t instance = puk_instance_find_context(sub_instance, local);
              if(instance)
                return instance;
            }
        }
    }
  return NULL;
}

const char*puk_instance_getattr(puk_instance_t instance, const char*label)
{
  const char*attr = (instance->context) ?
    puk_context_getattr(instance->context, label) :
    (instance->component->attrs ?
     (const char*)puk_attr_hashtable_lookup(instance->component->attrs, label) : NULL);
  return attr;
}


/* ********************************************************* */
/* *** Assembly marshalling ******************************** */

padico_string_t puk_component_serialize(puk_component_t component)
{
  padico_string_t name = padico_string_new();
  padico_string_catf(name, "_puk:_x(%p):%s", component, component->name);
  padico_string_t s_component = puk_component_serialize_id(component, padico_string_get(name));
  padico_string_delete(name);
  return s_component;
}

padico_string_t puk_component_serialize_id(puk_component_t component, const char*id)
{
  padico_string_t s = padico_string_new();
  if(puk_component_get_driver_PadicoComposite(component, NULL))
    {
      padico_string_catf(s, "<puk:composite id=\"%s\">\n", id);
      /* components */
      puk_component_enumerator_t components = puk_component_enumerator_new(component, BOTTOM_UP);
      puk_context_t context = puk_component_enumerator_next(components);
      puk_component_conn_vect_itor_t e;
      while(context)
        {
          padico_string_catf(s, "  <puk:component name=\"%s\" id=\"%p\">\n",
                             context->component->name, context);
          /* receptacles (uses) */
          puk_vect_foreach(e, puk_component_conn, &context->conns)
            {
              if(e->context != NULL)
                {
                  padico_string_catf(s, "    <puk:uses iface=\"%s\" port=\"%s\" provider-id=\"%p\" provider-port=\"%s\" />\n",
                                     e->receptacle->iface->name, e->receptacle->label,
                                     e->context, e->facet->label);
                }
            }
          /* attributes */
          if(context->attrs)
            {
              puk_attr_hashtable_enumerator_t attrs = puk_attr_hashtable_enumerator_new(context->attrs);
              char*label, *value;
              puk_attr_hashtable_enumerator_next2(attrs, &label, &value);
              while(label)
                {
                  padico_string_catf(s, "    <puk:attr label=\"%s\">%s</puk:attr>\n",
                                     label, (value == NULL) ? "" : value);
                  puk_attr_hashtable_enumerator_next2(attrs, &label, &value);
                }
              puk_attr_hashtable_enumerator_delete(attrs);
            }
          padico_string_catf(s, "  </puk:component>\n");
          context = puk_component_enumerator_next(components);
        }
      puk_component_enumerator_delete(components);
      /* exported facets */
      struct puk_composite_content_s*content = puk_composite_get(component);
      puk_vect_foreach(e, puk_component_conn, &content->entry_points)
        {
          padico_string_catf(s, "  <puk:entry-point iface=\"%s\" port=\"%s\" provider-id=\"%p\" provider-port=\"%s\"/>\n",
                             e->receptacle->iface->name, e->receptacle->label,
                             e->context, e->facet->label);
        }
      padico_string_catf(s, "</puk:composite>\n");
    }
  else
    {
      padico_string_catf(s, "<puk:component name=\"%s\"/>\n", component->name);
    }
  return s;
}

puk_component_t puk_component_parse(const char*s)
{
  puk_component_t component = NULL;
  struct puk_parse_entity_s e = puk_xml_parse_buffer(s, strlen(s), PUK_TRUST_LOCAL);
  if(puk_parse_is(&e, "puk:composite") || puk_parse_is(&e, "puk:component"))
    {
      component = puk_parse_get_content(&e);
    }
  else
    {
      padico_fatal("error while parsing *%s*: not a component.\n", s);
    }
  return component;
}

puk_component_t puk_component_parse_file(const char*filename)
{
  puk_component_t component = NULL;
  struct puk_parse_entity_s e = puk_xml_parse_file(filename, PUK_TRUST_LOCAL);
  padico_rc_t rc = puk_parse_get_rc(&e);
  if(padico_rc_iserror(rc))
    {
      padico_fatal("error while parsing file %s: *%s*.\n",
                   filename, padico_rc_gettext(rc));
    }
  else if(puk_parse_is(&e, "puk:composite") || puk_parse_is(&e, "puk:component"))
    {
      component = puk_parse_get_content(&e);
    }
  else
    {
      padico_fatal("error while parsing file %s: not a component (%s).\n", filename, e.tag);
    }
  return component;
}


/* ********************************************************* */
/* *** Assembly parsing ************************************ */


/* *** <puk:composite> */
static void puk_composite_start_handler(puk_parse_entity_t e)
{
  const char*id  = puk_parse_getattr(e, "id");
  const char*ref = puk_parse_getattr(e, "ref");
  puk_component_t composite = NULL;
  assert(!ref != !id);
  if(ref)
    {
      composite = puk_component_resolve(ref);
    }
  else
    {
      composite = puk_composite_new(id);
    }
  puk_parse_set_content(e, composite);
}
static void puk_composite_end_handler(puk_parse_entity_t e)
{
  puk_component_t composite = puk_parse_get_content(e);
  if(composite)
    {
      const struct puk_composite_driver_s*const composite_driver =
        puk_component_get_driver_PadicoComposite(composite, NULL);
      if(composite_driver)
        {
          struct puk_composite_content_s*content = composite_driver->content;
          if(puk_hashtable_size(content->contexts) == 0 ||
             puk_component_conn_vect_size(&content->entry_points) == 0)
            {
              padico_warning("invalid composite '%s'- skipping.\n", composite->name);
              puk_component_destroy(composite);
              composite = NULL;
            }
        }
    }
  puk_parse_set_content(e, composite);
}

/* *** <puk:component> */
static void puk_component_start_handler(puk_parse_entity_t e)
{
  const char*component_name = puk_parse_getattr(e, "name");
  const char*context_id = puk_parse_getattr(e, "id");
  const char*ref = puk_parse_getattr(e, "ref");
  puk_component_t component = NULL;
  if(ref)
    component = puk_component_resolve(ref);
  if(component_name)
    component = puk_component_resolve(component_name);
  if(component == NULL)
    {
      padico_warning("component '%s' not found- skipping.\n", component_name);
      puk_parse_set_content(e, NULL);
    }
  else
    {
      puk_component_t assembly = puk_parse_get_content(puk_parse_parent(e));
      puk_context_t context = puk_context_new(component, assembly, context_id);
      padico_out(40, "created component name=%s; id=%s\n", component_name, context_id);
      puk_parse_set_content(e, context);
    }
}
static void puk_component_end_handler(puk_parse_entity_t e)
{
  puk_context_t context = puk_parse_get_content(e);
  if(context)
    {
      /* check receptacles */
      puk_component_conn_vect_itor_t u;
      padico_rc_t rc = padico_rc_ok();
      puk_vect_foreach(u, puk_component_conn, &context->conns)
        {
          if(!u->facet)
            {
              rc = padico_rc_error("Receptacle '%s' not connected in context '%s'.\n",
                                   u->receptacle->iface->name, context->component->name);
              padico_warning("receptacle '%s' not connected in context '%s' (%s).\n",
                             u->receptacle->iface->name, context->id, context->component->name);
            }
        }
      /* check attributes */
      if(context->component->attrs)
        {
          puk_attr_hashtable_enumerator_t e = puk_attr_hashtable_enumerator_new(context->component->attrs);
          char*label = NULL;
          char*value = NULL;
          puk_attr_hashtable_enumerator_next2(e, &label, &value);
          while(label)
            {
              if((!context->attrs) || (context->attrs && !puk_attr_hashtable_probe(context->attrs, label)))
                {
                  if(value != NULL)
                    {
                      /* take the default value */
                      puk_context_putattr(context, label, value);
                    }
                  else
                    {
                      rc = padico_rc_error("Attribute '%s' has no value in context '%s'.\n",
                                           label, context->component->name);
                      padico_warning("attribute '%s' has no value in context '%s'.\n",
                                     label, context->component->name);
                    }
                }
              puk_attr_hashtable_enumerator_next2(e, &label, &value);
            }
          puk_attr_hashtable_enumerator_delete(e);
        }
      puk_parse_set_rc(e, rc);
    }
}

/* *** <puk:attr> */
static void puk_attr_end_handler(puk_parse_entity_t e)
{
  const char*label = puk_parse_getattr(e, "label");
  const char*value = puk_parse_get_text(e);
  if(!puk_parse_is(puk_parse_parent(e), "puk:component"))
    padico_fatal("bad content in parse entity for component: %s\n", puk_parse_parent(e)->tag);
  puk_context_t context = puk_parse_get_content(puk_parse_parent(e));
  if(context)
    {
      assert(context && label);
      puk_context_putattr(context, label, value);
      padico_out(40, "created attr %s\n", label);
    }
}

/* *** <puk:uses> */
static void puk_uses_start_handler(puk_parse_entity_t e)
{
  const char*iface_name    = puk_parse_getattr(e, "iface");
  puk_iface_t iface        = puk_iface_register(iface_name);
  const char*port_name     = puk_parse_getattr(e, "port");
  const char*provider_port = puk_parse_getattr(e, "provider-port");
  const char*provider_id   = puk_parse_getattr(e, "provider-id");
  if(!iface)
    padico_fatal("invalid 'uses'- interface '%s' does not exist.\n", iface_name);
  puk_context_t user = puk_parse_get_content(puk_parse_parent(e));
  if(user)
    {
      puk_context_conn_connect(user, port_name, provider_id, iface, provider_port);
    }
}

/* *** <puk:entry-point> */
static void puk_entrypoint_start_handler(puk_parse_entity_t e)
{
  const char*iface_name    = puk_parse_getattr(e, "iface");
  puk_iface_t iface        = puk_iface_register(iface_name);
  const char*port_name     = puk_parse_getattr(e, "port");
  const char*provider_port = puk_parse_getattr(e, "provider-port");
  const char*provider_id   = puk_parse_getattr(e, "provider-id");
  if(!iface)
    padico_fatal("invalid 'entry_point'- interface '%s' does not exist.\n", iface_name);
  puk_component_t composite = puk_parse_get_content(puk_parse_parent(e));
  puk_composite_add_entrypoint(composite, iface, port_name, provider_port, provider_id);
}


/* ********************************************************* */
/* *** Enumerator ****************************************** */

struct puk_component_enumerator_s
{
  puk_context_vect_t components;
  puk_context_vect_itor_t current;
  enum component_enumerator_way_e way;
};

puk_context_t puk_component_enumerator_next(puk_component_enumerator_t e)
{
  puk_context_t a = NULL;
  switch(e->way)
    {
    case BOTTOM_UP:
      if(e->current != puk_context_vect_end(e->components))
        {
          a = *e->current;
          e->current = puk_context_vect_next(e->current);
        }
      else
        {
          e->current = NULL;
          a = NULL;
        }
      break;
    case TOP_DOWN:
      if(e->current != puk_context_vect_rend(e->components))
        {
          a = *e->current;
          e->current = puk_context_vect_rnext(e->current);
        }
      else
        {
          e->current = NULL;
          a = NULL;
        }
      break;
    }
  return a;
}

void puk_component_enumerator_delete(puk_component_enumerator_t e)
{
  puk_context_vect_delete(e->components);
  padico_free(e);
}

puk_component_enumerator_t
puk_component_enumerator_new(puk_component_t assembly, int way)
{
  puk_component_enumerator_t e = padico_malloc(sizeof(struct puk_component_enumerator_s));
  e->way      = way;
  e->components = puk_context_vect_new();
  e->current  = NULL;
  puk_context_vect_t unsorted = puk_component_get_contexts(assembly);

  /* topological sorting based on zero-in method (strictly speaking,
   * this is implemented as zero-out rather than zero-in) */
  const int degree = puk_context_vect_size(unsorted);
  int*matrix = padico_calloc(degree*degree, sizeof(int));
  int*done   = padico_calloc(degree, sizeof(int));
  /* matrix[x*degree + y] == 1 ~~ "x uses y" */
  int i, j, k;
  puk_context_t a_i, a_j;
  assert(degree > 0);
  /* convert the assembly into an adjacency matrix */
  for(i = 0; i < degree; i++)
    {
      a_i = puk_context_vect_at(unsorted, i);
      padico_out(60, "scanning dependancies of context i=%s\n", a_i->id);
      for(k = 0; k < puk_component_conn_vect_size(&a_i->conns); k++)
        {
          for(j = 0; j < degree; j++)
            {
              a_j = puk_context_vect_at(unsorted, j);
              if(puk_component_conn_vect_ptr(&a_i->conns, k)->context == a_j)
                {
                  padico_out(60, "context i=%s uses context j=%s\n", a_i->id, a_j->id);
                  matrix[i*degree+j] = 1;
                }
            }
        }
    }
  for(k = 0; k < degree; k++)
    {
      for(i = 0; i < degree; i++)
        {
          /* find a "zero-out" component- trying context 'i' */
          padico_out(50, "searching for zero-out- k=%d; i=%d\n", k, i);
          if(done[i])
            {
              padico_out(50, "i=%d is already done- skipping.\n", i);
              goto next_i;
            }
          for(j = 0; j < degree; j++)
            {
              if(matrix[i*degree+j])
                {
                  padico_out(50, "i=%d is not zero-out- has out for j=%d\n", i, j);
                  goto next_i;
                }
            }
          padico_out(50, "i=%d is zero-out- clearing dependancies\n", i);
          done[i] = 1;
          /* clear all dependancies to context 'i' */
          puk_context_t context = puk_context_vect_at(unsorted, i);
          puk_context_vect_push_back(e->components, context);
          for(j = 0; j < degree; j++)
            {
              matrix[j*degree+i] = 0;
            }
          goto next_k;
        next_i:
          continue;
        }
    next_k:
      continue;
    }
  padico_free(matrix);
  padico_free(done);
  puk_context_vect_delete(unsorted);
  switch(way)
    {
    case BOTTOM_UP:
      e->current = puk_context_vect_begin(e->components);
      break;
    case TOP_DOWN:
      e->current = puk_context_vect_rbegin(e->components);
      break;
    }
  return e;
}

/*
 * Padico -- a High Performance Parallel and Distributed Computing Environment
 * Copyright (c) 2002-2023 INRIA and the University of Rennes 1
 * Alexandre DENIS <Alexandre.Denis@inria.fr>
 * Christian PEREZ <Christian.Perez@inria.fr>
 *
 * The software has been registered at the Agency for the Protection of
 * Programs (APP) under the number IDDN.FR.001.260013.000.S.P.2002.000.10000.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/** @file
 * @brief Puk hashtables
 * @ingroup Puk
 */

#include "Puk-internals.h"

PADICO_MODULE_HOOK(Puk);

static const int puk_hash_primes[] = {
  11, 23, 53, 97, 193, 389, 769, 1543, 3079, 6151, 12289, 24593, 49157, 98317,
  196613, 393241, 786433, 1572869, 3145739, 6291469, 12582917, 25165843,
  50331653, 100663319, 201326611, 402653189, 805306457, -1 /*, 16106212741 */
};


static void puk_hashtable_rehash(puk_hashtable_t h);
static inline int puk_hash_entry_oldprobe(puk_hashtable_t h, const void*key);
static inline struct puk_hash_entry_s*puk_hash_entry_lookup(puk_hashtable_t h, const void*key);
static inline void puk_hash_entry_insert(puk_hashtable_t h, const void*key, void*data);
static inline void puk_hash_entry_rehash_row(puk_hashtable_t h, int z);
static inline void puk_hash_entry_rehash_n(puk_hashtable_t h, int n);
static inline void puk_hash_entry_rehash_flush(puk_hashtable_t h);

/** stores an entry in the hashtable */
struct puk_hash_entry_s
{
  const void*key;               /**< key to index the entry- must be non-NULL */
  void*data;                    /**< data value associated with key */
  struct puk_hash_entry_s*next; /**< next entry in the same cell, in case of hash collision */
};

/** content of a hashtable */
struct puk_hashtable_s
{
  struct puk_hash_entry_s*table; /**< table to store entries */
  ssize_t size;                  /**< size of above table */
  ssize_t load;                  /**< number of elements stored in the table */
  puk_hash_hfunc_t hash_func;    /**< function to compute hash */
  puk_hash_eqfunc_t eq_func;     /**< function to test for equality */
  ssize_t rehashed;
  struct puk_hash_entry_s*oldtable;
  ssize_t oldsize;
};

PUK_ALLOCATOR_TYPE(puk_hashtable_entry, struct puk_hash_entry_s);
static puk_hashtable_entry_allocator_t puk_hashtable_entry_allocator = NULL;

static void puk_hashtable_finalizer(void) __attribute__((destructor));
static void puk_hashtable_finalizer(void)
{
  if(puk_hashtable_entry_allocator != NULL)
    {
      puk_hashtable_entry_allocator_delete(puk_hashtable_entry_allocator);
      puk_hashtable_entry_allocator = NULL;
    }
}

void puk_hashtable_enumerator_init(puk_hashtable_enumerator_t e, puk_hashtable_t h)
{
  puk_hash_entry_rehash_flush(h);
  e->h = h;
  e->i = -1;
  e->current = NULL;
}
void puk_hashtable_enumerator_next2(puk_hashtable_enumerator_t e, void*key, void*data)
{
  assert(e->h->oldtable == NULL);
  if(e->current && e->current->next)
    {
      /* bump one bucket */
      e->current = e->current->next;
    }
  else
    {
      do
        {
          e->i++;
        }
      while(e->i < 0 || (e->i < e->h->size && !e->h->table[e->i].key));
      e->current = e->h->table + e->i;
    }
  if(e->i < e->h->size)
    {
      if(key)
        *(const void**)key = e->current->key;
      if(data)
        *(void**)data = e->current->data;
    }
  else
    {
      if(key)
        *(const void**)key = NULL;
      if(data)
        *(void**)data = NULL;
    }
}



/** lookup in the oldtable; if found, migrate row and return cell in regular table */
static inline int puk_hash_entry_oldprobe(puk_hashtable_t h, const void*key)
{
  if(h->oldtable)
    {
      const uint32_t z = (*h->hash_func)(key);
      const int index = puk_hash_to_index(z, h->oldsize);
      struct puk_hash_entry_s*e = h->oldtable + index;
      while(e && e->key)
        {
          if((*h->eq_func)(key, e->key))
            {
              puk_hash_entry_rehash_row(h, index); /* rehash row of the considered key */
              puk_hash_entry_rehash_n(h, PUK_HASH_PROGRESSIVE_REHASH); /* make progressive rehash progress */
              return 1;
            }
          e = e->next;
        }
    }
  return 0;
}

static inline struct puk_hash_entry_s*puk_hash_entry_lookup(puk_hashtable_t h, const void*key)
{
  const uint32_t z = (*h->hash_func)(key);
  struct puk_hash_entry_s*e = NULL;
  do
    {
      e = h->table + puk_hash_to_index(z, h->size);
      while(e && e->key)
        {
          if((*h->eq_func)(key, e->key))
            {
              return e;
            }
          e = e->next;
        }
    }
  while(puk_hash_entry_oldprobe(h, key));
  return NULL;
}

static inline void puk_hash_entry_insert(puk_hashtable_t h, const void*key, void*data)
{
  const uint32_t z = (*h->hash_func)(key);
  struct puk_hash_entry_s*e = h->table + puk_hash_to_index(z, h->size);
  if(e->key)
    {
      while(e->next)
        {
          e = e->next;
        }
      e->next = puk_hashtable_entry_malloc(puk_hashtable_entry_allocator);
      e = e->next;
    }
  e->key  = key;
  e->data = data;
  e->next = NULL;
  h->load++;
}

/** force rehashing of given row */
static inline void puk_hash_entry_rehash_row(puk_hashtable_t h, int z)
{
  assert(h->oldtable != NULL);
  struct puk_hash_entry_s*source = h->oldtable + z;
  if(source->key)
    {
      puk_hash_entry_insert(h, source->key, source->data);
      source->key = NULL;
      int first = 1;
      while(source->next)
        {
          struct puk_hash_entry_s*n = source->next;
          puk_hash_entry_insert(h, n->key, n->data);
          n->key = NULL;
          if(!first)
            puk_hashtable_entry_free(puk_hashtable_entry_allocator, source);
          source = n;
          first = 0;
        }
    }
}

/** make progress on rehash for n rows */
static inline void puk_hash_entry_rehash_n(puk_hashtable_t h, int n)
{
  if(h->oldtable)
    {
      int k;
      for(k = 0; k < n; k++)
        {
          puk_hash_entry_rehash_row(h, h->rehashed + k);
          if(h->rehashed + k == h->oldsize - 1)
            {
              padico_free(h->oldtable);
              h->oldtable = NULL;
              h->rehashed = 0;
              return;
            }
        }
      h->rehashed += k;
    }
}

/** flush a pending rehash */
static inline void puk_hash_entry_rehash_flush(puk_hashtable_t h)
{
  if(h->oldtable)
    {
      puk_hash_entry_rehash_n(h, h->oldsize);
    }
  assert(h->oldtable == NULL);
}

puk_hashtable_t puk_hashtable_new(puk_hash_hfunc_t hash_func, puk_hash_eqfunc_t eq_func)
{
  puk_hashtable_t h = padico_malloc(sizeof(struct puk_hashtable_s));
  h->size = puk_hash_primes[0];
  h->load = 0;
  h->hash_func = hash_func;
  h->eq_func = eq_func;
  h->table = padico_calloc(h->size, sizeof(struct puk_hash_entry_s));
  h->oldtable = NULL;
  if(puk_hashtable_entry_allocator == NULL)
    {
      puk_hashtable_entry_allocator = puk_hashtable_entry_allocator_new(puk_hash_primes[0]);
    }
  return h;
}

void puk_hashtable_delete(puk_hashtable_t h, void (*destructor)(void*key, void*data))
{
  puk_hash_entry_rehash_flush(h);
  int i;
  for(i = 0; i < h->size; i++)
    {
      struct puk_hash_entry_s*e = h->table + i;
      if(e->key && destructor)
        {
          (*destructor)((void*)e->key, e->data);
        }
      e = e->next;
      while(e)
        {
          struct puk_hash_entry_s*n = e->next;
          if(destructor)
            {
              (*destructor)((void*)e->key, e->data);
            }
          puk_hashtable_entry_free(puk_hashtable_entry_allocator, e);
          e = n;
        }
    }
  padico_free(h->table);
  padico_free(h);
}

void puk_hashtable_insert(puk_hashtable_t h, const void*key, void*data)
{
  puk_hash_entry_rehash_n(h, PUK_HASH_PROGRESSIVE_REHASH);
  assert(key != NULL);
  struct puk_hash_entry_s*e = puk_hash_entry_lookup(h, key);
  if(e)
    {
      e->key  = key;
      e->data = data;
    }
  else
    {
      puk_hash_entry_insert(h, key, data);
    }
  if(h->load >= h->size * 3 / 4)
    {
      puk_hashtable_rehash(h);
    }
}

void*puk_hashtable_lookup(puk_hashtable_t h, const void*key)
{
  struct puk_hash_entry_s*e = puk_hash_entry_lookup(h, key);
  return e ? e->data : NULL;
}

int puk_hashtable_probe(puk_hashtable_t h, const void*key)
{
  struct puk_hash_entry_s*e = puk_hash_entry_lookup(h, key);
  return (e != NULL);
}

void puk_hashtable_lookup2(puk_hashtable_t h, const void*key, void*outkey, void*data)
{
  struct puk_hash_entry_s*e = puk_hash_entry_lookup(h, key);
  if(e != NULL)
    {
      if(outkey)
        *(void**)outkey = (void*)e->key;
      if(data)
        *(void**)data = e->data;
    }
  else
    {
      if(outkey)
        *(void**)outkey = NULL;
      if(data)
        *(void**)data = NULL;
    }
}

void puk_hashtable_remove(puk_hashtable_t h, const void*key)
{
  assert(key != NULL);
  const uint32_t z = (*h->hash_func)(key);
  struct puk_hash_entry_s*e = NULL;
  struct puk_hash_entry_s*prev = NULL;
  do
    {
      e = h->table + puk_hash_to_index(z, h->size);
      prev = NULL;
      while(e && e->key)
        {
          if((*h->eq_func)(key, e->key))
            {
              if(e->next)
                {
                  struct puk_hash_entry_s*next_e = e->next;
                  *e = *next_e;
                  puk_hashtable_entry_free(puk_hashtable_entry_allocator, next_e);
                  e = e->next;
                }
              else
                {
                  if(prev)
                    {
                      assert(prev->next == e);
                      prev->next = NULL;
                      puk_hashtable_entry_free(puk_hashtable_entry_allocator, e);
                    }
                  else
                    {
                      e->key = NULL;
                      e->data = NULL;
                    }
                }
              h->load--;
              return;
            }
          else
            {
              prev = e;
              e = e->next;
            }
        }
    }
  while(puk_hash_entry_oldprobe(h, key)); /* retry as long as rehashing is in progress */
}

static void puk_hashtable_rehash(puk_hashtable_t h)
{
  assert(h->oldtable == NULL);
  h->oldtable = h->table;
  h->oldsize  = h->size;
  h->rehashed = 0;
  int i = 0;
  while(h->size >= puk_hash_primes[i])
    {
      i++;
    }
  h->size = puk_hash_primes[i];
  assert(h->size > 0);
  assert(h->size > h->oldsize);
  h->table = padico_calloc(h->size, sizeof(struct puk_hash_entry_s));
  puk_hash_entry_rehash_n(h, PUK_HASH_PROGRESSIVE_REHASH);
}

size_t puk_hashtable_size(puk_hashtable_t h)
{
  return h->load;
}



/* *** hash functions collection *************************** */

/** one-at-a-time hash function by Bob Jenkins */
uint32_t puk_hash_oneatatime(const unsigned char*key, int len)
{
  register uint32_t z = 0;
  register int i;
  for (i = 0; i < len; i++)
    {
      z += key[i];
      z += (z << 10);
      z ^= (z >> 6);
    }
  z += (z << 3);
  z ^= (z >> 11);
  z += (z << 15);
  return z;
}

#if (SIZEOF_INT == 4)
static inline uint32_t puk_hash_oneatatime32(const uint32_t*key)
{
  register uint32_t z = 0;
  register int i;
  for (i = 0; i < 4; i++)
    {
      z += key[i];
      z += (z << 10);
      z ^= (z >> 6);
    }
  z += (z << 3);
  z ^= (z >> 11);
  z += (z << 15);
  return z;
}
#endif /* SIZEOF_INT == 4 */

#if (SIZEOF_INT == 8)
static inline uint32_t puk_hash_oneatatime64(const uint64_t*key)
{
  register uint32_t z = 0;
  register int i;
  for (i = 0; i < 8; i++)
    {
      z += key[i];
      z += (z << 10);
      z ^= (z >> 6);
    }
  z += (z << 3);
  z ^= (z >> 11);
  z += (z << 15);
  return z;
}
#endif /* SIZEOF_INT == 8 */

int puk_hash_int_eq(const void*key1, const void*key2)
{
  int a = (long)key1;
  int b = (long)key2;
  return a == b;
}

uint32_t puk_hash_int(const void*key)
{
#if (SIZEOF_INT == 4)
  uint32_t a = (long)key;
  return puk_hash_oneatatime32(&a);
#elif (SIZEOF_INT == 8)
  uint64_t a = (long)key;
  return puk_hash_oneatatime64(&a);
#else
  int a = (long)key;
  return puk_hash_oneatatime((void*)&a, sizeof(int));
#endif
}

int puk_hash_string_eq(const void*key1, const void*key2)
{
  return !strcmp((const char*)key1, (const char*)key2);
}
uint32_t puk_hash_string(const void*key)
{
  const unsigned char*s;
  uint32_t z = 0;
  for(s = (const unsigned char*)key; *s; s++)
    {
      z += *s;
      z += (z << 10);
      z ^= (z >> 6);
    }
  z += (z << 3);
  z ^= (z >> 11);
  z += (z << 15);
  return z;
}

int puk_hash_pointer_eq(const void*key1, const void*key2)
{
  return key1 == key2;
}
uint32_t puk_hash_pointer(const void*key)
{
  const uint32_t z = (uint32_t)((((unsigned long)key) >> 4) & 0xFFFFFFFF);
  /*
  const unsigned char*s = (unsigned char*)&key;
  uint32_t z = 0;
  int i;
  for (i = 0; i < sizeof(void*); i++)
    {
      z += s[i];
      z += (z << 10);
      z ^= (z >> 6);
    }
  z += (z << 3);
  z ^= (z >> 11);
  z += (z << 15);
  */
  return z;
}

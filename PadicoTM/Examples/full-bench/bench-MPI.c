/* bench-MPI.c
 */

#include "bench-common.h"
#include <Padico/Module.h>
#include <mpi.h>

int bench_mpi_run(int argc, char**argv);

PADICO_MODULE_DECLARE(bench_MPI, NULL, bench_mpi_run, NULL);

static int peer = -1;
static const int tag = 1;


void mpi_server(char*buffer, size_t lenbuf)
{
  MPI_Status status;
  MPI_Send(buffer, lenbuf, MPI_CHAR, peer, tag, MPI_COMM_WORLD);
  MPI_Recv(buffer, lenbuf, MPI_CHAR, peer, tag, MPI_COMM_WORLD, &status);
}

void mpi_client(char*buffer, size_t lenbuf)
{
  MPI_Status status;
  MPI_Recv(buffer, lenbuf, MPI_CHAR, peer, tag, MPI_COMM_WORLD, &status);
  MPI_Send(buffer, lenbuf, MPI_CHAR, peer, tag, MPI_COMM_WORLD);
}

static const struct bench_desc_s mpi_bench =
{
  "MPI",
  &mpi_server,
  &mpi_client
};

int bench_mpi_run(int argc, char**argv)
{
  static int init_done = 0;
  if(!init_done)
    {
      int self = -1;
      int size = -1;
      MPI_Init(&argc, &argv);
      MPI_Comm_size(MPI_COMM_WORLD, &size);
      MPI_Comm_rank(MPI_COMM_WORLD, &self);
      peer = 1 - self;
      padico_print("bench-MPI: size = %d; rank = %d; peer = %d\n", size, self, peer);
    }
  the_bench(&mpi_bench);
  return 0;
}

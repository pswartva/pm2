/*
 * NewMadeleine
 * Copyright (C) 2015-2024 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */


#ifndef NM_MPI_IO_H
#define NM_MPI_IO_H

/**
 * @ingroup mpi_interface
 * @file
 * declarations for MPI I/O
 * @{
 */

/** @name IO operations
 * @{ */

/** File handle */
typedef int MPI_File;
#define MPI_FILE_NULL ((MPI_File)0)
#define _NM_MPI_FILE_OFFSET 1

/** internal type for IO offsets */
typedef int64_t nm_mpi_offset_t;
/** offsets for MPI-IO */
typedef nm_mpi_offset_t MPI_Offset;
typedef void (MPI_File_errhandler_function)(MPI_File*, int*, ...);
typedef MPI_File_errhandler_function MPI_File_errhandler_fn; /**< pre MPI-2.2 compatibility type */

/** @name File modes
 * @{ */
#define MPI_MODE_RDONLY            0x0001
#define MPI_MODE_RDWR              0x0002
#define MPI_MODE_WRONLY            0x0004
#define MPI_MODE_CREATE            0x0008
#define MPI_MODE_EXCL              0x0010
#define MPI_MODE_APPEND            0x0020
#define MPI_MODE_DELETE_ON_CLOSE   0x0100
#define MPI_MODE_UNIQUE_OPEN       0x0200
#define MPI_MODE_SEQUENTIAL        0x0400
/** @} */

/** @name File seek
 * @{ */
#define MPI_SEEK_SET               0x0001
#define MPI_SEEK_CUR               0x0002
#define MPI_SEEK_END               0x0004
/** @} */

#define MPI_MAX_DATAREP_STRING     256
typedef int MPI_Datarep_extent_function(MPI_Datatype datatype, MPI_Aint*file_extent, void*extra_state);
typedef int MPI_Datarep_conversion_function(void*userbuf,
                                            MPI_Datatype datatype, int count, void*filebuf,
                                            MPI_Offset position, void*extra_state);
#define MPI_CONVERSION_FN_NULL NULL

int MPI_File_open(MPI_Comm comm, const char*filename, int amode, MPI_Info info, MPI_File*fh);
int MPI_File_close(MPI_File*fh);
int MPI_File_delete(const char*filename, MPI_Info info);
int MPI_File_sync(MPI_File fh);
int MPI_File_seek(MPI_File fh, MPI_Offset offset, int whence);
int MPI_File_get_position(MPI_File fh, MPI_Offset*offset);
int MPI_File_get_position_shared(MPI_File fh, MPI_Offset*offset);
int MPI_File_get_byte_offset(MPI_File fh, MPI_Offset offset, MPI_Offset*disp);
int MPI_File_set_size(MPI_File fh, MPI_Offset size);
int MPI_File_preallocate(MPI_File fh, MPI_Offset size);
int MPI_File_get_size(MPI_File fh, MPI_Offset*size);
int MPI_File_get_group(MPI_File fh, MPI_Group*group);
int MPI_File_get_amode(MPI_File fh, int*amode);
int MPI_File_get_type_extent(MPI_File fh, MPI_Datatype datatype, MPI_Aint*extent);
int MPI_File_set_info(MPI_File fh, MPI_Info info);
int MPI_File_get_info(MPI_File fh, MPI_Info*info);
int MPI_File_create_errhandler(MPI_File_errhandler_fn*function, MPI_Errhandler*errhandler);
int MPI_File_set_errhandler(MPI_File file, MPI_Errhandler errhandler);
int MPI_File_get_errhandler(MPI_File file, MPI_Errhandler*errhandler);
int MPI_File_call_errhandler(MPI_File file, int errorcode);
int MPI_File_set_atomicity(MPI_File fh, int flag);
int MPI_File_get_atomicity(MPI_File fh, int*flag);

int MPI_File_set_view(MPI_File fh, MPI_Offset disp, MPI_Datatype etype, MPI_Datatype filetype, const char*datarep, MPI_Info info);
int MPI_File_get_view(MPI_File fh, MPI_Offset*disp, MPI_Datatype*etype, MPI_Datatype *filetype, char *datarep);

int MPI_File_read(MPI_File fh, void*buf, int count, MPI_Datatype datatype, MPI_Status*status);
int MPI_File_write(MPI_File fh, const void*buf, int count, MPI_Datatype datatype, MPI_Status*status);

int MPI_File_read_at(MPI_File fh, MPI_Offset offset, void*buf, int count, MPI_Datatype datatype, MPI_Status*status);
int MPI_File_write_at(MPI_File fh, MPI_Offset offset, const void*buf, int count, MPI_Datatype datatype, MPI_Status*status);

int MPI_File_read_all(MPI_File fh, void*buf, int count, MPI_Datatype datatype, MPI_Status*status);
int MPI_File_write_all(MPI_File fh, const void*buf, int count, MPI_Datatype datatype, MPI_Status*status);
int MPI_File_read_at_all(MPI_File fh, MPI_Offset offset, void*buf, int count, MPI_Datatype datatype, MPI_Status*status);
int MPI_File_write_at_all(MPI_File fh, MPI_Offset offset, const void*buf, int count, MPI_Datatype datatype, MPI_Status*status);

int MPI_File_read_all_begin(MPI_File fh, void*buf, int count, MPI_Datatype datatype);
int MPI_File_read_all_end(MPI_File fh, void*buf, MPI_Status*status);
int MPI_File_write_all_begin(MPI_File fh, const void*buf, int count, MPI_Datatype datatype);
int MPI_File_write_all_end(MPI_File fh, const void*buf, MPI_Status*status);

int MPI_File_read_at_all_begin(MPI_File fh, MPI_Offset offset, void*buf, int count, MPI_Datatype datatype);
int MPI_File_read_at_all_end(MPI_File fh, void*buf, MPI_Status*status);
int MPI_File_write_at_all_begin(MPI_File fh, MPI_Offset offset, const void*buf, int count, MPI_Datatype datatype);
int MPI_File_write_at_all_end(MPI_File fh, const void*buf, MPI_Status*status);

/* not implemented, only stub is available */
int MPI_Register_datarep(const char*datarep,
                         MPI_Datarep_conversion_function*read_conversion_fn,
                         MPI_Datarep_conversion_function*write_conversion_fn,
                         MPI_Datarep_extent_function*dtype_file_extent_fn,
                         void*extra_state);

int MPI_File_iread_at(MPI_File fh, MPI_Offset offset, void*buf, int count, MPI_Datatype datatype, MPI_Request*request);
int MPI_File_iwrite_at(MPI_File fh, MPI_Offset offset, const void*buf, int count, MPI_Datatype datatype, MPI_Request*request);
int MPI_File_iread(MPI_File fh, void*buf, int count, MPI_Datatype datatype, MPI_Request*request);
int MPI_File_iwrite(MPI_File fh, const void*buf, int count, MPI_Datatype datatype, MPI_Request*request);

int MPI_File_seek_shared(MPI_File fh, MPI_Offset offset, int whence);
int MPI_File_write_shared(MPI_File fh, const void*buf, int count, MPI_Datatype datatype, MPI_Status*status);
int MPI_File_iwrite_shared(MPI_File fh, const void*buf, int count, MPI_Datatype datatype, MPI_Request*request);
int MPI_File_read_shared(MPI_File fh, void*buf, int count, MPI_Datatype datatype, MPI_Status*status);
int MPI_File_iread_shared(MPI_File fh, void*buf, int count, MPI_Datatype datatype, MPI_Request*request);
int MPI_File_write_ordered(MPI_File fh, const void*buf, int count, MPI_Datatype datatype, MPI_Status*status);
int MPI_File_read_ordered(MPI_File fh, void*buf, int count, MPI_Datatype datatype, MPI_Status*status);
int MPI_File_write_ordered_begin(MPI_File fh, const void*buf, int count, MPI_Datatype datatype);
int MPI_File_write_ordered_end(MPI_File fh, const void*buf, MPI_Status *status);
int MPI_File_read_ordered_begin(MPI_File fh, void*buf, int count, MPI_Datatype datatype);
int MPI_File_read_ordered_end(MPI_File fh, void *buf, MPI_Status*status);

#ifndef NMAD_FORTRAN_TARGET_NONE

MPI_File MPI_File_f2c(MPI_Fint file);
MPI_Fint MPI_File_c2f(MPI_File file);

#endif /* NMAD_FORTRAN_TARGET_NONE */

/** @} */
/** @} */


#endif /* NM_MPI_IO_H */

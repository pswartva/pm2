/*
 * Bench NBC
 * Copyright (C) 2019-2020 (see AUTHORS file)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#include <stdio.h>
#include <mpi.h>
#include <hwloc.h>


int main(int argc, char **argv)
   {
    int rank,  np;
    int token = 0;
    const int tag  = 666;
    MPI_Status  status;
    hwloc_topology_t topology;
    hwloc_cpuset_t   set;
    int   pu_rank = -1;
    char name[1024];
    int resultlen;



    MPI_Init(&argc,&argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &np);


    hwloc_topology_init(&topology);
    hwloc_topology_load(topology);
    set = hwloc_bitmap_alloc();
    hwloc_get_cpubind(topology,set,0);
    pu_rank = hwloc_bitmap_first(set);

    MPI_Get_processor_name( name, &resultlen );


    MPI_Barrier(MPI_COMM_WORLD);


    if (rank == 0) {
      fprintf(stdout, "Process %d  on core %d of %s\n", rank, pu_rank, name);
      token ++;
      MPI_Send (&token, 1, MPI_INT, rank+1, tag, MPI_COMM_WORLD);
      MPI_Recv (&token, 1, MPI_INT, np-1, tag, MPI_COMM_WORLD, &status);
    } else {
      MPI_Recv (&token, 1, MPI_INT, rank-1, tag, MPI_COMM_WORLD, &status);
      fprintf(stdout, "Process %d  on core %d of %s\n", rank, pu_rank, name);
      token ++;
      MPI_Send (&token, 1, MPI_INT, (rank+1) % np, tag, MPI_COMM_WORLD);
    }

    MPI_Finalize();
  }

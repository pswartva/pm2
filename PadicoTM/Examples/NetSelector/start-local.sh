#! /bin/sh

NS_CONFIG=$PWD/local-ssh.xml
rdvport=29856
peer=pointvirgule

padico-launch -v -c -nodefault -iNetSelector-preset-vanilla -iControler-preset-router -DNS_BASIC_CONF=${NS_CONFIG} -DPADICO_BOOTSTRAP_SYNC=2 -DPADICO_BOOTSTRAP_PORT=${rdvport} -DPADICO_BOOTSTRAP_HOST=${peer} -DBENCH_PEER=${peer} -Droundtrip=10 bench-VIO

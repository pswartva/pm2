#! /bin/bash 

PORT=8080

ENABLE_PADICO=yes

export ALTO_HOST=192.168.30.3 #gridtlse
export BOOTSTRAP_PORT=1234
export BOOTSTRAP_HOST=flille.lille.grid5000.fr

export PADICO_OPTION="-i$PWD/topo-g5k.xml -i$PWD/topo-flille.xml -i$PWD/topo-kvm.xml -v"

config_padico() {
    if [ "x${ENABLE_PADICO}" = "xyes" ]; then
	#PADICO_OPTION="-c -p -DNS_BASIC_CONF=${HOME}/controlssh.xml"
#	PADICO_OPTION="-c -v -p -d"
	export PADICO_LAUNCH="padico-launch ${PADICO_OPTION} -DPADICO_BOOTSTRAP_PORT=${BOOTSTRAP_PORT} -DPADICO_BOOTSTRAP_HOST=${BOOTSTRAP_HOST}"
	export PADICO_LAUNCH_RDV="padico-launch ${PADICO_OPTION} -DPADICO_BOOTSTRAP_LISTEN=${BOOTSTRAP_PORT}"
	PORT=8181
    fi
}

# ## variables used in this script
export TOMCAT_ROOT=/opt/jakarta-tomcat-5.0.28
export BASSO_ROOT=/home/gridtlse/TLSE_BASSO
export ALTO_ROOT=/home/gridtlse/TLSE_ALTO

# ## variables used by sub-programs
export TLSE_INSTALL=${HOME}/TLSE_BASSO
export TLSE_SERVICE_PATH=${TLSE_INSTALL}/service
export TLSE_SOLVER_PATH=${TLSE_INSTALL}/solver
export TLSE_CLIENT_PATH=${TLSE_INSTALL}/client
export TLSE_DAGDA_PATH=${TLSE_INSTALL}/dagda/
export PATH=${TLSE_CLIENT_PATH}/bin:${TLSE_SERVICE_PATH}/bin:${PATH}
export OMNIORB_CONFIG=${HOME}/omniORB.conf


config_omniorb() {
    cat > ${OMNIORB_CONFIG} <<EOF
InitRef = NameService=corbaname::${ALTO_HOST}
giopMaxMsgSize = 33554432
EOF
}

start_BASSO() {
    echo "# start BASSO gridtlse side- ALTO host is ${ALTO_HOST}"
    ${PADICO_LAUNCH} ${BASSO_ROOT}/service/bin/DIET_TLSE_SeD ${BASSO_ROOT}/service/config/TLSE_SeD.cfg ${ALTO_HOST} &
    ${PADICO_LAUNCH} ${BASSO_ROOT}/service/bin/DAGDA_TLSE_SeD ${BASSO_ROOT}/service/config/TLSE_SeD.cfg &
}


# ## start BASSO

config_padico
export DEPLOYMENT="http://${ALTO_HOST}:${PORT}/websolve/Services?type=xml&object=deployment"
config_omniorb
start_BASSO
